ALTER TABLE TASY.CAN_ORDEM_PROD_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAN_ORDEM_PROD_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAN_ORDEM_PROD_MAT
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  NR_SEQ_ORDEM                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  CD_MATERIAL                   NUMBER(6)       NOT NULL,
  QT_DOSE                       NUMBER(15,4)    NOT NULL,
  CD_UNIDADE_MEDIDA             VARCHAR2(30 BYTE) NOT NULL,
  IE_MEDIC_DILUENTE             VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_LOTE_FORNEC            NUMBER(10),
  QT_DOSE_REAL                  NUMBER(17,6)    NOT NULL,
  QT_PERDA                      NUMBER(15,4)    NOT NULL,
  CD_UNIDADE_MEDIDA_REAL        VARCHAR2(30 BYTE),
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_MOTIVO_BAIXA               VARCHAR2(1 BYTE),
  NR_SEQ_ITEM_PRESCR            NUMBER(10),
  CD_UNID_MED_PERDA             VARCHAR2(30 BYTE),
  IE_DEVOLVE_CABINE             VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM_ORIGEM           NUMBER(10),
  CD_ESTABELECIMENTO            NUMBER(4),
  DT_SOLIC_PERDA                DATE,
  NR_PRESCRICAO                 NUMBER(14),
  NR_SEQ_KIT                    NUMBER(6),
  IE_SOBRA_OVERFILL             VARCHAR2(1 BYTE),
  IE_REAPROVEITADO              VARCHAR2(1 BYTE),
  NR_SEQ_PRESCR_KIT             NUMBER(10),
  IE_ESTOQUE_CABINE             VARCHAR2(1 BYTE),
  NR_SEQ_QUIMIO_SOBRA_OVERFILL  NUMBER(10),
  NR_SEQ_SUPERIOR               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CANORPM_CANORIP_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(NR_SEQ_ITEM_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_CANORIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_CANORPR_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_CANORPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_ESTABEL_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_I ON TASY.CAN_ORDEM_PROD_MAT
(NR_SEQ_ORDEM_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPM_MATERIA_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_MATLOFO_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_MATLOFO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CANORPM_PK ON TASY.CAN_ORDEM_PROD_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_PK
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_PRESMAT_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(NR_PRESCRICAO, NR_SEQ_KIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_PRESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_UNIMEDI_FK_I ON TASY.CAN_ORDEM_PROD_MAT
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPM_UNIMEDI_FK1_I ON TASY.CAN_ORDEM_PROD_MAT
(CD_UNIDADE_MEDIDA_REAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_UNIMEDI_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPM_UNIMEDI_FK2_I ON TASY.CAN_ORDEM_PROD_MAT
(CD_UNID_MED_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPM_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.can_ordem_prod_mat_insert
after insert ON TASY.CAN_ORDEM_PROD_MAT for each row
declare
qt_estoque_w		number(15,4);
nr_seq_cabine_w		number(10);
ie_lote_w		varchar2(1);
ie_reaproveitamento	varchar2(1);
ie_tipo_manipulacao_w	varchar2(1);
ie_arredonda_dose_w	varchar2(1);
ie_via_aplicacao_w	varchar2(1);
qt_dose_real_w		number(18,6);
ie_atualiza_validade_w	varchar2(1);
dt_val_ordem_w		date;
dt_val_ordem_ww		date;
nr_seq_marca_w		number(15);
ie_sobras_overfill_w	varchar2(1);
ie_arredonda_dose_cabine_w	varchar2(15);
ie_baixa_kit_w varchar2(1 char);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

if (nvl(:new.ie_sobra_overfill,'N') not in ('S','O')) then

	qt_estoque_w := 0;

	Obter_Param_Usuario(3130,218,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_tipo_manipulacao_w);
	Obter_Param_Usuario(3130, 286, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_arredonda_dose_w);
	Obter_Param_Usuario(3130, 418, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_arredonda_dose_cabine_w);
	Obter_Param_Usuario(3130,369,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_validade_w);
    obter_param_usuario(3130, 366, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_sobras_overfill_w);
    obter_param_usuario(3130, 511, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_baixa_kit_w);

	select	b.nr_seq_cabine
	into	nr_seq_cabine_w
	from	far_etapa_producao b,
			can_ordem_prod a
	where	a.nr_sequencia 	= 	:new.nr_seq_ordem
	and		b.nr_sequencia 		= 	a.nr_seq_etapa_prod;

	ie_lote_w	:= 'N';


	if 	(nvl(:new.ie_reaproveitado,'N') = 'N') and
		(:new.nr_seq_ordem_origem is null) and
		((ie_tipo_manipulacao_w <> 'T') or (:new.ie_estoque_cabine = 'S')) and
		(:new.nr_seq_kit is null or ie_baixa_kit_w = 'S') then

		/*select	nvl(sum(qt_estoque),0)
		into	qt_estoque_w
		from	far_estoque_cabine
		where	nr_seq_cabine	= nr_seq_cabine_w
		and	cd_material	= :new.cd_material
		and	nr_seq_lote_fornec = :new.nr_seq_lote_fornec;*/

		select 	sum(x)
		into   	qt_estoque_w
		from (	select	nvl(sum(qt_estoque),0)   x
				from	far_estoque_cabine
				where	nr_seq_cabine	= nr_seq_cabine_w
				and		cd_material	= :new.cd_material
				and		nr_seq_lote_fornec  = :new.nr_seq_lote_fornec
				union
				select	nvl(sum(qt_saldo),0)  x
				from	quimio_sobra_overfill
				where	nr_seq_cabine	= nr_seq_cabine_w
				and		cd_material	=  :new.cd_material
				and		nr_seq_lote_fornec  = :new.nr_seq_lote_fornec
				and		qt_saldo > 0
				and		:new.ie_sobra_overfill = 'S');

		ie_lote_w	:= 'S';

			if (qt_estoque_w = 0) then
				/*select	NVL(sum(qt_estoque),0)
				into	qt_estoque_w
				from	far_estoque_cabine
				where	nr_seq_cabine	= nr_seq_cabine_w
				and		cd_material	= :new.cd_material
				and		nr_seq_lote_fornec is null;*/

				select 	sum(x)
				into   	qt_estoque_w
				from (	select	nvl(sum(qt_estoque),0)   x
						from	far_estoque_cabine
						where	nr_seq_cabine	= nr_seq_cabine_w
						and		cd_material	= :new.cd_material
						and		nr_seq_lote_fornec is null
						union
						select	nvl(sum(qt_saldo),0)  x
						from	quimio_sobra_overfill
						where	nr_seq_cabine	= nr_seq_cabine_w
						and		cd_material	=  :new.cd_material
						and		nr_seq_lote_fornec is null
						and		qt_saldo > 0
						and		:new.ie_sobra_overfill = 'S');


				if (qt_estoque_w = 0) and
					(ie_sobras_overfill_w <> 'S')then
					wheb_mensagem_pck.exibir_mensagem_abort(186924);
				end if;

				ie_lote_w	:= 'N';

			end if;

		qt_dose_real_w := :new.qt_dose_real;

		if 	(ie_arredonda_dose_w = 'S') and (ie_arredonda_dose_cabine_w = 'S') then
			qt_dose_real_w := Obter_qt_dose_onco(:new.cd_material,:new.qt_dose_real,ie_via_aplicacao_w);
		end if;

		qt_estoque_w	:= qt_estoque_w - qt_dose_real_w;

			if	(ie_lote_w = 'S') then
			if	(qt_estoque_w > 0) then
				update	far_estoque_cabine
				set	qt_estoque	= qt_estoque_w
				where	nr_seq_cabine	= nr_seq_cabine_w
				and	cd_material	= :new.cd_material
				and	nr_seq_lote_fornec = :new.nr_seq_lote_fornec
				and	nvl(:new.ie_sobra_overfill,'N') = 'N';
			else
				delete	far_estoque_cabine
				where	nr_seq_cabine	= nr_seq_cabine_w
				and	cd_material	= :new.cd_material
				and	nr_seq_lote_fornec = :new.nr_seq_lote_fornec
				and	nvl(:new.ie_sobra_overfill,'N') = 'N';
			end if;
		else
			if	(qt_estoque_w > 0) then
				update	far_estoque_cabine
				set	qt_estoque	= qt_estoque_w
				where	nr_seq_cabine	= nr_seq_cabine_w
				and	cd_material	= :new.cd_material
				and	nvl(:new.ie_sobra_overfill,'N') = 'N';
			else
				delete	far_estoque_cabine
				where	nr_seq_cabine	= nr_seq_cabine_w
				and	cd_material	= :new.cd_material
				and	nvl(:new.ie_sobra_overfill,'N') = 'N';
			end if;
		end if;
	end if;



	if 	(ie_atualiza_validade_w = 'S') then

		select 	sysdate + qt_obter_estabilidade_mat(:new.cd_material, :new.nr_seq_lote_fornec) / 24
		into	dt_val_ordem_w
		from	dual;

		select 	max(dt_validade_ordem)
		into	dt_val_ordem_ww
		from	can_ordem_prod
		where   nr_sequencia = :new.nr_seq_ordem;

		if 	(dt_val_ordem_w < dt_val_ordem_ww) or
			(dt_val_ordem_ww is null) then

			update 	can_ordem_prod
			set 	dt_validade_ordem 	= 	dt_val_ordem_w,
					dt_validade_rotulo 	= 	dt_val_ordem_w
			where	nr_sequencia		= 	:new.nr_seq_ordem;

		end if;
	end if;
end if;

delete	from far_estoque_cabine
where	cd_material	=	:new.cd_material
and		qt_estoque <= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.Can_ordem_prod_mat_delete
after delete ON TASY.CAN_ORDEM_PROD_MAT FOR EACH ROW
DECLARE
nr_seq_cabine_w		Number(10);
cd_estab_w			Number(4);
ie_baixa_kit_w varchar2(1 char);

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

obter_param_usuario(3130, 511, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_baixa_kit_w);

select	b.nr_seq_cabine
into	nr_seq_cabine_w
from	far_etapa_producao b,
	can_ordem_prod a
where	a.nr_sequencia = :old.nr_seq_ordem
and	b.nr_sequencia = a.nr_seq_etapa_prod;

select	cd_estabelecimento
into	cd_estab_w
from	far_cabine_seg_biol
where	nr_sequencia = nr_seq_cabine_w;

begin
if 	(nvl(:old.ie_devolve_cabine,'S') <> 'N') and
	(:old.nr_seq_kit is null or ie_baixa_kit_w = 'S') and
	(:old.IE_SOBRA_OVERFILL not in ('Y','X','Z')) then --nao tratar diluentes dos medic diluidos
	wheb_usuario_pck.set_ie_commit('N');
	Adiciona_item_cabine(:old.cd_material, :old.qt_dose_real, :old.nr_seq_lote_fornec, :old.nm_usuario, nr_seq_cabine_w, :old.IE_SOBRA_OVERFILL, :old.NR_SEQ_ORDEM_ORIGEM);
	wheb_usuario_pck.set_ie_commit('S');
end if;
if 	(nvl(:old.ie_devolve_cabine,'S') <> 'N') and
	(:old.nr_seq_kit is null or ie_baixa_kit_w = 'S') and
	(:old.IE_SOBRA_OVERFILL in ('Y','X')) then
--restaura a dose do medicamento diluido
	update	quimio_sobra_overfill
	set		qt_saldo = qt_saldo + round(Obter_conversao_ml(:old.cd_material, :old.qt_dose_real, :old.cd_unidade_medida_real),3)
	where	nr_sequencia = :old.nr_seq_quimio_sobra_overfill;
end if;
exception
	when others then
      	nr_seq_cabine_w := 1;
end;
END;
/


ALTER TABLE TASY.CAN_ORDEM_PROD_MAT ADD (
  CONSTRAINT CANORPM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAN_ORDEM_PROD_MAT ADD (
  CONSTRAINT CANORPM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CANORPM_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_KIT) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CANORPM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CANORPM_CANORIP_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PRESCR) 
 REFERENCES TASY.CAN_ORDEM_ITEM_PRESCR (NR_SEQUENCIA),
  CONSTRAINT CANORPM_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MED_PERDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CANORPM_CANORPR_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.CAN_ORDEM_PROD (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CANORPM_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CANORPM_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT CANORPM_UNIMEDI_FK1 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_REAL) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.CAN_ORDEM_PROD_MAT TO NIVEL_1;

