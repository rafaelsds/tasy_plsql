ALTER TABLE TASY.MATERIAL_COD_BARRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_COD_BARRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_COD_BARRA
(
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  CD_BARRA_MATERIAL         VARCHAR2(255 BYTE)  NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_CGC_FABRICANTE         VARCHAR2(14 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_FORNEC        NUMBER(10),
  QT_MATERIAL               NUMBER(10),
  IE_CONSIDERAR_FORNECEDOR  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATCOBA_MATERIA_FK ON TASY.MATERIAL_COD_BARRA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATCOBA_MATLOFO_FK_I ON TASY.MATERIAL_COD_BARRA
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATCOBA_MATLOFO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATCOBA_PESJURI_FK ON TASY.MATERIAL_COD_BARRA
(CD_CGC_FABRICANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATCOBA_PESJURI_FK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATCOBA_PK ON TASY.MATERIAL_COD_BARRA
(CD_BARRA_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_cod_barra_atual
before insert or update ON TASY.MATERIAL_COD_BARRA for each row
declare

qt_existe_w			number(3);
ie_swisslog_w			varchar2(1);
ds_param_integ_hl7_w		varchar2(4000) := '';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_estrut_int_w		mat_estrutura.nr_sequencia%type;

begin
cd_estabelecimento_w := obter_estabelecimento_ativo;

if	(:new.ie_considerar_fornecedor <> :old.ie_considerar_fornecedor) then
	-- Rotina para envio de materiais para integração Swisslog
	begin
	select	nr_seq_estrut_int
	into	nr_seq_estrut_int_w
	from	parametros_farmacia
	where	cd_estabelecimento = cd_estabelecimento_w;

	select	consistir_se_mat_contr_estrut(nr_seq_estrut_int_w,:new.cd_material)
	into	ie_swisslog_w
	from	dual;
	exception
	when others then
		nr_seq_estrut_int_w := 0;
		ie_swisslog_w := 'N';
	end;

	if	(nvl(ie_swisslog_w,'N') = 'S') then
		ds_param_integ_hl7_w := 'cd_material=' || :new.cd_material || obter_separador_bv;
		swisslog_gerar_integracao(438, ds_param_integ_hl7_w);
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.dankia_mat_cod_barra_insert
after insert or update ON TASY.MATERIAL_COD_BARRA for each row
declare
qt_registro_w	number(5);
BEGIN

gerar_int_dankia_pck.dankia_disp_barras_req(:new.cd_material,null,0,:new.cd_barra_material,:new.nm_usuario);

END dankia_mat_cod_barra_insert;
/


ALTER TABLE TASY.MATERIAL_COD_BARRA ADD (
  CONSTRAINT MATCOBA_PK
 PRIMARY KEY
 (CD_BARRA_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_COD_BARRA ADD (
  CONSTRAINT MATCOBA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATCOBA_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FABRICANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATCOBA_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MATERIAL_COD_BARRA TO NIVEL_1;

