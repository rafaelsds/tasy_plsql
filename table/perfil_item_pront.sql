ALTER TABLE TASY.PERFIL_ITEM_PRONT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ITEM_PRONT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ITEM_PRONT
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_PERFIL                 NUMBER(5)           NOT NULL,
  NR_SEQ_ITEM_PRONT         NUMBER(10)          NOT NULL,
  IE_ATUALIZAR              VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_APRES              NUMBER(5)           NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_CHECKUP                VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_APRES_CHECKUP      NUMBER(5)           NOT NULL,
  QT_MIN_ALTA               NUMBER(4),
  IE_ATUALIZAR_ALTA         VARCHAR2(1 BYTE),
  IE_ATUALIZAR_OBITO        VARCHAR2(1 BYTE),
  QT_MIN_OBITO              NUMBER(4),
  QT_HORAS_INATIVACAO       NUMBER(10),
  DS_ITEM_INSTITUICAO       VARCHAR2(40 BYTE),
  QT_TAMANHO_FONTE          NUMBER(10),
  DS_FONTE                  VARCHAR2(30 BYTE),
  DS_COR_FONTE              VARCHAR2(30 BYTE),
  IE_FIM_CONTA              VARCHAR2(1 BYTE),
  IE_ESTABELECIMENTO        VARCHAR2(1 BYTE),
  IE_VISUALIZA_INATIVO      VARCHAR2(1 BYTE),
  IE_LIBERAR_IMPRESSAO      VARCHAR2(1 BYTE),
  IE_CONFIGURAR_RELATORIO   VARCHAR2(1 BYTE),
  DS_ALERTA_IMPRESSAO       VARCHAR2(255 BYTE),
  IE_INATIVAR               VARCHAR2(1 BYTE),
  IE_SOMENTE_TEMPLATE       VARCHAR2(1 BYTE),
  IE_ATUALIZAR_TEMPLATE     VARCHAR2(1 BYTE),
  QT_REFRESH_TELA           NUMBER(10),
  IE_IMPRIMIR_LIBERAR       VARCHAR2(1 BYTE),
  QT_MIN_IMPRESSAO_ALTA     NUMBER(10),
  QT_DIAS_FILTRO            NUMBER(3),
  DS_PASTA_TEMPLATE         VARCHAR2(50 BYTE),
  IE_ADEP                   VARCHAR2(1 BYTE),
  NR_SEQ_PASTA_INICIAL      NUMBER(10),
  IE_IMP_NAO_LIBERADO       VARCHAR2(1 BYTE),
  IE_QUEST_TEXTO_PADRAO     VARCHAR2(1 BYTE),
  IE_SENHA_LIBERACAO        VARCHAR2(1 BYTE),
  IE_SENHA_INATIVACAO       VARCHAR2(1 BYTE),
  QT_DIAS_FILTRO_FUTURO     NUMBER(3),
  IE_IMPRIME_INATIVO        VARCHAR2(1 BYTE),
  IE_FORMATO_SAME           VARCHAR2(1 BYTE),
  IE_ATUALIZAR_SEM_ATEND    VARCHAR2(1 BYTE),
  QT_DIAS_TODAS             NUMBER(3),
  IE_FORMA_RESTRICAO        VARCHAR2(3 BYTE),
  IE_ATUALIZAR_ALTA_MEDICA  VARCHAR2(1 BYTE),
  IE_TEMPLATE_ITEM          VARCHAR2(1 BYTE),
  QT_HORAS_ATUA_ALTA        NUMBER(10),
  IE_TEMPLATE_SALVAR        VARCHAR2(1 BYTE),
  DS_COR_ARVORE_HTML        VARCHAR2(30 BYTE),
  IE_TEMPLATE_ARVORE        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERITPR_I1 ON TASY.PERFIL_ITEM_PRONT
(NR_SEQ_ITEM_PRONT, CD_PERFIL, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERITPR_PERFIL_FK_I ON TASY.PERFIL_ITEM_PRONT
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERITPR_PK ON TASY.PERFIL_ITEM_PRONT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERITPR_PROITEM_FK_I ON TASY.PERFIL_ITEM_PRONT
(NR_SEQ_ITEM_PRONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERITPR_PROPAST_FK_I ON TASY.PERFIL_ITEM_PRONT
(NR_SEQ_PASTA_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERITPR_UK ON TASY.PERFIL_ITEM_PRONT
(CD_PERFIL, NR_SEQ_ITEM_PRONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PERFIL_ITEM_PRONT_tp  after update ON TASY.PERFIL_ITEM_PRONT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ATUALIZAR_ALTA_MEDICA,1,4000),substr(:new.IE_ATUALIZAR_ALTA_MEDICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_ALTA_MEDICA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_RESTRICAO,1,4000),substr(:new.IE_FORMA_RESTRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_RESTRICAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TEMPLATE_SALVAR,1,4000),substr(:new.IE_TEMPLATE_SALVAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TEMPLATE_SALVAR',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_HORAS_ATUA_ALTA,1,4000),substr(:new.QT_HORAS_ATUA_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_ATUA_ALTA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR,1,4000),substr(:new.IE_ATUALIZAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ITEM_PRONT,1,4000),substr(:new.NR_SEQ_ITEM_PRONT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ITEM_PRONT',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES_CHECKUP,1,4000),substr(:new.NR_SEQ_APRES_CHECKUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES_CHECKUP',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CHECKUP,1,4000),substr(:new.IE_CHECKUP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CHECKUP',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_ALTA,1,4000),substr(:new.IE_ATUALIZAR_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_ALTA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_OBITO,1,4000),substr(:new.IE_ATUALIZAR_OBITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_OBITO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_ALTA,1,4000),substr(:new.QT_MIN_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_ALTA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_OBITO,1,4000),substr(:new.QT_MIN_OBITO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_OBITO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAMANHO_FONTE,1,4000),substr(:new.QT_TAMANHO_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAMANHO_FONTE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONTE,1,4000),substr(:new.DS_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONTE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR_FONTE,1,4000),substr(:new.DS_COR_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR_FONTE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_HORAS_INATIVACAO,1,4000),substr(:new.QT_HORAS_INATIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_INATIVACAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM_INSTITUICAO,1,4000),substr(:new.DS_ITEM_INSTITUICAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM_INSTITUICAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FIM_CONTA,1,4000),substr(:new.IE_FIM_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FIM_CONTA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTABELECIMENTO,1,4000),substr(:new.IE_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTABELECIMENTO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VISUALIZA_INATIVO,1,4000),substr(:new.IE_VISUALIZA_INATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VISUALIZA_INATIVO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ALERTA_IMPRESSAO,1,4000),substr(:new.DS_ALERTA_IMPRESSAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ALERTA_IMPRESSAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INATIVAR,1,4000),substr(:new.IE_INATIVAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_INATIVAR',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONFIGURAR_RELATORIO,1,4000),substr(:new.IE_CONFIGURAR_RELATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONFIGURAR_RELATORIO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIBERAR_IMPRESSAO,1,4000),substr(:new.IE_LIBERAR_IMPRESSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERAR_IMPRESSAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_TEMPLATE,1,4000),substr(:new.IE_ATUALIZAR_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_TEMPLATE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_REFRESH_TELA,1,4000),substr(:new.QT_REFRESH_TELA,1,4000),:new.nm_usuario,nr_seq_w,'QT_REFRESH_TELA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_TEMPLATE,1,4000),substr(:new.IE_SOMENTE_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_TEMPLATE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRIMIR_LIBERAR,1,4000),substr(:new.IE_IMPRIMIR_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRIMIR_LIBERAR',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_IMPRESSAO_ALTA,1,4000),substr(:new.QT_MIN_IMPRESSAO_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_IMPRESSAO_ALTA',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_FILTRO,1,4000),substr(:new.QT_DIAS_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_FILTRO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PASTA_TEMPLATE,1,4000),substr(:new.DS_PASTA_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'DS_PASTA_TEMPLATE',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ADEP,1,4000),substr(:new.IE_ADEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_ADEP',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PASTA_INICIAL,1,4000),substr(:new.NR_SEQ_PASTA_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PASTA_INICIAL',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMP_NAO_LIBERADO,1,4000),substr(:new.IE_IMP_NAO_LIBERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMP_NAO_LIBERADO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QUEST_TEXTO_PADRAO,1,4000),substr(:new.IE_QUEST_TEXTO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUEST_TEXTO_PADRAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_LIBERACAO,1,4000),substr(:new.IE_SENHA_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_LIBERACAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_INATIVACAO,1,4000),substr(:new.IE_SENHA_INATIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_INATIVACAO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_FILTRO_FUTURO,1,4000),substr(:new.QT_DIAS_FILTRO_FUTURO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_FILTRO_FUTURO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRIME_INATIVO,1,4000),substr(:new.IE_IMPRIME_INATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRIME_INATIVO',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_SEM_ATEND,1,4000),substr(:new.IE_ATUALIZAR_SEM_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_SEM_ATEND',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMATO_SAME,1,4000),substr(:new.IE_FORMATO_SAME,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMATO_SAME',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_TODAS,1,4000),substr(:new.QT_DIAS_TODAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_TODAS',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TEMPLATE_ITEM,1,4000),substr(:new.IE_TEMPLATE_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TEMPLATE_ITEM',ie_log_w,ds_w,'PERFIL_ITEM_PRONT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PERFIL_ITEM_PRONT ADD (
  CONSTRAINT PERITPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PERITPR_UK
 UNIQUE (CD_PERFIL, NR_SEQ_ITEM_PRONT)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ITEM_PRONT ADD (
  CONSTRAINT PERITPR_PROITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PRONT) 
 REFERENCES TASY.PRONTUARIO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PERITPR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT PERITPR_PROPAST_FK 
 FOREIGN KEY (NR_SEQ_PASTA_INICIAL) 
 REFERENCES TASY.PRONTUARIO_PASTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PERFIL_ITEM_PRONT TO NIVEL_1;

