ALTER TABLE TASY.SEGMENTO_COMPRAS_ESTRUT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SEGMENTO_COMPRAS_ESTRUT CASCADE CONSTRAINTS;

CREATE TABLE TASY.SEGMENTO_COMPRAS_ESTRUT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SEGMENTO       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_MATERIAL           NUMBER(10),
  NR_SEQ_FAMILIA        NUMBER(10),
  NR_SEQ_ESTRUT_MAT     NUMBER(10),
  IE_CURVA_ABC          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SECOEST_CLAMATE_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_GRUMATE_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_MATERIA_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_MATESTR_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_MATESTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_MATFAMI_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_MATFAMI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SECOEST_PK ON TASY.SEGMENTO_COMPRAS_ESTRUT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_PK
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_SEGCOMP_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(NR_SEQ_SEGMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_SEGCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SECOEST_SUBMATE_FK_I ON TASY.SEGMENTO_COMPRAS_ESTRUT
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SECOEST_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SEGMENTO_COMPRAS_ESTRUT_INSERT
after insert or update ON TASY.SEGMENTO_COMPRAS_ESTRUT for each row
declare

cd_estabelecimento_w	number(10);
qt_existe_w		number(10);

PRAGMA AUTONOMOUS_TRANSACTION;

begin

if	(:new.cd_material is not null) then
	begin

	select	count(*)
	into	qt_existe_w
	from	segmento_compras b,
		segmento_compras_estrut a
	where	b.nr_sequencia		= a.nr_seq_segmento
	and	b.nr_sequencia		<> :new.nr_seq_segmento
	and	a.cd_material		= :new.cd_material
	and	b.cd_estabelecimento is null;

	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(215676,'CD_MATERIAL_P='||:new.cd_material);
		/*r.aise_application_error(-20011,'Este material j� existe em uma regra geral (para todos estabelecimentos).');*/
	end if;


	select	nvl(max(cd_estabelecimento), 0)
	into	cd_estabelecimento_w
	from	segmento_compras
	where	nr_sequencia = :new.nr_seq_segmento;


	select	count(*)
	into	qt_existe_w
	from	segmento_compras b,
		segmento_compras_estrut a
	where	b.nr_sequencia		= a.nr_seq_segmento
	and	b.nr_sequencia		<> :new.nr_seq_segmento
	and	a.cd_material		= :new.cd_material
	and	b.cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(215677,'CD_MATERIAL_P='||:new.cd_material);
		/*r.aise_application_error(-20011,'Este material j� existe nesta regra para este estabelecimento.');*/
	end if;

	select	count(*)
	into	qt_existe_w
	from	segmento_compras b,
		segmento_compras_estrut a
	where	b.nr_sequencia		= a.nr_seq_segmento
	and	b.nr_sequencia		= :new.nr_seq_segmento
	and	a.nr_sequencia		<> :new.nr_sequencia
	and	a.cd_material		= :new.cd_material;

	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(215679,'CD_MATERIAL_P='||:new.cd_material);
		/*r.aise_application_error(-20011,'Este material j� existe neste segmento.');*/
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.SEGMENTO_COMPRAS_ESTRUT ADD (
  CONSTRAINT SECOEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SEGMENTO_COMPRAS_ESTRUT ADD (
  CONSTRAINT SECOEST_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT SECOEST_SEGCOMP_FK 
 FOREIGN KEY (NR_SEQ_SEGMENTO) 
 REFERENCES TASY.SEGMENTO_COMPRAS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SECOEST_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT SECOEST_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT SECOEST_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT SECOEST_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT SECOEST_MATESTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.MAT_ESTRUTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SEGMENTO_COMPRAS_ESTRUT TO NIVEL_1;

