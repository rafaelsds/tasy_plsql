ALTER TABLE TASY.SUS_ID_LAUDO_ACH
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_ID_LAUDO_ACH CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_ID_LAUDO_ACH
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTERNO       NUMBER(10)               NOT NULL,
  DS_ID_LAUDO_ACH      VARCHAR2(20 BYTE)        NOT NULL,
  NR_SEQ_LOTE          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSIDACH_I2 ON TASY.SUS_ID_LAUDO_ACH
(NR_SEQ_INTERNO, DS_ID_LAUDO_ACH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE UNIQUE INDEX TASY.SUSIDACH_PK ON TASY.SUS_ID_LAUDO_ACH
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSIDACH_SUSLAPA_FK_I ON TASY.SUS_ID_LAUDO_ACH
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSIDACH_SUSLOAU_FK_I ON TASY.SUS_ID_LAUDO_ACH
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_ID_LAUDO_ACH ADD (
  CONSTRAINT SUSIDACH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_ID_LAUDO_ACH ADD (
  CONSTRAINT SUSIDACH_SUSLOAU_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.SUS_LOTE_AUTOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUSIDACH_SUSLAPA_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.SUS_LAUDO_PACIENTE (NR_SEQ_INTERNO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUS_ID_LAUDO_ACH TO NIVEL_1;

