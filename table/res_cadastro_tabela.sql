ALTER TABLE TASY.RES_CADASTRO_TABELA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RES_CADASTRO_TABELA CASCADE CONSTRAINTS;

CREATE TABLE TASY.RES_CADASTRO_TABELA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NM_TABELA                 VARCHAR2(50 BYTE)   NOT NULL,
  NM_ATRIBUTO_CONSULTA      VARCHAR2(50 BYTE)   NOT NULL,
  NM_ATRIBUTO_LIBERACAO     VARCHAR2(50 BYTE),
  NM_TABELA_SUPERIOR        VARCHAR2(50 BYTE),
  NM_ATRIBUTO_PROFISSIONAL  VARCHAR2(50 BYTE),
  NR_SEQ_CAD_MACRO          NUMBER(10)          NOT NULL,
  DS_SQL                    VARCHAR2(4000 BYTE),
  DS_DOCUMENTACAO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RECATAB_PK ON TASY.RES_CADASTRO_TABELA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATAB_RECAMAC_FK_I ON TASY.RES_CADASTRO_TABELA
(NR_SEQ_CAD_MACRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATAB_TABSIST_FK_I ON TASY.RES_CADASTRO_TABELA
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RES_CADASTRO_TABELA ADD (
  CONSTRAINT RECATAB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RES_CADASTRO_TABELA ADD (
  CONSTRAINT RECATAB_RECAMAC_FK 
 FOREIGN KEY (NR_SEQ_CAD_MACRO) 
 REFERENCES TASY.RES_CADASTRO_MACRO (NR_SEQUENCIA),
  CONSTRAINT RECATAB_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA));

GRANT SELECT ON TASY.RES_CADASTRO_TABELA TO NIVEL_1;

