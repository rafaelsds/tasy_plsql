ALTER TABLE TASY.PTU_AVISO_RET_GUIA_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_AVISO_RET_GUIA_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_AVISO_RET_GUIA_GLOSA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RET_GUIA      NUMBER(10)               NOT NULL,
  CD_GLOSA             VARCHAR2(10 BYTE),
  DS_GLOSA             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUARGG_PK ON TASY.PTU_AVISO_RET_GUIA_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUARGG_PTUARGU_FK_I ON TASY.PTU_AVISO_RET_GUIA_GLOSA
(NR_SEQ_RET_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_AVISO_RET_GUIA_GLOSA ADD (
  CONSTRAINT PTUARGG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_AVISO_RET_GUIA_GLOSA ADD (
  CONSTRAINT PTUARGG_PTUARGU_FK 
 FOREIGN KEY (NR_SEQ_RET_GUIA) 
 REFERENCES TASY.PTU_AVISO_RET_GUIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_AVISO_RET_GUIA_GLOSA TO NIVEL_1;

