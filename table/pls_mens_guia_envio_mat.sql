ALTER TABLE TASY.PLS_MENS_GUIA_ENVIO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MENS_GUIA_ENVIO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MENS_GUIA_ENVIO_MAT
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_GUIA_ENVIO          NUMBER(10)         NOT NULL,
  NR_SEQ_CONTA_MAT           NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL            NUMBER(10),
  NR_SEQ_MATERIAL_CONVERSAO  NUMBER(10),
  DT_ATENDIMENTO             DATE,
  NR_SEQ_TISS_TABELA         NUMBER(10),
  VL_UNITARIO                NUMBER(15,2),
  VL_MATERIAL                NUMBER(15,2),
  QT_MATERIAL                NUMBER(12,4),
  IE_TIPO_DESPESA            VARCHAR2(1 BYTE),
  CD_MATERIAL_OPS            VARCHAR2(20 BYTE),
  TX_REDUCAO_ACRESCIMO       NUMBER(7,4),
  CD_MATERIAL_IMP            VARCHAR2(20 BYTE),
  VL_BENEFICIARIO            NUMBER(15,2),
  VL_BENEFICIARIO_UNIT       NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMGEM_PK ON TASY.PLS_MENS_GUIA_ENVIO_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMGEM_PLSCOMAT_FK_I ON TASY.PLS_MENS_GUIA_ENVIO_MAT
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGEM_PLSCOMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMGEM_PLSMEGE_FK_I ON TASY.PLS_MENS_GUIA_ENVIO_MAT
(NR_SEQ_GUIA_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGEM_PLSMEGE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_MENS_GUIA_ENVIO_MAT ADD (
  CONSTRAINT PLSMGEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MENS_GUIA_ENVIO_MAT ADD (
  CONSTRAINT PLSMGEM_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSMGEM_PLSMEGE_FK 
 FOREIGN KEY (NR_SEQ_GUIA_ENVIO) 
 REFERENCES TASY.PLS_MENSALIDADE_GUIA_ENVIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MENS_GUIA_ENVIO_MAT TO NIVEL_1;

