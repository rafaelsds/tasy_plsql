ALTER TABLE TASY.PAN_CONFIGURACAO_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAN_CONFIGURACAO_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAN_CONFIGURACAO_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_TEMPO_ADMISSAO     NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PACOSET_PK ON TASY.PAN_CONFIGURACAO_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACOSET_SETATEN_FK_I ON TASY.PAN_CONFIGURACAO_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PAN_CONFIGURACAO_SETOR ADD (
  CONSTRAINT PACOSET_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PAN_CONFIGURACAO_SETOR ADD (
  CONSTRAINT PACOSET_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PAN_CONFIGURACAO_SETOR TO NIVEL_1;

