ALTER TABLE TASY.SAN_TABELA_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_TABELA_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_TABELA_INTEGRACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TABELA              VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_INTEGRACAO      NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIPO_INTEG_TASYGER  VARCHAR2(1 BYTE),
  NR_SEQ_PROJ_XML        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANTAIN_PK ON TASY.SAN_TABELA_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTAIN_SANINTE_FK_I ON TASY.SAN_TABELA_INTEGRACAO
(NR_SEQ_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTAIN_XMLPROJ_FK_I ON TASY.SAN_TABELA_INTEGRACAO
(NR_SEQ_PROJ_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.san_tab_integracao_tipo
before insert or update ON TASY.SAN_TABELA_INTEGRACAO for each row
declare

begin

if	(((:new.nr_seq_integracao is not null) and
	((:new.ie_tipo_integ_tasyger is not null) or
	(:new.nr_seq_proj_xml is not null))) or
	((:new.ie_tipo_integ_tasyger is not null) and
	((:new.nr_seq_integracao is not null) or
	(:new.nr_seq_proj_xml is not null))) or
	((:new.nr_seq_proj_xml is not null) and
	((:new.nr_seq_integracao is not null) or
	(:new.ie_tipo_integ_tasyger is not null)))) or
	((:new.nr_seq_proj_xml is null) and
	(:new.nr_seq_integracao is null) and
	(:new.ie_tipo_integ_tasyger is null)) then

	wheb_mensagem_pck.exibir_mensagem_abort(729797);

end if;

end;
/


ALTER TABLE TASY.SAN_TABELA_INTEGRACAO ADD (
  CONSTRAINT SANTAIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_TABELA_INTEGRACAO ADD (
  CONSTRAINT SANTAIN_SANINTE_FK 
 FOREIGN KEY (NR_SEQ_INTEGRACAO) 
 REFERENCES TASY.SAN_INTEGRACAO (NR_SEQUENCIA),
  CONSTRAINT SANTAIN_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_XML) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_TABELA_INTEGRACAO TO NIVEL_1;

