ALTER TABLE TASY.ESCALA_SAPS3
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SAPS3 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SAPS3
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  IE_TIPO_ADMISSAO           NUMBER(2)          NOT NULL,
  IE_AIDS                    VARCHAR2(1 BYTE)   NOT NULL,
  IE_MALIG_HEMATOLOGICA      VARCHAR2(1 BYTE)   NOT NULL,
  QT_PLAQUETAS               NUMBER(15),
  QT_GLASGOW                 NUMBER(2),
  QT_IDADE                   NUMBER(3),
  QT_PA_SISTOLICA            NUMBER(3),
  QT_FREQ_CARDIACA           NUMBER(3),
  QT_TEMP                    NUMBER(4,1),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  QT_GLOBULOS_BRANCOS        NUMBER(15,3),
  QT_BILIRRUBINA             NUMBER(15,2),
  IE_ORIGEM_PACIENTE         NUMBER(1)          NOT NULL,
  QT_PH                      NUMBER(15,4),
  QT_CREATININA              NUMBER(6,2),
  QT_DIAS_INTERNACAO_PREV    NUMBER(4),
  IE_QUIMIOTERAPIA           VARCHAR2(1 BYTE),
  IE_CIRROSE                 VARCHAR2(1 BYTE),
  IE_METASTASE               VARCHAR2(1 BYTE),
  IE_ICC_NYHA                VARCHAR2(1 BYTE),
  IE_FAMACOS_VASOATIVOS      VARCHAR2(1 BYTE),
  IE_URGENCIA                NUMBER(3),
  IE_TIPO_OPERACAO           NUMBER(3),
  IE_ADMISSAO_UTI            VARCHAR2(1 BYTE),
  IE_NEUROLOGICA             NUMBER(2),
  IE_CARDIOLOGICA            NUMBER(3),
  IE_ABDOMEM                 NUMBER(3),
  IE_INFEC_NOSOCOMIAL        VARCHAR2(1 BYTE),
  IE_INFEC_RESP              VARCHAR2(1 BYTE),
  IE_OXIGENACAO              NUMBER(3),
  QT_PONTUACAO               NUMBER(4),
  PR_RISCO                   NUMBER(6,2)        DEFAULT null,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  IE_TIPO_SAPS3              VARCHAR2(15 BYTE),
  QT_BMI                     NUMBER(4,2),
  IE_INFEC_EXTENSAO          VARCHAR2(1 BYTE),
  IE_INFEC_AGENTE            VARCHAR2(1 BYTE),
  IE_DISFUNCAO_RENAL         VARCHAR2(1 BYTE),
  IE_FALENCIA_RENAL          VARCHAR2(1 BYTE),
  IE_DISFUNCAO_COAGULACAO    VARCHAR2(1 BYTE),
  IE_FALENCIA_COAGULACAO     VARCHAR2(1 BYTE),
  IE_FALENCIA_CARDIACA       VARCHAR2(1 BYTE),
  IE_FALENCIA_RESPIRATORIA   VARCHAR2(1 BYTE),
  IE_FALENCIA_SNC            VARCHAR2(1 BYTE),
  PR_RISCO_AMERICA_SUL       NUMBER(6,2)        DEFAULT null,
  IE_HEPATICO                NUMBER(3),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSAP3_ATEPACI_FK_I ON TASY.ESCALA_SAPS3
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAP3_EHRREEL_FK_I ON TASY.ESCALA_SAPS3
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSAP3_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSAP3_I1 ON TASY.ESCALA_SAPS3
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAP3_PESFISI_FK_I ON TASY.ESCALA_SAPS3
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSAP3_PK ON TASY.ESCALA_SAPS3
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAP3_TASASDI_FK_I ON TASY.ESCALA_SAPS3
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSAP3_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSAP3_TASASDI_FK2_I ON TASY.ESCALA_SAPS3
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSAP3_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_saps3_pend_atual
after insert or update ON TASY.ESCALA_SAPS3 for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'83');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_SAPS3_delete
after delete ON TASY.ESCALA_SAPS3 for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '83'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.Escala_SAPS3_atual
before insert or update ON TASY.ESCALA_SAPS3 for each row
declare

qt_reg_w		number(1);
qt_pontuacao_w		number(10,0) := 0;
qt_logit_w		number(30,15);
qt_logit_america_sul_w	number(30,15);

begin

if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Idade */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_idade < 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_idade < 60) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_idade < 70) then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	elsif	(:new.qt_idade < 75) then
		qt_pontuacao_w	:= qt_pontuacao_w + 13;
	elsif	(:new.qt_idade < 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 15;
	elsif	(:new.qt_idade >= 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 18;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_idade < 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_idade < 60) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_idade < 70) then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	elsif	(:new.qt_idade < 75) then
		qt_pontuacao_w	:= qt_pontuacao_w + 11;
	elsif	(:new.qt_idade < 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 14;
	elsif	(:new.qt_idade >= 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 17;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
	if	(:new.qt_idade < 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_idade < 60) then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	elsif	(:new.qt_idade < 70) then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	elsif	(:new.qt_idade < 75) then
		qt_pontuacao_w	:= qt_pontuacao_w + 11;
	elsif	(:new.qt_idade < 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 14;
	elsif	(:new.qt_idade >= 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 16;
	end if;
end if;

/* Dias de internacao previos (LOS)*/
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_dias_internacao_prev < 14) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_dias_internacao_prev < 28) then
		qt_pontuacao_w	:= qt_pontuacao_w + 6;
	elsif	(:new.qt_dias_internacao_prev >= 28) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_dias_internacao_prev < 14) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_dias_internacao_prev >= 14) then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
	if	(:new.qt_dias_internacao_prev < 14) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_dias_internacao_prev < 28) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_dias_internacao_prev >= 28) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
end if;

/* Comorbidades */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.ie_aids = 'S') then
		if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and  :new.ie_cirrose = 'S') then
			qt_pontuacao_w	:= qt_pontuacao_w + 16;
		else
			qt_pontuacao_w	:= qt_pontuacao_w + 8;
		end if;
	end if;
	if	(:new.ie_malig_hematologica = 'S') then
		if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and  :new.ie_icc_nyha = 'S') then
			qt_pontuacao_w	:= qt_pontuacao_w + 12;
		else
			qt_pontuacao_w	:= qt_pontuacao_w + 6;
		end if;
	end if;
	if	(:new.ie_quimioterapia = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
	if	(:new.ie_cirrose = 'S') then
		if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and  :new.ie_aids = 'S') then
			qt_pontuacao_w	:= qt_pontuacao_w + 16;
		else
			qt_pontuacao_w	:= qt_pontuacao_w + 8;
		end if;
	end if;
	if	(:new.ie_metastase = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 11;
	end if;
	if	(:new.ie_icc_nyha = 'S') then
		if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and  :new.ie_malig_hematologica = 'S') then
			qt_pontuacao_w	:= qt_pontuacao_w + 12;
		else
			qt_pontuacao_w	:= qt_pontuacao_w + 6;
		end if;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.ie_aids = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	end if;
	if	(:new.ie_quimioterapia = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
	if	(:new.ie_cirrose = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	end if;
	if	(:new.ie_metastase = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 11;
	end if;
	if	(:new.ie_icc_nyha = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
	if	(:new.ie_aids = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	end if;
	if	(:new.ie_cirrose = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	end if;
	if	(:new.ie_metastase = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 6;
	end if;
end if;

/* Farmacos vasoativos */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) and
	(:new.ie_famacos_vasoativos = 'S') then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
end if;

/* Procedencia */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') in ('ADM','PIR')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_origem_paciente,0);
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(nvl(:new.ie_origem_paciente,0) = 2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(nvl(:new.ie_origem_paciente,0) in (3,4)) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
end if;

/* Admissao paciente */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_tipo_admissao,0);
end if;

/* Urgencia */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_urgencia,0);
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(nvl(:new.ie_urgencia,0) in (1,3)) then
		qt_pontuacao_w	:= qt_pontuacao_w + 6;
	end if;
end if;

/* Tipo de operacao */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_tipo_operacao,0);
end if;

/* Neurologicas */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_neurologica,0);
end if;

/* Cardiologicas */
if (:new.ie_cardiologica = 500) then
	qt_pontuacao_w	:= qt_pontuacao_w + 5;
elsif (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and :new.ie_cardiologica = -5 and :new.ie_neurologica = -4) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_cardiologica + 1,0);
elsif (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_AT' and :new.ie_cardiologica = 3 and :new.ie_abdomem = 3) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_cardiologica + 6,0);
else
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_cardiologica,0);
end if;

/* Abdomen */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_abdomem,0);
end if;

/* Sistema hepatico */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_hepatico,0);
end if;

/* Infeccao nosocomial */
if	(nvl(:new.ie_infec_nosocomial,'N') = 'S') then
	qt_pontuacao_w	:= qt_pontuacao_w + 4;
end if;

/* Infeccao  respiratoria */
if	(:new.ie_infec_resp = 'S') then
	if	(nvl(:new.ie_tipo_SAPS3,'ADM') in ('ADM','PIR')) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
end if;

/* Admissao na UTI */
/* if	(:new.ie_admissao_uti = 'S') then
	if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
		qt_pontuacao_w	:= qt_pontuacao_w + 16;
	elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
		qt_pontuacao_w	:= qt_pontuacao_w + 20;
	elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	end if;
end if;*/

/* Escala de Glasgow */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_glasgow <= 4) then
		qt_pontuacao_w	:= qt_pontuacao_w + 15;
	elsif	(:new.qt_glasgow <= 5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 10;
	elsif	(:new.qt_glasgow <= 6) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	elsif	(:new.qt_glasgow <= 12) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	else
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_glasgow <= 4) then
		qt_pontuacao_w	:= qt_pontuacao_w + 14;
	elsif	(:new.qt_glasgow <= 5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 10;
	elsif	(:new.qt_glasgow <= 6) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	else
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
end if;

/* Freq cardiaca (bpm) */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_freq_cardiaca < 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_freq_cardiaca < 160) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_freq_cardiaca >= 160) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_freq_cardiaca < 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_freq_cardiaca < 160) then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	elsif	(:new.qt_freq_cardiaca >= 160) then
		qt_pontuacao_w	:= qt_pontuacao_w + 6;
	end if;
end if;

/* PA max (mmHg) */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_pa_sistolica < 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 11;
	elsif	(:new.qt_pa_sistolica < 70) then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	elsif	(:new.qt_pa_sistolica < 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	elsif	(:new.qt_pa_sistolica >= 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_pa_sistolica < 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 10;
	elsif	(:new.qt_pa_sistolica < 70) then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	elsif	(:new.qt_pa_sistolica < 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	elsif	(:new.qt_pa_sistolica >= 120) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
end if;

/* Temp (oC) */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_temp < 35) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_temp < 35) then
		qt_pontuacao_w	:= qt_pontuacao_w + 6;
	end if;
end if;

/* Leococitos */
if	((nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') or (nvl(:new.ie_tipo_SAPS3,'ADM') = '28D')) then
	if	(:new.qt_globulos_brancos >= 15000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	end if;
end if;

/* Bilirrubina (mg%) */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_bilirrubina >= 2) and (:new.qt_bilirrubina < 6) then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	elsif	(:new.qt_bilirrubina >= 6) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_bilirrubina >= 2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
end if;

/* Creatinina (mg%) */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_creatinina < 1.2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_creatinina < 2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(:new.qt_creatinina < 3.5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	elsif	(:new.qt_creatinina >= 3.5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_creatinina < 1.2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(:new.qt_creatinina < 2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(:new.qt_creatinina >= 2) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
end if;

/* pH */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_ph <= 7.25) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	else
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_ph <= 7.3) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	else
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
end if;

/* Plaquetas */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	if	(:new.qt_plaquetas < 20000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 13;
	elsif	(:new.qt_plaquetas < 50000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 8;
	elsif	(:new.qt_plaquetas < 100000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_plaquetas >= 100000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_plaquetas < 20000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 14;
	elsif	(:new.qt_plaquetas < 50000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	elsif	(:new.qt_plaquetas < 100000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	elsif	(:new.qt_plaquetas >= 100000) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	end if;
end if;

/* Oxigenacao */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.ie_oxigenacao,0);
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(nvl(:new.ie_oxigenacao,0) = 11) then
		qt_pontuacao_w	:= qt_pontuacao_w + 10;
	elsif	(nvl(:new.ie_oxigenacao,0) = 7) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	elsif	(nvl(:new.ie_oxigenacao,0) = 5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 7;
	end if;
end if;

/* BMI */
if	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	if	(:new.qt_bmi is not null) and (:new.qt_bmi < 18.5) then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	end if;
end if;

if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
/* Infection Extension */
	if	(nvl(:new.ie_infec_extensao,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	end if;

/* Infection Agent */
	if	(nvl(:new.ie_infec_agente,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 10;
	end if;

/* Organ dysfunction */
	if	(nvl(:new.ie_disfuncao_renal,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 4;
	end if;
	if	(nvl(:new.ie_disfuncao_coagulacao,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	end if;

/* Organ failure */
	if	(nvl(:new.ie_falencia_renal,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	end if;
	if	(nvl(:new.ie_falencia_coagulacao,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	end if;
	if	(nvl(:new.ie_falencia_cardiaca,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	end if;
	if	(nvl(:new.ie_falencia_respiratoria,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 5;
	end if;
	if	(nvl(:new.ie_falencia_snc,'N') = 'S') then
		qt_pontuacao_w	:= qt_pontuacao_w + 9;
	end if;
end if;

if	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'ADM') then
	qt_pontuacao_w			:= qt_pontuacao_w + 16;
	qt_logit_w			:= ((-32.6659) + LN(qt_pontuacao_w + 20.5958) * 7.3068);
	qt_logit_america_sul_w		:= ((-64.5990) + LN(qt_pontuacao_w + 71.0599) * 13.2322);
	:new.pr_risco_america_sul	:= (exp(qt_logit_america_sul_w) / ( 1 + exp(qt_logit_america_sul_w))) * 100;
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = '28D') then
	qt_pontuacao_w		:= qt_pontuacao_w + 20;
	qt_logit_w		:= ((-26.2477) + LN(qt_pontuacao_w + 4.5973) * 6.0521);
elsif	(nvl(:new.ie_tipo_SAPS3,'ADM') = 'PIR') then
	qt_logit_w		:= ((-46.67579) + LN(qt_pontuacao_w + 76.7688) * 9.8797);
end if;

:new.qt_pontuacao	:= qt_pontuacao_w;

:new.pr_risco		:= (exp(qt_logit_w) / ( 1 + exp(qt_logit_w))) * 100;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_saps3_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_atualizacao, :new.ie_cardiologica, :new.ie_hepatico,
						   :new.ie_abdomem, :new.ie_neurologica, :new.ie_tipo_operacao, :new.ie_infec_nosocomial, :new.ie_infec_resp);
end if;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
		send_saps3_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_atualizacao, :new.ie_cardiologica, :new.ie_hepatico,
							   :new.ie_abdomem, :new.ie_neurologica, :new.ie_tipo_operacao, :new.ie_infec_nosocomial, :new.ie_infec_resp);
end if;

<<Final>>
qt_reg_w	:= 0;


end Escala_SAPS3_atual;
/


ALTER TABLE TASY.ESCALA_SAPS3 ADD (
  CONSTRAINT ESCSAP3_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SAPS3 ADD (
  CONSTRAINT ESCSAP3_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCSAP3_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSAP3_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSAP3_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCSAP3_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_SAPS3 TO NIVEL_1;

