ALTER TABLE TASY.RESULT_LABORATORIO_QUEBRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_LABORATORIO_QUEBRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_LABORATORIO_QUEBRA
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NR_PRESCRICAO      NUMBER(14),
  NR_SEQ_RESULT_LAB  NUMBER(10),
  VL_QUEBRA          VARCHAR2(255 BYTE),
  NR_SEQ_PAGINA      NUMBER(10),
  NR_SEQ_LAUDO       NUMBER(10),
  NR_ATENDIMENTO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RESLAQU_I1 ON TASY.RESULT_LABORATORIO_QUEBRA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLAQU_I1
  MONITORING USAGE;


CREATE INDEX TASY.RESLAQU_I2 ON TASY.RESULT_LABORATORIO_QUEBRA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLAQU_I2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RESLAQU_PK ON TASY.RESULT_LABORATORIO_QUEBRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLAQU_PK
  MONITORING USAGE;


ALTER TABLE TASY.RESULT_LABORATORIO_QUEBRA ADD (
  CONSTRAINT RESLAQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.RESULT_LABORATORIO_QUEBRA TO NIVEL_1;

