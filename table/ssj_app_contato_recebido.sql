ALTER TABLE TASY.SSJ_APP_CONTATO_RECEBIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SSJ_APP_CONTATO_RECEBIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SSJ_APP_CONTATO_RECEBIDO
(
  NR_SEQUENCIA       INTEGER                    NOT NULL,
  NM_BENEFICIARIO    VARCHAR2(255 BYTE),
  NR_TELEFONE        VARCHAR2(30 BYTE),
  EMAIL              VARCHAR2(255 BYTE),
  DS_MOTIVO_CONTATO  VARCHAR2(255 BYTE),
  DS_MENSAGEM        VARCHAR2(2000 BYTE),
  EMAIL_DESTINO      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.SSJ_APP_CONTATO_RECEBIDO ADD (
  PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT INSERT, SELECT ON TASY.SSJ_APP_CONTATO_RECEBIDO TO APPSSJ;

GRANT SELECT ON TASY.SSJ_APP_CONTATO_RECEBIDO TO NIVEL_1;

