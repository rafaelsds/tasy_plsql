ALTER TABLE TASY.REHU_ASPIRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REHU_ASPIRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REHU_ASPIRACAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_CICLO                NUMBER(10)        NOT NULL,
  QT_OVOCITOS                 NUMBER(3),
  QT_OVULO_C1                 NUMBER(3),
  QT_OVULO_C2                 NUMBER(3),
  QT_OVULO_C3                 NUMBER(3),
  QT_OVULO_C4                 NUMBER(3),
  QT_OVULO_C5                 NUMBER(3),
  QT_OVOCITOS_CRIOP           NUMBER(3),
  QT_OVOCITOS_FIV             NUMBER(3),
  QT_OVOCITOS_MII             NUMBER(3),
  QT_OVOCITOS_MI              NUMBER(3),
  QT_OVOCITOS_P1              NUMBER(3),
  QT_OVOCITOS_DISM            NUMBER(3),
  QT_OVOCITOS_DOADO           NUMBER(3),
  QT_OVOCITOS_IIU             NUMBER(3),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  QT_OOCITO_INJ               NUMBER(3),
  QT_OOCITO_DISM              NUMBER(3),
  QT_ESPACO_PERV              NUMBER(3),
  QT_CORUSCULO_POLAR_GIGANTE  NUMBER(3),
  QT_ZP_ESPESSA               NUMBER(3),
  QT_FORMA_OVAL               NUMBER(3),
  QT_CITOPLASMA_HIALINO       NUMBER(3),
  QT_CITOPLASMA_VISCOSO       NUMBER(3),
  QT_GRANULACAO_CITO          NUMBER(3),
  QT_MEM_CITOP_RESIS          NUMBER(3),
  QT_ESPERMA_NORMAL           NUMBER(3),
  QT_ESPERMA_ALT_CABECA       NUMBER(3),
  QT_ESPERMA_ALT_CAUDA        NUMBER(3),
  QT_ESPERMA_IMATURO          NUMBER(3),
  QT_ESPERMA_ANORMAIS         NUMBER(3),
  QT_ALT_PECA_INTERM          NUMBER(3),
  QT_ESPERMA_IMOVEL           NUMBER(3),
  QT_ESPERMATOGORIA           NUMBER(3),
  IE_PICSI                    VARCHAR2(1 BYTE),
  IE_SUPER_ICSI               VARCHAR2(1 BYTE),
  DT_REGISTRO                 DATE              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RHASPIR_PK ON TASY.REHU_ASPIRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RHASPIR_PK
  MONITORING USAGE;


CREATE INDEX TASY.RHASPIR_RHCICLO_FK_I ON TASY.REHU_ASPIRACAO
(NR_SEQ_CICLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RHASPIR_RHCICLO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REHU_ASPIRACAO ADD (
  CONSTRAINT RHASPIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REHU_ASPIRACAO ADD (
  CONSTRAINT RHASPIR_RHCICLO_FK 
 FOREIGN KEY (NR_SEQ_CICLO) 
 REFERENCES TASY.REHU_CICLO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REHU_ASPIRACAO TO NIVEL_1;

