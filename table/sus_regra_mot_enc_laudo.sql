ALTER TABLE TASY.SUS_REGRA_MOT_ENC_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_REGRA_MOT_ENC_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_REGRA_MOT_ENC_LAUDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO_LAUDO_APAC   NUMBER(2)                NOT NULL,
  CD_MOTIVO_COBRANCA   NUMBER(2)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSRMEL_PK ON TASY.SUS_REGRA_MOT_ENC_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRMEL_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSRMEL_SUSMCOU_FK_I ON TASY.SUS_REGRA_MOT_ENC_LAUDO
(CD_MOTIVO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRMEL_SUSMCOU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_REGRA_MOT_ENC_LAUDO ADD (
  CONSTRAINT SUSRMEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_REGRA_MOT_ENC_LAUDO ADD (
  CONSTRAINT SUSRMEL_SUSMCOU_FK 
 FOREIGN KEY (CD_MOTIVO_COBRANCA) 
 REFERENCES TASY.SUS_MOTIVO_COBR_UNIF (CD_MOTIVO_COBRANCA));

GRANT SELECT ON TASY.SUS_REGRA_MOT_ENC_LAUDO TO NIVEL_1;

