ALTER TABLE TASY.TISS_REAP_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REAP_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REAP_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_LOTE              NUMBER(10)               NOT NULL,
  NR_SEQ_PROTOCOLO     NUMBER(10),
  NR_SEQ_RETORNO       NUMBER(10),
  DS_LOTE              VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  IE_TISS_TIPO_GUIA    VARCHAR2(3 BYTE)         NOT NULL,
  DT_FECHAMENTO        DATE,
  IE_STATUS_ACERTO     VARCHAR2(1 BYTE)         NOT NULL,
  DT_REAPRESENTACAO    DATE,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  VL_DIARIA            NUMBER(15,2),
  VL_GASES             NUMBER(15,2),
  VL_MATERIAL          NUMBER(15,2),
  VL_MEDICAMENTO       NUMBER(15,2),
  VL_PROCEDIMENTO      NUMBER(15,2),
  VL_TAXA              NUMBER(15,2),
  VL_TOTAL             NUMBER(15,2),
  VL_TOTAL_HONORARIO   NUMBER(15,2),
  NR_SEQ_LOTE_AUDIT    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSREL_CONVENI_FK_I ON TASY.TISS_REAP_LOTE
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREL_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREL_ESTABEL_FK_I ON TASY.TISS_REAP_LOTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREL_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREL_LOTAUHI_FK_I ON TASY.TISS_REAP_LOTE
(NR_SEQ_LOTE_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREL_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSREL_PK ON TASY.TISS_REAP_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREL_PK
  MONITORING USAGE;


CREATE INDEX TASY.TISSREL_PROCONV_FK_I ON TASY.TISS_REAP_LOTE
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREL_PROCONV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TISS_REAP_LOTE ADD (
  CONSTRAINT TISSREL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REAP_LOTE ADD (
  CONSTRAINT TISSREL_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT TISSREL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSREL_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSREL_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TISS_REAP_LOTE TO NIVEL_1;

