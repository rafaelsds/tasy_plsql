ALTER TABLE TASY.ATEND_TIMI_RISK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_TIMI_RISK CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_TIMI_RISK
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_AVALIACAO             DATE                 NOT NULL,
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  QT_ANO                   NUMBER(15,3)         NOT NULL,
  IE_FATOR_RISCO           VARCHAR2(1 BYTE)     NOT NULL,
  IE_DAC                   VARCHAR2(1 BYTE)     NOT NULL,
  IE_AAS                   VARCHAR2(1 BYTE)     NOT NULL,
  IE_ANGINA_24H            VARCHAR2(1 BYTE)     NOT NULL,
  IE_ELEV_ENZIMA           VARCHAR2(1 BYTE)     NOT NULL,
  IE_DESVIO_ST             VARCHAR2(1 BYTE)     NOT NULL,
  QT_ESCORE_ANO            NUMBER(15,3)         NOT NULL,
  QT_ESCORE_FAT_RISCO      NUMBER(15,3)         NOT NULL,
  QT_ESCORE                NUMBER(15,3)         NOT NULL,
  PR_MORTE_INFARTO         NUMBER(4,1)          NOT NULL,
  QT_ESCORE_DAC            NUMBER(15,3)         NOT NULL,
  PR_MORTE_INFARTO_REVASC  NUMBER(4,1)          NOT NULL,
  QT_ESCORE_AAS            NUMBER(15,3)         NOT NULL,
  QT_ESCORE_ANGINA         NUMBER(15,3)         NOT NULL,
  QT_ESCORE_ELEV_ENZIMA    NUMBER(15,3)         NOT NULL,
  QT_ESCORE_DESVIO         NUMBER(15,3)         NOT NULL,
  DT_LIBERACAO             DATE,
  CD_PERFIL_ATIVO          NUMBER(5),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  NR_HORA                  NUMBER(2),
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATETIRI_ATEPACI_FK_I ON TASY.ATEND_TIMI_RISK
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETIRI_I1 ON TASY.ATEND_TIMI_RISK
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETIRI_PERFIL_FK_I ON TASY.ATEND_TIMI_RISK
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETIRI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATETIRI_PESFISI_FK_I ON TASY.ATEND_TIMI_RISK
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATETIRI_PK ON TASY.ATEND_TIMI_RISK
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_TIMI_RISK_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ATEND_TIMI_RISK FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_escore_ano				:= 0;
:new.qt_escore_fat_risco			:= 0;
:new.qt_escore_dac				:= 0;
:new.qt_escore_aas				:= 0;
:new.qt_escore_angina			:= 0;
:new.qt_escore_elev_enzima			:= 0;
:new.qt_escore_desvio			:= 0;
:new.qt_escore				:= 0;
:new.pr_morte_infarto			:= 0;
:new.pr_morte_infarto_revasc		:= 0;

if	(:new.qt_ano > 64) then
	:new.qt_escore_ano			:= 1;
end if;
if	(:new.ie_fator_risco = 'S') then
	:new.qt_escore_fat_risco		:= 1;
end if;
if	(:new.ie_dac = 'S') then
	:new.qt_escore_dac			:= 1;
end if;
if	(:new.ie_aas = 'S') then
	:new.qt_escore_aas			:= 1;
end if;
if	(:new.ie_angina_24h = 'S') then
	:new.qt_escore_angina		:= 1;
end if;
if	(:new.ie_elev_enzima = 'S') then
	:new.qt_escore_elev_enzima		:= 1;
end if;
if	(:new.ie_desvio_st = 'S') then
	:new.qt_escore_desvio		:= 1;
end if;

:new.qt_escore				:=
		:new.qt_escore_ano	+
		:new.qt_escore_fat_risco +
		:new.qt_escore_dac +
		:new.qt_escore_aas +
		:new.qt_escore_angina +
		:new.qt_escore_elev_enzima	+
		:new.qt_escore_desvio;

if	(:new.qt_escore < 2) then
	:new.pr_morte_infarto		:= 3;
	:new.pr_morte_infarto_revasc	:= 5;
elsif	(:new.qt_escore = 2) then
	:new.pr_morte_infarto		:= 3;
	:new.pr_morte_infarto_revasc	:= 8;
elsif	(:new.qt_escore = 3) then
	:new.pr_morte_infarto		:= 5;
	:new.pr_morte_infarto_revasc	:= 13;
elsif	(:new.qt_escore = 4) then
	:new.pr_morte_infarto		:= 7;
	:new.pr_morte_infarto_revasc	:= 20;
elsif	(:new.qt_escore = 5) then
	:new.pr_morte_infarto		:= 12;
	:new.pr_morte_infarto_revasc	:= 26;
elsif	(:new.qt_escore > 5) then
	:new.pr_morte_infarto		:= 19;
	:new.pr_morte_infarto_revasc	:= 41;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ATEND_TIMI_RISK ADD (
  CONSTRAINT ATETIRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_TIMI_RISK ADD (
  CONSTRAINT ATETIRI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATETIRI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATETIRI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ATEND_TIMI_RISK TO NIVEL_1;

