ALTER TABLE TASY.PERSON_NAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERSON_NAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERSON_NAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TYPE              VARCHAR2(15 BYTE)        NOT NULL,
  DS_GIVEN_NAME        VARCHAR2(128 BYTE),
  DS_FAMILY_NAME       VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_1  VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_2  VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_3  VARCHAR2(128 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PERNAME_PK ON TASY.PERSON_NAME
(NR_SEQUENCIA, DS_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.NOM_PERSON_NAME_ATUAL
Before INSERT OR UPDATE ON TASY.PERSON_NAME for each row
declare

qt_minima_w	number(1) := 2;
qt_maxima_w	number(2) := 25;
begin

if	(obter_utiliza_nom(wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
	if	(LENGTH(:new.DS_GIVEN_NAME) < qt_minima_w or LENGTH(:new.DS_GIVEN_NAME) > qt_maxima_w ) then
		wheb_mensagem_pck.exibir_mensagem_abort(1073379,'DS_TIPO=' || obter_texto_dic_objeto(926380,wheb_usuario_pck.get_nr_seq_idioma,null) || ';' || 'QT_MINIMA=' || qt_minima_w || ';' || 'QT_MAXIMA=' || qt_maxima_w );
	end if;
	if	(LENGTH(:new.DS_FAMILY_NAME) < qt_minima_w or LENGTH(:new.DS_FAMILY_NAME) > qt_maxima_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(1073379,'DS_TIPO=' || obter_texto_dic_objeto(1025612,wheb_usuario_pck.get_nr_seq_idioma,null) || ';' || 'QT_MINIMA=' || qt_minima_w || ';' || 'QT_MAXIMA=' || qt_maxima_w );
	end if;
	if	(LENGTH(:new.DS_COMPONENT_NAME_1) < qt_minima_w or LENGTH(:new.DS_COMPONENT_NAME_1) > qt_maxima_w ) then
		wheb_mensagem_pck.exibir_mensagem_abort(1073379,'DS_TIPO=' || obter_texto_dic_objeto(1025613,wheb_usuario_pck.get_nr_seq_idioma,null) || ';' || 'QT_MINIMA=' || qt_minima_w || ';' || 'QT_MAXIMA=' || qt_maxima_w );
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Person_Name_Atual
BEFORE INSERT OR UPDATE ON TASY.PERSON_NAME FOR EACH ROW
DECLARE
ie_param_10_w    varchar2(1);
qt_caracter_w    number(10);
qt_regra_atrib_w number(10);
ds_locale_w      user_locale.ds_locale%type;
reg_integracao_p gerar_int_padrao.reg_integracao;
NR_PARAM_277_W   NUMBER(10);
nr_cpf_w pessoa_fisica.nr_cpf %type;

begin
  if  (wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
    if(wheb_usuario_pck.get_cd_funcao in (-2,4,5,32,230)) then
      Obter_Param_Usuario(5, 10, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 0), ie_param_10_w);
      NR_PARAM_277_W := NVL(OBTER_SOMENTE_NUMERO(OBTER_VALOR_PARAM_USUARIO(5, 277, OBTER_PERFIL_ATIVO, :NEW.NM_USUARIO, NVL(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, 0))), 0);

      ds_locale_w := obtain_user_locale(:new.nm_usuario);

      :new.ds_given_name := eliminar_caracter_esp_person(:new.ds_given_name, ds_locale_w);
      :new.ds_family_name := eliminar_caracter_esp_person(:new.ds_family_name, ds_locale_w);
      :new.ds_component_name_1 := eliminar_caracter_esp_person(:new.ds_component_name_1, ds_locale_w);
      :new.ds_component_name_2 := eliminar_caracter_esp_person(:new.ds_component_name_2, ds_locale_w);
      :new.ds_component_name_3 := eliminar_caracter_esp_person(:new.ds_component_name_3, ds_locale_w);

      if(ie_param_10_w = 'S') then
        :new.ds_given_name := INITCAP(:new.ds_given_name);
        :new.ds_family_name := INITCAP(:new.ds_family_name);
        :new.ds_component_name_1 := INITCAP(:new.ds_component_name_1);
        :new.ds_component_name_2 := INITCAP(:new.ds_component_name_2);
        :new.ds_component_name_3 := INITCAP(:new.ds_component_name_3);
      elsif(ie_param_10_w = 'T') then
        :new.ds_given_name := upper(:new.ds_given_name);
        :new.ds_family_name := upper(:new.ds_family_name);
        :new.ds_component_name_1 := upper(:new.ds_component_name_1);
        :new.ds_component_name_2 := upper(:new.ds_component_name_2);
        :new.ds_component_name_3 := upper(:new.ds_component_name_3);
      end if;

	    select nvl(length(:new.ds_given_name),0)+nvl(length(:new.ds_family_name),0)+nvl(length(:new.ds_component_name_1),0)+nvl(length(:new.ds_component_name_2),0)+nvl(length(:new.ds_component_name_3),0)
        into  qt_caracter_w
        from dual;

      IF (NR_PARAM_277_W > 0
      AND QT_CARACTER_W > 0
      AND QT_CARACTER_W < NR_PARAM_277_W) THEN
        WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(1112273);
      END IF;

      if (updating) then
        begin
        select  cd_pessoa_fisica,
          nr_prontuario,
          nvl(ie_funcionario,'N'),
          nr_cpf
        into  reg_integracao_p.cd_pessoa_fisica,
          reg_integracao_p.nr_prontuario,
          reg_integracao_p.ie_funcionario,
          nr_cpf_w
        from  pessoa_fisica
        where  nr_seq_person_name = :new.nr_sequencia;
        exception
        when others then
          reg_integracao_p.cd_pessoa_fisica  := null;
          reg_integracao_p.nr_prontuario    := null;
          reg_integracao_p.ie_funcionario    := null;
        end;

        reg_integracao_p.ie_operacao    := 'A';

        if nr_cpf_w is not null then
           reg_integracao_p.ie_possui_cpf := 'S';
        end if;

        SELECT decode(COUNT(*), 0, 'N', 'S')
          INTO reg_integracao_p.ie_pessoa_atend
          FROM pessoa_fisica_aux
         WHERE cd_pessoa_fisica = reg_integracao_p.cd_pessoa_fisica
           AND nr_primeiro_atend IS NOT NULL;

        if  (wheb_usuario_pck.get_ie_lote_contabil = 'N') then
          gerar_int_padrao.gravar_integracao('12',reg_integracao_p.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
        end if;
      end if;

      select  count(*)
      into  qt_regra_atrib_w
      from  tabela_atrib_regra
      where  nm_atributo = 'NR_SEQ_PERSON_NAME'
      and    nm_tabela = 'PESSOA_FISICA'
      and    nvl(cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
      and    nvl(cd_perfil,obter_perfil_ativo) = obter_perfil_ativo
      and    ((nvl(cd_setor_atendimento,wheb_usuario_pck.get_cd_setor_atendimento) = wheb_usuario_pck.get_cd_setor_atendimento) or (wheb_usuario_pck.get_cd_setor_atendimento is null))
      and    nvl(nm_usuario_param,wheb_usuario_pck.get_nm_usuario) = wheb_usuario_pck.get_nm_usuario
      and    upper(ds_regra) = '@REMOVER_NUMERICO';

      if  (qt_regra_atrib_w > 0) then
        :new.ds_given_name       := elimina_numeros(:new.ds_given_name);
        :new.ds_family_name     := elimina_numeros(:new.ds_family_name);
        :new.ds_component_name_1   := elimina_numeros(:new.ds_component_name_1);
        :new.ds_component_name_2   := elimina_numeros(:new.ds_component_name_2);
        :new.ds_component_name_3   := elimina_numeros(:new.ds_component_name_3);
      end if;
    end if;
  end if;
end;
/


ALTER TABLE TASY.PERSON_NAME ADD (
  CONSTRAINT PERNAME_PK
 PRIMARY KEY
 (NR_SEQUENCIA, DS_TYPE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PERSON_NAME TO NIVEL_1;

