ALTER TABLE TASY.MATERIAL_CLASSIF_EXAM_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_CLASSIF_EXAM_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_CLASSIF_EXAM_LAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_CLASSIFICACAO     VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MACLAEXL_PK ON TASY.MATERIAL_CLASSIF_EXAM_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MATERIAL_CLASSIF_EXAM_LAB ADD (
  CONSTRAINT MACLAEXL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_CLASSIF_EXAM_LAB TO NIVEL_1;

