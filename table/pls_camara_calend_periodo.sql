ALTER TABLE TASY.PLS_CAMARA_CALEND_PERIODO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CAMARA_CALEND_PERIODO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CAMARA_CALEND_PERIODO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CALENDARIO    NUMBER(10)               NOT NULL,
  NR_MES               NUMBER(2)                NOT NULL,
  NR_CAMARA            NUMBER(2)                NOT NULL,
  DT_LIMITE_A600       DATE,
  DT_LIMITE_A500       DATE,
  DT_PREVIA            DATE,
  DT_LIMITE_REC_DOC    DATE,
  DT_LIMITE_EXCLUSAO   DATE,
  DT_DEFINITIVO        DATE,
  DT_SALDO_DEVEDOR     DATE,
  DT_SALDO_CREDOR      DATE,
  DT_LIMITE_A550       DATE,
  DT_REPASSE           DATE,
  DT_LIMITE_CANC_A550  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCCPE_PK ON TASY.PLS_CAMARA_CALEND_PERIODO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCPE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCPE_PLSCCAL_FK_I ON TASY.PLS_CAMARA_CALEND_PERIODO
(NR_SEQ_CALENDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCPE_PLSCCAL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_camara_cal_periodo_after
after insert or update or delete ON TASY.PLS_CAMARA_CALEND_PERIODO for each row
declare

nr_camara_new_w		varchar2(15);
nr_camara_old_w		varchar2(15);

procedure inserir_log_periodo(	ie_opcao_p		varchar2,
				nm_campo_p		varchar2,
				vl_antigo_p		varchar2,
				vl_novo_p		varchar2) is

ds_historico_w		varchar2(4000);
nr_camara_w		varchar2(15);
nm_campo_w		varchar2(200);
nr_seq_calendario_w	number;
begin

nr_seq_calendario_w := :new.nr_seq_calendario;

if	(ie_opcao_p = 'I') then

	select 	decode(:new.nr_camara,1,'Primeira',2,'Segunda',3,'Terceira',4,'Quarta')
	into	nr_camara_w
	from 	dual;

	ds_historico_w := wheb_mensagem_pck.get_texto(1111919, 	'NR_MES=' 	|| substr(obter_mes_extenso(:new.nr_mes,'C'),1,50) 	|| ';' ||
								'NR_CAMARA=' 	|| nr_camara_w);

elsif	(ie_opcao_p = 'U') then

	select 	nvl(ds_label, ds_label_grid)
	into	nm_campo_w
	from	tabela_atributo
	where 	nm_tabela = 'PLS_CAMARA_CALEND_PERIODO'
	and	nm_atributo = nm_campo_p;

	ds_historico_w := wheb_mensagem_pck.get_texto(1111920, 	'NR_MES=' 	|| substr(obter_mes_extenso(:new.nr_mes,'C'),1,50) 	|| ';' ||
								'NM_CAMPO=' 	|| nm_campo_w 						|| ';' ||
								'VL_ANTIGO=' 	|| nvl(vl_antigo_p, 'null') 				|| ';' ||
								'VL_NOVO=' 	|| nvl(vl_novo_p, 'null'));
elsif	(ie_opcao_p = 'D') then

	nr_seq_calendario_w := :old.nr_seq_calendario;

	select 	decode(:old.nr_camara,1,'Primeira',2,'Segunda',3,'Terceira',4,'Quarta')
	into	nr_camara_w
	from 	dual;

	ds_historico_w := wheb_mensagem_pck.get_texto(1111921, 	'NR_MES=' 	|| substr(obter_mes_extenso(:old.nr_mes,'C'),1,50) 	|| ';' ||
								'NR_CAMARA=' 	|| nr_camara_w);
end if;

insert into pls_hist_camara_calend
	(nr_sequencia,
	cd_estabelecimento,
	ds_historico,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nm_tabela,
	nr_seq_camara_calend)
values (pls_hist_camara_calend_seq.nextval,
	wheb_usuario_pck.get_cd_estabelecimento,
	ds_historico_w,
	sysdate,
	sysdate,
	wheb_usuario_pck.get_nm_usuario,
	wheb_usuario_pck.get_nm_usuario,
	'PLS_CAMARA_CALEND_PERIODO',
	nr_seq_calendario_w);

end inserir_log_periodo;

begin

if	(inserting) then

	inserir_log_periodo('I', '','','');

elsif	(updating) then

	if	(nvl(:new.NR_MES,0) <> nvl(:old.NR_MES,0)) then
		inserir_log_periodo('U', 'NR_MES', :old.NR_MES, :new.NR_MES);
	end if;

	if	(nvl(:new.NR_CAMARA, 0) <>  nvl(:old.NR_CAMARA, 0)) then

		select 	decode(:old.nr_camara,1,'Primeira',2,'Segunda',3,'Terceira',4,'Quarta'),
			decode(:new.nr_camara,1,'Primeira',2,'Segunda',3,'Terceira',4,'Quarta')
		into	nr_camara_old_w,
			nr_camara_new_w
		from 	dual;

		inserir_log_periodo('U', 'NR_CAMARA', nr_camara_old_w, nr_camara_new_w);
	end if;

	if	(nvl(:new.DT_LIMITE_A600, sysdate) <>  nvl(:old.DT_LIMITE_A600, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_A600', to_char(:old.DT_LIMITE_A600,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_A600,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_LIMITE_A500, sysdate) <>  nvl(:old.DT_LIMITE_A500, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_A500', to_char(:old.DT_LIMITE_A500,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_A500,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_PREVIA, sysdate) <>  nvl(:old.DT_PREVIA, sysdate)) then
		inserir_log_periodo('U', 'DT_PREVIA', to_char(:old.DT_PREVIA,'dd/mm/rrrr'), to_char(:new.DT_PREVIA,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_LIMITE_REC_DOC, sysdate) <>  nvl(:old.DT_LIMITE_REC_DOC, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_REC_DOC', to_char(:old.DT_LIMITE_REC_DOC,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_REC_DOC,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_LIMITE_EXCLUSAO, sysdate) <>  nvl(:old.DT_LIMITE_EXCLUSAO, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_EXCLUSAO', to_char(:old.DT_LIMITE_EXCLUSAO,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_EXCLUSAO,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_DEFINITIVO, sysdate) <>  nvl(:old.DT_DEFINITIVO, sysdate)) then
		inserir_log_periodo('U', 'DT_DEFINITIVO', to_char(:old.DT_DEFINITIVO,'dd/mm/rrrr'), to_char(:new.DT_DEFINITIVO,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_SALDO_DEVEDOR, sysdate) <>  nvl(:old.DT_SALDO_DEVEDOR, sysdate)) then
		inserir_log_periodo('U', 'DT_SALDO_DEVEDOR', to_char(:old.DT_SALDO_DEVEDOR,'dd/mm/rrrr'), to_char(:new.DT_SALDO_DEVEDOR,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_SALDO_CREDOR, sysdate) <>  nvl(:old.DT_SALDO_CREDOR, sysdate)) then
		inserir_log_periodo('U', 'DT_SALDO_CREDOR', to_char(:old.DT_SALDO_CREDOR,'dd/mm/rrrr'), to_char(:new.DT_SALDO_CREDOR,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_LIMITE_A550, sysdate) <>  nvl(:old.DT_LIMITE_A550, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_A550', to_char(:old.DT_LIMITE_A550,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_A550,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_REPASSE, sysdate) <>  nvl(:old.DT_REPASSE, sysdate)) then
		inserir_log_periodo('U', 'DT_REPASSE', to_char(:old.DT_REPASSE,'dd/mm/rrrr'), to_char(:new.DT_REPASSE,'dd/mm/rrrr'));
	end if;

	if	(nvl(:new.DT_LIMITE_CANC_A550, sysdate) <>  nvl(:old.DT_LIMITE_CANC_A550, sysdate)) then
		inserir_log_periodo('U', 'DT_LIMITE_CANC_A550', to_char(:old.DT_LIMITE_CANC_A550,'dd/mm/rrrr'), to_char(:new.DT_LIMITE_CANC_A550,'dd/mm/rrrr'));
	end if;

elsif	(deleting) then

	inserir_log_periodo('D', '','','');
end if;

end;
/


ALTER TABLE TASY.PLS_CAMARA_CALEND_PERIODO ADD (
  CONSTRAINT PLSCCPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CAMARA_CALEND_PERIODO ADD (
  CONSTRAINT PLSCCPE_PLSCCAL_FK 
 FOREIGN KEY (NR_SEQ_CALENDARIO) 
 REFERENCES TASY.PLS_CAMARA_CALENDARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CAMARA_CALEND_PERIODO TO NIVEL_1;

