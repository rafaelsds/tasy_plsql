ALTER TABLE TASY.PROJ_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_AGENDA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_CONSULTOR           VARCHAR2(10 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE                   NOT NULL,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)      NOT NULL,
  DT_AGENDA              DATE                   NOT NULL,
  IE_STATUS              VARCHAR2(15 BYTE)      NOT NULL,
  IE_DIA_TODO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_HORA_INIC           DATE,
  CD_HORA_FIM            DATE,
  NR_SEQ_CLIENTE         NUMBER(10),
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  NR_SEQ_CANAL           NUMBER(10),
  NR_SEQ_PROJ            NUMBER(10),
  NR_SEQ_MOTIVO          NUMBER(10),
  IE_AGENDA_PREVISTA     VARCHAR2(1 BYTE),
  DS_NOME_CURTO_CLIENTE  VARCHAR2(255 BYTE),
  DS_OBS_ESCRITORIO      VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJAGE_COMCANA_FK_I ON TASY.PROJ_AGENDA
(NR_SEQ_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJAGE_COMCANA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJAGE_COMCLIE_FK_I ON TASY.PROJ_AGENDA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJAGE_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJAGE_PESFISI_FK_I ON TASY.PROJ_AGENDA
(CD_CONSULTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJAGE_PK ON TASY.PROJ_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJAGE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJAGE_PROJMOS_FK_I ON TASY.PROJ_AGENDA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJAGE_PROJMOS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJAGE_PROPROJ_FK_I ON TASY.PROJ_AGENDA
(NR_SEQ_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJAGE_PROPROJ_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_agenda_befup
before update ON TASY.PROJ_AGENDA for each row
declare

nm_atributo_w	varchar2(50);
ds_atributos_w	varchar2(255);
ds_atributo_w	varchar2(255);
cd_consultor_w	varchar(255);

Cursor C01 is
	select	NM_ATRIBUTO
	from	tabela_atributo
	where	nm_tabela = 'PROJ_AGENDA'
	and	ie_tipo_atributo <> 'FUNCTION'
	and	nr_seq_apresent is not null
	order by 1;
begin
ds_atributos_w := '';
	if	(:old.dt_agenda <> :new.dt_agenda) then
		select 	substr(OBTER_LABEL_ATRIBUTO('DT_AGENDA', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.CD_CONSULTOR <> :new.CD_CONSULTOR) then
		select 	substr(OBTER_LABEL_ATRIBUTO('CD_CONSULTOR', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.IE_DIA_TODO <> :new.IE_DIA_TODO) then
		select 	substr(OBTER_LABEL_ATRIBUTO('IE_DIA_TODO', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.IE_STATUS <> :new.IE_STATUS) then
		select 	substr(OBTER_LABEL_ATRIBUTO('IE_STATUS', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.CD_HORA_INIC <> :new.CD_HORA_INIC) then
		select 	substr(OBTER_LABEL_ATRIBUTO('CD_HORA_INIC', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.CD_HORA_FIM <> :new.CD_HORA_FIM) then
		select 	substr(OBTER_LABEL_ATRIBUTO('CD_HORA_FIM', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.NR_SEQ_CLIENTE <> :new.NR_SEQ_CLIENTE) then
		select 	substr(OBTER_LABEL_ATRIBUTO('NR_SEQ_CLIENTE', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.NR_SEQ_CANAL <> :new.NR_SEQ_CANAL) then
		select 	substr(OBTER_LABEL_ATRIBUTO('NR_SEQ_CANAL', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.NR_SEQ_PROJ <> :new.NR_SEQ_PROJ) then
		select 	substr(OBTER_LABEL_ATRIBUTO('NR_SEQ_PROJ', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.NR_SEQ_MOTIVO <> :new.NR_SEQ_MOTIVO) then
		select 	substr(OBTER_LABEL_ATRIBUTO('NR_SEQ_MOTIVO', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.DS_OBSERVACAO <> :new.DS_OBSERVACAO) then
		select 	substr(OBTER_LABEL_ATRIBUTO('DS_OBSERVACAO', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.DS_OBS_ESCRITORIO <> :new.DS_OBS_ESCRITORIO) then
		select 	substr(OBTER_LABEL_ATRIBUTO('DS_OBS_ESCRITORIO', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.DS_NOME_CURTO_CLIENTE <> :new.DS_NOME_CURTO_CLIENTE) then
		select 	substr(OBTER_LABEL_ATRIBUTO('DS_NOME_CURTO_CLIENTE', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;

	if	(:old.IE_AGENDA_PREVISTA <> :new.IE_AGENDA_PREVISTA) then
		select 	substr(OBTER_LABEL_ATRIBUTO('IE_AGENDA_PREVISTA', 'PROJ_AGENDA', ''),1,255)
		into	ds_atributo_w
		from 	dual;

		ds_atributos_w := ds_atributos_w || ds_atributo_w || ',';
	end if;


	if (ds_atributos_w is not null) then
	envia_ci_novo_proj('AA', :new.nr_seq_proj, wheb_usuario_pck.get_nm_usuario,0,0,0, ds_atributos_w);
	end if;
end;
/


ALTER TABLE TASY.PROJ_AGENDA ADD (
  CONSTRAINT PROJAGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_AGENDA ADD (
  CONSTRAINT PROJAGE_COMCANA_FK 
 FOREIGN KEY (NR_SEQ_CANAL) 
 REFERENCES TASY.COM_CANAL (NR_SEQUENCIA),
  CONSTRAINT PROJAGE_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PROJAGE_PESFISI_FK 
 FOREIGN KEY (CD_CONSULTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROJAGE_PROJMOS_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.PROJ_MOTIVO_AGENDA (NR_SEQUENCIA),
  CONSTRAINT PROJAGE_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_AGENDA TO NIVEL_1;

