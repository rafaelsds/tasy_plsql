ALTER TABLE TASY.PLS_AME_REGRA_BANDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AME_REGRA_BANDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AME_REGRA_BANDA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REGRA           NUMBER(10),
  DS_DESCRICAO           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_BANDA          VARCHAR2(2 BYTE),
  IE_SEPARADOR           VARCHAR2(1 BYTE),
  IE_SOMENTE_TITULAR     VARCHAR2(1 BYTE),
  IE_SOMA_VALOR_FAMILIA  VARCHAR2(1 BYTE),
  NR_ORDEM               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSARBAN_PK ON TASY.PLS_AME_REGRA_BANDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSARBAN_PLSARQ_FK_I ON TASY.PLS_AME_REGRA_BANDA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ame_regra_banda_befinsup
before insert or update ON TASY.PLS_AME_REGRA_BANDA for each row
declare

    qt_atributos_w NUMBER(10);
    qt_banda_w NUMBER(10);

pragma autonomous_transaction;
begin

select count(1)
into qt_banda_w
from pls_ame_regra_banda a
where a.ie_tipo_banda = :new.ie_tipo_banda
and a.nr_seq_regra = :new.nr_seq_regra
and :old.nr_sequencia <> :new.nr_sequencia;

if(qt_banda_w > 0) then
  if(:new.ie_tipo_banda = 1) then
    wheb_mensagem_pck.exibir_mensagem_abort(1056922);
  elsif (:new.ie_tipo_banda = 4) then
    wheb_mensagem_pck.exibir_mensagem_abort(1056923);
  end if;
end if;

select count(1)
into qt_atributos_w
from pls_ame_regra_atrib a
where a.nr_seq_banda = :old.nr_sequencia;

if(nvl(:old.ie_tipo_banda, 0) <> :new.ie_tipo_banda and qt_atributos_w > 0) then
  wheb_mensagem_pck.exibir_mensagem_abort(1056924);
end if;

end pls_ame_regra_banda_befinsup;
/


ALTER TABLE TASY.PLS_AME_REGRA_BANDA ADD (
  CONSTRAINT PLSARBAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_AME_REGRA_BANDA ADD (
  CONSTRAINT PLSARBAN_PLSARQ_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_AME_REGRA_ARQ (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AME_REGRA_BANDA TO NIVEL_1;

