ALTER TABLE TASY.ORCAMENTO_PACIENTE_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORCAMENTO_PACIENTE_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORCAMENTO_PACIENTE_PROC
(
  NR_SEQUENCIA_ORCAMENTO     NUMBER(10)         NOT NULL,
  CD_PROCEDIMENTO            NUMBER(15)         NOT NULL,
  IE_ORIGEM_PROCED           NUMBER(10)         NOT NULL,
  QT_PROCEDIMENTO            NUMBER(9,3)        NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  VL_PROCEDIMENTO            NUMBER(15,2)       NOT NULL,
  VL_MEDICO                  NUMBER(15,2),
  VL_ANESTESISTA             NUMBER(15,2),
  VL_FILME                   NUMBER(15,2),
  VL_AUXILIARES              NUMBER(15,2),
  VL_CUSTO_OPERACIONAL       NUMBER(15,2),
  VL_DESCONTO                NUMBER(15,2),
  CD_MEDICO                  VARCHAR2(10 BYTE),
  IE_PROCEDIMENTO_PRINCIPAL  VARCHAR2(1 BYTE),
  QT_DIA_INTERNACAO          NUMBER(3),
  IE_VALOR_INFORMADO         VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQUENCIA               NUMBER(10),
  NR_SEQ_EXAME               NUMBER(10),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  IE_VIA_ACESSO              VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_LANC          NUMBER(10),
  VL_CUSTO                   NUMBER(15,2),
  IE_PACOTE                  VARCHAR2(1 BYTE),
  NR_SEQ_PROC_PRINC          NUMBER(10),
  IE_AUTORIZACAO             VARCHAR2(3 BYTE),
  IE_LADO                    VARCHAR2(1 BYTE),
  TX_PROCEDIMENTO            NUMBER(15,4),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_ADICIONAL               VARCHAR2(1 BYTE),
  IE_AGENDAVEL               VARCHAR2(1 BYTE),
  IE_EXAME_ASSOC             VARCHAR2(1 BYTE),
  PR_DESCONTO                NUMBER(15,2),
  NR_SEQ_PACOTE              NUMBER(10),
  QT_DIAS_ENTREGA_EXAME      NUMBER(10),
  CD_MATERIAL_EXAME          VARCHAR2(20 BYTE),
  NR_SEQ_AJUSTE_PROC         NUMBER(10),
  CD_EDICAO_AMB              VARCHAR2(240 BYTE),
  VL_ORIGINAL_TABELA         NUMBER(15,2),
  CD_PROCED_TUSS             NUMBER(15),
  VL_PROCED_GLOSA            NUMBER(15,2),
  NR_SEQ_ORCAMENTO_PADRAO    NUMBER(10),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_REGRA_PRECO         NUMBER(10),
  IE_REGRA_PLANO             NUMBER(2)          DEFAULT null,
  QT_PORTE_ANESTESICO        NUMBER(2),
  CD_PROCEDIMENTO_LOC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.ORCPAPR_EXALABO_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAPR_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAPR_MATEXLA_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAPR_MATEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAPR_MEDICO_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAPR_ORCPACI_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQUENCIA_ORCAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAPR_ORCPADR_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQ_ORCAMENTO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCPAPR_PK ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAPR_PROCEDI_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAPR_PROINTE_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAPR_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAPR_REGLAAU_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQ_REGRA_LANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAPR_REGLAAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAPR_REGPRPR_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(NR_SEQ_REGRA_PRECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAPR_SETATEN_FK_I ON TASY.ORCAMENTO_PACIENTE_PROC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE OR REPLACE TRIGGER TASY.ORCAMENTO_PACIENTE_PROC_tp  after update ON TASY.ORCAMENTO_PACIENTE_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_EXAME,1,4000),substr(:new.NR_SEQ_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_PLANO,1,4000),substr(:new.IE_REGRA_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_PLANO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_PRECO,1,4000),substr(:new.NR_SEQ_REGRA_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_PRECO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.orcamento_paciente_proc_delete
after delete ON TASY.ORCAMENTO_PACIENTE_PROC for each row
declare

begin

if	(:old.ie_pacote = 'S') then
	ATUALIZAR_TX_ORCAMENTO_PACOTE(:old.nr_sequencia_orcamento, :old.nr_sequencia);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Orcamento_Pac_Proc_Insert
before insert ON TASY.ORCAMENTO_PACIENTE_PROC for each row
declare

cd_estabelecimento_w		number(4,0);
ie_atualiza_dias_exame_w	varchar2(2):= 'N';

begin

select 	nvl(max(cd_estabelecimento), wheb_usuario_pck.get_cd_estabelecimento)
into	cd_estabelecimento_w
from 	orcamento_paciente
where 	nr_sequencia_orcamento = :new.nr_sequencia_orcamento;

ie_atualiza_dias_exame_w := nvl(Obter_valor_param_usuario(106, 121, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w),'N');

if	(ie_atualiza_dias_exame_w = 'S') and
	(nvl(:new.nr_seq_exame,0) > 0) then
	:new.qt_dias_entrega_exame:= obter_exame_lab_regra_setor(cd_estabelecimento_w, :new.nr_seq_exame, 'QTDE');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.orcamento_paciente_proc_insert
before insert ON TASY.ORCAMENTO_PACIENTE_PROC for each row
declare

ie_tipo_convenio_w		number(2);
cd_setor_w			number(5);
cd_material_exame_w		varchar(20);
ie_tipo_atendimento_w		number(3);
cd_estabelecimento_w		number(5);
cd_plano_convenio_w		varchar2(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ds_erro_w			varchar2(255);
nr_seq_proc_interno_aux_w	number(10);
cd_estabelecimento_orc_w	number(5);
ie_tipo_atendimento_orc_w	number(3);
cd_plano_convenio_orc_w		varchar2(10);
nr_atendimento_w		number(10);
cd_convenio_w			number(5);
cd_categoria_convenio_w		varchar2(10);

begin

if (:new.nr_seq_proc_interno is null) then
	select	max(cd_estabelecimento),
		max(ie_tipo_atendimento),
		max(obter_plano_conv_atend(nr_atendimento)),
		max(nr_atendimento),
		max(cd_convenio),
		max(cd_categoria)
	into	cd_estabelecimento_orc_w,
		ie_tipo_atendimento_orc_w,
		cd_plano_convenio_orc_w,
		nr_atendimento_w,
		cd_convenio_w,
		cd_categoria_convenio_w
	from	orcamento_paciente
	where	nr_sequencia_orcamento = :new.nr_sequencia_orcamento;


	if ((cd_estabelecimento_orc_w is null) or
		(ie_tipo_atendimento_orc_w is null) or
		(cd_plano_convenio_orc_w is null)) then
		select	max(cd_estabelecimento),
			max(ie_tipo_atendimento),
			max(obter_plano_conv_atend(nr_atendimento))
		into	cd_estabelecimento_w,
			ie_tipo_atendimento_w,
			cd_plano_convenio_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_w;
	end if;

	if	(:new.nr_seq_exame is not null) then
		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from 	convenio
		where 	cd_convenio = cd_convenio_w;

		cd_setor_w := :new.cd_setor_atendimento;

		obter_exame_lab_convenio(:new.nr_seq_exame,
					 cd_convenio_w,
					 cd_categoria_convenio_w,
					 nvl(ie_tipo_atendimento_orc_w, ie_tipo_atendimento_w),
					 nvl(cd_estabelecimento_orc_w, cd_estabelecimento_w),
					 ie_tipo_convenio_w,
					 :new.nr_seq_proc_interno,
					 :new.cd_material_exame,
					 nvl(cd_plano_convenio_orc_w, cd_plano_convenio_w),
					 cd_setor_w,
					 cd_procedimento_w,
					 ie_origem_proced_w,
					 ds_erro_w,
					 nr_seq_proc_interno_aux_w,
					 :new.dt_atualizacao);

		if 	(nr_seq_proc_interno_aux_w > 0) then
			:new.nr_seq_proc_interno := nr_seq_proc_interno_aux_w;
		end if;
		if	(nvl(cd_procedimento_w,0) > 0) then
			:new.cd_procedimento	:= cd_procedimento_w;
			:new.ie_origem_proced	:= ie_origem_proced_w;
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.ORCAMENTO_PACIENTE_PROC ADD (
  CONSTRAINT ORCPAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORCAMENTO_PACIENTE_PROC ADD (
  CONSTRAINT ORCPAPR_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT ORCPAPR_ORCPADR_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO_PADRAO) 
 REFERENCES TASY.ORCAMENTO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT ORCPAPR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORCPAPR_REGPRPR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRECO) 
 REFERENCES TASY.REGRA_PRECO_PROC (NR_SEQUENCIA),
  CONSTRAINT ORCPAPR_REGLAAU_FK 
 FOREIGN KEY (NR_SEQ_REGRA_LANC) 
 REFERENCES TASY.REGRA_LANC_AUTOMATICO (NR_SEQUENCIA),
  CONSTRAINT ORCPAPR_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT ORCPAPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT ORCPAPR_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT ORCPAPR_ORCPACI_FK 
 FOREIGN KEY (NR_SEQUENCIA_ORCAMENTO) 
 REFERENCES TASY.ORCAMENTO_PACIENTE (NR_SEQUENCIA_ORCAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ORCPAPR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ORCAMENTO_PACIENTE_PROC TO NIVEL_1;

