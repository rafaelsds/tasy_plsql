ALTER TABLE TASY.PLS_PP_CTA_REC_SELECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PP_CTA_REC_SELECAO CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_PP_CTA_REC_SELECAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_ID_TRANSACAO        NUMBER(10)             NOT NULL,
  IE_TIPO_REGISTRO       VARCHAR2(2 BYTE)       NOT NULL,
  NR_SEQ_CONTA_REC       NUMBER(10)             NOT NULL,
  NR_SEQ_FILTRO          NUMBER(10)             NOT NULL,
  IE_VALIDO              VARCHAR2(1 BYTE)       NOT NULL,
  IE_VALIDO_TEMP         VARCHAR2(1 BYTE)       NOT NULL,
  IE_EXCECAO             VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_RESUMO_REC      NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA_PROC_REC  NUMBER(10),
  NR_SEQ_CONTA_MAT_REC   NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.PPCRSEL_I1 ON TASY.PLS_PP_CTA_REC_SELECAO
(NR_ID_TRANSACAO, IE_EXCECAO);


CREATE INDEX TASY.PPCRSEL_I2 ON TASY.PLS_PP_CTA_REC_SELECAO
(NR_ID_TRANSACAO, NR_SEQ_FILTRO, IE_VALIDO);


CREATE INDEX TASY.PPCRSEL_I3 ON TASY.PLS_PP_CTA_REC_SELECAO
(NR_ID_TRANSACAO, IE_VALIDO);


CREATE UNIQUE INDEX TASY.PPCRSEL_PK ON TASY.PLS_PP_CTA_REC_SELECAO
(NR_SEQUENCIA);


ALTER TABLE TASY.PLS_PP_CTA_REC_SELECAO ADD (
  CONSTRAINT PPCRSEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PP_CTA_REC_SELECAO TO NIVEL_1;

