ALTER TABLE TASY.AUDITORIA_MATPACI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUDITORIA_MATPACI CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUDITORIA_MATPACI
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_AUDITORIA        NUMBER(10)            NOT NULL,
  NR_SEQ_MATPACI          NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO           NUMBER(10),
  QT_ORIGINAL             NUMBER(15,3)          NOT NULL,
  QT_AJUSTE               NUMBER(15,3),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  CD_CONVENIO_AJUSTE      NUMBER(5),
  CD_CATEGORIA_AJUSTE     VARCHAR2(10 BYTE),
  IE_TIPO_AUDITORIA       VARCHAR2(1 BYTE)      NOT NULL,
  IE_PERDA                VARCHAR2(1 BYTE),
  CD_SETOR_AJUSTE         NUMBER(10),
  NR_SEQ_ATEPACU_AJUSTE   NUMBER(10),
  CD_SETOR_RESP_GLOSA     NUMBER(5),
  CD_PESSOA_RESP_JUST     VARCHAR2(10 BYTE),
  VL_MATERIAL             NUMBER(15,2),
  VL_MATERIAL_AJUSTE      NUMBER(15,2),
  NR_CONTA_ORIGEM         NUMBER(10),
  NR_SEQ_ESTAGIO_PEND     NUMBER(10),
  VL_TOTAL_AJUSTE         NUMBER(15,2),
  IE_EXIGE_AUDITORIA      VARCHAR2(1 BYTE),
  CD_PESSOA_RESP_GLOSA    VARCHAR2(10 BYTE),
  NR_SEQ_KIT_EST          NUMBER(10),
  DS_MAT_KIT_EST          VARCHAR2(150 BYTE),
  NR_SEQ_LOTE             NUMBER(10),
  CD_MEDICO_AUTENTICACAO  VARCHAR2(10 BYTE),
  CD_SETOR_ORIGINAL       NUMBER(5),
  IE_ORIGINAL             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUDMATP_ATEPACU_FK_I ON TASY.AUDITORIA_MATPACI
(NR_SEQ_ATEPACU_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_ATEPACU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_AUDCOPA_FK_I ON TASY.AUDITORIA_MATPACI
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDMATP_AUDMOTI_FK_I ON TASY.AUDITORIA_MATPACI
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_AUDMOTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_CATCONV_FK_I ON TASY.AUDITORIA_MATPACI
(CD_CONVENIO_AJUSTE, CD_CATEGORIA_AJUSTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_CONVENI_FK_I ON TASY.AUDITORIA_MATPACI
(CD_CONVENIO_AJUSTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_MATPACI_FK_I ON TASY.AUDITORIA_MATPACI
(NR_SEQ_MATPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDMATP_PESFISI_FK_I ON TASY.AUDITORIA_MATPACI
(CD_PESSOA_RESP_JUST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDMATP_PESFISI_FK2_I ON TASY.AUDITORIA_MATPACI
(CD_PESSOA_RESP_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUDMATP_PK ON TASY.AUDITORIA_MATPACI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDMATP_SETATEN_FK_I ON TASY.AUDITORIA_MATPACI
(CD_SETOR_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_SETATEN_FK2_I ON TASY.AUDITORIA_MATPACI
(CD_SETOR_RESP_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDMATP_SETATEN_FK3_I ON TASY.AUDITORIA_MATPACI
(CD_SETOR_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDMATP_SETATEN_FK3_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_MATPACI_UPDATE
before update ON TASY.AUDITORIA_MATPACI for each row
declare

nr_seq_pend_w		Number(10,0);
nr_seq_estagio_pend_w	Number(10,0);
cd_setor_resp_glosa_w	number(5);

vl_item_w		number(15,2);
qt_ajuste_w		number(10,0);
qt_original_w		number(10,0);
vl_unitario_w		number(15,4);
qt_dif_w		number(10,0);

nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
nr_conta_audit_w	number(10,0);
ds_call_stack_w		varchar2(2000);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_conta_w		date;
dt_atendimento_w	date;
qt_item_periodo_w	number(10,0);

begin

select 	max(nr_sequencia)
into	nr_seq_pend_w
from 	cta_pendencia
where 	nr_seq_auditoria = :new.nr_seq_auditoria;

if	(nvl(nr_seq_pend_w,0) > 0) then

	select 	max(nr_seq_estagio)
	into	nr_seq_estagio_pend_w
	from  	cta_pendencia
	where 	nr_sequencia = nr_seq_pend_w;

	if	(nvl(nr_seq_estagio_pend_w,0) > 0) then

		:new.nr_seq_estagio_pend:= nr_seq_estagio_pend_w;

	end if;
end if;

if	(nvl(:old.nr_seq_motivo,0) <> nvl(:new.nr_seq_motivo,0)) then

	insert into AUDIT_MATPACI_MOTIVO(
			NR_SEQUENCIA,
			NR_SEQ_AUDIT_MAT,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			NR_SEQ_MOTIVO,
			NR_SEQ_MOTIVO_ANT)
		values	(AUDIT_MATPACI_MOTIVO_seq.NextVal,
			:new.nr_sequencia,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_seq_motivo, -- Novo
			:old.nr_seq_motivo); -- Antigo

end if;

if	(:new.nr_seq_motivo is not null) then

	begin
	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_resp_glosa_w
	from 	regra_setor_resp_glosa
	where	nr_seq_motivo = :new.nr_seq_motivo
	and		ie_situacao = 'A';
	exception
	when others then
		cd_setor_resp_glosa_w := 0;
	end;

	if	(cd_setor_resp_glosa_w > 0) then
		:new.cd_setor_resp_glosa	 := cd_setor_resp_glosa_w;
	end if;

end if;

-- OS 377968
begin
select	nvl(a.vl_material,0),
	a.vl_unitario
into	vl_item_w,
	vl_unitario_w
from	material_atend_paciente a
where	a.nr_sequencia		= :new.nr_seq_matpaci
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
exception
	when others then
	:new.vl_total_ajuste:= 0;
end;

if	(:new.ie_tipo_auditoria = 'X') then
	:new.vl_total_ajuste:= 0;
else
	if	(:new.ie_tipo_auditoria = 'V') or (:new.vl_material_ajuste is not null) then

		:new.vl_total_ajuste:= :new.vl_material_ajuste;

		if 	(:new.qt_ajuste is not null) then

			qt_dif_w:= :new.qt_ajuste - :new.qt_original;
			:new.vl_total_ajuste:=   :new.vl_total_ajuste + (dividir(:new.vl_material_ajuste,:new.qt_original) * qt_dif_w);

		end if;

	else
		if 	(:new.qt_ajuste is not null) and
			((nvl(:new.qt_ajuste,-1) <> nvl(:old.qt_ajuste,-1)) or (:new.qt_original <> :old.qt_original)) then

			:new.vl_total_ajuste:= vl_item_w;
			qt_dif_w:= :new.qt_ajuste - :new.qt_original;
			:new.vl_total_ajuste:=   :new.vl_total_ajuste + (vl_unitario_w * qt_dif_w);

		end if;

	end if;
end if;

select	max(nr_interno_conta),
	max(nr_atendimento),
	max(dt_conta),
	max(dt_atendimento)
into	nr_interno_conta_w,
	nr_atendimento_w,
	dt_conta_w,
	dt_atendimento_w
from	material_atend_paciente
where	nr_sequencia = :new.nr_seq_matpaci;

select	max(nr_interno_conta),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_conta_audit_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from	auditoria_conta_paciente
where	nr_sequencia = :new.nr_seq_auditoria;

qt_item_periodo_w	:= 1;
begin
select	nvl(max(1),0)
into	qt_item_periodo_w
from	dual
where	((dt_conta_w between dt_periodo_inicial_w and dt_periodo_final_w) or
	 (dt_atendimento_w between dt_periodo_inicial_w and dt_periodo_final_w));
exception
	when others then
	qt_item_periodo_w	:= 1;
end;

if	((nvl(nr_interno_conta_w,0) <> 0) and
	 (nvl(nr_conta_audit_w,0) <> 0) and
	 (nvl(nr_interno_conta_w,0) <> nvl(nr_conta_audit_w,0))) or
	(qt_item_periodo_w = 0) then

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into w_log_atend_audit (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_audit_item,
		nr_seq_auditoria,
		ds_call_stack,
		ie_tipo_item,
		nr_seq_item,
		dt_periodo_inicial,
		dt_periodo_final,
		ds_observacao
	) values (
		w_log_atend_audit_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_auditoria,
		ds_call_stack_w,
		'M',
		:new.nr_seq_matpaci,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		decode(qt_item_periodo_w, 0, substr(wheb_mensagem_pck.get_texto(309510),1,255), substr(wheb_mensagem_pck.get_texto(309511),1,255))
	);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_MATPACI_INSERT
before insert ON TASY.AUDITORIA_MATPACI for each row
declare

nr_seq_pend_w		Number(10,0);
nr_seq_estagio_pend_w	Number(10,0);
vl_item_w		number(15,2);
nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
nr_conta_audit_w	number(10,0);
ds_call_stack_w		varchar2(2000);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_conta_w		date;
dt_atendimento_w	date;
qt_item_periodo_w	number(10,0);

begin

select 	max(nr_sequencia)
into	nr_seq_pend_w
from 	cta_pendencia
where 	nr_seq_auditoria = :new.nr_seq_auditoria;

if	(nvl(nr_seq_pend_w,0) > 0) then

	select 	max(nr_seq_estagio)
	into	nr_seq_estagio_pend_w
	from  	cta_pendencia
	where 	nr_sequencia = nr_seq_pend_w;

	if	(nvl(nr_seq_estagio_pend_w,0) > 0) then

		:new.nr_seq_estagio_pend:= nr_seq_estagio_pend_w;

	end if;
end if;

begin
select	nvl(a.vl_material,0)
into	vl_item_w
from	material_atend_paciente a
where	a.nr_sequencia		= :new.nr_seq_matpaci
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
exception
	when others then
	:new.vl_total_ajuste:= 0;
	vl_item_w:= 0;
end;

:new.vl_total_ajuste:= vl_item_w;

:new.ie_exige_auditoria := obter_se_exige_auditoria(:new.nr_seq_matpaci, 1);

select	max(nr_interno_conta),
	max(nr_atendimento),
	max(dt_conta),
	max(dt_atendimento)
into	nr_interno_conta_w,
	nr_atendimento_w,
	dt_conta_w,
	dt_atendimento_w
from	material_atend_paciente
where	nr_sequencia = :new.nr_seq_matpaci;

select	max(nr_interno_conta),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_conta_audit_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from	auditoria_conta_paciente
where	nr_sequencia = :new.nr_seq_auditoria;

qt_item_periodo_w	:= 1;
begin
select	nvl(max(1),0)
into	qt_item_periodo_w
from	dual
where	((dt_conta_w between dt_periodo_inicial_w and dt_periodo_final_w) or
	 (dt_atendimento_w between dt_periodo_inicial_w and dt_periodo_final_w));
exception
	when others then
	qt_item_periodo_w	:= 1;
end;

if	((nvl(nr_interno_conta_w,0) <> 0) and
	 (nvl(nr_conta_audit_w,0) <> 0) and
	 (nvl(nr_interno_conta_w,0) <> nvl(nr_conta_audit_w,0))) or
	(qt_item_periodo_w = 0) then

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into w_log_atend_audit (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_audit_item,
		nr_seq_auditoria,
		ds_call_stack,
		ie_tipo_item,
		nr_seq_item,
		dt_periodo_inicial,
		dt_periodo_final,
		ds_observacao
	) values (
		w_log_atend_audit_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_auditoria,
		ds_call_stack_w,
		'M',
		:new.nr_seq_matpaci,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		decode(qt_item_periodo_w, 0, substr(wheb_mensagem_pck.get_texto(309510),1,255), substr(wheb_mensagem_pck.get_texto(309511),1,255))
	);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_MATPACI_tp  after update ON TASY.AUDITORIA_MATPACI FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.QT_AJUSTE,1,500);gravar_log_alteracao(substr(:old.QT_AJUSTE,1,4000),substr(:new.QT_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_AJUSTE',ie_log_w,ds_w,'AUDITORIA_MATPACI',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AUDITORIA_MATPACI ADD (
  CONSTRAINT AUDMATP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUDITORIA_MATPACI ADD (
  CONSTRAINT AUDMATP_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_ORIGINAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDMATP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_AJUSTE) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AUDMATP_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_AJUSTE, CD_CATEGORIA_AJUSTE) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AUDMATP_MATPACI_FK 
 FOREIGN KEY (NR_SEQ_MATPACI) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUDMATP_AUDCOPA_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.AUDITORIA_CONTA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUDMATP_AUDMOTI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.AUDITORIA_MOTIVO (NR_SEQUENCIA),
  CONSTRAINT AUDMATP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_AJUSTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDMATP_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_RESP_GLOSA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDMATP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP_JUST) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUDMATP_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP_GLOSA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AUDITORIA_MATPACI TO NIVEL_1;

