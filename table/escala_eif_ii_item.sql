ALTER TABLE TASY.ESCALA_EIF_II_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_EIF_II_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_EIF_II_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESCALA        NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  NR_SEQ_RESULT        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESEIIII_EIEITRE_FK_I ON TASY.ESCALA_EIF_II_ITEM
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESEIIII_EIEITRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESEIIII_EIESIII_FK_I ON TASY.ESCALA_EIF_II_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESEIIII_EIESIII_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESEIIII_ESCEIII_FK_I ON TASY.ESCALA_EIF_II_ITEM
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESEIIII_ESCEIII_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ESEIIII_PK ON TASY.ESCALA_EIF_II_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESEIIII_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_escala_eif_ii_item_ins
before insert ON TASY.ESCALA_EIF_II_ITEM for each row
declare

qt_reg_w number(10);
nr_seq_result_w number(10);
qt_pontuacao_w number(10,2);

/*
    trigger para setar automaticamente o resultado com o valor minimo
*/

begin

    select count(*)
    into qt_reg_w
    from escala_eif_ii
    where nr_sequencia = :new.nr_seq_escala
    and nr_seq_escala in(2, 4, 8); --escala lapss

    if(qt_reg_w > 0 and :new.nr_seq_result is null)then
    
        select min(qt_pontuacao)
        into qt_pontuacao_w
        from eif_escala_ii_item_result
        where nr_seq_item = :new.nr_seq_item;

        select min(nr_sequencia)
        into nr_seq_result_w
        from eif_escala_ii_item_result
        where nr_seq_item = :new.nr_seq_item
        and qt_pontuacao = qt_pontuacao_w;
        
        if(nr_seq_result_w is not null)then
            :new.nr_seq_result := nr_seq_result_w;
        end if;
    
    end if;

end;
/


ALTER TABLE TASY.ESCALA_EIF_II_ITEM ADD (
  CONSTRAINT ESEIIII_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_EIF_II_ITEM ADD (
  CONSTRAINT ESEIIII_EIEITRE_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM_RESULT (NR_SEQUENCIA),
  CONSTRAINT ESEIIII_EIESIII_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM (NR_SEQUENCIA),
  CONSTRAINT ESEIIII_ESCEIII_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.ESCALA_EIF_II (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_EIF_II_ITEM TO NIVEL_1;

