ALTER TABLE TASY.INCISAO_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INCISAO_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.INCISAO_CIRURGIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_TOPOGRAFIA          NUMBER(10),
  QT_TAMANHO                 NUMBER(5,2),
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_LADO                    VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_INCISAO        NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  IE_NAO_REALIZA             VARCHAR2(1 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DT_EXECUCAO                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INCICIR_CIRURGI_FK_I ON TASY.INCISAO_CIRURGIA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INCICIR_PEPOCIR_FK_I ON TASY.INCISAO_CIRURGIA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INCICIR_PESFISI_FK_I ON TASY.INCISAO_CIRURGIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INCICIR_PK ON TASY.INCISAO_CIRURGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INCICIR_TASASDI_FK_I ON TASY.INCISAO_CIRURGIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INCICIR_TASASDI_FK2_I ON TASY.INCISAO_CIRURGIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.INCICIR_TIPOINC_FK_I ON TASY.INCISAO_CIRURGIA
(NR_SEQ_TIPO_INCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_TIPOINC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INCICIR_TOPDOR_FK_I ON TASY.INCISAO_CIRURGIA
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCICIR_TOPDOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.incisao_cirurgia_pend_atual 
after insert or update ON TASY.INCISAO_CIRURGIA 
for each row
declare 
 
qt_reg_w			number(1); 
ie_tipo_w			varchar2(10); 
ie_libera_incisao_w		varchar2(10); 
cd_pessoa_fisica_w		varchar2(10); 
nr_atendimento_w		number(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_libera_incisao) 
into	ie_libera_incisao_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
select	max(c.nr_atendimento), 
	max(c.cd_pessoa_fisica) 
into	nr_atendimento_w, 
	cd_pessoa_fisica_w 
from	cirurgia c 
where	c.nr_cirurgia = :new.nr_cirurgia; 
 
if 	(cd_pessoa_fisica_w is null) then 
	select	max(c.nr_atendimento), 
		max(c.cd_pessoa_fisica) 
	into	nr_atendimento_w, 
		cd_pessoa_fisica_w 
	from	pepo_cirurgia c 
	where	c.nr_sequencia = :new.nr_seq_pepo; 
end if; 
 
if	(nvl(ie_libera_incisao_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'IC'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
 
		 
		ie_tipo_w := 'XIC'; 
	end if; 
			 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario); 
	end if; 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


ALTER TABLE TASY.INCISAO_CIRURGIA ADD (
  CONSTRAINT INCICIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INCISAO_CIRURGIA ADD (
  CONSTRAINT INCICIR_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT INCICIR_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT INCICIR_TIPOINC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_INCISAO) 
 REFERENCES TASY.TIPO_INCISAO (NR_SEQUENCIA),
  CONSTRAINT INCICIR_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INCICIR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT INCICIR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT INCICIR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.INCISAO_CIRURGIA TO NIVEL_1;

