ALTER TABLE TASY.ESCALA_RANSON
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_RANSON CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_RANSON
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_PANCREATITE_AGUDA   VARCHAR2(1 BYTE)       NOT NULL,
  QT_IDADE               NUMBER(3)              NOT NULL,
  QT_LEUCOCITOS          NUMBER(6)              NOT NULL,
  QT_GLICEMIA            NUMBER(4)              NOT NULL,
  QT_DHL                 NUMBER(4)              NOT NULL,
  QT_TGO                 NUMBER(3)              NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  QT_AUMENTO_BUN         NUMBER(3,1)            NOT NULL,
  PR_QUEDA_HEMATOCRITO   NUMBER(2)              NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  QT_UREIA               NUMBER(3)              NOT NULL,
  QT_CALCEMIA            NUMBER(2)              NOT NULL,
  QT_PAO2                NUMBER(3)              NOT NULL,
  QT_BASE_EXCESS         NUMBER(2)              NOT NULL,
  QT_SEQUESTRO_LIQUIDO   NUMBER(2)              NOT NULL,
  QT_PONTUACAO           NUMBER(2)              NOT NULL,
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCRANS_ATEPACI_FK_I ON TASY.ESCALA_RANSON
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCRANS_PESFISI_FK_I ON TASY.ESCALA_RANSON
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCRANS_PK ON TASY.ESCALA_RANSON
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Escala_Ranson_Atual
before insert or update ON TASY.ESCALA_RANSON for each row
declare
qt_pontuacao_w	number(10)	:= 0;
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

if	(:new.IE_PANCREATITE_AGUDA	= 'N') then
	if	(:new.qt_idade	> 55) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_LEUCOCITOS	> 16000) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_GLICEMIA	> 200) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_DHL	> 350) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_TGO	> 250) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.PR_QUEDA_HEMATOCRITO	> 10) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_AUMENTO_BUN	> 5) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_CALCEMIA	< 8) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_PAO2	< 60) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;

	if	(:new.QT_BASE_EXCESS	> 4) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_SEQUESTRO_LIQUIDO	> 6) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;

elsif	(:new.IE_PANCREATITE_AGUDA	= 'B') then
	if	(:new.qt_idade	> 70) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_LEUCOCITOS	> 18000) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_GLICEMIA	> 220) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_DHL	> 400) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_TGO	> 250) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.PR_QUEDA_HEMATOCRITO	> 10) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_AUMENTO_BUN	> 2) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_CALCEMIA	< 8) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	/*
	if	(:new.QT_PAO2	< 60) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	*/
	if	(:new.QT_BASE_EXCESS	> 5) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
	if	(:new.QT_SEQUESTRO_LIQUIDO	> 4) then
		qt_pontuacao_w	:= qt_pontuacao_w+1;
	end if;
end if;
:new.qt_pontuacao	:= qt_pontuacao_w;

end;
/


ALTER TABLE TASY.ESCALA_RANSON ADD (
  CONSTRAINT ESCRANS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_RANSON ADD (
  CONSTRAINT ESCRANS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCRANS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_RANSON TO NIVEL_1;

