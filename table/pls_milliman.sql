ALTER TABLE TASY.PLS_MILLIMAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MILLIMAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MILLIMAN
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_MED_TED  NUMBER(10),
  DT_NASCIMENTO         DATE,
  IE_SEXO               VARCHAR2(1 BYTE),
  NR_CPF                VARCHAR2(11 BYTE),
  DT_INGRESSO           DATE,
  DT_SAIDA              DATE,
  IE_GRAU_DEPENDENCIA   VARCHAR2(255 BYTE),
  IE_TIPO_CONTRATO      VARCHAR2(255 BYTE),
  IE_TIPO_GARANTIA      VARCHAR2(255 BYTE),
  NR_MATRICULA_BENEF    VARCHAR2(255 BYTE),
  NM_BENEFICIARIO       VARCHAR2(255 BYTE),
  NM_MUNICIPIO          VARCHAR2(255 BYTE),
  CD_PRODUTO_ANS        VARCHAR2(255 BYTE),
  NR_MATRICULA_TITULAR  VARCHAR2(255 BYTE),
  NM_LOTE               VARCHAR2(255 BYTE),
  CD_RAMO_ATIVIDADE     VARCHAR2(255 BYTE),
  IE_TIPO_CONTRATACAO   VARCHAR2(255 BYTE),
  NR_CPF_TITULAR        VARCHAR2(255 BYTE),
  NR_MATRICULA_PJ       VARCHAR2(255 BYTE),
  NR_CNPJ               VARCHAR2(255 BYTE),
  DS_RAZAO_SOCIAL       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMILL_PK ON TASY.PLS_MILLIMAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMILL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSMILL_PLSCOMT_FK_I ON TASY.PLS_MILLIMAN
(NR_SEQ_CONTA_MED_TED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMILL_PLSCOMT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_MILLIMAN_tp  after update ON TASY.PLS_MILLIMAN FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'PLS_MILLIMAN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'PLS_MILLIMAN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PLS_MILLIMAN',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MILLIMAN ADD (
  CONSTRAINT PLSMILL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MILLIMAN ADD (
  CONSTRAINT PLSMILL_PLSCOMT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MED_TED) 
 REFERENCES TASY.PLS_CONTA_MEDICA_TED (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MILLIMAN TO NIVEL_1;

