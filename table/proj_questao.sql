ALTER TABLE TASY.PROJ_QUESTAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_QUESTAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_QUESTAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_QUESTAO           VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO_RISCO    NUMBER(10),
  NR_SEQ_PROJETO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRQU_PK ON TASY.PROJ_QUESTAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRQU_PROJTIRI_FK_I ON TASY.PROJ_QUESTAO
(NR_SEQ_TIPO_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRQU_PROPROJ_FK_I ON TASY.PROJ_QUESTAO
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROJ_QUESTAO ADD (
  CONSTRAINT PRQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_QUESTAO ADD (
  CONSTRAINT PRQU_PROJTIRI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_RISCO) 
 REFERENCES TASY.PROJ_TIPO_RISCO (NR_SEQUENCIA),
  CONSTRAINT PRQU_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_QUESTAO TO NIVEL_1;

