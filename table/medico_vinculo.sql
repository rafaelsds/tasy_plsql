ALTER TABLE TASY.MEDICO_VINCULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICO_VINCULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICO_VINCULO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MEDICO            VARCHAR2(10 BYTE)        NOT NULL,
  IE_VINCULO_MEDICO    NUMBER(2)                NOT NULL,
  DT_ADMISSAO          DATE,
  DT_EFETIVACAO        DATE,
  DT_DESLIGAMENTO      DATE,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_CATEGORIA     NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDIVIN_ESTABEL_FK_I ON TASY.MEDICO_VINCULO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDIVIN_MEDCATE_FK_I ON TASY.MEDICO_VINCULO
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDIVIN_MEDCATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDIVIN_PESFISI_FK_I ON TASY.MEDICO_VINCULO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDIVIN_PK ON TASY.MEDICO_VINCULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDIVIN_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MEDICO_VINCULO_tp  after update ON TASY.MEDICO_VINCULO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VINCULO_MEDICO,1,4000),substr(:new.IE_VINCULO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VINCULO_MEDICO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ADMISSAO,1,4000),substr(:new.DT_ADMISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ADMISSAO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DESLIGAMENTO,1,4000),substr(:new.DT_DESLIGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_DESLIGAMENTO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA,1,4000),substr(:new.NR_SEQ_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EFETIVACAO,1,4000),substr(:new.DT_EFETIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EFETIVACAO',ie_log_w,ds_w,'MEDICO_VINCULO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.medico_vinculo_befupdate_log
before update ON TASY.MEDICO_VINCULO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into medico_vinculo_log(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_medico,
		ie_vinculo_medico,
		dt_admissao,
		dt_efetivacao,
		dt_desligamento,
		ds_observacao,
		nr_seq_categoria)
	values( medico_vinculo_log_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.cd_medico,
		:old.ie_vinculo_medico,
		:old.dt_admissao,
		:old.dt_efetivacao,
		:old.dt_desligamento,
		:old.ds_observacao,
		:old.nr_seq_categoria);
end if;

end;
/


ALTER TABLE TASY.MEDICO_VINCULO ADD (
  CONSTRAINT MEDIVIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDICO_VINCULO ADD (
  CONSTRAINT MEDIVIN_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT MEDIVIN_MEDCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.MEDICO_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT MEDIVIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MEDICO_VINCULO TO NIVEL_1;

