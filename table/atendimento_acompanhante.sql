ALTER TABLE TASY.ATENDIMENTO_ACOMPANHANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_ACOMPANHANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_ACOMPANHANTE
(
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_ACOMPANHANTE        DATE                   NOT NULL,
  NR_ACOMPANHANTE        NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NM_ACOMPANHANTE        VARCHAR2(40 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  DT_SAIDA               DATE,
  NR_IDENTIDADE          VARCHAR2(15 BYTE),
  NR_TELEFONE            VARCHAR2(15 BYTE),
  NR_CONTROLE            VARCHAR2(20 BYTE),
  NR_SEQ_INTERNO         NUMBER(10),
  IE_ALOJAMENTO          VARCHAR2(1 BYTE),
  CD_PESSOA_AUTORIZACAO  VARCHAR2(10 BYTE),
  NM_USUARIO_SAIDA       VARCHAR2(15 BYTE),
  DT_ENTRADA_REAL        DATE,
  DT_SAIDA_REAL          DATE,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_LIBERACAO_ACESSO    DATE,
  NR_CONTROLE_ACESSO     NUMBER(10),
  NR_SEQ_TIPO            NUMBER(10),
  NR_DDI                 VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEACOM_ATEPACI_FK_I ON TASY.ATENDIMENTO_ACOMPANHANTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEACOM_PESFISI_FK_I ON TASY.ATENDIMENTO_ACOMPANHANTE
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEACOM_PESFISI_FK2_I ON TASY.ATENDIMENTO_ACOMPANHANTE
(CD_PESSOA_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEACOM_PK ON TASY.ATENDIMENTO_ACOMPANHANTE
(NR_ATENDIMENTO, DT_ACOMPANHANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEACOM_VISITAN_FK_I ON TASY.ATENDIMENTO_ACOMPANHANTE
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEACOM_VISITAN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_ACOMPANHANTE_tp  after update ON TASY.ATENDIMENTO_ACOMPANHANTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@DT_ACOMPANHANTE='||to_char(:old.DT_ACOMPANHANTE);gravar_log_alteracao(substr(:old.NM_ACOMPANHANTE,1,4000),substr(:new.NM_ACOMPANHANTE,1,4000),:new.nm_usuario,nr_seq_w,'NM_ACOMPANHANTE',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ACOMPANHANTE,1,4000),substr(:new.NR_ACOMPANHANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_ACOMPANHANTE',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ACOMPANHANTE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ACOMPANHANTE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ACOMPANHANTE',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_SAIDA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_SAIDA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_SAIDA',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALOJAMENTO,1,4000),substr(:new.IE_ALOJAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALOJAMENTO',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_AUTORIZACAO,1,4000),substr(:new.CD_PESSOA_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_AUTORIZACAO',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'ATENDIMENTO_ACOMPANHANTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_ATEND_ACOMPANHANTE_BEFDEL
BEFORE DELETE
ON TASY.ATENDIMENTO_ACOMPANHANTE 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
nm_usuario_w varchar(20);
BEGIN
    BEGIN
    select nm_usuario
    into nm_usuario_w
    from HSJ_AUTENTICACAO_WIFI where nm_usuario = TO_CHAR(OBTER_DADOS_PF(:old.cd_pessoa_fisica,'CPF'));
   
    EXCEPTION WHEN NO_DATA_FOUND THEN
      nm_usuario_w := 'N'; 
    end;                                       
    
    if nm_usuario_w <> 'N' then
      delete from tasy.hsj_autenticacao_wifi where nm_usuario = nm_usuario_w;
    end if;    
    
END;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_acomp_update
before update ON TASY.ATENDIMENTO_ACOMPANHANTE for each row
DECLARE
ie_possui_alta_w	varchar2(2);
ie_permite_alta_w     varchar(2);
begin
Obter_Param_Usuario(8014, 112, obter_perfil_ativo, :new.nm_usuario, 0, ie_permite_alta_w);

Select 	nvl(max('S'),'N')
into 	ie_possui_alta_w
from 	atendimento_paciente a
where 	:new.nr_atendimento = a.nr_atendimento
and 	a.dt_alta is not null;

if 	(:old.dt_saida is not null) and
	(:new.dt_saida is null) 	and
	(ie_possui_alta_w = 'S') 	and
	(ie_permite_alta_w = 'N')		then
	wheb_mensagem_pck.exibir_mensagem_abort(284133);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_acomp_insert
before INSERT ON TASY.ATENDIMENTO_ACOMPANHANTE FOR EACH ROW
DECLARE
ie_possui_alta_w		varchar2(2);
ie_permite_alta_w		varchar2(2);
ie_gerar_controle_acomp_w	varchar2(1);
nr_controle_novo_w		ATENDIMENTO_ACOMPANHANTE.nr_controle%type;
nr_controle_unico_w		varchar(2);

pragma autonomous_transaction;

begin
Obter_Param_Usuario(8014, 36,  obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_gerar_controle_acomp_w);
Obter_Param_Usuario(8014, 112, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_permite_alta_w);
Obter_Param_Usuario(8014, 119, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_controle_unico_w);

begin

	Select 	nvl(max('S'),'N')
	into 	ie_possui_alta_w
	from 	atendimento_paciente a
	where 	:new.nr_atendimento = a.nr_atendimento
	and 	a.dt_alta is not null;

	if 	(ie_possui_alta_w = 'S') and
		(ie_permite_alta_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(284133);
	end if;

	if	(ie_gerar_controle_acomp_w = 'S') and
		(:new.nr_controle is null) then

		if (nr_controle_unico_w = 'S') then
			select	CONTROLE_VISITA_UNICO_SEQ.nextval
			into	nr_controle_novo_w
			from	dual;
		else
			select	controle_acompanhante_seq.nextval
			into	nr_controle_novo_w
			from	dual;
		end if;
		:new.nr_controle := nr_controle_novo_w;
	end if;

	select	controle_visita_seq.nextval
	into	:new.nr_controle_acesso
	from	dual;

exception
	when	others then
		null;
end;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_acompanhante_atual
before insert or update ON TASY.ATENDIMENTO_ACOMPANHANTE for each row
declare
ie_atend_visit_bloq_w varchar(01) := '';
ie_atend_visit_lib_w varchar(01) := '';
ie_atend_visit_pessoa_w varchar(01) := '';
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    begin
        select 'S'
        into	ie_atend_visit_bloq_w
        from	atendimento_acomp_bloq
        where	nr_atendimento	= :new.nr_atendimento
        and   (cd_pessoa_fisica	= :new.cd_pessoa_fisica or NM_PESSOA_FISICA = :new.nm_acompanhante)
        and   rownum = 1;
    exception
    when others then
        ie_atend_visit_bloq_w := 'N';
    end;
    if (ie_atend_visit_bloq_w = 'S') then
        Wheb_mensagem_pck.exibir_mensagem_abort(1045155);
    end if;

    select	decode(count(*), 0, 'N', 'S')
    into	ie_atend_visit_lib_w
    from	acompanhante_visita_lib
    where	nr_atendimento	= :new.nr_atendimento
    and	(cd_pessoa_lib	is not null or nm_pessoa_liberacao is not null);

    select	decode(count(*), 0, 'S', 'N')
    into	ie_atend_visit_pessoa_w
    from	acompanhante_visita_lib a
    where	a.nr_atendimento	= :new.nr_atendimento
    and	(a.cd_pessoa_lib	= :new.cd_pessoa_fisica or upper(a.nm_pessoa_liberacao) = upper(:new.nm_acompanhante))
    and	not exists( select 	1
                    from	atendimento_acomp_bloq b
                    where	(b.cd_pessoa_fisica	= :new.cd_pessoa_fisica or b.NM_PESSOA_FISICA = :new.nm_acompanhante)
                    and		a.nr_atendimento = b.nr_atendimento
                    );

    if	(ie_atend_visit_lib_w = 'S' and
        ie_atend_visit_pessoa_w = 'S') then
        wheb_mensagem_pck.exibir_mensagem_abort(1045155);
    end if;

    if 	(:new.nr_controle is not null) and
        ((:old.nr_controle is null) or (:new.nr_controle != :old.nr_controle)) then
        valida_cracha_integracao(:new.nm_usuario, somente_numero(:new.nr_controle));
    end if;
end if;
    end;
/


ALTER TABLE TASY.ATENDIMENTO_ACOMPANHANTE ADD (
  CONSTRAINT ATEACOM_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, DT_ACOMPANHANTE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_ACOMPANHANTE ADD (
  CONSTRAINT ATEACOM_VISITAN_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.VISITANTE (NR_SEQUENCIA),
  CONSTRAINT ATEACOM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEACOM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEACOM_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_AUTORIZACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ATENDIMENTO_ACOMPANHANTE TO NIVEL_1;

