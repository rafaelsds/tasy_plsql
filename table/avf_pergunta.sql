ALTER TABLE TASY.AVF_PERGUNTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AVF_PERGUNTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AVF_PERGUNTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_PERGUNTA            VARCHAR2(255 BYTE)     NOT NULL,
  NR_SEQ_APRESENTACAO    NUMBER(5)              NOT NULL,
  NR_SEQ_TIPO_AVAL       NUMBER(10)             NOT NULL,
  NR_SEQ_GRUPO_PERGUNTA  NUMBER(10),
  NR_SEQ_TIPO_NOTA       NUMBER(10)             NOT NULL,
  NR_SEQ_NOTA_ITEM       NUMBER(10),
  PR_PESO_PERGUNTA       NUMBER(5,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AVFPERG_AVFGRPE_FK_I ON TASY.AVF_PERGUNTA
(NR_SEQ_GRUPO_PERGUNTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVFPERG_AVFGRPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVFPERG_AVFTIAV_FK_I ON TASY.AVF_PERGUNTA
(NR_SEQ_TIPO_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVFPERG_AVFTIAV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVFPERG_AVFTINO_FK_I ON TASY.AVF_PERGUNTA
(NR_SEQ_TIPO_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVFPERG_AVFTINO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AVFPERG_PK ON TASY.AVF_PERGUNTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.avf_pergunta_insert
after insert ON TASY.AVF_PERGUNTA for each row
declare
nr_seq_tipo_nota_item_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	avf_tipo_nota_item
	where	nr_seq_tipo_nota = :new.nr_seq_tipo_nota;

begin

open C01;
loop
fetch C01 into
	nr_seq_tipo_nota_item_w;
exit when C01%notfound;
	begin

	insert into avf_pergunta_tipo (	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_pergunta,
			nr_seq_tipo_nota_item)
	values	(	avf_pergunta_tipo_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario_nrec,
			:new.nr_sequencia,
			nr_seq_tipo_nota_item_w);
	end;
end loop;
close C01;

end;
/


CREATE OR REPLACE TRIGGER TASY.avf_pergunta_update
after update ON TASY.AVF_PERGUNTA for each row
declare
nr_seq_tipo_nota_item_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	avf_tipo_nota_item
	where	nr_seq_tipo_nota = :new.nr_seq_tipo_nota;

begin

if	(:new.nr_seq_tipo_nota <> :old.nr_seq_tipo_nota) then
	begin

	delete from avf_pergunta_tipo
	where	nr_seq_pergunta = :new.nr_sequencia;

	open C01;
	loop
	fetch C01 into
		nr_seq_tipo_nota_item_w;
	exit when C01%notfound;
		begin

		insert into avf_pergunta_tipo (	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pergunta,
				nr_seq_tipo_nota_item)
		values	(	avf_pergunta_tipo_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario_nrec,
				:new.nr_sequencia,
				nr_seq_tipo_nota_item_w);
		end;
	end loop;
	close C01;

	end;
end if;

end;
/


ALTER TABLE TASY.AVF_PERGUNTA ADD (
  CONSTRAINT AVFPERG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AVF_PERGUNTA ADD (
  CONSTRAINT AVFPERG_AVFTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_AVAL) 
 REFERENCES TASY.AVF_TIPO_AVALIACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AVFPERG_AVFGRPE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PERGUNTA) 
 REFERENCES TASY.AVF_GRUPO_PERGUNTA (NR_SEQUENCIA),
  CONSTRAINT AVFPERG_AVFTINO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_NOTA) 
 REFERENCES TASY.AVF_TIPO_NOTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.AVF_PERGUNTA TO NIVEL_1;

