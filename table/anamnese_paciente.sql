ALTER TABLE TASY.ANAMNESE_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANAMNESE_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANAMNESE_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ANANMESE                DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_MEDICO                  VARCHAR2(10 BYTE),
  DS_ANAMNESE_OLD            VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DS_ANAMNESE                LONG,
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  QT_CARACTERES              NUMBER(15),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_RESTRICAO_VISUALIZACAO  VARCHAR2(1 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_TIPO_EVOLUCAO           VARCHAR2(3 BYTE),
  CD_ESPECIALIDADE_MEDICO    NUMBER(5),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  NR_ATEND_ALTA              NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NR_SEQ_AVALIACAO           NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          60M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANAPACI_ATCONSPEPA_FK_I ON TASY.ANAMNESE_PACIENTE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_ATEPACI_FK_I ON TASY.ANAMNESE_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_ATEPACI_FK2_I ON TASY.ANAMNESE_PACIENTE
(NR_ATEND_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAPACI_ATEPACI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ANAPACI_ESPMEDI_FK2_I ON TASY.ANAMNESE_PACIENTE
(CD_ESPECIALIDADE_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAPACI_ESPMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ANAPACI_I1 ON TASY.ANAMNESE_PACIENTE
(DT_ANANMESE, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_MEDAVPA_FK_I ON TASY.ANAMNESE_PACIENTE
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_PERFIL_FK_I ON TASY.ANAMNESE_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAPACI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANAPACI_PESFISI_FK_I ON TASY.ANAMNESE_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_PESFISI_FK2_I ON TASY.ANAMNESE_PACIENTE
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ANAPACI_PK ON TASY.ANAMNESE_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_PROFISS_FK_I ON TASY.ANAMNESE_PACIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAPACI_TASASDI_FK_I ON TASY.ANAMNESE_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAPACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANAPACI_TASASDI_FK2_I ON TASY.ANAMNESE_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAPACI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.anamnese_paciente_pend_atual
after insert or update ON TASY.ANAMNESE_PACIENTE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_anamnese_w	varchar2(10);
nm_usuario_w        varchar2(15);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_lib_anamnese)
into	ie_lib_anamnese_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (:new.dt_liberacao is null) then

     if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
	   (:new.DT_LIBERACAO_AUX is not null)then

		select 	obter_usuario_pf(:new.cd_medico)
		into	nm_usuario_w
		from	dual;

		Update    pep_item_pendente
		set		nm_usuario = nm_usuario_w
		where	nr_seq_anamnese = :new.nr_sequencia;

	end if;

end if;

if	(nvl(ie_lib_anamnese_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'AP';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XAP';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.Anamnese_Paciente_Delete
before delete ON TASY.ANAMNESE_PACIENTE for each row
declare

qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if      (:old.dt_liberacao is not null) then
	--	Nao e possivel a exclusao de uma anamnese ja liberada.
        Wheb_mensagem_pck.exibir_mensagem_abort(263475);
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.anamnese_paciente_atual
before insert or update ON TASY.ANAMNESE_PACIENTE for each row
declare

qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_ANANMESE,sysdate+10) <> :new.DT_ANANMESE) and
	(:new.DT_ANANMESE is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ANANMESE,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

begin
	if	(:new.nr_atendimento is not null) and
		(:new.cd_pessoa_fisica is null) then
		:new.cd_pessoa_fisica := obter_pessoa_Atendimento(:new.nr_atendimento,'C');
	end if;

exception
	when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.ANAMNESE_PACIENTE ADD (
  CONSTRAINT ANAPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ANAMNESE_PACIENTE ADD (
  CONSTRAINT ANAPACI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ANAPACI_PESFISI_FK2 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ANAPACI_MEDAVPA_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.MED_AVALIACAO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT ANAPACI_ATEPACI_FK2 
 FOREIGN KEY (NR_ATEND_ALTA) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ANAPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ANAPACI_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ANAPACI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ANAPACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ANAPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ANAPACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ANAPACI_ESPMEDI_FK2 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICO) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.ANAMNESE_PACIENTE TO NIVEL_1;

