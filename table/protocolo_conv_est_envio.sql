ALTER TABLE TASY.PROTOCOLO_CONV_EST_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_CONV_EST_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_CONV_EST_ENVIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  DS_JUSTIFICATIVA     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRTCNVES_PK ON TASY.PROTOCOLO_CONV_EST_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRTCNVES_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRTCNVES_PROCONV_FK_I ON TASY.PROTOCOLO_CONV_EST_ENVIO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRTCNVES_PROCONV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROTOCOLO_CONV_EST_ENVIO ADD (
  CONSTRAINT PRTCNVES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_CONV_EST_ENVIO ADD (
  CONSTRAINT PRTCNVES_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROTOCOLO_CONV_EST_ENVIO TO NIVEL_1;

