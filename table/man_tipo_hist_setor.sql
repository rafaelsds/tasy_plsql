ALTER TABLE TASY.MAN_TIPO_HIST_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_TIPO_HIST_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_TIPO_HIST_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_TIPO_HIST      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_VISUALIZA          VARCHAR2(1 BYTE),
  CD_CARGO              NUMBER(10),
  NM_USUARIO_REGRA      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANTIPHS_CARGO_FK_I ON TASY.MAN_TIPO_HIST_SETOR
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTIPHS_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTIPHS_MANTIHI_FK_I ON TASY.MAN_TIPO_HIST_SETOR
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANTIPHS_PK ON TASY.MAN_TIPO_HIST_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANTIPHS_SETATEN_FK_I ON TASY.MAN_TIPO_HIST_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTIPHS_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTIPHS_USUARIO_FK_I ON TASY.MAN_TIPO_HIST_SETOR
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_TIPO_HIST_SETOR ADD (
  CONSTRAINT MANTIPHS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_TIPO_HIST_SETOR ADD (
  CONSTRAINT MANTIPHS_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT MANTIPHS_MANTIHI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.MAN_TIPO_HIST (NR_SEQUENCIA),
  CONSTRAINT MANTIPHS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MANTIPHS_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO));

GRANT SELECT ON TASY.MAN_TIPO_HIST_SETOR TO NIVEL_1;

