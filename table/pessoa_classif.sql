ALTER TABLE TASY.PESSOA_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_CLASSIF
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF               NUMBER(10)       NOT NULL,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  DT_INICIO_VIGENCIA           DATE             NOT NULL,
  DT_FINAL_VIGENCIA            DATE,
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  CD_PESSOA_REF                VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO           NUMBER(4),
  IE_CLINICA                   NUMBER(5),
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIF_INATIVACAO         VARCHAR2(255 BYTE),
  CD_CGC                       VARCHAR2(14 BYTE),
  CD_ESPECIALIDADE_REFERENCIA  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESCLAS_CLASPES_FK_I ON TASY.PESSOA_CLASSIF
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESCLAS_ESPMEDI_FK_I ON TASY.PESSOA_CLASSIF
(CD_ESPECIALIDADE_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESCLAS_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESCLAS_ESTABEL_FK_I ON TASY.PESSOA_CLASSIF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESCLAS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESCLAS_I1 ON TASY.PESSOA_CLASSIF
(CD_PESSOA_FISICA, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESCLAS_PESFISI_FK_I ON TASY.PESSOA_CLASSIF
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESCLAS_PESFISI_FK2_I ON TASY.PESSOA_CLASSIF
(CD_PESSOA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESCLAS_PESJURI_FK_I ON TASY.PESSOA_CLASSIF
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESCLAS_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESCLAS_PK ON TASY.PESSOA_CLASSIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_CLASSIF_tp  after update ON TASY.PESSOA_CLASSIF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PESSOA_CLASSIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FINAL_VIGENCIA,1,4000),substr(:new.DT_FINAL_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'PESSOA_CLASSIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_REF,1,4000),substr(:new.CD_PESSOA_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_REF',ie_log_w,ds_w,'PESSOA_CLASSIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PESSOA_CLASSIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'PESSOA_CLASSIF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ish_pessoa_classif_afterpost
after insert or update or delete ON TASY.PESSOA_CLASSIF for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
nr_prontuario_w			pessoa_fisica.nr_prontuario%type;
ie_funcionario_w		pessoa_fisica.ie_funcionario%type;
nr_seq_conselho_w		pessoa_fisica.nr_seq_conselho%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_cpf_w 			pessoa_fisica.nr_cpf%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	cd_pessoa_fisica_w		:=	nvl(:new.cd_pessoa_fisica, :old.cd_pessoa_fisica);
	reg_integracao_w.ie_operacao	:=	'A';

	if	(cd_pessoa_fisica_w is not null) then
		select	pf.nr_prontuario,
			pf.ie_funcionario,
			pf.nr_seq_conselho,
			nr_cpf
		into	nr_prontuario_w,
			ie_funcionario_w,
			nr_seq_conselho_w,
			nr_cpf_w
		from	pessoa_fisica pf
		where	pf.cd_pessoa_fisica = cd_pessoa_fisica_w;

		reg_integracao_w.nr_prontuario		:=	nr_prontuario_w;
		reg_integracao_w.cd_pessoa_fisica	:=	cd_pessoa_fisica_w;
		reg_integracao_w.ie_funcionario		:=	nvl(ie_funcionario_w,'N');
		reg_integracao_w.nr_seq_conselho	:=	nr_seq_conselho_w;
		reg_integracao_w.nr_seq_classif_pessoa	:=	nvl(:new.nr_seq_classif, :old.nr_seq_classif);

		select	decode(count(*), 0, 'N', 'S')
		into	reg_integracao_w.ie_pessoa_atend
		from	pessoa_fisica_aux
		where	cd_pessoa_fisica = cd_pessoa_fisica_w
		and	nr_primeiro_atend is not null;

		if 	(nr_cpf_w is not null) then
			reg_integracao_w.ie_possui_cpf := 'S';
		end if;

		if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
			gerar_int_padrao.gravar_integracao('12', cd_pessoa_fisica_w, nvl(:new.nm_usuario, obter_usuario_ativo), reg_integracao_w);

			if	(reg_integracao_w.nr_prontuario is not null) then
				begin
				if	(deleting) then
					reg_integracao_w.ie_operacao	:=	'E';
				end if;

				gerar_int_padrao.gravar_integracao('370', nvl(:new.nr_sequencia, :old.nr_sequencia) || '|' || cd_pessoa_fisica_w, nvl(:new.nm_usuario, obter_usuario_ativo), reg_integracao_w);
				reg_integracao_w.ie_operacao	:=	'A';
				end;
			end if;
		end if;

		if	(ie_funcionario_w = 'S') then
			gerar_int_padrao.gravar_integracao('304',  cd_pessoa_fisica_w, nvl(:new.nm_usuario, obter_usuario_ativo), reg_integracao_w);
		end if;
	end if;
end if;

end ish_pessoa_classif_afterpost;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_CLASSIF_ATUAL
before insert or update or delete ON TASY.PESSOA_CLASSIF for each row
declare

nm_pessoa_fisica_w	varchar2(60);
cd_pessoa_fisica_w	varchar2(10);
ds_mensagem_w		varchar2(4000);
ds_classif_ant_w		varchar2(80);
ds_classif_atual_w		varchar2(80);
nm_usuario_w		varchar2(15) := Wheb_usuario_pck.get_nm_usuario;

begin

if	(((updating) or (deleting)) and (:new.cd_pessoa_fisica is not null)) then
	begin
	select	nm_pessoa_fisica,
		cd_pessoa_fisica
	into	nm_pessoa_fisica_w,
		cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = :old.cd_pessoa_fisica;

	select	ds_classificacao
	into	ds_classif_ant_w
	from	classif_pessoa
	where	nr_sequencia = :old.nr_seq_classif;
	end;
end if;

if	(((inserting) or (updating)) and (:new.cd_pessoa_fisica is not null)) then
	begin
	select	nm_pessoa_fisica,
		cd_pessoa_fisica
	into	nm_pessoa_fisica_w,
		cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	select	ds_classificacao
	into	ds_classif_atual_w
	from	classif_pessoa
	where	nr_sequencia = :new.nr_seq_classif;
	end;
end if;

if	(inserting) then
	ds_mensagem_w := Wheb_mensagem_pck.get_texto(298872) || chr(13) || chr(10) ||
			Wheb_mensagem_pck.get_texto(298873) || ' ' || ds_classif_atual_w;
elsif	(updating) then
	ds_mensagem_w := Wheb_mensagem_pck.get_texto(298875) || chr(13) || chr(10) ||
			Wheb_mensagem_pck.get_texto(298876) || ' ' || ds_classif_ant_w || chr(13) || chr(10) ||
			Wheb_mensagem_pck.get_texto(298879) || ' ' || ds_classif_atual_w;
elsif	(deleting) then
	ds_mensagem_w := Wheb_mensagem_pck.get_texto(298880) || chr(13) || chr(10) ||
			Wheb_mensagem_pck.get_texto(298876) || ' ' || ds_classif_ant_w;
	if (nm_usuario_w is null) then
		nm_usuario_w := :old.nm_usuario;
	end if;
end if;

if (:new.cd_pessoa_fisica is not null) then
	insert into pessoa_fisica_historico(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_pessoa_fisica,
				cd_pessoa_fisica,
				dt_liberacao,
				ds_historico)
			values(	pessoa_fisica_historico_seq.nextval,
				sysdate,
				nvl(nm_usuario_w,:new.nm_usuario),
				sysdate,
				nvl(nm_usuario_w,:new.nm_usuario),
				nm_pessoa_fisica_w,
				cd_pessoa_fisica_w,
				sysdate,
				ds_mensagem_w);
end if;
end;
/


ALTER TABLE TASY.PESSOA_CLASSIF ADD (
  CONSTRAINT PESCLAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_CLASSIF ADD (
  CONSTRAINT PESCLAS_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_REFERENCIA) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PESCLAS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESCLAS_CLASPES_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PESCLAS_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_REF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESCLAS_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESCLAS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PESSOA_CLASSIF TO NIVEL_1;

