ALTER TABLE TASY.MAN_ORDEM_SERV_FIM_PREV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_FIM_PREV CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_FIM_PREV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM_SERV    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MOTIVO            VARCHAR2(255 BYTE),
  DT_FIM_PREVISTO      DATE                     NOT NULL,
  NR_SEQ_MOTIVO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSFP_MAMOREA_FK_I ON TASY.MAN_ORDEM_SERV_FIM_PREV
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSFP_MAMOREA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANOSFP_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_FIM_PREV
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSFP_MANORSE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANOSFP_PK ON TASY.MAN_ORDEM_SERV_FIM_PREV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSFP_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_ORDEM_SERV_FIM_PREV ADD (
  CONSTRAINT MANOSFP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_FIM_PREV ADD (
  CONSTRAINT MANOSFP_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT MANOSFP_MAMOREA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MAN_MOTIVO_REAPRAZAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_FIM_PREV TO NIVEL_1;

