ALTER TABLE TASY.PRESCR_OCORRENCIA_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_OCORRENCIA_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_OCORRENCIA_LAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(14)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_OCORRENCIA    NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREOCLA_LABTIOC_FK_I ON TASY.PRESCR_OCORRENCIA_LAB
(NR_SEQ_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREOCLA_LABTIOC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREOCLA_PK ON TASY.PRESCR_OCORRENCIA_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREOCLA_PRESMED_FK_I ON TASY.PRESCR_OCORRENCIA_LAB
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRESCR_OCORRENCIA_LAB ADD (
  CONSTRAINT PREOCLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_OCORRENCIA_LAB ADD (
  CONSTRAINT PREOCLA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PREOCLA_LABTIOC_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA) 
 REFERENCES TASY.LAB_TIPO_OCORRENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_OCORRENCIA_LAB TO NIVEL_1;

