ALTER TABLE TASY.PRODUTO_PHILIPS_APLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRODUTO_PHILIPS_APLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRODUTO_PHILIPS_APLIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRODUTO       NUMBER(10),
  CD_APLICACAO_TASY    VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPHAP_APLTASY_FK_I ON TASY.PRODUTO_PHILIPS_APLIC
(CD_APLICACAO_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROPHAP_PK ON TASY.PRODUTO_PHILIPS_APLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPHAP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROPHAP_PRODPHI_FK_I ON TASY.PRODUTO_PHILIPS_APLIC
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPHAP_PRODPHI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRODUTO_PHILIPS_APLIC ADD (
  CONSTRAINT PROPHAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRODUTO_PHILIPS_APLIC ADD (
  CONSTRAINT PROPHAP_APLTASY_FK 
 FOREIGN KEY (CD_APLICACAO_TASY) 
 REFERENCES TASY.APLICACAO_TASY (CD_APLICACAO_TASY),
  CONSTRAINT PROPHAP_PRODPHI_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_PHILIPS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRODUTO_PHILIPS_APLIC TO NIVEL_1;

