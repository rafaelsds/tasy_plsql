ALTER TABLE TASY.PLS_MOTIVO_GLOSA_NEGADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOTIVO_GLOSA_NEGADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOTIVO_GLOSA_NEGADA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MOTIVO            VARCHAR2(20 BYTE)        NOT NULL,
  DS_MOTIVO            VARCHAR2(255 BYTE)       NOT NULL,
  IE_PROCEDIMENTO      VARCHAR2(1 BYTE)         NOT NULL,
  IE_MATERIAL          VARCHAR2(1 BYTE)         NOT NULL,
  IE_CONTA             VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMGNE_ESTABEL_FK_I ON TASY.PLS_MOTIVO_GLOSA_NEGADA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGNE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSMGNE_PK ON TASY.PLS_MOTIVO_GLOSA_NEGADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGNE_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_MOTIVO_GLOSA_NEGADA ADD (
  CONSTRAINT PLSMGNE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MOTIVO_GLOSA_NEGADA ADD (
  CONSTRAINT PLSMGNE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MOTIVO_GLOSA_NEGADA TO NIVEL_1;

