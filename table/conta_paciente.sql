ALTER TABLE TASY.CONTA_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_PACIENTE
(
  NR_ATENDIMENTO           NUMBER(10),
  DT_ACERTO_CONTA          DATE                 NOT NULL,
  IE_STATUS_ACERTO         NUMBER(1)            NOT NULL,
  DT_PERIODO_INICIAL       DATE                 NOT NULL,
  DT_PERIODO_FINAL         DATE                 NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_CONVENIO_PARAMETRO    NUMBER(5)            NOT NULL,
  NR_PROTOCOLO             VARCHAR2(40 BYTE),
  DT_MESANO_REFERENCIA     DATE,
  DT_MESANO_CONTABIL       DATE,
  CD_CONVENIO_CALCULO      NUMBER(5),
  CD_CATEGORIA_CALCULO     VARCHAR2(10 BYTE),
  NR_INTERNO_CONTA         NUMBER(10)           NOT NULL,
  NR_SEQ_PROTOCOLO         NUMBER(10),
  CD_CATEGORIA_PARAMETRO   VARCHAR2(10 BYTE),
  DS_INCONSISTENCIA        VARCHAR2(255 BYTE),
  DT_RECALCULO             DATE,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  IE_CANCELAMENTO          VARCHAR2(1 BYTE),
  NR_LOTE_CONTABIL         NUMBER(10),
  NR_SEQ_APRESENT          NUMBER(10),
  IE_COMPLEXIDADE          VARCHAR2(1 BYTE),
  IE_TIPO_NASCIMENTO       VARCHAR2(3 BYTE),
  IE_TIPO_GUIA             VARCHAR2(2 BYTE),
  CD_AUTORIZACAO           VARCHAR2(20 BYTE),
  VL_CONTA                 NUMBER(15,2),
  VL_DESCONTO              NUMBER(15,2),
  DT_CANCELAMENTO          DATE,
  CD_PROC_PRINC            VARCHAR2(20 BYTE),
  NR_SEQ_CONTA_ORIGEM      NUMBER(10),
  NR_CONTA_CONVENIO        NUMBER(15),
  DT_CONTA_DEFINITIVA      DATE,
  DT_CONTA_PROTOCOLO       DATE,
  NR_SEQ_CONTA_PROT        NUMBER(15),
  NR_SEQ_PQ_PROTOCOLO      NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  IE_TIPO_ATEND_TISS       VARCHAR2(2 BYTE),
  NR_SEQ_SAIDA_CONSULTA    NUMBER(10),
  NR_SEQ_SAIDA_INT         NUMBER(10),
  NR_SEQ_SAIDA_SPSADT      NUMBER(10),
  DT_GERACAO_TISS          DATE,
  NR_LOTE_REPASSE          NUMBER(15),
  IE_REAPRESENTACAO_SUS    VARCHAR2(1 BYTE),
  NR_SEQ_RET_GLOSA         NUMBER(10),
  DT_PREV_PROTOCOLO        DATE,
  NR_CONTA_PACOTE_EXCED    NUMBER(10),
  DT_ALTA_TISS             DATE,
  DT_ENTRADA_TISS          DATE,
  IE_TIPO_FATUR_TISS       VARCHAR2(1 BYTE),
  IE_TIPO_CONSULTA_TISS    NUMBER(3),
  CD_ESPECIALIDADE_CONTA   NUMBER(5),
  CD_PLANO_RETORNO_CONV    VARCHAR2(10 BYTE),
  IE_TIPO_ATEND_CONTA      NUMBER(3),
  QT_DIAS_CONTA            NUMBER(10),
  CD_RESPONSAVEL           VARCHAR2(10 BYTE),
  NR_SEQ_ESTAGIO_CONTA     NUMBER(10),
  NM_USUARIO_ORIGINAL      VARCHAR2(15 BYTE),
  NR_SEQ_AUDIT_HIST_GLOSA  NUMBER(10),
  VL_REPASSE_CONTA         NUMBER(15,2),
  NR_FECHAMENTO            NUMBER(10),
  NR_SEQ_TIPO_FATURA       NUMBER(10),
  NR_SEQ_APRESENTACAO      NUMBER(10),
  NR_SEQ_MOTIVO_CANCEL     NUMBER(10),
  DT_CONFERENCIA_SUS       DATE,
  CD_MEDICO_CONTA          VARCHAR2(10 BYTE),
  QT_DIAS_PERIODO          NUMBER(10),
  DT_GERACAO_RESUMO        DATE,
  VL_CONTA_RELAT           NUMBER(15,2),
  IE_FAEC                  VARCHAR2(15 BYTE),
  NR_SEQ_APRESENT_SUS      NUMBER(10),
  NR_SEQ_STATUS_FAT        NUMBER(10),
  NR_SEQ_STATUS_MOB        NUMBER(10),
  NR_SEQ_REGRA_FLUXO       NUMBER(10),
  DT_ATUAL_CONTA_CONV      DATE,
  NR_CONTA_ORIG_DESDOB     NUMBER(10),
  PR_COSEGURO_HOSP         NUMBER(15,2),
  PR_COSEGURO_HONOR        NUMBER(15,2),
  VL_DEDUZIDO              NUMBER(15,2),
  VL_BASE_CONTA            NUMBER(15,2),
  DT_DEFINICAO_CONTA       DATE,
  NR_SEQ_ORDEM             NUMBER(10),
  IE_COMPLEX_AIH_ORIG      VARCHAR2(2 BYTE),
  VL_COSEGURO_HONOR        NUMBER(15,2),
  NR_SEQ_TIPO_COBRANCA     NUMBER(10),
  VL_COSEGURO_HOSP         NUMBER(15,2),
  PR_COSEG_NIVEL_HOSP      NUMBER(15,2),
  VL_COSEG_NIVEL_HOSP      NUMBER(15,2),
  VL_MAXIMO_COSEGURO       NUMBER(15,2),
  IE_CLAIM_TYPE            VARCHAR2(2 BYTE),
  NR_SEQ_CATEGORIA_IVA     NUMBER(10),
  DT_GERACAO_REL_CONV      DATE,
  NR_CODIGO_CONTROLE       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          265M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONPACI_ATEPACI_FK_I ON TASY.CONTA_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          38M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_CATCONV_FK_I ON TASY.CONTA_PACIENTE
(CD_CONVENIO_CALCULO, CD_CATEGORIA_CALCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          34M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_CATCONV_FK_I2 ON TASY.CONTA_PACIENTE
(CD_CONVENIO_PARAMETRO, CD_CATEGORIA_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          35M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_CATIVA_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_CATEGORIA_IVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_CFREGRA_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_REGRA_FLUXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_CFREGRA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_CFSTAFA_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_STATUS_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_CFSTAFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_CFSTMOB_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_STATUS_MOB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_CFSTMOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_CONPACI_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_CONTA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_CONPLAN_FK_I ON TASY.CONTA_PACIENTE
(CD_CONVENIO_PARAMETRO, CD_PLANO_RETORNO_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          30M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_CONRETO_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_RET_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_CONRETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_CONVENI_FK_I ON TASY.CONTA_PACIENTE
(CD_CONVENIO_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          30M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_ESPMEDI_FK_I ON TASY.CONTA_PACIENTE
(CD_ESPECIALIDADE_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_ESTABEL_FK_I ON TASY.CONTA_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_ESTCONT_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_ESTAGIO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_ESTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_FATPFAT_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_TIPO_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_FATPFAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_I1 ON TASY.CONTA_PACIENTE
(DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I2 ON TASY.CONTA_PACIENTE
(DT_ATUALIZACAO, NM_USUARIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          53M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I3 ON TASY.CONTA_PACIENTE
(CD_CONVENIO_PARAMETRO, DT_PERIODO_FINAL, NR_ATENDIMENTO, NR_PROTOCOLO, DT_PERIODO_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          121M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I4 ON TASY.CONTA_PACIENTE
(DT_MESANO_REFERENCIA, NR_INTERNO_CONTA, IE_STATUS_ACERTO, NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          96M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I5 ON TASY.CONTA_PACIENTE
(CD_CONVENIO_PARAMETRO, CD_ESTABELECIMENTO, IE_STATUS_ACERTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          45M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I6 ON TASY.CONTA_PACIENTE
(NR_CONTA_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_I6
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_I7 ON TASY.CONTA_PACIENTE
(IE_STATUS_ACERTO, IE_CANCELAMENTO, NR_ATENDIMENTO, DT_PERIODO_FINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I8 ON TASY.CONTA_PACIENTE
(NR_ATENDIMENTO, IE_STATUS_ACERTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_I9 ON TASY.CONTA_PACIENTE
(NR_INTERNO_CONTA, CD_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_LOTAUHI_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_AUDIT_HIST_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_LOTCONT_FK_I ON TASY.CONTA_PACIENTE
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_LOTCONT_FK2_I ON TASY.CONTA_PACIENTE
(NR_LOTE_REPASSE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_PESFISI_FK_I ON TASY.CONTA_PACIENTE
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_PESFISI_FK2_I ON TASY.CONTA_PACIENTE
(CD_MEDICO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONPACI_PK ON TASY.CONTA_PACIENTE
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_PQPROTO_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_PQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPACI_PQPROTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPACI_PROCONV_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          38M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_PROCONV_I ON TASY.CONTA_PACIENTE
(NR_PROTOCOLO, CD_CONVENIO_PARAMETRO, NR_ATENDIMENTO, DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          112M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONPACI_SEQ_ORDEM_UK ON TASY.CONTA_PACIENTE
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_TISSMSI_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_SAIDA_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_TISSTSC_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_SAIDA_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_TISSTSS_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_SAIDA_SPSADT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPACI_TPCOCON_FK_I ON TASY.CONTA_PACIENTE
(NR_SEQ_TIPO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.conta_paciente_afterinsert
after insert ON TASY.CONTA_PACIENTE for each row
declare

nr_sequencia_w		number(10,0);
nr_seq_etapa_padrao_w	number(10,0);
cd_convenio_w		number(6,0);
cd_estabelecimento_w	number(10,0);
ie_tipo_atendimento_w	number(3,0);
nr_seq_classificacao_w	number(10,0);
cd_categoria_w		varchar2(10);
ie_clinica_w		number(5,0);
cd_setor_atendimento_w	number(10,0);
nr_seq_motivo_dev_w	number(10,0);
cd_setor_atend_etapa_w	number(10);
ie_pessoa_etapa_w	varchar2(3);
ie_etapa_critica_w	varchar2(1);
nm_usuario_original_w	varchar2(15);
cd_pessoa_fisica_w	varchar2(10);
cd_perfil_w		perfil.cd_perfil%type := obter_perfil_ativo;
qt_regra_restringe_w	number(10,0);
nr_seq_w		fatur_etapa_conta.nr_sequencia%type;
qt_regra_w		Number(1);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_etapa,
		nr_seq_motivo_dev,
		cd_setor_atend_etapa,
		nvl(ie_pessoa_etapa,'L'),
		nvl(ie_etapa_critica,'N')
	from	fatur_etapa_conta
	where	ie_situacao = 'A'
	and 	nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0)
	and 	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
	and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)
	and	nvl(nr_seq_classificacao, nvl(nr_seq_classificacao_w,0)) = nvl(nr_seq_classificacao_w,0)
	and	nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0')
	and	nvl(ie_clinica, nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,0)
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and 	((nvl(IE_CONTA_AJUSTE_CANCEL,'N') = 'N') or ((nvl(IE_CONTA_AJUSTE_CANCEL,'N') = 'X') and (nvl(:new.nr_seq_conta_origem,0) = 0)))
	and	((nvl(ie_conta_desdobramento,'S') = 'S') or
		((nvl(ie_conta_desdobramento,'S') = 'N') and (:new.nr_conta_orig_desdob is null)))
	and	nvl(cd_perfil,nvl(cd_perfil_w,0))	= nvl(cd_perfil_w,0)
	order by nvl(cd_convenio,0),
		nvl(cd_estabelecimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(nr_seq_classificacao,0),
		nvl(cd_categoria,'0'),
		nvl(ie_clinica,0),
		nvl(cd_setor_atendimento,0),
		nvl(cd_perfil,0);

begin

cd_convenio_w		:= :new.cd_convenio_parametro;
cd_estabelecimento_w	:= :new.cd_estabelecimento;
cd_categoria_w		:= :new.cd_categoria_parametro;
cd_setor_atendimento_w	:= nvl(obter_setor_atendimento(:new.nr_atendimento),0);


nr_seq_etapa_padrao_w:= null;

select	nvl(max(ie_tipo_atendimento),0),
	nvl(max(nr_seq_classificacao),0),
	nvl(max(ie_clinica),0)
into	ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

open C01;
loop
fetch C01 into
	nr_seq_w,
	nr_seq_etapa_padrao_w,
	nr_seq_motivo_dev_w,
	cd_setor_atend_etapa_w,
	ie_pessoa_etapa_w,
	ie_etapa_critica_w;
exit when C01%notfound;
	begin
	nr_seq_etapa_padrao_w	:= nr_seq_etapa_padrao_w;
	nr_seq_motivo_dev_w	:= nr_seq_motivo_dev_w;
	cd_setor_atend_etapa_w	:= cd_setor_atend_etapa_w;
	ie_pessoa_etapa_w	:= ie_pessoa_etapa_w;
	ie_etapa_critica_w	:= ie_etapa_critica_w;
	end;
end loop;
close C01;

select 	count(*)
into	qt_regra_restringe_w
from 	fatur_etapa_conta_conv
where 	nr_seq_regra = nr_seq_w
and 	cd_convenio = cd_convenio_w;

if	(qt_regra_restringe_w > 0) then
	nr_seq_etapa_padrao_w:= null;
end if;


if	(nr_seq_etapa_padrao_w is not null) then

	select	substr(Obter_Pessoa_Fisica_Usuario(decode(ie_pessoa_etapa_w, 'L', wheb_usuario_pck.get_nm_usuario , :new.nm_usuario_original),'C'),1,10)
	into	cd_pessoa_fisica_w
	from	dual;

	select	conta_paciente_etapa_seq.NextVal
	into	nr_sequencia_w
	from 	dual;

	insert into conta_paciente_etapa (
		nr_sequencia,
		nr_interno_conta,
		dt_atualizacao,
		nm_usuario,
		dt_etapa,
		nr_seq_etapa,
		cd_setor_atendimento,
		cd_pessoa_fisica,
		nr_seq_motivo_dev,
		ds_observacao,
		nr_lote_barras,
		ie_etapa_critica)
	values	(nr_sequencia_w,
		:new.nr_interno_conta,
		sysdate,
		:new.nm_usuario,
		sysdate,
		nr_seq_etapa_padrao_w,
		cd_setor_atend_etapa_w,
		cd_pessoa_fisica_w,
		nr_seq_motivo_dev_w,
		substr(wheb_mensagem_pck.get_texto(304654),1,1999), --Etapa padr�o gerada na cria��o da conta
		null,
		ie_etapa_critica_w);

end if;

select	count(1)
into	qt_regra_w
from	regra_prontuario_gestao
where	cd_estabelecimento					= cd_estabelecimento_w
and	nvl(IE_FORMA_GERACAO,'A') = 'O';

if	(qt_regra_w > 0) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	gerar_regra_prontuario_gestao(ie_tipo_atendimento_w,nvl(cd_estabelecimento_w,0),:new.nr_atendimento,cd_pessoa_fisica_w,wheb_usuario_pck.get_nm_usuario,
					null,
					null,
					null,
					ie_clinica_w,
					cd_setor_atend_etapa_w,
					null,
					null,
					nvl(:new.nr_interno_conta,:old.nr_interno_conta),
					:new.dt_periodo_inicial,
					:new.dt_periodo_final);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_AfterUpdate
AFTER UPDATE ON TASY.CONTA_PACIENTE FOR EACH ROW
DECLARE


nr_sequencia_w			Number(10,0);
ie_historico_w			Varchar2(01);
dt_alta_w			Date;
dt_referencia_w			Date;
qt_historico_w			Number(10,0);
cd_funcao_w			Number(5);
ie_atualiza_dt_contab_prot_w	varchar2(1)	:= 'N';
dt_conta_protocolo_w		date;

ds_module_log_w conta_paciente_hist.ds_module%type;
ds_call_stack_w conta_paciente_hist.ds_call_stack%type;

BEGIN
/*Matheus  - OS 184506 Atribui para variavel para evitar erro do ORACLE */
dt_conta_protocolo_w	:= :new.dt_conta_protocolo;

select 	nvl(max(ie_historico_conta),'N')
into	ie_historico_w
from	parametro_faturamento
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(ie_historico_w	= 'S') then
	select	obter_funcao_ativa
	into	cd_funcao_w
	from	dual;
end if;

if	(:new.ie_status_acerto = 1) and
	(nvl(:new.nr_seq_protocolo,0) > 0) then
	-- A conta esta com status provisorio, nao pode ser inserida em um protocolo
	Wheb_mensagem_pck.exibir_mensagem_abort(184237);
end if;

select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
into	ds_module_log_w
from	v$session
where	audsid = (select userenv('sessionid') from dual);

if	(:new.ie_status_acerto = 1) and
	(:old.ie_status_acerto = 2) and
	(ie_historico_w = 'S') then
	select	dt_alta
	into	dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;
	select	conta_paciente_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into conta_paciente_hist(
		nr_sequencia, dt_atualizacao, nm_usuario,
		vl_conta, nr_seq_protocolo, nr_interno_conta,
		nr_nivel_anterior, nr_nivel_atual, dt_referencia,
		nr_atendimento, cd_convenio, dt_conta_protocolo,
		cd_funcao, ds_module, ds_call_stack)
	values	(
		nr_sequencia_w, sysdate, :new.nm_usuario,
		nvl(:new.vl_conta, 0), :new.nr_seq_protocolo, :new.nr_interno_conta,
		6, decode(dt_alta_w,null,2,4) , trunc(sysdate,'dd'),
		:new.nr_atendimento, :new.cd_convenio_parametro, dt_conta_protocolo_w,
		cd_funcao_w, ds_module_log_w, ds_call_stack_w);
elsif	(:new.ie_status_acerto = 2) and
	(:old.ie_status_acerto = 1) and
	(ie_historico_w = 'S') then
	/*select	count(*)
	into	qt_historico_w
	from	conta_paciente_hist
	where	nr_interno_conta	= :new.nr_interno_conta;
	if	(qt_historico_w > 0) then*/
		select	conta_paciente_hist_seq.nextval
		into	nr_sequencia_w
		from	dual;

		ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

		insert into conta_paciente_hist(
			nr_sequencia, dt_atualizacao, nm_usuario,
			vl_conta, nr_seq_protocolo, nr_interno_conta,
			nr_nivel_anterior, nr_nivel_atual, dt_referencia,
			nr_atendimento, cd_convenio, dt_conta_protocolo,
			cd_funcao, ds_module, ds_call_stack)
		values	(
			nr_sequencia_w, sysdate, :new.nm_usuario,
			nvl(:new.vl_conta, 0), :new.nr_seq_protocolo, :new.nr_interno_conta,
			4, 6, trunc(sysdate,'dd'),
			:new.nr_atendimento, :new.cd_convenio_parametro, dt_conta_protocolo_w,
			cd_funcao_w, ds_module_log_w, ds_call_stack_w);
/*	end if;*/
elsif	(:new.ie_status_acerto = 2) and
	(:old.ie_status_acerto = 2) and
	(nvl(:new.nr_seq_protocolo,0) <> nvl(:old.nr_seq_protocolo,0)) and
	(ie_historico_w = 'S') then
	select	conta_paciente_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into conta_paciente_hist(
		nr_sequencia, dt_atualizacao, nm_usuario,
		vl_conta, nr_seq_protocolo, nr_interno_conta,
		nr_nivel_anterior, nr_nivel_atual, dt_referencia,
		nr_atendimento, cd_convenio, dt_conta_protocolo,
		cd_funcao, ds_module, ds_call_stack)
	values	(
		nr_sequencia_w, sysdate, :new.nm_usuario,
		nvl(:new.vl_conta, 0), :new.nr_seq_protocolo, :new.nr_interno_conta,
		decode(:old.nr_seq_protocolo,null,6,8),
		decode(:new.nr_seq_protocolo,null,6,8) , trunc(sysdate,'dd'),
		:new.nr_atendimento, :new.cd_convenio_parametro, dt_conta_protocolo_w,
		cd_funcao_w, ds_module_log_w, ds_call_stack_w);
end if;


if	(:new.cd_convenio_parametro <> :old.cd_convenio_parametro) and
	(nvl(:old.cd_convenio_parametro,0) <> 0) and
	(ie_historico_w = 'S') then

	select	conta_paciente_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into conta_paciente_hist(
		nr_sequencia, dt_atualizacao, nm_usuario,
		vl_conta, nr_seq_protocolo, nr_interno_conta,
		nr_nivel_anterior, nr_nivel_atual, dt_referencia,
		nr_atendimento, cd_convenio, dt_conta_protocolo,
		cd_funcao, cd_convenio_orig, ds_module, ds_call_stack)
	values	(
		nr_sequencia_w, sysdate, :new.nm_usuario,
		nvl(:new.vl_conta, 0), :new.nr_seq_protocolo, :new.nr_interno_conta,
		0, 0 , trunc(sysdate,'dd'),
		:new.nr_atendimento, :new.cd_convenio_parametro, dt_conta_protocolo_w,
		cd_funcao_w, :old.cd_convenio_parametro, ds_module_log_w, ds_call_stack_w);
end if;

if	(:new.ie_status_acerto = :old.ie_status_acerto) and
	(:new.dt_mesano_referencia <> :old.dt_mesano_referencia) then
	select	max(dt_referencia)
	into	dt_referencia_w
	from	eis_conta_paciente
	where	nr_interno_conta	= :new.nr_interno_conta;
	if	(dt_referencia_w	<> trunc(:new.dt_mesano_referencia,'month')) then
		update eis_conta_paciente
		set	dt_referencia	=  trunc(:new.dt_mesano_referencia,'month')
		where	nr_interno_conta	= :new.nr_interno_conta;
	end if;
end if;

/*Francisco - 21/05/2009 - Tratar dt contabil titulo - OS 142718 */
if	(:new.dt_mesano_referencia <> :old.dt_mesano_referencia) then

	-- Feito dessa forma, colocando o comando aqui, devido a performance no update da conta_paciente_resumo que e executado na atualizacao do eis resultado.
	select 	nvl(max(ie_atualiza_dt_contab_prot),'N')
	into	ie_atualiza_dt_contab_prot_w
	from	parametro_faturamento
	where	cd_estabelecimento	= :new.cd_estabelecimento;

	if	(ie_atualiza_dt_contab_prot_w = 'S') then

		update	titulo_receber
		set	dt_contabil	= :new.dt_mesano_referencia
		where	nr_interno_conta	= :new.nr_interno_conta
		and	nvl(nr_lote_contabil,0)	= 0;

	end if;
end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_Insert
BEFORE INSERT ON TASY.CONTA_PACIENTE FOR EACH ROW
DECLARE


ie_atualiza_tipo_guia_w		boolean;
ie_tipo_guia_w			varchar2(2);
cd_responsavel_w		varchar2(10);
qt_atendimento_cancelado_w	number(10);
ie_tipo_guia_padrao_w		varchar2(2);
qt_prescricao_w			number(10);
dt_prev_protocolo_w		date;

pragma autonomous_transaction;

BEGIN
If	(:new.nr_atendimento is not null) then
	select	count(*)
	into	qt_atendimento_cancelado_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento
	and	dt_cancelamento is not null;

	If	(qt_atendimento_cancelado_w > 0) then
		--R.aise_application_error(-20011,'N�o � poss�vel criar conta para um atendimento cancelado!'||'#@#@');
		--wheb_mensagem_pck.exibir_mensagem_abort(263287);
		wheb_mensagem_pck.exibir_mensagem_abort(263287,'NR_ATEND=' || :new.nr_atendimento);

	End if;
End if;

ie_atualiza_tipo_guia_w	:= (Obter_Valor_Param_Usuario(67,251,obter_perfil_ativo,:new.nm_usuario,0) =  'S');
if	(:new.ie_tipo_guia is null) and
	(ie_atualiza_tipo_guia_w) then

	select	max(ie_tipo_guia)
	into	ie_tipo_guia_w
	from	atend_categoria_convenio
	where	nr_atendimento	= :new.nr_atendimento
	and	cd_convenio	= :new.cd_convenio_parametro
	and	cd_categoria	= :new.cd_categoria_parametro;

	:new.ie_tipo_guia := ie_tipo_guia_w;

end if;

ie_tipo_guia_padrao_w	:= nvl(Obter_Valor_Param_Usuario(67,580,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento),'0');

if	(ie_tipo_guia_padrao_w <> '0') then

	select	count(*)
	into	qt_prescricao_w
	from	prescr_medica
	where	nr_atendimento = :new.nr_atendimento;

	if	(qt_prescricao_w > 0) then

		:new.ie_tipo_guia := ie_tipo_guia_padrao_w;

	end if;

end if;

if	(:new.cd_convenio_calculo is null) and
	(:new.cd_categoria_calculo is null) then
	:new.cd_categoria_calculo 	:= :new.cd_categoria_parametro;
	:new.cd_convenio_calculo	:= :new.cd_convenio_parametro;
end if;

if	(:new.nm_usuario_original is null) then
	:new.nm_usuario_original:= nvl(wheb_usuario_pck.get_nm_usuario,'Tasy');
end if;

cd_responsavel_w := :new.cd_responsavel;

obter_responsavel_conta(:new.nr_interno_conta, :new.nr_atendimento, :new.cd_estabelecimento, :new.nm_usuario, cd_responsavel_w);
if	(cd_responsavel_w is not null) then
	:new.cd_responsavel := cd_responsavel_w;
end if;


:new.nr_seq_apresentacao := obter_seq_apresent_conta(	:new.nr_atendimento,
							:new.ie_tipo_atend_conta,
							:new.cd_convenio_parametro,
							:new.cd_estabelecimento);


:new.dt_prev_protocolo	:= OBTER_PREV_CONTA_PROT(	:new.nr_atendimento, :new.nr_interno_conta, :new.ie_tipo_atend_conta,
							:new.cd_estabelecimento, :new.cd_convenio_parametro, :new.dt_periodo_inicial);


commit;

END;
/


CREATE OR REPLACE TRIGGER TASY.CONTA_PACIENTE_tp  after update ON TASY.CONTA_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_INTERNO_CONTA);  ds_c_w:=null;  exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.conta_paciente_update
before update ON TASY.CONTA_PACIENTE 
for each row
declare

dt_alta_w			date;
dt_entrada_w		date;
dt_sistema_w		date            := sysdate;
dt_dia_vcto_w		number(2);
dt_ref_valida_w		date;
dt_virada_w		date;
ie_ok_w			varchar2(1) := 'S';
qt_titulo_w		number(05,0);
qt_nota_fiscal_w		number(05,0);
ie_tipo_convenio_w		number(05,0);
ie_alterar_final_aih_w	varchar2(10);
qt_aih_unif_w		number(05,0);
dt_final_w		date;
nr_seq_apresent_w		number(10);
nr_protocolo_w		varchar2(40);
dt_mesano_referencia_w	date;
ds_status_protocolo_w	varchar2(40);
qt_apac_unif_w		number(05,0);
ds_origem_w		varchar2(1800);
qt_existe_w		number(05,0);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_conta_conv_w	number(10);
nr_conta_atual_w		number(10) := 0;
nr_conta_inicial_w		number(10);
nr_final_conta_w		number(10);
ds_motivo_bloqueio_w	varchar2(255);
ds_obs_bloqueio_w	varchar2(255);
nm_usuario_bloqueio_w	varchar2(15);
dt_bloqueio_w		varchar2(25);

nr_seq_status_fat_w			cf_regra_estagio.nr_seq_status_fat%type;
nr_seq_status_mob_w			conta_paciente.nr_seq_status_mob%type;

begin

begin
select  dt_entrada,
        nvl(dt_alta,dt_sistema_w),
	cd_pessoa_fisica
into    dt_entrada_w,
        dt_alta_w,
	cd_pessoa_fisica_w
from    atendimento_paciente
where   nr_atendimento = :new.nr_atendimento;
exception
        when others then
        ie_ok_w := 'N';
end;

begin
select  max(ie_tipo_convenio)
into    ie_tipo_convenio_w
from    convenio
where   cd_convenio = :new.cd_convenio_parametro;
exception
        when others then
        ie_tipo_convenio_w := 0;
end;

if      (:new.ie_status_acerto = 1) and
        (:old.ie_status_acerto = 2) and
        (substr(obter_titulo_conta_protocolo(0, :new.nr_interno_conta),1,100) <> ' ') then
        begin
        -- A conta possui t�tulos, n�o pode ser aberta!
	Wheb_mensagem_pck.exibir_mensagem_abort(193540);
        end;
end if;

if      (:old.nr_seq_protocolo is not null) and
        (:new.nr_seq_protocolo is null) and
        (obter_status_protocolo(:old.nr_seq_protocolo) = 2) then
        begin
	-- N�o � poss�vel retirar a conta do protocolo Definitivo!
	Wheb_mensagem_pck.exibir_mensagem_abort(193541);
        end;
end if;

if      ((:old.nr_seq_protocolo is null) or 	
	(:old.nr_seq_protocolo <> :new.nr_seq_protocolo)) and	
	(:new.nr_seq_protocolo is not null) and        
	(obter_status_protocolo(:new.nr_seq_protocolo) = 2) then        
	begin	
	-- N�o � poss�vel inserir a conta em protocolo Definitivo!	
	Wheb_mensagem_pck.exibir_mensagem_abort(225218);        
	end;
end if;

if      (:old.nr_seq_protocolo is null) and
        (:new.nr_seq_protocolo is not null) then
        begin
        select  count(*)
        into    qt_nota_fiscal_w
        from    nota_fiscal
        where   nr_seq_protocolo        = :new.nr_seq_protocolo
        and     ie_situacao = '1';
        if      (qt_nota_fiscal_w > 0) then
                begin
		-- Existe nota fiscal associada ao protocolo. N�o pode associar mais contas
		Wheb_mensagem_pck.exibir_mensagem_abort(193543);
                end;
        end if;
        select  count(*)
        into    qt_titulo_w
        from    titulo_receber
        where   nr_seq_protocolo        = :new.nr_seq_protocolo;
        if      (qt_titulo_w > 0) then
                begin
		-- Existe titulo associado ao protocolo. N�o pode associar mais contas
		Wheb_mensagem_pck.exibir_mensagem_abort(193545);
                end;
        end if;

	select	count(*)
	into	qt_existe_w
	from	prot_conv_regra_ibge
	where	nr_seq_protocolo = :new.nr_seq_protocolo;

	if	(qt_existe_w > 0) then

		select	count(*)
		into	qt_existe_w
		from	prot_conv_regra_ibge b
		where	nr_seq_protocolo = :new.nr_seq_protocolo
		and	exists(
			select	1
			from	compl_pessoa_fisica a
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
			and	a.cd_municipio_ibge = b.cd_municipio_ibge);

		if	(qt_existe_w = 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(236176);
		end if;
	end if;
        end;
end if;

if	(:old.nr_seq_protocolo is null) and
	(:new.nr_seq_protocolo is not null) then

	if	(substr(obter_se_atend_bloqueado(:new.nr_atendimento),1,1) = 'S') and
		(substr(obter_se_tipo_bloqueado(:new.nr_atendimento, :new.cd_convenio_parametro, 'I'),1,1) = 'S') then
		ds_motivo_bloqueio_w	:= substr(obter_dados_bloqueio_pac(:new.nr_atendimento,1),1,255);
		ds_obs_bloqueio_w	:= substr(obter_dados_bloqueio_pac(:new.nr_atendimento,2),1,255);
		nm_usuario_bloqueio_w	:= substr(obter_dados_bloqueio_pac(:new.nr_atendimento,3),1,15);
		dt_bloqueio_w		:= substr(obter_dados_bloqueio_pac(:new.nr_atendimento,4),1,25);

		Wheb_mensagem_pck.exibir_mensagem_abort(247137,'DS_MOTIVO_BLOQUEIO=' || ds_motivo_bloqueio_w ||
			';DS_OBS_BLOQUEIO=' || nvl(ds_obs_bloqueio_w,' ') || ';NM_USUARIO_BLOQUEIO=' || nm_usuario_bloqueio_w ||
			';DT_BLOQUEIO=' || dt_bloqueio_w);
	end if;

end if;

begin
if	(ie_ok_w = 'S') and
	(:new.dt_periodo_inicial < dt_entrada_w) and
	(length(dt_entrada_w) < 8) then/*Inserido este tratamento para que n�o ocorra o erro 'ORA-12899 Valor  muito grande para a coluna', OS 228723 - Geliard*/
	begin
 	:new.dt_periodo_inicial := dt_entrada_w;
	end;
end if;
exception
	when others then
	null;
	end;

select  count(*),
        max(dt_final)
into    qt_aih_unif_w,
        dt_final_w
from    sus_aih_unif
where   nr_interno_conta = :new.nr_interno_conta;

select  count(*)
into    qt_apac_unif_w
from    sus_apac_unif
where   nr_interno_conta = :new.nr_interno_conta;

/*Inicio altera��o OS190963 Geliard*/
if      (ie_ok_w = 'S')                         and
        (:new.ie_status_acerto = 2)             and
        (ie_tipo_convenio_w = 3)                and
        (nvl(qt_aih_unif_w,0) > 0)              and
        ((:new.dt_periodo_final > dt_alta_w)    or
        (dt_final_w > dt_alta_w))               then
        begin
        if      (:new.dt_periodo_final > dt_alta_w) then
                begin
                :new.dt_periodo_final := dt_alta_w;
                end;
        end if;

	ie_alterar_final_aih_w := obter_valor_param_usuario(1123,91,obter_perfil_ativo,:new.nm_usuario,0);

        if      (ie_alterar_final_aih_w = 'S')  then
                begin

                begin
                update  sus_aih_unif
                set     dt_final = dt_alta_w
                where   nr_interno_conta = :new.nr_interno_conta
                and     dt_final > dt_alta_w;
                end;

                end;
        end if;

        end;
elsif   (ie_ok_w = 'S')                         and
        (:new.ie_status_acerto = 2)             and
	(nvl(qt_apac_unif_w,0) = 0)		and
        (:new.dt_periodo_final > dt_alta_w)     then
        begin
        :new.dt_periodo_final := dt_alta_w;
        end;
end if;
/*Fim altera��o OS190963 Geliard*/
if      (:new.dt_periodo_inicial > :new.dt_periodo_final) then
        begin
        :new.dt_periodo_final := (:new.dt_periodo_inicial + 365);
        end;
end if;

if      (ie_ok_w = 'S')                                         and
        (:new.ie_status_acerto = 2)                     then
        begin

	if      (:new.nr_seq_protocolo is null) then
                begin
                :new.nr_protocolo                       := '0';
                select  nvl(dt_dia_vencimento,30),
                        nvl(dt_ref_valida,sysdate)
                into            dt_dia_vcto_w,
                                dt_ref_valida_w
                from            convenio
                where   cd_convenio = :new.cd_convenio_parametro;

                if      (to_date(to_char(:new.dt_periodo_final,'dd/mm/yyyy'),'dd/mm/yyyy') >
                        to_date(to_char(dt_ref_valida_w,'dd/mm/yyyy'),'dd/mm/yyyy')) then
                        begin
                        dt_virada_w := PKG_DATE_UTILS.ADD_MONTH(dt_ref_valida_w,1,0);
                        if      (to_number(to_char(pkg_date_utils.end_of(dt_virada_w, 'MONTH'),'dd')) <= dt_dia_vcto_w) then
                                begin
                                dt_virada_w := pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_virada_w, 'MONTH'), NVL(dt_virada_w, PKG_DATE_UTILS.GET_TIME('00:00:00')));
                                end;
                        else
                                begin
                                dt_virada_w :=  to_date(to_char(dt_dia_vcto_w)||'/'||to_char(dt_virada_w,'mm/yyyy'),'DD/MM/YYYY');
                                end;
                        end if;
                        :new.dt_mesano_referencia := dt_virada_w;
                        end;
                end if;
                :new.nr_seq_apresent            := null;
                end;
        else
                begin
                select  dt_mesano_referencia,
                        nr_protocolo
                into    dt_mesano_referencia_w,
                        nr_protocolo_w
                from    protocolo_convenio
                where   nr_seq_protocolo = :new.nr_seq_protocolo;

                :new.dt_mesano_referencia       := dt_mesano_referencia_w;
                :new.nr_protocolo               := substr(nr_protocolo_w,1,40);

                if      (:new.nr_seq_apresent is null) then
                        begin
                        select conta_paciente_seq2.nextval
                        into    nr_seq_apresent_w
                        from dual;
                        :new.nr_seq_apresent := nr_seq_apresent_w;
                        end;
                end if;
                end;
        end if;
        end;
end if;

/* Marcus 11/10/2005 */
if      (:old.ie_status_acerto = 1)             and
        (:new.ie_status_acerto = 2)             then
        begin
        :new.dt_conta_definitiva        := dt_sistema_w;
        :new.qt_dias_conta              := :new.dt_conta_definitiva - :new.dt_periodo_inicial;
	:new.qt_dias_periodo		:= :new.dt_periodo_final - :new.dt_periodo_inicial;
        end;
elsif   (:new.ie_status_acerto = 1)             then
        begin
        :new.dt_conta_definitiva        := null;
        :new.qt_dias_conta              := null;
	:new.qt_dias_periodo		:= null;
        end;
end if;

if      (:old.nr_seq_protocolo is null)         and
        (:new.nr_seq_protocolo is not null)     then
        begin
        :new.dt_conta_protocolo := dt_sistema_w;
        end;
elsif   (:new.nr_seq_protocolo is null) then
        begin
        :new.dt_conta_protocolo := null;
        end;
end if;

select	nvl(max(nr_sequencia),0)
into	nr_seq_conta_conv_w
from	conta_convenio_atual
where	cd_convenio = :new.cd_convenio_parametro
and 	dt_competencia = trunc(:new.dt_mesano_referencia,'mm')
and	ie_situacao = 'A';

if	(nr_seq_conta_conv_w > 0) then
	begin

	if	(nvl(:new.nr_conta_convenio,0) > 0) and
		(nvl(:old.nr_conta_convenio,0) <> nvl(:new.nr_conta_convenio,0)) then
		begin


		begin
		select	nvl(nr_conta_inicial,0),
			nvl(nr_final_conta,0)
		into	nr_conta_inicial_w,
			nr_final_conta_w
		from	conta_convenio_atual
		where	nr_sequencia = nr_seq_conta_conv_w;
		exception
		when others then
			nr_conta_inicial_w	:= 0;
			nr_final_conta_w	:= 0;
		end;

		if	(:new.nr_conta_convenio > nr_final_conta_w) then
			begin
			:new.nr_conta_convenio := nr_conta_inicial_w;
			nr_conta_atual_w := nr_conta_inicial_w;
			end;
		else
			nr_conta_atual_w := :new.nr_conta_convenio;
		end if;

		if	(nr_conta_atual_w > 0) then
			begin

			update 	conta_convenio_atual
			set 	nr_conta_atual 	= nr_conta_atual_w,
				dt_atualizacao 	= sysdate,
				nm_usuario	= :new.nm_usuario
			where	nr_sequencia 	= nr_seq_conta_conv_w;

			end;
		end if;

		end;
	end if;

	end;
end if;

if      ((nvl(:new.nr_seq_status_fat,0) <> nvl(:old.nr_seq_status_fat,0))	or
	(nvl(:new.nr_seq_status_mob,0) <> nvl(:old.nr_seq_status_mob,0)))	then

	begin

	insert into cf_log_conta_paciente(
				nr_sequencia,
				nr_atendimento,
				nr_interno_conta,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_status_fat,
				nr_seq_status_fat_ant,
				nr_seq_status_mob,
				nr_seq_status_mob_ant,
				nr_seq_regra_fluxo)
			values	(
				cf_log_conta_paciente_seq.nextval,
				:new.nr_atendimento,
				:new.nr_interno_conta,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.nr_seq_status_fat,
				:old.nr_seq_status_fat,
				:new.nr_seq_status_mob,
				:old.nr_seq_status_mob,
				:new.nr_seq_regra_fluxo);

        end;
end if;

if 	(:new.nr_interno_conta is not null) and
	(nvl(:new.nr_atendimento,0) <> 0) and
	(nvl(:old.nr_atendimento,0) <> 0) and
	(:new.nr_atendimento <> :old.nr_atendimento) then
	begin
	insert into conta_hist_transf_atend(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_interno_conta,
				nr_atendimento_atual,
				nr_atendimento_old)
			values	(conta_hist_transf_atend_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.nr_interno_conta,
				:new.nr_atendimento,
				:old.nr_atendimento);


	end;
end if;

if	(:old.ie_status_acerto = 2 and :new.ie_status_acerto = 1) then
	cf_retornar_status_fat(	:new.nr_interno_conta,
							 'C',
							 nr_seq_status_fat_w,
							 nr_seq_status_mob_w,
							 :new.nm_usuario);						 
	
	:new.nr_seq_status_fat := nr_seq_status_fat_w;			
	:new.nr_seq_status_mob := nvl(nr_seq_status_mob_w, :new.nr_seq_status_mob);		

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.conta_paciente_delete
before delete ON TASY.CONTA_PACIENTE for each row
declare

ds_module_tasy_w		varchar2(255);
osuser_tasy_w			varchar2(100);
ds_origem_w			varchar2(1500);
qt_hist_laudo_w			number(10) := 0;

begin

select	max(module||' - ' || machine||' - ' || program|| ' - ' || OSUSER|| ' - ' || TERMINAL),
	max(substr(OSUSER,1,15))
into	ds_module_tasy_w,
	osuser_tasy_w
from	v$session
where	audsid = (select userenv('sessionid') from dual)
and 	upper(program) like '%TASY%';

if	(ds_module_tasy_w is not null) then
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1500);
end if;

select	count(1)
into	qt_hist_laudo_w
from	sus_hist_envio_laudo_pac
where	nr_interno_conta = :old.nr_interno_conta
and	rownum = 1;

if	(qt_hist_laudo_w > 0) then
	begin

	update	sus_hist_envio_laudo_pac
	set 	nr_interno_conta = null
	where	nr_interno_conta = :old.nr_interno_conta;

	end;
end if;

same_excluir_conta_pront(:old.nr_interno_conta, :old.nr_atendimento, wheb_usuario_pck.get_nm_usuario);

end;
/


DROP SYNONYM TASY_CONSULTA.CONTA_PACIENTE;

CREATE SYNONYM TASY_CONSULTA.CONTA_PACIENTE FOR TASY.CONTA_PACIENTE;


ALTER TABLE TASY.CONTA_PACIENTE ADD (
  CONSTRAINT CONPACI_PK
 PRIMARY KEY
 (NR_INTERNO_CONTA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          28M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CONPACI_SEQ_ORDEM_UK
 UNIQUE (NR_SEQ_ORDEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_PACIENTE ADD (
  CONSTRAINT CONPACI_CFREGRA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_FLUXO) 
 REFERENCES TASY.CF_REGRA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_CFSTAFA_FK 
 FOREIGN KEY (NR_SEQ_STATUS_FAT) 
 REFERENCES TASY.CF_STATUS_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT CONPACI_CFSTMOB_FK 
 FOREIGN KEY (NR_SEQ_STATUS_MOB) 
 REFERENCES TASY.CF_STATUS_MOBILIZACAO (NR_SEQUENCIA),
  CONSTRAINT CONPACI_TPCOCON_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBRANCA) 
 REFERENCES TASY.TIPO_COBRANCA_CONTA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_CATIVA_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA_IVA) 
 REFERENCES TASY.CATEGORIA_IVA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_CONTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONPACI_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_CONTA) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT CONPACI_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONPACI_ESTCONT_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO_CONTA) 
 REFERENCES TASY.ESTAGIO_CONTA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_AUDIT_HIST_GLOSA) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT CONPACI_FATPFAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_FATURA) 
 REFERENCES TASY.FATUR_TIPO_FATURA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_TISSMSI_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_INT) 
 REFERENCES TASY.TISS_MOTIVO_SAIDA_INT (NR_SEQUENCIA),
  CONSTRAINT CONPACI_TISSTSC_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_CONSULTA) 
 REFERENCES TASY.TISS_TIPO_SAIDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT CONPACI_TISSTSS_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_SPSADT) 
 REFERENCES TASY.TISS_TIPO_SAIDA_SPSADT (NR_SEQUENCIA),
  CONSTRAINT CONPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CONPACI_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_CALCULO, CD_CATEGORIA_CALCULO) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONPACI_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_PARAMETRO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT CONPACI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONPACI_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT CONPACI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CONPACI_CONPACI_FK 
 FOREIGN KEY (NR_SEQ_CONTA_ORIGEM) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT CONPACI_CATCONV_FK2 
 FOREIGN KEY (CD_CONVENIO_PARAMETRO, CD_CATEGORIA_PARAMETRO) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONPACI_PQPROTO_FK 
 FOREIGN KEY (NR_SEQ_PQ_PROTOCOLO) 
 REFERENCES TASY.PQ_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT CONPACI_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_REPASSE) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CONPACI_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA),
  CONSTRAINT CONPACI_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO_PARAMETRO, CD_PLANO_RETORNO_CONV) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO));

GRANT SELECT ON TASY.CONTA_PACIENTE TO NIVEL_1;

GRANT SELECT ON TASY.CONTA_PACIENTE TO TASY_CONSULTA;

