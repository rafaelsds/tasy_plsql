ALTER TABLE TASY.PARECER_MEDICO_REQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARECER_MEDICO_REQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARECER_MEDICO_REQ
(
  NR_PARECER                  NUMBER(10)        NOT NULL,
  NR_ATENDIMENTO              NUMBER(10),
  CD_MEDICO                   VARCHAR2(10 BYTE) NOT NULL,
  CD_ESPECIALIDADE            NUMBER(5),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_ESPECIALIDADE_DEST       NUMBER(5),
  DS_MOTIVO_CONSULTA          LONG,
  DT_LIBERACAO                DATE,
  CD_PERFIL_ATIVO             NUMBER(5),
  CD_PESSOA_PARECER           VARCHAR2(10 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  CD_ESPECIALIDADE_PROF       NUMBER(5),
  CD_ESPECIALIDADE_DEST_PROF  NUMBER(5),
  IE_TIPO_PARECER             VARCHAR2(1 BYTE),
  NR_SEQ_EQUIPE_DEST          NUMBER(10),
  NR_SEQ_ASSINATURA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO   NUMBER(10),
  NR_SEQ_TIPO_PARECER         NUMBER(10),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_AREA_ATUACAO         NUMBER(10),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE),
  NR_SEQ_SOAP                 NUMBER(10),
  IE_INTEGRACAO               VARCHAR2(1 BYTE),
  CD_CGC                      VARCHAR2(14 BYTE),
  NR_SEQ_REGULACAO_ORI        NUMBER(10),
  NR_SEQ_ORIGEM               NUMBER(10),
  DT_CRIACAO                  DATE,
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  IE_ACAO                     VARCHAR2(1 BYTE),
  IE_GET_MED_APPT             VARCHAR2(8 BYTE),
  NR_SEQ_FORMULARIO           NUMBER(10),
  IE_URGENCIA                 VARCHAR2(1 BYTE),
  NR_SEQ_AGENDA_CONS          NUMBER(10),
  NR_SEQ_NAIS_INSURANCE       NUMBER(10),
  CD_EVOLUCAO                 NUMBER(10),
  CD_ESPECIALIDADE_MED        NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARMERE_AREATUM_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_AREA_ATUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_ATCONSPEPA_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_ATEPACI_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_ATESOAP_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_EHRREGI_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_ESPMEDI_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_ESPMEDI2_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_ESPECIALIDADE_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_ESPMEDI2_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_ESPPROF_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_ESPECIALIDADE_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_ESPPROF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_ESPPROF_FK2_I ON TASY.PARECER_MEDICO_REQ
(CD_ESPECIALIDADE_DEST_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_ESPPROF_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_EVOPACI_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_I1 ON TASY.PARECER_MEDICO_REQ
(CD_PESSOA_PARECER, NR_ATENDIMENTO, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_I2 ON TASY.PARECER_MEDICO_REQ
(NR_ATENDIMENTO, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_NAISINS_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_PERFIL_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_PESFISI_FK_I ON TASY.PARECER_MEDICO_REQ
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_PESFISI_FK1_I ON TASY.PARECER_MEDICO_REQ
(CD_PESSOA_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_PESFISI_FK2_I ON TASY.PARECER_MEDICO_REQ
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_PFEQUIP_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_EQUIPE_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARMERE_PK ON TASY.PARECER_MEDICO_REQ
(NR_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMERE_TASASDI_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_TASASDI_FK2_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMERE_TIPPARE_FK_I ON TASY.PARECER_MEDICO_REQ
(NR_SEQ_TIPO_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMERE_TIPPARE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PARECER_MEDICO_REQ_SBIS_DEL
before delete ON TASY.PARECER_MEDICO_REQ for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 obter_pessoa_atendimento(:old.nr_atendimento,'C'),
									 obter_desc_expressao(487400),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));


end;
/


CREATE OR REPLACE TRIGGER TASY.PARECER_MEDICO_REQ_SBIS_IN
before insert or update ON TASY.PARECER_MEDICO_REQ for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       :new.cd_pessoa_fisica,
                       obter_desc_expressao(487400),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       :new.cd_pessoa_fisica,
                       obter_desc_expressao(487400),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_evolucao_parecer_req 
after update ON TASY.PARECER_MEDICO_REQ 
for each row
declare 
 

begin 


    if(:old.dt_liberacao is null and :new.dt_liberacao is not null and :new.dt_inativacao is null)then 
        hsj_gerar_evolucao_parecer(:new.nr_parecer, :new.nm_usuario, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.cd_medico, :new.cd_especialidade_dest, :new.cd_pessoa_parecer);
    end if; 


end hsj_evolucao_parecer_req;
/


CREATE OR REPLACE TRIGGER TASY.parecer_medico_req_pend_atual
after insert or update ON TASY.PARECER_MEDICO_REQ for each row
declare

qt_reg_w		number(1);
nm_usuario_w		varchar2(15);
ie_lib_parecer_medico_w	varchar2(10);
ie_pend_parecer_w	varchar2(10);
ie_gerar_evol_in_ref_w varchar2(5);

Cursor C01 is
	select	substr(obter_usuario_pf(cd_pessoa_fisica),1,100)
	from	medico_especialidade
	where	obter_se_corpo_clinico(cd_pessoa_fisica) = 'S'
	and	cd_especialidade = :new.cd_especialidade_dest
	and	substr(obter_usuario_pf(cd_pessoa_fisica),1,100) is not null
	and	:new.cd_pessoa_parecer is null
	and	nvl(:new.cd_especialidade_dest_prof,0) = 0
	union
	select	substr(obter_usuario_pf(cd_pessoa_fisica),1,100)
	from	profissional_especialidade
	where	cd_especialidade_prof = :new.cd_especialidade_dest_prof
	and	substr(obter_usuario_pf(cd_pessoa_fisica),1,100) is not null
	and	:new.cd_pessoa_parecer is null
	and	nvl(:new.cd_especialidade_dest,0) = 0
	union
	select	substr(obter_usuario_pf(:new.cd_pessoa_parecer),1,100)
	from	dual
	where	:new.cd_pessoa_parecer is not null;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

obter_param_usuario(281, 1641, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_evol_in_ref_w);

select	max(ie_lib_parecer_medico)
into	ie_lib_parecer_medico_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

select	max(ie_pend_parecer)
into	ie_pend_parecer_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_lib_parecer_medico_w,'N') = 'S') and
	(nvl(ie_pend_parecer_w,'S') = 'S') 	then
	if	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null)then
		Gerar_registro_pendente_PEP('XPM', :new.nr_parecer, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario);

		open C01;
		loop
		fetch C01 into
			nm_usuario_w;
		exit when C01%notfound;
			if	(nm_usuario_w	<> :new.nm_usuario) then
				Gerar_registro_pendente_PEP('PM', :new.nr_parecer, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w);
			end if;
		end loop;
		close C01;
	elsif	(:new.dt_liberacao 	is null) then
		Gerar_registro_pendente_PEP('PM', :new.nr_parecer, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario);

	end if;
end if;

if( ie_gerar_evol_in_ref_w ='S' and :new.cd_evolucao is not null and :new.dt_inativacao is not null and :old.dt_inativacao is null) then
    update evolucao_paciente
	set dt_inativacao = sysdate,
	ie_situacao ='I',
	dt_atualizacao = sysdate ,
	nm_usuario = wheb_usuario_pck.get_nm_usuario,
	nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
	ds_justificativa = :new.ds_justificativa
	where cd_evolucao = :new.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'INTERNAL_REF' and nr_seq_med_item = :new.NR_PARECER;
    end if;
<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.PARECER_MEDICO_REQ_ATUAL
before insert or update ON TASY.PARECER_MEDICO_REQ for each row
declare

qt_idade_w					number(10);
cd_estabelecimento_w		number(4) := obter_estabelecimento_ativo;
cd_setor_paciente_w			number(10)	:= 0;
nr_seq_evento_w				number(10);

nr_seq_tipo_parecer_ww      regra_envio_sms.nr_seq_tipo_parecer%type;
qt_tempo_normal_w			wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_w				wl_regra_item.nr_sequencia%type;
qt_pendencias_w				number(10);
nr_seq_tipo_adm_fat_atd_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w			atendimento_paciente.nr_seq_episodio%type;

qt_reg_w					number(1);

Cursor C01 is
    select
        nr_seq_evento
    from
    (
        select
            1 cd,
            nr_seq_evento
        from
            regra_envio_sms
        where
            cd_estabelecimento = cd_estabelecimento_w
            and ie_evento_disp = 'LPPEP'
            and qt_idade_w between nvl(qt_idade_min, 0) and nvl(qt_idade_max, 9999)
            and nvl(cd_setor_atendimento, cd_setor_paciente_w) = cd_setor_paciente_w
            and nvl(ie_situacao, 'A') = 'A'
            and nr_seq_tipo_parecer = :new.nr_seq_tipo_parecer
        union
        select
            2 cd,
            nr_seq_evento
        from
            regra_envio_sms
        where
            cd_estabelecimento = cd_estabelecimento_w
            and ie_evento_disp = 'LPPEP'
            and qt_idade_w between nvl(qt_idade_min, 0) and nvl(qt_idade_max, 9999)
            and nvl(cd_setor_atendimento, cd_setor_paciente_w) = cd_setor_paciente_w
            and nvl(ie_situacao, 'A') = 'A'
            and nvl(nr_seq_tipo_parecer, nvl(:new.nr_seq_tipo_parecer, - 1)) = nvl(:new.nr_seq_tipo_parecer, - 1)
    )
    where
        rownum = 1;

cursor C02 is

	select	nvl(b.qt_tempo_normal, 0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		nr_seq_tipo_parecer = :new.nr_seq_tipo_parecer
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'DL'
								and		x.ie_situacao = 'A');
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO_NREC, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.CD_PESSOA_PARECER is not null) then
	qt_idade_w	:= nvl(obter_idade_pf(:new.cd_pessoa_fisica,sysdate,'A'),0);
	if	(:new.nr_Atendimento is not null) then
		cd_setor_paciente_w	:= obter_setor_Atendimento(:new.nr_Atendimento);
	end if;

	open C01;
	loop
	fetch C01 into
       nr_seq_evento_w;
	exit when C01%notfound;
		begin
            gerar_evento_paciente(nr_seq_evento_w,:new.nr_atendimento,:new.cd_pessoa_fisica,null,:new.nm_usuario,null,null,null,null,:new.cd_medico,:new.CD_PESSOA_PARECER, null, null,null,null,'N',null,ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate),null,null,null,null,
                                  :new.cd_especialidade_dest,:new.cd_especialidade,:new.nr_seq_tipo_parecer,:new.nr_seq_equipe_dest,'S');
		end;
	end loop;
	close C01;

end if;

-- WorkList AtePac_WL (357)

if	(:new.ie_situacao = 'I') then

	select	count(*)
	into	qt_pendencias_w
	from	wl_worklist
	where	nr_parecer = :new.nr_parecer;

	if (qt_pendencias_w > 0) then
		wl_gerar_finalizar_tarefa('DL', 'F', null, null, :new.nm_usuario, sysdate,'N',null,null,null,null,null,null,null, :new.nr_parecer );

	end if;
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_seq_tipo_parecer is not null)then

	if (:new.nr_atendimento is not null) then

		select	max(nr_seq_tipo_admissao_fat),
				max(ie_tipo_atendimento),
				max(nr_seq_episodio)
		into	nr_seq_tipo_adm_fat_atd_w,
				ie_tipo_atendimento_w,
				nr_seq_episodio_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

	end if;

	open C02;
	loop
	fetch C02 into
		qt_tempo_normal_w,
		nr_seq_regra_w;
	exit when C02%notfound;
		begin

			if	(qt_tempo_normal_w > 0 and obter_se_regra_geracao(nr_seq_regra_w,nr_seq_episodio_w,nr_seq_tipo_adm_fat_atd_w) = 'S') then
				-- Gera Tarefa no Worklist para diagnostico de admissao
				wl_gerar_finalizar_tarefa('DL','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,(sysdate+(qt_tempo_normal_w/24)),'N',
									null,null,null,null,null,null,null,:new.nr_parecer,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_liberacao,nr_seq_episodio_w);
			end if;
		end;
	end loop;
	close C02;

end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_PARECER_MEDICO_REQ
before insert or update ON TASY.PARECER_MEDICO_REQ for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


ALTER TABLE TASY.PARECER_MEDICO_REQ ADD (
  CONSTRAINT PARMERE_PK
 PRIMARY KEY
 (NR_PARECER)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARECER_MEDICO_REQ ADD (
  CONSTRAINT PARMERE_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT PARMERE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PARMERE_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PARMERE_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT PARMERE_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT PARMERE_AREATUM_FK 
 FOREIGN KEY (NR_SEQ_AREA_ATUACAO) 
 REFERENCES TASY.AREA_ATUACAO_MEDICA (NR_SEQUENCIA),
  CONSTRAINT PARMERE_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_DEST) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT PARMERE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PARMERE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PARMERE_TIPPARE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PARECER) 
 REFERENCES TASY.TIPO_PARECER (NR_SEQUENCIA),
  CONSTRAINT PARMERE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PARMERE_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_PARECER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMERE_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMERE_ESPPROF_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_PROF) 
 REFERENCES TASY.ESPECIALIDADE_PROFISSIONAL (CD_ESPECIALIDADE_PROF),
  CONSTRAINT PARMERE_ESPPROF_FK2 
 FOREIGN KEY (CD_ESPECIALIDADE_DEST_PROF) 
 REFERENCES TASY.ESPECIALIDADE_PROFISSIONAL (CD_ESPECIALIDADE_PROF),
  CONSTRAINT PARMERE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PARMERE_ESPMEDI2_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_DEST) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PARMERE_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMERE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PARECER_MEDICO_REQ TO NIVEL_1;

