ALTER TABLE TASY.PAC_CLEREANCE_CREATININA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAC_CLEREANCE_CREATININA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAC_CLEREANCE_CREATININA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE               NOT NULL,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  DT_LIBERACAO               DATE,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_REGISTRO                DATE               NOT NULL,
  QT_PESO                    NUMBER(15,3)       NOT NULL,
  QT_IDADE                   NUMBER(15)         NOT NULL,
  IE_RACA_NEGRA              VARCHAR2(1 BYTE)   NOT NULL,
  QT_CREATININA_SERICA       NUMBER(15,2)       NOT NULL,
  QT_ALTURA_CM               NUMBER(3)          NOT NULL,
  IE_RN_PRE_TERMO            VARCHAR2(1 BYTE)   NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  QT_COCKCROFT_GAULT         NUMBER(15,2),
  QT_MDRD                    NUMBER(15,2),
  QT_SCHWARTZ                NUMBER(15,2),
  QT_COUNHAHAN_BARRATT       NUMBER(15,2),
  QT_CLEARENCE_MEDIDO        NUMBER(15,2),
  NR_ATEND_TASYMED           NUMBER(10),
  NR_SEQ_CLIENTE             NUMBER(10),
  DT_CREATININA              DATE,
  NR_SEQ_APAE                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  QT_JELLIFFE                NUMBER(15,2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACCLCR_ATEPACI_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACCLCR_AVAPRAN_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQ_APAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_AVAPRAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_EHRREEL_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_ESTABEL_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_I1 ON TASY.PAC_CLEREANCE_CREATININA
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACCLCR_MEDATEN_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_ATEND_TASYMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_MEDATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_MEDCLIE_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_MEDCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_PERFIL_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_PESFISI_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACCLCR_PESFISI_FK2_I ON TASY.PAC_CLEREANCE_CREATININA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACCLCR_PK ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACCLCR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACCLCR_TASASDI_FK_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACCLCR_TASASDI_FK2_I ON TASY.PAC_CLEREANCE_CREATININA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pac_clereance_creat_befins
before insert or update ON TASY.PAC_CLEREANCE_CREATININA for each row
declare
vl_mult_w			number(10,4);
vl_mult_mdrd_w			number(10,4);
vl_mult_negro_w			number(10,4);
nr_seq_exame_cre_ser_w		number(10,0);
nr_seq_exame_cle_cre_w		number(10,0);
qt_result_exam_cre_w		number(15,4):= 0;
qt_result_exam_cle_cre_w	number(15,4):= 0;
vl_const_schwartz_w			number(10,2);
qt_peso_gramas_w			number(20,4);
qt_superf_corporea_bb_w		number(18,7);
qt_jelliffe_w				number(15,4):= 0;
qt_reg_w	number(1);
begin
if	(:new.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is null) then
	:new.cd_pessoa_fisica := obter_pessoa_atendimento(:new.nr_atendimento,'C');
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Obter idade atual */
if	(:new.qt_idade is null) then
	select	to_number(nvl(max(obter_idade_pf(:new.cd_pessoa_fisica, sysdate, 'A')),0))
	into	:new.qt_idade
	from	dual;
end if;

/* Obter se raca negra */
if	(:new.ie_raca_negra is null) then
	select	nvl(max(ie_negro),'N')
	into	:new.ie_raca_negra
	from	cor_pele a,
		pessoa_fisica b
	where	b.nr_seq_cor_pele = a.nr_sequencia
	and	b.cd_pessoa_fisica = :new.cd_pessoa_fisica;
end if;

/* Obter multiplicador se raca negra */
select	nvl(decode(max(:new.ie_raca_negra),'S',1.210,'N',1),1)
into	vl_mult_negro_w
from	dual;

/* Obter valor multiplicacao caso seja do sexo feminino */
select	nvl(decode(obter_sexo_pf(:new.cd_pessoa_fisica,'C'),'F',0.85,1),1),
	nvl(decode(obter_sexo_pf(:new.cd_pessoa_fisica,'C'),'F',0.742,1),1)
into	vl_mult_w,
	vl_mult_mdrd_w
from	dual;

/* Obter valor da constante Schwartz */
if	(:new.ie_rn_pre_termo = 'S') then
	vl_const_schwartz_w:= 0.33;
elsif	(:new.qt_idade <= 2) and
	(:new.ie_rn_pre_termo = 'N' ) then
	vl_const_schwartz_w:= 0.45;
elsif	(:new.qt_idade > 2) and
	(obter_sexo_pf(:new.cd_pessoa_fisica,'C') = 'F') then
	vl_const_schwartz_w:= 0.55;
elsif	(:new.qt_idade > 2) and
	(obter_sexo_pf(:new.cd_pessoa_fisica,'C') = 'M') then
	vl_const_schwartz_w:= 0.70;
end if;

/* Equacao Cockcroft-Gault */
:new.qt_cockcroft_gault:= (140 - :new.qt_idade) * dividir(:new.qt_peso, (72 * :new.qt_creatinina_serica)) * (vl_mult_w);
/* Equacao simplificada do MDRD */
begin
:new.qt_mdrd:= 186 * power(:new.qt_creatinina_serica, - 1.154) * power(:new.qt_idade, - 0.203) * (vl_mult_mdrd_w) * (vl_mult_negro_w);
exception
	when others then
	:new.qt_mdrd := 0;
end;
/* Colocado exception pois estava dando numeric overfloat no HSC quando rodava o comando power(0, -1.154) - Oraci em 30/10/2007 */
/* Formula de Schwartz */
:new.qt_schwartz:= vl_const_schwartz_w * dividir(:new.qt_altura_cm, :new.qt_creatinina_serica);
/* Equacao de Counhahan-Barratt */
:new.qt_counhahan_barratt:= 0.43 * dividir(:new.qt_altura_cm, :new.qt_creatinina_serica);
/* Equacao de Jellife modificado */
begin
qt_superf_corporea_bb_w := obter_superficie_corporea(:new.qt_altura_cm,:new.qt_peso,'D');
qt_jelliffe_w := (dividir((98 - (0.8 * (:new.qt_idade - 20))),:new.qt_creatinina_serica) * dividir(qt_superf_corporea_bb_w,1.73));
if	(obter_sexo_pf(:new.cd_pessoa_fisica,'C') = 'F') then
	:new.qt_jelliffe:= (qt_jelliffe_w * 0.9);
else
	:new.qt_jelliffe:= (qt_jelliffe_w);
end if;
exception
	when others then
	:new.qt_jelliffe := 0;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PAC_CLEREANCE_CREATININA ADD (
  CONSTRAINT PACCLCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAC_CLEREANCE_CREATININA ADD (
  CONSTRAINT PACCLCR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACCLCR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACCLCR_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACCLCR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PACCLCR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACCLCR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PACCLCR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACCLCR_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACCLCR_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PACCLCR_MEDATEN_FK 
 FOREIGN KEY (NR_ATEND_TASYMED) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO),
  CONSTRAINT PACCLCR_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_APAE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PAC_CLEREANCE_CREATININA TO NIVEL_1;

