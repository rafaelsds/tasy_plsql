ALTER TABLE TASY.MAN_CLASSIFICACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_CLASSIFICACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_CLASSIFICACAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_EMPRESA                  NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_CLASSIF_PRINC            VARCHAR2(15 BYTE) NOT NULL,
  DS_CLASSIFICACAO            VARCHAR2(80 BYTE) NOT NULL,
  IE_AGREGA_VALOR             VARCHAR2(1 BYTE)  NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  NR_SEQ_NIVEL_VALOR          NUMBER(10),
  IE_PERMITE_ALTER_ABRANG_OS  VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_HIST            NUMBER(10),
  CD_EXP_CLASSIFICACAO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANCLAS_DICEXPR_FK_I ON TASY.MAN_CLASSIFICACAO
(CD_EXP_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANCLAS_EMPRESA_FK_I ON TASY.MAN_CLASSIFICACAO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANCLAS_ESTABEL_FK_I ON TASY.MAN_CLASSIFICACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCLAS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCLAS_MANNIVA_FK_I ON TASY.MAN_CLASSIFICACAO
(NR_SEQ_NIVEL_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCLAS_MANNIVA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCLAS_MANTIHI_FK_I ON TASY.MAN_CLASSIFICACAO
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCLAS_MANTIHI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANCLAS_PK ON TASY.MAN_CLASSIFICACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_CLASSIFICACAO ADD (
  CONSTRAINT MANCLAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_CLASSIFICACAO ADD (
  CONSTRAINT MANCLAS_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CLASSIFICACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT MANCLAS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MANCLAS_MANNIVA_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_VALOR) 
 REFERENCES TASY.MAN_NIVEL_VALOR (NR_SEQUENCIA),
  CONSTRAINT MANCLAS_MANTIHI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.MAN_TIPO_HIST (NR_SEQUENCIA),
  CONSTRAINT MANCLAS_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.MAN_CLASSIFICACAO TO NIVEL_1;

