ALTER TABLE TASY.ATEND_CATEGORIA_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_CATEGORIA_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_CATEGORIA_CONVENIO
(
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  CD_CONVENIO                 NUMBER(5)         NOT NULL,
  CD_CATEGORIA                VARCHAR2(10 BYTE) NOT NULL,
  DT_INICIO_VIGENCIA          DATE              NOT NULL,
  DT_FINAL_VIGENCIA           DATE,
  DT_ATUALIZACAO              DATE              NOT NULL,
  CD_USUARIO_CONVENIO         VARCHAR2(30 BYTE),
  CD_EMPRESA                  NUMBER(10),
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_DOC_CONVENIO             VARCHAR2(20 BYTE),
  CD_TIPO_ACOMODACAO          NUMBER(4),
  CD_MUNICIPIO_CONVENIO       NUMBER(8),
  CD_CONVENIO_GLOSA           NUMBER(5),
  CD_CATEGORIA_GLOSA          VARCHAR2(10 BYTE),
  DT_VALIDADE_CARTEIRA        DATE,
  NR_ACOMPANHANTE             NUMBER(3),
  CD_PLANO_CONVENIO           VARCHAR2(10 BYTE),
  CD_DEPENDENTE               NUMBER(3),
  NR_SEQ_INTERNO              NUMBER(10)        NOT NULL,
  NR_SEQ_ORIGEM               NUMBER(10),
  CD_SENHA                    VARCHAR2(20 BYTE),
  IE_TIPO_GUIA                VARCHAR2(2 BYTE),
  DS_OBSERVACAO               VARCHAR2(2000 BYTE),
  QT_DIA_INTERNACAO           NUMBER(3),
  DT_ULTIMO_PAGTO             DATE,
  CD_COMPLEMENTO              VARCHAR2(30 BYTE),
  DT_ACEITE_DIF_ACOMOD        DATE,
  NM_USUARIO_ACEITE           VARCHAR2(15 BYTE),
  DS_OBSERVACAO_ACEITE        VARCHAR2(255 BYTE),
  QT_DIETA_ACOMP              NUMBER(3),
  IE_LIB_DIETA                VARCHAR2(15 BYTE),
  NR_DOC_CONV_PRINCIPAL       VARCHAR2(20 BYTE),
  IE_REGIME_INTERNACAO        VARCHAR2(2 BYTE),
  CD_USUARIO_CONV_GLOSA       VARCHAR2(30 BYTE),
  CD_COMPLEMENTO_GLOSA        VARCHAR2(30 BYTE),
  DT_VALIDADE_CART_GLOSA      DATE,
  NM_USUARIO_ORIGINAL         VARCHAR2(15 BYTE),
  NR_SEQ_LIB_DIETA_CONV       NUMBER(10),
  NR_SEQ_TIPO_LIB_GUIA        NUMBER(10),
  NR_SEQ_COBERTURA            NUMBER(10),
  NR_SEQ_ABRANGENCIA          NUMBER(10),
  CD_SENHA_PROVISORIA         VARCHAR2(20 BYTE),
  CD_PLANO_GLOSA              VARCHAR2(10 BYTE),
  NR_SEQ_REGRA_ACOMP          NUMBER(10),
  DS_TARJA_CARTAO             VARCHAR2(4000 BYTE),
  DS_JUST_ALTERACAO           VARCHAR2(255 BYTE),
  IE_COD_USUARIO_MAE_RESP     VARCHAR2(1 BYTE),
  NR_BIOMETRIA_CONV           NUMBER(10),
  NR_SEQ_JUST_BIOMETRIA_CONV  NUMBER(10),
  IE_TIPO_CONVENIADO          NUMBER(3),
  CD_CONV_BROKER              NUMBER(5),
  IE_AUTORIZA_ENVIO_CONVENIO  VARCHAR2(5 BYTE),
  NR_SEQ_PATIENT_CATEGORY     NUMBER(10),
  NR_PRIORIDADE               NUMBER(4),
  CD_CON_CARD_TYPE            VARCHAR2(128 BYTE),
  NR_CON_CARD                 VARCHAR2(16 BYTE),
  QT_BONUS_CONV_APRESENTADO   NUMBER(10),
  NR_SEQ_CATEGORIA_IVA        NUMBER(6),
  CD_PESSOA_TITULAR           VARCHAR2(10 BYTE),
  EKVK_CD_PESSOA_FISICA       VARCHAR2(15 BYTE),
  EKVK_NM_PAIS                VARCHAR2(100 BYTE),
  EKVK_NR_CARTAO              VARCHAR2(30 BYTE),
  EKVK_DT_FIM                 DATE,
  EKVK_DT_INICIO              DATE,
  EKVK_NR_CONV                VARCHAR2(10 BYTE),
  EKVK_SG_CONV                VARCHAR2(4 BYTE),
  EKVK_NR_SEQ_TIPO_DOC        NUMBER(10),
  NR_SEQ_CONV_CATEG_SEG       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          135M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATECACO_ABRAPLA_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_ABRANGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_ABRAPLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_ATEPACI_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_ATJBICV_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_JUST_BIOMETRIA_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_CATCONV_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          35M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_CATCONV_FK_I2 ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_CATCONV_FK_I2
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_CATIVA_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_CATEGORIA_IVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_COCATSEG_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_CONV_CATEG_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_CONORUS_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_CONORUS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_CONVCOB_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_CONVCOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_EKVKTPDOC_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(EKVK_NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_EMPREFE_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_I1 ON TASY.ATEND_CATEGORIA_CONVENIO
(DT_FINAL_VIGENCIA, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_I2 ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_ATENDIMENTO, DT_INICIO_VIGENCIA, CD_CONVENIO, CD_CATEGORIA, DT_FINAL_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_I4 ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_CONVENIO, NR_DOC_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          37M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_I6 ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_CONVENIO, CD_USUARIO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_I7 ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ATECACO_I8 ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_NUTRECA_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_REGRA_ACOMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_NUTRECA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_PATCATE_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_PATIENT_CATEGORY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_PESTICO_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_PESSOA_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATECACO_PK ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_ATENDIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATECACO_TILIDIC_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_LIB_DIETA_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_TILIDIC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_TILIGUE_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_TIPO_LIB_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_TILIGUE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_TIPACOM_FK_I ON TASY.ATEND_CATEGORIA_CONVENIO
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          25M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACO_TIPACOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATECACO_UK ON TASY.ATEND_CATEGORIA_CONVENIO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_categ_convenio_update
AFTER UPDATE ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_situacao_w			varchar2(1) := 'A';
ie_situacao_categ_w		varchar2(1) := 'A';
dt_cancelamento_w		date := sysdate;
ie_exige_orc_atend_w		varchar2(1);
dt_entrada_w			Date;
cd_medico_resp_w		varchar2(10);
ie_tipo_atendimento_w	number(3);
ie_regra_w			varchar2(1);
cd_estabelecimento_w		Number(5);
ie_tipo_convenio_atend_w	number(2,0); /* Rafael em 18/08/06 OS39167 */
ie_tipo_convenio_atend_ww	number(2,0);
ie_consiste_tipo_conv_w		varchar2(1); /* Rafael em 18/08/06 OS39167 */
ie_atualizar_tipo_conv_atend_w	varchar2(1) := 'S';
ie_vigencia_entrada_w		varchar2(1);
ie_entrada_vigencia_w 		varchar2(1);
ie_atualiza_validade_titular_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
ie_contem_convenio_w		varchar2(1);
ie_resUnimed_w			varchar2(1);
ds_param_integ_hl7_w 	varchar2(4000);
dt_cancelamento_atend_w	atendimento_paciente.dt_cancelamento%type;
qt_registros_w		number(10);
dt_alta_w		atendimento_paciente.dt_alta%type;
qt_exist_rule_w	number(10);
pessoa_titular_convenio_row_w   pessoa_titular_convenio%rowtype;
ie_save_insurance_holder_w	varchar2(1);

cursor locked_records is
select  *
from 	pessoa_titular_convenio
where  	cd_pessoa_fisica     = cd_pessoa_fisica_w
for update;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

  select  max(cd_pessoa_fisica)
  into	cd_pessoa_fisica_w
  from	atendimento_paciente
  where	nr_atendimento = :new.nr_atendimento;

  open locked_records;
  loop
    fetch locked_records into pessoa_titular_convenio_row_w;
    exit when locked_records%NOTFOUND;
  end loop;
  close locked_records;

  if	(:new.ie_tipo_guia) <> (:old.ie_tipo_guia) and
    (:old.ie_tipo_guia is not null) then

    begin
    insert into log_atendimento(
      dt_atualizacao,
      nm_usuario,
      cd_log,
      nr_atendimento,
      ds_log)
    values	(sysdate,
      :new.nm_usuario,
      135,
      :new.nr_atendimento,
      'CALLSTACK: ' || dbms_utility.format_call_stack);
    exception
    when others then
      null;
    end;

  end if;

  select	max(cd_estabelecimento)
  into	cd_estabelecimento_w
  from	atendimento_paciente
  where 	nr_atendimento =  :new.nr_atendimento;

  select	nvl(max(ie_consiste_tipo_conv),'N'), /* Rafael em 18/08/06 OS39167*/
    nvl(max(ie_atualizar_tipo_conv_atend),'S')
  into	ie_consiste_tipo_conv_w,
    ie_atualizar_tipo_conv_atend_w
  from	parametro_faturamento
  where	cd_estabelecimento = cd_estabelecimento_w;

  begin
  select	Max(ie_tipo_convenio),
    max(ie_situacao),
    max(dt_cancelamento),
    max(Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_w, 'IE_EXIGE_ORC_ATEND')) ie_exige_orc_atend
  into	ie_tipo_convenio_w,
    ie_situacao_w,
    dt_cancelamento_w,
    ie_exige_orc_atend_w
  from	convenio
  where	cd_convenio = :new.cd_convenio;
  exception
    when others then
          ie_tipo_convenio_w := 2;
  end;
  ie_tipo_convenio_atend_ww := ie_tipo_convenio_w;
  /* Tratamento passado para a trigger ATEND_CATEG_CONVENIO_UP_PRAGMA  */
  if	(ie_atualizar_tipo_conv_atend_w = 'S') then
    begin
    update	atendimento_paciente
    set	ie_tipo_convenio = ie_tipo_convenio_w
    where	nr_atendimento 	 = :new.nr_atendimento;
    exception
      when others then
            ie_tipo_convenio_w := 2;
    end;
  end if;

  select	dt_entrada,
    cd_estabelecimento,
    cd_medico_resp,
    ie_tipo_atendimento,
    ie_tipo_convenio,
    dt_cancelamento,
    dt_alta
  into	dt_entrada_w,
    cd_estabelecimento_w,
    cd_medico_resp_w,
    ie_tipo_atendimento_w,
    ie_tipo_convenio_atend_w,
    dt_cancelamento_atend_w,
    dt_alta_w
  from	atendimento_paciente
  where	nr_atendimento	= :new.nr_atendimento;


  select	nvl(max(ie_vigencia_entrada),'N')
  into	ie_vigencia_entrada_w
  from	parametro_atendimento
  where	cd_estabelecimento	= cd_estabelecimento_w;

  if	(ie_vigencia_entrada_w = 'N') and
    (dt_entrada_w > :new.dt_inicio_vigencia) then
    Wheb_mensagem_pck.exibir_mensagem_abort( 262347 , 'DT_INICIO='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio_vigencia, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_ENTRADA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
    ---20011,'A data de inicio de vigencia '||to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss')||' nao pode ser menor que a data de entrada '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss'));
  end if;

  ie_atualiza_validade_titular_w := obter_valor_param_usuario(916, 1006, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

  if	(ie_atualiza_validade_titular_w = 'S') and
    (:old.dt_validade_carteira <> :new.dt_validade_carteira) then
    update	pessoa_titular_convenio
    set		dt_validade_carteira = :new.dt_validade_carteira
    where	cd_convenio			 = :new.cd_convenio
    and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)	<= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_fim_vigencia)
    and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate) 	>= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_inicio_vigencia)
    and		cd_pessoa_fisica     = cd_pessoa_fisica_w;
  end if;

  select  decode(count(*), 0, 'N', 'S')
  into	ie_contem_convenio_w
  from	pessoa_titular_convenio
  where	cd_convenio = :new.cd_convenio;

  if	(ie_contem_convenio_w = 'S') and
    (:new.ie_tipo_conveniado is not null) then
    update	pessoa_titular_convenio
    set		ie_tipo_conveniado = :new.ie_tipo_conveniado
    where	cd_convenio = :new.cd_convenio
    and		cd_pessoa_fisica = cd_pessoa_fisica_w;
  end if;

  ie_entrada_vigencia_w := obter_valor_param_usuario(916, 637, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);


  if	(ie_entrada_vigencia_w = 'S') and /* contrario da consistencia acima */
    (dt_entrada_w < :new.dt_inicio_vigencia) then
    Wheb_mensagem_pck.exibir_mensagem_abort( 262357 , 'DT_ENTRADA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_VIGENCIA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio_vigencia, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
    --or(-20011,'A data de entrada '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss') || ' nao pode ser menor que a data de inicio de vigencia '||to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss'));
  end if;

  if	(:new.dt_inicio_vigencia > :new.dt_final_vigencia) then
    Wheb_mensagem_pck.exibir_mensagem_abort(262358);
  end if;

  select	Obter_convenio_regra_atend(cd_medico_resp_w,:new.cd_convenio,ie_tipo_atendimento_w,cd_estabelecimento_w,'A',:new.cd_plano_convenio,:new.cd_categoria)
  into	ie_regra_w
  from	dual;

  if	(ie_regra_w = 'N') then
    Wheb_mensagem_pck.exibir_mensagem_abort(262359);
  end if;


  if	(ie_consiste_tipo_conv_w = 'S') and
    (ie_tipo_convenio_atend_ww <> obter_tipo_convenio(:new.cd_convenio)) then /* Rafael em 18/08/06 OS39167*/
    Wheb_mensagem_pck.exibir_mensagem_abort(262360);
  end if;

  if	(ie_exige_orc_atend_w = 'S') then
    verifica_orcamento_paciente(:new.nr_atendimento,:new.cd_convenio,:new.cd_categoria);
  end if;

  if	(ie_situacao_w <> 'A') then
    Wheb_mensagem_pck.exibir_mensagem_abort(262361);
  end if;

  if	(dt_cancelamento_w is not null) and
    (dt_entrada_w >= dt_cancelamento_w) then
    Wheb_mensagem_pck.exibir_mensagem_abort(262363);
  end if;


  select	nvl(max(ie_situacao ),'X')
  into	ie_situacao_categ_w
  from	categoria_convenio
  where	cd_convenio = :new.cd_convenio
  and	cd_categoria = :new.cd_categoria;


  if	(ie_situacao_categ_w <> 'A') then
    Wheb_mensagem_pck.exibir_mensagem_abort(262364);
  end if;

  if	(Obter_Se_Medico_Lib(	cd_estabelecimento_w,
          cd_medico_resp_w,
          :new.cd_convenio)	= 'N') then
    Wheb_mensagem_pck.exibir_mensagem_abort(262359);
  end if;


  if	(:old.ie_tipo_guia <> :new.ie_tipo_guia) or
    (:old.ie_tipo_guia is null and :new.ie_tipo_guia is not null) or
    (:old.CD_USUARIO_CONVENIO <> :new.CD_USUARIO_CONVENIO) or
    (:old.CD_USUARIO_CONVENIO is null and :new.CD_USUARIO_CONVENIO is not null) or
    (:new.CD_USUARIO_CONVENIO is null and :old.CD_USUARIO_CONVENIO is not null) or
    (:old.CD_PLANO_CONVENIO <> :new.CD_PLANO_CONVENIO) or
    (:old.CD_PLANO_CONVENIO is null and :new.CD_PLANO_CONVENIO is not null) or
    (:new.CD_PLANO_CONVENIO is null and :old.CD_PLANO_CONVENIO is not null) then

    gerar_home_care_regra(	:new.nr_atendimento,
          ie_tipo_atendimento_w,
          :new.ie_tipo_guia,
          :new.nm_usuario,
          :new.cd_convenio,
          :new.cd_plano_convenio,
          :new.cd_usuario_convenio
          );
  end if;

  ie_save_insurance_holder_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'SI');

  if	(nvl(ie_save_insurance_holder_w,'N') = 'S') and
    (nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'pt_BR') and
    (cd_pessoa_fisica_w  is not null) and
    (:new.cd_convenio is not null) and
    ((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
    (nvl(:new.cd_categoria, 0) <> nvl(:old.cd_categoria, 0)) or
    (nvl(:new.cd_usuario_convenio, '0') <> nvl(:old.cd_usuario_convenio, '0'))) or
    (nvl(:new.cd_pessoa_titular, '0') <> nvl(:old.cd_pessoa_titular, '0'))
    then

    insere_atualiza_titular_conv(
          :new.nm_usuario,
          :new.cd_convenio,
          :new.cd_categoria,
          cd_pessoa_fisica_w,
          :new.cd_plano_convenio,
          :new.dt_inicio_vigencia,
          :new.dt_final_vigencia,
          :new.dt_final_vigencia,
          :new.cd_pessoa_titular,
          :new.cd_usuario_convenio,
          null,
          'N',
          '2');

  end if;

  if ( (:new.cd_convenio is not null) and
      (:new.cd_convenio <> :old.cd_convenio) and
      (:new.cd_usuario_convenio is not null)) then

    begin

    --  Gerar RES UNIMED - 000710 - Criacao do RES do beneficiario
    gerar_transacao_res(:new.nr_atendimento,'00710',obter_usuario_pessoa(cd_medico_resp_w),:new.cd_convenio,'','');

    --  Consultar documentos RES PHILIPS - 01100 - Busca dos documentos clinicos
    gerar_transacao_res(:new.nr_atendimento,'01100',obter_usuario_pessoa(cd_medico_resp_w),:new.cd_convenio,'','');


    exception
    when others then
          ie_resUnimed_w := 'N';
    end;

  end if;

  select	count(*)
  into	qt_exist_rule_w
  from	patient_class_category
  where	nvl(ie_situacao,'A') = 'A';

  if	(qt_exist_rule_w > 0) and
    (:old.nr_seq_patient_category <> :new.nr_seq_patient_category) then
    begin

    update	patient_category_log
    set		dt_end_category = sysdate,
        nm_usuario = :new.nm_usuario
    where	dt_start_category is not null
    and		dt_end_category is null
    and		nr_atendimento = :new.nr_atendimento;

    insert into patient_category_log (
            nr_sequencia,
            nr_seq_interno,
            nr_seq_patient_category_old,
            nr_seq_patient_category_new,
            dt_atualizacao,
            dt_start_category,
            nm_usuario,
            nr_atendimento)
    values 		 	(patient_category_log_seq.nextval,
            :new.nr_seq_interno,
            :old.nr_seq_patient_category,
            :new.nr_seq_patient_category,
            sysdate,
            sysdate,
            :new.nm_usuario,
            :new.nr_atendimento);


    end;
  end if;

  select	count(*)
  into	qt_registros_w
  from	atend_paciente_unidade
  where	nr_atendimento = :new.nr_atendimento;

  if	(dt_alta_w is null) and
    (dt_cancelamento_atend_w is null) and
    (qt_registros_w > 0) then
    begin
    intpd_enviar_atendimento(:new.nr_atendimento, 'A', '0', :new.nm_usuario);
    /*O parametro ie_controle_tag_p e usado para controlar se foi uma movimentacao de paciente.
      0 - Nao foi uma movimentacao
      1 - Foi uma movimentacao
    Neste caso, nao e uma insercao na movimentacao do paciente, entao e 0 */
    end;
  end if;

  if  	(dt_cancelamento_atend_w is null) and
    (qt_registros_w > 0) then
    call_bifrost_content('patient.information.update','encounter_json_pck.get_encounter_message_clob('||:new.nr_atendimento||')', :new.nm_usuario);

  end if;

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_CATEG_CONVENIO_BEFINS
BEFORE INSERT ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_tipo_convenio_atend_w	number(2);
cd_estabelecimento_w		Number(05,0);
ie_categ_lib_w			varchar2(1);
ie_conv_lib_w			varchar2(1);
ie_atualizar_tipo_conv_atend_w	varchar2(1) := 'S';
ie_atualiza_guia_princ_w	varchar2(1);
qt_crm_atendimentos_w		number(10);
ie_integracao_dynamics_w	varchar2(1);
ie_tipo_guia_regra_w		varchar(1);
ie_tipo_guia_atend_w		varchar(5);
cd_perfil_w					perfil.cd_perfil%type;
ie_cat_lib_pf_w			varchar2(1);
ie_entrada_vigencia_w 		varchar2(1);
dt_entrada_w			Date;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

select	nvl(max(cd_estabelecimento),wheb_usuario_pck.get_cd_estabelecimento),
	max(ie_tipo_convenio),
	wheb_usuario_pck.get_cd_perfil,
	max(dt_entrada)
into	cd_estabelecimento_w,
	ie_tipo_convenio_atend_w,
	cd_perfil_w,
	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;


Obter_param_Usuario(916, 637, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_entrada_vigencia_w);
Obter_param_Usuario(916, 688, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_atualiza_guia_princ_w);
Obter_param_Usuario(916, 1016, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_tipo_guia_regra_w);

if	(ie_entrada_vigencia_w = 'S') and
	(dt_entrada_w < :new.dt_inicio_vigencia) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262357 , 'DT_ENTRADA='||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')||';DT_VIGENCIA='||to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss'));
end if;

begin
select ie_tipo_convenio
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio = :new.cd_convenio;
exception
	when others then
      	ie_tipo_convenio_w := 2;
end;

if	(:new.nm_usuario_original is null) then
	:new.nm_usuario_original := :new.nm_usuario;
end if;

/*     Retiado o comando abaixo pois estava gernado DEADLOCK na base do cliente Richart*/
/*select	nvl(max(ie_atualizar_tipo_conv_atend),'S')
into	ie_atualizar_tipo_conv_atend_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_atualizar_tipo_conv_atend_w = 'S') then
	begin
		update	atendimento_paciente
		set	ie_tipo_convenio = ie_tipo_convenio_w
		where	nr_atendimento 	 = :new.nr_atendimento;
	exception
		when others then
	      	ie_tipo_convenio_w := 2;
	end;
end if;*/

/* Rafael em 30/04/2008 OS91546 */
select	obter_se_categoria_lib_estab(cd_estabelecimento_w,:new.cd_convenio,:new.cd_categoria)
into	ie_categ_lib_w
from	dual;

if	(ie_categ_lib_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(196844);
end if;

/* Fim Rafael em 30/04/2008 OS91546 */

/* Fabricio em 05/09/2008 OS 107386*/
select	nvl(max('S'),'N')
into	ie_conv_lib_w
from 	convenio a
where 	exists (select 	1
		from 	convenio_estabelecimento b
		where 	b.cd_estabelecimento = cd_estabelecimento_w
		and 	b.cd_convenio = a.cd_convenio)
and 	a.ie_situacao = 'A'
and 	a.cd_convenio = :new.cd_convenio;

if	(ie_conv_lib_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(196845);
end if;
/* Fim Fabricio em 05/09/2008 OS 107386*/

If	(ie_atualiza_guia_princ_w = 'S') then
	:new.nr_doc_conv_principal := :new.nr_doc_convenio;
end if;

select	nvl(max(ie_integracao_dynamics),'N')
into	ie_integracao_dynamics_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_w;

-- Atualiza tabela de integracao CRM_ATENDIMENTOS
if	(ie_integracao_dynamics_w = 'S') then
	select	count(*)
	into	qt_crm_atendimentos_w
	from	crm_atendimentos
	where	nr_atendimento = :new.nr_atendimento;

	if 	(qt_crm_atendimentos_w > 0) then
		update	crm_atendimentos
		set	ds_convenio 	= substr(obter_nome_convenio(:new.cd_convenio),1,255),
			ds_categoria 	= substr(obter_categoria_convenio(:new.cd_convenio,:new.cd_categoria),1,80),
			ds_plano_convenio = substr(Obter_Desc_Plano_Conv(:new.cd_convenio,:new.cd_plano_convenio),1,80),
			ie_status	= 2,
			dt_atualizacao	= sysdate,
			nm_usuario	= :new.nm_usuario
		where	nr_atendimento 	= :new.nr_atendimento;
	end if;
end if;

if	(ie_tipo_guia_regra_w = 'S') then
	select 	max(a.ie_tipo_guia)
	into 	ie_tipo_guia_atend_w
	from	(
		select	ie_tipo_guia
		from	tipo_guia_atend
		where 	ie_tipo_atendimento = (
						select	ie_tipo_atendimento
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento)
		and		((cd_estabelecimento = (
						select	cd_estabelecimento
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento))
				or	(cd_estabelecimento is null))
		and		((ie_clinica = (
						select	nvl(ie_clinica,0)
						from	atendimento_paciente
						where	nr_atendimento = :new.nr_atendimento))
				or 	(ie_clinica is null))
        order by  ie_guia_padrao desc) a
	where
	rownum <= 1;

	if ( ie_tipo_guia_atend_w <> '') or ( ie_tipo_guia_atend_w is not null) then
		:new.ie_tipo_guia := ie_tipo_guia_atend_w ;
	end if;
end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_categ_conv_update
before update ON TASY.ATEND_CATEGORIA_CONVENIO for each row
declare
ie_categ_lib_w		varchar2(1);
ie_conv_lib_w		varchar2(1);
qt_aih_w		Number(5);
qt_laudo_aih_w		Number(5);
ie_tipo_convenio_w	Number(2);
ie_troca_conv_at_aih_laudo_w	varchar2(15) := 'N';
qt_crm_atendimentos_w		number(10);
ie_integracao_dynamics_w	varchar2(1);

begin
/* Rafael em 30/4/8 OS91546 */
if	((:old.cd_categoria is null) and
	 (:new.cd_categoria is not null)) or
	((:old.cd_categoria is not null) and
	 (:new.cd_categoria is not null) and
	 (:old.cd_categoria <> :new.cd_categoria)) then

	select	obter_se_categoria_lib_estab(obter_estab_atend(:new.nr_atendimento),:new.cd_convenio,:new.cd_categoria)
	into	ie_categ_lib_w
	from	dual;

	if	(ie_categ_lib_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(206605);
		/*Esta categoria n�o est� liberada para o estabelecimento do atendimento, favor verificar.*/
	end if;

end if;
/* Fim Rafael em 30/4/8 OS91546 */


/* Fabr�cio em 05/09/2008 OS 107386*/
if	((:old.cd_convenio is null) and
	 (:new.cd_convenio is not null)) or
	((:old.cd_convenio is not null) and
	 (:new.cd_convenio is not null) and
	 (:old.cd_convenio <> :new.cd_convenio)) then

	 select	nvl(max('S'),'N')
	 into	ie_conv_lib_w
	 from 	convenio a
	 where 	exists (select 	1
			from 	convenio_estabelecimento b
			where 	b.cd_estabelecimento = obter_estab_atend(:new.nr_atendimento)
			and 	b.cd_convenio = a.cd_convenio)
	and 	a.ie_situacao = 'A'
	and 	a.cd_convenio = :new.cd_convenio;

	if	(ie_conv_lib_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(206609);
		/*Este conv�nio n�o est� liberado para o estabelecimento do atendimento, favor verificar.*/
	end if;

end if;
/* Fim Fabr�cio em 05/09/2008 OS 107386*/

ie_tipo_convenio_w	:= obter_tipo_convenio(:old.cd_convenio);

if      (ie_tipo_convenio_w = 3) then
	begin
	/*Andr� 05/11/2008 OS115792*/
	select  count(*)
	into    qt_aih_w
	from    sus_aih_unif
	where   nr_atendimento  = :new.nr_atendimento;

	select  count(*)
	into    qt_laudo_aih_w
	from    sus_laudo_paciente
	where   nr_atendimento  = :new.nr_atendimento;

	begin
	select	nvl(max(ie_troca_conv_atend_aih_laudo),'N')
	into	ie_troca_conv_at_aih_laudo_w
	from	parametro_faturamento
	where	cd_estabelecimento = obter_estab_atend(:new.nr_atendimento);
	exception
	when others then
		ie_troca_conv_at_aih_laudo_w := 'N';
	end;

	if	(:old.cd_convenio <> :new.cd_convenio) and
	        ((qt_aih_w > 0) or
	        (qt_laudo_aih_w > 0)) and
		(ie_troca_conv_at_aih_laudo_w = 'N') then
	        begin
		wheb_mensagem_pck.exibir_mensagem_abort(206610);
	        /*N�o � permitido alterar o conv�nio de um atendimento SUS se o mesmo possuir AIH ou laudo!*/
	        end;
	end if;
	end;
end if;
/*Andr� 05/11/2008 OS115792*/

select	nvl(max(ie_integracao_dynamics),'N')
into	ie_integracao_dynamics_w
from	parametro_atendimento
where	cd_estabelecimento = obter_estab_atend(:new.nr_atendimento);

-- Atualiza tabela de integra��o CRM_ATENDIMENTOS
if	(ie_integracao_dynamics_w = 'S') then
	select	count(*)
	into	qt_crm_atendimentos_w
	from	crm_atendimentos
	where	nr_atendimento = :new.nr_atendimento;

	if 	(qt_crm_atendimentos_w > 0) and
		((:old.cd_convenio <> :new.cd_convenio) or
		(:old.cd_categoria <> :new.cd_categoria) or
		(:old.cd_plano_convenio <> :new.cd_plano_convenio)) then
		update	crm_atendimentos
		set	ds_convenio 	= substr(obter_nome_convenio(:new.cd_convenio),1,255),
			ds_categoria 	= substr(obter_categoria_convenio(:new.cd_convenio,:new.cd_categoria),1,80),
			ds_plano_convenio = substr(Obter_Desc_Plano_Conv(:new.cd_convenio,:new.cd_plano_convenio),1,80),
			ie_status	= 2,
			dt_atualizacao	= sysdate,
			nm_usuario	= :new.nm_usuario
		where	nr_atendimento 	= :new.nr_atendimento;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Atend_Categ_Convenio_BefInsert
BEFORE INSERT ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_plano_w				number(1);
ie_regulacao_gerint_w	parametro_atendimento.ie_regulacao_gerint%type;
nr_cerih_w				gerint_solic_internacao.nr_protocolo_solicitacao%type;

BEGIN

if ('00/00/0000 00:00:00' = to_char(:new.dt_validade_carteira,'dd/mm/yyyy hh24:mi:ss')) then
	:new.dt_validade_carteira := null;
end if;

select nvl(max(1), 0)
into ie_plano_w
from convenio_plano
where cd_plano = :new.cd_plano_convenio;

if (ie_plano_w = 0) then
	:new.cd_plano_convenio := null;
end if;

select	nvl(max(ie_regulacao_gerint),'N')
into	ie_regulacao_gerint_w
from	parametro_atendimento
where	cd_estabelecimento	= (	select	max(cd_estabelecimento)
								from	atendimento_paciente
								where 	nr_atendimento =  :new.nr_atendimento);

if (ie_regulacao_gerint_w = 'S') then
	select	max(nr_protocolo_solicitacao)
	into	nr_cerih_w
	from	gerint_solic_internacao
	where	nr_atendimento = :new.nr_atendimento;

	if (nr_cerih_w is not null) and (:new.cd_senha is null) then
		:new.cd_senha := nr_cerih_w;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_categ_conv_delete_taxa
after delete ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

pragma autonomous_transaction;

BEGIN

delete from pessoa_fisica_taxa
where nr_atendimento = :old.nr_atendimento
and nr_seq_atecaco is null;

commit;

END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_CATEGORIA_CONVENIO_tp  after update ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@CD_CONVENIO='||to_char(:old.CD_CONVENIO)||'#@#@CD_CATEGORIA='||to_char(:old.CD_CATEGORIA)||'#@#@DT_INICIO_VIGENCIA='||to_char(:old.DT_INICIO_VIGENCIA);gravar_log_alteracao(substr(:old.NR_DOC_CONVENIO,1,4000),substr(:new.NR_DOC_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOC_CONVENIO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_ACOMODACAO,1,4000),substr(:new.CD_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ACOMODACAO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SENHA,1,4000),substr(:new.CD_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SENHA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PLANO_GLOSA,1,4000),substr(:new.CD_PLANO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO_GLOSA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PLANO_CONVENIO,1,4000),substr(:new.CD_PLANO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO_CONVENIO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_USUARIO_CONVENIO,1,4000),substr(:new.CD_USUARIO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_USUARIO_CONVENIO',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CARTEIRA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CARTEIRA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CARTEIRA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SENHA_PROVISORIA,1,4000),substr(:new.CD_SENHA_PROVISORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SENHA_PROVISORIA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'ATEND_CATEGORIA_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Atend_Categ_Convenio_BefUpdate
BEFORE UPDATE ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

ie_permite_guia_senha_atend_w	varchar2(1);
ie_plano_w			number(1);
ie_entrada_vigencia_w 		varchar2(1);
dt_entrada_w			Date;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

select	max(dt_entrada)
into	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

Obter_param_Usuario(916, 637, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_entrada_vigencia_w);
Obter_Param_Usuario(916, 1018, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_permite_guia_senha_atend_w);

if	(ie_entrada_vigencia_w = 'S') and
	(dt_entrada_w < :new.dt_inicio_vigencia) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262357 , 'DT_ENTRADA='||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')||';DT_VIGENCIA='||to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss'));
end if;

if	(ie_permite_guia_senha_atend_w = 'N') and
	((:new.nr_doc_convenio is not null) or
	(:new.cd_senha is not null)) then
	if	(to_char(:new.nr_atendimento) = :new.nr_doc_convenio) then
		Wheb_mensagem_pck.exibir_mensagem_abort(223884);
	end if;

	if	(to_char(:new.nr_atendimento) = :new.cd_senha) then
		Wheb_mensagem_pck.exibir_mensagem_abort(223885);
	end if;
end if;

if ('00/00/0000 00:00:00' = to_char(:new.dt_validade_carteira,'dd/mm/yyyy hh24:mi:ss')) then
	:new.dt_validade_carteira := null;
end if;

if (:old.IE_LIB_DIETA <> :new.IE_LIB_DIETA) then
	:new.dt_atualizacao := sysdate;
	:new.nm_usuario := wheb_usuario_pck.get_nm_usuario;
end if;

select 	nvl(max(1), 0)
into 	ie_plano_w
from 	convenio_plano
where 	cd_plano = :new.cd_plano_convenio;

if (ie_plano_w = 0) then
	:new.cd_plano_convenio := null;
end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Atend_Categ_Convenio_Insert
AFTER INSERT ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

ie_tipo_convenio_w		number(2) := 2;
ie_tipo_convenio_ww		number(2) := 2;
ie_tipo_convenio_atend_w	number(2);
ie_situacao_w			varchar2(1) := 'A';
dt_cancelamento_w		date := sysdate;
cd_estabelecimento_w		Number(05,0);
nr_seq_regra_funcao_w		Number(10,0);
nr_seq_regra_funcao_ww		Number(10,0);
ie_situacao_categ_w		varchar2(1) := 'A';
ie_exige_orc_atend_w		varchar2(1);
dt_entrada_w			Date;
cd_medico_resp_w		varchar2(10);
ie_tipo_atendimento_w		number(3);
ie_regra_w			varchar2(1);
ie_consiste_tipo_conv_w		varchar2(1);
ie_atualizar_tipo_conv_atend_w	varchar2(1) := 'S';
ie_vigencia_entrada_w		varchar2(1);
cd_pessoa_fisica_w		varchar2(10);
ie_atualiza_titular_conv_w	varchar2(1);
ie_resUnimed_w			varchar2(1);
ds_param_integ_hl7_w 		varchar2(4000);
cd_convenio_empresa_w 		convenio_empresa.cd_convenio%type;
ie_save_insurance_holder_w	varchar2(1);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

  select	max(cd_estabelecimento)
  into	cd_estabelecimento_w
  from	atendimento_paciente
  where 	nr_atendimento =  :new.nr_atendimento;

  select	nvl(max(ie_consiste_tipo_conv),'N'), /* Rafael em 18/08/06 OS39167*/
    nvl(max(ie_atualizar_tipo_conv_atend),'S')
  into	ie_consiste_tipo_conv_w,
    ie_atualizar_tipo_conv_atend_w
  from	parametro_faturamento
  where	cd_estabelecimento = cd_estabelecimento_w;


  begin
  select ie_tipo_convenio,
    ie_situacao,
    dt_cancelamento,
    Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_w, 'IE_EXIGE_ORC_ATEND') ie_exige_orc_atend
  into	ie_tipo_convenio_w,
    ie_situacao_w,
    dt_cancelamento_w,
    ie_exige_orc_atend_w
  from	convenio
  where	cd_convenio = :new.cd_convenio;
  exception
    when others then
          ie_tipo_convenio_w := 2;
  end;
  ie_tipo_convenio_ww	:= ie_tipo_convenio_w;
  if	(ie_atualizar_tipo_conv_atend_w = 'S') then
    begin
    update	atendimento_paciente
    set	ie_tipo_convenio = ie_tipo_convenio_w
    where	nr_atendimento 	 = :new.nr_atendimento;
    exception
      when others then
            ie_tipo_convenio_w := 2;
    end;
  end if;

  select	nvl(max(nr_seq_regra_funcao),0),
    max(dt_entrada),
    max(cd_medico_resp),
    max(ie_tipo_atendimento),
    max(ie_tipo_convenio),
    max(cd_pessoa_fisica)
  into	nr_seq_regra_funcao_w,
    dt_entrada_w,
    cd_medico_resp_w,
    ie_tipo_atendimento_w,
    ie_tipo_convenio_atend_w,
    cd_pessoa_fisica_w
  from	atendimento_paciente
  where	nr_atendimento	= :new.nr_atendimento;


  if	(ie_consiste_tipo_conv_w = 'S') and
    (obter_tipo_convenio(:new.cd_convenio) <> ie_tipo_convenio_ww) then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237344);
  end if;

  if	(:new.ie_tipo_guia is not null) then

    gerar_home_care_regra(	:new.nr_atendimento,
          ie_tipo_atendimento_w,
          :new.ie_tipo_guia,
          :new.nm_usuario,
          :new.cd_convenio,
          :new.cd_plano_convenio,
          :new.cd_usuario_convenio);

  end if;

  select	Obter_convenio_regra_atend(cd_medico_resp_w, :new.cd_convenio,ie_tipo_atendimento_w,cd_estabelecimento_w,'A',:new.cd_plano_convenio,:new.cd_categoria)
  into	ie_regra_w
  from	dual;

  if	(ie_regra_w = 'N') then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237337);
  end if;

  select	nvl(max(nr_seq_regra_funcao),0)
  into	nr_seq_regra_funcao_ww
  from	convenio_estabelecimento
  where	cd_estabelecimento	= cd_estabelecimento_w
  and	cd_convenio		= :new.cd_convenio;
  if	(nr_seq_regra_funcao_ww > 0) and
    (nr_seq_regra_funcao_ww <> nr_seq_regra_funcao_w)then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237338);
  end if;

  select	nvl(max(ie_vigencia_entrada),'N')
  into	ie_vigencia_entrada_w
  from	parametro_atendimento
  where	cd_estabelecimento	= cd_estabelecimento_w;

  if	(ie_vigencia_entrada_w = 'N') and
    (dt_entrada_w > :new.dt_inicio_vigencia) then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237339,'DT_INICIO_VIGENCIA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio_vigencia, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_ENTRADA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
  end if;

  if	(:new.dt_inicio_vigencia > :new.dt_final_vigencia) then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237343);
  end if;


  if	(ie_exige_orc_atend_w = 'S') then
    verifica_orcamento_paciente(:new.nr_atendimento,:new.cd_convenio,:new.cd_categoria);
  end if;

  if	(ie_situacao_w <> 'A') then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237345);
  end if;

  if	(dt_cancelamento_w is not null) and
    (dt_entrada_w >= dt_cancelamento_w) then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237346);
  end if;

  select	nvl(max(ie_situacao ),'X')
  into	ie_situacao_categ_w
  from	categoria_convenio
  where	cd_convenio = :new.cd_convenio
  and	cd_categoria = :new.cd_categoria;

  if	(ie_situacao_categ_w <> 'A') then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237347);
  end if;


  if	(Obter_Se_Medico_Lib(	cd_estabelecimento_w,
          cd_medico_resp_w,
          :new.cd_convenio)	= 'N') then
    WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(237348);
  end if;

  gerar_regra_prontuario_conv(:new.nr_atendimento,:new.nm_usuario,:new.cd_convenio,:new.cd_municipio_convenio);

  ie_save_insurance_holder_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'SI');

  if	(nvl(ie_save_insurance_holder_w,'N') = 'S') and
    (nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'pt_BR') and
    (cd_pessoa_fisica_w  is not null) and
    (:new.cd_convenio is not null) and
    ((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
    (nvl(:new.cd_categoria, 0) <> nvl(:old.cd_categoria, 0)) or
    (nvl(:new.cd_usuario_convenio, '0') <> nvl(:old.cd_usuario_convenio, '0')))
    then
    insere_atualiza_titular_conv(
          :new.nm_usuario,
          :new.cd_convenio,
          :new.cd_categoria,
          cd_pessoa_fisica_w,
          :new.cd_plano_convenio,
          :new.dt_inicio_vigencia,
          :new.dt_final_vigencia,
          :new.dt_final_vigencia,
          :new.cd_pessoa_titular,
          :new.cd_usuario_convenio,
          null,
          'N',
          '2');
  else
    obter_param_usuario(916, 842, Obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_atualiza_titular_conv_w);
    if	(nvl(ie_atualiza_titular_conv_w,'N') <> 'N') then

      if (nvl(pkg_i18n.get_user_locale, 'pt_BR') not in ('de_DE', 'de_AT')) then
        atualiza_titular_convenio(:new.nm_usuario,:new.cd_convenio,:new.cd_categoria,cd_pessoa_fisica_w,
              :new.cd_plano_convenio,:new.dt_inicio_vigencia,:new.dt_final_vigencia,:new.dt_validade_carteira,:new.cd_usuario_convenio, :new.ie_tipo_conveniado);
      else
        if  (existe_atend_controle(:new.nr_atendimento,0) = 'N') then
          cd_convenio_empresa_w	:= nvl(obter_convenio_tipo_pj(:new.nr_atendimento), 0);
          ie_tipo_convenio_w := obter_tipo_convenio(:new.cd_convenio);

          if (cd_convenio_empresa_w <> :new.cd_convenio
            and ie_tipo_convenio_w <> 1) then -- Self Payment
            atualiza_titular_convenio(:new.nm_usuario,:new.cd_convenio,:new.cd_categoria,cd_pessoa_fisica_w,
            :new.cd_plano_convenio,:new.dt_inicio_vigencia,:new.dt_final_vigencia,:new.dt_validade_carteira,:new.cd_usuario_convenio, :new.ie_tipo_conveniado);
          end if;
        end if;
      end if;
    end if;
  end if;


  if (:new.cd_convenio is not null and :new.cd_usuario_convenio is not null) then
    begin
    --  Gerar RES UNIMED - 000710 - Criacao do RES do beneficiario
    gerar_transacao_res(:new.nr_atendimento,'00710',obter_usuario_pessoa(cd_medico_resp_w),:new.cd_convenio,'','');
    --  Consultar documentos RES PHILIPS - 01100 - Busca dos documentos clinicos
    gerar_transacao_res(:new.nr_atendimento,'01100',obter_usuario_pessoa(cd_medico_resp_w),:new.cd_convenio,'','');

    exception
    when others then
          ie_resUnimed_w := 'N';
    end;

  end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Atend_Categ_Convenio_Delete
before delete ON TASY.ATEND_CATEGORIA_CONVENIO FOR EACH ROW
DECLARE

BEGIN

insert	into	log_exclusao	(
				nm_tabela,
				dt_atualizacao,
				nm_usuario,
				ds_chave
				)
			values	(
				'ATEND_CATEGORIA_CONVENIO',
				sysdate,
				:old.nm_usuario,
				substr('Cascade atendimento_paciente:'									||
				' NR_ATENDIMENTO= '	|| :old.nr_atendimento							||
				' CD_CONVENIO= ' 	|| :old.cd_convenio								||
				' CD_CATEGORIA= '	|| :old.cd_categoria								||
				' DT_INICIO_VIGENCIA= ' || to_char(:old.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss')	||
				' DS_CONVENIO= '	|| substr(obter_nome_convenio(:old.cd_convenio),1,40),1,255)
				);
END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_CATEGORIA_CONV_ISH_ATUAL
after insert or update or delete ON TASY.ATEND_CATEGORIA_CONVENIO for each row
declare
reg_integracao_p	gerar_int_padrao.reg_integracao;
nr_sequencia_w		Varchar2(35);
ie_gravar_w		varchar2(1);
qt_eventos_w		Number(10)	:= 0;
ie_inserindo_conv_w	number(10)	:= 0;

begin

/* Verificar se existe cadastrado pelo menos um evento 135 - Enviar dados do conv�nio do atendimento do paciente */
begin
select	1
into	qt_eventos_w
from	intpd_eventos
where	ie_evento	= '135'
and	ie_situacao	= 'A'
and	rownum		= 1;
exception
when others then
	qt_eventos_w	:= 0;
end;

select	count(*)
into	ie_inserindo_conv_w
from	v$session
where	audsid		= (select userenv('sessionid') from dual)
and	upper(action) 	= 'INTPD_INSERIR_ATEND_CONVENIO';

if	(qt_eventos_w > 0) and
	(ie_inserindo_conv_w = 0) then

	if	(inserting) then

		reg_integracao_p.ie_operacao		:= 'I';
		reg_integracao_p.nr_seq_agrupador	:= :new.nr_atendimento;
		reg_integracao_p.ie_status		:= 'A';
		nr_sequencia_w				:= :new.nr_seq_interno;
		gerar_int_padrao.gravar_integracao('135', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);

	elsif	(updating) then

		nr_sequencia_w				:= :new.nr_seq_interno;
		reg_integracao_p.nr_seq_agrupador	:= :new.nr_atendimento;
		reg_integracao_p.ie_status		:= 'A';
		reg_integracao_p.ie_operacao	:= 'A';

		ie_gravar_w	:= 'S';

		begin --verificar se o item j� n�o est� como pendente na operacao 'I - insert'
		select	'N'
		into	ie_gravar_w
		from	intpd_fila_transmissao
		where	nr_seq_documento	= :new.nr_seq_interno
		and	ie_evento	= '135'
		and	ie_status		in ('A','P')
		and	ie_operacao		= 'I'
		and	rownum	= 1;
		exception
		when others then
			ie_gravar_w	:= 'S';
		end;

		if	(ie_gravar_w = 'S') then
			gerar_int_padrao.gravar_integracao('135', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);
		end if;

	elsif	(deleting) then

		reg_integracao_p.ie_operacao		:= 'E';
		reg_integracao_p.ie_status		:= 'A';
		reg_integracao_p.nr_seq_agrupador	:= :old.nr_atendimento;
		nr_sequencia_w				:= :old.nr_seq_interno;
		gerar_int_padrao.gravar_integracao('135', nr_sequencia_w, :old.nm_usuario, reg_integracao_p);

	end if;
end if;

end ATEND_CATEGORIA_CONV_ISH_ATUAL;
/


ALTER TABLE TASY.ATEND_CATEGORIA_CONVENIO ADD (
  CONSTRAINT ATECACO_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          72M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT ATECACO_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_CATEGORIA_CONVENIO ADD (
  CONSTRAINT ATECACO_EKVKTPDOC_FK 
 FOREIGN KEY (EKVK_NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.EKVK_TIPO_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT ATECACO_CATIVA_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA_IVA) 
 REFERENCES TASY.CATEGORIA_IVA (NR_SEQUENCIA),
  CONSTRAINT ATECACO_COCATSEG_FK 
 FOREIGN KEY (NR_SEQ_CONV_CATEG_SEG) 
 REFERENCES TASY.CONV_CATEGORIA_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT ATECACO_PATCATE_FK 
 FOREIGN KEY (NR_SEQ_PATIENT_CATEGORY) 
 REFERENCES TASY.PATIENT_CATEGORY (NR_SEQUENCIA),
  CONSTRAINT ATECACO_ATJBICV_FK 
 FOREIGN KEY (NR_SEQ_JUST_BIOMETRIA_CONV) 
 REFERENCES TASY.ATEND_JUST_BIOMETRIA_CONV (NR_SEQUENCIA),
  CONSTRAINT ATECACO_NUTRECA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ACOMP) 
 REFERENCES TASY.NUT_REGRA_CONSISTE_ACOMP (NR_SEQUENCIA),
  CONSTRAINT ATECACO_ABRAPLA_FK 
 FOREIGN KEY (NR_SEQ_ABRANGENCIA) 
 REFERENCES TASY.ABRANGENCIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT ATECACO_CATCONV_FK2 
 FOREIGN KEY (CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT ATECACO_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT ATECACO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATECACO_CONORUS_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.CONVENIO_ORIGEM_USUARIO (NR_SEQUENCIA),
  CONSTRAINT ATECACO_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT ATECACO_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT ATECACO_TILIDIC_FK 
 FOREIGN KEY (NR_SEQ_LIB_DIETA_CONV) 
 REFERENCES TASY.TIPO_LIBERACAO_DIETA_CONV (NR_SEQUENCIA),
  CONSTRAINT ATECACO_TILIGUE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LIB_GUIA) 
 REFERENCES TASY.TIPO_LIB_GUIA_ENTRADA (NR_SEQUENCIA),
  CONSTRAINT ATECACO_CONVCOB_FK 
 FOREIGN KEY (NR_SEQ_COBERTURA) 
 REFERENCES TASY.CONVENIO_COBERTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_CATEGORIA_CONVENIO TO NIVEL_1;

