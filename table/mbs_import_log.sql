DROP TABLE TASY.MBS_IMPORT_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.MBS_IMPORT_LOG
(
  DT_START   DATE,
  DT_END     DATE,
  IE_STATUS  VARCHAR2(10 BYTE),
  DS_ERROR   VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.MBS_IMPORT_LOG TO NIVEL_1;

