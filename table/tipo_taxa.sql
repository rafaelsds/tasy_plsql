ALTER TABLE TASY.TIPO_TAXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_TAXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_TAXA
(
  CD_TIPO_TAXA         NUMBER(10)               NOT NULL,
  DS_TIPO_TAXA         VARCHAR2(50 BYTE)        NOT NULL,
  IE_TIPO_TAXA         VARCHAR2(1 BYTE)         NOT NULL,
  IE_PRE_POS_FIXADO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_FORMA_CALCULO     VARCHAR2(1 BYTE),
  NR_DIAS_LIVRE        NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TIPTAXA_PK ON TASY.TIPO_TAXA
(CD_TIPO_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TIPO_TAXA_DELETE
BEFORE DELETE ON TASY.TIPO_TAXA FOR EACH ROW
DECLARE

cont_w	number(10,0);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	count(*)
into	cont_w
from	titulo_pagar
where	cd_tipo_taxa_juro	= :old.cd_tipo_taxa
or	cd_tipo_taxa_multa	= :old.cd_tipo_taxa;

if	(cont_w > 0) then
	/*Nao e possivel excluir este tipo de taxa!
	Ja existem titulos gerados com este tipo de taxa!*/
	wheb_mensagem_pck.exibir_mensagem_abort(267027);
end if;

select	count(*)
into	cont_w
from	titulo_receber
where	cd_tipo_taxa_juro	= :old.cd_tipo_taxa
or	cd_tipo_taxa_multa	= :old.cd_tipo_taxa;

if	(cont_w > 0) then
	/*Nao e possivel excluir este tipo de taxa!
	Ja existem titulos gerados com este tipo de taxa!*/
	wheb_mensagem_pck.exibir_mensagem_abort(267027);
end if;
end if;

end;
/


ALTER TABLE TASY.TIPO_TAXA ADD (
  CONSTRAINT TIPTAXA_PK
 PRIMARY KEY
 (CD_TIPO_TAXA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TIPO_TAXA TO NIVEL_1;

