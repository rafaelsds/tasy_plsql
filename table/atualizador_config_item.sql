ALTER TABLE TASY.ATUALIZADOR_CONFIG_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATUALIZADOR_CONFIG_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATUALIZADOR_CONFIG_ITEM
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_ATUALIZADOR  NUMBER(10)                NOT NULL,
  CD_CONFIGURACAO     VARCHAR2(15 BYTE)         NOT NULL,
  VL_CONFIGURACAO     VARCHAR2(255 BYTE),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATCOITE_ATUCONF_FK_I ON TASY.ATUALIZADOR_CONFIG_ITEM
(NR_SEQ_ATUALIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATCOITE_ATUCONF_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATCOITE_PK ON TASY.ATUALIZADOR_CONFIG_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATUALIZADOR_CONFIG_ITEM ADD (
  CONSTRAINT ATCOITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATUALIZADOR_CONFIG_ITEM ADD (
  CONSTRAINT ATCOITE_ATUCONF_FK 
 FOREIGN KEY (NR_SEQ_ATUALIZADOR) 
 REFERENCES TASY.ATUALIZADOR_CONFIG (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATUALIZADOR_CONFIG_ITEM TO NIVEL_1;

