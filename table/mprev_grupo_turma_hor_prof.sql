ALTER TABLE TASY.MPREV_GRUPO_TURMA_HOR_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_GRUPO_TURMA_HOR_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_GRUPO_TURMA_HOR_PROF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_HORARIO       NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  CD_AGENDA            NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRGTHP_AGENDA_FK_I ON TASY.MPREV_GRUPO_TURMA_HOR_PROF
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTHP_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRGTHP_MPRGCTH_FK_I ON TASY.MPREV_GRUPO_TURMA_HOR_PROF
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTHP_MPRGCTH_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRGTHP_PESFISI_FK_I ON TASY.MPREV_GRUPO_TURMA_HOR_PROF
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRGTHP_PK ON TASY.MPREV_GRUPO_TURMA_HOR_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTHP_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPRGTHP_UK ON TASY.MPREV_GRUPO_TURMA_HOR_PROF
(NR_SEQ_HORARIO, CD_PESSOA_FISICA, CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_GRUPO_TURMA_HOR_PROF ADD (
  CONSTRAINT MPRGTHP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT MPRGTHP_UK
 UNIQUE (NR_SEQ_HORARIO, CD_PESSOA_FISICA, CD_AGENDA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_GRUPO_TURMA_HOR_PROF ADD (
  CONSTRAINT MPRGTHP_MPRGCTH_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRGTHP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MPRGTHP_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA));

GRANT SELECT ON TASY.MPREV_GRUPO_TURMA_HOR_PROF TO NIVEL_1;

