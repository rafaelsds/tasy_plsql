ALTER TABLE TASY.AGEINT_QUEST_PERGUNTAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_QUEST_PERGUNTAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_QUEST_PERGUNTAS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_PERGUNTA          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGIQPER_PK ON TASY.AGEINT_QUEST_PERGUNTAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ageint_quest_perguntas_atual
BEFORE UPDATE ON TASY.AGEINT_QUEST_PERGUNTAS FOR EACH ROW
DECLARE

qt_reg_w	number(10);


BEGIN

	if :new.ie_situacao <> 'A' then

		select	count(1)
		into 		qt_reg_w
		from		ageint_quest_estrutura
		where 	nr_seq_pergunta = :new.nr_sequencia
		and 		ie_situacao = 'A';

		if qt_reg_w > 0 then

			wheb_mensagem_pck.Exibir_Mensagem_Abort(1085539);

		end if;

	end if;

END;
/


ALTER TABLE TASY.AGEINT_QUEST_PERGUNTAS ADD (
  CONSTRAINT AGIQPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AGEINT_QUEST_PERGUNTAS TO NIVEL_1;

