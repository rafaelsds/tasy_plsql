ALTER TABLE TASY.APROV_AUDIT_SUBSTITUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APROV_AUDIT_SUBSTITUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.APROV_AUDIT_SUBSTITUTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_CARGO              NUMBER(10),
  CD_PESSOA_SUBSTITUTA  VARCHAR2(10 BYTE)       NOT NULL,
  DT_VIGENCIA_FINAL     DATE                    NOT NULL,
  DT_VIGENCIA_INICIAL   DATE                    NOT NULL,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APRAUDSUB_CARGO_FK_I ON TASY.APROV_AUDIT_SUBSTITUTO
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APRAUDSUB_PESFISI_FK_I ON TASY.APROV_AUDIT_SUBSTITUTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APRAUDSUB_PESFISI_FKI_I ON TASY.APROV_AUDIT_SUBSTITUTO
(CD_PESSOA_SUBSTITUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APRAUDSUB_PK ON TASY.APROV_AUDIT_SUBSTITUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.audit_substituto_afterdelete
after delete ON TASY.APROV_AUDIT_SUBSTITUTO for each row
declare

ds_log_w	varchar2(4000);

begin
/*ds_log_w 	:= substr('Exclu�do substituto abaixo.' || chr(13) || chr(10) || chr(13) || chr(10) ||
			'PESSOA F�SICA: ' || :old.cd_pessoa_fisica || ' - ' || obter_nome_pf(:old.cd_pessoa_fisica) || chr(13) || chr(10) ||
			'CARGO: ' || :old.cd_cargo || ' - ' || obter_desc_cargo(:old.cd_cargo) || chr(13) || chr(10) ||
			'SUBSTITUTO: ' || :old.cd_pessoa_substituta || ' - ' || obter_nome_pf(:old.cd_pessoa_substituta) || chr(13) || chr(10) ||
			'DT. LIMITE: ' || :old.dt_limite,1,4000);*/
ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(313837) || chr(13) || chr(10) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313838, 'CD_PESSOA_FISICA=' || :old.cd_pessoa_fisica || ';' ||
									  'NM_PESSOA_FISICA=' || substr(obter_nome_pf(:old.cd_pessoa_fisica),1,255)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313839, 'CD_CARGO=' || :old.cd_cargo || ';' ||
								  'DS_CARGO=' || obter_desc_cargo(:old.cd_cargo)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313840, 'CD_PESSOA_SUBSTITUTA=' || :old.cd_pessoa_substituta || ';' ||
								  'NM_PESSOA_SUBSTITUTA=' || substr(obter_nome_pf(:old.cd_pessoa_substituta),1,255)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(455374, 'DT_VIGENCIA_INICIAL=' || :old.dt_vigencia_inicial) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(455376, 'DT_VIGENCIA_FINAL=' || :old.dt_vigencia_final),1,4000);

insert into aprov_audit_substituto_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log)
values(	spa_substituto_log_seq.nextval,
	sysdate,
	:old.nm_usuario,
	sysdate,
	:old.nm_usuario,
	--'Exclus�o substituto',
	wheb_mensagem_pck.get_texto(313836),
	ds_log_w);

end;
/


CREATE OR REPLACE TRIGGER TASY.audit_substituto_afterinsert
after insert ON TASY.APROV_AUDIT_SUBSTITUTO for each row
declare

ds_log_w	varchar2(4000);

begin
/*ds_log_w 	:= substr('Inclu�do substituto abaixo.' || chr(13) || chr(10) || chr(13) || chr(10) ||
			'PESSOA F�SICA: ' || :new.cd_pessoa_fisica || ' - ' || obter_nome_pf(:new.cd_pessoa_fisica) || chr(13) || chr(10) ||
			'CARGO: ' || :new.cd_cargo || ' - ' || obter_desc_cargo(:new.cd_cargo) || chr(13) || chr(10) ||
			'SUBSTITUTO: ' || :new.cd_pessoa_substituta || ' - ' || obter_nome_pf(:new.cd_pessoa_substituta) || chr(13) || chr(10) ||
			'DT. LIMITE: ' || :new.dt_limite,1,4000);*/
ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(313844) || chr(13) || chr(10) || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(313838, 'CD_PESSOA_FISICA=' || :new.cd_pessoa_fisica || ';' ||
			    				      'NM_PESSOA_FISICA=' || substr(obter_nome_pf(:new.cd_pessoa_fisica),1,255)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313839, 'CD_CARGO=' || :new.cd_cargo || ';' ||
							      'DS_CARGO=' || obter_desc_cargo(:new.cd_cargo)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313840, 'CD_PESSOA_SUBSTITUTA=' || :new.cd_pessoa_substituta || ';' ||
							      'NM_PESSOA_SUBSTITUTA=' || substr(obter_nome_pf(:new.cd_pessoa_substituta),1,255)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(455374, 'DT_VIGENCIA_INICIAL=' || :new.dt_vigencia_inicial) || chr(13) || chr(10) ||
              wheb_mensagem_pck.get_texto(455376, 'DT_VIGENCIA_FINAL=' || :new.dt_vigencia_final),1,4000);

insert into aprov_audit_substituto_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log)
values(	spa_substituto_log_seq.nextval,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	--'Inclus�o substituto',
	wheb_mensagem_pck.get_texto(313843),
	ds_log_w);

end;
/


CREATE OR REPLACE TRIGGER TASY.APROV_AUDIT_SUBSTITUTO_tp  after update ON TASY.APROV_AUDIT_SUBSTITUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_SUBSTITUTA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_SUBSTITUTA,1,4000),substr(:new.CD_PESSOA_SUBSTITUTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SUBSTITUTA',ie_log_w,ds_w,'APROV_AUDIT_SUBSTITUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'APROV_AUDIT_SUBSTITUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'APROV_AUDIT_SUBSTITUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.APROV_AUDIT_SUBSTITUTO ADD (
  CONSTRAINT APRAUDSUB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.APROV_AUDIT_SUBSTITUTO ADD (
  CONSTRAINT APRAUDSUB_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT APRAUDSUB_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT APRAUDSUB_PESFISI_FKI 
 FOREIGN KEY (CD_PESSOA_SUBSTITUTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.APROV_AUDIT_SUBSTITUTO TO NIVEL_1;

