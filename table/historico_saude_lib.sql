ALTER TABLE TASY.HISTORICO_SAUDE_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HISTORICO_SAUDE_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.HISTORICO_SAUDE_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_HIST_SAUDE     NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PERFIL             NUMBER(5),
  CD_ESPECIALIDADE      NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HISALIB_ESPMEDI_FK_I ON TASY.HISTORICO_SAUDE_LIB
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISALIB_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISALIB_ESTABEL_FK_I ON TASY.HISTORICO_SAUDE_LIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISALIB_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISALIB_HISTSAU_FK_I ON TASY.HISTORICO_SAUDE_LIB
(NR_SEQ_HIST_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISALIB_HISTSAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISALIB_PERFIL_FK_I ON TASY.HISTORICO_SAUDE_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISALIB_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HISALIB_PK ON TASY.HISTORICO_SAUDE_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISALIB_SETATEN_FK_I ON TASY.HISTORICO_SAUDE_LIB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISALIB_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.HISTORICO_SAUDE_LIB ADD (
  CONSTRAINT HISALIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HISTORICO_SAUDE_LIB ADD (
  CONSTRAINT HISALIB_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_SAUDE) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HISALIB_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT HISALIB_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT HISALIB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HISALIB_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.HISTORICO_SAUDE_LIB TO NIVEL_1;

