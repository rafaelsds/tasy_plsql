ALTER TABLE TASY.BIOB_RACK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BIOB_RACK CASCADE CONSTRAINTS;

CREATE TABLE TASY.BIOB_RACK
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_RACK              VARCHAR2(100 BYTE)       NOT NULL,
  NR_SEQ_GAVETA        NUMBER(10)               NOT NULL,
  NM_CURTO             VARCHAR2(30 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BIORACK_BIOGAVT_FK_I ON TASY.BIOB_RACK
(NR_SEQ_GAVETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BIORACK_BIOGAVT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.BIORACK_PK ON TASY.BIOB_RACK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BIORACK_PK
  MONITORING USAGE;


ALTER TABLE TASY.BIOB_RACK ADD (
  CONSTRAINT BIORACK_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BIOB_RACK ADD (
  CONSTRAINT BIORACK_BIOGAVT_FK 
 FOREIGN KEY (NR_SEQ_GAVETA) 
 REFERENCES TASY.BIOB_GAVETA (NR_SEQUENCIA));

GRANT SELECT ON TASY.BIOB_RACK TO NIVEL_1;

