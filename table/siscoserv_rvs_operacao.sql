ALTER TABLE TASY.SISCOSERV_RVS_OPERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SISCOSERV_RVS_OPERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SISCOSERV_RVS_OPERACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_RVS           NUMBER(10)               NOT NULL,
  NR_SEQ_NBS           NUMBER(10)               NOT NULL,
  DT_INICIAL           DATE                     NOT NULL,
  DT_FINAL             DATE,
  VL_OPERACAO          NUMBER(22,4),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SISCOROP_LISTANBS_FK_I ON TASY.SISCOSERV_RVS_OPERACAO
(NR_SEQ_NBS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SISCOROP_PK ON TASY.SISCOSERV_RVS_OPERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISCOROP_SISCORVS_FK_I ON TASY.SISCOSERV_RVS_OPERACAO
(NR_SEQ_RVS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SISCOSERV_RVS_OPERACAO ADD (
  CONSTRAINT SISCOROP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SISCOSERV_RVS_OPERACAO ADD (
  CONSTRAINT SISCOROP_LISTANBS_FK 
 FOREIGN KEY (NR_SEQ_NBS) 
 REFERENCES TASY.LISTA_NBS (NR_SEQUENCIA),
  CONSTRAINT SISCOROP_SISCORVS_FK 
 FOREIGN KEY (NR_SEQ_RVS) 
 REFERENCES TASY.SISCOSERV_RVS (NR_SEQUENCIA));

GRANT SELECT ON TASY.SISCOSERV_RVS_OPERACAO TO NIVEL_1;

