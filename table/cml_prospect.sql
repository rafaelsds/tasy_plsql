ALTER TABLE TASY.CML_PROSPECT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CML_PROSPECT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CML_PROSPECT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  NR_SEQ_CLASSIF        NUMBER(10),
  NR_SEQ_ORIGEM         NUMBER(10),
  NR_SEQ_ESTAGIO        NUMBER(10),
  NR_SEQ_ATIVIDADE      NUMBER(10),
  DT_PROSPECT           DATE,
  DT_LIBERACAO          DATE,
  CD_CNPJ               VARCHAR2(14 BYTE),
  NR_SEQ_PRODUTO        NUMBER(10),
  DS_CLIENTE            VARCHAR2(255 BYTE),
  NR_SEQ_ESPECIALIDADE  NUMBER(10),
  NR_SEQ_LEAD           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMLPROS_CMLATIV_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_CMLATIV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_CMLCLASS_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_CMLCLASS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_CMLESTA_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_CMLESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_CMLORIG_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_CMLORIG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_CMLPROD_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_CMLPROD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_COMLEADS_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_LEAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_COMLEADS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_ESPMEDI_FK_I ON TASY.CML_PROSPECT
(NR_SEQ_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_ESTABEL_FK_I ON TASY.CML_PROSPECT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPROS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPROS_PESFISI_FK_I ON TASY.CML_PROSPECT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CMLPROS_PK ON TASY.CML_PROSPECT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cml_prospect_insert
after Insert or update ON TASY.CML_PROSPECT FOR EACH ROW
DECLARE
qt_reg_w	number(1);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.nr_seq_classif is not null) and
	(:new.nr_seq_classif <> :old.nr_seq_classif) then
	insert into cml_prospect_classif(
		nr_sequencia,
		nr_seq_classif,
		nr_seq_prospect,
		nm_usuario,
		dt_atualizacao)
	values( cml_prospect_classif_seq.nextval,
		:new.nr_seq_classif,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);
end if;

if	(:new.nr_seq_origem is not null) and
	(:new.nr_seq_origem <> :old.nr_seq_origem) then
	insert into cml_prospect_origem(
		nr_sequencia,
		nr_seq_origem,
		nr_seq_prospect,
		nm_usuario,
		dt_atualizacao)
	values( cml_prospect_origem_seq.nextval,
		:new.nr_seq_origem,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);
end if;


if	(:new.nr_seq_estagio is not null) and
	(:new.nr_seq_estagio <> :old.nr_seq_estagio) then
	insert into cml_prospect_estagio(
		nr_sequencia,
		nr_seq_estagio,
		nr_seq_prospect,
		nm_usuario,
		dt_atualizacao)
	values( cml_prospect_estagio_seq.nextval,
		:new.nr_seq_estagio,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);
end if;

if	(:new.nr_seq_atividade is not null) and
	(:new.nr_seq_atividade <> :old.nr_seq_atividade) then
	insert into cml_prospect_atividade(
		nr_sequencia,
		nr_seq_atividade,
		nr_seq_prospect,
		nm_usuario,
		dt_atualizacao)
	values( cml_prospect_atividade_seq.nextval,
		:new.nr_seq_atividade,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.CML_PROSPECT ADD (
  CONSTRAINT CMLPROS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CML_PROSPECT ADD (
  CONSTRAINT CMLPROS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMLPROS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMLPROS_CMLCLASS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CML_CLASSIFICACAO (NR_SEQUENCIA),
  CONSTRAINT CMLPROS_CMLORIG_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.CML_ORIGEM (NR_SEQUENCIA),
  CONSTRAINT CMLPROS_CMLESTA_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.CML_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT CMLPROS_CMLATIV_FK 
 FOREIGN KEY (NR_SEQ_ATIVIDADE) 
 REFERENCES TASY.CML_ATIVIDADE (NR_SEQUENCIA),
  CONSTRAINT CMLPROS_CMLPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.CML_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT CMLPROS_ESPMEDI_FK 
 FOREIGN KEY (NR_SEQ_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT CMLPROS_COMLEADS_FK 
 FOREIGN KEY (NR_SEQ_LEAD) 
 REFERENCES TASY.COM_LEADS (NR_SEQUENCIA));

GRANT SELECT ON TASY.CML_PROSPECT TO NIVEL_1;

