ALTER TABLE TASY.MAN_ORDEM_SERV_HIST_TP_CLI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_HIST_TP_CLI CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_HIST_TP_CLI
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ORDEM_SERVICO  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TIPO           NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSTC_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_HIST_TP_CLI
(NR_SEQ_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSTC_MANTIOS_FK_I ON TASY.MAN_ORDEM_SERV_HIST_TP_CLI
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSTC_MANTIOS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANOSTC_PK ON TASY.MAN_ORDEM_SERV_HIST_TP_CLI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSTC_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_ORDEM_SERV_HIST_TP_CLI ADD (
  CONSTRAINT MANOSTC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_HIST_TP_CLI ADD (
  CONSTRAINT MANOSTC_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANOSTC_MANTIOS_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MAN_TIPO_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_HIST_TP_CLI TO NIVEL_1;

