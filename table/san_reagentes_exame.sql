ALTER TABLE TASY.SAN_REAGENTES_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_REAGENTES_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_REAGENTES_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE_FORNEC   NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANREAGEXA_MATERIA_FK_I ON TASY.SAN_REAGENTES_EXAME
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREAGEXA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANREAGEXA_PK ON TASY.SAN_REAGENTES_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREAGEXA_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANREAGEXA_SANEXAM_FK_I ON TASY.SAN_REAGENTES_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREAGEXA_SANEXAM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_REAGENTES_EXAME ADD (
  CONSTRAINT SANREAGEXA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_REAGENTES_EXAME ADD (
  CONSTRAINT SANREAGEXA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SANREAGEXA_SANEXAM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_REAGENTES_EXAME TO NIVEL_1;

