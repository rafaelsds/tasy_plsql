ALTER TABLE TASY.HFP_IMAGEM_EXERCICIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HFP_IMAGEM_EXERCICIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HFP_IMAGEM_EXERCICIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EXERCICIO     NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(100 BYTE),
  DS_TITULO            VARCHAR2(80 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HFPIMEX_HFPEXER_FK_I ON TASY.HFP_IMAGEM_EXERCICIO
(NR_SEQ_EXERCICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HFPIMEX_HFPEXER_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HFPIMEX_PK ON TASY.HFP_IMAGEM_EXERCICIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HFPIMEX_PK
  MONITORING USAGE;


ALTER TABLE TASY.HFP_IMAGEM_EXERCICIO ADD (
  CONSTRAINT HFPIMEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HFP_IMAGEM_EXERCICIO ADD (
  CONSTRAINT HFPIMEX_HFPEXER_FK 
 FOREIGN KEY (NR_SEQ_EXERCICIO) 
 REFERENCES TASY.HFP_EXERCICIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HFP_IMAGEM_EXERCICIO TO NIVEL_1;

