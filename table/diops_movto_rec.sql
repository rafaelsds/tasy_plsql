ALTER TABLE TASY.DIOPS_MOVTO_REC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_MOVTO_REC CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_MOVTO_REC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_MOVTO    NUMBER(10),
  NR_SEQ_PERIODO       NUMBER(10),
  VL_IND_CONTRAP       NUMBER(15,2),
  VL_COL_CONTRAP       NUMBER(15,2),
  VL_IND_PROV_CONTRAP  NUMBER(15,2),
  VL_COL_PROV_CONTRAP  NUMBER(15,2),
  DT_MES_COMPETENCIA   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIOPSMORE_DIOPERI_FK_I ON TASY.DIOPS_MOVTO_REC
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIOPSMORE_DIOPSTMRE_FK_I ON TASY.DIOPS_MOVTO_REC
(NR_SEQ_TIPO_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIOPSMORE_PK ON TASY.DIOPS_MOVTO_REC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIOPS_MOVTO_REC ADD (
  CONSTRAINT DIOPSMORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIOPS_MOVTO_REC ADD (
  CONSTRAINT DIOPSMORE_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA),
  CONSTRAINT DIOPSMORE_DIOPSTMRE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MOVTO) 
 REFERENCES TASY.DIOPS_TIPO_MOVTO_REC (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIOPS_MOVTO_REC TO NIVEL_1;

