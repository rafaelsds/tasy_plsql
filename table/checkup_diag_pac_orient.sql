ALTER TABLE TASY.CHECKUP_DIAG_PAC_ORIENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHECKUP_DIAG_PAC_ORIENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHECKUP_DIAG_PAC_ORIENT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_DIAG           NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_ORIENTACAO     NUMBER(10)              NOT NULL,
  IE_RELATORIO          VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_SUSPENSAO          DATE,
  NM_USUARIO_SUSPENSAO  VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEDIPO_CHEDIAG_FK_I ON TASY.CHECKUP_DIAG_PAC_ORIENT
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEDIPO_CHEDIAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEDIPO_CHETICO_FK_I ON TASY.CHECKUP_DIAG_PAC_ORIENT
(NR_SEQ_ORIENTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEDIPO_CHETICO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CHEDIPO_PK ON TASY.CHECKUP_DIAG_PAC_ORIENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CHECKUP_DIAG_PAC_ORIENT_INSERT
AFTER Insert ON TASY.CHECKUP_DIAG_PAC_ORIENT FOR EACH ROW
DECLARE

cd_pessoa_fisica_w	Varchar2(10);
nr_atendimento_w	Number(10);
nr_seq_orientacao_w	Number(10);
nr_seq_apres_w		Number(5);
nr_seq_orient_w		Number(10);
ds_orientacao_w		Varchar2(2000);
nr_seq_diag_w		Number(10);

Cursor C01 is
	select	a.nr_seq_orientacao,
		b.ds_orientacao,
		b.nr_seq_apresent
	from	checkup_tipo_orientacao b,
		checkup_diag_orient a
	where	a.nr_seq_diag		= nr_seq_diag_w
	and	a.nr_seq_orientacao	= b.nr_sequencia
	and	not exists (	select	1
				from	checkup_orientacao c
				where	c.nr_seq_orientacao	= a.nr_seq_orientacao
				and	c.nr_seq_orientacao	= :new.nr_seq_orientacao
				and	c.nr_atendimento	= nr_atendimento_w);

begin

select	cd_pessoa_fisica,
	nr_atendimento,
	nr_seq_diag
into	cd_pessoa_fisica_w,
	nr_atendimento_w,
	nr_seq_diag_w
from	checkup_diagnostico
where	nr_sequencia	= :new.nr_seq_diag;

begin
select	b.nr_sequencia,
	b.ds_orientacao,
	b.nr_seq_apresent
into	nr_seq_orientacao_w,
	ds_orientacao_w,
	nr_seq_apres_w
from	checkup_tipo_orientacao b
where	b.nr_sequencia	= :new.nr_seq_orientacao
and	not exists (	select	1
			from	checkup_orientacao c
			where	c.nr_seq_orientacao	= b.nr_sequencia
			and	c.nr_atendimento	= nr_atendimento_w);
exception
when others then
	nr_seq_orientacao_w	:= 0;
end;

if	(nr_seq_orientacao_w > 0) then
	select	checkup_orientacao_seq.nextval
	into	nr_seq_orient_w
	from	dual;

	insert into checkup_orientacao(
		nr_sequencia,
		nr_atendimento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_orientacao,
		cd_pessoa_fisica,
		dt_registro,
		ds_orientacao,
		nr_seq_impressao)
	Values(
		nr_seq_orient_w,
		nr_atendimento_W,
		sysdate,
		:new.nm_usuario,
		nr_seq_orientacao_w,
		cd_pessoa_fisica_w,
		sysdate,
		ds_orientacao_w,
		nr_seq_apres_w);
end if;

END;
/


ALTER TABLE TASY.CHECKUP_DIAG_PAC_ORIENT ADD (
  CONSTRAINT CHEDIPO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHECKUP_DIAG_PAC_ORIENT ADD (
  CONSTRAINT CHEDIPO_CHETICO_FK 
 FOREIGN KEY (NR_SEQ_ORIENTACAO) 
 REFERENCES TASY.CHECKUP_TIPO_ORIENTACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CHEDIPO_CHEDIAG_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.CHECKUP_DIAGNOSTICO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CHECKUP_DIAG_PAC_ORIENT TO NIVEL_1;

