ALTER TABLE TASY.HDM_INDIC_FT_GROUP_ACT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HDM_INDIC_FT_GROUP_ACT CASCADE CONSTRAINTS;

CREATE TABLE TASY.HDM_INDIC_FT_GROUP_ACT
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_DAY_SCHEDULED    NUMBER(10)            NOT NULL,
  NR_SEQ_MONTH_SCHEDULED  NUMBER(10)            NOT NULL,
  NR_SEQ_DAY_BEGIN        NUMBER(10),
  NR_SEQ_DAY_END          NUMBER(10),
  QT_EXPECTED             NUMBER(10)            NOT NULL,
  QT_REAL                 NUMBER(10)            NOT NULL,
  SI_FIRST_ENCOUNTER      VARCHAR2(1 BYTE)      NOT NULL,
  SI_LAST_ENCOUNTER       VARCHAR2(1 BYTE)      NOT NULL,
  PR_CONCLUSION           NUMBER(7),
  QT_INDIVIDUAL_AFTER     NUMBER(10),
  NR_SEQ_APP_DETAIL       NUMBER(10)            NOT NULL,
  DS_UNIQUE               VARCHAR2(255 BYTE),
  NR_DIF_PERSON           VARCHAR2(10 BYTE),
  SI_PRESENT              VARCHAR2(1 BYTE),
  NR_SEQ_SCHEDULE         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDMIFGA_HDMIDAD_FK_I ON TASY.HDM_INDIC_FT_GROUP_ACT
(NR_SEQ_APP_DETAIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDMIFGA_HDMIDID_FK_I ON TASY.HDM_INDIC_FT_GROUP_ACT
(NR_SEQ_DAY_SCHEDULED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDMIFGA_HDMIDID_FK2_I ON TASY.HDM_INDIC_FT_GROUP_ACT
(NR_SEQ_DAY_BEGIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDMIFGA_HDMIDID_FK3_I ON TASY.HDM_INDIC_FT_GROUP_ACT
(NR_SEQ_DAY_END)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDMIFGA_PK ON TASY.HDM_INDIC_FT_GROUP_ACT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HDM_INDIC_FT_GROUP_ACT ADD (
  CONSTRAINT HDMIFGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HDM_INDIC_FT_GROUP_ACT ADD (
  CONSTRAINT HDMIFGA_HDMIDAD_FK 
 FOREIGN KEY (NR_SEQ_APP_DETAIL) 
 REFERENCES TASY.HDM_INDIC_DM_APP_DETAILS (NR_SEQUENCIA),
  CONSTRAINT HDMIFGA_HDMIDID_FK 
 FOREIGN KEY (NR_SEQ_DAY_SCHEDULED) 
 REFERENCES TASY.HDM_INDIC_DM_DAY (NR_SEQUENCIA),
  CONSTRAINT HDMIFGA_HDMIDID_FK2 
 FOREIGN KEY (NR_SEQ_DAY_BEGIN) 
 REFERENCES TASY.HDM_INDIC_DM_DAY (NR_SEQUENCIA),
  CONSTRAINT HDMIFGA_HDMIDID_FK3 
 FOREIGN KEY (NR_SEQ_DAY_END) 
 REFERENCES TASY.HDM_INDIC_DM_DAY (NR_SEQUENCIA));

GRANT SELECT ON TASY.HDM_INDIC_FT_GROUP_ACT TO NIVEL_1;

