ALTER TABLE TASY.PCS_MOTIVOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_MOTIVOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_MOTIVOS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_MOTIVO                   VARCHAR2(255 BYTE) NOT NULL,
  IE_ATRASO_ATIVIDADE         VARCHAR2(15 BYTE) NOT NULL,
  IE_NAO_REALIZ_ATIVIDADE     VARCHAR2(15 BYTE) NOT NULL,
  IE_CANCEL_PARECER           VARCHAR2(15 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  IE_RECLASSIF_ABC            VARCHAR2(15 BYTE) NOT NULL,
  IE_SITUACAO_ENCERRAMENTO    VARCHAR2(1 BYTE)  NOT NULL,
  IE_CANCEL_ATIV_AGENDA       VARCHAR2(1 BYTE),
  IE_INATIVA_REGRA_GERA_ATIV  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCSMOTI_PK ON TASY.PCS_MOTIVOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSMOTI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PCS_MOTIVOS_tp  after update ON TASY.PCS_MOTIVOS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_MOTIVO,1,4000),substr(:new.DS_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECLASSIF_ABC,1,4000),substr(:new.IE_RECLASSIF_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECLASSIF_ABC',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_REALIZ_ATIVIDADE,1,4000),substr(:new.IE_NAO_REALIZ_ATIVIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_REALIZ_ATIVIDADE',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CANCEL_PARECER,1,4000),substr(:new.IE_CANCEL_PARECER,1,4000),:new.nm_usuario,nr_seq_w,'IE_CANCEL_PARECER',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATRASO_ATIVIDADE,1,4000),substr(:new.IE_ATRASO_ATIVIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATRASO_ATIVIDADE',ie_log_w,ds_w,'PCS_MOTIVOS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PCS_MOTIVOS ADD (
  CONSTRAINT PCSMOTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PCS_MOTIVOS TO NIVEL_1;

