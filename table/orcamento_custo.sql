ALTER TABLE TASY.ORCAMENTO_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORCAMENTO_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORCAMENTO_CUSTO
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_TABELA_CUSTO      NUMBER(5)                NOT NULL,
  CD_CENTRO_CONTROLE   NUMBER(8)                NOT NULL,
  CD_NATUREZA_GASTO    NUMBER(20)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_ORCADO            NUMBER(15,4)             NOT NULL,
  VL_AVISTA            NUMBER(15,4),
  VL_DISTRIBUIDO       NUMBER(15,4),
  VL_RECEB_DISTRIB     NUMBER(15,4),
  QT_DIAS_PRAZO        NUMBER(15,4),
  VL_A_DISTRIBUIR      NUMBER(15,2),
  IE_TIPO_GASTO        VARCHAR2(2 BYTE)         NOT NULL,
  NR_SEQUENCIA         NUMBER(15)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_NG            NUMBER(10)               NOT NULL,
  NR_SEQ_TABELA        NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  VL_DISTRIB_CONTABIL  NUMBER(15,4),
  NR_LOTE_CONTABIL     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCCUST_CENCONT_FK_I ON TASY.ORCAMENTO_CUSTO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCCUST_LOTCONT_FK_I ON TASY.ORCAMENTO_CUSTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCCUST_NATGAST_FK_I ON TASY.ORCAMENTO_CUSTO
(CD_ESTABELECIMENTO, CD_NATUREZA_GASTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCCUST_NATGAST_FK_I2 ON TASY.ORCAMENTO_CUSTO
(NR_SEQ_NG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          144K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCCUST_PK ON TASY.ORCAMENTO_CUSTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCCUST_TABCUST_FK_I ON TASY.ORCAMENTO_CUSTO
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCCUST_TABCUST_FK_I2 ON TASY.ORCAMENTO_CUSTO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCCUST_UK ON TASY.ORCAMENTO_CUSTO
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO, CD_CENTRO_CONTROLE, NR_SEQ_NG, IE_TIPO_GASTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          400K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.orcamento_custo_atual
before insert or update ON TASY.ORCAMENTO_CUSTO for each row
declare

cd_empresa_w	number(10);
nr_seq_ng_w	number(10);

begin

if	(:new.nr_seq_ng is null) then
	begin
	cd_empresa_w	:= obter_empresa_estab(:new.cd_estabelecimento);

	select	max(nr_sequencia)
	into	nr_seq_ng_w
	from	natureza_gasto
	where	cd_empresa		= cd_empresa_w
	and	nvl(cd_estabelecimento, :new.cd_estabelecimento)	= :new.cd_estabelecimento
	and	cd_natureza_gasto	= :new.cd_natureza_gasto;

	if	(nr_seq_ng_w is not null) then
		:new.nr_seq_ng	:= nr_seq_ng_w;
	end if;
	end;
elsif	(:new.cd_natureza_gasto is null) then
	select	max(cd_natureza_gasto)
	into	:new.cd_natureza_gasto
	from	natureza_gasto
	where	nr_sequencia	= :new.nr_seq_ng;
end if;
end orcamento_custo_atual;
/


CREATE OR REPLACE TRIGGER TASY.ctb_orcamento_custo_iud
before insert or update or delete ON TASY.ORCAMENTO_CUSTO for each row
declare

qt_registro_w	number(10);

begin

if	(inserting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(updating) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(deleting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:old.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
end if;

end ctb_orcamento_custo_iud;
/


ALTER TABLE TASY.ORCAMENTO_CUSTO ADD (
  CONSTRAINT ORCCUST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT ORCCUST_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_TABELA_CUSTO, CD_CENTRO_CONTROLE, NR_SEQ_NG, IE_TIPO_GASTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          400K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORCAMENTO_CUSTO ADD (
  CONSTRAINT ORCCUST_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ORCCUST_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE),
  CONSTRAINT ORCCUST_NATGAST_FK 
 FOREIGN KEY (NR_SEQ_NG) 
 REFERENCES TASY.NATUREZA_GASTO (NR_SEQUENCIA),
  CONSTRAINT ORCCUST_TABCUST_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.TABELA_CUSTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ORCAMENTO_CUSTO TO NIVEL_1;

