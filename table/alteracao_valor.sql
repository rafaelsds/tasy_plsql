ALTER TABLE TASY.ALTERACAO_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALTERACAO_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALTERACAO_VALOR
(
  NR_TITULO           NUMBER(10)                NOT NULL,
  NR_SEQUENCIA        NUMBER(5),
  DT_ALTERACAO        DATE                      NOT NULL,
  VL_ANTERIOR         NUMBER(15,2)              NOT NULL,
  VL_ALTERACAO        NUMBER(15,2)              NOT NULL,
  CD_MOTIVO           NUMBER(10)                NOT NULL,
  CD_MOEDA            NUMBER(5)                 NOT NULL,
  IE_AUMENTA_DIMINUI  VARCHAR2(1 BYTE)          NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_LOTE_CONTABIL    NUMBER(10),
  DS_OBSERVACAO       VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN    NUMBER(10),
  NR_EXTERNO          VARCHAR2(255 BYTE),
  NR_CODIGO_CONTROLE  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ALTVALO_LOTCONT_FK_I ON TASY.ALTERACAO_VALOR
(NR_LOTE_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTVALO_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALTVALO_MOEDA_FK_I ON TASY.ALTERACAO_VALOR
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTVALO_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALTVALO_MOTAVTR_FK_I ON TASY.ALTERACAO_VALOR
(CD_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ALTVALO_PK ON TASY.ALTERACAO_VALOR
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALTVALO_TITRECE_FK_I ON TASY.ALTERACAO_VALOR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALTVALO_TRAFINA_FK_I ON TASY.ALTERACAO_VALOR
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTVALO_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ALTERACAO_VALOR_INSERT
BEFORE INSERT ON TASY.ALTERACAO_VALOR FOR EACH ROW
DECLARE

cd_estabelecimento_w  number(5);
cd_convenio_w   number(5);
ie_processo_camara_w  pls_parametros_camara.ie_processo_camara%type;
nr_titulo_receber_w   pls_titulo_lote_camara.nr_titulo_receber%type;
nr_seq_lote_w    pls_titulo_lote_camara.nr_sequencia%type;
ie_baixa_camara_compensacao_w number(10);


begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select max(cd_estabelecimento),
 obter_convenio_tit_rec(max(nr_titulo))
into cd_estabelecimento_w,
 cd_convenio_w
from titulo_receber
where nr_titulo = :new.nr_titulo;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.DT_ALTERACAO);



if (cd_estabelecimento_w is not null) then

 /*verificar se o titulo que esta recebendo a baixa esta em um lote de camra de compensacao que nao esta baixado.*/
 select count(a.nr_titulo_receber),
   max(b.nr_sequencia)
 into nr_titulo_receber_w,
   nr_seq_lote_w
 from pls_titulo_lote_camara a,
   pls_lote_camara_comp b
 where a.nr_seq_lote_camara = b.nr_sequencia
 and  a.nr_titulo_receber  = :new.nr_titulo;

 select max(a.ie_processo_camara)
 into ie_processo_camara_w
 from pls_parametros_camara a
 where  cd_estabelecimento = cd_estabelecimento_w;

 /*Nao deixar alterar  valor para titulos que estao em lote de camara de compensacao.*/
 if (nvl(ie_processo_camara_w,'CO') = 'CA') and (nr_titulo_receber_w > 0) then /*se  o processo camara de compensacao for regime de caixa e o titulo estiver na camara*/

   wheb_mensagem_pck.exibir_mensagem_abort(335788, 'NR_SEQ_LOTE_W=' || nr_seq_lote_w ||
         ';NR_TITULO_W=' || :new.nr_titulo);
 end if;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_valor_after_insert
after insert ON TASY.ALTERACAO_VALOR for each row
declare

cd_agencia_bancaria_w   banco_estabelecimento.cd_agencia_bancaria%type;
cd_banco_w              banco_estabelecimento.cd_banco%type;
cd_camara_compensacao_w pessoa_fisica_conta.cd_camara_compensacao%type;
cd_centro_custo_desc_w  titulo_receber_liq_desc.cd_centro_custo%type;
cd_cgc_w                titulo_receber.cd_cgc%type;
cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
cd_pessoa_fisica_w      titulo_receber.cd_pessoa_fisica%type;
ie_desc_previsto_w      varchar2(1);
ie_digito_conta_w       banco_estabelecimento.ie_digito_conta%type;
ie_entrada_confirmada_w titulo_receber.ie_entrada_confirmada%type;
ie_juros_multa_w        varchar2(1);
ie_retorno_w            number(10);
ie_tipo_ocorrencia_w    number(10);
nr_conta_w              banco_estabelecimento.cd_conta%type;
nr_multiplicador_w      number(1);
nr_seq_conta_banco_w    titulo_receber.nr_seq_conta_banco%type;
nr_seq_motivo_desc_w    titulo_receber_liq_desc.nr_seq_motivo_desc%type;
vl_desc_previsto_w      titulo_receber.vl_desc_previsto%type;
vl_titulo_w				titulo_receber.vl_titulo%type;

cursor c01 is
        select  cd_banco,
                cd_agencia_bancaria,
                nr_conta,
                nr_digito_conta,
                cd_camara_compensacao
        from    pessoa_fisica_conta
        where   cd_pessoa_fisica        = cd_pessoa_fisica_w
        union
        select  cd_banco,
                cd_agencia_bancaria,
                nr_conta,
                nr_digito_conta,
                cd_camara_compensacao
        from    pessoa_juridica_conta
        where   cd_cgc  = cd_cgc_w;


cursor c02 is
        select  a.nm_atributo,
                a.cd_tipo_lote_contab
        from    atributo_contab a
        where   a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in ( 'VL_ALTERACAO');

c02_w   c02%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  max(a.ie_entrada_confirmada),
        max(a.cd_pessoa_fisica),
        max(a.cd_cgc),
        max(a.nr_seq_conta_banco),
        max(a.vl_desc_previsto),
		max(a.vl_titulo)
into    ie_entrada_confirmada_w,
        cd_pessoa_fisica_w,
        cd_cgc_w,
        nr_seq_conta_banco_w,
        vl_desc_previsto_w,
		vl_titulo_w
from    titulo_receber a
where   a.nr_titulo = :new.nr_titulo;

/*Se o titulo ja foi incluido em cobranca escritural e a alteracao diminuir o valor, deve fazer o insert*/
if (ie_entrada_confirmada_w = 'C') and (:new.ie_aumenta_diminui = 'D') then

        if ( (nvl(:new.vl_anterior,0) - nvl(:new.vl_alteracao,0)) = 0 ) then
                /*Tipo de ocorrencia 2 pois esta zerando o saldo do titulo.*/
                ie_tipo_ocorrencia_w := 2;
        else
                /*Tipo de ocorrencia 4 pois se trata de concessao de desconto.*/
                ie_tipo_ocorrencia_w := 4;
        end if;
        begin
        select  a.cd_banco,
                a.cd_agencia_bancaria,
                a.cd_conta,
                a.ie_digito_conta
        into    cd_banco_w,
                cd_agencia_bancaria_w,
                nr_conta_w,
                ie_digito_conta_w
        from    banco_estabelecimento a
        where   a.nr_sequencia = nr_seq_conta_banco_w;
        exception when others then
                ie_retorno_w    := 0;
        end;

        if      (ie_retorno_w = 0) then
                open c01;
                loop
                fetch c01 into
                        cd_banco_w,
                        cd_agencia_bancaria_w,
                        nr_conta_w,
                        ie_digito_conta_w,
                        cd_camara_compensacao_w;
                exit when c01%notfound;
                        cd_banco_w      := cd_banco_w;
                end loop;
                close c01;
        end if;

        obter_param_usuario(815, 9, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_desc_previsto_w);
        obter_param_usuario(815, 16, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_juros_multa_w);

        select  max(a.cd_centro_custo),
                        max(a.nr_seq_motivo_desc)
        into    cd_centro_custo_desc_w,
                        nr_seq_motivo_desc_w
        from    titulo_receber_liq_desc a
        where   a.nr_titulo             = :new.nr_titulo
        and             a.nr_bordero    is null
        and             a.nr_seq_liq    is null;

        insert  into    titulo_receber_instr (
                                nr_seq_cobranca,
                                nr_titulo,
                                vl_cobranca,
                                dt_atualizacao,
                                nm_usuario,
                                cd_banco,
                                cd_agencia_bancaria,
                                nr_conta,
                                cd_moeda,
                                ie_digito_conta,
                                cd_camara_compensacao,
                                vl_desconto,
                                vl_desc_previsto,
                                vl_acrescimo,
                                nr_sequencia,
                                vl_despesa_bancaria,
                                vl_saldo_inclusao,
                                qt_dias_instrucao,
                                cd_ocorrencia,
                                ie_instrucao_enviada,
                                nr_seq_motivo_desc,
                                cd_centro_custo_desc,
                                vl_juros,
                                vl_multa,
                                ie_selecionado)
                values  (
                                null,
                                :new.nr_titulo,
                                decode(ie_tipo_ocorrencia_w,4, vl_titulo_w, (nvl(:new.vl_anterior,0) - nvl(:new.vl_alteracao,0))),
                                sysdate,
                                :new.nm_usuario,
                                cd_banco_w,
                                cd_agencia_bancaria_w,
                                nr_conta_w,
                                :new.cd_moeda,
                                ie_digito_conta_w,
                                cd_camara_compensacao_w,
                                decode(ie_tipo_ocorrencia_w, 4,  (nvl(:new.vl_anterior,0) - nvl(:new.vl_alteracao,0)), decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0)) + nvl(to_number(obter_dados_titulo_receber(:new.nr_titulo,'VNC')),0)),
                                decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0),0),
                                0,
                                titulo_receber_instr_seq.nextval,
                                0,
                                (nvl(:new.vl_anterior,0) - nvl(:new.vl_alteracao,0)),
                                null,
                                substr(obter_ocorrencia_envio_cre(ie_tipo_ocorrencia_w,cd_banco_w),1,3),
                                'N',
                                nr_seq_motivo_desc_w,
                                cd_centro_custo_desc_w,
                                decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','J')),null),
                                decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','M')),null),
                                'N');
end if;


if      (nvl(:new.nr_titulo, 0) <> 0) then
        begin

        select  cd_estabelecimento
        into    cd_estabelecimento_w
        from    titulo_receber
        where   nr_titulo = :new.nr_titulo;

        nr_multiplicador_w := 1;
        if      (:new.ie_aumenta_diminui <> 'A') then
                begin
                nr_multiplicador_w:= -1;
                end;
        end if;

        open c02;
        loop
        fetch c02 into
                c02_w;
        exit when c02%notfound;
                begin

                if      (nvl(:new.vl_alteracao, 0) <> 0) and (nvl(:new.nr_seq_trans_fin,0) <> 0)  then
                        begin

                        ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                                trunc(:new.dt_alteracao),
                                                                                c02_w.cd_tipo_lote_contab,
                                                                                :new.nr_seq_trans_fin,
                                                                                54,
                                                                                :new.nr_titulo,
                                                                                :new.nr_sequencia,
                                                                                0,
                                                                                :new.vl_alteracao * nr_multiplicador_w,
                                                                                'ALTERACAO_VALOR',
                                                                                c02_w.nm_atributo,
                                                                                :new.nm_usuario);

                        end;
                end if;

                end;
        end loop;
        close c02;
        end;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_valor_bef_update
before update ON TASY.ALTERACAO_VALOR for each row
declare
cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if 	(nvl(:new.nr_titulo, 0) <> 0) then

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;

	/* Projeto Davita - Nao deixa gerar movimento contabil apos fechamento da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w), :new.dt_alteracao);

end if;
end if;

end;
/


ALTER TABLE TASY.ALTERACAO_VALOR ADD (
  CONSTRAINT ALTVALO_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ALTERACAO_VALOR ADD (
  CONSTRAINT ALTVALO_MOTAVTR_FK 
 FOREIGN KEY (CD_MOTIVO) 
 REFERENCES TASY.MOTIVO_ALT_VALOR_TIT_REC (NR_SEQUENCIA),
  CONSTRAINT ALTVALO_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ALTVALO_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT ALTVALO_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT ALTVALO_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ALTERACAO_VALOR TO NIVEL_1;

