ALTER TABLE TASY.LAB_IMPORTACAO_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_IMPORTACAO_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_IMPORTACAO_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PRESCRICAO        NUMBER(10),
  NR_SEQ_PRESCR        NUMBER(6),
  NR_SEQ_EXAME         NUMBER(10),
  CD_TIPO_REGISTRO     VARCHAR2(15 BYTE),
  CD_EXAME             VARCHAR2(15 BYTE),
  CD_MATERIAL          VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL      NUMBER(10),
  CD_PARAMETRO         VARCHAR2(15 BYTE),
  DS_LISTA_EXAME       VARCHAR2(2000 BYTE),
  NM_USUARIO_IMP       VARCHAR2(15 BYTE),
  UNIDADE_MEDIDA       VARCHAR2(255 BYTE),
  DS_REFERENCIA        VARCHAR2(4000 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_APROVACAO         DATE,
  DT_INTEGRACAO        DATE,
  DS_ERRO              VARCHAR2(4000 BYTE),
  VL_RESULTADO         VARCHAR2(2000 BYTE),
  IE_SEXO              VARCHAR2(5 BYTE),
  NR_ANOS              NUMBER(20,6),
  SIGLA_EQUIP          VARCHAR2(10 BYTE),
  NM_USUARIO_APROV     VARCHAR2(15 BYTE),
  DT_ERRO_APROV        DATE,
  DT_ERRO              DATE,
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LABIMPE_ESTABEL_FK_I ON TASY.LAB_IMPORTACAO_EXAME
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABIMPE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LABIMPE_I1 ON TASY.LAB_IMPORTACAO_EXAME
(DT_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABIMPE_I1
  MONITORING USAGE;


CREATE INDEX TASY.LABIMPE_I2 ON TASY.LAB_IMPORTACAO_EXAME
(NR_PRESCRICAO, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABIMPE_I2
  MONITORING USAGE;


CREATE INDEX TASY.LABIMPE_I3 ON TASY.LAB_IMPORTACAO_EXAME
(DT_APROVACAO, DT_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABIMPE_I3
  MONITORING USAGE;


CREATE INDEX TASY.LABIMPE_I4 ON TASY.LAB_IMPORTACAO_EXAME
(DT_INTEGRACAO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABIMPE_PK ON TASY.LAB_IMPORTACAO_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABIMPE_PK
  MONITORING USAGE;


ALTER TABLE TASY.LAB_IMPORTACAO_EXAME ADD (
  CONSTRAINT LABIMPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_IMPORTACAO_EXAME ADD (
  CONSTRAINT LABIMPE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.LAB_IMPORTACAO_EXAME TO NIVEL_1;

