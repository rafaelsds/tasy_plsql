ALTER TABLE TASY.PLS_GRG_PROTOCOLO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRG_PROTOCOLO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRG_PROTOCOLO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_GRG_LOTE       NUMBER(10)              NOT NULL,
  NR_LOTE_PRESTADOR     VARCHAR2(70 BYTE),
  NR_PROTOCOLO          VARCHAR2(70 BYTE),
  DT_PROTOCOLO          DATE,
  NR_SEQ_DMA_PROTOCOLO  NUMBER(10)              NOT NULL,
  NR_SEQ_GUIA_ARQUIVO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSGRGP_PK ON TASY.PLS_GRG_PROTOCOLO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGRGP_PLSDAPR_FK_I ON TASY.PLS_GRG_PROTOCOLO
(NR_SEQ_DMA_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGRGP_PLSFGEN_FK_I ON TASY.PLS_GRG_PROTOCOLO
(NR_SEQ_GUIA_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGRGP_PLSGRGL_FK_I ON TASY.PLS_GRG_PROTOCOLO
(NR_SEQ_GRG_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_GRG_PROTOCOLO ADD (
  CONSTRAINT PLSGRGP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GRG_PROTOCOLO ADD (
  CONSTRAINT PLSGRGP_PLSDAPR_FK 
 FOREIGN KEY (NR_SEQ_DMA_PROTOCOLO) 
 REFERENCES TASY.PLS_DMA_PROTOCOLO_IMP (NR_SEQUENCIA),
  CONSTRAINT PLSGRGP_PLSFGEN_FK 
 FOREIGN KEY (NR_SEQ_GUIA_ARQUIVO) 
 REFERENCES TASY.PLS_FAT_GUIA_ARQUIVO (NR_SEQUENCIA),
  CONSTRAINT PLSGRGP_PLSGRGL_FK 
 FOREIGN KEY (NR_SEQ_GRG_LOTE) 
 REFERENCES TASY.PLS_GRG_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_GRG_PROTOCOLO TO NIVEL_1;

