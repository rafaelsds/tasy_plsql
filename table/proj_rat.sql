ALTER TABLE TASY.PROJ_RAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_RAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_RAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLIENTE       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_EXECUTOR          VARCHAR2(10 BYTE)        NOT NULL,
  DT_INICIO            DATE                     NOT NULL,
  DT_FINAL             DATE                     NOT NULL,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  DT_APROVACAO         DATE,
  NM_USUARIO_APROV     VARCHAR2(15 BYTE),
  QT_TOTAL_HORA        NUMBER(17,2),
  VL_HORA_COBRAR       NUMBER(15,2),
  QT_HORA_COBRAR       NUMBER(15,2),
  VL_COBRAR_CLIENTE    NUMBER(15,2),
  VL_HORA_PAGAR        NUMBER(15,2),
  QT_HORA_PAGAR        NUMBER(15,2),
  VL_PAGAR             NUMBER(15,2),
  NR_TITULO_REC        NUMBER(10),
  NR_TITULO_PAGAR      NUMBER(10),
  NR_SEQ_NF            NUMBER(10),
  NR_ORDEM_COMPRA      NUMBER(15),
  DT_PREV_NF           DATE,
  IE_STATUS_COBRAR     VARCHAR2(2 BYTE)         NOT NULL,
  IE_PAGA_RAT          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_RECEB_RAT         DATE,
  NM_USUARIO_RECEB     VARCHAR2(15 BYTE),
  NR_SEQ_CANAL         NUMBER(10),
  QT_DIA_EXEC          NUMBER(15),
  QT_DIA_REG           NUMBER(15),
  IE_FUNCAO_EXEC       VARCHAR2(15 BYTE),
  NR_SEQ_PROJ          NUMBER(10),
  VL_IMPOSTO           NUMBER(15,2),
  VL_ADICIONAL_CLT     NUMBER(15,2),
  VL_CUSTO_INST        NUMBER(15,2),
  VL_FIXO_CONSULTOR    NUMBER(15,2),
  DT_REC_FIN           DATE,
  DT_APROV_FIN         DATE,
  NR_SEQ_FECH_MES      NUMBER(10),
  IE_STATUS_RAT        VARCHAR2(15 BYTE)        NOT NULL,
  IE_REGIME_CONTR      VARCHAR2(15 BYTE),
  NR_SEQ_CRONOGRAMA    NUMBER(10),
  IE_FATURADO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJRAT_COMCANA_FK_I ON TASY.PROJ_RAT
(NR_SEQ_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_COMCANA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_COMCLIE_FK_I ON TASY.PROJ_RAT
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_NOTFISC_FK_I ON TASY.PROJ_RAT
(NR_SEQ_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROJRAT_ORDCOMP_FK_I ON TASY.PROJ_RAT
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_ORDCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_PESFISI_FK_I ON TASY.PROJ_RAT
(CD_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJRAT_PK ON TASY.PROJ_RAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_PROCRON_FK_I ON TASY.PROJ_RAT
(NR_SEQ_CRONOGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_PROCRON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_PROJRAM_FK_I ON TASY.PROJ_RAT
(NR_SEQ_FECH_MES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_PROJRAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_PROPROJ_FK_I ON TASY.PROJ_RAT
(NR_SEQ_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_PROPROJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_TITPAGA_FK_I ON TASY.PROJ_RAT
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_TITPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJRAT_TITRECE_FK_I ON TASY.PROJ_RAT
(NR_TITULO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRAT_TITRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PROJ_RAT_ATUAL
BEFORE INSERT or UPDATE ON TASY.PROJ_RAT FOR EACH ROW
DECLARE

qt_reg_w		number(1);
ie_regime_contr_w	varchar2(15);
cd_cargo_w		number(10);
ie_cobrado_w		varchar2(2);

BEGIN
	if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
		goto Final;
	end if;

	if	(:new.ie_paga_rat = 'N') then
		:new.vl_hora_pagar := 0;
		:new.vl_pagar := 0;
	end if;

	if	(:new.vl_hora_cobrar is not null) and
		(:new.qt_hora_cobrar is not null) then
		:new.vl_cobrar_cliente	:= :new.vl_hora_cobrar * :new.qt_hora_cobrar;
	end if;

	if	(:new.vl_hora_pagar is not null) and
		(:new.qt_hora_pagar is not null) then
		:new.vl_pagar			:= :new.vl_hora_pagar * :new.qt_hora_pagar;
	end if;

	if	(:new.dt_aprov_fin is not null) then
		:new.ie_status_rat			:= 'AF';
	elsif	(:new.dt_rec_fin is not null) then
		:new.ie_status_rat			:= 'RF';
	elsif	(:new.dt_aprovacao is not null) then
		:new.ie_status_rat			:= 'AP';
	elsif	(:new.dt_receb_rat is not null) then
		:new.ie_status_rat			:= 'RP';
	elsif	(:new.dt_liberacao is not null) then
		:new.ie_status_rat			:= 'LC';
	else
		:new.ie_status_rat			:= 'PL';
	end if;

	if	(:new.ie_status_rat = 'AF') and
		(:new.vl_cobrar_cliente = 0) and
		(:new.ie_status_cobrar <> 'NC') then
		Wheb_mensagem_pck.exibir_mensagem_abort(279324);
	end if;

	if	(:new.ie_status_rat = 'AF') and
		(:new.vl_cobrar_cliente > 0) and
		(:new.ie_status_cobrar = 'SN') then
		Wheb_mensagem_pck.exibir_mensagem_abort(279325);
	end if;

	if	(:new.ie_regime_contr is null) then
		select	max(ie_regime_contr)
		into	ie_regime_contr_w
		from	com_canal_consultor
		where	cd_pessoa_fisica = :new.cd_executor;

		if (ie_regime_contr_w is not null) then
			:new.ie_regime_contr := ie_regime_contr_w;
		end if;
	end if;


	if (inserting) then

		select	max(ie_cobrado)
		into	ie_cobrado_w
		from	proj_cronograma
		where	nr_sequencia = :new.nr_seq_cronograma;

		if (ie_cobrado_w = 'N') then
			:new.ie_status_cobrar := 'NC';
		else
			:new.ie_status_cobrar := 'CV';
		end if;


		select	max(cd_cargo)
		into	cd_cargo_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_executor;

		if (cd_cargo_w in (842, 261, 171, 101, 991, 163, 941, 974, 972, 973, 164, 165, 162)) then -- Consultor
			:new.ie_paga_rat := 'S';
		else
			:new.ie_paga_rat := 'X';
		end if;
	end if;

	<<Final>>
	qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.PROJ_RAT ADD (
  CONSTRAINT PROJRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_RAT ADD (
  CONSTRAINT PROJRAT_PROCRON_FK 
 FOREIGN KEY (NR_SEQ_CRONOGRAMA) 
 REFERENCES TASY.PROJ_CRONOGRAMA (NR_SEQUENCIA),
  CONSTRAINT PROJRAT_PESFISI_FK 
 FOREIGN KEY (CD_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROJRAT_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PROJRAT_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NF) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT PROJRAT_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PROJRAT_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_REC) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT PROJRAT_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT PROJRAT_COMCANA_FK 
 FOREIGN KEY (NR_SEQ_CANAL) 
 REFERENCES TASY.COM_CANAL (NR_SEQUENCIA),
  CONSTRAINT PROJRAT_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROJRAT_PROJRAM_FK 
 FOREIGN KEY (NR_SEQ_FECH_MES) 
 REFERENCES TASY.PROJ_RAT_MES (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_RAT TO NIVEL_1;

