ALTER TABLE TASY.ADEP_PROCESSO_AREA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADEP_PROCESSO_AREA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADEP_PROCESSO_AREA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO_NREC      DATE                 NOT NULL,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_PROCESSO          NUMBER(10)           NOT NULL,
  NR_SEQ_AREA_PREP         NUMBER(10)           NOT NULL,
  DT_DISPENSACAO           DATE,
  NM_USUARIO_DISPENSACAO   VARCHAR2(15 BYTE),
  DT_PREPARO               DATE,
  NM_USUARIO_PREPARO       VARCHAR2(15 BYTE),
  DT_HIGIENIZACAO          DATE,
  NM_USUARIO_HIGIENIZACAO  VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADEPROA_ADEPAPR_FK_I ON TASY.ADEP_PROCESSO_AREA
(NR_SEQ_AREA_PREP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROA_ADEPAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROA_ADEPROC_FK_I ON TASY.ADEP_PROCESSO_AREA
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROA_ADEPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ADEPROA_PK ON TASY.ADEP_PROCESSO_AREA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROA_PK
  MONITORING USAGE;


ALTER TABLE TASY.ADEP_PROCESSO_AREA ADD (
  CONSTRAINT ADEPROA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADEP_PROCESSO_AREA ADD (
  CONSTRAINT ADEPROA_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT ADEPROA_ADEPAPR_FK 
 FOREIGN KEY (NR_SEQ_AREA_PREP) 
 REFERENCES TASY.ADEP_AREA_PREP (NR_SEQUENCIA));

GRANT SELECT ON TASY.ADEP_PROCESSO_AREA TO NIVEL_1;

