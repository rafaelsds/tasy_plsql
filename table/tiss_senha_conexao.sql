ALTER TABLE TASY.TISS_SENHA_CONEXAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_SENHA_CONEXAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_SENHA_CONEXAO
(
  NR_SEQUENCIA            NUMBER(10),
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  NM_USUARIO_TISS         VARCHAR2(20 BYTE),
  DS_SENHA_TISS           VARCHAR2(20 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5),
  NM_USUARIO_SENHA        VARCHAR2(15 BYTE),
  IE_COMUNICACAO          VARCHAR2(15 BYTE)     NOT NULL,
  DS_DIR_ENTRADA          VARCHAR2(255 BYTE),
  DS_DIR_SAIDA            VARCHAR2(255 BYTE),
  DS_CAMINHO_CERTIFICADO  VARCHAR2(255 BYTE),
  DS_SENHA_CERTIFICADO    VARCHAR2(255 BYTE),
  DS_CAMINHO_KEYSTORE     VARCHAR2(255 BYTE),
  DS_SENHA_KEYSTORE       VARCHAR2(255 BYTE),
  DS_LOGIN_PREST          VARCHAR2(20 BYTE),
  DS_SENHA_PREST          VARCHAR2(100 BYTE),
  DS_SENHA_PREST_HASH     VARCHAR2(32 BYTE),
  CD_SETOR_ATENDIMENTO    NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSECO_CONVENI_FK_I ON TASY.TISS_SENHA_CONEXAO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSECO_ESTABEL_FK_I ON TASY.TISS_SENHA_CONEXAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSECO_PK ON TASY.TISS_SENHA_CONEXAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSECO_SETATEN_FK_I ON TASY.TISS_SENHA_CONEXAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_SENHA_CONEXAO_BEF
before update or insert ON TASY.TISS_SENHA_CONEXAO for each row
declare

ds_hash_senha_w		varchar2(32);
begin

if	(:new.ds_senha_prest is not null) or
	(:new.ds_senha_prest <> :old.ds_senha_prest) then

	ds_hash_senha_w 		:= RAWTOHEX(dbms_obfuscation_toolkit.md5(input => utl_raw.cast_to_raw(:new.ds_senha_prest)));
	:new.ds_senha_prest_hash	:= lower(ds_hash_senha_w);
	
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TISS_SENHA_CONEXAO_tp  after update ON TASY.TISS_SENHA_CONEXAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_COMUNICACAO,1,500);gravar_log_alteracao(substr(:old.IE_COMUNICACAO,1,4000),substr(:new.IE_COMUNICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMUNICACAO',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DIR_ENTRADA,1,500);gravar_log_alteracao(substr(:old.DS_DIR_ENTRADA,1,4000),substr(:new.DS_DIR_ENTRADA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_ENTRADA',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DIR_SAIDA,1,500);gravar_log_alteracao(substr(:old.DS_DIR_SAIDA,1,4000),substr(:new.DS_DIR_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_SAIDA',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_TISS,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_TISS,1,4000),substr(:new.NM_USUARIO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_TISS',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SENHA_TISS,1,500);gravar_log_alteracao(substr(:old.DS_SENHA_TISS,1,4000),substr(:new.DS_SENHA_TISS,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_TISS',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SENHA_KEYSTORE,1,500);gravar_log_alteracao(substr(:old.DS_SENHA_KEYSTORE,1,4000),substr(:new.DS_SENHA_KEYSTORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_KEYSTORE',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_SENHA,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_SENHA,1,4000),substr(:new.NM_USUARIO_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_SENHA',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CAMINHO_CERTIFICADO,1,500);gravar_log_alteracao(substr(:old.DS_CAMINHO_CERTIFICADO,1,4000),substr(:new.DS_CAMINHO_CERTIFICADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_CERTIFICADO',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SENHA_CERTIFICADO,1,500);gravar_log_alteracao(substr(:old.DS_SENHA_CERTIFICADO,1,4000),substr(:new.DS_SENHA_CERTIFICADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_CERTIFICADO',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CAMINHO_KEYSTORE,1,500);gravar_log_alteracao(substr(:old.DS_CAMINHO_KEYSTORE,1,4000),substr(:new.DS_CAMINHO_KEYSTORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_KEYSTORE',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONVENIO,1,500);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'TISS_SENHA_CONEXAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_SENHA_CONEXAO ADD (
  CONSTRAINT TISSECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_SENHA_CONEXAO ADD (
  CONSTRAINT TISSECO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSECO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSECO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.TISS_SENHA_CONEXAO TO NIVEL_1;

