ALTER TABLE TASY.PLS_REGRA_REAJ_COBR_RETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_REAJ_COBR_RETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_REAJ_COBR_RETRO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_REGRA_RETRO      NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_MES_REF_MENSALIDADE  VARCHAR2(7 BYTE),
  DS_MES_REF_RETROATIVO   VARCHAR2(7 BYTE),
  DT_MENSALIDADE          DATE,
  DT_RETROATIVO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRRCR_PK ON TASY.PLS_REGRA_REAJ_COBR_RETRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRCR_PLSRGRR_FK_I ON TASY.PLS_REGRA_REAJ_COBR_RETRO
(NR_SEQ_REGRA_RETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_REAJ_COBR_RETRO ADD (
  CONSTRAINT PLSRRCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_REAJ_COBR_RETRO ADD (
  CONSTRAINT PLSRRCR_PLSRGRR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_RETRO) 
 REFERENCES TASY.PLS_REGRA_REAJUSTE_RETRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_REAJ_COBR_RETRO TO NIVEL_1;

