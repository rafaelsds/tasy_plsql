ALTER TABLE TASY.PARAMETROS_EPISODIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETROS_EPISODIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETROS_EPISODIO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_UTILIZA_EPISODIO         VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_EPISODIO_RN     NUMBER(10),
  NR_SEQ_SUBTIPO_EPISODIO_RN  NUMBER(10),
  IE_STATUS_RN                VARCHAR2(1 BYTE),
  IE_CARTAO_ID                NUMBER(3),
  CD_PERFIL                   NUMBER(5)         DEFAULT null,
  IE_GERA_TAXA_DIARIA         VARCHAR2(1 BYTE),
  IE_CASE_FECHADO             VARCHAR2(1 BYTE),
  IE_CASE_FUTURO              VARCHAR2(1 BYTE),
  IE_CHAMADA_DIAG             VARCHAR2(1 BYTE),
  IE_DPC                      VARCHAR2(1 BYTE),
  IE_MOST_EXP_DISEASE         VARCHAR2(2 BYTE),
  SI_DPC_DESCRIPTION          VARCHAR2(1 BYTE),
  IE_SHOW_CONFIRMATION        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAREPI_CASUEP_FK_I ON TASY.PARAMETROS_EPISODIO
(NR_SEQ_SUBTIPO_EPISODIO_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAREPI_CATPEP_FK_I ON TASY.PARAMETROS_EPISODIO
(NR_SEQ_TIPO_EPISODIO_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAREPI_ESTABEL_FK_I ON TASY.PARAMETROS_EPISODIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAREPI_PERFIL_FK_I ON TASY.PARAMETROS_EPISODIO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAREPI_PK ON TASY.PARAMETROS_EPISODIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAREPI_UK ON TASY.PARAMETROS_EPISODIO
(CD_ESTABELECIMENTO, CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PARAMETROS_EPISODIO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PARAMETROS_EPISODIO FOR EACH ROW
DECLARE

qt_registros_w	number(10);

BEGIN
	if (:new.CD_PERFIL is null and :new.CD_ESTABELECIMENTO is not null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_PERFIL is null
		and		CD_ESTABELECIMENTO = :new.CD_ESTABELECIMENTO;
	elsif (:new.CD_PERFIL is not null and :new.CD_ESTABELECIMENTO is null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_ESTABELECIMENTO is null
		and		CD_PERFIL = :new.CD_PERFIL;
	elsif (:new.CD_PERFIL is not null and :new.CD_ESTABELECIMENTO is not null) then
		select	count(1)
		into	qt_registros_w
		from	parametros_episodio
		where	CD_ESTABELECIMENTO = :new.CD_ESTABELECIMENTO
		and		CD_PERFIL = :new.CD_PERFIL;
	end if;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1070645);
	end if;
END;
/


ALTER TABLE TASY.PARAMETROS_EPISODIO ADD (
  CONSTRAINT PAREPI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETROS_EPISODIO ADD (
  CONSTRAINT PAREPI_CASUEP_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_EPISODIO_RN) 
 REFERENCES TASY.SUBTIPO_EPISODIO (NR_SEQUENCIA),
  CONSTRAINT PAREPI_CATPEP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EPISODIO_RN) 
 REFERENCES TASY.TIPO_EPISODIO (NR_SEQUENCIA),
  CONSTRAINT PAREPI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PAREPI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.PARAMETROS_EPISODIO TO NIVEL_1;

