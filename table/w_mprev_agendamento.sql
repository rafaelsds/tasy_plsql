ALTER TABLE TASY.W_MPREV_AGENDAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_MPREV_AGENDAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MPREV_AGENDAMENTO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_AGENDA                 NUMBER(10)          NOT NULL,
  NR_MINUTO_DURACAO         NUMBER(3)           NOT NULL,
  DT_AGENDA                 DATE                NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_AGENDAMENTO        NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  NR_SEQ_PARTICIPANTE       NUMBER(10),
  NR_SEQ_TURMA              NUMBER(10),
  NR_SEQ_STATUS_PAC         NUMBER(10),
  CD_AGENDA_LOCAL           NUMBER(10),
  CD_AGENDA_PROFISSIONAL    NUMBER(10),
  DS_UTILIZACAO             VARCHAR2(255 BYTE),
  IE_TIPO_ATENDIMENTO       VARCHAR2(1 BYTE),
  IE_FORMA_ATENDIMENTO      VARCHAR2(2 BYTE),
  NR_SEQ_PARTIC_CICLO_ITEM  NUMBER(10),
  NR_SEQ_MPREV_ATEND        NUMBER(10),
  NR_SEQ_CAPTACAO           NUMBER(10),
  IE_PREVISTO               VARCHAR2(1 BYTE),
  IE_TIPO_AGENDAMENTO       VARCHAR2(3 BYTE),
  NR_SEQ_ATIV_EXTRA         NUMBER(10),
  NR_SEQ_GRUPO_TEMA         NUMBER(10),
  NR_SEQ_MOTIVO_CANC        NUMBER(10),
  NR_SEQ_FORMA_ATEND        NUMBER(10),
  NR_SEQ_PAC_SENHA_FILA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WMPRAGE_AGENDA_FK_I ON TASY.W_MPREV_AGENDAMENTO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_AGMOCAN_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_ESTABEL_FK_I ON TASY.W_MPREV_AGENDAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRATEA_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_ATIV_EXTRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRATEN_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_MPREV_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRCAPT_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRFORA_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_FORMA_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRGCTU_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRGTEN_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_GRUPO_TEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRPACI_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_PARTIC_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_MPRPART_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_PACSEFI_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WMPRAGE_PK ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WMPRAGE_STAPAAG_FK_I ON TASY.W_MPREV_AGENDAMENTO
(NR_SEQ_STATUS_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.w_mprev_agendamento_update
before update ON TASY.W_MPREV_AGENDAMENTO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Limpar os campos do do BDPanel inferior no cadastro do agendamento na HDM - Agenda
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

if	(nvl(:new.ie_tipo_agendamento,'X') <> nvl(:old.ie_tipo_agendamento,'X')) or
	(nvl(:new.ie_tipo_atendimento,'X') <> nvl(:old.ie_tipo_atendimento,'X')) then

	:new.nr_seq_ativ_extra := null;
	:new.ie_previsto := null;
	:new.nr_seq_captacao := null;
	:new.nr_seq_turma := null;
	:new.nr_seq_grupo_tema := null;
	:new.ds_utilizacao := null;
	:new.nr_seq_participante := null;
	:new.nr_seq_partic_ciclo_item := null;

end if;

end;
/


ALTER TABLE TASY.W_MPREV_AGENDAMENTO ADD (
  CONSTRAINT WMPRAGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_MPREV_AGENDAMENTO ADD (
  CONSTRAINT WMPRAGE_MPRATEN_FK 
 FOREIGN KEY (NR_SEQ_MPREV_ATEND) 
 REFERENCES TASY.MPREV_ATENDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WMPRAGE_STAPAAG_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PAC) 
 REFERENCES TASY.STATUS_PACIENTE_AGENDA (NR_SEQUENCIA),
  CONSTRAINT WMPRAGE_MPRFORA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ATEND) 
 REFERENCES TASY.MPREV_FORMA_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT WMPRAGE_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WMPRAGE_AGMOCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.AGENDA_MOTIVO_CANCELAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_MPREV_AGENDAMENTO TO NIVEL_1;

