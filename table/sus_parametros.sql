ALTER TABLE TASY.SUS_PARAMETROS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_PARAMETROS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_PARAMETROS
(
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_COMPETENCIA_AIH      DATE                  NOT NULL,
  CD_ORGAO_EMISSOR_AIH    VARCHAR2(10 BYTE)     NOT NULL,
  CD_ORGAO_LOCAL_AIH      VARCHAR2(10 BYTE)     NOT NULL,
  CD_DIRETOR_CLINICO      VARCHAR2(10 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_MEDICO_AUDITOR       VARCHAR2(10 BYTE),
  CD_ORGAO_ORIGEM_BPA     VARCHAR2(7 BYTE),
  NM_ORGAO_ORIGEM_BPA     VARCHAR2(30 BYTE),
  NM_ORGAO_DESTINO_BPA    VARCHAR2(40 BYTE),
  IE_ORGAO_DESTINO_BPA    VARCHAR2(1 BYTE),
  CD_UNIDADE_BPA          NUMBER(10),
  NR_AIH_INICIAL          NUMBER(13),
  NR_AIH_FINAL            NUMBER(13),
  PR_CESARIANA_PERMITIDA  NUMBER(7,4),
  DT_COMPETENCIA_BPA      DATE,
  IE_VERSAO_BPA           VARCHAR2(20 BYTE),
  IE_VERSAO_AIH           VARCHAR2(20 BYTE),
  DT_COMPETENCIA_APAC     DATE,
  IE_SIGLA_APAC           VARCHAR2(6 BYTE),
  NM_ORGAO_DESTINO_APAC   VARCHAR2(40 BYTE),
  IE_ORGAO_DESTINO_APAC   VARCHAR2(1 BYTE),
  CD_UNIDADE_APAC         VARCHAR2(7 BYTE),
  CD_ORGAO_AUTORIZ_APAC   NUMBER(10),
  CD_CPF_AUTORIZ_APAC     NUMBER(11),
  IE_ALTERA_TIPO_SERV     VARCHAR2(1 BYTE)      NOT NULL,
  NR_APAC_INICIAL         NUMBER(13),
  NR_APAC_FINAL           NUMBER(13),
  DT_FINAL_PERIODO_APAC   DATE,
  CD_DIRETOR_UPS          VARCHAR2(10 BYTE),
  CD_CNS_DIRETOR          NUMBER(15),
  CD_CNS_RESPONSAVEL      NUMBER(15),
  CD_CNES                 NUMBER(7),
  CD_CGC_MANTENEDORA      NUMBER(14),
  IE_GERA_DIARIA_UTI      VARCHAR2(1 BYTE),
  QT_LEITO_UTI            NUMBER(3),
  CD_MEDICO_GESTOR        VARCHAR2(10 BYTE),
  CD_ORGAO_EMISSOR_SIHD   VARCHAR2(10 BYTE),
  IE_GERAR_PARTIC_CIGURG  VARCHAR2(1 BYTE)      NOT NULL,
  IE_GERA_LONGA_PERMAN    VARCHAR2(1 BYTE),
  QT_LEITO_TOTAL          NUMBER(4),
  QT_LEITO_CREDENCIADO    NUMBER(4),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_AUDITOR_LAUDO        VARCHAR2(1 BYTE),
  IE_SEPARA_CONTA_SETOR   VARCHAR2(1 BYTE),
  IE_DIARIA_SUS           VARCHAR2(1 BYTE),
  IE_REPASSE_PROC         VARCHAR2(1 BYTE),
  CD_PROC_DIARIA_SH       NUMBER(15),
  IE_ORIGEM_DIARIA_SH     NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSPARA_AUDITOR_FK_I ON TASY.SUS_PARAMETROS
(CD_MEDICO_AUDITOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPARA_MEDICO_FK_I ON TASY.SUS_PARAMETROS
(CD_DIRETOR_CLINICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPARA_MEDICO_FK5_I ON TASY.SUS_PARAMETROS
(CD_MEDICO_GESTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPARA_PESFISI_FK_I ON TASY.SUS_PARAMETROS
(CD_DIRETOR_UPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSPARA_PK ON TASY.SUS_PARAMETROS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPARA_PROCEDI_FK_I ON TASY.SUS_PARAMETROS
(CD_PROC_DIARIA_SH, IE_ORIGEM_DIARIA_SH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSPARA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUS_PARAMETROS_tp  after update ON TASY.SUS_PARAMETROS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_ESTABELECIMENTO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MEDICO_AUDITOR,1,4000),substr(:new.CD_MEDICO_AUDITOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_AUDITOR',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_DIARIA_SH,1,4000),substr(:new.IE_ORIGEM_DIARIA_SH,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_DIARIA_SH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_COMPETENCIA_AIH,1,4000),substr(:new.DT_COMPETENCIA_AIH,1,4000),:new.nm_usuario,nr_seq_w,'DT_COMPETENCIA_AIH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ORGAO_EMISSOR_AIH,1,4000),substr(:new.CD_ORGAO_EMISSOR_AIH,1,4000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_EMISSOR_AIH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DIRETOR_CLINICO,1,4000),substr(:new.CD_DIRETOR_CLINICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DIRETOR_CLINICO',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ORGAO_ORIGEM_BPA,1,4000),substr(:new.CD_ORGAO_ORIGEM_BPA,1,4000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_ORIGEM_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORGAO_DESTINO_BPA,1,4000),substr(:new.IE_ORGAO_DESTINO_BPA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORGAO_DESTINO_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_BPA,1,4000),substr(:new.CD_UNIDADE_BPA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AIH_FINAL,1,4000),substr(:new.NR_AIH_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_AIH_FINAL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_CESARIANA_PERMITIDA,1,4000),substr(:new.PR_CESARIANA_PERMITIDA,1,4000),:new.nm_usuario,nr_seq_w,'PR_CESARIANA_PERMITIDA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_BPA,1,4000),substr(:new.IE_VERSAO_BPA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_COMPETENCIA_BPA,1,4000),substr(:new.DT_COMPETENCIA_BPA,1,4000),:new.nm_usuario,nr_seq_w,'DT_COMPETENCIA_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AIH_INICIAL,1,4000),substr(:new.NR_AIH_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_AIH_INICIAL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ORGAO_ORIGEM_BPA,1,4000),substr(:new.NM_ORGAO_ORIGEM_BPA,1,4000),:new.nm_usuario,nr_seq_w,'NM_ORGAO_ORIGEM_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ORGAO_AUTORIZ_APAC,1,4000),substr(:new.CD_ORGAO_AUTORIZ_APAC,1,4000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_AUTORIZ_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORGAO_DESTINO_APAC,1,4000),substr(:new.IE_ORGAO_DESTINO_APAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORGAO_DESTINO_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CPF_AUTORIZ_APAC,1,4000),substr(:new.CD_CPF_AUTORIZ_APAC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CPF_AUTORIZ_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_AIH,1,4000),substr(:new.IE_VERSAO_AIH,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_AIH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SIGLA_APAC,1,4000),substr(:new.IE_SIGLA_APAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_SIGLA_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ORGAO_DESTINO_APAC,1,4000),substr(:new.NM_ORGAO_DESTINO_APAC,1,4000),:new.nm_usuario,nr_seq_w,'NM_ORGAO_DESTINO_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_APAC,1,4000),substr(:new.CD_UNIDADE_APAC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ORGAO_LOCAL_AIH,1,4000),substr(:new.CD_ORGAO_LOCAL_AIH,1,4000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_LOCAL_AIH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ORGAO_DESTINO_BPA,1,4000),substr(:new.NM_ORGAO_DESTINO_BPA,1,4000),:new.nm_usuario,nr_seq_w,'NM_ORGAO_DESTINO_BPA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DIRETOR_UPS,1,4000),substr(:new.CD_DIRETOR_UPS,1,4000),:new.nm_usuario,nr_seq_w,'CD_DIRETOR_UPS',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNS_DIRETOR,1,4000),substr(:new.CD_CNS_DIRETOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNS_DIRETOR',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNS_RESPONSAVEL,1,4000),substr(:new.CD_CNS_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNS_RESPONSAVEL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_COMPETENCIA_APAC,1,4000),substr(:new.DT_COMPETENCIA_APAC,1,4000),:new.nm_usuario,nr_seq_w,'DT_COMPETENCIA_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_APAC_INICIAL,1,4000),substr(:new.NR_APAC_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_APAC_INICIAL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_APAC_FINAL,1,4000),substr(:new.NR_APAC_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_APAC_FINAL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FINAL_PERIODO_APAC,1,4000),substr(:new.DT_FINAL_PERIODO_APAC,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL_PERIODO_APAC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTERA_TIPO_SERV,1,4000),substr(:new.IE_ALTERA_TIPO_SERV,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTERA_TIPO_SERV',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LEITO_UTI,1,4000),substr(:new.QT_LEITO_UTI,1,4000),:new.nm_usuario,nr_seq_w,'QT_LEITO_UTI',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNES,1,4000),substr(:new.CD_CNES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNES',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_MANTENEDORA,1,4000),substr(:new.CD_CGC_MANTENEDORA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_MANTENEDORA',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_DIARIA_UTI,1,4000),substr(:new.IE_GERA_DIARIA_UTI,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_DIARIA_UTI',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_GESTOR,1,4000),substr(:new.CD_MEDICO_GESTOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_GESTOR',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LEITO_TOTAL,1,4000),substr(:new.QT_LEITO_TOTAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_LEITO_TOTAL',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LEITO_CREDENCIADO,1,4000),substr(:new.QT_LEITO_CREDENCIADO,1,4000),:new.nm_usuario,nr_seq_w,'QT_LEITO_CREDENCIADO',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_PARTIC_CIGURG,1,4000),substr(:new.IE_GERAR_PARTIC_CIGURG,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_PARTIC_CIGURG',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_LONGA_PERMAN,1,4000),substr(:new.IE_GERA_LONGA_PERMAN,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_LONGA_PERMAN',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ORGAO_EMISSOR_SIHD,1,4000),substr(:new.CD_ORGAO_EMISSOR_SIHD,1,4000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_EMISSOR_SIHD',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITOR_LAUDO,1,4000),substr(:new.IE_AUDITOR_LAUDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITOR_LAUDO',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEPARA_CONTA_SETOR,1,4000),substr(:new.IE_SEPARA_CONTA_SETOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEPARA_CONTA_SETOR',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIARIA_SUS,1,4000),substr(:new.IE_DIARIA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIARIA_SUS',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REPASSE_PROC,1,4000),substr(:new.IE_REPASSE_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_REPASSE_PROC',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_DIARIA_SH,1,4000),substr(:new.CD_PROC_DIARIA_SH,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_DIARIA_SH',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUS_PARAMETROS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUS_PARAMETROS ADD (
  CONSTRAINT SUSPARA_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_PARAMETROS ADD (
  CONSTRAINT SUSPARA_AUDITOR_FK 
 FOREIGN KEY (CD_MEDICO_AUDITOR) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSPARA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SUSPARA_MEDICO_FK 
 FOREIGN KEY (CD_DIRETOR_CLINICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSPARA_PESFISI_FK 
 FOREIGN KEY (CD_DIRETOR_UPS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSPARA_MEDICO_FK5 
 FOREIGN KEY (CD_MEDICO_GESTOR) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSPARA_PROCEDI_FK 
 FOREIGN KEY (CD_PROC_DIARIA_SH, IE_ORIGEM_DIARIA_SH) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.SUS_PARAMETROS TO NIVEL_1;

