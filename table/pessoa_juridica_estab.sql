ALTER TABLE TASY.PESSOA_JURIDICA_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA_ESTAB
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_CGC                       VARCHAR2(14 BYTE) NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  NM_PESSOA_CONTATO            VARCHAR2(255 BYTE),
  NR_RAMAL_CONTATO             NUMBER(5),
  DS_EMAIL                     VARCHAR2(255 BYTE),
  CD_COND_PAGTO                NUMBER(10),
  QT_DIA_PRAZO_ENTREGA         NUMBER(3),
  VL_MINIMO_NF                 NUMBER(15,2),
  IE_QUALIDADE                 VARCHAR2(6 BYTE),
  CD_REFERENCIA_FORNEC         VARCHAR2(20 BYTE),
  IE_FORMA_PAGTO               VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_CONTA_DIF_NF              VARCHAR2(1 BYTE) NOT NULL,
  CD_INTERNO_LAB               VARCHAR2(20 BYTE),
  CD_INTERF_LAB_LOTE_ENVIO     NUMBER(10),
  CD_INTERF_LAB_LOTE_RET       NUMBER(10),
  NR_SEQ_XML_LAB_ENVIO         NUMBER(10),
  NR_SEQ_XML_LAB_RET           NUMBER(10),
  CD_PORTADOR                  NUMBER(10),
  CD_TIPO_PORTADOR             NUMBER(5),
  CD_TIPO_BAIXA                NUMBER(5),
  NR_CODIGO_SERV_PREST         VARCHAR2(10 BYTE),
  TX_DESC_ANTECIPACAO          NUMBER(7,4),
  CD_INTERF_PLS_MENSALIDADE    NUMBER(10),
  IE_RATEIO_ADIANT             VARCHAR2(2 BYTE),
  IE_RETEM_ISS                 VARCHAR2(1 BYTE),
  TX_DESC_ORDEM                NUMBER(7,4),
  IE_ENDERECO_CORRESPONDENCIA  NUMBER(2),
  PR_DESC_FINANCEIRO           NUMBER(13,4),
  CD_SISTEMA_ANT               VARCHAR2(255 BYTE),
  IE_FRETE                     VARCHAR2(1 BYTE),
  IE_EXIGE_ANEXO_NF            VARCHAR2(15 BYTE),
  IE_BAIXA_ABAT                VARCHAR2(1 BYTE),
  IE_EXIGE_ORDEM_COMPRA        VARCHAR2(1 BYTE),
  TX_DESC_ASSIDUIDADE          NUMBER(7,4),
  CD_OPERACAO_NF               NUMBER(4),
  IE_CONSISTE_NR_SERIE_NF      VARCHAR2(1 BYTE),
  IE_STATUS_APENADO            VARCHAR2(15 BYTE),
  IE_REC_EMAIL_COMERCIAL       VARCHAR2(1 BYTE),
  IE_MOEDA_ESTRANGEIRA         VARCHAR2(1 BYTE),
  DS_SENHA_INTERNO_LAB         VARCHAR2(64 BYTE),
  PR_DESC_FINANCEIRO_NF        NUMBER(13,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESJUES_CONPAGA_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_COND_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_ESTABEL_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUES_INTERFA_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_INTERF_LAB_LOTE_ENVIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_INTERFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_INTERFA_FK2_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_INTERF_LAB_LOTE_RET)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_INTERFA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_INTERFA_FK3_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_INTERF_PLS_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_INTERFA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_OPENOTA_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_OPENOTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_PESJURI_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESJUES_PK ON TASY.PESSOA_JURIDICA_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUES_PORTADO_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUES_TIPBACP_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(CD_TIPO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUES_TIPBACP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESJUES_UK ON TASY.PESSOA_JURIDICA_ESTAB
(CD_CGC, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUES_XMLPROJ_FK_I ON TASY.PESSOA_JURIDICA_ESTAB
(NR_SEQ_XML_LAB_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUES_XMLPROJ_FK2_I ON TASY.PESSOA_JURIDICA_ESTAB
(NR_SEQ_XML_LAB_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_estab_delete
after delete ON TASY.PESSOA_JURIDICA_ESTAB for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

select  obter_tipo_pessoa_juridica(:old.cd_cgc)
into	cd_tipo_pessoa_w
from    dual;

reg_integracao_p.ie_operacao		:=	'A';
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :old.cd_cgc,:old.nm_usuario, reg_integracao_p);

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_estab_atual
before insert or update ON TASY.PESSOA_JURIDICA_ESTAB for each row
declare
ds_razao_social_w				pessoa_juridica.ds_razao_social%type;
nm_fantasia_w				varchar2(80);
ds_nome_abrev_w				varchar2(50);
cd_municipio_ibge_w			varchar2(6);
cd_cep_w				varchar2(15);
ds_endereco_w				varchar2(40);
nr_endereco_w				varchar2(10);
ds_complemento_w				varchar2(255);
ds_bairro_w				varchar2(40);
ds_municipio_w				varchar2(40);
sg_estado_w				pessoa_juridica.sg_estado%type;
nr_telefone_w				varchar2(15);
nr_fax_w					varchar2(15);
nr_inscricao_estadual_w			varchar2(20);
nr_inscricao_municipal_w			varchar2(20);
cd_internacional_w				varchar2(20);
ds_site_internet_w				varchar2(255);
cd_pf_resp_tecnico_w			varchar2(10);
ds_orgao_reg_resp_tecnico_w		varchar2(10);
nr_autor_func_w				pessoa_juridica.nr_autor_func%type;
nr_alvara_sanitario_w			varchar2(20);
nr_alvara_sanitario_munic_w			varchar2(20);
nr_certificado_boas_prat_w			varchar2(20);
cd_ans_w				varchar2(20);
dt_validade_autor_func_w			date;
dt_validade_alvara_sanit_w			date;
dt_validade_cert_boas_prat_w		date;
cd_cnes_w				varchar2(20);
dt_validade_resp_tecnico_w			date;
nr_registro_resp_tecnico_w			varchar2(20);
dt_validade_alvara_munic_w			date;
ds_resp_tecnico_w				varchar2(255);
qt_existe_w				number(10);
begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pj b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PJ'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.nm_usuario <> 'INTEGR_TASY') then

	select	ds_razao_social,
		nm_fantasia,
		ds_nome_abrev,
		cd_municipio_ibge,
		cd_cep,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_bairro,
		ds_municipio,
		sg_estado,
		nr_telefone,
		nr_fax,
		nr_inscricao_estadual,
		nr_inscricao_municipal,
		cd_internacional,
		ds_site_internet,
		cd_pf_resp_tecnico,
		ds_orgao_reg_resp_tecnico,
		nr_autor_func,
		nr_alvara_sanitario,
		nr_alvara_sanitario_munic,
		nr_certificado_boas_prat,
		cd_ans,
		dt_validade_autor_func,
		dt_validade_alvara_sanit,
		dt_validade_cert_boas_prat,
		cd_cnes,
		dt_validade_resp_tecnico,
		nr_registro_resp_tecnico,
		dt_validade_alvara_munic,
		ds_resp_tecnico
	into	ds_razao_social_w,
		nm_fantasia_w,
		ds_nome_abrev_w,
		cd_municipio_ibge_w,
		cd_cep_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_bairro_w,
		ds_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		nr_fax_w,
		nr_inscricao_estadual_w,
		nr_inscricao_municipal_w,
		cd_internacional_w,
		ds_site_internet_w,
		cd_pf_resp_tecnico_w,
		ds_orgao_reg_resp_tecnico_w,
		nr_autor_func_w,
		nr_alvara_sanitario_w,
		nr_alvara_sanitario_munic_w,
		nr_certificado_boas_prat_w,
		cd_ans_w,
		dt_validade_autor_func_w,
		dt_validade_alvara_sanit_w,
		dt_validade_cert_boas_prat_w,
		cd_cnes_w,
		dt_validade_resp_tecnico_w,
		nr_registro_resp_tecnico_w,
		dt_validade_alvara_munic_w,
		ds_resp_tecnico_w
	from	pessoa_juridica
	where	cd_cgc = :new.cd_cgc;

	insert into sup_int_pj(
		nr_sequencia,
		ie_forma_integracao,
		dt_liberacao,
		cd_cgc,
		ds_razao_social,
		nm_fantasia,
		ds_nome_abrev,
		cd_municipio_ibge,
		cd_cep,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_bairro,
		ds_municipio,
		sg_estado,
		nr_telefone,
		nr_fax,
		nr_inscricao_estadual,
		nr_inscricao_municipal,
		cd_internacional,
		ds_site_internet,
		cd_pf_resp_tecnico,
		ds_orgao_reg_resp_tecnico,
		nr_autor_func,
		nr_alvara_sanitario,
		nr_alvara_sanitario_munic,
		nr_certificado_boas_prat,
		cd_ans,
		dt_validade_autor_func,
		dt_validade_alvara_sanit,
		dt_validade_cert_boas_prat,
		cd_cnes,
		dt_validade_resp_tecnico,
		nr_registro_resp_tecnico,
		dt_validade_alvara_munic,
		ds_resp_tecnico,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_referencia_fornec,
		ds_email,
		nm_pessoa_contato,
		nr_ramal_contato,
		qt_dia_prazo_entrega,
		vl_minimo_nf)
	values(	sup_int_pj_seq.nextval,
		'E',
		sysdate,
		:new.cd_cgc,
		substr(ds_razao_social_w,1,80),
		nm_fantasia_w,
		ds_nome_abrev_w,
		cd_municipio_ibge_w,
		cd_cep_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_bairro_w,
		ds_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		nr_fax_w,
		nr_inscricao_estadual_w,
		nr_inscricao_municipal_w,
		cd_internacional_w,
		ds_site_internet_w,
		cd_pf_resp_tecnico_w,
		ds_orgao_reg_resp_tecnico_w,
		nr_autor_func_w,
		nr_alvara_sanitario_w,
		nr_alvara_sanitario_munic_w,
		nr_certificado_boas_prat_w,
		cd_ans_w,
		dt_validade_autor_func_w,
		dt_validade_alvara_sanit_w,
		dt_validade_cert_boas_prat_w,
		cd_cnes_w,
		dt_validade_resp_tecnico_w,
		nr_registro_resp_tecnico_w,
		dt_validade_alvara_munic_w,
		ds_resp_tecnico_w,
		sysdate,
		'INTEGR_TASY',
		sysdate,
		'INTEGR_TASY',
		:new.cd_referencia_fornec,
		:new.ds_email,
		:new.nm_pessoa_contato,
		:new.nr_ramal_contato,
		:new.qt_dia_prazo_entrega,
		:new.vl_minimo_nf);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_estab_a400_up
before update ON TASY.PESSOA_JURIDICA_ESTAB for each row
declare
nr_seq_prestador_w	pls_prestador.nr_sequencia%type;
ie_divulga_email_w	pls_prestador.ie_divulga_email%type;
qt_email_w		pls_integer;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	((:new.ds_email <> :old.ds_email) or
		 (:new.ds_email is null and :old.ds_email is not null))then

		select	max(ie_divulga_email),
			max(nr_sequencia)
		into	ie_divulga_email_w,
			nr_seq_prestador_w
		from	pls_prestador
		where	cd_cgc = :new.cd_cgc
		and	ie_divulga_email	= 'S'
		and	ie_ptu_a400		= 'S';

		if	(nr_seq_prestador_w is not null) and
			(nvl(ie_divulga_email_w, 'N') = 'S') and
			(nvl(obter_valor_param_usuario(1323, 6, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'S') = 'N') then

			select	count(1)
			into	qt_email_w
			from	ptu_prestador a,
				ptu_prestador_endereco b
			where	a.nr_sequencia = b.nr_seq_prestador
			and	a.nr_seq_prestador = nr_seq_prestador_w
			and	b.ds_email = :old.ds_email
			and	b.ie_tipo_endereco_original = 'PJ';

			if	(qt_email_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(387909);
			end if;
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_estab_after
after insert or update  ON TASY.PESSOA_JURIDICA_ESTAB for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_opcao_w	varchar2(1) := 'I';
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

if 	(updating) then
	ie_opcao_w	:= 'A';
end if;

select 	max(cd_tipo_pessoa)
into	cd_tipo_pessoa_w
from 	pessoa_juridica
where 	cd_cgc = :new.cd_cgc
and 	ie_situacao = 'A';

reg_integracao_p.ie_operacao		:=	ie_opcao_w;
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :new.cd_cgc,:new.nm_usuario, reg_integracao_p);

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JURIDICA_ESTAB_tp  after update ON TASY.PESSOA_JURIDICA_ESTAB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_COND_PAGTO,1,4000),substr(:new.CD_COND_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_COND_PAGTO',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_PRAZO_ENTREGA,1,4000),substr(:new.QT_DIA_PRAZO_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_PRAZO_ENTREGA',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRETE,1,4000),substr(:new.IE_FRETE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRETE',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_PAGTO,1,4000),substr(:new.IE_FORMA_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_PAGTO',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PESSOA_CONTATO,1,4000),substr(:new.NM_PESSOA_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_CONTATO',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL_CONTATO,1,4000),substr(:new.NR_RAMAL_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL_CONTATO',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO_NF,1,4000),substr(:new.VL_MINIMO_NF,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO_NF',ie_log_w,ds_w,'PESSOA_JURIDICA_ESTAB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JURIDICA_ESTAB ADD (
  CONSTRAINT PESJUES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESJUES_UK
 UNIQUE (CD_CGC, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JURIDICA_ESTAB ADD (
  CONSTRAINT PESJUES_INTERFA_FK 
 FOREIGN KEY (CD_INTERF_LAB_LOTE_ENVIO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT PESJUES_INTERFA_FK2 
 FOREIGN KEY (CD_INTERF_LAB_LOTE_RET) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT PESJUES_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_XML_LAB_ENVIO) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PESJUES_XMLPROJ_FK2 
 FOREIGN KEY (NR_SEQ_XML_LAB_RET) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PESJUES_CONPAGA_FK 
 FOREIGN KEY (CD_COND_PAGTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PESJUES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESJUES_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PESJUES_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT PESJUES_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA),
  CONSTRAINT PESJUES_INTERFA_FK3 
 FOREIGN KEY (CD_INTERF_PLS_MENSALIDADE) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT PESJUES_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF));

GRANT SELECT ON TASY.PESSOA_JURIDICA_ESTAB TO NIVEL_1;

