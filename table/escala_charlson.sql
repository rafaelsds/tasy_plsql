ALTER TABLE TASY.ESCALA_CHARLSON
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CHARLSON CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CHARLSON
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  IE_INSUF_CARDIAC_CONG     VARCHAR2(1 BYTE)    NOT NULL,
  IE_INF_MIOCARDIO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_VASC_PER        VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_CEREB_VASC      VARCHAR2(1 BYTE)    NOT NULL,
  IE_DEMENCIA               VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_PULM_CRONICA    VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_TEC_CONJUNTIVO  VARCHAR2(1 BYTE)    NOT NULL,
  IE_ULCERA                 VARCHAR2(1 BYTE)    NOT NULL,
  IE_CIRROSE                VARCHAR2(1 BYTE)    NOT NULL,
  IE_DIABETES_SEM_COMPL     VARCHAR2(1 BYTE)    NOT NULL,
  IE_DEPRESSAO              VARCHAR2(1 BYTE)    NOT NULL,
  IE_VARFARINA              VARCHAR2(1 BYTE)    NOT NULL,
  IE_HIPERTENSAO            VARCHAR2(1 BYTE)    NOT NULL,
  IE_HEMIPLEGIA             VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_RENAL           VARCHAR2(1 BYTE)    NOT NULL,
  IE_DIABETES_COM_COMPL     VARCHAR2(1 BYTE)    NOT NULL,
  IE_TUMOR                  VARCHAR2(1 BYTE)    NOT NULL,
  IE_LEUCEMIA               VARCHAR2(1 BYTE)    NOT NULL,
  IE_LINFOMA                VARCHAR2(1 BYTE)    NOT NULL,
  IE_ULCERA_CELULITE        VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_FIGADO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_TUMOR_MALIGNO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_AIDS                   VARCHAR2(1 BYTE)    NOT NULL,
  QT_PONTUACAO              NUMBER(3),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  QT_PONTUACAO_IDADE        NUMBER(3),
  NR_HORA                   NUMBER(2),
  NR_SEQ_REG_ELEMENTO       NUMBER(10),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCHAR_ATEPACI_FK_I ON TASY.ESCALA_CHARLSON
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHAR_EHRREEL_FK_I ON TASY.ESCALA_CHARLSON
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCHAR_EHRREEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ESCCHAR_PK ON TASY.ESCALA_CHARLSON
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCHAR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_charlson_tr
before insert or update ON TASY.ESCALA_CHARLSON for each row
declare
qt_ponto_w	number(3) := 0;
qt_reg_w	number(1);
qt_idade_w	number(10);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto final;
end if;
qt_idade_w	:= obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),sysdate,'A');
--pontos = 1
if (nvl(:new.ie_insuf_cardiac_cong,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_inf_miocardio,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_doenca_vasc_per,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_doenca_cereb_vasc,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_demencia,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_doenca_pulm_cronica,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_doenca_tec_conjuntivo,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_ulcera,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_cirrose,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_diabetes_sem_compl,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_depressao,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_varfarina,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_hipertensao,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

--pontos = 2
if (nvl(:new.ie_hemiplegia,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_doenca_renal,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_diabetes_com_compl,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_tumor,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_leucemia,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_linfoma,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

if (nvl(:new.ie_ulcera_celulite,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

--pontos = 3
if (nvl(:new.ie_doenca_figado,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 3;
end if;

--pontos = 6
if (nvl(:new.ie_tumor_maligno,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 6;
end if;

if (nvl(:new.ie_aids,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 6;
end if;

:new.qt_pontuacao := qt_ponto_w;

:new.QT_PONTUACAO_IDADE	:= qt_ponto_w;

if	(qt_idade_w	>=50) and
	(qt_idade_w	<=59) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 1;
elsif	(qt_idade_w	>=60) and
	(qt_idade_w	<=69) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 2;
elsif	(qt_idade_w	>=70) and
	(qt_idade_w	<=79) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 3;
elsif	(qt_idade_w	>=80) and
	(qt_idade_w	<=89) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 4;
elsif	(qt_idade_w	>=90) and
	(qt_idade_w	<=99) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 5;
elsif	(qt_idade_w	>=100) and
	(qt_idade_w	<=109) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 6;
elsif	(qt_idade_w	>=110) and
	(qt_idade_w	<=119) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 7;
elsif	(qt_idade_w	>=120) and
	(qt_idade_w	<=129) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 8;
elsif	(qt_idade_w	>=130) and
	(qt_idade_w	<=139) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 9;
elsif	(qt_idade_w	>=140) and
	(qt_idade_w	<=149) then
	:new.QT_PONTUACAO_IDADE	:= :new.QT_PONTUACAO_IDADE + 10;
end if;
<<final>>
qt_reg_w := 0;
end escala_charlson_tr;
/


ALTER TABLE TASY.ESCALA_CHARLSON ADD (
  CONSTRAINT ESCCHAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CHARLSON ADD (
  CONSTRAINT ESCCHAR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCHAR_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_CHARLSON TO NIVEL_1;

