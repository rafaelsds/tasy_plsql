ALTER TABLE TASY.REGRA_REPASSE_PONT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_REPASSE_PONT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_REPASSE_PONT_ITEM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_REP_PONTO   NUMBER(10)           NOT NULL,
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  DT_FIM_VIGENCIA          DATE,
  QT_ITEM_PERIODO          NUMBER(10),
  IE_TIPO_PERIODO_REPASSE  VARCHAR2(15 BYTE),
  IE_FORMA_CONS_QTD_ITEM   VARCHAR2(15 BYTE),
  IE_TIPO_DATA_ITEM        VARCHAR2(15 BYTE),
  VL_INICIAL_FAIXA         NUMBER(15,2),
  VL_FINAL_FAIXA           NUMBER(15,2),
  QT_PONTO_FAIXA           NUMBER(10),
  PR_REPASSAR              NUMBER(15,2),
  QT_PONTO_ITEM            NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REREPOI_PK ON TASY.REGRA_REPASSE_PONT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REREPOI_REREPPO_FK_I ON TASY.REGRA_REPASSE_PONT_ITEM
(NR_SEQ_REGRA_REP_PONTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_REPASSE_PONT_ITEM ADD (
  CONSTRAINT REREPOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_REPASSE_PONT_ITEM ADD (
  CONSTRAINT REREPOI_REREPPO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_REP_PONTO) 
 REFERENCES TASY.REGRA_REPASSE_PONTUACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_REPASSE_PONT_ITEM TO NIVEL_1;

