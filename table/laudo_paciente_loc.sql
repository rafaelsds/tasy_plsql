ALTER TABLE TASY.LAUDO_PACIENTE_LOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE_LOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE_LOC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_LOC             NUMBER(6)              NOT NULL,
  IE_TIPO_LOCAL          VARCHAR2(3 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  DT_RETIRADA            DATE,
  NM_PESSOA_FISICA       VARCHAR2(60 BYTE),
  CD_SETOR_REGISTRO      NUMBER(5),
  IE_COM_PROTOCOLO       VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_JUST_INATIVACAO     VARCHAR2(80 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  CD_PESSOA_RECEBIDO     VARCHAR2(10 BYTE),
  DS_SENHA               VARCHAR2(20 BYTE),
  CD_PESSOA_ENTREGA      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUPALO_LAUPACI_FK_I ON TASY.LAUDO_PACIENTE_LOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPALO_PESFISI_FK_I ON TASY.LAUDO_PACIENTE_LOC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPALO_PESFISI_FK2_I ON TASY.LAUDO_PACIENTE_LOC
(CD_PESSOA_RECEBIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPALO_PESFISI_FK3_I ON TASY.LAUDO_PACIENTE_LOC
(CD_PESSOA_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAUPALO_PK ON TASY.LAUDO_PACIENTE_LOC
(NR_SEQUENCIA, NR_SEQ_LOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPALO_SETATEND_FK_I ON TASY.LAUDO_PACIENTE_LOC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPALO_SETATEND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPALO_SETATEN_FK2_I ON TASY.LAUDO_PACIENTE_LOC
(CD_SETOR_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPALO_SETATEN_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Laudo_Paciente_Loc_Atual
AFTER INSERT OR UPDATE ON TASY.LAUDO_PACIENTE_LOC FOR EACH ROW
DECLARE

dt_liberacao_w	Date;
qt_reg_w	number(1);
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select 	max(dt_liberacao)
into	dt_liberacao_w
from 	laudo_paciente
where	nr_sequencia	= :new.nr_sequencia;
gravar_log_cdi(55411,'Fun��o -' || obter_funcao_ativa || ' Perfil -' || obter_perfil_ativo,:new.nm_usuario);
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_loc_update
BEFORE UPDATE ON TASY.LAUDO_PACIENTE_LOC FOR EACH ROW
declare
qt_regra_w		number(10,0);
nr_seq_regra_w		number(10,0);
ds_assunto_w		varchar2(100);
ds_email_origem_w	varchar2(100);
nm_usuario_ref_w	varchar2(15);
ds_conteudo_w		varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);
ds_email_destino_w	varchar2(255);
ds_titulo_laudo_w	varchar2(255);
ds_local_w		varchar2(255);
ds_setor_atendimento_w	varchar2(255);
dt_exame_w		date;
ds_exames_w		varchar2(1000) := '';
ds_procedimento_w	varchar2(255);
ie_email_valido_w	varchar2(2);

Cursor C01 is
	select  substr(obter_desc_procedimento(a.cd_procedimento, a.ie_origem_proced),1,255)
	from  	procedimento_paciente a
	where	nr_laudo = :new.nr_sequencia;

BEGIN

if	(:old.ie_tipo_local <> :new.ie_tipo_local) then
	begin

	select	count(*)
	into	qt_regra_w
	from	regra_envio_loc_laudo
	where	ie_tipo_local = :new.ie_tipo_local
	and	ie_situacao = 'A';

	if	(qt_regra_w > 0) then
		begin

		select	nvl(max(nr_sequencia),0)
		into	nr_seq_regra_w
		from	regra_envio_loc_laudo
		where	ie_tipo_local = :new.ie_tipo_local
		and	ie_situacao = 'A';

		if	(nr_seq_regra_w <> 0) then
			begin

			select	ds_titulo,
				ds_email_origem,
				nvl(nm_usuario_ref, :new.nm_usuario),
				ds_email
			into	ds_assunto_w,
				ds_email_origem_w,
				nm_usuario_ref_w,
				ds_conteudo_w
			from	regra_envio_loc_laudo
			where	nr_sequencia = nr_seq_regra_w;

			select	max(nvl(nvl(obter_dados_atendimento(nr_atendimento, 'CP'), obter_dados_prescricao(nr_prescricao, 'P')), cd_pessoa_fisica)),
				max(ds_titulo_laudo),
				max(dt_exame)
			into	cd_pessoa_fisica_w,
				ds_titulo_laudo_w,
				dt_exame_w
			from	laudo_paciente
			where	nr_sequencia = :new.nr_sequencia;

			if	(cd_pessoa_fisica_w is not null) then
				begin

				ds_email_destino_w	:= substr(obter_compl_pf(cd_pessoa_fisica_w, 1, 'M'),1,255);

				if	(ds_assunto_w is not null) and
					(ds_conteudo_w is not null) and
					(ds_email_destino_w is not null) and
					(nm_usuario_ref_w is not null) then
					begin

					select substr(obter_valor_dominio(138,:new.ie_tipo_local),1,255)
					into   ds_local_w
					from   dual;

					select  nvl(max(ds_setor_atendimento),'')
					into	ds_setor_atendimento_w
					from 	setor_atendimento
					where 	cd_setor_atendimento = :new.cd_setor_registro;

					open C01;
					loop
					fetch C01 into
						ds_procedimento_w;
					exit when C01%notfound;
						begin
						ds_exames_w := ds_exames_w || ' ' ||  ds_procedimento_w;
						end;
					end loop;
					close C01;

					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@TITULO@',ds_titulo_laudo_w),1,2000);
					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@LOCAL@',ds_local_w),1,2000);
					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@SETOR@',ds_setor_atendimento_w),1,2000);
					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@PACIENTE@', obter_nome_pf(cd_pessoa_fisica_w)),1,2000);
					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@DTEXAME@', to_char(dt_exame_w,'DD/MM/YYYY HH24:MI:SS')),1,2000);
					ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@EXAME@', ds_exames_w),1,2000);

					select obter_se_email_valido(ds_email_destino_w)
					into	ie_email_valido_w
					from	dual;

					if	(ie_email_valido_w = 'S') then
						enviar_email(ds_assunto_w, ds_conteudo_w, ds_email_origem_w, ds_email_destino_w, nm_usuario_ref_w, 'M');
					end if;


					insert into laudo_paciente_loc_envio (
							nr_sequencia,
							dt_envio,
							nm_usuario,
							ds_email_origem,
							ds_email_destino,
							ds_conteudo,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_loc)
					values(		laudo_paciente_loc_envio_seq.nextval,
							sysdate,
							nm_usuario_ref_w,
							ds_email_origem_w,
							ds_email_destino_w,
							ds_conteudo_w,
							sysdate,
							sysdate,
							:new.nm_usuario,
							:new.nr_seq_loc);
					end;
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_loc_insert
AFTER INSERT ON TASY.LAUDO_PACIENTE_LOC FOR EACH ROW
declare
qt_regra_w		number(10,0);
nr_seq_regra_w		number(10,0);
ds_assunto_w		varchar2(100);
ds_email_origem_w	varchar2(100);
nm_usuario_ref_w	varchar2(15);
ds_conteudo_w		varchar2(2000);
cd_pessoa_fisica_w	varchar2(10);
ds_email_destino_w	varchar2(255);
ds_titulo_laudo_w	varchar2(255);
ds_local_w		varchar2(255);
ds_setor_atendimento_w	varchar2(255);
dt_exame_w		date;
ds_exames_w		varchar2(1000) := '';
ds_procedimento_w	varchar2(255);
ie_email_valido_w	varchar2(2);

Cursor C01 is
	select  substr(obter_desc_procedimento(a.cd_procedimento, a.ie_origem_proced),1,255)
	from  	procedimento_paciente a
	where	nr_laudo = :new.nr_sequencia;

BEGIN

select	count(*)
into	qt_regra_w
from	regra_envio_loc_laudo
where	ie_tipo_local = :new.ie_tipo_local
and	ie_situacao = 'A';

if	(qt_regra_w > 0) then
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_regra_w
	from	regra_envio_loc_laudo
	where	ie_tipo_local = :new.ie_tipo_local
	and	ie_situacao = 'A';

	if	(nr_seq_regra_w <> 0) then
		begin

		select	ds_titulo,
			ds_email_origem,
			nvl(nm_usuario_ref, :new.nm_usuario),
			ds_email
		into	ds_assunto_w,
			ds_email_origem_w,
			nm_usuario_ref_w,
			ds_conteudo_w
		from	regra_envio_loc_laudo
		where	nr_sequencia = nr_seq_regra_w;

		select	max(nvl(nvl(obter_dados_atendimento(nr_atendimento, 'CP'), obter_dados_prescricao(nr_prescricao, 'P')), cd_pessoa_fisica)),
			max(ds_titulo_laudo),
			max(dt_exame)
		into	cd_pessoa_fisica_w,
			ds_titulo_laudo_w,
			dt_exame_w
		from	laudo_paciente
		where	nr_sequencia = :new.nr_sequencia;

		if	(cd_pessoa_fisica_w is not null) then
			begin

			ds_email_destino_w	:= substr(obter_compl_pf(cd_pessoa_fisica_w, 1, 'M'),1,255);

			if	(ds_assunto_w is not null) and
				(ds_conteudo_w is not null) and
				(ds_email_destino_w is not null) and
				(nm_usuario_ref_w is not null) then
				begin

				select substr(obter_valor_dominio(138,:new.ie_tipo_local),1,255)
				into   ds_local_w
				from   dual;

				select  nvl(max(ds_setor_atendimento),'')
				into	ds_setor_atendimento_w
				from 	setor_atendimento
				where 	cd_setor_atendimento = :new.cd_setor_registro;

				open C01;
				loop
				fetch C01 into
					ds_procedimento_w;
				exit when C01%notfound;
					begin
					ds_exames_w := ds_exames_w || ' ' ||  ds_procedimento_w;
					end;
				end loop;
				close C01;

				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@TITULO@',ds_titulo_laudo_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@LOCAL@',ds_local_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@SETOR@',ds_setor_atendimento_w),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@PACIENTE@', obter_nome_pf(cd_pessoa_fisica_w)),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@DTEXAME@', to_char(dt_exame_w,'DD/MM/YYYY HH24:MI:SS')),1,2000);
				ds_conteudo_w := substr(replace_macro(ds_conteudo_w,'@EXAME@', ds_exames_w),1,2000);

				select obter_se_email_valido(ds_email_destino_w)
				into	ie_email_valido_w
				from	dual;

				if	(ie_email_valido_w = 'S') then
					enviar_email(ds_assunto_w, ds_conteudo_w, ds_email_origem_w, ds_email_destino_w, nm_usuario_ref_w, 'M');
				end if;

				insert into laudo_paciente_loc_envio (
						nr_sequencia,
						dt_envio,
						nm_usuario,
						ds_email_origem,
						ds_email_destino,
						ds_conteudo,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_loc)
				values(		laudo_paciente_loc_envio_seq.nextval,
						sysdate,
						nm_usuario_ref_w,
						ds_email_origem_w,
						ds_email_destino_w,
						ds_conteudo_w,
						sysdate,
						sysdate,
						:new.nm_usuario,
						:new.nr_seq_loc);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;

END;
/


ALTER TABLE TASY.LAUDO_PACIENTE_LOC ADD (
  CONSTRAINT LAUPALO_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_LOC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE_LOC ADD (
  CONSTRAINT LAUPALO_LAUPACI_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LAUPALO_SETATEND_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT LAUPALO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPALO_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_REGISTRO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LAUPALO_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RECEBIDO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPALO_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_ENTREGA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.LAUDO_PACIENTE_LOC TO NIVEL_1;

