ALTER TABLE TASY.CONVENIO_RECEB_ADIANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RECEB_ADIANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RECEB_ADIANT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RECEB         NUMBER(10)               NOT NULL,
  NR_ADIANTAMENTO      NUMBER(10)               NOT NULL,
  VL_VINCULADO         NUMBER(15,2)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREADI_ADIANTA_FK_I ON TASY.CONVENIO_RECEB_ADIANT
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COREADI_CONRECE_FK_I ON TASY.CONVENIO_RECEB_ADIANT
(NR_SEQ_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COREADI_PK ON TASY.CONVENIO_RECEB_ADIANT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREADI_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COREADI_UK ON TASY.CONVENIO_RECEB_ADIANT
(NR_SEQ_RECEB, NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREADI_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_ADIANT_INSERT
after insert or update ON TASY.CONVENIO_RECEB_ADIANT for each row
declare

dt_documento_w		convenio_receb.dt_recebimento%type;
vl_adiantamento_w	adiantamento.vl_adiantamento%type;
nr_seq_trans_fin_w	adiantamento.nr_seq_trans_fin%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

cursor c01 is
	select		a.nm_atributo,
			a.cd_tipo_lote_contab
	from		atributo_contab a
	where 		a.cd_tipo_lote_contab = 11
	and 		a.nm_atributo in ('VL_ADIANTAMENTO');

c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(inserting) then

	select  a.vl_adiantamento,
		nvl(a.nr_seq_trans_fin, b.nr_seq_trans_fin),
		b.dt_recebimento,
		b.cd_estabelecimento
	into    vl_adiantamento_w,
		nr_seq_trans_fin_w,
		dt_documento_w,
		cd_estabelecimento_w
	from    adiantamento a,
		Convenio_Receb b
	where   b.nr_sequencia		= :new.nr_seq_receb
	and 	a.nr_adiantamento	= :new.nr_adiantamento;

	open c01;
	loop
	fetch c01 into
	c01_w;
	exit when c01%notfound;
		begin

		if	(nvl(vl_adiantamento_w, 0) <> 0) and
			(nvl(nr_seq_trans_fin_w, 0) <> 0) then
			begin
			ctb_concil_financeira_pck.ctb_gravar_documento	(
										cd_estabelecimento_w,
										trunc(dt_documento_w),
										c01_w.cd_tipo_lote_contab,
										nr_seq_trans_fin_w,
										8,
										:new.nr_seq_receb,
										:new.nr_sequencia,
										null,
										vl_adiantamento_w,
										'CONVENIO_RECEB_ADIANT',
										c01_w.nm_atributo,
										:new.nm_usuario);

			end;
		end if;
		end;
	end loop;
	close c01;

elsif (updating) then

	select  a.vl_adiantamento,
		nvl(a.nr_seq_trans_fin, b.nr_seq_trans_fin)
	into    vl_adiantamento_w,
		nr_seq_trans_fin_w
	from    adiantamento a,
		Convenio_Receb b
	where   b.nr_sequencia		= :new.nr_seq_receb
	and 	a.nr_adiantamento	= :new.nr_adiantamento;

	begin
	update  ctb_documento
	set     vl_movimento 		= nvl(vl_adiantamento_w,0),
		nr_seq_trans_financ	= nr_seq_trans_fin_w
	where     nm_tabela   		= 'CONVENIO_RECEB_ADIANT'
	and     nr_documento		= :new.nr_seq_receb
	and	nr_seq_doc_compl 	= :new.nr_sequencia
	and	nm_atributo		= 'VL_ADIANTAMENTO'
	and	nvl(nr_lote_contabil,0)	= 0;
	end;

end if;

end if;

end CONVENIO_RECEB_ADIC_INSERT;
/


ALTER TABLE TASY.CONVENIO_RECEB_ADIANT ADD (
  CONSTRAINT COREADI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT COREADI_UK
 UNIQUE (NR_SEQ_RECEB, NR_ADIANTAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RECEB_ADIANT ADD (
  CONSTRAINT COREADI_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT COREADI_CONRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEB) 
 REFERENCES TASY.CONVENIO_RECEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_RECEB_ADIANT TO NIVEL_1;

