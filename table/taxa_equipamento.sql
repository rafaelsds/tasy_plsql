ALTER TABLE TASY.TAXA_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAXA_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAXA_EQUIPAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_EQUIPAMENTO        NUMBER(10),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  NR_SEQ_CLASSIF_EQUIP  NUMBER(10),
  NR_SEQ_PROC           NUMBER(10),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAXAEQU_AREPROC_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_CLEQUIP_FK_I ON TASY.TAXA_EQUIPAMENTO
(NR_SEQ_CLASSIF_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_CLEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_EQUIPAM_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_EQUIPAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_ESPPROC_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_ESTABEL_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_GRUPROC_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TAXAEQU_PK ON TASY.TAXA_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_PK
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_PROCEDI_FK_I ON TASY.TAXA_EQUIPAMENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_PROINTE_FK_I ON TASY.TAXA_EQUIPAMENTO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXAEQU_PROINTE_FK2_I ON TASY.TAXA_EQUIPAMENTO
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXAEQU_PROINTE_FK2_I
  MONITORING USAGE;


ALTER TABLE TASY.TAXA_EQUIPAMENTO ADD (
  CONSTRAINT TAXAEQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TAXA_EQUIPAMENTO ADD (
  CONSTRAINT TAXAEQU_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT TAXAEQU_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT TAXAEQU_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT TAXAEQU_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT TAXAEQU_EQUIPAM_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO (CD_EQUIPAMENTO),
  CONSTRAINT TAXAEQU_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT TAXAEQU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TAXAEQU_PROINTE_FK2 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT TAXAEQU_CLEQUIP_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_EQUIP) 
 REFERENCES TASY.CLASSIF_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TAXA_EQUIPAMENTO TO NIVEL_1;

