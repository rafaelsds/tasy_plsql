ALTER TABLE TASY.FIS_REGRA_EFD_F600
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_REGRA_EFD_F600 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_REGRA_EFD_F600
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_EFD     NUMBER(10)               NOT NULL,
  IE_FORMA_REGISTRO    VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EFDF600_FISRGEFD_FK_I ON TASY.FIS_REGRA_EFD_F600
(NR_SEQ_REGRA_EFD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EFDF600_PK ON TASY.FIS_REGRA_EFD_F600
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_REGRA_EFD_F600 ADD (
  CONSTRAINT EFDF600_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_REGRA_EFD_F600 ADD (
  CONSTRAINT EFDF600_FISRGEFD_FK 
 FOREIGN KEY (NR_SEQ_REGRA_EFD) 
 REFERENCES TASY.FIS_REGRA_EFD (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_REGRA_EFD_F600 TO NIVEL_1;

