ALTER TABLE TASY.WFILTRO_ATRIB_REGRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WFILTRO_ATRIB_REGRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.WFILTRO_ATRIB_REGRA_ITEM
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ITEM                     VARCHAR2(10 BYTE) NOT NULL,
  IE_CONFIGURACAO             VARCHAR2(200 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_ATRIB_REGRA          NUMBER(10),
  VL_MINIMO                   NUMBER(15,4),
  VL_MAXIMO                   NUMBER(15,4),
  IE_TIPO_CALCULO             VARCHAR2(1 BYTE),
  DS_MSG_AVISO                VARCHAR2(255 BYTE),
  VL_PADRAO                   VARCHAR2(2000 BYTE),
  IE_OBRIGATORIO              VARCHAR2(1 BYTE),
  IE_HABILITADO               VARCHAR2(1 BYTE),
  DS_SCRIPT                   VARCHAR2(2000 BYTE),
  IE_TIPO_SCRIPT              VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_PAR_REGRA_ITEM  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WFIREGIT_PK ON TASY.WFILTRO_ATRIB_REGRA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFIREGIT_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WFIREGIT_UK ON TASY.WFILTRO_ATRIB_REGRA_ITEM
(NR_SEQ_ATRIB_REGRA, CD_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WFIREGIT_WFATRREG_FK_I ON TASY.WFILTRO_ATRIB_REGRA_ITEM
(NR_SEQ_ATRIB_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFIREGIT_WFATRREG_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.wfiltro_atrib_regra_item_atual
before insert or update ON TASY.WFILTRO_ATRIB_REGRA_ITEM for each row
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	wfiltro_atrib_regra_item
	where	nr_seq_atrib_regra	= :new.nr_seq_atrib_regra
	and	((cd_item		= :new.cd_item) or (cd_item is null and :new.cd_item is null))
	and	nr_sequencia 	<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/


ALTER TABLE TASY.WFILTRO_ATRIB_REGRA_ITEM ADD (
  CONSTRAINT WFIREGIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT WFIREGIT_UK
 UNIQUE (NR_SEQ_ATRIB_REGRA, CD_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.WFILTRO_ATRIB_REGRA_ITEM ADD (
  CONSTRAINT WFIREGIT_WFATRREG_FK 
 FOREIGN KEY (NR_SEQ_ATRIB_REGRA) 
 REFERENCES TASY.WFILTRO_ATRIB_REGRA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.WFILTRO_ATRIB_REGRA_ITEM TO NIVEL_1;

