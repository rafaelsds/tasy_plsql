ALTER TABLE TASY.REGRA_CALC_ORC_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CALC_ORC_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CALC_ORC_CUSTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  NR_SEQ_CALCULO           NUMBER(5)            NOT NULL,
  PR_CALCULAR              NUMBER(15,4)         NOT NULL,
  IE_SOBREPOR              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_NAT_GASTO_ORIGEM      NUMBER(20),
  CD_NAT_GASTO_DESTINO     NUMBER(20),
  CD_CENTRO_CONTROLE_ORIG  NUMBER(8),
  CD_CENTRO_CONTROLE_DEST  NUMBER(8),
  CD_GRUPO_NAT_GASTO       NUMBER(8),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_GNG               NUMBER(10),
  NR_SEQ_NG_ORIGEM         NUMBER(10),
  NR_SEQ_NG_DESTINO        NUMBER(10),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCAOC_CENCONT_FK_I ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_CENCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_CENCONT_FK2_I ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_CENCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_ESTABEL_FK_I ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCAOC_GRUNAGA_FK_I ON TASY.REGRA_CALC_ORC_CUSTO
(NR_SEQ_GNG)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_GRUNAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_I1 ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO, CD_GRUPO_NAT_GASTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_I1
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_NATGAST_FK_I ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO, CD_NAT_GASTO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_NATGAST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_NATGAST_FK_I2 ON TASY.REGRA_CALC_ORC_CUSTO
(NR_SEQ_NG_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_NATGAST_FK_I2
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_NATGAST_FK2_I ON TASY.REGRA_CALC_ORC_CUSTO
(CD_ESTABELECIMENTO, CD_NAT_GASTO_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_NATGAST_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCAOC_NATGAST_FK2_I2 ON TASY.REGRA_CALC_ORC_CUSTO
(NR_SEQ_NG_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_NATGAST_FK2_I2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGCAOC_PK ON TASY.REGRA_CALC_ORC_CUSTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCAOC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.regra_calc_orc_custo_atual
before insert or update ON TASY.REGRA_CALC_ORC_CUSTO for each row
declare
ie_situacao_w		varchar2(1);

begin

if (:new.cd_centro_controle_orig is not null) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	centro_controle
	where	cd_centro_controle = :new.cd_centro_controle_orig
	and	cd_estabelecimento = :new.cd_estabelecimento;

	exception when others then
		ie_situacao_w := '';
	end;

	if (ie_situacao_w = 'I') then
		--O centro de controle de origem informado est� inativo
		wheb_mensagem_pck.exibir_mensagem_abort(277787);
	end if;
end if;

if (:new.cd_centro_controle_dest is not null) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	centro_controle
	where	cd_centro_controle = :new.cd_centro_controle_dest
	and	cd_estabelecimento = :new.cd_estabelecimento;

	exception when others then
		ie_situacao_w := '';
	end;

	if (ie_situacao_w = 'I') then
		-- O centro de controle de destino informado est� inativo
		wheb_mensagem_pck.exibir_mensagem_abort(277788);
	end if;
end if;

if (:new.nr_seq_gng is not null) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	grupo_natureza_gasto
	where	nr_sequencia = :new.nr_seq_gng;

	exception when others then
		ie_situacao_w := '';
	end;

	if (ie_situacao_w = 'I') then
		-- O grupo de natureza informado est� inativo.
		wheb_mensagem_pck.exibir_mensagem_abort(277790);
	end if;
end if;

if (:new.nr_seq_ng_origem is not null) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	natureza_gasto
	where	nr_sequencia = :new.nr_seq_ng_origem;

	exception when others then
		ie_situacao_w := '';
	end;

	if (ie_situacao_w = 'I') then
		-- A natureza origem informada est� inativa.
		wheb_mensagem_pck.exibir_mensagem_abort(277791);
	end if;
end if;

if (:new.nr_seq_ng_destino is not null) then
	begin
	select	ie_situacao
	into	ie_situacao_w
	from	natureza_gasto
	where	nr_sequencia = :new.nr_seq_ng_destino;

	exception when others then
		ie_situacao_w := '';
	end;

	if (ie_situacao_w = 'I') then
		-- A natureza destino informada est� inativa.
		wheb_mensagem_pck.exibir_mensagem_abort(277792);
	end if;
end if;

end;
/


ALTER TABLE TASY.REGRA_CALC_ORC_CUSTO ADD (
  CONSTRAINT REGCAOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CALC_ORC_CUSTO ADD (
  CONSTRAINT REGCAOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGCAOC_NATGAST_FK 
 FOREIGN KEY (NR_SEQ_NG_ORIGEM) 
 REFERENCES TASY.NATUREZA_GASTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGCAOC_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_ORIG) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE)
    ON DELETE CASCADE,
  CONSTRAINT REGCAOC_CENCONT_FK2 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_DEST) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE),
  CONSTRAINT REGCAOC_GRUNAGA_FK 
 FOREIGN KEY (NR_SEQ_GNG) 
 REFERENCES TASY.GRUPO_NATUREZA_GASTO (NR_SEQUENCIA),
  CONSTRAINT REGCAOC_NATGAST_FK2 
 FOREIGN KEY (NR_SEQ_NG_DESTINO) 
 REFERENCES TASY.NATUREZA_GASTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_CALC_ORC_CUSTO TO NIVEL_1;

