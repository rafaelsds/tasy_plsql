ALTER TABLE TASY.OFT_CONSULTA_LENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_CONSULTA_LENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_CONSULTA_LENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CONS_MED            NUMBER(10),
  IE_LADO                    VARCHAR2(15 BYTE),
  NR_SEQ_LENTE               NUMBER(10)         NOT NULL,
  NR_SEQ_CONSULTA            NUMBER(10),
  QT_GRAU                    NUMBER(15,3),
  QT_GRAU_ESFERICO           NUMBER(15,3),
  QT_GRAU_CILINDRICO         NUMBER(15,3),
  QT_GRAU_LONGE              NUMBER(15,3),
  QT_EIXO                    NUMBER(15,3),
  QT_ADICAO                  NUMBER(15,3),
  DT_LANCAMENTO              DATE,
  DT_REGISTRO                DATE,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  VL_AV                      NUMBER(10),
  QT_CURVA_BASE              NUMBER(15,3),
  QT_CURVA_BASE_DOIS         NUMBER(15,3),
  QT_CURVA_BASE_UM           NUMBER(15,3),
  QT_DIAMETRO                NUMBER(15,3),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTCONL_OFCOFOR_FK_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCONL_OFTCOME_FK_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_CONS_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONL_OFTCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONL_OFTCONS_FK_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONL_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONL_OFTLENC_FK_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_LENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONL_OFTLENC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONL_PESFISI_FK_I ON TASY.OFT_CONSULTA_LENTE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTCONL_PK ON TASY.OFT_CONSULTA_LENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONL_PK
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONL_TASASDI_FK_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCONL_TASASDI_FK2_I ON TASY.OFT_CONSULTA_LENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OFT_CONSULTA_LENTE_PEND_ATUAL
after insert or update or delete ON TASY.OFT_CONSULTA_LENTE for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into	ie_libera_exames_oft_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'CLEN';
		elsif	(:old.dt_liberacao is null) and
               (:new.dt_liberacao is not null) then
               ie_tipo_w := 'XCLEN';
		end if;

      select	max(a.nr_atendimento),
                  max(a.cd_pessoa_fisica)
      into	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;

		begin
		if	(ie_tipo_w	is not null) then
			gerar_registro_pendente_pep(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	ie_tipo_registro = 'CLEN'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(ie_tipo_pendencia,'L')	= 'L';

            commit;
    end if;

	commit;
end if;

<<final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.OFT_CONSULTA_LENTE ADD (
  CONSTRAINT OFTCONL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_CONSULTA_LENTE ADD (
  CONSTRAINT OFTCONL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCONL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCONL_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT OFTCONL_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT OFTCONL_OFTLENC_FK 
 FOREIGN KEY (NR_SEQ_LENTE) 
 REFERENCES TASY.OFT_LENTE_CONTATO (NR_SEQUENCIA),
  CONSTRAINT OFTCONL_OFTCOME_FK 
 FOREIGN KEY (NR_SEQ_CONS_MED) 
 REFERENCES TASY.OFT_CONSULTA_MEDICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OFTCONL_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OFT_CONSULTA_LENTE TO NIVEL_1;

