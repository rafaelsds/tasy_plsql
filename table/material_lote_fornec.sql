ALTER TABLE TASY.MATERIAL_LOTE_FORNEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_LOTE_FORNEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_LOTE_FORNEC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  NR_DIGITO_VERIF            NUMBER(1)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_LOTE_FORNEC             VARCHAR2(20 BYTE)  NOT NULL,
  DT_VALIDADE                DATE,
  CD_CGC_FORNEC              VARCHAR2(14 BYTE)  NOT NULL,
  QT_MATERIAL                NUMBER(10)         NOT NULL,
  NR_SEQUENCIA_NF            NUMBER(10),
  NR_ITEM_NF                 NUMBER(5),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_LOTE_PRODUCAO           NUMBER(10),
  DT_IMPRESSAO               DATE,
  DT_REIMPRESSAO             DATE,
  NM_USUARIO_REIMPRESSAO     VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_MARCA               NUMBER(10),
  IE_ORIGEM_LOTE             VARCHAR2(1 BYTE),
  IE_VALIDADE_INDETERMINADA  VARCHAR2(1 BYTE)   NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_SUPERIOR            NUMBER(10),
  NM_USUARIO_IMPRESSAO       VARCHAR2(15 BYTE),
  NR_NOTA_FISCAL             VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NM_USUARIO_BLOQUEIO        VARCHAR2(15 BYTE),
  DT_BLOQUEIO                DATE,
  IE_BLOQUEIO                VARCHAR2(1 BYTE),
  DT_VALIDADE_ORIGINAL       DATE,
  CD_BARRA_MATERIAL          VARCHAR2(40 BYTE),
  DS_BARRAS                  VARCHAR2(4000 BYTE),
  DT_FABRICACAO              DATE,
  DS_LOCALIZACAO             VARCHAR2(80 BYTE),
  NR_SEQ_LOCAL               NUMBER(10),
  NR_SERIE_MATERIAL          VARCHAR2(80 BYTE),
  NR_SEQ_ITEM_UNITARIZACAO   NUMBER(10),
  CD_CONVENIO                NUMBER(5),
  NR_EMPRESTIMO              NUMBER(10),
  IE_TIPO_PROGRAMA           VARCHAR2(15 BYTE),
  IE_CLASSIF_RENAME          VARCHAR2(15 BYTE),
  IE_ORIGEM_MANUAL           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATLOFO_CONVENI_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_EMPREST_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_ESTABEL_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_ITNUNTR_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_SEQ_ITEM_UNITARIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATLOFO_ITNUNTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATLOFO_I1 ON TASY.MATERIAL_LOTE_FORNEC
(DT_VALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I2 ON TASY.MATERIAL_LOTE_FORNEC
(DS_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I3 ON TASY.MATERIAL_LOTE_FORNEC
(UPPER("DS_LOTE_FORNEC"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I4 ON TASY.MATERIAL_LOTE_FORNEC
(CD_BARRA_MATERIAL, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I5 ON TASY.MATERIAL_LOTE_FORNEC
(DS_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          392K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I6 ON TASY.MATERIAL_LOTE_FORNEC
(CD_ESTABELECIMENTO, DS_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   166
STORAGE    (
            INITIAL          392K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_I7 ON TASY.MATERIAL_LOTE_FORNEC
(CD_ESTABELECIMENTO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_LOTPROD_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_LOTE_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATLOFO_LOTPROD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATLOFO_MARCA_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_MATERIA_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_MATLOCA_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATLOFO_MATLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATLOFO_NOTFIIT_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(NR_SEQUENCIA_NF, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATLOFO_PESJURI_FK_I ON TASY.MATERIAL_LOTE_FORNEC
(CD_CGC_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATLOFO_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATLOFO_PK ON TASY.MATERIAL_LOTE_FORNEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_LOTE_FORNEC_tp  after update ON TASY.MATERIAL_LOTE_FORNEC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_LOTE_FORNEC,1,4000),substr(:new.DS_LOTE_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOTE_FORNEC',ie_log_w,ds_w,'MATERIAL_LOTE_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_FORNEC,1,4000),substr(:new.CD_CGC_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_FORNEC',ie_log_w,ds_w,'MATERIAL_LOTE_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_BARRA_MATERIAL,1,4000),substr(:new.CD_BARRA_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_BARRA_MATERIAL',ie_log_w,ds_w,'MATERIAL_LOTE_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'MATERIAL_LOTE_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MATERIAL_LOTE_FORNEC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Material_lote_fornec_update
BEFORE insert or UPDATE ON TASY.MATERIAL_LOTE_FORNEC FOR EACH ROW
DECLARE
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_grupo_material_w     grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w     subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w     classe_material.cd_classe_material%type;
nr_seq_familia_w     material.nr_seq_familia%type;

BEGIN
reg_integracao_p.ie_operacao		:=	'A';

if	(inserting) then
	begin
	reg_integracao_p.ie_operacao	:=	'I';

	:new.ie_bloqueio	:=	nvl(:new.ie_bloqueio, 'N');

	if	(:new.dt_atualizacao_nrec is null) then
		begin
		:new.dt_atualizacao_nrec	:=	:new.dt_atualizacao;
		:new.nm_usuario_nrec	:=	:new.nm_usuario;
		end;
	end if;

    select
		gm.cd_grupo_material,
		sm.cd_subgrupo_material,
		cm.cd_classe_material,
		nvl(m.nr_seq_familia, 0) as nr_seq_familia
    into cd_grupo_material_w,
         cd_subgrupo_material_w,
         cd_classe_material_w,
         nr_seq_familia_w
	from material m,
		classe_material cm,
		subgrupo_material sm,
		grupo_material gm
	where m.cd_classe_material = cm.cd_classe_material
      and cm.cd_subgrupo_material = sm.cd_subgrupo_material
      and sm.cd_grupo_material = gm.cd_grupo_material
      and m.cd_material = :new.cd_material;

    if(:new.ie_origem_manual IS NULL OR :new.ie_origem_manual = 'N') THEN
        if (:new.ie_tipo_programa is null) THEN
            :new.ie_tipo_programa := obter_regra_prog_saude(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
        -- Buscar IE_CLASSIF_RENAME pelo caso venha vazio
        if (:new.ie_classif_rename is null) THEN
            :new.ie_classif_rename := obter_regra_rename(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
    end if;
	end;
end if;

if	(:new.ie_validade_indeterminada = 'N') and
	(:new.dt_validade is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266254);
	--'Deve ser informado a validade do lote, ou como validade indeterminada');
elsif	(:new.ie_validade_indeterminada = 'S') and
	(:new.dt_validade is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266255);
	--'Se a validade for indeterminada, n�o deve ser informado a data de validade');
end if;

/*Enviar para integra��o padr�o*/

select	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material
into	reg_integracao_p.cd_grupo_material,
	reg_integracao_p.cd_subgrupo_material,
	reg_integracao_p.cd_classe_material
from	estrutura_material_v a
where	a.cd_material = :new.cd_material;


reg_integracao_p.cd_estab_documento	:= :new.cd_estabelecimento;
reg_integracao_p.ie_origem_lote	:= :new.ie_origem_lote;
reg_integracao_p.cd_material		:= :new.cd_material;

gerar_int_padrao.gravar_integracao('75', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);

END;
/


ALTER TABLE TASY.MATERIAL_LOTE_FORNEC ADD (
  CONSTRAINT MATLOFO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_LOTE_FORNEC ADD (
  CONSTRAINT MATLOFO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT MATLOFO_EMPREST_FK 
 FOREIGN KEY (NR_EMPRESTIMO) 
 REFERENCES TASY.EMPRESTIMO (NR_EMPRESTIMO),
  CONSTRAINT MATLOFO_MATLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.MATERIAL_LOCAL (NR_SEQUENCIA),
  CONSTRAINT MATLOFO_ITNUNTR_FK 
 FOREIGN KEY (NR_SEQ_ITEM_UNITARIZACAO) 
 REFERENCES TASY.ITEM_UNITARIZACAO (NR_SEQUENCIA),
  CONSTRAINT MATLOFO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATLOFO_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQUENCIA_NF, NR_ITEM_NF) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF)
    ON DELETE CASCADE,
  CONSTRAINT MATLOFO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATLOFO_LOTPROD_FK 
 FOREIGN KEY (NR_LOTE_PRODUCAO) 
 REFERENCES TASY.LOTE_PRODUCAO (NR_LOTE_PRODUCAO),
  CONSTRAINT MATLOFO_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNEC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATLOFO_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_LOTE_FORNEC TO NIVEL_1;

