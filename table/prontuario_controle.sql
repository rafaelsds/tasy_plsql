ALTER TABLE TASY.PRONTUARIO_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRONTUARIO_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRONTUARIO_CONTROLE
(
  NR_PRONTUARIO          NUMBER(10)             NOT NULL,
  DT_OPERACAO            DATE                   NOT NULL,
  IE_TIPO_OPERACAO       VARCHAR2(1 BYTE)       NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  DT_BAIXA               DATE,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  NR_ATENDIMENTO         NUMBER(10),
  NR_PRESCRICAO          NUMBER(10),
  DS_ARQUIVO_MORTO       VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_PESSOA_SOLIC        VARCHAR2(10 BYTE),
  NM_USUARIO_EMPRESTIMO  VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROCONT_ATEPACI_FK_I ON TASY.PRONTUARIO_CONTROLE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONT_ESTABEL_FK_I ON TASY.PRONTUARIO_CONTROLE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONT_PESFISI_FK_I ON TASY.PRONTUARIO_CONTROLE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONT_PESFISI_FK2_I ON TASY.PRONTUARIO_CONTROLE
(CD_PESSOA_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROCONT_PK ON TASY.PRONTUARIO_CONTROLE
(NR_PRONTUARIO, DT_OPERACAO, IE_TIPO_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRONTUARIO_CONTROLE ADD (
  CONSTRAINT PROCONT_PK
 PRIMARY KEY
 (NR_PRONTUARIO, DT_OPERACAO, IE_TIPO_OPERACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRONTUARIO_CONTROLE ADD (
  CONSTRAINT PROCONT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROCONT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PROCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROCONT_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PRONTUARIO_CONTROLE TO NIVEL_1;

