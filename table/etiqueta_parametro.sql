ALTER TABLE TASY.ETIQUETA_PARAMETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ETIQUETA_PARAMETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ETIQUETA_PARAMETRO
(
  NR_SEQUENCIA    NUMBER(3)                     NOT NULL,
  NR_SEQ_TIPO     NUMBER(10)                    NOT NULL,
  NM_PARAMETRO    VARCHAR2(50 BYTE)             NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ETIPARA_PK ON TASY.ETIQUETA_PARAMETRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ETIPARA_PK
  MONITORING USAGE;


CREATE INDEX TASY.ETIPARA_TIPOETIQ_FK_I ON TASY.ETIQUETA_PARAMETRO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ETIPARA_TIPOETIQ_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ETIQUETA_PARAMETRO ADD (
  CONSTRAINT ETIPARA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ETIQUETA_PARAMETRO TO NIVEL_1;

