ALTER TABLE TASY.NAIS_CONVERSION_MASTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NAIS_CONVERSION_MASTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.NAIS_CONVERSION_MASTER
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NM_TASY_TABLE                 VARCHAR2(255 BYTE),
  NM_TASY_COLUMN                VARCHAR2(255 BYTE),
  VL_TASY_CODE                  VARCHAR2(50 BYTE),
  CD_BODY_PART                  NUMBER(10),
  CD_DIRECTION                  NUMBER(10),
  NM_ITEM_CONVERSION            VARCHAR2(255 BYTE),
  DT_VALID_START                DATE,
  DT_VALID_END                  DATE,
  IE_SEND_FLAF                  VARCHAR2(1 BYTE),
  CD_MEDICAL_ACTION             VARCHAR2(20 BYTE),
  CD_MEDICAL_AFFAIR             VARCHAR2(20 BYTE),
  CD_MEDICAL_AFFAIR_TWO         VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM            VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM_POS        VARCHAR2(2 BYTE),
  CD_ADDITIONAL_ITEM_TWO        VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM_POS_TWO    VARCHAR2(2 BYTE),
  CD_ADDITIONAL_ITEM_THREE      VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM_POS_THREE  VARCHAR2(2 BYTE),
  CD_ADDITIONAL_ITEM_FOUR       VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM_POS_FOUR   VARCHAR2(2 BYTE),
  CD_ADDITIONAL_ITEM_FIVE       VARCHAR2(20 BYTE),
  CD_ADDITIONAL_ITEM_POS_FIVE   VARCHAR2(2 BYTE),
  IE_TYPE_MESSAGE               VARCHAR2(2 BYTE),
  CD_MEDICAL_AFFAIR_THREE       VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.NAISCM_PK ON TASY.NAIS_CONVERSION_MASTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NAIS_CONVERSION_MASTER ADD (
  CONSTRAINT NAISCM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

