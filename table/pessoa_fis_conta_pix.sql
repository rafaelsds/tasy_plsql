ALTER TABLE TASY.PESSOA_FIS_CONTA_PIX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FIS_CONTA_PIX CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FIS_CONTA_PIX
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  CD_BANCO             NUMBER(5),
  CD_AGENCIA_BANCARIA  VARCHAR2(8 BYTE),
  NR_CONTA             VARCHAR2(20 BYTE),
  DS_CHAVE             VARCHAR2(255 BYTE),
  IE_TIPO_CHAVE        VARCHAR2(2 BYTE),
  IE_PREFERENCIAL      VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEFICOPIX_PESFICO_FK_I ON TASY.PESSOA_FIS_CONTA_PIX
(CD_BANCO, CD_AGENCIA_BANCARIA, CD_PESSOA_FISICA, NR_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEFICOPIX_PK ON TASY.PESSOA_FIS_CONTA_PIX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fis_conta_pix_before
before insert or update
ON TASY.PESSOA_FIS_CONTA_PIX for each row
declare
    qt_preferencial_w number(1);
    pragma autonomous_transaction;
begin
    if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

        pix_pck.valida_chave( ds_chave_p => :new.ds_chave,
                              ie_tipo_chave_p => :new.ie_tipo_chave);

        if (:new.ie_preferencial = 'S') then
            begin
                update pessoa_fis_conta_pix a
                set    a.ie_preferencial = 'N'
                where  a.cd_pessoa_fisica = :new.cd_pessoa_fisica
                and    a.cd_banco = :new.cd_banco
                and    a.cd_agencia_bancaria = :new.cd_agencia_bancaria
                and    a.nr_conta = :new.nr_conta
                and    a.nr_sequencia <> :new.nr_sequencia;
                commit;
            end;
        else
            select count(1)
            into   qt_preferencial_w
            from   pessoa_fis_conta_pix a
            where  a.cd_pessoa_fisica = :new.cd_pessoa_fisica
            and    a.cd_banco = :new.cd_banco
            and    a.cd_agencia_bancaria = :new.cd_agencia_bancaria
            and    a.nr_conta = :new.nr_conta
            and    a.ie_preferencial = 'S';

            if (qt_preferencial_w = 0) then
                :new.ie_preferencial := 'S';
            end if;
        end if;
    end if;
end pessoa_fis_conta_pix_before;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FIS_CONTA_PIX_tp  after update ON TASY.PESSOA_FIS_CONTA_PIX FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_BANCO,1,4000),substr(:new.CD_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'CD_BANCO',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AGENCIA_BANCARIA,1,4000),substr(:new.CD_AGENCIA_BANCARIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_AGENCIA_BANCARIA',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PREFERENCIAL,1,4000),substr(:new.IE_PREFERENCIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREFERENCIAL',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CHAVE,1,4000),substr(:new.DS_CHAVE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CHAVE',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CHAVE,1,4000),substr(:new.IE_TIPO_CHAVE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CHAVE',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTA,1,4000),substr(:new.NR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTA',ie_log_w,ds_w,'PESSOA_FIS_CONTA_PIX',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_FIS_CONTA_PIX ADD (
  CONSTRAINT PEFICOPIX_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

