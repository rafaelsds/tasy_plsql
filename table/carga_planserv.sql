ALTER TABLE TASY.CARGA_PLANSERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARGA_PLANSERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARGA_PLANSERV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_INFORMACAO        VARCHAR2(150 BYTE)       NOT NULL,
  IE_TIPO              VARCHAR2(3 BYTE)         NOT NULL,
  CD_VERSAO            VARCHAR2(40 BYTE)        NOT NULL,
  DT_UTILIZACAO        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CARPLAN_PK ON TASY.CARGA_PLANSERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CARGA_PLANSERV_tp  after update ON TASY.CARGA_PLANSERV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_VERSAO,1,4000),substr(:new.CD_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO',ie_log_w,ds_w,'CARGA_PLANSERV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CARGA_PLANSERV ADD (
  CONSTRAINT CARPLAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CARGA_PLANSERV TO NIVEL_1;

