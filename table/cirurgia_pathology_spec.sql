ALTER TABLE TASY.CIRURGIA_PATHOLOGY_SPEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_PATHOLOGY_SPEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_PATHOLOGY_SPEC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_SEQ_PEPO            NUMBER(10),
  IE_SPECIMEN_PATOLOGIA  VARCHAR2(1 BYTE),
  NR_CIRURGIA            NUMBER(10),
  DS_SPECIMEN_DETAILS    VARCHAR2(2000 BYTE),
  IE_BACTERIAL_MICRO     VARCHAR2(1 BYTE),
  IE_FROZEN_SECTION      VARCHAR2(1 BYTE),
  IE_HISTOLOGY_ROUTINE   VARCHAR2(1 BYTE),
  IE_FRESH               VARCHAR2(1 BYTE),
  IE_TAKEN_BY            VARCHAR2(1 BYTE),
  IE_XRAY                VARCHAR2(1 BYTE),
  IE_ORDERLY             VARCHAR2(1 BYTE),
  IE_PATHOLOGIST         VARCHAR2(1 BYTE),
  NR_SPECIMEN            NUMBER(10),
  DT_RECORD              DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRPATSPEC_CIRURGI_FK_I ON TASY.CIRURGIA_PATHOLOGY_SPEC
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRPATSPEC_PEPOCIR_FK_I ON TASY.CIRURGIA_PATHOLOGY_SPEC
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRPATSPEC_PESFISI_FK_I ON TASY.CIRURGIA_PATHOLOGY_SPEC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRPATSPEC_PK ON TASY.CIRURGIA_PATHOLOGY_SPEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CIRURGIA_PATHOLOGY_SPEC ADD (
  CONSTRAINT CIRPATSPEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CIRURGIA_PATHOLOGY_SPEC ADD (
  CONSTRAINT CIRPATSPEC_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRPATSPEC_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRPATSPEC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CIRURGIA_PATHOLOGY_SPEC TO NIVEL_1;

