ALTER TABLE TASY.SUP_MOVTO_PENDENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_MOVTO_PENDENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_MOVTO_PENDENTE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_MOVIMENTO_ESTOQUE  NUMBER(10)              NOT NULL,
  IE_PENDENTE           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUMOPEN_MOVESTO_FK_I ON TASY.SUP_MOVTO_PENDENTE
(NR_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUMOPEN_MOVESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUMOPEN_PK ON TASY.SUP_MOVTO_PENDENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUMOPEN_PK
  MONITORING USAGE;


ALTER TABLE TASY.SUP_MOVTO_PENDENTE ADD (
  CONSTRAINT SUMOPEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_MOVTO_PENDENTE ADD (
  CONSTRAINT SUMOPEN_MOVESTO_FK 
 FOREIGN KEY (NR_MOVIMENTO_ESTOQUE) 
 REFERENCES TASY.MOVIMENTO_ESTOQUE (NR_MOVIMENTO_ESTOQUE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_MOVTO_PENDENTE TO NIVEL_1;

