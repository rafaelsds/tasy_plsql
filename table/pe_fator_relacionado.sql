ALTER TABLE TASY.PE_FATOR_RELACIONADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_FATOR_RELACIONADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_FATOR_RELACIONADO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RESULT        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_FATOR         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEFATRE_PECADFR_FK_I ON TASY.PE_FATOR_RELACIONADO
(NR_SEQ_FATOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEFATRE_PECADFR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEFATRE_PEITERE_FK_I ON TASY.PE_FATOR_RELACIONADO
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEFATRE_PEITERE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEFATRE_PK ON TASY.PE_FATOR_RELACIONADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEFATRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.PE_FATOR_RELACIONADO ADD (
  CONSTRAINT PEFATRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_FATOR_RELACIONADO ADD (
  CONSTRAINT PEFATRE_PECADFR_FK 
 FOREIGN KEY (NR_SEQ_FATOR) 
 REFERENCES TASY.PE_CAD_FATOR_RELAC (NR_SEQUENCIA),
  CONSTRAINT PEFATRE_PEITERE_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.PE_ITEM_RESULTADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_FATOR_RELACIONADO TO NIVEL_1;

