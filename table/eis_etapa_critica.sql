DROP TABLE TASY.EIS_ETAPA_CRITICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_ETAPA_CRITICA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_REFERENCIA         DATE,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ETAPA              DATE,
  NR_ATENDIMENTO        NUMBER(10),
  NR_INTERNO_CONTA      NUMBER(10),
  NR_SEQ_ETAPA          NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_MOTIVO_DEV     NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  DT_ALTA               DATE,
  VL_CONTA              NUMBER(15,2),
  CD_CONVENIO           NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.EIS_ETAPA_CRITICA TO NIVEL_1;

