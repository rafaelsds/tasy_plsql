ALTER TABLE TASY.ESCALA_CHUNG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CHUNG CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CHUNG
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  IE_SINAIS_VITAIS           NUMBER(2)          NOT NULL,
  IE_ATIVIDADE               NUMBER(2)          NOT NULL,
  IE_NAUSEAS                 NUMBER(2)          NOT NULL,
  IE_DOR                     NUMBER(2)          NOT NULL,
  IE_SANGRAMENTO             NUMBER(2)          NOT NULL,
  QT_CHUNG                   NUMBER(2),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_SAEP                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_ATENDIMENTO             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCHUN_ATEPACI_FK_I ON TASY.ESCALA_CHUNG
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHUN_CIRURGI_FK_I ON TASY.ESCALA_CHUNG
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHUN_PEPOCIR_FK_I ON TASY.ESCALA_CHUNG
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHUN_PESFISI_FK_I ON TASY.ESCALA_CHUNG
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCHUN_PK ON TASY.ESCALA_CHUNG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHUN_SAEPERO_FK_I ON TASY.ESCALA_CHUNG
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCHUN_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCCHUN_TASASDI_FK_I ON TASY.ESCALA_CHUNG
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHUN_TASASDI_FK2_I ON TASY.ESCALA_CHUNG
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_chung_pend_atual
after insert or update ON TASY.ESCALA_CHUNG for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_libera_partic_w	varchar2(10);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
into		nr_atendimento_w,
			cd_pessoa_fisica_w
from		cirurgia c
where		c.nr_cirurgia = :new.nr_cirurgia;

if 	(cd_pessoa_fisica_w is null) then
	select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
	into		nr_atendimento_w,
				cd_pessoa_fisica_w
	from		pepo_cirurgia c
	where		c.nr_sequencia = :new.nr_seq_pepo;
end if;

if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESCCC';
elsif	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESCCC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_CHUNG_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_CHUNG FOR EACH ROW
declare
qt_reg_w	number(1);
cd_setor_atendimento_w	number(10);
BEGIN



if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;



:new.qt_chung	:= 	:new.ie_sinais_vitais +
			:new.ie_atividade +
			:new.ie_nauseas +
			:new.ie_dor +
			:new.ie_sangramento;



<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ESCALA_CHUNG ADD (
  CONSTRAINT ESCCHUN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CHUNG ADD (
  CONSTRAINT ESCCHUN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCHUN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCCHUN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCCHUN_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCCHUN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCCHUN_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT ESCCHUN_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_CHUNG TO NIVEL_1;

