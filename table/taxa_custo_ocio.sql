ALTER TABLE TASY.TAXA_CUSTO_OCIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAXA_CUSTO_OCIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAXA_CUSTO_OCIO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_TABELA_CUSTO        NUMBER(5)              NOT NULL,
  CD_CENTRO_CONTROLE     NUMBER(8)              NOT NULL,
  CD_REDUTOR_CAPACIDADE  NUMBER(10)             NOT NULL,
  VL_OCIOSIDADE          NUMBER(15,2),
  QT_REDUCAO             NUMBER(15,4),
  NR_SEQ_TABELA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAXCUOC_CENCONT_FK_I ON TASY.TAXA_CUSTO_OCIO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXCUOC_CENCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TAXCUOC_PK ON TASY.TAXA_CUSTO_OCIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXCUOC_PK
  MONITORING USAGE;


CREATE INDEX TASY.TAXCUOC_REDCAPA_FK_I ON TASY.TAXA_CUSTO_OCIO
(CD_REDUTOR_CAPACIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXCUOC_REDCAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXCUOC_TABCUST_FK_I ON TASY.TAXA_CUSTO_OCIO
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAXCUOC_TABCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TAXCUOC_TABCUST_FK_I2 ON TASY.TAXA_CUSTO_OCIO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TAXA_CUSTO_OCIO ADD (
  CONSTRAINT TAXCUOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TAXA_CUSTO_OCIO ADD (
  CONSTRAINT TAXCUOC_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE),
  CONSTRAINT TAXCUOC_TABCUST_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.TABELA_CUSTO (NR_SEQUENCIA),
  CONSTRAINT TAXCUOC_REDCAPA_FK 
 FOREIGN KEY (CD_REDUTOR_CAPACIDADE) 
 REFERENCES TASY.REDUTOR_CAPACIDADE (CD_REDUTOR_CAPACIDADE));

GRANT SELECT ON TASY.TAXA_CUSTO_OCIO TO NIVEL_1;

