ALTER TABLE TASY.RXT_APLIC_TRAT_ROENTGEN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_APLIC_TRAT_ROENTGEN CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_APLIC_TRAT_ROENTGEN
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_KVP           NUMBER(10),
  NR_SEQ_MA            NUMBER(10),
  NR_SEQ_APLICADOR     NUMBER(10),
  QT_TAMANHO_X         NUMBER(5),
  QT_TAMANHO_Y         NUMBER(5),
  NR_MINUTO_DURACAO    NUMBER(10),
  NR_SEQ_CAMPO_TRAT    NUMBER(10)               NOT NULL,
  NR_SEGUNDO_DURACAO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RXTAPTR_PK ON TASY.RXT_APLIC_TRAT_ROENTGEN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAPTR_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTAPTR_RADTPMA_FK_I ON TASY.RXT_APLIC_TRAT_ROENTGEN
(NR_SEQ_MA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAPTR_RADTPMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAPTR_RDTPKVP_FK_I ON TASY.RXT_APLIC_TRAT_ROENTGEN
(NR_SEQ_KVP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAPTR_RDTPKVP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAPTR_RXTAPLI_FK_I ON TASY.RXT_APLIC_TRAT_ROENTGEN
(NR_SEQ_APLICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAPTR_RXTAPLI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAPTR_RXTCPTR_FK_I ON TASY.RXT_APLIC_TRAT_ROENTGEN
(NR_SEQ_CAMPO_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAPTR_RXTCPTR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RXT_APLIC_TRAT_ROENTGEN_tp  after update ON TASY.RXT_APLIC_TRAT_ROENTGEN FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_KVP,1,4000),substr(:new.NR_SEQ_KVP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_KVP',ie_log_w,ds_w,'RXT_APLIC_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MA,1,4000),substr(:new.NR_SEQ_MA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MA',ie_log_w,ds_w,'RXT_APLIC_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MINUTO_DURACAO,1,4000),substr(:new.NR_MINUTO_DURACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MINUTO_DURACAO',ie_log_w,ds_w,'RXT_APLIC_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAMANHO_X,1,4000),substr(:new.QT_TAMANHO_X,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAMANHO_X',ie_log_w,ds_w,'RXT_APLIC_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APLICADOR,1,4000),substr(:new.NR_SEQ_APLICADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APLICADOR',ie_log_w,ds_w,'RXT_APLIC_TRAT_ROENTGEN',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RXT_APLIC_TRAT_ROENTGEN ADD (
  CONSTRAINT RXTAPTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_APLIC_TRAT_ROENTGEN ADD (
  CONSTRAINT RXTAPTR_RXTCPTR_FK 
 FOREIGN KEY (NR_SEQ_CAMPO_TRAT) 
 REFERENCES TASY.RXT_CAMPO_TRAT_ROENTGEN (NR_SEQUENCIA),
  CONSTRAINT RXTAPTR_RDTPKVP_FK 
 FOREIGN KEY (NR_SEQ_KVP) 
 REFERENCES TASY.RXT_KVP (NR_SEQUENCIA),
  CONSTRAINT RXTAPTR_RADTPMA_FK 
 FOREIGN KEY (NR_SEQ_MA) 
 REFERENCES TASY.RXT_MA (NR_SEQUENCIA),
  CONSTRAINT RXTAPTR_RXTAPLI_FK 
 FOREIGN KEY (NR_SEQ_APLICADOR) 
 REFERENCES TASY.RXT_APLICADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.RXT_APLIC_TRAT_ROENTGEN TO NIVEL_1;

