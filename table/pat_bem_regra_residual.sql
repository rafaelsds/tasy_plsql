ALTER TABLE TASY.PAT_BEM_REGRA_RESIDUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_BEM_REGRA_RESIDUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_BEM_REGRA_RESIDUAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_BEM           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_RESIDUAL          NUMBER(15,2)             NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATBRES_PATBEM_FK_I ON TASY.PAT_BEM_REGRA_RESIDUAL
(NR_SEQ_BEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PATBRES_PK ON TASY.PAT_BEM_REGRA_RESIDUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PAT_BEM_REGRA_RESIDUAL ADD (
  CONSTRAINT PATBRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_BEM_REGRA_RESIDUAL ADD (
  CONSTRAINT PATBRES_PATBEM_FK 
 FOREIGN KEY (NR_SEQ_BEM) 
 REFERENCES TASY.PAT_BEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PAT_BEM_REGRA_RESIDUAL TO NIVEL_1;

