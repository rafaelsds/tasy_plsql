ALTER TABLE TASY.SOFTWARE_ESTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SOFTWARE_ESTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SOFTWARE_ESTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SOFTWARE      NUMBER(10)               NOT NULL,
  NM_ESTACAO_LICENCA   VARCHAR2(60 BYTE)        NOT NULL,
  NM_USUARIO_ESTACAO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SOFTEST_PK ON TASY.SOFTWARE_ESTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOFTEST_SOFTWAR_FK_I ON TASY.SOFTWARE_ESTACAO
(NR_SEQ_SOFTWARE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOFTEST_SOFTWAR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SOFTWARE_ESTACAO ADD (
  CONSTRAINT SOFTEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SOFTWARE_ESTACAO ADD (
  CONSTRAINT SOFTEST_SOFTWAR_FK 
 FOREIGN KEY (NR_SEQ_SOFTWARE) 
 REFERENCES TASY.SOFTWARE (NR_SEQUENCIA));

GRANT SELECT ON TASY.SOFTWARE_ESTACAO TO NIVEL_1;

