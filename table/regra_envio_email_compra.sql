ALTER TABLE TASY.REGRA_ENVIO_EMAIL_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_ENVIO_EMAIL_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_ENVIO_EMAIL_COMPRA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_ASSUNTO               VARCHAR2(255 BYTE)   NOT NULL,
  DS_MENSAGEM_PADRAO       VARCHAR2(2000 BYTE)  NOT NULL,
  IE_TIPO_MENSAGEM         NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  IE_CONSIGNADO            VARCHAR2(1 BYTE)     NOT NULL,
  CD_GRUPO_MATERIAL        NUMBER(3),
  CD_SUBGRUPO_MATERIAL     NUMBER(3),
  CD_CLASSE_MATERIAL       NUMBER(5),
  CD_MATERIAL              NUMBER(6),
  IE_USUARIO               VARCHAR2(3 BYTE)     NOT NULL,
  DS_EMAIL_ADICIONAL       VARCHAR2(2000 BYTE),
  IE_SOMENTE_LIB_INTERNET  VARCHAR2(1 BYTE)     NOT NULL,
  CD_PERFIL_DISPARAR       NUMBER(5),
  IE_MOMENTO_ENVIO         VARCHAR2(1 BYTE)     NOT NULL,
  IE_ENVIA_PESSOA_DELEG    VARCHAR2(1 BYTE)     NOT NULL,
  DS_EMAIL_REMETENTE       VARCHAR2(255 BYTE),
  IE_EMAIL_DIA_UTIL        VARCHAR2(1 BYTE),
  DS_SENHA_REMETENTE       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REENECO_CLAMATE_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_CLASSE_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENECO_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REENECO_ESTABEL_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REENECO_GRUMATE_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_GRUPO_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENECO_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REENECO_MATERIA_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENECO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REENECO_PERFIL_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_PERFIL_DISPARAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENECO_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REENECO_PK ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REENECO_SUBMATE_FK_I ON TASY.REGRA_ENVIO_EMAIL_COMPRA
(CD_SUBGRUPO_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENECO_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.regra_env_email_compra_insert
BEFORE INSERT ON TASY.REGRA_ENVIO_EMAIL_COMPRA for each row
declare

ds_macro_disp_w	varchar2(4000);
ds_mensagem_padrao_w	varchar2(4000);
ds_palavra_w		varchar2(4000);
ds_lista_palavra_w	varchar2(4000);
qt_tamanho_lista_w	number(10);
ie_pos_separador_w	number(10);
ds_lista_caracter_w	varchar2(255) := ',.;:/?\|!#$%&*()-+=]}[{';

BEGIN

select	substr(obter_macro_tipo_msg_compra(:new.ie_tipo_mensagem),1,2000),
	replace(replace(:new.ds_mensagem_padrao,chr(13),' '),chr(10),' ') || ' '
into	ds_macro_disp_w,
	ds_mensagem_padrao_w
from	dual;

ds_lista_palavra_w	:= ds_mensagem_padrao_w;

while	ds_lista_palavra_w is not null loop
	begin
	qt_tamanho_lista_w := length(ds_lista_palavra_w);
	ie_pos_separador_w := instr(ds_lista_palavra_w,' ');

	if	(ie_pos_separador_w > 0) then

		ds_palavra_w 	   := substr(ds_lista_palavra_w,1,(ie_pos_separador_w - 1));
		ds_lista_palavra_w := substr(ds_lista_palavra_w,(ie_pos_separador_w + 1), qt_tamanho_lista_w);

		if	(substr(ds_palavra_w,length(ds_palavra_w),length(ds_palavra_w)) in
			(',','.',';',':','/','?','\','|','!','#','$','%','&','*','(',')','-','+','=',']','}','[','{')) then
			ds_palavra_w := substr(ds_palavra_w,1,length(ds_palavra_w)-1);
		end if;

		if	(substr(ds_palavra_w,1,1) = '@') and
			(instr(ds_macro_disp_w,ds_palavra_w) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(237422);
			/*(-20111,'Existem macros n�o permitidas na mensagem padr�o.');*/
		end if;
	else
		ds_lista_palavra_w := null;
	end if;
	end;
end loop;
END;
/


ALTER TABLE TASY.REGRA_ENVIO_EMAIL_COMPRA ADD (
  CONSTRAINT REENECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_ENVIO_EMAIL_COMPRA ADD (
  CONSTRAINT REENECO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REENECO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REENECO_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT REENECO_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT REENECO_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT REENECO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_DISPARAR) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.REGRA_ENVIO_EMAIL_COMPRA TO NIVEL_1;

