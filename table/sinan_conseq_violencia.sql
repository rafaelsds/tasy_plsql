ALTER TABLE TASY.SINAN_CONSEQ_VIOLENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SINAN_CONSEQ_VIOLENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SINAN_CONSEQ_VIOLENCIA
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_NOTIFICACAO            NUMBER(10)      NOT NULL,
  IE_ABORTO                     NUMBER(3)       NOT NULL,
  IE_GRAVIDEZ                   NUMBER(3)       NOT NULL,
  IE_DST                        NUMBER(3)       NOT NULL,
  IE_TENTATIVA_SUICIDIO         NUMBER(3)       NOT NULL,
  IE_TRANSTORNO_MENTAL          NUMBER(3)       NOT NULL,
  IE_TRANSTORNO_COMPORTAMENTAL  NUMBER(3)       NOT NULL,
  IE_ESTRESSE_POS_TRAUMATICO    NUMBER(3)       NOT NULL,
  DS_OUTRAS_CONSEQUENCIAS       VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SINCOVI_ESTABEL_FK_I ON TASY.SINAN_CONSEQ_VIOLENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINCOVI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SINCOVI_NOTSINA_FK_I ON TASY.SINAN_CONSEQ_VIOLENCIA
(NR_SEQ_NOTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINCOVI_NOTSINA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SINCOVI_PK ON TASY.SINAN_CONSEQ_VIOLENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINCOVI_PK
  MONITORING USAGE;


ALTER TABLE TASY.SINAN_CONSEQ_VIOLENCIA ADD (
  CONSTRAINT SINCOVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SINAN_CONSEQ_VIOLENCIA ADD (
  CONSTRAINT SINCOVI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SINCOVI_NOTSINA_FK 
 FOREIGN KEY (NR_SEQ_NOTIFICACAO) 
 REFERENCES TASY.NOTIFICACAO_SINAN (NR_SEQUENCIA));

GRANT SELECT ON TASY.SINAN_CONSEQ_VIOLENCIA TO NIVEL_1;

