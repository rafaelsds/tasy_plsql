ALTER TABLE TASY.MED_AVALIACAO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_AVALIACAO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_AVALIACAO_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  CD_MEDICO                  VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_TIPO_AVALIACAO      NUMBER(10),
  IE_AVALIACAO_PARCIAL       VARCHAR2(1 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  NR_PRESCRICAO              NUMBER(14),
  NR_SEQ_AVAL_COMPL          NUMBER(10),
  DT_LIBERACAO               DATE,
  NR_SEQ_CHECKUP             NUMBER(10),
  NR_SEQ_COM_CLIENTE         NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_APRESENTAR_WEB          VARCHAR2(1 BYTE),
  NR_SEQ_LOTE                NUMBER(10),
  NR_SEQ_AGENDA_PAC          NUMBER(10),
  NR_SEQ_CONTATO             NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_AGEINT              NUMBER(10),
  NR_SEQ_AGENDA_CONS         NUMBER(10),
  NR_SEQ_AVAL_PRE            NUMBER(10),
  IE_RESTRICAO_VISUALIZACAO  VARCHAR2(1 BYTE),
  NR_SEQ_MATERIAL            NUMBER(10),
  NR_SEQ_CHAMADO             NUMBER(10),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ATEPACU             NUMBER(10),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4),
  NR_SEQ_CONSULTA            NUMBER(10),
  NR_SEQ_REGULACAO           NUMBER(10),
  NR_SEQ_PRESCR              NUMBER(6),
  NR_SEQ_ATEND_CHECKLIST     NUMBER(10),
  NR_SEQ_CHECKLIST_ITEM      NUMBER(10),
  NR_SEQ_ATENDIMENTO         NUMBER(10),
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_UNIDADE                 VARCHAR2(100 BYTE),
  IE_ATUALIZA_MACRO          VARCHAR2(1 BYTE),
  NR_SEQ_PAC_HFP             NUMBER(10),
  NR_SEQ_PLANEJAMENTO        NUMBER(10),
  NR_SEQ_HEM_ETAPA           NUMBER(10),
  NR_SEQ_AFERESE_TERAP       NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_MAT_CPOE            NUMBER(10),
  NR_SEQ_EPISODIO            NUMBER(10),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NR_SEQ_PROC_CPOE           NUMBER(10),
  NR_SEQ_REQUISITO           NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_SOAP                NUMBER(10),
  PR_COMPLETUDE              NUMBER(5),
  NR_SEQ_ITEM_PRONT          NUMBER(10),
  NR_SEQ_ANAT_PAT_CPOE       NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_TEC_ANEST           NUMBER(10),
  IE_ACAO                    VARCHAR2(1 BYTE),
  NR_SEQ_RP_TRATAMENTO       NUMBER(10),
  IE_ETAPA_TRATAMENTO        VARCHAR2(1 BYTE),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  NR_SEQ_HEMO_CPOE           NUMBER(10),
  CD_DEPARTAMENTO_MED        NUMBER(5)          DEFAULT null,
  CD_ESPECIALIDADE_MED       NUMBER(5)          DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDAVPA_AGECONS_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_AGEINTE_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_AGEPACI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AGENDA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_ATCONSPEPA_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_ATENCHE_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ATEND_CHECKLIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_ATENCHE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_ATEPACI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_ATEPACU_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ATEPACU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_ATEPACU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_ATESOAP_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CAEPPA_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CHECKUP_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_CHECKUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_CHECKUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_CHLIPRO_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_CHECKLIST_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_CHLIPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_CIRTEAN_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_TEC_ANEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CIRURGI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_COMCLIE_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_COM_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_CPOANAP_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ANAT_PAT_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CPOEMAT_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_MAT_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CPOEPRO_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_PROC_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_CPOHEMO_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_HEMO_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_EMECHAM_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_CHAMADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_EMECHAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_EMEREGU_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_REGULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_EMEREGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_ESTABEL_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_HEMPLAE_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_HEM_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_HEMPLAN_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_PLANEJAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_HFPPACI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_PAC_HFP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_HFPPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_I1 ON TASY.MED_AVALIACAO_PACIENTE
(DT_AVALIACAO, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_I2 ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_LATREQU_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_REQUISITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_MEAVLOT_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_MEDAVCP_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AVAL_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_MEDAVCP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_MEDTIAV_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_TIPO_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_NAISINS_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_OFTCONS_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_PACATEN_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_PACATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_PEPOCIR_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_PERFIL_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_PERFIL_ATIVO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_PESFISI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_PESFISI_FK2_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_PESFISI_FK3_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDAVPA_PK ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_PRESMAT_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_PRESCRICAO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_PRESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_PRESMED_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_RPTRATA_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_RP_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_SANSOAT_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AFERESE_TERAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDAVPA_SETATEN_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_TASASDI_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_TASASDI_FK2_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVPA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDAVPA_VALOR_D_FK_I ON TASY.MED_AVALIACAO_PACIENTE
(NR_SEQ_AVAL_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_med_aval_pac_gerar_pt_upd
after update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

qt_prev_alta_w number(10);
dt_alta_prevista_w varchar2(40);
nr_prescricao_w number(10);

begin

    if(:new.nr_seq_tipo_avaliacao   = 3231 and :old.dt_liberacao is null and :new.dt_liberacao is not null and :new.ie_situacao = 'A')then
    
        hsj_plano_terapeutico.hsj_gerar_previsao_alta(:new.nr_sequencia, :new.nr_atendimento, :new.nm_usuario, :new.CD_MEDICO);
        hsj_plano_terapeutico.hsj_gerar_prescricao(:new.cd_pessoa_fisica, :new.cd_medico, :new.nr_atendimento, :new.nm_usuario, :new.nr_sequencia);
        
    end if;  
    
end hsj_med_aval_pac_gerar_pt_upd;
/


CREATE OR REPLACE TRIGGER TASY.med_avaliacao_paciente_insert
before update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare
nr_seq_atepacu_w	number(10);
begin
if	(:new.nr_atendimento is not null) then
	begin
	nr_seq_atepacu_w	:= Obter_Atepacu_paciente(:new.nr_atendimento,'A');
	if	(nr_seq_atepacu_w	> 0) then
		:new.nr_seq_atepacu	:= nr_seq_atepacu_w;

	end if;

	exception
	when others then
		null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.med_avaliacao_pac_pend_atual
after insert or update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
nm_usuario_w	varchar2(15);
ie_gerar_autor_aval_w varchar2(2);

begin

obter_param_usuario(281,1605,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_gerar_autor_aval_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_liberacao is null) then

	if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
	   (:new.DT_LIBERACAO_AUX is not null)then

		select 	obter_usuario_pf(:new.cd_medico)
		into	nm_usuario_w
		from	dual;

		Update  pep_item_pendente
		set		nm_usuario = nm_usuario_w
		where	NR_SEQ_AVALIACAO = :new.nr_sequencia;
	end if;

	ie_tipo_w := 'AV';
elsif	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XAV';
end if;

	if	(ie_tipo_w	is not null) then
		begin
		  Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,
										null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,:new.nr_seq_item_pront);

			if (nvl(ie_gerar_autor_aval_w,'N') = 'S') and (Obter_Funcao_Ativa = 281) Then
				Gerar_Avaliacao_Autorizacao(:new.nr_sequencia, :new.NR_SEQ_TIPO_AVALIACAO, :new.nr_atendimento, :new.dt_avaliacao, :new.DT_LIBERACAO, :new.nm_usuario_lib, :new.ie_situacao, :new.NM_USUARIO_INATIVACAO ,:new.DT_INATIVACAO);
			end if;
		end;
	end if;


<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.MED_AVALIACAO_PACIENTE_UPDATE
AFTER
UPDATE ON TASY.MED_AVALIACAO_PACIENTE FOR EACH ROW
DECLARE
qt_reg_w	number(1);
cd_evolucao_pendencia_w 	evolucao_paciente.cd_evolucao%type;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Almir em 15/07/2008 OS89916  */
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	Gerar_comunic_aval(:new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nr_seq_tipo_avaliacao, :new.dt_avaliacao, :new.nm_usuario);

	if(:new.nr_seq_contato is not null)then
		update	cih_contato a
		set	dt_lib_avaliacao	= sysdate,
			nm_usuario_lib_aval	= :new.nm_usuario
		where 	a.nr_sequencia 		= :new.nr_seq_contato;
	end if;
end if;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
	update	evolucao_paciente
	set	dt_inativacao = :new.dt_inativacao,
		IE_SITUACAO = 'I',
		NM_USUARIO_INATIVACAO = :new.NM_USUARIO_INATIVACAO,
		DS_JUSTIFICATIVA = :new.DS_JUSTIFICATIVA
	where	nr_seq_avaliacao = :new.nr_sequencia;

	select 	max(cd_evolucao)
	into   	cd_evolucao_pendencia_w
	from   	evolucao_paciente
	where	nr_seq_avaliacao = :new.nr_sequencia;

	if(cd_evolucao_pendencia_w > 0) then
		Gerar_registro_pendente_PEP('XE', cd_evolucao_pendencia_w, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario,'A');
	end if;

	if(:new.nr_seq_contato is not null)then
		update	cih_contato a
		set	dt_inativacao_aval	= sysdate,
			nm_usuario_inat_aval	= :new.nm_usuario
		where 	a.nr_sequencia 		= :new.nr_seq_contato;
	end if;
end if;


<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.MED_AVAL_PAC_SBIS_IN
before insert or update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);

begin

IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
										 nvl(obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'), obter_pessoa_fisica_usuario(:new.nm_usuario,'C')),
										 obter_desc_expressao(284113),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 :new.nm_usuario);
else
	ie_inativacao_w := 'N';

	if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
		ie_inativacao_w := 'S';
	end if;

	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 nvl(obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'), obter_pessoa_fisica_usuario(:new.nm_usuario,'C')),
										 obter_desc_expressao(284113),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 :new.nm_usuario);
end if;
    EXCEPTION
		WHEN OTHERS THEN
			Raise_application_error(-20000,obter_desc_expressao(1036226));

end;
/


CREATE OR REPLACE TRIGGER TASY.med_avaliacao_paciente_delete
before delete ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

ie_possib_exluir_lib_w   varchar2(1);
cd_estabelecimento_w     number          := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_w             varchar2(255)   := wheb_usuario_pck.get_nm_usuario;
qt_reg_w		NUMBER(1);

begin
IF	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  THEN
	GOTO Final;
END IF;

obter_param_usuario(944, 5, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w,ie_possib_exluir_lib_w);

if (ie_possib_exluir_lib_w <> 'S') and (:old.dt_liberacao	is not null) then
	--N�o � poss�vel excluir uma avalia��o j� liberada.
	Wheb_mensagem_pck.exibir_mensagem_abort(191755);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_med_avaliac_pac_pend_atual
after insert or update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

qt_reg_w        number(1);
ie_tipo_w        varchar2(10);
nm_usuario_w        varchar2(15) := :new.nm_usuario;


begin
if    (wheb_usuario_pck.get_ie_executar_trigger    = 'N')  then
    goto Final;
end if;


    if(:new.dt_liberacao is null)then
        if(nvl(:new.ie_avaliador_aux,'N') = 'S') then
            if(:new.dt_liberacao_aux is null)then
                
                ie_tipo_w := 'AV';
                nm_usuario_w := :new.nm_usuario;
                    
            elsif(:new.dt_liberacao_aux is not null)then  
                    
                ie_tipo_w := 'XAV';
                nm_usuario_w := :new.nm_usuario;
                Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w);
                    
                ie_tipo_w := 'AV';
                nm_usuario_w := substr(obter_usuario_pessoa(:new.cd_medico),1,15);
                    
            end if;
        end if;
        
    elsif(nvl(:new.ie_avaliador_aux,'N') = 'S' and :new.dt_liberacao_aux is not null)then
        ie_tipo_w := 'XAV';
        nm_usuario_w := :new.nm_usuario;
    end if;
    
    if(ie_tipo_w is not null)then
        Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 7);
    end if;

<<Final>>
qt_reg_w    := 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.block_med_avaliacao_paciente
BEFORE update ON TASY.MED_AVALIACAO_PACIENTE FOR EACH ROW
DECLARE

BEGIN
	if (wheb_usuario_pck.get_ie_executar_trigger <> 'N' and :new.dt_liberacao is null and :old.dt_liberacao is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(267643);--Nao e possivel alterar a data de liberacao de um registro ja liberado.
	end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.hsj_gerar_evol_sofa
after update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

ds_evolucao_w long;
ds_paciente_w varchar2(200);
qt_idade_w varchar2(200);
qt_sofa_w  varchar2(200);
qt_resp_w varchar2(200);
qt_coag_w varchar2(200);
qt_cardio_w varchar2(200);
qt_glasgow_w varchar2(200);
qt_figado_w varchar2(200);
qt_renal_w varchar2(200);
qt_spict_w  varchar2(200);
qt_ecog_w varchar2(200);
qt_pontuacao_w varchar2(200);


begin
          
    if(:new.NR_SEQ_TIPO_AVALIACAO=3621 and :new.dt_liberacao is not null and :old.dt_liberacao is null) then
        
        ds_paciente_w:='Paciente: '||OBTER_NOME_PACIENTE(:new.nr_atendimento);
        qt_idade_w:=', Idade: '|| OBTER_DADOS_PF(:new.cd_pessoa_fisica,'I');
        qt_sofa_w:=CHR(10)||CHR(10)||' SOFA: '|| AVAL(:new.nr_sequencia,'34301');
        qt_resp_w:=CHR(10)||'Respira��o: '|| AVAL(:new.nr_sequencia,'34238');
        qt_coag_w:=CHR(10)||'Coagula��o (Plaquetas): '|| AVAL(:new.nr_sequencia,'34247');
        qt_cardio_w:=CHR(10)||'Cardiovascular: '|| AVAL(:new.nr_sequencia,'34248');
        qt_glasgow_w:=CHR(10)||'Glasgow: '|| AVAL(:new.nr_sequencia,'34249');
        qt_figado_w:=CHR(10)||'F�gado (Bilirrubina): '|| AVAL(:new.nr_sequencia,'34251');
        qt_renal_w:=CHR(10)||'Renal (Creatinina): '|| AVAL(:new.nr_sequencia,'34253');
        qt_spict_w:=CHR(10)||CHR(10)||'SPICT-BR: '|| case when SUBSTR(AVAL(:new.nr_sequencia,'34303'),0,1)  > 0 then 'Positivo - Com comorbidade grave, com expectativa de sobrevida menor que um ano. (3 Pontos)' else 
                                                                                                                                 'Negativo - Sem comorbidade grave, com expectativa de sobrevida menor que um ano. (0 Pontos)' end;
        qt_ecog_w:=CHR(10)||CHR(10)||'ECOG: '|| AVAL(:new.nr_sequencia,'34304');                                                                                                                                      
       
        qt_pontuacao_w:=CHR(10)||CHR(10)||'Pontua��o Total AMIB: '|| AVAL(:new.nr_sequencia,'34307');
        
        ds_evolucao_w:=ds_paciente_w||qt_idade_w||qt_sofa_w||qt_resp_w||qt_coag_w||qt_cardio_w||qt_glasgow_w||qt_figado_w||qt_renal_w||qt_spict_w||qt_ecog_w||qt_pontuacao_w;
   
        HSJ_GERAR_EVOLUCAO(OBTER_FUNCAO_USUARIO(NVL(OBTER_USUARIO_ATIVO,:new.nm_usuario)) , :new.nr_atendimento, ds_evolucao_w, 'PA', NVL(OBTER_USUARIO_ATIVO,:new.nm_usuario));

end if;
    
end hsj_gerar_evol_sofa;
/


CREATE OR REPLACE TRIGGER TASY.MED_AVALIACAO_PACIENTE_tp  after update ON TASY.MED_AVALIACAO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'MED_AVALIACAO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'MED_AVALIACAO_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.MED_AVALIACAO_PACIENTE_ATUAL
before UPDATE or insert ON TASY.MED_AVALIACAO_PACIENTE FOR EACH ROW
DECLARE
qt_reg_w	number(1);
qt_hora_w	number(15,4);
ie_tipo_w			varchar2(10);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') or
	((:old.nr_seq_assinatura is null) and
	(:new.nr_seq_assinatura is not null)) then
	goto Final;
end if;

if	(nvl(:old.DT_AVALIACAO,sysdate+10) <> :new.DT_AVALIACAO) and
	(:new.DT_AVALIACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_AVALIACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

Obter_Param_Usuario(281,976,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,0,qt_hora_w);

if	(nvl(qt_hora_w,-1)  < 0 ) then
	select	nvl(max(QT_HORAS_PASSADO_AVAL),-1)
	into	qt_hora_w
	from	parametro_medico
	where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
end if;


if	(qt_hora_w >= 0) and
	(:new.dt_avaliacao < (sysdate - qt_hora_w/24)) then
	--A data da avaliacao nao pode ser menor que '|| to_char(qt_hora_w)||' hora(s) em relacao a data atual.#@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(262184, 'QT_EXAME='||to_char(qt_hora_w));
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'AV';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XAV';
end if;

if ((nvl(:new.cd_pessoa_fisica,0) > 0) and
	(ie_tipo_w	is not null)) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario,
								null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,:new.nr_seq_item_pront);
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_bundle_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_liberacao,
							:new.nr_seq_tipo_avaliacao, :new.dt_atualizacao, :new.dt_avaliacao);
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_MED_AVALIACAO_PACIENTE
before insert or update ON TASY.MED_AVALIACAO_PACIENTE for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MED_AVAL_PAC_SBIS_DEL
before delete ON TASY.MED_AVALIACAO_PACIENTE for each row
declare
nr_log_seq_w		number(10);

begin
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(493387) ,
										 wheb_usuario_pck.get_nm_maquina,
										 nvl(obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'),:old.cd_pessoa_fisica),
										 obter_desc_expressao(284113),
										 :old.nr_atendimento,
										 :old.dt_liberacao,
										 :old.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


ALTER TABLE TASY.MED_AVALIACAO_PACIENTE ADD (
  CONSTRAINT MEDAVPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_AVALIACAO_PACIENTE ADD (
  CONSTRAINT MEDAVPA_CPOHEMO_FK 
 FOREIGN KEY (NR_SEQ_HEMO_CPOE) 
 REFERENCES TASY.CPOE_HEMOTERAPIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MEDAVPA_SANSOAT_FK 
 FOREIGN KEY (NR_SEQ_AFERESE_TERAP) 
 REFERENCES TASY.SAN_SOLIC_AFERESE_TERAP (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CPOEMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_CPOE) 
 REFERENCES TASY.CPOE_MATERIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_CAEPPA_FK 
 FOREIGN KEY (NR_SEQ_EPISODIO) 
 REFERENCES TASY.EPISODIO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROC_CPOE) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_PESFISI_FK3 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDAVPA_LATREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISITO) 
 REFERENCES TASY.LATAM_REQUISITO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CPOANAP_FK 
 FOREIGN KEY (NR_SEQ_ANAT_PAT_CPOE) 
 REFERENCES TASY.CPOE_ANATOMIA_PATOLOGICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_MEDTIAV 
 FOREIGN KEY (NR_SEQ_TIPO_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MEDAVPA_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDAVPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDAVPA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_MEDAVCP_FK 
 FOREIGN KEY (NR_SEQ_AVAL_COMPL) 
 REFERENCES TASY.MED_AVAL_COMPLETA_PAC (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_COM_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CHECKUP_FK 
 FOREIGN KEY (NR_SEQ_CHECKUP) 
 REFERENCES TASY.CHECKUP (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_MEAVLOT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.MED_AVALIACAO_LOTE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT MEDAVPA_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_VALOR_D_FK 
 FOREIGN KEY (NR_SEQ_AVAL_PRE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_PAC) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_EMECHAM_FK 
 FOREIGN KEY (NR_SEQ_CHAMADO) 
 REFERENCES TASY.EME_CHAMADO (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MEDAVPA_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_EMEREGU_FK 
 FOREIGN KEY (NR_SEQ_REGULACAO) 
 REFERENCES TASY.EME_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_ATENCHE_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CHECKLIST) 
 REFERENCES TASY.ATENDIMENTO_CHECKLIST (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_CHLIPRO_FK 
 FOREIGN KEY (NR_SEQ_CHECKLIST_ITEM) 
 REFERENCES TASY.CHECKLIST_PROCESSO_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO),
  CONSTRAINT MEDAVPA_HEMPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANEJAMENTO) 
 REFERENCES TASY.HEM_PLANEJAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDAVPA_HFPPACI_FK 
 FOREIGN KEY (NR_SEQ_PAC_HFP) 
 REFERENCES TASY.HFP_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_HEMPLAE_FK 
 FOREIGN KEY (NR_SEQ_HEM_ETAPA) 
 REFERENCES TASY.HEM_PLAN_ETAPA (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_RPTRATA_FK 
 FOREIGN KEY (NR_SEQ_RP_TRATAMENTO) 
 REFERENCES TASY.RP_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT MEDAVPA_CIRTEAN_FK 
 FOREIGN KEY (NR_SEQ_TEC_ANEST) 
 REFERENCES TASY.CIRURGIA_TEC_ANESTESICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_AVALIACAO_PACIENTE TO NIVEL_1;

