ALTER TABLE TASY.REGRA_CONCEITO_AVAL_FORNEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CONCEITO_AVAL_FORNEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CONCEITO_AVAL_FORNEC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_CONCEITO_AVALIACAO  VARCHAR2(1 BYTE)       NOT NULL,
  IE_FAIXA_MINIMO        NUMBER(3)              NOT NULL,
  IE_FAIXA_MAXIMO        NUMBER(3)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECOAFO_ESTABEL_FK_I ON TASY.REGRA_CONCEITO_AVAL_FORNEC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RECOAFO_PK ON TASY.REGRA_CONCEITO_AVAL_FORNEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECOAFO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_CONCEITO_AVAL_FORNEC_tp  after update ON TASY.REGRA_CONCEITO_AVAL_FORNEC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_CONCEITO_AVAL_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONCEITO_AVALIACAO,1,4000),substr(:new.IE_CONCEITO_AVALIACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONCEITO_AVALIACAO',ie_log_w,ds_w,'REGRA_CONCEITO_AVAL_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FAIXA_MAXIMO,1,4000),substr(:new.IE_FAIXA_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FAIXA_MAXIMO',ie_log_w,ds_w,'REGRA_CONCEITO_AVAL_FORNEC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FAIXA_MINIMO,1,4000),substr(:new.IE_FAIXA_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FAIXA_MINIMO',ie_log_w,ds_w,'REGRA_CONCEITO_AVAL_FORNEC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_CONCEITO_AVAL_FORNEC ADD (
  CONSTRAINT RECOAFO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CONCEITO_AVAL_FORNEC ADD (
  CONSTRAINT RECOAFO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_CONCEITO_AVAL_FORNEC TO NIVEL_1;

