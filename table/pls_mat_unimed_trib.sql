ALTER TABLE TASY.PLS_MAT_UNIMED_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MAT_UNIMED_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MAT_UNIMED_TRIB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_MAT_UNIMED       NUMBER(10)            NOT NULL,
  VL_PERC_ICMS            NUMBER(7,2),
  VL_PMC                  NUMBER(15,4),
  VL_TCL                  NUMBER(2),
  DT_INICIO_VIGENCIA      DATE,
  DT_INICIO_VIGENCIA_REF  DATE,
  ID_ALC                  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMUTR_I1 ON TASY.PLS_MAT_UNIMED_TRIB
(NR_SEQ_MAT_UNIMED, VL_PERC_ICMS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMUTR_I2 ON TASY.PLS_MAT_UNIMED_TRIB
(NR_SEQ_MAT_UNIMED, DT_INICIO_VIGENCIA_REF, VL_PERC_ICMS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMUTR_PK ON TASY.PLS_MAT_UNIMED_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUTR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUTR_PLSMAUN_FK_I ON TASY.PLS_MAT_UNIMED_TRIB
(NR_SEQ_MAT_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUTR_PLSMAUN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_mat_unimed_trib_atual
before insert or update ON TASY.PLS_MAT_UNIMED_TRIB for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');

end pls_mat_unimed_trib_atual;
/


ALTER TABLE TASY.PLS_MAT_UNIMED_TRIB ADD (
  CONSTRAINT PLSMUTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MAT_UNIMED_TRIB ADD (
  CONSTRAINT PLSMUTR_PLSMAUN_FK 
 FOREIGN KEY (NR_SEQ_MAT_UNIMED) 
 REFERENCES TASY.PLS_MATERIAL_UNIMED (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_MAT_UNIMED_TRIB TO NIVEL_1;

