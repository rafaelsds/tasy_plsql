ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONVENIO_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONVENIO_ENVIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AUTORIZACAO   NUMBER(10)               NOT NULL,
  DT_ENVIO             DATE                     NOT NULL,
  DS_DESTINO           VARCHAR2(255 BYTE)       NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUCOENV_AUTCONV_FK_I ON TASY.AUTORIZACAO_CONVENIO_ENVIO
(NR_SEQ_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCOENV_AUTCONV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUCOENV_PK ON TASY.AUTORIZACAO_CONVENIO_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ENVIO ADD (
  CONSTRAINT AUCOENV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ENVIO ADD (
  CONSTRAINT AUCOENV_AUTCONV_FK 
 FOREIGN KEY (NR_SEQ_AUTORIZACAO) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AUTORIZACAO_CONVENIO_ENVIO TO NIVEL_1;

