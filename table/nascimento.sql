ALTER TABLE TASY.NASCIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NASCIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NASCIMENTO
(
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  NR_SEQUENCIA               NUMBER(3)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_NASCIMENTO              DATE,
  IE_CREDE                   VARCHAR2(1 BYTE),
  IE_UNICO_NASC_VIVO         VARCHAR2(1 BYTE),
  QT_GEMELAR_VIVOS           NUMBER(2),
  QT_GEMELAR_MORTOS          NUMBER(2),
  QT_APGAR_PRIM_MIN          NUMBER(3,1),
  QT_APGAR_QUINTO_MIN        NUMBER(3,1),
  IE_ELIM_URINA_PARTO        VARCHAR2(1 BYTE),
  IE_ELIM_MECONIO_PARTO      VARCHAR2(1 BYTE),
  QT_SEM_IG_TOTAL            NUMBER(5,2),
  DS_MANOBRAS_REALIZADAS     VARCHAR2(255 BYTE),
  DS_MEDIC_UTILIZADOS        VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_SEXO                    VARCHAR2(1 BYTE),
  QT_SEM_IG                  NUMBER(5,2),
  IE_TAMANHO                 VARCHAR2(3 BYTE),
  QT_PESO                    NUMBER(4),
  QT_ALTURA                  NUMBER(4,1),
  QT_PC                      NUMBER(3,1),
  QT_PT                      NUMBER(3,1),
  QT_PA                      NUMBER(3,1),
  QT_TEMP_AXILAR             NUMBER(3,1),
  QT_TEMP_RETAL              NUMBER(3,1),
  IE_ELIM_URINA_BERC         VARCHAR2(1 BYTE),
  IE_ELIM_MECONIO_BERC       VARCHAR2(1 BYTE),
  QT_FA_X                    NUMBER(3,1),
  QT_FA_Y                    NUMBER(3,1),
  QT_FP_X                    NUMBER(3,1),
  QT_FP_Y                    NUMBER(3,1),
  IE_PALATO                  VARCHAR2(1 BYTE),
  DS_OBSERVACAO_CABECA       VARCHAR2(255 BYTE),
  QT_FC                      NUMBER(3),
  QT_FR                      NUMBER(3),
  IE_CLAVICULAS              VARCHAR2(1 BYTE),
  DS_AUSCULTA                VARCHAR2(255 BYTE),
  DS_OBSERVACAO_ABDOME       VARCHAR2(255 BYTE),
  DS_OBSERVACAO_OUTROS       VARCHAR2(255 BYTE),
  CD_PEDIATRA                VARCHAR2(10 BYTE),
  IE_DESTINO                 VARCHAR2(15 BYTE),
  NR_COR_PELE                NUMBER(10),
  IE_TIPO_NASCIMENTO         VARCHAR2(3 BYTE),
  IE_SITUACAO_RN             VARCHAR2(3 BYTE),
  DT_OBITO                   DATE,
  NR_ATEND_RN                NUMBER(10),
  CD_PEDIATRA_RN             VARCHAR2(10 BYTE),
  IE_HIV_RAPIDO              VARCHAR2(1 BYTE),
  IE_TRIAGEM_NEONATAL        VARCHAR2(1 BYTE),
  DT_COLETA_TRIAGEM          DATE,
  IE_TIPO_SANGUE             VARCHAR2(2 BYTE),
  IE_FATOR_RH                VARCHAR2(1 BYTE),
  NR_DNV                     VARCHAR2(40 BYTE),
  DT_PREV_TESTE_PEZINHO      DATE,
  NR_DFM                     VARCHAR2(40 BYTE),
  DS_OBSERVACAO_GENITAIS     VARCHAR2(255 BYTE),
  DS_OBSERVACAO_PELE         VARCHAR2(255 BYTE),
  DS_OBSERVACAO_MEMBROS      VARCHAR2(255 BYTE),
  IE_AMAM_CONTATO_PELE       VARCHAR2(15 BYTE),
  IE_AMAM_COLOSTRO           VARCHAR2(15 BYTE),
  IE_AMAM_TEMPO_SUCCAO       VARCHAR2(15 BYTE),
  IE_AMAM_HGT_RN             VARCHAR2(1 BYTE),
  QT_AMAM_HGT_RN             NUMBER(15),
  IE_VITAMINA_K              VARCHAR2(1 BYTE),
  IE_CELULA_TRONCO           VARCHAR2(1 BYTE),
  IE_TESTE_OLHINHO           VARCHAR2(15 BYTE),
  QT_PLACENTA                NUMBER(10,2),
  CD_PESSOA_RESP             VARCHAR2(10 BYTE),
  NR_PULSEIRA                NUMBER(10),
  QT_APGAR_TER_MIN           NUMBER(3,1),
  QT_APGAR_DECIMO_MIN        NUMBER(3,1),
  IE_ASPIRACAO_VAS           VARCHAR2(1 BYTE),
  IE_ASPIRACAO_GAS           VARCHAR2(1 BYTE),
  IE_ASPIRACAO_TRAQ          VARCHAR2(1 BYTE),
  DS_ASPIRACAO_VAS           VARCHAR2(255 BYTE),
  DS_ASPIRACAO_GAS           VARCHAR2(255 BYTE),
  DS_ASPIRACAO_TRAQ          VARCHAR2(255 BYTE),
  IE_MASCARA                 VARCHAR2(1 BYTE),
  IE_PRESSAO_POSITIVA        VARCHAR2(1 BYTE),
  IE_ENTUBACAO               VARCHAR2(1 BYTE),
  IE_MASSAG_CARDIACA         VARCHAR2(1 BYTE),
  IE_MEDICAMENTO             VARCHAR2(1 BYTE),
  IE_AMAM_MAT_EXCLUSIVO      VARCHAR2(15 BYTE),
  CD_PESSOA_RN               VARCHAR2(10 BYTE),
  QT_DIA_IG_TOTAL            NUMBER(5,2),
  QT_DIA_IG                  NUMBER(5,2),
  DS_AMAMENTACAO_ANT         VARCHAR2(250 BYTE),
  IE_MAMILOS                 VARCHAR2(2 BYTE),
  IE_AMAMENTACAO_ANT         VARCHAR2(1 BYTE),
  IE_FISSURA_MAMARIA         VARCHAR2(1 BYTE),
  IE_INTERC_AMAMENTACAO      VARCHAR2(1 BYTE),
  DS_INTERC_AMAMENTACAO      VARCHAR2(200 BYTE),
  IE_CIRURGIA_MAMA           VARCHAR2(1 BYTE),
  DS_CIRURGIA_MAMA           VARCHAR2(255 BYTE),
  IE_PRIMEIRA_MAMADA         VARCHAR2(3 BYTE),
  QT_PESO_SALA_PARTO         NUMBER(4),
  CD_PAI_RN                  VARCHAR2(10 BYTE),
  DT_ENTREGA_DNV             DATE,
  IE_MALFORMACAO             VARCHAR2(3 BYTE),
  IE_PERMEAB_NASAL_DIR       VARCHAR2(1 BYTE),
  IE_PERMEABILIDADE_ANAL     VARCHAR2(1 BYTE),
  IE_TIPO_PERMEAB_ANAL       VARCHAR2(1 BYTE),
  IE_ESOFAGO                 VARCHAR2(1 BYTE),
  IE_COLORACAO               VARCHAR2(1 BYTE),
  IE_ICTERICA                VARCHAR2(1 BYTE),
  QT_TEMP_SALA_PARTO         NUMBER(3,1),
  IE_PERMEAB_NASAL_ESQ       VARCHAR2(1 BYTE),
  IE_TRANSP_MEMBRANA         VARCHAR2(1 BYTE),
  IE_ASP_FACE_FETAL          VARCHAR2(1 BYTE),
  IE_ASP_FACE_MATERNA        VARCHAR2(1 BYTE),
  IE_ANATOMIA_PATOLOGICA     VARCHAR2(1 BYTE),
  IE_CATETERISMO             VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_LOCAL_CERT_NASC     NUMBER(10),
  IE_VITAMINA_A              VARCHAR2(1 BYTE),
  QT_PLACENTA_GR             NUMBER(10,2),
  IE_UNID_MED_PESO           VARCHAR2(10 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_PARTO_NORMAL            VARCHAR2(1 BYTE),
  IE_PARTO_DISTOCICO         VARCHAR2(1 BYTE),
  IE_PARTO_CESARIA           VARCHAR2(1 BYTE),
  IE_LAQUEADURA              VARCHAR2(1 BYTE),
  IE_PARTO_FORCEPS           VARCHAR2(1 BYTE),
  IE_PARTO_EPISIO            VARCHAR2(1 BYTE),
  IE_PARTO_ANALGESIA         VARCHAR2(1 BYTE),
  NR_SEQ_CERT_PARTO          NUMBER(10),
  NR_SEQ_PROF_PARTO          NUMBER(10),
  NR_SEQ_PERSON_CER          NUMBER(10),
  NR_SEQ_PERSON_RES          NUMBER(10),
  NR_SEQ_PESSOA_ENDERECO     NUMBER(10),
  NR_DDI_TELEFONE            VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE            VARCHAR2(3 BYTE),
  NR_TELEFONE                VARCHAR2(15 BYTE),
  DS_ESPEC_CERT              VARCHAR2(25 BYTE),
  DS_CEDULA_PROF_CERT        VARCHAR2(20 BYTE),
  DS_ESPEC_RESP              VARCHAR2(255 BYTE),
  IE_LOCAL_NASC              VARCHAR2(3 BYTE),
  IE_OUTRO_TIPO_PARTO        VARCHAR2(1 BYTE),
  DS_OUTRO_TIPO_PARTO        VARCHAR2(25 BYTE),
  QT_SILVERMAN_EXAME         NUMBER(2),
  IE_TEXTURA_PELE            VARCHAR2(10 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  QT_APGAR_VINTE_MIN         NUMBER(3,1),
  QT_APGAR_QUINZE_MIN        NUMBER(3,1),
  NR_SEQ_FORMULARIO          NUMBER(10),
  IE_DISPROPORCAO_CABECA     VARCHAR2(1 BYTE),
  DS_TRANSPORTE_TERAPIA      VARCHAR2(50 BYTE),
  IE_CONTROLE_RECOMENDADO    VARCHAR2(1 BYTE),
  IE_ANALISE_SANGUE          VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_POS_BIRTH           NUMBER(10),
  NR_SEQ_TYP_DEL             NUMBER(12),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_PARENTS_CONSANGUINEOUS  VARCHAR2(1 BYTE),
  IE_FAMILY_MEMBER_CANCER    VARCHAR2(1 BYTE),
  DS_COMMENT                 VARCHAR2(4000 BYTE),
  DT_ESTIMATED_DELIVERY      DATE,
  IE_ABNORMAL_BIRTH          VARCHAR2(1 BYTE),
  CD_CLASSIF_ABNORMAL_BIRTH  NUMBER(10),
  NR_BIRTH_NUMBER            NUMBER(6),
  NR_SEQ_INTERNO             NUMBER(10),
  CD_ARTERIAL_PH             VARCHAR2(10 BYTE),
  CD_VENOUS_PH               VARCHAR2(10 BYTE),
  CD_ARTERIAL_BASE_EXCESS    VARCHAR2(10 BYTE),
  IE_BLOOD_GAS_UMBILICAL     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NASCIME_ATCONSPEPA_FK_I ON TASY.NASCIMENTO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_ATEPACI_FK_I ON TASY.NASCIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_ATEPACI_FK2_I ON TASY.NASCIMENTO
(NR_ATEND_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_CATATEP_FK_I ON TASY.NASCIMENTO
(NR_SEQ_PROF_PARTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_CATCERP_FK_I ON TASY.NASCIMENTO
(NR_SEQ_CERT_PARTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_CATLCNA_FK_I ON TASY.NASCIMENTO
(NR_SEQ_LOCAL_CERT_NASC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_CORPELE_FK_I ON TASY.NASCIMENTO
(NR_COR_PELE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NASCIME_CORPELE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NASCIME_EHRREGI_FK_I ON TASY.NASCIMENTO
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_I ON TASY.NASCIMENTO
(DT_NASCIMENTO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_MEDICO_FK_I ON TASY.NASCIMENTO
(CD_PEDIATRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_PESFISI_FK_I ON TASY.NASCIMENTO
(CD_PEDIATRA_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_PESFISI_FK2_I ON TASY.NASCIMENTO
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_PESFISI_FK3_I ON TASY.NASCIMENTO
(CD_PESSOA_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_PESFISI_FK4_I ON TASY.NASCIMENTO
(CD_PAI_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_PESSEND_FK_I ON TASY.NASCIMENTO
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NASCIME_PK ON TASY.NASCIMENTO
(NR_ATENDIMENTO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_TASASDI_FK_I ON TASY.NASCIMENTO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NASCIME_TASASDI_FK2_I ON TASY.NASCIMENTO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NASCIME_UK ON TASY.NASCIMENTO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nascimento_afterpost
after insert or update ON TASY.NASCIMENTO for each row
declare

cd_pessoa_rn_w		varchar2(10);
ie_operacao_w		varchar2(1)	:=	'X';
reg_integracao_w		gerar_int_padrao.reg_integracao;
nr_seq_episodio_w		atendimento_paciente.nr_seq_episodio%type;
cd_pessoa_fisica_w	atendimento_paciente.cd_pessoa_fisica%type;

begin
if	(:new.nr_atend_rn is not null) then
	gerar_igc_rn(:new.nr_atend_rn,:new.nm_usuario);
end if;

if	(nvl(:new.ie_sexo,'0') <> nvl(:old.ie_sexo,'0')) then
	begin
	if	(:new.cd_pessoa_rn is not null) then
		cd_pessoa_rn_w	:=	:new.cd_pessoa_rn;
	elsif	(:new.nr_atend_rn is not null) then
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_rn_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atend_rn;
	end if;

	if	(cd_pessoa_rn_w is not null) then
		update	pessoa_fisica
		set	ie_sexo = :new.ie_sexo
		where	cd_pessoa_fisica = cd_pessoa_rn_w;
	end if;
	end;
end if;

if	(inserting) and (:new.dt_liberacao is not null) and (:new.dt_inativacao is null) then
	ie_operacao_w	:=	'I';
elsif	(updating) and (:new.dt_liberacao is not null) and (:old.dt_liberacao is null) then
	ie_operacao_w	:=	'I';
elsif	(updating) and (:new.dt_inativacao is not null) and (:old.dt_inativacao is null) then
	ie_operacao_w	:=	'E';
elsif	(updating) and (:new.dt_liberacao is not null) and (:new.dt_inativacao is null) then
	ie_operacao_w	:=	'A';
end if;

if	(ie_operacao_w <> 'X') then
	begin
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	 = :new.nr_atendimento;

	reg_integracao_w.ie_operacao	:=	ie_operacao_w;
	reg_integracao_w.cd_pessoa_fisica	:=	cd_pessoa_fisica_w;
	reg_integracao_w.nr_atendimento	:=	:new.nr_atendimento;

	gerar_int_padrao.gravar_integracao('190', :new.nr_atendimento || '�' || :new.nr_sequencia, nvl(obter_usuario_ativo,:new.nm_usuario), reg_integracao_w);
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nascimento_delete
before delete ON TASY.NASCIMENTO for each row
declare
cd_estabelecimento_w	number(10);
IE_CANC_ATEND_RN_w	varchar2(10);

begin

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

select	max(IE_CANC_ATEND_RN)
into	IE_CANC_ATEND_RN_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(IE_CANC_ATEND_RN_w	= 'S') and
	(:old.nr_atend_rn	is not null) then
	cancelar_atend_rn(:old.nr_atend_rn,:old.nm_usuario);
end if;


if	(:old.NR_DNV is not null) and
	(length(somente_numero(:old.NR_DNV))	= length(:old.NR_DNV)) then
	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'S'
	where	nr_declaracao	= :old.NR_DNV
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DNV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.NASCIMENTO_tp  after update ON TASY.NASCIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.IE_UNICO_NASC_VIVO,1,4000),substr(:new.IE_UNICO_NASC_VIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UNICO_NASC_VIVO',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PULSEIRA,1,4000),substr(:new.NR_PULSEIRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_PULSEIRA',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DNV,1,4000),substr(:new.NR_DNV,1,4000),:new.nm_usuario,nr_seq_w,'NR_DNV',ie_log_w,ds_w,'NASCIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.nascimento_insert
before insert or update ON TASY.NASCIMENTO for each row
declare

ds_tipo_nascimento_w	varchar2(255);
nr_digitos_w		number(2);
ds_consistencia_w	varchar2(255);
cd_estabelecimento_w	number(4,0);
qt_reg_w		number(1);
qt_benef_plano_w	number(10);
qt_solic_lead_w		number(10);
dt_rescisao_w		date;
dt_liberacao_w		date;
ie_gerar_lead_nascimento_w	Varchar2(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

Obter_Param_Usuario(281,318,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ds_tipo_nascimento_w);
Obter_Param_Usuario(281,203,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,nr_digitos_w);
Obter_Param_Usuario(1237,17,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_gerar_lead_nascimento_w);

if	(:new.dt_prev_teste_pezinho is null) then
	:new.dt_prev_teste_pezinho := :new.dt_nascimento + 2;

end if;

if	(ds_tipo_nascimento_w is not null) and
	(obter_se_contido(:new.ie_tipo_nascimento,ds_tipo_nascimento_w) = 'S') and
	(:new.nr_dnv is null) then
	ds_consistencia_w :=  obter_texto_dic_objeto(289090, wheb_usuario_pck.get_nr_seq_idioma, null) || chr(13) || chr(10);
end if;

if	((:new.ie_unico_nasc_vivo = 'S') and (:new.ie_tipo_nascimento in (2,5,7,8,9,11))) /*codigos dos tipos de nascimento referente a morto */
	or ((:new.ie_unico_nasc_vivo <> 'S') and (:new.ie_tipo_nascimento in (3,4,10)))	then /*codigos dos tipos de nascimento referente a vivo */
	ds_consistencia_w := ds_consistencia_w || obter_texto_dic_objeto(289091, wheb_usuario_pck.get_nr_seq_idioma, null) || chr(13) || chr(10);
end if;


if	(not deleting) then
    if	(nr_digitos_w > 0) and (length(to_char(trim(:new.nr_dnv))) <> nr_digitos_w) then
	    ds_consistencia_w := ds_consistencia_w ||obter_texto_dic_objeto(289092, wheb_usuario_pck.get_nr_seq_idioma, 'NR_DIGITOS='||nr_digitos_w);
    end if;
end if;

if	(ds_consistencia_w is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(246815,'DS_CONSISTENCIA='||ds_consistencia_w);
end if;

if	(:new.dt_nascimento > sysdate) then
	Wheb_mensagem_pck.exibir_mensagem_abort(246814);
end if;

/* Lan�ar uma solicita��o de lead quando ocorrer o nascimento de um filho de uma benefici�ria do plano de sa�de. */
if	(nvl(:new.ie_unico_nasc_vivo,'N') = 'S') then
	select	count(*)
	into	qt_solic_lead_w
	from	pls_solicitacao_comercial
	where	nr_atendimento	= :new.nr_atendimento;

	select	max(a.dt_rescisao),
		max(a.dt_liberacao)
	into	dt_rescisao_w,
		dt_liberacao_w
	from	pls_segurado a,
		atendimento_paciente b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_atendimento	= :new.nr_atendimento;

	select	count(*)
	into	qt_benef_plano_w
	from	pls_segurado a,
		atendimento_paciente b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_atendimento	= :new.nr_atendimento;

	if	((qt_benef_plano_w > 0) and (qt_solic_lead_w = 0) and (:old.nr_atendimento is null)) then
		if	((dt_rescisao_w is null) and (dt_liberacao_w is not null) and (ie_gerar_lead_nascimento_w = 'S')) then
			begin
			pls_gerar_solicitacao_lead(null, :new.nr_atendimento, 'T',null, :new.nm_usuario, cd_estabelecimento_w, null);
			exception
			when others then
				ds_consistencia_w	:= ds_consistencia_w || obter_texto_dic_objeto(289093, wheb_usuario_pck.get_nr_seq_idioma, null);
			end;
		end if;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.NASCIMENTO_ATUAL
before insert or update ON TASY.NASCIMENTO for each row
declare
qt_reg_w		number(10);
cd_estabelecimento_w	number(10);
nr_seq_evento_w		number(10);
nr_counter_w     number;
nr_birth_seq_w      number;
Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	ie_evento_disp = 'ARNV'
	and	nvl(ie_situacao,'A') = 'A';

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
if 	((:old.DT_INATIVACAO is null) and (:new.DT_INATIVACAO is not null) and (:old.nr_dnv is not null)) then

	update	REGRA_NUMERACAO_DEC_ITEM a
	set		IE_DISPONIVEL = 'S'
	where	nr_declaracao	= :old.nr_dnv
	and	exists (
	select	1
	from	regra_numeracao_declaracao b
	where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
	and	b.ie_situacao	= 'A'
	and	b.ie_tipo_numeracao	= 'DNV');

end if;

if	(nvl(:old.DT_ATUALIZACAO,sysdate+10) <> :new.DT_ATUALIZACAO) and
	(:new.DT_ATUALIZACAO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ATUALIZACAO, 'HV');
end if;

if	(nvl(:new.NR_DNV,'0')	<> nvl(:old.NR_DNV,'0')) and
	(length(somente_numero(:new.NR_DNV))	= length(:new.NR_DNV)) then


	begin
	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'S'
	where	nr_declaracao	= :old.nr_dnv
	and		ie_disponivel <> 'C'
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DNV');
	exception
		when others then
		null;
	end;

	select	count(*)
	into	qt_reg_w
	from	regra_numeracao_declaracao a,
		REGRA_NUMERACAO_DEC_ITEM b
	where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM
	and	nr_declaracao	= :new.nr_dnv
	and	IE_DISPONIVEL	<> 'S'
	and	a.ie_situacao	= 'A'
	and	a.ie_tipo_numeracao	= 'DNV';

	if	(qt_reg_w	> 0)  then
		Wheb_mensagem_pck.exibir_mensagem_abort(264504);
	end if;

	select	count(*)
	into	qt_reg_w
	from	regra_numeracao_declaracao a,
		REGRA_NUMERACAO_DEC_ITEM b
	where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM
	and	a.ie_situacao	= 'A'
	and	a.ie_tipo_numeracao	= 'DNV';

	if	(qt_reg_w	> 0) then

		select	count(*)
		into	qt_reg_w
		from	regra_numeracao_declaracao a,
			REGRA_NUMERACAO_DEC_ITEM b
		where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM
		and	a.ie_situacao	= 'A'
		and	a.ie_tipo_numeracao	= 'DNV'
		and	nr_declaracao	= :new.nr_dnv;

		if	(qt_reg_w	= 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(264505);
		end if;

	end if;

	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'N'
	where	nr_declaracao	= :new.nr_dnv
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DNV');


	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

	if (:new.IE_UNICO_NASC_VIVO = 'S') then
		open C01;
		loop
		fetch C01 into
			nr_seq_evento_w;
		exit when C01%notfound;
			gerar_evento_paciente(nr_seq_evento_w,:new.NR_ATENDIMENTO, OBTER_PESSOA_ATENDIMENTO(:new.NR_ATENDIMENTO, 'C'),null,wheb_usuario_pck.get_nm_usuario,
			   null, null, null, null, null, null, null, null, null, null, 'N');
		end loop;
		close C01;
	end if;
end if;
if ( inserting ) then
    select
        count(*)
    into nr_counter_w
    from
        nascimento
    where
        extract(year from dt_nascimento) = extract(year from sysdate);

    if ( nr_counter_w = 0 ) then
        execute immediate 'alter sequence birth_no_seq restart start with 1';
    end if;
	select birth_no_seq.nextval into nr_birth_seq_w from dual;
    :new.nr_birth_number := nr_birth_seq_w;
end if;
end if;
end;
/


ALTER TABLE TASY.NASCIMENTO ADD (
  CONSTRAINT NASCIME_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT NASCIME_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NASCIMENTO ADD (
  CONSTRAINT NASCIME_CATATEP_FK 
 FOREIGN KEY (NR_SEQ_PROF_PARTO) 
 REFERENCES TASY.CAT_PROFISSIONAL_PARTO (NR_SEQUENCIA),
  CONSTRAINT NASCIME_CATCERP_FK 
 FOREIGN KEY (NR_SEQ_CERT_PARTO) 
 REFERENCES TASY.CAT_CERT_PARTO (NR_SEQUENCIA),
  CONSTRAINT NASCIME_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT NASCIME_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT NASCIME_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NASCIME_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT NASCIME_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT NASCIME_CATLCNA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_CERT_NASC) 
 REFERENCES TASY.CAT_LUGAR_CERT_NASC (NR_SEQUENCIA),
  CONSTRAINT NASCIME_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_RN) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NASCIME_PESFISI_FK4 
 FOREIGN KEY (CD_PAI_RN) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NASCIME_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NASCIME_MEDICO_FK 
 FOREIGN KEY (CD_PEDIATRA) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT NASCIME_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT NASCIME_CORPELE_FK 
 FOREIGN KEY (NR_COR_PELE) 
 REFERENCES TASY.COR_PELE (NR_SEQUENCIA),
  CONSTRAINT NASCIME_ATEPACI_FK2 
 FOREIGN KEY (NR_ATEND_RN) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT NASCIME_PESFISI_FK 
 FOREIGN KEY (CD_PEDIATRA_RN) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.NASCIMENTO TO NIVEL_1;

