ALTER TABLE TASY.MOTIVO_PERDA_UNITARIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_PERDA_UNITARIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_PERDA_UNITARIZACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MOTIVO            VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_MOTIVO       VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MOTPRUN_PK ON TASY.MOTIVO_PERDA_UNITARIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MOTIVO_PERDA_UNITARIZACAO_tp  after update ON TASY.MOTIVO_PERDA_UNITARIZACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_MOTIVO,1,4000),substr(:new.DS_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO',ie_log_w,ds_w,'MOTIVO_PERDA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MOTIVO,1,4000),substr(:new.IE_TIPO_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MOTIVO',ie_log_w,ds_w,'MOTIVO_PERDA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MOTIVO_PERDA_UNITARIZACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MOTIVO_PERDA_UNITARIZACAO ADD (
  CONSTRAINT MOTPRUN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MOTIVO_PERDA_UNITARIZACAO TO NIVEL_1;

