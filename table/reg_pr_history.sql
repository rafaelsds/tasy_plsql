ALTER TABLE TASY.REG_PR_HISTORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_PR_HISTORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_PR_HISTORY
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_CODE                  VARCHAR2(255 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_TITLE                 VARCHAR2(255 BYTE)   NOT NULL,
  DS_DESCRIPTION           CLOB,
  DT_APROVACAO             DATE,
  NR_SEQ_APRESENTACAO      NUMBER(10),
  NR_CUSTOMER_REQUIREMENT  NUMBER(10),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_VERSAO                VARCHAR2(255 BYTE),
  NR_SEQ_PR                NUMBER(10)           NOT NULL,
  NM_USUARIO_APROVACAO     VARCHAR2(15 BYTE),
  DT_LIBERACAO_DEV         DATE,
  DT_LIBERACAO_VV          DATE,
  NR_SEQ_IDIOMA            NUMBER(10),
  NM_ANALISTA_LIBEROU      VARCHAR2(15 BYTE),
  NM_LIBEROU_VV            VARCHAR2(15 BYTE),
  DT_LIBEROU_GER_VV        DATE,
  NM_LIBEROU_GER_VV        VARCHAR2(15 BYTE),
  DS_DESCRIPTION_PR        VARCHAR2(4000 BYTE),
  NR_SEQ_PRODUCT_SUP       NUMBER(10),
  NR_REVISION              NUMBER(10),
  IE_REVISION_TYPE         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_DESCRIPTION) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGPRHIS_PK ON TASY.REG_PR_HISTORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_PR_HISTORY ADD (
  CONSTRAINT REGPRHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REG_PR_HISTORY TO NIVEL_1;

