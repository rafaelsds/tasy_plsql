ALTER TABLE TASY.ESCALA_DAST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DAST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DAST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PONTUACAO           NUMBER(5,2),
  IE_NAO_INDICADO        VARCHAR2(2 BYTE)       NOT NULL,
  IE_DOSE_MAIOR          VARCHAR2(2 BYTE)       NOT NULL,
  IE_ABUSA               VARCHAR2(2 BYTE)       NOT NULL,
  IE_SEMANA              VARCHAR2(2 BYTE)       NOT NULL,
  IE_CONTROLE            VARCHAR2(2 BYTE)       NOT NULL,
  IE_BLACKOUTS           VARCHAR2(2 BYTE)       NOT NULL,
  IE_CULPADO             VARCHAR2(2 BYTE)       NOT NULL,
  IE_ENVOLVIMENTO        VARCHAR2(2 BYTE)       NOT NULL,
  IE_CONJUGE             VARCHAR2(2 BYTE)       NOT NULL,
  IE_AMIGOS              VARCHAR2(2 BYTE)       NOT NULL,
  IE_NEGLIGENCIA         VARCHAR2(2 BYTE)       NOT NULL,
  IE_TRABALHO            VARCHAR2(2 BYTE)       NOT NULL,
  IE_EMPREGO             VARCHAR2(2 BYTE)       NOT NULL,
  IE_BRIGAS              VARCHAR2(2 BYTE)       NOT NULL,
  IE_ATIVIDADE_ILEGAL    VARCHAR2(2 BYTE)       NOT NULL,
  IE_PRESO               VARCHAR2(2 BYTE)       NOT NULL,
  IE_ABSTINENCIA         VARCHAR2(2 BYTE)       NOT NULL,
  IE_PROBLEMA_MEDICO     VARCHAR2(2 BYTE)       NOT NULL,
  IE_AJUDA               VARCHAR2(2 BYTE)       NOT NULL,
  IE_TRATAMENTO          VARCHAR2(2 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESC_DAST_ATEPACI_FK_I ON TASY.ESCALA_DAST
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESC_DAST_PESFISI_FK_I ON TASY.ESCALA_DAST
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESC_DAST_PK ON TASY.ESCALA_DAST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESC_DAST_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_DAST_ATUAL
before update or insert ON TASY.ESCALA_DAST for each row
declare

qt_pontuacao_w		Number(5,2) := 0;

begin

if	(:new.IE_NAO_INDICADO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_DOSE_MAIOR = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_ABUSA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_SEMANA = 'N') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_CONTROLE = 'N') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_BLACKOUTS = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_CULPADO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_ENVOLVIMENTO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_CONJUGE = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_AMIGOS = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_NEGLIGENCIA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_TRABALHO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_EMPREGO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_BRIGAS = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_ATIVIDADE_ILEGAL = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_PRESO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_ABSTINENCIA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_PROBLEMA_MEDICO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_AJUDA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;
if	(:new.IE_TRATAMENTO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

:new.qt_pontuacao := qt_pontuacao_w;

end;
/


ALTER TABLE TASY.ESCALA_DAST ADD (
  CONSTRAINT ESC_DAST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_DAST ADD (
  CONSTRAINT ESC_DAST_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESC_DAST_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_DAST TO NIVEL_1;

