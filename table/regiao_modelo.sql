ALTER TABLE TASY.REGIAO_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGIAO_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGIAO_MODELO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NU_COORDENADA_ESQUERDA  NUMBER(10)            NOT NULL,
  NU_COORDENADA_TOPO      NUMBER(10)            NOT NULL,
  NU_COORDENADA_DIREITA   NUMBER(10)            NOT NULL,
  NU_COORDENADA_BASE      NUMBER(10)            NOT NULL,
  PK_FK_ID_MODELO         NUMBER(10)            NOT NULL,
  NM_REGIAO               VARCHAR2(255 BYTE),
  TP_ROTACAO              VARCHAR2(255 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RM_MS_FK_I ON TASY.REGIAO_MODELO
(PK_FK_ID_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RM_MS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RM_PK ON TASY.REGIAO_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RM_PK
  MONITORING USAGE;


ALTER TABLE TASY.REGIAO_MODELO ADD (
  CONSTRAINT RM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGIAO_MODELO ADD (
  CONSTRAINT RM_MS_FK 
 FOREIGN KEY (PK_FK_ID_MODELO) 
 REFERENCES TASY.MODELO_SCANNER (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGIAO_MODELO TO NIVEL_1;

