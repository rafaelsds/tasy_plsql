ALTER TABLE TASY.PLS_CP_CTA_EMAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_EMAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_EMAT
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_INICIO_VIGENCIA           DATE             NOT NULL,
  TX_AJUSTE_PFB                NUMBER(15,4),
  TX_AJUSTE_PMC_NEUT           NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_PMC_POS            NUMBER(15,4)     NOT NULL,
  NR_SEQ_CP_COMBINADA          NUMBER(10)       NOT NULL,
  TX_AJUSTE_PMC_NEG            NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_SIMPRO_PFB         NUMBER(15,4),
  TX_AJUSTE_SIMPRO_PMC         NUMBER(15,4)     NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_ORIGEM_PRECO              VARCHAR2(5 BYTE),
  TX_AJUSTE_TAB_PROPRIA        NUMBER(15,4)     NOT NULL,
  DT_FIM_VIGENCIA              DATE,
  TX_AJUSTE                    NUMBER(15,4),
  VL_NEGOCIADO                 NUMBER(15,4),
  NR_SEQ_MATERIAL_PRECO        NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(4000 BYTE),
  IE_TABELA_ADICIONAL          NUMBER(10),
  DT_BASE_FIXO                 DATE,
  IE_TIPO_PRECO_SIMPRO         VARCHAR2(1 BYTE),
  IE_TIPO_PRECO_BRASINDICE     VARCHAR2(3 BYTE),
  IE_TIPO_PRECO_TAB_ADIC       VARCHAR2(3 BYTE),
  IE_TIPO_AJUSTE_PFB           VARCHAR2(1 BYTE),
  VL_PFB_NEUTRA_SIMPRO         NUMBER(15,4),
  VL_PFB_POSITIVA_SIMPRO       NUMBER(15,4),
  VL_PFB_NEGATIVA_SIMPRO       NUMBER(15,4),
  VL_PFB_NAO_APLICAVEL_SIMPRO  NUMBER(15,4),
  VL_PFB_NEUTRA_BRASINDICE     NUMBER(15,4),
  VL_PFB_POSITIVA_BRASINDICE   NUMBER(15,4),
  VL_PFB_NEGATIVA_BRASINDICE   NUMBER(15,4),
  IE_NAO_GERA_TX_INTER         VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF       DATE,
  DT_FIM_VIGENCIA_REF          DATE,
  IE_FORMA_VALORIZACAO         VARCHAR2(1 BYTE) NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLCPMAT_PK ON TASY.PLS_CP_CTA_EMAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPMAT_PLSCPCC_FK_I ON TASY.PLS_CP_CTA_EMAT
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPMAT_PLSMAPR_FK_I ON TASY.PLS_CP_CTA_EMAT
(NR_SEQ_MATERIAL_PRECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_emat_atual
before insert or update ON TASY.PLS_CP_CTA_EMAT for each row
declare

begin
-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_cp_cta_emat_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_EMAT_tp  after update ON TASY.PLS_CP_CTA_EMAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_POS,1,4000),substr(:new.TX_AJUSTE_PMC_POS,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_POS',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_VALORIZACAO,1,4000),substr(:new.IE_FORMA_VALORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_VALORIZACAO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_TAB_ADIC,1,4000),substr(:new.IE_TIPO_PRECO_TAB_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_TAB_ADIC',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_SIMPRO,1,4000),substr(:new.IE_TIPO_PRECO_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_SIMPRO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_BRASINDICE,1,4000),substr(:new.IE_TIPO_PRECO_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_BRASINDICE',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_AJUSTE_PFB,1,4000),substr(:new.IE_TIPO_AJUSTE_PFB,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_AJUSTE_PFB',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_SIMPRO_PFB,1,4000),substr(:new.TX_AJUSTE_SIMPRO_PFB,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_SIMPRO_PFB',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PFB,1,4000),substr(:new.TX_AJUSTE_PFB,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PFB',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEUTRA_SIMPRO,1,4000),substr(:new.VL_PFB_NEUTRA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEUTRA_SIMPRO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_POSITIVA_SIMPRO,1,4000),substr(:new.VL_PFB_POSITIVA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_POSITIVA_SIMPRO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEGATIVA_SIMPRO,1,4000),substr(:new.VL_PFB_NEGATIVA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEGATIVA_SIMPRO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NAO_APLICAVEL_SIMPRO,1,4000),substr(:new.VL_PFB_NAO_APLICAVEL_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NAO_APLICAVEL_SIMPRO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEUTRA_BRASINDICE,1,4000),substr(:new.VL_PFB_NEUTRA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEUTRA_BRASINDICE',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_POSITIVA_BRASINDICE,1,4000),substr(:new.VL_PFB_POSITIVA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_POSITIVA_BRASINDICE',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEGATIVA_BRASINDICE,1,4000),substr(:new.VL_PFB_NEGATIVA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEGATIVA_BRASINDICE',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_GERA_TX_INTER,1,4000),substr(:new.IE_NAO_GERA_TX_INTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_GERA_TX_INTER',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_TAB_PROPRIA,1,4000),substr(:new.TX_AJUSTE_TAB_PROPRIA,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_TAB_PROPRIA',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_NEGOCIADO,1,4000),substr(:new.VL_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_NEGOCIADO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA_REF,1,4000),substr(:new.DT_FIM_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA_REF,1,4000),substr(:new.DT_INICIO_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_NEUT,1,4000),substr(:new.TX_AJUSTE_PMC_NEUT,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_NEUT',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PRECO,1,4000),substr(:new.IE_ORIGEM_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PRECO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL_PRECO,1,4000),substr(:new.NR_SEQ_MATERIAL_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL_PRECO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TABELA_ADICIONAL,1,4000),substr(:new.IE_TABELA_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TABELA_ADICIONAL',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE,1,4000),substr(:new.TX_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_SIMPRO_PMC,1,4000),substr(:new.TX_AJUSTE_SIMPRO_PMC,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_SIMPRO_PMC',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_NEG,1,4000),substr(:new.TX_AJUSTE_PMC_NEG,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_NEG',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_BASE_FIXO,1,4000),substr(:new.DT_BASE_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'DT_BASE_FIXO',ie_log_w,ds_w,'PLS_CP_CTA_EMAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_EMAT ADD (
  CONSTRAINT PLCPMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_EMAT ADD (
  CONSTRAINT PLCPMAT_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLCPMAT_PLSMAPR_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_PRECO) 
 REFERENCES TASY.PLS_MATERIAL_PRECO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_EMAT TO NIVEL_1;

