ALTER TABLE TASY.HC_PAD_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_PAD_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_PAD_MATERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTROLE      NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  CD_KIT_MATERIAL      NUMBER(5),
  QT_MATERIAL          NUMBER(11,4),
  CD_INTERVALO         VARCHAR2(7 BYTE),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDPADMAT_HCPADCONT_FK_I ON TASY.HC_PAD_MATERIAL
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPADMAT_HCPADCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDPADMAT_INTMATE_FK_I ON TASY.HC_PAD_MATERIAL
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPADMAT_KITMATE_FK_I ON TASY.HC_PAD_MATERIAL
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPADMAT_KITMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDPADMAT_MATERIA_FK_I ON TASY.HC_PAD_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPADMAT_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDPADMAT_PK ON TASY.HC_PAD_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPADMAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.HDPADMAT_UNIMEDI_FK_I ON TASY.HC_PAD_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPADMAT_VIAAPLI_FK_I ON TASY.HC_PAD_MATERIAL
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HC_PAD_MATERIAL_tp  after update ON TASY.HC_PAD_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_KIT_MATERIAL,1,4000),substr(:new.CD_KIT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_KIT_MATERIAL',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTROLE,1,4000),substr(:new.NR_SEQ_CONTROLE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTROLE',ie_log_w,ds_w,'HC_PAD_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.HC_PAD_MATERIAL ADD (
  CONSTRAINT HDPADMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HC_PAD_MATERIAL ADD (
  CONSTRAINT HDPADMAT_INTMATE_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT HDPADMAT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT HDPADMAT_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT HDPADMAT_HCPADCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.HC_PAD_CONTROLE (NR_SEQUENCIA),
  CONSTRAINT HDPADMAT_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT HDPADMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.HC_PAD_MATERIAL TO NIVEL_1;

