ALTER TABLE TASY.ANESTESIA_COMPLICACOES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANESTESIA_COMPLICACOES CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANESTESIA_COMPLICACOES
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_COMPLICACAO             VARCHAR2(3 BYTE)   NOT NULL,
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANSTCOM_CIRURGI_FK_I ON TASY.ANESTESIA_COMPLICACOES
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANSTCOM_PEPOCIR_FK_I ON TASY.ANESTESIA_COMPLICACOES
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANSTCOM_PESFISI_FK_I ON TASY.ANESTESIA_COMPLICACOES
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ANSTCOM_PK ON TASY.ANESTESIA_COMPLICACOES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANSTCOM_TASASDI_FK_I ON TASY.ANESTESIA_COMPLICACOES
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANSTCOM_TASASDI_FK2_I ON TASY.ANESTESIA_COMPLICACOES
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ANESTESIA_COMPLICACOES_DELETE
after delete ON TASY.ANESTESIA_COMPLICACOES for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_registro = 'XCMA'
and		nr_seq_registro  = :old.nr_sequencia
and     nvl(ie_tipo_pendencia, 'L') =  'L';

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ANESTESIA_COMPL_PEND_ATUAL
after insert or update ON TASY.ANESTESIA_COMPLICACOES for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

 select    max(c.nr_atendimento),
    max(c.cd_pessoa_fisica)
into    nr_atendimento_w,
    cd_pessoa_fisica_w
from    cirurgia c
where    c.nr_cirurgia = :new.nr_cirurgia;
if     (cd_pessoa_fisica_w is null) then
    select    max(c.nr_atendimento),
        max(c.cd_pessoa_fisica)
    into    nr_atendimento_w,
        cd_pessoa_fisica_w
    from    pepo_cirurgia c
    where    c.nr_sequencia = :new.nr_seq_pepo;
end if;

if    (:new.dt_liberacao is null) then
    ie_tipo_w := 'CMA';
elsif    (:old.dt_liberacao is null) and
        (:new.dt_liberacao is not null) then
    ie_tipo_w := 'XCMA';
end if;
Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ANESTESIA_COMPLICACOES ADD (
  CONSTRAINT ANSTCOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ANESTESIA_COMPLICACOES ADD (
  CONSTRAINT ANSTCOM_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ANSTCOM_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ANSTCOM_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ANSTCOM_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ANSTCOM_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ANESTESIA_COMPLICACOES TO NIVEL_1;

