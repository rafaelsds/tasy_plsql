ALTER TABLE TASY.MULT_APROV_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MULT_APROV_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.MULT_APROV_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DOC           NUMBER(10)               NOT NULL,
  NR_SEQ_FUNCAO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MULTAPI_MULTAPF_FK_I ON TASY.MULT_APROV_ITEM
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MULTAPI_OBJSCHE_FK_I ON TASY.MULT_APROV_ITEM
(NR_SEQ_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MULTAPI_PK ON TASY.MULT_APROV_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MULTAPI_UK ON TASY.MULT_APROV_ITEM
(NR_SEQ_FUNCAO, NR_SEQ_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MULT_APROV_ITEM ADD (
  CONSTRAINT MULTAPI_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT MULTAPI_UK
 UNIQUE (NR_SEQ_FUNCAO, NR_SEQ_DOC));

ALTER TABLE TASY.MULT_APROV_ITEM ADD (
  CONSTRAINT MULTAPI_MULTAPF_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.MULT_APROV_FUNCAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MULTAPI_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_DOC) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.MULT_APROV_ITEM TO NIVEL_1;

