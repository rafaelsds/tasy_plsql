ALTER TABLE TASY.PLS_GRUPO_CONTRATO_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRUPO_CONTRATO_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRUPO_CONTRATO_WEB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NM_USUARIO_WEB         VARCHAR2(15 BYTE),
  DS_SENHA               VARCHAR2(64 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_GRUPO_CONTRATO  NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_SEQ_PERFIL_WEB      NUMBER(10),
  DT_ALTERACAO_SENHA     DATE,
  QT_TENTATIVA_ACESSO    NUMBER(10),
  DS_TEC                 VARCHAR2(15 BYTE),
  DS_EMAIL               VARCHAR2(60 BYTE),
  IE_ORIGEM_GRUPO_WEB    NUMBER(3),
  DS_GRUPO               VARCHAR2(60 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSGCW_ESTABEL_FK_I ON TASY.PLS_GRUPO_CONTRATO_WEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGCW_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGCW_PESFISI_FK_I ON TASY.PLS_GRUPO_CONTRATO_WEB
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGCW_PK ON TASY.PLS_GRUPO_CONTRATO_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGCW_PLSGRCO_FK_I ON TASY.PLS_GRUPO_CONTRATO_WEB
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGCW_PLSGRCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGCW_PLSPERW_FK_I ON TASY.PLS_GRUPO_CONTRATO_WEB
(NR_SEQ_PERFIL_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGCW_PLSPERW_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSGCW_UK ON TASY.PLS_GRUPO_CONTRATO_WEB
(NM_USUARIO_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGCW_UK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_GRUPO_CONTRATO_WEB ADD (
  CONSTRAINT PLSGCW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSGCW_UK
 UNIQUE (NM_USUARIO_WEB)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GRUPO_CONTRATO_WEB ADD (
  CONSTRAINT PLSGCW_PLSPERW_FK 
 FOREIGN KEY (NR_SEQ_PERFIL_WEB) 
 REFERENCES TASY.PLS_PERFIL_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSGCW_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSGCW_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSGCW_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PLS_GRUPO_CONTRATO_WEB TO NIVEL_1;

