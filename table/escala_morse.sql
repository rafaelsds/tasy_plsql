ALTER TABLE TASY.ESCALA_MORSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MORSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MORSE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  IE_HIST_QUEDA              VARCHAR2(1 BYTE)   NOT NULL,
  IE_DIAG_SECUNDARIO         VARCHAR2(1 BYTE)   NOT NULL,
  IE_AJUDA_DEAMBULAR         NUMBER(3)          NOT NULL,
  IE_USO_HEPARINA            VARCHAR2(1 BYTE)   NOT NULL,
  IE_MODO_ANDAR              NUMBER(3)          NOT NULL,
  IE_ESTADO_MENTAL           NUMBER(3)          NOT NULL,
  QT_PONTUACAO               NUMBER(10)         NOT NULL,
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_PRESCR              NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMORS_ATEPACI_FK_I ON TASY.ESCALA_MORSE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMORS_I1 ON TASY.ESCALA_MORSE
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMORS_PEPRESC_FK_I ON TASY.ESCALA_MORSE
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMORS_PESFISI_FK_I ON TASY.ESCALA_MORSE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMORS_PK ON TASY.ESCALA_MORSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMORS_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCMORS_TASASDI_FK_I ON TASY.ESCALA_MORSE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMORS_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMORS_TASASDI_FK2_I ON TASY.ESCALA_MORSE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMORS_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATEND_ESCALA_MORSE_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_MORSE FOR EACH ROW
declare

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

:new.qt_pontuacao	:= 0;
If  (:new.IE_HIST_QUEDA = 'S') then
  :new.qt_pontuacao := :new.qt_pontuacao + 25;
end if;



If  (:new.IE_DIAG_SECUNDARIO = 'S') then
  :new.qt_pontuacao := :new.qt_pontuacao + 15;
end if;



If  	(:new.IE_USO_HEPARINA = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 20;
end if;


:new.qt_pontuacao	:= :new.qt_pontuacao + :new.IE_AJUDA_DEAMBULAR;

:new.qt_pontuacao	:= :new.qt_pontuacao + :new.IE_MODO_ANDAR;

:new.qt_pontuacao	:= :new.qt_pontuacao + :new.IE_ESTADO_MENTAL;


if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_atendimento is not null) then

	begin
		gravar_agend_rothman(:new.nr_atendimento,:new.nr_sequencia,'MOR',:new.nm_usuario);
	exception
	when others then
		null;
	end;

end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.escala_morse_pend_atual
after insert or update ON TASY.ESCALA_MORSE for each row
declare

pragma autonomous_transaction;

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'89');
end if;

if 	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	gerar_evolucao_Morse(:new.nm_usuario,:new.nr_sequencia);
end if;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MORSE_delete
after delete ON TASY.ESCALA_MORSE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '89'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_MORSE ADD (
  CONSTRAINT ESCMORS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MORSE ADD (
  CONSTRAINT ESCMORS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMORS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMORS_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT ESCMORS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMORS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_MORSE TO NIVEL_1;

