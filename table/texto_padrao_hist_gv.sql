ALTER TABLE TASY.TEXTO_PADRAO_HIST_GV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEXTO_PADRAO_HIST_GV CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEXTO_PADRAO_HIST_GV
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_TEXTO_PADRAO          VARCHAR2(4000 BYTE)  NOT NULL,
  IE_TIPO_TEXTO            VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_MOTIVO_HISTORICO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TXPHIGV_GEVAMOH_FK_I ON TASY.TEXTO_PADRAO_HIST_GV
(NR_SEQ_MOTIVO_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPHIGV_GEVAMOH_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXPHIGV_PK ON TASY.TEXTO_PADRAO_HIST_GV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TEXTO_PADRAO_HIST_GV ADD (
  CONSTRAINT TXPHIGV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TEXTO_PADRAO_HIST_GV ADD (
  CONSTRAINT TXPHIGV_GEVAMOH_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_HISTORICO) 
 REFERENCES TASY.GESTAO_VAGA_MOTIVO_HIST (NR_SEQUENCIA));

GRANT SELECT ON TASY.TEXTO_PADRAO_HIST_GV TO NIVEL_1;

