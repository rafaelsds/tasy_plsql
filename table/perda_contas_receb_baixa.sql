ALTER TABLE TASY.PERDA_CONTAS_RECEB_BAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERDA_CONTAS_RECEB_BAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERDA_CONTAS_RECEB_BAIXA
(
  NR_SEQUENCIA            NUMBER(5)             NOT NULL,
  NR_SEQ_PERDA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_BAIXA                DATE                  NOT NULL,
  VL_BAIXA                NUMBER(15,2)          NOT NULL,
  NR_SEQ_TIPO_BAIXA       NUMBER(10)            NOT NULL,
  IE_ACAO                 VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_BAIXA_ORIG       NUMBER(5),
  NR_SEQ_MOVTO_TRANS_FIN  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRDCREB_FNTPBPE_FK_I ON TASY.PERDA_CONTAS_RECEB_BAIXA
(NR_SEQ_TIPO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRDCREB_FNTPBPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRDCREB_MOVTRFI_FK_I ON TASY.PERDA_CONTAS_RECEB_BAIXA
(NR_SEQ_MOVTO_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRDCREB_MOVTRFI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRDCREB_PK ON TASY.PERDA_CONTAS_RECEB_BAIXA
(NR_SEQUENCIA, NR_SEQ_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRDCREB_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRDCREB_PRDACRE_FK_I ON TASY.PERDA_CONTAS_RECEB_BAIXA
(NR_SEQ_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.perda_contas_receb_baixa_ins
before insert ON TASY.PERDA_CONTAS_RECEB_BAIXA for each row
declare

ie_tipo_consistencia_w	varchar2(15);
vl_saldo_w	number(15,2);

begin
select	vl_Saldo
into	vl_saldo_w
from	perda_contas_receber
where	nr_sequencia = :new.nr_Seq_perda;

select	max(ie_tipo_consistencia)
into	ie_tipo_consistencia_w
from	fin_tipo_baixa_perda
where	nr_sequencia = :new.nr_Seq_tipo_baixa;

if	(ie_tipo_consistencia_w in (0,1,2)) and
	(:new.ie_acao = '1') and
	(vl_saldo_w < :new.vl_baixa) then
	wheb_mensagem_pck.exibir_mensagem_abort(217442);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.perda_contas_receb_bx_afins
after insert ON TASY.PERDA_CONTAS_RECEB_BAIXA for each row
declare

ie_tipo_consistencia_w		varchar2(15);
nr_seq_cobranca_w		number(10);
cd_estabelecimento_w		number(4);
nr_seq_hist_cob_liq_w		number(10);
nr_seq_hist_cob_pend_w		number(10);
nr_seq_hist_cob_perda_baixa_w	number(10);
nr_seq_hist_cob_perda_est_bx_w	number(10);
nr_seq_historico_w			number(10);

nr_titulo_w			number(10);
nr_seq_cheque_w			number(10);
vl_acobrar_w			number(17,4);

vl_cheque_w				cheque_cr.vl_cheque%type;
vl_saldo_negociado_w	cheque_cr.vl_saldo_negociado%type;

cursor c01 is
select	nr_sequencia,
	vl_acobrar
from	cobranca
where	nr_seq_cheque = nr_seq_cheque_w
union
select	nr_sequencia,
	vl_acobrar
from	cobranca
where	nr_titulo = nr_titulo_w;

begin
select	max(ie_tipo_consistencia)
into	ie_tipo_consistencia_w
from	fin_tipo_baixa_perda
where	nr_sequencia = :new.nr_seq_tipo_baixa;

if	(ie_tipo_consistencia_w in (0,1,2)) then
	begin
	select	nr_titulo,
		nr_seq_cheque,
		cd_estabelecimento
	into	nr_titulo_w,
		nr_seq_cheque_w,
		cd_estabelecimento_w
	from	perda_contas_receber
	where	nr_sequencia = :new.nr_seq_perda;

	select	max(nr_seq_hist_cob),
		max(nr_seq_hist_cob_pend),
		max(nr_seq_hist_cob_perda_baixa),
		max(nr_seq_hist_cob_perda_est_bx)
	into	nr_seq_hist_cob_liq_w,
		nr_seq_hist_cob_pend_w,
		nr_seq_hist_cob_perda_baixa_w,
		nr_seq_hist_cob_perda_est_bx_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nr_seq_hist_cob_perda_baixa_w is null) or
		(nr_seq_hist_cob_perda_est_bx_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(231544);
	end if;

	open c01;
	loop
	fetch c01 into
		nr_seq_cobranca_w,
		vl_acobrar_w;
	exit when c01%notfound;
		begin
		vl_acobrar_w		:= vl_acobrar_w - :new.vl_baixa;

		update	cobranca
		set	vl_acobrar	= vl_acobrar_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_cobranca_w;

		if	(:new.vl_baixa > 0) then
			nr_seq_historico_w	:= nr_seq_hist_cob_perda_baixa_w;
		else
			nr_seq_historico_w	:= nr_seq_hist_cob_perda_est_bx_w;
		end if;

		insert into cobranca_historico
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_cobranca,
			nr_seq_historico,
			dt_historico,
			ds_historico)
		values(	cobranca_historico_seq.nextval,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			sysdate,
			nr_seq_cobranca_w,
			nr_seq_historico_w,
			sysdate,
			substr(wheb_mensagem_pck.get_texto(304189) || campo_mascara_virgula(:new.vl_baixa) || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(304196) || to_char(:new.dt_baixa,'dd/mm/yyyy hh24:mi:ss'),1,4000));
		end;
	end loop;
	close c01;
	end;
end if;

/*OS 1799459 Se o cheque tiver baixa por perda, verificar se a perda foi cancelada, para devolver o saldo negociado ao cheque e poder ser negociado novamente*/
if	(ie_tipo_consistencia_w = 4) then

	select	max(nr_seq_cheque)
	into	nr_seq_cheque_w
	from	perda_contas_receber
	where	nr_sequencia = :new.nr_seq_perda;

	if (nr_seq_cheque_w is not null) then

		--Nao chamei a atualizar_saldo_neg_cheque_cr aqui onde faz esse c�lculo pois ia dar mutante
		select	max(vl_cheque),
				max(vl_saldo_negociado)
		into	vl_cheque_w,
				vl_saldo_negociado_w
		from	cheque_cr
		where	nr_seq_cheque = nr_seq_cheque_w;

		if  ((nvl(vl_saldo_negociado_w,0) + nvl(:new.vl_baixa,0)) <= vl_cheque_w) then --O saldo negociado atual do cheque somado com o valor dessa baixa deve ser menor ou no m�ximo igual ao valor do cheque
				update	cheque_cr
				set		vl_saldo_negociado	= vl_saldo_negociado + nvl(:new.vl_baixa,0)
				where	nr_seq_cheque		= nr_seq_cheque_w;
		end if;

	end if;
end if;
/*OS 1799459 - FIM*/

end;
/


ALTER TABLE TASY.PERDA_CONTAS_RECEB_BAIXA ADD (
  CONSTRAINT PRDCREB_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_PERDA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERDA_CONTAS_RECEB_BAIXA ADD (
  CONSTRAINT PRDCREB_FNTPBPE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_BAIXA) 
 REFERENCES TASY.FIN_TIPO_BAIXA_PERDA (NR_SEQUENCIA),
  CONSTRAINT PRDCREB_PRDACRE_FK 
 FOREIGN KEY (NR_SEQ_PERDA) 
 REFERENCES TASY.PERDA_CONTAS_RECEBER (NR_SEQUENCIA),
  CONSTRAINT PRDCREB_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_TRANS_FIN) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PERDA_CONTAS_RECEB_BAIXA TO NIVEL_1;

