ALTER TABLE TASY.PLS_COMB_CTA_ITEM_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMB_CTA_ITEM_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMB_CTA_ITEM_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA     NUMBER(10),
  NR_SEQ_COMBINACAO    NUMBER(10)               NOT NULL,
  IE_AND_OR            VARCHAR2(3 BYTE)         NOT NULL,
  NR_SEQ_GRUPO_REC     NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  IE_TIPO_DESP_PROC    VARCHAR2(1 BYTE),
  IE_GERA_OCORRENCIA   VARCHAR2(1 BYTE)         NOT NULL,
  QT_PROC_BASE_SIMUL   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCCIP_GRURECE_FK_I ON TASY.PLS_COMB_CTA_ITEM_PROC
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCIP_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCIP_I1 ON TASY.PLS_COMB_CTA_ITEM_PROC
(NR_SEQ_COMBINACAO, IE_TIPO_DESP_PROC, IE_GERA_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCCIP_PK ON TASY.PLS_COMB_CTA_ITEM_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCIP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCIP_PLSCICT_FK_I ON TASY.PLS_COMB_CTA_ITEM_PROC
(NR_SEQ_COMBINACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCIP_PLSCICT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCIP_PLSOCES_FK_I ON TASY.PLS_COMB_CTA_ITEM_PROC
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCIP_PLSOCES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCIP_PROCEDI_FK_I ON TASY.PLS_COMB_CTA_ITEM_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCIP_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_comb_cta_item_proc_atual
before insert or update or delete ON TASY.PLS_COMB_CTA_ITEM_PROC for each row
declare
nr_seq_combinacao_w	pls_combinacao_item_cta.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_combinacao_w := :old.nr_seq_combinacao;
else
	nr_seq_combinacao_w := :new.nr_seq_combinacao;
end if;

pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_COMBINACAO_ITEM_TM', wheb_usuario_pck.get_nm_usuario,
							'PLS_COMB_CTA_ITEM_PROC_ATUAL',
							'nr_seq_combinacao_p=' || nr_seq_combinacao_w);

end pls_comb_cta_item_proc_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_COMB_CTA_ITEM_PROC_tp  after update ON TASY.PLS_COMB_CTA_ITEM_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_PROC_BASE_SIMUL,1,4000),substr(:new.QT_PROC_BASE_SIMUL,1,4000),:new.nm_usuario,nr_seq_w,'QT_PROC_BASE_SIMUL',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESP_PROC,1,4000),substr(:new.IE_TIPO_DESP_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESP_PROC',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_OCORRENCIA,1,4000),substr(:new.IE_GERA_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_OCORRENCIA',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AND_OR,1,4000),substr(:new.IE_AND_OR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AND_OR',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_COMB_CTA_ITEM_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COMB_CTA_ITEM_PROC ADD (
  CONSTRAINT PLSCCIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMB_CTA_ITEM_PROC ADD (
  CONSTRAINT PLSCCIP_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSCCIP_PLSCICT_FK 
 FOREIGN KEY (NR_SEQ_COMBINACAO) 
 REFERENCES TASY.PLS_COMBINACAO_ITEM_CTA (NR_SEQUENCIA),
  CONSTRAINT PLSCCIP_PLSOCES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_OCORRENCIA_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSCCIP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_COMB_CTA_ITEM_PROC TO NIVEL_1;

