ALTER TABLE TASY.PLS_OC_REGRA_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_REGRA_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_REGRA_PARTICIPANTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DS_REGRA             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCRP_ESTABEL_FK_I ON TASY.PLS_OC_REGRA_PARTICIPANTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSOCRP_PK ON TASY.PLS_OC_REGRA_PARTICIPANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_oc_regra_partic_atual
before insert or update or delete ON TASY.PLS_OC_REGRA_PARTICIPANTE for each row
declare
nr_seq_regra_w	pls_oc_regra_participante.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_regra_w := :old.nr_sequencia;

-- se for atualiza��o
elsif	(updating) then
	-- s� atualiza os registros da tabela TM se mudar a situa��o
	-- n�o faz sentido atualizar se mudou apenas a descri��o da regra
	if	(nvl(:new.ie_situacao, 'A') != nvl(:old.ie_situacao, 'A')) then
		nr_seq_regra_w := :new.nr_sequencia;
	else
		nr_seq_regra_w := null;
	end if;
else
	-- inser��o
	nr_seq_regra_w := :new.nr_sequencia;
end if;

-- se tiver a regra sinaliza que � necess�rio atualizar a tabela
if	(nr_seq_regra_w is not null) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_PARTIC_TM', wheb_usuario_pck.get_nm_usuario,
								'PLS_OC_REGRA_PARTIC_ATUAL',
								'nr_seq_regra_p=' || nr_seq_regra_w);
end if;

end pls_oc_regra_partic_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_OC_REGRA_PARTICIPANTE_tp  after update ON TASY.PLS_OC_REGRA_PARTICIPANTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_REGRA,1,500);gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_OC_REGRA_PARTICIPANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_OC_REGRA_PARTICIPANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_REGRA_PARTICIPANTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_REGRA_PARTICIPANTE ADD (
  CONSTRAINT PLSOCRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_REGRA_PARTICIPANTE ADD (
  CONSTRAINT PLSOCRP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_OC_REGRA_PARTICIPANTE TO NIVEL_1;

