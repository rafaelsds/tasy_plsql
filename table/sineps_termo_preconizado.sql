ALTER TABLE TASY.SINEPS_TERMO_PRECONIZADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SINEPS_TERMO_PRECONIZADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SINEPS_TERMO_PRECONIZADO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_TERMO_PRECONIZADO  NUMBER(10)              NOT NULL,
  CD_SO                 NUMBER(12),
  DS_TERMO_PRECONIZADO  VARCHAR2(40 BYTE)       NOT NULL,
  DS_CIENT_PRECONIZADO  VARCHAR2(40 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SINTEPR_PK ON TASY.SINEPS_TERMO_PRECONIZADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SINEPS_TERMO_PRECONIZADO ADD (
  CONSTRAINT SINTEPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SINEPS_TERMO_PRECONIZADO TO NIVEL_1;

