ALTER TABLE TASY.PLS_TIPO_GUIA_MED_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_GUIA_MED_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_GUIA_MED_ATEND
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_GUIA     NUMBER(10)               NOT NULL,
  NR_SEQ_PLANO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTGMA_PK ON TASY.PLS_TIPO_GUIA_MED_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGMA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGMA_PLSPLAN_FK_I ON TASY.PLS_TIPO_GUIA_MED_ATEND
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTGMA_PLSTGME_FK_I ON TASY.PLS_TIPO_GUIA_MED_ATEND
(NR_SEQ_TIPO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGMA_PLSTGME_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_TIPO_GUIA_MED_ATEND ADD (
  CONSTRAINT PLSTGMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_GUIA_MED_ATEND ADD (
  CONSTRAINT PLSTGMA_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSTGMA_PLSTGME_FK 
 FOREIGN KEY (NR_SEQ_TIPO_GUIA) 
 REFERENCES TASY.PLS_TIPO_GUIA_MEDICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_GUIA_MED_ATEND TO NIVEL_1;

