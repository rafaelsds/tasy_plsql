ALTER TABLE TASY.W_PLS_LOTE_FAT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_LOTE_FAT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_LOTE_FAT_ITEM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  NR_SEQ_CONTA_PROC        NUMBER(10),
  NR_SEQ_CONTA_MAT         NUMBER(10),
  NR_SEQ_EVENTO            NUMBER(10),
  IE_EVENTO_LOTE           VARCHAR2(1 BYTE),
  NR_SEQ_SEGURADO          NUMBER(10),
  NR_SEQ_CONTA             NUMBER(10),
  NR_SEQ_CONTA_POS         NUMBER(10),
  VL_ITEM                  NUMBER(15,2),
  IE_TIPO_COBRANCA         VARCHAR2(3 BYTE),
  IE_IMPEDIMENTO_COBRANCA  VARCHAR2(3 BYTE),
  VL_ITEM_NDC              NUMBER(15,2),
  IE_ORIGEM_ITEM           VARCHAR2(1 BYTE),
  NR_SEQ_CONTA_POS_CONTAB  NUMBER(10),
  NR_SEQ_POS_ESTAB_TAXA    NUMBER(10),
  NR_SEQ_POS_TAXA_CONTAB   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPLSLFI_I1 ON TASY.W_PLS_LOTE_FAT_ITEM
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSLFI_I2 ON TASY.W_PLS_LOTE_FAT_ITEM
(NR_SEQ_LOTE, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSLFI_I3 ON TASY.W_PLS_LOTE_FAT_ITEM
(NR_SEQ_POS_ESTAB_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WPLSLFI_PK ON TASY.W_PLS_LOTE_FAT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.w_pls_lote_fat_item_insert 
before insert or update ON TASY.W_PLS_LOTE_FAT_ITEM 
for each row 
 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
Finalidade: 
------------------------------------------------------------------------------------------------------------------- 
Locais de chamada direta: 
[ ] Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [ ] Relat�rios [ ] Outros: 
 ------------------------------------------------------------------------------------------------------------------ 
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare 
 
qt_registro_w	pls_integer := 0; 
 
begin 
select	count(1) 
into	qt_registro_w 
from	pls_lote_fat_lib 
where	nr_seq_lote_fat = :new.nr_seq_lote; 
 
if	(qt_registro_w = 0) then 
	if	(:new.vl_item < 0) or 
		(:new.vl_item_ndc < 0) then 
		wheb_mensagem_pck.exibir_mensagem_abort( 281537, 'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_seq_conta_pos ); 
	end if; 
 
	-- OS 808183 - wcbernardino - Retirado do momento de gerar a fatura e colocado na "Consistir fatura" 
	/*select	count(1) 
	into	qt_registro_w 
	from	pls_conta_pos_estabelecido 
	where	nr_sequencia		= :new.nr_seq_conta_pos 
	and	ie_status_faturamento	<> 'N' 
	and	nvl(vl_beneficiario,0)	<> (	nvl(vl_custo_operacional,0) + nvl(vl_materiais,0) + nvl(vl_medico,0) + 
						nvl(vl_lib_taxa_servico,0) + nvl(vl_lib_taxa_co,0) + nvl(vl_lib_taxa_material,0)); 
						 
	if	(qt_registro_w > 0) then				 
		wheb_mensagem_pck.exibir_mensagem_abort(287945,'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_seq_conta_pos); 
	end if; 
 
	select	count(1) 
	into	qt_registro_w 
	from	pls_conta_pos_estabelecido 
	where	nr_sequencia		= :new.nr_seq_conta_pos 
	and	ie_status_faturamento	<> 'N' 
	and	(nvl(vl_custo_operacional,0) + nvl(vl_materiais,0) + nvl(vl_medico,0)) <> :new.vl_item_ndc; 
 
	if	(qt_registro_w > 0) and 
		(:new.ie_tipo_cobranca not in ('3')) then				 
		wheb_mensagem_pck.exibir_mensagem_abort(288942,'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_seq_conta_pos); 
	end if;*/ 
end if; 
 
end;
/


ALTER TABLE TASY.W_PLS_LOTE_FAT_ITEM ADD (
  CONSTRAINT WPLSLFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_LOTE_FAT_ITEM TO NIVEL_1;

