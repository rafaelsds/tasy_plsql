DROP TABLE TASY.SALDO_ESTOQUE_LOTE_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.SALDO_ESTOQUE_LOTE_LOG
(
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_LOCAL_ESTOQUE      NUMBER(4)               NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  DT_MESANO_REFERENCIA  DATE                    NOT NULL,
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  QT_ESTOQUE            NUMBER(13,4)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_ACAO               VARCHAR2(1 BYTE)        NOT NULL,
  DS_STACK              VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SELLOG_LOCESTO_FK_I ON TASY.SALDO_ESTOQUE_LOTE_LOG
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SELLOG_MATERIA_FK_I ON TASY.SALDO_ESTOQUE_LOTE_LOG
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SELLOG_MATLOFO_FK_I ON TASY.SALDO_ESTOQUE_LOTE_LOG
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SALDO_ESTOQUE_LOTE_LOG ADD (
  CONSTRAINT SELLOG_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT SELLOG_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SELLOG_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA));

GRANT SELECT ON TASY.SALDO_ESTOQUE_LOTE_LOG TO NIVEL_1;

