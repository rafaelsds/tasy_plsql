ALTER TABLE TASY.PESSOA_FISICA_EXC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_EXC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_EXC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_HASH              VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEFIEXC_I1 ON TASY.PESSOA_FISICA_EXC
(DS_HASH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEFIEXC_PK ON TASY.PESSOA_FISICA_EXC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_EXC_tp  after update ON TASY.PESSOA_FISICA_EXC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PESSOA_FISICA_EXC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PESSOA_FISICA_EXC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HASH,1,4000),substr(:new.DS_HASH,1,4000),:new.nm_usuario,nr_seq_w,'DS_HASH',ie_log_w,ds_w,'PESSOA_FISICA_EXC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_FISICA_EXC ADD (
  CONSTRAINT PEFIEXC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_FISICA_EXC TO NIVEL_1;

