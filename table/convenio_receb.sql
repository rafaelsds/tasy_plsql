ALTER TABLE TASY.CONVENIO_RECEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RECEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RECEB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  DT_RECEBIMENTO          DATE                  NOT NULL,
  VL_RECEBIMENTO          NUMBER(15,2)          NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  IE_STATUS               VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_CONTA_BANCO      NUMBER(10),
  NR_SEQ_TRANS_FIN        NUMBER(10),
  NR_LOTE_CONTABIL        NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  VL_DESPESA_BANCARIA     NUMBER(15,2),
  VL_DEPOSITO             NUMBER(15,2),
  DT_FLUXO_CAIXA          DATE,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_LIBERACAO            DATE,
  IE_INTEGRAR_CB_FLUXO    VARCHAR2(1 BYTE)      NOT NULL,
  IE_TIPO_GLOSA           VARCHAR2(1 BYTE),
  NR_ADIANTAMENTO         NUMBER(10),
  NR_SEQ_COBRANCA         NUMBER(10),
  IE_DEDUZIR_ADIC         VARCHAR2(1 BYTE),
  VL_MOEDA_ORIGINAL       NUMBER(15,2),
  CD_MOEDA                NUMBER(3),
  TX_CAMBIAL              NUMBER(15,4),
  VL_RECEBIMENTO_ESTRANG  NUMBER(15,2),
  VL_COTACAO              NUMBER(15,4),
  VL_COMPLEMENTO          NUMBER(15,2),
  NR_SISTEMA_EXTERNO      VARCHAR2(255 BYTE),
  NR_SISTEMA_EXTERNO_AUX  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONRECE_ADIANTA_FK_I ON TASY.CONVENIO_RECEB
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONRECE_BANESTA_FK_I ON TASY.CONVENIO_RECEB
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONRECE_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONRECE_COBESCR_FK_I ON TASY.CONVENIO_RECEB
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONRECE_COBESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONRECE_CONVENI_FK_I ON TASY.CONVENIO_RECEB
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONRECE_ESTABEL_FK_I ON TASY.CONVENIO_RECEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONRECE_LOTCONT_FK_I ON TASY.CONVENIO_RECEB
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONRECE_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONRECE_MOEDA_FK_I ON TASY.CONVENIO_RECEB
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONRECE_PK ON TASY.CONVENIO_RECEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONRECE_TRAFINA_FK_I ON TASY.CONVENIO_RECEB
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONRECE_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Convenio_Receb_Update
BEFORE UPDATE ON Convenio_Receb
FOR EACH ROW
BEGIN
/* Bruna OS48856 22/01/2007
select	 decode(	obter_baixa_conrece(:new.nr_sequencia), 0, 'N',
			decode(:new.vl_recebimento - obter_baixa_conrece(:new.nr_sequencia), 0, 'T', 'P'))
into	:new.ie_status
from dual;
*/
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_INSERT
after insert or update ON TASY.CONVENIO_RECEB for each row
declare

nr_seq_movto_w		number(10,0);
cd_perfil_w		number(5);
ie_receb_adic_w		varchar2(1);
cd_cgc_convenio_w	varchar2(14);
ie_situacao_w		varchar2(255);
ie_inativos_w		varchar2(255);
qt_movto_w		number(5);
ds_convenio_w		varchar2(255);
nr_seq_trans_financ_w	number(10,0);
vl_movimento_w		number(15,2);

cursor c01 is
select	a.nm_atributo,
	a.cd_tipo_lote_contab
from	atributo_contab a
where	a.cd_tipo_lote_contab = 11
and 	a.nm_atributo in ('VL_RECEBIMENTO',  'VL_DEPOSITO', 'VL_DESPESA_BANCARIA');

c01_w		c01%rowtype;

begin

select	obter_perfil_ativo
into	cd_perfil_w
from	dual;

obter_param_usuario(27, 8, cd_perfil_w, :new.nm_usuario, :new.cd_estabelecimento, ie_receb_adic_w);
obter_param_usuario(27, 191, cd_perfil_w, :new.nm_usuario, :new.cd_estabelecimento, ie_inativos_w);

select	cd_cgc,
	ie_situacao,
	ds_convenio
into	cd_cgc_convenio_w,
	ie_situacao_w,
	ds_convenio_w
from	convenio
where	cd_convenio	= :new.cd_convenio;

if	(ie_situacao_w = 'I') and (inserting) and (ie_inativos_w = 'N') then
	--(-20011, 'Este conv�nio est� inativo! (' || ds_convenio_w || ') ' || chr(13) ||
	--			'N�o � poss�vel lan�ar recebimentos para este conv�nio.');
	wheb_mensagem_pck.exibir_mensagem_abort(224090, 'DS_CONVENIO_W='||ds_convenio_w);
end if;

/* bruna 28-06-2007,

feito tratamento no delphi para somente lan�ar a transa��o automaticamente quando lan�ado o recebimmento pe
fun��o retorno de conv�nios

if	(:new.nr_seq_conta_banco is not null) and
	(:new.nr_seq_trans_fin is not null) and
	(nvl(:new.ie_integrar_cb_fluxo,'S') = 'S') then

	if	(inserting and (ie_receb_adic_w = 'N')) or
		(updating  and (ie_receb_adic_w = 'S') and (:new.dt_liberacao is not null)) then

		select	count(*)
		into	qt_movto_w
		from	movto_trans_financ
		where	nr_seq_conv_receb = :new.nr_sequencia;

		if	(qt_movto_w = 0) then
			select	movto_trans_financ_seq.nextval
			into	nr_seq_movto_w
			from	dual;

			insert	into movto_trans_financ
				(nr_sequencia,
				dt_transacao,
				nr_seq_trans_financ,
				vl_transacao,
				nr_seq_banco,
				nr_seq_conv_receb,
				dt_referencia_saldo,
				dt_atualizacao,
				nm_usuario,
				nr_lote_contabil,
				ie_conciliacao,
				ds_historico,
				cd_cgc)
			values	(nr_seq_movto_w,
				:new.dt_recebimento,
				:new.nr_seq_trans_fin,
				decode(nvl(:new.vl_deposito,0),0, :new.vl_recebimento - nvl(:new.vl_despesa_bancaria,0), :new.vl_deposito),
				:new.nr_seq_conta_banco,
				:new.nr_sequencia,
				trunc(:new.dt_recebimento,'MM'),
				:new.dt_atualizacao,
				:new.nm_usuario,
				0,
				'N',
				:new.ds_observacao,
				cd_cgc_convenio_w);
		end if;
	end if;
end if;
*/

if	(inserting) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'RC',:new.dt_recebimento,'I',:new.nm_usuario);

	open c01;
	loop
	fetch c01 into
		c01_w;
	exit when c01%notfound;
		begin
		if	(c01_w.nm_atributo = 'VL_RECEBIMENTO') then
			vl_movimento_w 	:= :new.vl_recebimento;

		elsif	(c01_w.nm_atributo = 'VL_DEPOSITO') then
			vl_movimento_w 	:= :new.vl_deposito;

		elsif	(c01_w.nm_atributo = 'VL_DESPESA_BANCARIA') then
			vl_movimento_w 	:= :new.vl_despesa_bancaria;
		end if;

		if	(nvl(vl_movimento_w, 0) <> 0) and
			(nvl(:new.nr_seq_trans_fin, 0) <> 0) then
			begin

			ctb_concil_financeira_pck.ctb_gravar_documento	(	:new.cd_estabelecimento,
									trunc(:new.dt_recebimento),
									c01_w.cd_tipo_lote_contab,
									:new.nr_seq_trans_fin,
									8,
									:new.nr_sequencia,
									null,
									null,
									vl_movimento_w,
									'CONVENIO_RECEB',
									c01_w.nm_atributo,
									:new.nm_usuario);

			end;
		end if;
		end;
	end loop;
	close c01;

elsif (updating) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'RC',:new.dt_recebimento,'A',:new.nm_usuario);

	update  ctb_documento
	set     vl_movimento = :new.vl_recebimento,
		nr_seq_trans_financ = :new.nr_seq_trans_fin
	where   nm_atributo = 'VL_RECEBIMENTO'
	and     nm_tabela   = 'CONVENIO_RECEB'
	and     nr_documento = :new.nr_sequencia;

	update  ctb_documento
	set     vl_movimento = :new.vl_deposito,
		nr_seq_trans_financ = :new.nr_seq_trans_fin
	where   nm_atributo = 'VL_DEPOSITO'
	and     nm_tabela   = 'CONVENIO_RECEB'
	and     nr_documento = :new.nr_sequencia;

	update  ctb_documento
	set     vl_movimento = :new.vl_despesa_bancaria,
		nr_seq_trans_financ = :new.nr_seq_trans_fin
	where   nm_atributo = 'VL_DESPESA_BANCARIA'
	and     nm_tabela   = 'CONVENIO_RECEB'
	and     nr_documento = :new.nr_sequencia;

end if;

end convenio_receb_insert;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_BEFINSERT
before insert ON TASY.CONVENIO_RECEB for each row
declare

ie_vl_conv_rec_adic_w	varchar2(1);

begin

select 	nvl(max(ie_vl_conv_rec_adic),'N')
into	ie_vl_conv_rec_adic_w
from 	parametro_contas_receber
where 	cd_estabelecimento	= :new.cd_estabelecimento;

ie_vl_conv_rec_adic_w	:= nvl(OBTER_SE_ATUALIZA_JUROS_RECEB(:new.cd_convenio,:new.dt_recebimento), ie_vl_conv_rec_adic_w);

if	(ie_vl_conv_rec_adic_w = 'O') then
	:new.ie_deduzir_adic	:= 'N';
end if;

end CONVENIO_RECEB_BEFINSERT;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_DELETE
BEFORE DELETE ON TASY.CONVENIO_RECEB FOR EACH ROW
DECLARE

NR_SEQ_MOVTO_W		NUMBER(10,0);
IE_STATUS_W		VARCHAR2(01);
DT_FECHAMENTO_W		DATE := NULL;

BEGIN


IF	(:OLD.IE_STATUS <> 'N') THEN
	--N�o pode ser excluido um recebimendo j� vinculado');
	Wheb_mensagem_pck.exibir_mensagem_abort(261630);
END IF;

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'RC',:old.dt_recebimento,'E',:old.nm_usuario);

END;
/


ALTER TABLE TASY.CONVENIO_RECEB ADD (
  CONSTRAINT CONRECE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RECEB ADD (
  CONSTRAINT CONRECE_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT CONRECE_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CONRECE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT CONRECE_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CONRECE_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT CONRECE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONRECE_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT CONRECE_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_RECEB TO NIVEL_1;

