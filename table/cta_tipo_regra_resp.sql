ALTER TABLE TASY.CTA_TIPO_REGRA_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTA_TIPO_REGRA_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTA_TIPO_REGRA_RESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_PEND     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_PEND    NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTATIRR_CTARRPE_FK_I ON TASY.CTA_TIPO_REGRA_RESP
(NR_SEQ_REGRA_PEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTATIRR_CTARRPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTATIRR_CTATIPE_FK_I ON TASY.CTA_TIPO_REGRA_RESP
(NR_SEQ_TIPO_PEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTATIRR_CTATIPE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTATIRR_PK ON TASY.CTA_TIPO_REGRA_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CTA_TIPO_REGRA_RESP ADD (
  CONSTRAINT CTATIRR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTA_TIPO_REGRA_RESP ADD (
  CONSTRAINT CTATIRR_CTATIPE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PEND) 
 REFERENCES TASY.CTA_TIPO_PEND (NR_SEQUENCIA),
  CONSTRAINT CTATIRR_CTARRPE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PEND) 
 REFERENCES TASY.CTA_REGRA_RESP_PEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTA_TIPO_REGRA_RESP TO NIVEL_1;

