DROP TABLE TASY.PTU_LOTACAO_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_LOTACAO_BENEF
(
  CD_LOTACAO           VARCHAR2(10 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DS_LOTACAO           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTULOBE_ESTABEL_FK_I ON TASY.PTU_LOTACAO_BENEF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTULOBE_ESTABEL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_LOTACAO_BENEF ADD (
  CONSTRAINT PTULOBE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PTU_LOTACAO_BENEF TO NIVEL_1;

