ALTER TABLE TASY.W_PLS_INAD_ANUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_INAD_ANUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_INAD_ANUAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORDEM             NUMBER(10),
  DS_RECEITA           VARCHAR2(80 BYTE),
  IE_TIPO_RECEITA      VARCHAR2(5 BYTE),
  VL_JAN               NUMBER(15,2),
  VL_FEV               NUMBER(15,2),
  VL_MAR               NUMBER(15,2),
  VL_ABR               NUMBER(15,2),
  VL_MAI               NUMBER(15,2),
  VL_JUN               NUMBER(15,2),
  VL_JUL               NUMBER(15,2),
  VL_AGO               NUMBER(15,2),
  VL_SET               NUMBER(15,2),
  VL_OUT               NUMBER(15,2),
  VL_NOV               NUMBER(15,2),
  VL_DEZ               NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSINA_PK ON TASY.W_PLS_INAD_ANUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSINA_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_INAD_ANUAL ADD (
  CONSTRAINT WPLSINA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_INAD_ANUAL TO NIVEL_1;

