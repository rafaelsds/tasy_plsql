ALTER TABLE TASY.W_PROTHEUS_PEDIDO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PROTHEUS_PEDIDO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PROTHEUS_PEDIDO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PEDIDO        NUMBER(10),
  NR_ITEM              VARCHAR2(10 BYTE),
  CD_PRODUTO           VARCHAR2(15 BYTE),
  QT_ITEM              VARCHAR2(20 BYTE),
  CD_UNIDADE_MEDIDA    VARCHAR2(2 BYTE),
  VL_UNITARIO          VARCHAR2(20 BYTE),
  DT_ENTREGA           DATE,
  NR_SOLICITACAO       VARCHAR2(10 BYTE),
  NR_SOLICITACAO_ITEM  VARCHAR2(10 BYTE),
  NR_SOLICITACAO_TASY  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPROTPEDIT_PK ON TASY.W_PROTHEUS_PEDIDO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPROTPEDIT_PK
  MONITORING USAGE;


CREATE INDEX TASY.WPROTPEDIT_WPROTPED_FK_I ON TASY.W_PROTHEUS_PEDIDO_ITEM
(NR_SEQ_PEDIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPROTPEDIT_WPROTPED_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_PROTHEUS_PEDIDO_ITEM ADD (
  CONSTRAINT WPROTPEDIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PROTHEUS_PEDIDO_ITEM ADD (
  CONSTRAINT WPROTPEDIT_WPROTPED_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO) 
 REFERENCES TASY.W_PROTHEUS_PEDIDO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_PROTHEUS_PEDIDO_ITEM TO NIVEL_1;

