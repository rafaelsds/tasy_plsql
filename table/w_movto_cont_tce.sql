ALTER TABLE TASY.W_MOVTO_CONT_TCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_MOVTO_CONT_TCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MOVTO_CONT_TCE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_APRESENT       NUMBER(10),
  IE_TIPO_REGISTRO      NUMBER(10),
  CD_CGC                VARCHAR2(14 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_INICIO_INFORMACAO  DATE,
  DT_FIM_INFORMACAO     DATE,
  DT_GERACAO_ARQ        DATE,
  NM_SETOR_GOV          VARCHAR2(80 BYTE),
  CD_CONTA_SG           VARCHAR2(20 BYTE),
  NR_LOTE_CONTABIL      NUMBER(10),
  NR_SEQ_TRANS_FIN      NUMBER(10),
  DT_MOVIMENTO          DATE,
  IE_DEBITO_CREDITO     VARCHAR2(1 BYTE),
  VL_MOVIMENTO          NUMBER(15,2),
  DS_HISTORICO_LANC     VARCHAR2(180 BYTE),
  IE_ENCERRAMENTO       VARCHAR2(1 BYTE),
  QT_REGISTROS          NUMBER(10),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WMOVTCE_PK ON TASY.W_MOVTO_CONT_TCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WMOVTCE_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_MOVTO_CONT_TCE ADD (
  CONSTRAINT WMOVTCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          640K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_MOVTO_CONT_TCE TO NIVEL_1;

