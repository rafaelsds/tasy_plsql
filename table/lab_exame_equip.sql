ALTER TABLE TASY.LAB_EXAME_EQUIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_EXAME_EQUIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_EXAME_EQUIP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_EXAME           NUMBER(10)             NOT NULL,
  CD_EQUIPAMENTO         NUMBER(5)              NOT NULL,
  CD_EXAME_EQUIP         VARCHAR2(20 BYTE)      NOT NULL,
  IE_PADRAO              VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL        NUMBER(10),
  CD_MATERIAL_INTEGR     VARCHAR2(100 BYTE),
  DS_TEMPO               VARCHAR2(255 BYTE),
  DS_CONSERVANTE_INTEGR  VARCHAR2(255 BYTE),
  IE_TIPO_ATENDIMENTO    NUMBER(3),
  IE_URGENTE             VARCHAR2(1 BYTE),
  CD_FORMATO_INTEGRACAO  VARCHAR2(20 BYTE),
  NR_PRIOR_GERACAO       NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LABEXEQ_EQUILAB_FK_I ON TASY.LAB_EXAME_EQUIP
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABEXEQ_EQUILAB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LABEXEQ_EXALABO_FK_I ON TASY.LAB_EXAME_EQUIP
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LABEXEQ_I2_I ON TASY.LAB_EXAME_EQUIP
(NR_SEQ_EXAME, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABEXEQ_PK ON TASY.LAB_EXAME_EQUIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lab_exame_equip_before_ins_upd
before insert or update ON TASY.LAB_EXAME_EQUIP for each row
declare
ie_cons_cod_equip_w	varchar2(1);
qt_equip_duplicado_w	number(10);
ds_exame_duplicado_w	varchar2(255);

pragma autonomous_transaction;

cursor c01 is
	select	distinct
		nr_seq_exame,
		substr(obter_desc_exame(nr_seq_exame),1,250) ds_exame
	from	lab_exame_equip
	where	cd_equipamento = :new.cd_equipamento
	and	cd_exame_equip = :new.cd_exame_equip
	order by 2;

begin

obter_param_usuario(746, 38, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cons_cod_equip_w);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if (ie_cons_cod_equip_w = 'S') then

		select	count(1)
		into	qt_equip_duplicado_w
		from	lab_exame_equip
		where	cd_equipamento = :new.cd_equipamento
		and	cd_exame_equip = :new.cd_exame_equip
		and	rownum < 2;

		if  (qt_equip_duplicado_w > 0) then

			for r_c01_w in c01 loop
				begin
					if (ds_exame_duplicado_w is not null) then
						ds_exame_duplicado_w := substr(ds_exame_duplicado_w||chr(10)||r_c01_w.nr_seq_exame||' - '||r_c01_w.ds_exame,1,255);
						exit when length (ds_exame_duplicado_w) >= 254;
					else
						ds_exame_duplicado_w := chr(10)||chr(13)||r_c01_w.nr_seq_exame||' - '||r_c01_w.ds_exame;
					end if;
				end;
			end loop;
		wheb_mensagem_pck.exibir_mensagem_abort(289474,'DS_ERRO='||ds_exame_duplicado_w);

		end if;

	end if;

end if;

end;
/


ALTER TABLE TASY.LAB_EXAME_EQUIP ADD (
  CONSTRAINT LABEXEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_EXAME_EQUIP ADD (
  CONSTRAINT LABEXEQ_EQUILAB_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO_LAB (CD_EQUIPAMENTO),
  CONSTRAINT LABEXEQ_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT LABEXEQ_EXALAMA_FK 
 FOREIGN KEY (NR_SEQ_EXAME, NR_SEQ_MATERIAL) 
 REFERENCES TASY.EXAME_LAB_MATERIAL (NR_SEQ_EXAME,NR_SEQ_MATERIAL));

GRANT SELECT ON TASY.LAB_EXAME_EQUIP TO NIVEL_1;

