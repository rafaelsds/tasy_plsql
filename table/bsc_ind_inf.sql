ALTER TABLE TASY.BSC_IND_INF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BSC_IND_INF CASCADE CONSTRAINTS;

CREATE TABLE TASY.BSC_IND_INF
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_EMPRESA              NUMBER(4)             NOT NULL,
  NR_SEQ_INDICADOR        NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE                  NOT NULL,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE)     NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_ANO                  NUMBER(4)             NOT NULL,
  CD_PERIODO              NUMBER(2)             NOT NULL,
  QT_META                 NUMBER(15,2),
  QT_REAL                 NUMBER(15,2),
  QT_LIMITE               NUMBER(15,2),
  IE_FECHADO              VARCHAR2(1 BYTE),
  NR_SEQ_RESULT           NUMBER(10),
  DT_LIBERACAO            DATE,
  QT_REFERENCIA           NUMBER(15,2),
  DS_JUSTIFICATIVA        VARCHAR2(2000 BYTE),
  DS_ANALISE              LONG,
  IE_INFORMADO            VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_ANALISE  DATE,
  NM_USUARIO_ANALISE      VARCHAR2(15 BYTE),
  DT_LIB_ANALISE          DATE,
  NM_USUARIO_LIB          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BSCININ_BSCCLRE_FK_I ON TASY.BSC_IND_INF
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BSCININ_BSCCLRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BSCININ_BSCINDI_FK_I ON TASY.BSC_IND_INF
(NR_SEQ_INDICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BSCININ_EMPRESA_FK_I ON TASY.BSC_IND_INF
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BSCININ_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BSCININ_ESTABEL_FK_I ON TASY.BSC_IND_INF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BSCININ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BSCININ_I1 ON TASY.BSC_IND_INF
(NR_SEQ_INDICADOR, CD_ANO, CD_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BSCININ_PK ON TASY.BSC_IND_INF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.bsc_ind_inf_update
before update ON TASY.BSC_IND_INF for each row
declare

ie_regra_result_w	varchar2(15);
nm_indicador_w	varchar2(255);

begin

select	nvl(max(ie_regra_result),'MA'),
	max(nm_indicador)
into	ie_regra_result_w,
	nm_indicador_w
from	bsc_indicador
where	nr_sequencia = :new.nr_seq_indicador;

if	(ie_regra_result_w = 'MA') then
	begin
	if	(nvl(:new.qt_limite,0) > nvl(:new.qt_meta,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(281869,'NR_SEQ_INDICADOR='||:new.nr_seq_indicador ||';NM_INDICADOR='|| nm_indicador_w);
	end if;
	end;
elsif	(ie_regra_result_w = 'ME') then
	begin
	if	(nvl(:new.qt_limite,0) < nvl(:new.qt_meta,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(281870,'NR_SEQ_INDICADOR='||:new.nr_seq_indicador ||';NM_INDICADOR='|| nm_indicador_w);
	end if;
	end;
end if;
end bsc_ind_inf_update;
/


ALTER TABLE TASY.BSC_IND_INF ADD (
  CONSTRAINT BSCININ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BSC_IND_INF ADD (
  CONSTRAINT BSCININ_BSCINDI_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.BSC_INDICADOR (NR_SEQUENCIA),
  CONSTRAINT BSCININ_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT BSCININ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT BSCININ_BSCCLRE_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.BSC_CLASSIF_RESULT (NR_SEQUENCIA));

GRANT SELECT ON TASY.BSC_IND_INF TO NIVEL_1;

