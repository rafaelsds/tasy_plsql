ALTER TABLE TASY.SAN_LOG_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_LOG_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_LOG_EQUIPAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRODUCAO      NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANEQUIP_PK ON TASY.SAN_LOG_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEQUIP_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANEQUIP_SANEQUI_FK_I ON TASY.SAN_LOG_EQUIPAMENTO
(NR_SEQ_EQUIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEQUIP_SANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANEQUIP_SANPROD_FK_I ON TASY.SAN_LOG_EQUIPAMENTO
(NR_SEQ_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_LOG_EQUIPAMENTO ADD (
  CONSTRAINT SANEQUIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_LOG_EQUIPAMENTO ADD (
  CONSTRAINT SANEQUIP_SANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPO) 
 REFERENCES TASY.SAN_EQUIPO (NR_SEQUENCIA),
  CONSTRAINT SANEQUIP_SANPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUCAO) 
 REFERENCES TASY.SAN_PRODUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_LOG_EQUIPAMENTO TO NIVEL_1;

