ALTER TABLE TASY.TITULO_PAGAR_INFO_AUX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_INFO_AUX CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_INFO_AUX
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_CONTRATO      NUMBER(10),
  NR_SEQ_CONTA_MEDICA  NUMBER(10),
  NR_SEQ_SEGURADO      NUMBER(10),
  DT_RECEBIMENTO       DATE,
  DT_ATENDIMENTO       DATE,
  NM_PRESTADOR         VARCHAR2(255 BYTE),
  NR_NOTA_FISCAL       VARCHAR2(255 BYTE),
  VL_TOTAL_CONTA       NUMBER(15,2),
  VL_GLOSA             NUMBER(15,2),
  VL_COPARTICIPACAO    NUMBER(15,2),
  NR_SEQ_PLANO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TITPAGIAU_PK ON TASY.TITULO_PAGAR_INFO_AUX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGIAU_TITPAGA_FK_I ON TASY.TITULO_PAGAR_INFO_AUX
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TITULO_PAGAR_INFO_AUX ADD (
  CONSTRAINT TITPAGIAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_INFO_AUX ADD (
  CONSTRAINT TITPAGIAU_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.TITULO_PAGAR_INFO_AUX TO NIVEL_1;

