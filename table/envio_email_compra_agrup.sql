ALTER TABLE TASY.ENVIO_EMAIL_COMPRA_AGRUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ENVIO_EMAIL_COMPRA_AGRUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENVIO_EMAIL_COMPRA_AGRUP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_DOCUMENTO         NUMBER(10)               NOT NULL,
  IE_TIPO_DOCUMENTO    VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_MENSAGEM     VARCHAR2(15 BYTE)        NOT NULL,
  DT_CANCELAMENTO      DATE,
  DS_EMAIL_ORIGEM      VARCHAR2(255 BYTE),
  NM_USUARIO_ORIGEM    VARCHAR2(255 BYTE),
  DS_ASSUNTO           VARCHAR2(255 BYTE),
  DS_MENSAGEM          VARCHAR2(4000 BYTE),
  NR_SEQ_APROVACAO     NUMBER(10),
  NR_SEQ_PROC_APROV    NUMBER(10),
  IE_CANCELADO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENEMCAG_ESTABEL_FK_I ON TASY.ENVIO_EMAIL_COMPRA_AGRUP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENEMCAG_I1 ON TASY.ENVIO_EMAIL_COMPRA_AGRUP
(DT_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENEMCAG_I2 ON TASY.ENVIO_EMAIL_COMPRA_AGRUP
(IE_CANCELADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ENEMCAG_PK ON TASY.ENVIO_EMAIL_COMPRA_AGRUP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ENEMCAG_PK
  MONITORING USAGE;


CREATE INDEX TASY.ENEMCAG_PROAPCO_FK_I ON TASY.ENVIO_EMAIL_COMPRA_AGRUP
(NR_SEQ_APROVACAO, NR_SEQ_PROC_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ENVIO_EMAIL_COMPRA_AGRUP ADD (
  CONSTRAINT ENEMCAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ENVIO_EMAIL_COMPRA_AGRUP ADD (
  CONSTRAINT ENEMCAG_PROAPCO_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO, NR_SEQ_PROC_APROV) 
 REFERENCES TASY.PROCESSO_APROV_COMPRA (NR_SEQUENCIA,NR_SEQ_PROC_APROV)
    ON DELETE CASCADE,
  CONSTRAINT ENEMCAG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.ENVIO_EMAIL_COMPRA_AGRUP TO NIVEL_1;

