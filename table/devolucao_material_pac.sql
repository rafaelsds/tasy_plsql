ALTER TABLE TASY.DEVOLUCAO_MATERIAL_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DEVOLUCAO_MATERIAL_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.DEVOLUCAO_MATERIAL_PAC
(
  NR_DEVOLUCAO            NUMBER(10)            NOT NULL,
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  DT_ENTRADA_UNIDADE      DATE                  NOT NULL,
  DT_DEVOLUCAO            DATE                  NOT NULL,
  CD_PESSOA_FISICA_DEVOL  VARCHAR2(10 BYTE)     NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5)             NOT NULL,
  NM_USUARIO_DEVOL        VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO_BAIXA        VARCHAR2(15 BYTE),
  DT_LIBERACAO_BAIXA      DATE,
  DT_BAIXA_TOTAL          DATE,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_IMPRESSAO            DATE,
  DT_ENTREGA_FISICA       DATE,
  NM_USUARIO_ENTREGA      VARCHAR2(15 BYTE),
  NR_DOC_INTERNO          NUMBER(15),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4),
  IE_PERMITE_ESTORNAR     VARCHAR2(1 BYTE),
  CD_MOTIVO_DEVOLUCAO     VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DEVMAPA_ESTABEL_FK_I ON TASY.DEVOLUCAO_MATERIAL_PAC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEVMAPA_SETATE2_FK_I ON TASY.DEVOLUCAO_MATERIAL_PAC
(NR_ATENDIMENTO, CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEVMATP_ATEPACU_FK_I ON TASY.DEVOLUCAO_MATERIAL_PAC
(NR_ATENDIMENTO, DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEVMATP_PESFISI_FK_I ON TASY.DEVOLUCAO_MATERIAL_PAC
(CD_PESSOA_FISICA_DEVOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DEVMATP_PK ON TASY.DEVOLUCAO_MATERIAL_PAC
(NR_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEVMATP_SETATEN_FK_I ON TASY.DEVOLUCAO_MATERIAL_PAC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.devolucao_material_pac_update
after update or insert ON TASY.DEVOLUCAO_MATERIAL_PAC for each row
declare

ds_log_w		varchar2(2000);
reg_integracao_p	gerar_int_padrao.reg_integracao;

begin

if	(inserting) then
  ds_log_w := substr(ds_log_w ||';NR_DEVOLUCAO='||:new.nr_devolucao || ';call stack='|| substr(dbms_utility.format_call_stack,1,1800),1,2000);
	gravar_log_tasy(5135, ds_log_w, :new.nm_usuario);		gravar_log_tasy(5135, ds_log_w, :new.nr_devolucao);
else
  begin

  if	((:old.dt_liberacao_baixa is null) and (:new.dt_liberacao_baixa is not null)) or
    ((:new.dt_liberacao_baixa is null) and (:old.dt_liberacao_baixa is not null)) then
    reg_integracao_p.cd_setor_atendimento	:= :new.cd_setor_atendimento;
    reg_integracao_p.nr_seq_agrupador	:= :new.nr_devolucao;

    gerar_int_padrao.gravar_integracao('290', :new.nr_devolucao, :new.nm_usuario, reg_integracao_p);
  end if;

  if	(nvl(:new.dt_liberacao_baixa,sysdate) <> nvl(:old.dt_liberacao_baixa,sysdate-15)) then
    ds_log_w := 'DT_LIBERACAO OLD/NEW:' || to_char(:old.dt_liberacao_baixa,'dd/mm/yyyy hh24:mi:ss') || '-' || to_char(:new.dt_liberacao_baixa,'dd/mm/yyyy hh24:mi:ss');
  end if;

  if	(nvl(:new.dt_baixa_total, sysdate) <> nvl(:old.dt_baixa_total, sysdate)) then
    ds_log_w := 'DT_BAIXA_TOTAL OLD/NEW:' || to_char(:old.dt_baixa_total,'dd/mm/yyyy hh24:mi:ss') || '-' || to_char(:new.dt_baixa_total,'dd/mm/yyyy hh24:mi:ss');
  end if;

  if	(ds_log_w is not null) then
    ds_log_w := substr(ds_log_w ||';NR_DEVOLUCAO='||:new.nr_devolucao || ';call stack='|| substr(dbms_utility.format_call_stack,1,1800),1,2000);
    gravar_log_tasy(5135, ds_log_w, :new.nm_usuario);
  end if;

  end;
end if;
end;
/


ALTER TABLE TASY.DEVOLUCAO_MATERIAL_PAC ADD (
  CONSTRAINT DEVMATP_PK
 PRIMARY KEY
 (NR_DEVOLUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DEVOLUCAO_MATERIAL_PAC ADD (
  CONSTRAINT DEVMAPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DEVMATP_ATEPACU_FK 
 FOREIGN KEY (NR_ATENDIMENTO, DT_ENTRADA_UNIDADE) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_ATENDIMENTO,DT_ENTRADA_UNIDADE),
  CONSTRAINT DEVMATP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_DEVOL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DEVMATP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.DEVOLUCAO_MATERIAL_PAC TO NIVEL_1;

