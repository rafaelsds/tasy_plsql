ALTER TABLE TASY.DECHARACTERIZATION_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DECHARACTERIZATION_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DECHARACTERIZATION_RULE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_NAME                VARCHAR2(255 BYTE)     NOT NULL,
  CD_EXP_NOTE            NUMBER(10)             NOT NULL,
  NR_STRATEGY            NUMBER(10)             NOT NULL,
  NR_CUSTOM_ALGORITHM    NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DECHRULE_CUSTMALGOR_FK_I ON TASY.DECHARACTERIZATION_RULE
(NR_CUSTOM_ALGORITHM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECHRULE_DICEXPR_FK_I ON TASY.DECHARACTERIZATION_RULE
(CD_EXP_NOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DECHRULE_PK ON TASY.DECHARACTERIZATION_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECHRULE_STRTGY_FK_I ON TASY.DECHARACTERIZATION_RULE
(NR_STRATEGY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DECHARACTERIZATION_RULE ADD (
  CONSTRAINT DECHRULE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DECHARACTERIZATION_RULE ADD (
  CONSTRAINT DECHRULE_CUSTMALGOR_FK 
 FOREIGN KEY (NR_CUSTOM_ALGORITHM) 
 REFERENCES TASY.CUSTOM_ALGORITHM (NR_SEQUENCIA),
  CONSTRAINT DECHRULE_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_NOTE) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DECHRULE_STRTGY_FK 
 FOREIGN KEY (NR_STRATEGY) 
 REFERENCES TASY.STRATEGY (NR_SEQUENCIA));

GRANT SELECT ON TASY.DECHARACTERIZATION_RULE TO NIVEL_1;

