ALTER TABLE TASY.QUE_QUESTAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUE_QUESTAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUE_QUESTAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_SEXO                  VARCHAR2(1 BYTE),
  NR_SEQ_QUESTIONARIO      NUMBER(10),
  QT_NIVEL                 NUMBER(3),
  NR_SEQ_APRESENTACAO      NUMBER(5),
  DS_QUESTAO               VARCHAR2(255 BYTE),
  IE_LOCAL_RESPOSTA        VARCHAR2(1 BYTE),
  NR_SEQ_QUESTAO_OPCAO     NUMBER(10),
  QT_IDADE_MIN             NUMBER(3),
  QT_IDADE_MAX             NUMBER(3),
  IE_TIPO_ATENDIMENTO      NUMBER(5),
  IE_TIPO_RESPOSTA         VARCHAR2(1 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_QUESTAO_SUP       NUMBER(10),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_IMAGEM_BANCO      NUMBER(10),
  DS_SQL                   VARCHAR2(4000 BYTE),
  IE_EXIBE_OPCAO_ANTERIOR  VARCHAR2(1 BYTE),
  DS_SQL_OBSERVACAO        VARCHAR2(4000 BYTE),
  DS_SQL_INFORMACAO        VARCHAR2(4000 BYTE),
  DS_INFORMACAO            VARCHAR2(255 BYTE),
  DS_MASCARA               VARCHAR2(255 BYTE),
  IE_TIPO_TECLADO          VARCHAR2(3 BYTE),
  IE_RESPOSTA_OBRIGATORIA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUEQST_IMABANC_FK_I ON TASY.QUE_QUESTAO
(NR_SEQ_IMAGEM_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUEQST_PK ON TASY.QUE_QUESTAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUEQST_QUEQUEOP_FK_I ON TASY.QUE_QUESTAO
(NR_SEQ_QUESTAO_OPCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEQST_QUEQUEOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUEQST_QUEQUEST_FK_I ON TASY.QUE_QUESTAO
(NR_SEQ_QUESTIONARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEQST_QUEQUEST_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.QUE_QUESTAO_INSERT
before insert ON TASY.QUE_QUESTAO for each row
declare

qt_nivel_w	number(3);

begin

if	(:new.NR_SEQ_QUESTAO_SUP is null) then
	:new.qt_nivel := 1;
else
	select	qt_nivel
	into	qt_nivel_w
	from	que_questao
	where	nr_sequencia = :new.NR_SEQ_QUESTAO_SUP;

	:new.qt_nivel := nvl(qt_nivel_w, 0) + 1;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.QUE_QUESTAO_UPDATE
before update ON TASY.QUE_QUESTAO for each row
declare

Cursor C01 is
	select	nr_sequencia
	from	que_questao
	where	nr_seq_questao_sup = :new.nr_sequencia;

nivel_w			number(3) := 0;
nr_seq_inferior_w	number(10);

pragma autonomous_transaction;

begin
if	(:new.NR_SEQ_QUESTAO_SUP is null) then
	:new.qt_nivel := 1;
else
	if	(nvl(:old.NR_SEQ_QUESTAO_SUP, 0) <> nvl(:new.NR_SEQ_QUESTAO_SUP, 0)) then

		select	qt_nivel
		into	nivel_w
		from	que_questao
		where	nr_sequencia = :new.NR_SEQ_QUESTAO_SUP;

		:new.qt_nivel := nvl(nivel_w, 0) + 1;
	end if;
end if;

open C01;
loop
fetch	C01 into
	nr_seq_inferior_w;
exit when C01%notfound;
	begin

	update	que_questao
	set	qt_nivel = :new.qt_nivel +1
	where	nr_sequencia = nr_seq_inferior_w;

	end;
end loop;
close C01;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.que_questao_before_delete
before delete ON TASY.QUE_QUESTAO for each row
declare

ie_existe_questao_inf_w	Varchar2(1);
pragma autonomous_transaction;

begin
select	nvl(max('S'),'N')
into	ie_existe_questao_inf_w
from	que_questao
where	nr_seq_questao_sup = :new.nr_sequencia;

if	(ie_existe_questao_inf_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(682736);
end if;

end;
/


ALTER TABLE TASY.QUE_QUESTAO ADD (
  CONSTRAINT QUEQST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUE_QUESTAO ADD (
  CONSTRAINT QUEQST_IMABANC_FK 
 FOREIGN KEY (NR_SEQ_IMAGEM_BANCO) 
 REFERENCES TASY.IMAGEM_BANCO (NR_SEQUENCIA),
  CONSTRAINT QUEQST_QUEQUEOP_FK 
 FOREIGN KEY (NR_SEQ_QUESTAO_OPCAO) 
 REFERENCES TASY.QUE_QUESTAO_OPCAO (NR_SEQUENCIA),
  CONSTRAINT QUEQST_QUEQUEST_FK 
 FOREIGN KEY (NR_SEQ_QUESTIONARIO) 
 REFERENCES TASY.QUE_QUESTIONARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUE_QUESTAO TO NIVEL_1;

