ALTER TABLE TASY.AGENDA_PAC_EQUIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_EQUIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_EQUIP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_AGENDA          NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_EQUIPAMENTO         NUMBER(10),
  DT_CONFIRMACAO         DATE,
  NR_SEQ_CLASSIF_EQUIP   NUMBER(10),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ORIGEM_INF          VARCHAR2(1 BYTE),
  NR_SEQ_PROC_INTERNO    NUMBER(10),
  IE_OBRIGATORIO         VARCHAR2(1 BYTE),
  IE_STATUS_EQUIPAMENTO  VARCHAR2(15 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  DT_APROVACAO           DATE,
  DT_CHECAGEM            DATE,
  NM_USUARIO_CHECAGEM    VARCHAR2(15 BYTE),
  NR_SEQ_LISTA_ESPERA    NUMBER(10),
  IE_TIPO_EXCLUSAO       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEPAEQ_AGEPACI_FK_I ON TASY.AGENDA_PAC_EQUIP
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAEQ_AGLIESP_FK_I ON TASY.AGENDA_PAC_EQUIP
(NR_SEQ_LISTA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAEQ_CLEQUIP_FK_I ON TASY.AGENDA_PAC_EQUIP
(NR_SEQ_CLASSIF_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAEQ_CLEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAEQ_EQUIPAM_FK_I ON TASY.AGENDA_PAC_EQUIP
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAEQ_EQUIPAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAEQ_I1 ON TASY.AGENDA_PAC_EQUIP
(IE_ORIGEM_INF, NR_SEQ_CLASSIF_EQUIP, NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAEQ_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGEPAEQ_PK ON TASY.AGENDA_PAC_EQUIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAEQ_PROINTE_FK_I ON TASY.AGENDA_PAC_EQUIP
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAEQ_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_pac_equip_atual_hl7
after insert or update ON TASY.AGENDA_PAC_EQUIP for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
cd_pessoa_fisica_w	varchar2(10);
dt_confirmacao_w	date;
ie_gerar_integracao_w	varchar2(1);

begin

ds_sep_bv_w := obter_separador_bv;

if	(inserting)  or
	(:old.cd_equipamento <> :new.cd_equipamento)  then

	select	max(a.cd_pessoa_fisica),
		max(a.dt_confirmacao),
		max(ie_integra_rel_dixtal)
	into	cd_pessoa_fisica_w,
		dt_confirmacao_w,
		ie_gerar_integracao_w
	from	agenda b,
		agenda_paciente a
	where	a.nr_sequencia = :new.nr_seq_agenda
	and	b.cd_agenda = a.cd_agenda;

	if	(cd_pessoa_fisica_w is not null) and (dt_confirmacao_w is not null) and (ie_gerar_integracao_w = 'S') then
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'nr_sequencia=' || :new.nr_seq_agenda || ds_sep_bv_w;
		gravar_agend_integracao(193, ds_param_integ_hl7_w);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_pac_equip_atual
before insert or update ON TASY.AGENDA_PAC_EQUIP for each row
declare
ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);

pragma autonomous_transaction;

begin
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);

select  a.ie_status_agenda,
	b.cd_tipo_agenda
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :new.nr_seq_agenda;

exception
	when others then
      	null;
end;
if	(ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E') then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

end if;

if	(updating) and (nvl(:new.ie_origem_inf,'I') = 'E') and (:new.ie_tipo_exclusao is null)	then
	:new.ie_tipo_exclusao := '2';
elsif	(updating) and (nvl(:new.ie_origem_inf,'I') = 'I') then
	:new.ie_tipo_exclusao := null;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_EQUIP_DELETE
before delete ON TASY.AGENDA_PAC_EQUIP FOR EACH ROW
DECLARE

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);

pragma autonomous_transaction;

BEGIN
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);

select  max(a.ie_status_agenda),
	max(b.cd_tipo_agenda)
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :old.nr_seq_agenda;

exception
	when others then
      	null;
end;

if	((ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E')) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_EQUIP_tp  after update ON TASY.AGENDA_PAC_EQUIP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EQUIPAMENTO,1,4000),substr(:new.CD_EQUIPAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EQUIPAMENTO',ie_log_w,ds_w,'AGENDA_PAC_EQUIP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_EQUIPAMENTO,1,4000),substr(:new.IE_STATUS_EQUIPAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_EQUIPAMENTO',ie_log_w,ds_w,'AGENDA_PAC_EQUIP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_EQUIP,1,4000),substr(:new.NR_SEQ_CLASSIF_EQUIP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_EQUIP',ie_log_w,ds_w,'AGENDA_PAC_EQUIP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AGENDA_PAC_EQUIP ADD (
  CONSTRAINT AGEPAEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_EQUIP ADD (
  CONSTRAINT AGEPAEQ_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA_ESPERA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEPAEQ_CLEQUIP_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_EQUIP) 
 REFERENCES TASY.CLASSIF_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEPAEQ_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEPAEQ_EQUIPAM_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO (CD_EQUIPAMENTO),
  CONSTRAINT AGEPAEQ_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_PAC_EQUIP TO NIVEL_1;

