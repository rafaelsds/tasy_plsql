ALTER TABLE TASY.WORK_HISTORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WORK_HISTORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.WORK_HISTORY
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_OCCUPATION          NUMBER(10),
  DT_EMPLOYMENT          DATE,
  DT_DISMISSION          DATE,
  DS_NOTE                VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_WORK_SHIFT          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WOHIST_PESFISI_FK_I ON TASY.WORK_HISTORY
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WOHIST_PK ON TASY.WORK_HISTORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WOHIST_PROFISS_FK_I ON TASY.WORK_HISTORY
(CD_OCCUPATION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WOHIST_TURTRAB_FK_I ON TASY.WORK_HISTORY
(CD_WORK_SHIFT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.WORK_HISTORY ADD (
  CONSTRAINT WOHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.WORK_HISTORY ADD (
  CONSTRAINT WOHIST_TURTRAB_FK 
 FOREIGN KEY (CD_WORK_SHIFT) 
 REFERENCES TASY.TURNO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT WOHIST_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WOHIST_PROFISS_FK 
 FOREIGN KEY (CD_OCCUPATION) 
 REFERENCES TASY.PROFISSAO (CD_PROFISSAO));

