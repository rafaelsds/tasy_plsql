ALTER TABLE TASY.CAPTURA_SOFTWARE_DETALHE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAPTURA_SOFTWARE_DETALHE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAPTURA_SOFTWARE_DETALHE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_INCONSISTENCIA  NUMBER(10),
  DS_SOFTWARE_CAP        VARCHAR2(255 BYTE),
  NR_SEQ_CAPT_SOFTWARE   NUMBER(10)             NOT NULL,
  NR_ID_SOFT_CAP         NUMBER(10),
  IE_INCONSISTENTE       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPSODET_CAPSOFT_FK_I ON TASY.CAPTURA_SOFTWARE_DETALHE
(NR_SEQ_CAPT_SOFTWARE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPSODET_PK ON TASY.CAPTURA_SOFTWARE_DETALHE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CPSODET_PK
  MONITORING USAGE;


ALTER TABLE TASY.CAPTURA_SOFTWARE_DETALHE ADD (
  CONSTRAINT CPSODET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAPTURA_SOFTWARE_DETALHE ADD (
  CONSTRAINT CPSODET_CAPSOFT_FK 
 FOREIGN KEY (NR_SEQ_CAPT_SOFTWARE) 
 REFERENCES TASY.CAPTURA_SOFTWARE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CAPTURA_SOFTWARE_DETALHE TO NIVEL_1;

