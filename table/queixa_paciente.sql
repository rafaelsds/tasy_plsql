ALTER TABLE TASY.QUEIXA_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUEIXA_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUEIXA_PACIENTE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_QUEIXA             VARCHAR2(80 BYTE)       NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_PLANO_SAUDE        VARCHAR2(1 BYTE),
  IE_HEMODIALISE        VARCHAR2(1 BYTE),
  DS_COR                VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_EXTERNO            VARCHAR2(10 BYTE),
  IE_PROT_INSTITUT      VARCHAR2(1 BYTE),
  IE_PROT_INSTITUT_ANT  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUEPACI_ESTABEL_FK_I ON TASY.QUEIXA_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUEPACI_PK ON TASY.QUEIXA_PACIENTE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.QUEIXA_PACIENTE_tp  after update ON TASY.QUEIXA_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EXTERNO,1,4000),substr(:new.CD_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXTERNO',ie_log_w,ds_w,'QUEIXA_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.QUEIXA_PACIENTE ADD (
  CONSTRAINT QUEPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUEIXA_PACIENTE ADD (
  CONSTRAINT QUEPACI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.QUEIXA_PACIENTE TO NIVEL_1;

