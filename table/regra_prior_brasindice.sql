ALTER TABLE TASY.REGRA_PRIOR_BRASINDICE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PRIOR_BRASINDICE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PRIOR_BRASINDICE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_PRECO        VARCHAR2(3 BYTE)         NOT NULL,
  NR_PRIORIDADE        NUMBER(3)                NOT NULL,
  DT_VIGENCIA          DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REPRIBR_PK ON TASY.REGRA_PRIOR_BRASINDICE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRIBR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_PRIOR_BRASINDICE_tp  after update ON TASY.REGRA_PRIOR_BRASINDICE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'REGRA_PRIOR_BRASINDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA,1,4000),substr(:new.DT_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA',ie_log_w,ds_w,'REGRA_PRIOR_BRASINDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRIORIDADE,1,4000),substr(:new.NR_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRIORIDADE',ie_log_w,ds_w,'REGRA_PRIOR_BRASINDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO,1,4000),substr(:new.IE_TIPO_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO',ie_log_w,ds_w,'REGRA_PRIOR_BRASINDICE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_PRIOR_BRASINDICE ADD (
  CONSTRAINT REPRIBR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_PRIOR_BRASINDICE TO NIVEL_1;

