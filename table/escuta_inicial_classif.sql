ALTER TABLE TASY.ESCUTA_INICIAL_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCUTA_INICIAL_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCUTA_INICIAL_CLASSIF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF             NUMBER(10),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_TRIAGEM_PRIORIDADE  NUMBER(10),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ESCUTA              NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCINCL_ATEPACI_FK_I ON TASY.ESCUTA_INICIAL_CLASSIF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCINCL_ATESINI_FK_I ON TASY.ESCUTA_INICIAL_CLASSIF
(NR_SEQ_ESCUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCINCL_PESFISI_FK2_I ON TASY.ESCUTA_INICIAL_CLASSIF
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCINCL_PK ON TASY.ESCUTA_INICIAL_CLASSIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCINCL_TRCLPRI_FK_I ON TASY.ESCUTA_INICIAL_CLASSIF
(NR_SEQ_TRIAGEM_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCINCL_TRICLRI_FK_I ON TASY.ESCUTA_INICIAL_CLASSIF
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESCUTA_INICIAL_CLASSIF ADD (
  CONSTRAINT ESCINCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCUTA_INICIAL_CLASSIF ADD (
  CONSTRAINT ESCINCL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCINCL_ATESINI_FK 
 FOREIGN KEY (NR_SEQ_ESCUTA) 
 REFERENCES TASY.ATEND_ESCUTA_INICIAL (NR_SEQUENCIA),
  CONSTRAINT ESCINCL_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCINCL_TRCLPRI_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM_PRIORIDADE) 
 REFERENCES TASY.TRIAGEM_CLASSIF_PRIORIDADE (NR_SEQUENCIA),
  CONSTRAINT ESCINCL_TRICLRI_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.TRIAGEM_CLASSIF_RISCO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCUTA_INICIAL_CLASSIF TO NIVEL_1;

