ALTER TABLE TASY.PLS_LOTE_ESCRIT_QUOTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_ESCRIT_QUOTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_ESCRIT_QUOTA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_LOTE                   DATE                NOT NULL,
  DT_FECHAMENTO             DATE,
  NR_SEQ_TIPO_ESCRIT_QUOTA  NUMBER(10)          NOT NULL,
  QT_QUOTAS                 NUMBER(10,4)        NOT NULL,
  CD_MOEDA                  NUMBER(3),
  VL_ESCRITURACAO           NUMBER(15,2)        NOT NULL,
  CD_CONDICAO_PAGAMENTO     NUMBER(10),
  VL_ENTRADA                NUMBER(15,2)        NOT NULL,
  PR_ACRESCIMO              NUMBER(7,4),
  QT_PARCELA                NUMBER(5),
  IE_MES_VENCIMENTO         VARCHAR2(1 BYTE),
  IE_ACAO_NAO_UTIL          VARCHAR2(1 BYTE),
  NR_DIA_VENC               NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOEQ_CONPAGA_FK_I ON TASY.PLS_LOTE_ESCRIT_QUOTA
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEQ_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEQ_MOEDA_FK_I ON TASY.PLS_LOTE_ESCRIT_QUOTA
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEQ_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOEQ_PK ON TASY.PLS_LOTE_ESCRIT_QUOTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEQ_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEQ_PLSTIEQ_FK_I ON TASY.PLS_LOTE_ESCRIT_QUOTA
(NR_SEQ_TIPO_ESCRIT_QUOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEQ_PLSTIEQ_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_LOTE_ESCRIT_QUOTA_tp  after update ON TASY.PLS_LOTE_ESCRIT_QUOTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_CONDICAO_PAGAMENTO,1,500);gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'PLS_LOTE_ESCRIT_QUOTA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MOEDA,1,500);gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'PLS_LOTE_ESCRIT_QUOTA',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_ENTRADA,1,500);gravar_log_alteracao(substr(:old.VL_ENTRADA,1,4000),substr(:new.VL_ENTRADA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ENTRADA',ie_log_w,ds_w,'PLS_LOTE_ESCRIT_QUOTA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_QUOTAS,1,500);gravar_log_alteracao(substr(:old.QT_QUOTAS,1,4000),substr(:new.QT_QUOTAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_QUOTAS',ie_log_w,ds_w,'PLS_LOTE_ESCRIT_QUOTA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_ESCRIT_QUOTA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ESCRIT_QUOTA,1,4000),substr(:new.NR_SEQ_TIPO_ESCRIT_QUOTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ESCRIT_QUOTA',ie_log_w,ds_w,'PLS_LOTE_ESCRIT_QUOTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_LOTE_ESCRIT_QUOTA ADD (
  CONSTRAINT PLSLOEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_ESCRIT_QUOTA ADD (
  CONSTRAINT PLSLOEQ_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PLSLOEQ_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSLOEQ_PLSTIEQ_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ESCRIT_QUOTA) 
 REFERENCES TASY.PLS_TIPO_ESCRIT_QUOTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_ESCRIT_QUOTA TO NIVEL_1;

