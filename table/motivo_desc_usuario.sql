ALTER TABLE TASY.MOTIVO_DESC_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_DESC_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_DESC_USUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO        NUMBER(10)               NOT NULL,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MODEUSU_MOTDESC_FK_I ON TASY.MOTIVO_DESC_USUARIO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MODEUSU_MOTDESC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MODEUSU_PK ON TASY.MOTIVO_DESC_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MODEUSU_PK
  MONITORING USAGE;


CREATE INDEX TASY.MODEUSU_USUARIO_FK_I ON TASY.MOTIVO_DESC_USUARIO
(NM_USUARIO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MODEUSU_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.MOTIVO_DESC_USUARIO ADD (
  CONSTRAINT MODEUSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_DESC_USUARIO ADD (
  CONSTRAINT MODEUSU_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_LIB) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT MODEUSU_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MOTIVO_DESC_USUARIO TO NIVEL_1;

