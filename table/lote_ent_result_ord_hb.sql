ALTER TABLE TASY.LOTE_ENT_RESULT_ORD_HB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENT_RESULT_ORD_HB CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENT_RESULT_ORD_HB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_CORRIDA           NUMBER(5)                NOT NULL,
  CD_EXAME             VARCHAR2(20 BYTE),
  CD_BARRAS            VARCHAR2(100 BYTE),
  DS_RESULTADO         VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LEREORHB_ESTABEL_FK_I ON TASY.LOTE_ENT_RESULT_ORD_HB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEREORHB_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LEREORHB_PK ON TASY.LOTE_ENT_RESULT_ORD_HB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEREORHB_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.lote_ent_result_ord_hb_update
before update ON TASY.LOTE_ENT_RESULT_ORD_HB for each row
declare
nr_prescricao_w		number(14);
nr_seq_resultado_w	number(10);
nr_seq_prescr_w		number(10);
nr_seq_exame_w		number(10);
ie_formato_result_w	varchar2(3);
begin

select	MAX(a.nr_seq_exame),
		MAX(a.nr_sequencia),
		max(a.nr_prescricao),
		max(e.ie_formato_resultado)
into	nr_seq_exame_w,
		nr_seq_prescr_w,
		nr_prescricao_w,
		ie_formato_result_w
from   	exame_laboratorio e,
		lote_ent_sec_ficha c,
		prescr_medica d,
		prescr_procedimento a
where  	a.nr_prescricao = d.nr_prescricao
and		a.nr_seq_exame = e.nr_seq_exame
and		d.nr_prescricao = c.nr_prescricao
and		a.cd_motivo_baixa = 0
and		a.ie_status_atend = 30
and		c.cd_barras = :new.cd_barras
and		nvl(nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'PERKINS'),e.cd_exame_integracao),e.cd_exame) = :new.cd_exame;

if (nr_prescricao_w is not null) then
	select 	max(nr_seq_resultado)
	into	nr_seq_resultado_w
	from	exame_lab_resultado
	where	nr_prescricao = nr_prescricao_w;

	if	(substr(ie_formato_result_w,1,1) = 'V') then
		update	exame_lab_result_item
		set		qt_resultado = :new.ds_resultado,
				dt_atualizacao = sysdate,
				nm_usuario = :new.nm_usuario
		where	nr_seq_resultado = nr_seq_resultado_w
		and		nr_seq_prescr = nr_seq_prescr_w;

	elsif (substr(ie_formato_result_w,1,1) = 'D') then
		update	exame_lab_result_item
		set		ds_resultado = :new.ds_resultado,
				dt_atualizacao = sysdate,
				nm_usuario = :new.nm_usuario
		where	nr_seq_resultado = nr_seq_resultado_w
		and		nr_seq_prescr = nr_seq_prescr_w;

	end if;
end if;
end;
/


ALTER TABLE TASY.LOTE_ENT_RESULT_ORD_HB ADD (
  CONSTRAINT LEREORHB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENT_RESULT_ORD_HB ADD (
  CONSTRAINT LEREORHB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.LOTE_ENT_RESULT_ORD_HB TO NIVEL_1;

