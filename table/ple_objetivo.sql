ALTER TABLE TASY.PLE_OBJETIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLE_OBJETIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLE_OBJETIVO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_EDICAO          NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_OBJETIVO            VARCHAR2(4000 BYTE)    NOT NULL,
  NR_SEQ_APRES           NUMBER(15)             NOT NULL,
  NR_SEQ_CLASSIF         NUMBER(10)             NOT NULL,
  NR_SEQ_TEMA            NUMBER(10),
  CD_PF_RESP             VARCHAR2(10 BYTE),
  PR_PARTIC_PE           NUMBER(15,2),
  DS_TITULO              VARCHAR2(120 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  CD_EXP_OBJETIVO        NUMBER(10),
  CD_EXP_TITULO          NUMBER(10),
  DS_JUSTIFICATIVA       VARCHAR2(2000 BYTE),
  DS_RESULTADO           VARCHAR2(2000 BYTE),
  DS_PONTO_ATENCAO       VARCHAR2(2000 BYTE),
  IE_IMPACTO_TEMA        VARCHAR2(15 BYTE),
  IE_PRIORIDADE          VARCHAR2(15 BYTE),
  IE_NIVEL_INVESTIMENTO  VARCHAR2(15 BYTE),
  IE_NIVEL_RH            VARCHAR2(15 BYTE),
  CD_PF_PATROCINADOR     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLEOBJE_BSCTEES_FK_I ON TASY.PLE_OBJETIVO
(NR_SEQ_TEMA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_DICEXPR_FK_I ON TASY.PLE_OBJETIVO
(CD_EXP_OBJETIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_DICEXPR_FK1_I ON TASY.PLE_OBJETIVO
(CD_EXP_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_PESFISI_FK_I ON TASY.PLE_OBJETIVO
(CD_PF_RESP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_PESFISI_FK2_I ON TASY.PLE_OBJETIVO
(CD_PF_PATROCINADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLEOBJE_PK ON TASY.PLE_OBJETIVO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_PLECLAS_FK_I ON TASY.PLE_OBJETIVO
(NR_SEQ_CLASSIF)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLEOBJE_PLEEDIC_FK_I ON TASY.PLE_OBJETIVO
(NR_SEQ_EDICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLE_OBJETIVO ADD (
  CONSTRAINT PLEOBJE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLE_OBJETIVO ADD (
  CONSTRAINT PLEOBJE_PESFISI_FK2 
 FOREIGN KEY (CD_PF_PATROCINADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLEOBJE_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_OBJETIVO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT PLEOBJE_DICEXPR_FK1 
 FOREIGN KEY (CD_EXP_TITULO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT PLEOBJE_PLEEDIC_FK 
 FOREIGN KEY (NR_SEQ_EDICAO) 
 REFERENCES TASY.PLE_EDICAO (NR_SEQUENCIA),
  CONSTRAINT PLEOBJE_PLECLAS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PLE_CLASSIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PLEOBJE_BSCTEES_FK 
 FOREIGN KEY (NR_SEQ_TEMA) 
 REFERENCES TASY.BSC_TEMA_ESTRATEGICO (NR_SEQUENCIA),
  CONSTRAINT PLEOBJE_PESFISI_FK 
 FOREIGN KEY (CD_PF_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PLE_OBJETIVO TO NIVEL_1;

