ALTER TABLE TASY.AGENDA_INTEGRADA_RESUMO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_INTEGRADA_RESUMO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_INTEGRADA_RESUMO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AGEINT        NUMBER(10)               NOT NULL,
  DS_RESUMO            VARCHAR2(4000 BYTE),
  DS_RESUMO_LONG       LONG
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEINRE_AGEINTE_FK_I ON TASY.AGENDA_INTEGRADA_RESUMO
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINRE_AGEINTE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGEINRE_PK ON TASY.AGENDA_INTEGRADA_RESUMO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.AGENDA_INTEGRADA_RESUMO ADD (
  CONSTRAINT AGEINRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_INTEGRADA_RESUMO ADD (
  CONSTRAINT AGEINRE_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_INTEGRADA_RESUMO TO NIVEL_1;

