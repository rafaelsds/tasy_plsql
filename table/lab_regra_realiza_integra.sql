ALTER TABLE TASY.LAB_REGRA_REALIZA_INTEGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_REGRA_REALIZA_INTEGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_REGRA_REALIZA_INTEGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_ATENDIMENTO  NUMBER(3),
  CD_ESTABELECIMENTO   NUMBER(4),
  IE_AUTORIZACAO       VARCHAR2(3 BYTE),
  IE_INTEGRAR          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LAREGAINT_PK ON TASY.LAB_REGRA_REALIZA_INTEGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LAB_REGRA_REALIZA_INTEGRA_tp  after update ON TASY.LAB_REGRA_REALIZA_INTEGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTEGRAR,1,4000),substr(:new.IE_INTEGRAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTEGRAR',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTORIZACAO,1,4000),substr(:new.IE_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTORIZACAO',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'LAB_REGRA_REALIZA_INTEGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LAB_REGRA_REALIZA_INTEGRA ADD (
  CONSTRAINT LAREGAINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_REGRA_REALIZA_INTEGRA TO NIVEL_1;

