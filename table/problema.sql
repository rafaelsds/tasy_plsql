ALTER TABLE TASY.PROBLEMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROBLEMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROBLEMA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_CLASSIF       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  QT_URGENTE           NUMBER(3)                NOT NULL,
  QT_IMPORTANTE        NUMBER(3)                NOT NULL,
  QT_ESCORE            NUMBER(3)                NOT NULL,
  DT_REGISTRO          DATE                     NOT NULL,
  DS_PROBLEMA          VARCHAR2(100 BYTE)       NOT NULL,
  DS_DETALHE           VARCHAR2(2000 BYTE)      NOT NULL,
  VL_PERDA_ANUAL       NUMBER(15,2)             NOT NULL,
  DS_CAUSA             VARCHAR2(2000 BYTE),
  DS_AREA_ENVOLVIDA    VARCHAR2(255 BYTE),
  DS_CLIENTE           VARCHAR2(255 BYTE),
  CD_RESPONSAVEL       VARCHAR2(10 BYTE),
  DS_ESTATISTICA       VARCHAR2(255 BYTE),
  QT_TENDENCIA         NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROBLEM_ESTABEL_FK_I ON TASY.PROBLEMA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROBLEM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROBLEM_PESFISI_FK_I ON TASY.PROBLEMA
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROBLEM_PK ON TASY.PROBLEMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROBLEM_PROCLAS_FK_I ON TASY.PROBLEMA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROBLEM_PROCLAS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Problema_atual
BEFORE insert or update ON TASY.PROBLEMA FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(nvl(:new.qt_urgente,1) > 10) or
	(nvl(:new.qt_importante,1) > 10) or
	(nvl(:new.qt_tendencia,1) > 10) or
	(nvl(:new.qt_urgente,1) < 1) or
	(nvl(:new.qt_importante,1) < 1) or
	(nvl(:new.qt_tendencia,1) < 1) then
	--Os valores de urg�ncia, import�ncia e tend�ncia devem ser de 1 a 10.');
	Wheb_mensagem_pck.exibir_mensagem_abort(261595);
end if;


:new.qt_escore	:= nvl(:new.qt_urgente,1) * nvl(:new.qt_importante,1) * nvl(:new.qt_tendencia,1);
<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.PROBLEMA ADD (
  CONSTRAINT PROBLEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROBLEMA ADD (
  CONSTRAINT PROBLEM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROBLEM_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROBLEM_PROCLAS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROBLEMA_CLASSIF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROBLEMA TO NIVEL_1;

