DROP TABLE TASY.CCD_ACCEPTABLE_TEXT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CCD_ACCEPTABLE_TEXT
(
  NM_OBJECT            VARCHAR2(255 BYTE),
  DS_ACCEPTABLE_TEXT   VARCHAR2(120 BYTE),
  CD_COMPARE           NUMBER(1),
  NR_OBJECT            NUMBER(10),
  IE_REFERENCE         NUMBER(3),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.CCD_ACCEPTABLE_TEXT TO NIVEL_1;

