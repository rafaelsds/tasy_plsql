ALTER TABLE TASY.PATIENT_IMP_FIE_DET
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATIENT_IMP_FIE_DET CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATIENT_IMP_FIE_DET
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PAT_IMP_FIELD    NUMBER(10),
  OBX_CODE_SYSTEM         VARCHAR2(255 BYTE),
  OBX_NAME_SYSTEM         VARCHAR2(255 BYTE),
  NR_SEQ_TAB_ATRI_LINKED  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAIMFIEDET_PATIMPFIE_FK_I ON TASY.PATIENT_IMP_FIE_DET
(NR_SEQ_PAT_IMP_FIELD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAIMFIEDET_PK ON TASY.PATIENT_IMP_FIE_DET
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAIMFIEDET_TABATRLIN_FK_I ON TASY.PATIENT_IMP_FIE_DET
(NR_SEQ_TAB_ATRI_LINKED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PATIENT_IMP_FIE_DET ADD (
  CONSTRAINT PAIMFIEDET_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PATIENT_IMP_FIE_DET ADD (
  CONSTRAINT PAIMFIEDET_PATIMPFIE_FK 
 FOREIGN KEY (NR_SEQ_PAT_IMP_FIELD) 
 REFERENCES TASY.PATIENT_IMPORT_FIELDS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PAIMFIEDET_TABATRLIN_FK 
 FOREIGN KEY (NR_SEQ_TAB_ATRI_LINKED) 
 REFERENCES TASY.TABELA_ATRIBUTO_LINKED (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PATIENT_IMP_FIE_DET TO NIVEL_1;

