ALTER TABLE TASY.ATEND_TIMI_RISK_SUPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_TIMI_RISK_SUPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_TIMI_RISK_SUPRA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_AVALIACAO                DATE              NOT NULL,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  QT_ANO                      NUMBER(3)         NOT NULL,
  IE_DM_HTN_ANGINA            VARCHAR2(1 BYTE)  NOT NULL,
  IE_PAS                      VARCHAR2(1 BYTE)  NOT NULL,
  IE_FC                       VARCHAR2(1 BYTE)  NOT NULL,
  IE_PESO                     VARCHAR2(1 BYTE)  NOT NULL,
  IE_KILLIP                   VARCHAR2(1 BYTE)  NOT NULL,
  IE_SUPRA_ST_ANT_BRE         VARCHAR2(1 BYTE)  NOT NULL,
  IE_TEMPO_RX                 VARCHAR2(1 BYTE)  NOT NULL,
  QT_ESCORE_ANO               NUMBER(15,3)      NOT NULL,
  QT_ESCORE_DM_HTN_ANGINA     NUMBER(15,3)      NOT NULL,
  QT_ESCORE_PAS               NUMBER(15,3)      NOT NULL,
  QT_ESCORE_FC                NUMBER(15,3)      NOT NULL,
  QT_ESCORE_PESO              NUMBER(15,3)      NOT NULL,
  QT_ESCORE_KILLIP            NUMBER(15,3)      NOT NULL,
  QT_ESCORE_SUPRA_ST_ANT_BRE  NUMBER(15,3)      NOT NULL,
  QT_ESCORE_TEMPO_RX          NUMBER(15,3)      NOT NULL,
  QT_ESCORE                   NUMBER(15,3)      NOT NULL,
  PR_RISCO_MORTE_30D          NUMBER(4,1)       NOT NULL,
  DT_LIBERACAO                DATE,
  CD_PERFIL_ATIVO             NUMBER(5),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATETIRS_ATEPACI_FK_I ON TASY.ATEND_TIMI_RISK_SUPRA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETIRS_I1 ON TASY.ATEND_TIMI_RISK_SUPRA
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETIRS_PERFIL_FK_I ON TASY.ATEND_TIMI_RISK_SUPRA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETIRS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATETIRS_PESFISI_FK_I ON TASY.ATEND_TIMI_RISK_SUPRA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATETIRS_PK ON TASY.ATEND_TIMI_RISK_SUPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_TIMI_RISK_SUPRA_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ATEND_TIMI_RISK_SUPRA FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_escore_ano				:= 0;
:new.qt_escore_dm_htn_angina		:= 0;
:new.qt_escore_pas				:= 0;
:new.qt_escore_fc				:= 0;
:new.qt_escore_killip			:= 0;
:new.qt_escore_peso				:= 0;
:new.qt_escore_supra_st_ant_bre		:= 0;
:new.qt_escore_tempo_rx			:= 0;

:new.qt_escore				:= 0;
:new.pr_risco_morte_30d			:= 0;

if	(:new.qt_ano > 74) then
	:new.qt_escore_ano			:= 3;
elsif	(:new.qt_ano > 64) then
	:new.qt_escore_ano			:= 2;
end if;
if	(:new.ie_dm_htn_angina = 'S') then
	:new.qt_escore_dm_htn_angina	:= 1;
end if;
if	(:new.ie_pas = 'S') then
	:new.qt_escore_pas			:= 3;
end if;
if	(:new.ie_fc = 'S') then
	:new.qt_escore_fc			:= 2;
end if;
if	(:new.ie_peso = 'S') then
	:new.qt_escore_peso			:= 1;
end if;
if	(:new.ie_killip = 'S') then
	:new.qt_escore_killip		:= 2;
end if;
if	(:new.ie_supra_st_ant_bre = 'S') then
	:new.qt_escore_supra_st_ant_bre	:= 1;
end if;
if	(:new.ie_tempo_rx = 'S') then
	:new.qt_escore_tempo_rx		:= 1;
end if;

:new.qt_escore				:=
		:new.qt_escore_ano	+
		:new.qt_escore_dm_htn_angina +
		:new.qt_escore_pas +
		:new.qt_escore_fc +
		:new.qt_escore_killip +
		:new.qt_escore_peso +
		:new.qt_escore_tempo_rx +
		:new.qt_escore_supra_st_ant_bre;

if	(:new.qt_escore = 0) then
	:new.pr_risco_morte_30d		:= 0.8;
elsif	(:new.qt_escore = 1) then
	:new.pr_risco_morte_30d		:= 1.6;
elsif	(:new.qt_escore = 2) then
	:new.pr_risco_morte_30d		:= 2.2;
elsif	(:new.qt_escore = 3) then
	:new.pr_risco_morte_30d		:= 4.4;
elsif	(:new.qt_escore = 4) then
	:new.pr_risco_morte_30d		:= 7.3;
elsif	(:new.qt_escore = 5) then
	:new.pr_risco_morte_30d		:= 12;
elsif	(:new.qt_escore = 6) then
	:new.pr_risco_morte_30d		:= 16;
elsif	(:new.qt_escore = 7) then
	:new.pr_risco_morte_30d		:= 23;
elsif	(:new.qt_escore = 8) then
	:new.pr_risco_morte_30d		:= 27;
elsif	(:new.qt_escore > 8) then
	:new.pr_risco_morte_30d		:= 36;
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ATEND_TIMI_RISK_SUPRA ADD (
  CONSTRAINT ATETIRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_TIMI_RISK_SUPRA ADD (
  CONSTRAINT ATETIRS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATETIRS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATETIRS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ATEND_TIMI_RISK_SUPRA TO NIVEL_1;

