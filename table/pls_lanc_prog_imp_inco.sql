ALTER TABLE TASY.PLS_LANC_PROG_IMP_INCO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LANC_PROG_IMP_INCO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LANC_PROG_IMP_INCO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LANC_PROG_IMP   NUMBER(10),
  NR_SEQ_LANC_PROG_INCO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLPII_PK ON TASY.PLS_LANC_PROG_IMP_INCO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPII_PLSILPI_FK_I ON TASY.PLS_LANC_PROG_IMP_INCO
(NR_SEQ_LANC_PROG_INCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPII_PLSLPIM_FK_I ON TASY.PLS_LANC_PROG_IMP_INCO
(NR_SEQ_LANC_PROG_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LANC_PROG_IMP_INCO ADD (
  CONSTRAINT PLSLPII_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LANC_PROG_IMP_INCO ADD (
  CONSTRAINT PLSLPII_PLSILPI_FK 
 FOREIGN KEY (NR_SEQ_LANC_PROG_INCO) 
 REFERENCES TASY.PLS_INCONS_LANC_PROG_IMP (NR_SEQUENCIA),
  CONSTRAINT PLSLPII_PLSLPIM_FK 
 FOREIGN KEY (NR_SEQ_LANC_PROG_IMP) 
 REFERENCES TASY.PLS_LANC_PROG_IMPORTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LANC_PROG_IMP_INCO TO NIVEL_1;

