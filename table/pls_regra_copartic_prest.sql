ALTER TABLE TASY.PLS_REGRA_COPARTIC_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_PREST
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_REGRA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_PRESTADOR_COOPERADO       VARCHAR2(1 BYTE),
  NR_SEQ_PRESTADOR_ATEND       NUMBER(10),
  NR_SEQ_TIPO_PRESTADOR_ATEND  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRCPE_PK ON TASY.PLS_REGRA_COPARTIC_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCPE_PLSPRES_FK_I ON TASY.PLS_REGRA_COPARTIC_PREST
(NR_SEQ_PRESTADOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCPE_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_PREST
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCPE_PLSTIPR_FK_I ON TASY.PLS_REGRA_COPARTIC_PREST
(NR_SEQ_TIPO_PRESTADOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_COPARTIC_PREST_tp  after update ON TASY.PLS_REGRA_COPARTIC_PREST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR_ATEND,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR_ATEND,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR_ATEND',ie_log_w,ds_w,'PLS_REGRA_COPARTIC_PREST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_copart_prest_update
before update ON TASY.PLS_REGRA_COPARTIC_PREST for each row
declare

begin
if	(:new.ie_prestador_cooperado <> :old.ie_prestador_cooperado) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_prestador_cooperado, :new.ie_prestador_cooperado, 'PLS_REGRA_COPARTIC_PREST', 'IE_PRESTADOR_COOPERADO', :new.nm_usuario);
end if;
if	(:new.nr_seq_tipo_prestador_atend <> :old.nr_seq_tipo_prestador_atend) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_tipo_prestador_atend, :new.nr_seq_tipo_prestador_atend, 'PLS_REGRA_COPARTIC_PREST', 'NR_SEQ_TIPO_PRESTADOR_ATEND', :new.nm_usuario);
end if;
if	(:new.nr_seq_prestador_atend <> :old.nr_seq_prestador_atend) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_prestador_atend, :new.nr_seq_prestador_atend, 'PLS_REGRA_COPARTIC_PREST', 'NR_SEQ_PRESTADOR_ATEND', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_PREST ADD (
  CONSTRAINT PLSRCPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_PREST ADD (
  CONSTRAINT PLSRCPE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_ATEND) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRCPE_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSRCPE_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR_ATEND) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_PREST TO NIVEL_1;

