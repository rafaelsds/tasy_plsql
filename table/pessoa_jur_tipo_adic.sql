ALTER TABLE TASY.PESSOA_JUR_TIPO_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JUR_TIPO_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JUR_TIPO_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  CD_TIPO_PESSOA       NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEJUTAD_ESTABEL_FK_I ON TASY.PESSOA_JUR_TIPO_ADIC
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEJUTAD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEJUTAD_PJCOCON_FK_I ON TASY.PESSOA_JUR_TIPO_ADIC
(CD_CGC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEJUTAD_PK ON TASY.PESSOA_JUR_TIPO_ADIC
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEJUTAD_TIPPEJU_FK_I ON TASY.PESSOA_JUR_TIPO_ADIC
(CD_TIPO_PESSOA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_JUR_TIPO_ADIC_INSERT
after INSERT or update ON TASY.PESSOA_JUR_TIPO_ADIC FOR EACH ROW
declare
cd_conta_pagamento_w	varchar2(20);
cd_conta_recebimento_w	varchar2(20);
cd_estabelecimento_w	number(6);
qt_existe_w		number(6);

begin

select	nvl(max(cd_conta_pagamento), 'X'),
	nvl(max(cd_conta_recebimento), 'X'),
	nvl(max(cd_estabelecimento), 0)
into	cd_conta_pagamento_w,
	cd_conta_recebimento_w,
	cd_estabelecimento_w
from	tipo_pessoa_juridica
where	cd_tipo_pessoa = :new.cd_tipo_pessoa
and	nvl(ie_atualiza_pj, 'D') = 'D';


if	(cd_conta_pagamento_w <> 'X') then
	begin
	select	count(*)
	into	qt_existe_w
	from	pessoa_jur_conta_cont
	where	ie_tipo_conta = 'P'
	and	cd_estabelecimento = cd_estabelecimento_w
	and	cd_cgc = :new.cd_cgc;

	if	(qt_existe_w > 0) then

		update	pessoa_jur_conta_cont
		set	cd_conta_contabil = cd_conta_pagamento_w,
			dt_inicio_vigencia = obter_vigencia_conta_contabil(cd_conta_pagamento_w,'I'),
			dt_fim_vigencia = obter_vigencia_conta_contabil(cd_conta_pagamento_w,'F')
		where	ie_tipo_conta = 'P'
		and	cd_estabelecimento = cd_estabelecimento_w
		and	cd_cgc = :new.cd_cgc;
	else
		insert into pessoa_jur_conta_cont(
			nr_sequencia,
			cd_cgc,
			cd_conta_contabil,
			dt_atualizacao,
			nm_usuario,
			cd_empresa,
			ie_tipo_conta,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			cd_estabelecimento)
		select pessoa_jur_conta_cont_seq.nextval,
			:new.cd_cgc,
			cd_conta_pagamento_w,
			sysdate,
			:new.nm_usuario,
			a.cd_empresa,
			'P',
			sysdate,
			:new.nm_usuario,
			obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'I'),
			obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'F'),
			decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
			from empresa a;
	end if;
	end;
end if;


if	(cd_conta_recebimento_w <> 'X') then
	begin
	select	count(*)
	into	qt_existe_w
	from	pessoa_jur_conta_cont
	where	ie_tipo_conta = 'R'
	and	cd_estabelecimento = cd_estabelecimento_w
	and	cd_cgc = :new.cd_cgc;

	if	(qt_existe_w > 0) then
		update	pessoa_jur_conta_cont
		set	cd_conta_contabil = cd_conta_recebimento_w,
			dt_inicio_vigencia = obter_vigencia_conta_contabil(cd_conta_recebimento_w,'I'),
			dt_fim_vigencia = obter_vigencia_conta_contabil(cd_conta_recebimento_w,'F')
		where	ie_tipo_conta = 'R'
		and	cd_estabelecimento = cd_estabelecimento_w
		and	cd_cgc = :new.cd_cgc;

	else

		insert into pessoa_jur_conta_cont(
			nr_sequencia,
			cd_cgc,
			cd_conta_contabil,
			dt_atualizacao,
			nm_usuario,
			cd_empresa,
			ie_tipo_conta,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			cd_estabelecimento)
		select pessoa_jur_conta_cont_seq.nextval,
			:new.cd_cgc,
			cd_conta_recebimento_w,
			sysdate,
			:new.nm_usuario,
			a.cd_empresa,
			'R',
			sysdate,
			:new.nm_usuario,
			obter_vigencia_conta_contabil(cd_conta_recebimento_w, 'I'),
			obter_vigencia_conta_contabil(cd_conta_recebimento_w, 'F'),
			decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
		from	empresa a;
	end if;
	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JUR_TIPO_ADIC_tp  after update ON TASY.PESSOA_JUR_TIPO_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_TIPO_PESSOA,1,4000),substr(:new.CD_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PESSOA',ie_log_w,ds_w,'PESSOA_JUR_TIPO_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JUR_TIPO_ADIC ADD (
  CONSTRAINT PEJUTAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JUR_TIPO_ADIC ADD (
  CONSTRAINT PEJUTAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PEJUTAD_PJCOCON_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PEJUTAD_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PESSOA) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA));

GRANT SELECT ON TASY.PESSOA_JUR_TIPO_ADIC TO NIVEL_1;

