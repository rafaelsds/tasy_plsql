ALTER TABLE TASY.PFCS_DETAIL_EXAM_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_DETAIL_EXAM_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_DETAIL_EXAM_LAB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_EXAME          NUMBER(10),
  NM_EXAM               VARCHAR2(80 BYTE),
  IE_STATUS_ATEND       NUMBER(2),
  DT_APPROVAL           DATE,
  NM_USER_APPROVAL      VARCHAR2(15 BYTE),
  NR_REQUEST            NUMBER(14),
  DT_REQUEST            DATE,
  DT_RELEASE_REQUEST    DATE,
  DS_REQUEST_PHYSICIAN  VARCHAR2(60 BYTE),
  DS_RESULT             VARCHAR2(4000 BYTE),
  IE_FAST_RESULT_CHECK  VARCHAR2(1 BYTE),
  NR_SEQ_DETAIL         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PFCSDTEXAM_PK ON TASY.PFCS_DETAIL_EXAM_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PFCS_DETAIL_EXAM_LAB ADD (
  CONSTRAINT PFCSDTEXAM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PFCS_DETAIL_EXAM_LAB TO NIVEL_1;

