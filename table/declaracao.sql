ALTER TABLE TASY.DECLARACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DECLARACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DECLARACAO
(
  CD_DECLARACAO         VARCHAR2(10 BYTE)       NOT NULL,
  DS_DECLARACAO         VARCHAR2(4000 BYTE)     NOT NULL,
  IE_TIPO_DECLARACAO    VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_TITULO_DECLARACAO  VARCHAR2(255 BYTE),
  DS_DECLARACAO_CLOB    CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_DECLARACAO_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DECLARA_PK ON TASY.DECLARACAO
(CD_DECLARACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DECLARACAO_AFT_UPD
BEFORE INSERT OR UPDATE ON TASY.DECLARACAO FOR EACH ROW
BEGIN
  if (updating) then

	if (pkg_i18n.get_user_locale() = 'en_AU' and (nvl(:old.DS_DECLARACAO_CLOB, 'XPTO') <> nvl(:new.DS_DECLARACAO_CLOB, 'XPTO'))

     )then /*quando alterado pelo Java ou HTML*/

		:new.DS_DECLARACAO := SUBSTR(:new.DS_DECLARACAO_CLOB,1,3999);

	elsif (:old.DS_DECLARACAO <> :new.DS_DECLARACAO) then /*quando alterado pelo Delphi*/

		:new.DS_DECLARACAO_CLOB := :new.DS_DECLARACAO;

	end if;

elsif (inserting) then

	if (pkg_i18n.get_user_locale() = 'en_AU' and (nvl(:new.DS_DECLARACAO_CLOB,'XPTO') <> 'XPTO' or :new.DS_DECLARACAO_CLOB is not null)) then

		:new.DS_DECLARACAO := SUBSTR(:new.DS_DECLARACAO_CLOB,1,3999);

	elsif (nvl(:new.DS_DECLARACAO,'XPTO') <> 'XPTO') then /*quando alterado pelo Delphi*/

		:new.DS_DECLARACAO_CLOB := :new.DS_DECLARACAO;

	end if;

end if;

END;
/


ALTER TABLE TASY.DECLARACAO ADD (
  CONSTRAINT DECLARA_PK
 PRIMARY KEY
 (CD_DECLARACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.DECLARACAO TO NIVEL_1;

