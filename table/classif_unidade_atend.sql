ALTER TABLE TASY.CLASSIF_UNIDADE_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLASSIF_UNIDADE_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLASSIF_UNIDADE_ATEND
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_CLASSIFICACAO      VARCHAR2(255 BYTE)      NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_DESTACAR_PANORAMA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CLSUNAT_PK ON TASY.CLASSIF_UNIDADE_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.classif_unidade_atend_update
before update ON TASY.CLASSIF_UNIDADE_ATEND for each row
declare
ie_possui_registro_w	number(10);
ds_setor_atendimento_w	varchar2(255);
ds_setor_setor_msg_w	varchar2(4000);
cd_setor_atendimento_w	number(10);
ie_acao_inativar_unidade_w	varchar2(1);

Cursor C01 is
		select	distinct(cd_setor_atendimento),
				substr(obter_nome_setor(cd_setor_atendimento),1,40)
		from	unidade_atendimento
		where	nr_seq_classif = :new.nr_sequencia;

begin

Obter_param_Usuario(1, 27, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_acao_inativar_unidade_w);

if	(:new.ie_situacao <> :old.ie_situacao) and
	(:new.ie_situacao = 'I') then
	select	count(*)
	into	ie_possui_registro_w
	from	unidade_atendimento
	where	nr_seq_classif = :new.nr_sequencia;

	if	(nvl(ie_possui_registro_w,0) > 0) then
		open C01;
		loop
		fetch C01 into
			cd_setor_atendimento_w,
			ds_setor_atendimento_w;
		exit when C01%notfound;
			begin
			ds_setor_setor_msg_w	:= ds_setor_setor_msg_w||', '||ds_setor_atendimento_w;
			end;
		end loop;
		if	(ie_acao_inativar_unidade_w <> 'N') then
			if	(ie_acao_inativar_unidade_w = 'B') then
				Wheb_mensagem_pck.exibir_mensagem_abort(242734,'DS_SETOR_ATEND=' ||substr(ds_setor_setor_msg_w,2,4000));
			elsif (ie_acao_inativar_unidade_w = 'A') then
				update	unidade_atendimento
				set		nr_seq_classif = null
				where	nr_seq_classif = :new.nr_sequencia;
			end if;
		end if;
		close C01;
	end if;
end if;

end;
/


ALTER TABLE TASY.CLASSIF_UNIDADE_ATEND ADD (
  CONSTRAINT CLSUNAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CLASSIF_UNIDADE_ATEND TO NIVEL_1;

