ALTER TABLE TASY.GMDAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GMDAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.GMDAT
(
  NR_SEQUENCIA         NUMBER(10),
  PRODCODE             NUMBER(15),
  FORMCODE             NUMBER(15),
  GENCODE              NUMBER(15),
  GENERICSORT          NUMBER(15),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VERSION              NUMBER(10)               DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MIMGM_MIMSDAT_FK_I ON TASY.GMDAT
(PRODCODE, FORMCODE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MIMGM_MIMSGEN_FK_I ON TASY.GMDAT
(GENCODE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MIMGM_PK ON TASY.GMDAT
(PRODCODE, FORMCODE, GENCODE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GMDAT ADD (
  CONSTRAINT MIMGM_PK
 PRIMARY KEY
 (PRODCODE, FORMCODE, GENCODE));

ALTER TABLE TASY.GMDAT ADD (
  CONSTRAINT MIMGM_MIMSGEN_FK 
 FOREIGN KEY (GENCODE) 
 REFERENCES TASY.GENDAT (GENCODE)
    ON DELETE CASCADE,
  CONSTRAINT MIMGM_MIMSDAT_FK 
 FOREIGN KEY (PRODCODE, FORMCODE) 
 REFERENCES TASY.FORMDAT (PRODCODE,FORMCODE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.GMDAT TO NIVEL_1;

