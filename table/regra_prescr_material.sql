ALTER TABLE TASY.REGRA_PRESCR_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PRESCR_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PRESCR_MATERIAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_PERMITE_PRESCREVER  VARCHAR2(1 BYTE)       NOT NULL,
  CD_MATERIAL            NUMBER(10),
  CD_GRUPO_MATERIAL      NUMBER(3),
  CD_SUBGRUPO_MATERIAL   NUMBER(3),
  CD_CLASSE_MATERIAL     NUMBER(5),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  IE_TIPO_ATENDIMENTO    NUMBER(3),
  CD_CONVENIO            NUMBER(5),
  DS_MENSAGEM            VARCHAR2(255 BYTE),
  IE_SOMENTE_PRESCRITOR  VARCHAR2(1 BYTE),
  IE_CONSISTE_ASSOC      VARCHAR2(1 BYTE),
  QT_IDADE_MIN           NUMBER(3),
  QT_IDADE_MAX           NUMBER(3),
  IE_RESPONSAVEL         VARCHAR2(1 BYTE),
  CD_CATEGORIA           VARCHAR2(10 BYTE),
  IE_SOMENTE_CIRURGIA    VARCHAR2(1 BYTE),
  QT_IDADE_MIN_MES       NUMBER(15,2),
  QT_IDADE_MIN_DIA       NUMBER(15,2),
  QT_IDADE_MAX_MES       NUMBER(15,2),
  QT_IDADE_MAX_DIA       NUMBER(15,2),
  IE_VIA_APLICACAO       VARCHAR2(5 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_INTERVALO           VARCHAR2(7 BYTE),
  IE_DURACAO             VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGPREM_CATCONV_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_CLAMATE_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_CONVENI_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_ESTABEL_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPREM_GRUMATE_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_INTPRES_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPREM_MATERIA_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGPREM_PK ON TASY.REGRA_PRESCR_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPREM_SETATEN_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_SUBMATE_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPREM_SUBMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGPREM_VIAAPLI_FK_I ON TASY.REGRA_PRESCR_MATERIAL
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_PRESCR_MATERIAL ADD (
  CONSTRAINT REGPREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PRESCR_MATERIAL ADD (
  CONSTRAINT REGPREM_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGPREM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REGPREM_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT REGPREM_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT REGPREM_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT REGPREM_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGPREM_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REGPREM_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT REGPREM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGPREM_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO));

GRANT SELECT ON TASY.REGRA_PRESCR_MATERIAL TO NIVEL_1;

