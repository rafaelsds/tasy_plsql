ALTER TABLE TASY.HEM_SYNTAX_DOENCA_DIFUSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_SYNTAX_DOENCA_DIFUSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_SYNTAX_DOENCA_DIFUSA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SYNTAX        NUMBER(10)               NOT NULL,
  NR_SEQ_SEGMENTO      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMDODI_HEMSEGS_FK_I ON TASY.HEM_SYNTAX_DOENCA_DIFUSA
(NR_SEQ_SEGMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMDODI_HEMSYNT_FK_I ON TASY.HEM_SYNTAX_DOENCA_DIFUSA
(NR_SEQ_SYNTAX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMDODI_PK ON TASY.HEM_SYNTAX_DOENCA_DIFUSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEM_SYNTAX_DOENCA_DIFUSA ADD (
  CONSTRAINT HEMDODI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_SYNTAX_DOENCA_DIFUSA ADD (
  CONSTRAINT HEMDODI_HEMSYNT_FK 
 FOREIGN KEY (NR_SEQ_SYNTAX) 
 REFERENCES TASY.HEM_SYNTAX (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMDODI_HEMSEGS_FK 
 FOREIGN KEY (NR_SEQ_SEGMENTO) 
 REFERENCES TASY.HEM_SEGMENTO_SYNTAX (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_SYNTAX_DOENCA_DIFUSA TO NIVEL_1;

