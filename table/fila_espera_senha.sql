ALTER TABLE TASY.FILA_ESPERA_SENHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FILA_ESPERA_SENHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FILA_ESPERA_SENHA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_FILA                     VARCHAR2(80 BYTE) NOT NULL,
  DS_CURTO                    VARCHAR2(50 BYTE),
  DS_COR_PAINEL               VARCHAR2(15 BYTE) NOT NULL,
  QT_FAIXA_INICIO             NUMBER(5)         NOT NULL,
  QT_FAIXA_FIM                NUMBER(5)         NOT NULL,
  NR_PRIORIDADE               NUMBER(5),
  HR_REINICIO_PRIORIDADE      DATE,
  QT_FAIXA_ATUAL              NUMBER(5),
  IE_TIPO                     VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIFICACAO        NUMBER(10),
  DS_LETRA_VERIFICACAO        VARCHAR2(10 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  DS_OBSERVACAO               VARCHAR2(80 BYTE),
  NR_SEQ_APRESENTACAO         NUMBER(5),
  IE_FILA_RETORNO             VARCHAR2(1 BYTE),
  NR_DIGITO_FILA              NUMBER(1),
  CD_PERFIL                   NUMBER(5),
  DS_COR_FONTE                VARCHAR2(15 BYTE),
  HR_INUTILIZACAO_SENHA       DATE,
  QT_TEMPO_MEDIO              NUMBER(10),
  QT_TEMPO_MAXIMO             NUMBER(10),
  QT_TAMANHO_FONTE_OBS        NUMBER(10),
  IE_FONTE_OBS                NUMBER(10),
  DS_COR_FONTE_OBS            VARCHAR2(15 BYTE),
  IE_PERMITE_CHAMADA          VARCHAR2(1 BYTE),
  NR_PRIORIDADE_PADRAO        NUMBER(5),
  NR_SEQ_FILA_ESPERA_DESTINO  NUMBER(10),
  QT_TEMPO_FINALIZACAO        NUMBER(10),
  IE_MOSTRA_MONITOR           VARCHAR2(1 BYTE),
  QT_TEMPO_ESPERA             NUMBER(10),
  IE_PERM_TRANSF_SEM_ATEND    VARCHAR2(1 BYTE),
  QT_TEMPO_ATENDIMENTO        NUMBER(10),
  IE_PREFERENCIAL             VARCHAR2(1 BYTE),
  IE_NOVO_ATEND_AUTOMATICO    VARCHAR2(1 BYTE),
  IE_EXIGE_MOT_FINALIZACAO    VARCHAR2(1 BYTE),
  QT_SIMULTANEAS              NUMBER(5),
  DS_COR_HTML5                VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FILESPS_CLASSFL_FK_I ON TASY.FILA_ESPERA_SENHA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILESPS_CLASSFL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FILESPS_ESTABEL_FK_I ON TASY.FILA_ESPERA_SENHA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FILESPS_FILESPS_FK_I ON TASY.FILA_ESPERA_SENHA
(NR_SEQ_FILA_ESPERA_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILESPS_FILESPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FILESPS_PERFIL_FK_I ON TASY.FILA_ESPERA_SENHA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILESPS_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FILESPS_PK ON TASY.FILA_ESPERA_SENHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FILA_ESPERA_SENHA ADD (
  CONSTRAINT FILESPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FILA_ESPERA_SENHA ADD (
  CONSTRAINT FILESPS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FILESPS_CLASSFL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_FILA (NR_SEQUENCIA),
  CONSTRAINT FILESPS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT FILESPS_FILESPS_FK 
 FOREIGN KEY (NR_SEQ_FILA_ESPERA_DESTINO) 
 REFERENCES TASY.FILA_ESPERA_SENHA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FILA_ESPERA_SENHA TO NIVEL_1;

