ALTER TABLE TASY.AUTORIZACAO_CONVENIO_TISS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONVENIO_TISS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONVENIO_TISS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQUENCIA_AUTOR       NUMBER(10)           NOT NULL,
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  CD_CATEGORIA             VARCHAR2(10 BYTE)    NOT NULL,
  CD_USUARIO_CONVENIO      VARCHAR2(30 BYTE)    NOT NULL,
  CD_PLANO_CONVENIO        VARCHAR2(10 BYTE)    NOT NULL,
  DT_VALIDADE_CARTEIRA     DATE,
  IE_COD_USUARIO_MAE_RESP  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUCTISS_AUTCONV_FK_I ON TASY.AUTORIZACAO_CONVENIO_TISS
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUCTISS_CATCONV_FK_I ON TASY.AUTORIZACAO_CONVENIO_TISS
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCTISS_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUCTISS_CONPLAN_FK_I ON TASY.AUTORIZACAO_CONVENIO_TISS
(CD_CONVENIO, CD_PLANO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCTISS_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUCTISS_CONVENI_FK_I ON TASY.AUTORIZACAO_CONVENIO_TISS
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCTISS_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUCTISS_ESTABEL_FK_I ON TASY.AUTORIZACAO_CONVENIO_TISS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCTISS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUCTISS_PK ON TASY.AUTORIZACAO_CONVENIO_TISS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCTISS_PK
  MONITORING USAGE;


ALTER TABLE TASY.AUTORIZACAO_CONVENIO_TISS ADD (
  CONSTRAINT AUCTISS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONVENIO_TISS ADD (
  CONSTRAINT AUCTISS_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUCTISS_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AUCTISS_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO_CONVENIO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT AUCTISS_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AUCTISS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.AUTORIZACAO_CONVENIO_TISS TO NIVEL_1;

