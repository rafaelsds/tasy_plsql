ALTER TABLE TASY.CONVENIO_AMB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_AMB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_AMB
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  CD_CATEGORIA         VARCHAR2(10 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  IE_PRIORIDADE        NUMBER(10)               NOT NULL,
  CD_EDICAO_AMB        NUMBER(6)                NOT NULL,
  TX_AJUSTE_GERAL      NUMBER(15,4)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_CH_HONORARIOS     NUMBER(15,4),
  VL_CH_CUSTO_OPER     NUMBER(15,4),
  VL_FILME             NUMBER(15,4),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  DT_FINAL_VIGENCIA    DATE,
  NR_SEQ_TISS_TABELA   NUMBER(10),
  NR_SEQ_CBHPM_EDICAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONAMB_CATCONV_FK_I ON TASY.CONVENIO_AMB
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONAMB_EDIAMB_FK_I ON TASY.CONVENIO_AMB
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONAMB_ESTABEL_FK_I ON TASY.CONVENIO_AMB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONAMB_PK ON TASY.CONVENIO_AMB
(CD_ESTABELECIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, IE_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONAMB_TISSTTA_FK_I ON TASY.CONVENIO_AMB
(NR_SEQ_TISS_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CONVENIO_AMB_tp  after update ON TASY.CONVENIO_AMB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO)||'#@#@CD_CONVENIO='||to_char(:old.CD_CONVENIO)||'#@#@CD_CATEGORIA='||to_char(:old.CD_CATEGORIA)||'#@#@DT_INICIO_VIGENCIA='||to_char(:old.DT_INICIO_VIGENCIA)||'#@#@IE_PRIORIDADE='||to_char(:old.IE_PRIORIDADE);gravar_log_alteracao(substr(:old.NR_SEQ_TISS_TABELA,1,4000),substr(:new.NR_SEQ_TISS_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TISS_TABELA',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIORIDADE,1,4000),substr(:new.IE_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBHPM_EDICAO,1,4000),substr(:new.NR_SEQ_CBHPM_EDICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBHPM_EDICAO',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_HONORARIOS,1,4000),substr(:new.VL_CH_HONORARIOS,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_HONORARIOS',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_GERAL,1,4000),substr(:new.TX_AJUSTE_GERAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_GERAL',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_CUSTO_OPER,1,4000),substr(:new.VL_CH_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_CUSTO_OPER',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FINAL_VIGENCIA,1,4000),substr(:new.DT_FINAL_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CONVENIO_AMB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CONVENIO_AMB ADD (
  CONSTRAINT CONAMB_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, IE_PRIORIDADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_AMB ADD (
  CONSTRAINT CONAMB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONAMB_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT CONAMB_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA)
    ON DELETE CASCADE,
  CONSTRAINT CONAMB_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TABELA) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_AMB TO NIVEL_1;

