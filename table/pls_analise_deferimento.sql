ALTER TABLE TASY.PLS_ANALISE_DEFERIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ANALISE_DEFERIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ANALISE_DEFERIMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SOLICITACAO   NUMBER(10)               NOT NULL,
  DT_DEFERIMENTO       DATE                     NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_DEFERIMENTO       VARCHAR2(2000 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSADEF_PESFISI_FK_I ON TASY.PLS_ANALISE_DEFERIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSADEF_PK ON TASY.PLS_ANALISE_DEFERIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSADEF_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSADEF_PLSASOL_FK_I ON TASY.PLS_ANALISE_DEFERIMENTO
(NR_SEQ_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSADEF_PLSASOL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ANALISE_DEFERIMENTO ADD (
  CONSTRAINT PLSADEF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ANALISE_DEFERIMENTO ADD (
  CONSTRAINT PLSADEF_PLSASOL_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO) 
 REFERENCES TASY.PLS_ANALISE_SOLICITACAO (NR_SEQUENCIA),
  CONSTRAINT PLSADEF_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PLS_ANALISE_DEFERIMENTO TO NIVEL_1;

