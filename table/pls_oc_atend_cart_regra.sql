ALTER TABLE TASY.PLS_OC_ATEND_CART_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_ATEND_CART_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_ATEND_CART_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_POS_INICIO        NUMBER(10),
  QT_POS_FINAL         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOACR_PK ON TASY.PLS_OC_ATEND_CART_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOACR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOACR_PLSOACA_FK_I ON TASY.PLS_OC_ATEND_CART_REGRA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOACR_PLSOACA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_ATEND_CART_REGRA_tp  after update ON TASY.PLS_OC_ATEND_CART_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'PLS_OC_ATEND_CART_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_POS_FINAL,1,4000),substr(:new.QT_POS_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_POS_FINAL',ie_log_w,ds_w,'PLS_OC_ATEND_CART_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_POS_INICIO,1,4000),substr(:new.QT_POS_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_POS_INICIO',ie_log_w,ds_w,'PLS_OC_ATEND_CART_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_ATEND_CART_REGRA ADD (
  CONSTRAINT PLSOACR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_ATEND_CART_REGRA ADD (
  CONSTRAINT PLSOACR_PLSOACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_OC_ATEND_CARTEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_ATEND_CART_REGRA TO NIVEL_1;

