ALTER TABLE TASY.UNIDADE_ATEND_PREV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_ATEND_PREV CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_ATEND_PREV
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_EVENTO             VARCHAR2(3 BYTE)        NOT NULL,
  NR_SEQ_LOCALIZACAO    NUMBER(10)              NOT NULL,
  NR_SEQ_EQUIP          NUMBER(10)              NOT NULL,
  NR_SEQ_PLANEJ         NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE)       NOT NULL,
  CD_UNIDADE_COMPL      VARCHAR2(10 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UNIATPR_MANEQUI_FK_I ON TASY.UNIDADE_ATEND_PREV
(NR_SEQ_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATPR_MANLOCA_FK_I ON TASY.UNIDADE_ATEND_PREV
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATPR_MANLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATPR_MANPLPR_FK_I ON TASY.UNIDADE_ATEND_PREV
(NR_SEQ_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNIATPR_PK ON TASY.UNIDADE_ATEND_PREV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATPR_UNIATEN_FK_I ON TASY.UNIDADE_ATEND_PREV
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATPR_UNIATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.UNIDADE_ATEND_PREV ADD (
  CONSTRAINT UNIATPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNIDADE_ATEND_PREV ADD (
  CONSTRAINT UNIATPR_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIP) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT UNIATPR_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL)
    ON DELETE CASCADE,
  CONSTRAINT UNIATPR_MANPLPR_FK 
 FOREIGN KEY (NR_SEQ_PLANEJ) 
 REFERENCES TASY.MAN_PLANEJ_PREV (NR_SEQUENCIA),
  CONSTRAINT UNIATPR_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.UNIDADE_ATEND_PREV TO NIVEL_1;

