ALTER TABLE TASY.PLS_REGRA_DIV_FAT_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_DIV_FAT_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_DIV_FAT_BENEF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  IE_BENEF_REMIDO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRDFB_ESTABEL_FK_I ON TASY.PLS_REGRA_DIV_FAT_BENEF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRDFB_PK ON TASY.PLS_REGRA_DIV_FAT_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_DIV_FAT_BENEF ADD (
  CONSTRAINT PLSRDFB_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_REGRA_DIV_FAT_BENEF ADD (
  CONSTRAINT PLSRDFB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_DIV_FAT_BENEF TO NIVEL_1;

