ALTER TABLE TASY.SUP_INT_REGRA_OC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_REGRA_OC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_REGRA_OC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTEGRACAO    NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_LOCAL_ENTREGA     NUMBER(4),
  IE_LIB_APROV         VARCHAR2(1 BYTE),
  IE_ENVIA_SEMPRE      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUINROC_LOCESTO_FK_I ON TASY.SUP_INT_REGRA_OC
(CD_LOCAL_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINROC_LOCESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUINROC_PK ON TASY.SUP_INT_REGRA_OC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINROC_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUINROC_SUPAINT_FK_I ON TASY.SUP_INT_REGRA_OC
(NR_SEQ_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_INT_REGRA_OC_tp  after update ON TASY.SUP_INT_REGRA_OC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_INT_REGRA_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIB_APROV,1,4000),substr(:new.IE_LIB_APROV,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIB_APROV',ie_log_w,ds_w,'SUP_INT_REGRA_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ENTREGA,1,4000),substr(:new.CD_LOCAL_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ENTREGA',ie_log_w,ds_w,'SUP_INT_REGRA_OC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_INT_REGRA_OC ADD (
  CONSTRAINT SUINROC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_INT_REGRA_OC ADD (
  CONSTRAINT SUINROC_SUPAINT_FK 
 FOREIGN KEY (NR_SEQ_INTEGRACAO) 
 REFERENCES TASY.SUP_PARAMETRO_INTEGRACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUINROC_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ENTREGA) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE));

GRANT SELECT ON TASY.SUP_INT_REGRA_OC TO NIVEL_1;

