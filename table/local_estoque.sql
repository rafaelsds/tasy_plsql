ALTER TABLE TASY.LOCAL_ESTOQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOCAL_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOCAL_ESTOQUE
(
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  CD_LOCAL_ESTOQUE             NUMBER(4)        NOT NULL,
  DS_LOCAL_ESTOQUE             VARCHAR2(40 BYTE) NOT NULL,
  IE_PERMITE_DIGITACAO         VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_CENTRO_CUSTO              NUMBER(8),
  IE_TIPO_LOCAL                VARCHAR2(5 BYTE),
  IE_PONTO_PEDIDO              VARCHAR2(1 BYTE),
  IE_REQ_AUTOMATICA            VARCHAR2(1 BYTE),
  IE_PROPRIO                   VARCHAR2(1 BYTE) NOT NULL,
  CD_COMPRADOR_CONSIG          VARCHAR2(10 BYTE),
  CD_PESSOA_SOLIC_CONSIG       VARCHAR2(10 BYTE),
  IE_REQ_MAT_ESTOQUE           VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_CENTRO_INVENTARIO         VARCHAR2(1 BYTE) NOT NULL,
  IE_BAIXA_DISP                VARCHAR2(1 BYTE) NOT NULL,
  QT_DIA_AJUSTE_MINIMO         NUMBER(5),
  QT_DIA_AJUSTE_MAXIMO         NUMBER(5),
  QT_DIA_CONSUMO               NUMBER(5),
  IE_PERMITE_ESTOQUE_NEGATIVO  VARCHAR2(15 BYTE) NOT NULL,
  IE_CENTRO_CUSTO_ORDEM        VARCHAR2(15 BYTE) NOT NULL,
  IE_LOCAL_ENTREGA_OC          VARCHAR2(1 BYTE) NOT NULL,
  IE_EXTERNO                   VARCHAR2(1 BYTE) NOT NULL,
  IE_GERA_LOTE                 VARCHAR2(1 BYTE),
  IE_CONSIDERA_TRANSF_PADRAO   VARCHAR2(1 BYTE),
  IE_CENTRO_CUSTO_NF           VARCHAR2(15 BYTE) NOT NULL,
  IE_PERMITE_EMPRESTIMO        VARCHAR2(1 BYTE),
  IE_LIMPA_REQUISICAO          VARCHAR2(1 BYTE),
  DS_COMPLEMENTO               VARCHAR2(255 BYTE),
  IE_CONSISTE_SALDO_REP        VARCHAR2(1 BYTE),
  IE_PERMITE_OC                VARCHAR2(1 BYTE),
  IE_CENTRO_PERDA              VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_LOCAL_EXT            VARCHAR2(1 BYTE),
  CD_CNS                       VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.CD_ESTABELECIMENTO IS 'Codigo da Filial.';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.CD_LOCAL_ESTOQUE IS 'Codigo do Local de Estoque.';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.DS_LOCAL_ESTOQUE IS 'Descricao do Local de Estoque';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.IE_PERMITE_DIGITACAO IS 'Indicador se permite digitacao de movimento para o local';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.IE_SITUACAO IS 'Indicador da Situacao do Local';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.DT_ATUALIZACAO IS 'Data de Atualizacao';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.NM_USUARIO IS 'Nome do Usuario';

COMMENT ON COLUMN TASY.LOCAL_ESTOQUE.CD_CENTRO_CUSTO IS 'Codigo do Centro de Custo';


CREATE INDEX TASY.LOCESTO_CENCUST_FK_I ON TASY.LOCAL_ESTOQUE
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOCESTO_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOCESTO_COMPRAD_FK_I ON TASY.LOCAL_ESTOQUE
(CD_COMPRADOR_CONSIG, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOCESTO_ESTABEL_FK_I ON TASY.LOCAL_ESTOQUE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOCESTO_I1 ON TASY.LOCAL_ESTOQUE
(CD_ESTABELECIMENTO, IE_SITUACAO, IE_LOCAL_ENTREGA_OC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOCESTO_PESFISI_FK_I ON TASY.LOCAL_ESTOQUE
(CD_PESSOA_SOLIC_CONSIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOCESTO_PK ON TASY.LOCAL_ESTOQUE
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.dankia_local_estoque_insert
after insert or update ON TASY.LOCAL_ESTOQUE for each row
begin
if	(inserting) then
	gerar_int_dankia_pck.dankia_local_estoque(:new.cd_local_estoque,:new.ie_tipo_local,:new.ds_local_estoque,'I',:new.nm_usuario);
elsif	(updating) then
	if	(:old.ie_tipo_local = '11') or
		(:new.ie_tipo_local = '11') then
		if	(nvl(:new.ie_tipo_local,'X') <> 'X') and
			(nvl(:old.ie_tipo_local,'X') <> nvl(:new.ie_tipo_local,'X')) then
			if	(:new.ie_tipo_local = '11') then
				gerar_int_dankia_pck.dankia_local_estoque(:old.cd_local_estoque,:new.ie_tipo_local, nvl(:new.ds_local_estoque,:old.ds_local_estoque),'I',:new.nm_usuario);
			else
				gerar_int_dankia_pck.dankia_local_estoque(:old.cd_local_estoque,:old.ie_tipo_local, :old.ds_local_estoque,'E', :new.nm_usuario);
			end if;
		elsif	(nvl(:new.ie_tipo_local,'X') = 'X') and
				(nvl(:old.ie_tipo_local,'X') = '11') then
				gerar_int_dankia_pck.dankia_local_estoque(:old.cd_local_estoque,:old.ie_tipo_local, :old.ds_local_estoque,'E', :new.nm_usuario);

		elsif	(nvl(:new.ds_local_estoque,'X') <> 'X') and
				(nvl(:old.ds_local_estoque,'X') <> nvl(:new.ds_local_estoque,'X')) then
			gerar_int_dankia_pck.dankia_local_estoque(:old.cd_local_estoque,:new.ie_tipo_local, :new.ds_local_estoque,'A',:new.nm_usuario);
		end if;
	end if;
end if;

end dankia_local_estoque_insert;
/


CREATE OR REPLACE TRIGGER TASY.LOCAL_ESTOQUE_tp  after update ON TASY.LOCAL_ESTOQUE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_LOCAL_ESTOQUE);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PERMITE_OC,1,4000),substr(:new.IE_PERMITE_OC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_OC',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_SALDO_REP,1,4000),substr(:new.IE_CONSISTE_SALDO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_SALDO_REP',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOCAL_ESTOQUE,1,4000),substr(:new.DS_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOCAL_ESTOQUE',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_DIGITACAO,1,4000),substr(:new.IE_PERMITE_DIGITACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_DIGITACAO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PONTO_PEDIDO,1,4000),substr(:new.IE_PONTO_PEDIDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PONTO_PEDIDO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROPRIO,1,4000),substr(:new.IE_PROPRIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROPRIO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_LOCAL,1,4000),substr(:new.IE_TIPO_LOCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_LOCAL',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQ_AUTOMATICA,1,4000),substr(:new.IE_REQ_AUTOMATICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQ_AUTOMATICA',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_SOLIC_CONSIG,1,4000),substr(:new.CD_PESSOA_SOLIC_CONSIG,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SOLIC_CONSIG',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_COMPRADOR_CONSIG,1,4000),substr(:new.CD_COMPRADOR_CONSIG,1,4000),:new.nm_usuario,nr_seq_w,'CD_COMPRADOR_CONSIG',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQ_MAT_ESTOQUE,1,4000),substr(:new.IE_REQ_MAT_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQ_MAT_ESTOQUE',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_INVENTARIO,1,4000),substr(:new.IE_CENTRO_INVENTARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_INVENTARIO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_DISP,1,4000),substr(:new.IE_BAIXA_DISP,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_DISP',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_AJUSTE_MINIMO,1,4000),substr(:new.QT_DIA_AJUSTE_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_AJUSTE_MINIMO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_AJUSTE_MAXIMO,1,4000),substr(:new.QT_DIA_AJUSTE_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_AJUSTE_MAXIMO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_CONSUMO,1,4000),substr(:new.QT_DIA_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_CONSUMO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_ESTOQUE_NEGATIVO,1,4000),substr(:new.IE_PERMITE_ESTOQUE_NEGATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_ESTOQUE_NEGATIVO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_CUSTO_ORDEM,1,4000),substr(:new.IE_CENTRO_CUSTO_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_CUSTO_ORDEM',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOCAL_ENTREGA_OC,1,4000),substr(:new.IE_LOCAL_ENTREGA_OC,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOCAL_ENTREGA_OC',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXTERNO,1,4000),substr(:new.IE_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXTERNO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_LOTE,1,4000),substr(:new.IE_GERA_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_LOTE',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_TRANSF_PADRAO,1,4000),substr(:new.IE_CONSIDERA_TRANSF_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_TRANSF_PADRAO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_CUSTO_NF,1,4000),substr(:new.IE_CENTRO_CUSTO_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_CUSTO_NF',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_EMPRESTIMO,1,4000),substr(:new.IE_PERMITE_EMPRESTIMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_EMPRESTIMO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIMPA_REQUISICAO,1,4000),substr(:new.IE_LIMPA_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIMPA_REQUISICAO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'LOCAL_ESTOQUE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.local_estoque_after
after insert or update or delete ON TASY.LOCAL_ESTOQUE for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin

if	(inserting) then
	reg_integracao_p.ie_operacao	:=	'I';
elsif	(updating) then
	reg_integracao_p.ie_operacao	:=	'A';
elsif	(deleting) then
	reg_integracao_p.ie_operacao	:=	'E';
end if;

reg_integracao_p.cd_estab_documento	:=	nvl(:new.cd_estabelecimento, :old.cd_estabelecimento);
reg_integracao_p.ie_tipo_local	:=	nvl(:new.ie_tipo_local, :old.ie_tipo_local);

gerar_int_padrao.gravar_integracao('8', nvl(:new.cd_local_estoque, :old.cd_local_estoque), nvl(:new.nm_usuario, :old.nm_usuario), reg_integracao_p);

end local_estoque_after;
/


ALTER TABLE TASY.LOCAL_ESTOQUE ADD (
  CHECK ( ie_situacao IN ( 'A' , 'I' )  ),
  CONSTRAINT LOCESTO_PK
 PRIMARY KEY
 (CD_LOCAL_ESTOQUE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOCAL_ESTOQUE ADD (
  CONSTRAINT LOCESTO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT LOCESTO_COMPRAD_FK 
 FOREIGN KEY (CD_COMPRADOR_CONSIG, CD_ESTABELECIMENTO) 
 REFERENCES TASY.COMPRADOR (CD_PESSOA_FISICA,CD_ESTABELECIMENTO),
  CONSTRAINT LOCESTO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_SOLIC_CONSIG) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LOCESTO_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.LOCAL_ESTOQUE TO NIVEL_1;

