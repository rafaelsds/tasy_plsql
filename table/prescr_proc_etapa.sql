ALTER TABLE TASY.PRESCR_PROC_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_ETAPA
(
  NR_PRESCRICAO             NUMBER(14)          NOT NULL,
  NR_SEQ_PRESCRICAO         NUMBER(6)           NOT NULL,
  IE_ETAPA                  NUMBER(2)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_EXAME_BLOQUEADO        VARCHAR2(1 BYTE),
  IE_AMOSTRA                VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_ALT_STATUS  NUMBER(10),
  DS_JUSTIFICATIVA_ALT      VARCHAR2(255 BYTE),
  DS_CALL_STACK             VARCHAR2(4000 BYTE),
  NR_SEQ_FUNCAO             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          176M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRPROET_I1 ON TASY.PRESCR_PROC_ETAPA
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO, IE_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          136M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPROET_I2 ON TASY.PRESCR_PROC_ETAPA
(DT_ATUALIZACAO, IE_ETAPA, NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRPROET_PK ON TASY.PRESCR_PROC_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          111M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPROET_PRESPRO_FK_I ON TASY.PRESCR_PROC_ETAPA
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          133M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_prescr_proc_estapa_upd
after insert ON TASY.PRESCR_PROC_ETAPA 
for each row
declare

jobno    number;

begin

    if :new.ie_etapa = 12 then -- Caso o status seja alterado para recoleta, ser� automaticamente alterado pelo sistema para "Solicita��o Liberada", assim a enfermagem ter� acesso
        dbms_job.submit(jobno, 'update prescr_procedimento set ie_status_atend = 10 where nr_prescricao = '||:new.nr_prescricao ||'and nr_sequencia = '||:new.nr_seq_prescricao||';');
    end if;

exception
when others then
    enviar_email('Exception_prescr_laboratorio','Verificar trigger hsj_prescr_proc_estapa_upd, erro na prescri��o '||:new.nr_prescricao,'noreply@hsjose.com.br','sistemas@hsjose.com.br','RAFAELSDS','A');

end;
/


ALTER TABLE TASY.PRESCR_PROC_ETAPA ADD (
  CONSTRAINT PRPROET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          111M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_ETAPA ADD (
  CONSTRAINT PRPROET_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_PROC_ETAPA TO NIVEL_1;

