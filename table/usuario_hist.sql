ALTER TABLE TASY.USUARIO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_HIST
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  NM_USUARIO_REF  VARCHAR2(15 BYTE)             NOT NULL,
  DS_ALTERACAO    VARCHAR2(2000 BYTE)           NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  DS_OPERACAO     VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.USUHIST_PK ON TASY.USUARIO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUHIST_USUARIO_FK_I ON TASY.USUARIO_HIST
(NM_USUARIO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUHIST_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.USUARIO_HIST ADD (
  CONSTRAINT USUHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_HIST ADD (
  CONSTRAINT USUHIST_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REF) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.USUARIO_HIST TO NIVEL_1;

