ALTER TABLE TASY.SAN_DERIVADO_ESTOQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_DERIVADO_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_DERIVADO_ESTOQUE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DERIVADO      NUMBER(10)               NOT NULL,
  IE_FATOR_RH          VARCHAR2(1 BYTE),
  IE_TIPO_SANGUINEO    VARCHAR2(2 BYTE),
  QT_ESTOQUE_MINIMO    NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANDEES_PK ON TASY.SAN_DERIVADO_ESTOQUE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDEES_SANDERI_FK_I ON TASY.SAN_DERIVADO_ESTOQUE
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDEES_SANDERI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_DERIVADO_ESTOQUE ADD (
  CONSTRAINT SANDEES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_DERIVADO_ESTOQUE ADD (
  CONSTRAINT SANDEES_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_DERIVADO_ESTOQUE TO NIVEL_1;

