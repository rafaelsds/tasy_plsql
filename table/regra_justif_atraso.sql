ALTER TABLE TASY.REGRA_JUSTIF_ATRASO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_JUSTIF_ATRASO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_JUSTIF_ATRASO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  HR_INICIO_TRATAMENTO  DATE                    NOT NULL,
  HR_FIM_TRATAMENTO     DATE                    NOT NULL,
  IE_DIA_SEGUINTE       VARCHAR2(1 BYTE),
  HR_INICIO_LIBERACAO   DATE                    NOT NULL,
  HR_FIM_LIBERACAO      DATE                    NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REJUATR_ESTABEL_FK_I ON TASY.REGRA_JUSTIF_ATRASO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REJUATR_PK ON TASY.REGRA_JUSTIF_ATRASO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUATR_PK
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_JUSTIF_ATRASO ADD (
  CONSTRAINT REJUATR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_JUSTIF_ATRASO ADD (
  CONSTRAINT REJUATR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_JUSTIF_ATRASO TO NIVEL_1;

