ALTER TABLE TASY.PLS_AP_REGRA_CARTEIRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AP_REGRA_CARTEIRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AP_REGRA_CARTEIRA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_AUT_PROCESSO         NUMBER(10)        NOT NULL,
  IE_FORMA_GERACAO            VARCHAR2(5 BYTE)  NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  IE_TIPO_LOTE                VARCHAR2(1 BYTE)  NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_TIPO_DATA                VARCHAR2(2 BYTE),
  HR_EXECUCAO                 DATE,
  NR_DIA_GERACAO              NUMBER(2),
  IE_TIPO_SOLICITACAO         VARCHAR2(2 BYTE),
  IE_TIPO_CONTRATO            VARCHAR2(2 BYTE),
  IE_TIPO_BENEFICIARIO        VARCHAR2(10 BYTE),
  IE_TIPO_PESSOA              VARCHAR2(2 BYTE),
  IE_TIPO_PAGADOR             VARCHAR2(2 BYTE),
  DS_OBSERVACAO               VARCHAR2(2000 BYTE),
  NR_VIA_INICIAL              NUMBER(5),
  NR_VIA_FINAL                NUMBER(5),
  NR_SEQ_DESTINO_CORRESP      NUMBER(10),
  IE_DESTINO_CORRESPONDENCIA  VARCHAR2(1 BYTE),
  NR_JOB                      NUMBER(15),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSARCA_ESTABEL_FK_I ON TASY.PLS_AP_REGRA_CARTEIRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSARCA_PK ON TASY.PLS_AP_REGRA_CARTEIRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSARCA_PLSATPR_FK_I ON TASY.PLS_AP_REGRA_CARTEIRA
(NR_SEQ_AUT_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSARCA_PLSDSTC_FK_I ON TASY.PLS_AP_REGRA_CARTEIRA
(NR_SEQ_DESTINO_CORRESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ap_regra_carteira_atual
before update or insert or delete ON TASY.PLS_AP_REGRA_CARTEIRA for each row
declare

ie_acao_w		varchar2(255);
nr_mes_w		pls_integer := 0;
dt_execucao_w		date;
dt_proxima_exec_w	varchar2(255);
hr_exec_w		varchar2(10);
nr_job_w		pls_ap_regra_carteira.nr_job%type;

begin
nr_job_w	:= :old.nr_job;

if	(inserting) then
	ie_acao_w := 'I';
elsif	(updating) then
	ie_acao_w := 'U';
elsif	(deleting) then
	ie_acao_w := 'D';
end if;

if	(:new.hr_execucao is not null) then
	hr_exec_w := to_char(:new.hr_execucao, 'hh24:mi:ss');
else
	hr_exec_w := to_char((fim_dia(sysdate) - 1/3072), 'hh24:mi:ss');
end if;

if	(:new.ie_forma_geracao = 'M') then
	if	(:new.nr_dia_geracao is null) and
		(ie_acao_w in ('I','U')) then
		wheb_mensagem_pck.exibir_mensagem_abort(1077327);
	end if;

	if	(((trunc(sysdate,'month') + :new.nr_dia_geracao) - 1) < trunc(sysdate)) then
		nr_mes_w := + 1;
	end if;

	dt_execucao_w		:= to_date(to_char(to_char(add_months(((trunc(sysdate,'month') + :new.nr_dia_geracao) - 1),nr_mes_w),'dd/mm/yyyy') || ' ' || hr_exec_w), 'dd/mm/yyyy hh24:mi:ss');
	dt_proxima_exec_w	:= 'add_months(to_date(to_char(to_char(add_months(((trunc(sysdate,''month'') + ' || :new.nr_dia_geracao || ') - 1),' || nr_mes_w || '),''dd/mm/yyyy'') || '' '' || ''' || hr_exec_w || '''), ''dd/mm/yyyy hh24:mi:ss''),1)';
elsif	(:new.ie_forma_geracao = 'D') then
	if	(:new.nr_dia_geracao is not null) and
		(ie_acao_w in ('I','U')) then
		:new.nr_dia_geracao	:= null;
	end if;

	dt_execucao_w		:= to_date((to_char(sysdate,'dd/mm/yyyy') || ' ' || hr_exec_w),'dd/mm/yyyy hh24:mi:ss');
	dt_proxima_exec_w	:= 'to_date((to_char(sysdate,''dd/mm/yyyy'') || '' '' || ''' || hr_exec_w || '''),''dd/mm/yyyy hh24:mi:ss'') + 1';
end if;

pls_criar_job_automatizacao(	ie_acao_w,
				nr_job_w,
				'PLS_PA_LOTE_CARTEIRA('''|| to_char(:old.nr_sequencia) || ''', ''' || :new.nm_usuario_nrec || ''');',
				dt_execucao_w,
				dt_proxima_exec_w,
				:new.ie_situacao);

if	(ie_acao_w in ('I','U')) then
	:new.nr_job	:= nr_job_w;
end if;

end;
/


ALTER TABLE TASY.PLS_AP_REGRA_CARTEIRA ADD (
  CONSTRAINT PLSARCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_AP_REGRA_CARTEIRA ADD (
  CONSTRAINT PLSARCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSARCA_PLSATPR_FK 
 FOREIGN KEY (NR_SEQ_AUT_PROCESSO) 
 REFERENCES TASY.PLS_AUTOMATIZACAO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT PLSARCA_PLSDSTC_FK 
 FOREIGN KEY (NR_SEQ_DESTINO_CORRESP) 
 REFERENCES TASY.PLS_DESTINO_CORRESP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AP_REGRA_CARTEIRA TO NIVEL_1;

