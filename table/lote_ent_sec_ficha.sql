ALTER TABLE TASY.LOTE_ENT_SEC_FICHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENT_SEC_FICHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENT_SEC_FICHA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_LOTE_SEC           NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_PRONTUARIO_EX          VARCHAR2(20 BYTE),
  CD_USUARIO_CONV_EXT       VARCHAR2(30 BYTE),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  NR_FICHA_EXT              VARCHAR2(20 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  IE_SUSP_AM_INAD           VARCHAR2(1 BYTE),
  CD_BARRAS                 VARCHAR2(100 BYTE),
  NR_CARTAO_NAC_SUS         VARCHAR2(20 BYTE),
  IE_FICHA_ADICIONAL        VARCHAR2(1 BYTE)    NOT NULL,
  CD_ENTIDADE_F             VARCHAR2(5 BYTE),
  CD_LOTE_F                 VARCHAR2(10 BYTE),
  CD_EXAME_F                VARCHAR2(2 BYTE),
  IE_PREMATURO_F            VARCHAR2(1 BYTE),
  IE_TRANSFUSAO_F           VARCHAR2(1 BYTE),
  DT_COLETA_F               DATE,
  IE_TIPO_TESTE_F           VARCHAR2(1 BYTE),
  DS_REPETICAO_EXAME_F      VARCHAR2(60 BYTE),
  CD_ENTRADA_F              VARCHAR2(5 BYTE),
  CD_EXAME_FICHA_F          VARCHAR2(2 BYTE),
  CD_PRONTUARIO_F           VARCHAR2(10 BYTE),
  NM_RN_F                   VARCHAR2(29 BYTE),
  CD_DNV_F                  VARCHAR2(11 BYTE),
  DT_COLETA_FICHA_F         DATE,
  HR_COLETA_F               DATE,
  IE_PERIODO_COL_F          VARCHAR2(1 BYTE),
  DT_NASCIMENTO_F           DATE,
  HR_NASCIMENTO_F           DATE,
  IE_PERIODO_NASC_F         VARCHAR2(1 BYTE),
  QT_PESO_F                 NUMBER(4),
  IE_SEXO_F                 VARCHAR2(1 BYTE),
  IE_COR_PF_F               VARCHAR2(10 BYTE),
  IE_NPP_F                  VARCHAR2(1 BYTE),
  IE_PREMAT_S_F             VARCHAR2(1 BYTE),
  NR_IDADE_GEST_F           NUMBER(2),
  IE_PREMAT_N_F             VARCHAR2(1 BYTE),
  IE_TRANSF_S_F             VARCHAR2(1 BYTE),
  DT_TRANSF_F               DATE,
  IE_TRANSF_N_F             VARCHAR2(1 BYTE),
  IE_GEMELAR_F              VARCHAR2(1 BYTE),
  IE_AMOSTRA_INADEQUADA     VARCHAR2(1 BYTE),
  CD_MEDICO_RESP            VARCHAR2(10 BYTE),
  IE_TIPO_FICHA             VARCHAR2(5 BYTE),
  CD_MATERIAL_EXAME         VARCHAR2(20 BYTE),
  IE_STATUS_FICHA           VARCHAR2(1 BYTE),
  NR_SEQ_MOT_INADEQ         NUMBER(10),
  DS_MOTIVO_INADEQ          VARCHAR2(255 BYTE),
  IE_BARRA_GERADO           VARCHAR2(1 BYTE),
  NM_MAE_F                  VARCHAR2(60 BYTE),
  NR_ATENDIMENTO            NUMBER(10),
  NR_PRESCRICAO             NUMBER(14),
  NR_SEQ_TIPO_MAT_LOTE      NUMBER(10),
  IE_AMOSTRA                VARCHAR2(1 BYTE),
  DT_VALIDADE               DATE,
  NR_SEQ_GRAU_PARENTESCO    NUMBER(10),
  DT_FABRICACAO             DATE,
  NR_LOTE_FICHA             VARCHAR2(10 BYTE),
  CD_PESSOA_EXTERNA         VARCHAR2(10 BYTE),
  CD_MOTIVO_BAIXA           NUMBER(3),
  IE_SUSPENSO               VARCHAR2(1 BYTE),
  IE_CORTICOIDE_F           VARCHAR2(1 BYTE),
  DS_CORTICOIDE_F           VARCHAR2(100 BYTE),
  IE_FICHA_URG              VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL              NUMBER(10),
  NR_SEQ_LOCAL_CAIXA        NUMBER(10),
  NR_SEQ_FICHA_ANT          NUMBER(10),
  IE_AMAMENTADO_F           VARCHAR2(1 BYTE),
  IE_ALIM_LEITE_F           VARCHAR2(1 BYTE),
  IE_ICTERICIA_F            VARCHAR2(1 BYTE),
  IE_MAE_VEG_F              VARCHAR2(1 BYTE),
  QT_GEST_F                 NUMBER(2),
  DT_ULT_MENST              DATE,
  IE_TIPO_PARTO             VARCHAR2(1 BYTE),
  IE_ABORTO                 VARCHAR2(1 BYTE),
  QT_ABORTO                 NUMBER(3),
  NR_SISPRENATAL            NUMBER(12),
  DS_UNIDADE_BASICA_ORIGEM  VARCHAR2(255 BYTE),
  NR_DDI                    VARCHAR2(3 BYTE),
  NR_DDI_CELULAR            VARCHAR2(3 BYTE),
  NR_SEQ_MOTIVO_NAO_ELUIDO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LESECFI_ATEPACI_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LESECFI_GRAUPA_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_GRAU_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_GRAUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_LELOCAL_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_LELOCAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_LELOCAX_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_LOCAL_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_LELOCAX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_LEMOIN_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_MOT_INADEQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_LEMOIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_LESEC_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_LOTE_SEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_LESEC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_LOTEENTMAT_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQ_TIPO_MAT_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_LOTEENTMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_MATEXLA_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESECFI_MATEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESECFI_PESFISI_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LESECFI_PESFISI_FK2_I ON TASY.LOTE_ENT_SEC_FICHA
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LESECFI_PK ON TASY.LOTE_ENT_SEC_FICHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LESECFI_PRESMED_FK_I ON TASY.LOTE_ENT_SEC_FICHA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lote_ent_sec_ficha_update
before update ON TASY.LOTE_ENT_SEC_FICHA for each row
declare

--pragma autonomous_transaction;

nr_sequencia_w      lote_ent_sec_ficha_log.nr_sequencia%type;
nr_seq_lote_ent_w    lote_ent_secretaria.nr_sequencia%type;
nr_seq_evento_w      ev_evento.nr_sequencia%type;
nr_lote_w      lote_ent_secretaria.nr_lote%type;
ds_retorno_w      varchar2(4000);
nr_seq_tipo_ocorr_w    varchar2(255);
nr_seq_ocorrencia_w    number(10);
nr_seq_exame_prescr_w    prescr_procedimento.nr_seq_exame%type;
nr_seq_material_prescr_w  material_exame_lab.nr_Sequencia%type;
nr_seq_prescr_w               exame_lab_result_item.nr_seq_prescr%type;
nr_Seq_resultado_w            exame_lab_result_item.nr_Seq_resultado%type;
nr_prescricao_w               prescr_procedimento.nr_prescricao%type;
qt_resultado_w                exame_lab_result_item.qt_resultado%type;
pr_resultado_w                exame_lab_result_item.pr_resultado%type;
ds_resultado_w            exame_lab_result_item.ds_resultado%type;
dt_aprovacao_exame_pai_w      exame_lab_result_item.dt_aprovacao%type;
nr_exames_aprovados_w         number;
nr_seq_metodo_w               exame_lab_result_item.nr_Seq_metodo%type;

ficha_row                       lote_ent_sec_ficha%rowtype;

ie_trigger_prescr_proc    char(1);

cursor c01 is
  select  a.nr_seq_evento
  from  regra_envio_sms a
  where  a.ie_evento_disp = 'ALTLIBFIC'
  and  nvl(a.ie_situacao,'A') = 'A';

-- exames principais
cursor c02 is
  select   f.dt_aprovacao,
            b.nr_prescricao,
            c.nr_sequencia nr_seq_prescr,
            d.nr_sequencia nr_seq_material
  from     prescr_medica b,
            prescr_procedimento c,
            material_exame_lab d,
            exame_lab_resultado e,
            exame_lab_result_item f
  where    b.nr_seq_ficha_lote = :old.nr_sequencia
  and      b.nr_prescricao = :old.nr_prescricao
  and      b.nr_prescricao = c.nr_prescricao
  and      c.nr_seq_exame is not null
  and      c.cd_material_exame = d.cd_material_exame
  and      b.nr_prescricao = e.nr_prescricao
  and      f.nr_seq_resultado = e.nr_seq_resultado
  and      f.nr_seq_prescr = c.nr_sequencia
  and      f.nr_seq_material is not null;

-- exames filhos
cursor c03 is
  select c.nr_seq_exame,
    f.nr_seq_resultado,
    f.qt_resultado,
    f.pr_resultado,
    f.ds_resultado
  from     prescr_medica b,
    prescr_procedimento c,
    exame_lab_resultado e,
    exame_lab_result_item f
  where    b.nr_seq_ficha_lote = :old.nr_sequencia
  and      b.nr_prescricao = nr_prescricao_w
  and      b.nr_prescricao = c.nr_prescricao
  and      c.nr_seq_exame is not null
  and      b.nr_prescricao = e.nr_prescricao
  and      f.nr_seq_resultado = e.nr_seq_resultado
    and     f.nr_seq_prescr = nr_seq_prescr_w
  and      f.nr_seq_prescr = c.nr_sequencia;

begin

begin

  select  'S'
  into    ie_trigger_prescr_proc
  from    dual
  where   lower( substr( dbms_utility.format_call_stack, 1, 4000 ) ) like '%prescr_procedimento_update%';

exception
when others then

    select  'N'
    into    ie_trigger_prescr_proc
    from    dual;
end;

ficha_row.NR_SEQUENCIA                 :=     :new.NR_SEQUENCIA;
ficha_row.NR_SEQ_LOTE_SEC              :=     :new.NR_SEQ_LOTE_SEC;
ficha_row.DT_ATUALIZACAO               :=     :new.DT_ATUALIZACAO;
ficha_row.NM_USUARIO                   :=     :new.NM_USUARIO;
ficha_row.DT_ATUALIZACAO_NREC          :=     :new.DT_ATUALIZACAO_NREC;
ficha_row.NM_USUARIO_NREC              :=     :new.NM_USUARIO_NREC;
ficha_row.NR_PRONTUARIO_EX             :=     :new.NR_PRONTUARIO_EX;
ficha_row.CD_USUARIO_CONV_EXT          :=     :new.CD_USUARIO_CONV_EXT;
ficha_row.NR_FICHA_EXT                 :=     :new.NR_FICHA_EXT;
ficha_row.DS_OBSERVACAO                :=     :new.DS_OBSERVACAO;
ficha_row.IE_SUSP_AM_INAD              :=     :new.IE_SUSP_AM_INAD;
ficha_row.CD_BARRAS                    :=     :new.CD_BARRAS;
ficha_row.NR_CARTAO_NAC_SUS            :=     :new.NR_CARTAO_NAC_SUS;
ficha_row.IE_FICHA_ADICIONAL           :=     :new.IE_FICHA_ADICIONAL;
ficha_row.CD_ENTIDADE_F                :=     :new.CD_ENTIDADE_F;
ficha_row.CD_LOTE_F                    :=     :new.CD_LOTE_F;
ficha_row.CD_EXAME_F                   :=     :new.CD_EXAME_F;
ficha_row.IE_PREMATURO_F               :=     :new.IE_PREMATURO_F;
ficha_row.IE_TRANSFUSAO_F              :=     :new.IE_TRANSFUSAO_F;
ficha_row.DT_COLETA_F                  :=     :new.DT_COLETA_F;
ficha_row.IE_TIPO_TESTE_F              :=     :new.IE_TIPO_TESTE_F;
ficha_row.DS_REPETICAO_EXAME_F         :=     :new.DS_REPETICAO_EXAME_F;
ficha_row.CD_ENTRADA_F                 :=     :new.CD_ENTRADA_F;
ficha_row.CD_EXAME_FICHA_F             :=     :new.CD_EXAME_FICHA_F;
ficha_row.CD_PRONTUARIO_F              :=     :new.CD_PRONTUARIO_F;
ficha_row.NM_RN_F                      :=     :new.NM_RN_F;
ficha_row.CD_DNV_F                     :=     :new.CD_DNV_F;
ficha_row.DT_COLETA_FICHA_F            :=     :new.DT_COLETA_FICHA_F;
ficha_row.IE_PERIODO_COL_F             :=     :new.IE_PERIODO_COL_F;
ficha_row.DT_NASCIMENTO_F              :=     :new.DT_NASCIMENTO_F;
ficha_row.IE_PERIODO_NASC_F            :=     :new.IE_PERIODO_NASC_F;
ficha_row.QT_PESO_F                    :=     :new.QT_PESO_F;
ficha_row.IE_SEXO_F                    :=     :new.IE_SEXO_F;
ficha_row.IE_COR_PF_F                  :=     :new.IE_COR_PF_F;
ficha_row.IE_NPP_F                     :=     :new.IE_NPP_F;
ficha_row.IE_PREMAT_S_F                :=     :new.IE_PREMAT_S_F;
ficha_row.NR_IDADE_GEST_F              :=     :new.NR_IDADE_GEST_F;
ficha_row.IE_PREMAT_N_F                :=     :new.IE_PREMAT_N_F;
ficha_row.IE_TRANSF_S_F                :=     :new.IE_TRANSF_S_F;
ficha_row.DT_TRANSF_F                  :=     :new.DT_TRANSF_F;
ficha_row.IE_TRANSF_N_F                :=     :new.IE_TRANSF_N_F;
ficha_row.IE_GEMELAR_F                 :=     :new.IE_GEMELAR_F;
ficha_row.IE_TIPO_FICHA                :=     :new.IE_TIPO_FICHA;
ficha_row.CD_MATERIAL_EXAME            :=     :new.CD_MATERIAL_EXAME;
ficha_row.IE_STATUS_FICHA              :=     :new.IE_STATUS_FICHA;
ficha_row.NR_SEQ_MOT_INADEQ            :=     :new.NR_SEQ_MOT_INADEQ;
ficha_row.DS_MOTIVO_INADEQ             :=     :new.DS_MOTIVO_INADEQ;
ficha_row.CD_PESSOA_FISICA             :=     :new.CD_PESSOA_FISICA;
ficha_row.IE_BARRA_GERADO              :=     :new.IE_BARRA_GERADO;
ficha_row.NM_MAE_F                     :=     :new.NM_MAE_F;
ficha_row.CD_MEDICO_RESP               :=     :new.CD_MEDICO_RESP;
ficha_row.NR_SEQ_TIPO_MAT_LOTE         :=     :new.NR_SEQ_TIPO_MAT_LOTE;
ficha_row.NR_PRESCRICAO                :=     :new.NR_PRESCRICAO;
ficha_row.IE_AMOSTRA                   :=     :new.IE_AMOSTRA;
ficha_row.NR_SEQ_GRAU_PARENTESCO       :=     :new.NR_SEQ_GRAU_PARENTESCO;
ficha_row.DT_VALIDADE                  :=     :new.DT_VALIDADE;
ficha_row.DT_FABRICACAO                :=     :new.DT_FABRICACAO;
ficha_row.NR_LOTE_FICHA                :=     :new.NR_LOTE_FICHA;
ficha_row.NR_ATENDIMENTO               :=     :new.NR_ATENDIMENTO;
ficha_row.CD_PESSOA_EXTERNA            :=     :new.CD_PESSOA_EXTERNA;
ficha_row.HR_COLETA_F                  :=     :new.HR_COLETA_F;
ficha_row.HR_NASCIMENTO_F              :=     :new.HR_NASCIMENTO_F;
ficha_row.CD_MOTIVO_BAIXA              :=     :new.CD_MOTIVO_BAIXA;
ficha_row.IE_CORTICOIDE_F              :=     :new.IE_CORTICOIDE_F;
ficha_row.DS_CORTICOIDE_F              :=     :new.DS_CORTICOIDE_F;
ficha_row.IE_SUSPENSO                  :=     :new.IE_SUSPENSO;
ficha_row.IE_FICHA_URG                 :=     :new.IE_FICHA_URG;
ficha_row.NR_SEQ_LOCAL                 :=     :new.NR_SEQ_LOCAL;
ficha_row.NR_SEQ_LOCAL_CAIXA           :=     :new.NR_SEQ_LOCAL_CAIXA;
ficha_row.NR_SEQ_FICHA_ANT             :=     :new.NR_SEQ_FICHA_ANT;
ficha_row.IE_AMAMENTADO_F              :=     :new.IE_AMAMENTADO_F;
ficha_row.IE_ALIM_LEITE_F              :=     :new.IE_ALIM_LEITE_F;
ficha_row.IE_ICTERICIA_F               :=     :new.IE_ICTERICIA_F;
ficha_row.IE_MAE_VEG_F                 :=     :new.IE_MAE_VEG_F;
ficha_row.QT_GEST_F                    :=     :new.QT_GEST_F;
ficha_row.DT_ULT_MENST                 :=     :new.DT_ULT_MENST;
ficha_row.IE_TIPO_PARTO                :=     :new.IE_TIPO_PARTO;
ficha_row.IE_ABORTO                    :=     :new.IE_ABORTO;
ficha_row.QT_ABORTO                    :=     :new.QT_ABORTO;
ficha_row.NR_SISPRENATAL               :=     :new.NR_SISPRENATAL;
ficha_row.DS_UNIDADE_BASICA_ORIGEM     :=     :new.DS_UNIDADE_BASICA_ORIGEM;
ficha_row.NR_DDI                       :=     :new.NR_DDI;
ficha_row.NR_DDI_CELULAR               :=     :new.NR_DDI_CELULAR;
ficha_row.NR_SEQ_MOTIVO_NAO_ELUIDO     :=     :new.NR_SEQ_MOTIVO_NAO_ELUIDO;

lote_ent_sec_ficha_pck.set_row(ficha_row);

ds_retorno_w := '';

obter_param_usuario(10060,64,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,nr_seq_tipo_ocorr_w);

if (:new.cd_pessoa_fisica is not null) then
    Lote_ent_sec_atualiz_dnv(:new.cd_pessoa_fisica, :new.cd_dnv_f, :new.nm_usuario);
end if;


if (nr_seq_tipo_ocorr_w is null) then

  select  MAX(nr_sequencia)
  into  nr_seq_tipo_ocorr_w
  from   lote_ent_tipo_ocorrencia;

  if (nr_seq_tipo_ocorr_w is null) then
    wheb_mensagem_pck.exibir_mensagem_abort(294313);
  end if;


end if;

select   MAX(nr_sequencia),
    MAX(nr_lote)
into   nr_seq_lote_ent_w,
    nr_lote_w
from     lote_ent_secretaria
where  nr_sequencia = :new.nr_seq_lote_sec
and   dt_liberacao is not null;

if (nr_seq_lote_ent_w is not null) and
  (:old.nr_prescricao is not null) then
  select   lote_ent_sec_ficha_log_seq.nextVal
  into   nr_sequencia_w
  from   dual;

  ds_retorno_w := wheb_mensagem_pck.get_texto(308667) || ' ' /*'Ficha alterada: '*/ || nr_sequencia_w || chr(13) || chr(10) || ' ' || wheb_mensagem_pck.get_texto(308670) || ' ' /*' Coluna    -    Vl. antigo   -   Vl. novo '*/ || chr(13) || chr(10);

  if (:old.cd_motivo_baixa <> :new.cd_motivo_baixa) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308673) || ': ' /*'Motivo baixa: '*/|| :old.cd_motivo_baixa || ' - ' || :new.cd_motivo_baixa || chr(13) || chr(10);
  end if;

  if (:old.nr_seq_lote_sec <> :new.nr_seq_lote_sec) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308675) || ': ' /*'Seq lote: '*/|| :old.nr_seq_lote_sec || ' - ' || :new.nr_seq_lote_sec || chr(13) || chr(10);
  end if;

  if (:old.nm_usuario <> :new.nm_usuario) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308677) || ' ' /*'Usu�rio: '*/|| :old.nm_usuario || ' - ' || :new.nm_usuario || chr(13) || chr(10);
  end if;

  if (:old.nr_prontuario_ex <> :new.nr_prontuario_ex) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308681) || ' ' /*'Prontu�rio: '*/|| :old.nr_prontuario_ex || ' - ' || :new.nr_prontuario_ex || chr(13) || chr(10);
  end if;

  if (:old.cd_usuario_conv_ext <> :new.cd_usuario_conv_ext) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308682) || ' ' /*'Cd. Usu�rio Ext: '*/|| :old.cd_usuario_conv_ext || ' - ' || :new.cd_usuario_conv_ext || chr(13) || chr(10);
  end if;

  if (:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308684) || ': ' /*'Pessoa f�sica: '*/|| :old.cd_pessoa_fisica || ' - ' || :new.cd_pessoa_fisica || chr(13) || chr(10);
  end if;

  if (:old.nr_ficha_ext <> :new.nr_ficha_ext) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308687) || ' '/*'Ficha ext: '*/|| :old.nr_ficha_ext || ' - ' || :new.nr_ficha_ext || chr(13) || chr(10);
  end if;

  if (:old.ds_observacao <> :new.ds_observacao) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308689) || ' ' /*'Observa��o: '*/|| :old.ds_observacao || ' - ' || :new.ds_observacao || chr(13) || chr(10);
  end if;

  if (:old.ie_susp_am_inad <> :new.ie_susp_am_inad) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308691) || ' ' /*'Supende amostra inad: '*/|| :old.ie_susp_am_inad || ' - ' || :new.ie_susp_am_inad || chr(13) || chr(10);
  end if;

  if (:old.cd_barras <> :new.cd_barras) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308692) || ' ' /*'Barras: '*/|| :old.cd_barras || ' - ' || :new.cd_barras || chr(13) || chr(10);
  end if;

  if (:old.nr_cartao_nac_sus <> :new.nr_cartao_nac_sus) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308693) || ': ' /*'Cart�o SUS: '*/|| :old.nr_cartao_nac_sus || ' - ' || :new.nr_cartao_nac_sus || chr(13) || chr(10);
  end if;

  if (:old.ie_suspenso <> :new.ie_suspenso) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308694) || ': ' /*'Suspenso: '*/|| :old.ie_suspenso || ' - ' || :new.ie_suspenso || chr(13) || chr(10);
  end if;

  if (:old.ie_ficha_adicional <> :new.ie_ficha_adicional) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308695) || ' ' /*'Ficha adicional: '*/|| :old.ie_ficha_adicional || ' - ' || :new.ie_ficha_adicional || chr(13) || chr(10);
  end if;

  if (:old.cd_entidade_f <> :new.cd_entidade_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308696) || ': ' /*'Cod. entidade: '*/|| :old.cd_entidade_f || ' - ' || :new.cd_entidade_f || chr(13) || chr(10);
  end if;

  if (:old.cd_lote_f <> :new.cd_lote_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308697) || ' ' /*'Cod. lote: '*/|| :old.cd_lote_f || ' - ' || :new.cd_lote_f || chr(13) || chr(10);
  end if;

  if (:old.cd_exame_f <> :new.cd_exame_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308698) || ' ' /*'Cod. exame: '*/|| :old.cd_exame_f || ' - ' || :new.cd_exame_f || chr(13) || chr(10);
  end if;

  if (:old.ie_prematuro_f <> :new.ie_prematuro_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308699) || ': ' /*'Prematuro: '*/|| :old.ie_prematuro_f || ' - ' || :new.ie_prematuro_f || chr(13) || chr(10);
  end if;

  if (:old.ie_transfusao_f <> :new.ie_transfusao_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308700) || ': ' /*'Transfundido: '*/|| :old.ie_transfusao_f || ' - ' || :new.ie_transfusao_f || chr(13) || chr(10);
  end if;

  if (:old.dt_coleta_f <> :new.dt_coleta_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308701) || ' ' /*'Dt. coleta: '*/|| :old.dt_coleta_f || ' - ' || :new.dt_coleta_f || chr(13) || chr(10);
  end if;

  if (:old.ie_tipo_teste_f <> :new.ie_tipo_teste_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308702) || ': ' /*'Tipo teste: '*/|| :old.ie_tipo_teste_f || ' - ' || :new.ie_tipo_teste_f || chr(13) || chr(10);
  end if;

  if (:old.cd_entrada_f <> :new.cd_entrada_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308703) || ' ' /*'Cod. entrada: '*/|| :old.cd_entrada_f || ' - ' || :new.cd_entrada_f || chr(13) || chr(10);
  end if;

  if (:old.cd_exame_ficha_f <> :new.cd_exame_ficha_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308704) || ' ' /*'Cod. exame ficha: '*/|| :old.cd_exame_ficha_f || ' - ' || :new.cd_exame_ficha_f || chr(13) || chr(10);
  end if;

  if (:old.cd_prontuario_f <> :new.cd_prontuario_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308705) || ' ' /*'Cod. prontu�rio: '*/|| :old.cd_prontuario_f || ' - ' || :new.cd_prontuario_f || chr(13) || chr(10);
  end if;

  if (:old.nm_rn_f <> :new.nm_rn_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308707) || ': ' /*'RN: '*/|| :old.nm_rn_f || ' - ' || :new.nm_rn_f || chr(13) || chr(10);
  end if;

  if (:old.cd_dnv_f <> :new.cd_dnv_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308708) || ' ' /*'Cod. DNV: '*/|| :old.cd_dnv_f || ' - ' || :new.cd_dnv_f || chr(13) || chr(10);
  end if;

  if (:old.dt_coleta_ficha_f <> :new.dt_coleta_ficha_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308709) || ' ' /*'Dt. coleta ficha: '*/|| :old.dt_coleta_ficha_f || ' - ' || :new.dt_coleta_ficha_f || chr(13) || chr(10);
  end if;

  if (:old.ie_periodo_col_f <> :new.ie_periodo_col_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308710) || ' ' /*'Per�odo coleta: '*/|| :old.ie_periodo_col_f || ' - ' || :new.ie_periodo_col_f || chr(13) || chr(10);
  end if;

  if (:old.dt_nascimento_f <> :new.dt_nascimento_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308711) || ' ' /*'Dt. nascimento: '*/|| :old.dt_nascimento_f || ' - ' || :new.dt_nascimento_f || chr(13) || chr(10);
  end if;

  if (:old.nr_prescricao <> :new.nr_prescricao) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308714) || ' ' /*'Prescri��o: '*/|| :old.nr_prescricao || ' - ' || :new.nr_prescricao || chr(13) || chr(10);
  end if;

  if (:old.nr_seq_tipo_mat_lote <> :new.nr_seq_tipo_mat_lote) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308715) || ' ' /*'Tipo mat. lote: '*/|| :old.nr_seq_tipo_mat_lote || ' - ' || :new.nr_seq_tipo_mat_lote || chr(13) || chr(10);
  end if;

  if (:old.hr_coleta_f <> :new.hr_coleta_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308716) || ' ' /*'Hr. coleta: '*/|| :old.hr_coleta_f || ' - ' || :new.hr_coleta_f || chr(13) || chr(10);
  end if;

  if (:old.hr_nascimento_f <> :new.hr_nascimento_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308717) || ' ' /*'Hr. nascimento: '*/|| :old.hr_nascimento_f || ' - ' || :new.hr_nascimento_f || chr(13) || chr(10);
  end if;

  if (:old.ie_periodo_nasc_f <> :new.ie_periodo_nasc_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308718) || ' ' /*'Per�odo nasc.: '*/|| :old.ie_periodo_nasc_f || ' - ' || :new.ie_periodo_nasc_f || chr(13) || chr(10);
  end if;

  if (:old.qt_peso_f <> :new.qt_peso_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308719) || ': '/*'Peso: '*/|| :old.qt_peso_f || ' - ' || :new.qt_peso_f || chr(13) || chr(10);
  end if;

  if (:old.ie_sexo_f <> :new.ie_sexo_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308720) || ': ' /*'Sexo: '*/|| :old.ie_sexo_f || ' - ' || :new.ie_sexo_f || chr(13) || chr(10);
  end if;

  if (:old.ie_cor_pf_f <> :new.ie_cor_pf_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308721) || ' ' /*'Cod. Cor: '*/|| :old.ie_cor_pf_f || ' - ' || :new.ie_cor_pf_f || chr(13) || chr(10);
  end if;

  if (:old.ie_npp_f <> :new.ie_npp_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308722) || ': ' /*'NPP: '*/|| :old.ie_npp_f || ' - ' || :new.ie_npp_f || chr(13) || chr(10);
  end if;

  if (:old.ie_premat_s_f <> :new.ie_premat_s_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308699) || ': ' /*'Prematuro: '*/|| :old.ie_premat_s_f || ' - ' || :new.ie_premat_s_f || chr(13) || chr(10);
  end if;

  if (:old.nr_idade_gest_f <> :new.nr_idade_gest_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308723) || ': ' /*'Idade gest: '*/|| :old.nr_idade_gest_f || ' - ' || :new.nr_idade_gest_f || chr(13) || chr(10);
  end if;

  if (:old.ie_premat_n_f <> :new.ie_premat_n_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308699) || ': ' /*'Prematuro: '*/|| :old.ie_premat_n_f || ' - ' || :new.ie_premat_n_f || chr(13) || chr(10);
  end if;

  if (:old.ie_transf_s_f <> :new.ie_transf_s_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308700) || ': ' /*'Transfundido: '*/|| :old.ie_transf_s_f || ' - ' || :new.ie_transf_s_f || chr(13) || chr(10);
  end if;

  if (:old.dt_transf_f <> :new.dt_transf_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308725) || ': ' /*'Dt. transfus�o: '*/|| :old.dt_transf_f || ' - ' || :new.dt_transf_f || chr(13) || chr(10);
  end if;

  if (:old.ie_transf_n_f <> :new.ie_transf_n_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308700) || ': ' /*'Transfundido: '*/|| :old.ie_transf_n_f || ' - ' || :new.ie_transf_n_f || chr(13) || chr(10);
  end if;

  if (:old.ie_gemelar_f <> :new.ie_gemelar_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308726) || ': ' /*'Gemelar: '*/|| :old.ie_gemelar_f || ' - ' || :new.ie_gemelar_f || chr(13) || chr(10);
  end if;

  if (:old.ie_corticoide_f <> :new.ie_corticoide_f) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308727) || ': ' /*'Cortic�ide: '*/|| :old.ie_corticoide_f || ' - ' || :new.ie_corticoide_f || chr(13) || chr(10);
  end if;

  if (:old.dt_validade <> :new.dt_validade) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308728) || ' ' /*'Dt. validade: '*/|| :old.dt_validade || ' - ' || :new.dt_validade || chr(13) || chr(10);
  end if;

  if (:old.ie_amostra <> :new.ie_amostra) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308730) || ': ' /*'Amostra: '*/|| :old.ie_amostra || ' - ' || :new.ie_amostra || chr(13) || chr(10);
  end if;

  if (:old.nr_seq_grau_parentesco <> :new.nr_seq_grau_parentesco) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308731) || ': ' /*'Grau parentesco: '*/|| :old.nr_seq_grau_parentesco || ' - ' || :new.nr_seq_grau_parentesco || chr(13) || chr(10);
  end if;

  if (:old.dt_fabricacao <> :new.dt_fabricacao) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308732) || ': ' /*'Dt. fabrica��o: '*/|| :old.dt_fabricacao || ' - ' || :new.dt_fabricacao || chr(13) || chr(10);
  end if;

  if (:old.nr_lote_ficha <> :new.nr_lote_ficha) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308733) || ' ' /*'Nr lote: '*/|| :old.nr_lote_ficha || ' - ' || :new.nr_lote_ficha || chr(13) || chr(10);
  end if;

  if (:old.cd_pessoa_externa <> :new.cd_pessoa_externa) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308734) || ' ' /*'Cod. pessoa ext.: '*/|| :old.cd_pessoa_externa || ' - ' || :new.cd_pessoa_externa || chr(13) || chr(10);
  end if;

  if (:old.cd_medico_resp <> :new.cd_medico_resp) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308736) || ': ' /*'C�d. m�dico resp: '*/|| :old.cd_medico_resp || ' - ' || :new.cd_medico_resp || chr(13) || chr(10);
  end if;

  if (:old.nr_seq_ficha_ant <> :new.nr_seq_ficha_ant) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308738) || ': ' /*'Ficha anterior: '*/|| :old.nr_seq_ficha_ant || ' - ' || :new.nr_seq_ficha_ant || chr(13) || chr(10);
  end if;

  if (:old.ie_tipo_ficha <> :new.ie_tipo_ficha) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308739) || ': ' /*'Tipo ficha: '*/|| :old.ie_tipo_ficha || ' - ' || :new.ie_tipo_ficha || chr(13) || chr(10);
  end if;

  if (:old.cd_material_exame <> :new.cd_material_exame) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308740) || ': ' /*'C�d. material: '*/|| :old.cd_material_exame || ' - ' || :new.cd_material_exame || chr(13) || chr(10);
  end if;

  if (:old.ie_status_ficha <> :new.ie_status_ficha) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308741) || ': ' /*'Status ficha: '*/|| :old.ie_status_ficha || ' - ' || :new.ie_status_ficha || chr(13) || chr(10);
  end if;

  if (:old.ds_motivo_inadeq <> :new.ds_motivo_inadeq) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308742) || '.: ' /*'Motivo inad.: '*/|| :old.ds_motivo_inadeq || ' - ' || :new.ds_motivo_inadeq || chr(13) || chr(10);
  end if;

  if (:old.ie_barra_gerado <> :new.ie_barra_gerado) then
    ds_retorno_w := ds_retorno_w || Wheb_mensagem_pck.get_texto(308743) || ': ' /*'Barras gerado: '*/|| :old.ie_barra_gerado || ' - ' || :new.ie_barra_gerado || chr(13) || chr(10);
  end if;

  select   lote_ent_ocorrencia_seq.nextval
  into  nr_seq_ocorrencia_w
  from   dual;

  insert into lote_ent_ocorrencia (
    nr_sequencia,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    ie_situacao,
    nr_seq_tipo_ocorrencia,
    ds_observacao,
    NR_SEQ_FICHA,
    NR_SEQ_LOTE_SEC
  ) values (
    nr_seq_ocorrencia_w,
    sysdate,
    :new.nm_usuario,
    sysdate,
    :new.nm_usuario,
    'A',
    nr_seq_tipo_ocorr_w,
    ds_retorno_w,
    :new.nr_sequencia,
    nr_seq_lote_ent_w
  );

  insert into lote_ent_sec_ficha_log (nr_sequencia,
                    cd_motivo_baixa,
                    nr_seq_lote_sec,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    nr_prontuario_ex,
                    cd_usuario_conv_ext,
                    cd_pessoa_fisica,
                    nr_ficha_ext,
                    ds_observacao,
                    ie_susp_am_inad,
                    cd_barras,
                    nr_seq_ficha_orig,
                    nr_cartao_nac_sus,
                    ie_suspenso,
                    ie_ficha_adicional,
                    cd_entidade_f,
                    cd_lote_f,
                    cd_exame_f,
                    ie_prematuro_f,
                    ie_transfusao_f,
                    dt_coleta_f,
                    ie_tipo_teste_f,
                    cd_entrada_f,
                    cd_exame_ficha_f,
                    cd_prontuario_f,
                    nm_rn_f,
                    cd_dnv_f,
                    dt_coleta_ficha_f,
                    ie_periodo_col_f,
                    dt_nascimento_f,
                    nr_prescricao,
                    nr_seq_tipo_mat_lote,
                    hr_coleta_f,
                    hr_nascimento_f,
                    ie_periodo_nasc_f,
                    qt_peso_f,
                    ie_sexo_f,
                    ie_cor_pf_f,
                    ie_npp_f,
                    ie_premat_s_f,
                    nr_idade_gest_f,
                    ie_premat_n_f,
                    ie_transf_s_f,
                    dt_transf_f,
                    ie_transf_n_f,
                    ie_gemelar_f,
                    ie_corticoide_f,
                    dt_validade,
                    ie_amostra,
                    nr_seq_grau_parentesco,
                    dt_fabricacao,
                    nr_lote_ficha,
                    cd_pessoa_externa,
                    cd_medico_resp,
                    nr_seq_ficha_ant,
                    ie_tipo_ficha,
                    cd_material_exame,
                    ie_status_ficha,
                    ds_motivo_inadeq,
                    ie_barra_gerado)
                    values (
                    nr_sequencia_w,
                    :old.cd_motivo_baixa,
                    :old.nr_seq_lote_sec,
                    :old.dt_atualizacao,
                    :old.nm_usuario,
                    :old.dt_atualizacao_nrec,
                    :old.nm_usuario_nrec,
                    :old.nr_prontuario_ex,
                    :old.cd_usuario_conv_ext,
                    :old.cd_pessoa_fisica,
                    :old.nr_ficha_ext,
                    :old.ds_observacao,
                    :old.ie_susp_am_inad,
                    :old.cd_barras,
                    :old.nr_sequencia,
                    :old.nr_cartao_nac_sus,
                    :old.ie_suspenso,
                    nvl(:old.ie_ficha_adicional,'N'),
                    :old.cd_entidade_f,
                    :old.cd_lote_f,
                    :old.cd_exame_f,
                    :old.ie_prematuro_f,
                    :old.ie_transfusao_f,
                    :old.dt_coleta_f,
                    :old.ie_tipo_teste_f,
                    :old.cd_entrada_f,
                    :old.cd_exame_ficha_f,
                    :old.cd_prontuario_f,
                    :old.nm_rn_f,
                    :old.cd_dnv_f,
                    :old.dt_coleta_ficha_f,
                    :old.ie_periodo_col_f,
                    :old.dt_nascimento_f,
                    :old.nr_prescricao,
                    :old.nr_seq_tipo_mat_lote,
                    :old.hr_coleta_f,
                    :old.hr_nascimento_f,
                    :old.ie_periodo_nasc_f,
                    :old.qt_peso_f,
                    :old.ie_sexo_f,
                    :old.ie_cor_pf_f,
                    :old.ie_npp_f,
                    :old.ie_premat_s_f,
                    :old.nr_idade_gest_f,
                    :old.ie_premat_n_f,
                    :old.ie_transf_s_f,
                    :old.dt_transf_f,
                    :old.ie_transf_n_f,
                    :old.ie_gemelar_f,
                    :old.ie_corticoide_f,
                    :old.dt_validade,
                    :old.ie_amostra,
                    :old.nr_seq_grau_parentesco,
                    :old.dt_fabricacao,
                    :old.nr_lote_ficha,
                    :old.cd_pessoa_externa,
                    :old.cd_medico_resp,
                    :old.nr_seq_ficha_ant,
                    :old.ie_tipo_ficha,
                    :old.cd_material_exame,
                    :old.ie_status_ficha,
                    :old.ds_motivo_inadeq,
                    :old.ie_barra_gerado
                    );

    if   (:old.dt_coleta_f <> :new.dt_coleta_f) or
    (:old.hr_coleta_f <> :new.hr_coleta_f) or
    (:old.dt_nascimento_f <> :new.dt_nascimento_f) or
    (:old.hr_nascimento_f <> :new.hr_nascimento_f)  then
    lab_atualiza_regra_pacote_proc ( nr_seq_lote_ent_w,:old.nr_sequencia,:new.nm_usuario, :new.dt_coleta_f, :new.hr_coleta_f,:new.dt_nascimento_f, :new.hr_nascimento_f);
  end if;

    open C01;
    loop
    fetch C01 into
      nr_seq_evento_w;
    exit when C01%notfound;
      begin
      gerar_evento_alter_ficha_lib (nr_seq_evento_w, nr_lote_w,:old.nr_sequencia, :old.nm_usuario,:old.nr_prescricao,:old.cd_pessoa_fisica);
      end;
    end loop;
    close C01;

  if ((:new.ie_tipo_ficha = :old.ie_tipo_ficha) and (ie_trigger_prescr_proc = 'N')) then
    nr_exames_aprovados_w := 0;

    open C02;
    loop
    fetch C02 into
      dt_aprovacao_exame_pai_w,
      nr_prescricao_w,
            nr_seq_prescr_w,
      nr_seq_material_prescr_w;
    exit when C02%notfound;
      begin
      -- Se exame principal n�o estiver aprovado, atualiza, sen�o exibe mensagem
      if (dt_aprovacao_exame_pai_w is null) then
        open C03;
        loop
        fetch C03 into
          nr_seq_exame_prescr_w,
          nr_seq_resultado_w,
          qt_resultado_w,
          pr_resultado_w,
          ds_resultado_w;
        exit when C03%notfound;
          begin

                        select  b.nr_seq_metodo
                        into    nr_seq_metodo_w
                        from    exame_lab_result_item b
                        where   b.nr_seq_resultado = nr_seq_resultado_w and
                                b.nr_seq_exame  = nr_seq_exame_prescr_w and
                                b.nr_seq_prescr = nr_seq_prescr_w;

            update exame_lab_result_item
            set    ie_tipo_val_crit = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,1, nr_seq_metodo_w,nr_seq_resultado_w),1,255),
                 ie_valor_crit = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,2, nr_seq_metodo_w,nr_seq_resultado_w),1,2),
                 ds_mensagem_criterio =  SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,3,nr_seq_metodo_w,nr_seq_resultado_w),1,4000),
                 ds_acao_criterio = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,4,nr_seq_metodo_w,nr_seq_resultado_w),1,255),
                 ie_acao_criterio_lote = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,7,nr_seq_metodo_w,nr_seq_resultado_w),1,1),
                 ds_result_sug_crit = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,8,nr_seq_metodo_w,nr_seq_resultado_w),1,255),
                 ds_material_criterio = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, nvl(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,9,nr_seq_metodo_w,nr_seq_resultado_w),1,255),
                 ds_metodo_criterio = SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(qt_resultado_w,pr_resultado_w), ds_resultado_w, nr_prescricao_w, nr_seq_prescr_w,10,nr_seq_metodo_w,nr_seq_resultado_w),1,255),
                 dt_atualizacao = SYSDATE
            where  nr_seq_resultado = nr_seq_resultado_w
            and     nr_seq_prescr = nr_seq_prescr_w;

          end;
        end loop;
        close C03;
      else
        -- se o exame principal j� estiver aprovado, n�o deve alterar dados do exame principal ou filhos
        nr_exames_aprovados_w := nr_exames_aprovados_w + 1;
      end if;
      end;
    end loop;
    close C02;

    if (nr_exames_aprovados_w > 0) then
      gravar_log_lab_pragma(37, wheb_mensagem_pck.get_texto(334623), wheb_usuario_pck.get_nm_usuario);
    end if;

  end if;

end if;

--commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.lote_ent_sec_ficha_insert 
before insert ON TASY.LOTE_ENT_SEC_FICHA 
for each row
declare 
 
begin 
 
  if (:new.cd_pessoa_fisica is not null) then 
    Lote_ent_sec_atualiz_dnv(:new.cd_pessoa_fisica, :new.cd_dnv_f, :new.nm_usuario);		 
  end if; 
 
end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_ENT_SEC_FICHA_tp  after update ON TASY.LOTE_ENT_SEC_FICHA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_AMAMENTADO_F,1,500);gravar_log_alteracao(substr(:old.IE_AMAMENTADO_F,1,4000),substr(:new.IE_AMAMENTADO_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_AMAMENTADO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ALIM_LEITE_F,1,500);gravar_log_alteracao(substr(:old.IE_ALIM_LEITE_F,1,4000),substr(:new.IE_ALIM_LEITE_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALIM_LEITE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ICTERICIA_F,1,500);gravar_log_alteracao(substr(:old.IE_ICTERICIA_F,1,4000),substr(:new.IE_ICTERICIA_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_ICTERICIA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MAE_VEG_F,1,500);gravar_log_alteracao(substr(:old.IE_MAE_VEG_F,1,4000),substr(:new.IE_MAE_VEG_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_MAE_VEG_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ULT_MENST,1,500);gravar_log_alteracao(substr(:old.DT_ULT_MENST,1,4000),substr(:new.DT_ULT_MENST,1,4000),:new.nm_usuario,nr_seq_w,'DT_ULT_MENST',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_GEST_F,1,500);gravar_log_alteracao(substr(:old.QT_GEST_F,1,4000),substr(:new.QT_GEST_F,1,4000),:new.nm_usuario,nr_seq_w,'QT_GEST_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_PARTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PARTO,1,4000),substr(:new.IE_TIPO_PARTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PARTO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ABORTO,1,500);gravar_log_alteracao(substr(:old.IE_ABORTO,1,4000),substr(:new.IE_ABORTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABORTO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_ABORTO,1,500);gravar_log_alteracao(substr(:old.QT_ABORTO,1,4000),substr(:new.QT_ABORTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_ABORTO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SISPRENATAL,1,500);gravar_log_alteracao(substr(:old.NR_SISPRENATAL,1,4000),substr(:new.NR_SISPRENATAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SISPRENATAL',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_NASCIMENTO_F,1,500);gravar_log_alteracao(substr(:old.DT_NASCIMENTO_F,1,4000),substr(:new.DT_NASCIMENTO_F,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PERIODO_COL_F,1,500);gravar_log_alteracao(substr(:old.IE_PERIODO_COL_F,1,4000),substr(:new.IE_PERIODO_COL_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERIODO_COL_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PERIODO_NASC_F,1,500);gravar_log_alteracao(substr(:old.IE_PERIODO_NASC_F,1,4000),substr(:new.IE_PERIODO_NASC_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERIODO_NASC_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_PESO_F,1,500);gravar_log_alteracao(substr(:old.QT_PESO_F,1,4000),substr(:new.QT_PESO_F,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SEXO_F,1,500);gravar_log_alteracao(substr(:old.IE_SEXO_F,1,4000),substr(:new.IE_SEXO_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_COR_PF_F,1,500);gravar_log_alteracao(substr(:old.IE_COR_PF_F,1,4000),substr(:new.IE_COR_PF_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_COR_PF_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NPP_F,1,500);gravar_log_alteracao(substr(:old.IE_NPP_F,1,4000),substr(:new.IE_NPP_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_NPP_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PREMAT_S_F,1,500);gravar_log_alteracao(substr(:old.IE_PREMAT_S_F,1,4000),substr(:new.IE_PREMAT_S_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREMAT_S_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_IDADE_GEST_F,1,500);gravar_log_alteracao(substr(:old.NR_IDADE_GEST_F,1,4000),substr(:new.NR_IDADE_GEST_F,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDADE_GEST_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PREMAT_N_F,1,500);gravar_log_alteracao(substr(:old.IE_PREMAT_N_F,1,4000),substr(:new.IE_PREMAT_N_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREMAT_N_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TRANSF_S_F,1,500);gravar_log_alteracao(substr(:old.IE_TRANSF_S_F,1,4000),substr(:new.IE_TRANSF_S_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSF_S_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_TRANSF_F,1,500);gravar_log_alteracao(substr(:old.DT_TRANSF_F,1,4000),substr(:new.DT_TRANSF_F,1,4000),:new.nm_usuario,nr_seq_w,'DT_TRANSF_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TRANSF_N_F,1,500);gravar_log_alteracao(substr(:old.IE_TRANSF_N_F,1,4000),substr(:new.IE_TRANSF_N_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSF_N_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GEMELAR_F,1,500);gravar_log_alteracao(substr(:old.IE_GEMELAR_F,1,4000),substr(:new.IE_GEMELAR_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_GEMELAR_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_BARRA_GERADO,1,500);gravar_log_alteracao(substr(:old.IE_BARRA_GERADO,1,4000),substr(:new.IE_BARRA_GERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BARRA_GERADO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_MAE_F,1,500);gravar_log_alteracao(substr(:old.NM_MAE_F,1,4000),substr(:new.NM_MAE_F,1,4000),:new.nm_usuario,nr_seq_w,'NM_MAE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MEDICO_RESP,1,500);gravar_log_alteracao(substr(:old.CD_MEDICO_RESP,1,4000),substr(:new.CD_MEDICO_RESP,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_RESP',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOTE_SEC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_SEC,1,4000),substr(:new.NR_SEQ_LOTE_SEC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_SEC',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_PRONTUARIO_EX,1,500);gravar_log_alteracao(substr(:old.NR_PRONTUARIO_EX,1,4000),substr(:new.NR_PRONTUARIO_EX,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONTUARIO_EX',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_USUARIO_CONV_EXT,1,500);gravar_log_alteracao(substr(:old.CD_USUARIO_CONV_EXT,1,4000),substr(:new.CD_USUARIO_CONV_EXT,1,4000),:new.nm_usuario,nr_seq_w,'CD_USUARIO_CONV_EXT',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_FICHA_EXT,1,500);gravar_log_alteracao(substr(:old.NR_FICHA_EXT,1,4000),substr(:new.NR_FICHA_EXT,1,4000),:new.nm_usuario,nr_seq_w,'NR_FICHA_EXT',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SUSP_AM_INAD,1,500);gravar_log_alteracao(substr(:old.IE_SUSP_AM_INAD,1,4000),substr(:new.IE_SUSP_AM_INAD,1,4000),:new.nm_usuario,nr_seq_w,'IE_SUSP_AM_INAD',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_BARRAS,1,500);gravar_log_alteracao(substr(:old.CD_BARRAS,1,4000),substr(:new.CD_BARRAS,1,4000),:new.nm_usuario,nr_seq_w,'CD_BARRAS',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_CARTAO_NAC_SUS,1,500);gravar_log_alteracao(substr(:old.NR_CARTAO_NAC_SUS,1,4000),substr(:new.NR_CARTAO_NAC_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_NAC_SUS',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_FICHA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_FICHA,1,4000),substr(:new.IE_TIPO_FICHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FICHA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL_EXAME,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL_EXAME,1,4000),substr(:new.CD_MATERIAL_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_EXAME',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FICHA_ADICIONAL,1,500);gravar_log_alteracao(substr(:old.IE_FICHA_ADICIONAL,1,4000),substr(:new.IE_FICHA_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FICHA_ADICIONAL',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS_FICHA,1,500);gravar_log_alteracao(substr(:old.IE_STATUS_FICHA,1,4000),substr(:new.IE_STATUS_FICHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_FICHA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_MOT_INADEQ,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MOT_INADEQ,1,4000),substr(:new.NR_SEQ_MOT_INADEQ,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOT_INADEQ',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MOTIVO_INADEQ,1,500);gravar_log_alteracao(substr(:old.DS_MOTIVO_INADEQ,1,4000),substr(:new.DS_MOTIVO_INADEQ,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_INADEQ',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ENTIDADE_F,1,500);gravar_log_alteracao(substr(:old.CD_ENTIDADE_F,1,4000),substr(:new.CD_ENTIDADE_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_ENTIDADE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_LOTE_F,1,500);gravar_log_alteracao(substr(:old.CD_LOTE_F,1,4000),substr(:new.CD_LOTE_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOTE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_EXAME_F,1,500);gravar_log_alteracao(substr(:old.CD_EXAME_F,1,4000),substr(:new.CD_EXAME_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXAME_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PREMATURO_F,1,500);gravar_log_alteracao(substr(:old.IE_PREMATURO_F,1,4000),substr(:new.IE_PREMATURO_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREMATURO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TRANSFUSAO_F,1,500);gravar_log_alteracao(substr(:old.IE_TRANSFUSAO_F,1,4000),substr(:new.IE_TRANSFUSAO_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSFUSAO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_COLETA_F,1,500);gravar_log_alteracao(substr(:old.DT_COLETA_F,1,4000),substr(:new.DT_COLETA_F,1,4000),:new.nm_usuario,nr_seq_w,'DT_COLETA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_TESTE_F,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_TESTE_F,1,4000),substr(:new.IE_TIPO_TESTE_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TESTE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_REPETICAO_EXAME_F,1,500);gravar_log_alteracao(substr(:old.DS_REPETICAO_EXAME_F,1,4000),substr(:new.DS_REPETICAO_EXAME_F,1,4000),:new.nm_usuario,nr_seq_w,'DS_REPETICAO_EXAME_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ENTRADA_F,1,500);gravar_log_alteracao(substr(:old.CD_ENTRADA_F,1,4000),substr(:new.CD_ENTRADA_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_ENTRADA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_EXAME_FICHA_F,1,500);gravar_log_alteracao(substr(:old.CD_EXAME_FICHA_F,1,4000),substr(:new.CD_EXAME_FICHA_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXAME_FICHA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PRONTUARIO_F,1,500);gravar_log_alteracao(substr(:old.CD_PRONTUARIO_F,1,4000),substr(:new.CD_PRONTUARIO_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRONTUARIO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_RN_F,1,500);gravar_log_alteracao(substr(:old.NM_RN_F,1,4000),substr(:new.NM_RN_F,1,4000),:new.nm_usuario,nr_seq_w,'NM_RN_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_DNV_F,1,500);gravar_log_alteracao(substr(:old.CD_DNV_F,1,4000),substr(:new.CD_DNV_F,1,4000),:new.nm_usuario,nr_seq_w,'CD_DNV_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_COLETA_FICHA_F,1,500);gravar_log_alteracao(substr(:old.DT_COLETA_FICHA_F,1,4000),substr(:new.DT_COLETA_FICHA_F,1,4000),:new.nm_usuario,nr_seq_w,'DT_COLETA_FICHA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_EXTERNA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_EXTERNA,1,4000),substr(:new.CD_PESSOA_EXTERNA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_EXTERNA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AMOSTRA,1,500);gravar_log_alteracao(substr(:old.IE_AMOSTRA,1,4000),substr(:new.IE_AMOSTRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AMOSTRA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_MAT_LOTE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_MAT_LOTE,1,4000),substr(:new.NR_SEQ_TIPO_MAT_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_MAT_LOTE',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_PRESCRICAO,1,500);gravar_log_alteracao(substr(:old.NR_PRESCRICAO,1,4000),substr(:new.NR_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRESCRICAO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_VALIDADE,1,500);gravar_log_alteracao(substr(:old.DT_VALIDADE,1,4000),substr(:new.DT_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_VALIDADE',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_GRAU_PARENTESCO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_GRAU_PARENTESCO,1,4000),substr(:new.NR_SEQ_GRAU_PARENTESCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRAU_PARENTESCO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_FABRICACAO,1,500);gravar_log_alteracao(substr(:old.DT_FABRICACAO,1,4000),substr(:new.DT_FABRICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_FABRICACAO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_LOTE_FICHA,1,500);gravar_log_alteracao(substr(:old.NR_LOTE_FICHA,1,4000),substr(:new.NR_LOTE_FICHA,1,4000),:new.nm_usuario,nr_seq_w,'NR_LOTE_FICHA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.HR_COLETA_F,1,500);gravar_log_alteracao(substr(:old.HR_COLETA_F,1,4000),substr(:new.HR_COLETA_F,1,4000),:new.nm_usuario,nr_seq_w,'HR_COLETA_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.HR_NASCIMENTO_F,1,500);gravar_log_alteracao(substr(:old.HR_NASCIMENTO_F,1,4000),substr(:new.HR_NASCIMENTO_F,1,4000),:new.nm_usuario,nr_seq_w,'HR_NASCIMENTO_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FICHA_URG,1,500);gravar_log_alteracao(substr(:old.IE_FICHA_URG,1,4000),substr(:new.IE_FICHA_URG,1,4000),:new.nm_usuario,nr_seq_w,'IE_FICHA_URG',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SUSPENSO,1,500);gravar_log_alteracao(substr(:old.IE_SUSPENSO,1,4000),substr(:new.IE_SUSPENSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SUSPENSO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CORTICOIDE_F,1,500);gravar_log_alteracao(substr(:old.IE_CORTICOIDE_F,1,4000),substr(:new.IE_CORTICOIDE_F,1,4000),:new.nm_usuario,nr_seq_w,'IE_CORTICOIDE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CORTICOIDE_F,1,500);gravar_log_alteracao(substr(:old.DS_CORTICOIDE_F,1,4000),substr(:new.DS_CORTICOIDE_F,1,4000),:new.nm_usuario,nr_seq_w,'DS_CORTICOIDE_F',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MOTIVO_BAIXA,1,500);gravar_log_alteracao(substr(:old.CD_MOTIVO_BAIXA,1,4000),substr(:new.CD_MOTIVO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_BAIXA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_FICHA_ANT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_FICHA_ANT,1,4000),substr(:new.NR_SEQ_FICHA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FICHA_ANT',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOCAL,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOCAL,1,4000),substr(:new.NR_SEQ_LOCAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCAL',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOCAL_CAIXA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOCAL_CAIXA,1,4000),substr(:new.NR_SEQ_LOCAL_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCAL_CAIXA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOTE_ENT_SEC_FICHA ADD (
  CONSTRAINT LESECFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENT_SEC_FICHA ADD (
  CONSTRAINT LESECFI_LESEC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_SEC) 
 REFERENCES TASY.LOTE_ENT_SECRETARIA (NR_SEQUENCIA),
  CONSTRAINT LESECFI_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT LESECFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LESECFI_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LESECFI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT LESECFI_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT LESECFI_LOTEENTMAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MAT_LOTE) 
 REFERENCES TASY.LOTE_ENT_TIPO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT LESECFI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT LESECFI_LELOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.LOTE_ENT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT LESECFI_LELOCAX_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_CAIXA) 
 REFERENCES TASY.LOTE_ENT_LOCAL_CAIXA (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_ENT_SEC_FICHA TO NIVEL_1;

