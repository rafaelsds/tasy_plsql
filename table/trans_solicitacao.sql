ALTER TABLE TASY.TRANS_SOLICITACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRANS_SOLICITACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRANS_SOLICITACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_SETOR_ORIGEM          NUMBER(5),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  NR_ATENDIMENTO           NUMBER(10),
  DS_OBS_CANCELAMENTO      VARCHAR2(255 BYTE),
  CD_MOTIVO_CANCELAMENTO   NUMBER(10),
  DT_CANCELAMENTO          DATE,
  CD_TRANSPORTADOR         VARCHAR2(10 BYTE),
  CD_EQUIPAMENTO           NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  CD_SOLICITANTE           VARCHAR2(10 BYTE),
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_STATUS                NUMBER(1),
  DT_LIBERACAO             DATE,
  IE_REGRA_PRIORIDADE      VARCHAR2(1 BYTE),
  CD_SETOR_DESTINO         NUMBER(5),
  NR_RAMAL                 VARCHAR2(20 BYTE),
  NR_PRONTUARIO            NUMBER(10),
  IE_OXIGENIO              VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_REMOCAO      NUMBER(10),
  DS_CONDICAO_CLINICA_PAC  VARCHAR2(2000 BYTE),
  IE_MED_BOMBA_INFUSAO     VARCHAR2(1 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  DT_PREVISTA              DATE,
  NR_SEQ_LOCAL_EXTERNO     NUMBER(10),
  NM_CONTATO               VARCHAR2(100 BYTE),
  NR_TELEFONE_CONTATO      VARCHAR2(40 BYTE),
  NM_ACOMP_PAC             VARCHAR2(100 BYTE),
  DS_LOCALIDADE            VARCHAR2(255 BYTE),
  IE_TIPO_SOLICITACAO      VARCHAR2(3 BYTE),
  IE_PAC_VULNERAVEL        VARCHAR2(1 BYTE),
  QT_PESO_PAC              NUMBER(10,3),
  CD_CNPJ_DESTINO          VARCHAR2(14 BYTE),
  NR_DDI_CONTATO           VARCHAR2(3 BYTE),
  CD_UNIDADE_BASICA_ORIG   VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL_ORIG    VARCHAR2(10 BYTE),
  CD_UNIDADE_BASICA_DEST   VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL_DEST    VARCHAR2(10 BYTE),
  IE_CINTO                 VARCHAR2(1 BYTE),
  IE_CADEIRA_RODAS         VARCHAR2(1 BYTE),
  IE_MACA                  VARCHAR2(1 BYTE),
  IE_PACIENTE_ISOLADO      VARCHAR2(1 BYTE),
  IE_EM_QUIMIO             VARCHAR2(1 BYTE),
  DT_SOLICITACAO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TRANSSO_MANEQUI_FK_I ON TASY.TRANS_SOLICITACAO
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_PESFISI_FK_I ON TASY.TRANS_SOLICITACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRANSSO_PESFISI_FK2_I ON TASY.TRANS_SOLICITACAO
(CD_TRANSPORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRANSSO_PESFISI_FK3_I ON TASY.TRANS_SOLICITACAO
(CD_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRANSSO_PESJURI_FK_I ON TASY.TRANS_SOLICITACAO
(CD_CNPJ_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TRANSSO_PK ON TASY.TRANS_SOLICITACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRANSSO_SETATEN_FK_I ON TASY.TRANS_SOLICITACAO
(CD_SETOR_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_SETATEN_FK2_I ON TASY.TRANS_SOLICITACAO
(CD_SETOR_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_TRAMOCA_FK_I ON TASY.TRANS_SOLICITACAO
(CD_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_TRAMOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_TRANSLEX_FK_I ON TASY.TRANS_SOLICITACAO
(NR_SEQ_LOCAL_EXTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_TRANSLEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_TRANSTR_FK_I ON TASY.TRANS_SOLICITACAO
(NR_SEQ_TIPO_REMOCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRANSSO_TRANSTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRANSSO_UNIATEN_FK_I ON TASY.TRANS_SOLICITACAO
(CD_SETOR_ORIGEM, CD_UNIDADE_BASICA_ORIG, CD_UNIDADE_COMPL_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRANSSO_UNIATEN_FK2_I ON TASY.TRANS_SOLICITACAO
(CD_SETOR_DESTINO, CD_UNIDADE_BASICA_DEST, CD_UNIDADE_COMPL_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.trans_solicitacao_bfinsert
BEFORE INSERT ON TASY.TRANS_SOLICITACAO FOR EACH ROW
DECLARE

nr_sequencia_w	number(10);
nr_idade_pf_w	number(10);

BEGIN

/* Gravar histórico de status da solicitação de transporte */
select	trans_historico_seq.nextval
into	nr_sequencia_w
from	dual;

insert into trans_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_solicitacao,
	ie_status,
	dt_inicio,
	dt_final)
values (nr_sequencia_w,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_sequencia,
	:new.ie_status,
	sysdate,
	null);

/*  Tratar se a solicitação se encaixa em alguma regra de prioridade */
nr_idade_pf_w := Obter_Idade_PF(:new.cd_pessoa_fisica, sysdate, 'A');

begin
	select	nr_sequencia
	into	nr_sequencia_w
	from	trans_regra_prioridade
	where	cd_setor_atendimento = :new.cd_setor_origem
	and	nr_idade_pf_w < qt_menor_faixa
	and	nr_idade_pf_w > qt_maior_faixa;
exception
	when	no_data_found then
		nr_sequencia_w := 0;
end;

If	(nr_sequencia_w > 0) then
	:new.ie_regra_prioridade := 'S';
else
	:new.ie_regra_prioridade := 'N';
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.trans_solicitacao_update
BEFORE UPDATE ON TASY.TRANS_SOLICITACAO FOR EACH ROW
DECLARE

nr_sequencia_w			number(10);

BEGIN

select	trans_historico_seq.nextval
into	nr_sequencia_w
from	dual;

if	(:new.ie_status	<>	:old.ie_status) then
	begin
	insert into trans_historico(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_solicitacao,
				ie_status,
				dt_inicio,
				dt_final)
			values (
				nr_sequencia_w,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.nr_sequencia,
				:new.ie_status,
				sysdate,
				null);

	update	trans_historico
	set	dt_final 		= sysdate,
		nm_usuario 		= :new.nm_usuario
	where	nr_seq_solicitacao	= :new.nr_sequencia
	and	dt_final		is null
	and	ie_status 	 <> :new.ie_status
	and	nr_sequencia		= (	select	max(b.nr_sequencia)
						from	trans_historico b
						where	b.NR_SEQ_SOLICITACAO	= :new.nr_sequencia
						and	b.nr_sequencia	<> nr_sequencia_w);

	if (:new.ie_status = 3) then
		update	transportador
		set		ie_status_recurso = 2
		where	cd_pessoa_fisica = :new.cd_transportador;
	end if;

	if (:new.ie_status = 8) then
		update	transportador
		set		ie_status_recurso = 1
		where	cd_pessoa_fisica = :new.cd_transportador;
	end if;

	end;
end if;


END;
/


ALTER TABLE TASY.TRANS_SOLICITACAO ADD (
  CONSTRAINT TRANSSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRANS_SOLICITACAO ADD (
  CONSTRAINT TRANSSO_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_DESTINO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TRANSSO_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ORIGEM, CD_UNIDADE_BASICA_ORIG, CD_UNIDADE_COMPL_ORIG) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL),
  CONSTRAINT TRANSSO_UNIATEN_FK2 
 FOREIGN KEY (CD_SETOR_DESTINO, CD_UNIDADE_BASICA_DEST, CD_UNIDADE_COMPL_DEST) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL),
  CONSTRAINT TRANSSO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TRANSSO_PESFISI_FK2 
 FOREIGN KEY (CD_TRANSPORTADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TRANSSO_PESFISI_FK3 
 FOREIGN KEY (CD_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TRANSSO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_DESTINO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TRANSSO_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ORIGEM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TRANSSO_TRAMOCA_FK 
 FOREIGN KEY (CD_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.TRANS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT TRANSSO_TRANSLEX_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_EXTERNO) 
 REFERENCES TASY.TRANS_LOCAL_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT TRANSSO_TRANSTR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_REMOCAO) 
 REFERENCES TASY.TRANS_TIPO_REMOCAO (NR_SEQUENCIA),
  CONSTRAINT TRANSSO_MANEQUI_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRANS_SOLICITACAO TO NIVEL_1;

