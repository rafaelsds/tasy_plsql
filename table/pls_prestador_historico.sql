ALTER TABLE TASY.PLS_PRESTADOR_HISTORICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_HISTORICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_HISTORICO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_HISTORICO          DATE                    NOT NULL,
  NM_USUARIO_HISTORICO  VARCHAR2(60 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_HISTORICO          VARCHAR2(4000 BYTE),
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(60 BYTE),
  NR_SEQ_PRESTADOR      NUMBER(10)              NOT NULL,
  NR_SEQ_TIPO_HIST      NUMBER(10),
  IE_EXIBE_PRESTADOR    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPREH_PK ON TASY.PLS_PRESTADOR_HISTORICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREH_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPREH_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_HISTORICO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREH_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPREH_PLSTIHP_FK_I ON TASY.PLS_PRESTADOR_HISTORICO
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREH_PLSTIHP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PRESTADOR_HISTORICO ADD (
  CONSTRAINT PLSPREH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_HISTORICO ADD (
  CONSTRAINT PLSPREH_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPREH_PLSTIHP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.PLS_TIPO_HIST_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_HISTORICO TO NIVEL_1;

