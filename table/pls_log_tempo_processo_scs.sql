ALTER TABLE TASY.PLS_LOG_TEMPO_PROCESSO_SCS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOG_TEMPO_PROCESSO_SCS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOG_TEMPO_PROCESSO_SCS
(
  NR_SEQUENCIA   NUMBER(10)                     NOT NULL,
  DS_CONTEUDO    VARCHAR2(4000 BYTE),
  DT_INICIO      DATE,
  DT_FIM         DATE,
  DS_TEMPO_EXEC  VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLTPS_I1 ON TASY.PLS_LOG_TEMPO_PROCESSO_SCS
(DT_INICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSLTPS_PK ON TASY.PLS_LOG_TEMPO_PROCESSO_SCS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOG_TEMPO_PROCESSO_SCS ADD (
  CONSTRAINT PLSLTPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_LOG_TEMPO_PROCESSO_SCS TO NIVEL_1;

