ALTER TABLE TASY.PROC_PARTIC_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_PARTIC_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_PARTIC_VALOR
(
  NR_SEQ_PROCEDIMENTO  NUMBER(10)               NOT NULL,
  NR_SEQ_PARTIC        NUMBER(10)               NOT NULL,
  NR_SEQUENCIA         NUMBER(6)                NOT NULL,
  IE_TIPO_VALOR        NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_PARTICIPANTE      NUMBER(15,2)             NOT NULL,
  VL_CONTA             NUMBER(15,2)             NOT NULL,
  PR_VALOR             NUMBER(7,4)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROPAVL_PK ON TASY.PROC_PARTIC_VALOR
(NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPAVL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROPAVL_PROPART_FK_I ON TASY.PROC_PARTIC_VALOR
(NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROC_PARTIC_VALOR ADD (
  CONSTRAINT PROPAVL_PK
 PRIMARY KEY
 (NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_PARTIC_VALOR ADD (
  CONSTRAINT PROPAVL_PROPART_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC) 
 REFERENCES TASY.PROCEDIMENTO_PARTICIPANTE (NR_SEQUENCIA,NR_SEQ_PARTIC)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROC_PARTIC_VALOR TO NIVEL_1;

