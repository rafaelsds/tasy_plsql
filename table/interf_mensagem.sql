ALTER TABLE TASY.INTERF_MENSAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERF_MENSAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERF_MENSAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROJETO       NUMBER(10)               NOT NULL,
  DS_MENSAGEM_ORIG     VARCHAR2(255 BYTE)       NOT NULL,
  DS_MENSAGEM_PORT     VARCHAR2(255 BYTE),
  DS_MENSAGEM_ING      VARCHAR2(255 BYTE),
  DS_ORIENTACOES       VARCHAR2(4000 BYTE),
  IE_BANDA_SEG         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INTMENS_PK ON TASY.INTERF_MENSAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTMENS_PROJINT_FK_I ON TASY.INTERF_MENSAGEM
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTERF_MENSAGEM ADD (
  CONSTRAINT INTMENS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERF_MENSAGEM ADD (
  CONSTRAINT INTMENS_PROJINT_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJETO_INTERFACE (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTERF_MENSAGEM TO NIVEL_1;

