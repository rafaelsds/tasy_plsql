ALTER TABLE TASY.CID_DOENCA_GM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CID_DOENCA_GM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CID_DOENCA_GM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CARGA           NUMBER(10)             NOT NULL,
  DT_INICIO_PROC         DATE,
  DT_FIM_PROC            DATE,
  NR_SEQ_CARGA_ARQ       NUMBER(10)             NOT NULL,
  CD_CLASSIFICACAO       VARCHAR2(1 BYTE),
  CD_CHAVE               VARCHAR2(1 BYTE),
  CD_TIPO_POSICAO        VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE       VARCHAR2(2 BYTE),
  CD_CATEGORIA_INICIAL   VARCHAR2(3 BYTE),
  CD_CID_DOENCA          VARCHAR2(7 BYTE),
  CD_CHAVE1              VARCHAR2(6 BYTE),
  CD_CHAVE2              VARCHAR2(5 BYTE),
  DS_CID_DOENCA          VARCHAR2(255 BYTE),
  DS_CID_DOENCA3         VARCHAR2(255 BYTE),
  DS_CID_DOENCA4         VARCHAR2(255 BYTE),
  DS_CID_DOENCA5         VARCHAR2(255 BYTE),
  P295                   VARCHAR2(1 BYTE),
  P301                   VARCHAR2(1 BYTE),
  CD_MORTALIDADE1        VARCHAR2(5 BYTE),
  CD_MORTALIDADE2        VARCHAR2(5 BYTE),
  CD_MORTALIDADE3        VARCHAR2(5 BYTE),
  CD_MORTALIDADE4        VARCHAR2(5 BYTE),
  CD_MORBIDADE           VARCHAR2(5 BYTE),
  IE_SEXO                VARCHAR2(1 BYTE),
  CD_ERRO_SEXO           VARCHAR2(1 BYTE),
  NR_MENOR_IDADE_LIMITE  VARCHAR2(4 BYTE),
  NR_MAIOR_IDADE_LIMITE  VARCHAR2(4 BYTE),
  CD_ERRO_IDADE          VARCHAR2(1 BYTE),
  CD_DOENCA_RARA         VARCHAR2(1 BYTE),
  CD_CONTEUDO_OCUPADO    VARCHAR2(1 BYTE),
  CD_IFSG                VARCHAR2(1 BYTE),
  CD_IFSGLABOR           VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA     VARCHAR2(30 BYTE),
  DT_FINAL_VIGENCIA      VARCHAR2(30 BYTE),
  CD_VERSAO              VARCHAR2(40 BYTE),
  IE_STATUS              VARCHAR2(10 BYTE),
  NR_LINHA               NUMBER(10),
  DS_CHAVE_TASY          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIDDOEGM_GERCARI_FK_I ON TASY.CID_DOENCA_GM
(NR_SEQ_CARGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIDDOEGM_GERCGARQ_FK_I ON TASY.CID_DOENCA_GM
(NR_SEQ_CARGA_ARQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIDDOEGM_PK ON TASY.CID_DOENCA_GM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CID_DOENCA_GM ADD (
  CONSTRAINT CIDDOEGM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CID_DOENCA_GM ADD (
  CONSTRAINT CIDDOEGM_GERCARI_FK 
 FOREIGN KEY (NR_SEQ_CARGA) 
 REFERENCES TASY.GER_CARGA_INICIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIDDOEGM_GERCGARQ_FK 
 FOREIGN KEY (NR_SEQ_CARGA_ARQ) 
 REFERENCES TASY.GER_CARGA_ARQ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CID_DOENCA_GM TO NIVEL_1;

