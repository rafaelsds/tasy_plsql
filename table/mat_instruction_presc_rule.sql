ALTER TABLE TASY.MAT_INSTRUCTION_PRESC_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_INSTRUCTION_PRESC_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_INSTRUCTION_PRESC_RULE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  SI_INSTRUCTION_TYPE  VARCHAR2(3 BYTE),
  DT_START             DATE                     NOT NULL,
  DT_END               DATE,
  SI_ACTION            VARCHAR2(3 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATIPRR_ESTABEL_FK_I ON TASY.MAT_INSTRUCTION_PRESC_RULE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATIPRR_MATERIA_FK_I ON TASY.MAT_INSTRUCTION_PRESC_RULE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATIPRR_PK ON TASY.MAT_INSTRUCTION_PRESC_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAT_INSTRUCTION_PRESC_RULE ADD (
  CONSTRAINT MATIPRR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MAT_INSTRUCTION_PRESC_RULE ADD (
  CONSTRAINT MATIPRR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATIPRR_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

