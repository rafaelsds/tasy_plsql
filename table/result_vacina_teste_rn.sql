ALTER TABLE TASY.RESULT_VACINA_TESTE_RN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_VACINA_TESTE_RN CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_VACINA_TESTE_RN
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_RESULTADO          VARCHAR2(80 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_VACINA_TESTE   NUMBER(10),
  NR_SEQ_APRES          NUMBER(3)               NOT NULL,
  IE_CAT_NOM            VARCHAR2(10 BYTE),
  IE_RESULTADO_AUDICAO  NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REVATER_PK ON TASY.RESULT_VACINA_TESTE_RN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REVATER_VATESRN_FK_I ON TASY.RESULT_VACINA_TESTE_RN
(NR_SEQ_VACINA_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RESULT_VACINA_TESTE_RN ADD (
  CONSTRAINT REVATER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULT_VACINA_TESTE_RN ADD (
  CONSTRAINT REVATER_VATESRN_FK 
 FOREIGN KEY (NR_SEQ_VACINA_TESTE) 
 REFERENCES TASY.VACINA_TESTE_RN (NR_SEQUENCIA));

GRANT SELECT ON TASY.RESULT_VACINA_TESTE_RN TO NIVEL_1;

