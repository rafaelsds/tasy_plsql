ALTER TABLE TASY.ESCALA_MAN_LF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MAN_LF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MAN_LF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PESO                    NUMBER(10,3)       NOT NULL,
  QT_ALTURA                  NUMBER(3)          NOT NULL,
  QT_CIRCUN_PANTURRILHA      NUMBER(15,2),
  IE_DIMINUICAO_ALIMENTAR    NUMBER(3),
  IE_PERDA_PESO              NUMBER(3),
  IE_MOBILIDADE              NUMBER(3),
  IE_ESTRESSE                NUMBER(3),
  IE_NEUROPSICOLOGICOS       NUMBER(3),
  QT_PONTUACAO               NUMBER(5,2),
  QT_IMC                     NUMBER(4,1),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMANL_ATEPACI_FK_I ON TASY.ESCALA_MAN_LF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMANL_EHRREEL_FK_I ON TASY.ESCALA_MAN_LF
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMANL_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMANL_PESFISI_FK_I ON TASY.ESCALA_MAN_LF
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMANL_PK ON TASY.ESCALA_MAN_LF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMANL_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCMANL_TASASDI_FK_I ON TASY.ESCALA_MAN_LF
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMANL_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMANL_TASASDI_FK2_I ON TASY.ESCALA_MAN_LF
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMANL_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_man_lf_pend_atual
after insert or update ON TASY.ESCALA_MAN_LF for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'139');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MAN_LF_delete
after delete ON TASY.ESCALA_MAN_LF for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '139'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.escala_man_lf_atual
before insert or update ON TASY.ESCALA_MAN_LF for each row
declare
qt_imc_w		number(10,2);
begin


:new.qt_pontuacao	:= 0;

qt_imc_w	:= dividir(:new.qt_peso,(:new.qt_altura /100) * (:new.qt_altura /100));

:new.qt_imc	:= qt_imc_w;

if	(qt_imc_w	>=19) and
	(qt_imc_w	<21) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
elsif	(qt_imc_w	>=21) and
	(qt_imc_w	< 23) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
elsif	(qt_imc_w	>=23) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.qt_circun_panturrilha	>=31) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

:new.qt_pontuacao	:= :new.qt_pontuacao + nvl(:new.IE_ESTRESSE,0)+ nvl(:new.IE_DIMINUICAO_ALIMENTAR,0)+ nvl(:new.IE_PERDA_PESO,0)+ nvl(:new.IE_MOBILIDADE,0)+ nvl(:new.IE_NEUROPSICOLOGICOS,0);

end;
/


ALTER TABLE TASY.ESCALA_MAN_LF ADD (
  CONSTRAINT ESCMANL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MAN_LF ADD (
  CONSTRAINT ESCMANL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMANL_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMANL_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCMANL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMANL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_MAN_LF TO NIVEL_1;

