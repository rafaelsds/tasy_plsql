ALTER TABLE TASY.PLS_OC_CTA_VAL_NSENHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_NSENHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_NSENHA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB    NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA_NSENHA   NUMBER(10),
  IE_TIPO_SENHA         VARCHAR2(5 BYTE),
  IE_SENHA_DIFERENTE    VARCHAR2(5 BYTE),
  QT_MIN_CARACTERES     NUMBER(7),
  DS_CARAC_VALIDO       VARCHAR2(255 BYTE),
  IE_SENHA_AUTORIZADA   VARCHAR2(3 BYTE),
  IE_VALIDADE_EXPIRADA  VARCHAR2(3 BYTE),
  IE_DATA_BASE_VAL      VARCHAR2(3 BYTE),
  IE_COMPARAR_ORIGEM    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCNS_PK ON TASY.PLS_OC_CTA_VAL_NSENHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCNS_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_NSENHA
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCNS_PLSRNSH_FK_I ON TASY.PLS_OC_CTA_VAL_NSENHA
(NR_SEQ_REGRA_NSENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_NSENHA_tp  after update ON TASY.PLS_OC_CTA_VAL_NSENHA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_SENHA,1,4000),substr(:new.IE_TIPO_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SENHA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_NSENHA,1,4000),substr(:new.NR_SEQ_REGRA_NSENHA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_NSENHA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_DIFERENTE,1,4000),substr(:new.IE_SENHA_DIFERENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_DIFERENTE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDADE_EXPIRADA,1,4000),substr(:new.IE_VALIDADE_EXPIRADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDADE_EXPIRADA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CARAC_VALIDO,1,4000),substr(:new.DS_CARAC_VALIDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CARAC_VALIDO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DATA_BASE_VAL,1,4000),substr(:new.IE_DATA_BASE_VAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_BASE_VAL',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_AUTORIZADA,1,4000),substr(:new.IE_SENHA_AUTORIZADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_AUTORIZADA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_CARACTERES,1,4000),substr(:new.QT_MIN_CARACTERES,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_CARACTERES',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NSENHA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_NSENHA ADD (
  CONSTRAINT PLSOCNS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_NSENHA ADD (
  CONSTRAINT PLSOCNS_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSOCNS_PLSRNSH_FK 
 FOREIGN KEY (NR_SEQ_REGRA_NSENHA) 
 REFERENCES TASY.PLS_REGRA_NSENHA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_NSENHA TO NIVEL_1;

