DROP TABLE TASY.CONCURRENT_USERS_T CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.CONCURRENT_USERS_T
(
  DT_WHEN   DATE,
  NR_COUNT  NUMBER(8)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.CONCURRENT_USERS_T TO NIVEL_1;

