ALTER TABLE TASY.KV_ATESTADO_CRIANCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.KV_ATESTADO_CRIANCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.KV_ATESTADO_CRIANCA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DS_ENDERECO_SEGURADO   VARCHAR2(50 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  NR_ZIP_CODE_SEGURADO   NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NM_CIDADE_SEGURADO     VARCHAR2(30 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  DS_PESSOA_SEGURADO     VARCHAR2(255 BYTE),
  IE_PAGAMENTO           VARCHAR2(1 BYTE),
  DT_INICIO              DATE,
  IE_MUSTER              VARCHAR2(2 BYTE)       NOT NULL,
  NM_TITULAR_CONTA       VARCHAR2(50 BYTE),
  CD_CONTA_BANCARIA      VARCHAR2(22 BYTE),
  CD_BANCO_TITULAR       VARCHAR2(11 BYTE),
  NM_INSTITUICAO_FIN     VARCHAR2(30 BYTE),
  DT_FIM                 DATE,
  IE_ACIDENTE            VARCHAR2(1 BYTE),
  DT_NASC_SEGURADO       DATE,
  IE_NECESSARIO          VARCHAR2(1 BYTE),
  NR_SEGURADO            NUMBER(10),
  IE_NAO_PAGAMENTO       VARCHAR2(1 BYTE),
  IE_CUIDADO             VARCHAR2(1 BYTE),
  QT_PAGAMENTO           NUMBER(5),
  IE_PAI_SOLTEIRO        VARCHAR2(1 BYTE),
  QT_DIAS_CUIDADO        NUMBER(10),
  IE_NAO_CUIDADO         VARCHAR2(1 BYTE),
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_RQE                 VARCHAR2(20 BYTE),
  NR_BSNR                VARCHAR2(25 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.KVATCRI_ATEPACI_FK_I ON TASY.KV_ATESTADO_CRIANCA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVATCRI_PESFISI_FK_I ON TASY.KV_ATESTADO_CRIANCA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.KVATCRI_PK ON TASY.KV_ATESTADO_CRIANCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVATCRI_PROFISS_FK_I ON TASY.KV_ATESTADO_CRIANCA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.KV_ATESTADO_CRIANCA_UPDATE
before update ON TASY.KV_ATESTADO_CRIANCA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into is_rule_exists_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV21'
    and   obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
		nvl(b.nr_sequencia, 0)
    into qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV21'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


		wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
    end if;
	end if;
end;
/


ALTER TABLE TASY.KV_ATESTADO_CRIANCA ADD (
  CONSTRAINT KVATCRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.KV_ATESTADO_CRIANCA ADD (
  CONSTRAINT KVATCRI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT KVATCRI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT KVATCRI_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.KV_ATESTADO_CRIANCA TO NIVEL_1;

