DROP TABLE TASY.W_GERAR_VALORES_SUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_GERAR_VALORES_SUS
(
  QT_AIH                 NUMBER(15,4),
  VL_SSM                 NUMBER(15,4),
  VL_SP                  NUMBER(15,4),
  VL_SADT                NUMBER(15,4),
  VL_NEURO               NUMBER(15,4),
  VL_TRANSPL             NUMBER(15,4),
  VL_ANALGESIA           NUMBER(15,4),
  VL_COMPONENTE          NUMBER(15,4),
  VL_NOTIF               NUMBER(15,4),
  VL_UTI_ESP             NUMBER(15,4),
  VL_OPM                 NUMBER(15,4),
  VL_SANGUE              NUMBER(15,4),
  VL_RN                  NUMBER(15,4),
  VL_UTI                 NUMBER(15,4),
  VL_ACOMP               NUMBER(15,4),
  VL_NUT_ENTERAL         NUMBER(15,4),
  VL_REGCIVIL            NUMBER(15,4),
  VL_HIV                 NUMBER(15,4),
  VL_TOTAL               NUMBER(15,4),
  QT_PTO_SP              NUMBER(15,4),
  QT_PTO_SADT            NUMBER(15,4),
  VL_PTO_SP              NUMBER(15,4),
  VL_PTO_SADT            NUMBER(15,4),
  IE_ESPECIALIDADE_AIH   NUMBER(15,4),
  NR_SEQ                 NUMBER(15,4),
  NR_AIH                 NUMBER(17,4),
  CD_PROCEDIMENTO_SOLIC  NUMBER(15),
  DT_ALTA                DATE,
  QT_DIA_UTI             NUMBER(15,4),
  QT_DIA_ACOMPANHANTE    NUMBER(15,4),
  NM_PESSOA              VARCHAR2(60 BYTE),
  NR_FAEC                NUMBER(10),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  VL_CATETERISMO         NUMBER(15,4),
  VL_RENAL_CRONICO       NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_GERAR_VALORES_SUS TO NIVEL_1;

