ALTER TABLE TASY.ADIANT_PAGO_DEV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADIANT_PAGO_DEV CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADIANT_PAGO_DEV
(
  NR_ADIANTAMENTO       NUMBER(10)              NOT NULL,
  NR_SEQUENCIA          NUMBER(5)               NOT NULL,
  DT_DEVOLUCAO          DATE                    NOT NULL,
  VL_DEVOLUCAO          NUMBER(13,2)            NOT NULL,
  CD_MOEDA              NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_MOTIVO_DEV         VARCHAR2(255 BYTE),
  NR_LOTE_CONTABIL      NUMBER(10),
  NR_SEQ_TRANS_FIN      NUMBER(10),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  VL_DEVOLUCAO_ESTRANG  NUMBER(13,2),
  VL_COTACAO            NUMBER(21,10),
  VL_COMPLEMENTO        NUMBER(15,2),
  NR_CODIGO_CONTROLE    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ADIAPADE_PK ON TASY.ADIANT_PAGO_DEV
(NR_ADIANTAMENTO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPADE_ADIPAGO_FK_I ON TASY.ADIANT_PAGO_DEV
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPADE_LOTCONT_FK_I ON TASY.ADIANT_PAGO_DEV
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPADE_TRAFINA_FK_I ON TASY.ADIANT_PAGO_DEV
(NR_SEQ_TRANS_FIN)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPADE_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.adiant_pago_dev_after
after insert ON TASY.ADIANT_PAGO_DEV for each row
declare

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(nvl(:new.vl_devolucao, 0) <> 0) then
	begin

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	adiantamento_pago
	where	nr_adiantamento = :new.nr_adiantamento;

	ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
								:new.dt_devolucao,
								7,
								:new.nr_seq_trans_fin,
								17,
								:new.nr_adiantamento,
								:new.nr_sequencia,
								null,
								:new.vl_devolucao,
								'ADIANT_PAGO_DEV',
								'VL_ADIANT_DEV',
								:new.nm_usuario);

	end;
end if;
end if;

end;
/


ALTER TABLE TASY.ADIANT_PAGO_DEV ADD (
  CONSTRAINT ADIAPADE_PK
 PRIMARY KEY
 (NR_ADIANTAMENTO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADIANT_PAGO_DEV ADD (
  CONSTRAINT ADIPADE_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ADIPADE_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ADIPADE_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ADIANT_PAGO_DEV TO NIVEL_1;

