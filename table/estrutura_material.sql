ALTER TABLE TASY.ESTRUTURA_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESTRUTURA_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESTRUTURA_MATERIAL
(
  CD_ESTRUTURA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESTMATE_ESTPROD_FK_I ON TASY.ESTRUTURA_MATERIAL
(CD_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESTMATE_ESTPROD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESTMATE_MATERIA_FK_I ON TASY.ESTRUTURA_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESTMATE_PK ON TASY.ESTRUTURA_MATERIAL
(CD_ESTRUTURA, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESTMATE_PK
  MONITORING USAGE;


ALTER TABLE TASY.ESTRUTURA_MATERIAL ADD (
  CONSTRAINT ESTMATE_PK
 PRIMARY KEY
 (CD_ESTRUTURA, CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESTRUTURA_MATERIAL ADD (
  CONSTRAINT ESTMATE_ESTPROD_FK 
 FOREIGN KEY (CD_ESTRUTURA) 
 REFERENCES TASY.ESTRUTURA_PRODUTO (CD_ESTRUTURA)
    ON DELETE CASCADE,
  CONSTRAINT ESTMATE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESTRUTURA_MATERIAL TO NIVEL_1;

