ALTER TABLE TASY.PE_PROCEDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_PROCEDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_PROCEDIMENTO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_PROCEDIMENTO           VARCHAR2(255 BYTE)  NOT NULL,
  CD_INTERVALO              VARCHAR2(7 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_NIC                    NUMBER(15),
  DS_DEFINICAO              VARCHAR2(255 BYTE),
  IE_ORIGEM                 VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_ORIGEM             NUMBER(10)          NOT NULL,
  DS_HORARIO_PADRAO         VARCHAR2(255 BYTE),
  DS_OBSERVACAO_PADR        VARCHAR2(255 BYTE),
  IE_ADEP                   VARCHAR2(1 BYTE),
  IE_PADRAO                 VARCHAR2(1 BYTE),
  DS_PROCEDIMENTO_PESQUISA  VARCHAR2(255 BYTE),
  NR_SEQ_GRUPO              NUMBER(10),
  CD_ESTABELECIMENTO        NUMBER(4),
  IE_APRES_DISP             VARCHAR2(1 BYTE),
  IE_FAOSE                  VARCHAR2(15 BYTE),
  IE_PROFISSIONAL           VARCHAR2(15 BYTE),
  IE_OBRIGA_TOPOGRAFIA      VARCHAR2(1 BYTE),
  IE_AUXILIAR               VARCHAR2(15 BYTE),
  IE_ENCAMINHAR             VARCHAR2(15 BYTE),
  IE_FAZER                  VARCHAR2(15 BYTE),
  IE_ORIENTAR               VARCHAR2(15 BYTE),
  IE_SUPERVISIONAR          VARCHAR2(15 BYTE),
  IE_EVOLUCAO               VARCHAR2(1 BYTE),
  IE_PLANO_EDUCACIONAL      VARCHAR2(1 BYTE),
  QT_TEMPO_PREV             NUMBER(3),
  IE_EVOLUCAO_CLINICA       VARCHAR2(3 BYTE),
  NR_SEQ_TIPO               NUMBER(10),
  NR_SEQ_EVENTO             NUMBER(10),
  IE_FORMA_EV               VARCHAR2(15 BYTE),
  IE_NIVEL_COMPLEXIDADE     NUMBER(2),
  IE_SISTEMA_TERCEIRO       VARCHAR2(1 BYTE),
  DS_INTERVENCAO_ORIG       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPROCE_ESTABEL_FK_I ON TASY.PE_PROCEDIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPROCE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPROCE_EVEVENT_FK_I ON TASY.PE_PROCEDIMENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPROCE_GRUINTE_FK_I ON TASY.PE_PROCEDIMENTO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPROCE_GRUINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPROCE_INTPRES_FK_I ON TASY.PE_PROCEDIMENTO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPROCE_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPROCE_ORIGDIAG_FK_I ON TASY.PE_PROCEDIMENTO
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPROCE_ORIGDIAG_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEPROCE_PK ON TASY.PE_PROCEDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPROCE_TIPEVOL_FK_I ON TASY.PE_PROCEDIMENTO
(IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPROCE_TIPEVOL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEPROCE_UK ON TASY.PE_PROCEDIMENTO
(DS_INTERVENCAO_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PE_Procedimento_Atual
BEFORE INSERT OR UPDATE ON TASY.PE_PROCEDIMENTO FOR EACH ROW
declare

begin
if	(:new.ds_procedimento_pesquisa is null) or
	(:new.ds_procedimento <> :old.ds_procedimento ) then
	:new.ds_procedimento_pesquisa	:= upper(elimina_acentuacao(:new.ds_procedimento));
end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.PE_PROCEDIMENTO_tp  after update ON TASY.PE_PROCEDIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PROFISSIONAL,1,4000),substr(:new.IE_PROFISSIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROFISSIONAL',ie_log_w,ds_w,'PE_PROCEDIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Smart_pe_proced_aftInsUpdDel
after insert or update or delete ON TASY.PE_PROCEDIMENTO for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ie_integra_w			varchar2(1) := 'N';
nr_seq_sae_proced_w		pe_procedimento.nr_sequencia%type;
nm_usuario_w			pe_procedimento.nm_usuario%type;
ie_evento_w				varchar2(10) := '327';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

If (:new.ie_sistema_terceiro = 'S') then

	Select	OBTER_ESTABELECIMENTO_ATIVO
	into	cd_estabelecimento_w
	from 	dual;

	if (deleting) then

		nr_seq_sae_proced_w := :old.NR_SEQUENCIA;
		nm_usuario_w 		:= :old.nm_usuario;

	else

		nr_seq_sae_proced_w := :new.NR_SEQUENCIA;
		nm_usuario_w 		:= :new.nm_usuario;

	end if;

	Select 	Smart_obter_se_sae(nr_seq_sae_proced_w, cd_estabelecimento_w, ie_evento_w,'PE',nm_usuario_w)
	into	ie_integra_w
	from 	dual;

	if (nvl( ie_integra_w,'N') = 'S') then

		reg_integracao_w.cd_estab_documento	:=	cd_estabelecimento_w;

		if (inserting) then
			BEGIN
				gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_sae_proced_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=CREATE');
			END;
		elsif  (updating) then
			BEGIN
				if (:new.IE_SITUACAO = 'I' and :old.IE_SITUACAO <> 'I') then
					gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_sae_proced_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=DELETE');
				else
					gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_sae_proced_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=UPDATE');
				end if;
			END;
		elsif  (deleting) then
			BEGIN
				gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_sae_proced_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=DELETE');
			END;
		end if;

	end if;

end if;

<<Final>>
ie_integra_w	:= 'N';

end;
/


ALTER TABLE TASY.PE_PROCEDIMENTO ADD (
  CONSTRAINT PEPROCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PEPROCE_UK
 UNIQUE (DS_INTERVENCAO_ORIG)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          32K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_PROCEDIMENTO ADD (
  CONSTRAINT PEPROCE_EVEVENT_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.EV_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PEPROCE_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PEPROCE_ORIGDIAG_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.ORIGEM_DIAGNOSTICO (NR_SEQUENCIA),
  CONSTRAINT PEPROCE_GRUINTE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_INTERV_ENF (NR_SEQUENCIA),
  CONSTRAINT PEPROCE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PEPROCE_TIPEVOL_FK 
 FOREIGN KEY (IE_EVOLUCAO_CLINICA) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO));

GRANT SELECT ON TASY.PE_PROCEDIMENTO TO NIVEL_1;

