ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM_SERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_ORDEM_SERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_ORDEM_SERV
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_TIPO_PRECO           VARCHAR2(2 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO     NUMBER(10),
  NM_ATRIBUTO             VARCHAR2(255 BYTE),
  IE_TIPO_TABELA          VARCHAR2(2 BYTE),
  IE_ORDEM_CLASSIFICACAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRPOS_ESTABEL_FK_I ON TASY.PLS_REGRA_PRECO_ORDEM_SERV
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPOS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRPOS_PK ON TASY.PLS_REGRA_PRECO_ORDEM_SERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPOS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_alt_regra_preco_ordem_serv
before update or insert or delete ON TASY.PLS_REGRA_PRECO_ORDEM_SERV for each row
declare
qt_registro_w	pls_integer;
begin
-- esta trigger tem apenas a finalidade de especificar na tabela pls_regra_preco_controle se as views de pre�o precisam ser geradas novamente
select	count(1)
into	qt_registro_w
from	pls_regra_preco_controle;

if	(qt_registro_w > 0) then
	update	pls_regra_preco_controle set
		ie_gera_novamente = 'S';
else
	insert into pls_regra_preco_controle (ie_gera_novamente) values ('S');
end if;

end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM_SERV ADD (
  CONSTRAINT PLSRPOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM_SERV ADD (
  CONSTRAINT PLSRPOS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_ORDEM_SERV TO NIVEL_1;

