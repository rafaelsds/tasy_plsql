ALTER TABLE TASY.BANCO_ESTAB_PIX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_ESTAB_PIX CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_ESTAB_PIX
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTA_BANCO   NUMBER(10)               NOT NULL,
  DS_CHAVE             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_CHAVE        VARCHAR2(2 BYTE)         NOT NULL,
  IE_WEBHOOK           VARCHAR2(2 BYTE),
  DS_WEBHOOK           VARCHAR2(2000 BYTE),
  IE_PREFERENCIAL      VARCHAR2(2 BYTE),
  IE_RECEB_CAIXA       VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANEPIX_BANESTA_FK_I ON TASY.BANCO_ESTAB_PIX
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BANEPIX_PK ON TASY.BANCO_ESTAB_PIX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.BANCO_ESTAB_PIX_tp  after update ON TASY.BANCO_ESTAB_PIX FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PREFERENCIAL,1,4000),substr(:new.IE_PREFERENCIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREFERENCIAL',ie_log_w,ds_w,'BANCO_ESTAB_PIX',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEB_CAIXA,1,4000),substr(:new.IE_RECEB_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEB_CAIXA',ie_log_w,ds_w,'BANCO_ESTAB_PIX',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.banco_estab_pix_aut_bef
before insert or update
ON TASY.BANCO_ESTAB_PIX for each row
declare
    qt_preferencial_w number(1);
    pragma autonomous_transaction;
begin
    if  (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        if(:new.ie_preferencial = 'S') then
            begin
                update banco_estab_pix a
                set    a.ie_preferencial = 'N'
                where  a.nr_seq_conta_banco = :new.nr_seq_conta_banco
                and    a.nr_sequencia <> :new.nr_sequencia;
                commit;
            end;
        else
            select count(1)
            into   qt_preferencial_w
            from   banco_estab_pix a
            where  a.nr_seq_conta_banco = :new.nr_seq_conta_banco
            and    a.ie_preferencial = 'S';

            if (qt_preferencial_w = 0) then
                :new.ie_preferencial := 'S';
            end if;
        end if;
    end if;
end banco_estab_pix_aut_bef;
/


CREATE OR REPLACE TRIGGER TASY.banco_estab_pix_before_insert
before insert or update
ON TASY.BANCO_ESTAB_PIX for each row
begin
    if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
        pix_pck.valida_chave( ds_chave_p => :new.ds_chave,
                              ie_tipo_chave_p => :new.ie_tipo_chave);
    end if;
end banco_estab_pix_before_insert;
/


ALTER TABLE TASY.BANCO_ESTAB_PIX ADD (
  CONSTRAINT BANEPIX_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.BANCO_ESTAB_PIX ADD (
  CONSTRAINT BANEPIX_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.BANCO_ESTAB_PIX TO NIVEL_1;

