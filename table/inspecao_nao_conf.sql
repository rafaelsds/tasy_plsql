ALTER TABLE TASY.INSPECAO_NAO_CONF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INSPECAO_NAO_CONF CASCADE CONSTRAINTS;

CREATE TABLE TASY.INSPECAO_NAO_CONF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO              VARCHAR2(255 BYTE)       NOT NULL,
  IE_JUSTIFICATIVA     VARCHAR2(1 BYTE)         NOT NULL,
  IE_OBRIGA_MOT_DEV    VARCHAR2(1 BYTE)         NOT NULL,
  IE_PERMITE_OUTRA_OC  VARCHAR2(1 BYTE)         NOT NULL,
  IE_GERAR_NF          VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_OBSERVACAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INSNACO_PK ON TASY.INSPECAO_NAO_CONF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INSPECAO_NAO_CONF ADD (
  CONSTRAINT INSNACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.INSPECAO_NAO_CONF TO NIVEL_1;

