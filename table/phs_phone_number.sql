ALTER TABLE TASY.PHS_PHONE_NUMBER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PHS_PHONE_NUMBER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PHS_PHONE_NUMBER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PHS               VARCHAR2(5 BYTE),
  CD_DEPARTAMENTO      NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PHSPHNM_DEPMED_FK_I ON TASY.PHS_PHONE_NUMBER
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PHSPHNM_PK ON TASY.PHS_PHONE_NUMBER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PHS_PHONE_NUMBER ADD (
  CONSTRAINT PHSPHNM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PHS_PHONE_NUMBER ADD (
  CONSTRAINT PHSPHNM_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO));

