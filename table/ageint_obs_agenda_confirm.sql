ALTER TABLE TASY.AGEINT_OBS_AGENDA_CONFIRM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_OBS_AGENDA_CONFIRM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_OBS_AGENDA_CONFIRM
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA             NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE) NOT NULL,
  DS_CONFIRMACAO            VARCHAR2(80 BYTE),
  NR_SEQ_FORMA_CONFIRMACAO  NUMBER(10),
  IE_TIPO_AGENDAMENTO       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIOBAC_AGFOCON_FK_I ON TASY.AGEINT_OBS_AGENDA_CONFIRM
(NR_SEQ_FORMA_CONFIRMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGIOBAC_PK ON TASY.AGEINT_OBS_AGENDA_CONFIRM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIOBAC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ageint_obs_age_confirm_atual
after insert or update ON TASY.AGEINT_OBS_AGENDA_CONFIRM for each row
declare
ds_observacao_agenda_w varchar2(4000);

begin

if :new.ie_tipo_agendamento in ('C','S') then

select 	max(ds_observacao)
	into    ds_observacao_agenda_w
	from	agenda_consulta
	where	nr_sequencia	= :new.nr_seq_agenda;

	if (:new.ds_observacao is not null) then

		if (ds_observacao_agenda_w is not null) then
			ds_observacao_agenda_w := substr(ds_observacao_agenda_w || chr(10) || :new.ds_observacao ,1,2000);
		else
			ds_observacao_agenda_w := substr(:new.ds_observacao,1,2000);
		end if;
	else
		ds_observacao_agenda_w := null;
	end if;

	update	agenda_consulta
		set	ds_observacao	= nvl(ds_observacao_agenda_w, ds_observacao),
				nr_seq_forma_confirmacao = :new.nr_seq_forma_confirmacao,
				DS_CONFIRMACAO			= :new.ds_confirmacao
		where	nr_sequencia	= :new.nr_seq_agenda;

elsif (:new.ie_tipo_agendamento in ('E')) then

	select 	max(ds_observacao)
	into    ds_observacao_agenda_w
	from	agenda_paciente
	where	nr_sequencia	= :new.nr_seq_agenda;

	if (:new.ds_observacao is not null) then

		if (ds_observacao_agenda_w is not null) then
			ds_observacao_agenda_w := substr(ds_observacao_agenda_w || chr(10) || :new.ds_observacao ,1,4000);
		else
			ds_observacao_agenda_w := substr(:new.ds_observacao,1,4000);
		end if;

	else
		ds_observacao_agenda_w := null;
	end if;

	update	agenda_paciente
		set	ds_observacao	= nvl(ds_observacao_agenda_w, ds_observacao),
				nr_seq_forma_confirmacao = :new.nr_seq_forma_confirmacao,
				DS_CONFIRMACAO			= :new.ds_confirmacao
		where	nr_sequencia	= :new.nr_seq_agenda;

end if;

end;
/


ALTER TABLE TASY.AGEINT_OBS_AGENDA_CONFIRM ADD (
  CONSTRAINT AGIOBAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_OBS_AGENDA_CONFIRM ADD (
  CONSTRAINT AGIOBAC_AGFOCON_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CONFIRMACAO) 
 REFERENCES TASY.AGENDA_FORMA_CONTATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGEINT_OBS_AGENDA_CONFIRM TO NIVEL_1;

