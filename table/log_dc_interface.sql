ALTER TABLE TASY.LOG_DC_INTERFACE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_DC_INTERFACE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_DC_INTERFACE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  INVOICE_ID           VARCHAR2(20 BYTE),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  PATIENT_ID           VARCHAR2(20 BYTE),
  DS_MESSAGE           CLOB,
  IE_REQUEST_RESPONSE  VARCHAR2(1 BYTE),
  DT_EVENT             DATE,
  DS_EVENT             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOGDCIN_PK ON TASY.LOG_DC_INTERFACE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.log_dc_interface_aftinsert
after insert ON TASY.LOG_DC_INTERFACE for each row
declare

begin

if  ((:new.ds_event ='directcontrol.oec') or (:new.ds_event = 'directcontrol.oec.resend')) and
	(:new.ie_request_response = 'D') and
	(:new.ds_message like '%success=false%') and
	(:new.invoice_id is not null)	then


	insert into autorizacao_convenio_hist
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		nr_seq_autorizacao,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_sequencia_autor) values (
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		'DCInterface',
		null,
		null,
		substr(:new.ds_message,1,4000),
		sysdate,
		'DCInterface',
		:new.invoice_id);

end if;

if  ((:new.ds_event ='directcontrol.transmitclaim') or (:new.ds_event = 'directcontrol.encounter.billing')) and
	(:new.ie_request_response = 'D') and
	(:new.ds_message like '%success=false%') and
	(:new.invoice_id is not null)	then

	insert into eclipse_claim_history (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_interno_conta,
		ds_historico) values (
		eclipse_claim_history_seq.nextval,
		sysdate,
		'DCInterface',
		sysdate,
		'DCInterface',
		:new.invoice_id,
		substr(:new.ds_message,1,4000));

end if;

end;
/


ALTER TABLE TASY.LOG_DC_INTERFACE ADD (
  CONSTRAINT LOGDCIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_DC_INTERFACE TO NIVEL_1;

