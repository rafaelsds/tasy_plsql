ALTER TABLE TASY.AE_REPORT_PARAMS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_REPORT_PARAMS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_REPORT_PARAMS
(
  EVENT_ID                   VARCHAR2(36 BYTE)  NOT NULL,
  PARAMS_SEQ                 NUMBER(10)         NOT NULL,
  PARAMETER_NAME             VARCHAR2(256 BYTE),
  DESCRIPTION                VARCHAR2(256 BYTE),
  PARAMETER_VALUE_NAME       VARCHAR2(2000 BYTE),
  PARAMETER_VALUE_NUMBER     NUMBER(38,16),
  PARAMETER_VALUE_VARCHAR    VARCHAR2(4000 BYTE),
  PARAMETER_VALUE_TIMESTAMP  TIMESTAMP(6),
  PARAMETER_VALUE_CLOB       CLOB,
  PARAMETER_VALUE_BLOB       BLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (PARAMETER_VALUE_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (PARAMETER_VALUE_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AE_REPORT_PARAMS ON TASY.AE_REPORT_PARAMS
(EVENT_ID, PARAMS_SEQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_REPORT_PARAMS ADD (
  CONSTRAINT PK_AE_REPORT_PARAMS
 PRIMARY KEY
 (EVENT_ID, PARAMS_SEQ)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_REPORT_PARAMS TO NIVEL_1;

