ALTER TABLE TASY.REGRA_DIVERG_OC_NF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DIVERG_OC_NF CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DIVERG_OC_NF
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_LOCAL_ESTOQUE         NUMBER(4),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_GRUPO_MATERIAL        NUMBER(3),
  CD_SUBGRUPO_MATERIAL     NUMBER(3),
  CD_CLASSE_MATERIAL       NUMBER(5),
  CD_MATERIAL              NUMBER(6),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  PR_DIFERENCA             NUMBER(5,2),
  IE_TIPO_REGRA            VARCHAR2(3 BYTE)     NOT NULL,
  QT_DIFERENCA             NUMBER(5,2),
  IE_CONSIDERAR_REGRA      VARCHAR2(1 BYTE)     NOT NULL,
  IE_PERMITE_JUSTIF        VARCHAR2(1 BYTE)     NOT NULL,
  IE_CONSISTE_SALVAR_ITEM  VARCHAR2(1 BYTE)     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGDION_CLAMATE_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDION_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDION_ESTABEL_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGDION_GRUMATE_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_GRUPO_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDION_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDION_LOCESTO_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_LOCAL_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDION_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDION_MATERIA_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDION_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGDION_PK ON TASY.REGRA_DIVERG_OC_NF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGDION_SUBMATE_FK_I ON TASY.REGRA_DIVERG_OC_NF
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDION_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.regra_diverg_oc_nf_insert
BEFORE INSERT OR UPDATE ON TASY.REGRA_DIVERG_OC_NF FOR EACH ROW
BEGIN

if	(:new.qt_diferenca is null) and
	(:new.pr_diferenca is null) then
	/*r.aise_application_error(-20011,'╔ preciso informar a quantidade, ou o valor, ou o percentual de diferenša para a regra.');*/
	wheb_mensagem_pck.Exibir_Mensagem_Abort(233800);
end if;

if	(:new.qt_diferenca is not null) and
	(:new.pr_diferenca is not null) then
	/*r.aise_application_error(-20011,'Informar somente a quantidade, ou o valor, ou o percentual.');*/
	wheb_mensagem_pck.Exibir_Mensagem_Abort(233801);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.REGRA_DIVERG_OC_NF_tp  after update ON TASY.REGRA_DIVERG_OC_NF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_SALVAR_ITEM,1,4000),substr(:new.IE_CONSISTE_SALVAR_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_SALVAR_ITEM',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DIFERENCA,1,4000),substr(:new.PR_DIFERENCA,1,4000),:new.nm_usuario,nr_seq_w,'PR_DIFERENCA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_REGRA,1,4000),substr(:new.IE_TIPO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIFERENCA,1,4000),substr(:new.QT_DIFERENCA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIFERENCA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERAR_REGRA,1,4000),substr(:new.IE_CONSIDERAR_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_JUSTIF,1,4000),substr(:new.IE_PERMITE_JUSTIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_JUSTIF',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_DIVERG_OC_NF ADD (
  CONSTRAINT REGDION_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DIVERG_OC_NF ADD (
  CONSTRAINT REGDION_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGDION_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT REGDION_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REGDION_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT REGDION_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT REGDION_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.REGRA_DIVERG_OC_NF TO NIVEL_1;

