ALTER TABLE TASY.AGENDA_PAC_CONSIGNADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_CONSIGNADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_CONSIGNADO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA         NUMBER(10),
  DS_MATERIAL           VARCHAR2(255 BYTE)      NOT NULL,
  QT_QUANTIDADE         NUMBER(15,3),
  DS_FORNECEDOR         VARCHAR2(255 BYTE),
  IE_TIPO_ITEM          NUMBER(10)              NOT NULL,
  IE_AUTORIZADO         VARCHAR2(3 BYTE),
  CD_CGC                VARCHAR2(14 BYTE),
  IE_ENVIO_COMUNIC      VARCHAR2(1 BYTE),
  NR_GUIA_CONVENIO      VARCHAR2(25 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_MATERIAL_CONVENIO  VARCHAR2(25 BYTE),
  NR_SEQ_MARCA          NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  IE_PERMANENTE         VARCHAR2(1 BYTE),
  NR_SEQ_ANTERIOR       NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  QT_AUTORIZADA         NUMBER(15,3),
  NR_SEQ_MAT_AUTOR      NUMBER(10),
  IE_TIPO_MATERIAL      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGPACON_AGEPACI_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACON_MARCA_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACON_MATAUTO_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(NR_SEQ_MAT_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACON_MATERIA_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACON_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPACON_MATMARC_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(CD_MATERIAL, NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACON_MATMARC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPACON_PESJURI_FK_I ON TASY.AGENDA_PAC_CONSIGNADO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACON_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGPACON_PK ON TASY.AGENDA_PAC_CONSIGNADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACON_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_pac_consignado_atual
before insert or update ON TASY.AGENDA_PAC_CONSIGNADO for each row
declare
ds_alteracao_w		varchar2(4000);
ds_solic_medica_w	varchar2(255);
qt_registro_w		number(10);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(342700, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(773913, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(773915, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(618484, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(622382, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao6_w	varchar2(255) := obter_desc_expressao_idioma(773916, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao7_w	varchar2(255) := obter_desc_expressao_idioma(773917, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao8_w	varchar2(255) := obter_desc_expressao_idioma(310214, null, wheb_usuario_pck.get_nr_seq_idioma);--de
expressao9_w	varchar2(255) := obter_desc_expressao_idioma(773921, null, wheb_usuario_pck.get_nr_seq_idioma);--
expressao10_w	varchar2(255) := obter_desc_expressao_idioma(773922, null, wheb_usuario_pck.get_nr_seq_idioma);--Foi alterada a observa��o do material
expressao11_w	varchar2(255) := obter_desc_expressao_idioma(318357, null, wheb_usuario_pck.get_nr_seq_idioma);--nula
expressao12_w	varchar2(255) := obter_desc_expressao_idioma(773923, null, wheb_usuario_pck.get_nr_seq_idioma);--para a observa��o
expressao13_w	varchar2(255) := obter_desc_expressao_idioma(773924, null, wheb_usuario_pck.get_nr_seq_idioma);--Realizada a inclus�o do material


begin

if	(updating) then
	if	(nvl(:old.DS_MATERIAL,'XPTO') <> nvl(:new.DS_MATERIAL,'XPTO')) then
		ds_alteracao_w	:=	substr(expressao1_w || ' ' ||:old.DS_MATERIAL|| ' ' ||
					expressao2_w || ' ' ||:new.DS_MATERIAL,1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'AS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.IE_PERMANENTE,'N') <> nvl(:new.IE_PERMANENTE,'N')) then
		ds_alteracao_w	:=	substr(expressao3_w || ' ' ||:new.DS_MATERIAL||' ' || expressao8_w|| ' ' ||nvl(:old.IE_PERMANENTE,'N')|| ' ' ||
					expressao4_w || ' ' ||nvl(:new.IE_PERMANENTE,'N'),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'AS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;


	if	(nvl(:old.QT_QUANTIDADE,'-1') <> nvl(:new.QT_QUANTIDADE,'-1')) then
		ds_alteracao_w	:=	substr(expressao5_w || ' ' ||:new.DS_MATERIAL||' ' || expressao8_w|| ' ' ||:old.QT_QUANTIDADE|| ' ' ||
					expressao6_w || ' ' ||:new.QT_QUANTIDADE,1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'AS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.DS_FORNECEDOR,'XPTO') <> nvl(:new.DS_FORNECEDOR,'XPTO')) then
		ds_alteracao_w	:=	substr(expressao7_w || ' ' ||:new.DS_MATERIAL||' ' || expressao8_w|| ' ' ||:old.DS_FORNECEDOR|| ' ' ||
					expressao9_w || ' ' ||:new.DS_FORNECEDOR,1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'AS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.ds_observacao,'X') <> nvl(:new.ds_observacao,'X')) then
		ds_alteracao_w	:=	substr(expressao10_w || ' ' ||:new.DS_MATERIAL||' ' || expressao8_w|| ' ' ||nvl(:old.ds_observacao,expressao11_w)|| ' ' ||
					expressao12_w || ' ' ||nvl(:new.ds_observacao,expressao11_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'AS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

else
	if (:new.NR_SEQ_ANTERIOR is null) then
		ds_solic_medica_w 	:= 	substr(:new.DS_MATERIAL,1,255);
		ds_alteracao_w		:=	substr(expressao13_w || ' ' ||ds_solic_medica_w,1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(:new.nr_seq_agenda,'IS',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_CONSIGNADO_tp  after update ON TASY.AGENDA_PAC_CONSIGNADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_CGC,1,500);gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AUTORIZADO,1,500);gravar_log_alteracao(substr(:old.IE_AUTORIZADO,1,4000),substr(:new.IE_AUTORIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTORIZADO',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA,1,4000),substr(:new.NR_SEQ_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATERIAL,1,4000),substr(:new.DS_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FORNECEDOR,1,4000),substr(:new.DS_FORNECEDOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORNECEDOR',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_QUANTIDADE,1,4000),substr(:new.QT_QUANTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_QUANTIDADE',ie_log_w,ds_w,'AGENDA_PAC_CONSIGNADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_pac_consignado_delete
before delete ON TASY.AGENDA_PAC_CONSIGNADO for each row
declare
ds_alteracao_w		varchar2(4000) := null;
begin

if	(nvl(:old.DS_MATERIAL,'XPTO') <> 'XPTO') then
	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800099)||':  '||
				wheb_mensagem_pck.get_texto(798501)||':  '|| :old.DS_MATERIAL   ||'  '||
				wheb_mensagem_pck.get_texto(799261)||':  '||:old.QT_QUANTIDADE ||'  '||
				wheb_mensagem_pck.get_texto(800101)||':  '||:old.DS_FORNECEDOR ||'  '||
				wheb_mensagem_pck.get_texto(800103)||':  '||:old.IE_PERMANENTE ||'  '||
				wheb_mensagem_pck.get_texto(800105)||':  '||:old.ds_observacao,1,4000);
end if;

if	(ds_alteracao_w is not null) then
	gravar_hist_planeja_consig(:old.nr_seq_agenda,'ES',ds_alteracao_w,:old.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.AGENDA_PAC_CONSIGNADO ADD (
  CONSTRAINT AGPACON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_CONSIGNADO ADD (
  CONSTRAINT AGPACON_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPACON_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT AGPACON_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT AGPACON_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT AGPACON_MATMARC_FK 
 FOREIGN KEY (CD_MATERIAL, NR_SEQ_MARCA) 
 REFERENCES TASY.MATERIAL_MARCA (CD_MATERIAL,NR_SEQUENCIA),
  CONSTRAINT AGPACON_MATAUTO_FK 
 FOREIGN KEY (NR_SEQ_MAT_AUTOR) 
 REFERENCES TASY.MATERIAL_AUTORIZADO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_PAC_CONSIGNADO TO NIVEL_1;

