ALTER TABLE TASY.W_PREVISAO_PROJ_DESENV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PREVISAO_PROJ_DESENV CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PREVISAO_PROJ_DESENV
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  DT_PREVISAO             DATE                  NOT NULL,
  NM_USUARIO_PREVISAO     VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_GERENCIA         NUMBER(10)            NOT NULL,
  DT_REFERENCIA           DATE                  NOT NULL,
  QT_HORAS_PREVISTAS      NUMBER(15,4),
  QT_HORAS_REALIZADAS     NUMBER(15,4),
  QT_HORAS_SALDO          NUMBER(15,4),
  QT_HORAS_DESENVOLVER    NUMBER(15,4),
  QT_DIAS_UTEIS           NUMBER(10),
  QT_HORAS_UTEIS          NUMBER(10),
  QT_COLABORADORES        NUMBER(10),
  QT_HORAS_COLABORADOR    NUMBER(10),
  QT_HORAS_POSSIVEIS      NUMBER(15,4),
  QT_HORAS_REAL_PREV      NUMBER(15,4),
  QT_PERCENTUAL_PREVISTO  NUMBER(5,2),
  NR_SEQ_HIST_PREV        NUMBER(10)            NOT NULL,
  NR_SEQ_GRUPO_DES        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPRPROD_GERWHEB_FK_I ON TASY.W_PREVISAO_PROJ_DESENV
(NR_SEQ_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRPROD_GERWHEB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WPRPROD_GRUDESE_FK_I ON TASY.W_PREVISAO_PROJ_DESENV
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRPROD_GRUDESE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WPRPROD_PK ON TASY.W_PREVISAO_PROJ_DESENV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRPROD_PK
  MONITORING USAGE;


CREATE INDEX TASY.WPRPROD_WHPPDES_FK_I ON TASY.W_PREVISAO_PROJ_DESENV
(NR_SEQ_HIST_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRPROD_WHPPDES_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_PREVISAO_PROJ_DESENV ADD (
  CONSTRAINT WPRPROD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PREVISAO_PROJ_DESENV ADD (
  CONSTRAINT WPRPROD_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA),
  CONSTRAINT WPRPROD_WHPPDES_FK 
 FOREIGN KEY (NR_SEQ_HIST_PREV) 
 REFERENCES TASY.W_HIST_PREV_PROJ_DESENV (NR_SEQUENCIA),
  CONSTRAINT WPRPROD_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PREVISAO_PROJ_DESENV TO NIVEL_1;

