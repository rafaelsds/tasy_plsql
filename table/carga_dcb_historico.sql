ALTER TABLE TASY.CARGA_DCB_HISTORICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARGA_DCB_HISTORICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARGA_DCB_HISTORICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_HISTORICO         DATE                     NOT NULL,
  DS_TITULO            VARCHAR2(255 BYTE)       NOT NULL,
  DS_HISTORICO         LONG                     NOT NULL,
  IE_TIPO              VARCHAR2(1 BYTE)         NOT NULL,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CADCBHI_PK ON TASY.CARGA_DCB_HISTORICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CADCBHI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CARGA_DCB_HISTORICO_tp  after update ON TASY.CARGA_DCB_HISTORICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'CARGA_DCB_HISTORICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIB,1,4000),substr(:new.NM_USUARIO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIB',ie_log_w,ds_w,'CARGA_DCB_HISTORICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'CARGA_DCB_HISTORICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CARGA_DCB_HISTORICO ADD (
  CONSTRAINT CADCBHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CARGA_DCB_HISTORICO TO NIVEL_1;

