ALTER TABLE TASY.SIP_LOTE_ITEM_ASSIST_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIP_LOTE_ITEM_ASSIST_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIP_LOTE_ITEM_ASSIST_LOG
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO_NREC_LOG  DATE,
  NM_USUARIO_NREC_LOG      VARCHAR2(15 BYTE),
  CD_CLASSIFICACAO_SIP     VARCHAR2(10 BYTE),
  IE_BENEF_CARENCIA        VARCHAR2(1 BYTE),
  IE_DESPESA               VARCHAR2(1 BYTE),
  IE_EVENTO                VARCHAR2(1 BYTE),
  DT_OCORRENCIA            DATE,
  IE_SEGMENTACAO_SIP       NUMBER(3),
  IE_TIPO_CONTRATACAO      VARCHAR2(2 BYTE),
  NR_SEQ_APRES             NUMBER(10)           NOT NULL,
  NR_SEQ_ITEM_SIP          NUMBER(10)           NOT NULL,
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  NR_SEQ_SUPERIOR          NUMBER(10),
  QT_BENEFICIARIO          NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO_LOG       DATE                 NOT NULL,
  NM_USUARIO_LOG           VARCHAR2(15 BYTE)    NOT NULL,
  QT_EVENTO                NUMBER(15,3)         NOT NULL,
  SG_UF                    VARCHAR2(2 BYTE)     NOT NULL,
  VL_DESPESA               NUMBER(15,2)         NOT NULL,
  NR_SEQUENCIA_LOG         NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SIPLOIL_PK ON TASY.SIP_LOTE_ITEM_ASSIST_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPLOIL_PK
  MONITORING USAGE;


CREATE INDEX TASY.SIPLOIL_PLSLSIP_FK_I ON TASY.SIP_LOTE_ITEM_ASSIST_LOG
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPLOIL_PLSLSIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIPLOIL_SIPITAS_FK_I ON TASY.SIP_LOTE_ITEM_ASSIST_LOG
(NR_SEQ_ITEM_SIP)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPLOIL_SIPITAS_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SIP_LOTE_ITEM_ASSIST_LOG ADD (
  CONSTRAINT SIPLOIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SIP_LOTE_ITEM_ASSIST_LOG TO NIVEL_1;

