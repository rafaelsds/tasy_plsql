ALTER TABLE TASY.INTEROP_TASY_PARAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTEROP_TASY_PARAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTEROP_TASY_PARAM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_LOCAL_DEPARA       VARCHAR2(1 BYTE),
  CD_ESTBELECIMENTO     NUMBER(4),
  IE_INTEGRA_LAUDO      VARCHAR2(1 BYTE),
  IE_PERMITE_ATUAL_PAC  VARCHAR2(1 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PROCEDENCIA        NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INTTASPAR_PK ON TASY.INTEROP_TASY_PARAM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTEROP_TASY_PARAM_tp  after update ON TASY.INTEROP_TASY_PARAM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_LOCAL_DEPARA,1,4000),substr(:new.IE_LOCAL_DEPARA,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOCAL_DEPARA',ie_log_w,ds_w,'INTEROP_TASY_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_ATUAL_PAC,1,4000),substr(:new.IE_PERMITE_ATUAL_PAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_ATUAL_PAC',ie_log_w,ds_w,'INTEROP_TASY_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTEGRA_LAUDO,1,4000),substr(:new.IE_INTEGRA_LAUDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTEGRA_LAUDO',ie_log_w,ds_w,'INTEROP_TASY_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTBELECIMENTO,1,4000),substr(:new.CD_ESTBELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTBELECIMENTO',ie_log_w,ds_w,'INTEROP_TASY_PARAM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.INTEROP_TASY_PARAM ADD (
  CONSTRAINT INTTASPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.INTEROP_TASY_PARAM TO NIVEL_1;

