ALTER TABLE TASY.UNIDADE_ATEND_CLASSIF_P312
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_ATEND_CLASSIF_P312 CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_ATEND_CLASSIF_P312
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_UNID_ATEND    NUMBER(10)               NOT NULL,
  IE_CLASSIF_P312      VARCHAR2(5 BYTE)         NOT NULL,
  DT_INICIO_CLASSIF    DATE                     NOT NULL,
  DT_FIM_CLASSIF       DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.UNAT312_PK ON TASY.UNIDADE_ATEND_CLASSIF_P312
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNAT312_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.UNAT312_UK ON TASY.UNIDADE_ATEND_CLASSIF_P312
(NR_SEQ_UNID_ATEND, IE_CLASSIF_P312)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNAT312_UK
  MONITORING USAGE;


CREATE INDEX TASY.UNAT312_UNIATEN_FK_I ON TASY.UNIDADE_ATEND_CLASSIF_P312
(NR_SEQ_UNID_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNAT312_UNIATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.UNIDADE_ATEND_CLASSIF_P312 ADD (
  CONSTRAINT UNAT312_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT UNAT312_UK
 UNIQUE (NR_SEQ_UNID_ATEND, IE_CLASSIF_P312)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNIDADE_ATEND_CLASSIF_P312 ADD (
  CONSTRAINT UNAT312_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_UNID_ATEND) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO));

GRANT SELECT ON TASY.UNIDADE_ATEND_CLASSIF_P312 TO NIVEL_1;

