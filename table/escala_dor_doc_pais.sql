ALTER TABLE TASY.ESCALA_DOR_DOC_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DOR_DOC_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DOR_DOC_PAIS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PAIS              NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_REVISAO           DATE,
  IE_LIBERADO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_ESCALA        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EDDP_EDD_FK_I ON TASY.ESCALA_DOR_DOC_PAIS
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EDDP_PK ON TASY.ESCALA_DOR_DOC_PAIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESCALA_DOR_DOC_PAIS ADD (
  CONSTRAINT EDDP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_DOR_DOC_PAIS ADD (
  CONSTRAINT EDDP_EDD_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.ESCALA_DOR_DOCUMENTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_DOR_DOC_PAIS TO NIVEL_1;

