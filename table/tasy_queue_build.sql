ALTER TABLE TASY.TASY_QUEUE_BUILD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_QUEUE_BUILD CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_QUEUE_BUILD
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_VERSAO                 VARCHAR2(20 BYTE)   NOT NULL,
  IE_APLICACAO              VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_MAN_OS_CTRL_DESC   NUMBER(10),
  NM_USUARIO_SOLICITANTE    VARCHAR2(15 BYTE),
  IE_ESTADO_BUILD           VARCHAR2(1 BYTE)    NOT NULL,
  DT_INICIO_BUILD           DATE,
  DT_FIM_BUILD              DATE,
  IE_STATUS_BUILD           VARCHAR2(1 BYTE),
  NR_SEQ_CALENDARIO         NUMBER(10),
  NR_SEQ_MAN_OS_BUILD_EMER  NUMBER(10),
  BRANCH_NAME               VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QBUILD_MAOSCTDE_FK_I ON TASY.TASY_QUEUE_BUILD
(NR_SEQ_MAN_OS_CTRL_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QBUILD_PK ON TASY.TASY_QUEUE_BUILD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_QUEUE_BUILD ADD (
  CONSTRAINT QBUILD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_QUEUE_BUILD ADD (
  CONSTRAINT QBUILD_MAOSCTDE_FK 
 FOREIGN KEY (NR_SEQ_MAN_OS_CTRL_DESC) 
 REFERENCES TASY.MAN_OS_CTRL_DESC (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_QUEUE_BUILD TO NIVEL_1;

