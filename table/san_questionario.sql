ALTER TABLE TASY.SAN_QUESTIONARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_QUESTIONARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_QUESTIONARIO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_DOACAO            NUMBER(10)           NOT NULL,
  NR_SEQ_IMPEDIMENTO       NUMBER(10)           NOT NULL,
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_OCORRENCIA            DATE,
  IE_RESPOSTA              VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_IMPEDE_DOACAO         VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_INAPTIDAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANQUES_PK ON TASY.SAN_QUESTIONARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANQUES_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANQUES_SANDOAC_FK_I ON TASY.SAN_QUESTIONARIO
(NR_SEQ_DOACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANQUES_SANDOAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANQUES_SANMINA_FK_I ON TASY.SAN_QUESTIONARIO
(NR_SEQ_MOTIVO_INAPTIDAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANQUES_SANMINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.san_questionario_after_update
after update ON TASY.SAN_QUESTIONARIO for each row
declare

dt_liberacao_w		san_doacao_impedimento.dt_liberacao%type;
ie_novo_impedimento_w	varchar(1);
ie_definitivo_w		varchar(1);
pragma autonomous_transaction;

begin

select	decode(count(1), 0, 'S', 'N')
into	ie_novo_impedimento_w
from	san_doacao_impedimento
where	nr_seq_doacao = :new.nr_seq_doacao
and	nr_seq_impedimento = :new.nr_seq_impedimento;

if (:new.ie_impede_doacao = 'S') then

	select 	max(ie_definitivo)
	into 	ie_definitivo_w
	from 	san_impedimento
	where 	nr_sequencia = :new.nr_seq_impedimento;

	if(:new.dt_ocorrencia is not null) then
        select 	trunc(:new.dt_ocorrencia + nr_dias_inaptidao)
		into 	dt_liberacao_w
		from 	san_impedimento
		where 	nr_sequencia = :new.nr_seq_impedimento;
	end if;

	if (ie_novo_impedimento_w = 'S') then

		insert	into san_doacao_impedimento
			(nr_seq_doacao,
			nr_seq_impedimento,
			dt_atualizacao,
			nm_usuario,
			ds_observacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_ocorrencia,
			ds_justificativa,
			cd_pessoa_fisica,
			ie_definitivo,
			dt_liberacao)
		values	(:new.nr_seq_doacao,
			:new.nr_seq_impedimento,
			sysdate,
			:new.nm_usuario,
			:new.ds_observacao,
			sysdate,
			:new.nm_usuario,
			:new.dt_ocorrencia,
			:new.ds_justificativa,
			san_obter_doador(:new.nr_seq_doacao, 'C'),
			decode(ie_definitivo_w, 'I', 'N', decode(dt_liberacao_w,null,'S','N')),
			dt_liberacao_w);

	else

		update san_doacao_impedimento
		set	dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario,
			ds_observacao = :new.ds_observacao,
			dt_ocorrencia = :new.dt_ocorrencia,
			ds_justificativa = :new.ds_justificativa,
			ie_definitivo = decode(ie_definitivo_w, 'I', 'N', decode(dt_liberacao_w,null,'S','N')),
			dt_liberacao = dt_liberacao_w
		where	nr_seq_doacao = :new.nr_seq_doacao
		and	nr_seq_impedimento = :new.nr_seq_impedimento;

	end if;

	commit;

elsif (ie_novo_impedimento_w = 'N') then

	delete 	san_doacao_impedimento
	where	nr_seq_doacao = :new.nr_seq_doacao
	and	nr_seq_impedimento = :new.nr_seq_impedimento;

	commit;

end if;

end;
/


ALTER TABLE TASY.SAN_QUESTIONARIO ADD (
  CONSTRAINT SANQUES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_QUESTIONARIO ADD (
  CONSTRAINT SANQUES_SANDOAC_FK 
 FOREIGN KEY (NR_SEQ_DOACAO) 
 REFERENCES TASY.SAN_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANQUES_SANMINA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INAPTIDAO) 
 REFERENCES TASY.SAN_MOTIVO_INAPTIDAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_QUESTIONARIO TO NIVEL_1;

