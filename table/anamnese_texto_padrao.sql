ALTER TABLE TASY.ANAMNESE_TEXTO_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANAMNESE_TEXTO_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANAMNESE_TEXTO_PADRAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO_CONFIG     VARCHAR2(15 BYTE),
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TEXTO          NUMBER(10)              NOT NULL,
  IE_TIPO_ATENDIMENTO   NUMBER(3),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_ESPECIALIDADE      NUMBER(5),
  IE_CLINICA            NUMBER(5),
  CD_ESTABELECIMENTO    NUMBER(4),
  NR_SEQ_AGRUPAMENTO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANMTEPA_AGRUSET_FK_I ON TASY.ANAMNESE_TEXTO_PADRAO
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANMTEPA_ESPMEDI_FK_I ON TASY.ANAMNESE_TEXTO_PADRAO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANMTEPA_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANMTEPA_ESTABEL_FK_I ON TASY.ANAMNESE_TEXTO_PADRAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANMTEPA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ANMTEPA_PK ON TASY.ANAMNESE_TEXTO_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANMTEPA_SETATEN_FK_I ON TASY.ANAMNESE_TEXTO_PADRAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANMTEPA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANMTEPA_TEXPADR_FK_I ON TASY.ANAMNESE_TEXTO_PADRAO
(NR_SEQ_TEXTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANMTEPA_TEXPADR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ANAMNESE_TEXTO_PADRAO ADD (
  CONSTRAINT ANMTEPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ANAMNESE_TEXTO_PADRAO ADD (
  CONSTRAINT ANMTEPA_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT ANMTEPA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ANMTEPA_TEXPADR_FK 
 FOREIGN KEY (NR_SEQ_TEXTO) 
 REFERENCES TASY.TEXTO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT ANMTEPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ANMTEPA_AGRUSET_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.AGRUPAMENTO_SETOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.ANAMNESE_TEXTO_PADRAO TO NIVEL_1;

