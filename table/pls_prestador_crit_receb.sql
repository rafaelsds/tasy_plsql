ALTER TABLE TASY.PLS_PRESTADOR_CRIT_RECEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_CRIT_RECEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_CRIT_RECEB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR        NUMBER(10)            NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_PREST_RECEB      NUMBER(10),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRCR_I1 ON TASY.PLS_PRESTADOR_CRIT_RECEB
(NR_SEQ_PRESTADOR, DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRCR_PK ON TASY.PLS_PRESTADOR_CRIT_RECEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCR_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_CRIT_RECEB
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCR_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCR_PLSPRES_FK2_I ON TASY.PLS_PRESTADOR_CRIT_RECEB
(NR_SEQ_PREST_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCR_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prestador_crit_receb_atual
before insert or update ON TASY.PLS_PRESTADOR_CRIT_RECEB for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_prestador_crit_receb_atual;
/


ALTER TABLE TASY.PLS_PRESTADOR_CRIT_RECEB ADD (
  CONSTRAINT PLSPRCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_CRIT_RECEB ADD (
  CONSTRAINT PLSPRCR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRCR_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PREST_RECEB) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_CRIT_RECEB TO NIVEL_1;

