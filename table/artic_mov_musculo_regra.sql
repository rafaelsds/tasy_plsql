ALTER TABLE TASY.ARTIC_MOV_MUSCULO_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARTIC_MOV_MUSCULO_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARTIC_MOV_MUSCULO_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_ART_MOV_MUSCULO  NUMBER(10)            NOT NULL,
  QT_IDADE_MIN            NUMBER(3)             NOT NULL,
  QT_IDADE_MAX            NUMBER(3)             NOT NULL,
  QT_DOSE_MIN             NUMBER(15,4)          NOT NULL,
  QT_DOSE_MAX             NUMBER(15,4)          NOT NULL,
  QT_DOSE_PADRAO          NUMBER(15,4),
  QT_PONTOS_PADRAO        NUMBER(15,4),
  QT_PONTOS_MIN           NUMBER(15,4),
  QT_PONTOS_MAX           NUMBER(15,4),
  QT_PESO_MIN             NUMBER(10,3),
  QT_PESO_MAX             NUMBER(10,3),
  QT_MULTIPLO             NUMBER(10,2),
  IE_FORMA_CALCULO        VARCHAR2(3 BYTE),
  NR_SEQ_TOXINA           NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ARMOMUR_ARTMOMU_FK_I ON TASY.ARTIC_MOV_MUSCULO_REGRA
(NR_SEQ_ART_MOV_MUSCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ARMOMUR_ARTMOMU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ARMOMUR_PK ON TASY.ARTIC_MOV_MUSCULO_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ARMOMUR_PK
  MONITORING USAGE;


CREATE INDEX TASY.ARMOMUR_TOXBOTU_FK_I ON TASY.ARTIC_MOV_MUSCULO_REGRA
(NR_SEQ_TOXINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ARMOMUR_TOXBOTU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Artic_Mov_Musculo_Regra_Atual
before insert or update ON TASY.ARTIC_MOV_MUSCULO_REGRA FOR EACH ROW
DECLARE

qt_idade_max_w	number(10);

BEGIN

qt_idade_max_w	:= somente_numero(obter_valor_param_usuario(2468, 1, obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento));

if 	(qt_idade_max_w > 0) and
	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

	if	(qt_idade_max_w < :new.QT_IDADE_MAX) and
		(:new.QT_IDADE_MIN < qt_idade_max_w) then
		-- A idade m�xima para pacientes infantis deve ser de at� '||qt_idade_max_w||' anos.'||CHR(13)||' Par�metro [1] da fun��o Escala Modificada de Ashworth.','A');
		Wheb_mensagem_pck.exibir_mensagem_abort(264595,	'QT_IDADE_MAX_W='||to_char(QT_IDADE_MAX_W));
	end if;

	if 	(:new.QT_IDADE_MIN is not null) and
		(:new.QT_IDADE_MAX is not null) and
		(qt_idade_max_w between :new.QT_IDADE_MIN and :new.QT_IDADE_MAX) then
		:new.IE_FORMA_CALCULO := 'Kg';
	else
		:new.IE_FORMA_CALCULO := 'F';
	end if;
end if;
END;
/


ALTER TABLE TASY.ARTIC_MOV_MUSCULO_REGRA ADD (
  CONSTRAINT ARMOMUR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ARTIC_MOV_MUSCULO_REGRA ADD (
  CONSTRAINT ARMOMUR_TOXBOTU_FK 
 FOREIGN KEY (NR_SEQ_TOXINA) 
 REFERENCES TASY.TOXINA_BOTULINICA (NR_SEQUENCIA),
  CONSTRAINT ARMOMUR_ARTMOMU_FK 
 FOREIGN KEY (NR_SEQ_ART_MOV_MUSCULO) 
 REFERENCES TASY.ARTIC_MOV_MUSCULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ARTIC_MOV_MUSCULO_REGRA TO NIVEL_1;

