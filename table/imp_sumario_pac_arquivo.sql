ALTER TABLE TASY.IMP_SUMARIO_PAC_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMP_SUMARIO_PAC_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_SUMARIO_PAC_ARQUIVO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SUMARIO       NUMBER(10)               NOT NULL,
  IE_TIPO_ARQUIVO      VARCHAR2(10 BYTE)        NOT NULL,
  DS_CONTEUDO          CLOB                     NOT NULL,
  DS_CONTEUDO_BLOB     BLOB,
  DS_OID_DOCUMENT      VARCHAR2(255 BYTE),
  IE_ANEXADO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_CONTEUDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_CONTEUDO_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPSPAA_IMPSPAC_FK_I ON TASY.IMP_SUMARIO_PAC_ARQUIVO
(NR_SEQ_SUMARIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMPSPAA_PK ON TASY.IMP_SUMARIO_PAC_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_SUMARIO_PAC_ARQUIVO ADD (
  CONSTRAINT IMPSPAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.IMP_SUMARIO_PAC_ARQUIVO ADD (
  CONSTRAINT IMPSPAA_IMPSPAC_FK 
 FOREIGN KEY (NR_SEQ_SUMARIO) 
 REFERENCES TASY.IMP_SUMARIO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.IMP_SUMARIO_PAC_ARQUIVO TO NIVEL_1;

