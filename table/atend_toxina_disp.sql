ALTER TABLE TASY.ATEND_TOXINA_DISP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_TOXINA_DISP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_TOXINA_DISP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  NR_SEQ_ATENDIMENTO   NUMBER(10)               NOT NULL,
  QT_MATERIAL          NUMBER(18,6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATTODIS_ATETOXI_FK_I ON TASY.ATEND_TOXINA_DISP
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATTODIS_ATETOXI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATTODIS_MATERIA_FK_I ON TASY.ATEND_TOXINA_DISP
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATTODIS_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATTODIS_PK ON TASY.ATEND_TOXINA_DISP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATTODIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.ATTODIS_UNIMEDI_FK_I ON TASY.ATEND_TOXINA_DISP
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATTODIS_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ATEND_TOXINA_DISP ADD (
  CONSTRAINT ATTODIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_TOXINA_DISP ADD (
  CONSTRAINT ATTODIS_ATETOXI_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_TOXINA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATTODIS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ATTODIS_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.ATEND_TOXINA_DISP TO NIVEL_1;

