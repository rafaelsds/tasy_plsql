ALTER TABLE TASY.SAN_PRODUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_PRODUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_PRODUCAO
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  NR_SEQ_DERIVADO                 NUMBER(10)    NOT NULL,
  DT_PRODUCAO                     DATE          NOT NULL,
  CD_PF_REALIZOU                  VARCHAR2(10 BYTE) NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_VENCIMENTO                   DATE          NOT NULL,
  NR_SANGUE                       VARCHAR2(20 BYTE) NOT NULL,
  NR_SEQ_DOACAO                   NUMBER(10),
  NR_SEQ_TRANSFUSAO               NUMBER(10),
  NR_SEQ_RESERVA                  NUMBER(10),
  NR_SEQ_INUTIL                   NUMBER(10),
  NR_SEQ_EMP_ENT                  NUMBER(10),
  NR_SEQ_EMP_SAIDA                NUMBER(10),
  NR_SEQ_PROPACI                  NUMBER(10),
  IE_TIPO_SANGUE                  VARCHAR2(2 BYTE),
  IE_FATOR_RH                     VARCHAR2(1 BYTE),
  QT_VOLUME                       NUMBER(4),
  NM_DOADOR                       VARCHAR2(60 BYTE),
  DT_UTILIZACAO                   DATE,
  DT_TERMINO_UTIL                 DATE,
  IE_IRRADIADO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_LAVADO                       VARCHAR2(1 BYTE) NOT NULL,
  IE_FILTRADO                     VARCHAR2(1 BYTE) NOT NULL,
  IE_ALIQUOTADO                   VARCHAR2(1 BYTE) NOT NULL,
  DS_OBSERVACAO                   VARCHAR2(255 BYTE),
  NR_SEC_SAUDE                    VARCHAR2(20 BYTE),
  DT_LIBERACAO                    DATE,
  NM_USUARIO_LIB                  VARCHAR2(15 BYTE),
  NR_SEQ_PROD_ORIGEM              NUMBER(10),
  QT_VOL_TRANSF                   NUMBER(4),
  NR_SEQ_EQUIPO                   NUMBER(10),
  NR_SEQ_LOTE_EQUIPO              VARCHAR2(20 BYTE),
  DT_VAL_EQUIPO                   DATE,
  W_NR_SEQ_LOTE_EQUIPO            NUMBER(10),
  NR_SEQ_LOTE                     VARCHAR2(20 BYTE),
  QT_UNIDADE_DERIVADO             NUMBER(10),
  QT_UNIDADE_DERIV_UTIL           NUMBER(10),
  NR_SEQ_ATEND_PAC_UNID           NUMBER(10),
  CD_BARRAS                       VARCHAR2(30 BYTE),
  NR_TRANSFUSAO_CONTROLE          VARCHAR2(30 BYTE),
  IE_AFERESE                      VARCHAR2(1 BYTE) NOT NULL,
  QT_PESO_BOLSA                   NUMBER(10,3),
  QT_PESO_BOLSA_VAZIA             NUMBER(10,3),
  NM_USUARIO_IRRADIACAO           VARCHAR2(15 BYTE),
  NM_USUARIO_LAVADO               VARCHAR2(15 BYTE),
  DT_FIM_REPOUSO                  DATE,
  DT_INICIO_REPOUSO               DATE,
  DT_FIM_FILTRAGEM                DATE,
  DT_INICIO_FILTRAGEM             DATE,
  CD_MATERIAL                     NUMBER(6),
  NR_SEQ_CONSERVANTE              NUMBER(10),
  DT_VALIDADE_CONSERVANTE         DATE,
  NM_USUARIO_INI_ALICO            VARCHAR2(15 BYTE),
  DT_FIM_ALICOTAGEM               DATE,
  NM_USUARIO_FIM_ALICO            VARCHAR2(15 BYTE),
  DT_INICIO_ALICOTAGEM            DATE,
  DT_RECEBIMENTO                  DATE,
  NM_USUARIO_RECEBIMENTO          VARCHAR2(15 BYTE),
  NR_SEQ_ARMAZENAMENTO            NUMBER(10),
  NM_USUARIO_ENT_ESTOQUE          VARCHAR2(15 BYTE),
  DT_ENTRADA_ESTOQUE              DATE,
  QT_DOSE_ESTOQUE                 NUMBER(4),
  DT_INICIO_PRODUCAO              DATE,
  NM_USUARIO_INI_PRODUCAO         VARCHAR2(15 BYTE),
  DT_FIM_PRODUCAO                 DATE,
  NM_USUARIO_FIM_PRODUCAO         VARCHAR2(15 BYTE),
  QT_PESO_APOS_FILTRAGEM          NUMBER(10,3),
  QT_VOLUME_APOS_FILTRAGEM        NUMBER(4),
  DT_VALIDADE_APOS_FILTRAGEM      DATE,
  DT_INICIO_PROD_EMPRESTIMO       DATE,
  DT_FIM_PROD_EMPRESTIMO          DATE,
  NM_USUARIO_INI_PROD_EMPRESTIMO  VARCHAR2(15 BYTE),
  NM_USUARIO_FIM_PROD_EMPRESTIMO  VARCHAR2(15 BYTE),
  NM_USUARIO_CONFERENCIA          VARCHAR2(15 BYTE),
  DT_CONFERENCIA                  DATE,
  DT_CONF_TRANSF                  DATE,
  NM_USUARIO_CONF_TRANS           VARCHAR2(15 BYTE),
  IE_BOLSA_FENOTIPADA             VARCHAR2(1 BYTE),
  IE_PAI_REPRODUZIDO              VARCHAR2(1 BYTE),
  IE_REPRODUZIDO                  VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL_ARMAZ              NUMBER(10),
  NM_USUARIO_LOCAL_ARMAZ          VARCHAR2(15 BYTE),
  DT_LOCAL_ARMAZ                  DATE,
  DT_INICIO_INFUSAO               DATE,
  NM_USUARIO_INI_INFUSAO          VARCHAR2(15 BYTE),
  DT_FIM_INFUSAO                  DATE,
  NM_USUARIO_FIM_INFUSAO          VARCHAR2(15 BYTE),
  IE_LOCAL_INUTILIZACAO           VARCHAR2(3 BYTE),
  NR_SEQ_MOTIVO_BLOQUEIO          NUMBER(10),
  DT_BLOQUEIO                     DATE,
  NM_USUARIO_BLOQUEIO             VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_BLOQUEIO       VARCHAR2(255 BYTE),
  IE_BOLSA_FILHA                  VARCHAR2(1 BYTE),
  IE_REALIZA_NAT                  VARCHAR2(1 BYTE),
  NR_SEQ_AGRUPAMENTO              NUMBER(10),
  NR_SEQ_STATUS_INTEGRACAO        NUMBER(10),
  NR_PAUSA_INTEGRACAO             NUMBER(15,3),
  QT_MIN_PAUSA_INTEGRACAO         NUMBER(15,3),
  NR_SEQ_CENTRIFUGA               NUMBER(10),
  IE_HEMOCOMP_INTEGRACAO          VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO              NUMBER(4),
  CD_ESTAB_ATUAL                  NUMBER(4),
  IE_ENCAMINHADO                  VARCHAR2(1 BYTE),
  NM_USUARIO_RESP                 VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_FORNEC              NUMBER(10),
  NR_BOLSA                        VARCHAR2(20 BYTE),
  DT_INUTILIZACAO                 DATE,
  NM_USUARIO_INUT                 VARCHAR2(15 BYTE),
  IE_POOL                         VARCHAR2(1 BYTE),
  NR_SEQ_DERIVADO_ORIGEM          NUMBER(10),
  DS_JUSTIFICATIVA                VARCHAR2(255 BYTE),
  NR_OPCAO_FRAC                   NUMBER(5),
  NM_USUARIO_CONF_INTEGRACAO      VARCHAR2(15 BYTE),
  DT_CONFERENCIA_INTEGRACAO       DATE,
  CD_BARRAS_INTEGRACAO            VARCHAR2(22 BYTE),
  IE_TIPO_BLOQUEIO                VARCHAR2(1 BYTE),
  IE_SISTEMA_ALIQUOTAGEM          VARCHAR2(1 BYTE),
  IE_EM_REPRODUCAO                VARCHAR2(1 BYTE),
  NR_SEQ_ISBT                     NUMBER(10),
  CD_IDENTIFICACAO_ISBT           VARCHAR2(13 BYTE),
  DT_COLETA_ISBT                  DATE,
  NR_SEQ_TIPO_DOACAO_ISBT         NUMBER(10),
  CD_PRODUTO_ISBT                 VARCHAR2(8 BYTE),
  CD_DIVISAO_ISBT                 VARCHAR2(2 BYTE),
  DT_DISPENSACAO                  DATE,
  NM_USUARIO_DISPENSACAO          VARCHAR2(15 BYTE),
  DT_LIBERACAO_DIREC              DATE,
  NM_USUARIO_DIREC                VARCHAR2(15 BYTE),
  CD_ISBT_ANTIGENO                VARCHAR2(18 BYTE),
  DS_JUST_IRRADIACAO              VARCHAR2(500 BYTE),
  DT_IRRADIACAO                   DATE,
  QT_UNIDADE                      NUMBER(8),
  IE_VALIDATED                    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANPROD_ESTABEL_FK_I ON TASY.SAN_PRODUCAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_ESTABEL_FK2_I ON TASY.SAN_PRODUCAO
(CD_ESTAB_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_ESTABEL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_I1 ON TASY.SAN_PRODUCAO
(DT_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_MATERIA_FK_I ON TASY.SAN_PRODUCAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_MATLOFO_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_MATLOFO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_PESFISI_FK_I ON TASY.SAN_PRODUCAO
(CD_PF_REALIZOU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANPROD_PK ON TASY.SAN_PRODUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_PROPACI_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANAGRU_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANARBO_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_ARMAZENAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANARBO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANCENT_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_CENTRIFUGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANCENT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANCONS_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_CONSERVANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANDERI_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANDERI_FK1_I ON TASY.SAN_PRODUCAO
(NR_SEQ_DERIVADO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANDERI_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANDOAC_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_DOACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANEMPR_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_EMP_ENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANEMPR_FK_I2 ON TASY.SAN_PRODUCAO
(NR_SEQ_EMP_SAIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANEMPR_FK_I2
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANEQUI_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_EQUIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANINUT_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_INUTIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANPRLO_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_LOCAL_ARMAZ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANPRLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANPROD_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_PROD_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANRESE_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_RESERVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANSTFR_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_STATUS_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPROD_SANSTFR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPROD_SANTIDO_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_TIPO_DOACAO_ISBT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPROD_SANTRAN_FK_I ON TASY.SAN_PRODUCAO
(NR_SEQ_TRANSFUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SAN_PRODUCAO_tp  after update ON TASY.SAN_PRODUCAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_PRODUCAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PRODUCAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PRODUCAO',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SANGUE,1,4000),substr(:new.NR_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SANGUE',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INUTIL,1,4000),substr(:new.NR_SEQ_INUTIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INUTIL',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DERIVADO,1,4000),substr(:new.NR_SEQ_DERIVADO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DERIVADO',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_VOLUME,1,4000),substr(:new.QT_VOLUME,1,4000),:new.nm_usuario,nr_seq_w,'QT_VOLUME',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RESERVA,1,4000),substr(:new.NR_SEQ_RESERVA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RESERVA',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EMP_ENT,1,4000),substr(:new.NR_SEQ_EMP_ENT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EMP_ENT',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SANGUE,1,4000),substr(:new.IE_TIPO_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SANGUE',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATOR_RH,1,4000),substr(:new.IE_FATOR_RH,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATOR_RH',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANSFUSAO,1,4000),substr(:new.NR_SEQ_TRANSFUSAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANSFUSAO',ie_log_w,ds_w,'SAN_PRODUCAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.san_producao_delete
before delete ON TASY.SAN_PRODUCAO for each row
declare

begin

if  ((:old.dt_liberacao is not null) or (:old.dt_fim_producao is not null)) and (:old.nr_seq_doacao is not null) then
	--N�o � poss�vel excluir um hemocomponente liberado ou finalizada a produ��o!
	Wheb_mensagem_pck.exibir_mensagem_abort(264222);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.San_Producao_Update
BEFORE UPDATE ON TASY.SAN_PRODUCAO FOR EACH ROW
declare
qt_procedimento_w		number(9,3);
nr_seq_reserva_w		number(10);
nr_sequencia_w			number(10);
qt_reg_w			number(1);
ie_alterar_usuario_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
dt_emprestimo_w			date;
ie_consite_data_emp_w		varchar2(1);
excluir_todos_proc_w		Varchar(1);
ie_realiza_nat_w		varchar2(1);
nr_seq_reserva_transf_old_w	number(10);
ie_status_hemocomp_res_w	varchar2(15);


BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


obter_param_usuario(450,72, obter_perfil_ativo, wheb_usuario_pck.GET_NM_USUARIO, 0, ie_alterar_usuario_w);
obter_param_usuario(450,91, obter_perfil_ativo, wheb_usuario_pck.GET_NM_USUARIO, 0, ie_consite_data_emp_w);
obter_param_usuario(450,257, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, excluir_todos_proc_w);
/*Par�metro Hemoterapia, [489] - Status do hemocomponente na reserva ap�s cancelar seu uso na transfus�o */
obter_param_usuario(450,489, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_status_hemocomp_res_w);

if	(ie_consite_data_emp_w = 'N') and
	(:new.nr_seq_emp_ent is not null) and
	(:new.dt_utilizacao is not null) and
	(:new.dt_utilizacao <> nvl(:old.dt_utilizacao,sysdate-1)) then

	select 	max(dt_emprestimo)
	into	dt_emprestimo_w
	from 	san_emprestimo
	where	nr_sequencia = :new.nr_seq_emp_ent;

	if	(:new.dt_utilizacao < dt_emprestimo_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192250);

	end if;
end if;

if	(:new.nr_seq_transfusao is not null) and
	(:old.nr_seq_transfusao is null) then
	select	nvl(max(nr_seq_reserva),0)
	into	nr_seq_reserva_w
	from	san_transfusao
	where	nr_sequencia = :new.nr_seq_transfusao;

	if	(nr_seq_reserva_w <> 0) then
		update san_exame_lote
		set 	nr_seq_producao = :new.nr_sequencia,
			ie_origem = 'T'
		where nr_seq_res_prod = (select max(nr_sequencia)
					 from san_reserva_prod
					 where nr_seq_reserva	= nr_seq_reserva_w
					   and nr_seq_producao	= :new.nr_sequencia);
	end if;


	if (ie_alterar_usuario_w = 'S') then
		cd_pessoa_fisica_w := Obter_Dados_Usuario_Opcao(wheb_usuario_pck.GET_NM_USUARIO,'C');
	else
		cd_pessoa_fisica_w := :new.cd_pf_realizou;
	end if;

	if (cd_pessoa_fisica_w = '') then
		cd_pessoa_fisica_w := :new.cd_pf_realizou;
	end if;

	select	max(ie_realiza_nat)
	into	ie_realiza_nat_w
	from	san_doacao a
	where	a.nr_sequencia = :new.nr_seq_doacao;


	gerar_exame_realizado(2, :new.nr_sequencia, null, cd_pessoa_fisica_w, :new.nm_usuario,
				 :new.ie_irradiado, :new.ie_lavado, :new.ie_filtrado, :new.ie_aliquotado,:new.nr_seq_derivado, :new.nr_seq_doacao, null, ie_realiza_nat_w, :new.ie_aferese, wheb_usuario_pck.get_cd_estabelecimento);
elsif ((:new.nr_seq_reserva is not null) and
	 (:old.nr_seq_reserva is null)) or
	((:new.nr_seq_reserva is not null) and
	 (:old.nr_seq_reserva is not null) and
	 (:new.nr_seq_reserva <> :old.nr_seq_reserva)) then

	select	max(ie_realiza_nat)
	into	ie_realiza_nat_w
	from	san_doacao a
	where	a.nr_sequencia = :new.nr_seq_doacao;

	gerar_exame_realizado(9, :new.nr_sequencia, :new.nr_seq_reserva, cd_pessoa_fisica_w, :new.nm_usuario,
				 :new.ie_irradiado, :new.ie_lavado, :new.ie_filtrado, :new.ie_aliquotado,:new.nr_seq_derivado, :new.nr_seq_doacao, null, ie_realiza_nat_w, :new.ie_aferese, wheb_usuario_pck.get_cd_estabelecimento);

elsif	(:new.nr_seq_transfusao is null) and
	(:old.nr_seq_transfusao is not null) then

	:new.ds_justificativa := null;

	delete san_exame_lote
	where nr_seq_producao = :new.nr_sequencia
	  and ie_origem = 'T'
	  and nr_seq_res_prod is null;

	if	(:new.nr_seq_doacao is not null) then
		update	san_exame_realizado a
		set	nr_seq_propaci = null
		where	a.nr_seq_exame_lote in (	select nr_sequencia
						from san_exame_lote x
						where nr_seq_doacao = :new.nr_seq_doacao)
		  and	a.nr_seq_propaci is not null;

	elsif	(:new.nr_seq_emp_ent is not null) then
		update	san_exame_realizado a
		set	nr_seq_propaci = null
		where	a.nr_seq_exame_lote in (	select nr_sequencia
						from san_exame_lote x
						where nr_seq_producao = :new.nr_sequencia)
		  and	a.nr_seq_propaci is not null;
	end if;

	update san_exame_realizado
	set	nr_seq_propaci = null
	where	nr_seq_propaci is not null
	  and	nr_seq_exame_lote in (	select nr_sequencia
					from san_exame_lote
					where nr_seq_res_prod in (	select nr_sequencia
									from san_reserva_prod
									where nr_seq_producao = :new.nr_sequencia));
elsif	((:new.nr_seq_reserva is null) and
	 (:old.nr_seq_reserva is not null)) then
	delete san_exame_lote
	where nr_seq_producao = :new.nr_sequencia
	  and nr_seq_reserva = :old.nr_seq_reserva;

	update	san_exame_realizado a
	set	nr_seq_propaci = null
	where	a.nr_seq_exame_lote = (	select nr_sequencia
					from san_exame_lote x
					where nr_seq_doacao = :new.nr_seq_doacao)
	  and	a.nr_seq_propaci is not null;
end if;

if	(:new.nr_seq_transfusao is null) and
	(:old.nr_seq_transfusao is not null) then

	select	max(nr_seq_reserva)
	into	nr_seq_reserva_transf_old_w
	from 	san_transfusao
	where	nr_sequencia = :old.nr_seq_transfusao;

	update san_reserva_prod
	set	ie_status = ie_status_hemocomp_res_w,
		nr_seq_propaci = null
	where nr_seq_producao = :new.nr_sequencia
	and  nr_seq_reserva = nr_seq_reserva_transf_old_w;

	if	(:new.nr_seq_propaci is not null) then
		:new.nr_seq_propaci := null;
	end if;
end if;

if	(:old.nr_seq_propaci is not null) and
	(:new.nr_seq_propaci is null) then
	select nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from procedimento_paciente
	where nr_sequencia = :old.nr_seq_propaci;

	if	(qt_procedimento_w > 1) and (excluir_todos_proc_w = 'N') then
		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_sequencia = :old.nr_seq_propaci;

		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		update material_atend_paciente
		set qt_material = qt_material - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;
	else
		delete procedimento_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		delete material_atend_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		delete procedimento_paciente
		where nr_sequencia = :old.nr_seq_propaci;
	end if;
end if;

if	(:new.nr_seq_equipo > 0) and
	(:old.nr_seq_equipo <> :new.nr_seq_equipo) then
	san_gravar_log_equipamento(:new.nr_sequencia, :new.nr_seq_equipo, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.SAN_PRODUCAO ADD (
  CONSTRAINT SANPROD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_PRODUCAO ADD (
  CONSTRAINT SANPROD_SANTIDO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOACAO_ISBT) 
 REFERENCES TASY.SAN_TIPO_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANDERI_FK1 
 FOREIGN KEY (NR_SEQ_DERIVADO_ORIGEM) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSERVANTE) 
 REFERENCES TASY.SAN_CONSERVANTE (NR_SEQUENCIA),
  CONSTRAINT SANPROD_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SANPROD_SANARBO_FK 
 FOREIGN KEY (NR_SEQ_ARMAZENAMENTO) 
 REFERENCES TASY.SAN_ARMAZENAMENTO_BOLSA (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANPRLO_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ARMAZ) 
 REFERENCES TASY.SAN_PRODUCAO_LOCAL (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANAGRU_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.SAN_AGRUPAMENTO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANCENT_FK 
 FOREIGN KEY (NR_SEQ_CENTRIFUGA) 
 REFERENCES TASY.SAN_CENTRIFUGA (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANSTFR_FK 
 FOREIGN KEY (NR_SEQ_STATUS_INTEGRACAO) 
 REFERENCES TASY.SAN_INTEGRACAO_STATUS_FRAC (NR_SEQUENCIA),
  CONSTRAINT SANPROD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANPROD_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_ATUAL) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANPROD_SANTRAN_FK 
 FOREIGN KEY (NR_SEQ_TRANSFUSAO) 
 REFERENCES TASY.SAN_TRANSFUSAO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_PESFISI_FK 
 FOREIGN KEY (CD_PF_REALIZOU) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANPROD_SANDOAC_FK 
 FOREIGN KEY (NR_SEQ_DOACAO) 
 REFERENCES TASY.SAN_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANRESE_FK 
 FOREIGN KEY (NR_SEQ_RESERVA) 
 REFERENCES TASY.SAN_RESERVA (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANINUT_FK 
 FOREIGN KEY (NR_SEQ_INUTIL) 
 REFERENCES TASY.SAN_INUTILIZACAO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANEMPR_FK2 
 FOREIGN KEY (NR_SEQ_EMP_SAIDA) 
 REFERENCES TASY.SAN_EMPRESTIMO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPO) 
 REFERENCES TASY.SAN_EQUIPO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANEMPR_FK 
 FOREIGN KEY (NR_SEQ_EMP_ENT) 
 REFERENCES TASY.SAN_EMPRESTIMO (NR_SEQUENCIA),
  CONSTRAINT SANPROD_SANPROD_FK 
 FOREIGN KEY (NR_SEQ_PROD_ORIGEM) 
 REFERENCES TASY.SAN_PRODUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_PRODUCAO TO NIVEL_1;

