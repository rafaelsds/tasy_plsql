ALTER TABLE TASY.TIPO_APLICACAO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_APLICACAO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_APLICACAO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_APLIC    NUMBER(15)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DS_HISTORICO         VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPAPLHIST_CTABATIPAP_FK_I ON TASY.TIPO_APLICACAO_HIST
(NR_SEQ_TIPO_APLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPAPLHIST_ESTABEL_FK_I ON TASY.TIPO_APLICACAO_HIST
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPAPLHIST_PK ON TASY.TIPO_APLICACAO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_APLICACAO_HIST ADD (
  CONSTRAINT TIPAPLHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_APLICACAO_HIST ADD (
  CONSTRAINT TIPAPLHIST_CTABATIPAP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_APLIC) 
 REFERENCES TASY.CONTA_BANCO_TIPO_APLIC (NR_SEQUENCIA),
  CONSTRAINT TIPAPLHIST_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TIPO_APLICACAO_HIST TO NIVEL_1;

