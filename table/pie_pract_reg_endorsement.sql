ALTER TABLE TASY.PIE_PRACT_REG_ENDORSEMENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PIE_PRACT_REG_ENDORSEMENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PIE_PRACT_REG_ENDORSEMENT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ENDORSEMENT_EDIT    DATE                   NOT NULL,
  DS_ENDORSEMENT_DETAIL  VARCHAR2(1920 BYTE)    NOT NULL,
  TP_ENDORSEMENT         VARCHAR2(100 BYTE),
  TP_ENDORSEMENT_SUB     VARCHAR2(100 BYTE),
  NR_SEQ_PIE_PRACT_REG   NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PIEPRAENDR_PIEPRAREG_FK_I ON TASY.PIE_PRACT_REG_ENDORSEMENT
(NR_SEQ_PIE_PRACT_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PIEPRAENDR_PK ON TASY.PIE_PRACT_REG_ENDORSEMENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PIE_PRACT_REG_ENDORSEMENT ADD (
  CONSTRAINT PIEPRAENDR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PIE_PRACT_REG_ENDORSEMENT ADD (
  CONSTRAINT PIEPRAENDR_PIEPRAREG_FK 
 FOREIGN KEY (NR_SEQ_PIE_PRACT_REG) 
 REFERENCES TASY.PIE_PRACT_REGISTRATION (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PIE_PRACT_REG_ENDORSEMENT TO NIVEL_1;

