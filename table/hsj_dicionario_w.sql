DROP TABLE TASY.HSJ_DICIONARIO_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_DICIONARIO_W
(
  DS_PALAVRA  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_DICIONARIO_W TO NIVEL_1;

