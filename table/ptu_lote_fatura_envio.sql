ALTER TABLE TASY.PTU_LOTE_FATURA_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_LOTE_FATURA_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_LOTE_FATURA_ENVIO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_INICIO              DATE                   NOT NULL,
  DT_FIM                 DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_UNIMED_DESTINO  NUMBER(10),
  NR_SEQ_PROTOCOLO       NUMBER(10),
  DT_GERACAO_TITULO      DATE,
  DT_GERACAO_LOTE        DATE,
  NR_SEQ_CAMARA          NUMBER(10),
  NR_SEQ_PERIODO         NUMBER(10),
  DT_INTEGRACAO_CAMARA   DATE,
  NR_SEQ_LOTE_CAMARA     NUMBER(10),
  IE_SEM_CAMARA_COMP     VARCHAR2(3 BYTE),
  DT_GERACAO_A600        DATE,
  DT_PREVISAO_ENVIO      DATE,
  NR_SEQ_CONGENERE       NUMBER(10),
  IE_ESTRUTURA_CONG      VARCHAR2(1 BYTE),
  NR_SEQ_PERIODO_PGTO    NUMBER(10),
  DT_VENCIMENTO          DATE,
  DT_EMISSAO             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOFE_ESTABEL_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOFE_PK ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSCACO_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSCACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSCCPE_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSCCPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSCONG_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_UNIMED_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSCONG_FK2_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSCONG_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSLOCC_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSLOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSPEPA_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_PERIODO_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFE_PLSPRCO_FK_I ON TASY.PTU_LOTE_FATURA_ENVIO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFE_PLSPRCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_LOTE_FATURA_ENVIO ADD (
  CONSTRAINT PLSLOFE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_LOTE_FATURA_ENVIO ADD (
  CONSTRAINT PLSLOFE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOFE_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_UNIMED_DESTINO) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSCACO_FK 
 FOREIGN KEY (NR_SEQ_CAMARA) 
 REFERENCES TASY.PLS_CAMARA_COMPENSACAO (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSCCPE_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.PLS_CAMARA_CALEND_PERIODO (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSLOFE_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO_PGTO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_LOTE_FATURA_ENVIO TO NIVEL_1;

