ALTER TABLE TASY.TISS_REGRA_VALOR_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_VALOR_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_VALOR_PROC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  IE_TISS_TIPO_GUIA       VARCHAR2(2 BYTE)      NOT NULL,
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  IE_REGRA_VALOR          VARCHAR2(5 BYTE)      NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_AREA_PROCEDIMENTO    NUMBER(15),
  CD_ESPECIALIDADE        NUMBER(15),
  CD_GRUPO_PROC           NUMBER(15),
  IE_FUNCAO_MEDICO        VARCHAR2(10 BYTE),
  IE_HONORARIO            VARCHAR2(1 BYTE),
  IE_PACOTE               VARCHAR2(5 BYTE),
  CD_PLANO_CONVENIO       VARCHAR2(10 BYTE),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  IE_TIPO_ATENDIMENTO     NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRVP_AREPROC_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_AREA_PROCEDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRVP_CONPLAN_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_CONVENIO, CD_PLANO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRVP_CONVENI_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRVP_ESPPROC_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRVP_ESTABEL_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRVP_GRUPROC_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRVP_PESJURI_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRVP_PK ON TASY.TISS_REGRA_VALOR_PROC
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRVP_PROCEDI_FK_I ON TASY.TISS_REGRA_VALOR_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRVP_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_VALOR_PROC_tp  after update ON TASY.TISS_REGRA_VALOR_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TISS_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_TISS_TIPO_GUIA,1,4000),substr(:new.IE_TISS_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TISS_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.CD_CGC_PRESTADOR,1,4000),substr(:new.CD_CGC_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_PRESTADOR',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RESPONSAVEL_CREDITO,1,500);gravar_log_alteracao(substr(:old.IE_RESPONSAVEL_CREDITO,1,4000),substr(:new.IE_RESPONSAVEL_CREDITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESPONSAVEL_CREDITO',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_VALOR,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_VALOR,1,4000),substr(:new.IE_REGRA_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_VALOR',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_AREA_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PACOTE,1,500);gravar_log_alteracao(substr(:old.IE_PACOTE,1,4000),substr(:new.IE_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PACOTE',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_GRUPO_PROC,1,500);gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FUNCAO_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_FUNCAO_MEDICO,1,4000),substr(:new.IE_FUNCAO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCAO_MEDICO',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_HONORARIO,1,500);gravar_log_alteracao(substr(:old.IE_HONORARIO,1,4000),substr(:new.IE_HONORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_HONORARIO',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ESPECIALIDADE,1,500);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'TISS_REGRA_VALOR_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_VALOR_PROC ADD (
  CONSTRAINT TISSRVP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_VALOR_PROC ADD (
  CONSTRAINT TISSRVP_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO_CONVENIO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT TISSRVP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSRVP_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TISSRVP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRVP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT TISSRVP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT TISSRVP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT TISSRVP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.TISS_REGRA_VALOR_PROC TO NIVEL_1;

