ALTER TABLE TASY.FILTER_QUERY_DAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FILTER_QUERY_DAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.FILTER_QUERY_DAR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_QUERY               VARCHAR2(50 BYTE),
  DS_QUERY               VARCHAR2(255 BYTE),
  CD_SHARED_WITH         NUMBER(10),
  NR_MAX_ROWS            NUMBER(4),
  IE_CLINICAL_UNIT       VARCHAR2(1 BYTE),
  NR_SEQ_CLINICAL_UNIT   NUMBER(10),
  IE_ENCOUNTER           VARCHAR2(1 BYTE),
  NR_SEQ_ENCOUNTER       NUMBER(10),
  IE_LOGICAL_OPER_GROUP  VARCHAR2(3 BYTE),
  IE_PIVOT               VARCHAR2(1 BYTE),
  DS_DISPLAY_AGG         CLOB,
  IE_TIME_RANGE          VARCHAR2(1 BYTE),
  DT_INITIAL_TR          DATE,
  DT_FINAL_TR            DATE,
  NR_SEQ_TREE            NUMBER(10),
  IE_DATA_ELEMENTS       VARCHAR2(1 BYTE),
  NR_SEQ_DATA_ELEM       NUMBER(10),
  QT_DAYS_TR             NUMBER(5),
  IE_LINKED_FUNCAO       VARCHAR2(2 BYTE),
  IE_DISTINCT            VARCHAR2(2 BYTE),
  IE_CLINICAL_DATA       VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QUERY_DAR_PK ON TASY.FILTER_QUERY_DAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.alt_titulo_arvore_after_insert
after insert or update ON TASY.FILTER_QUERY_DAR for each row
declare

count_exec_sql_w number;

begin

update tree_query_dar set ds_titulo = :new.nm_query where nr_seq_filter = :new.nr_sequencia;

select count(nr_sequencia) into count_exec_sql_w from dar_tables_control where nr_seq_sql = :new.nr_sequencia;

if(count_exec_sql_w > 0) then
	update dar_tables_control set ds_datamodel = :new.nm_query where nr_seq_sql = :new.nr_sequencia;
end if;

end alt_titulo_arvore_after_insert;
/


ALTER TABLE TASY.FILTER_QUERY_DAR ADD (
  CONSTRAINT QUERY_DAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.FILTER_QUERY_DAR TO NIVEL_1;

