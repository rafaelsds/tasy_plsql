ALTER TABLE TASY.HEM_PLAN_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_PLAN_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_PLAN_ETAPA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ETAPA             NUMBER(10),
  NR_SEQ_PLAN          NUMBER(10),
  IE_ACESSO            VARCHAR2(100 BYTE),
  DS_BALAO             VARCHAR2(80 BYTE),
  DS_STENT             VARCHAR2(80 BYTE),
  DS_CATETER           VARCHAR2(80 BYTE),
  DS_CORDA             VARCHAR2(80 BYTE),
  IE_ATC_CONVENCIONAL  VARCHAR2(1 BYTE),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  NR_SEQ_PROC          NUMBER(10),
  DT_REALIZADA         DATE,
  DS_OBSERVACAO_REL    VARCHAR2(255 BYTE),
  NM_USUARIO_REL       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMPLAE_HEMPLAN_FK_I ON TASY.HEM_PLAN_ETAPA
(NR_SEQ_PLAN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMPLAE_HEMPROC_FK_I ON TASY.HEM_PLAN_ETAPA
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMPLAE_PK ON TASY.HEM_PLAN_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEM_PLAN_ETAPA ADD (
  CONSTRAINT HEMPLAE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_PLAN_ETAPA ADD (
  CONSTRAINT HEMPLAE_HEMPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLAN) 
 REFERENCES TASY.HEM_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT HEMPLAE_HEMPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.HEM_PROC (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_PLAN_ETAPA TO NIVEL_1;

