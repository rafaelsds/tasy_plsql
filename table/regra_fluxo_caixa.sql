ALTER TABLE TASY.REGRA_FLUXO_CAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_FLUXO_CAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_FLUXO_CAIXA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_TITULO              VARCHAR2(100 BYTE),
  DS_FUNCIONAMENTO       VARCHAR2(2000 BYTE),
  CD_CONTA_FINANC        NUMBER(10)             NOT NULL,
  IE_REGRA_ORIGEM        VARCHAR2(3 BYTE)       NOT NULL,
  IE_REGRA_DATA          VARCHAR2(3 BYTE)       NOT NULL,
  NR_DIA_FIXO            NUMBER(2)              NOT NULL,
  QT_MES_ANTERIOR        NUMBER(3)              NOT NULL,
  VL_FIXO                NUMBER(15,2)           NOT NULL,
  IE_ACUMULAR_FLUXO      VARCHAR2(1 BYTE)       NOT NULL,
  DT_VIGENCIA_INICIAL    DATE,
  DT_VIGENCIA_FINAL      DATE,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ANTECIPAR_DIA_UTIL  VARCHAR2(1 BYTE)       NOT NULL,
  IE_DIA_FIXO            VARCHAR2(1 BYTE)       NOT NULL,
  PR_FLUXO               NUMBER(7,4),
  IE_CLASSIF_FLUXO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGFLCA_CONFINA_FK_I ON TASY.REGRA_FLUXO_CAIXA
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGFLCA_ESTABEL_FK_I ON TASY.REGRA_FLUXO_CAIXA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGFLCA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGFLCA_PK ON TASY.REGRA_FLUXO_CAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_FLUXO_CAIXA_tp  after update ON TASY.REGRA_FLUXO_CAIXA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CLASSIF_FLUXO,1,4000),substr(:new.IE_CLASSIF_FLUXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_FLUXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_FLUXO,1,4000),substr(:new.PR_FLUXO,1,4000),:new.nm_usuario,nr_seq_w,'PR_FLUXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_ORIGEM,1,4000),substr(:new.IE_REGRA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_ORIGEM',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_DATA,1,4000),substr(:new.IE_REGRA_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIA_FIXO,1,4000),substr(:new.NR_DIA_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIA_FIXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANTECIPAR_DIA_UTIL,1,4000),substr(:new.IE_ANTECIPAR_DIA_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ANTECIPAR_DIA_UTIL',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FIXO,1,4000),substr(:new.VL_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'VL_FIXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACUMULAR_FLUXO,1,4000),substr(:new.IE_ACUMULAR_FLUXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACUMULAR_FLUXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_INICIAL,1,4000),substr(:new.DT_VIGENCIA_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_INICIAL',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_FINAL,1,4000),substr(:new.DT_VIGENCIA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_FINAL',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIA_FIXO,1,4000),substr(:new.IE_DIA_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIA_FIXO',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MES_ANTERIOR,1,4000),substr(:new.QT_MES_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_MES_ANTERIOR',ie_log_w,ds_w,'REGRA_FLUXO_CAIXA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_FLUXO_CAIXA ADD (
  CONSTRAINT REGFLCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_FLUXO_CAIXA ADD (
  CONSTRAINT REGFLCA_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT REGFLCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_FLUXO_CAIXA TO NIVEL_1;

