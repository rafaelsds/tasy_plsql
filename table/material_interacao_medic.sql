ALTER TABLE TASY.MATERIAL_INTERACAO_MEDIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_INTERACAO_MEDIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_INTERACAO_MEDIC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_MATERIAL             NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_MATERIAL_INTERACAO   NUMBER(10),
  IE_SEVERIDADE           VARCHAR2(3 BYTE),
  DS_TIPO                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_FICHA            NUMBER(10),
  NR_SEQ_FICHA_INTERACAO  NUMBER(10),
  DS_ORIENTACAO           VARCHAR2(2000 BYTE),
  CD_GRUPO_MATERIAL       NUMBER(3),
  CD_SUBGRUPO_MATERIAL    NUMBER(3),
  CD_CLASSE_MATERIAL      NUMBER(5),
  IE_EXIBIR_GRAVIDADE     VARCHAR2(1 BYTE),
  IE_MENSAGEM_CADASTRADA  VARCHAR2(1 BYTE),
  DS_REF_BIBLIOGRAFICA    VARCHAR2(4000 BYTE),
  IE_CONSISTIR            VARCHAR2(15 BYTE),
  IE_GRAVAR               VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4),
  IE_CLASSIFICACAO        VARCHAR2(15 BYTE),
  IE_FUNCAO_PRESCRITOR    VARCHAR2(3 BYTE),
  DS_OBSERVATION_NOTES    VARCHAR2(256 BYTE),
  DS_PRECAUTION_NOTES     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATINME_CLAMATE_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATINME_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATINME_ESTABEL_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATINME_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATINME_GRUMATE_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATINME_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATINME_MATERIA_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINME_MATERIA_FK2_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_MATERIAL_INTERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINME_MEDFITE_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINME_MEDFITE_FK2_I ON TASY.MATERIAL_INTERACAO_MEDIC
(NR_SEQ_FICHA_INTERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATINME_PK ON TASY.MATERIAL_INTERACAO_MEDIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINME_SUBMATE_FK_I ON TASY.MATERIAL_INTERACAO_MEDIC
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATINME_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.material_interacao_medic_ins
before insert ON TASY.MATERIAL_INTERACAO_MEDIC for each row
declare

begin
:new.cd_estabelecimento := null;

end;
/


ALTER TABLE TASY.MATERIAL_INTERACAO_MEDIC ADD (
  CONSTRAINT MATINME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_INTERACAO_MEDIC ADD (
  CONSTRAINT MATINME_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATINME_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATINME_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_INTERACAO) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATINME_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATINME_MEDFITE_FK2 
 FOREIGN KEY (NR_SEQ_FICHA_INTERACAO) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT MATINME_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT MATINME_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT MATINME_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL));

GRANT SELECT ON TASY.MATERIAL_INTERACAO_MEDIC TO NIVEL_1;

