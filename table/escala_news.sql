ALTER TABLE TASY.ESCALA_NEWS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NEWS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NEWS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  QT_SATURACAO_02            NUMBER(3),
  QT_FREQ_RESP               NUMBER(3),
  QT_TEMP                    NUMBER(4,1),
  IE_O2_SUPLEMENTAR          VARCHAR2(1 BYTE),
  QT_PA_SISTOLICA            NUMBER(3),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  QT_FREQ_CARDIACA           NUMBER(3),
  IE_NIVEL_CONSCIENCIA       VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PONTUACAO               NUMBER(3),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCNEWS_ATEPACI_FK_I ON TASY.ESCALA_NEWS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNEWS_PESFISI_FK_I ON TASY.ESCALA_NEWS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCNEWS_PK ON TASY.ESCALA_NEWS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNEWS_TASASDI_FK_I ON TASY.ESCALA_NEWS
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNEWS_TASASDI_FK2_I ON TASY.ESCALA_NEWS
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_news_atual
before insert or update ON TASY.ESCALA_NEWS for each row
declare

qt_reg_w number(10);

begin

:new.qt_pontuacao := 0;

if (:new.qt_freq_resp <= 8) then
	:new.qt_pontuacao := 3;
elsif (:new.qt_freq_resp between 9 and 11) then
	:new.qt_pontuacao := 1;
elsif (:new.qt_freq_resp between 21 and 24) then
	:new.qt_pontuacao := 2;
elsif (:new.qt_freq_resp >= 25) then
	:new.qt_pontuacao := 3;
end if;

if (:new.qt_saturacao_02 <= 91) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
elsif (:new.qt_saturacao_02 between 92 and 93) then
	:new.qt_pontuacao := :new.qt_pontuacao + 2;
elsif (:new.qt_saturacao_02 between 94 and 95) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
end if;

if (nvl(:new.ie_o2_suplementar,'N') = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 2;
end if;

if (:new.qt_temp <= 35) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
elsif (:new.qt_temp between 35.1 and 36) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
elsif (:new.qt_temp between 38.1 and 39) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
elsif (:new.qt_temp >= 39.1) then
	:new.qt_pontuacao := :new.qt_pontuacao + 2;
end if;

if (:new.qt_pa_sistolica <= 90) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
elsif (:new.qt_pa_sistolica between 91 and 100) then
	:new.qt_pontuacao := :new.qt_pontuacao + 2;
elsif (:new.qt_pa_sistolica between 101 and 110) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
elsif (:new.qt_pa_sistolica >= 220) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
end if;

if (:new.qt_freq_cardiaca <= 40) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
elsif (:new.qt_freq_cardiaca between 41 and 50) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
elsif (:new.qt_freq_cardiaca between 91 and 110) then
	:new.qt_pontuacao := :new.qt_pontuacao + 1;
elsif (:new.qt_freq_cardiaca between 111 and 130) then
	:new.qt_pontuacao := :new.qt_pontuacao + 2;
elsif (:new.qt_freq_cardiaca >= 131) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
end if;

if (:new.ie_nivel_consciencia in ('1','2','3')) then
	:new.qt_pontuacao := :new.qt_pontuacao + 3;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_news_pend_atual
after insert or update ON TASY.ESCALA_NEWS for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'183');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_NEWS_delete
after delete ON TASY.ESCALA_NEWS for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '183'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_NEWS ADD (
  CONSTRAINT ESCNEWS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NEWS ADD (
  CONSTRAINT ESCNEWS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCNEWS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCNEWS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCNEWS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_NEWS TO NIVEL_1;

