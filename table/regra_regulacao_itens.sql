ALTER TABLE TASY.REGRA_REGULACAO_ITENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_REGULACAO_ITENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_REGULACAO_ITENS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_REGRA_REG            NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_JUSTIFICAR               VARCHAR2(2 BYTE),
  NR_SEQ_AVALIACAO            NUMBER(10),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  DS_SQL                      VARCHAR2(4000 BYTE),
  DS_TITULO                   VARCHAR2(80 BYTE),
  DS_DOCUMENTACAO             VARCHAR2(200 BYTE),
  IE_ESPECIALIDADE            VARCHAR2(2 BYTE),
  IE_CID                      VARCHAR2(2 BYTE),
  IE_CONFIRMA_ENCAMINHAMENTO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REREITE_PK ON TASY.REGRA_REGULACAO_ITENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REREITE_REGREGU_FK_I ON TASY.REGRA_REGULACAO_ITENS
(NR_SEQ_REGRA_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.regra_regulacao_itens_atual
before insert or update ON TASY.REGRA_REGULACAO_ITENS for each row
declare
customSQL_w     varchar2(4000);
ie_resultado_w  varchar2(02);
begin

  if (:new.ie_situacao = 'A') then

    customSQL_w :=  ' select nvl(max(''S''), ''N'') ' ||
                    ' from  atendimento_paciente a'     ||
                    ' where  a.nr_atendimento = -1  '   ||
                    :new.DS_SQL ;

    begin
      execute immediate customSQL_w
      into   ie_resultado_w;

    exception
    when others then
      Wheb_mensagem_pck.exibir_mensagem_abort(751853, 'SQL=' || customSQL_w || ';ERRO='||sqlerrm);
    end;


  end if;

end;
/


ALTER TABLE TASY.REGRA_REGULACAO_ITENS ADD (
  CONSTRAINT REREITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_REGULACAO_ITENS ADD (
  CONSTRAINT REREITE_REGREGU_FK 
 FOREIGN KEY (NR_SEQ_REGRA_REG) 
 REFERENCES TASY.REGRA_REGULACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_REGULACAO_ITENS TO NIVEL_1;

