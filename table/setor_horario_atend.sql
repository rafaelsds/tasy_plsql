ALTER TABLE TASY.SETOR_HORARIO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SETOR_HORARIO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.SETOR_HORARIO_ATEND
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_HORARIO            DATE                    NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  HR_LIMITE             VARCHAR2(5 BYTE),
  DT_LIMITE             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SETHOAT_PK ON TASY.SETOR_HORARIO_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETHOAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.SETHOAT_SETATEN_FK_I ON TASY.SETOR_HORARIO_ATEND
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atual_setor_horario_atend
before insert or update ON TASY.SETOR_HORARIO_ATEND for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if(:new.DT_HORARIO is not null) then
		:new.dt_horario := to_date('30/12/1899' || ' ' || to_char(:new.dt_horario,'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');
	end if;

	if (:new.hr_limite is not null) and
	   ((:new.hr_limite <> :old.hr_limite) or (:old.dt_limite is null)) then
		:new.dt_limite := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_limite || ':00', 'dd/mm/yyyy hh24:mi:ss');
	end if;

end if;

end;
/


ALTER TABLE TASY.SETOR_HORARIO_ATEND ADD (
  CONSTRAINT SETHOAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SETOR_HORARIO_ATEND ADD (
  CONSTRAINT SETHOAT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.SETOR_HORARIO_ATEND TO NIVEL_1;

