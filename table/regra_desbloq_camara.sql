ALTER TABLE TASY.REGRA_DESBLOQ_CAMARA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DESBLOQ_CAMARA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DESBLOQ_CAMARA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_VALOR   NUMBER(10)               NOT NULL,
  NR_SEQ_CAMARA        NUMBER(10)               NOT NULL,
  QT_DIA_DESBLOQUEIO   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REDECAM_PK ON TASY.REGRA_DESBLOQ_CAMARA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REDECAM_PK
  MONITORING USAGE;


CREATE INDEX TASY.REDECAM_REDEVAL_FK_I ON TASY.REGRA_DESBLOQ_CAMARA
(NR_SEQ_REGRA_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REDECAM_REDEVAL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_DESBLOQ_CAMARA ADD (
  CONSTRAINT REDECAM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DESBLOQ_CAMARA ADD (
  CONSTRAINT REDECAM_REDEVAL_FK 
 FOREIGN KEY (NR_SEQ_REGRA_VALOR) 
 REFERENCES TASY.REGRA_DESBLOQ_VALOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_DESBLOQ_CAMARA TO NIVEL_1;

