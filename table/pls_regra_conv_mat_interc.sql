ALTER TABLE TASY.PLS_REGRA_CONV_MAT_INTERC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_CONV_MAT_INTERC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_CONV_MAT_INTERC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_TABELA          NUMBER(1),
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  CD_MATERIAL_INICIAL     NUMBER(15),
  CD_MATERIAL_FINAL       NUMBER(15),
  NR_SEQ_MATERIAL         NUMBER(10),
  IE_ENVIO_RECEB          VARCHAR2(1 BYTE)      NOT NULL,
  CD_MATERIAL_A900        VARCHAR2(30 BYTE),
  NR_SEQ_MATERIAL_UNIMED  NUMBER(10),
  IE_PTU_A500             VARCHAR2(1 BYTE),
  IE_PTU_SCS              VARCHAR2(1 BYTE),
  IE_PTU_A700             VARCHAR2(1 BYTE),
  IE_TIPO_DESPESA_MAT     VARCHAR2(2 BYTE),
  IE_TIPO_INTERCAMBIO     VARCHAR2(10 BYTE)     NOT NULL,
  IE_SOMENTE_CODIGO       VARCHAR2(1 BYTE),
  IE_PTU_A1200            VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRCMI_I1 ON TASY.PLS_REGRA_CONV_MAT_INTERC
(IE_ENVIO_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRCMI_PK ON TASY.PLS_REGRA_CONV_MAT_INTERC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCMI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCMI_PLSMAT_FK_I ON TASY.PLS_REGRA_CONV_MAT_INTERC
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCMI_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCMI_PLSMAUN_FK_I ON TASY.PLS_REGRA_CONV_MAT_INTERC
(NR_SEQ_MATERIAL_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCMI_PLSMAUN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_CONV_MAT_INTERC_tp  after update ON TASY.PLS_REGRA_CONV_MAT_INTERC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SOMENTE_CODIGO,1,4000),substr(:new.IE_SOMENTE_CODIGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_CODIGO',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_SCS,1,4000),substr(:new.IE_PTU_SCS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_SCS',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA_MAT,1,4000),substr(:new.IE_TIPO_DESPESA_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA_MAT',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_A700,1,4000),substr(:new.IE_PTU_A700,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A700',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_A1200,1,4000),substr(:new.IE_PTU_A1200,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A1200',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TABELA,1,4000),substr(:new.IE_TIPO_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TABELA',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_INICIAL,1,4000),substr(:new.CD_MATERIAL_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_INICIAL',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_FINAL,1,4000),substr(:new.CD_MATERIAL_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_FINAL',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENVIO_RECEB,1,4000),substr(:new.IE_ENVIO_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENVIO_RECEB',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_A900,1,4000),substr(:new.CD_MATERIAL_A900,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_A900',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL_UNIMED,1,4000),substr(:new.NR_SEQ_MATERIAL_UNIMED,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL_UNIMED',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_A500,1,4000),substr(:new.IE_PTU_A500,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A500',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_INTERCAMBIO,1,4000),substr(:new.IE_TIPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_CONV_MAT_INTERC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_CONV_MAT_INTERC ADD (
  CONSTRAINT PLSRCMI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_CONV_MAT_INTERC ADD (
  CONSTRAINT PLSRCMI_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSRCMI_PLSMAUN_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_UNIMED) 
 REFERENCES TASY.PLS_MATERIAL_UNIMED (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_CONV_MAT_INTERC TO NIVEL_1;

