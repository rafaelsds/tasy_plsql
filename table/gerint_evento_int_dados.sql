ALTER TABLE TASY.GERINT_EVENTO_INT_DADOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GERINT_EVENTO_INT_DADOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.GERINT_EVENTO_INT_DADOS
(
  NR_SEQ_EVENTO                NUMBER(16)       NOT NULL,
  DS_IDENT_LEITO               VARCHAR2(255 BYTE),
  DS_COD_TIPO_LEITO            VARCHAR2(255 BYTE),
  DS_IDENT_MOTIVO_BLOQUEIO     VARCHAR2(255 BYTE),
  IE_LEITO_EXTRA               VARCHAR2(1 BYTE),
  DT_ALTA                      DATE,
  DT_OBITO                     DATE,
  DS_MOTIVO_ALTA               VARCHAR2(255 BYTE),
  DS_JUSTIF_TRANSFERENCIA      VARCHAR2(255 BYTE),
  NR_CARTAO_SUS                VARCHAR2(15 BYTE),
  DS_TIPO_INTERNACAO           VARCHAR2(15 BYTE),
  DS_TIPO_PROTOCOLO_ORIGEM     VARCHAR2(15 BYTE),
  NR_PROTOCOLO_ORIGEM          VARCHAR2(12 BYTE),
  DS_TIPO_ACESSO               VARCHAR2(60 BYTE),
  IE_INTERNACAO_PROPRIA        VARCHAR2(1 BYTE),
  DS_TIPO_LEITO                VARCHAR2(60 BYTE),
  CD_CID_PRINCIPAL             VARCHAR2(4 BYTE),
  NR_CPF_PROF_SOLICITANTE      VARCHAR2(11 BYTE),
  DS_SINAIS_SINTOMAS           VARCHAR2(2000 BYTE),
  DS_JUSTIFICATIVA_INTERNACAO  VARCHAR2(2000 BYTE),
  DS_COR                       VARCHAR2(60 BYTE),
  QT_FREQ_CARDIACA             NUMBER(3),
  QT_FREQ_RESPIRATORIA         NUMBER(3),
  QT_PRESSAO_ART_MAXIMA        NUMBER(3),
  QT_PRESSAO_ART_MINIMA        NUMBER(3),
  QT_TEMPERATURA               NUMBER(4,1),
  QT_SATURACAO_O2              NUMBER(3),
  DS_NIVEL_CONSCIENCIA         VARCHAR2(60 BYTE),
  DS_DEBITO_URINARIO           VARCHAR2(60 BYTE),
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  IE_DIALISE                   VARCHAR2(1 BYTE),
  DS_SUPORTE_O2                VARCHAR2(15 BYTE),
  QT_FLUXO                     NUMBER(15,4),
  QT_FIO2                      NUMBER(15,4),
  QT_SAT_O2                    NUMBER(15,4),
  QT_PEEP                      NUMBER(15,4),
  DS_EVOLUCAO_CASO             VARCHAR2(3000 BYTE),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  CD_UNIDADE_BASICA            VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL             VARCHAR2(10 BYTE),
  NR_CPF_PACIENTE              VARCHAR2(11 BYTE),
  DS_CONDICAO                  VARCHAR2(40 BYTE),
  NM_PESSOA_FISICA             VARCHAR2(60 BYTE),
  DS_SEXO_PF                   VARCHAR2(20 BYTE),
  QT_IDADE_PF                  NUMBER(3),
  DS_ENDERECO_PF               VARCHAR2(255 BYTE),
  DS_MUNICIPIO_RESIDENCIA_PF   VARCHAR2(100 BYTE),
  QT_DINAMICA                  NUMBER(10),
  IE_TOQUE_NAO_REALIZADO       VARCHAR2(1 BYTE),
  QT_BCF                       NUMBER(3),
  IE_BOLSA_INTEGRA             VARCHAR2(1 BYTE),
  DT_RUPTURA                   DATE,
  DS_COR_LIQUIDO_AMINIOTICO    VARCHAR2(100 BYTE),
  IE_GRUMOS                    VARCHAR2(1 BYTE),
  IE_ECOGRAFIA                 VARCHAR2(1 BYTE),
  DS_ECOGRAFIA                 VARCHAR2(500 BYTE),
  IE_DROGAS_VASOATIVAS         VARCHAR2(1 BYTE),
  IE_TIPO_LEITO                VARCHAR2(2 BYTE),
  CD_MUNICIPIO_IBGE            VARCHAR2(6 BYTE),
  NR_CERIH_TRANSF              VARCHAR2(12 BYTE),
  DS_PROCEDIMENTO_RET_TRANSF   VARCHAR2(100 BYTE),
  NR_CARTAO_SUS_RET_TRANSF     VARCHAR2(15 BYTE),
  DT_INTERNACAO                DATE,
  DS_JUSTIF_DATA_RETROATIVA    VARCHAR2(1000 BYTE),
  DS_ORIENTACAO                CLOB,
  CD_DOENCA                    VARCHAR2(10 BYTE),
  DS_RECOMENDACAO              VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_ORIENTACAO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GEEVINDA_CIDDOEN_FK_I ON TASY.GERINT_EVENTO_INT_DADOS
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEEVINDA_CIDDOEN_FK2_I ON TASY.GERINT_EVENTO_INT_DADOS
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GEEVINDA_PK ON TASY.GERINT_EVENTO_INT_DADOS
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEEVINDA_PROCEDI_FK_I ON TASY.GERINT_EVENTO_INT_DADOS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEEVINDA_UNIATEN_FK_I ON TASY.GERINT_EVENTO_INT_DADOS
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GERINT_EV_INT_DADOS_AFTINSERT
after insert ON TASY.GERINT_EVENTO_INT_DADOS for each row
declare

nr_evento_integracao_w		number(10);
nm_usuario_w				gerint_evento_integracao.nm_usuario%type;
reg_integracao_w			gerar_int_padrao.reg_integracao; --Nao e utilizado. Apenas criado pois e obrigatorio na chamada da procedure GRAVAR_INTEGRACAO.
ds_param_adicional_w		varchar2(100);
nr_cerih_w					gerint_evento_integracao.nr_protocolo_solicitacao%type;
cd_estab_w					GERINT_EVENTO_INTEGRACAO.cd_estabelecimento%type;

begin
	/*O evento 12 sempre sera gerado na trigger GERINT_EVENTO_INT_AFTINSERT pois nao envia dados, apenas recebe*/

	SELECT	DECODE(id_evento,
					1,88, 	--Servico de bloqueio de leito
					2,89, 	--Servico de desbloqueio de leito
					3,90, 	--Servico de internacao
					4,91, 	--Servico de internacao em um leito extra
					5,92, 	--Servico de liberacao de internacao
					6,93, 	--Servico de transferencia do leito da internacao
					7,94, 	--Servico de reversao da internacao
					8,95, 	--Servico de reversao da alta/obito
					9,96, 	--Servico de solicitacao de internacao
					10,97,	--Servico de nova evolucao do paciente
					11,99,	--Servico de permuta de leito
					12,100,	--Servico de consulta da situacao das Solicitacoes
					13,101, --Servico de identificacao do paciente
					14,107, --Servico de reinternacao da solicitacao
					16,136),--Servico de solicitacao de transferencia de paciente.
			nm_usuario,
			nr_protocolo_solicitacao,
			cd_Estabelecimento
	INTO	nr_evento_integracao_w,
			nm_usuario_w,
			nr_cerih_w,
			cd_estab_w
	FROM	gerint_evento_integracao
	where	nr_sequencia = :new.nr_seq_evento
	and		ie_situacao = 'N';

	reg_integracao_w.cd_estab_documento := cd_estab_w;

	if (nr_evento_integracao_w = 101) then
		ds_param_adicional_w := 'numeroCerih='||nr_cerih_w||';';
	end if;

	if (((nr_evento_integracao_w is not null) and (nr_evento_integracao_w <> 12)) and (:new.nr_seq_evento > 0))then
		gerar_int_padrao.gravar_integracao(nr_evento_integracao_w,:new.nr_seq_evento,nm_usuario_w,reg_integracao_w,ds_param_adicional_w);
	end if;
end;
/


ALTER TABLE TASY.GERINT_EVENTO_INT_DADOS ADD (
  CONSTRAINT GEEVINDA_PK
 PRIMARY KEY
 (NR_SEQ_EVENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GERINT_EVENTO_INT_DADOS ADD (
  CONSTRAINT GEEVINDA_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT GEEVINDA_GEEVIN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.GERINT_EVENTO_INTEGRACAO (NR_SEQUENCIA),
  CONSTRAINT GEEVINDA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT GEEVINDA_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL),
  CONSTRAINT GEEVINDA_CIDDOEN_FK2 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.GERINT_EVENTO_INT_DADOS TO NIVEL_1;

