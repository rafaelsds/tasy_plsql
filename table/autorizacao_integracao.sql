ALTER TABLE TASY.AUTORIZACAO_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_INTEGRACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_LISTA_PROCEDIMENTOS  VARCHAR2(2000 BYTE),
  CD_STATUS_AUTORIZACAO   VARCHAR2(2 BYTE),
  NR_SEQUENCIA_AUTOR      NUMBER(10)            NOT NULL,
  NR_SEQ_REGRA_GERACAO    NUMBER(10),
  DS_LISTA_PROC_RETORNO   VARCHAR2(2000 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  CD_SENHA                VARCHAR2(20 BYTE),
  CD_AUTORIZACAO          VARCHAR2(20 BYTE),
  DS_LISTA_SEQUENCES      VARCHAR2(2000 BYTE),
  DS_ERRO_PROC            VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTORINTEG_AUTCONV_FK_I ON TASY.AUTORIZACAO_INTEGRACAO
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUTORINTEG_PK ON TASY.AUTORIZACAO_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTORINTEG_REGAUTINT_FK_I ON TASY.AUTORIZACAO_INTEGRACAO
(NR_SEQ_REGRA_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.autorizacao_integracao_update
before update ON TASY.AUTORIZACAO_INTEGRACAO 
for each row
declare
		     
				     
				     

tamanho_lista 		number(15) := 0;
cd_item			varchar2(255);

ds_lista		varchar2(2000);
qt_item			varchar2(20);

qt_pendente_w		number(5) := 0;
qt_autorizada_w		number(5) := 0;
nr_seq_estagio_w	number(10);
ds_erro_w		varchar2(2000);
ds_modulo_w		varchar2(48);
ds_action_old_w		varchar2(32) := '';
ds_action_w		varchar2(32) := '';
qt_reg_w		number(1);
begin

if	wheb_usuario_pck.get_ie_executar_trigger = 'N' then
	goto final;
end if;

DBMS_APPLICATION_INFO.READ_MODULE(ds_modulo_w, ds_action_w);
	
if	(Nvl(ds_action_w,'X') <> 'AUTORIZACAO_CONVENIO_AFTUPDATE') then

	DBMS_APPLICATION_INFO.SET_ACTION('AUTORIZACAO_INTEGRACAO_UPDATE');
	begin
	if	(:new.cd_status_autorizacao = 'PA') and
		(:old.cd_status_autorizacao <> 'PA') then
		
		if	(Nvl(:new.ds_lista_proc_retorno,'X') <> Nvl(:old.ds_lista_proc_retorno,'X')) then
		
			ds_lista 	:= :new.ds_lista_proc_retorno;
			tamanho_lista := length(ds_lista);

			for i in 1..tamanho_lista  loop
				
				if	(substr(:new.ds_lista_proc_retorno,i,1) = 'C') then
						
					cd_item := substr(ds_lista,2,(instr(ds_lista,'Q',1,1)-2));

					if	(instr(ds_lista,'C',1,2) > 0) then
						qt_item := substr(ds_lista,(instr(ds_lista,'Q',1,1)+1),(instr(ds_lista,'C',1,2) - (instr(ds_lista,'Q',1,1)+1)));
					else
						qt_item := substr(ds_lista,(instr(ds_lista,'Q',1,1)+1),100);
					end if;
					
					if (instr(ds_lista,'C',1,2) = 0) then
						ds_lista := substr(ds_lista,2,length(ds_lista));
					else
						ds_lista := substr(ds_lista,instr(ds_lista,'C',1,2),length(ds_lista));
					end if;
					
					begin
					if	(to_number(qt_item) > 0) then
						qt_item := to_number(qt_item);
					end if;
					exception
					when others then
						Wheb_mensagem_pck.exibir_mensagem_abort(949182,'QT_ITEM='||qt_item);
					end;
					
			
					
					begin
					if	(to_number(cd_item) > 0) then
						cd_item := to_number(cd_item);
					end if;
					exception
					when others then
						Wheb_mensagem_pck.exibir_mensagem_abort(949181,'CD_ITEM='||cd_item);
					end;
					
				
					update	procedimento_autorizado
					set	qt_autorizada 	= qt_item,
						nm_usuario	= wheb_mensagem_pck.get_texto(934860),
						dt_atualizacao	= sysdate
					where	nr_sequencia_autor = :new.nr_sequencia_autor
					and	obter_se_contido(nr_sequencia,:new.ds_lista_sequences) = 'S'
					and	((cd_procedimento_tuss = cd_item) or
						(cd_procedimento_convenio = cd_item) or
						(cd_procedimento	= cd_item));

				end if;
				
			end loop;
			
			if	(tamanho_lista = length(ds_lista)) then
				Wheb_mensagem_pck.exibir_mensagem_abort(949181,'CD_ITEM='||ds_lista);
			end if;
		end if;
		
		if	(:new.cd_senha is not null) then
			begin
			select	min(nr_sequencia)
			into	nr_seq_estagio_w
			from	estagio_autorizacao
			where	ie_situacao	= 'A'
			and	nvl(ie_parcial,'N') = 'S'
			and	ie_interno	= '10';
			
			update	autorizacao_convenio
			set	cd_senha	= :new.cd_senha,
				dt_atualizacao	= sysdate,
				nm_usuario	= wheb_mensagem_pck.get_texto(934860),
				dt_retorno	= sysdate
			where	nr_sequencia	= :new.nr_sequencia_autor;
			
			
			ATUALIZAR_AUTORIZACAO_CONVENIO(:new.nr_sequencia_autor,wheb_mensagem_pck.get_texto(934860),nr_seq_estagio_w,'N','N','N');
			end;
		end if;
		

	elsif	(:new.cd_status_autorizacao = 'A') and
		(:old.cd_status_autorizacao <> 'A') and
		(:new.cd_senha is not null) then	

		select	min(nr_sequencia)
		into	nr_seq_estagio_w
		from	estagio_autorizacao
		where	ie_situacao	= 'A'
		and	nvl(ie_parcial,'N') = 'N'
		and	ie_interno	= '10';
		
		

		update	autorizacao_convenio
		set	cd_senha	= :new.cd_senha,
			dt_atualizacao	= sysdate,
			dt_retorno	= sysdate,
			nm_usuario	= wheb_mensagem_pck.get_texto(934860)
		where	nr_sequencia	= :new.nr_sequencia_autor;	
		
		update   procedimento_autorizado 
		 set    	qt_autorizada 		= nvl(qt_solicitada,0),
			dt_atualizacao		= sysdate,
			nm_usuario		= wheb_mensagem_pck.get_texto(934860)
		 where  	nr_sequencia_autor  	= :new.nr_sequencia_autor;
						
		ATUALIZAR_AUTORIZACAO_CONVENIO(:new.nr_sequencia_autor,wheb_mensagem_pck.get_texto(934860),nr_seq_estagio_w,'N','N','N');

	elsif	(:new.cd_status_autorizacao = 'N') and
		(:old.cd_status_autorizacao <> 'N') then	


		select	min(nr_sequencia)
		into	nr_seq_estagio_w
		from	estagio_autorizacao
		where	ie_situacao	= 'A'
		and	ie_interno	= '90';
		
		update	autorizacao_convenio
		set	dt_atualizacao	= sysdate,
			dt_retorno	= sysdate,
			nm_usuario	= wheb_mensagem_pck.get_texto(934860)
		where	nr_sequencia	= :new.nr_sequencia_autor;
		
		ATUALIZAR_AUTORIZACAO_CONVENIO(:new.nr_sequencia_autor,wheb_mensagem_pck.get_texto(934860),nr_seq_estagio_w,'N','N','N');
	elsif	(:new.cd_status_autorizacao = 'E') and
		(:old.cd_status_autorizacao <> 'E') then
		
		select	min(nr_sequencia)
		into	nr_seq_estagio_w
		from	estagio_autorizacao
		where	ie_situacao	= 'A'
		and	ie_interno	= '5';
		
		ATUALIZAR_AUTORIZACAO_CONVENIO(:new.nr_sequencia_autor,wheb_mensagem_pck.get_texto(934860),nr_seq_estagio_w,'N','N','N');
	end if;
	DBMS_APPLICATION_INFO.SET_ACTION(ds_action_w);
	
	
	exception
	when others then
		:new.ds_erro_proc := :old.ds_erro_proc || chr(13)|| sqlerrm;
		:new.cd_status_autorizacao := 'EI'; 
		DBMS_APPLICATION_INFO.SET_ACTION(ds_action_w);
	end;
end if;
							
<<final>>
qt_reg_w := 0;
end;
/


ALTER TABLE TASY.AUTORIZACAO_INTEGRACAO ADD (
  CONSTRAINT AUTORINTEG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_INTEGRACAO ADD (
  CONSTRAINT AUTORINTEG_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTORINTEG_REGAUTINT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GERACAO) 
 REFERENCES TASY.REGRA_AUTOR_INTEGRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AUTORIZACAO_INTEGRACAO TO NIVEL_1;

