ALTER TABLE TASY.REGRA_ETIQUETA_ENF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_ETIQUETA_ENF CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_ETIQUETA_ENF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_EMITE              VARCHAR2(1 BYTE)        NOT NULL,
  CD_MATERIAL           NUMBER(6),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_GRUPO_MATERIAL     NUMBER(3),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGETEN_CLAMATE_FK_I ON TASY.REGRA_ETIQUETA_ENF
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGETEN_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGETEN_GRUMATE_FK_I ON TASY.REGRA_ETIQUETA_ENF
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGETEN_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGETEN_MATERIA_FK_I ON TASY.REGRA_ETIQUETA_ENF
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGETEN_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGETEN_PK ON TASY.REGRA_ETIQUETA_ENF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGETEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.REGETEN_SUBMATE_FK_I ON TASY.REGRA_ETIQUETA_ENF
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGETEN_SUBMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_ETIQUETA_ENF ADD (
  CONSTRAINT REGETEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_ETIQUETA_ENF ADD (
  CONSTRAINT REGETEN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT REGETEN_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT REGETEN_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT REGETEN_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_ETIQUETA_ENF TO NIVEL_1;

