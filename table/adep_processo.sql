ALTER TABLE TASY.ADEP_PROCESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADEP_PROCESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADEP_PROCESSO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE                 NOT NULL,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)    NOT NULL,
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  DT_PROCESSO              DATE                 NOT NULL,
  NM_USUARIO_PROCESSO      VARCHAR2(15 BYTE)    NOT NULL,
  DT_PREPARO               DATE,
  NM_USUARIO_PREPARO       VARCHAR2(15 BYTE),
  DT_PACIENTE              DATE,
  NM_USUARIO_PACIENTE      VARCHAR2(15 BYTE),
  DT_HORARIO_PROCESSO      DATE,
  DT_LEITURA               DATE,
  NM_USUARIO_LEITURA       VARCHAR2(15 BYTE),
  NR_PRESCRICAO            NUMBER(14),
  NR_SEQ_SOLUCAO           NUMBER(6),
  NR_ETAPA                 NUMBER(3),
  NR_SEQ_HORARIO           NUMBER(10),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  IE_STATUS_PROCESSO       VARCHAR2(15 BYTE),
  CD_LOCAL_ESTOQUE         NUMBER(4),
  DT_DISPENSACAO           DATE,
  NM_USUARIO_DISPENSACAO   VARCHAR2(15 BYTE),
  DT_RETIRADA              DATE,
  NM_USUARIO_RETIRADA      VARCHAR2(15 BYTE),
  CD_PESSOA_RETIRADA       VARCHAR2(10 BYTE),
  IE_TIPO_PROCESSO         VARCHAR2(15 BYTE),
  NR_SEQ_PROCESSO          NUMBER(10),
  IE_INCONSISTENCIA        VARCHAR2(15 BYTE),
  IE_SEPARADO              VARCHAR2(1 BYTE),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_HIGIENIZACAO          DATE,
  NM_USUARIO_HIGIENIZACAO  VARCHAR2(15 BYTE),
  IE_ORIGEM_PROCESSO       VARCHAR2(15 BYTE),
  CD_FUNCAO_ORIGEM         NUMBER(5),
  IE_GEDIPA                VARCHAR2(1 BYTE),
  IE_URGENTE               VARCHAR2(15 BYTE),
  IE_LOCAL_PREPARO         VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL          NUMBER(6),
  CD_INTERVALO             VARCHAR2(7 BYTE),
  NR_SEQ_PROCEDIMENTO      NUMBER(6),
  IE_SE_NECESSARIO         VARCHAR2(1 BYTE),
  IE_ACM                   VARCHAR2(1 BYTE),
  IE_DOSE_ESPECIAL         VARCHAR2(1 BYTE),
  IE_HIGIENIZACAO          VARCHAR2(1 BYTE),
  IE_PREPARO               VARCHAR2(1 BYTE),
  IE_CONFERENCIA           VARCHAR2(1 BYTE),
  IE_TRANSFERIDO           VARCHAR2(1 BYTE),
  DT_EXPEDICAO             DATE,
  NM_USUARIO_EXPEDICAO     VARCHAR2(15 BYTE),
  DT_ENTREGA               DATE,
  NM_USUARIO_ENTREGA       VARCHAR2(15 BYTE),
  DT_RECEBIMENTO           DATE,
  NM_USUARIO_RECEB         VARCHAR2(15 BYTE),
  NR_SEQ_REGRA             NUMBER(10),
  DS_STACK                 VARCHAR2(2000 BYTE),
  DT_PRIMEIRA_CHECAGEM     DATE,
  DT_SUSPENSAO             DATE,
  NM_USUARIO_SUSP          VARCHAR2(15 BYTE),
  NR_SEQ_DIETA             NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADEPROC_ADEPASIR_FK_I ON TASY.ADEP_PROCESSO
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_ADEPASIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_ATEPACI_FK_I ON TASY.ADEP_PROCESSO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_FUNCAO_FK_I ON TASY.ADEP_PROCESSO
(CD_FUNCAO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_INTPRES_FK_I ON TASY.ADEP_PROCESSO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_I1 ON TASY.ADEP_PROCESSO
(NR_PRESCRICAO, NR_SEQ_SOLUCAO, NR_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_I2 ON TASY.ADEP_PROCESSO
(DT_HORARIO_PROCESSO, CD_LOCAL_ESTOQUE, CD_SETOR_ATENDIMENTO, IE_STATUS_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_I3 ON TASY.ADEP_PROCESSO
(DT_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_I3
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_LOCESTO_FK_I ON TASY.ADEP_PROCESSO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_PESFISI_FK_I ON TASY.ADEP_PROCESSO
(CD_PESSOA_RETIRADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADEPROC_PK ON TASY.ADEP_PROCESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_PREMAHO_FK_I ON TASY.ADEP_PROCESSO
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_PREMAHO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_PREMATE_FK_I ON TASY.ADEP_PROCESSO
(NR_PRESCRICAO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_PREMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_PRESDIE_FK_I ON TASY.ADEP_PROCESSO
(NR_PRESCRICAO, NR_SEQ_DIETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_PRESOLU_FK_I ON TASY.ADEP_PROCESSO
(NR_PRESCRICAO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPROC_REGETAG_FK_I ON TASY.ADEP_PROCESSO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_REGETAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPROC_SETATEN_FK_I ON TASY.ADEP_PROCESSO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPROC_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.adep_processo_atual
before insert or update ON TASY.ADEP_PROCESSO for each row
declare
ie_status_processo_w	varchar2(15)	:= 'X';
ie_grava_log_gedipa_w	varchar2(1);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

if (inserting) then
	:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_w
	from	prescr_medica	a
	where	nr_prescricao	= :new.nr_prescricao;

	select	nvl(max(ie_grava_log_gedipa),'N')
	into	ie_grava_log_gedipa_w
	from	parametros_farmacia
	where	cd_estabelecimento = nvl(cd_estabelecimento_w,wheb_usuario_pck.get_cd_estabelecimento);

	if	(:new.nr_seq_processo is null) and
		(:new.dt_processo is not null) and
		(:old.dt_processo is null) then
		ie_status_processo_w	:= 'G';
	end if;

	if	(ie_status_processo_w <> 'X') then
		:new.ie_status_processo	:= ie_status_processo_w;

	end if;

	if	(ie_grava_log_gedipa_w = 'S') then
		if (:old.dt_horario_processo <> :new.dt_horario_processo) then
			insert into LOG_GEDIPA(
				NR_SEQUENCIA,
				DT_LOG,
				NR_LOG,
				NM_OBJETO_EXECUCAO,
				NM_OBJETO_CHAMADO,
				DS_PARAMETROS,
				DS_LOG,
				NR_SEQ_PROCESSO)
			values(
				LOG_GEDIPA_seq.nextval,
				sysdate,
				2200,
				'ADEP_PROCESSO_ATUAL',
				null,
				substr('DT_HORARIO_PROCESSO Anterior= ' || to_char(:old.dt_horario_processo, 'dd/mm/yyyy hh24:mi:ss') || chr(10) ||
				'DT_HORARIO_PROCESSO Novo= ' || to_char(:new.dt_horario_processo, 'dd/mm/yyyy hh24:mi:ss'),1,2000),
				substr(dbms_utility.format_call_stack,1,1500),
				:new.nr_sequencia);
		end if;

		if	(:old.dt_cancelamento is null) and
			(:new.dt_cancelamento is not null) then

			insert into LOG_GEDIPA(
				NR_SEQUENCIA,
				DT_LOG,
				NR_LOG,
				NM_OBJETO_EXECUCAO,
				NM_OBJETO_CHAMADO,
				DS_PARAMETROS,
				DS_LOG,
				NR_SEQ_PROCESSO)
			values(
				LOG_GEDIPA_seq.nextval,
				sysdate,
				2200,
				'ADEP_PROCESSO_ATUAL',
				null,
				null,
				substr(dbms_utility.format_call_stack,1,1500),
				:new.nr_sequencia);

		end if;

	end if;

	/*
	if	(updating) and
		(nvl(:new.cd_local_estoque,0) <> nvl(:old.cd_local_estoque,0)) then

		insert into LOG_GEDIPA(
			NR_SEQUENCIA,
			DT_LOG,
			NR_LOG,
			NM_OBJETO_EXECUCAO,
			NM_OBJETO_CHAMADO,
			DS_PARAMETROS,
			DS_LOG)
		values(
			LOG_GEDIPA_seq.nextval,
			sysdate,
			2000,
			'ADEP_PROCESSO_ATUAL',
			null,
			null,
			substr('Processo: ' || :new.nr_sequencia || ' / Local: ' || :old.cd_local_estoque ||' / ' || :new.cd_local_estoque || '  ' ||substr(dbms_utility.format_call_stack,1,1500),1,2000));

	end if;

	if	(updating) and
		(:old.dt_preparo is null) and
		(:new.dt_preparo is not null) then

		insert into LOG_GEDIPA(
			NR_SEQUENCIA,
			DT_LOG,
			NR_LOG,
			NM_OBJETO_EXECUCAO,
			NM_OBJETO_CHAMADO,
			DS_PARAMETROS,
			DS_LOG,
			NR_SEQ_PROCESSO)
		values(
			LOG_GEDIPA_seq.nextval,
			sysdate,
			2001,
			'ADEP_PROCESSO_ATUAL',
			null,
			null,
			substr(dbms_utility.format_call_stack,1,1500),
			:new.nr_sequencia);
	end if;
	*/

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.adep_processo_delete
before delete ON TASY.ADEP_PROCESSO for each row
declare
ds_log_w            log_tasy.ds_log%type;
pragma autonomous_transaction;

cursor c01 is
select  a.nr_sequencia,
        a.nr_seq_material,
        a.nr_seq_solucao,
        to_char(a.dt_horario, 'dd/mm/yyyy hh24:mi:ss') dt_horario,
        a.nr_prescricao,
        a.nr_seq_processo,
        a.ie_agrupador,
        to_char(a.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') dt_atualizacao
from    prescr_mat_hor a
where   a.nr_seq_processo = :old.nr_sequencia;

begin

ds_log_w := substr('(OLD\NEW)' || chr(10) ||
            'Processo: '        || :old.nr_sequencia    || ' \ ' || :new.nr_sequencia ||chr(10) ||
            'Prescr: '          || :old.nr_prescricao   || ' \ ' || :new.nr_prescricao || chr(10) ||
            'Seq Sol: '         || :old.nr_seq_solucao  || ' \ ' || :old.nr_seq_solucao || chr(10) ||
            'Seq Hor: '         || :old.nr_seq_horario  || ' \ ' || :old.nr_seq_horario || chr(10) ||
            'Seq Mat: '         || :old.nr_seq_material || ' \ ' || :old.nr_seq_material || chr(10) ||
            'Etapa: '           || :old.nr_etapa        || ' \ ' || :old.nr_etapa || chr(10) ||
            'Atendimento: '     || :old.nr_atendimento  || ' \ ' || :old.nr_atendimento || chr(10) ||
            'Dt Atualizacao: '  || to_char(:old.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss')  || ' \ ' || to_char(:old.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') || chr(10) ||
            'Dt Processo: '     || to_char(:old.dt_processo, 'dd/mm/yyyy hh24:mi:ss')     || ' \ ' || to_char(:old.dt_processo, 'dd/mm/yyyy hh24:mi:ss') || chr(10) ||
            'Stack: '           || substr(dbms_utility.format_call_stack,1,1500),1,2000);

gravar_log_tasy(896, substr(ds_log_w,1,2000), :old.nm_usuario);
commit;

for c01_w in c01
loop

ds_log_w := substr('Seq Hor: '         || c01_w.nr_sequencia || chr(10) ||
            'Seq Mat: '         || c01_w.nr_seq_material || chr(10) ||
            'Seq Sol: '         || c01_w.nr_seq_solucao || chr(10) ||
            'Dt_horario: '      || c01_w.dt_horario || chr(10) ||
            'Prescr: '          || c01_w.nr_prescricao || chr(10) ||
            'Processo: '        || c01_w.nr_seq_processo || chr(10) ||
            'Ie_agrupador: '    || c01_w.ie_agrupador || chr(10) ||
            'Dt Atualizacao: '  || c01_w.dt_atualizacao,1,2000);

gravar_log_tasy(897, substr(ds_log_w,1,2000), :old.nm_usuario);
commit;

end loop;

end;
/


ALTER TABLE TASY.ADEP_PROCESSO ADD (
  CONSTRAINT ADEPROC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADEP_PROCESSO ADD (
  CONSTRAINT ADEPROC_PRESDIE_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_DIETA) 
 REFERENCES TASY.PRESCR_DIETA (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT ADEPROC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ADEPROC_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT ADEPROC_PREMATE_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT ADEPROC_REGETAG_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_ETAPA_GEDIPA (NR_SEQUENCIA),
  CONSTRAINT ADEPROC_PRESOLU_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PRESCR_SOLUCAO (NR_PRESCRICAO,NR_SEQ_SOLUCAO),
  CONSTRAINT ADEPROC_PREMAHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.PRESCR_MAT_HOR (NR_SEQUENCIA),
  CONSTRAINT ADEPROC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ADEPROC_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT ADEPROC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RETIRADA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ADEPROC_ADEPASIR_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT ADEPROC_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO_ORIGEM) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO));

GRANT SELECT ON TASY.ADEP_PROCESSO TO NIVEL_1;

