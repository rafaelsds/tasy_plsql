ALTER TABLE TASY.EVOLUCAO_PACIENTE_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EVOLUCAO_PACIENTE_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EVOLUCAO_PACIENTE_AGENDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EVOLUCAO          NUMBER(10),
  NR_SEQ_AGENDA        NUMBER(10),
  CD_TIPO_AGENDA       NUMBER(10),
  NR_SEQ_TEMPLATE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EVOPAAG_EHRREGI_FK_I ON TASY.EVOLUCAO_PACIENTE_AGENDA
(NR_SEQ_TEMPLATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPAAG_EVOPACI_FK_I ON TASY.EVOLUCAO_PACIENTE_AGENDA
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EVOPAAG_PK ON TASY.EVOLUCAO_PACIENTE_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EVOLUCAO_PACIENTE_AGENDA ADD (
  CONSTRAINT EVOPAAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EVOLUCAO_PACIENTE_AGENDA ADD (
  CONSTRAINT EVOPAAG_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_TEMPLATE) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EVOPAAG_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.EVOLUCAO_PACIENTE_AGENDA TO NIVEL_1;

