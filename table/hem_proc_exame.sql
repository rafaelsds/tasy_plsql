ALTER TABLE TASY.HEM_PROC_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_PROC_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_PROC_EXAME
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_SEQ_EXAME    NUMBER(10),
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  NR_SEQ_PROC     NUMBER(10)                    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMPROEX_HEMEXAM_FK_I ON TASY.HEM_PROC_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMPROEX_HEMEXAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HEMPROEX_HEMPROC_FK_I ON TASY.HEM_PROC_EXAME
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMPROEX_HEMPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HEMPROEX_PK ON TASY.HEM_PROC_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMPROEX_PK
  MONITORING USAGE;


ALTER TABLE TASY.HEM_PROC_EXAME ADD (
  CONSTRAINT HEMPROEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_PROC_EXAME ADD (
  CONSTRAINT HEMPROEX_HEMEXAM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.HEM_EXAME (NR_SEQUENCIA),
  CONSTRAINT HEMPROEX_HEMPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.HEM_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HEM_PROC_EXAME TO NIVEL_1;

