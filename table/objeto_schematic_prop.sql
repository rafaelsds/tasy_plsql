ALTER TABLE TASY.OBJETO_SCHEMATIC_PROP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OBJETO_SCHEMATIC_PROP CASCADE CONSTRAINTS;

CREATE TABLE TASY.OBJETO_SCHEMATIC_PROP
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_OBJETO                NUMBER(10)       NOT NULL,
  IE_TIPO_FILTER               VARCHAR2(15 BYTE),
  DS_ACTION_NAME               VARCHAR2(255 BYTE),
  IE_PADRAO_APRES              VARCHAR2(15 BYTE),
  IE_ACAO_POS_CONSULTA         VARCHAR2(15 BYTE),
  QT_LARGURA                   NUMBER(5),
  QT_ALTURA                    NUMBER(5),
  IE_MULTISELECT               VARCHAR2(1 BYTE),
  IE_SOMENTE_DETALHE           VARCHAR2(1 BYTE),
  QT_REG_PAGINACAO             NUMBER(5),
  NR_SEQ_BTN_FOCO              NUMBER(10),
  NR_SEQ_LEGENDA               NUMBER(10),
  QT_MAX_REGISTRO              NUMBER(3),
  IE_CHAMAR_PAINEL_FILHO       VARCHAR2(1 BYTE),
  CD_EXP_LABEL_BREAD           NUMBER(10),
  NM_ATRIBUTO_BREADCRUMB       VARCHAR2(50 BYTE),
  IE_ATIVAR_DETALHE            VARCHAR2(1 BYTE),
  NR_PARAMETRO_TIMER           NUMBER(10),
  IE_READONLY                  VARCHAR2(1 BYTE),
  NM_ATRIB_LOCATE              VARCHAR2(30 BYTE),
  IE_ABRIR_DIALOG              VARCHAR2(1 BYTE),
  NR_SEQ_DIC_OBJETO            NUMBER(10),
  CD_INTERFACE                 NUMBER(5),
  IE_ATIVAR_VAZIO              VARCHAR2(1 BYTE),
  DS_ACTION_GRID_EDITAVEL      VARCHAR2(2000 BYTE),
  IE_GRID_EDIT                 VARCHAR2(1 BYTE),
  IE_HABILITA_MODAL            VARCHAR2(1 BYTE),
  IE_POSICAO_TELA              VARCHAR2(5 BYTE),
  IE_PRIMEIRO_REGISTRO         VARCHAR2(1 BYTE),
  IE_TIPO_WDLG                 VARCHAR2(15 BYTE),
  IE_GRID_SALVAR               VARCHAR2(1 BYTE),
  IE_SALVAR_ORDEM_GRID         VARCHAR2(1 BYTE),
  NR_SEQ_OBJETO_VALIDACAO      NUMBER(10),
  IE_LIBERAR_AO_SALVAR         VARCHAR2(1 BYTE),
  NR_SEQ_OBJ_RECARREGAR        NUMBER(10),
  IE_LAZY_LOAD_DETAIL          VARCHAR2(1 BYTE),
  IE_DESTROY_DETAIL            VARCHAR2(1 BYTE),
  IE_RESPONSIVO                VARCHAR2(1 BYTE),
  IE_CONF_RELATORIO            VARCHAR2(1 BYTE),
  IE_PAGINACAO                 VARCHAR2(1 BYTE),
  DS_ENDPOINT_API              VARCHAR2(255 BYTE),
  IE_TIPO_COMP_API_TWS         VARCHAR2(5 BYTE),
  IE_ADD_CARD                  VARCHAR2(1 BYTE),
  IE_ALINHAMENTO               VARCHAR2(5 BYTE),
  IE_FULL_SCREEN               VARCHAR2(1 BYTE),
  IE_NAVEGACAO                 VARCHAR2(1 BYTE),
  IE_UNIFICAR_COUNT_REGISTROS  VARCHAR2(1 BYTE),
  IE_NOVO_GRID                 VARCHAR2(1 BYTE),
  IE_TAMANHO_CAMPO             VARCHAR2(5 BYTE),
  IE_NOVA_LINHA                VARCHAR2(1 BYTE),
  DS_URL_IMG                   VARCHAR2(255 BYTE),
  DS_TITLE                     VARCHAR2(255 BYTE),
  DS_SUBTITLE                  VARCHAR2(255 BYTE),
  IE_CACHE_DETAIL_CHANGE_VIEW  VARCHAR2(2 BYTE),
  IE_TIPO_TAMANHO              VARCHAR2(15 BYTE),
  IE_TIPO_SELECAO              VARCHAR2(5 BYTE),
  IE_STATUS                    VARCHAR2(255 BYTE),
  DT_REGISTRO                  DATE,
  IE_ALINHAMENTO_VERTICAL      VARCHAR2(5 BYTE),
  IE_DIRECAO_LAYOUT            VARCHAR2(5 BYTE),
  IE_CONSISTIR_RELATORIO       VARCHAR2(1 BYTE),
  NM_ATRIB_RESULT              VARCHAR2(30 BYTE),
  IE_TIPO_PAGINACAO            VARCHAR2(2 BYTE),
  IE_SHOW_ROW_DENSITY          VARCHAR2(1 BYTE),
  IE_USAR_PATIENTBAR_POS       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OBJSCHPRO_DICEXPR_FK_I ON TASY.OBJETO_SCHEMATIC_PROP
(CD_EXP_LABEL_BREAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHPRO_DICOBJE_FK2_I ON TASY.OBJETO_SCHEMATIC_PROP
(NR_SEQ_DIC_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHPRO_OBJSCHE_FK_I ON TASY.OBJETO_SCHEMATIC_PROP
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHPRO_OBJSCHPRO_FK2_I ON TASY.OBJETO_SCHEMATIC_PROP
(NR_SEQ_OBJETO_VALIDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OBJSCHPRO_PK ON TASY.OBJETO_SCHEMATIC_PROP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHPRO_TASLEGE_FK_I ON TASY.OBJETO_SCHEMATIC_PROP
(NR_SEQ_LEGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OBJETO_SCHEMATIC_PROP_tp  after update ON TASY.OBJETO_SCHEMATIC_PROP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'OBJETO_SCHEMATIC_PROP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ACTION_NAME,1,4000),substr(:new.DS_ACTION_NAME,1,4000),:new.nm_usuario,nr_seq_w,'DS_ACTION_NAME',ie_log_w,ds_w,'OBJETO_SCHEMATIC_PROP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'OBJETO_SCHEMATIC_PROP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OBJETO_SCHEMATIC_PROP ADD (
  CONSTRAINT OBJSCHPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OBJETO_SCHEMATIC_PROP ADD (
  CONSTRAINT OBJSCHPRO_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OBJSCHPRO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_LABEL_BREAD) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT OBJSCHPRO_TASLEGE_FK 
 FOREIGN KEY (NR_SEQ_LEGENDA) 
 REFERENCES TASY.TASY_LEGENDA (NR_SEQUENCIA),
  CONSTRAINT OBJSCHPRO_DICOBJE_FK2 
 FOREIGN KEY (NR_SEQ_DIC_OBJETO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OBJSCHPRO_OBJSCHPRO_FK2 
 FOREIGN KEY (NR_SEQ_OBJETO_VALIDACAO) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.OBJETO_SCHEMATIC_PROP TO NIVEL_1;

