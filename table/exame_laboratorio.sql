ALTER TABLE TASY.EXAME_LABORATORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LABORATORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LABORATORIO
(
  NR_SEQ_EXAME                  NUMBER(10)      NOT NULL,
  NM_EXAME                      VARCHAR2(80 BYTE) NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  CD_PROCEDIMENTO               NUMBER(15),
  IE_ORIGEM_PROCED              NUMBER(10),
  NR_SEQ_SUPERIOR               NUMBER(10),
  CD_EXAME                      VARCHAR2(20 BYTE),
  IE_FORMATO_RESULTADO          VARCHAR2(3 BYTE),
  NR_SEQ_GRUPO                  NUMBER(10),
  IE_SOLICITACAO                VARCHAR2(1 BYTE),
  DS_ORIENTACAO_USUARIO         VARCHAR2(4000 BYTE),
  DS_UNIDADE_MEDIDA             VARCHAR2(15 BYTE),
  DS_FORMULA                    VARCHAR2(255 BYTE),
  QT_DECIMAIS                   NUMBER(1),
  IE_CAMPO_CALCULO              VARCHAR2(1 BYTE) NOT NULL,
  CD_EXAME_INTEGRACAO           VARCHAR2(20 BYTE),
  NR_SEQ_APRESENT               NUMBER(15),
  DS_REGRA                      VARCHAR2(255 BYTE),
  IE_RESTRICAO                  VARCHAR2(1 BYTE) NOT NULL,
  DS_EXAME_PRESCRICAO           VARCHAR2(255 BYTE),
  NR_SEQ_UNID_MED               NUMBER(15),
  NR_SEQ_GRUPO_IMP              NUMBER(10),
  DS_ORIENTACAO_TECNICA         VARCHAR2(4000 BYTE),
  IE_APACHE_II                  VARCHAR2(3 BYTE),
  IE_PRESCRICAO_ROTINA          VARCHAR2(1 BYTE) NOT NULL,
  CD_CGC_EXTERNO                VARCHAR2(14 BYTE),
  DS_ORIENTACAO_MEDICAMENTO     VARCHAR2(4000 BYTE),
  IE_GRID_PEP                   VARCHAR2(1 BYTE),
  NM_EXAME_LOC                  VARCHAR2(80 BYTE),
  IE_ANATOMIA_PATOLOGICA        VARCHAR2(1 BYTE),
  DS_TEXTO_PADRAO               VARCHAR2(255 BYTE),
  NR_ORDEM_AMOSTRA              NUMBER(10),
  IE_AGORA                      VARCHAR2(1 BYTE),
  IE_SEXO                       VARCHAR2(1 BYTE),
  IE_PARADA_FORMULA             VARCHAR2(1 BYTE),
  IE_CONSISTE_CAMPO_FORMULA     VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_LOTE             NUMBER(10),
  CD_PROCEDIMENTO_TUSS          NUMBER(15),
  IE_ORIGEM_PROC_TUSS           NUMBER(10),
  IE_ORIENTACAO                 VARCHAR2(1 BYTE),
  NR_SEQ_EXAME_PEP              NUMBER(10),
  IE_INFORMAR_ULT_DOSE          VARCHAR2(1 BYTE),
  IE_EXAME_EXTERNO              VARCHAR2(1 BYTE),
  IE_GERAR_LINHA_RESULT_INTEGR  VARCHAR2(1 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DS_COR_FUNDO                  VARCHAR2(15 BYTE),
  IE_NEGRITO                    VARCHAR2(1 BYTE),
  IE_ITALICO                    VARCHAR2(1 BYTE),
  IE_NECESSITA_MEDIC_USO        VARCHAR2(1 BYTE),
  IE_TOPOGRAFIA                 VARCHAR2(1 BYTE),
  IE_IMPRIME_MAPA               VARCHAR2(1 BYTE),
  IE_MOSTRA_DADOS_ADIC_PRESCR   VARCHAR2(1 BYTE),
  IE_QUESTIONAR_AGORA           VARCHAR2(1 BYTE),
  IE_OBS_EXAME_PRESCR_IMP       VARCHAR2(1 BYTE),
  IE_CONTROLA_PENDENTE          VARCHAR2(1 BYTE),
  IE_APRESENTA_EXTERNO          VARCHAR2(1 BYTE),
  DS_COR                        VARCHAR2(15 BYTE),
  IE_SELECIONA_FRASE            VARCHAR2(1 BYTE),
  IE_EXIGE_MENSTRUACAO          VARCHAR2(1 BYTE),
  IE_MENSAGEM_CONSISTE          VARCHAR2(1 BYTE),
  IE_CONGELACAO                 VARCHAR2(5 BYTE),
  IE_QUESTIONAR_EXAME           VARCHAR2(1 BYTE),
  QT_TEMPO_PADRAO_JEJUM         NUMBER(10,3),
  IE_ORIENTACAO_EUP             VARCHAR2(1 BYTE),
  IE_EXIGE_JUSTIFICATIVA        VARCHAR2(1 BYTE),
  IE_QUESTIONAR_INTERV          VARCHAR2(1 BYTE),
  IE_CONFIRMA_USUARIO           VARCHAR2(1 BYTE),
  NR_SEQ_PROC_INTERNO           NUMBER(10),
  IE_EXIGE_TEMPO_JEJUM_REAL     VARCHAR2(1 BYTE),
  IE_OBRIGA_TIPO                VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_INT              NUMBER(10),
  IE_GERA_ISOLAMENTO            VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIFICACAO          NUMBER(10),
  CD_ESTABELECIMENTO            NUMBER(4),
  DS_TEMPO_AMOSTRA              VARCHAR2(20 BYTE),
  IE_EDIT_LOTE_ENTRADA          VARCHAR2(1 BYTE),
  IE_PRIMARIO_LOTE_ENT          VARCHAR2(1 BYTE),
  IE_VANCOCINEMIA               VARCHAR2(1 BYTE),
  QT_TEMPO_PROC                 NUMBER(10),
  IE_EXIBE_LOC_PRESCR           VARCHAR2(1 BYTE),
  IE_SUMARIO_ALTA               VARCHAR2(1 BYTE),
  IE_EXAME_INVASIVO             VARCHAR2(1 BYTE),
  IE_AVISO_SONORO               VARCHAR2(1 BYTE),
  IE_CONSISTE_RES_DESC          VARCHAR2(1 BYTE),
  IE_DUPLA_CHECAGEM_RES         VARCHAR2(1 BYTE),
  IE_EXIBE_GQA                  VARCHAR2(1 BYTE),
  IE_ISOLAR_EXAME_IMPRESSAO     VARCHAR2(1 BYTE),
  IE_PERMITIR_DESCARTE          VARCHAR2(1 BYTE),
  NR_SEQ_LOINC                  NUMBER(10),
  IE_IMUNOHISTOQUIMICA          VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_RES              NUMBER(10),
  IE_GRUPO_CRONICAS             VARCHAR2(5 BYTE),
  IE_TIPO_EXAME                 VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXALABO_ESTABEL_FK_I ON TASY.EXAME_LABORATORIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_EXALABO_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALABO_EXALABO_FK2_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_EXAME_PEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_EXALABO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_GRPRIN_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_GRUPO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_GRPRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_GRUEXLA_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALABO_I1 ON TASY.EXAME_LABORATORIO
(NM_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALABO_LABCLEX_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_LABCLEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_LABGRIM_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_GRUPO_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_LABGRIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_LABGRIM_FK2_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_GRUPO_LOTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_LABGRIM_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_LABUNME_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_UNID_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALABO_LABUNME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALABO_LGREXARES_FK_I ON TASY.EXAME_LABORATORIO
(NR_SEQ_GRUPO_RES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALABO_PESJURI_FK_I ON TASY.EXAME_LABORATORIO
(CD_CGC_EXTERNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EXALABO_PK ON TASY.EXAME_LABORATORIO
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALABO_PROCEDI_FK_I ON TASY.EXAME_LABORATORIO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EXALABO_UK ON TASY.EXAME_LABORATORIO
(CD_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EXAME_LABORATORIO_tp  after update ON TASY.EXAME_LABORATORIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_EXAME);  ds_c_w:=null; ds_w:=substr(:new.CD_EXAME,1,500);gravar_log_alteracao(substr(:old.CD_EXAME,1,4000),substr(:new.CD_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXAME',ie_log_w,ds_w,'EXAME_LABORATORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_FORMULA,1,500);gravar_log_alteracao(substr(:old.DS_FORMULA,1,4000),substr(:new.DS_FORMULA,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORMULA',ie_log_w,ds_w,'EXAME_LABORATORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ORIENTACAO_TECNICA,1,500);gravar_log_alteracao(substr(:old.DS_ORIENTACAO_TECNICA,1,4000),substr(:new.DS_ORIENTACAO_TECNICA,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_TECNICA',ie_log_w,ds_w,'EXAME_LABORATORIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'EXAME_LABORATORIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXAME_INTEGRACAO,1,4000),substr(:new.CD_EXAME_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXAME_INTEGRACAO',ie_log_w,ds_w,'EXAME_LABORATORIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Exame_Laboratorio_delete
BEFORE DELETE ON TASY.EXAME_LABORATORIO 
FOR EACH ROW
declare
reg_integracao_w		gerar_int_padrao.reg_integracao;
ds_param_adicional_w		varchar2(100);
BEGIN

if (:old.nr_seq_superior is null) then
	reg_integracao_w.cd_estab_documento :=obter_estabelecimento_ativo();
	ds_param_adicional_w := 'operacao_p=DELETE;';
	gerar_int_padrao.gravar_integracao('309', :old.nr_seq_exame, :old.nm_usuario, reg_integracao_w,ds_param_adicional_w);
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.Exame_Laboratorio_Insert
BEFORE INSERT ON TASY.EXAME_LABORATORIO 
FOR EACH ROW
declare
reg_integracao_w		gerar_int_padrao.reg_integracao;
ds_param_adicional_w		varchar2(100);
BEGIN

if	(length(:new.nr_seq_exame) > 8) then
	wheb_mensagem_pck.exibir_mensagem_abort(184939);
end if;

if	(:new.nm_exame is not null) then
	:new.nm_exame_loc	:= upper(elimina_acentuacao(:new.nm_exame));
end if;

if (:new.nr_seq_superior is null) then
  reg_integracao_w.cd_estab_documento :=obter_estabelecimento_ativo();
	ds_param_adicional_w := 'operacao_p=INSERT;';
	gerar_int_padrao.gravar_integracao('309', :new.nr_seq_exame, :new.nm_usuario, reg_integracao_w,ds_param_adicional_w);
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.Exame_Laboratorio_Update
BEFORE UPDATE ON TASY.EXAME_LABORATORIO FOR EACH ROW
declare
reg_integracao_w		gerar_int_padrao.reg_integracao;
ds_param_adicional_w		varchar2(100);
nr_seq_evento_w		number(10);

Cursor C01 is
select	a.nr_seq_evento
from	regra_envio_sms a
where	a.ie_evento_disp	= 'ASE'
and	nvl(a.ie_situacao,'A') = 'A';

begin

if	(:new.nm_exame_loc	is null) or
	(:new.nm_exame <> :old.nm_exame )	then
	:new.nm_exame_loc	:= upper(elimina_acentuacao(:new.nm_exame));

end if;

if	(:new.ie_solicitacao <> :old.ie_solicitacao) then

	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_exame_lab_trigger(nr_seq_evento_w,:new.nm_usuario	,:new.nr_seq_exame,:new.nm_exame);
		end;
	end loop;
	close C01;

end if;

if 	(nvl(:new.nr_seq_superior,0) <> nvl(:old.nr_seq_superior,0)) and
	(:new.nr_seq_superior is not null) and
	(:new.nr_seq_superior = :new.nr_seq_exame) then
	wheb_mensagem_pck.exibir_mensagem_abort(224250,'');
end if;

if (:new.nr_seq_superior is null) and (:new.ie_situacao <>'I') then

  reg_integracao_w.cd_estab_documento :=obter_estabelecimento_ativo();
	ds_param_adicional_w := 'operacao_p=UPDATE;';
	gerar_int_padrao.gravar_integracao('309', :new.nr_seq_exame, :new.nm_usuario, reg_integracao_w,ds_param_adicional_w);
elsif (:new.nr_seq_superior is null) and (:new.ie_situacao ='I') then
  reg_integracao_w.cd_estab_documento :=obter_estabelecimento_ativo();
	ds_param_adicional_w := 'operacao_p=DELETE;';
	gerar_int_padrao.gravar_integracao('309', :new.nr_seq_exame, :new.nm_usuario, reg_integracao_w,ds_param_adicional_w);
end if;

END;
/


ALTER TABLE TASY.EXAME_LABORATORIO ADD (
  CONSTRAINT EXALABO_PK
 PRIMARY KEY
 (NR_SEQ_EXAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT EXALABO_UK
 UNIQUE (CD_EXAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXAME_LABORATORIO ADD (
  CONSTRAINT EXALABO_LGREXARES_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_RES) 
 REFERENCES TASY.LAB_GRUPO_EXAME_RESULTADO (NR_SEQUENCIA),
  CONSTRAINT EXALABO_LABGRIM_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_LOTE) 
 REFERENCES TASY.LAB_GRUPO_IMPRESSAO (NR_SEQUENCIA),
  CONSTRAINT EXALABO_EXALABO_FK2 
 FOREIGN KEY (NR_SEQ_EXAME_PEP) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT EXALABO_GRPRIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INT) 
 REFERENCES TASY.GRUPO_PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT EXALABO_LABCLEX_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.LAB_CLASSIFICACAO_EXAME (NR_SEQUENCIA),
  CONSTRAINT EXALABO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EXALABO_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME)
    ON DELETE CASCADE,
  CONSTRAINT EXALABO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT EXALABO_GRUEXLA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXALABO_LABGRIM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_IMP) 
 REFERENCES TASY.LAB_GRUPO_IMPRESSAO (NR_SEQUENCIA),
  CONSTRAINT EXALABO_PESJURI_FK 
 FOREIGN KEY (CD_CGC_EXTERNO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT EXALABO_LABUNME_FK 
 FOREIGN KEY (NR_SEQ_UNID_MED) 
 REFERENCES TASY.LAB_UNIDADE_MEDIDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.EXAME_LABORATORIO TO NIVEL_1;

