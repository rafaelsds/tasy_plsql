DROP TABLE TASY.W_ADEP_DUPLICIDADE_T CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_ADEP_DUPLICIDADE_T
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_ITEM              VARCHAR2(255 BYTE),
  DS_ITEM              VARCHAR2(240 BYTE),
  IE_TIPO_ITEM         VARCHAR2(15 BYTE),
  NR_PRESCRICAO        NUMBER(14),
  NR_SEQ_ITEM          NUMBER(20),
  NR_SEQ_HORARIO       NUMBER(10),
  NR_SEQ_APRESENT      NUMBER(10),
  DT_HORARIO           DATE,
  DT_LIBERACAO         DATE
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_ADEP_DUPLICIDADE_T TO NIVEL_1;

