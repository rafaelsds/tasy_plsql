ALTER TABLE TASY.FORMULARIO_AUDICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMULARIO_AUDICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMULARIO_AUDICAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROVEDOR_SERVICO    NUMBER(10),
  NR_NOTA_FISCAL         VARCHAR2(100 BYTE),
  NR_RECEITA             VARCHAR2(50 BYTE),
  IE_POSSUI_DISPOSITIVO  VARCHAR2(1 BYTE),
  DS_DISPOSITIVO         VARCHAR2(100 BYTE),
  IE_CANAL_AURIC_DIR     VARCHAR2(1 BYTE),
  IE_TIMPANO_DIR         VARCHAR2(1 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_CANAL_AURIC_ESQ     VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIMPANO_ESQ         VARCHAR2(1 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_CID_DOENCA          VARCHAR2(20 BYTE),
  IE_NECESSITA_APARELHO  VARCHAR2(1 BYTE),
  VL_KHZ_APARELHO_DIR    NUMBER(6,2),
  VL_DB_APARELHO_DIR     NUMBER(6,2),
  VL_KHZ_APARELHO_ESQ    NUMBER(6,2),
  VL_DB_APARELHO_ESQ     NUMBER(6,2),
  IE_RESULT_PERDA_AUD    VARCHAR2(1 BYTE),
  PR_MONOSSILABO         NUMBER(15,2),
  PR_MULTISSILABICO      NUMBER(15,2),
  PR_SENTENCA            NUMBER(15,2),
  DS_APARELHO            VARCHAR2(50 BYTE),
  NR_POSICAO_APAR        NUMBER(10),
  DS_RAZAO               VARCHAR2(100 BYTE),
  VL_TOTAL               NUMBER(15,2),
  DT_EMISSAO             DATE,
  IE_CUSTO_ESTIMADO      VARCHAR2(1 BYTE),
  VL_CUSTO_ESTIMADO      NUMBER(15,2),
  DT_CUSTO_ESTIMADO      DATE,
  IE_CERT_MEDICO         VARCHAR2(1 BYTE),
  DT_CERT_MEDICO         DATE,
  IE_ACIDENTE            VARCHAR2(1 BYTE),
  IE_DESCUIDO            VARCHAR2(1 BYTE),
  DS_DIAGNOSTICO         VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_RQE                 VARCHAR2(20 BYTE),
  NR_BSNR                VARCHAR2(25 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FORMAUD_ATEPACI_FK_I ON TASY.FORMULARIO_AUDICAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORMAUD_PESFISI_FK_I ON TASY.FORMULARIO_AUDICAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FORMAUD_PK ON TASY.FORMULARIO_AUDICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORMAUD_PROFISS_FK_I ON TASY.FORMULARIO_AUDICAO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FORMULARIO_AUDICAO_UPDATE
before update ON TASY.FORMULARIO_AUDICAO for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
			into is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV15'
    and   	obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV15'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
	end if;
  end if;
end;
/


ALTER TABLE TASY.FORMULARIO_AUDICAO ADD (
  CONSTRAINT FORMAUD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMULARIO_AUDICAO ADD (
  CONSTRAINT FORMAUD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FORMAUD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FORMAUD_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FORMULARIO_AUDICAO TO NIVEL_1;

