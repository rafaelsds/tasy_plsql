ALTER TABLE TASY.CORPO_HUMANO_ATEND_REPRES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CORPO_HUMANO_ATEND_REPRES CASCADE CONSTRAINTS;

CREATE TABLE TASY.CORPO_HUMANO_ATEND_REPRES
(
  NR_SEQ_REP_CORPORAL    NUMBER(10)             NOT NULL,
  NR_SEQ_ATEND_REPRES    NUMBER(10)             NOT NULL,
  NR_SEQ_ATEND           NUMBER(10)             NOT NULL,
  NR_SEQ_REPRES_IDADE    NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_SEXO                VARCHAR2(15 BYTE),
  IE_TIPO                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COHAREP_COHUATE_FK_I ON TASY.CORPO_HUMANO_ATEND_REPRES
(NR_SEQ_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHAREP_COHURCOR_FK_I ON TASY.CORPO_HUMANO_ATEND_REPRES
(NR_SEQ_REP_CORPORAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHAREP_COHURI_FK_I ON TASY.CORPO_HUMANO_ATEND_REPRES
(NR_SEQ_REPRES_IDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COHAREP_PK ON TASY.CORPO_HUMANO_ATEND_REPRES
(NR_SEQ_ATEND_REPRES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CORPO_HUMANO_ATEND_REPRES ADD (
  CONSTRAINT COHAREP_PK
 PRIMARY KEY
 (NR_SEQ_ATEND_REPRES));

ALTER TABLE TASY.CORPO_HUMANO_ATEND_REPRES ADD (
  CONSTRAINT COHAREP_COHUATE_FK 
 FOREIGN KEY (NR_SEQ_ATEND) 
 REFERENCES TASY.CORPO_HUMANO_ATEND (NR_SEQ_ATEND),
  CONSTRAINT COHAREP_COHURCOR_FK 
 FOREIGN KEY (NR_SEQ_REP_CORPORAL) 
 REFERENCES TASY.CORPO_HUMANO_REP_CORPORAL (NR_SEQ_REP_CORPORAL),
  CONSTRAINT COHAREP_COHURI_FK 
 FOREIGN KEY (NR_SEQ_REPRES_IDADE) 
 REFERENCES TASY.CORPO_HUMANO_REPRES_IDADE (NR_SEQ_REPRES_IDADE));

GRANT SELECT ON TASY.CORPO_HUMANO_ATEND_REPRES TO NIVEL_1;

