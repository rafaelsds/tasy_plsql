ALTER TABLE TASY.PLS_WEB_PARAM_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_WEB_PARAM_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_WEB_PARAM_USUARIO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_SEQ_PARAM               NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_SEGURADO            NUMBER(10),
  NR_SEQ_USU_ESTIPULANTE     NUMBER(10),
  NR_SEQ_USU_PRESTADOR       NUMBER(10),
  VL_PARAMETRO               VARCHAR2(255 BYTE) NOT NULL,
  NM_USUARIO_PARAM           VARCHAR2(15 BYTE),
  NR_SEQ_FUNCAO_PARAM        NUMBER(10)         NOT NULL,
  CD_FUNCAO                  NUMBER(5)          NOT NULL,
  IE_TIPO_ACESSO             VARCHAR2(2 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_PERFIL_WEB          NUMBER(10),
  NR_SEQ_USU_GRUPO_CONTRATO  NUMBER(10),
  NR_SEQ_USU_COOPERADO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSWEPU_ESTABEL_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_FUNPARA_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(CD_FUNCAO, NR_SEQ_FUNCAO_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSWEPU_PK ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSWEPU_PLESWEB_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_USU_ESTIPULANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLESWEB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSCOOW_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_USU_COOPERADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLSCOOW_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSGCW_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_USU_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLSGCW_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSPERW_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_PERFIL_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLSPERW_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSSEGU_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_SEGURADO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSUSWE_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_USU_PRESTADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSWEPU_PLSUSWE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSWEPU_PLSWEPO_FK_I ON TASY.PLS_WEB_PARAM_USUARIO
(NR_SEQ_PARAM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_WEB_PARAM_USUARIO_tp  after update ON TASY.PLS_WEB_PARAM_USUARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_USUARIO_PARAM,1,4000),substr(:new.NM_USUARIO_PARAM,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_PARAM',ie_log_w,ds_w,'PLS_WEB_PARAM_USUARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_WEB_PARAM_USUARIO ADD (
  CONSTRAINT PLSWEPU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_WEB_PARAM_USUARIO ADD (
  CONSTRAINT PLSWEPU_PLSCOOW_FK 
 FOREIGN KEY (NR_SEQ_USU_COOPERADO) 
 REFERENCES TASY.PLS_COOPERADO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_USU_PRESTADOR) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_PLESWEB_FK 
 FOREIGN KEY (NR_SEQ_USU_ESTIPULANTE) 
 REFERENCES TASY.PLS_ESTIPULANTE_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSWEPU_PLSWEPO_FK 
 FOREIGN KEY (NR_SEQ_PARAM) 
 REFERENCES TASY.PLS_WEB_PARAM_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_FUNPARA_FK 
 FOREIGN KEY (CD_FUNCAO, NR_SEQ_FUNCAO_PARAM) 
 REFERENCES TASY.FUNCAO_PARAMETRO (CD_FUNCAO,NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_PLSPERW_FK 
 FOREIGN KEY (NR_SEQ_PERFIL_WEB) 
 REFERENCES TASY.PLS_PERFIL_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSWEPU_PLSGCW_FK 
 FOREIGN KEY (NR_SEQ_USU_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO_WEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_WEB_PARAM_USUARIO TO NIVEL_1;

