ALTER TABLE TASY.DEPARTAMENTO_PHILIPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DEPARTAMENTO_PHILIPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.DEPARTAMENTO_PHILIPS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_DEPARTAMENTO       VARCHAR2(255 BYTE),
  DS_DEPARTAMENTO       VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  CD_CNPJ               VARCHAR2(14 BYTE),
  NR_GRUPO_DEP_PHILIPS  NUMBER(10),
  CD_CLASSIFICACAO      VARCHAR2(20 BYTE),
  NR_SEQ_SUPERIOR       NUMBER(10),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_APOIO              VARCHAR2(1 BYTE),
  NR_SEQ_CENTRO_CUSTO   NUMBER(10),
  CD_APROV_SUBSTITUTO   VARCHAR2(10 BYTE),
  IE_APROV_EXCECAO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DEPPHIL_CENCUST_FK_I ON TASY.DEPARTAMENTO_PHILIPS
(NR_SEQ_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPPHIL_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DEPPHIL_DEPPHIL_FK_I ON TASY.DEPARTAMENTO_PHILIPS
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPPHIL_DEPPHIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DEPPHIL_GDPHILIPS_FK_I ON TASY.DEPARTAMENTO_PHILIPS
(NR_GRUPO_DEP_PHILIPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPPHIL_GDPHILIPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DEPPHIL_PESFISI_FK_I ON TASY.DEPARTAMENTO_PHILIPS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEPPHIL_PESFISI_FKI_I ON TASY.DEPARTAMENTO_PHILIPS
(CD_APROV_SUBSTITUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEPPHIL_PESJURI_FK_I ON TASY.DEPARTAMENTO_PHILIPS
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPPHIL_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DEPPHIL_PK ON TASY.DEPARTAMENTO_PHILIPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DEPARTAMENTO_PHILIPS ADD (
  CONSTRAINT DEPPHIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DEPARTAMENTO_PHILIPS ADD (
  CONSTRAINT DEPPHIL_PESFISI_FKI 
 FOREIGN KEY (CD_APROV_SUBSTITUTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DEPPHIL_CENCUST_FK 
 FOREIGN KEY (NR_SEQ_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT DEPPHIL_DEPPHIL_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.DEPARTAMENTO_PHILIPS (NR_SEQUENCIA),
  CONSTRAINT DEPPHIL_GDPHILIPS_FK 
 FOREIGN KEY (NR_GRUPO_DEP_PHILIPS) 
 REFERENCES TASY.GRUPO_DEP_PHILIPS (NR_SEQUENCIA),
  CONSTRAINT DEPPHIL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DEPPHIL_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.DEPARTAMENTO_PHILIPS TO NIVEL_1;

