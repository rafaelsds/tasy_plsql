ALTER TABLE TASY.REG_FEATURES_CUSTOMER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_FEATURES_CUSTOMER CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_FEATURES_CUSTOMER
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_AREA_CUSTOMER    NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO            NUMBER(10),
  DS_FEATURE              VARCHAR2(255 BYTE)    NOT NULL,
  DT_APROVACAO            DATE,
  NR_SEQ_APRESENTACAO     NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_ESTAGIO          NUMBER(10),
  DS_OVERALL_DESC_REL     CLOB,
  CD_VERSAO               VARCHAR2(255 BYTE),
  DS_OVERALL_DESCRIPTION  VARCHAR2(4000 BYTE),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO    VARCHAR2(255 BYTE),
  NM_USUARIO_APROVACAO    VARCHAR2(15 BYTE),
  DT_LIBERACAO            DATE,
  NM_USUARIO_LIBERACAO    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_OVERALL_DESC_REL) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGFEC_PK ON TASY.REG_FEATURES_CUSTOMER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGFEC_REGARC_FK_I ON TASY.REG_FEATURES_CUSTOMER
(NR_SEQ_AREA_CUSTOMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGFEC_REGEST_FK_I ON TASY.REG_FEATURES_CUSTOMER
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGFEC_REGRUC_FK_I ON TASY.REG_FEATURES_CUSTOMER
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REG_FEATURES_CUSTOMER_INSERT
BEFORE INSERT ON TASY.REG_FEATURES_CUSTOMER FOR EACH ROW
DECLARE
	dt_aprovacao_w	reg_area_customer.dt_aprovacao%TYPE;
BEGIN
	SELECT	dt_aprovacao
	INTO	dt_aprovacao_w
	FROM	reg_area_customer
	WHERE	nr_sequencia = :NEW.nr_seq_area_customer;

	IF	dt_aprovacao_w IS NULL THEN
		wheb_mensagem_pck.exibir_mensagem_abort(901110);--N�o � poss�vel adicionar uma feature em uma �rea n�o aprovada
	END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.reg_features_customer_after
after insert or update ON TASY.REG_FEATURES_CUSTOMER for each row
declare

nr_seq_hist_w			reg_features_customer_hist.nr_sequencia%type;
ie_tipo_alteracao_w		reg_features_customer_hist.ie_tipo_alteracao%type;

pragma autonomous_transaction;

begin

select	reg_features_customer_hist_seq.nextval
into	nr_seq_hist_w
from	dual;

if (inserting) then

	insert into reg_features_customer_hist (
		nr_sequencia,
		nr_seq_area_customer,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_grupo,
		ds_feature,
		dt_aprovacao,
		nr_seq_apresentacao,
		nr_seq_estagio,
		ie_situacao,
		cd_versao,
		nr_seq_reg_version_rev,
		ds_overall_desc_rel,
		ds_overall_description,
		nr_seq_feature_cust,
		ie_tipo_alteracao)
	values (
		nr_seq_hist_w,
		:new.nr_seq_area_customer,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.nr_seq_grupo,
		:new.ds_feature,
		:new.dt_aprovacao,
		:new.nr_seq_apresentacao,
		:new.nr_seq_estagio,
		:new.ie_situacao,
		:new.cd_versao,
		null,
		:new.ds_overall_desc_rel,
		' ',
		:new.nr_sequencia,
		'I');


elsif (updating) then

	if (:new.ie_situacao = 'I') then
		ie_tipo_alteracao_w := 'E';
	else
		ie_tipo_alteracao_w := 'A';
	end if;

	insert into reg_features_customer_hist (
		nr_sequencia,
		nr_seq_area_customer,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_grupo,
		ds_feature,
		dt_aprovacao,
		nr_seq_apresentacao,
		nr_seq_estagio,
		ie_situacao,
		cd_versao,
		nr_seq_reg_version_rev,
		ds_overall_desc_rel,
		ds_overall_description,
		nr_seq_feature_cust,
		ie_tipo_alteracao)
	values (
		nr_seq_hist_w,
		:new.nr_seq_area_customer,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.nr_seq_grupo,
		:new.ds_feature,
		:new.dt_aprovacao,
		:new.nr_seq_apresentacao,
		:new.nr_seq_estagio,
		:new.ie_situacao,
		:new.cd_versao,
		null,
		:new.ds_overall_desc_rel,
		' ',
		:new.nr_sequencia,
		ie_tipo_alteracao_w);

end if;

commit;

copia_campo_long_de_para_java(	'REG_FEATURES_CUSTOMER',
				'DS_OVERALL_DESCRIPTION',
				' where nr_sequencia = :nr_sequencia',
				'nr_sequencia=' || :new.nr_sequencia,
				'REG_FEATURES_CUSTOMER_HIST',
				'DS_OVERALL_DESCRIPTION',
				' where nr_sequencia = :nr_sequencia',
				'nr_sequencia=' || nr_seq_hist_w,
				'L');

commit;

end;
/


ALTER TABLE TASY.REG_FEATURES_CUSTOMER ADD (
  CONSTRAINT REGFEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_FEATURES_CUSTOMER ADD (
  CONSTRAINT REGFEC_REGARC_FK 
 FOREIGN KEY (NR_SEQ_AREA_CUSTOMER) 
 REFERENCES TASY.REG_AREA_CUSTOMER (NR_SEQUENCIA),
  CONSTRAINT REGFEC_REGRUC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.REG_GRUPO_CUSTOMER (NR_SEQUENCIA),
  CONSTRAINT REGFEC_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_FEATURES_CUSTOMER TO NIVEL_1;

