ALTER TABLE TASY.PLS_CAD_INCONSIST_PESSOA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CAD_INCONSIST_PESSOA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CAD_INCONSIST_PESSOA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_INCONSISTENCIA    NUMBER(4)                NOT NULL,
  DS_INCONSISTENCIA    VARCHAR2(255 BYTE)       NOT NULL,
  IE_PAGADOR           VARCHAR2(1 BYTE)         NOT NULL,
  IE_BENEFICIARIO      VARCHAR2(1 BYTE)         NOT NULL,
  IE_ESTIPULANTE       VARCHAR2(1 BYTE)         NOT NULL,
  IE_PRESTADOR         VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_PESSOA       VARCHAR2(2 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ACAO_USUARIO      VARCHAR2(4000 BYTE),
  IE_PESSOA_FISICA     VARCHAR2(1 BYTE)         NOT NULL,
  IE_MENOR_IDADE       VARCHAR2(1 BYTE)         NOT NULL,
  IE_OPERADORA         VARCHAR2(1 BYTE),
  QT_DIAS_REVISAO      NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCAIP_ESTABEL_FK_I ON TASY.PLS_CAD_INCONSIST_PESSOA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCAIP_PK ON TASY.PLS_CAD_INCONSIST_PESSOA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CAD_INCONSIST_PES_UPDATE
after update ON TASY.PLS_CAD_INCONSIST_PESSOA for each row
declare
i			number(2);
nm_campo_w		varchar2(255)	:= '';
vl_anterior_w		varchar2(255)	:= '';
vl_atual_w		varchar2(255)	:= '';

begin

for i in 1..10 loop
	begin
	if	(:old.cd_inconsistencia  is not null) and
		(:old.cd_inconsistencia  <> :new.cd_inconsistencia ) and
		(i = 1) then
		nm_campo_w	:= 'CD_INCONSISTENCIA';
		vl_anterior_w	:= :old.cd_inconsistencia ;
		vl_atual_w	:= :new.cd_inconsistencia ;
	end if;

	if	(:old.ds_acao_usuario is not null) and
		(:old.ds_acao_usuario <> :new.ds_acao_usuario) and
		(i = 2) then
		nm_campo_w	:= 'DS_ACAO_USUARIO';
		vl_anterior_w	:= :old.ds_acao_usuario;
		vl_atual_w	:= :new.ds_acao_usuario;
	end if;

	if	(:old.ds_inconsistencia is not null) and
		(:old.ds_inconsistencia <> :new.ds_inconsistencia) and
		(i = 3) then
		nm_campo_w	:= 'DS_INCONSISTENCIA';
		vl_anterior_w	:= :old.ds_inconsistencia;
		vl_atual_w	:= :new.ds_inconsistencia;
	end if;

	if	(:old.ie_beneficiario is not null) and
		(:old.ie_beneficiario <> :new.ie_beneficiario) and
		(i = 4) then
		nm_campo_w	:= 'IE_BENEFICIARIO';
		vl_anterior_w	:= :old.ie_beneficiario;
		vl_atual_w	:= :new.ie_beneficiario;
	end if;

	if	(:old.ie_estipulante is not null) and
		(:old.ie_estipulante <> :new.ie_estipulante) and
		(i = 5) then
		nm_campo_w	:= 'IE_ESTIPULANTE';
		vl_anterior_w	:= :old.ie_estipulante;
		vl_atual_w	:= :new.ie_estipulante;
	end if;

	if	(:old.ie_pagador is not null) and
		(:old.ie_pagador <> :new.ie_pagador) and
		(i = 6) then
		nm_campo_w	:= 'IE_PAGADOR';
		vl_anterior_w	:= :old.ie_pagador;
		vl_atual_w	:= :new.ie_pagador;
	end if;

	if	(:old.ie_pessoa_fisica is not null) and
		(:old.ie_pessoa_fisica <> :new.ie_pessoa_fisica) and
		(i = 7) then
		nm_campo_w	:= 'IE_PESSOA_FISICA';
		vl_anterior_w	:= :old.ie_pessoa_fisica;
		vl_atual_w	:= :new.ie_pessoa_fisica;
	end if;

	if	(:old.ie_prestador is not null) and
		(:old.ie_prestador <> :new.ie_prestador) and
		(i = 8) then
		nm_campo_w	:= 'IE_PRESTADOR';
		vl_anterior_w	:= :old.ie_prestador;
		vl_atual_w	:= :new.ie_prestador;
	end if;

	if	(:old.ie_situacao is not null) and
		(:old.ie_situacao <> :new.ie_situacao) and
		(i = 9) then
		nm_campo_w	:= 'IE_SITUACAO';
		vl_anterior_w	:= :old.ie_situacao;
		vl_atual_w	:= :new.ie_situacao;
	end if;

	if	(:old.ie_tipo_pessoa is not null) and
		(:old.ie_tipo_pessoa <> :new.ie_tipo_pessoa) and
		(i = 10) then
		nm_campo_w	:= 'IE_TIPO_PESSOA';
		vl_anterior_w	:= :old.ie_tipo_pessoa;
		vl_atual_w	:= :new.ie_tipo_pessoa;
	end if;

	if	(nm_campo_w is not null) then
		insert into pls_cad_inc_pes_log_alt
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			nm_campo,
			vl_anterior,
			vl_atual,
			cd_inconsistencia)
		values	(pls_cad_inc_pes_log_alt_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.cd_estabelecimento,
			nm_campo_w,
			vl_anterior_w,
			vl_atual_w,
			:old.cd_inconsistencia);

		nm_campo_w	:= '';
	end if;
	end;
end loop;
end;
/


ALTER TABLE TASY.PLS_CAD_INCONSIST_PESSOA ADD (
  CONSTRAINT PLSCAIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CAD_INCONSIST_PESSOA ADD (
  CONSTRAINT PLSCAIP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_CAD_INCONSIST_PESSOA TO NIVEL_1;

