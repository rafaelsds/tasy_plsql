ALTER TABLE TASY.SIMPRO_CADASTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIMPRO_CADASTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIMPRO_CADASTRO
(
  CD_SIMPRO           NUMBER(15)                NOT NULL,
  DS_PRODUTO          VARCHAR2(100 BYTE)        NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_FABRICANTE       VARCHAR2(20 BYTE),
  DT_FORA_LINHA       DATE,
  CD_ESTABELECIMENTO  NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIMMATE_ESTABEL_FK_I ON TASY.SIMPRO_CADASTRO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIMMATE_I1 ON TASY.SIMPRO_CADASTRO
(CD_FABRICANTE, CD_SIMPRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIMMATE_I2 ON TASY.SIMPRO_CADASTRO
(DS_PRODUTO, CD_SIMPRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMMATE_I2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SIMMATE_PK ON TASY.SIMPRO_CADASTRO
(CD_SIMPRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SIMPRO_CADASTRO ADD (
  CONSTRAINT SIMMATE_PK
 PRIMARY KEY
 (CD_SIMPRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIMPRO_CADASTRO ADD (
  CONSTRAINT SIMMATE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SIMPRO_CADASTRO TO NIVEL_1;

