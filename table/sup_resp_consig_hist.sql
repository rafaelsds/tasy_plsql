ALTER TABLE TASY.SUP_RESP_CONSIG_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_RESP_CONSIG_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_RESP_CONSIG_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RESP          NUMBER(10)               NOT NULL,
  DS_HISTORICO         LONG                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SURCHIS_PK ON TASY.SUP_RESP_CONSIG_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCHIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.SURCHIS_SURECON_FK_I ON TASY.SUP_RESP_CONSIG_HIST
(NR_SEQ_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCHIS_SURECON_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUP_RESP_CONSIG_HIST ADD (
  CONSTRAINT SURCHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_RESP_CONSIG_HIST ADD (
  CONSTRAINT SURCHIS_SURECON_FK 
 FOREIGN KEY (NR_SEQ_RESP) 
 REFERENCES TASY.SUP_RESP_CONSIGNADO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_RESP_CONSIG_HIST TO NIVEL_1;

