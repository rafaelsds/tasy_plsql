ALTER TABLE TASY.PARAMETRO_NFS_LISTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_NFS_LISTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_NFS_LISTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_SQL               VARCHAR2(2000 BYTE)      NOT NULL,
  CD_MATERIAL          NUMBER(6),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORDENACAO         NUMBER(3),
  IE_LISTA_ITENS       VARCHAR2(1 BYTE),
  DS_SQL_SEQ_ITEM      VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARNFLI_MATERIA_FK_I ON TASY.PARAMETRO_NFS_LISTA
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARNFLI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARNFLI_PARNFS_FK_I ON TASY.PARAMETRO_NFS_LISTA
(CD_ESTABELECIMENTO, CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARNFLI_PK ON TASY.PARAMETRO_NFS_LISTA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARNFLI_PROCEDI_FK_I ON TASY.PARAMETRO_NFS_LISTA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARNFLI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_parametro_nfs_lista_insupd
before insert or update or delete ON TASY.PARAMETRO_NFS_LISTA for each row
begin

    if(wheb_usuario_pck.get_cd_perfil != 272 and :new.cd_convenio = 5135)then
    
        if(inserting)then
            hsj_gerar_log('Perfil sem permiss�o para inserir o registro!');
        elsif(updating)then
            hsj_gerar_log('Perfil sem permiss�o para alterar o registro!');
        elsif(deleting)then
            hsj_gerar_log('Perfil sem permiss�o para excluir o registro!');
        else
            hsj_gerar_log('Perfil sem permiss�o!');
        end if;
    
    end if;
    
end hsj_parametro_nfs_lista_insupd;
/


CREATE OR REPLACE TRIGGER TASY.PARAMETRO_NFS_LISTA_tp  after update ON TASY.PARAMETRO_NFS_LISTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ORDENACAO,1,4000),substr(:new.IE_ORDENACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LISTA_ITENS,1,4000),substr(:new.IE_LISTA_ITENS,1,4000),:new.nm_usuario,nr_seq_w,'IE_LISTA_ITENS',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PARAMETRO_NFS_LISTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PARAMETRO_NFS_LISTA ADD (
  CONSTRAINT PARNFLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETRO_NFS_LISTA ADD (
  CONSTRAINT PARNFLI_PARNFS_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CONVENIO) 
 REFERENCES TASY.PARAMETRO_NFS (CD_ESTABELECIMENTO,CD_CONVENIO),
  CONSTRAINT PARNFLI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PARNFLI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PARAMETRO_NFS_LISTA TO NIVEL_1;

