ALTER TABLE TASY.PLS_LOTE_PAGAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_PAGAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_PAGAMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_COMPETENCIA       NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  NR_SEQ_PERIODO           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_STATUS                VARCHAR2(1 BYTE)     NOT NULL,
  VL_LOTE                  NUMBER(15,2),
  DT_MES_COMPETENCIA       DATE,
  DT_GERACAO_TITULOS       DATE,
  DT_FECHAMENTO            DATE,
  NM_USUARIO_FECHAMENTO    VARCHAR2(15 BYTE),
  DT_INICIO_COMP           DATE,
  DT_FIM_COMP              DATE,
  DT_REF_TRIBUTO           DATE,
  DT_VENC_LOTE             DATE,
  DT_MES_COMPETENCIA_PROT  DATE,
  NR_LOTE_CONTABIL         NUMBER(10),
  DT_GERACAO_VENCIMENTOS   DATE,
  DT_LIB_PAG_PROT_INICIAL  DATE,
  DT_LIB_PAG_PROT_FINAL    DATE,
  NR_SEQ_LOTE_RET_TRIB     NUMBER(10),
  IE_EXIBE_PORTAL          VARCHAR2(1 BYTE),
  IE_COMPLEMENTAR_C        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOPA_ESTABEL_FK_I ON TASY.PLS_LOTE_PAGAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOPA_I ON TASY.PLS_LOTE_PAGAMENTO
(DT_MES_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOPA_LOTCONT_FK_I ON TASY.PLS_LOTE_PAGAMENTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPA_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOPA_PK ON TASY.PLS_LOTE_PAGAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOPA_PLSCOMP_FK_I ON TASY.PLS_LOTE_PAGAMENTO
(NR_SEQ_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPA_PLSCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOPA_PLSLORT_FK_I ON TASY.PLS_LOTE_PAGAMENTO
(NR_SEQ_LOTE_RET_TRIB)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPA_PLSLORT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOPA_PLSPEPA_FK_I ON TASY.PLS_LOTE_PAGAMENTO
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPA_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_lote_pagamento_update
before insert or update ON TASY.PLS_LOTE_PAGAMENTO for each row
declare

ie_tributo_mes_w		Varchar2(10);

begin

select	nvl(max(ie_tributo_mes),'L')
into	ie_tributo_mes_w
from	pls_periodo_pagamento
where	nr_sequencia	= :new.nr_seq_periodo;

if	(ie_tributo_mes_w	= 'L') then
	:new.dt_ref_tributo	:= :new.dt_mes_competencia;
elsif	(ie_tributo_mes_w	= 'A') then
	:new.dt_ref_tributo	:= add_months(:new.dt_mes_competencia,-1);
end if;

:new.dt_mes_competencia	:= trunc(:new.dt_mes_competencia, 'month');

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_LOTE_PAGAMENTO_tp  after update ON TASY.PLS_LOTE_PAGAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_EXIBE_PORTAL,1,4000),substr(:new.IE_EXIBE_PORTAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIBE_PORTAL',ie_log_w,ds_w,'PLS_LOTE_PAGAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_LOTE_PAGAMENTO ADD (
  CONSTRAINT PLSLOPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_PAGAMENTO ADD (
  CONSTRAINT PLSLOPA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSLOPA_PLSLORT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RET_TRIB) 
 REFERENCES TASY.PLS_LOTE_RETENCAO_TRIB (NR_SEQUENCIA),
  CONSTRAINT PLSLOPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOPA_PLSCOMP_FK 
 FOREIGN KEY (NR_SEQ_COMPETENCIA) 
 REFERENCES TASY.PLS_COMPETENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSLOPA_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_PAGAMENTO TO NIVEL_1;

