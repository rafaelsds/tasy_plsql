ALTER TABLE TASY.LIB_MATERIAL_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LIB_MATERIAL_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LIB_MATERIAL_PACIENTE
(
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  QT_DOSE_DIARIA        NUMBER(15,3),
  CD_INTERVALO          VARCHAR2(7 BYTE),
  QT_DIAS_TRATAMENTO    NUMBER(3),
  DT_SUSPENSO           DATE,
  NM_USUARIO_SUSP       VARCHAR2(15 BYTE),
  DT_INICIO_VALIDADE    DATE,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_JUSTIFICATIVA      VARCHAR2(2000 BYTE),
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(50 BYTE),
  NR_PRESCRICAO         NUMBER(15),
  QT_DIAS_LIBERADOS     NUMBER(3),
  NM_USUARIO_ANALISE    VARCHAR2(15 BYTE),
  DT_ANALISE            DATE,
  NR_SEQ_MATERIAL       NUMBER(15),
  NR_ATENDIMENTO        NUMBER(10),
  NR_SEQ_PACIENTE       NUMBER(10),
  IE_URGENTE            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LIBMAPA_INTPRES_FK_I ON TASY.LIB_MATERIAL_PACIENTE
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LIBMAPA_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LIBMAPA_MATERIAL_FK_I ON TASY.LIB_MATERIAL_PACIENTE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIBMAPA_PESFISI_FK_I ON TASY.LIB_MATERIAL_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LIBMAPA_PK ON TASY.LIB_MATERIAL_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Lib_Material_Paciente_insert
AFTER INSERT ON Lib_Material_Paciente
FOR EACH ROW
declare
nr_sequencia_w	number(10);

BEGIN

select	lib_material_paciente_hist_seq.NextVal
into	nr_sequencia_w
from	dual;

insert into lib_material_paciente_hist(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_pessoa_fisica,
	cd_material,
	dt_acao,
	cd_pessoa_acao,
	ie_tipo_acao,
	qt_dias_tratamento,
	dt_inicio_validade)
values(
	nr_sequencia_w,
	sysdate,
	:new.nm_usuario,
	:new.cd_pessoa_fisica,
	:new.cd_material,
	sysdate,
	substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
	'I',
	:new.qt_dias_tratamento,
	:new.dt_inicio_validade);
END;
/


CREATE OR REPLACE TRIGGER TASY.Lib_Material_Paciente_Update
BEFORE UPDATE ON TASY.LIB_MATERIAL_PACIENTE FOR EACH ROW
declare
nr_sequencia_w	number(10);

BEGIN

select	lib_material_paciente_hist_seq.NextVal
into	nr_sequencia_w
from	dual;

if	(:new.dt_suspenso is not null)	and
    	(:old.dt_suspenso is null)		then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'S',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);

elsif	(:new.dt_suspenso is null)		and
    	(:old.dt_suspenso is not null)	then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'L',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);

elsif	(:new.qt_dias_tratamento <> :old.qt_dias_tratamento)	then
	insert into lib_material_paciente_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_material,
		dt_acao,
		cd_pessoa_acao,
		ie_tipo_acao,
		qt_dias_tratamento,
		dt_inicio_validade)
	values(
		nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.cd_material,
		sysdate,
		substr(obter_dados_usuario_opcao(:new.nm_usuario,'C'),1,100),
		'A',
		:new.qt_dias_tratamento,
		:new.dt_inicio_validade);
end if;

if	(:new.dt_liberacao is not null) then
	update	prescr_material
	set	QT_TOTAL_DIAS_LIB 	= :new.QT_DIAS_TRATAMENTO
	where	nr_prescricao		= :new.NR_PRESCRICAO
	and	CD_MATERIAL		= :new.CD_MATERIAL;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Lib_Material_Paciente_delete
BEFORE DELETE ON TASY.LIB_MATERIAL_PACIENTE FOR EACH ROW
declare
nr_sequencia_w	number(10);

BEGIN

update	prescr_material
set	nr_seq_lib_mat_pac	= null
where	nr_prescricao		= :old.nr_prescricao
and	nr_sequencia		= :old.nr_seq_material
and	nr_seq_lib_mat_pac	= :old.nr_sequencia;

select	lib_material_paciente_hist_seq.NextVal
into	nr_sequencia_w
from	dual;

insert into lib_material_paciente_hist(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_pessoa_fisica,
	cd_material,
	dt_acao,
	cd_pessoa_acao,
	ie_tipo_acao,
	qt_dias_tratamento,
	dt_inicio_validade)
values(
	nr_sequencia_w,
	sysdate,
	:old.nm_usuario,
	:old.cd_pessoa_fisica,
	:old.cd_material,
	sysdate,
	substr(obter_dados_usuario_opcao(:old.nm_usuario,'C'),1,100),
	'E',
	:old.qt_dias_tratamento,
	:old.dt_inicio_validade);
END;
/


ALTER TABLE TASY.LIB_MATERIAL_PACIENTE ADD (
  CONSTRAINT LIBMAPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LIB_MATERIAL_PACIENTE ADD (
  CONSTRAINT LIBMAPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LIBMAPA_MATERIAL_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT LIBMAPA_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO));

GRANT SELECT ON TASY.LIB_MATERIAL_PACIENTE TO NIVEL_1;

