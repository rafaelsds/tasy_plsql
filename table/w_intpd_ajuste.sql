DROP TABLE TASY.W_INTPD_AJUSTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTPD_AJUSTE
(
  NR_SEQ_EVENTO        NUMBER(10)               NOT NULL,
  NR_SEQ_PROJETO_XML   NUMBER(10),
  NR_SEQ_ELEMENTO_XML  NUMBER(10),
  NR_SEQ_ATRIBUTO_XML  NUMBER(10),
  IE_TIPO_AJUSTE       VARCHAR2(15 BYTE)        NOT NULL,
  QT_MINUTO            NUMBER(5)                NOT NULL,
  IE_IMPACTO_LEGADO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_BLOQUEIA          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WINTPDA_INTPDCEV_FK_I ON TASY.W_INTPD_AJUSTE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WINTPDA_XMLATRI_FK_I ON TASY.W_INTPD_AJUSTE
(NR_SEQ_ATRIBUTO_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WINTPDA_XMLELEM_FK_I ON TASY.W_INTPD_AJUSTE
(NR_SEQ_ELEMENTO_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WINTPDA_XMLPROJ_FK_I ON TASY.W_INTPD_AJUSTE
(NR_SEQ_PROJETO_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_INTPD_AJUSTE ADD (
  CONSTRAINT WINTPDA_XMLATRI_FK 
 FOREIGN KEY (NR_SEQ_ATRIBUTO_XML) 
 REFERENCES TASY.XML_ATRIBUTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WINTPDA_XMLELEM_FK 
 FOREIGN KEY (NR_SEQ_ELEMENTO_XML) 
 REFERENCES TASY.XML_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WINTPDA_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO_XML) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WINTPDA_INTPDCEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.INTPD_CONFIG_EVENTOS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_INTPD_AJUSTE TO NIVEL_1;

