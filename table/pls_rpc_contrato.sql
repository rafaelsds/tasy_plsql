ALTER TABLE TASY.PLS_RPC_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RPC_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RPC_CONTRATO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_LOTE            NUMBER(10)             NOT NULL,
  NR_SEQ_CONTRATO        NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_REAJUSTE        NUMBER(10),
  NM_ARQUIVO             VARCHAR2(255 BYTE)     DEFAULT null,
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  DS_CARACTERISTICA      VARCHAR2(255 BYTE),
  IE_CARACTERISTICA      VARCHAR2(2 BYTE),
  NR_SEQ_REAJUSTE_PROG   NUMBER(10),
  NR_CONTRATO_ARQUIVO    VARCHAR2(30 BYTE),
  IE_GERAR_FRANQUIA      VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_CONTRATO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPCC_PK ON TASY.PLS_RPC_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPCC_PLSPRRC_FK_I ON TASY.PLS_RPC_CONTRATO
(NR_SEQ_REAJUSTE_PROG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPCC_PLSREAJ_FK_I ON TASY.PLS_RPC_CONTRATO
(NR_SEQ_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPCC_PLSRPCL_FK_I ON TASY.PLS_RPC_CONTRATO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_RPC_CONTRATO ADD (
  CONSTRAINT PLSRPCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RPC_CONTRATO ADD (
  CONSTRAINT PLSRPCC_PLSREAJ_FK 
 FOREIGN KEY (NR_SEQ_REAJUSTE) 
 REFERENCES TASY.PLS_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSRPCC_PLSRPCL_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_RPC_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSRPCC_PLSPRRC_FK 
 FOREIGN KEY (NR_SEQ_REAJUSTE_PROG) 
 REFERENCES TASY.PLS_PROG_REAJ_COLETIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RPC_CONTRATO TO NIVEL_1;

