ALTER TABLE TASY.MATERIAL_HORARIO_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_HORARIO_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_HORARIO_PADRAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  HR_INICIO            DATE                     NOT NULL,
  HR_FINAL             DATE                     NOT NULL,
  DT_FIM               DATE,
  DT_INICIO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATHPAD_MATERIA_FK_I ON TASY.MATERIAL_HORARIO_PADRAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATHPAD_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATHPAD_PK ON TASY.MATERIAL_HORARIO_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_horario_padrao_atual
before insert or update ON TASY.MATERIAL_HORARIO_PADRAO for each row
declare

begin
begin
	if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.MATERIAL_HORARIO_PADRAO ADD (
  CONSTRAINT MATHPAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_HORARIO_PADRAO ADD (
  CONSTRAINT MATHPAD_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.MATERIAL_HORARIO_PADRAO TO NIVEL_1;

