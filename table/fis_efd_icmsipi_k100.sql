ALTER TABLE TASY.FIS_EFD_ICMSIPI_K100
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_ICMSIPI_K100 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_ICMSIPI_K100
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  DT_INI               DATE,
  DT_FIN               DATE,
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ICMSK100_EFDICMSCON_FK_I ON TASY.FIS_EFD_ICMSIPI_K100
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ICMSK100_PK ON TASY.FIS_EFD_ICMSIPI_K100
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_EFD_ICMSIPI_K100 ADD (
  CONSTRAINT ICMSK100_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_EFD_ICMSIPI_K100 ADD (
  CONSTRAINT ICMSK100_EFDICMSCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_EFD_ICMSIPI_K100 TO NIVEL_1;

