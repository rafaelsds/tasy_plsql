ALTER TABLE TASY.CLASSE_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLASSE_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLASSE_MATERIAL
(
  CD_CLASSE_MATERIAL    NUMBER(5)               NOT NULL,
  DS_CLASSE_MATERIAL    VARCHAR2(255 BYTE)      NOT NULL,
  CD_SUBGRUPO_MATERIAL  NUMBER(3)               NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE),
  IE_TIPO_CLASSE        VARCHAR2(2 BYTE),
  NR_SEQ_APRES          NUMBER(3),
  CD_SISTEMA_ANT        VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.CLASSE_MATERIAL.CD_CLASSE_MATERIAL IS 'Codigo da Classe de Material';

COMMENT ON COLUMN TASY.CLASSE_MATERIAL.DS_CLASSE_MATERIAL IS 'Descricao da Classe de Material';

COMMENT ON COLUMN TASY.CLASSE_MATERIAL.CD_SUBGRUPO_MATERIAL IS 'Codigo do Subgrupo de Material';

COMMENT ON COLUMN TASY.CLASSE_MATERIAL.IE_SITUACAO IS 'Indicaor da situacao da classe do Material.';


CREATE INDEX TASY.CLAMATE_CONCONT_FK_I ON TASY.CLASSE_MATERIAL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CLAMATE_PK ON TASY.CLASSE_MATERIAL
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CLAMATE_SUBGRMA_FK_I ON TASY.CLASSE_MATERIAL
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CLASSE_MATERIAL_tp  after update ON TASY.CLASSE_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CLASSE_MATERIAL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_CLASSE,1,4000),substr(:new.IE_TIPO_CLASSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CLASSE',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CLASSE_MATERIAL,1,4000),substr(:new.DS_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_CLASSE_MATERIAL',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'CLASSE_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.classe_material_after
after insert or update ON TASY.CLASSE_MATERIAL for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin
reg_integracao_p.cd_classe_material	:=	:new.cd_classe_material;
reg_integracao_p.cd_subgrupo_material	:=	:new.cd_subgrupo_material;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;

if	(inserting) then
	reg_integracao_p.ie_operacao	:=	'I';
elsif	(updating) then
	reg_integracao_p.ie_operacao	:=	'A';
end if;

gerar_int_padrao.gravar_integracao('4', :new.cd_classe_material, :new.nm_usuario, reg_integracao_p);
end;
/


ALTER TABLE TASY.CLASSE_MATERIAL ADD (
  CONSTRAINT CLAMATE_PK
 PRIMARY KEY
 (CD_CLASSE_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CLASSE_MATERIAL ADD (
  CONSTRAINT CLAMATE_SUBGRMA_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT CLAMATE_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.CLASSE_MATERIAL TO NIVEL_1;

