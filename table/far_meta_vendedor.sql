ALTER TABLE TASY.FAR_META_VENDEDOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FAR_META_VENDEDOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.FAR_META_VENDEDOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_META          NUMBER(10)               NOT NULL,
  NR_SEQ_VENDEDOR      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FARMVEN_FARMEVE_FK_I ON TASY.FAR_META_VENDEDOR
(NR_SEQ_META)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FARMVEN_FARMEVE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FARMVEN_FARVEND_FK_I ON TASY.FAR_META_VENDEDOR
(NR_SEQ_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FARMVEN_FARVEND_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FARMVEN_PK ON TASY.FAR_META_VENDEDOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FAR_META_VENDEDOR ADD (
  CONSTRAINT FARMVEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FAR_META_VENDEDOR ADD (
  CONSTRAINT FARMVEN_FARVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR) 
 REFERENCES TASY.FAR_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT FARMVEN_FARMEVE_FK 
 FOREIGN KEY (NR_SEQ_META) 
 REFERENCES TASY.FAR_META_VENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FAR_META_VENDEDOR TO NIVEL_1;

