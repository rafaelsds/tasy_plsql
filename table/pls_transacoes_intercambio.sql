ALTER TABLE TASY.PLS_TRANSACOES_INTERCAMBIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TRANSACOES_INTERCAMBIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TRANSACOES_INTERCAMBIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE_INT      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REQUISICAO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTRIN_PK ON TASY.PLS_TRANSACOES_INTERCAMBIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRIN_PLSLTRI_FK_I ON TASY.PLS_TRANSACOES_INTERCAMBIO
(NR_SEQ_LOTE_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRIN_PLSLTRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRIN_PLSREQU_FK_I ON TASY.PLS_TRANSACOES_INTERCAMBIO
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRIN_PLSREQU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_TRANSACOES_INTERCAMBIO ADD (
  CONSTRAINT PLSTRIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TRANSACOES_INTERCAMBIO ADD (
  CONSTRAINT PLSTRIN_PLSLTRI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_INT) 
 REFERENCES TASY.PLS_LOTE_TRANSACOES_INT (NR_SEQUENCIA),
  CONSTRAINT PLSTRIN_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TRANSACOES_INTERCAMBIO TO NIVEL_1;

