ALTER TABLE TASY.AGEINT_MARCACAO_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_MARCACAO_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_MARCACAO_USUARIO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_AGEINT               NUMBER(10),
  NM_USUARIO                  VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA               NUMBER(10),
  IE_GERADO                   VARCHAR2(1 BYTE),
  NR_MINUTO_DURACAO           NUMBER(10),
  HR_AGENDA                   DATE,
  CD_AGENDA                   NUMBER(10),
  NR_SEQ_AGEINT_ITEM          NUMBER(10),
  IE_HORARIO_AUXILIAR         VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  IE_ENCAIXE                  VARCHAR2(1 BYTE),
  NM_USUARIO_CONFIRM_ENCAIXE  VARCHAR2(15 BYTE),
  DT_CONFIRM_ENCAIXE          DATE,
  NR_SEQ_COMBO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIMAUS_AGEINIT_FK_I ON TASY.AGEINT_MARCACAO_USUARIO
(NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMAUS_AGEINIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIMAUS_I1 ON TASY.AGEINT_MARCACAO_USUARIO
(CD_AGENDA, HR_AGENDA, NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMAUS_I1
  MONITORING USAGE;


CREATE INDEX TASY.AGIMAUS_I2 ON TASY.AGEINT_MARCACAO_USUARIO
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMAUS_I2
  MONITORING USAGE;


CREATE INDEX TASY.AGIMAUS_I3 ON TASY.AGEINT_MARCACAO_USUARIO
(CD_PESSOA_FISICA, HR_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGIMAUS_I4 ON TASY.AGEINT_MARCACAO_USUARIO
(NR_SEQ_AGEINT_ITEM, IE_GERADO, HR_AGENDA, NR_MINUTO_DURACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGIMAUS_PESFISI_FK_I ON TASY.AGEINT_MARCACAO_USUARIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGIMAUS_PK ON TASY.AGEINT_MARCACAO_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMAUS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ageint_marcacao_usuario_delete
after delete ON TASY.AGEINT_MARCACAO_USUARIO for each row
declare

pragma autonomous_transaction;

begin

if	(:old.nr_seq_combo is not null) then
	update	ageint_marcacao_usuario
	set	nr_seq_combo	= null
	where	nm_usuario	= :old.nm_usuario
	and	nr_seq_ageint	= :old.nr_seq_ageint
	and	nr_seq_combo	= :old.nr_seq_combo
	and	nr_sequencia	!= :old.nr_sequencia;
	commit;
end if;

end;
/


ALTER TABLE TASY.AGEINT_MARCACAO_USUARIO ADD (
  CONSTRAINT AGIMAUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_MARCACAO_USUARIO ADD (
  CONSTRAINT AGIMAUS_AGEINIT_FK 
 FOREIGN KEY (NR_SEQ_AGEINT_ITEM) 
 REFERENCES TASY.AGENDA_INTEGRADA_ITEM (NR_SEQUENCIA),
  CONSTRAINT AGIMAUS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGEINT_MARCACAO_USUARIO TO NIVEL_1;

