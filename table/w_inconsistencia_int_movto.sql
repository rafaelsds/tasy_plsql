ALTER TABLE TASY.W_INCONSISTENCIA_INT_MOVTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INCONSISTENCIA_INT_MOVTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INCONSISTENCIA_INT_MOVTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_ATUALIZACAO        DATE,
  NM_USUARIO            VARCHAR2(15 BYTE),
  IE_TIPO_INTEGRACAO    VARCHAR2(15 BYTE),
  DS_INCONSISTENCIA     VARCHAR2(4000 BYTE),
  IE_ENVIO_RECEBE       VARCHAR2(1 BYTE),
  IE_TIPO_CONSISTENCIA  VARCHAR2(15 BYTE),
  NR_SEQ_MOVTO_INT      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINTMOV_PK ON TASY.W_INCONSISTENCIA_INT_MOVTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTMOV_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INCONSISTENCIA_INT_MOVTO ADD (
  CONSTRAINT WINTMOV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INCONSISTENCIA_INT_MOVTO TO NIVEL_1;

