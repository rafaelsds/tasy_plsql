ALTER TABLE TASY.SUP_INT_MAT_MARCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_MAT_MARCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_MAT_MARCA
(
  CD_MATERIAL                    NUMBER(6)      NOT NULL,
  IE_FORMA_INTEGRACAO            VARCHAR2(15 BYTE) NOT NULL,
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_LEITURA                     DATE,
  DT_CONFIRMA_INTEGRACAO         DATE,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  CD_REFERENCIA                  VARCHAR2(80 BYTE),
  QT_PRIORIDADE                  NUMBER(5),
  NR_REGISTRO_ANVISA             VARCHAR2(60 BYTE),
  DT_VALIDADE_REG_ANVISA         DATE,
  CD_CNPJ                        VARCHAR2(14 BYTE),
  DT_REPROVACAO                  DATE,
  NM_USUARIO_REPROVACAO          VARCHAR2(15 BYTE),
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  QT_CONV_COMPRA_EST             NUMBER(15,4),
  CD_UNIDADE_MEDIDA              VARCHAR2(30 BYTE),
  NR_CERTIFICADO_APROVACAO       VARCHAR2(20 BYTE),
  DT_VALIDADE_CERTIFICADO_APROV  DATE,
  IE_PADRONIZADO                 VARCHAR2(15 BYTE),
  NR_SEQ_STATUS_AVAL             NUMBER(10),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DS_JUSTIFICATIVA_PADRAO        VARCHAR2(4000 BYTE),
  CD_SISTEMA_ANT                 VARCHAR2(80 BYTE),
  IE_VIGENTE_ANVISA              VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINTMMAR_MARCA_PK ON TASY.SUP_INT_MAT_MARCA
(NR_SEQUENCIA, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_INT_MAT_MARCA_tp  after update ON TASY.SUP_INT_MAT_MARCA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA)||'#@#@CD_MATERIAL='||to_char(:old.CD_MATERIAL);gravar_log_alteracao(substr(:old.QT_PRIORIDADE,1,4000),substr(:new.QT_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ,1,4000),substr(:new.CD_CNPJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_STATUS_AVAL,1,4000),substr(:new.NR_SEQ_STATUS_AVAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_STATUS_AVAL',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_ANVISA,1,4000),substr(:new.NR_REGISTRO_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_ANVISA',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_APROVACAO,1,4000),substr(:new.NR_CERTIFICADO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_APROVACAO',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_REG_ANVISA',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERTIFICADO_APROV',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA_PADRAO,1,4000),substr(:new.DS_JUSTIFICATIVA_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA_PADRAO',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_REFERENCIA,1,4000),substr(:new.CD_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_REFERENCIA',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_EST,1,4000),substr(:new.QT_CONV_COMPRA_EST,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_EST',ie_log_w,ds_w,'SUP_INT_MAT_MARCA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.sup_int_mat_marca_atual
before update or insert ON TASY.SUP_INT_MAT_MARCA for each row
declare

nr_sequencia_w			marca.nr_sequencia%type;
nr_seq_material_marca_w		material_marca.nr_sequencia%type := 0;
cd_material_w			material.cd_material%type;
qt_existe_erros_w		number(10);

begin

if (:new.ie_forma_integracao = 'R') then

	:new.dt_leitura	:= sysdate;

	consiste_sup_int_mat_marca(	:new.nr_sequencia,
					:new.cd_material,
					:new.cd_unidade_medida,
					:new.ie_padronizado,
					:new.nr_seq_status_aval,
					:new.nm_usuario );

	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_marca_consist
	where	nr_sequencia = :new.nr_sequencia
	and	ie_integracao = '1';

	if	(qt_existe_erros_w = 0) then

		if (updating) then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_material_marca_w
			from	material_marca
			where	cd_sistema_ant = to_char(:new.nr_sequencia);

		end if;

		if (nr_seq_material_marca_w = 0) then

			select 	max(nr_sequencia)
			into	nr_sequencia_w
			from 	marca
			where	cd_sistema_ant = to_char(:new.nr_sequencia);

			select 	max(cd_material)
			into	cd_material_w
			from 	material
			where 	cd_sistema_ant = to_char(:new.cd_material);

			insert into material_marca(
				nr_sequencia,
				cd_material,
				qt_prioridade,
				ie_situacao,
				nr_registro_anvisa,
				dt_validade_reg_anvisa,
				cd_cnpj,
				dt_reprovacao,
				nm_usuario_reprovacao,
				ds_observacao,
				cd_referencia,
				qt_conv_compra_est,
				cd_unidade_medida,
				nr_certificado_aprovacao,
				dt_validade_certificado_aprov,
				ie_padronizado,
				nr_seq_status_aval,
				ds_justificativa_padrao,
				ie_vigente_anvisa,
				cd_sistema_ant,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_sequencia_w,
				cd_material_w,
				:new.qt_prioridade,
				decode(:new.ie_situacao,'I',:new.ie_situacao,'A'),
				:new.nr_registro_anvisa,
				:new.dt_validade_reg_anvisa,
				:new.cd_cnpj,
				:new.dt_reprovacao,
				:new.nm_usuario_reprovacao,
				:new.ds_observacao,
				:new.cd_referencia,
				:new.qt_conv_compra_est,
				:new.cd_unidade_medida,
				:new.nr_certificado_aprovacao,
				:new.dt_validade_certificado_aprov,
				:new.ie_padronizado,
				:new.nr_seq_status_aval,
				:new.ds_justificativa_padrao,
				:new.ie_vigente_anvisa,
				:new.nr_sequencia,
				:new.dt_atualizacao,
				:new.nm_usuario,
				:new.dt_atualizacao_nrec,
				:new.nm_usuario_nrec);

			:new.dt_confirma_integracao := sysdate;
			:new.cd_sistema_ant := nr_sequencia_w;

		else

			select 	max(cd_material)
			into	cd_material_w
			from 	material
			where 	cd_sistema_ant = :new.cd_material;

			update	material_marca
			set	cd_material = cd_material_w,
				qt_prioridade = :new.qt_prioridade,
				ie_situacao = :new.ie_situacao,
				nr_registro_anvisa = :new.nr_registro_anvisa,
				dt_validade_reg_anvisa = :new.dt_validade_reg_anvisa,
				cd_cnpj = :new.cd_cnpj,
				dt_reprovacao = :new.dt_reprovacao,
				nm_usuario_reprovacao = :new.nm_usuario_reprovacao,
				ds_observacao = :new.ds_observacao,
				cd_referencia = :new.cd_referencia,
				qt_conv_compra_est = :new.qt_conv_compra_est,
				cd_unidade_medida = :new.cd_unidade_medida,
				nr_certificado_aprovacao = :new.nr_certificado_aprovacao,
				dt_validade_certificado_aprov = :new.dt_validade_certificado_aprov,
				ie_padronizado = :new.ie_padronizado,
				nr_seq_status_aval = :new.nr_seq_status_aval,
				ds_justificativa_padrao = :new.ds_justificativa_padrao,
				ie_vigente_anvisa = :new.ie_vigente_anvisa,
				cd_sistema_ant = :new.cd_sistema_ant,
				dt_atualizacao = :new.dt_atualizacao,
				nm_usuario = :new.nm_usuario,
				dt_atualizacao_nrec = :new.dt_atualizacao_nrec,
				nm_usuario_nrec = :new.nm_usuario
			where	cd_sistema_ant = :new.nr_sequencia;

		end if;

	end if;

end if;

end;
/


ALTER TABLE TASY.SUP_INT_MAT_MARCA ADD (
  CONSTRAINT SUINTMMAR_MARCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA, CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_MAT_MARCA TO NIVEL_1;

