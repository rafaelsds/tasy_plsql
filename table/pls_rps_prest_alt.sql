ALTER TABLE TASY.PLS_RPS_PREST_ALT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RPS_PREST_ALT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RPS_PREST_ALT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RPS_PREST     NUMBER(10)               NOT NULL,
  NR_SEQ_ALT_PREST     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPAL_PK ON TASY.PLS_RPS_PREST_ALT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPAL_PLSAPRP_FK_I ON TASY.PLS_RPS_PREST_ALT
(NR_SEQ_ALT_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPAL_PLSRPPR_FK_I ON TASY.PLS_RPS_PREST_ALT
(NR_SEQ_RPS_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_RPS_PREST_ALT ADD (
  CONSTRAINT PLSRPAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RPS_PREST_ALT ADD (
  CONSTRAINT PLSRPAL_PLSAPRP_FK 
 FOREIGN KEY (NR_SEQ_ALT_PREST) 
 REFERENCES TASY.PLS_ALT_PREST_RPS (NR_SEQUENCIA),
  CONSTRAINT PLSRPAL_PLSRPPR_FK 
 FOREIGN KEY (NR_SEQ_RPS_PREST) 
 REFERENCES TASY.PLS_RPS_PRESTADOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_RPS_PREST_ALT TO NIVEL_1;

