ALTER TABLE TASY.EHR_CONSULTA_GRID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_CONSULTA_GRID CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_CONSULTA_GRID
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_APRES         NUMBER(10),
  NM_CAMPO_BASE        VARCHAR2(30 BYTE)        NOT NULL,
  QT_LARGURA           NUMBER(5),
  DS_INFORMACAO        VARCHAR2(255 BYTE)       NOT NULL,
  DS_MASCARA           VARCHAR2(42 BYTE),
  IE_TIPO_EDICAO       VARCHAR2(5 BYTE)         NOT NULL,
  NR_SEQ_CONSULTA      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRCONG_EHRCONS_FK_I ON TASY.EHR_CONSULTA_GRID
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRCONG_PK ON TASY.EHR_CONSULTA_GRID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EHR_CONSULTA_GRID_tp  after update ON TASY.EHR_CONSULTA_GRID FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_CAMPO_BASE,1,4000),substr(:new.NM_CAMPO_BASE,1,4000),:new.nm_usuario,nr_seq_w,'NM_CAMPO_BASE',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_EDICAO,1,4000),substr(:new.IE_TIPO_EDICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_EDICAO',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INFORMACAO,1,4000),substr(:new.DS_INFORMACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_INFORMACAO',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MASCARA,1,4000),substr(:new.DS_MASCARA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LARGURA,1,4000),substr(:new.QT_LARGURA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LARGURA',ie_log_w,ds_w,'EHR_CONSULTA_GRID',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.EHR_CONSULTA_GRID ADD (
  CONSTRAINT EHRCONG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.EHR_CONSULTA_GRID ADD (
  CONSTRAINT EHRCONG_EHRCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.EHR_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.EHR_CONSULTA_GRID TO NIVEL_1;

