ALTER TABLE TASY.PROCEDIMENTO_MAT_CONSIGNADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO_MAT_CONSIGNADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO_MAT_CONSIGNADO
(
  CD_MATERIAL       NUMBER(6)                   NOT NULL,
  CD_PROCEDIMENTO   NUMBER(15)                  NOT NULL,
  IE_ORIGEM_PROCED  NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRMACON_MATERIA_FK_I ON TASY.PROCEDIMENTO_MAT_CONSIGNADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMACON_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRMACON_PK ON TASY.PROCEDIMENTO_MAT_CONSIGNADO
(CD_MATERIAL, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMACON_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRMACON_PROCEDI_FK_I ON TASY.PROCEDIMENTO_MAT_CONSIGNADO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMACON_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROCEDIMENTO_MAT_CONSIGNADO ADD (
  CONSTRAINT PRMACON_PK
 PRIMARY KEY
 (CD_MATERIAL, CD_PROCEDIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO_MAT_CONSIGNADO ADD (
  CONSTRAINT PRMACON_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PRMACON_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PROCEDIMENTO_MAT_CONSIGNADO TO NIVEL_1;

