ALTER TABLE TASY.REGRA_QT_MINIMA_FORNEC_COT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_QT_MINIMA_FORNEC_COT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_QT_MINIMA_FORNEC_COT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_MINIMO            NUMBER(13,4)             NOT NULL,
  VL_MAXIMO            NUMBER(13,4)             NOT NULL,
  QT_MINIMO_FORNEC     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REQTMIFC_ESTABEL_FK_I ON TASY.REGRA_QT_MINIMA_FORNEC_COT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REQTMIFC_PK ON TASY.REGRA_QT_MINIMA_FORNEC_COT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQTMIFC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_QT_MINIMA_FORNEC_COT_tp  after update ON TASY.REGRA_QT_MINIMA_FORNEC_COT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.VL_MINIMO,1,4000),substr(:new.VL_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO',ie_log_w,ds_w,'REGRA_QT_MINIMA_FORNEC_COT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_QT_MINIMA_FORNEC_COT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_FORNEC,1,4000),substr(:new.QT_MINIMO_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_FORNEC',ie_log_w,ds_w,'REGRA_QT_MINIMA_FORNEC_COT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAXIMO,1,4000),substr(:new.VL_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAXIMO',ie_log_w,ds_w,'REGRA_QT_MINIMA_FORNEC_COT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_QT_MINIMA_FORNEC_COT ADD (
  CONSTRAINT REQTMIFC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_QT_MINIMA_FORNEC_COT ADD (
  CONSTRAINT REQTMIFC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_QT_MINIMA_FORNEC_COT TO NIVEL_1;

