ALTER TABLE TASY.REG_LIC_MOD_DOCTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_LIC_MOD_DOCTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_LIC_MOD_DOCTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOD_COMPRA    NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_DOCTO    NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGLIDO_PK ON TASY.REG_LIC_MOD_DOCTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIDO_PK
  MONITORING USAGE;


CREATE INDEX TASY.REGLIDO_REGLIMC_FK_I ON TASY.REG_LIC_MOD_DOCTO
(NR_SEQ_MOD_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIDO_REGLIMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLIDO_TIDOFOR_FK_I ON TASY.REG_LIC_MOD_DOCTO
(NR_SEQ_TIPO_DOCTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIDO_TIDOFOR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REG_LIC_MOD_DOCTO ADD (
  CONSTRAINT REGLIDO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_LIC_MOD_DOCTO ADD (
  CONSTRAINT REGLIDO_REGLIMC_FK 
 FOREIGN KEY (NR_SEQ_MOD_COMPRA) 
 REFERENCES TASY.REG_LIC_MOD_COMPRA (NR_SEQUENCIA),
  CONSTRAINT REGLIDO_TIDOFOR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCTO) 
 REFERENCES TASY.TIPO_DOCUMENTO_FORNEC (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_LIC_MOD_DOCTO TO NIVEL_1;

