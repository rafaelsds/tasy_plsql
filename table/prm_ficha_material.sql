ALTER TABLE TASY.PRM_FICHA_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRM_FICHA_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRM_FICHA_MATERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FICHA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRMFIMA_MATERIA_FK_I ON TASY.PRM_FICHA_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIMA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRMFIMA_PK ON TASY.PRM_FICHA_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIMA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRMFIMA_PRMFIOCO_FK_I ON TASY.PRM_FICHA_MATERIAL
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIMA_PRMFIOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRM_FICHA_MATERIAL ADD (
  CONSTRAINT PRMFIMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRM_FICHA_MATERIAL ADD (
  CONSTRAINT PRMFIMA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PRMFIMA_PRMFIOCO_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.PRM_FICHA_OCORRENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRM_FICHA_MATERIAL TO NIVEL_1;

