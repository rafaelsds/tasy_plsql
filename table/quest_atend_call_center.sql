ALTER TABLE TASY.QUEST_ATEND_CALL_CENTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUEST_ATEND_CALL_CENTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUEST_ATEND_CALL_CENTER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_LIBERACAO         DATE,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NR_SEQ_MODELO        NUMBER(10)               NOT NULL,
  NR_SEQ_ATENDIMENTO   NUMBER(10),
  NR_SEQ_EVENTO_ATEND  NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_HOUVE_RESPOSTA    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUEATCC_MODELOT_FK_I ON TASY.QUEST_ATEND_CALL_CENTER
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEATCC_MODELOT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUEATCC_PESFISI_FK_I ON TASY.QUEST_ATEND_CALL_CENTER
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUEATCC_PK ON TASY.QUEST_ATEND_CALL_CENTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEATCC_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUEATCC_PLSASEV_FK_I ON TASY.QUEST_ATEND_CALL_CENTER
(NR_SEQ_EVENTO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEATCC_PLSASEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUEATCC_PLSATEN_FK_I ON TASY.QUEST_ATEND_CALL_CENTER
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEATCC_PLSATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUEST_ATEND_CALL_CENTER ADD (
  CONSTRAINT QUEATCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUEST_ATEND_CALL_CENTER ADD (
  CONSTRAINT QUEATCC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUEATCC_MODELOT_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MODELO (NR_SEQUENCIA),
  CONSTRAINT QUEATCC_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT QUEATCC_PLSASEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_ATEND) 
 REFERENCES TASY.PLS_ATENDIMENTO_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUEST_ATEND_CALL_CENTER TO NIVEL_1;

