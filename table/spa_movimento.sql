ALTER TABLE TASY.SPA_MOVIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_MOVIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_MOVIMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SPA            NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CLASSIFICACAO      NUMBER(5)               NOT NULL,
  NR_ATENDIMENTO        NUMBER(10),
  NR_INTERNO_CONTA      NUMBER(10),
  NR_TITULO             NUMBER(10),
  NR_NOTA_FISCAL        VARCHAR2(255 BYTE),
  NR_SEQ_NF             NUMBER(10),
  NR_SEQ_PROTOCOLO      NUMBER(10),
  CD_CONVENIO           NUMBER(5),
  NR_SEQ_PERDA          NUMBER(10),
  DT_CONTABIL           DATE,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_PESSOA_JURIDICA    VARCHAR2(14 BYTE),
  NR_SEQ_NOTA_CREDITO   NUMBER(10),
  NR_ADIANTAMENTO       NUMBER(10),
  NR_SEQ_CHEQUE         NUMBER(10),
  NR_SEQ_PERDA_CTA_REC  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SPAMOVT_ADIANTA_FK_I ON TASY.SPA_MOVIMENTO
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_CHEQUES_FK_I ON TASY.SPA_MOVIMENTO
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_I1 ON TASY.SPA_MOVIMENTO
(NR_INTERNO_CONTA, CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_PESFISI_FK_I ON TASY.SPA_MOVIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_PESJURI_FK_I ON TASY.SPA_MOVIMENTO
(CD_PESSOA_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SPAMOVT_PK ON TASY.SPA_MOVIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPAMOVT_PK
  MONITORING USAGE;


CREATE INDEX TASY.SPAMOVT_PRDACRE_FK_I ON TASY.SPA_MOVIMENTO
(NR_SEQ_PERDA_CTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_PREFATPE_FK_I ON TASY.SPA_MOVIMENTO
(NR_SEQ_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAMOVT_SPA_FK_I ON TASY.SPA_MOVIMENTO
(NR_SEQ_SPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPAMOVT_SPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.spa_movimento_update
before update ON TASY.SPA_MOVIMENTO for each row
declare

cd_convenio_w	number(5,0);

begin

if	(:new.cd_classificacao = 1) then

	select	max(obter_convenio_atendimento(:new.nr_atendimento))
	into	cd_convenio_w
	from	dual;

elsif	(:new.cd_classificacao = 2) then

	select	max(cd_convenio_parametro)
	into	cd_convenio_w
	from	conta_paciente
	where	nr_interno_conta = :new.nr_interno_conta;

elsif	(:new.cd_classificacao = 3) then

	select	max(obter_convenio_nf(nr_sequencia))
	into	cd_convenio_w
	from	nota_fiscal
	where	nr_sequencia = :new.nr_seq_nf;

elsif	(:new.cd_classificacao = 4) then

	select	max(Obter_Convenio_Tit_Rec(nr_titulo))
	into	cd_convenio_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;

elsif	(:new.cd_classificacao = 5) then

	select	max(cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio
	where	nr_seq_protocolo = :new.nr_seq_protocolo;

end if;

if	(nvl(cd_convenio_w,0) <> 0) and
	(nvl(:new.cd_convenio,0) <> nvl(cd_convenio_w,0)) then
	:new.cd_convenio	:= cd_convenio_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.spa_movimento_insert
before insert ON TASY.SPA_MOVIMENTO for each row
declare

cd_convenio_w	number(5,0);

begin

if	(:new.cd_classificacao = 1) then

	select	max(obter_convenio_atendimento(:new.nr_atendimento))
	into	cd_convenio_w
	from	dual;

elsif	(:new.cd_classificacao = 2) then

	select	max(cd_convenio_parametro)
	into	cd_convenio_w
	from	conta_paciente
	where	nr_interno_conta = :new.nr_interno_conta;

elsif	(:new.cd_classificacao = 3) then

	select	max(obter_convenio_nf(nr_sequencia))
	into	cd_convenio_w
	from	nota_fiscal
	where	nr_sequencia = :new.nr_seq_nf;

elsif	(:new.cd_classificacao = 4) then

	select	max(Obter_Convenio_Tit_Rec(nr_titulo))
	into	cd_convenio_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;

elsif	(:new.cd_classificacao = 5) then

	select	max(cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio
	where	nr_seq_protocolo = :new.nr_seq_protocolo;

end if;

if	(nvl(cd_convenio_w,0) <> 0) then
	:new.cd_convenio	:= cd_convenio_w;
end if;

end;
/


ALTER TABLE TASY.SPA_MOVIMENTO ADD (
  CONSTRAINT SPAMOVT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_MOVIMENTO ADD (
  CONSTRAINT SPAMOVT_SPA_FK 
 FOREIGN KEY (NR_SEQ_SPA) 
 REFERENCES TASY.SPA (NR_SEQUENCIA),
  CONSTRAINT SPAMOVT_PREFATPE_FK 
 FOREIGN KEY (NR_SEQ_PERDA) 
 REFERENCES TASY.PRE_FATUR_PERDA (NR_SEQUENCIA),
  CONSTRAINT SPAMOVT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SPAMOVT_PESJURI_FK 
 FOREIGN KEY (CD_PESSOA_JURIDICA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SPAMOVT_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT SPAMOVT_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE),
  CONSTRAINT SPAMOVT_PRDACRE_FK 
 FOREIGN KEY (NR_SEQ_PERDA_CTA_REC) 
 REFERENCES TASY.PERDA_CONTAS_RECEBER (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_MOVIMENTO TO NIVEL_1;

