ALTER TABLE TASY.PLS_TED_CONTA_DADOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TED_CONTA_DADOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TED_CONTA_DADOS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_TED_BENEF  NUMBER(20),
  NR_SEQ_CONTA           NUMBER(20),
  NR_SEQ_PREST_EXEC      NUMBER(20),
  CD_PRESTADOR_ANT       VARCHAR2(20 BYTE),
  DS_PREST_EXEC          VARCHAR2(40 BYTE),
  IE_TIPO_GUIA           VARCHAR2(2 BYTE),
  CD_CID                 VARCHAR2(10 BYTE),
  IE_TIPO_ATEND          NUMBER(3),
  CD_TIPO_ACIDENTE       NUMBER(1),
  CD_ITEM                NUMBER(20),
  DS_ITEM                VARCHAR2(250 BYTE),
  VL_ITEM                NUMBER(10,2),
  DT_ITEM                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTEDCTAD_ESTABEL_FK_I ON TASY.PLS_TED_CONTA_DADOS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSTEDCTAD_PK ON TASY.PLS_TED_CONTA_DADOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTEDCTAD_PLSCOME_FK_I ON TASY.PLS_TED_CONTA_DADOS
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTEDCTAD_PLSPRES_FK_I ON TASY.PLS_TED_CONTA_DADOS
(NR_SEQ_PREST_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTEDCTAD_PLSTEDCTAB_FK_I ON TASY.PLS_TED_CONTA_DADOS
(NR_SEQ_LOTE_TED_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TED_CONTA_DADOS ADD (
  CONSTRAINT PLSTEDCTAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_TED_CONTA_DADOS ADD (
  CONSTRAINT PLSTEDCTAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTEDCTAD_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSTEDCTAD_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PREST_EXEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTEDCTAD_PLSTEDCTAB_FK 
 FOREIGN KEY (NR_SEQ_LOTE_TED_BENEF) 
 REFERENCES TASY.PLS_TED_CONTA_BENEF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TED_CONTA_DADOS TO NIVEL_1;

