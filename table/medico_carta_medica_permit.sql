ALTER TABLE TASY.MEDICO_CARTA_MEDICA_PERMIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICO_CARTA_MEDICA_PERMIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICO_CARTA_MEDICA_PERMIT
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_MEDICO                   VARCHAR2(10 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_LIBERACAO                DATE,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  DT_INICIO                   DATE,
  DT_FIM                      DATE,
  NR_SEQ_MEDICO_CARTA_MEDICA  NUMBER(10),
  IE_APROVACAO_DUPLA          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MECAMEP_MECAMED_FK_I ON TASY.MEDICO_CARTA_MEDICA_PERMIT
(NR_SEQ_MEDICO_CARTA_MEDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MECAMEP_PESFISI_FK_I ON TASY.MEDICO_CARTA_MEDICA_PERMIT
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MECAMEP_PK ON TASY.MEDICO_CARTA_MEDICA_PERMIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.med_carta_medica_permit_update
before insert or update ON TASY.MEDICO_CARTA_MEDICA_PERMIT for each row
declare

begin

	if (:new.dt_inicio is not null and
		:new.dt_fim is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1043579);
	elsif (:new.dt_inicio is null and
			:new.dt_fim is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1043580);
	elsif (:new.dt_inicio is not null and
			:new.dt_fim is not null) then
			if (:new.dt_inicio > :new.dt_fim) then
				Wheb_mensagem_pck.exibir_mensagem_abort(1043581);
			end if;
	end if;

end;
/


ALTER TABLE TASY.MEDICO_CARTA_MEDICA_PERMIT ADD (
  CONSTRAINT MECAMEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDICO_CARTA_MEDICA_PERMIT ADD (
  CONSTRAINT MECAMEP_MECAMED_FK 
 FOREIGN KEY (NR_SEQ_MEDICO_CARTA_MEDICA) 
 REFERENCES TASY.MEDICO_CARTA_MEDICA (NR_SEQUENCIA),
  CONSTRAINT MECAMEP_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MEDICO_CARTA_MEDICA_PERMIT TO NIVEL_1;

