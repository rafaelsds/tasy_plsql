ALTER TABLE TASY.LAB_IMPRESSAO_MAPA_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_IMPRESSAO_MAPA_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_IMPRESSAO_MAPA_LOG
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_PRESCRICAO   NUMBER(10)                    NOT NULL,
  NR_SEQ_PRESCR   NUMBER(10)                    NOT NULL,
  IE_TIPO         VARCHAR2(2 BYTE)              NOT NULL,
  CD_MAPA         NUMBER(10),
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAIMPML_I1 ON TASY.LAB_IMPRESSAO_MAPA_LOG
(CD_MAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAIMPML_PK ON TASY.LAB_IMPRESSAO_MAPA_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAIMPML_PRESPRO_FK_I ON TASY.LAB_IMPRESSAO_MAPA_LOG
(NR_PRESCRICAO, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAB_IMPRESSAO_MAPA_LOG ADD (
  CONSTRAINT LAIMPML_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_IMPRESSAO_MAPA_LOG ADD (
  CONSTRAINT LAIMPML_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LAB_IMPRESSAO_MAPA_LOG TO NIVEL_1;

