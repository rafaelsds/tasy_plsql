ALTER TABLE TASY.PLS_PRECO_GRUPO_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRECO_GRUPO_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRECO_GRUPO_SERVICO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DS_GRUPO                     VARCHAR2(255 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_REGRA_PRECO               VARCHAR2(1 BYTE),
  IE_CRITERIO_HORARIO          VARCHAR2(1 BYTE),
  IE_OC_CONTA_MEDICA           VARCHAR2(1 BYTE),
  IE_OC_CONTA_MEDICA_WEB       VARCHAR2(1 BYTE),
  IE_CAD_PRESTADOR             VARCHAR2(1 BYTE),
  IE_QTD_EXECUCAO              VARCHAR2(1 BYTE),
  IE_VIA_ACESSO_OBRIG          VARCHAR2(1 BYTE),
  IE_LIB_VALOR_CONTA           VARCHAR2(1 BYTE),
  IE_TIPO_GUIA                 VARCHAR2(1 BYTE),
  IE_REGRA_COLETA              VARCHAR2(1 BYTE),
  IE_AUTOGERADO                VARCHAR2(1 BYTE),
  IE_TAXA_ADM                  VARCHAR2(1 BYTE),
  IE_COBERTURA_CONTRATO        VARCHAR2(1 BYTE),
  DT_REFERENCIA                DATE,
  IE_ITEM_ASSISTENCIAL_SIP     VARCHAR2(1 BYTE),
  IE_VIA_ACESSO_PROCEDIMENTO   VARCHAR2(1 BYTE),
  IE_COMPLEMENTO_GUIA          VARCHAR2(1 BYTE),
  IE_JUSTIFICATIVA_AUTOMATICA  VARCHAR2(1 BYTE),
  IE_REGRA_PROCEDIMENTO        VARCHAR2(1 BYTE),
  IE_GUIA_MEDICO               VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRGS_ESTABEL_FK_I ON TASY.PLS_PRECO_GRUPO_SERVICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRGS_PK ON TASY.PLS_PRECO_GRUPO_SERVICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_preco_grupo_servico_atual
before insert or update or delete ON TASY.PLS_PRECO_GRUPO_SERVICO for each row
declare
begin

-- se for atualiza��o s� manda alterar se mudar o status.
-- os outros campos da tabela n�o influenciam em nada
if	(updating) then

	if	(:new.ie_situacao != :old.ie_situacao) then

		pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_SERVICO_TM',
									wheb_usuario_pck.get_nm_usuario,
									'PLS_PRECO_GRUPO_SERVICO_ATUAL',
									'nr_seq_grupo_p=' || :new.nr_sequencia);
	end if;

-- se for exclus�o apenas informa para fazer somente a exclus�o
elsif	(deleting) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_SERVICO_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_PRECO_GRUPO_SERVICO_ATUAL',
								'nr_seq_grupo_p=' || :old.nr_sequencia);

-- inser��o cria os novos registros
else
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_SERVICO_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_PRECO_GRUPO_SERVICO_ATUAL',
								'nr_seq_grupo_p=' || :new.nr_sequencia);
end if;

end pls_preco_grupo_servico_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PRECO_GRUPO_SERVICO_tp  after update ON TASY.PLS_PRECO_GRUPO_SERVICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_GUIA_MEDICO,1,4000),substr(:new.IE_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICATIVA_AUTOMATICA,1,4000),substr(:new.IE_JUSTIFICATIVA_AUTOMATICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICATIVA_AUTOMATICA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_GRUPO,1,4000),substr(:new.DS_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRUPO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_PRECO,1,4000),substr(:new.IE_REGRA_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_PRECO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRITERIO_HORARIO,1,4000),substr(:new.IE_CRITERIO_HORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRITERIO_HORARIO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OC_CONTA_MEDICA,1,4000),substr(:new.IE_OC_CONTA_MEDICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OC_CONTA_MEDICA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OC_CONTA_MEDICA_WEB,1,4000),substr(:new.IE_OC_CONTA_MEDICA_WEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_OC_CONTA_MEDICA_WEB',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAD_PRESTADOR,1,4000),substr(:new.IE_CAD_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAD_PRESTADOR',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QTD_EXECUCAO,1,4000),substr(:new.IE_QTD_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_QTD_EXECUCAO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_ACESSO_OBRIG,1,4000),substr(:new.IE_VIA_ACESSO_OBRIG,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_ACESSO_OBRIG',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIB_VALOR_CONTA,1,4000),substr(:new.IE_LIB_VALOR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIB_VALOR_CONTA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TAXA_ADM,1,4000),substr(:new.IE_TAXA_ADM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAXA_ADM',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTOGERADO,1,4000),substr(:new.IE_AUTOGERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTOGERADO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_COLETA,1,4000),substr(:new.IE_REGRA_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_COLETA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REFERENCIA,1,4000),substr(:new.DT_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_REFERENCIA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ITEM_ASSISTENCIAL_SIP,1,4000),substr(:new.IE_ITEM_ASSISTENCIAL_SIP,1,4000),:new.nm_usuario,nr_seq_w,'IE_ITEM_ASSISTENCIAL_SIP',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_ACESSO_PROCEDIMENTO,1,4000),substr(:new.IE_VIA_ACESSO_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_ACESSO_PROCEDIMENTO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPLEMENTO_GUIA,1,4000),substr(:new.IE_COMPLEMENTO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPLEMENTO_GUIA',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_PROCEDIMENTO,1,4000),substr(:new.IE_REGRA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_PROCEDIMENTO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBERTURA_CONTRATO,1,4000),substr(:new.IE_COBERTURA_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBERTURA_CONTRATO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_SERVICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PRECO_GRUPO_SERVICO ADD (
  CONSTRAINT PLSPRGS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRECO_GRUPO_SERVICO ADD (
  CONSTRAINT PLSPRGS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_PRECO_GRUPO_SERVICO TO NIVEL_1;

