ALTER TABLE TASY.XML_PROJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.XML_PROJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.XML_PROJETO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_PROJETO            VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_CABECALHO          VARCHAR2(2000 BYTE),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  IE_TIPO_COMPLEXO      VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_PROJ_COMPLEXO  NUMBER(10),
  IE_HOSP_PLS           VARCHAR2(1 BYTE)        NOT NULL,
  DS_VERSAO             VARCHAR2(20 BYTE),
  IE_TISS               VARCHAR2(1 BYTE)        NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_GERAR_QUEBRA       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.XMLPROJ_PK ON TASY.XML_PROJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLPROJ_XMLPROJ_FK_I ON TASY.XML_PROJETO
(NR_SEQ_PROJ_COMPLEXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.XML_PROJETO ADD (
  CONSTRAINT XMLPROJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.XML_PROJETO ADD (
  CONSTRAINT XMLPROJ_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_COMPLEXO) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.XML_PROJETO TO NIVEL_1;

