ALTER TABLE TASY.ESCALA_SEPSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SEPSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SEPSE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PONTUACAO           NUMBER(3),
  DT_LIBERACAO           DATE,
  NR_SEQ_EVENTO          NUMBER(10),
  NR_SEQ_SV              NUMBER(10),
  NM_USUARIO_STATUS      VARCHAR2(15 BYTE),
  IE_STATUS_SEPSIS       VARCHAR2(3 BYTE),
  DT_LIBERACAO_STATUS    DATE,
  DS_JUSTIF_STATUS       VARCHAR2(255 BYTE),
  IE_PADRAO_VARIAVEL     VARCHAR2(1 BYTE),
  NR_SEQ_ORIGEM          NUMBER(10),
  NM_TABELA              VARCHAR2(50 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  IE_FIM_VIDA            VARCHAR2(1 BYTE),
  IE_DOENCAS_ATIPICAS    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSEPS_ATEPACI_FK_I ON TASY.ESCALA_SEPSE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSEPS_I1 ON TASY.ESCALA_SEPSE
(NR_SEQ_SV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSEPS_PESFISI_FK_I ON TASY.ESCALA_SEPSE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSEPS_PK ON TASY.ESCALA_SEPSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSEPS_QUAEVPA_FK_I ON TASY.ESCALA_SEPSE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_pont_upd
before update ON TASY.ESCALA_SEPSE for each row
declare
PRAGMA AUTONOMOUS_TRANSACTION;

qt_anos_paciente_w          number(10);
nr_seq_pendencia_sepse_w    number(10):=6;
ie_versao_sepse_w           varchar2(1):='1';

    procedure hsj_alterar_status_escala_seps(    nr_seq_escala_p        number,
                                        ie_status_p            varchar2,
                                        nm_usuario_p        varchar2,
                                        ds_justificativa_p    varchar2 default null,
                                        nr_atendimento_p    number default 0,
                                        ie_opcao_p          varchar2 default 'N',
                                        ie_tipo_descarte_p varchar2 default '') is

    nr_atendimento_w        Number(10);
    ie_status_w                Varchar2(3);
    dt_status_ant_w         date;
    dt_status_atual_w       date;
    qt_intervalo_status_w   number(10,4);
    nr_regras_atendidas_w   varchar2(2000);
    ie_fim_vida_w           varchar2(1);
    ie_doencas_atipicas_w   varchar2(1);

    begin

    if    (nr_seq_escala_p > 0) then

        ie_status_w := ie_status_p;

        if(ie_status_p = 'SD')then
            ie_status_w := 'SN';
        end if;

        :new.ie_status_sepsis := ie_status_w;
        :new.nm_usuario_status := nm_usuario_p;
        

        if(ie_status_p in ('RC','RF','RE','RD','SC','SN','D','CS'))then

            :new.dt_liberacao_status := sysdate;
            :new.ds_justif_status := ds_justificativa_p;

        end if;

        if (ie_tipo_descarte_p = 'FV') then
            :new.ie_fim_vida := 'S';
        end if;

        if (ie_tipo_descarte_p = 'DA') then
            :new.ie_doencas_atipicas := 'S';
        end if;

        dt_status_atual_w := sysdate;


        select  nvl(max(x.dt_atualizacao),dt_status_atual_w)
        into    dt_status_ant_w
        from    escala_sepse_status x
        where   x.nr_seq_escala = nr_seq_escala_p;

        qt_intervalo_status_w := round(dt_status_atual_w - dt_status_ant_w, 4);

        insert into escala_sepse_status(    nr_sequencia,
                                            dt_atualizacao,
                                            nm_usuario,
                                            dt_atualizacao_nrec,
                                            nm_usuario_nrec,
                                            ie_status_sepsis,
                                            nr_seq_escala,
                                            dt_avaliacao,
                                            ds_justif_status,
                                            qt_intervalo_status)
                                values(        escala_sepse_status_seq.nextval,
                                            dt_status_atual_w,
                                        nm_usuario_p,
                                        dt_status_atual_w,
                                        nm_usuario_p,
                                        ie_status_w,
                                        nr_seq_escala_p,
                                        dt_status_atual_w,
                                        ds_justificativa_p,
                                        qt_intervalo_status_w);


        If     (:new.nr_atendimento > 0 and ie_status_w = 'SE') then

            gqa_liberacao_escala(:new.nr_atendimento, nr_seq_escala_p, nm_usuario_p, 'ESCALA_SEPSE', nr_regras_atendidas_w, ie_status_w);
            gera_protocolo_assistencial(:new.nr_atendimento, nm_usuario_p);

        end if;

    end if;
    end hsj_alterar_status_escala_seps;
    
    
    procedure hsj_verificar_se_sepse_aberto2(  nr_sequencia_p number, nr_atendimento_p number, nm_usuario_p Varchar2, ie_versao_sepse_p varchar2 default '1', nr_seq_pendencia_sepse_p number default 6) is

    qt_var_w               number(10);
    qt_disfuncao_w         number(10);
    qt_suspeita_w          number(10);


    begin

        select  count(1)
        into    qt_disfuncao_w
        from    escala_sepse_item s
        where   nr_seq_escala = nr_sequencia_p
        and     ie_resultado = 'S'
        and exists
            (select  1
            from SEPSE_ATRIBUTO a
            join SEPSE_ATRIBUTO_cliente b on(b.nr_seq_atributo = a.nr_sequencia)
            where  nvl(ie_versao,'1') = ie_versao_sepse_p
            and nvl(ie_tipo,'A') in('A','G')
            and a.nr_sequencia = s.nr_seq_atributo );                                       
                

        select  count(1)
        into    qt_suspeita_w
        from    escala_sepse_item s
        where   nr_seq_escala = nr_sequencia_p
        and     ie_resultado = 'S'
        and exists
            (select  1
            from SEPSE_ATRIBUTO a
            join SEPSE_ATRIBUTO_cliente b on(b.nr_seq_atributo = a.nr_sequencia)
            where  nvl(ie_versao,'1') = ie_versao_sepse_p
            and nvl(ie_tipo,'A') in('A','S')
            and a.nr_sequencia = s.nr_seq_atributo );

        select  count(1)
        into    qt_var_w
        from    gqa_pendencia_regra a,
                gqa_pendencia       b
        where   b.nr_sequencia = a.nr_seq_pendencia
        and     a.nr_seq_pendencia = nr_seq_pendencia_sepse_p
        and     b.ie_situacao = 'A'
        and     a.ie_regra_sepse = 'SE'
        and     a.ie_situacao = 'A'
        and     qt_suspeita_w >= nvl(a.qt_var_suspeita,0)
        and     qt_disfuncao_w >= nvl(a.qt_var_confirmada,0)
        and     a.qt_var_suspeita is not null
        and     a.qt_var_confirmada is not null
        and     (a.cd_setor_atendimento = obter_setor_atendimento(nr_atendimento_p) or a.cd_setor_atendimento is null)
        and     GQA_regra_complementar(nm_usuario_p,nr_atendimento_p,a.nr_sequencia)='X';

        if( qt_var_w > 0 ) then
            hsj_alterar_status_escala_seps(nr_sequencia_p,'SE',nm_usuario_p,'');
        end if;

    end hsj_verificar_se_sepse_aberto2;

begin


    if(/*:new.qt_pontuacao != nvl(:old.qt_pontuacao,0)*/ :new.dt_liberacao is not null and :old.dt_liberacao is null and  :new.ie_padrao_variavel = 'P' and :new.nr_seq_origem is null)then
    
        qt_anos_paciente_w := to_number(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento, 'C'), sysdate, 'A'));
        
        if qt_anos_paciente_w >= 14 then -- SEPSE Adulto
            nr_seq_pendencia_sepse_w:= 6;
            ie_versao_sepse_w:='1';
        else -- SEPSE Pedi�trica
            nr_seq_pendencia_sepse_w:= 26;
            ie_versao_sepse_w:='2';
        end if;
        
        hsj_verificar_se_sepse_aberto2(:new.nr_sequencia, :new.nr_atendimento, :new.nm_usuario, ie_versao_sepse_w, nr_seq_pendencia_sepse_w);
        
        
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_sepse_atual
before update ON TASY.ESCALA_SEPSE for each row
declare
    nr_seq_evento_w		number(10);

begin

if (inserting) then
  :new.ie_padrao_variavel := obter_padrao_deflagador_sepse;
end if;

if	(:old.dt_liberacao_status is null) and
	(:new.dt_liberacao_status is not null) and
	(:new.ie_status_sepsis in ('SC','RC')) then
	gerar_evento_sepse(:new.nr_atendimento,nr_seq_evento_w,:new.nr_sequencia,:new.nm_usuario_status);

	:new.nr_seq_evento := nr_seq_evento_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_evol_upd
after update ON TASY.ESCALA_SEPSE for each row
declare

jobno number;
ds_evolucao_w long;

cursor c_deflag is
    select substr(obter_desc_atributo_sepse(nr_seq_atributo),1,255)ds
    from escala_sepse_item a
    where nr_seq_escala = :new.nr_sequencia
    and ie_resultado = 'S';


begin


    if(nvl(:old.ie_status_sepsis,'X') not in('SM','RN') and :new.ie_status_sepsis in('SM','RN') )then
        
        ds_evolucao_w:= 'Evolu��o Protocolo Sepse - Enfermeiro

Paciente apresentou os seguintes deflagradores (sinais de sepse)
';
    
        for v01 in c_deflag loop
            ds_evolucao_w:= ds_evolucao_w||'
'||v01.ds;
        end loop;

        ds_evolucao_w:= ds_evolucao_w||'

Avalio paciente e imediatamente comunico m�dico
Dr.:  
Hor�rio: 
O mesmo informou que: ';

        hsj_gerar_evolucao(12, :new.nr_atendimento, ds_evolucao_w, 'SEP', nvl(obter_usuario_ativo,:new.nm_usuario));
                  
    end if;
    
end hsj_escala_sepse_evol_upd;
/


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_upd
BEFORE UPDATE ON TASY.ESCALA_SEPSE FOR EACH ROW
declare
PRAGMA AUTONOMOUS_TRANSACTION;

qt_horas_w              number(10);
qt_reg_w                number(10);
qt_regra_w              number(10);
cd_setor_atendimento_w  number(10);
qt_disfuncao_w          number(10);
qt_suspeita_w           number(10);
nr_regras_atendidas_w   varchar2(255);
qt_anos_paciente_w      number(10);

nr_seq_pendencia_sepse_w number(10):=6;
ie_versao_sepse_w       varchar2(1):='1';
ie_status_sepsis_novo_w varchar2(10);

begin

    /*
        Objetivo: Quando houver escala com status "Suspeita Confirmada - Pendente A��o M�dica" gerada em at� X horas(Conforme regrs), n�o dever� alterar o status
        de "Suspeita - Pendente Confirma��o" para "Suspeita Confirmada".
        OBS: Definido em Reuni�o dia 26/02/2019 pela dire��o
    */
    
    qt_anos_paciente_w := to_number(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento, 'C'), sysdate, 'A'));
    
    if qt_anos_paciente_w >= 14 then -- SEPSE Adulto
    
        nr_seq_pendencia_sepse_w:= hsj_obter_tipo_sepse(:new.nr_atendimento, 'PM') ;
        ie_versao_sepse_w:='1';
        
    else -- SEPSE Pedi�trica
    
        nr_seq_pendencia_sepse_w:= hsj_obter_tipo_sepse(:new.nr_atendimento, 'PM') ;
        ie_versao_sepse_w:='2';
        
    end if;
    
    if(nvl(:old.ie_status_sepsis,'X') not in('SM','RN') and :new.ie_status_sepsis in('SM','RN') )then

        select max(qt_horas)
        into qt_horas_w
        from sepse_horas_status
        where ie_status = 'SM';
        
        select count(1)
        into   qt_reg_w
        from   escala_sepse
        where  nr_atendimento = :new.nr_atendimento
        and    dt_inativacao is null
        and    nvl(ie_situacao,'A') = 'A'
        and    ie_status_sepsis in('SM','RN') 
        and    dt_liberacao between (sysdate  - (1/24 * qt_horas_w)) and sysdate;  

        if(qt_reg_w > 0)then
            
            :new.ie_status_sepsis := 'SN';
            :new.ds_justif_status := 'Existe Suspeita Confirmada nas �ltimas '|| qt_horas_w ||' horas - Status Autom�tico';
            :new.dt_liberacao_status := sysdate;
            :new.nm_usuario_status := wheb_usuario_pck.get_nm_usuario;
            :new.dt_atualizacao := sysdate;
            :new.nm_usuario := wheb_usuario_pck.get_nm_usuario;
            
        else
            
            ie_status_sepsis_novo_w:= :new.ie_status_sepsis;
        
            select  count(1)
            into    qt_disfuncao_w
            from    escala_sepse_item s
            where   nr_seq_escala = :new.nr_sequencia
            and     ie_resultado = 'S'
            and exists
                (select  1
                from SEPSE_ATRIBUTO a
                join SEPSE_ATRIBUTO_cliente b on(b.nr_seq_atributo = a.nr_sequencia)
                where  nvl(ie_versao,'1') = ie_versao_sepse_w
                and nvl(ie_tipo,'A') in('A','G')
                and a.nr_sequencia = s.nr_seq_atributo ); 
             
            if(:new.ie_status_sepsis = 'RN' and qt_disfuncao_w = 0)then -- Corrigir o status para suspeita sirs confirmada, caso os deflagradores marcados forem apenas SIRS (Sem DO)
                :new.ie_status_sepsis := 'SM';
                ie_status_sepsis_novo_w:= 'SM';                    
            end if;
                             
             hsj_gqa_liberacao_escala(:new.nr_atendimento, :new.nr_sequencia, wheb_usuario_pck.get_nm_usuario, 'ESCALA_SEPSE', nr_regras_atendidas_w, ie_status_sepsis_novo_w);                
     
            :new.dt_liberacao_status:=sysdate;
            
        end if;
            
            
    /*  RAFAELSDS - 27/02/2019
        A partir da vers�o 1740 a function obter_se_gera_sepsis verifica por data de libera��o para considerar o tempo para gerar ou n�o nova escala conforme regra da tabela sepse_horas_status.
        Ap�s atualizar status da escala para "Suspeita Sepse" j� inserir data de libera��o de status para que ent�o n�o fique gerando novas escalas enquanto tiver uma como Suspeita Sepse (Lib ou n�o).
            
        OBS: Ficar atento quando inserir mais registros na tabela sepse_horas_status e n�o querer que gere escala durante o per�odo da regra. A escala anterior dever� conter dt_liberacao_status.
            
            
            
        RAFAELSDS - 05/08/2019
        N�o ir� gerar escala sepse proveniente de SV, caso existir alguma escala ativa registrada nos ultimos 15 minutos
    */    
    elsif(nvl(:old.ie_status_sepsis,'X') != 'SE' and :new.ie_status_sepsis = 'SE' and :new.nr_seq_sv is not null)then

        if(:new.nr_seq_sv is not null)then
            
            select max(cd_setor_atendimento)
            into cd_setor_atendimento_w
            from ATENDIMENTO_SINAL_VITAL
            where nr_sequencia = :new.nr_seq_sv;
                
            select count(*)
            into qt_regra_w
            from GQA_PENDENCIA_REGRA a
            where a.nr_seq_pendencia = nr_seq_pendencia_sepse_w
            and a.ie_situacao = 'A'
            and (a.cd_setor_atendimento = cd_setor_atendimento_w
            or a.cd_setor_atendimento is null);
                
            select count(*)
            into qt_reg_w
            from escala_sepse
            where nr_sequencia != :new.nr_sequencia
            and nr_atendimento = :new.nr_atendimento
            and ie_situacao = 'A'
            and dt_inativacao is null
            and dt_liberacao is not null
            and (ie_status_sepsis = 'SE' or dt_liberacao >= sysdate - ((1 / 24) / 60) * 15 );
                
            if(nvl(qt_regra_w,0) = 0 or qt_reg_w > 0)then
                :new.ie_status_sepsis:=null;
            end if;
                       
        end if;
       
    end if;

    :new.dt_atualizacao:= sysdate;
    :new.nm_usuario:= nvl(obter_usuario_ativo,:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_n_lib_ins
BEFORE INSERT ON TASY.ESCALA_SEPSE FOR EACH ROW
declare
PRAGMA AUTONOMOUS_TRANSACTION;

seq_w number(10):=0;
seq_enf_w number(10):=0;
nr_atendimento_w number(10):=0;
nm_profissional_w varchar2(255);
dt_avaliacao_w date;

begin

    if(:new.nr_seq_origem is null and :new.ie_padrao_variavel = 'P')then    
    
        
    
        select  nvl(max(nr_sequencia),0)
        into    seq_w
        from escala_sepse 
        where nr_atendimento = :new.nr_atendimento
        and dt_avaliacao < :new.dt_atualizacao
        and nr_sequencia != :new.nr_sequencia
        and nm_usuario = :new.nm_usuario
        and ie_situacao = 'A'
        and (dt_liberacao is null or ie_status_sepsis = 'SE');
    
        select  nvl(max(nr_sequencia),0)
        into    seq_enf_w
        from escala_sepse 
        where nr_atendimento = :new.nr_atendimento
        and dt_avaliacao < :new.dt_atualizacao
        and nr_sequencia != :new.nr_sequencia
        and dt_liberacao is not null 
        and ie_situacao = 'A'
        and ie_status_sepsis = 'SE';
    
        if(seq_w > 0)then
        
            select  initcap(substr(obter_nome_pf(cd_profissional),1,255))ds, 
                    dt_avaliacao
            into    nm_profissional_w,
                    dt_avaliacao_w
            from escala_sepse
            where nr_sequencia = seq_w;
            
        
            hsj_gerar_log('Existe escala n�o liberada! Ser� necess�rio liber�-la ou exclu�-la!
Dt. Avalia��o: '||to_char(dt_avaliacao_w,'dd/mm/yyyy hh24:mi:ss')||'
Profissional: '||nm_profissional_w);
        
        elsif(seq_enf_w > 0)then
        
            select  nr_atendimento
            into    nr_atendimento_w
            from escala_sepse
            where nr_sequencia = seq_enf_w;
        
            hsj_gerar_log('Atendimento: '|| nr_atendimento_w||'     
J� existe uma escala pendente de avalia��o enfermagem!
Ser� necess�rio fechar e abrir o prontu�rio paciente para visualiza��o da mesma!');
        
        end if;
              
        
        :new.nm_usuario_status := nvl(obter_usuario_ativo,'tasy'); --Registra usu�rio e data de altera��o de status, para travar a altera��o manual de deflagradores
        :new.dt_liberacao_status := sysdate;
        
    end if;    


end hsj_escala_sepse_n_lib_ins;
/


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_aftins
after INSERT ON TASY.ESCALA_SEPSE FOR EACH ROW
declare

qt_anos_paciente_w  number(10);
qt_pontos_itens_w   number(10);
ds_comando_w        varchar2(4000);


nr_sequencia_w number(10);
nr_atendimento_w number(10);
nm_usuario_w varchar2(50);
ie_commit_w varchar2(10):='N';

begin

    /*
        Inserir escala e atendimento em uma tabela auxiliar
    */

    insert into hsj_escala_sepse_atend
    (
        nr_seq_escala, 
        nr_atendimento,
        ie_padrao_variavel,
        nr_seq_origem
    )
    values
    (
        :new.nr_sequencia,
        :new.nr_atendimento,
        :new.ie_padrao_variavel,
        :new.nr_seq_origem
    );

    
    /*
        Caso o paciente for pediatrico, ir� gerar itens da escala de SEPSE manualmente, pois dever� gerar os deflagradores pedi�tricos.
    */
    
    qt_anos_paciente_w := to_number(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento, 'C'), sysdate, 'A'));
    
    if :new.nr_seq_origem is null and :new.ie_padrao_variavel = 'P' then


        if qt_anos_paciente_w <= 13 then 
            nr_sequencia_w:= :new.nr_sequencia;
            nr_atendimento_w:= :new.nr_atendimento;
            nm_usuario_w:= :new.nm_usuario;

            ds_comando_w:= 'begin HSJ_GERAR_ESCALA_SEPSE_PED.hsj_gerar_itens_sepse_pck(:1, :2, :3, :4); end;';
            execute immediate ds_comando_w using nr_sequencia_w, nr_atendimento_w, nm_usuario_w, ie_commit_w;
        end if;
 
    end if;

end hsj_escala_sepse_aftins;
/


ALTER TABLE TASY.ESCALA_SEPSE ADD (
  CONSTRAINT ESCSEPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SEPSE ADD (
  CONSTRAINT ESCSEPS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSEPS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSEPS_QUAEVPA_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.QUA_EVENTO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_SEPSE TO NIVEL_1;

