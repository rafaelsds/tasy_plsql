ALTER TABLE TASY.W_INTEGRACAO_ERITEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTEGRACAO_ERITEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTEGRACAO_ERITEL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_RAMAL              NUMBER(10),
  IE_INTEGRADO          VARCHAR2(15 BYTE)       NOT NULL,
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE)       NOT NULL,
  CD_UNIDADE_COMPL      VARCHAR2(10 BYTE)       NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  IE_STATUS_ERITEL      NUMBER(10),
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WINTERI_ESTABEL_FK_I ON TASY.W_INTEGRACAO_ERITEL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTERI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WINTERI_PK ON TASY.W_INTEGRACAO_ERITEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTERI_PK
  MONITORING USAGE;


CREATE INDEX TASY.WINTERI_SETATEN_FK_I ON TASY.W_INTEGRACAO_ERITEL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTERI_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_INTEGRACAO_ERITEL ADD (
  CONSTRAINT WINTERI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_INTEGRACAO_ERITEL ADD (
  CONSTRAINT WINTERI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WINTERI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.W_INTEGRACAO_ERITEL TO NIVEL_1;

