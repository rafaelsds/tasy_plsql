DROP TABLE TASY.SAN_DERIVADO_DADOS_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_DERIVADO_DADOS_LAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DERIVADO      NUMBER(10)               NOT NULL,
  IE_HEMOGLOBINA       VARCHAR2(1 BYTE),
  IE_HEMATOCRITO       VARCHAR2(1 BYTE),
  IE_PLAQUETA          VARCHAR2(1 BYTE),
  IE_TAP               VARCHAR2(1 BYTE),
  IE_TAP_INR           VARCHAR2(1 BYTE),
  IE_TTPA              VARCHAR2(1 BYTE),
  IE_TTPA_REL          VARCHAR2(1 BYTE),
  IE_FIBRINOGENIO      VARCHAR2(1 BYTE),
  IE_ALBUMINA          VARCHAR2(1 BYTE),
  IE_CALCIO            VARCHAR2(1 BYTE),
  IE_MAGNESIO          VARCHAR2(1 BYTE),
  IE_BILIRRUBINA_DIR   VARCHAR2(1 BYTE),
  IE_BILIRRUBINA_IND   VARCHAR2(1 BYTE),
  IE_HEMOGLOBINA_S     VARCHAR2(1 BYTE),
  IE_LEUCOCITOS        VARCHAR2(1 BYTE),
  IE_COOMBS_DIRETO     VARCHAR2(1 BYTE),
  IE_COAGULOPATIA      VARCHAR2(1 BYTE),
  NM_USUARIO           VARCHAR2(100 BYTE)       NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(100 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANDDLA_SANDERI_FK_I ON TASY.SAN_DERIVADO_DADOS_LAB
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_DERIVADO_DADOS_LAB ADD (
  CONSTRAINT SANDDLA_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_DERIVADO_DADOS_LAB TO NIVEL_1;

