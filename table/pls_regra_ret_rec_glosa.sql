ALTER TABLE TASY.PLS_REGRA_RET_REC_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_RET_REC_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_RET_REC_GLOSA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_SEQ_PRESTADOR        NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR  NUMBER(10),
  IE_IDENT_PRESTADOR      VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRRRG_ESTABEL_FK_I ON TASY.PLS_REGRA_RET_REC_GLOSA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRRRG_PK ON TASY.PLS_REGRA_RET_REC_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRRG_PLSPRES_FK_I ON TASY.PLS_REGRA_RET_REC_GLOSA
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRRG_PLSPRGP_FK_I ON TASY.PLS_REGRA_RET_REC_GLOSA
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_ret_rec_glosa_atual
before insert or update ON TASY.PLS_REGRA_RET_REC_GLOSA for each row
declare

begin

-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_regra_ret_rec_glosa_atual;
/


ALTER TABLE TASY.PLS_REGRA_RET_REC_GLOSA ADD (
  CONSTRAINT PLSRRRG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_RET_REC_GLOSA ADD (
  CONSTRAINT PLSRRRG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRRRG_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRRRG_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_RET_REC_GLOSA TO NIVEL_1;

