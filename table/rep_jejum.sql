ALTER TABLE TASY.REP_JEJUM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REP_JEJUM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REP_JEJUM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_PRESCRICAO           NUMBER(14)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_TIPO             NUMBER(10)            NOT NULL,
  NR_SEQ_OBJETIVO         NUMBER(10),
  IE_INICIO               VARCHAR2(15 BYTE)     NOT NULL,
  DT_INICIO               DATE,
  DT_FIM                  DATE,
  DT_EVENTO               DATE,
  QT_MIN_ANT              NUMBER(5),
  QT_HORA_ANT             NUMBER(5),
  QT_MIN_DEPOIS           NUMBER(5),
  QT_HORA_DEPOIS          NUMBER(5),
  DS_EVENTO               VARCHAR2(255 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  IE_SUSPENSO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_REPETE_COPIA         VARCHAR2(1 BYTE)      NOT NULL,
  CD_PERFIL_ATIVO         NUMBER(5),
  NR_SEQ_ASSINATURA       NUMBER(10),
  DT_EMISSAO              DATE,
  NR_SEQ_ANTERIOR         NUMBER(15),
  IE_ESTENDIDO            VARCHAR2(1 BYTE),
  DT_EXTENSAO             DATE,
  NR_PRESCRICAO_ORIGINAL  NUMBER(15),
  NR_SEQ_DIETA_CPOE       NUMBER(10),
  CD_PROTOCOLO            NUMBER(10),
  NR_SEQ_PROTOCOLO        NUMBER(6),
  NR_SEQ_ASSINATURA_SUSP  NUMBER(15)            DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPJEJU_I1 ON TASY.REP_JEJUM
(NR_SEQ_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPJEJU_I1
  MONITORING USAGE;


CREATE INDEX TASY.REPJEJU_PERFIL_FK_I ON TASY.REP_JEJUM
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPJEJU_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPJEJU_PK ON TASY.REP_JEJUM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPJEJU_PRESMED_FK_I ON TASY.REP_JEJUM
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPJEJU_REPOBJE_FK_I ON TASY.REP_JEJUM
(NR_SEQ_OBJETIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPJEJU_REPOBJE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPJEJU_REPTIJE_FK_I ON TASY.REP_JEJUM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPJEJU_TASASDI_FK_I ON TASY.REP_JEJUM
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPJEJU_TASASDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SMART_JEJUM
after update ON TASY.REP_JEJUM FOR EACH ROW
declare
	reg_integracao_w		gerar_int_padrao.reg_integracao;

BEGIN
begin
	if 	((:old.ie_suspenso = 'N') and (:new.ie_suspenso = 'S')) or
	     ((:new.dt_fim 	<> :old.dt_fim)) then
		begin
			reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
			gerar_int_padrao.gravar_integracao('306', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQUENCIA || ';DS_OPERACAO=UPDATE');
		END;
	end if;
exception when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_jejum_update_hl7_dixtal
AFTER UPDATE ON TASY.REP_JEJUM FOR EACH ROW
declare

nr_seq_interno_w		number(10);
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	number(10);
ie_leito_monit			varchar2(1);

BEGIN

if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then

	ds_sep_bv_w := obter_separador_bv;

	select	nr_atendimento, cd_pessoa_fisica
	into	nr_atendimento_w, cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = :new.nr_prescricao;

	select	max(obter_atepacu_paciente(nr_atendimento_w ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_w),'N')
	into	ie_leito_monit
	from 	dual;

	if (ie_leito_monit = 'S') then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w   || ds_sep_bv_w ||
					'nr_atendimento='    || nr_atendimento_w      || ds_sep_bv_w ||
					'nr_seq_interno='     || nr_seq_interno_w       || ds_sep_bv_w ||
					'nr_prescricao='       || :new.nr_prescricao     || ds_sep_bv_w ||
					'nr_seq_jejum='        || :new.nr_sequencia     || ds_sep_bv_w;
		gravar_agend_integracao(51, ds_param_integ_hl7_w);
		end;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Rep_jejum_Insert
BEFORE INSERT ON TASY.REP_JEJUM FOR EACH ROW
DECLARE

dt_inicio_prescr_w	date;
dt_validade_prescr_w	date;

BEGIN

if	(:new.dt_inicio is null) then

	select	max(dt_inicio_prescr),
		max(dt_validade_prescr)
	into	dt_inicio_prescr_w,
		dt_validade_prescr_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;

	:new.dt_inicio	:= dt_inicio_prescr_w;
	--:new.dt_fim	:= dt_validade_prescr_w;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.rep_jejum_Delete
BEFORE DELETE ON TASY.REP_JEJUM FOR EACH ROW
BEGIN
begin
Deletar_jejum(:old.nr_sequencia, :old.dt_inicio, :old.dt_fim, :old.nr_prescricao, :old.nm_usuario);
exception when others then
	null;
end;

END;
/


ALTER TABLE TASY.REP_JEJUM ADD (
  CONSTRAINT REPJEJU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REP_JEJUM ADD (
  CONSTRAINT REPJEJU_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT REPJEJU_REPTIJE_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.REP_TIPO_JEJUM (NR_SEQUENCIA),
  CONSTRAINT REPJEJU_REPOBJE_FK 
 FOREIGN KEY (NR_SEQ_OBJETIVO) 
 REFERENCES TASY.REP_OBJETIVO_JEJUM (NR_SEQUENCIA),
  CONSTRAINT REPJEJU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REPJEJU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.REP_JEJUM TO NIVEL_1;

