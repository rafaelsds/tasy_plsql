ALTER TABLE TASY.HD_DIALISE_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DIALISE       NUMBER(10),
  NR_SEQ_ACESSO        NUMBER(10)               NOT NULL,
  DT_ACESSO            DATE,
  CD_PF_ACESSO         VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_UNID_DIALISE  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDIALAC_ESTABEL_FK_I ON TASY.HD_DIALISE_ACESSO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDIALAC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDIALAC_HDACESS_FK_I ON TASY.HD_DIALISE_ACESSO
(NR_SEQ_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDIALAC_HDACESS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDIALAC_HDDIALE_FK_I ON TASY.HD_DIALISE_ACESSO
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDIALAC_HDUNIDD_FK_I ON TASY.HD_DIALISE_ACESSO
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDIALAC_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDIALAC_PESFISI_FK_I ON TASY.HD_DIALISE_ACESSO
(CD_PF_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDIALAC_PK ON TASY.HD_DIALISE_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDIALAC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hd_dialise_acesso_befup_sign
before update ON TASY.HD_DIALISE_ACESSO for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_seq_dialise
and 	ie_opcao = 'CP'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec     <> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec         <> :new.nm_usuario_nrec
		or :old.nr_seq_acesso           <> :new.nr_seq_acesso
		or :old.dt_acesso               <> :new.dt_acesso
		or :old.cd_pf_acesso            <> :new.cd_pf_acesso
		or :old.cd_estabelecimento      <> :new.cd_estabelecimento
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or (:old.nr_sequencia 		is null		and :new.nr_sequencia 		is not null)
		or (:old.dt_atualizacao_nrec 	is null		and :new.dt_atualizacao_nrec 	is not null)
		or (:old.nm_usuario_nrec 	is null		and :new.nm_usuario_nrec 	is not null)
		or (:old.nr_seq_acesso 		is null		and :new.nr_seq_acesso 		is not null)
		or (:old.dt_acesso 		is null		and :new.dt_acesso 		is not null)
		or (:old.cd_pf_acesso 		is null		and :new.cd_pf_acesso 		is not null)
		or (:old.cd_estabelecimento 	is null		and :new.cd_estabelecimento 	is not null)
		or (:old.nr_seq_dialise 	is null		and :new.nr_seq_dialise 	is not null)
		or (:old.nr_seq_unid_dialise 	is null		and :new.nr_seq_unid_dialise 	is not null)
		or (:old.nr_sequencia 		is not null	and :new.nr_sequencia 		is null)
		or (:old.dt_atualizacao_nrec 	is not null	and :new.dt_atualizacao_nrec 	is null)
		or (:old.nm_usuario_nrec 	is not null	and :new.nm_usuario_nrec 	is null)
		or (:old.nr_seq_acesso 		is not null	and :new.nr_seq_acesso 		is null)
		or (:old.dt_acesso 		is not null	and :new.dt_acesso 		is null)
		or (:old.cd_pf_acesso 		is not null	and :new.cd_pf_acesso 		is null)
		or (:old.cd_estabelecimento 	is not null	and :new.cd_estabelecimento 	is null)
		or (:old.nr_seq_dialise 	is not null	and :new.nr_seq_dialise 	is null)
		or (:old.nr_seq_unid_dialise 	is not null	and :new.nr_seq_unid_dialise 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_DIALISE_ACESSO ADD (
  CONSTRAINT HDIALAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE_ACESSO ADD (
  CONSTRAINT HDIALAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HDIALAC_HDACESS_FK 
 FOREIGN KEY (NR_SEQ_ACESSO) 
 REFERENCES TASY.HD_ACESSO (NR_SEQUENCIA),
  CONSTRAINT HDIALAC_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDIALAC_PESFISI_FK 
 FOREIGN KEY (CD_PF_ACESSO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDIALAC_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE_ACESSO TO NIVEL_1;

