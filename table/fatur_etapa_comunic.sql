ALTER TABLE TASY.FATUR_ETAPA_COMUNIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FATUR_ETAPA_COMUNIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.FATUR_ETAPA_COMUNIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ETAPA         NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PERFIL            NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FAETACOM_FATETAP_FK_I ON TASY.FATUR_ETAPA_COMUNIC
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAETACOM_FATETAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAETACOM_PERFIL_FK_I ON TASY.FATUR_ETAPA_COMUNIC
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAETACOM_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FAETACOM_PK ON TASY.FATUR_ETAPA_COMUNIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAETACOM_PK
  MONITORING USAGE;


ALTER TABLE TASY.FATUR_ETAPA_COMUNIC ADD (
  CONSTRAINT FAETACOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FATUR_ETAPA_COMUNIC ADD (
  CONSTRAINT FAETACOM_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT FAETACOM_FATETAP_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.FATUR_ETAPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FATUR_ETAPA_COMUNIC TO NIVEL_1;

