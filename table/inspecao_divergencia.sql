ALTER TABLE TASY.INSPECAO_DIVERGENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INSPECAO_DIVERGENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.INSPECAO_DIVERGENCIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_REGISTRO        NUMBER(10)             NOT NULL,
  NR_SEQ_INSPECAO        NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO            NUMBER(10)             NOT NULL,
  CD_MATERIAL            NUMBER(6)              NOT NULL,
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_PESSOA_RESP_JUSTIF  VARCHAR2(10 BYTE),
  IE_MOTIVO_ACEITE       VARCHAR2(15 BYTE)      NOT NULL,
  IE_GERACAO             VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INSDIVE_INSRECE_FK_I ON TASY.INSPECAO_DIVERGENCIA
(NR_SEQ_INSPECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INSDIVE_INSREGI_FK_I ON TASY.INSPECAO_DIVERGENCIA
(NR_SEQ_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INSDIVE_INSTIDI_FK_I ON TASY.INSPECAO_DIVERGENCIA
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSDIVE_INSTIDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INSDIVE_MATERIA_FK_I ON TASY.INSPECAO_DIVERGENCIA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSDIVE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INSDIVE_PESFISI_FK_I ON TASY.INSPECAO_DIVERGENCIA
(CD_PESSOA_RESP_JUSTIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INSDIVE_PK ON TASY.INSPECAO_DIVERGENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INSPECAO_DIVERGENCIA ADD (
  CONSTRAINT INSDIVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INSPECAO_DIVERGENCIA ADD (
  CONSTRAINT INSDIVE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT INSDIVE_INSREGI_FK 
 FOREIGN KEY (NR_SEQ_REGISTRO) 
 REFERENCES TASY.INSPECAO_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INSDIVE_INSRECE_FK 
 FOREIGN KEY (NR_SEQ_INSPECAO) 
 REFERENCES TASY.INSPECAO_RECEBIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INSDIVE_INSTIDI_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.INSPECAO_TIPO_DIVERGENCIA (NR_SEQUENCIA),
  CONSTRAINT INSDIVE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP_JUSTIF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.INSPECAO_DIVERGENCIA TO NIVEL_1;

