ALTER TABLE TASY.SIGA_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIGA_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIGA_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_COMPROVANTE   NUMBER(10)               NOT NULL,
  NR_LOTE              NUMBER(15)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SIGALOTE_PK ON TASY.SIGA_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIGALOTE_SIGACOMP_FK_I ON TASY.SIGA_LOTE
(NR_SEQ_COMPROVANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SIGA_LOTE_tp  after update ON TASY.SIGA_LOTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_LOTE,1,500);gravar_log_alteracao(substr(:old.NR_LOTE,1,4000),substr(:new.NR_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_LOTE',ie_log_w,ds_w,'SIGA_LOTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_COMPROVANTE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_COMPROVANTE,1,4000),substr(:new.NR_SEQ_COMPROVANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COMPROVANTE',ie_log_w,ds_w,'SIGA_LOTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SIGA_LOTE ADD (
  CONSTRAINT SIGALOTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIGA_LOTE ADD (
  CONSTRAINT SIGALOTE_SIGACOMP_FK 
 FOREIGN KEY (NR_SEQ_COMPROVANTE) 
 REFERENCES TASY.SIGA_COMPROVANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIGA_LOTE TO NIVEL_1;

