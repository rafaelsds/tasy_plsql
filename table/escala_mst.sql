ALTER TABLE TASY.ESCALA_MST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_PERDA_PESO          VARCHAR2(3 BYTE)       NOT NULL,
  IE_PESO_PERDIDO        VARCHAR2(3 BYTE),
  IE_MAL_ALIMENTACAO     VARCHAR2(1 BYTE)       NOT NULL,
  QT_PONTUACAO           NUMBER(3),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMST_ATEPACI_FK_I ON TASY.ESCALA_MST
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMST_PESFISI_FK_I ON TASY.ESCALA_MST
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMST_PK ON TASY.ESCALA_MST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMST_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_mst_atual
before insert or update ON TASY.ESCALA_MST for each row
declare
qt_reg_w	number(1);
qt_pontos_w	number(5);
qt_pontuacao_w	number(10);


nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w			number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	ie_evento_disp		= 'LMST'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.ie_perda_peso <> 'S') then
	:new.ie_peso_perdido	:= null;
end if;

qt_pontos_w	:= 0;

if	(:new.ie_perda_peso = 'NC') then
	qt_pontos_w	:= qt_pontos_w + 2;
elsif	(:new.ie_perda_peso = 'S') then
	qt_pontos_w	:= qt_pontos_w + :new.ie_peso_perdido;
end if;

if	(:new.ie_mal_alimentacao = 'S') then
	qt_pontos_w	:= qt_pontos_w + 1;
end if;

:new.qt_pontuacao	:= qt_pontos_w;


if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.QT_PONTUACAO	>= 1)then
	cd_pessoa_fisica_w	:= obter_pessoa_atendimento(:new.nr_atendimento,'C');
	qt_idade_w	:= nvl(obter_idade_pf( cd_pessoa_fisica_w,sysdate,'A'),0);

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w ;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger( nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario,null,:new.dt_liberacao,:new.qt_pontuacao );
		end;
	end loop;
	close C01;

	exception
		when others then
		null;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;
end escala_mst_atual;
/


ALTER TABLE TASY.ESCALA_MST ADD (
  CONSTRAINT ESCMST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MST ADD (
  CONSTRAINT ESCMST_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMST_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_MST TO NIVEL_1;

