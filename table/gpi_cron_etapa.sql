ALTER TABLE TASY.GPI_CRON_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_CRON_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_CRON_ETAPA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ESTRUTURA     NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_APRES         NUMBER(15)               NOT NULL,
  NM_ETAPA             VARCHAR2(100 BYTE)       NOT NULL,
  DS_OBJETIVO          VARCHAR2(4000 BYTE),
  DT_INICIO_PREV       DATE,
  DT_FIM_PREV          DATE,
  DT_INICIO_REAL       DATE,
  DT_FIM_REAL          DATE,
  QT_HORA_PREV         NUMBER(15,2)             NOT NULL,
  QT_HORA_REAL         NUMBER(15,2)             NOT NULL,
  PR_ETAPA             NUMBER(15,2)             NOT NULL,
  NR_SEQ_CRONOGRAMA    NUMBER(10)               NOT NULL,
  NR_SEQ_SUPERIOR      NUMBER(10),
  NR_GRUPO_TRABALHO    NUMBER(10),
  NR_SEQ_TIPO_ETAPA    NUMBER(10),
  NR_PREDECESSORA      NUMBER(10),
  DT_CANCELAMENTO      DATE,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPICRETA_GPICRES_FK_I ON TASY.GPI_CRON_ETAPA
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPICRETA_GPICRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPICRETA_GPICRETA_FK_I ON TASY.GPI_CRON_ETAPA
(NR_SEQ_SUPERIOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPICRETA_GPICRON_FK_I ON TASY.GPI_CRON_ETAPA
(NR_SEQ_CRONOGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPICRETA_GPITPETA_FK_I ON TASY.GPI_CRON_ETAPA
(NR_SEQ_TIPO_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPICRETA_GPITPETA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPICRETA_MANGRTR_FK_I ON TASY.GPI_CRON_ETAPA
(NR_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPICRETA_MANGRTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GPICRETA_PK ON TASY.GPI_CRON_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.gpi_log_data_update
after UPDATE ON TASY.GPI_CRON_ETAPA FOR EACH ROW
begin

if (:old.dt_inicio_prev <> :new.dt_inicio_prev) or
   (:old.dt_fim_prev    <> :new.dt_fim_prev) then
gravar_log_regra_data(:old.nr_seq_cronograma,:new.nm_usuario,obter_perfil_ativo,:old.dt_inicio_prev,:new.dt_inicio_prev,:old.dt_fim_prev,:new.dt_fim_prev,:old.nr_sequencia);
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.GPI_CRON_ETAPA_tp  after update ON TASY.GPI_CRON_ETAPA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_ETAPA,1,500);gravar_log_alteracao(substr(:old.NM_ETAPA,1,4000),substr(:new.NM_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'NM_ETAPA',ie_log_w,ds_w,'GPI_CRON_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_PREV,1,4000),substr(:new.DT_INICIO_PREV,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_PREV',ie_log_w,ds_w,'GPI_CRON_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_PREV,1,4000),substr(:new.DT_FIM_PREV,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_PREV',ie_log_w,ds_w,'GPI_CRON_ETAPA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.gpi_cron_etapa_atual
before insert or update ON TASY.GPI_CRON_ETAPA for each row
declare
nr_seq_cronograma_w	number(10);
nr_seq_superior_w		number(10);

pragma autonomous_transaction;

begin
if	(:new.dt_inicio_prev is not null) and
	(:new.dt_fim_prev is not null) and
	(:new.dt_inicio_prev > :new.dt_fim_prev) then
	--r.aise_application_error(-20011,'A data de in�cio prevista deve ser menor que a data final prevista para a etapa.#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(234610);
end if;

if	(:new.nr_seq_superior is not null) then
	begin
	if	(:new.nr_seq_superior = :new.nr_sequencia) then
		--r.aise_application_error(-20011,'N�o � poss�vel selecionar a mesma etapa como superior!#@#@');
		wheb_mensagem_pck.exibir_mensagem_abort(234611);
	elsif	(nvl(:old.nr_seq_superior,0) <> nvl(:new.nr_seq_superior,0)) then
		begin
		nr_seq_superior_w	:= :new.nr_seq_superior;

		while 	(nr_seq_superior_w is not null) loop
			begin
			if	(nr_seq_superior_w = :new.nr_sequencia) then
				--r.aise_application_error(-20011,'N�o � poss�vel selecionar uma etapa inferior como superior!#@#@');
				wheb_mensagem_pck.exibir_mensagem_abort(234612);
			end if;

			select	nr_seq_superior
			into	nr_seq_superior_w
			from	gpi_cron_etapa
			where	nr_sequencia = nr_seq_superior_w;
			end;
		end loop;
		end;
	end if;
	end;
end if;


end;
/


ALTER TABLE TASY.GPI_CRON_ETAPA ADD (
  CONSTRAINT GPICRETA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_CRON_ETAPA ADD (
  CONSTRAINT GPICRETA_GPICRES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.GPI_CRON_ESTRUT (NR_SEQUENCIA),
  CONSTRAINT GPICRETA_GPICRON_FK 
 FOREIGN KEY (NR_SEQ_CRONOGRAMA) 
 REFERENCES TASY.GPI_CRONOGRAMA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT GPICRETA_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT GPICRETA_MANGRTR_FK 
 FOREIGN KEY (NR_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT GPICRETA_GPITPETA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ETAPA) 
 REFERENCES TASY.GPI_TIPO_ETAPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.GPI_CRON_ETAPA TO NIVEL_1;

