ALTER TABLE TASY.ADEP_ELIMINACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADEP_ELIMINACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADEP_ELIMINACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  DT_ELIMINACAO         DATE                    NOT NULL,
  NR_SEQ_TIPO           NUMBER(10)              NOT NULL,
  IE_ORIGEM             VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_CARACT         NUMBER(10),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  NR_SEQ_CONSIST        NUMBER(10),
  NR_SEQ_COLORACAO      NUMBER(10),
  NR_SEQ_QUANT          NUMBER(10),
  QT_VOLUME             NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADEPELI_ADEPTEI_FK_I ON TASY.ADEP_ELIMINACAO
(NR_SEQ_CARACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_ADEPTEI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_ADEPTEI_FK2_I ON TASY.ADEP_ELIMINACAO
(NR_SEQ_CONSIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_ADEPTEI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_ADEPTEI_FK3_I ON TASY.ADEP_ELIMINACAO
(NR_SEQ_COLORACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_ADEPTEI_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_ADEPTEI_FK4_I ON TASY.ADEP_ELIMINACAO
(NR_SEQ_QUANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_ADEPTEI_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_ADEPTIE_FK_I ON TASY.ADEP_ELIMINACAO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_ADEPTIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_ATEPACI_FK_I ON TASY.ADEP_ELIMINACAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPELI_PESFISI_FK_I ON TASY.ADEP_ELIMINACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADEPELI_PK ON TASY.ADEP_ELIMINACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_PK
  MONITORING USAGE;


CREATE INDEX TASY.ADEPELI_SETATEN_FK_I ON TASY.ADEP_ELIMINACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPELI_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ADEP_ELIMINACAO ADD (
  CONSTRAINT ADEPELI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADEP_ELIMINACAO ADD (
  CONSTRAINT ADEPELI_ADEPTEI_FK2 
 FOREIGN KEY (NR_SEQ_CONSIST) 
 REFERENCES TASY.ADEP_TIPO_ELIM_INFOR (NR_SEQUENCIA),
  CONSTRAINT ADEPELI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ADEPELI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ADEPELI_ADEPTIE_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.ADEP_TIPO_ELIMINACAO (NR_SEQUENCIA),
  CONSTRAINT ADEPELI_ADEPTEI_FK 
 FOREIGN KEY (NR_SEQ_CARACT) 
 REFERENCES TASY.ADEP_TIPO_ELIM_INFOR (NR_SEQUENCIA),
  CONSTRAINT ADEPELI_ADEPTEI_FK3 
 FOREIGN KEY (NR_SEQ_COLORACAO) 
 REFERENCES TASY.ADEP_TIPO_ELIM_INFOR (NR_SEQUENCIA),
  CONSTRAINT ADEPELI_ADEPTEI_FK4 
 FOREIGN KEY (NR_SEQ_QUANT) 
 REFERENCES TASY.ADEP_TIPO_ELIM_INFOR (NR_SEQUENCIA),
  CONSTRAINT ADEPELI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ADEP_ELIMINACAO TO NIVEL_1;

