ALTER TABLE TASY.QUA_EVENTO_PAC_AVAL_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_EVENTO_PAC_AVAL_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_EVENTO_PAC_AVAL_RESULT
(
  NR_SEQ_EVENTO_AVAL   NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_RESULTADO         NUMBER(15,4),
  DS_RESULTADO         VARCHAR2(4000 BYTE)      DEFAULT null,
  DS_RESULT_LONG       LONG RAW
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAEPAR_MEDITAV_FK_I ON TASY.QUA_EVENTO_PAC_AVAL_RESULT
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEPAR_MEDITAV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUAEPAR_PK ON TASY.QUA_EVENTO_PAC_AVAL_RESULT
(NR_SEQ_EVENTO_AVAL, NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEPAR_QUAEPAV_FK_I ON TASY.QUA_EVENTO_PAC_AVAL_RESULT
(NR_SEQ_EVENTO_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEPAR_QUAEPAV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUA_EVENTO_PAC_AVAL_RESULT ADD (
  CONSTRAINT QUAEPAR_PK
 PRIMARY KEY
 (NR_SEQ_EVENTO_AVAL, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_EVENTO_PAC_AVAL_RESULT ADD (
  CONSTRAINT QUAEPAR_MEDITAV_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.MED_ITEM_AVALIAR (NR_SEQUENCIA),
  CONSTRAINT QUAEPAR_QUAEPAV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_AVAL) 
 REFERENCES TASY.QUA_EVENTO_PAC_AVAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.QUA_EVENTO_PAC_AVAL_RESULT TO NIVEL_1;

