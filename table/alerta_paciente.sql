ALTER TABLE TASY.ALERTA_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALERTA_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALERTA_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ALERTA                  DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_ALERTA                  VARCHAR2(2000 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_FIM_ALERTA              DATE,
  NR_SEQ_BO                  NUMBER(10),
  CD_FUNCAO                  NUMBER(10),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_TIPO_ALERTA         NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_PRECAUCAO           NUMBER(10),
  NR_SEQ_SOAP                NUMBER(10),
  NR_SEQ_ALERTA              NUMBER(10),
  IE_ACAO                    VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ALEPACI_ATCONSPEPA_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_ATEPREC_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_PRECAUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_ATESOAP_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_ESTABEL_FK_I ON TASY.ALERTA_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_I1 ON TASY.ALERTA_PACIENTE
(DT_ALERTA, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_I2 ON TASY.ALERTA_PACIENTE
(NM_USUARIO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_I2
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_PERFIL_FK_I ON TASY.ALERTA_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_PESFISI_FK_I ON TASY.ALERTA_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ALEPACI_PK ON TASY.ALERTA_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_PK
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_SACBOOC_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_BO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_SACBOOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_TASASDI_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_TASASDI_FK2_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALEPACI_TIALATE_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_TIPO_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALEPACI_TIALATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALEPACI_TIALATEO_FK_I ON TASY.ALERTA_PACIENTE
(NR_SEQ_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ALERTA_PACIENTE_atual
before insert or update ON TASY.ALERTA_PACIENTE for each row
declare


begin
if	(nvl(:old.DT_ALERTA,sysdate+10) <> :new.DT_ALERTA) and
	(:new.DT_ALERTA is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ALERTA,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.alerta_paciente_delete
before delete ON TASY.ALERTA_PACIENTE for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'),
									 obter_desc_expressao(345825),
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 wheb_usuario_pck.get_nm_usuario);


end;
/


CREATE OR REPLACE TRIGGER TASY.alerta_paciente_pend_atual
after insert or update ON TASY.ALERTA_PACIENTE for each row
declare

qt_reg_w				number(1);
ie_lib_alerta_pac_w		parametro_medico.ie_lib_alerta_pac%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	max(ie_lib_alerta_pac)
into	ie_lib_alerta_pac_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (nvl(ie_lib_alerta_pac_w,'N') = 'S') then

	if (:new.dt_liberacao is null) then
		Gerar_registro_pendente_PEP('ALP', :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario);
	elsif (:old.dt_liberacao is null) and (:new.dt_liberacao is not null) then
		Gerar_registro_pendente_PEP('XALP', :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario);
	end if;

end if;

<<Final>>
qt_reg_w := 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_ALERTA_PACIENTE
before insert or update ON TASY.ALERTA_PACIENTE for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ALERTA_PACIENTE_SBIS_IN
before insert or update ON TASY.ALERTA_PACIENTE for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);

begin
IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
										 :new.cd_pessoa_fisica,
										 obter_desc_expressao(345825),
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
else
	ie_inativacao_w := 'N';

	if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
		ie_inativacao_w := 'S';
	end if;

	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 :new.cd_pessoa_fisica,
										 obter_desc_expressao(345825),
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
end if;
end;
/


ALTER TABLE TASY.ALERTA_PACIENTE ADD (
  CONSTRAINT ALEPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ALERTA_PACIENTE ADD (
  CONSTRAINT ALEPACI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ALEPACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ALEPACI_SACBOOC_FK 
 FOREIGN KEY (NR_SEQ_BO) 
 REFERENCES TASY.SAC_BOLETIM_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ALEPACI_TIALATE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ALERTA) 
 REFERENCES TASY.TIPO_ALERTA_ATEND (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_TIALATEO_FK 
 FOREIGN KEY (NR_SEQ_ALERTA) 
 REFERENCES TASY.TIPO_ALERTA_ATEND_OPTION (NR_SEQUENCIA),
  CONSTRAINT ALEPACI_ATEPREC_FK 
 FOREIGN KEY (NR_SEQ_PRECAUCAO) 
 REFERENCES TASY.ATENDIMENTO_PRECAUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ALERTA_PACIENTE TO NIVEL_1;

