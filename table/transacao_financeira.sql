ALTER TABLE TASY.TRANSACAO_FINANCEIRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRANSACAO_FINANCEIRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRANSACAO_FINANCEIRA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_TRANSACAO              VARCHAR2(10 BYTE)   NOT NULL,
  DS_TRANSACAO              VARCHAR2(250 BYTE)  NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_CAIXA                  VARCHAR2(1 BYTE)    NOT NULL,
  IE_BANCO                  VARCHAR2(1 BYTE)    NOT NULL,
  IE_EXTRA_CAIXA            VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONTA_PAGAR            VARCHAR2(1 BYTE),
  IE_CONTA_RECEBER          VARCHAR2(1 BYTE),
  IE_SALDO_CAIXA            VARCHAR2(1 BYTE)    NOT NULL,
  IE_ACAO                   NUMBER(2)           NOT NULL,
  NR_SEQ_TRANS_BANCO        NUMBER(10),
  IE_AUTENTICAR             VARCHAR2(1 BYTE)    NOT NULL,
  CD_CONTA_FINANC           NUMBER(10),
  CD_ESTABELECIMENTO        NUMBER(4),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_HISTORICO_BANCO        NUMBER(10),
  IE_CENTRO_CUSTO           VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONTAB_BANCO           VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_HISTORICO_CAIXA        NUMBER(10),
  CD_EMPRESA                NUMBER(4)           NOT NULL,
  CD_TIPO_RECEBIMENTO       NUMBER(5),
  IE_CHEQUE_CR              VARCHAR2(1 BYTE)    NOT NULL,
  IE_CARTAO_CR              VARCHAR2(1 BYTE)    NOT NULL,
  IE_CARTAO_DEBITO          VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_TRANS_TRANSF       NUMBER(10),
  CD_PORTADOR               NUMBER(10),
  CD_TIPO_PORTADOR          NUMBER(5),
  IE_ESPECIE                VARCHAR2(3 BYTE)    NOT NULL,
  IE_DEV_CHEQUE             VARCHAR2(1 BYTE)    NOT NULL,
  IE_NEGOCIACAO_CHEQUE_CR   VARCHAR2(1 BYTE)    NOT NULL,
  CD_TIPO_BAIXA             NUMBER(5),
  DS_ORIENTACAO             VARCHAR2(4000 BYTE),
  NR_SEQ_TRANS_CHEQUE       NUMBER(10),
  NR_SEQ_TRANS_DEV_ADIANT   NUMBER(10),
  IE_VARIACAO_CAMBIAL       VARCHAR2(3 BYTE)    NOT NULL,
  NR_SEQ_TRANS_BANCO_CAIXA  NUMBER(10),
  IE_PROJ_REC               VARCHAR2(5 BYTE),
  IE_CRED_NAO_IDENT         VARCHAR2(1 BYTE),
  CD_EXTERNO                VARCHAR2(255 BYTE),
  IE_FRANQUIA               VARCHAR2(1 BYTE),
  IE_REGRA_CONTAB_EMPRESA   VARCHAR2(1 BYTE),
  IE_REPASSE_ITEM           VARCHAR2(1 BYTE),
  IE_RECEB_ADIANT           VARCHAR2(1 BYTE),
  IE_UTIL_CAIXA             VARCHAR2(1 BYTE),
  IE_ADIANT_PAGO            VARCHAR2(1 BYTE),
  IE_TIT_PAGAR_ADIANT       VARCHAR2(1 BYTE),
  IE_CONTAB_CB              VARCHAR2(1 BYTE),
  IE_ESTORNAR_CB            VARCHAR2(1 BYTE),
  IE_BAIXA_CARTAO_CR        VARCHAR2(1 BYTE),
  IE_TRANS_TAXA_CAIXA       VARCHAR2(1 BYTE),
  IE_CONTROLE_BANC          VARCHAR2(1 BYTE),
  IE_CONTAB_TESOURARIA      VARCHAR2(1 BYTE),
  IE_CONFIRMACAO_CB         VARCHAR2(1 BYTE),
  NR_SEQ_APRES              NUMBER(10),
  IE_RECEB_CHEQUE           VARCHAR2(1 BYTE),
  IE_RECEB_CARTAO           VARCHAR2(1 BYTE),
  IE_ESTORNAR_TES           VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_DEV_RECEB    NUMBER(10),
  IE_RPS_CAIXA_RECEB        VARCHAR2(1 BYTE),
  IE_CGC_CAIXA_RECEB        VARCHAR2(1 BYTE),
  IE_OBS_ADIANTAMENTO       VARCHAR2(1 BYTE),
  IE_RECIBO_CB              VARCHAR2(1 BYTE),
  IE_AGRUPA_LOTE_CB         VARCHAR2(1 BYTE),
  IE_TITULO_PAGAR           VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_TIPO_BAIXA_PERDA   NUMBER(10),
  IE_SALDO_CB_ACAO_INVERSA  VARCHAR2(1 BYTE),
  NR_SEQ_BANDEIRA           NUMBER(10),
  IE_GERAR_TIT_REC          VARCHAR2(1 BYTE),
  IE_CONFERIR_MOVTO_CB      VARCHAR2(1 BYTE),
  IE_RETORNO_CONVENIO       VARCHAR2(1 BYTE),
  IE_EMISSAO_CHEQUE         VARCHAR2(1 BYTE),
  IE_CONTROLE_CONTRATO      VARCHAR2(1 BYTE),
  IE_EXIGE_CENTRO_CUSTO     VARCHAR2(1 BYTE),
  CD_GRUPO                  NUMBER(10),
  NR_SEQ_CLASSIFICACAO      NUMBER(10),
  IE_SALDO_FLUXO            VARCHAR2(1 BYTE),
  IE_EMPRESTIMO             VARCHAR2(1 BYTE),
  IE_TROCA_VALORES          VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_APLIC        NUMBER(10),
  IE_DEV_ADIANT_NEGATIVO    VARCHAR2(1 BYTE),
  IE_DEPOSITO_DIRETO        VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_FIN_REF      NUMBER(10),
  IE_TRANSACAO_PIX          VARCHAR2(15 BYTE),
  DS_DESCRICAO_GLOSA        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TRAFINA_BANCACR_FK_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_BANDEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_BANCACR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_CONFINA_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_CTBGRCO_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_EMPRESA_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_ESTABEL_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_FNTPBPE_FK_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TIPO_BAIXA_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_FNTPBPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_HISPADR_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_HISTORICO_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_HISPADR_FK2_I ON TASY.TRANSACAO_FINANCEIRA
(CD_HISTORICO_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_HISPADR_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TRAFINA_PK ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_PORTADO_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TIPBACP_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_TIPO_BAIXA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TIPBACP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TIPRECE_FK_I ON TASY.TRANSACAO_FINANCEIRA
(CD_TIPO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TIPRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK3_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TRAFINA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK4_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TRAFINA_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK5_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_DEV_ADIANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TRAFINA_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK6_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_BANCO_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TRAFINA_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK7_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_DEV_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFINA_TRAFINA_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFINA_TRAFINA_FK8_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_TRANS_FIN_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFINA_TRNFINCSF_FK_I ON TASY.TRANSACAO_FINANCEIRA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TRANSACAO_FINANCEIRA_tp  after update ON TASY.TRANSACAO_FINANCEIRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONTROLE_CONTRATO,1,4000),substr(:new.IE_CONTROLE_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE_CONTRATO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TRANSACAO,1,4000),substr(:new.CD_TRANSACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TRANSACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TRANSACAO,1,4000),substr(:new.DS_TRANSACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TRANSACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAIXA,1,4000),substr(:new.IE_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXTRA_CAIXA,1,4000),substr(:new.IE_EXTRA_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXTRA_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_PAGAR,1,4000),substr(:new.IE_CONTA_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_PAGAR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_RECEBER,1,4000),substr(:new.IE_CONTA_RECEBER,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_RECEBER',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACAO,1,4000),substr(:new.IE_ACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BANCO,1,4000),substr(:new.IE_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BANCO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALDO_CAIXA,1,4000),substr(:new.IE_SALDO_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALDO_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_BANCO,1,4000),substr(:new.NR_SEQ_TRANS_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_BANCO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_FINANC,1,4000),substr(:new.CD_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTENTICAR,1,4000),substr(:new.IE_AUTENTICAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTENTICAR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_HISTORICO_BANCO,1,4000),substr(:new.CD_HISTORICO_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'CD_HISTORICO_BANCO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_CUSTO,1,4000),substr(:new.IE_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_CUSTO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_BANCO,1,4000),substr(:new.IE_CONTAB_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_BANCO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_TRANSF,1,4000),substr(:new.NR_SEQ_TRANS_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_TRANSF',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_HISTORICO_CAIXA,1,4000),substr(:new.CD_HISTORICO_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'CD_HISTORICO_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CHEQUE_CR,1,4000),substr(:new.IE_CHEQUE_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CHEQUE_CR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARTAO_CR,1,4000),substr(:new.IE_CARTAO_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARTAO_CR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARTAO_DEBITO,1,4000),substr(:new.IE_CARTAO_DEBITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARTAO_DEBITO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_RECEBIMENTO,1,4000),substr(:new.CD_TIPO_RECEBIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_RECEBIMENTO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEV_CHEQUE,1,4000),substr(:new.IE_DEV_CHEQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEV_CHEQUE',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NEGOCIACAO_CHEQUE_CR,1,4000),substr(:new.IE_NEGOCIACAO_CHEQUE_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGOCIACAO_CHEQUE_CR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESPECIE,1,4000),substr(:new.IE_ESPECIE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESPECIE',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PORTADOR,1,4000),substr(:new.CD_PORTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PORTADOR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PORTADOR,1,4000),substr(:new.CD_TIPO_PORTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PORTADOR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO,1,4000),substr(:new.DS_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_CHEQUE,1,4000),substr(:new.NR_SEQ_TRANS_CHEQUE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_CHEQUE',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_BAIXA,1,4000),substr(:new.CD_TIPO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_BAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VARIACAO_CAMBIAL,1,4000),substr(:new.IE_VARIACAO_CAMBIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VARIACAO_CAMBIAL',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_DEV_ADIANT,1,4000),substr(:new.NR_SEQ_TRANS_DEV_ADIANT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_DEV_ADIANT',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_BANCO_CAIXA,1,4000),substr(:new.NR_SEQ_TRANS_BANCO_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_BANCO_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROJ_REC,1,4000),substr(:new.IE_PROJ_REC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROJ_REC',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_CONTAB_EMPRESA,1,4000),substr(:new.IE_REGRA_CONTAB_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_CONTAB_EMPRESA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRANQUIA,1,4000),substr(:new.IE_FRANQUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRANQUIA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REPASSE_ITEM,1,4000),substr(:new.IE_REPASSE_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_REPASSE_ITEM',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXTERNO,1,4000),substr(:new.CD_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXTERNO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRED_NAO_IDENT,1,4000),substr(:new.IE_CRED_NAO_IDENT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRED_NAO_IDENT',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEB_ADIANT,1,4000),substr(:new.IE_RECEB_ADIANT,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEB_ADIANT',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UTIL_CAIXA,1,4000),substr(:new.IE_UTIL_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTIL_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ADIANT_PAGO,1,4000),substr(:new.IE_ADIANT_PAGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ADIANT_PAGO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTORNAR_CB,1,4000),substr(:new.IE_ESTORNAR_CB,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTORNAR_CB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIT_PAGAR_ADIANT,1,4000),substr(:new.IE_TIT_PAGAR_ADIANT,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIT_PAGAR_ADIANT',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_CB,1,4000),substr(:new.IE_CONTAB_CB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_CB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_CARTAO_CR,1,4000),substr(:new.IE_BAIXA_CARTAO_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_CARTAO_CR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRANS_TAXA_CAIXA,1,4000),substr(:new.IE_TRANS_TAXA_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANS_TAXA_CAIXA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLE_BANC,1,4000),substr(:new.IE_CONTROLE_BANC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE_BANC',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_TESOURARIA,1,4000),substr(:new.IE_CONTAB_TESOURARIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_TESOURARIA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONFIRMACAO_CB,1,4000),substr(:new.IE_CONFIRMACAO_CB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONFIRMACAO_CB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTORNAR_TES,1,4000),substr(:new.IE_ESTORNAR_TES,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTORNAR_TES',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEB_CHEQUE,1,4000),substr(:new.IE_RECEB_CHEQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEB_CHEQUE',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEB_CARTAO,1,4000),substr(:new.IE_RECEB_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEB_CARTAO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_DEV_RECEB,1,4000),substr(:new.NR_SEQ_TRANS_DEV_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_DEV_RECEB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RPS_CAIXA_RECEB,1,4000),substr(:new.IE_RPS_CAIXA_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_RPS_CAIXA_RECEB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECIBO_CB,1,4000),substr(:new.IE_RECIBO_CB,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECIBO_CB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBS_ADIANTAMENTO,1,4000),substr(:new.IE_OBS_ADIANTAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBS_ADIANTAMENTO',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CGC_CAIXA_RECEB,1,4000),substr(:new.IE_CGC_CAIXA_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CGC_CAIXA_RECEB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AGRUPA_LOTE_CB,1,4000),substr(:new.IE_AGRUPA_LOTE_CB,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUPA_LOTE_CB',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TITULO_PAGAR,1,4000),substr(:new.IE_TITULO_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TITULO_PAGAR',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALDO_CB_ACAO_INVERSA,1,4000),substr(:new.IE_SALDO_CB_ACAO_INVERSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALDO_CB_ACAO_INVERSA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_BAIXA_PERDA,1,4000),substr(:new.NR_SEQ_TIPO_BAIXA_PERDA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_BAIXA_PERDA',ie_log_w,ds_w,'TRANSACAO_FINANCEIRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.TRANSACAO_FINANCEIRA_UPDATE
before update ON TASY.TRANSACAO_FINANCEIRA for each row
declare

cont_banco_w		number(10,0);
cont_caixa_w		number(10,0);
ie_alterar_desc_w	varchar2(255);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	count(*)
into	cont_caixa_w
from	movto_trans_financ
where	nr_seq_trans_financ 	= :old.nr_sequencia
and	(nr_seq_caixa is not null or nr_seq_saldo_caixa is not null);

select	count(*)
into	cont_banco_w
from	movto_trans_financ
where	nr_seq_trans_financ	= :old.nr_sequencia
and	(nr_seq_banco is not null or nr_seq_saldo_banco is not null);

Obter_Param_Usuario(1120,12,wheb_usuario_pck.get_cd_perfil,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_alterar_desc_w);

if	((ie_alterar_desc_w = 'N') and (:new.ds_transacao <> :old.ds_transacao) and ((cont_caixa_w > 0) or (cont_banco_w > 0))) or
	((:new.ie_situacao in ('A','I')) and /*consistir inativas tambem conforme OS 792317*/
	 (((:old.ie_acao	<> :new.ie_acao) and ((cont_caixa_w > 0) or (cont_banco_w > 0))) or
	  ((cont_caixa_w	> 0) and (:new.ie_saldo_caixa	<> :old.ie_saldo_caixa)) or
	  ((cont_banco_w	> 0) and (:new.ie_banco		<> :old.ie_banco))))	then
	wheb_mensagem_pck.exibir_mensagem_abort(	176493,
						'CONTA_CAIXA_W='||cont_caixa_w||';'||
						'CONT_BANCO_W='||cont_banco_w);

end if;

end if;

end;
/


ALTER TABLE TASY.TRANSACAO_FINANCEIRA ADD (
  CONSTRAINT TRAFINA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRANSACAO_FINANCEIRA ADD (
  CONSTRAINT TRAFINA_TRAFINA_FK8 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_REF) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_TRAFINA_FK7 
 FOREIGN KEY (NR_SEQ_TRANS_DEV_RECEB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_TRAFINA_FK3 
 FOREIGN KEY (NR_SEQ_TRANS_TRANSF) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_BANCO) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TRAFINA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TRAFINA_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO_BANCO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TRAFINA_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_CAIXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TRAFINA_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT TRAFINA_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEBIMENTO) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO),
  CONSTRAINT TRAFINA_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT TRAFINA_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA),
  CONSTRAINT TRAFINA_TRAFINA_FK4 
 FOREIGN KEY (NR_SEQ_TRANS_CHEQUE) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_TRAFINA_FK5 
 FOREIGN KEY (NR_SEQ_TRANS_DEV_ADIANT) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_TRAFINA_FK6 
 FOREIGN KEY (NR_SEQ_TRANS_BANCO_CAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_CTBGRCO_FK 
 FOREIGN KEY (CD_GRUPO) 
 REFERENCES TASY.CTB_GRUPO_CONTA (CD_GRUPO),
  CONSTRAINT TRAFINA_TRNFINCSF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.TRANS_FINANC_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT TRAFINA_FNTPBPE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_BAIXA_PERDA) 
 REFERENCES TASY.FIN_TIPO_BAIXA_PERDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRANSACAO_FINANCEIRA TO NIVEL_1;

