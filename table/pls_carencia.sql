ALTER TABLE TASY.PLS_CARENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CARENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CARENCIA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_PLANO              NUMBER(10),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_TIPO_CARENCIA      NUMBER(10),
  NR_SEQ_SEGURADO           NUMBER(10),
  QT_DIAS                   NUMBER(5),
  DT_INICIO_VIGENCIA        DATE,
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  NR_SEQ_PESSOA_PROPOSTA    NUMBER(10),
  NR_SEQ_REGRA_AUT          NUMBER(10),
  NR_SEQ_SCA                NUMBER(10),
  IE_MES_POSTERIOR          VARCHAR2(1 BYTE),
  IE_CARENCIA_ANTERIOR      VARCHAR2(1 BYTE),
  NR_SEQ_ANALISE_PREEXIST   NUMBER(10),
  DT_FIM_VIGENCIA           DATE,
  CD_SISTEMA_ANTERIOR       VARCHAR2(30 BYTE),
  IE_ISENCAO_CARENCIA       VARCHAR2(2 BYTE),
  NR_SEQ_PROPOSTA           NUMBER(10),
  NR_SEQ_REGRA_ISENCAO      NUMBER(10),
  NR_SEQ_GRUPO_CARENCIA     NUMBER(10),
  IE_CPT                    VARCHAR2(1 BYTE),
  IE_ORIGEM_CARENCIA_BENEF  VARCHAR2(2 BYTE),
  NR_SEQ_REDE               NUMBER(10),
  NR_SEQ_PLANO_CONTRATO     NUMBER(10),
  QT_DIAS_FORA_ABRANG_ANT   NUMBER(5),
  NR_SEQ_REGRA_ALT_ACOMOD   NUMBER(10),
  DT_INICIO_VIG_PLANO       DATE,
  DT_FIM_VIG_PLANO          DATE,
  NR_SEQ_ANALISE_ADESAO     NUMBER(10),
  QT_DIAS_REDUCAO_MAN       NUMBER(5),
  NR_SEQ_REGRA_PADRAO       NUMBER(10),
  DT_INICIO_VIG_PLANO_REF   DATE,
  DT_FIM_VIG_PLANO_REF      DATE,
  QT_DIAS_ORIGEM            NUMBER(5),
  IE_ORIGEM_CARENCIA        NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCAREN_I1 ON TASY.PLS_CARENCIA
(QT_DIAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_I2 ON TASY.PLS_CARENCIA
(IE_CPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCAREN_PK ON TASY.PLS_CARENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSAADE_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_ANALISE_ADESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSANPR_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_ANALISE_PREEXIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSANPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSCONT_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSGRCA_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_GRUPO_CARENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSPLAN_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSPLAN_FK2_I ON TASY.PLS_CARENCIA
(NR_SEQ_PLANO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSPLAN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSPRAD_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSPRAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSPROB_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_PESSOA_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSPROB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSRACA_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_REGRA_ALT_ACOMOD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSRACA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSRCPA_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_REGRA_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSREAT_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_REDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSRECI_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_REGRA_ISENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSRECI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSSCOA_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSSCOA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAREN_PLSSEGU_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAREN_PLSTICA_FK_I ON TASY.PLS_CARENCIA
(NR_SEQ_TIPO_CARENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAREN_PLSTICA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_carencia_after_update
after update ON TASY.PLS_CARENCIA for each row
declare

ds_alteracao_w	varchar(4000) := ' ';
ds_anterior_w	varchar(255);
ds_novo_w	varchar(255);

begin
if	((:new.nr_seq_grupo_carencia  <> :old.nr_seq_grupo_carencia ) or
	(:new.nr_seq_grupo_carencia  is not null and :old.nr_seq_grupo_carencia is null) or
	(:new.nr_seq_grupo_carencia is null and :old.nr_seq_grupo_carencia is not null))then

	select 	substr(pls_obter_dados_grupo_carencia(:new.nr_seq_grupo_carencia,'N'),1,255),
		substr(pls_obter_dados_grupo_carencia(:old.nr_seq_grupo_carencia,'N'),1,255)
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;

	--Altera��o do grupo de car�ncia
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352337 , 'NR_CARENCIA_OLD=' || :old.nr_seq_grupo_carencia||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_grupo_carencia||';'||'DS_CARENCIA_NEW='||ds_novo_w);
end if;

if	((trunc(:new.dt_fim_vig_plano,'DAY') <> trunc(:old.dt_fim_vig_plano,'DAY')) or
	(:new.dt_fim_vig_plano is not null  and :old.dt_fim_vig_plano is null) or
	(:new.dt_fim_vig_plano is null and :old.dt_fim_vig_plano is not null))then

	--Altera��o da data de fim de vig�ncia do plano.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352340 , 'DT_CARENCIA_OLD=' ||:old.dt_fim_vig_plano||';'||'DT_CARENCIA_NEW='||:new.dt_fim_vig_plano);
end if;

if	((trunc(:new.dt_inicio_vig_plano,'DAY') <> trunc(:old.dt_inicio_vig_plano,'DAY')) or
	(:new.dt_inicio_vig_plano is not null  and :old.dt_inicio_vig_plano is null) or
	(:new.dt_inicio_vig_plano is null and :old.dt_inicio_vig_plano is not null))then

	--Altera��o da data de in�cio de vig�ncia do plano.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352341 , 'DT_CARENCIA_OLD=' ||:old.dt_inicio_vig_plano||';'||'DT_CARENCIA_NEW='||:new.dt_inicio_vig_plano);
end if;

if 	((:new.nr_seq_plano_contrato <> :old.nr_seq_plano_contrato) or
	(:new.nr_seq_plano_contrato is not null and :old.nr_seq_plano_contrato is null) or
	(:new.nr_seq_plano_contrato is null and :old.nr_seq_plano_contrato is not null))then

	select 	substr(pls_obter_dados_produto(:new.nr_seq_plano_contrato,'N'),1,255),
		substr(pls_obter_dados_produto(:old.nr_seq_plano_contrato,'N'),1,255)
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;
	--Altera��o do produto do contrato que a car�ncia � v�lida.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352345 , 'NR_CARENCIA_OLD=' || :old.nr_seq_plano_contrato ||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_plano_contrato||';'||'DS_CARENCIA_NEW='||ds_novo_w);
end if;

if	((:new.nr_seq_tipo_carencia <> :old.nr_seq_tipo_carencia) or
	(:new.nr_seq_tipo_carencia is not null and :old.nr_seq_tipo_carencia is null) or
	(:new.nr_seq_tipo_carencia is null and :old.nr_seq_tipo_carencia is not null))then

	select	nvl(max(ds_carencia),'')
	into	ds_anterior_w
	from	pls_tipo_carencia
	where	nr_sequencia	= :old.nr_seq_tipo_carencia;

	select	nvl(max(ds_carencia),'')
	into	ds_novo_w
	from	pls_tipo_carencia
	where	nr_sequencia	= :new.nr_seq_tipo_carencia;

	--Altera��o do tipo de car�ncia do contrato.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352346 , 'NR_CARENCIA_OLD=' || :old.nr_seq_tipo_carencia ||';'||'DS_CARENCIA_OLD='||ds_anterior_w||';'|| 'NR_CARENCIA_NEW='||:new.nr_seq_tipo_carencia||';'||'DS_CARENCIA_NEW='||ds_novo_w);
end if;

if	((:new.ds_observacao <> :old.ds_observacao) or
	(:new.ds_observacao is not null and :old.ds_observacao is null) or
	(:new.ds_observacao is null and :old.ds_observacao is not null))then

	--Altera��o da observa��o.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352348 , 'DS_OBSERVACAO_OLD=' ||:old.ds_observacao||';'||'DS_OBSERVACAO_NEW='||:new.ds_observacao);
end if;

if 	((:new.qt_dias_fora_abrang_ant <> :old.qt_dias_fora_abrang_ant) or
	(:new.qt_dias_fora_abrang_ant is not null and :old.qt_dias_fora_abrang_ant is null) or
	(:new.qt_dias_fora_abrang_ant is null and :old.qt_dias_fora_abrang_ant is not null))then

	--Altera��o na contagem de dias fora abrang�ncia anterior.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352350 , 'QT_DIAS_OLD=' ||:old.qt_dias_fora_abrang_ant||';'||'QT_DIAS_NEW='||:new.qt_dias_fora_abrang_ant);
end if;

if	((trunc(:new.dt_fim_vigencia, 'DAY') <> trunc(:old.dt_fim_vigencia,'DAY'))or
	(:new.dt_fim_vigencia is not null and :old.dt_fim_vigencia is null) or
	(:new.dt_fim_vigencia is null and :old.dt_fim_vigencia is not null))then

	--Altera��o da data de fim de vig�ncia para a car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352342 , 'DT_CARENCIA_OLD=' ||:old.dt_fim_vigencia||';'||'DT_CARENCIA_NEW='||:new.dt_fim_vigencia);
end if;

if 	((trunc(:new.dt_inicio_vigencia,'DAY') <> trunc(:old.dt_inicio_vigencia,'DAY')) or
	(:new.dt_inicio_vigencia is not null and :old.dt_inicio_vigencia is null) or
	(:new.dt_inicio_vigencia is null and :old.dt_inicio_vigencia is not null))then

	--Altera��o da data de in�cio de vig�ncia para a car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352343 , 'DT_CARENCIA_OLD=' ||:old.dt_inicio_vigencia||';'||'DT_CARENCIA_NEW='||:new.dt_inicio_vigencia);
end if;

if	(:new.qt_dias <> :old.qt_dias) then

	--Altera��o da quantidade de dias para a regra de car�ncia.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352351 , 'QT_DIAS_OLD=' ||:old.qt_dias||';'||'QT_DIAS_NEW='||:new.qt_dias);
end if;

if 	(:new.ie_mes_posterior <> :old.ie_mes_posterior) then

	select 	decode(:new.ie_mes_posterior,'S','Sim','N','N�o', ' '),
		decode(:old.ie_mes_posterior,'S','Sim','N','N�o', ' ')
	into	ds_novo_w,
		ds_anterior_w
	from 	dual;

	--Altera��o da vig�ncia a partir do 1� dia do m�s subsequente.
	ds_alteracao_w := ds_alteracao_w || wheb_mensagem_pck.get_texto(352353 , 'DS_OLD=' ||ds_anterior_w||';'||'DS_NEW='||ds_novo_w);
end if;

if 	(ds_alteracao_w	<> ' ') then
	insert into pls_carencia_log
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec, ds_alteracao, nr_seq_carencia)
	values	(pls_carencia_log_seq.NextVal, sysdate, null,:new.nm_usuario, null, ds_alteracao_w, :old.nr_sequencia);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_carencia_insert
before insert or update ON TASY.PLS_CARENCIA for each row
declare

ie_cpt_w	varchar2(1);
ds_erro_w	varchar2(255);

begin

begin
if	(:new.nr_seq_tipo_carencia is null) then
	:new.ie_cpt	:= 'N';
else
	select	max(ie_cpt)
	into	ie_cpt_w
	from	pls_tipo_carencia
	where	nr_sequencia	= :new.nr_seq_tipo_carencia;

	:new.ie_cpt	:= nvl(ie_cpt_w,'N');
end if;
exception
when others then
	ds_erro_w	:= '';
end;

-- Performance do calculo da car�ncia do SIP
:new.dt_inicio_vig_plano_ref := trunc(pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vig_plano, 'I'),'dd');
:new.dt_fim_vig_plano_ref := fim_dia(pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vig_plano, 'F'));

-- OS 1371436 - Obs: O campo QT_DIAS_ORIGEM foi criado nesta OS e est� sendo usado apenas nas car�ncias do benefici�rio por enquanto
if	(inserting) then
	:new.qt_dias_origem := :new.qt_dias;
end if;

end;
/


ALTER TABLE TASY.PLS_CARENCIA ADD (
  CONSTRAINT PLSCAREN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CARENCIA ADD (
  CONSTRAINT PLSCAREN_PLSRCPA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PADRAO) 
 REFERENCES TASY.PLS_REGRA_CARENCIA_PADRAO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSRACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ALT_ACOMOD) 
 REFERENCES TASY.PLS_REGRA_ALT_CAREN_ACOMOD (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSAADE_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_ADESAO) 
 REFERENCES TASY.PLS_ANALISE_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSTICA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CARENCIA) 
 REFERENCES TASY.PLS_TIPO_CARENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSPROB_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSSCOA_FK 
 FOREIGN KEY (NR_SEQ_SCA) 
 REFERENCES TASY.PLS_SERVICO_COBERTURA_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSANPR_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_PREEXIST) 
 REFERENCES TASY.PLS_ANALISE_PREEXISTENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSRECI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ISENCAO) 
 REFERENCES TASY.PLS_REGRA_CARENCIA_ISENCAO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSGRCA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CARENCIA) 
 REFERENCES TASY.PLS_GRUPO_CARENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSREAT_FK 
 FOREIGN KEY (NR_SEQ_REDE) 
 REFERENCES TASY.PLS_REDE_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCAREN_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_PLANO_CONTRATO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CARENCIA TO NIVEL_1;

