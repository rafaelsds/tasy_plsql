ALTER TABLE TASY.MAN_TIPO_ATIV_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_TIPO_ATIV_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_TIPO_ATIV_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_VISUALIZA          VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ATIV      NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO_TRAB     NUMBER(10),
  NR_SEQ_GRUPO_PLANEJ   NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PERFIL             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANTPLIB_MANGRPL_FK_I ON TASY.MAN_TIPO_ATIV_LIB
(NR_SEQ_GRUPO_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTPLIB_MANGRTR_FK_I ON TASY.MAN_TIPO_ATIV_LIB
(NR_SEQ_GRUPO_TRAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTPLIB_MANTPATV_FK_I ON TASY.MAN_TIPO_ATIV_LIB
(NR_SEQ_TIPO_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_MANTPATV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTPLIB_PERFIL_FK_I ON TASY.MAN_TIPO_ATIV_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANTPLIB_PK ON TASY.MAN_TIPO_ATIV_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_PK
  MONITORING USAGE;


CREATE INDEX TASY.MANTPLIB_SETATEN_FK_I ON TASY.MAN_TIPO_ATIV_LIB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTPLIB_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.MAN_TIPO_ATIV_LIB ADD (
  CONSTRAINT MANTPLIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_TIPO_ATIV_LIB ADD (
  CONSTRAINT MANTPLIB_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT MANTPLIB_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRAB) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANTPLIB_MANTPATV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATIV) 
 REFERENCES TASY.MAN_TIPO_ATIV (NR_SEQUENCIA),
  CONSTRAINT MANTPLIB_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT MANTPLIB_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.MAN_TIPO_ATIV_LIB TO NIVEL_1;

