ALTER TABLE TASY.PLS_AUTO_GER_ITENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AUTO_GER_ITENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AUTO_GER_ITENS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  PR_CALCULO_TOTAL     NUMBER(7,4),
  PR_CALCULO_AUTO_GER  NUMBER(7,4),
  PR_TOTAL             NUMBER(7,4),
  VL_CALCULO_TOTAL     NUMBER(15,2),
  VL_CALCULO_AUTO_GER  NUMBER(15,2),
  NR_SEQ_PRESTADOR     NUMBER(10),
  CD_PRESTADOR         VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSAGER_I1 ON TASY.PLS_AUTO_GER_ITENS
(NR_SEQ_LOTE, CD_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAGER_I2 ON TASY.PLS_AUTO_GER_ITENS
(NR_SEQ_LOTE, NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSAGER_PK ON TASY.PLS_AUTO_GER_ITENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGER_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAGER_PLLOAUG_FK_I ON TASY.PLS_AUTO_GER_ITENS
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAGER_PLSPRES_FK_I ON TASY.PLS_AUTO_GER_ITENS
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGER_PLSPRES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_auto_ger_itens_atual
before update or insert ON TASY.PLS_AUTO_GER_ITENS for each row
declare
qt_registro_w	number(10);

begin

select	count(1)
into	qt_registro_w
from	pls_lote_auto_gerado
where	nr_sequencia = :new.nr_seq_lote
and	ie_status = 'F';

if	(qt_registro_w = 0) then
	-- para garantir que o percentual sempre estar� correto
	:new.pr_total	:= 100 * (dividir(:new.vl_calculo_auto_ger, :new.vl_calculo_total));
else
	-- aqui avisa que n�o pode ser feita altera��es em lotes fechados
	wheb_mensagem_pck.exibir_mensagem_abort(285148);
end if;

end pls_auto_ger_itens_atual;
/


ALTER TABLE TASY.PLS_AUTO_GER_ITENS ADD (
  CONSTRAINT PLSAGER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AUTO_GER_ITENS ADD (
  CONSTRAINT PLSAGER_PLLOAUG_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_AUTO_GERADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSAGER_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AUTO_GER_ITENS TO NIVEL_1;

