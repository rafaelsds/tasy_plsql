ALTER TABLE TASY.REGRA_UNIDADE_GAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_UNIDADE_GAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_UNIDADE_GAS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GAS           NUMBER(10)               NOT NULL,
  IE_UNIDADE_MEDIDA    VARCHAR2(15 BYTE)        NOT NULL,
  IE_DISP_RESP_ESP     VARCHAR2(15 BYTE)        NOT NULL,
  QT_MAX_PERMITIDA     NUMBER(8,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REUMGAS_GAS_FK_I ON TASY.REGRA_UNIDADE_GAS
(NR_SEQ_GAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REUMGAS_PK ON TASY.REGRA_UNIDADE_GAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REUMGAS_PK
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_UNIDADE_GAS ADD (
  CONSTRAINT REUMGAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_UNIDADE_GAS ADD (
  CONSTRAINT REUMGAS_GAS_FK 
 FOREIGN KEY (NR_SEQ_GAS) 
 REFERENCES TASY.GAS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_UNIDADE_GAS TO NIVEL_1;

