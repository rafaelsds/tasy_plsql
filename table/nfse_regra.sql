ALTER TABLE TASY.NFSE_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NFSE_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.NFSE_REGRA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_TIPO_RPS               NUMBER(6),
  IE_REGIME_ESPECIAL_TRIB   NUMBER(6),
  IE_OPTANTE_SIMPLES_NAC    VARCHAR2(1 BYTE),
  IE_INCENTIVADOR_CULTURAL  VARCHAR2(1 BYTE),
  NR_SEQ_TRIB_PRESTADOR     NUMBER(10),
  IE_MUNICIPIO_PRODUCAO     NUMBER(10),
  IE_MUNICIPIO_HOMOLOGACAO  NUMBER(10),
  NR_SEQ_RELATORIO          NUMBER(10),
  IE_ASSINAR_PRODUCAO       VARCHAR2(1 BYTE),
  NR_TENTATIVAS             NUMBER(10)          NOT NULL,
  IE_ASSINAR_HOMOLOGACAO    VARCHAR2(1 BYTE),
  NR_TEMPO_ESPERA           NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  IE_ALTERA_VALOR_NFSE      VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_DOCUMENTO     NUMBER(10),
  IE_TIPO_ENDERECO          NUMBER(10),
  NR_SEQ_REL_JAVA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFSEREGR_ESTABEL_FK_I ON TASY.NFSE_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFSEREGR_NFSETPDOC_FK_I ON TASY.NFSE_REGRA
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFSEREGR_NFSETPDOC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NFSEREGR_PK ON TASY.NFSE_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFSEREGR_RELATOR_FK_I ON TASY.NFSE_REGRA
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFSEREGR_RELATOR_JAVA_FK_I ON TASY.NFSE_REGRA
(NR_SEQ_REL_JAVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFSEREGR_SITTRPS_FK_I ON TASY.NFSE_REGRA
(NR_SEQ_TRIB_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFSEREGR_SITTRPS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.NFSE_REGRA_tp  after update ON TASY.NFSE_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_RPS,1,4000),substr(:new.IE_TIPO_RPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RPS',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ENDERECO,1,4000),substr(:new.IE_TIPO_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ENDERECO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPTANTE_SIMPLES_NAC,1,4000),substr(:new.IE_OPTANTE_SIMPLES_NAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPTANTE_SIMPLES_NAC',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INCENTIVADOR_CULTURAL,1,4000),substr(:new.IE_INCENTIVADOR_CULTURAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_INCENTIVADOR_CULTURAL',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRIB_PRESTADOR,1,4000),substr(:new.NR_SEQ_TRIB_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRIB_PRESTADOR',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MUNICIPIO_PRODUCAO,1,4000),substr(:new.IE_MUNICIPIO_PRODUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MUNICIPIO_PRODUCAO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MUNICIPIO_HOMOLOGACAO,1,4000),substr(:new.IE_MUNICIPIO_HOMOLOGACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MUNICIPIO_HOMOLOGACAO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TENTATIVAS,1,4000),substr(:new.NR_TENTATIVAS,1,4000),:new.nm_usuario,nr_seq_w,'NR_TENTATIVAS',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TEMPO_ESPERA,1,4000),substr(:new.NR_TEMPO_ESPERA,1,4000),:new.nm_usuario,nr_seq_w,'NR_TEMPO_ESPERA',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RELATORIO,1,4000),substr(:new.NR_SEQ_RELATORIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RELATORIO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ASSINAR_HOMOLOGACAO,1,4000),substr(:new.IE_ASSINAR_HOMOLOGACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ASSINAR_HOMOLOGACAO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ASSINAR_PRODUCAO,1,4000),substr(:new.IE_ASSINAR_PRODUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ASSINAR_PRODUCAO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTERA_VALOR_NFSE,1,4000),substr(:new.IE_ALTERA_VALOR_NFSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTERA_VALOR_NFSE',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_DOCUMENTO,1,4000),substr(:new.NR_SEQ_TIPO_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_DOCUMENTO',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGIME_ESPECIAL_TRIB,1,4000),substr(:new.IE_REGIME_ESPECIAL_TRIB,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGIME_ESPECIAL_TRIB',ie_log_w,ds_w,'NFSE_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.NFSE_REGRA ADD (
  CONSTRAINT NFSEREGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NFSE_REGRA ADD (
  CONSTRAINT NFSEREGR_RELATOR_JAVA_FK 
 FOREIGN KEY (NR_SEQ_REL_JAVA) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA),
  CONSTRAINT NFSEREGR_NFSETPDOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.NFSE_TIPO_DOC_FISCAL (NR_SEQUENCIA),
  CONSTRAINT NFSEREGR_SITTRPS_FK 
 FOREIGN KEY (NR_SEQ_TRIB_PRESTADOR) 
 REFERENCES TASY.SITUACAO_TRIB_PREST_SERV (NR_SEQUENCIA),
  CONSTRAINT NFSEREGR_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA),
  CONSTRAINT NFSEREGR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.NFSE_REGRA TO NIVEL_1;

