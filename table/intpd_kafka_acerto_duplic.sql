ALTER TABLE TASY.INTPD_KAFKA_ACERTO_DUPLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_KAFKA_ACERTO_DUPLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_KAFKA_ACERTO_DUPLIC
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  IE_EVENTO           VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  NR_SEQ_FILA         NUMBER(10),
  DS_JSON             CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INTKAAD_PK ON TASY.INTPD_KAFKA_ACERTO_DUPLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTPD_KAFKA_ACERTO_DUPLIC ADD (
  CONSTRAINT INTKAAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTPD_KAFKA_ACERTO_DUPLIC TO NIVEL_1;

