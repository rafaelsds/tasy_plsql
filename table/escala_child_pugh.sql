ALTER TABLE TASY.ESCALA_CHILD_PUGH
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CHILD_PUGH CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CHILD_PUGH
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_BILIRRUBINA             NUMBER(1)          NOT NULL,
  IE_ALBUMINA                NUMBER(1)          NOT NULL,
  IE_INR                     NUMBER(1),
  IE_ASCITE                  NUMBER(1)          NOT NULL,
  IE_ENCEFALOPATIA_HEPATICA  NUMBER(1)          NOT NULL,
  QT_PONTUACAO               NUMBER(2),
  PR_SOBREVIDA1              NUMBER(3),
  PR_SOBREVIDA2              NUMBER(3),
  NR_SEQ_AVAL_PRE            NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  IE_QUICK                   NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCHPU_ATEPACI_FK_I ON TASY.ESCALA_CHILD_PUGH
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHPU_AVAPRAN_FK_I ON TASY.ESCALA_CHILD_PUGH
(NR_SEQ_AVAL_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHPU_PESFISI_FK2_I ON TASY.ESCALA_CHILD_PUGH
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCHPU_PK ON TASY.ESCALA_CHILD_PUGH
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCHPU_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCCHPU_TASASDI_FK_I ON TASY.ESCALA_CHILD_PUGH
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCHPU_TASASDI_FK2_I ON TASY.ESCALA_CHILD_PUGH
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Escala_child_pugh_atual
before insert or update ON TASY.ESCALA_CHILD_PUGH for each row
declare

qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.qt_pontuacao	:=	nvl(:new.ie_bilirrubina,0) +
						nvl(:new.ie_albumina,0) +
						nvl(:new.ie_inr,0) +
						nvl(:new.ie_ascite,0) +
						nvl(:new.IE_QUICK,0) +
						nvl(:new.ie_encefalopatia_hepatica,0);

if	(:new.qt_pontuacao <= 6) then
	:new.pr_sobrevida1	:=	100;
	:new.pr_sobrevida2	:=	85;
elsif	(:new.qt_pontuacao <= 9) then
	:new.pr_sobrevida1	:=	81;
	:new.pr_sobrevida2	:=	57;
else
	:new.pr_sobrevida1	:=	45;
	:new.pr_sobrevida2	:=	35;
end if;

<<Final>>
qt_reg_w	:= 0;

end Escala_child_pugh_atual;
/


ALTER TABLE TASY.ESCALA_CHILD_PUGH ADD (
  CONSTRAINT ESCCHPU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CHILD_PUGH ADD (
  CONSTRAINT ESCCHPU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCCHPU_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCCHPU_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCCHPU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCHPU_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_AVAL_PRE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_CHILD_PUGH TO NIVEL_1;

