ALTER TABLE TASY.ESCALA_NBS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NBS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NBS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_POSTURA             NUMBER(1),
  QT_ANGULO_PUNHO        NUMBER(1),
  QT_RETRACAO_BRACO      NUMBER(1),
  QT_POPLITEO            NUMBER(1),
  QT_SINAL_XALE          NUMBER(1),
  QT_CALCANHAR_ORELHA    NUMBER(1),
  QT_PELE                NUMBER(1)              NOT NULL,
  QT_LANUGO              NUMBER(1)              NOT NULL,
  QT_SUPERFICIE_PLANAR   NUMBER(1)              NOT NULL,
  QT_GLANDULA_MAMARIA    NUMBER(1)              NOT NULL,
  QT_OLHOS_ORELHAS       NUMBER(1)              NOT NULL,
  QT_GENITAL_MASCULINO   NUMBER(1),
  QT_GENITAL_FEMININO    NUMBER(1),
  QT_PONTUACAO           NUMBER(3),
  NR_HORA                NUMBER(2),
  QT_DIAS                NUMBER(10),
  QT_SEMANAS             NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCANBC_ATEPACI_FK_I ON TASY.ESCALA_NBS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANBC_PESFISI_FK_I ON TASY.ESCALA_NBS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCANBC_PK ON TASY.ESCALA_NBS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANBC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_nbs_atual
before insert or update ON TASY.ESCALA_NBS for each row
declare
qt_ponto_w	number(10)	:= 0;
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.QT_POSTURA	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_POSTURA,0);
end if;

if	(:new.QT_ANGULO_PUNHO	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_ANGULO_PUNHO,0);
end if;
if	(:new.QT_RETRACAO_BRACO	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_RETRACAO_BRACO,0);
end if;
if	(:new.QT_POPLITEO	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_POPLITEO,0);
end if;
if	(:new.QT_SINAL_XALE	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_SINAL_XALE,0);
end if;
if	(:new.QT_CALCANHAR_ORELHA	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_CALCANHAR_ORELHA,0);
end if;
if	(:new.QT_OLHOS_ORELHAS	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.QT_OLHOS_ORELHAS,0);
end if;

qt_ponto_w	:= 	qt_ponto_w + :new.qt_pele + :new.qt_lanugo+ :new.QT_SUPERFICIE_PLANAR	+ :new.QT_GLANDULA_MAMARIA	+
			nvl(:new.QT_GENITAL_MASCULINO,0) + nvl(:new.QT_GENITAL_FEMININO,0);


if	(:new.QT_GENITAL_MASCULINO	is not null)	and
	(:new.QT_GENITAL_FEMININO 	is not null) then
	--N�o � poss�vel selecionar masculino e feminino. #@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(261614);
end if;

:new.QT_PONTUACAO	:= qt_ponto_w;



:NEW.QT_SEMANAS:= NULL;
:NEW.QT_DIAS := NULL;

if	(:NEW.QT_PONTUACAO	= -10) then
	:NEW.QT_SEMANAS:=	20;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= -9) then
	--return	'20 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 20;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= -8) then
	--return	'20 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 20;
	:NEW.QT_DIAS := 6;

elsif	(:NEW.QT_PONTUACAO	= -7) then
	--return	'21 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 21;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= -6) then
	--return	'21 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 21;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= -5) then
	--return	'22 semanas ';
	:NEW.QT_SEMANAS:= 22;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= -4) then
	--return	'22 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 22;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= -3) then
	--return	'22 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 22;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= -2) then
	--return	'23 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 23;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= -1) then
	--return	'23 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 23;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 0) then
	--return	'24 semanas';
	:NEW.QT_SEMANAS:= 24;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 1) then
	--return	'24 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 24;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 2) then
	--return	'24 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 24;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 3) then
	--return	'25 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 25;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 4) then
	--return	'25 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 25;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 5) then
	--return	'26 semanas';
	:NEW.QT_SEMANAS:= 26;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 6) then
	--return	'26 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 26;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 7) then
	--return	'26 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 26;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 8) then
	--return	'27 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 27;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 9) then
--	return	'27 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 27;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 10) then
	--return	'28 semanas';
	:NEW.QT_SEMANAS:= 28;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 11) then
	--return	'28 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 28;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 12) then
	--return	'28 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 28;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 13) then
	--return	'29 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 29;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 14) then
	--return	'29 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 29;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 15) then
	--return	'30 semanas';
	:NEW.QT_SEMANAS:= 30;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 16) then
	--return	'30 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 30;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 17) then
	--return	'30 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 30;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 18) then
	--return	'31 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 31;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 19) then
	--return	'31 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 31;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 20) then
	--return	'32 semanas';
	:NEW.QT_SEMANAS:= 32;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 21) then
	--return	'32 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 32;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 22) then
	--return	'32 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 32;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 23) then
	--return	'33 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 33;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 24) then
	--return	'33 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 33;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 25) then
	--return	'34 semanas';
	:NEW.QT_SEMANAS:= 34;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 26) then
	--return	'34 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 34;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 27) then
	--return	'34 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 34;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 28) then
	--return	'35 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 35;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 29) then
	--return	'35 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 35;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 30) then
	--return	'36 semanas';
	:NEW.QT_SEMANAS:= 36;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 31) then
	--return	'36 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 36;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 32) then
	--return	'36 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 36;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 33) then
	--return	'37 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 37;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 34) then
	--return	'37 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 37;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 35) then
	--return	'38 semanas';
	:NEW.QT_SEMANAS:= 38;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 36) then
--	return	'38 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 38;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 37) then
	--return	'38 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 38;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 38) then
	--return	'39 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 39;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 39) then
	--return	'39 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 39;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 40) then
	--return	'40 semanas';
	:NEW.QT_SEMANAS:= 40;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 41) then
	--return	'40 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 40;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 42) then
	--return	'40 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 40;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 43) then
	--return	'41 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 41;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 44) then
	--return	'41 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 41;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 45) then
	--return	'42 semanas';
	:NEW.QT_SEMANAS:= 42;
	:NEW.QT_DIAS := 0;
elsif	(:NEW.QT_PONTUACAO	= 46) then
	--return	'42 semanas + 3 dias';
	:NEW.QT_SEMANAS:= 42;
	:NEW.QT_DIAS := 3;
elsif	(:NEW.QT_PONTUACAO	= 47) then
	--return	'42 semanas + 6 dias';
	:NEW.QT_SEMANAS:= 42;
	:NEW.QT_DIAS := 6;
elsif	(:NEW.QT_PONTUACAO	= 48) then
	--return	'43 semanas + 1 dia';
	:NEW.QT_SEMANAS:= 43;
	:NEW.QT_DIAS := 1;
elsif	(:NEW.QT_PONTUACAO	= 49) then
	--return	'43 semanas + 4 dias';
	:NEW.QT_SEMANAS:= 43;
	:NEW.QT_DIAS := 4;
elsif	(:NEW.QT_PONTUACAO	= 50) then
	--return	'44 semanas';
	:NEW.QT_SEMANAS:= 44;
	:NEW.QT_DIAS := 0;
end if;


if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	Gerar_IGC_RN(:new.nr_atendimento,:new.nm_usuario);
end if;


end;
/


ALTER TABLE TASY.ESCALA_NBS ADD (
  CONSTRAINT ESCANBC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NBS ADD (
  CONSTRAINT ESCANBC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCANBC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_NBS TO NIVEL_1;

