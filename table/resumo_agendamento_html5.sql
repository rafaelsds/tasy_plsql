DROP TABLE TASY.RESUMO_AGENDAMENTO_HTML5 CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESUMO_AGENDAMENTO_HTML5
(
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  DT_AGENDAMENTO   DATE,
  CD_PROFISSIONAL  VARCHAR2(10 BYTE),
  QT_AGENDAMENTO   NUMBER(10),
  QT_CONFIRMADO    NUMBER(10),
  QT_CANCELAMENTO  NUMBER(10),
  QT_MIN_DURACAO   NUMBER(15,2),
  QT_EXAME         NUMBER(10),
  QT_CONSULTA      NUMBER(10),
  QT_TRANSFERIDO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.RESUMO_AGENDAMENTO_HTML5 TO NIVEL_1;

