ALTER TABLE TASY.OFT_CONSULTA_MEDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_CONSULTA_MEDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_CONSULTA_MEDICA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  DT_CONSULTA                DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DS_QUEIXA_PACIENTE         VARCHAR2(4000 BYTE),
  VL_PRESSAO_OD              VARCHAR2(40 BYTE),
  VL_PRESSAO_OE              VARCHAR2(40 BYTE),
  HR_PRESSAO                 DATE,
  DS_AV_SC_OD                VARCHAR2(40 BYTE),
  DS_AV_CC_OD                VARCHAR2(40 BYTE),
  DS_AV_SC_OE                VARCHAR2(40 BYTE),
  DS_AV_CC_OE                VARCHAR2(40 BYTE),
  DS_MEO                     VARCHAR2(60 BYTE),
  VL_DE_RD_OD                NUMBER(6,2),
  VL_DC_RD_OD                NUMBER(6,2),
  VL_EIXO_RD_OD              NUMBER(6,2),
  DS_VISAO_RD_OD             VARCHAR2(40 BYTE),
  VL_DE_RD_OE                NUMBER(6,2),
  VL_VISAO_RD_OD             NUMBER(3),
  VL_DC_RD_OE                NUMBER(6,2),
  VL_EIXO_RD_OE              NUMBER(6,2),
  DS_VISAO_RD_OE             VARCHAR2(40 BYTE),
  VL_VISAO_RD_OE             NUMBER(3),
  VL_DE_RE_OD                NUMBER(6,2),
  VL_DC_RE_OD                NUMBER(6,2),
  VL_EIXO_RE_OD              NUMBER(6,2),
  DS_VISAO_RE_OD             VARCHAR2(40 BYTE),
  VL_VISAO_RE_OD             NUMBER(3),
  VL_DE_RE_OE                NUMBER(6,2),
  VL_DC_RE_OE                NUMBER(6,2),
  VL_EIXO_RE_OE              NUMBER(6,2),
  DS_VISAO_RE_OE             VARCHAR2(40 BYTE),
  VL_DE_RECEITA_OD           NUMBER(6,2),
  VL_VISAO_RE_OE             NUMBER(3),
  VL_DC_RECEITA_OD           NUMBER(6,2),
  VL_EIXO_RECEITA_OD         NUMBER(6,2),
  DS_VISAO_RECEITA_OD        VARCHAR2(40 BYTE),
  VL_DE_RECEITA_OE           NUMBER(6,2),
  VL_VISAO_RECEITA_OD        NUMBER(3),
  VL_DC_RECEITA_OE           NUMBER(6,2),
  VL_EIXO_RECEITA_OE         NUMBER(6,2),
  DS_VISAO_RECEITA_OE        VARCHAR2(40 BYTE),
  VL_VISAO_RECEITA_OE        NUMBER(3),
  VL_ADICAO_OD               NUMBER(6,2),
  VL_ADICAO_OE               NUMBER(6,2),
  IE_DINAMICA                VARCHAR2(1 BYTE)   NOT NULL,
  IE_ESTATICA                VARCHAR2(1 BYTE)   NOT NULL,
  IE_RECEITA                 VARCHAR2(1 BYTE)   NOT NULL,
  IE_ADICAO                  VARCHAR2(1 BYTE)   NOT NULL,
  DS_FUNDOSCOPIA             VARCHAR2(4000 BYTE),
  IE_TIPO_FUNDOSCOPIA        VARCHAR2(1 BYTE),
  DS_BIOMICROSCOPIA          VARCHAR2(4000 BYTE),
  DS_OUTROS_EXAMES           VARCHAR2(4000 BYTE),
  DS_CONDUTA                 VARCHAR2(4000 BYTE),
  IE_BASE_ADICAO             VARCHAR2(1 BYTE),
  DS_OBS_RECEITA             VARCHAR2(255 BYTE),
  NR_SEQ_RETORNO             NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_ATEND_ANT               NUMBER(10),
  IE_SOMENTE_PERTO           VARCHAR2(1 BYTE),
  DS_HIP_DIAG                VARCHAR2(4000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_MODELO_FORMATACAO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTCOME_ATEPACI_FK_I ON TASY.OFT_CONSULTA_MEDICA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOME_ATEPACI_FK2_I ON TASY.OFT_CONSULTA_MEDICA
(NR_ATEND_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOME_I1 ON TASY.OFT_CONSULTA_MEDICA
(DT_CONSULTA, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOME_OFTRETO_FK_I ON TASY.OFT_CONSULTA_MEDICA
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCOME_OFTRETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCOME_PERFIL_FK_I ON TASY.OFT_CONSULTA_MEDICA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCOME_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCOME_PESFISI_FK_I ON TASY.OFT_CONSULTA_MEDICA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTCOME_PK ON TASY.OFT_CONSULTA_MEDICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOME_TASASDI_FK_I ON TASY.OFT_CONSULTA_MEDICA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOME_TASASDI_FK2_I ON TASY.OFT_CONSULTA_MEDICA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_consulta_medica_aftupdate
before update ON TASY.OFT_CONSULTA_MEDICA for each row
declare

begin

if	(:new.dt_liberacao	is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_atendimento is not null)then
	executar_evento_agenda_atend(:new.nr_atendimento,'OFT',obter_estab_atend(:new.nr_atendimento),:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.OFT_CONSULTA_MEDICA_ATUAL
before insert or update ON TASY.OFT_CONSULTA_MEDICA for each row
declare


begin
if	(nvl(:old.DT_CONSULTA,sysdate+10) <> :new.DT_CONSULTA) and
	(:new.DT_CONSULTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_CONSULTA, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.OFT_CONSULTA_MEDICA_pend_atual
after insert or update or delete ON TASY.OFT_CONSULTA_MEDICA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
IE_LIB_OFTALMOLOGIA_w	varchar2(10);
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(IE_LIB_OFTALMOLOGIA)
into	IE_LIB_OFTALMOLOGIA_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

begin

	if	(inserting) or
		(updating) then

		if	(nvl(IE_LIB_OFTALMOLOGIA_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'OFT';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XOFT';
			end if;


			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
			end if;


		end if;
	elsif	(deleting) then
		ie_tipo_w := 'XOFT';
		Gerar_registro_pendente_PEP(ie_tipo_w, :old.nr_sequencia, obter_pessoa_atendimento(:old.nr_atendimento,'C'), :old.nr_atendimento, :old.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.oft_consulta_medica_before_del
before delete ON TASY.OFT_CONSULTA_MEDICA FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

update   OFT_CONSULTA_IMAGEM
set      NR_SEQ_CONS_MED = null
where    NR_SEQ_CONS_MED = :old.nr_sequencia;

update   OFT_CONSULTA_ANEXO
set      NR_SEQ_CONS_MED = null
where    NR_SEQ_CONS_MED = :old.nr_sequencia;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.OFT_CONSULTA_MEDICA ADD (
  CONSTRAINT OFTCOME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_CONSULTA_MEDICA ADD (
  CONSTRAINT OFTCOME_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCOME_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCOME_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT OFTCOME_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT OFTCOME_OFTRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.OFT_RETORNO (NR_SEQUENCIA),
  CONSTRAINT OFTCOME_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT OFTCOME_ATEPACI_FK2 
 FOREIGN KEY (NR_ATEND_ANT) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.OFT_CONSULTA_MEDICA TO NIVEL_1;

