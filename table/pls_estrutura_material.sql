ALTER TABLE TASY.PLS_ESTRUTURA_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ESTRUTURA_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ESTRUTURA_MATERIAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_EMPRESA             NUMBER(4)              NOT NULL,
  DS_ESTRUTURA           VARCHAR2(255 BYTE)     NOT NULL,
  NR_SEQ_SUPERIOR        NUMBER(10),
  IE_NIVEL               VARCHAR2(1 BYTE),
  CD_EXTERNO             VARCHAR2(20 BYTE),
  DS_ESTRUTURA_PESQUISA  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSESMAT_EMPRESA_FK_I ON TASY.PLS_ESTRUTURA_MATERIAL
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSESMAT_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSESMAT_ESTABEL_FK_I ON TASY.PLS_ESTRUTURA_MATERIAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSESMAT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSESMAT_PK ON TASY.PLS_ESTRUTURA_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSESMAT_PLSESMAT_FK_I ON TASY.PLS_ESTRUTURA_MATERIAL
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_estrutura_material_atual
before insert or update ON TASY.PLS_ESTRUTURA_MATERIAL for each row
declare

begin

if	(:new.ds_estrutura <> nvl(:old.ds_estrutura,'X')) then
	:new.ds_estrutura_pesquisa	:= padronizar_nome(:new.ds_estrutura);
end if;

end pls_estrutura_material_atual;
/


CREATE OR REPLACE TRIGGER TASY.pls_estrut_mat_atual_tot
before insert or update or delete ON TASY.PLS_ESTRUTURA_MATERIAL for each row
declare
begin

-- se alguma estrutura for apagada, seta para limpar a tabela que grava as estruturas e seus respectivos materiais
if	(deleting) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_ESTRUTURA_MATERIAL_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_ESTRUT_MAT_ATUAL_TOT',
								'nr_seq_estrutura_p=' || :old.nr_sequencia);

	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_ESTRUTURA_OCOR_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_ESTRUT_MAT_ATUAL_TOT',
								'nr_seq_estrutura_p=' || :old.nr_sequencia);
end if;

end pls_estrut_mat_atual_tot;
/


ALTER TABLE TASY.PLS_ESTRUTURA_MATERIAL ADD (
  CONSTRAINT PLSESMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ESTRUTURA_MATERIAL ADD (
  CONSTRAINT PLSESMAT_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PLSESMAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSESMAT_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_ESTRUTURA_MATERIAL TO NIVEL_1;

