ALTER TABLE TASY.PLS_PRESTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_CGC                       VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_PRESTADOR_RECEB       NUMBER(10),
  IE_TIPO_RELACAO              VARCHAR2(2 BYTE) NOT NULL,
  IE_ABRAMGE                   VARCHAR2(1 BYTE) NOT NULL,
  DT_CADASTRO                  DATE             NOT NULL,
  NR_SEQ_TIPO_PRESTADOR        NUMBER(10),
  NR_SEQ_CONTRATO              NUMBER(10),
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_LIMITE_INTEGRACAO         NUMBER(2),
  IE_GUIA_MEDICO               VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_COMPLEMENTO          NUMBER(2),
  IE_TIPO_ENDERECO             VARCHAR2(4 BYTE),
  IE_TIPO_VINCULO              VARCHAR2(2 BYTE) NOT NULL,
  QT_DIAS_PROTOCOLO            NUMBER(3),
  IE_TIPO_GUIA_MEDICO          VARCHAR2(2 BYTE),
  IE_REGRA_DATA_PRECO_PROC     VARCHAR2(1 BYTE),
  IE_REGRA_DATA_PRECO_MAT      VARCHAR2(1 BYTE),
  IE_TIPO_VALIDACAO_AUT_SOLIC  VARCHAR2(2 BYTE),
  QT_DIAS_ENVIO_CONTAS         NUMBER(5),
  IE_TIPO_PRESTADOR            VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO         NUMBER(10),
  NR_SEQ_ANTERIOR              NUMBER(10),
  SG_UF_SIP                    VARCHAR2(3 BYTE),
  IE_REGRA_DATA_PRECO_DIARIA   VARCHAR2(1 BYTE),
  IE_REGRA_DATA_PRECO_TAXAS    VARCHAR2(1 BYTE),
  IE_REGRA_DATA_PRECO_PACOTE   VARCHAR2(1 BYTE),
  IE_TIPO_PRODUTO_PTU          NUMBER(1),
  CD_PRESTADOR                 VARCHAR2(30 BYTE),
  DT_EXCLUSAO                  DATE,
  IE_TIPO_CLASSIF_PTU          NUMBER(1),
  IE_PRESTADOR_ALTO_CUSTO      VARCHAR2(1 BYTE) NOT NULL,
  IE_ACIDENTE_TRABALHO         VARCHAR2(1 BYTE) NOT NULL,
  IE_URGENCIA_EMERGENCIA       VARCHAR2(1 BYTE) NOT NULL,
  DT_INICIO_SERVICO            DATE,
  DT_INICIO_CONTRATO           DATE,
  IE_DISPONBILIDADE_SERV       VARCHAR2(3 BYTE),
  IE_TABELA_PROPRIA            VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_REGULAMENTACAO_PTU   NUMBER(1),
  IE_TIPO_REDE_MIN_PTU         NUMBER(1),
  NR_SEQ_COMPL_PF_TEL_ADIC     NUMBER(10),
  IE_DISPONIBILIDADE_SERV      VARCHAR2(3 BYTE),
  IE_CONSISTIR_COOPERADOS      VARCHAR2(3 BYTE),
  IE_PTU_A400                  VARCHAR2(1 BYTE) NOT NULL,
  IE_PERFIL_ASSIS_PTU          NUMBER(10),
  CD_PREST_A400                VARCHAR2(15 BYTE),
  NM_ADIC_GUIA_MEDICO          VARCHAR2(80 BYTE),
  NM_INTERNO                   VARCHAR2(80 BYTE),
  IE_PREFIXO_GUIA_MEDICO       VARCHAR2(10 BYTE),
  NR_SEQ_PREST_PRINC           NUMBER(10),
  IE_INSTITUICAO_ACRED_PTU     VARCHAR2(10 BYTE),
  IE_NIVEL_ACREDITACAO_PTU     NUMBER(2),
  NR_SEQ_COMPL_PJ              NUMBER(10),
  IE_PARTICIP_QUALISS_ANS      VARCHAR2(1 BYTE) NOT NULL,
  IE_VALOR_BASE_TRIB_NF        VARCHAR2(1 BYTE),
  IE_NOTIVISA                  VARCHAR2(1 BYTE),
  DT_FIM_CONTRATO              DATE,
  IE_FILIAL                    VARCHAR2(1 BYTE),
  IE_PUBLICA_ANS               VARCHAR2(1 BYTE),
  IE_RESIDENCIA_SAUDE          VARCHAR2(1 BYTE),
  CD_UNIMED_PRESTADORA         VARCHAR2(4 BYTE),
  IE_LOGIN_WSD_TISS            VARCHAR2(1 BYTE),
  IE_CADU                      VARCHAR2(1 BYTE),
  SG_CONSELHO                  VARCHAR2(12 BYTE),
  IE_DIVULGA_QUALIFICACAO      VARCHAR2(1 BYTE),
  IE_POS_GRAD_360              VARCHAR2(1 BYTE),
  IE_SITUACAO_PTU              VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_COMPL_ADIC       NUMBER(10),
  CD_PRESTADOR_EXTERNO         VARCHAR2(60 BYTE),
  IE_IND_GINEC_OBST            VARCHAR2(1 BYTE),
  IE_POSTO_COLETA              VARCHAR2(1 BYTE),
  IE_PRESTADOR_MATRIZ          VARCHAR2(1 BYTE),
  CD_OCORRENCIA_SEFIP          VARCHAR2(15 BYTE),
  CD_CATEGORIA_SEFIP           VARCHAR2(15 BYTE),
  CD_SISTEMA_ANT               VARCHAR2(255 BYTE),
  IE_TIPO_ENDERECO_PTU         NUMBER(1),
  NR_REFERENCIA_END            NUMBER(2),
  CD_PRESTADOR_UPPER           VARCHAR2(30 BYTE),
  CD_PRESTADOR_UPPER_ESPE      VARCHAR2(30 BYTE),
  NR_LATITUDE                  VARCHAR2(20 BYTE),
  NR_LONGITUDE                 VARCHAR2(20 BYTE),
  NM_BUSCA_GUIA_MEDICO         VARCHAR2(2000 BYTE),
  NR_TELEFONE_DOIS             NUMBER(12),
  IE_DIVULGA_EMAIL             VARCHAR2(2 BYTE),
  IE_DIVULGA_SITE              VARCHAR2(2 BYTE),
  QT_DIAS_RECURSO              NUMBER(3),
  NR_SEQ_FORNEC_MAT_FED        NUMBER(10),
  IE_INTERCAMBIO               VARCHAR2(5 BYTE),
  IE_DOUTORADO_POS             VARCHAR2(5 BYTE),
  IE_MESTRADO                  VARCHAR2(5 BYTE),
  CD_EXTERNO_ESOCIAL           VARCHAR2(60 BYTE),
  NR_SEQ_PREST_ANS             NUMBER(10),
  QT_DIAS_TREPLICA             NUMBER(3),
  NR_SEQ_SERVICO_URG_EMERG     NUMBER(10),
  IE_FONE_WHATS_GUIA_MEDICO    VARCHAR2(1 BYTE),
  NR_DDD_FONE_WHATSAPP         VARCHAR2(3 BYTE),
  NR_FONE_WHATSAPP             VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRES_CONTRAT_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRES_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRES_COPFTEA_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_COMPL_PF_TEL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRES_COPFTEA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRES_ESTABEL_FK_I ON TASY.PLS_PRESTADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_I1 ON TASY.PLS_PRESTADOR
(CD_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_I2 ON TASY.PLS_PRESTADOR
(CD_PRESTADOR_UPPER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_I3 ON TASY.PLS_PRESTADOR
(CD_PRESTADOR_UPPER_ESPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_I4 ON TASY.PLS_PRESTADOR
(IE_TIPO_RELACAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_I5 ON TASY.PLS_PRESTADOR
(IE_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PESFISI_FK_I ON TASY.PLS_PRESTADOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PESJUCP_FK_I ON TASY.PLS_PRESTADOR
(CD_CGC, NR_SEQ_COMPL_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PESJURI_FK_I ON TASY.PLS_PRESTADOR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRES_PK ON TASY.PLS_PRESTADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PLSCLPR_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PLSFMFS_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_FORNEC_MAT_FED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_PLSPRES_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_PRESTADOR_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRES_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRES_PLSPRES_FK2_I ON TASY.PLS_PRESTADOR
(NR_SEQ_PREST_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRES_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRES_PLSTIPR_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRES_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRES_PTUSRUE_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_SERVICO_URG_EMERG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRES_TIPCOAD_FK_I ON TASY.PLS_PRESTADOR
(NR_SEQ_TIPO_COMPL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ssj_inserir_produto_p
after insert
ON TASY.PLS_PRESTADOR for each row
declare

/*trigger criada para chamar a procedure que faz a inser��o de produtos 
em cadastro de prestador novos inseri todos produtos existente em 
ops - cadastro de produto*/ 


jobno    number;

begin
    
    if (:new.nr_sequencia is not null)then
        dbms_job.submit(jobno,'ssj_inserir_produto_prest('''||:new.nr_sequencia||''');'); 
    else 
        raise_application_error(-20011, 'Novo prestador n�o localizado!');     
    end if;
    
    
end ssj_inserir_produto_p;
/


CREATE OR REPLACE TRIGGER TASY.SSJ_INSERIR_COMPL_PRESTADOR
AFTER INSERT or update
ON TASY.PLS_PRESTADOR 

FOR EACH ROW
DECLARE

nr_seq_prestador_w number(10);
dt_inclusao_w date;
dt_inicio_servico_w date;
dt_inicio_vigencia_w date;
cd_pessoa_fisica_w varchar2(10);
cd_cgc_w varchar2(14);
ie_existe_w number(10);
ie_tipo_relacao_w varchar(2);

BEGIN
   
    nr_seq_prestador_w := :new.nr_sequencia;
    dt_inicio_servico_w := :new.dt_inicio_servico;
    dt_inclusao_w := :new.dt_cadastro;
    cd_pessoa_fisica_w := :new.cd_pessoa_fisica;
    cd_cgc_w := :new.cd_cgc;
    dt_inicio_vigencia_w:= nvl(nvl(dt_inicio_servico_w,dt_inclusao_w),to_date(sysdate));
    ie_tipo_relacao_w := :new.IE_TIPO_RELACAO;
    
    select count(nr_sequencia)
    into ie_existe_w
    from PLS_PRESTADOR_PAGTO
    where nr_seq_prestador = nr_seq_prestador_w;

            
    begin
    
        if(ie_existe_w = 0 and nr_seq_prestador_w is not null)then
               
        
            insert into PLS_PRESTADOR_PAGTO
            (
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_prestador,
                cd_condicao_pagamento,
                qt_dia_vencimento,
                ie_geracao_nota_titulo,
                ie_saldo_negativo,
                dt_inicio_vigencia,
                cd_cgc,
                cd_pessoa_fisica,
                ie_periodo_aprop_neg,
                ie_excecao_pag_negativo
            )
            values
            (
                PLS_PRESTADOR_PAGTO_SEQ.nextval,--nr_sequencia,
                sysdate,                        --dt_atualizacao,
                'TASY',                         --nm_usuario,
                sysdate,                        --dt_atualizacao_nrec,
                'TASY',                         --nm_usuario_nrec,
                nr_seq_prestador_w,             --nr_seq_prestador,
                1036,                           --cd_condicao_pagamento,
                25,                             --qt_dia_vencimento,
                'T',                            --ie_geracao_nota_titulo,
                'CP',                           --ie_saldo_negativo,
                 dt_inicio_vigencia_w,          --dt_inicio_vigencia,
                 cd_cgc_w,                      --cd_cgc,
                 cd_pessoa_fisica_w,            --cd_pessoa_fisica,
                 'N',                           --ie_periodo_aprop_neg,
                 'N'                            --ie_excecao_pag_negativo
            );
        
        end if;    
           
    end;
       
END SSJ_INSERIR_COMPL_PRESTADOR;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_atual
before insert or update ON TASY.PLS_PRESTADOR for each row
declare
nm_medico_ou_prestador_w	pls_prestador.nm_busca_guia_medico%type;
nm_prestador_w			varchar2(255);
nm_usual_w			varchar2(255);

begin

-- se for insert ou modificou o valor do campo cd_prestador atualiza os campos de redund�ncia formatados
if	((inserting) or
	 (nvl(:new.cd_prestador, 'A') != nvl(:old.cd_prestador, 'A'))) then

	-- converte para otimizar as buscas
	:new.cd_prestador_upper := upper(:new.cd_prestador);
	:new.cd_prestador_upper_espe := elimina_caractere_especial(upper(:new.cd_prestador));
end if;

if	((:new.cd_cgc <> :old.cd_cgc) or
	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) or
	(nvl(:new.ie_prefixo_guia_medico,'X') <> nvl(:old.ie_prefixo_guia_medico,'X')) or
	(nvl(:new.nm_adic_guia_medico,'X') <> nvl(:old.nm_adic_guia_medico,'X')) or
	(nvl(:new.nm_interno,'X') <> nvl(:old.nm_interno,'X'))) then
	if	(:new.cd_cgc is not null) then
		select	a.ds_razao_social,
			a.nm_fantasia
		into	nm_prestador_w,
			nm_usual_w
		from	pessoa_juridica a
		where	a.cd_cgc	= :new.cd_cgc;
	elsif	(:new.cd_pessoa_fisica is not null) then
		select	a.nm_pessoa_fisica,
			b.nm_guerra
		into	nm_prestador_w,
			nm_usual_w
		from	medico b,
			pessoa_fisica a
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
		and	a.cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	end if;

	nm_medico_ou_prestador_w	:= pls_obter_nome_busca_guia(:new.ie_prefixo_guia_medico,
								     nm_prestador_w,
								     nm_usual_w,
								     :new.nm_interno,
								     :new.nm_adic_guia_medico);

	:new.nm_busca_guia_medico	:= nm_medico_ou_prestador_w;
end if;

end pls_prestador_atual;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_pls_rps
after update ON TASY.PLS_PRESTADOR for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w 		varchar2(1) := 'N';
cd_cgc_cpf_old_w	pls_alt_prest_rps.cd_cgc_cpf_old%type;

begin
begin
	if	(nvl(:new.cd_cgc,'0')			!= nvl(:old.cd_cgc,'0')) or
		(nvl(:new.cd_pessoa_fisica,'0')		!= nvl(:old.cd_pessoa_fisica,'0')) or
		(nvl(:new.dt_inicio_servico,sysdate)	!= nvl(:old.dt_inicio_servico,sysdate))or
		(nvl(:new.dt_inicio_contrato,sysdate)	!= nvl(:old.dt_inicio_contrato,sysdate)) or
		(nvl(:new.ie_tipo_relacao,'0')		!= nvl(:old.ie_tipo_relacao,'0')) or
		(nvl(:new.ie_urgencia_emergencia,'0')	!= nvl(:old.ie_urgencia_emergencia,'0')) or
		(nvl(:new.ie_tipo_classif_ptu,0)	!= nvl(:old.ie_tipo_classif_ptu,0)) or
		(nvl(:new.ie_disponibilidade_serv,'0')	!= nvl(:old.ie_disponibilidade_serv,'0')) or
		(nvl(:new.sg_uf_sip,'0')		!= nvl(:old.sg_uf_sip,'0')) then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S') then
		if	(:new.cd_cgc is not null) or
			(:old.cd_cgc is not null) then
			cd_cgc_cpf_old_w := :old.cd_cgc;

		elsif	(:new.cd_pessoa_fisica is not null) or
			(:old.cd_pessoa_fisica is not null) then
			select	max(nr_cpf)
			into	cd_cgc_cpf_old_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
		end if;

		pls_gerar_alt_prest_rps( null, null, :new.nr_sequencia, cd_cgc_cpf_old_w, null, null, :new.nm_usuario, null, null, null);
	end if;
exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_tp  after update ON TASY.PLS_PRESTADOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_IND_GINEC_OBST,1,500);gravar_log_alteracao(substr(:old.IE_IND_GINEC_OBST,1,4000),substr(:new.IE_IND_GINEC_OBST,1,4000),:new.nm_usuario,nr_seq_w,'IE_IND_GINEC_OBST',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.SG_CONSELHO,1,500);gravar_log_alteracao(substr(:old.SG_CONSELHO,1,4000),substr(:new.SG_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'SG_CONSELHO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC,1,500);gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PRESTADOR_RECEB,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR_RECEB,1,4000),substr(:new.NR_SEQ_PRESTADOR_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR_RECEB',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_RELACAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ABRAMGE,1,500);gravar_log_alteracao(substr(:old.IE_ABRAMGE,1,4000),substr(:new.IE_ABRAMGE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABRAMGE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_MEDICO,1,4000),substr(:new.IE_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ENDERECO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ENDERECO,1,4000),substr(:new.IE_TIPO_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ENDERECO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_VINCULO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_VINCULO,1,4000),substr(:new.IE_TIPO_VINCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VINCULO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIMITE_INTEGRACAO,1,500);gravar_log_alteracao(substr(:old.DT_LIMITE_INTEGRACAO,1,4000),substr(:new.DT_LIMITE_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIMITE_INTEGRACAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DIAS_PROTOCOLO,1,500);gravar_log_alteracao(substr(:old.QT_DIAS_PROTOCOLO,1,4000),substr(:new.QT_DIAS_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_PROTOCOLO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_COMPLEMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_GUIA_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_GUIA_MEDICO,1,4000),substr(:new.IE_TIPO_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DIAS_RECURSO,1,500);gravar_log_alteracao(substr(:old.QT_DIAS_RECURSO,1,4000),substr(:new.QT_DIAS_RECURSO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_RECURSO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_DATA_PRECO_PROC,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_DATA_PRECO_PROC,1,4000),substr(:new.IE_REGRA_DATA_PRECO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA_PRECO_PROC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_DATA_PRECO_MAT,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_DATA_PRECO_MAT,1,4000),substr(:new.IE_REGRA_DATA_PRECO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA_PRECO_MAT',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_VALIDACAO_AUT_SOLIC,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_VALIDACAO_AUT_SOLIC,1,4000),substr(:new.IE_TIPO_VALIDACAO_AUT_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VALIDACAO_AUT_SOLIC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DIAS_ENVIO_CONTAS,1,500);gravar_log_alteracao(substr(:old.QT_DIAS_ENVIO_CONTAS,1,4000),substr(:new.QT_DIAS_ENVIO_CONTAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_ENVIO_CONTAS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR,1,4000),substr(:new.IE_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.SG_UF_SIP,1,500);gravar_log_alteracao(substr(:old.SG_UF_SIP,1,4000),substr(:new.SG_UF_SIP,1,4000),:new.nm_usuario,nr_seq_w,'SG_UF_SIP',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DISPONIBILIDADE_SERV,1,500);gravar_log_alteracao(substr(:old.IE_DISPONIBILIDADE_SERV,1,4000),substr(:new.IE_DISPONIBILIDADE_SERV,1,4000),:new.nm_usuario,nr_seq_w,'IE_DISPONIBILIDADE_SERV',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PERFIL_ASSIS_PTU,1,500);gravar_log_alteracao(substr(:old.IE_PERFIL_ASSIS_PTU,1,4000),substr(:new.IE_PERFIL_ASSIS_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_ASSIS_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PTU_A400,1,500);gravar_log_alteracao(substr(:old.IE_PTU_A400,1,4000),substr(:new.IE_PTU_A400,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A400',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_COOPERADOS,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_COOPERADOS,1,4000),substr(:new.IE_CONSISTIR_COOPERADOS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_COOPERADOS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_DATA_PRECO_DIARIA,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_DATA_PRECO_DIARIA,1,4000),substr(:new.IE_REGRA_DATA_PRECO_DIARIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA_PRECO_DIARIA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_DATA_PRECO_TAXAS,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_DATA_PRECO_TAXAS,1,4000),substr(:new.IE_REGRA_DATA_PRECO_TAXAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA_PRECO_TAXAS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_DATA_PRECO_PACOTE,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_DATA_PRECO_PACOTE,1,4000),substr(:new.IE_REGRA_DATA_PRECO_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_DATA_PRECO_PACOTE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PREST_A400,1,500);gravar_log_alteracao(substr(:old.CD_PREST_A400,1,4000),substr(:new.CD_PREST_A400,1,4000),:new.nm_usuario,nr_seq_w,'CD_PREST_A400',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_CLASSIF_PTU,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_CLASSIF_PTU,1,4000),substr(:new.IE_TIPO_CLASSIF_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CLASSIF_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PRESTADOR_ALTO_CUSTO,1,500);gravar_log_alteracao(substr(:old.IE_PRESTADOR_ALTO_CUSTO,1,4000),substr(:new.IE_PRESTADOR_ALTO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_ALTO_CUSTO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACIDENTE_TRABALHO,1,500);gravar_log_alteracao(substr(:old.IE_ACIDENTE_TRABALHO,1,4000),substr(:new.IE_ACIDENTE_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACIDENTE_TRABALHO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_URGENCIA_EMERGENCIA,1,500);gravar_log_alteracao(substr(:old.IE_URGENCIA_EMERGENCIA,1,4000),substr(:new.IE_URGENCIA_EMERGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_URGENCIA_EMERGENCIA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TABELA_PROPRIA,1,500);gravar_log_alteracao(substr(:old.IE_TABELA_PROPRIA,1,4000),substr(:new.IE_TABELA_PROPRIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TABELA_PROPRIA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_REGULAMENTACAO_PTU,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_REGULAMENTACAO_PTU,1,4000),substr(:new.IE_TIPO_REGULAMENTACAO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REGULAMENTACAO_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_REDE_MIN_PTU,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_REDE_MIN_PTU,1,4000),substr(:new.IE_TIPO_REDE_MIN_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REDE_MIN_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_PRODUTO_PTU,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PRODUTO_PTU,1,4000),substr(:new.IE_TIPO_PRODUTO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRODUTO_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_INSTITUICAO_ACRED_PTU,1,500);gravar_log_alteracao(substr(:old.IE_INSTITUICAO_ACRED_PTU,1,4000),substr(:new.IE_INSTITUICAO_ACRED_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_INSTITUICAO_ACRED_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NIVEL_ACREDITACAO_PTU,1,500);gravar_log_alteracao(substr(:old.IE_NIVEL_ACREDITACAO_PTU,1,4000),substr(:new.IE_NIVEL_ACREDITACAO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_NIVEL_ACREDITACAO_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_COMPL_PJ,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_COMPL_PJ,1,4000),substr(:new.NR_SEQ_COMPL_PJ,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COMPL_PJ',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FILIAL,1,500);gravar_log_alteracao(substr(:old.IE_FILIAL,1,4000),substr(:new.IE_FILIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILIAL',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ENDERECO_PTU,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ENDERECO_PTU,1,4000),substr(:new.IE_TIPO_ENDERECO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ENDERECO_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_POSTO_COLETA,1,4000),substr(:new.IE_POSTO_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_POSTO_COLETA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_PTU,1,4000),substr(:new.IE_SITUACAO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_PTU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_EXTERNO,1,4000),substr(:new.CD_PRESTADOR_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_EXTERNO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_COMPL_ADIC,1,4000),substr(:new.NR_SEQ_TIPO_COMPL_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_COMPL_ADIC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESTADOR_MATRIZ,1,4000),substr(:new.IE_PRESTADOR_MATRIZ,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_MATRIZ',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_OCORRENCIA_SEFIP,1,4000),substr(:new.CD_OCORRENCIA_SEFIP,1,4000),:new.nm_usuario,nr_seq_w,'CD_OCORRENCIA_SEFIP',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA_SEFIP,1,4000),substr(:new.CD_CATEGORIA_SEFIP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA_SEFIP',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_POS_GRAD_360,1,4000),substr(:new.IE_POS_GRAD_360,1,4000),:new.nm_usuario,nr_seq_w,'IE_POS_GRAD_360',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CADASTRO,1,4000),substr(:new.DT_CADASTRO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CADASTRO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LONGITUDE,1,4000),substr(:new.NR_LONGITUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_LONGITUDE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LATITUDE,1,4000),substr(:new.NR_LATITUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_LATITUDE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_BUSCA_GUIA_MEDICO,1,4000),substr(:new.NM_BUSCA_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'NM_BUSCA_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIVULGA_EMAIL,1,4000),substr(:new.IE_DIVULGA_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIVULGA_EMAIL',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIVULGA_SITE,1,4000),substr(:new.IE_DIVULGA_SITE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIVULGA_SITE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_UPPER,1,4000),substr(:new.CD_PRESTADOR_UPPER,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_UPPER',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_UPPER_ESPE,1,4000),substr(:new.CD_PRESTADOR_UPPER_ESPE,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_UPPER_ESPE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_DOIS,1,4000),substr(:new.NR_TELEFONE_DOIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_DOIS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REFERENCIA_END,1,4000),substr(:new.NR_REFERENCIA_END,1,4000),:new.nm_usuario,nr_seq_w,'NR_REFERENCIA_END',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ANTERIOR,1,4000),substr(:new.NR_SEQ_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ANTERIOR',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_INTERNO,1,4000),substr(:new.NM_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NM_INTERNO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PREFIXO_GUIA_MEDICO,1,4000),substr(:new.IE_PREFIXO_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREFIXO_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ADIC_GUIA_MEDICO,1,4000),substr(:new.NM_ADIC_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ADIC_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR,1,4000),substr(:new.CD_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EXCLUSAO,1,4000),substr(:new.DT_EXCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EXCLUSAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_SERVICO,1,4000),substr(:new.DT_INICIO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_SERVICO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_CONTRATO,1,4000),substr(:new.DT_INICIO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_CONTRATO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COMPL_PF_TEL_ADIC,1,4000),substr(:new.NR_SEQ_COMPL_PF_TEL_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COMPL_PF_TEL_ADIC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PREST_PRINC,1,4000),substr(:new.NR_SEQ_PREST_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PREST_PRINC',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PARTICIP_QUALISS_ANS,1,4000),substr(:new.IE_PARTICIP_QUALISS_ANS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARTICIP_QUALISS_ANS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR_BASE_TRIB_NF,1,4000),substr(:new.IE_VALOR_BASE_TRIB_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_BASE_TRIB_NF',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOTIVISA,1,4000),substr(:new.IE_NOTIVISA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOTIVISA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_CONTRATO,1,4000),substr(:new.DT_FIM_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_CONTRATO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIVULGA_QUALIFICACAO,1,4000),substr(:new.IE_DIVULGA_QUALIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIVULGA_QUALIFICACAO',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PUBLICA_ANS,1,4000),substr(:new.IE_PUBLICA_ANS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PUBLICA_ANS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESIDENCIA_SAUDE,1,4000),substr(:new.IE_RESIDENCIA_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESIDENCIA_SAUDE',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIMED_PRESTADORA,1,4000),substr(:new.CD_UNIMED_PRESTADORA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIMED_PRESTADORA',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOGIN_WSD_TISS,1,4000),substr(:new.IE_LOGIN_WSD_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOGIN_WSD_TISS',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CADU,1,4000),substr(:new.IE_CADU,1,4000),:new.nm_usuario,nr_seq_w,'IE_CADU',ie_log_w,ds_w,'PLS_PRESTADOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PRESTADOR ADD (
  CONSTRAINT PLSPRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR ADD (
  CONSTRAINT PLSPRES_PTUSRUE_FK 
 FOREIGN KEY (NR_SEQ_SERVICO_URG_EMERG) 
 REFERENCES TASY.PTU_SERVICO_URG_EMERG (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PLSFMFS_FK 
 FOREIGN KEY (NR_SEQ_FORNEC_MAT_FED) 
 REFERENCES TASY.PLS_FORNEC_MAT_FED_SC (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_TIPCOAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPL_ADIC) 
 REFERENCES TASY.TIPO_COMPLEMENTO_ADICIONAL (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_RECEB) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRES_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSPRES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPRES_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_COPFTEA_FK 
 FOREIGN KEY (NR_SEQ_COMPL_PF_TEL_ADIC) 
 REFERENCES TASY.COMPL_PF_TEL_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PREST_PRINC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRES_PESJUCP_FK 
 FOREIGN KEY (CD_CGC, NR_SEQ_COMPL_PJ) 
 REFERENCES TASY.PESSOA_JURIDICA_COMPL (CD_CGC,NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR TO APPSSJ;

GRANT SELECT ON TASY.PLS_PRESTADOR TO NIVEL_1;

