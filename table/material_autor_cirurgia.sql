ALTER TABLE TASY.MATERIAL_AUTOR_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_AUTOR_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_AUTOR_CIRURGIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_AUTORIZACAO       NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_MATERIAL              NUMBER(6)            NOT NULL,
  QT_MATERIAL              NUMBER(15,3)         NOT NULL,
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  VL_MATERIAL              NUMBER(15,2)         NOT NULL,
  IE_APROVACAO             VARCHAR2(1 BYTE)     NOT NULL,
  VL_UNITARIO_MATERIAL     NUMBER(17,4)         NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_CGC_FORNEC            VARCHAR2(14 BYTE),
  QT_SOLICITADA            NUMBER(15,3),
  NR_SEQ_FABRICANTE        NUMBER(10),
  IE_ORIGEM_PRECO          NUMBER(2),
  PR_ADICIONAL             NUMBER(15,4),
  NR_SEQ_MARCA             NUMBER(10),
  IE_VALOR_CONTA           VARCHAR2(1 BYTE),
  IE_VALOR_INFORMADO       VARCHAR2(1 BYTE),
  IE_GERADO_AGENDA         VARCHAR2(1 BYTE),
  CD_MATERIAL_CONVENIO     VARCHAR2(20 BYTE),
  DS_MAT_CONVENIO          VARCHAR2(255 BYTE),
  NR_SEQ_APRESENTACAO      NUMBER(10),
  NR_SEQ_OPME              NUMBER(10),
  NR_COT_COMPRA            NUMBER(10),
  NR_ITEM_COT_COMPRA       NUMBER(10),
  NR_SEQ_REGRA_PLANO       NUMBER(10),
  IE_REDUCAO_ACRESCIMO     VARCHAR2(1 BYTE),
  NR_SEQ_AGRUP_REL         NUMBER(5),
  NR_ORDEM_COMPRA          NUMBER(10),
  IE_FATURAMENTO_DIRETO    VARCHAR2(1 BYTE),
  DT_FATUR_DIRETO          DATE,
  NM_USUARIO_FATUR_DIRETO  VARCHAR2(15 BYTE),
  NR_ITEM_OCI              NUMBER(5),
  CD_SETOR_FATUR_DIRETO    NUMBER(5),
  NR_PRESCRICAO            NUMBER(14),
  NR_SEQ_PRESCRICAO        NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATAUCI_AUTCIRU_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(NR_SEQ_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUCI_MATERIA_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUCI_MATFABR_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(NR_SEQ_FABRICANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCI_MATFABR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUCI_MATMARC_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(CD_MATERIAL, NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCI_MATMARC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUCI_PESJURI_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(CD_CGC_FORNEC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATAUCI_PK ON TASY.MATERIAL_AUTOR_CIRURGIA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUCI_PRESMAT_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUCI_SETATEN_FK_I ON TASY.MATERIAL_AUTOR_CIRURGIA
(CD_SETOR_FATUR_DIRETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_autor_cirurgia_update
after update ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
declare

nr_sequencia_autor_w		number(10);
nr_seq_material_autor_w		number(10);
nr_seq_estagio_w		number(10);
ie_interno_w			varchar2(2);
ie_lib_materiais_esp_w		varchar2(1)	:= 'N';
ie_atualizando_autor_w		number(10);
ie_valor_conta_w		varchar2(15) := 'N';
ie_atualiza_valor_conta_w	varchar2(15) := 'S';
cd_estabelecimento_w		number(5);
ie_atualiza_dados_mat_autor_w	varchar2(1) := 'N';
ds_teste_w			varchar2(2000);
begin

select	nvl(max(ie_lib_materiais_esp),'N'),
	nvl(max(b.cd_estabelecimento),0)
into	ie_lib_materiais_esp_w,
	cd_estabelecimento_w
from	parametro_faturamento b,
	autorizacao_cirurgia a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= :new.nr_seq_autorizacao;

ie_atualiza_valor_conta_w 	:= nvl(obter_valor_param_usuario(3004, 148, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w),'S');
ie_atualiza_dados_mat_autor_w 	:= nvl(obter_valor_param_usuario(3006, 71, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w),'N');
if	(nvl(ie_lib_materiais_esp_w,'N') = 'N')  then

	select	max(nr_sequencia),
		max(nr_seq_estagio)
	into	nr_sequencia_autor_w,
		nr_seq_estagio_w
	from	autorizacao_convenio
	where	nr_seq_autor_cirurgia	= :new.nr_seq_autorizacao;

	if	(nr_sequencia_autor_w is null) then

		select	max(a.nr_seq_autor_conv),
			max(b.nr_seq_estagio)
		into	nr_sequencia_autor_w,
			nr_seq_estagio_w
		from	autorizacao_cirurgia a,
			autorizacao_convenio b
		where	a.nr_seq_autor_conv = b.nr_sequencia
		and	a.nr_sequencia	    = :new.nr_seq_autorizacao;

	end if;

	if	(nr_sequencia_autor_w is not null) then

		select	max(ie_interno)
		into	ie_interno_w
		from	estagio_autorizacao
		where	nr_sequencia	= nvl(nr_seq_estagio_w,0)
		and		OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

		select	max(nr_sequencia),
			nvl(max(ie_valor_conta),'N')
		into	nr_seq_material_autor_w,
			ie_valor_conta_w
		from	material_autorizado
		where	cd_material		= :old.cd_material
		and	nr_sequencia_autor	= nr_sequencia_autor_w;


		if	(nr_seq_material_autor_w is not null) and
			((ie_atualiza_dados_mat_autor_w = 'N') and
			(ie_interno_w in ('1','2') or nr_seq_estagio_w is null)) or
			(ie_atualiza_dados_mat_autor_w = 'S') then


			select	count(*)
			into	ie_atualizando_autor_w
			from	v$session
			where	audsid		= (select userenv('sessionid') from dual)
			and	upper(action) 	like 'ATUALIZAR_AUTORIZACAO_CONVENIO%';

			/* Francisco - 11/09/2009 - OS 165733 - S� fazer esse update quando n�o estiver mudando est�gio da autoriza��o */
			if	(ie_atualizando_autor_w = 0) and
				((ie_atualiza_valor_conta_w = 'S') or
				((ie_atualiza_valor_conta_w = 'N') and
				(nvl(ie_valor_conta_w,'N') = 'N'))) then

				update	material_autorizado
				set	cd_material	= :new.cd_material,
					vl_unitario	= :new.vl_unitario_material,
					qt_solicitada	= nvl(:new.qt_solicitada,qt_solicitada),
					qt_autorizada	= nvl(:new.qt_material,0),
					ds_observacao	= substr(:new.ds_observacao,1,2000),
					cd_cgc_fabricante	= decode(:new.cd_cgc_fornec,null,cd_cgc_fabricante,:new.cd_cgc_fornec),
					nr_seq_fabricante	= :new.nr_seq_fabricante,
					pr_adicional		= :new.pr_adicional,
					ie_reducao_acrescimo	= :new.ie_reducao_acrescimo,
					ie_valor_conta		= :new.ie_valor_conta,
					nr_seq_opme		= :new.nr_seq_opme,
					ie_origem_preco		= :new.ie_origem_preco,
					nr_seq_marca		= :new.nr_seq_marca,
					ie_faturamento_direto 	= :new.ie_faturamento_direto,
					dt_fatur_direto		= :new.dt_fatur_direto,
					nm_usuario_fatur_direto	= :new.nm_usuario_fatur_direto,
					dt_atualizacao 		= sysdate,
					nm_usuario		= :new.nm_usuario
				where	nr_sequencia	= nr_seq_material_autor_w;
			end if;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.mat_autor_cirur_before_delete
before delete ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
declare

ie_permite_mat_especiais_w	Varchar2(1);
qt_registro_w			number(10);

begin

begin

obter_param_usuario(871, 333, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_permite_mat_especiais_w);

if	(ie_permite_mat_especiais_w = 'N') and
	(:old.ie_gerado_agenda = 'S') then
	select 	count(*)
	into	qt_registro_w
	from	autorizacao_cirurgia
	where	nr_seq_agenda is not null
	and	nr_sequencia 		= :old.nr_seq_autorizacao;

	if	(qt_registro_w > 0) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267483);
	end if;
end if;


exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.MATERIAL_AUTOR_CIRURGIA_tp
after update ON TASY.MATERIAL_AUTOR_CIRURGIA FOR EACH ROW
DECLARE

nr_seq_w number(10);
ds_s_w   varchar2(50);
ds_c_w   varchar2(500);
ds_w	   varchar2(500);
ie_log_w varchar2(1);

begin

begin

ds_s_w := to_char(:old.NR_SEQUENCIA);
ds_c_w:=null;

ds_w:=substr(:new.CD_CGC_FORNEC,1,500);
gravar_log_alteracao(substr(:old.CD_CGC_FORNEC,1,4000),substr(:new.CD_CGC_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_FORNEC',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.CD_MATERIAL,1,500);
gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_ORIGEM_PRECO,1,500);
gravar_log_alteracao(substr(:old.IE_ORIGEM_PRECO,1,4000),substr(:new.IE_ORIGEM_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PRECO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.NR_SEQ_FABRICANTE,1,500);
gravar_log_alteracao(substr(:old.NR_SEQ_FABRICANTE,1,4000),substr(:new.NR_SEQ_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FABRICANTE',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.PR_ADICIONAL,1,500);
gravar_log_alteracao(substr(:old.PR_ADICIONAL,1,4000),substr(:new.PR_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'PR_ADICIONAL',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.NR_SEQ_MARCA,1,500);
gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_VALOR_CONTA,1,500);
gravar_log_alteracao(substr(:old.IE_VALOR_CONTA,1,4000),substr(:new.IE_VALOR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_CONTA',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_GERADO_AGENDA,1,500);
gravar_log_alteracao(substr(:old.IE_GERADO_AGENDA,1,4000),substr(:new.IE_GERADO_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERADO_AGENDA',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_VALOR_INFORMADO,1,500);
gravar_log_alteracao(substr(:old.IE_VALOR_INFORMADO,1,4000),substr(:new.IE_VALOR_INFORMADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_INFORMADO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.DS_OBSERVACAO,1,500);
gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.NR_SEQ_AUTORIZACAO,1,500);
gravar_log_alteracao(substr(:old.NR_SEQ_AUTORIZACAO,1,4000),substr(:new.NR_SEQ_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AUTORIZACAO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.QT_MATERIAL,1,500);
gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.VL_MATERIAL,1,500);
gravar_log_alteracao(substr(:old.VL_MATERIAL,1,4000),substr(:new.VL_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_MATERIAL',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.VL_UNITARIO_MATERIAL,1,500);
gravar_log_alteracao(substr(:old.VL_UNITARIO_MATERIAL,1,4000),substr(:new.VL_UNITARIO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNITARIO_MATERIAL',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_APROVACAO,1,500);
gravar_log_alteracao(substr(:old.IE_APROVACAO,1,4000),substr(:new.IE_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APROVACAO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

ds_w:=substr(:new.QT_SOLICITADA,1,500);
gravar_log_alteracao(substr(:old.QT_SOLICITADA,1,4000),substr(:new.QT_SOLICITADA,1,4000),:new.nm_usuario,nr_seq_w,'QT_SOLICITADA',ie_log_w,ds_w,'MATERIAL_AUTOR_CIRURGIA',ds_s_w,ds_c_w);

exception when others then

ds_w:= '1';

end;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_autor_cirurgia_delete
after delete ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
declare

nr_sequencia_autor_w	number(10);
nr_seq_material_autor_w	number(10);
nr_seq_estagio_w	number(10);
ie_interno_w		varchar2(2);
ie_lib_materiais_esp_w	varchar2(1)	:= 'N';

begin

begin

select	nvl(max(ie_lib_materiais_esp),'N')
into	ie_lib_materiais_esp_w
from	parametro_faturamento b,
	autorizacao_cirurgia a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= :old.nr_seq_autorizacao;

if	(nvl(ie_lib_materiais_esp_w,'N') = 'N')  then

	select	max(nr_sequencia),
		max(nr_seq_estagio)
	into	nr_sequencia_autor_w,
		nr_seq_estagio_w
	from	autorizacao_convenio
	where	nr_seq_autor_cirurgia	= :old.nr_seq_autorizacao;

	if	(nr_sequencia_autor_w is not null) then

		select	max(ie_interno)
		into	ie_interno_w
		from	estagio_autorizacao
		where	nr_sequencia	= nvl(nr_seq_estagio_w,0)
		and		OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

		select	max(nr_sequencia)
		into	nr_seq_material_autor_w
		from	material_autorizado
		where	cd_material		= :old.cd_material
		and	nr_sequencia_autor	= nr_sequencia_autor_w;

		if	(nr_seq_material_autor_w is not null) and
			(ie_interno_w = '1' or nr_seq_estagio_w is null) then

			delete	from	material_autorizado
			where	nr_sequencia	= nr_seq_material_autor_w;
		end if;
	end if;
end if;

exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_autor_cirurgia_insert
after insert ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
declare

nr_sequencia_autor_w	number(10);
cont_w			number(5);
ie_lib_materiais_esp_w	varchar2(1)	:= 'N';
nr_seq_autor_conv_w	number(10);
ie_gera_hist_item_w	varchar2(5) := 'N';

begin

if	wheb_usuario_pck.get_ie_executar_trigger = 'S' then


	obter_param_usuario(3006,50,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.Get_cd_estabelecimento,ie_gera_hist_item_w);


	select	nvl(max(ie_lib_materiais_esp),'N'),
		max((select max(x.nr_sequencia) from autorizacao_convenio x where x.nr_sequencia = a.nr_seq_autor_conv)) nr_seq_autor_conv
	into	ie_lib_materiais_esp_w,
		nr_seq_autor_conv_w
	from	parametro_faturamento b,
		autorizacao_cirurgia a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.nr_sequencia		= :new.nr_seq_autorizacao;

	if	(nvl(ie_lib_materiais_esp_w,'N') = 'N') then

		select	max(nr_sequencia)
		into	nr_sequencia_autor_w
		from	autorizacao_convenio
		where	nr_seq_autor_cirurgia	= :new.nr_seq_autorizacao;

		if	(nr_sequencia_autor_w is not null) or
			(nr_seq_autor_conv_w is not null)then

			select	count(*)
			into	cont_w
			from	material_autorizado
			where	nr_sequencia_autor	= nvl(nr_sequencia_autor_w,nr_seq_autor_conv_w)
			and	cd_material		= :new.cd_material
			and	((nr_seq_opme is null) or
				(nr_seq_opme		= :new.nr_seq_opme));

			if	(cont_w = 0) then

				insert	into	material_autorizado
					(nr_sequencia,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nr_sequencia_autor,
					cd_material,
					qt_solicitada,
					qt_autorizada,
					vl_unitario,
					ds_observacao,
					cd_cgc_fabricante,
					nr_seq_fabricante,
					pr_adicional,
					ie_valor_conta,
					nr_seq_opme,
					nr_seq_regra_plano,
					ie_reducao_acrescimo)
				values	(material_autorizado_seq.nextval,
					:new.nm_usuario,
					:new.nm_usuario,
					:new.dt_atualizacao,
					:new.dt_atualizacao,
					nvl(nr_sequencia_autor_w,nr_seq_autor_conv_w),
					:new.cd_material,
					:new.qt_solicitada,
					0,
					:new.vl_unitario_material,
					substr(:new.ds_observacao,1,2000),
					:new.cd_cgc_fornec,
					:new.nr_seq_fabricante,
					:new.pr_adicional,
					:new.ie_valor_conta,
					:new.nr_seq_opme,
					:new.nr_seq_regra_plano,
					:new.ie_reducao_acrescimo);
			end if;
		end if;
	end if;

	if	(nvl(ie_gera_hist_item_w,'N') = 'S') then


		insert into autorizacao_cirurgia_hist(
			ds_historico,
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_liberacao,
			nm_usuario,
			nm_usuario_lib,
			nm_usuario_nrec,
			nr_seq_autor_cirurgia,
			nr_sequencia)
		values	(
			substr(wheb_mensagem_pck.get_texto(311758) || chr(13) || chr(10) ||
			:new.cd_material || ' - ' || substr(obter_desc_material(:new.cd_material),1,100) || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(311521) || :new.qt_solicitada,1,4000),
			sysdate,
			sysdate,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			:new.nm_usuario,
			:new.nr_seq_autorizacao,
			autorizacao_cirurgia_hist_seq.nextval);
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.material_autor_cir_beforinsert
before insert ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
declare

cd_convenio_w		number(10)	:= null;
cd_estabelecimento_w	number(10);
cd_grupo_w		varchar2(20)	:= '';
cd_mat_convenio_w	varchar2(20)	:= '';
ds_mat_convenio_w	varchar2(255)	:= '';
ie_tipo_atendimento_w	number(10);
nr_atendimento_w	number(10);
nr_atendimento_autor_w	number(10);
nr_seq_agenda_w		number(10);
nr_seq_autor_conv_w	number(10);
nr_seq_conversao_w	number(10);
cd_empresa_ref_w	number(10,0);
ie_carater_inter_sus_w	varchar2(2);
ie_clinica_w		atendimento_paciente.ie_clinica%type;

begin

select	max(a.nr_seq_autor_conv),
	max(a.nr_seq_agenda),
	max(a.nr_atendimento)
into	nr_seq_autor_conv_w,
	nr_seq_agenda_w,
	nr_atendimento_w
from	autorizacao_cirurgia a
where	a.nr_sequencia		= :new.nr_seq_autorizacao;


if	(nr_seq_autor_conv_w is not null) then

	select	max(a.cd_convenio),
		max(a.nr_atendimento),
		max(to_number(obter_dados_autor_convenio(a.nr_sequencia, 'E')))
	into	cd_convenio_w,
		nr_atendimento_autor_w,
		cd_estabelecimento_w
	from	autorizacao_convenio a
	where	a.nr_sequencia		= nr_seq_autor_conv_w;

	if	(nr_atendimento_autor_w is not null) then
		select	max(ie_tipo_atendimento)
		into	ie_tipo_atendimento_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_w;
	end if;
end if;

if	(cd_convenio_w is null) and
	(nr_atendimento_w is not null) then

	select	max(b.cd_convenio),
		max(a.nr_atendimento),
		max(a.cd_estabelecimento),
		max(a.ie_tipo_atendimento),
		nvl(max(b.cd_empresa),0),
		max(a.ie_carater_inter_sus),
		max(a.ie_clinica)
	into	cd_convenio_w,
		nr_atendimento_autor_w,
		cd_estabelecimento_w,
		ie_tipo_atendimento_w,
		cd_empresa_ref_w,
		ie_carater_inter_sus_w,
		ie_clinica_w
	from	atend_categoria_convenio b,
		atendimento_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	b.nr_seq_interno	= Obter_Atecaco_atendimento(a.nr_atendimento)
	and 	a.nr_atendimento	= nr_atendimento_w;

end if;

if	(cd_convenio_w is null) and
	(nr_seq_agenda_w is not null) then

	select	max(a.cd_convenio),
		max(a.ie_tipo_atendimento),
		max(obter_estab_agenda(a.cd_agenda))
	into	cd_convenio_w,
		ie_tipo_atendimento_w,
		cd_estabelecimento_w
	from 	agenda_paciente a
	where	a.nr_sequencia		= nr_seq_agenda_w;
end if;


if	(:new.cd_material is not null) and
	(cd_convenio_w is not null) then

	converte_material_convenio
		(cd_convenio_w,
		:new.cd_material,
		null,
		null,
		:new.cd_cgc_fornec,
		cd_estabelecimento_w,
		sysdate,
		null,
		cd_mat_convenio_w,
		cd_grupo_w,
		nr_seq_conversao_w,
		ie_tipo_atendimento_w,
		:new.ie_origem_preco,
		cd_empresa_ref_w,
		null,
		ie_carater_inter_sus_w,
		null,
		null,
		ie_clinica_w,
		null);
end if;

if	(cd_mat_convenio_w is not null) then

	:new.cd_material_convenio	:= cd_mat_convenio_w;

	select	max(obter_desc_mat_convenio(cd_mat_convenio_w, cd_convenio_w))
	into	ds_mat_convenio_w
	from	dual;

	:new.ds_mat_convenio		:= ds_mat_convenio_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MATERIAL_AUTOR_CIR_BF_INSERT
before insert ON TASY.MATERIAL_AUTOR_CIRURGIA for each row
begin

:NEW.IE_FATURAMENTO_DIRETO := 'N';

end MATERIAL_AUTOR_CIR_BF_INSERT;
/


ALTER TABLE TASY.MATERIAL_AUTOR_CIRURGIA ADD (
  CONSTRAINT MATAUCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_AUTOR_CIRURGIA ADD (
  CONSTRAINT MATAUCI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_FATUR_DIRETO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MATAUCI_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT MATAUCI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATAUCI_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNEC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATAUCI_AUTCIRU_FK 
 FOREIGN KEY (NR_SEQ_AUTORIZACAO) 
 REFERENCES TASY.AUTORIZACAO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATAUCI_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRICANTE) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MATAUCI_MATMARC_FK 
 FOREIGN KEY (CD_MATERIAL, NR_SEQ_MARCA) 
 REFERENCES TASY.MATERIAL_MARCA (CD_MATERIAL,NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_AUTOR_CIRURGIA TO NIVEL_1;

