ALTER TABLE TASY.LOG_CLASSIFICACAO_SP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_CLASSIFICACAO_SP CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_CLASSIFICACAO_SP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_VERSAO            VARCHAR2(10 BYTE),
  NR_SERVICE_PACK      VARCHAR2(25 BYTE),
  DS_ALTERACAO         VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LCSP_PK ON TASY.LOG_CLASSIFICACAO_SP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOG_CLASSIFICACAO_SP ADD (
  CONSTRAINT LCSP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

