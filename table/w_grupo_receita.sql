ALTER TABLE TASY.W_GRUPO_RECEITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_GRUPO_RECEITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_GRUPO_RECEITA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_TIPO_VALOR         NUMBER(2)               NOT NULL,
  NR_SEQ_GRUPO_REC      NUMBER(10)              NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  VL_GRUPO              NUMBER(15,2),
  CD_CONVENIO           NUMBER(5),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WGRUREC_PK ON TASY.W_GRUPO_RECEITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WGRUREC_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_GRUPO_RECEITA ADD (
  CONSTRAINT WGRUREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_GRUPO_RECEITA TO NIVEL_1;

