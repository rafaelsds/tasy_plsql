ALTER TABLE TASY.FUNCAO_REGRA_GRID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_REGRA_GRID CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_REGRA_GRID
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4),
  NM_GRID             VARCHAR2(255 BYTE)        NOT NULL,
  CD_FUNCAO           NUMBER(5),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_PERFIL           NUMBER(5),
  DS_ORDEM_GRID       VARCHAR2(4000 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUREGRI_ESTABEL_FK_I ON TASY.FUNCAO_REGRA_GRID
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUREGRI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FUREGRI_FUNCAO_FK_I ON TASY.FUNCAO_REGRA_GRID
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUREGRI_I1 ON TASY.FUNCAO_REGRA_GRID
(CD_FUNCAO, CD_PERFIL, UPPER("NM_GRID"))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUREGRI_PERFIL_FK_I ON TASY.FUNCAO_REGRA_GRID
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUREGRI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FUREGRI_PK ON TASY.FUNCAO_REGRA_GRID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUREGRI_PK
  MONITORING USAGE;


ALTER TABLE TASY.FUNCAO_REGRA_GRID ADD (
  CONSTRAINT FUREGRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_REGRA_GRID ADD (
  CONSTRAINT FUREGRI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FUREGRI_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT FUREGRI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.FUNCAO_REGRA_GRID TO NIVEL_1;

