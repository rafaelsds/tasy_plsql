ALTER TABLE TASY.PLS_NOTIF_REGRA_INCID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_NOTIF_REGRA_INCID CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_NOTIF_REGRA_INCID
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_REGRA_GERACAO     NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  QT_INTERVALO_INCIDENCIA  NUMBER(5)            NOT NULL,
  NR_SEQ_TIPO_NOTIFICACAO  NUMBER(10)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSNRIN_ESTABEL_FK_I ON TASY.PLS_NOTIF_REGRA_INCID
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSNRIN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSNRIN_PK ON TASY.PLS_NOTIF_REGRA_INCID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSNRIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSNRIN_PLSNCTL_FK_I ON TASY.PLS_NOTIF_REGRA_INCID
(NR_SEQ_TIPO_NOTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSNRIN_PLSNCTL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSNRIN_PLSNORE_FK_I ON TASY.PLS_NOTIF_REGRA_INCID
(NR_SEQ_REGRA_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSNRIN_PLSNORE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_NOTIF_REGRA_INCID ADD (
  CONSTRAINT PLSNRIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_NOTIF_REGRA_INCID ADD (
  CONSTRAINT PLSNRIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSNRIN_PLSNCTL_FK 
 FOREIGN KEY (NR_SEQ_TIPO_NOTIFICACAO) 
 REFERENCES TASY.PLS_NOTIF_TIPO_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSNRIN_PLSNORE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GERACAO) 
 REFERENCES TASY.PLS_NOTIFICACAO_REGRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_NOTIF_REGRA_INCID TO NIVEL_1;

