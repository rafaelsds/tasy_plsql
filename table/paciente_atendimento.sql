ALTER TABLE TASY.PACIENTE_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATENDIMENTO
(
  NR_SEQ_ATENDIMENTO          NUMBER(10)        NOT NULL,
  NR_SEQ_PACIENTE             NUMBER(10)        NOT NULL,
  NR_CICLO                    NUMBER(3)         NOT NULL,
  DS_DIA_CICLO                VARCHAR2(5 BYTE)  NOT NULL,
  DT_PREVISTA                 DATE              NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_REAL                     DATE,
  QT_PESO                     VARCHAR2(6 BYTE),
  QT_SUPERF_CORPORAL          NUMBER(15,4),
  NR_PRESCRICAO               NUMBER(14),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  NR_SEQ_PROTOCOLO            NUMBER(6),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  IE_EXIGE_LIBERACAO          VARCHAR2(1 BYTE)  NOT NULL,
  DT_CHEGADA                  DATE,
  DT_LIBERACAO                DATE,
  DT_INICIO_ADM               DATE,
  DT_FIM_ADM                  DATE,
  NR_SEQ_LOCAL                NUMBER(10),
  DT_DISPENSACAO_MEDIC        DATE,
  DT_ESTORNO_CHEGADA          DATE,
  NM_USUARIO_ESTORNO_CHEGADA  VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10),
  DT_CANCELAMENTO             DATE,
  NM_USUARIO_CANCEL           VARCHAR2(15 BYTE),
  NM_USUARIO_CHECK_LIB        VARCHAR2(15 BYTE),
  DT_CHECK_LIB                DATE,
  QT_ALTURA                   NUMBER(3),
  DT_ACOMODACAO_PACIENTE      DATE,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_GERACAO_PRESCRICAO       DATE,
  DT_SUSPENSAO                DATE,
  NM_USUARIO_SUSPENSAO        VARCHAR2(15 BYTE),
  DT_ANTERIOR                 DATE,
  DT_INICIO_TRIAGEM           DATE,
  DT_FIM_TRIAGEM              DATE,
  DT_RETIRADA_MEDICACAO       DATE,
  DT_LIBERACAO_ATEND          DATE,
  NM_USUARIO_LIB_ATEND        VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA_CONS          NUMBER(10),
  QT_AUC                      NUMBER(4,1),
  QT_CREATININA               NUMBER(5,2),
  IE_L_DL_CREATININA          NUMBER(1),
  QT_CLEARANCE_CREATININA     NUMBER(14,2),
  QT_MG_CARBOPLATINA          NUMBER(15,4),
  NR_SEQ_PEND_AGENDA          NUMBER(10),
  NM_USUARIO_FIM_ADM          VARCHAR2(15 BYTE),
  DT_AGUARD_RESULT_EXAME      DATE,
  DT_QUIMIO_LIBERADA          DATE,
  CD_ESTABELECIMENTO          NUMBER(4),
  QT_TEMPO_MEDIC              NUMBER(10),
  DT_RECEBIMENTO_MEDIC        DATE,
  NM_USUARIO_RECEB            VARCHAR2(15 BYTE),
  DT_IMPRESSAO                DATE,
  PR_REDUCAO                  VARCHAR2(3 BYTE),
  DT_SAIDA_PREVISTO           DATE,
  NR_SEQ_LOCAL_PREVISTO       NUMBER(10),
  CD_ENFERMAGEM               VARCHAR2(10 BYTE),
  DT_APTO                     DATE,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  QT_TEMPO_AGENDA             NUMBER(10),
  DT_CHAMADA_PAINEL           DATE,
  DT_CHEGADA_PAINEL           DATE,
  NR_SEQ_MOTIVO_SUSP          NUMBER(10),
  NR_SEQ_MOTIVO_ALTER_DATA    NUMBER(10),
  DT_SOLIC_TRANSF             DATE,
  DS_SOLIC_TRANSF             VARCHAR2(255 BYTE),
  NM_USUARIO_INICIO_ADM       VARCHAR2(15 BYTE),
  NM_USUARIO_ALTER_DATA_PREV  VARCHAR2(15 BYTE),
  DT_ALTER_DATA_PREV          DATE,
  DT_PREVISTA_AGENDA          DATE,
  NM_USUARIO_SUSP_MEDICO      VARCHAR2(15 BYTE),
  DT_SUSPENSAO_MEDICO         DATE,
  DS_DIA_CICLO_REAL           VARCHAR2(5 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  DT_INTERROMPIDO             DATE,
  NM_USUARIO_INTERROMPIDO     VARCHAR2(15 BYTE),
  DT_ENTREGA_MEDICACAO        DATE,
  NM_USUARIO_ENTREGUA         VARCHAR2(15 BYTE),
  DT_RETIRADA_PRE_MEDICACAO   DATE,
  NM_RETIRADA_PRE_MEDICACAO   VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_BLOQ          NUMBER(10),
  DS_BLOQ_TRAT_QUIMIO         VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO   NUMBER(10),
  DT_LIBERACAO_REG            DATE,
  DT_NAO_APTO                 DATE,
  IE_REAGENDAMENTO            VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_FALTA         NUMBER(10),
  DT_FALTA                    DATE,
  DS_MOTIVO_FALTA             VARCHAR2(255 BYTE),
  NR_SEQ_JUST_ATRASO          NUMBER(10),
  DS_JUST_PRESCRICAO          VARCHAR2(255 BYTE),
  DT_SAIDA_PAINEL             DATE,
  NM_USUARIO_CHEGADA          VARCHAR2(15 BYTE),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE),
  NR_SEQ_PAC_SENHA_FILA       NUMBER(10),
  IE_CHECAGEM_EXAME           VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_SOLIC_PERDA   NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  IE_ALOCADO                  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACATEN_AGECONS_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_AGECONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_ATCONSPEPA_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_ATEPACI_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACATEND_PK ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_ESTABEL_FK_I ON TASY.PACIENTE_ATENDIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_I1 ON TASY.PACIENTE_ATENDIMENTO
(DT_PREVISTA, DT_REAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_I2 ON TASY.PACIENTE_ATENDIMENTO
(DT_REAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_I2
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_MEDICO_FK_I ON TASY.PACIENTE_ATENDIMENTO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_MOALDAQ_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_MOTIVO_ALTER_DATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_MOALDAQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_MOBLTRQ_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_MOTIVO_BLOQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_MOBLTRQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_MOTSUSQ_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_MOTSUSQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_PACSEFI_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_PACSETO_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_PACIENTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_PESFISI_FK_I ON TASY.PACIENTE_ATENDIMENTO
(CD_ENFERMAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_PRESMED_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_QTLOCAL_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_QTLOCAL_FK2_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_LOCAL_PREVISTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_QTLOCAL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_QTMTFAL_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_MOTIVO_FALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_QTMTFAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_QTPENAG_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_PEND_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATEN_QUJUATR_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_JUST_ATRASO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_QUJUATR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_SETATEN_FK_I ON TASY.PACIENTE_ATENDIMENTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_TASASDI_FK_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATEN_TASASDI_FK2_I ON TASY.PACIENTE_ATENDIMENTO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATEN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATENDIMENTO_DELETE
before delete ON TASY.PACIENTE_ATENDIMENTO for each row
declare

dt_cancelamento_w		date;
dt_atual_w			date;
nr_seq_quimio_w			number(10,0);
nr_seq_autor_w			number(10,0) := 0;
nr_seq_estagio_w		number(10);
ie_cancela_autor_w		varchar2(1);
nm_usuario_w			varchar2(15) := wheb_usuario_pck.Get_Nm_Usuario;
nr_seq_autor_dia_w		number(10,0);
pragma autonomous_transaction;
begin

if (:old.nr_seq_agenda_cons is not null) then

	select	dt_agenda
	into	dt_atual_w
	from 	agenda_consulta
	where	nr_sequencia	= :old.nr_seq_agenda_cons;


	select	/*+ INDEX(AGENDA_CONSULTA AGECONS_I4) */
		nvl(max(dt_agenda), dt_atual_w)
	into	dt_cancelamento_w
	from	agenda_consulta
	where	trunc(dt_agenda, 'mi') = trunc(dt_atual_w, 'mi');

	update	agenda_consulta
	set	ie_status_agenda	= 'C',
		dt_agenda		= dt_cancelamento_w + 1/86400,
		nm_usuario		= :old.nm_usuario,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= :old.nr_seq_agenda_cons;
end if;

select	max(nvl(nr_sequencia, 0))
into	nr_seq_quimio_w
from	agenda_quimio
where	nr_seq_atendimento = :old.nr_seq_atendimento;

if	(nr_seq_quimio_w > 0) then

	update	agenda_quimio
	set	ie_status_agenda	= 'C',
		dt_cancelada		= sysdate,
		nm_usuario		= :old.nm_usuario,
		dt_atualizacao		= sysdate,
		--ds_observacao		= 'Dia exclu�do'
		ds_observacao		= wheb_mensagem_pck.get_texto(309988)
	where	nr_sequencia		= nr_seq_quimio_w;

	update	agenda_quimio
	set	nr_seq_atendimento = null
	where	nr_sequencia = nr_seq_quimio_w;

end if;

delete	agenda_quimio_marcacao
where	nr_seq_atendimento = :old.nr_seq_atendimento;

select	nvl(max(a.nr_sequencia),0)
into	nr_seq_autor_dia_w
from	autorizacao_convenio a
where	nr_seq_paciente = :old.nr_seq_atendimento;


begin
select	a.nr_sequencia
into	nr_seq_autor_w
from	autorizacao_convenio a
where	a.nr_seq_paciente_setor = :old.nr_seq_paciente
and	a.nr_ciclo	  	= :old.nr_ciclo
and	a.nr_seq_paciente is null
and	nvl(nr_seq_autor_dia_w,0) = 0
and	rownum  = 1
and	not exists(	select 	1
			from	paciente_atendimento x
			where	x.nr_ciclo = a.nr_ciclo
			and	x.nr_seq_paciente = a.nr_seq_paciente_setor
			and	x.ds_dia_ciclo <> :old.ds_dia_ciclo);
exception
when others then
	nr_seq_autor_w := 0;
end;

obter_param_usuario(281, 1428, obter_perfil_ativo, :old.nm_usuario, :old.cd_estabelecimento, ie_cancela_autor_w);

begin
select	a.nr_sequencia
into	nr_seq_estagio_w
from	estagio_autorizacao a
where	a.cd_empresa	= obter_empresa_estab(:old.cd_estabelecimento)
and	a.ie_situacao	= 'A'
and	a.ie_interno	= '70'
and	rownum 		= 1;
exception
when others then
	nr_seq_estagio_w	:= null;
end;

if	(nr_seq_autor_w > 0) then --Verifica autoriza��es geradas para o ciclo
	begin
		if	(nvl(ie_cancela_autor_w,'N') = 'S') and
			(nr_seq_estagio_w is not null) then
			update	autorizacao_convenio
			set	nr_seq_estagio	= nr_seq_estagio_w,
				nm_usuario	= nm_usuario_w,
				dt_atualizacao	= sysdate
			where	nr_sequencia 	= nr_seq_autor_w;

			--gerar_historico_autorizacao(nr_seq_autor_w,null,null,'Cancelado pela exclus�o do tratamento oncol�gico.',nm_usuario_w);
			gerar_historico_autorizacao(nr_seq_autor_w,null,null,wheb_mensagem_pck.get_texto(309989),nm_usuario_w);
		else
			delete
			from	autorizacao_convenio
			where	nr_sequencia = nr_seq_autor_w;
		end if;
	end;

elsif	(nvl(ie_cancela_autor_w,'N') = 'S') and --Verifica autoriza��es geradas para o dia.
	(nr_seq_estagio_w is not null) and
	(nvl(nr_seq_autor_dia_w,0) > 0) then

	/*
	begin
	select	a.nr_sequencia
	into	nr_seq_autor_w
	from	autorizacao_convenio a
	where	a.nr_seq_paciente 	= :old.nr_seq_atendimento
	and	a.nr_ciclo	  	= :old.nr_ciclo
	and	a.ds_dia_ciclo		= :old.ds_dia_ciclo
	and	rownum			= 1;
	exception
	when others then
		nr_seq_autor_w	:= 0;
	end;*/

	if	(nr_seq_autor_dia_w > 0) then
		update	autorizacao_convenio
		set	nr_seq_estagio	= nr_seq_estagio_w,
			nm_usuario	= nm_usuario_w,
			dt_atualizacao	= sysdate,
			nr_seq_paciente	= null
		where	nr_sequencia 	= nr_seq_autor_dia_w;

		--gerar_historico_autorizacao(nr_seq_autor_dia_w,null,null,'Cancelado pela exclus�o do tratamento oncol�gico.',:old.nm_usuario);
		gerar_historico_autorizacao(nr_seq_autor_dia_w,null,null,wheb_mensagem_pck.get_texto(309989),:old.nm_usuario);

	end if;
end if;

commit;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atendimento_hist
before insert or update ON TASY.PACIENTE_ATENDIMENTO for each row
declare

nr_sequencia_w		number(10);

begin

if	(:new.dt_chegada <> :old.dt_chegada) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_chegada)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_chegada);
end if;

if	(:new.dt_fim_adm <> :old.dt_fim_adm) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_fim_adm)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_fim_adm);
end if;

if	(:new.dt_inicio_adm <> :old.dt_inicio_adm) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_inicio_adm)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_inicio_adm);
end if;

if	(:new.dt_acomodacao_paciente <> :old.dt_acomodacao_paciente)   then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_acomodacao_paciente)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_acomodacao_paciente);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATENDIMENTO_tp  after update ON TASY.PACIENTE_ATENDIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_ATENDIMENTO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_PREVISTA,1,4000),substr(:new.DT_PREVISTA,1,4000),:new.nm_usuario,nr_seq_w,'DT_PREVISTA',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REAL,1,4000),substr(:new.DT_REAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_REAL',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_AUC,1,4000),substr(:new.QT_AUC,1,4000),:new.nm_usuario,nr_seq_w,'QT_AUC',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SUPERF_CORPORAL,1,4000),substr(:new.QT_SUPERF_CORPORAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_SUPERF_CORPORAL',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CREATININA,1,4000),substr(:new.QT_CREATININA,1,4000),:new.nm_usuario,nr_seq_w,'QT_CREATININA',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO,1,4000),substr(:new.QT_PESO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO',ie_log_w,ds_w,'PACIENTE_ATENDIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atend_pend_atual
after insert or update ON TASY.PACIENTE_ATENDIMENTO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_libera_ciclo_onc_w	varchar2(10);
cd_pessoa_fisica_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(wheb_usuario_pck.get_cd_funcao = 281) then

	select	max(IE_LIBERA_CICLO_ONC)
	into	ie_libera_ciclo_onc_w
	from	parametro_medico
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if	(nvl(ie_libera_ciclo_onc_w,'N') = 'S') then
		if	(:new.dt_liberacao_reg is null) then
			ie_tipo_w := 'ONC';
		elsif	(:old.dt_liberacao_reg is null) and
				(:new.dt_liberacao_reg is not null) then
			ie_tipo_w := 'XONC';
		end if;

		if	(:new.dt_cancelamento is not null) then

			ie_tipo_w := '';

			delete	from   pep_item_pendente
			where  nr_seq_atend_ciclo  = :new.nr_seq_atendimento;

		end if;

		if	(ie_tipo_w	is not null) then

			Select 	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	paciente_setor
			where	nr_seq_paciente = :new.nr_seq_paciente;

			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_seq_atendimento, nvl(cd_pessoa_fisica_w,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
		end if;
	end if;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATENDIMENTO_ATUAL
before insert or update ON TASY.PACIENTE_ATENDIMENTO for each row
declare
qt_altura_w				Number(5,0);
qt_redutor_sc_w			Number(15,4);
ie_formula_sc_w			varchar2(10);
qt_superf_corporal_w	number(15,5);
cd_pessoa_fisica_w   	varchar2(20);
ie_registro_liberado_w 	varchar2(1);
begin
if	(:new.dt_cancelamento is null) then
	Abortar_Se_Protoc_Inativo(:new.nr_seq_paciente);
end if;

if	(nvl(:old.DT_PREVISTA,sysdate+10) <> :new.DT_PREVISTA) and
	(:new.DT_PREVISTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_PREVISTA, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(NVL(:new.qt_peso,0) <> NVL(:OLD.qt_peso,0)) OR
    (NVL(:new.qt_altura,0) <> NVL(:old.qt_altura,0))then

	select	nvl(qt_altura,0),
		nvl(qt_redutor_sc,0),
		ie_formula_sup_corporea,
      cd_pessoa_fisica
	into qt_altura_w,
		qt_redutor_sc_w,
		ie_formula_sc_w,
      cd_pessoa_fisica_w
	from	paciente_setor
	where	nr_seq_paciente = :new.nr_seq_paciente;

	if	(nvl(:new.qt_altura,0) > 0) then
		qt_altura_w := :new.qt_altura;
	end if;

	qt_superf_corporal_w := round(obter_superficie_corp_red_ped(:new.qt_peso, qt_altura_w,qt_redutor_sc_w, cd_pessoa_fisica_w, obter_usuario_ativo,ie_formula_sc_w),obter_numero_casas_sc);
	if	(nvl(qt_superf_corporal_w,0) > 0) then
		:new.qt_superf_corporal	:= qt_superf_corporal_w;
	end if;


end if;

if	(:new.dt_cancelamento is not null) and
	(:old.dt_cancelamento is null) and
	(:new.NM_USUARIO_CANCEL is null) then
	:new.NM_USUARIO_CANCEL	:= obter_usuario_ativo;
end if;

if	(:old.ie_exige_liberacao = 'N') and
	(:new.ie_exige_liberacao = 'S') and
	(:new.nr_prescricao is not null) then
	select 	nvl(max('S'),'N')
	into	ie_registro_liberado_w
	from 	prescr_material
	where 	nr_prescricao = :new.nr_prescricao
	and 	dt_baixa  is not null;

	if (ie_registro_liberado_w = 'S') then
		Wheb_mensagem_pck.exibir_mensagem_abort(326065);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atendimento_insert
before insert ON TASY.PACIENTE_ATENDIMENTO for each row
declare
cd_pessoa_atend_w varchar2(10);
cd_pessoa_qui_w varchar2(10);
cd_estabelecimento_w number(4);
begin



if	(:new.nr_atendimento is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_atend_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_qui_w
	from	paciente_setor
	where	nr_seq_paciente	= :new.nr_seq_paciente;

	if	(cd_pessoa_atend_w is not null) and
		(cd_pessoa_qui_w is not null) and
		(cd_pessoa_atend_w	<> cd_pessoa_qui_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(197786);
	end if;
end if;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	paciente_setor
where	nr_seq_paciente	= :new.nr_seq_paciente;

if	(:new.cd_estabelecimento is null) then
	:new.cd_estabelecimento := nvl(cd_estabelecimento_w,wheb_usuario_pck.get_cd_estabelecimento);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOG_281_PACIENTE_ATENDIMENTO
BEFORE INSERT OR UPDATE ON TASY.PACIENTE_ATENDIMENTO FOR EACH ROW
declare

ie_247_w varchar2(10);
ie_acao_w varchar2(40);

BEGIN

begin

ie_acao_w := 'update';

if	(inserting) then
	ie_acao_w := 'insert';
end if;

Obter_Param_Usuario(281, 247, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_247_w);

if	(:new.nr_atendimento is not null or :old.nr_atendimento is not null) then
	begin
	insert into log_tasy (  dt_atualizacao,
                        nm_usuario,
                        cd_log,
                        ds_log)
        values 	     ( 	sysdate,
                        'tasy',
                        281,
                        substr('NR_SEQ_ATENDIMENTO: ' || :new.NR_SEQ_ATENDIMENTO
                            || ', NR_ATENDIMENTO: ' || :new.NR_ATENDIMENTO
                            || ', NR_SEQ_PACIENTE: ' || :NEW.NR_SEQ_PACIENTE
                            || ', stack trace: ' || dbms_utility.format_call_stack
                            || ', when: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
                            || ', dt_prevista: ' || :new.dt_prevista
                            || ', perf: ' || obter_perfil_ativo
                            || ', ie_247_w : ' || ie_247_w
                            || ', ie_ac : ' ||  ie_acao_w
                            || ', user: ' || wheb_usuario_pck.get_nm_usuario,1,2000));
	end;
end if;

exception
when others then
	insert into log_tasy (  dt_atualizacao,
                        nm_usuario,
                        cd_log,
                        ds_log)
        values 	     ( 	sysdate,
                        'tasy',
                        281,
                        'N�o gerou log!');
end;

END;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATENDIMENTO_UPDATE
BEFORE INSERT OR UPDATE ON TASY.PACIENTE_ATENDIMENTO FOR EACH ROW
DECLARE
qt_altura_w		Number(5,0);
qt_redutor_sc_w		Number(15,2);
qt_agenda_quimio_w	number(10);
cd_pessoa_atend_w	varchar2(10);
cd_pessoa_qui_w		varchar2(10);
ie_permite_alterar_w	varchar2(1);
ie_setar_data_real_w 	varchar2(1);
ie_substituir_w 		varchar2(1);
ie_cancela_prescr_w	varchar2(1);
ds_log_w			varchar2(1500);
ds_liberacao_w 		varchar2(255);
nr_seq_agequi_w		number(10);
ie_status_agenda_w	varchar2(15);
nr_seq_pend_agenda_w	number(10);
dt_agenda_w		date;
nr_seq_pac_se_w		atendimento_paciente.nr_seq_pac_senha_fila%type;
ie_cancelar_agend_w 	varchar2(1) := 'N';

BEGIN

Obter_Param_Usuario(3130, 342, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alterar_w);
Obter_Param_Usuario(3130, 352, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cancela_prescr_w);
obter_param_usuario(3130, 150, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_substituir_w);
obter_param_usuario(3130, 54, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_setar_data_real_w);
Obter_Param_Usuario(281, 1255, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cancelar_agend_w);


if 	(ie_permite_alterar_w = 'N') and
	(:new.dt_inicio_adm is not null) and
	(:old.dt_prevista is not null) and
	(:old.dt_prevista <> :new.dt_prevista) then
	wheb_mensagem_pck.exibir_mensagem_abort(197783);
end if;

if	(ie_permite_alterar_w = 'N') and
	(:new.dt_inicio_adm is not null) and
	(:old.dt_real is not null) and
	(:new.dt_real	<> :old.dt_real) then
	wheb_mensagem_pck.exibir_mensagem_abort(197784);
end if;

if	(:new.ie_exige_liberacao = 'S') and
	(:old.ie_exige_liberacao = 'N') and
	(nvl(:new.dt_real,sysdate) < sysdate) then
	wheb_mensagem_pck.exibir_mensagem_abort(197785);
end if;

if	(:new.nr_atendimento is not null) and
	(:new.nr_atendimento	<> nvl(:old.nr_atendimento,0)) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_atend_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_qui_w
	from	paciente_setor
	where	nr_seq_paciente	= :new.nr_seq_paciente;

	if	(cd_pessoa_atend_w is not null) and
		(cd_pessoa_qui_w is not null) and
		(cd_pessoa_atend_w	<> cd_pessoa_qui_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(197786);
	end if;
end if;

if 	(nvl(ie_substituir_w,'S') = 'S') and
	(:new.nr_atendimento is not null) and
	(:new.nr_atendimento <> nvl(:old.nr_atendimento,0)) then
	update	can_ordem_prod
	set	nr_atendimento	= :new.nr_atendimento
	where	nr_prescricao	= :new.nr_prescricao;
end if;

if (:new.ie_exige_liberacao not in  ('N','S')) then --This field request only S or N
	ds_liberacao_w := obter_desc_expressao(292540);
	wheb_mensagem_pck.exibir_mensagem_abort(997925,'field='|| ds_liberacao_w);
end if;

if	(:new.ie_exige_liberacao = 'S') and
	(:old.ie_exige_liberacao = 'N') and
	(:new.nr_prescricao is not null) then

	if 	(nvl(ie_cancela_prescr_w,'S') = 'S') then
		update	prescr_medica
		set	dt_suspensao		= sysdate,
			nm_usuario_susp		= :new.nm_usuario
		where	nr_prescricao	= :new.nr_prescricao;
	end if;

	update	can_ordem_prod
	set	ie_suspensa	= 'S'
	where	nr_prescricao	= :new.nr_prescricao;

elsif	(:new.ie_exige_liberacao = 'N') and
	(:old.ie_exige_liberacao = 'S') and
	(:new.nr_prescricao is not null) then
	update	prescr_medica
	set	dt_suspensao	= null,
		nm_usuario_susp	= null
	where	nr_prescricao	= :new.nr_prescricao;

	update	can_ordem_prod
	set	ie_suspensa	= 'N'
	where	nr_prescricao	= :new.nr_prescricao;
end if;

if	(:old.ie_exige_liberacao = 'N') and
	(:new.ie_exige_liberacao <> :old.ie_exige_liberacao) then
	:new.nm_usuario_check_lib	:= :new.nm_usuario;
	:new.dt_check_lib		:= sysdate;
end if;

if 	(:old.dt_inicio_adm is null) and
	(:new.dt_inicio_adm is not null) then
	:new.nm_usuario_inicio_adm := :new.nm_usuario;
end if;

if 	(:old.dt_fim_adm is null) and
	(:new.dt_fim_adm is not null) then
	:new.nm_usuario_fim_adm	:= :new.nm_usuario;
end if;

if	(:old.ie_exige_liberacao = 'S') and
	(:new.ie_exige_liberacao <> :old.ie_exige_liberacao) then
	:new.nm_usuario_check_lib	:= null;
	:new.dt_check_lib		:= null;
end if;

if  (ie_setar_data_real_w = 'S') and
	(:old.nr_prescricao is null) and
	(:new.nr_prescricao is not null) and
	(obter_funcao_ativa = 3130)	then
	:new.dt_real := sysdate;
End if;

select	count(*)
into	qt_agenda_quimio_w
from	agenda_quimio
where	nr_seq_atendimento	= :new.nr_seq_atendimento
and	nvl(ie_status_agenda, 'N')	<> 'C';

if	(qt_agenda_quimio_w> 0) then
	if	(:new.nr_seq_local is not null) and
		(nvl(:new.nr_seq_local, 0)	<> nvl(:old.nr_seq_local, 0)) then
		update	agenda_quimio
		set	nr_seq_local		= :new.nr_seq_local,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_atendimento	= :new.nr_seq_atendimento
		and	ie_status_agenda not in ('C','S','F');

		update 	agenda_quimio_marcacao a
		set	nr_seq_local		= :new.nr_seq_local,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_atendimento	= :new.nr_seq_atendimento
		and	ie_gerado = 'S'
		and	exists (select 1 from agenda_quimio  b
				where	a.nr_seq_atendimento = b.nr_seq_atendimento
				and	a.nr_seq_pend_agenda = b.nr_seq_pend_agenda
				and	ie_status_agenda not in ('C','S','F'));
	end if;
	if	(:old.dt_real is not null) and
		(:new.dt_real is not null) and
		(:new.dt_real	<> :old.dt_real) then
		update	agenda_quimio
		set	dt_agenda		= :new.dt_real,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_atendimento	= :new.nr_seq_atendimento
		and	ie_status_agenda not in ('C','S','F');

       	update	agenda_quimio_marcacao
		set		dt_agenda		= :new.dt_real,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
		where	nr_seq_atendimento	= :new.nr_seq_atendimento;

	end if;
	if	(((:old.nr_atendimento is null) and
		(:new.nr_atendimento is not null)) or
		((:old.nr_atendimento is not null) and
		(:new.nr_atendimento is null))) and
		(:old.ds_observacao = :new.ds_observacao) then
		update	agenda_quimio
		set	nr_atendimento	= :new.nr_atendimento,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_atendimento	= :new.nr_seq_atendimento
		and	ie_status_agenda not in ('C','S','F');
	end if;
end if;


/*if 	((:new.dt_prevista is not null) and
	(:old.dt_prevista is not null)) or
	((:new.dt_real is not null) and
	(:old.dt_real is not null)) and
	((:old.dt_prevista <> :new.dt_prevista) or
	(:old.dt_real <> :new.dt_real)) and
	(obter_funcao_ativa = 865)then
	:new.nm_usuario_alter_data_prev :=  :new.nm_usuario;
	:new.dt_alter_data_prev := :new.dt_atualizacao;	*/

if 	((:new.dt_prevista is not null) and
	(:old.dt_prevista is not null) and
	(:old.dt_prevista <> :new.dt_prevista)) or
	((:new.dt_real is not null) and
	(:old.dt_real is not null) and
	(:old.dt_real <> :new.dt_real))then
	:new.nm_usuario_alter_data_prev := :new.nm_usuario;
	:new.dt_alter_data_prev 		:= :new.dt_atualizacao;

	ds_log_w := 	substr(obter_desc_expressao(287659) || ' ' ||:new.ds_dia_ciclo||chr(13)||chr(10)||
					obter_desc_expressao(284890) || ' ' ||:new.nr_ciclo||chr(13)||chr(10)||
					obter_desc_expressao(711343) || ' ' ||:new.nr_seq_atendimento||chr(13)||chr(10)||
					obter_desc_expressao(737036) || ' ' ||:new.nr_seq_paciente||chr(13)||chr(10)||
					'Data antiga: '||:old.dt_prevista||chr(13)||chr(10)||
					'Data nova: '||:new.dt_prevista||chr(13)||chr(10)||
					'Data real antiga: '||:old.dt_real||chr(13)||chr(10)||
					'Data real nova: '||:new.dt_real||chr(13)||chr(10)||
					obter_desc_expressao(298822) || ' ' ||dbms_utility.format_call_stack,1,1500);

	insert into log_mov(NM_USUARIO,
							DT_ATUALIZACAO,
							DS_LOG,
							CD_LOG)
					values 	(:new.nm_usuario,
							sysdate,
							ds_log_w,
							14458);
end if;

if (:old.dt_cancelamento is null) and
   (:new.dt_cancelamento is not null)	then

	select	max(nr_sequencia),
		max(ie_status_agenda),
		max(nr_seq_pend_agenda),
		max(dt_agenda)
	into	nr_seq_agequi_w,
		ie_status_agenda_w,
		nr_seq_pend_agenda_w,
		dt_agenda_w
	from	agenda_quimio
	where	nr_seq_atendimento	= :new.nr_seq_atendimento;

	if	(nr_seq_agequi_w	> 0) and
		(ie_status_agenda_w	<> 'C') then

		update	agenda_quimio
		set	ie_status_agenda	= ie_status_agenda_w,
			dt_cancelada		= sysdate,
			nr_seq_mot_cancelamento = null,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate,
			nm_usuario_cancel	= :new.nm_usuario,
			ds_motivo_status        = null
		where	nr_sequencia		= nr_seq_agequi_w;


    if(ie_cancelar_agend_w <> 'R') then

		delete	agenda_quimio_marcacao
		where	nr_seq_pend_agenda	= nr_seq_pend_agenda_w
		and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_Agenda)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda_w);

    end if;

	end if;

end if;

if	(:new.nr_atendimento is not null) and
	(:new.nr_atendimento	<> nvl(:old.nr_atendimento,0)) then
	select 	NR_SEQ_PAC_SENHA_FILA
	into	nr_seq_pac_se_w
	from 	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if (nr_seq_pac_se_w > 0) then
		:new.nr_seq_pac_senha_fila := nr_seq_pac_se_w;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atendimento_CI
after update ON TASY.PACIENTE_ATENDIMENTO for each row
declare
nr_seq_evento_w			number(10);
qt_idade_w				number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w		varchar2(10);
ds_informacao_w			varchar2(255);
nr_atendimento_w		paciente_atendimento.nr_atendimento%type;

cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and		ie_evento_disp		= 'ATO'
	and		qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and		nvl(ie_situacao,'A') = 'A';

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

begin

nr_atendimento_w := :new.nr_atendimento;

if (nvl(nr_atendimento_w, 0) = 0) then

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	paciente_setor
	where	nr_seq_paciente = :new.nr_seq_paciente;

	select	max(nr_atendimento)
	into 	nr_atendimento_w
	from 	atendimento_paciente
	where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
end if;

if (:old.dt_prevista <> :new.dt_prevista) and
	(nvl(nr_atendimento_w,0) > 0) then
	begin

	select	nvl(max(cd_estabelecimento),0),
		max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
		cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

	select	obter_desc_expressao(781783, 'MACRO1' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_prevista, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)|| 'MACRO2' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_prevista, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone))/*'Alterada a data de ' || to_char(:old.dt_prevista,'dd/mm/yyyy hh24:mi:ss') || ' para ' || to_char(:new.dt_prevista,'dd/mm/yyyy hh24:mi:ss')*/
	into	ds_informacao_w
	from	dual;

	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,:new.nm_usuario,ds_informacao_w);
		end;
	end loop;
	close C01;

	end;
end if;

exception
when others then
	null;

end;

<<final>>
null;

end;
/


ALTER TABLE TASY.PACIENTE_ATENDIMENTO ADD (
  CONSTRAINT PACATEND_PK
 PRIMARY KEY
 (NR_SEQ_ATENDIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ATENDIMENTO ADD (
  CONSTRAINT PACATEN_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACATEN_QTMTFAL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FALTA) 
 REFERENCES TASY.QT_MOTIVO_FALTA (NR_SEQUENCIA),
  CONSTRAINT PACATEN_QUJUATR_FK 
 FOREIGN KEY (NR_SEQ_JUST_ATRASO) 
 REFERENCES TASY.QUIMIO_JUSTIF_ATRASO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACATEN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACATEN_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT PACATEN_QTPENAG_FK 
 FOREIGN KEY (NR_SEQ_PEND_AGENDA) 
 REFERENCES TASY.QT_PENDENCIA_AGENDA (NR_SEQUENCIA),
  CONSTRAINT PACATEN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PACATEN_QTLOCAL_FK2 
 FOREIGN KEY (NR_SEQ_LOCAL_PREVISTO) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PACATEN_PESFISI_FK 
 FOREIGN KEY (CD_ENFERMAGEM) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACATEN_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PACATEN_MOTSUSQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_QUMIO (NR_SEQUENCIA),
  CONSTRAINT PACATEN_MOALDAQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ALTER_DATA) 
 REFERENCES TASY.MOTIVO_ALTER_DATA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT PACATEN_MOBLTRQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_BLOQ) 
 REFERENCES TASY.MOTIVO_BLOQ_TRAT_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT PACATEN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATEN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATEN_PACSETO_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE) 
 REFERENCES TASY.PACIENTE_SETOR (NR_SEQ_PACIENTE)
    ON DELETE CASCADE,
  CONSTRAINT PACATEN_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PACATEN_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PACATEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACATEN_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_ATENDIMENTO TO NIVEL_1;

