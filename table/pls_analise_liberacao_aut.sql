ALTER TABLE TASY.PLS_ANALISE_LIBERACAO_AUT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ANALISE_LIBERACAO_AUT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ANALISE_LIBERACAO_AUT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE_ANALISE  NUMBER(10)               NOT NULL,
  NR_SEQ_AUDITORIA     NUMBER(10)               NOT NULL,
  IE_LIBERADA          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REQUISICAO    NUMBER(10),
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSANLA_PK ON TASY.PLS_ANALISE_LIBERACAO_AUT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANLA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSANLA_PLSAUDI_FK_I ON TASY.PLS_ANALISE_LIBERACAO_AUT
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANLA_PLSAUDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSANLA_PLSLALA_FK_I ON TASY.PLS_ANALISE_LIBERACAO_AUT
(NR_SEQ_LOTE_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANLA_PLSLALA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSANLA_PLSREQU_FK_I ON TASY.PLS_ANALISE_LIBERACAO_AUT
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANLA_PLSREQU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ANALISE_LIBERACAO_AUT ADD (
  CONSTRAINT PLSANLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ANALISE_LIBERACAO_AUT ADD (
  CONSTRAINT PLSANLA_PLSAUDI_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.PLS_AUDITORIA (NR_SEQUENCIA),
  CONSTRAINT PLSANLA_PLSLALA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ANALISE) 
 REFERENCES TASY.PLS_LOTE_ANALISE_LIB_AUT (NR_SEQUENCIA),
  CONSTRAINT PLSANLA_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ANALISE_LIBERACAO_AUT TO NIVEL_1;

