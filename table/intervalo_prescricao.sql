ALTER TABLE TASY.INTERVALO_PRESCRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERVALO_PRESCRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERVALO_PRESCRICAO
(
  CD_INTERVALO               VARCHAR2(7 BYTE)   NOT NULL,
  DS_INTERVALO               VARCHAR2(40 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_OPERACAO                VARCHAR2(1 BYTE),
  QT_OPERACAO                NUMBER(6,2),
  DS_HORARIOS                VARCHAR2(255 BYTE),
  DS_PRESCRICAO              VARCHAR2(30 BYTE),
  IE_DOSE_DIFERENCIADA       VARCHAR2(1 BYTE),
  IE_DOSE_ESPECIAL           VARCHAR2(1 BYTE),
  DS_PRIM_HORARIO            VARCHAR2(255 BYTE),
  IE_PRESCRICAO_DIETA        VARCHAR2(1 BYTE)   NOT NULL,
  QT_SE_NECESSARIO           NUMBER(3),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_AGORA                   VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_ACM                     VARCHAR2(1 BYTE),
  IE_SE_NECESSARIO           VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_APRESENT            NUMBER(3)          NOT NULL,
  QT_MIN_INTERVALO           NUMBER(5),
  IE_GASOTERAPIA             VARCHAR2(1 BYTE),
  QT_MIN_AGORA               NUMBER(3),
  CD_ESTABELECIMENTO         NUMBER(4),
  IE_SEGUNDA                 VARCHAR2(2 BYTE),
  IE_TERCA                   VARCHAR2(2 BYTE),
  IE_QUARTA                  VARCHAR2(2 BYTE),
  IE_QUINTA                  VARCHAR2(2 BYTE),
  IE_SEXTA                   VARCHAR2(2 BYTE),
  IE_SABADO                  VARCHAR2(2 BYTE),
  IE_DOMINGO                 VARCHAR2(2 BYTE),
  IE_PERMITE_ALTERAR_INTERV  VARCHAR2(1 BYTE),
  IE_CONSISTIR_HORARIOS      VARCHAR2(1 BYTE),
  HR_REFERENCIA_DIETA        VARCHAR2(5 BYTE),
  QT_FREQUENCIA_RECEITA      NUMBER(5,2),
  IE_SE_FARMACIA_AMB         VARCHAR2(1 BYTE)   NOT NULL,
  IE_LIMPA_PRIM_HOR          VARCHAR2(1 BYTE),
  IE_SEM_APRAZAMENTO         VARCHAR2(1 BYTE),
  NR_ETAPAS                  NUMBER(5),
  IE_REORDENAR_FIXO          VARCHAR2(1 BYTE),
  IE_PADRAO_AGORA            VARCHAR2(1 BYTE),
  IE_UTILIZA_QUANTIDADE      VARCHAR2(1 BYTE),
  IE_EDITAR_PRIM_HOR         VARCHAR2(1 BYTE),
  IE_PADRAO_NUT              VARCHAR2(1 BYTE),
  IE_LIMPAR_HORARIOS         VARCHAR2(1 BYTE),
  IE_PADRAO_ACM_SAE          VARCHAR2(1 BYTE),
  IE_SO_RETROGRADA           VARCHAR2(1 BYTE),
  DS_COR                     VARCHAR2(15 BYTE),
  IE_24_H                    VARCHAR2(1 BYTE),
  QT_OPERACAO_FA             NUMBER(15,4),
  IE_CALCULA_VOLUME          VARCHAR2(1 BYTE),
  IE_COPIAR                  VARCHAR2(1 BYTE),
  IE_QUESTIONAR_DIA          VARCHAR2(1 BYTE),
  IE_CONTINUO                VARCHAR2(1 BYTE),
  IE_PRIM_HOR_VALIDADE       VARCHAR2(1 BYTE),
  DT_FINAL                   DATE,
  IE_CONTROLA_INTERVALO      VARCHAR2(1 BYTE),
  IE_DOSE_UNICA_CPOE         VARCHAR2(1 BYTE),
  IE_DOSE_UNICA_AGORA_CPOE   VARCHAR2(1 BYTE),
  IE_CHECK_MAX_DOSE          VARCHAR2(1 BYTE),
  STANDARD_ADM_CODE          VARCHAR2(16 BYTE),
  SI_MANUAL_INPUT            VARCHAR2(1 BYTE),
  IE_HIDE_SCHEDULES          VARCHAR2(15 BYTE),
  DS_HINT_DIF_DOSE           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPRES_ESTABEL_FK_I ON TASY.INTERVALO_PRESCRICAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTPRES_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INTPRES_I1 ON TASY.INTERVALO_PRESCRICAO
(IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPRES_PK ON TASY.INTERVALO_PRESCRICAO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTERVALO_PRESCRICAO_tp  after update ON TASY.INTERVALO_PRESCRICAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_INTERVALO);  ds_c_w:=null; ds_w:=substr(:new.CD_INTERVALO,1,500);gravar_log_alteracao(substr(:old.CD_INTERVALO,1,4000),substr(:new.CD_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO',ie_log_w,ds_w,'INTERVALO_PRESCRICAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.intervalo_prescricao_atual
before insert or update ON TASY.INTERVALO_PRESCRICAO for each row
declare

ds_horarios_w		intervalo_prescricao.ds_horarios%type;
ds_horarios_ww		intervalo_prescricao.ds_horarios%type;
ds_prim_horario_w	intervalo_prescricao.ds_prim_horario%type;
ds_prim_horario_ww	intervalo_prescricao.ds_prim_horario%type;
ds_texto_w		varchar2(255);

begin
begin
	if (:new.hr_referencia_dieta is not null) and ((:new.hr_referencia_dieta <> :old.hr_referencia_dieta) or (:old.dt_final is null)) then
		:new.dt_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_referencia_dieta,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

if  	((nvl(:new.ie_acm, 'N') = 'N') and
	 (nvl(:new.ie_se_necessario, 'N') = 'N') and
	 (:new.ds_horarios is not null or
	  :new.ds_prim_horario is not null)) then

	ds_horarios_w := replace(replace(:new.ds_horarios, ':', null), ' ', null);
	ds_prim_horario_w := replace(replace(:new.ds_prim_horario, ':', null), ' ', null);

	ds_horarios_ww := nvl(trim(ds_horarios_w),0);
	ds_prim_horario_ww := nvl(trim(ds_prim_horario_w),0);

	if (obter_se_somente_numero(ds_horarios_ww) = 'N') or (obter_se_somente_numero(ds_prim_horario_ww) = 'N') then

		ds_texto_w := substr(obter_desc_expressao(345333)||' '||obter_desc_expressao(349053)||chr(10),1,255);

		if (obter_se_somente_numero(ds_horarios_ww) = 'N') then
			ds_texto_w := substr(ds_texto_w ||' - '||obter_desc_expressao(291449)||chr(10),1,255);
		end if;

		if (obter_se_somente_numero(ds_prim_horario_ww) = 'N') then
			ds_texto_w := substr(ds_texto_w ||' - '||obter_desc_expressao(296314),1,255);
		end if;

		wheb_mensagem_pck.exibir_mensagem_abort(ds_texto_w);
	end if;
end if;

end;
/


ALTER TABLE TASY.INTERVALO_PRESCRICAO ADD (
  CONSTRAINT INTPRES_PK
 PRIMARY KEY
 (CD_INTERVALO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERVALO_PRESCRICAO ADD (
  CONSTRAINT INTPRES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.INTERVALO_PRESCRICAO TO NIVEL_1;

