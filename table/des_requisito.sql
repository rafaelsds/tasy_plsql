ALTER TABLE TASY.DES_REQUISITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DES_REQUISITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DES_REQUISITO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PROJETO       NUMBER(10),
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  CD_ANALISTA          VARCHAR2(10 BYTE)        NOT NULL,
  DT_REQUISITO         DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_CLIENTE_WHEB      VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_APRESENTACAO  NUMBER(10)               NOT NULL,
  DT_APROVACAO         DATE,
  NR_SEQ_ORDEM_SERV    NUMBER(10),
  IE_RELATORIO         VARCHAR2(1 BYTE),
  NR_SEQ_PR            NUMBER(10),
  NR_SEQ_CR            NUMBER(10),
  IE_RISCO             VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESREQU_MANORSE_FK_I ON TASY.DES_REQUISITO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESREQU_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DESREQU_PESFISI_FK_I ON TASY.DES_REQUISITO
(CD_ANALISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESREQU_PK ON TASY.DES_REQUISITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESREQU_PK
  MONITORING USAGE;


CREATE INDEX TASY.DESREQU_PROPROJ_FK_I ON TASY.DES_REQUISITO
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESREQU_PROPROJ_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.reg_des_requisito_before
	before update ON TASY.DES_REQUISITO for each row
declare
	ie_html5_w	varchar(1);
	nr_seq_pr_w	reg_product_requirement.nr_sequencia%type;
begin
	if	(:old.dt_aprovacao is null) and (:new.dt_aprovacao is not null) then
		select
			case
				when ie_html5           = 'S'
				or (nvl(ie_java, 'N')   = 'N'
				and nvl(ie_delphi, 'N') = 'N')
				then 'S'
				else ie_html5
			end
		into ie_html5_w
		from proj_projeto
		where nr_sequencia = :new.nr_seq_projeto;

		if	ie_html5_w = 'S' then

			select	max(pr.nr_sequencia)
			into	nr_seq_pr_w
			from	des_requisito_item ri
			left join reg_caso_teste tc on tc.nr_seq_product = ri.nr_seq_pr
			join reg_product_requirement pr on pr.nr_sequencia = ri.nr_seq_pr
			where	ri.nr_seq_requisito	= :new.nr_sequencia
			and	tc.nr_sequencia is null;

			if	nr_seq_pr_w is not null then
				wheb_mensagem_pck.exibir_mensagem_abort(997073);
			end if;
		end if;
	end if;
end;
/


ALTER TABLE TASY.DES_REQUISITO ADD (
  CONSTRAINT DESREQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DES_REQUISITO ADD (
  CONSTRAINT DESREQU_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT DESREQU_PESFISI_FK 
 FOREIGN KEY (CD_ANALISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DESREQU_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DES_REQUISITO TO NIVEL_1;

