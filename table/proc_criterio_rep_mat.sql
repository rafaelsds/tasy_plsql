ALTER TABLE TASY.PROC_CRITERIO_REP_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_CRITERIO_REP_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_CRITERIO_REP_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CRITERIO      NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRCRREM_MATERIA_FK_I ON TASY.PROC_CRITERIO_REP_MAT
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRCRREM_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRCRREM_PK ON TASY.PROC_CRITERIO_REP_MAT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRCRREM_PROCRRE_FK_I ON TASY.PROC_CRITERIO_REP_MAT
(NR_SEQ_CRITERIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROC_CRITERIO_REP_MAT ADD (
  CONSTRAINT PRCRREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_CRITERIO_REP_MAT ADD (
  CONSTRAINT PRCRREM_PROCRRE_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO) 
 REFERENCES TASY.PROC_CRITERIO_REPASSE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRCRREM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PROC_CRITERIO_REP_MAT TO NIVEL_1;

