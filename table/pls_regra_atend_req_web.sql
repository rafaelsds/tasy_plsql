ALTER TABLE TASY.PLS_REGRA_ATEND_REQ_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ATEND_REQ_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ATEND_REQ_WEB
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  CD_AREA_PROCEDIMENTO      NUMBER(15),
  CD_ESPECIALIDADE          NUMBER(15),
  CD_GRUPO_PROC             NUMBER(15),
  QT_IDADE_MIN              NUMBER(3),
  QT_IDADE_MAX              NUMBER(3),
  IE_TIPO_GUIA              VARCHAR2(2 BYTE),
  IE_EXIGE_BIOMETRIA        VARCHAR2(1 BYTE),
  IE_EXIGE_CARTAO           VARCHAR2(1 BYTE),
  IE_REQUISICAO             VARCHAR2(1 BYTE),
  IE_EXECUCAO_REQ           VARCHAR2(2 BYTE),
  IE_CONSULTA_URGENCIA      VARCHAR2(1 BYTE),
  IE_CARATER_ATENDIMENTO    VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  IE_EXIGE_BIOMETRIA_TOKEN  VARCHAR2(1 BYTE),
  IE_EXIGE_REC_FACIAL       VARCHAR2(1 BYTE),
  IE_FUNCAO                 NUMBER(3),
  IE_APLICACAO_REGRA        NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRARW_AREPROC_FK_I ON TASY.PLS_REGRA_ATEND_REQ_WEB
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRARW_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRARW_ESPPROC_FK_I ON TASY.PLS_REGRA_ATEND_REQ_WEB
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRARW_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRARW_ESTABEL_FK_I ON TASY.PLS_REGRA_ATEND_REQ_WEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRARW_GRUPROC_FK_I ON TASY.PLS_REGRA_ATEND_REQ_WEB
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRARW_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRARW_PK ON TASY.PLS_REGRA_ATEND_REQ_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRARW_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRARW_PROCEDI_FK_I ON TASY.PLS_REGRA_ATEND_REQ_WEB
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRARW_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_ATEND_REQ_WEB ADD (
  CONSTRAINT PLSRARW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_ATEND_REQ_WEB ADD (
  CONSTRAINT PLSRARW_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRARW_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSRARW_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSRARW_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSRARW_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_REGRA_ATEND_REQ_WEB TO NIVEL_1;

