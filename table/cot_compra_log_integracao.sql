ALTER TABLE TASY.COT_COMPRA_LOG_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA_LOG_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA_LOG_INTEGRACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_COT_COMPRA        NUMBER(10),
  IE_STATUS_ENVIO      VARCHAR2(15 BYTE)        NOT NULL,
  DS_LOG               VARCHAR2(4000 BYTE),
  IE_TIPO_INTEGRACAO   VARCHAR2(10 BYTE)        NOT NULL,
  NR_SOLIC_COMPRA      NUMBER(10),
  CD_MATERIAL          NUMBER(6),
  CD_CNPJ              VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COCOLIN_COTCOMP_FK_I ON TASY.COT_COMPRA_LOG_INTEGRACAO
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOLIN_MATERIA_FK_I ON TASY.COT_COMPRA_LOG_INTEGRACAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COCOLIN_PK ON TASY.COT_COMPRA_LOG_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOLIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.COCOLIN_SOLCOMP_FK_I ON TASY.COT_COMPRA_LOG_INTEGRACAO
(NR_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOLIN_SOLCOMP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.COT_COMPRA_LOG_INTEGRACAO ADD (
  CONSTRAINT COCOLIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA_LOG_INTEGRACAO ADD (
  CONSTRAINT COCOLIN_COTCOMP_FK 
 FOREIGN KEY (NR_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA (NR_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COCOLIN_SOLCOMP_FK 
 FOREIGN KEY (NR_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA (NR_SOLIC_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COCOLIN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COT_COMPRA_LOG_INTEGRACAO TO NIVEL_1;

