ALTER TABLE TASY.CPOE_RULE_PROC_IMAGES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_RULE_PROC_IMAGES CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_RULE_PROC_IMAGES
(
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  DT_ATUALIZACAO_NREC    DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_TYPE_REQUEST    NUMBER(10),
  NR_SEQ_PROC_INTERNO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RULEPROCIM_CPTIPE_FK_I ON TASY.CPOE_RULE_PROC_IMAGES
(NR_SEQ_TYPE_REQUEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RULEPROCIM_PK ON TASY.CPOE_RULE_PROC_IMAGES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RULEPROCIM_PROINTE_FK_I ON TASY.CPOE_RULE_PROC_IMAGES
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_RULE_PROC_IMAGES ADD (
  CONSTRAINT RULEPROCIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_RULE_PROC_IMAGES ADD (
  CONSTRAINT RULEPROCIM_CPTIPE_FK 
 FOREIGN KEY (NR_SEQ_TYPE_REQUEST) 
 REFERENCES TASY.CPOE_TIPO_PEDIDO (NR_SEQUENCIA),
  CONSTRAINT RULEPROCIM_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

