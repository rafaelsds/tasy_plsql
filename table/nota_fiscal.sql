ALTER TABLE TASY.NOTA_FISCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL
(
  NR_SEQUENCIA                NUMBER(10),
  CD_ESTABELECIMENTO          NUMBER(4),
  CD_CGC_EMITENTE             VARCHAR2(14 BYTE),
  CD_SERIE_NF                 VARCHAR2(255 BYTE),
  NR_SEQUENCIA_NF             NUMBER(10),
  CD_OPERACAO_NF              NUMBER(4),
  DT_EMISSAO                  DATE              NOT NULL,
  DT_ENTRADA_SAIDA            DATE,
  IE_ACAO_NF                  VARCHAR2(1 BYTE)  NOT NULL,
  IE_EMISSAO_NF               VARCHAR2(1 BYTE)  NOT NULL,
  IE_TIPO_FRETE               VARCHAR2(1 BYTE)  NOT NULL,
  VL_MERCADORIA               NUMBER(13,2),
  VL_TOTAL_NOTA               NUMBER(13,2),
  QT_PESO_BRUTO               NUMBER(13,4)      NOT NULL,
  QT_PESO_LIQUIDO             NUMBER(13,4)      NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_CONDICAO_PAGAMENTO       NUMBER(10),
  DT_CONTABIL                 DATE,
  CD_CGC                      VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  VL_IPI                      NUMBER(13,2),
  VL_DESCONTOS                NUMBER(13,2),
  VL_FRETE                    NUMBER(13,2),
  VL_SEGURO                   NUMBER(13,2),
  VL_DESPESA_ACESSORIA        NUMBER(13,2),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NR_NOTA_REFERENCIA          NUMBER(7),
  CD_SERIE_REFERENCIA         VARCHAR2(5 BYTE),
  CD_NATUREZA_OPERACAO        NUMBER(4),
  DT_ATUALIZACAO_ESTOQUE      DATE,
  VL_DESCONTO_RATEIO          NUMBER(13,4),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  NR_ORDEM_COMPRA             NUMBER(10),
  NR_LOTE_CONTABIL            NUMBER(10),
  NR_SEQUENCIA_REF            NUMBER(10),
  NR_INTERNO_CONTA            NUMBER(10),
  DT_ATUALIZACAO_CP_CR        DATE,
  NR_SEQ_PROTOCOLO            NUMBER(10),
  CD_MOEDA                    NUMBER(3),
  VL_CONV_MOEDA               NUMBER(15,4),
  DS_INCONSISTENCIA           VARCHAR2(255 BYTE),
  DT_IMPRESSAO                DATE,
  DS_OBS_DESCONTO_NF          VARCHAR2(80 BYTE),
  NR_SEQ_CLASSIF_FISCAL       NUMBER(10),
  IE_ENTREGUE_BLOQUETO        VARCHAR2(1 BYTE),
  IE_STATUS_ENVIO             VARCHAR2(1 BYTE),
  NR_SEQ_EXPORTADA            NUMBER(10),
  IE_TIPO_NOTA                VARCHAR2(3 BYTE),
  CD_SETOR_IMPRESSAO          NUMBER(5),
  NR_SEQ_MOTIVO_CANCEL        NUMBER(10),
  NR_SEQ_MOTIVO_DEVOL         NUMBER(10),
  NR_RECIBO                   VARCHAR2(20 BYTE),
  NR_SEQ_MENSALIDADE          NUMBER(10),
  CD_SETOR_DIGITACAO          NUMBER(5),
  NR_SEQ_PROT_RES_PLS         NUMBER(10),
  NR_SEQ_LOTE_RES_PLS         NUMBER(10),
  CD_CONV_INTEGRACAO          NUMBER(5),
  NR_NOTA_FISCAL              VARCHAR2(255 BYTE) NOT NULL,
  NR_NOTA_FRETE               VARCHAR2(20 BYTE),
  NR_DANFE                    VARCHAR2(60 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_PROT            NUMBER(10),
  NR_NFE_IMP                  VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA_PAC           NUMBER(10),
  CD_TIPO_SERVICO             VARCHAR2(80 BYTE),
  NR_SEQ_REG_INSPECAO         NUMBER(10),
  IE_NF_ELETRONICA            VARCHAR2(1 BYTE),
  NR_SEQ_IMPORTADA            NUMBER(10),
  IE_COMPL_TRIBUTO            VARCHAR2(1 BYTE),
  NR_NFE_IMP_PROTOCOLO        VARCHAR2(255 BYTE),
  CD_CONV_ENTRADA             NUMBER(5),
  DS_DADOS_ADIC_ITENS         VARCHAR2(255 BYTE),
  NR_SEQ_NF_LOTE_NFE          NUMBER(10),
  QT_ESPECIE                  NUMBER(13,4),
  DS_ESPECIE                  VARCHAR2(80 BYTE),
  DS_MARCA                    VARCHAR2(80 BYTE),
  IE_STATUS_NFE               VARCHAR2(15 BYTE),
  DT_AUTOR_PROT_NFE           DATE,
  DT_CANCELAMENTO_NFE         DATE,
  NR_RPS                      VARCHAR2(20 BYTE),
  CD_SERIE_RPS                VARCHAR2(20 BYTE),
  NR_CONTRATO                 NUMBER(10),
  NR_SEQ_MODELO               NUMBER(10),
  NR_SEQ_ROMANEIO             NUMBER(10),
  CD_ASSINATURA_RPS           VARCHAR2(2000 BYTE),
  CD_PACIENTE_INF             VARCHAR2(15 BYTE),
  CD_VERIFICACAO_NFSE         VARCHAR2(255 BYTE),
  NR_SEQ_PGTO_PREST           NUMBER(10),
  NR_FORMULARIO               VARCHAR2(255 BYTE),
  IE_NAT_OPER_RPS             NUMBER(4),
  VL_DESPESA_DOC              NUMBER(13,2),
  NR_SEQ_CME_ENVIO_EXT        NUMBER(10),
  NM_ARQUIVO_NFSE             VARCHAR2(100 BYTE),
  DT_INTEGRACAO               DATE,
  DT_EMISSAO_NFE              DATE,
  NR_SEQ_RESP_CONSIG          NUMBER(10),
  NR_SEQ_TRANSF_CME           NUMBER(10),
  NR_SEQ_EME_FATURA           NUMBER(10),
  NR_NF_EXTERNO               VARCHAR2(80 BYTE),
  NR_SEQ_COB_PREVIA           NUMBER(10),
  NR_SEQ_GRUPO_PROD           NUMBER(10),
  NR_SEQ_NF_REF               NUMBER(10),
  CD_FAB_ESTRANGEIRO          VARCHAR2(80 BYTE),
  NR_LACRE                    VARCHAR2(100 BYTE),
  NR_SEQ_FATURA               NUMBER(10),
  DS_LINK_RPS                 VARCHAR2(255 BYTE),
  NR_SEQ_FORMA_PAGTO          NUMBER(10),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  DT_APROVACAO                DATE,
  NM_USUARIO_APROV            VARCHAR2(15 BYTE),
  DS_DIGEST_VALUE_NFE         VARCHAR2(80 BYTE),
  DS_VERSAO_APLIC_NFE         VARCHAR2(80 BYTE),
  UF_EMBARQUE                 VARCHAR2(2 BYTE),
  DS_LOCAL_EMBARQUE           VARCHAR2(80 BYTE),
  DT_CANCELAMENTO             DATE,
  NM_USUARIO_CANCEL           VARCHAR2(15 BYTE),
  NR_DANFE_REFERENCIA         VARCHAR2(60 BYTE),
  DT_MES_REF_FATUR            DATE,
  DT_MES_RECEBIMENTO          DATE,
  CD_BANCO                    NUMBER(5),
  CD_AGENCIA_BANCARIA         VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA           VARCHAR2(2 BYTE),
  NR_CONTA                    VARCHAR2(8 BYTE),
  NR_DIGITO_CONTA             VARCHAR2(2 BYTE),
  VL_RECEBIDO                 NUMBER(13,4),
  NR_SEQ_TRANS_FINANC         NUMBER(10),
  NR_SEQ_CLIENTE              NUMBER(10),
  DT_COMPETENCIA              DATE,
  NR_SEQ_NF_REF_CONSUMO       NUMBER(10),
  NR_SEQ_NF_PAGADOR           NUMBER(10),
  NM_USUARIO_CALC             VARCHAR2(15 BYTE),
  NR_SEQ_FATURA_NDC           NUMBER(10),
  DS_RAZAO_EMITENTE_ORIG      VARCHAR2(255 BYTE),
  DS_RAZAO_TOMADOR_ORIG       VARCHAR2(255 BYTE),
  NR_SEQ_CLASSIFICACAO        NUMBER(10),
  CD_MOEDA_ESTRANGEIRA        NUMBER(3),
  VL_CONV_ESTRANGEIRO         NUMBER(13,4),
  DS_QR_CODE                  LONG RAW,
  DS_SELO_DIGITAL_EMISSOR     VARCHAR2(4000 BYTE),
  DS_CERTIF_DIGITAL_SAT       VARCHAR2(4000 BYTE),
  DS_SELO_CERTIF_DIGITAL_SAT  VARCHAR2(4000 BYTE),
  DS_SERIE_CERTIF_CONTR       VARCHAR2(4000 BYTE),
  DS_SERIE_CERTIF_SAT         VARCHAR2(4000 BYTE),
  NR_SEQ_FAR_VENDA            NUMBER(10),
  IE_TIPO_RECEBIMENTO         VARCHAR2(1 BYTE),
  DS_MOTIVO_CANCEL_NFE        VARCHAR2(4000 BYTE),
  NR_SEQ_FATURA_OLD           NUMBER(10),
  NR_SEQ_FATURA_NDC_OLD       NUMBER(10),
  CD_RETORNO_INTEGR_AX        VARCHAR2(100 BYTE),
  NR_SENHA                    VARCHAR2(80 BYTE),
  IE_SISTEMA_ORIGEM           VARCHAR2(15 BYTE),
  NR_RPS_SEQUENCIAL           NUMBER(10),
  CD_PAGAMENTO_CFDI           VARCHAR2(5 BYTE),
  IE_STATUS_INTEG_MXM         VARCHAR2(1 BYTE),
  DS_OBS_INTEG_MXM            VARCHAR2(4000 BYTE),
  CD_CONTROLE                 VARCHAR2(255 BYTE),
  IE_BLOQUEIO_INTPD           VARCHAR2(2 BYTE),
  NR_SEQ_BAIXA_TIT            NUMBER(10),
  NR_SEQ_ADIANTAMENTO         NUMBER(10),
  DT_ENVIO_INTEGR_PADRAO      DATE,
  NR_CCO                      VARCHAR2(255 BYTE),
  NR_CHAVE_CUPOM              VARCHAR2(255 BYTE),
  CD_CNO                      VARCHAR2(14 BYTE),
  CD_IND_OBRA                 NUMBER(10),
  NR_SEQ_NOTA_REEMISSAO       NUMBER(10),
  DS_MOD_ECF                  VARCHAR2(2 BYTE),
  NR_SEQUENCIAL_ECF           NUMBER(10),
  NR_COO_ECF                  NUMBER(10),
  VL_MAT_TERC                 NUMBER(15,2),
  VL_SUB                      NUMBER(15,2),
  VL_DESCONTOS_CONDICIONAL    NUMBER(13,2),
  CD_CONTROL_CODE             VARCHAR2(14 BYTE),
  IE_MEIO_PAGAMENTO           VARCHAR2(3 BYTE),
  NR_AUTORIZACAO              VARCHAR2(15 BYTE),
  NR_CODIGO_CONTROLE          VARCHAR2(15 BYTE),
  DS_XML_COMPL                VARCHAR2(4000 BYTE),
  IE_TIPO_FATURA              VARCHAR2(1 BYTE)  DEFAULT null,
  NR_SEQ_RETORNO              NUMBER(10),
  NR_SEQ_LOTE_AUDIT           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          33M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTFISC_ADIANTA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_AGEPACI_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_AGENDA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_BANCO_FK_I ON TASY.NOTA_FISCAL
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CLAFIPS_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_CLASSIF_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CLAFIPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CLTITPA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CLTITPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CMENEXT_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_CME_ENVIO_EXT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CMENEXT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CMLOTRA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_TRANSF_CME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CMLOTRA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_COMCLIE_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CONPACI_FK_I ON TASY.NOTA_FISCAL
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_CONPAGA_FK_I ON TASY.NOTA_FISCAL
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CONTRAT_FK_I ON TASY.NOTA_FISCAL
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_CONVENI_FK_I ON TASY.NOTA_FISCAL
(CD_CONV_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_CONVENI_FK2_I ON TASY.NOTA_FISCAL
(CD_CONV_ENTRADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_CONVENI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_COPANFP_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_NF_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_COPANFP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_DISROMA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_ROMANEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_DISROMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_EMEFATU_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_EME_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_EMEFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_ESTABEL_FK_I ON TASY.NOTA_FISCAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_FARVNDA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_FAR_VENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_FORPAGA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_FORMA_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_FORPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_GRPRFIN_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_INSREGI_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_REG_INSPECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_I1 ON TASY.NOTA_FISCAL
(DT_ENTRADA_SAIDA, CD_ESTABELECIMENTO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_I2 ON TASY.NOTA_FISCAL
(DT_EMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_I3 ON TASY.NOTA_FISCAL
(NR_INTERNO_CONTA, NR_SEQ_PROTOCOLO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_I4 ON TASY.NOTA_FISCAL
(DT_ATUALIZACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_I5 ON TASY.NOTA_FISCAL
(CD_PESSOA_FISICA, DT_EMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_LOTCONT_FK_I ON TASY.NOTA_FISCAL
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_LOTEPRO_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_LOTE_PROT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_MODENOT_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_MOTIVO_DEVOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_MODENOT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_MODNOFI_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_MODNOFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_MOEDA_FK_I ON TASY.NOTA_FISCAL
(CD_MOEDA_ESTRANGEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_MOTCASO_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_MOTCASO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_NATOPER_FK_I ON TASY.NOTA_FISCAL
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_NFLONFE_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_NF_LOTE_NFE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_NOTFIEX_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_EXPORTADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_NOTFIEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_NOTFIIM_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_IMPORTADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_NOTFIIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_NOTFISC_FK_I ON TASY.NOTA_FISCAL
(NR_SEQUENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_NOTFISC_FK1_I ON TASY.NOTA_FISCAL
(NR_SEQ_NF_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_NOTFISC_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_NOTIDES_FK_I ON TASY.NOTA_FISCAL
(CD_TIPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          568K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_NOTIDES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_OPENOTA_FK_I ON TASY.NOTA_FISCAL
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_ORDCOMP_FK_I ON TASY.NOTA_FISCAL
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PEJUEMI_FK_I ON TASY.NOTA_FISCAL
(CD_CGC_EMITENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PESFISI_FK_I ON TASY.NOTA_FISCAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PESFISI_FK2_I ON TASY.NOTA_FISCAL
(CD_PACIENTE_INF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PESJURI_FK_I ON TASY.NOTA_FISCAL
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NOTFISC_PK ON TASY.NOTA_FISCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PLSCPSE_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_COB_PREVIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_PLSCPSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_PLSFATU_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_PLSFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_PLSFATU_FK2_I ON TASY.NOTA_FISCAL
(NR_SEQ_FATURA_NDC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_PLSFATU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_PLSLOPO_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_LOTE_RES_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PLSMENS_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PLSPAGP_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_PGTO_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_PLSPAGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_PLSPCTI_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_PROT_RES_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_PROCONV_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFISC_SETATEN_FK_I ON TASY.NOTA_FISCAL
(CD_SETOR_IMPRESSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_SURECON_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_RESP_CONSIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_SURECON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFISC_TRAFINA_FK_I ON TASY.NOTA_FISCAL
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFISC_TRAFINA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NOTFISC_UK ON TASY.NOTA_FISCAL
(CD_ESTABELECIMENTO, CD_CGC_EMITENTE, CD_SERIE_NF, NR_NOTA_FISCAL, NR_SEQUENCIA_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_update_consist
before update or insert ON TASY.NOTA_FISCAL for each row
declare
nr_sequencia_w		number(10);
ie_entrada_saida_w	varchar2(1);
ie_bloqueio_nr_serie_w	varchar2(1);
ie_bloqueio_nr_w	varchar2(1);
ie_consiste_estab_w	varchar2(1);

pragma autonomous_transaction;

begin

if	( ((updating) and (:new.dt_atualizacao_estoque is not null) and (:old.dt_atualizacao_estoque is null)) or /*Se a nota j�  existir e tiver calculando*/
	((inserting) and (:new.dt_atualizacao_estoque is not null))) and /*Se a nota est� sendo criada j� calculada*/
	(:new.ie_situacao = '1') and
	((:new.cd_cgc_emitente is not null) or (:new.ie_tipo_nota = 'EF')) then
	begin
	nr_sequencia_w		:= null;
	ie_bloqueio_nr_serie_w	:= substr(nvl(obter_valor_param_usuario(40, 102, Obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento),'S'),1,1);
	ie_consiste_estab_w	:= substr(nvl(obter_valor_param_usuario(40, 277, Obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento),'S'),1,1);

	if	(ie_bloqueio_nr_serie_w = 'D') then
		ie_bloqueio_nr_serie_w := substr(obter_consiste_nf_mesmo_nr(:new.cd_cgc_emitente, :new.cd_pessoa_fisica, :new.cd_estabelecimento),1,1);
	end if;

	if	(ie_bloqueio_nr_serie_w = 'P') then

		if	(:new.ie_tipo_nota = 'EF') then
			begin
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	nota_fiscal
			where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
			and	trunc(dt_emissao,'yyyy') = trunc(:new.dt_emissao,'yyyy')
			and	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	cd_serie_nf = :new.cd_serie_nf
			and	nr_nota_fiscal = :new.nr_nota_fiscal
			and	ie_situacao = '1'
			and	nr_sequencia <> :new.nr_sequencia
			and	dt_atualizacao_estoque is not null;
			end;
		else
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	nota_fiscal
			where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
			and	trunc(dt_emissao,'yyyy') = trunc(:new.dt_emissao,'yyyy')
			and	cd_cgc_emitente = :new.cd_cgc_emitente
			and	cd_serie_nf = :new.cd_serie_nf
			and	nr_nota_fiscal = :new.nr_nota_fiscal
			and	ie_situacao = '1'
			and	nr_sequencia <> :new.nr_sequencia
			and	dt_atualizacao_estoque is not null;
		end if;


	elsif	(ie_bloqueio_nr_serie_w = 'S') then

		if	(:new.ie_tipo_nota = 'EF') then
			begin

			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	nota_fiscal
			where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
			and	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	cd_serie_nf = :new.cd_serie_nf
			and	nr_nota_fiscal = :new.nr_nota_fiscal
			and	ie_situacao = '1'
			and	nr_sequencia <> :new.nr_sequencia
			and	dt_atualizacao_estoque is not null;

			end;
		else
			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	nota_fiscal
			where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
			and	cd_cgc_emitente = :new.cd_cgc_emitente
			and	cd_serie_nf = :new.cd_serie_nf
			and	nr_nota_fiscal = :new.nr_nota_fiscal
			and	ie_situacao = '1'
			and	nr_sequencia <> :new.nr_sequencia
			and	dt_atualizacao_estoque is not null;

		end if;


	end if;

	if	(nr_sequencia_w is null) then
		begin
		ie_bloqueio_nr_w	:= substr(nvl(obter_valor_param_usuario(40, 183, Obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento),'S'),1,1);

		if	(ie_bloqueio_nr_w = 'S') then

			if	(:new.ie_tipo_nota = 'EF') then
				begin
				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	nota_fiscal
				where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
				and	cd_pessoa_fisica = :new.cd_pessoa_fisica
				and	nr_nota_fiscal = :new.nr_nota_fiscal
				and	ie_situacao = '1'
				and	nr_sequencia <> :new.nr_sequencia
				and	dt_atualizacao_estoque is not null;
				end;
			else
				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	nota_fiscal
				where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
				and	cd_cgc_emitente = :new.cd_cgc_emitente
				and	nr_nota_fiscal = :new.nr_nota_fiscal
				and	ie_situacao = '1'
				and	nr_sequencia <> :new.nr_sequencia
				and	dt_atualizacao_estoque is not null;
			end if;

		elsif	(ie_bloqueio_nr_w = 'P') then
			if	(:new.ie_tipo_nota = 'EF')  then
				begin
				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	nota_fiscal
				where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
				and	trunc(dt_emissao,'yyyy') = trunc(:new.dt_emissao,'yyyy')
				and	cd_pessoa_fisica = :new.cd_pessoa_fisica
				and	nr_nota_fiscal = :new.nr_nota_fiscal
				and	ie_situacao = '1'
				and	nr_sequencia <> :new.nr_sequencia
				and	dt_atualizacao_estoque is not null;
				end;
			else
				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	nota_fiscal
				where	(ie_consiste_estab_w = 'N' or cd_estabelecimento = :new.cd_estabelecimento)
				and	trunc(dt_emissao,'yyyy') = trunc(:new.dt_emissao,'yyyy')
				and	cd_cgc_emitente = :new.cd_cgc_emitente
				and	nr_nota_fiscal = :new.nr_nota_fiscal
				and	ie_situacao = '1'
				and	nr_sequencia <> :new.nr_sequencia
				and	dt_atualizacao_estoque is not null;
			end if;


		end if;
		end;
	end if;

	if	(nr_sequencia_w > 0) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(248040,'NR_SEQUENCIA_W='||NR_SEQUENCIA_W);
	end if;

	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_afterinsert
after insert ON TASY.NOTA_FISCAL for each row
declare

qt_existe_w			number(10);
qt_registro_w			number(10);
ie_tipo_frete_w			regra_padrao_tipo_nf.ie_tipo_frete%type;
cd_cnpj_transportador_w		pessoa_juridica.cd_cgc%type;

Cursor C01 is
	select	cd_cnpj_transportador,
		ie_tipo_frete
	from	regra_padrao_tipo_nf
	where	nvl(cd_estabelecimento,:new.cd_estabelecimento) = :new.cd_estabelecimento
	and	((ie_entrada = 'S') and (:new.ie_tipo_nota = 'EN')) or
		((ie_complementar = 'S') and (:new.ie_tipo_nota = 'NC')) or
		((ie_entrada_pf = 'S') and (:new.ie_tipo_nota = 'EF')) or
		((ie_entrada_ep = 'S') and (:new.ie_tipo_nota = 'EP')) or
		((ie_saida_pf = 'S') and (:new.ie_tipo_nota = 'SF')) or
		((ie_saida_digitacao = 'S') and (:new.ie_tipo_nota = 'SD')) or
		((ie_saida_emissao = 'S') and (:new.ie_tipo_nota = 'SE'));

begin

select	count(1)
into	qt_existe_w
from	regra_padrao_tipo_nf;

select	count(1)
into	qt_registro_w
from	nota_fiscal_transportadora
where	nr_sequencia = :new.nr_sequencia;

if	(qt_existe_w > 0) and (qt_registro_w = 0) then

	open C01;
	loop
	fetch C01 into
		cd_cnpj_transportador_w,
		ie_tipo_frete_w;
	exit when C01%notfound;
		begin
		if	(nvl(cd_cnpj_transportador_w, '0') <> '0') then

			insert into nota_fiscal_transportadora (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_nota,
				cd_cnpj,
				ie_tipo_frete)
			values(	nota_fiscal_transportadora_seq.nextval,
				sysdate,
				:new.nm_usuario,
				:new.nr_sequencia,
				cd_cnpj_transportador_w,
				ie_tipo_frete_w);
		end if;
		end;
	end loop;
	close C01;


end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_atual
before update ON TASY.NOTA_FISCAL for each row
declare

qt_existe_w			number(10);
ie_tipo_ordem_w		ordem_compra.ie_tipo_ordem%type;
count_w				number(10);
reg_integracao_p	gerar_int_padrao.reg_integracao;

begin


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


select	count(*)
into	qt_existe_w
from	sup_parametro_integracao
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_evento = 'NF'
and	ie_forma = 'E'
and	ie_situacao = 'A';

if	(updating) then
	if	(:new.cd_cgc_emitente <> :old.cd_cgc_emitente) and (:new.nr_ordem_compra is not null) then
		begin

		select	nvl(ie_tipo_ordem,'N')
		into	ie_tipo_ordem_w
		from	ordem_compra
		where	nr_ordem_compra = :new.nr_ordem_compra;

		if	(ie_tipo_ordem_w = 'T') then
			begin
			wheb_mensagem_pck.Exibir_Mensagem_Abort(358504);
			end;
		end if;

		end;
	end if;

end if;

if	(qt_existe_w > 0) and
	(:new.ie_tipo_nota in ('SD','SE','SF')) and
	(:old.dt_atualizacao_estoque is null) and
	(:new.dt_atualizacao_Estoque is not null) then

	envio_sup_int_nf(
		:new.nr_sequencia,
		:new.ie_tipo_nota,
		:new.cd_estabelecimento,
		:new.nr_nota_fiscal,
		:new.dt_emissao,
		:new.cd_cgc_emitente,
		:new.cd_cgc,
		:new.cd_pessoa_fisica,
		:new.cd_serie_nf,
		:new.cd_natureza_operacao,
		:new.cd_condicao_pagamento,
		:new.cd_operacao_nf,
		:new.vl_seguro,
		:new.vl_despesa_acessoria,
		:new.vl_frete,
		:new.ds_observacao,
		:new.nm_usuario);
end if;


if	(:new.ie_situacao = '1') and
	(:new.dt_atualizacao_estoque is not null) and
	(:old.nr_nfe_imp is null) and
	(:new.nr_nfe_imp is not null) then

reg_integracao_p.ie_operacao	:=	'I';
reg_integracao_p.cd_operacao_nf :=  :new.cd_operacao_nf;
reg_integracao_p.cd_natureza_operacao := :new.cd_natureza_operacao;
reg_integracao_p.cd_estabelecimento := :new.cd_estabelecimento;

gerar_int_padrao.gravar_integracao('433', :new.nr_sequencia, 'TASY', reg_integracao_p);

end if;
<<Final>>
count_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_update
before update ON TASY.NOTA_FISCAL for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
nr_nota_fiscal_w		varchar2(255);
cd_funcao_ativa_w		number(5);
cd_perfil_ativo_w		number(5);
ds_historico_w		varchar2(255);
qt_reg_w			number(1);
nr_ordem_compra_w	number(10);
ie_altera_fornec_w		varchar2(1);

cursor c01 is
select	distinct nr_ordem_compra
from	nota_fiscal_item
where	nr_sequencia = :old.nr_sequencia
and	nr_ordem_compra is not null;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.cd_cgc is null) and
	(:new.cd_pessoa_fisica is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266259);
	--'Para emiss�o da nota tem que ter um CGC ou pessoa fisica informada');
end if;

select	(max(Obter_Valor_Param_Usuario(40, 275, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento)))
into	ie_altera_fornec_w
from	dual;

begin
select	to_number(:new.nr_nota_fiscal)
into	nr_nota_fiscal_w
from	dual;

:new.nr_nota_fiscal := nr_nota_fiscal_w;

exception when others then
	wheb_mensagem_pck.exibir_mensagem_abort(266260);
	--'N�o � permitido informar caracteres alfanum�ricos no n�mero da nota fiscal.');
end;

if	(ie_altera_fornec_w = 'S') and
	(:new.cd_cgc_emitente <> :old.cd_cgc_emitente) then

	open C01;
	loop
	fetch C01 into
		nr_ordem_compra_w;
	exit when C01%notfound;
		begin
		alterar_fornec_ordem_compra(nr_ordem_compra_w, :new.cd_cgc_emitente, :new.nm_usuario);
		end;
	end loop;
	close C01;
end if;

if	(:new.dt_atualizacao_estoque is null) and
	(:old.dt_atualizacao_estoque is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266261);
	--'Esta nota fiscal j� foi calculada, n�o pode ser alterada!');
end if;

/*Integra��o padr�o - Evento 65 - Enviar nota fiscal (antes de calcular)*/
if	(:new.dt_envio_integr_padrao is not null) and
	(:old.dt_envio_integr_padrao is null) then

	reg_integracao_p.ie_operacao		:=	'I';
	gerar_int_padrao.gravar_integracao('65', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
end if;


<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.NOTA_FISCAL_DELETE
BEFORE DELETE ON TASY.NOTA_FISCAL FOR EACH ROW
DECLARE

qt_existe_w	number(10);

begin

if	(:old.dt_atualizacao_estoque is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266256);
	--'Esta nota j� foi calculada, N�o pode mais ser modificada!');
end if;

select	count(*)
into	qt_existe_w
from	inspecao_registro
where	nr_seq_nf = :old.nr_sequencia
and	nvl(ie_origem,'X') = 'NF';

if	(qt_existe_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(324801);
else
	select	count(*)
	into	qt_existe_w
	from	inspecao_registro
	where	nr_seq_nf = :old.nr_sequencia;

	if	(qt_existe_w > 0) then
		update	inspecao_registro
		set	nr_seq_nf = null
		where	nr_seq_nf = :old.nr_sequencia;
	end if;
end if;

select	count(*)
into	qt_existe_w
from	inspecao_recebimento
where	nr_seq_nota_fiscal = :old.nr_sequencia;

if	(qt_existe_w > 0) then
	update	inspecao_recebimento
	set	nr_seq_nota_fiscal = null,
		nr_seq_item_nf	   = null
	where	nr_seq_nota_fiscal = :old.nr_sequencia;
end if;

if	(:old.nr_seq_lote_prot is not null) then
	update	lote_protocolo
	set	dt_geracao_nota	= null
	where	nr_sequencia 	= :old.nr_seq_lote_prot;
end if;

-- valida��es da fun��o OPS - Pagamento de Produ��o M�dica
select	count(1)
into	qt_existe_w
from	pls_pp_base_acum_trib
where	nr_seq_nota_fiscal = :old.nr_sequencia;

if	(qt_existe_w > 0) then
	-- N�o � poss�vel excluir esta nota, seus tributos foram utilizados como base de c�lculo na fun��o
	-- OPS - Pagamentos de Produ��o M�dica(nova).
	wheb_mensagem_pck.exibir_mensagem_abort(461022);
end if;

select	count(1)
into	qt_existe_w
from	pls_pp_lr_base_trib
where	nr_seq_nota_fiscal = :old.nr_sequencia;

if	(qt_existe_w > 0) then
	-- N�o � poss�vel excluir esta nota, seus tributos foram utilizados como base de c�lculo na fun��o
	-- OPS - Pagamentos de Produ��o M�dica(nova).
	wheb_mensagem_pck.exibir_mensagem_abort(461022);
end if;
-- fim valida��es da fun��o OPS - Pagamento de Produ��o M�dica

desvincular_nf_venc_contrato(:old.nr_sequencia);

END;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_insert
before insert or update ON TASY.NOTA_FISCAL 
for each row
declare

nr_nota_fiscal_w		nota_fiscal.nr_nota_fiscal%type;
nr_seq_produto_w		produto_financeiro.nr_sequencia%type;
nr_seq_grupo_prod_w		grupo_prod_financ.nr_seq_grupo_sup%type;
ie_pais_w				valor_dominio.vl_dominio%type;
ie_operacao_fiscal_w	operacao_nota.ie_operacao_fiscal%type;
cd_natureza_operacao_w	natureza_operacao.cd_natureza_operacao%type;
cd_setor_digitacao_w	setor_atendimento.cd_setor_atendimento%type;
cd_cgc_scp_w			nota_fiscal.cd_cgc_emitente%type; 		
ie_entrada_saida_w		operacao_estoque.ie_entrada_saida%type;
ie_recusa_w				operacao_nota.ie_recusa%type;
cd_operacao_estoque_w	operacao_nota.cd_operacao_estoque%type;
ie_considerar_dia_mes_w operacao_nota.ie_considerar_dia_mes%type;

nr_dia_limite_lanc_nota_w number(8);

dt_util_limite_lanc_nota_w	date;
count_w				number(10);
ie_status_w  number;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(inserting) then
  select nvl(max(ie_considerar_dia_mes),'N')	
    into ie_considerar_dia_mes_w
	  from operacao_nota
	 where cd_operacao_nf = :new.cd_operacao_nf;
	 
  if  (ie_considerar_dia_mes_w = 'S') then
    nr_dia_limite_lanc_nota_w := obter_valor_param_usuario(40, 490, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento);
    
    dt_util_limite_lanc_nota_w:= 	obter_dia_util_mes(:new.cd_estabelecimento, sysdate, nr_dia_limite_lanc_nota_w);
    
    if  ((nvl(trunc(dt_util_limite_lanc_nota_w), trunc(sysdate)) <= trunc(sysdate)) and (trunc(:new.dt_emissao) < trunc(sysdate, 'mm'))) then
        wheb_mensagem_pck.exibir_mensagem_abort(1075164);
    end if; 
  end if;

	if	(:new.cd_cgc is null) and
		(:new.cd_pessoa_fisica is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266257);
		--'Para emissao da nota tem que ter um CGC ou pessoa fisica informada');
	end if;

	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		cd_setor_digitacao_w := nvl(wheb_usuario_pck.get_cd_setor_atendimento,obter_setor_usuario(:new.nm_usuario));
		if	(nvl(cd_setor_digitacao_w,0) > 0) then
			:new.cd_setor_digitacao := cd_setor_digitacao_w;
		end if;
	end if;

	begin
	select	to_number(:new.nr_nota_fiscal)
	into	nr_nota_fiscal_w
	from	dual;

	:new.nr_nota_fiscal := nr_nota_fiscal_w;

	exception when others then
		wheb_mensagem_pck.exibir_mensagem_abort(266258);
		--'Nao e permitido informar caracteres alfanumericos no numero da nota fiscal.');
	end;

	obter_produto_financeiro(:new.cd_estabelecimento,null,null,nr_seq_produto_w,null,null,nr_seq_grupo_prod_w);

	if	(nvl(nr_seq_grupo_prod_w,0) > 0) then
		:new.nr_seq_grupo_prod := nr_seq_grupo_prod_w;
	end if;
	
	if	(:new.IE_TIPO_NOTA = 'EF') then
		:new.cd_cgc := OBTER_CGC_ESTABELECIMENTO(:new.cd_estabelecimento);
	end if;
	
	if (:new.cd_estabelecimento is not null) and (:new.ie_tipo_nota in('SD', 'SE', 'SF', 'ST', 'EP')) then
		
		select 	max(cd_operacao_estoque),
				max(ie_recusa)
		into	cd_operacao_estoque_w,
				ie_recusa_w
		from 	operacao_nota
		where 	cd_operacao_nf = :new.cd_operacao_nf;
		
		select 	max(ie_entrada_saida)
		into 	ie_entrada_saida_w
		from 	operacao_estoque
		where 	cd_operacao_estoque = cd_operacao_estoque_w;
										
		if (ie_entrada_saida_w = 'S') or (ie_recusa_w = 'S') then	
			
			select max(obter_cgc_estabelecimento(cd_estab_socio_ost_scp))
			into cd_cgc_scp_w
			from estabelecimento 
			where cd_estabelecimento = :new.cd_estabelecimento
			and ie_scp = 'S';
			
			if (cd_cgc_scp_w is not null) then
				:new.cd_cgc_emitente := cd_cgc_scp_w;
			end if;
			
		end if;
	end if;
	
	
end if;

if (updating) then
	if (:old.dt_atualizacao_estoque is null
	    and :new.dt_atualizacao_estoque is not null) then
		select 	count(*) 
		into 	ie_status_w
		from 	NOTA_FISCAL_ITEM
		where 	NR_SEQUENCIA = :new.nr_sequencia 
		and 	obter_bloq_canc_proj_rec(nr_seq_proj_rec) > 0
		and 	rownum = 1;

		if (ie_status_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1144309);
		end if;
	end if;
end if;

ie_pais_w	:=	nvl(pkg_i18n.get_user_locale, 'pt_BR');

if	(ie_pais_w = 'es_MX') then
	begin
	begin
	select	cd_natureza_operacao
	into	cd_natureza_operacao_w
	from	natureza_operacao
	where	cd_natureza_operacao = :new.cd_natureza_operacao;
	exception
	when others then
		:new.cd_natureza_operacao	:=	null;
	end;

	if	(:new.cd_operacao_nf > 0) and
		(nvl(:new.cd_natureza_operacao,0) = 0) then
		begin
		select	nvl(ie_operacao_fiscal,'E')
		into	ie_operacao_fiscal_w
		from	operacao_nota
		where	cd_operacao_nf = :new.cd_operacao_nf;
		
		select	nvl(max(cd_natureza_operacao),0)
		into	cd_natureza_operacao_w
		from	natureza_operacao
		where	ie_entrada_saida = ie_operacao_fiscal_w
		and	ie_situacao = 'A';
		
		if	(cd_natureza_operacao_w = 0) then
		
			select	nvl(max(cd_natureza_operacao),0) + 1
			into	cd_natureza_operacao_w
			from	natureza_operacao;
			
			insert into natureza_operacao(
				cd_natureza_operacao,
				ds_natureza_operacao,
				ie_entrada_saida,
				ie_situacao,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_transferencia_estab,
				ie_integra_contas_pagar,
				ie_integra_contas_receber)
			values(	cd_natureza_operacao_w,
				decode(ie_operacao_fiscal_w,'S',WHEB_MENSAGEM_PCK.get_texto(312467), WHEB_MENSAGEM_PCK.get_texto(312468)),  --'Saida','Entrada'
				ie_operacao_fiscal_w,
				'A',
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				'S',
				'S',
				'S');
		end if;
		
		:new.cd_natureza_operacao := cd_natureza_operacao_w;		
		end;
	end if;
	end;
end if;

<<Final>>
count_w	:= 0;

END;
/


ALTER TABLE TASY.NOTA_FISCAL ADD (
  CONSTRAINT NOTFISC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT NOTFISC_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_CGC_EMITENTE, CD_SERIE_NF, NR_NOTA_FISCAL, NR_SEQUENCIA_NF)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          10M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL ADD (
  CONSTRAINT NOTFISC_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_ESTRANGEIRA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT NOTFISC_FARVNDA_FK 
 FOREIGN KEY (NR_SEQ_FAR_VENDA) 
 REFERENCES TASY.FAR_VENDA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_ADIANTA_FK 
 FOREIGN KEY (NR_SEQ_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT NOTFISC_PLSFATU_FK2 
 FOREIGN KEY (NR_SEQ_FATURA_NDC) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_COPANFP_FK 
 FOREIGN KEY (NR_SEQ_NF_PAGADOR) 
 REFERENCES TASY.CONTA_PACIENTE_NF_PAG (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT NOTFISC_PLSFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_FORPAGA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_PAGTO) 
 REFERENCES TASY.FORMA_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT NOTFISC_LOTEPRO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PROT) 
 REFERENCES TASY.LOTE_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_PAC) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_NOTIDES_FK 
 FOREIGN KEY (CD_TIPO_SERVICO) 
 REFERENCES TASY.NOTA_TIPO_SERVICO (CD_TIPO_SERVICO),
  CONSTRAINT NOTFISC_INSREGI_FK 
 FOREIGN KEY (NR_SEQ_REG_INSPECAO) 
 REFERENCES TASY.INSPECAO_REGISTRO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_NOTFIIM_FK 
 FOREIGN KEY (NR_SEQ_IMPORTADA) 
 REFERENCES TASY.NOTA_FISCAL_IMPORTADA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CONVENI_FK2 
 FOREIGN KEY (CD_CONV_ENTRADA) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT NOTFISC_NFLONFE_FK 
 FOREIGN KEY (NR_SEQ_NF_LOTE_NFE) 
 REFERENCES TASY.NOTA_FISCAL_LOTE_NFE (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_DISROMA_FK 
 FOREIGN KEY (NR_SEQ_ROMANEIO) 
 REFERENCES TASY.DIS_ROMANEIO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_MODNOFI_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MODELO_NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_PESFISI_FK2 
 FOREIGN KEY (CD_PACIENTE_INF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NOTFISC_PLSPAGP_FK 
 FOREIGN KEY (NR_SEQ_PGTO_PREST) 
 REFERENCES TASY.PLS_PAGAMENTO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_NOTFISC_FK1 
 FOREIGN KEY (NR_SEQ_NF_REF) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CMENEXT_FK 
 FOREIGN KEY (NR_SEQ_CME_ENVIO_EXT) 
 REFERENCES TASY.CM_ENVIO_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CMLOTRA_FK 
 FOREIGN KEY (NR_SEQ_TRANSF_CME) 
 REFERENCES TASY.CM_LOTE_TRANSFERENCIA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_SURECON_FK 
 FOREIGN KEY (NR_SEQ_RESP_CONSIG) 
 REFERENCES TASY.SUP_RESP_CONSIGNADO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_EMEFATU_FK 
 FOREIGN KEY (NR_SEQ_EME_FATURA) 
 REFERENCES TASY.EME_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_PLSCPSE_FK 
 FOREIGN KEY (NR_SEQ_COB_PREVIA) 
 REFERENCES TASY.PLS_COBRANCA_PREVIA_SERV (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_IMPRESSAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NOTFISC_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT NOTFISC_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT NOTFISC_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT NOTFISC_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT NOTFISC_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT NOTFISC_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT NOTFISC_PEJUEMI_FK 
 FOREIGN KEY (CD_CGC_EMITENTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT NOTFISC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NOTFISC_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT NOTFISC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT NOTFISC_NOTFISC_FK 
 FOREIGN KEY (NR_SEQUENCIA_REF) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CLAFIPS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_FISCAL) 
 REFERENCES TASY.CLASSIF_FISCAL_PREST_SERV (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_NOTFIEX_FK 
 FOREIGN KEY (NR_SEQ_EXPORTADA) 
 REFERENCES TASY.NOTA_FISCAL_EXPORTADA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NOTFISC_MODENOT_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEVOL) 
 REFERENCES TASY.MOTIVO_DEVOLUCAO_NOTA (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_MOTCASO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.MOTIVO_CANCEL_SC_OC (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_PLSPCTI_FK 
 FOREIGN KEY (NR_SEQ_PROT_RES_PLS) 
 REFERENCES TASY.PLS_PROT_CONTA_TITULO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_PLSLOPO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RES_PLS) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT NOTFISC_CONVENI_FK 
 FOREIGN KEY (CD_CONV_INTEGRACAO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.NOTA_FISCAL TO NIVEL_1;

