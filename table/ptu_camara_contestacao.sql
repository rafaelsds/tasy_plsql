ALTER TABLE TASY.PTU_CAMARA_CONTESTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_CAMARA_CONTESTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_CAMARA_CONTESTACAO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  CD_UNIMED_DESTINO            VARCHAR2(4 BYTE) NOT NULL,
  CD_UNIMED_ORIGEM             VARCHAR2(10 BYTE) NOT NULL,
  CD_UNIMED_CREDORA            VARCHAR2(10 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DT_GERACAO                   DATE,
  DT_VENC_FATURA               DATE,
  VL_TOTAL_FATURA              NUMBER(12,2),
  VL_TOTAL_CONTESTACAO         NUMBER(12,2),
  VL_TOTAL_ACORDO              NUMBER(12,2),
  IE_TIPO_ARQUIVO              NUMBER(5),
  NR_VERSAO_TRANSACAO          NUMBER(2),
  VL_TOTAL_PAGO                NUMBER(12,2),
  IE_OPERACAO                  VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_CONTESTACAO           NUMBER(10),
  DT_ENVIO_ARQUIVO             DATE,
  NR_DOCUMENTO                 NUMBER(11),
  DT_VENC_DOC                  DATE,
  IE_CONCLUSAO                 NUMBER(1),
  NR_SEQ_LOTE_CONTEST          NUMBER(10),
  NR_SEQ_LOTE_DISCUSSAO        NUMBER(10),
  DT_CANCELAMENTO              DATE,
  NM_USUARIO_ENVIO             VARCHAR2(15 BYTE),
  DT_POSTAGEM_ARQUIVO          DATE,
  IE_CLASSIF_COBRANCA_A500     VARCHAR2(1 BYTE),
  NR_NOTA_CREDITO_DEBITO_A500  VARCHAR2(30 BYTE),
  DT_VENCIMENTO_NDC_A500       DATE,
  VL_TOTAL_NDC_A500            NUMBER(15,2),
  VL_TOTAL_CONTEST_NDC         NUMBER(15,2),
  VL_TOTAL_PAGO_NDC            NUMBER(15,2),
  NR_DOCUMENTO2                VARCHAR2(30 BYTE),
  DT_VENC_DOC2                 DATE,
  IE_STATUS                    VARCHAR2(3 BYTE),
  NR_FATURA                    VARCHAR2(30 BYTE),
  QT_TOT_R552                  NUMBER(10),
  IE_STATUS_IMP                VARCHAR2(3 BYTE),
  DS_SID_PROCESSO              VARCHAR2(255 BYTE),
  DS_SERIAL_PROCESSO           VARCHAR2(255 BYTE),
  IE_TIPO_ARQUIVO_COB          VARCHAR2(10 BYTE),
  TP_ARQ_PARCIAL               VARCHAR2(1 BYTE),
  DS_HASH                      VARCHAR2(255 BYTE),
  DS_ARQUIVO                   VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.PTUCAMC_ESTABEL_FK_I ON TASY.PTU_CAMARA_CONTESTACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCAMC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUCAMC_I1 ON TASY.PTU_CAMARA_CONTESTACAO
(NM_USUARIO_NREC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUCAMC_I2 ON TASY.PTU_CAMARA_CONTESTACAO
(NR_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTUCAMC_PK ON TASY.PTU_CAMARA_CONTESTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCAMC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUCAMC_PLSLOCO_FK_I ON TASY.PTU_CAMARA_CONTESTACAO
(NR_SEQ_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCAMC_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUCAMC_PLSLODI_FK_I ON TASY.PTU_CAMARA_CONTESTACAO
(NR_SEQ_LOTE_DISCUSSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCAMC_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUCAMC_PTUCAMC_FK_I ON TASY.PTU_CAMARA_CONTESTACAO
(NR_SEQ_CONTESTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCAMC_PTUCAMC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ptu_camara_contestacao_ins_up
before insert or update ON TASY.PTU_CAMARA_CONTESTACAO for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Alterar status do lote de contesta��o / discuss�o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
1               Arquivo para inclus�o de questionamentos
2               Arquivo de confirma��o da inclus�o de questionamentos na C�mara Arbitral
3               Fechamento parcial da Unimed Credora da NDC
4               Fechamento parcial da Unimed Devedora da NDC
5*		Arquivo de fechamento da Unimed Credora da NDC
6*		Arquivo de fechamento da Unimed Devedora da NDC
7*              Fechamento complementar da Unimed Credora da NDC
8*              Fechamento complementar da Unimed Devedora da NDC
9*              Fechamento por decurso de prazo
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

Cursor C01 (	nr_seq_lote_contest_pc	pls_lote_contestacao.nr_sequencia%type) is
	select	a.nr_seq_conta
	from	pls_contestacao a
	where	a.nr_seq_lote	= nr_seq_lote_contest_pc
	group by
		a.nr_seq_conta;

begin
if	(:new.ie_tipo_arquivo in (5,6,7,8)) and
	(:new.nr_seq_lote_discussao is not null) then -- A op��o "9" ser� tratado dentro da trigger PLS_LOTE_DISCUSSAO_INSERT

	--Altera o status com a rotina nova
	pls_altera_status_contestacao(:new.nr_seq_lote_contest, 'C', :new.nm_usuario);

	--atualiza a data de vencimento separadamente
	update	pls_lote_contestacao
	set	dt_fechamento	= nvl(dt_fechamento,sysdate)
	where	nr_sequencia	= :new.nr_seq_lote_contest;

	for r_c01_w in c01( :new.nr_seq_lote_contest ) loop
		pls_atualiza_status_copartic( r_c01_w.nr_seq_conta, 'LC', :new.nr_sequencia, :new.nm_usuario, :new.cd_estabelecimento);
	end loop;
end if;

end;
/


ALTER TABLE TASY.PTU_CAMARA_CONTESTACAO ADD (
  CONSTRAINT PTUCAMC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PTUCAMC_UK
 UNIQUE (NR_SEQ_LOTE_CONTEST));

ALTER TABLE TASY.PTU_CAMARA_CONTESTACAO ADD (
  CONSTRAINT PTUCAMC_PTUCAMC_FK 
 FOREIGN KEY (NR_SEQ_CONTESTACAO) 
 REFERENCES TASY.PTU_CAMARA_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PTUCAMC_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PTUCAMC_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_DISCUSSAO) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT PTUCAMC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PTU_CAMARA_CONTESTACAO TO NIVEL_1;

