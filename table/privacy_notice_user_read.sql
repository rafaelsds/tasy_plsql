ALTER TABLE TASY.PRIVACY_NOTICE_USER_READ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRIVACY_NOTICE_USER_READ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRIVACY_NOTICE_USER_READ
(
  NR_SEQUENCIA         NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  LOGGED_IN_USER       VARCHAR2(15 BYTE),
  PRIVACY_CUSTOMER_ID  VARCHAR2(10 BYTE),
  PRIVACY_PHILIPS_ID   VARCHAR2(10 BYTE),
  PRIVACY_READ_STATUS  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRINOTU_PK ON TASY.PRIVACY_NOTICE_USER_READ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRIVACY_NOTICE_USER_READ ADD (
  CONSTRAINT PRINOTU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PRIVACY_NOTICE_USER_READ TO NIVEL_1;

