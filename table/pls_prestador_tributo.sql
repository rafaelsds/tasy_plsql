ALTER TABLE TASY.PLS_PRESTADOR_TRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_TRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_TRIBUTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR      NUMBER(10),
  CD_TRIBUTO            NUMBER(3)               NOT NULL,
  NR_SEQ_PRESTADOR_MED  NUMBER(10),
  VL_TRIBUTO            NUMBER(15,2)            NOT NULL,
  DT_REFERENCIA         DATE                    NOT NULL,
  VL_BASE_CALCULO       NUMBER(15,2),
  DS_OBSERVACAO         VARCHAR2(4000 BYTE),
  DS_EMP_RETENCAO       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRTR_PK ON TASY.PLS_PRESTADOR_TRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRTR_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_TRIBUTO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRTR_PLSPRME_FK_I ON TASY.PLS_PRESTADOR_TRIBUTO
(NR_SEQ_PRESTADOR_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRTR_PLSPRME_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prestador_tributo_delete
after delete ON TASY.PLS_PRESTADOR_TRIBUTO for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_TRIBUTO',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


ALTER TABLE TASY.PLS_PRESTADOR_TRIBUTO ADD (
  CONSTRAINT PLSPRTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_TRIBUTO ADD (
  CONSTRAINT PLSPRTR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRTR_PLSPRME_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_MED) 
 REFERENCES TASY.PLS_PRESTADOR_MEDICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_TRIBUTO TO NIVEL_1;

