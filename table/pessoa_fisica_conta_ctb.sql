ALTER TABLE TASY.PESSOA_FISICA_CONTA_CTB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_CONTA_CTB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_CONTA_CTB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_CONTA        VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFICC_CONCONT_FK_I ON TASY.PESSOA_FISICA_CONTA_CTB
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFICC_EMPRESA_FK_I ON TASY.PESSOA_FISICA_CONTA_CTB
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFICC_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFICC_ESTABEL_FK_I ON TASY.PESSOA_FISICA_CONTA_CTB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFICC_PESFISI_FK_I ON TASY.PESSOA_FISICA_CONTA_CTB
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFICC_PK ON TASY.PESSOA_FISICA_CONTA_CTB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_conta_ctb_update
BEFORE UPDATE ON TASY.PESSOA_FISICA_CONTA_CTB FOR EACH ROW
DECLARE
qt_existe_vigencia_w		Number(3);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;
ie_conta_vigente_w		Varchar2(1);
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	count(*)
into	qt_existe_vigencia_w
from	conta_contabil
where	cd_conta_contabil = :new.cd_conta_contabil
and	dt_inicio_vigencia is not null
and	dt_fim_vigencia is not null;

if	(qt_existe_vigencia_w > 0) then
	begin
	if	(:new.dt_inicio_vigencia is null) or
		(:new.dt_fim_vigencia is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(278469);
	end if;

	select	dt_inicio_vigencia,
		dt_fim_vigencia
	into	dt_inicio_vigencia_w,
		dt_fim_vigencia_w
	from	conta_contabil
	where	cd_conta_contabil = :new.cd_conta_contabil;

	if	(dt_inicio_vigencia_w < :new.dt_inicio_vigencia) then
		Wheb_mensagem_pck.exibir_mensagem_abort(278471);
	end if;
	if	(dt_fim_vigencia_w > :new.dt_fim_vigencia) then
		Wheb_mensagem_pck.exibir_mensagem_abort(278473);
	end if;

	end;
end if;

if	(:new.dt_inicio_vigencia > :new.dt_fim_vigencia) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278475);
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_conta_ctb_atual
BEFORE INSERT OR UPDATE ON TASY.PESSOA_FISICA_CONTA_CTB for each row
declare

cd_estabelecimento_w	number(4);
cd_empresa_w			number(4);
ds_contabil_w		varchar2(100);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	ds_contabil_w		:= substr(wheb_mensagem_pck.get_texto(351029,''),1, 100);
	CTB_Consistir_Conta_Titulo(:new.cd_conta_contabil, ds_contabil_w);
	end;
end if;

end;
/


ALTER TABLE TASY.PESSOA_FISICA_CONTA_CTB ADD (
  CONSTRAINT PESFICC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_CONTA_CTB ADD (
  CONSTRAINT PESFICC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESFICC_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PESFICC_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA)
    ON DELETE CASCADE,
  CONSTRAINT PESFICC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PESSOA_FISICA_CONTA_CTB TO NIVEL_1;

