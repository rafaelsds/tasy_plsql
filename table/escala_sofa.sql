ALTER TABLE TASY.ESCALA_SOFA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SOFA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SOFA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_LIBERACAO               DATE,
  QT_REL_PAO2_FIO2           NUMBER(3),
  QT_PLAQUETAS               NUMBER(15),
  QT_BILIRRUBINA_SERICA      NUMBER(15,2),
  QT_PAM                     NUMBER(3),
  IE_DOBUTAMINA              VARCHAR2(1 BYTE),
  IE_DOPAMINA                NUMBER(1)          NOT NULL,
  IE_ADRENALINA              NUMBER(1)          NOT NULL,
  IE_NORADRENALINA           NUMBER(1)          NOT NULL,
  QT_GLASGOW                 NUMBER(2),
  QT_CREATININA_SERICA       NUMBER(4,2)        DEFAULT null,
  QT_DIURESE                 NUMBER(5),
  QT_PA_SISTOLICA            NUMBER(3),
  QT_PA_DIASTOLICA           NUMBER(3),
  QT_PTO_RESP                NUMBER(1)          NOT NULL,
  QT_PTO_COAG                NUMBER(1)          NOT NULL,
  QT_PTO_FIGADO              NUMBER(1)          NOT NULL,
  QT_PTO_CARDIO              NUMBER(1)          NOT NULL,
  QT_PTO_SNC                 NUMBER(1)          NOT NULL,
  QT_PTO_RENAL               NUMBER(1)          NOT NULL,
  QT_PONTUACAO               NUMBER(3)          NOT NULL,
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSOFA_ATEPACI_FK_I ON TASY.ESCALA_SOFA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSOFA_EHRREEL_FK_I ON TASY.ESCALA_SOFA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSOFA_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSOFA_EHRREGI_FK_I ON TASY.ESCALA_SOFA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSOFA_I1 ON TASY.ESCALA_SOFA
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSOFA_PERFIL_FK_I ON TASY.ESCALA_SOFA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSOFA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSOFA_PESFISI_FK_I ON TASY.ESCALA_SOFA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSOFA_PK ON TASY.ESCALA_SOFA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSOFA_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCSOFA_TASASDI_FK_I ON TASY.ESCALA_SOFA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSOFA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSOFA_TASASDI_FK2_I ON TASY.ESCALA_SOFA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSOFA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_sofa_befins
before insert or update ON TASY.ESCALA_SOFA for each row
declare
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Pontuacao relacao Pa02/FiO2 */
if	(:new.qt_rel_pao2_fio2 is null) then
	:new.qt_pto_resp:= 0;
elsif	(:new.qt_rel_pao2_fio2 >= 400) then
	:new.qt_pto_resp:= 0;
elsif	(:new.qt_rel_pao2_fio2 >= 300) and
	(:new.qt_rel_pao2_fio2 < 400) then
	:new.qt_pto_resp:= 1;
elsif	(:new.qt_rel_pao2_fio2 >= 200) and
	(:new.qt_rel_pao2_fio2 < 300) then
	:new.qt_pto_resp:= 2;
elsif	(:new.qt_rel_pao2_fio2 >= 100) and
	(:new.qt_rel_pao2_fio2 < 200) then
	:new.qt_pto_resp:= 3;
elsif	(:new.qt_rel_pao2_fio2 < 100) then
	:new.qt_pto_resp:= 4;
end if;
/* Pontuacao Contagem de plaquetas */
if	(:new.qt_plaquetas is null) then
	:new.qt_pto_coag:= 0;
elsif	(:new.qt_plaquetas >= 150) then
	:new.qt_pto_coag:= 0;
elsif	(:new.qt_plaquetas >= 100) and
	(:new.qt_plaquetas < 150) then
	:new.qt_pto_coag:= 1;
elsif	(:new.qt_plaquetas >= 50) and
	(:new.qt_plaquetas < 100) then
	:new.qt_pto_coag:= 2;
elsif	(:new.qt_plaquetas >= 20) and
	(:new.qt_plaquetas < 50) then
	:new.qt_pto_coag:= 3;
elsif	(:new.qt_plaquetas < 20) then
	:new.qt_pto_coag:= 4;
end if;
/* Pontuacao Bilirrubina serica */
if	(:new.qt_bilirrubina_serica is null) then
	:new.qt_pto_figado:= 0;
elsif	(:new.qt_bilirrubina_serica >= 12) then
	:new.qt_pto_figado:= 4;
elsif	(:new.qt_bilirrubina_serica >= 6) and
	(:new.qt_bilirrubina_serica < 11.9) then
	:new.qt_pto_figado:= 3;
elsif	(:new.qt_bilirrubina_serica > 2) and
	(:new.qt_bilirrubina_serica < 6) then
	:new.qt_pto_figado:= 2;
elsif	(:new.qt_bilirrubina_serica >= 1.2) and
	(:new.qt_bilirrubina_serica < 2) then
	:new.qt_pto_figado:= 1;
elsif	(:new.qt_bilirrubina_serica < 1.2) then
	:new.qt_pto_figado:= 0;
end if;
/* Pontuacao para Hipotensao */
if	(:new.qt_pa_diastolica is not null) and
	(:new.qt_pa_sistolica is not null) then
	:new.qt_pam:= nvl(dividir((:new.qt_pa_sistolica + (:new.qt_pa_diastolica * 2)), 3),0);
end if;

if	(:new.ie_dobutamina = 'N') and
	(:new.qt_pam is null) and
	(:new.ie_noradrenalina = 0) and
	(:new.ie_adrenalina = 0) and
	(:new.ie_dopamina = 0) then
	:new.qt_pto_cardio	:= 0;
elsif	(:new.ie_noradrenalina = 2) or
	(:new.ie_adrenalina = 2) or
	(:new.ie_dopamina = 3) then
	:new.qt_pto_cardio	:= 4;
elsif	(:new.ie_dopamina = 2) or
	(:new.ie_adrenalina = 1) or
	(:new.ie_noradrenalina = 1) then
	:new.qt_pto_cardio	:= 3;
elsif	(:new.ie_dobutamina = 'S') or
	(:new.ie_dopamina = 1) then
	:new.qt_pto_cardio	:= 2;
elsif	(:new.qt_pam < 70) then
	:new.qt_pto_cardio	:= 1;
elsif	(:new.qt_pam >= 70) then
	:new.qt_pto_cardio	:= 0;
end if;
/* Pontuacao Glasgow */
if	(:new.qt_glasgow is null) then
	:new.qt_pto_snc	:= 0;
elsif	(:new.qt_glasgow >= 15) then
	:new.qt_pto_snc	:= 0;
elsif	(:new.qt_glasgow >= 13) and
	(:new.qt_glasgow < 15) then
	:new.qt_pto_snc	:= 1;
elsif	(:new.qt_glasgow >= 10) and
	(:new.qt_glasgow < 13) then
	:new.qt_pto_snc	:= 2;
elsif	(:new.qt_glasgow >= 6) and
	(:new.qt_glasgow < 10) then
	:new.qt_pto_snc	:= 3;
elsif	(:new.qt_glasgow < 6) then
	:new.qt_pto_snc	:= 4;
end if;

/* Pontuacao Creatinina Serica ou Diurese */
if	(:new.qt_creatinina_serica is null) and
	(:new.qt_diurese is null) then
	:new.qt_pto_renal	:= 0;
elsif	(:new.qt_creatinina_serica > 5) or
	(:new.qt_diurese < 200) then
	:new.qt_pto_renal:= 4;
elsif	(:new.qt_creatinina_serica >= 3.5) and
	(:new.qt_creatinina_serica < 5) or
	(:new.qt_diurese > 200) and
	(:new.qt_diurese < 500) then
	:new.qt_pto_renal:= 3;
elsif	(:new.qt_creatinina_serica >= 2) and
	(:new.qt_creatinina_serica < 3.5) then
	:new.qt_pto_renal:= 2;
elsif	(:new.qt_creatinina_serica >= 1.2) and
	(:new.qt_creatinina_serica < 2) then
	:new.qt_pto_renal:= 1;
elsif	(:new.qt_creatinina_serica < 1.2) then
	:new.qt_pto_renal:= 0;
end if;

/* Obter a pontuacao total */
:new.qt_pontuacao:= nvl(:new.qt_pto_resp,0)	+ nvl(:new.qt_pto_coag,0)	+ nvl(:new.qt_pto_figado,0) +
		    nvl(:new.qt_pto_cardio,0)	+ nvl(:new.qt_pto_snc,0)	+ nvl(:new.qt_pto_renal,0);

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_sofa_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_liberacao, :new.dt_avaliacao, :new.QT_REL_PAO2_FIO2,
						:new.ie_dopamina, :new.qt_pam, :new.ie_dobutamina , :new.ie_noradrenalina, :new.qt_plaquetas,
						:new.qt_creatinina_serica, :new.qt_bilirrubina_serica, :new.qt_glasgow, :new.qt_pa_sistolica);
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_sofa_pend_atual
after insert or update ON TASY.ESCALA_SOFA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'18');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_SOFA_delete
after delete ON TASY.ESCALA_SOFA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '18'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_SOFA ADD (
  CONSTRAINT ESCSOFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SOFA ADD (
  CONSTRAINT ESCSOFA_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCSOFA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSOFA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSOFA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ESCSOFA_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCSOFA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCSOFA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_SOFA TO NIVEL_1;

