ALTER TABLE TASY.W_PACOTE_EXP_FUNC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PACOTE_EXP_FUNC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PACOTE_EXP_FUNC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PACOTE        NUMBER(10)               NOT NULL,
  CD_FUNCAO            NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPAEXFU_FUNCAO_FK_I ON TASY.W_PACOTE_EXP_FUNC
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WPAEXFU_PK ON TASY.W_PACOTE_EXP_FUNC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPAEXFU_WPACEXP_FK_I ON TASY.W_PACOTE_EXP_FUNC
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PACOTE_EXP_FUNC ADD (
  CONSTRAINT WPAEXFU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PACOTE_EXP_FUNC ADD (
  CONSTRAINT WPAEXFU_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT WPAEXFU_WPACEXP_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.W_PACOTE_EXPRESSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PACOTE_EXP_FUNC TO NIVEL_1;

