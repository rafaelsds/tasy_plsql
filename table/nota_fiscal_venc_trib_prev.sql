ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB_PREV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_VENC_TRIB_PREV CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_VENC_TRIB_PREV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_NOTA          NUMBER(10)               NOT NULL,
  DT_VENCIMENTO        DATE                     NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  VL_TRIBUTO           NUMBER(13,2),
  VL_BASE_CALCULO      NUMBER(15,2)             NOT NULL,
  TX_TRIBUTO           NUMBER(15,4),
  VL_TRIB_NAO_RETIDO   NUMBER(15,2),
  VL_BASE_NAO_RETIDO   NUMBER(15,2),
  VL_TRIB_ADIC         NUMBER(15,2),
  VL_BASE_ADIC         NUMBER(15,2),
  VL_REDUCAO           NUMBER(15,2),
  VL_DESC_BASE         NUMBER(15,2),
  IE_ORIGEM            VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_REGRA_TRIB    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFVTPRE_NOTFIVE_FK_I ON TASY.NOTA_FISCAL_VENC_TRIB_PREV
(NR_SEQ_NOTA, DT_VENCIMENTO, IE_ORIGEM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFVTPRE_PK ON TASY.NOTA_FISCAL_VENC_TRIB_PREV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFVTPRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.NFVTPRE_TRIBUTO_FK_I ON TASY.NOTA_FISCAL_VENC_TRIB_PREV
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFVTPRE_TRIBUTO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_trib_prev_ins
BEFORE INSERT ON TASY.NOTA_FISCAL_VENC_TRIB_PREV FOR EACH ROW
BEGIN

if	(:new.ie_origem is null) then
	:new.ie_origem := 'N';
end if;

END;
/


ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB_PREV ADD (
  CONSTRAINT NFVTPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB_PREV ADD (
  CONSTRAINT NFVTPRE_NOTFIVE_FK 
 FOREIGN KEY (NR_SEQ_NOTA, DT_VENCIMENTO, IE_ORIGEM) 
 REFERENCES TASY.NOTA_FISCAL_VENC (NR_SEQUENCIA,DT_VENCIMENTO,IE_ORIGEM)
    ON DELETE CASCADE,
  CONSTRAINT NFVTPRE_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.NOTA_FISCAL_VENC_TRIB_PREV TO NIVEL_1;

