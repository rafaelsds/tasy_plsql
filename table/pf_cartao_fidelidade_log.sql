ALTER TABLE TASY.PF_CARTAO_FIDELIDADE_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_CARTAO_FIDELIDADE_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_CARTAO_FIDELIDADE_LOG
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE,
  NM_USUARIO            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CARTAO         NUMBER(10),
  DT_INICIO_VALIDADE    DATE,
  DT_FIM_VALIDADE       DATE,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  NR_CARTAO_FIDELIDADE  VARCHAR2(16 BYTE),
  DT_EMISSAO_CARTAO     DATE,
  CD_PESSOA_TITULAR     VARCHAR2(10 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_PESSOA_VENDEDOR    VARCHAR2(10 BYTE),
  QT_PONTUACAO_CM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PFCAFIL_PK ON TASY.PF_CARTAO_FIDELIDADE_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFCAFIL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PF_CARTAO_FIDELIDADE_LOG_tp  after update ON TASY.PF_CARTAO_FIDELIDADE_LOG FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_TITULAR,1,4000),substr(:new.CD_PESSOA_TITULAR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_TITULAR',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_VENDEDOR,1,4000),substr(:new.CD_PESSOA_VENDEDOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_VENDEDOR',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTAO,1,4000),substr(:new.NR_SEQ_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VALIDADE,1,4000),substr(:new.DT_FIM_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VALIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VALIDADE,1,4000),substr(:new.DT_INICIO_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VALIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_FIDELIDADE,1,4000),substr(:new.NR_CARTAO_FIDELIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_FIDELIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EMISSAO_CARTAO,1,4000),substr(:new.DT_EMISSAO_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CARTAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE_LOG',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PF_CARTAO_FIDELIDADE_LOG ADD (
  CONSTRAINT PFCAFIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PF_CARTAO_FIDELIDADE_LOG TO NIVEL_1;

