ALTER TABLE TASY.PA_STATUS_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PA_STATUS_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PA_STATUS_PACIENTE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_STATUS                VARCHAR2(80 BYTE)    NOT NULL,
  IE_REAVALIACAO_MEDICA    VARCHAR2(1 BYTE),
  IE_LIBERACAO_ENFERMAGEM  VARCHAR2(1 BYTE),
  IE_ESPECIALIDADE         VARCHAR2(1 BYTE),
  IE_DESFECHO              VARCHAR2(1 BYTE),
  IE_TROCAR_CLINICA        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PASTAPA_ESTABEL_FK_I ON TASY.PA_STATUS_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PASTAPA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PASTAPA_PK ON TASY.PA_STATUS_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PA_STATUS_PACIENTE ADD (
  CONSTRAINT PASTAPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PA_STATUS_PACIENTE ADD (
  CONSTRAINT PASTAPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PA_STATUS_PACIENTE TO NIVEL_1;

