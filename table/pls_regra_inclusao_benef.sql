ALTER TABLE TASY.PLS_REGRA_INCLUSAO_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_INCLUSAO_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_INCLUSAO_BENEF
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CONTRATO          NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_LIMITE_MOVIMENTACAO   NUMBER(2),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  DT_DIA_INCLUSAO          NUMBER(2),
  IE_TIPO_DATA             VARCHAR2(2 BYTE),
  IE_ACAO_CONTRATO         VARCHAR2(2 BYTE),
  IE_ESTIPULANTE_CONTRATO  VARCHAR2(2 BYTE),
  IE_DATA_ACAO_REGRA       VARCHAR2(2 BYTE),
  IE_TIPO_CONTRATACAO      VARCHAR2(2 BYTE),
  IE_APLICACAO_REGRA       VARCHAR2(2 BYTE),
  IE_MANTER_DATA_INCLUSAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREIB_ESTABEL_FK_I ON TASY.PLS_REGRA_INCLUSAO_BENEF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREIB_PK ON TASY.PLS_REGRA_INCLUSAO_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIB_PLSCONT_FK_I ON TASY.PLS_REGRA_INCLUSAO_BENEF
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_INCLUSAO_BENEF ADD (
  CONSTRAINT PLSREIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_INCLUSAO_BENEF ADD (
  CONSTRAINT PLSREIB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREIB_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_INCLUSAO_BENEF TO NIVEL_1;

