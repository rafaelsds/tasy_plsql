ALTER TABLE TASY.W_PLS_RESUMO_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_RESUMO_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_RESUMO_ATENDIMENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_ITEM             NUMBER(10),
  DS_ITEM                 VARCHAR2(255 BYTE),
  QT_TOTAL_ATENDIMENTO    NUMBER(10),
  QT_MEDIA_ATENDIMENTO    NUMBER(10,2),
  QT_MINUTOS_ATENDIMENTO  NUMBER(10,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSRAT_PK ON TASY.W_PLS_RESUMO_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_RESUMO_ATENDIMENTO ADD (
  CONSTRAINT WPLSRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_RESUMO_ATENDIMENTO TO NIVEL_1;

