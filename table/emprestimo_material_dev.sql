ALTER TABLE TASY.EMPRESTIMO_MATERIAL_DEV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMPRESTIMO_MATERIAL_DEV CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMPRESTIMO_MATERIAL_DEV
(
  NR_EMPRESTIMO     NUMBER(10)                  NOT NULL,
  NR_SEQUENCIA      NUMBER(5)                   NOT NULL,
  NR_SEQ_DEV        NUMBER(5),
  CD_MATERIAL       NUMBER(6)                   NOT NULL,
  QT_MATERIAL       NUMBER(13,4)                NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  DS_OBSERVACAO     VARCHAR2(255 BYTE),
  DS_LOTE_FORNEC    VARCHAR2(20 BYTE),
  DT_VALIDADE       DATE,
  NR_SEQ_LOTE       NUMBER(10),
  NR_SEQ_NOTA       NUMBER(10),
  NR_SEQ_ITEM_NOTA  NUMBER(5),
  IE_INDETERMINADO  VARCHAR2(1 BYTE),
  NR_SEQ_MARCA      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMPMADE_EMPMATE_FK_I ON TASY.EMPRESTIMO_MATERIAL_DEV
(NR_EMPRESTIMO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMADE_MARCA_FK_I ON TASY.EMPRESTIMO_MATERIAL_DEV
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMADE_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPMADE_MATERIA_FK_I ON TASY.EMPRESTIMO_MATERIAL_DEV
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMADE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPMADE_MATLOFO_FK_I ON TASY.EMPRESTIMO_MATERIAL_DEV
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMADE_MATLOFO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPMADE_NOTFIIT_FK_I ON TASY.EMPRESTIMO_MATERIAL_DEV
(NR_SEQ_NOTA, NR_SEQ_ITEM_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMADE_NOTFIIT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EMPMADE_PK ON TASY.EMPRESTIMO_MATERIAL_DEV
(NR_EMPRESTIMO, NR_SEQUENCIA, NR_SEQ_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.emprestimo_material_dev_atual
before insert or update ON TASY.EMPRESTIMO_MATERIAL_DEV for each row
declare
cd_local_estoque_w	number(4);
cd_estabelecimento_w	number(4);
cd_material_w		number(6);
ie_consiste_inv_w	varchar2(1);
ie_bloqueado_inventario_w	varchar2(1);
ie_estoque_lote_w	varchar2(1);
ie_tipo_w		emprestimo.ie_tipo%type;

begin
select	cd_estabelecimento,
	cd_local_estoque,
	ie_tipo
into	cd_estabelecimento_w,
	cd_local_estoque_w,
	ie_tipo_w
from	emprestimo
where	nr_emprestimo = :new.nr_emprestimo;

Obter_Param_Usuario(143,232,obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_consiste_inv_w);
ie_estoque_lote_w := substr(obter_se_material_estoque_lote(cd_estabelecimento_w, :new.cd_material),1,1);

if	(ie_estoque_lote_w = 'S') and
	(:new.nr_seq_lote is null) and
	(ie_tipo_w = 'E') then
	wheb_mensagem_pck.exibir_mensagem_abort(205365,'CD_MATERIAL=' || :new.cd_material);
end if;

if	(ie_consiste_inv_w = 'S') then
	begin
	select	cd_material
	into	cd_material_w
	from	emprestimo_material
	where	nr_emprestimo = :new.nr_emprestimo
	and	nr_sequencia = :new.nr_sequencia;

	ie_bloqueado_inventario_w := substr(Obter_Se_Material_Bloqueio_Inv(cd_estabelecimento_w, cd_material_w, cd_local_estoque_w),1,1);

	if	(ie_bloqueado_inventario_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(205366,'CD_MATERIAL=' || :new.cd_material);
	end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Emprestimo_Material_dev_delete
before delete ON TASY.EMPRESTIMO_MATERIAL_DEV for each row
declare

ie_erro_w			varchar2(255);
ie_tipo_w			varchar2(1);
ie_acao_w		varchar2(1);
cd_estabelecimento_w	Number(05,0);
ie_estoque_lote_w		varchar2(1);
dt_mesano_referencia_w	Date;
cd_local_estoque_w	Number(05,0);
qt_saldo_atual_w		Number(13,4);
nm_usuario_w		varchar2(15) := wheb_usuario_pck.get_nm_usuario;
cd_material_w		number(6);
ie_consiste_inv_w		varchar2(1);
ie_bloqueado_inventario_w	varchar2(1);
cd_material_estoque_w	number(6);

BEGIN
dt_mesano_referencia_w		:= trunc(sysdate, 'mm');

select	cd_estabelecimento,
	ie_tipo,
	cd_local_estoque
into	cd_estabelecimento_w,
	ie_tipo_w,
	cd_local_estoque_w
from	emprestimo
where	nr_emprestimo		= :old.nr_emprestimo;

Obter_Param_Usuario(143,232,obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_consiste_inv_w);

if	(ie_consiste_inv_w = 'S') then
	begin
	select	cd_material
	into	cd_material_w
	from	emprestimo_material
	where	nr_emprestimo = :old.nr_emprestimo
	and	nr_sequencia = :old.nr_sequencia;

	ie_bloqueado_inventario_w := substr(Obter_Se_Material_Bloqueio_Inv(cd_estabelecimento_w, cd_material_w, cd_local_estoque_w),1,1);

	if	(ie_bloqueado_inventario_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(265973,'CD_MATERIAL=' || cd_material_w);
		--'O material ['|| cd_material_w || '] esta bloqueado para inventario!' || chr(13) || chr(10) ||
		--'Este emprestimo nao pode ser alterado neste momento.');
	end if;
	end;
end if;

update	emprestimo
set	ie_situacao = 'A'
where	nr_emprestimo = :old.nr_emprestimo;

select	substr(obter_se_material_estoque_lote(cd_estabelecimento_w, :old.cd_material),1,1)
into	ie_estoque_lote_w
from	dual;

if	(ie_estoque_lote_w = 'S') and
	(:old.dt_validade is not null) and
	(:old.ds_lote_fornec is not null) and
	(:old.nr_seq_lote is not null) then
	begin
	select	cd_local_estoque
	into	cd_local_estoque_w
	from	emprestimo
	where	nr_emprestimo		= :old.nr_emprestimo;

	ie_acao_w	:= '1';
	if	(ie_tipo_w = 'S') then
		ie_acao_w	:= '2';
	end if;

	select	cd_material_estoque
	into	cd_material_estoque_w
	from	material
	where	cd_material = :old.cd_material;

	/*Consistir se existe saldo na devolucao do emprestimo de saida. Lote pode ja ter sido consumido*/
	if	(ie_acao_w = '2') then
		select	nvl(sum(qt_estoque),0)
		into	qt_saldo_atual_w
		from	saldo_estoque_lote
		where	cd_estabelecimento		= cd_estabelecimento_w
		and	dt_mesano_referencia	= dt_mesano_referencia_w
		and	cd_local_estoque		= cd_local_estoque_w
		and	cd_material		= cd_material_estoque_w
		and	nr_seq_lote		= :old.nr_seq_lote;
		if	(:old.qt_material > qt_saldo_atual_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(265974,'DS_LOTE_FORNEC=' || :old.ds_lote_fornec || ';QT_SALDO_ATUAL=' || qt_saldo_atual_w);
			--'Esta baixa nao pode ser excluida, pois nao existe estoque suficiente.' || chr(13) || chr(10) ||
			--'Lote = ' || :old.ds_lote_fornec || '  Saldo = ' || qt_saldo_atual_w
		end if;
	end if;

	atualizar_saldo_lote(
			cd_estabelecimento_w,
			cd_local_estoque_w,
			:old.cd_material,
			dt_mesano_referencia_w,
			:old.nr_seq_lote,
			0,
			:old.qt_material,
			ie_acao_w,
			:old.nm_usuario,
			ie_erro_w);
	end;
end if;

if (:old.nr_seq_nota is not null) then
    begin
        update emprestimo_material
        set qt_material =  qt_material + :old.qt_material
        where nr_emprestimo = :old.nr_emprestimo
        and cd_material = :old.cd_material
        and nr_sequencia = :old.nr_seq_item_nota;
    end;
end if;

END;
/


ALTER TABLE TASY.EMPRESTIMO_MATERIAL_DEV ADD (
  CONSTRAINT EMPMADE_PK
 PRIMARY KEY
 (NR_EMPRESTIMO, NR_SEQUENCIA, NR_SEQ_DEV)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMPRESTIMO_MATERIAL_DEV ADD (
  CONSTRAINT EMPMADE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT EMPMADE_EMPMATE_FK 
 FOREIGN KEY (NR_EMPRESTIMO, NR_SEQUENCIA) 
 REFERENCES TASY.EMPRESTIMO_MATERIAL (NR_EMPRESTIMO,NR_SEQUENCIA),
  CONSTRAINT EMPMADE_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT EMPMADE_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQ_NOTA, NR_SEQ_ITEM_NOTA) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF)
    ON DELETE CASCADE,
  CONSTRAINT EMPMADE_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMPRESTIMO_MATERIAL_DEV TO NIVEL_1;

