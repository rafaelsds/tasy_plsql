ALTER TABLE TASY.SUP_LOG_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_LOG_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_LOG_INTEGRACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_DOCUMENTO         NUMBER(15)               NOT NULL,
  NR_DOCTO_EXTERNO     VARCHAR2(100 BYTE),
  IE_ACAO              VARCHAR2(15 BYTE)        NOT NULL,
  IE_STATUS            VARCHAR2(15 BYTE)        NOT NULL,
  DT_DOCUMENTO         DATE,
  DT_PROCESSO          DATE,
  DT_ENVIO_EMAIL       DATE,
  DS_CONSISTENCIA      VARCHAR2(4000 BYTE),
  IE_EVENTO            VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO              VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_DOC          VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SPLGINT_PK ON TASY.SUP_LOG_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.sup_log_integracao_insert
before insert ON TASY.SUP_LOG_INTEGRACAO for each row
declare

ie_envio_w 	varchar2(1) := 'N';

begin
if	(nvl(:new.ds_consistencia,'X') <> 'X') then
	begin
	sup_enviar_email_int(	:new.ie_evento,
				'ME',
				:new.nr_documento,
				wheb_usuario_pck.get_cd_estabelecimento,
				:new.nm_usuario,
				ie_envio_w,
				:new.ie_tipo_doc,
				:new.ie_acao,
				:new.ds_consistencia);

	if	(ie_envio_w = 'S') then
		begin
		:new.dt_envio_email := sysdate;
		end;
	end if;
	end;
end if;

end;
/


ALTER TABLE TASY.SUP_LOG_INTEGRACAO ADD (
  CONSTRAINT SPLGINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_LOG_INTEGRACAO TO NIVEL_1;

