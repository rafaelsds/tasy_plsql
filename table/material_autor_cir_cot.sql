ALTER TABLE TASY.MATERIAL_AUTOR_CIR_COT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_AUTOR_CIR_COT CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_AUTOR_CIR_COT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_CGC                 VARCHAR2(14 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  VL_COTADO              NUMBER(15,2)           NOT NULL,
  CD_CONDICAO_PAGAMENTO  NUMBER(10),
  VL_UNITARIO_COTADO     NUMBER(17,4)           NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_APROVACAO           VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_MARCA           NUMBER(10),
  NR_ORCAMENTO           VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATAUCC_CONPAGA_FK_I ON TASY.MATERIAL_AUTOR_CIR_COT
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCC_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUCC_MATAUCI_FK_I ON TASY.MATERIAL_AUTOR_CIR_COT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUCC_MATMARC_FK_I ON TASY.MATERIAL_AUTOR_CIR_COT
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCC_MATMARC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUCC_PESJURI_FK_I ON TASY.MATERIAL_AUTOR_CIR_COT
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATAUCC_PK ON TASY.MATERIAL_AUTOR_CIR_COT
(NR_SEQUENCIA, CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUCC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_AUTOR_CIR_COT_tp
after update ON TASY.MATERIAL_AUTOR_CIR_COT FOR EACH ROW
DECLARE

nr_seq_w number(10);
ds_s_w   varchar2(50);
ds_c_w   varchar2(500);
ds_w	   varchar2(500);
ie_log_w varchar2(1);

begin

begin

ds_s_w := to_char(:old.NR_SEQUENCIA);
ds_c_w := null;

ds_w:=substr(:new.CD_CGC,1,500);
gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.IE_APROVACAO,1,500);
gravar_log_alteracao(substr(:old.IE_APROVACAO,1,4000),substr(:new.IE_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APROVACAO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.NR_SEQ_MARCA,1,500);
gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.NR_ORCAMENTO,1,500);
gravar_log_alteracao(substr(:old.NR_ORCAMENTO,1,4000),substr(:new.NR_ORCAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORCAMENTO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.VL_COTADO,1,500);
gravar_log_alteracao(substr(:old.VL_COTADO,1,4000),substr(:new.VL_COTADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_COTADO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.CD_CONDICAO_PAGAMENTO,1,500);
gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.DT_ATUALIZACAO,1,500);
gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.NM_USUARIO,1,500);
gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

ds_w:=substr(:new.VL_UNITARIO_COTADO,1,500);
gravar_log_alteracao(substr(:old.VL_UNITARIO_COTADO,1,4000),substr(:new.VL_UNITARIO_COTADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNITARIO_COTADO',ie_log_w,ds_w,'MATERIAL_AUTOR_CIR_COT',ds_s_w,ds_c_w);

exception when others then
ds_w:= '1';
end;
end;
/


ALTER TABLE TASY.MATERIAL_AUTOR_CIR_COT ADD (
  CONSTRAINT MATAUCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA, CD_CGC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_AUTOR_CIR_COT ADD (
  CONSTRAINT MATAUCC_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATAUCC_MATAUCI_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.MATERIAL_AUTOR_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATAUCC_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO));

GRANT SELECT ON TASY.MATERIAL_AUTOR_CIR_COT TO NIVEL_1;

