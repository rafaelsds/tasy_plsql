ALTER TABLE TASY.QUA_TIPO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_TIPO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_TIPO_EVENTO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_EMPRESA                  NUMBER(5)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DS_TIPO_EVENTO              VARCHAR2(80 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_TIPO_EVENTO              VARCHAR2(5 BYTE),
  IE_RESTRINGIR_VISUALIZACAO  VARCHAR2(1 BYTE),
  CD_ESTAB_EXCLUSIVO          NUMBER(4),
  CD_EXP_TIPO_EVENTO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUATIEV_DICEXPR_FK_I ON TASY.QUA_TIPO_EVENTO
(CD_EXP_TIPO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUATIEV_ESTABEL_FK_I ON TASY.QUA_TIPO_EVENTO
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATIEV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUATIEV_PK ON TASY.QUA_TIPO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.qua_tipo_evento_after
after insert or update ON TASY.QUA_TIPO_EVENTO for each row
declare

qt_reg_w		number(1);
qt_existe_w	number(10,0);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if	(:new.ie_situacao = 'I') then
	begin
	select	count(*)
	into	qt_existe_w
	from	qua_evento
	where	nr_seq_tipo = :new.nr_sequencia;

	if	(qt_existe_w > 0) then
		update	qua_evento
		set	ie_situacao = :new.ie_situacao
		where	nr_seq_tipo = :new.nr_sequencia;
	end if;
	end;
end if;

<<Final>>
qt_reg_w := 0;

end;
/


ALTER TABLE TASY.QUA_TIPO_EVENTO ADD (
  CONSTRAINT QUATIEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_TIPO_EVENTO ADD (
  CONSTRAINT QUATIEV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUATIEV_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_TIPO_EVENTO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.QUA_TIPO_EVENTO TO NIVEL_1;

