ALTER TABLE TASY.EXAME_LOG_ALTERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LOG_ALTERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LOG_ALTERACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EXAME         NUMBER(10),
  NM_TABELA            VARCHAR2(30 BYTE),
  DS_VALOR_ANT         VARCHAR2(4000 BYTE),
  NM_ATRIBUTO          VARCHAR2(90 BYTE),
  DS_VALOR_NOVO        VARCHAR2(4000 BYTE),
  CD_FUNCAO            NUMBER(10),
  CD_PERFIL            NUMBER(10),
  DT_LIBERACAO         DATE,
  NM_LIBERACAO         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXALOAL_ESTABEL_FK_I ON TASY.EXAME_LOG_ALTERACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALOAL_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EXALOAL_PK ON TASY.EXAME_LOG_ALTERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALOAL_PK
  MONITORING USAGE;


ALTER TABLE TASY.EXAME_LOG_ALTERACAO ADD (
  CONSTRAINT EXALOAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXAME_LOG_ALTERACAO ADD (
  CONSTRAINT EXALOAL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.EXAME_LOG_ALTERACAO TO NIVEL_1;

