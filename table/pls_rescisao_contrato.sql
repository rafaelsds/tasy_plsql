ALTER TABLE TASY.PLS_RESCISAO_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RESCISAO_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RESCISAO_CONTRATO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_SEGURADO         NUMBER(10),
  DT_RESCISAO             DATE                  NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_CONTRATO         NUMBER(10),
  IE_TIPO_SOLICITANTE     VARCHAR2(2 BYTE),
  DT_SOLICITACAO          DATE                  NOT NULL,
  NR_SEQ_MOTIVO_RESCISAO  NUMBER(10),
  NM_USUARIO_SOLICITACAO  VARCHAR2(60 BYTE),
  IE_PROCESSO             VARCHAR2(1 BYTE),
  NR_SEQ_PAGADOR          NUMBER(10),
  NR_SEQ_INTERCAMBIO      NUMBER(10),
  DT_LIMITE_UTILIZACAO    DATE,
  IE_DEVOLUCAO_CARTEIRA   VARCHAR2(1 BYTE),
  DT_OBITO                DATE,
  NR_CERTIDAO_OBITO       VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_REJEICAO  NUMBER(10),
  DS_OBSERVACAO_REJEICAO  VARCHAR2(4000 BYTE),
  QT_MESES_CONTRIBUICAO   NUMBER(6),
  DT_CONCESSAO            DATE,
  DT_LIBERACAO            DATE,
  IE_STATUS_LIB           VARCHAR2(1 BYTE),
  IE_EXIGE_LIBERACAO_WEB  VARCHAR2(1 BYTE),
  NR_SEQ_CAUSA_RESCISAO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRECO_PK ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECO_PLCONPAG_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECO_PLSCONT_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECO_PLSINCA_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECO_PLSMCRS_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_MOTIVO_REJEICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRECO_PLSMCRS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRECO_PLSMOCA_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_MOTIVO_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECO_PLSSEGU_FK_I ON TASY.PLS_RESCISAO_CONTRATO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ssj_gerar_dados_resc_prog
before insert
ON TASY.PLS_RESCISAO_CONTRATO 
for each row
declare

dt_solicitacao_w date;
ie_tipo_solicitacao_w char(1);

begin

  dt_solicitacao_w := :new.dt_solicitacao;
  ie_tipo_solicitacao_w := :new.ie_tipo_solicitante;
        
   if(:new.nr_seq_motivo_rescisao = 27)then
   
        :new.dt_rescisao := ssj_obter_dados_rescisao_prog(:new.nr_seq_segurado,'DTR');
        :new.DT_LIMITE_UTILIZACAO := ssj_obter_dados_rescisao_prog(:new.nr_seq_segurado,'DTL');
        :new.QT_MESES_CONTRIBUICAO := ssj_obter_dados_rescisao_prog(:new.nr_seq_segurado,'TC');
        :new.ds_observacao := ssj_obter_dados_rescisao_prog(:new.nr_seq_segurado,'P');
        
   end if;
   
   if(:new.nr_seq_motivo_rescisao = 28)then
   
        if(to_char(dt_solicitacao_w,'dd') <= 21)then
        
            :new.dt_rescisao := to_date(sysdate);
            :new.DT_LIMITE_UTILIZACAO := last_day(trunc(sysdate,'month'));
        
        else
        
            :new.dt_rescisao := trunc(add_months(sysdate,1),'month');
            :new.DT_LIMITE_UTILIZACAO := last_day(trunc(add_months(sysdate,1),'month'));
            
        end if;
        
   end if;
   
   if(:new.nr_seq_motivo_rescisao = 5 and ie_tipo_solicitacao_w = 'W')then
   
        :new.dt_rescisao := to_date(sysdate);
    
   end if;

end ssj_gerar_dados_resc_prog;
/


ALTER TABLE TASY.PLS_RESCISAO_CONTRATO ADD (
  CONSTRAINT PLSRECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RESCISAO_CONTRATO ADD (
  CONSTRAINT PLSRECO_PLSMCRS_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REJEICAO) 
 REFERENCES TASY.PLS_MOTIVO_CANCEL_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSRECO_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSRECO_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSRECO_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RESCISAO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSRECO_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRECO_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RESCISAO_CONTRATO TO NIVEL_1;

