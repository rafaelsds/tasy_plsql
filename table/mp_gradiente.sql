ALTER TABLE TASY.MP_GRADIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MP_GRADIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MP_GRADIENTE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_OBJETO_FONTE   NUMBER(10),
  NR_SEQ_OBJETO_FUNDO   NUMBER(10),
  IE_BORDA              VARCHAR2(1 BYTE)        NOT NULL,
  QT_ANGULO             NUMBER(10)              NOT NULL,
  QT_BALANCO            NUMBER(10)              NOT NULL,
  IE_DIRECAO            VARCHAR2(3 BYTE)        NOT NULL,
  DS_COR_INICIO         VARCHAR2(15 BYTE),
  DS_COR_MEIO           VARCHAR2(15 BYTE),
  DS_COR_FIM            VARCHAR2(15 BYTE),
  QT_RADIANOS_X         NUMBER(10)              NOT NULL,
  QT_RADIANOS_Y         NUMBER(10)              NOT NULL,
  IE_VISIVEL            VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_SUB_GRADIENTE  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPGRADCOR_MPGRADCOR_FK_I ON TASY.MP_GRADIENTE
(NR_SEQ_SUB_GRADIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPGRADCOR_MPGRADCOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPGRADCOR_MPPROOB_FK_I ON TASY.MP_GRADIENTE
(NR_SEQ_OBJETO_FONTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPGRADCOR_MPPROOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPGRADCOR_MPPROOB_FK1_I ON TASY.MP_GRADIENTE
(NR_SEQ_OBJETO_FUNDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPGRADCOR_MPPROOB_FK1_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPGRADCOR_PK ON TASY.MP_GRADIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPGRADCOR_PK
  MONITORING USAGE;


ALTER TABLE TASY.MP_GRADIENTE ADD (
  CONSTRAINT MPGRADCOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MP_GRADIENTE ADD (
  CONSTRAINT MPGRADCOR_MPPROOB_FK 
 FOREIGN KEY (NR_SEQ_OBJETO_FONTE) 
 REFERENCES TASY.MP_PROCESSO_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPGRADCOR_MPPROOB_FK1 
 FOREIGN KEY (NR_SEQ_OBJETO_FUNDO) 
 REFERENCES TASY.MP_PROCESSO_OBJETO (NR_SEQUENCIA),
  CONSTRAINT MPGRADCOR_MPGRADCOR_FK 
 FOREIGN KEY (NR_SEQ_SUB_GRADIENTE) 
 REFERENCES TASY.MP_GRADIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MP_GRADIENTE TO NIVEL_1;

