ALTER TABLE TASY.W_ENVIO_DIRF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ENVIO_DIRF CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ENVIO_DIRF
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_LOTE                 NUMBER(10),
  NR_SEQ_REGISTRO             VARCHAR2(8 BYTE),
  IE_TIPO_REGISTRO            NUMBER(10),
  NR_CPF_CNPJ                 VARCHAR2(14 BYTE),
  NM_ARQUIVO                  VARCHAR2(4 BYTE),
  DT_ANO                      VARCHAR2(4 BYTE),
  IE_TIPO_DECLARACAO          VARCHAR2(1 BYTE),
  IE_SITUACAO_DECLARACAO      VARCHAR2(3 BYTE),
  IE_TIPO_DECLARANTE          VARCHAR2(1 BYTE),
  IE_NATUREZA_DECLARANTE      VARCHAR2(3 BYTE),
  IE_TIPO_RENDIMENTO          VARCHAR2(3 BYTE),
  IE_INDICADOR_DEPOSITARIO    VARCHAR2(3 BYTE),
  NM_DECLARANTE               VARCHAR2(255 BYTE),
  CD_CPF                      VARCHAR2(11 BYTE),
  DT_EVENTO                   DATE,
  NR_RECIBO                   VARCHAR2(255 BYTE),
  CD_CPF_RESPONSAVEL          VARCHAR2(11 BYTE),
  NM_RESPONSAVEL              VARCHAR2(60 BYTE),
  NR_DDD_RESP                 VARCHAR2(3 BYTE),
  NR_TELEFONE_RESP            VARCHAR2(50 BYTE),
  NR_RAMAL_RESP               VARCHAR2(5 BYTE),
  NR_FAX_RESP                 VARCHAR2(50 BYTE),
  DS_EMAIL_RESP               VARCHAR2(255 BYTE),
  IE_USO_DECLARANTE           NUMBER(2),
  CD_RETENCAO                 VARCHAR2(10 BYTE),
  IE_TIPO_BENEFICIARIO        VARCHAR2(1 BYTE),
  NR_BENEFICIARIO             VARCHAR2(14 BYTE),
  NM_BENEFICIARIO             VARCHAR2(255 BYTE),
  VL_REND_JAN                 NUMBER(15,2),
  VL_DEDUCOES_JAN             NUMBER(15,2),
  VL_IR_JAN                   NUMBER(15,2),
  VL_REND_FEV                 NUMBER(15,2),
  VL_IR_FEV                   NUMBER(15,2),
  VL_DEDUCOES_FEV             NUMBER(15,2),
  VL_REND_MAR                 NUMBER(15,2),
  VL_DEDUCOES_MAR             NUMBER(15,2),
  VL_IR_MAR                   NUMBER(15,2),
  VL_REND_ABR                 NUMBER(15,2),
  VL_DEDUCOES_ABR             NUMBER(15,2),
  VL_IR_ABR                   NUMBER(15,2),
  VL_REND_MAI                 NUMBER(15,2),
  VL_DEDUCOES_MAI             NUMBER(15,2),
  VL_IR_MAI                   NUMBER(15,2),
  VL_REND_JUN                 NUMBER(15,2),
  VL_DEDUCOES_JUN             NUMBER(15,2),
  VL_IR_JUN                   NUMBER(15,2),
  VL_REND_JUL                 NUMBER(15,2),
  VL_DEDUCOES_JUL             NUMBER(15,2),
  VL_IR_JUL                   NUMBER(15,2),
  VL_REND_AGO                 NUMBER(15,2),
  VL_DEDUCOES_AGO             NUMBER(15,2),
  VL_IR_AGO                   NUMBER(15,2),
  VL_REND_SET                 NUMBER(15,2),
  VL_DEDUCOES_SET             NUMBER(15,2),
  VL_IR_SET                   NUMBER(15,2),
  VL_REND_OUT                 NUMBER(15,2),
  VL_DEDUCOES_OUT             NUMBER(15,2),
  VL_IR_OUT                   NUMBER(15,2),
  VL_REND_NOV                 NUMBER(15,2),
  VL_DEDUCOES_NOV             NUMBER(15,2),
  VL_IR_NOV                   NUMBER(15,2),
  VL_REND_DEZ                 NUMBER(15,2),
  VL_DEDUCOES_DEZ             NUMBER(15,2),
  VL_IR_DEZ                   NUMBER(15,2),
  VL_REND_13_SALARIO          NUMBER(15,2),
  VL_DEDUCOES_13_SALARIO      NUMBER(15,2),
  VL_IR_13_SALARIO            NUMBER(15,2),
  DS_SRF                      VARCHAR2(2 BYTE),
  IE_SITUACAO                 NUMBER(2),
  VL_TOTAL_REGISTROS          NUMBER(10),
  VL_TOT_REND_JAN             NUMBER(15,2),
  VL_TOT_DEDUCOES_JAN         NUMBER(15,2),
  VL_TOT_IR_JAN               NUMBER(15,2),
  VL_TOT_REND_FEV             NUMBER(15,2),
  VL_TOT_DEDUCOES_FEV         NUMBER(15,2),
  VL_TOT_IR_FEV               NUMBER(15,2),
  VL_TOT_REND_MAR             NUMBER(15,2),
  VL_TOT_DEDUCOES_MAR         NUMBER(15,2),
  VL_TOT_IR_MAR               NUMBER(15,2),
  VL_TOT_REND_ABR             NUMBER(15,2),
  VL_TOT_DEDUCOES_ABR         NUMBER(15,2),
  VL_TOT_IR_ABR               NUMBER(15,2),
  VL_TOT_REND_MAI             NUMBER(15,2),
  VL_TOT_DEDUCOES_MAI         NUMBER(15,2),
  VL_TOT_IR_MAI               NUMBER(15,2),
  VL_TOT_REND_JUN             NUMBER(15,2),
  VL_TOT_DEDUCOES_JUN         NUMBER(15,2),
  VL_TOT_IR_JUN               NUMBER(15,2),
  VL_TOT_REND_JUL             NUMBER(15,2),
  VL_TOT_DEDUCOES_JUL         NUMBER(15,2),
  VL_TOT_IR_JUL               NUMBER(15,2),
  VL_TOT_REND_AGO             NUMBER(15,2),
  VL_TOT_DEDUCOES_AGO         NUMBER(15,2),
  VL_TOT_IR_AGO               NUMBER(15,2),
  VL_TOT_REND_SET             NUMBER(15,2),
  VL_TOT_DEDUCOES_SET         NUMBER(15,2),
  VL_TOT_IR_SET               NUMBER(15,2),
  VL_TOT_REND_OUT             NUMBER(15,2),
  VL_TOT_DEDUCOES_OUT         NUMBER(15,2),
  VL_TOT_IR_OUT               NUMBER(15,2),
  VL_TOT_REND_NOV             NUMBER(15,2),
  VL_TOT_DEDUCOES_NOV         NUMBER(15,2),
  VL_TOT_IR_NOV               NUMBER(15,2),
  VL_TOT_REND_DEZ             NUMBER(15,2),
  VL_TOT_DEDUCOES_DEZ         NUMBER(15,2),
  VL_TOT_IR_DEZ               NUMBER(15,2),
  VL_TOT_REND_13_SALARIO      NUMBER(15,2),
  VL_TOT_DEDUCOES_13_SALARIO  NUMBER(15,2),
  VL_TOT_IR_13_SALARIO        NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WENDIRF_PK ON TASY.W_ENVIO_DIRF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WENDIRF_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_ENVIO_DIRF ADD (
  CONSTRAINT WENDIRF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ENVIO_DIRF TO NIVEL_1;

