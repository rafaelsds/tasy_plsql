ALTER TABLE TASY.PLS_REGRA_COPARTIC_APROP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_APROP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_APROP
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_REGRA               NUMBER(10)         NOT NULL,
  NR_SEQ_CENTRO_APROPRIACAO  NUMBER(10)         NOT NULL,
  IE_TIPO_APROPRIACAO        VARCHAR2(2 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  VL_APROPRIACAO             NUMBER(15,2),
  TX_APROPRIACAO             NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRACP_PK ON TASY.PLS_REGRA_COPARTIC_APROP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRACP_PLSCEAP_FK_I ON TASY.PLS_REGRA_COPARTIC_APROP
(NR_SEQ_CENTRO_APROPRIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRACP_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_APROP
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copart_aprop_update
before update ON TASY.PLS_REGRA_COPARTIC_APROP for each row
declare

begin
if	(:new.nr_seq_centro_apropriacao <> :old.nr_seq_centro_apropriacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_centro_apropriacao, :new.nr_seq_centro_apropriacao, 'PLS_REGRA_COPARTIC_APROP', 'NR_SEQ_CENTRO_APROPRIACAO', :new.nm_usuario);
end if;
if	(:new.ie_tipo_apropriacao <> :old.ie_tipo_apropriacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_apropriacao, :new.ie_tipo_apropriacao, 'PLS_REGRA_COPARTIC_APROP', 'IE_TIPO_APROPRIACAO', :new.nm_usuario);
end if;
if	(:new.vl_apropriacao <> :old.vl_apropriacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.vl_apropriacao, :new.vl_apropriacao, 'PLS_REGRA_COPARTIC_APROP', 'VL_APROPRIACAO', :new.nm_usuario);
end if;
if	(:new.tx_apropriacao <> :old.tx_apropriacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.tx_apropriacao, :new.tx_apropriacao, 'PLS_REGRA_COPARTIC_APROP', 'TX_APROPRIACAO', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_APROP ADD (
  CONSTRAINT PLSRACP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_APROP ADD (
  CONSTRAINT PLSRACP_PLSCEAP_FK 
 FOREIGN KEY (NR_SEQ_CENTRO_APROPRIACAO) 
 REFERENCES TASY.PLS_CENTRO_APROPRIACAO (NR_SEQUENCIA),
  CONSTRAINT PLSRACP_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_APROP TO NIVEL_1;

