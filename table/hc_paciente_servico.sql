ALTER TABLE TASY.HC_PACIENTE_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_PACIENTE_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_PACIENTE_SERVICO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PAC_HOME_CARE  NUMBER(10)              NOT NULL,
  NR_SEQ_SERVICO        NUMBER(10)              NOT NULL,
  QT_TEMPO_MINUTO       NUMBER(5),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_FREQUENCIA     NUMBER(10),
  NR_SEQ_STATUS         NUMBER(10),
  DT_PREV_FINALIZACAO   DATE,
  DT_REVISAO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HCPASER_HCFRSER_FK_I ON TASY.HC_PACIENTE_SERVICO
(NR_SEQ_FREQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCPASER_HCFRSER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HCPASER_HCSERVI_FK_I ON TASY.HC_PACIENTE_SERVICO
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCPASER_HCSERVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HCPASER_HCSTTSER_FK_I ON TASY.HC_PACIENTE_SERVICO
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCPASER_PACHOCA_FK_I ON TASY.HC_PACIENTE_SERVICO
(NR_SEQ_PAC_HOME_CARE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCPASER_PACHOCA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HCPASER_PK ON TASY.HC_PACIENTE_SERVICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCPASER_PK
  MONITORING USAGE;


ALTER TABLE TASY.HC_PACIENTE_SERVICO ADD (
  CONSTRAINT HCPASER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HC_PACIENTE_SERVICO ADD (
  CONSTRAINT HCPASER_HCFRSER_FK 
 FOREIGN KEY (NR_SEQ_FREQUENCIA) 
 REFERENCES TASY.HC_FREQUENCIA_SERVICO (NR_SEQUENCIA),
  CONSTRAINT HCPASER_PACHOCA_FK 
 FOREIGN KEY (NR_SEQ_PAC_HOME_CARE) 
 REFERENCES TASY.PACIENTE_HOME_CARE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HCPASER_HCSERVI_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.HC_SERVICO (NR_SEQUENCIA),
  CONSTRAINT HCPASER_HCSTTSER_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.HC_STATUS_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HC_PACIENTE_SERVICO TO NIVEL_1;

