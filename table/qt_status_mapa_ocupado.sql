ALTER TABLE TASY.QT_STATUS_MAPA_OCUPADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_STATUS_MAPA_OCUPADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_STATUS_MAPA_OCUPADO
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_STATUS_QUIMIO     VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQUENCIA         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QTSTMPO_PK ON TASY.QT_STATUS_MAPA_OCUPADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTSTMPO_PK
  MONITORING USAGE;


ALTER TABLE TASY.QT_STATUS_MAPA_OCUPADO ADD (
  CONSTRAINT QTSTMPO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.QT_STATUS_MAPA_OCUPADO TO NIVEL_1;

