ALTER TABLE TASY.PTU_SERVICO_PRE_TOTAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_SERVICO_PRE_TOTAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_SERVICO_PRE_TOTAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_CONTAS              NUMBER(10),
  QT_ITENS_CONTAS        NUMBER(10),
  VL_TOTAL_PROCEDIMENTO  NUMBER(15,2),
  VL_TOTAL               NUMBER(15,2),
  VL_TOTAL_MATERIAIS     NUMBER(15,2),
  NR_SEQ_SERV_PRE_PAGTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUSETO_PK ON TASY.PTU_SERVICO_PRE_TOTAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUSETO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUSETO_PTUSEPP_FK_I ON TASY.PTU_SERVICO_PRE_TOTAL
(NR_SEQ_SERV_PRE_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUSETO_PTUSEPP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_SERVICO_PRE_TOTAL ADD (
  CONSTRAINT PTUSETO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_SERVICO_PRE_TOTAL ADD (
  CONSTRAINT PTUSETO_PTUSEPP_FK 
 FOREIGN KEY (NR_SEQ_SERV_PRE_PAGTO) 
 REFERENCES TASY.PTU_SERVICO_PRE_PAGTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_SERVICO_PRE_TOTAL TO NIVEL_1;

