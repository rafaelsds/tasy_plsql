ALTER TABLE TASY.FUNC_MOD_SOFTWARE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNC_MOD_SOFTWARE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNC_MOD_SOFTWARE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MODULO        NUMBER(10),
  CD_FUNCAO            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FMSOFT_APLSMOD_FK_I ON TASY.FUNC_MOD_SOFTWARE
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FMSOFT_FUNCAO_FK_I ON TASY.FUNC_MOD_SOFTWARE
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FMSOFT_PK ON TASY.FUNC_MOD_SOFTWARE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FMSOFT_PK
  MONITORING USAGE;


ALTER TABLE TASY.FUNC_MOD_SOFTWARE ADD (
  CONSTRAINT FMSOFT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNC_MOD_SOFTWARE ADD (
  CONSTRAINT FMSOFT_APLSMOD_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.APLICACAO_SOFT_MODULO (NR_SEQUENCIA),
  CONSTRAINT FMSOFT_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO));

GRANT SELECT ON TASY.FUNC_MOD_SOFTWARE TO NIVEL_1;

