ALTER TABLE TASY.TX_DOADOR_ORGAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_DOADOR_ORGAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_DOADOR_ORGAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DOADOR        NUMBER(10)               NOT NULL,
  NR_SEQ_ORGAO         NUMBER(10)               NOT NULL,
  DT_RETIRADA          DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXDOORG_PK ON TASY.TX_DOADOR_ORGAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXDOORG_TXDOAD_FK_I ON TASY.TX_DOADOR_ORGAO
(NR_SEQ_DOADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOORG_TXDOAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXDOORG_TXORGA_FK_I ON TASY.TX_DOADOR_ORGAO
(NR_SEQ_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOORG_TXORGA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_doador_orgao_befinsupd
before insert or update ON TASY.TX_DOADOR_ORGAO for each row
declare
ie_doador_vivo_w	varchar2(1);
ie_tipo_doador_w	varchar2(15);

begin

select	ie_doador_vivo
into	ie_doador_vivo_w
from	tx_orgao
where	nr_sequencia	= :new.nr_seq_orgao;

select	ie_tipo_doador
into	ie_tipo_doador_w
from	tx_doador
where	nr_sequencia	= :new.nr_seq_doador;

if	(ie_tipo_doador_w = 'VIV') and
	(ie_doador_vivo_w <> 'S') then
	--O �rg�o informado no cadastro do doador n�o pode ser doado em vida.
	Wheb_mensagem_pck.exibir_mensagem_abort(264228);
end if;

end;
/


ALTER TABLE TASY.TX_DOADOR_ORGAO ADD (
  CONSTRAINT TXDOORG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_DOADOR_ORGAO ADD (
  CONSTRAINT TXDOORG_TXORGA_FK 
 FOREIGN KEY (NR_SEQ_ORGAO) 
 REFERENCES TASY.TX_ORGAO (NR_SEQUENCIA),
  CONSTRAINT TXDOORG_TXDOAD_FK 
 FOREIGN KEY (NR_SEQ_DOADOR) 
 REFERENCES TASY.TX_DOADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_DOADOR_ORGAO TO NIVEL_1;

