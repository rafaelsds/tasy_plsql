ALTER TABLE TASY.PLS_LOG_POS_ESTABELECIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOG_POS_ESTABELECIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOG_POS_ESTABELECIDO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_CONTA_POS           NUMBER(10),
  NR_SEQ_CONTA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_STATUS_FATURAMENTO_ANT  VARCHAR2(2 BYTE),
  IE_STATUS_FATURAMENTO_NEW  VARCHAR2(2 BYTE),
  NR_LOTE_CONTABIL           NUMBER(10),
  DS_LOG                     VARCHAR2(4000 BYTE),
  IE_TIPO_REGISTRO           VARCHAR2(2 BYTE),
  NR_SEQ_CONTA_MAT_POS       NUMBER(10),
  NR_SEQ_CONTA_POS_PROC      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLPOS_LOTCONT_FK_I ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSLPOS_PK ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPOS_PLSCOME_FK_I ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPOS_PLSCOVB_FK_I ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_SEQ_CONTA_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPOS_PLSCPM_FK_I ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_SEQ_CONTA_MAT_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPOS_PLSPPROC_FK_I ON TASY.PLS_LOG_POS_ESTABELECIDO
(NR_SEQ_CONTA_POS_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_log_pos_estab_insert
before insert or update ON TASY.PLS_LOG_POS_ESTABELECIDO for each row
declare

ie_lote_gerado_w		varchar2(1);
ie_gerando_lote_contab_w	varchar2(1);
qt_registros_log_w		number(10);
dt_mes_competencia_w		date;

begin

select	max(a.dt_mes_competencia)
into	dt_mes_competencia_w
from	pls_protocolo_conta	a,
	pls_conta		b
where	a.nr_sequencia		= b.nr_seq_protocolo
and	b.nr_sequencia		= :new.nr_seq_conta;

ie_lote_gerado_w		:= obter_se_lote_contabil_gerado(43,dt_mes_competencia_w);
ie_gerando_lote_contab_w	:= wheb_usuario_pck.get_ie_lote_contabil;

if	(ie_lote_gerado_w = 'N') and
	(ie_gerando_lote_contab_w = 'N') then
	:new.dt_atualizacao_nrec	:= dt_mes_competencia_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_LOG_POS_ESTABELECIDO_tp  after update ON TASY.PLS_LOG_POS_ESTABELECIDO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_STATUS_FATURAMENTO_NEW,1,4000),substr(:new.IE_STATUS_FATURAMENTO_NEW,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_FATURAMENTO_NEW',ie_log_w,ds_w,'PLS_LOG_POS_ESTABELECIDO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_FATURAMENTO_ANT,1,4000),substr(:new.IE_STATUS_FATURAMENTO_ANT,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_FATURAMENTO_ANT',ie_log_w,ds_w,'PLS_LOG_POS_ESTABELECIDO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_LOG_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSLPOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOG_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSLPOS_PLSPPROC_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS_PROC) 
 REFERENCES TASY.PLS_CONTA_POS_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSLPOS_PLSCPM_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT_POS) 
 REFERENCES TASY.PLS_CONTA_POS_MAT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSLPOS_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSLPOS_PLSCOVB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS) 
 REFERENCES TASY.PLS_CONTA_POS_ESTABELECIDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSLPOS_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL));

GRANT SELECT ON TASY.PLS_LOG_POS_ESTABELECIDO TO NIVEL_1;

