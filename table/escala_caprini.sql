ALTER TABLE TASY.ESCALA_CAPRINI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CAPRINI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CAPRINI
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_RISCO                VARCHAR2(1 BYTE),
  DT_AVALIACAO            DATE                  NOT NULL,
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_LIBERACAO            DATE,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  QT_IDADE                NUMBER(3),
  NR_SEQ_MOTIVO_TEV       NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_CIRURGIA             VARCHAR2(1 BYTE),
  IE_PERNAS               VARCHAR2(1 BYTE),
  IE_VARIZES              VARCHAR2(1 BYTE),
  IE_BMI                  VARCHAR2(1 BYTE),
  IE_CIRUR_MES            VARCHAR2(1 BYTE),
  IE_INSUFICIENCIA_CARD   VARCHAR2(1 BYTE),
  IE_ATAQUE_CARDIO        VARCHAR2(1 BYTE),
  IE_DESCANSO             VARCHAR2(1 BYTE),
  IE_IBD                  VARCHAR2(1 BYTE),
  IE_DIALISE              VARCHAR2(1 BYTE),
  IE_SEPSIS               VARCHAR2(1 BYTE),
  IE_INFECCAO             VARCHAR2(1 BYTE),
  IE_HRT                  VARCHAR2(1 BYTE),
  IE_GRAVIDA              VARCHAR2(1 BYTE),
  IE_ABORTO               VARCHAR2(1 BYTE),
  IE_OUTROS_01            VARCHAR2(255 BYTE),
  IE_CANCER               VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE),
  IE_CAMA                 VARCHAR2(1 BYTE),
  IE_OUTROS_02            VARCHAR2(255 BYTE),
  IE_OUTROS_03            VARCHAR2(255 BYTE),
  IE_MOBILIDADE           VARCHAR2(1 BYTE),
  IE_OUTROS_04            VARCHAR2(255 BYTE),
  IE_TUBO                 VARCHAR2(1 BYTE),
  IE_DVT                  VARCHAR2(1 BYTE),
  IE_HIST_FAMILIA         VARCHAR2(1 BYTE),
  IE_LEIDEN               VARCHAR2(1 BYTE),
  IE_LESOES_MULTIPLAS     VARCHAR2(1 BYTE),
  IE_HIT                  VARCHAR2(1 BYTE),
  IE_HYPERHOMO            VARCHAR2(1 BYTE),
  IE_ANTI_CARD            VARCHAR2(1 BYTE),
  IE_AVC                  VARCHAR2(1 BYTE),
  IE_OUTRO_TROMB          VARCHAR2(255 BYTE),
  IE_FRATURA              VARCHAR2(1 BYTE),
  IE_PARALESIA            VARCHAR2(1 BYTE),
  QT_PONTUACAO            NUMBER(10),
  DS_RESULTADO            VARCHAR2(2000 BYTE),
  IE_NAO_SE_APLICA        VARCHAR2(1 BYTE),
  IE_MUTACAO_TROMBOFILIA  VARCHAR2(1 BYTE),
  IE_OUTRO_FATOR          VARCHAR2(1 BYTE),
  IE_MUTACAO_PROTROMBINA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCAPR_ATEPACI_FK_I ON TASY.ESCALA_CAPRINI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCAPR_PESFISI_FK_I ON TASY.ESCALA_CAPRINI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCAPR_PK ON TASY.ESCALA_CAPRINI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCAPR_TEVMOTI_FK_I ON TASY.ESCALA_CAPRINI
(NR_SEQ_MOTIVO_TEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_caprini_Atual
before
insert or update ON TASY.ESCALA_CAPRINI for each row
declare
qt_risco_w		number(10)	:= 0;

begin

if	(:new.QT_IDADE >= 41 and :new.QT_IDADE <= 60) then
	qt_risco_w	:= qt_risco_w + 1;
elsif (:new.QT_IDADE >= 61 and :new.QT_IDADE <= 74 ) then
	qt_risco_w	:= qt_risco_w + 2;
elsif (:new.QT_IDADE >= 75 ) then
	qt_risco_w	:= qt_risco_w + 3;
end if;

qt_risco_w	:= qt_risco_w + :new.IE_CIRURGIA;

if	(:new.IE_PERNAS = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_VARIZES = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_BMI = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_CIRUR_MES = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_INSUFICIENCIA_CARD = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_INSUFICIENCIA_CARD = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_DESCANSO = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_IBD = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_DIALISE = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_SEPSIS = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_INFECCAO = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_HRT = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_GRAVIDA = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_ABORTO = 'S') then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_OUTROS_01 is not null) then
	qt_risco_w	:= qt_risco_w + 1;
end if;


if	(:new.IE_OUTROS_02 is not null) then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_OUTROS_03 is not null) then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_OUTROS_04 is not null) then
	qt_risco_w	:= qt_risco_w + 1;
end if;

if	(:new.IE_CANCER = 'S') then
	qt_risco_w	:= qt_risco_w + 2;
end if;

if	(:new.IE_CAMA = 'S') then
	qt_risco_w	:= qt_risco_w + 2;
end if;

if	(:new.IE_MOBILIDADE = 'S') then
	qt_risco_w	:= qt_risco_w + 2;
end if;

if	(:new.IE_TUBO = 'S') then
	qt_risco_w	:= qt_risco_w + 2;
end if;

if	(:new.IE_DVT = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_HIST_FAMILIA = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_LEIDEN = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_LESOES_MULTIPLAS = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_HIT = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_HYPERHOMO	 = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_ANTI_CARD = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;

if	(:new.IE_OUTRO_TROMB = 'S') then
	qt_risco_w	:= qt_risco_w + 3;
end if;


if	(:new.IE_AVC = 'S') then
	qt_risco_w	:= qt_risco_w + 5;
end if;

if	(:new.IE_FRATURA = 'S') then
	qt_risco_w	:= qt_risco_w + 5;
end if;

if	(:new.IE_PARALESIA = 'S') then
	qt_risco_w	:= qt_risco_w + 5;
end if;

if	(qt_risco_w = 0) then
	:new.ie_risco	:= 'Z';
elsif	(qt_risco_w between 1 and 2) then
	:new.ie_risco	:= 'B';
elsif	(qt_risco_w between 3 and 4) then
	:new.ie_risco	:= 'M';
elsif	(qt_risco_w >= 5) then
	:new.ie_risco	:= 'A';
end if;

:new.qt_pontuacao := qt_risco_w;

IF(:new.ie_risco = 'Z') AND (:new.ie_risco IS NOT NULL) THEN
    :new.DS_RESULTADO := obter_desc_expressao(802666);
ELSIF (:new.ie_risco = 'B') AND (:new.ie_risco IS NOT NULL) THEN
    :new.DS_RESULTADO := obter_desc_expressao(802668);
ELSIF (:new.ie_risco = 'M') AND (:new.ie_risco IS NOT NULL) THEN
    :new.DS_RESULTADO := obter_desc_expressao(802670);
ELSIF (:new.ie_risco = 'A') AND (:new.ie_risco IS NOT NULL) THEN
    :new.DS_RESULTADO := obter_desc_expressao(802672);
END IF;
end;
/


ALTER TABLE TASY.ESCALA_CAPRINI ADD (
  CONSTRAINT ESCCAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CAPRINI ADD (
  CONSTRAINT ESCCAPR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCAPR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCCAPR_TEVMOTI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TEV) 
 REFERENCES TASY.TEV_MOTIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_CAPRINI TO NIVEL_1;

