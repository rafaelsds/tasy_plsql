ALTER TABLE TASY.PACIENTE_TRANSFUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_TRANSFUSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_TRANSFUSAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_DERIVADO            NUMBER(10),
  DT_TRANSFUSAO              DATE,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SANGUE                  VARCHAR2(20 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  IE_IRRADIADO               VARCHAR2(1 BYTE)   NOT NULL,
  IE_LAVADO                  VARCHAR2(1 BYTE)   NOT NULL,
  IE_FILTRADO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_ALIQUOTADO              VARCHAR2(1 BYTE)   NOT NULL,
  IE_AFERESE                 VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  DT_REGISTRO                DATE               NOT NULL,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_NEGA_TRANSFUSAO         VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  DS_ORIENTACAO              VARCHAR2(255 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACITRA_ATCONSPEPA_FK_I ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACITRA_EHRREEL_FK_I ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACITRA_I1 ON TASY.PACIENTE_TRANSFUSAO
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_I1
  MONITORING USAGE;


CREATE INDEX TASY.PACITRA_PESFISI_FK_I ON TASY.PACIENTE_TRANSFUSAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACITRA_PESFISI_FK2_I ON TASY.PACIENTE_TRANSFUSAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACITRA_PK ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACITRA_SANDERI_FK_I ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_SANDERI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACITRA_TASASDI_FK_I ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACITRA_TASASDI_FK2_I ON TASY.PACIENTE_TRANSFUSAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACITRA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_TRANSFUSAO_ATUAL
before insert or update ON TASY.PACIENTE_TRANSFUSAO for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pac_transfusao_pend_atual 
after insert or update ON TASY.PACIENTE_TRANSFUSAO 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSTR'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSTR'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_TRANSFUSAO_tp  after update ON TASY.PACIENTE_TRANSFUSAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_ORIENTACAO,1,500);gravar_log_alteracao(substr(:old.DS_ORIENTACAO,1,4000),substr(:new.DS_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AFERESE,1,500);gravar_log_alteracao(substr(:old.IE_AFERESE,1,4000),substr(:new.IE_AFERESE,1,4000),:new.nm_usuario,nr_seq_w,'IE_AFERESE',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NEGA_TRANSFUSAO,1,500);gravar_log_alteracao(substr(:old.IE_NEGA_TRANSFUSAO,1,4000),substr(:new.IE_NEGA_TRANSFUSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGA_TRANSFUSAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FILTRADO,1,500);gravar_log_alteracao(substr(:old.IE_FILTRADO,1,4000),substr(:new.IE_FILTRADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRADO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_IRRADIADO,1,500);gravar_log_alteracao(substr(:old.IE_IRRADIADO,1,4000),substr(:new.IE_IRRADIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IRRADIADO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LAVADO,1,500);gravar_log_alteracao(substr(:old.IE_LAVADO,1,4000),substr(:new.IE_LAVADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LAVADO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_DERIVADO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_DERIVADO,1,4000),substr(:new.NR_SEQ_DERIVADO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DERIVADO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_TRANSFUSAO,1,500);gravar_log_alteracao(to_char(:old.DT_TRANSFUSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_TRANSFUSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_TRANSFUSAO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ALIQUOTADO,1,500);gravar_log_alteracao(substr(:old.IE_ALIQUOTADO,1,4000),substr(:new.IE_ALIQUOTADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALIQUOTADO',ie_log_w,ds_w,'PACIENTE_TRANSFUSAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACIENTE_TRANSFUSAO ADD (
  CONSTRAINT PACITRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_TRANSFUSAO ADD (
  CONSTRAINT PACITRA_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACITRA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACITRA_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACITRA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACITRA_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT PACITRA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACITRA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_TRANSFUSAO TO NIVEL_1;

