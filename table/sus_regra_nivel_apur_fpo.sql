ALTER TABLE TASY.SUS_REGRA_NIVEL_APUR_FPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_REGRA_NIVEL_APUR_FPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_REGRA_NIVEL_APUR_FPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FORMA_ORG     NUMBER(10),
  NR_SEQ_SUBGRUPO      NUMBER(10),
  NR_SEQ_GRUPO         NUMBER(10),
  CD_REGISTRO          NUMBER(2),
  IE_NIVEL_APUR_FPO    VARCHAR2(15 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSRNAF_PK ON TASY.SUS_REGRA_NIVEL_APUR_FPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSRNAF_SUSFORG_FK_I ON TASY.SUS_REGRA_NIVEL_APUR_FPO
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRNAF_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSRNAF_SUSGRUP_FK_I ON TASY.SUS_REGRA_NIVEL_APUR_FPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRNAF_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSRNAF_SUSREGI_FK_I ON TASY.SUS_REGRA_NIVEL_APUR_FPO
(CD_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRNAF_SUSREGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSRNAF_SUSSUBG_FK_I ON TASY.SUS_REGRA_NIVEL_APUR_FPO
(NR_SEQ_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSRNAF_SUSSUBG_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_REGRA_NIVEL_APUR_FPO ADD (
  CONSTRAINT SUSRNAF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_REGRA_NIVEL_APUR_FPO ADD (
  CONSTRAINT SUSRNAF_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT SUSRNAF_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT SUSRNAF_SUSREGI_FK 
 FOREIGN KEY (CD_REGISTRO) 
 REFERENCES TASY.SUS_REGISTRO (CD_REGISTRO),
  CONSTRAINT SUSRNAF_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUS_REGRA_NIVEL_APUR_FPO TO NIVEL_1;

