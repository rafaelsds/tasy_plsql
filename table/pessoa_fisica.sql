ALTER TABLE TASY.PESSOA_FISICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA
(
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE) NOT NULL,
  IE_TIPO_PESSOA                 NUMBER(1)      NOT NULL,
  NM_PESSOA_FISICA               VARCHAR2(60 BYTE) NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_NASCIMENTO                  DATE,
  IE_SEXO                        VARCHAR2(1 BYTE),
  IE_ESTADO_CIVIL                VARCHAR2(2 BYTE),
  NR_CPF                         VARCHAR2(11 BYTE),
  NR_IDENTIDADE                  VARCHAR2(15 BYTE),
  NR_TELEFONE_CELULAR            VARCHAR2(40 BYTE),
  IE_GRAU_INSTRUCAO              NUMBER(2),
  NR_CEP_CIDADE_NASC             VARCHAR2(15 BYTE),
  NR_PRONTUARIO                  NUMBER(10),
  CD_RELIGIAO                    NUMBER(5),
  NR_PIS_PASEP                   VARCHAR2(11 BYTE),
  CD_NACIONALIDADE               VARCHAR2(8 BYTE),
  IE_DEPENDENCIA_SUS             VARCHAR2(1 BYTE),
  QT_ALTURA_CM                   NUMBER(4,1),
  IE_TIPO_SANGUE                 VARCHAR2(2 BYTE),
  IE_FATOR_RH                    VARCHAR2(1 BYTE),
  DT_OBITO                       DATE,
  NR_ISS                         VARCHAR2(20 BYTE),
  NR_INSS                        VARCHAR2(20 BYTE),
  NR_CERT_NASC                   VARCHAR2(255 BYTE),
  CD_CARGO                       NUMBER(10),
  DS_CODIGO_PROF                 VARCHAR2(15 BYTE),
  IE_FUNCIONARIO                 VARCHAR2(1 BYTE),
  NR_SEQ_COR_PELE                NUMBER(10),
  DS_ORGAO_EMISSOR_CI            VARCHAR2(40 BYTE),
  NR_CARTAO_NAC_SUS_ANT          NUMBER(20),
  CD_CBO_SUS                     NUMBER(6),
  CD_ATIVIDADE_SUS               NUMBER(3),
  IE_VINCULO_SUS                 NUMBER(1),
  CD_ESTABELECIMENTO             NUMBER(4),
  CD_SISTEMA_ANT                 VARCHAR2(20 BYTE),
  CD_FUNCIONARIO                 VARCHAR2(15 BYTE),
  NR_PAGER_BIP                   VARCHAR2(25 BYTE),
  NR_TRANSACAO_SUS               VARCHAR2(20 BYTE),
  CD_MEDICO                      VARCHAR2(10 BYTE),
  NM_PESSOA_PESQUISA             VARCHAR2(60 BYTE),
  DT_EMISSAO_CI                  DATE,
  NR_SEQ_CONSELHO                NUMBER(10),
  DT_ADMISSAO_HOSP               DATE,
  IE_FLUENCIA_PORTUGUES          VARCHAR2(5 BYTE),
  NR_TITULO_ELEITOR              VARCHAR2(20 BYTE),
  NR_ZONA                        VARCHAR2(5 BYTE),
  NR_SECAO                       VARCHAR2(15 BYTE),
  NR_CARTAO_ESTRANGEIRO          VARCHAR2(30 BYTE),
  NR_REG_GERAL_ESTRANG           VARCHAR2(30 BYTE),
  DT_CHEGADA_BRASIL              DATE,
  NM_USUARIO_ORIGINAL            VARCHAR2(15 BYTE),
  DT_CADASTRO_ORIGINAL           DATE,
  DS_HISTORICO                   VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_TIPO_PRONTUARIO             NUMBER(3),
  DS_OBSERVACAO                  VARCHAR2(2000 BYTE),
  QT_DEPENDENTE                  NUMBER(2),
  NR_TRANSPLANTE                 NUMBER(10),
  CD_EMPRESA                     NUMBER(4),
  NM_PESSOA_FISICA_SEM_ACENTO    VARCHAR2(60 BYTE),
  DT_REVISAO                     DATE,
  NM_USUARIO_REVISAO             VARCHAR2(15 BYTE),
  DT_DEMISSAO_HOSP               DATE,
  NR_CONTRA_REF_SUS              VARCHAR2(12 BYTE),
  CD_PUERICULTURA                NUMBER(6),
  SG_EMISSORA_CI                 VARCHAR2(2 BYTE),
  DT_NATURALIZACAO_PF            DATE,
  NR_CTPS                        NUMBER(10),
  UF_EMISSORA_CTPS               VARCHAR2(15 BYTE),
  DT_EMISSAO_CTPS                DATE,
  NR_PORTARIA_NAT                VARCHAR2(16 BYTE),
  DS_SENHA                       VARCHAR2(20 BYTE),
  DS_FONETICA                    VARCHAR2(60 BYTE),
  IE_REVISAR                     VARCHAR2(1 BYTE),
  CD_CNES                        VARCHAR2(20 BYTE),
  NR_SEQ_PERFIL                  NUMBER(10),
  NR_SAME                        NUMBER(10),
  NR_SEQ_CBO_SAUDE               NUMBER(10),
  DT_INTEGRACAO_EXTERNA          DATE,
  DS_FONETICA_CNS                VARCHAR2(60 BYTE),
  DS_APELIDO                     VARCHAR2(60 BYTE),
  DT_GERACAO_PRONT               DATE,
  CD_MUNICIPIO_IBGE              VARCHAR2(6 BYTE),
  NR_SEQ_PAIS                    NUMBER(10),
  NR_CERT_CASAMENTO              VARCHAR2(255 BYTE),
  NR_SEQ_CARTORIO_NASC           NUMBER(10),
  NR_SEQ_CARTORIO_CASAMENTO      NUMBER(10),
  DT_EMISSAO_CERT_NASC           DATE,
  DT_EMISSAO_CERT_CASAMENTO      DATE,
  NR_LIVRO_CERT_NASC             NUMBER(10),
  NR_LIVRO_CERT_CASAMENTO        NUMBER(10),
  NR_FOLHA_CERT_NASC             NUMBER(10),
  NR_FOLHA_CERT_CASAMENTO        NUMBER(10),
  NR_SEQ_NUT_PERFIL              NUMBER(10),
  NR_REGISTRO_PLS                VARCHAR2(20 BYTE),
  DT_VALIDADE_RG                 DATE,
  QT_PESO_NASC                   NUMBER(6,3),
  UF_CONSELHO                    VARCHAR2(15 BYTE),
  IE_STATUS_EXPORTAR             VARCHAR2(1 BYTE),
  IE_ENDERECO_CORRESPONDENCIA    NUMBER(2),
  NR_PRONT_DV                    VARCHAR2(1 BYTE),
  NM_ABREVIADO                   VARCHAR2(80 BYTE),
  NR_DDD_CELULAR                 VARCHAR2(3 BYTE),
  NR_DDI_CELULAR                 VARCHAR2(3 BYTE),
  IE_FREQUENTA_ESCOLA            VARCHAR2(1 BYTE),
  NR_CCM                         NUMBER(10),
  DT_FIM_EXPERIENCIA             DATE,
  DT_VALIDADE_CONSELHO           DATE,
  IE_NF_CORREIO                  VARCHAR2(15 BYTE),
  DS_PROFISSAO                   VARCHAR2(255 BYTE),
  DS_EMPRESA_PF                  VARCHAR2(255 BYTE),
  NR_PASSAPORTE                  VARCHAR2(255 BYTE),
  CD_TIPO_PJ                     NUMBER(3),
  NR_CNH                         VARCHAR2(255 BYTE),
  NR_CERT_MILITAR                VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO                NUMBER(5),
  NR_PRONT_EXT                   VARCHAR2(100 BYTE),
  DT_INICIO_OCUP_ATUAL           DATE,
  IE_ESCOLARIDADE_CNS            VARCHAR2(10 BYTE),
  IE_SITUACAO_CONJ_CNS           VARCHAR2(10 BYTE),
  NR_FOLHA_CERT_DIV              NUMBER(10),
  NR_CERT_DIVORCIO               VARCHAR2(20 BYTE),
  NR_SEQ_CARTORIO_DIVORCIO       NUMBER(10),
  DT_EMISSAO_CERT_DIVORCIO       DATE,
  NR_LIVRO_CERT_DIVORCIO         NUMBER(10),
  DT_ALTA_INSTITUCIONAL          DATE,
  DT_TRANSPLANTE                 DATE,
  CD_FAMILIA                     NUMBER(10),
  NR_MATRICULA_NASC              VARCHAR2(32 BYTE),
  IE_DOADOR                      VARCHAR2(15 BYTE),
  DT_VENCIMENTO_CNH              DATE,
  DS_CATEGORIA_CNH               VARCHAR2(30 BYTE),
  CD_PESSOA_MAE                  VARCHAR2(10 BYTE),
  DT_PRIMEIRA_ADMISSAO           DATE,
  DT_FIM_PRORROGACAO             DATE,
  NR_CERTIDAO_OBITO              VARCHAR2(255 BYTE),
  NR_SEQ_ETNIA                   NUMBER(10),
  NR_CARTAO_NAC_SUS              VARCHAR2(20 BYTE),
  QT_PESO                        NUMBER(6,3),
  IE_EMANCIPADO                  VARCHAR2(1 BYTE),
  IE_VINCULO_PROFISSIONAL        NUMBER(2),
  CD_NIT                         VARCHAR2(11 BYTE),
  IE_DEPENDENTE                  VARCHAR2(1 BYTE),
  DS_EMAIL_CCIH                  VARCHAR2(255 BYTE),
  CD_CGC_ORIG_TRANSPL            VARCHAR2(14 BYTE),
  DT_AFASTAMENTO                 DATE,
  NR_SEQ_TIPO_BENEFICIO          NUMBER(10),
  NR_SEQ_TIPO_INCAPACIDADE       NUMBER(10),
  IE_TRATAMENTO_PSIQUIATRICO     VARCHAR2(1 BYTE),
  NR_SEQ_AGENCIA_INSS            NUMBER(10),
  QT_DIAS_IG                     NUMBER(10),
  QT_SEMANAS_IG                  NUMBER(10),
  IE_REGRA_IG                    VARCHAR2(10 BYTE),
  DT_NASCIMENTO_IG               DATE,
  IE_STATUS_USUARIO_EVENT        VARCHAR2(2 BYTE),
  CD_CID_DIRETA                  VARCHAR2(10 BYTE),
  IE_COREN                       VARCHAR2(10 BYTE),
  DT_VALIDADE_COREN              DATE,
  NM_SOCIAL                      VARCHAR2(200 BYTE),
  NR_SEQ_COR_OLHO                NUMBER(10),
  NR_SEQ_COR_CABELO              NUMBER(10),
  DT_CAD_SISTEMA_ANT             DATE,
  IE_PERM_SMS_EMAIL              VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_PAC_AGE         NUMBER(10),
  NR_INSCRICAO_ESTADUAL          VARCHAR2(20 BYTE),
  IE_SOCIO                       VARCHAR2(15 BYTE),
  CD_ULT_PROFISSAO               NUMBER(10),
  IE_VEGETARIANO                 VARCHAR2(15 BYTE),
  DS_ORIENTACAO_COBRANCA         VARCHAR2(4000 BYTE),
  IE_CONSELHEIRO                 VARCHAR2(15 BYTE),
  IE_FUMANTE                     VARCHAR2(1 BYTE),
  NR_CELULAR_NUMEROS             NUMBER(20),
  NR_CODIGO_SERV_PREST           VARCHAR2(20 BYTE),
  DT_ADOCAO                      DATE,
  DS_LAUDO_ANAT_PATOL            VARCHAR2(255 BYTE),
  DT_LAUDO_ANAT_PATOL            DATE,
  NR_RIC                         VARCHAR2(11 BYTE),
  IE_CONSISTE_NR_SERIE_NF        VARCHAR2(1 BYTE),
  NR_RGA                         VARCHAR2(20 BYTE),
  CD_PESSOA_CROSS                NUMBER(15),
  CD_BARRAS_PESSOA               VARCHAR2(40 BYTE),
  NR_SEQ_FUNCAO_PF               NUMBER(10),
  NM_USUARIO_PRINC_CI            VARCHAR2(15 BYTE),
  NR_SEQ_TURNO_TRABALHO          NUMBER(10),
  NR_SEQ_CHEFIA                  NUMBER(10),
  CD_DECLARACAO_NASC_VIVO        VARCHAR2(30 BYTE),
  NR_TERMO_CERT_NASC             VARCHAR2(8 BYTE),
  CD_CURP                        VARCHAR2(18 BYTE),
  CD_RFC                         VARCHAR2(13 BYTE),
  SG_ESTADO_NASC                 VARCHAR2(15 BYTE),
  IE_FORNECEDOR                  VARCHAR2(1 BYTE),
  CD_IFE                         VARCHAR2(20 BYTE),
  NM_PRIMEIRO_NOME               VARCHAR2(60 BYTE),
  NM_SOBRENOME_PAI               VARCHAR2(60 BYTE),
  NM_SOBRENOME_MAE               VARCHAR2(60 BYTE),
  IE_RH_FRACO                    VARCHAR2(1 BYTE),
  DS_MUNICIPIO_NASC_ESTRANGEIRO  VARCHAR2(255 BYTE),
  IE_GEMELAR                     VARCHAR2(1 BYTE),
  IE_SUBTIPO_SANGUINEO           NUMBER(10),
  NR_SEQ_PERSON_NAME             NUMBER(10),
  IE_TIPO_DEFINITIVO_PROVISORIO  VARCHAR2(1 BYTE),
  NR_INSCRICAO_MUNICIPAL         VARCHAR2(20 BYTE),
  NR_SEQ_FORMA_TRAT              NUMBER(10),
  NR_SPSS                        VARCHAR2(15 BYTE),
  NR_SEQ_LINGUA_INDIGENA         NUMBER(10),
  NR_SEQ_NOME_SOLTEIRO           NUMBER(10),
  NR_SERIE_CTPS                  VARCHAR2(10 BYTE),
  IE_CONSIDERA_INDIO             VARCHAR2(1 BYTE),
  IE_OCUPACAO_HABITUAL           VARCHAR2(15 BYTE),
  IE_NASC_ESTIMADO               VARCHAR2(1 BYTE),
  IE_UNID_MED_PESO               VARCHAR2(10 BYTE),
  QT_PESO_UM                     NUMBER(10,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          104M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.PESSOA_FISICA.CD_PESSOA_FISICA IS 'Codigo da Pessoa fisica';

COMMENT ON COLUMN TASY.PESSOA_FISICA.IE_TIPO_PESSOA IS 'Indicador do Tipo de Pessoa';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NM_PESSOA_FISICA IS 'Nome da Pessoa Fisica';

COMMENT ON COLUMN TASY.PESSOA_FISICA.DT_NASCIMENTO IS 'Data de Nascimento';

COMMENT ON COLUMN TASY.PESSOA_FISICA.IE_SEXO IS 'Sexo da Pessoa';

COMMENT ON COLUMN TASY.PESSOA_FISICA.IE_ESTADO_CIVIL IS 'Estado Civil';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NR_CPF IS 'Numero do CPF';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NR_IDENTIDADE IS 'Numero da Carteira de Identidade';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NR_TELEFONE_CELULAR IS 'Numero do Telefone Celular';

COMMENT ON COLUMN TASY.PESSOA_FISICA.IE_GRAU_INSTRUCAO IS 'Grau de Instru�ao';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NR_CEP_CIDADE_NASC IS 'Cep da Cidade de Nascimento';

COMMENT ON COLUMN TASY.PESSOA_FISICA.NR_PRONTUARIO IS 'Numero do Prontuario';

COMMENT ON COLUMN TASY.PESSOA_FISICA.CD_RELIGIAO IS 'Codigo da Religiao';


CREATE INDEX TASY.PESFISI_AGCLSPC_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_CLASSIF_PAC_AGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_AGEINSS_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_AGENCIA_INSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_AGEINSS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_ATCNAER_FK_I ON TASY.PESSOA_FISICA
(CD_ATIVIDADE_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_ATCNAER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CARGO_FK_I ON TASY.PESSOA_FISICA
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_CARTORI_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_CARTORIO_NASC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CARTORI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CARTORI_FK2_I ON TASY.PESSOA_FISICA
(NR_SEQ_CARTORIO_CASAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CARTORI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CATLINI_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_LINGUA_INDIGENA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_CBORED_FK_I ON TASY.PESSOA_FISICA
(CD_CBO_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CBORED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CBOSAUD_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_CHEFIPF_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_CHEFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CHEFIPF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CIDDOEN_FK_I ON TASY.PESSOA_FISICA
(CD_CID_DIRETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CONPROF_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_CONSELHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_CORCABE_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_COR_CABELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CORCABE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_COROLHO_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_COR_OLHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_COROLHO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_CORPELE_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_COR_PELE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_CORPELE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_EMPRESA_FK_I ON TASY.PESSOA_FISICA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_ESTABEL_FK_I ON TASY.PESSOA_FISICA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_FUNPESF_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_FUNCAO_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_FUNPESF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_I1 ON TASY.PESSOA_FISICA
(IE_TIPO_PESSOA, NM_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          21M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I10 ON TASY.PESSOA_FISICA
(CD_SISTEMA_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_I10
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_I11 ON TASY.PESSOA_FISICA
(DS_CODIGO_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_I11
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_I14 ON TASY.PESSOA_FISICA
(IE_STATUS_EXPORTAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_I14
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_I15 ON TASY.PESSOA_FISICA
(CD_FUNCIONARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I2 ON TASY.PESSOA_FISICA
(DT_NASCIMENTO, NM_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          26M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I3 ON TASY.PESSOA_FISICA
(NR_CPF, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I4 ON TASY.PESSOA_FISICA
(NR_TELEFONE_CELULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I5 ON TASY.PESSOA_FISICA
(NR_IDENTIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I8 ON TASY.PESSOA_FISICA
(DS_FONETICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_I9 ON TASY.PESSOA_FISICA
(NM_PESSOA_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_MEDICO_FK_I ON TASY.PESSOA_FISICA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_NACIONA_FK_I ON TASY.PESSOA_FISICA
(CD_NACIONALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_NACIONA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_NUTPERF_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_NUT_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_NUTPERF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_PAIS_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_PAIS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_PEPPEPA_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_PERFIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_PEPPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_PERFIL_FK_I ON TASY.PESSOA_FISICA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_PERSON_NAME_I ON TASY.PESSOA_FISICA
(NR_SEQ_PERSON_NAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PESFISI_PESFISI_FK_I ON TASY.PESSOA_FISICA
(CD_PESSOA_MAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_PESJURI_FK_I ON TASY.PESSOA_FISICA
(CD_CGC_ORIG_TRANSPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFISI_PK ON TASY.PESSOA_FISICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_PROFISS_FK_I ON TASY.PESSOA_FISICA
(CD_ULT_PROFISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_PROFISS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_RELIGIA_FK_I ON TASY.PESSOA_FISICA
(CD_RELIGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_SEARCH ON TASY.PESSOA_FISICA
("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("NM_PESSOA_FISICA"))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_SEMASC ON TASY.PESSOA_FISICA
(NM_PESSOA_FISICA, NM_PESSOA_PESQUISA, NM_PESSOA_FISICA_SEM_ACENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_SUBSAN_FK_I ON TASY.PESSOA_FISICA
(IE_SUBTIPO_SANGUINEO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_SUSETNI_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_ETNIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_SUSETNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_SUSMUNI_FK_I ON TASY.PESSOA_FISICA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_TIPOBEN_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_TIPO_BENEFICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_TIPOBEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_TIPPEJU_FK_I ON TASY.PESSOA_FISICA
(CD_TIPO_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_TIPPEJU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_TPINCAP_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_TIPO_INCAPACIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_TPINCAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_TURTRAB_FK_I ON TASY.PESSOA_FISICA
(NR_SEQ_TURNO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_TURTRAB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_U1 ON TASY.PESSOA_FISICA
(NR_PRONTUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFISI_U2 ON TASY.PESSOA_FISICA
(NR_TRANSPLANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_U2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESFISI_U3 ON TASY.PESSOA_FISICA
(NR_CONTRA_REF_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_U3
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_11 ON TASY.PESSOA_FISICA
(DT_ADMISSAO_HOSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_11
  MONITORING USAGE;


CREATE INDEX TASY.PESFISI_12 ON TASY.PESSOA_FISICA
(DS_FONETICA_CNS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFISI_13 ON TASY.PESSOA_FISICA
(DS_APELIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISI_13
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_AfterUp
after insert or update ON TASY.PESSOA_FISICA for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
nm_contato_w			varchar2(60);
ds_endereco_w			varchar2(100);
cd_cep_w			varchar2(15);
nr_endereco_w			number(5);
ds_complemento_w		varchar2(40);
ds_municipio_w			varchar2(40);
ds_bairro_w			varchar2(40);
nr_telefone_w			varchar2(15);
nr_ramal_w			number(5);
ds_fone_adic_w			varchar2(80);
ds_email_w			varchar2(255);
cd_profissao_w			number(10);
cd_empresa_refer_w		number(10);
ds_setor_trabalho_w		varchar2(30);
ds_horario_trabalho_w		varchar2(30);
nr_matricula_trabalho_w		varchar2(20);
cd_municipio_ibge_w		varchar2(6);
ds_fax_w			varchar2(80);
cd_tipo_logradouro_w		varchar2(3);
nr_ddd_telefone_w		varchar2(3);
nr_ddi_telefone_w		varchar2(3);
nr_ddi_fax_w			varchar2(3);
nr_ddd_fax_w			varchar2(3);
ds_website_w			varchar2(255);
nm_contato_pesquisa_w		varchar2(60);
ie_tipo_complemento_w		number(2);
qt_existe_w			number(10);
ds_mensagem_w			varchar2(255);
ds_email_origem_w		varchar2(255);
qt_reg_w			number(1);
ie_opcao_w			varchar2(1) := 'I';
sg_estado_w		compl_pessoa_fisica.sg_estado%type;
ie_cad_completo_w	FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
ie_cad_simplif_w	FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
IE_ATUAL_PAC_AGEINT_w	parametro_agenda.IE_ATUAL_PAC_AGEINT%type;
IE_ATUAL_PAC_AGECON_AGESERV_w	parametro_agenda.IE_ATUAL_PAC_AGECON_AGESERV%type;
ie_existe_w	varchar2(1);
IE_STATUS_CPF_W VARCHAR2(1);
NR_QTD_REG_EST_W NUMBER(10);

cursor C01 is
	select	ds_email_envio
	from	regra_aviso_pessoa_cns
	where	ie_situacao = 'A';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Obter_param_Usuario(5, 164, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_completo_w);
Obter_param_Usuario(32, 44, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_simplif_w);

select  nvl(max(IE_ATUAL_PAC_AGEINT), 'N'),
	nvl(max(IE_ATUAL_PAC_AGECON_AGESERV), 'N')
into	IE_ATUAL_PAC_AGEINT_w,
	IE_ATUAL_PAC_AGECON_AGESERV_w
from 	PARAMETRO_AGENDA
where 	cd_estabelecimento = :new.cd_estabelecimento;

if	(:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica and updating) then
	--(OBTER_FUNCAO_ATIVA = 871) then -- OS 626900

	wheb_usuario_pck.set_ie_executar_trigger('N');

	update	agenda_paciente
	set	nm_paciente		= :new.nm_pessoa_fisica,
		nm_usuario		= 'Tasy',
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

	if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then
		if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
			update	agenda_consulta
			set	nm_paciente		= :new.nm_pessoa_fisica,
				nm_usuario		= 'Tasy',
				dt_atualizacao		= sysdate
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
		end if;
	end if;
	if (IE_ATUAL_PAC_AGEINT_w = 'S') then
		update	agenda_integrada
		set	NM_PACIENTE		= :new.nm_pessoa_fisica,
			nm_usuario		= 'Tasy',
			dt_atualizacao		= sysdate
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	end if;

	begin
		select	'S'
		into	ie_existe_w
		from	gerint_solic_internacao
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and		rownum = 1;

		update	gerint_solic_internacao
		set		NM_PESSOA_FISICA	= :new.nm_pessoa_fisica,
				nm_usuario			= 'Tasy',
				dt_atualizacao		= sysdate
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	exception
	when others then
		null;
	end;



	wheb_usuario_pck.set_ie_executar_trigger('S');
end if;


select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pf b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PF'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.nm_usuario <> 'INTEGR_TASY') then

	envia_sup_int_pf(
		:new.cd_pessoa_fisica,
		:new.nr_identidade,
		:new.nm_pessoa_fisica,
		:new.nr_telefone_celular,
		:new.ie_grau_instrucao,
		:new.nr_cep_cidade_nasc,
		:new.nr_prontuario,
		:new.nm_usuario,
		:new.cd_religiao,
		:new.nr_pis_pasep,
		:new.cd_nacionalidade,
		:new.ie_dependencia_sus,
		:new.qt_altura_cm,
		:new.ie_tipo_sangue,
		:new.ie_fator_rh,
		:new.dt_nascimento,
		:new.dt_obito,
		:new.ie_sexo,
		:new.nr_iss,
		:new.ie_estado_civil,
		:new.nr_inss,
		:new.nr_cpf,
		:new.nr_cert_nasc,
		:new.cd_cargo,
		:new.nr_cert_casamento,
		:new.ds_codigo_prof,
		:new.cd_empresa,
		:new.ie_funcionario,
		:new.nr_seq_cor_pele,
		:new.ds_orgao_emissor_ci,
		:new.nr_cartao_nac_sus,
		:new.cd_cbo_sus,
		:new.cd_atividade_sus,
		:new.ie_vinculo_sus,
		:new.cd_sistema_ant,
		:new.ie_frequenta_escola,
		:new.cd_funcionario,
		:new.nr_pager_bip,
		:new.nr_transacao_sus,
		:new.cd_medico,
		:new.ie_tipo_prontuario,
		:new.dt_emissao_ci,
		:new.nr_seq_conselho,
		:new.dt_admissao_hosp,
		:new.ie_fluencia_portugues,
		:new.nr_titulo_eleitor,
		:new.nr_zona,
		:new.nr_secao,
		:new.nr_cartao_estrangeiro,
		:new.nr_reg_geral_estrang,
		:new.dt_chegada_brasil,
		:new.sg_emissora_ci,
		:new.dt_naturalizacao_pf,
		:new.nr_ctps,
		:new.nr_serie_ctps,
		:new.uf_emissora_ctps,
		:new.dt_emissao_ctps,
		:new.nr_portaria_nat,
		:new.nr_seq_cartorio_nasc,
		:new.nr_seq_cartorio_casamento,
		:new.dt_emissao_cert_nasc,
		:new.dt_emissao_cert_casamento,
		:new.nr_livro_cert_nasc,
		:new.nr_livro_cert_casamento,
		:new.dt_cadastro_original,
		:new.nr_folha_cert_nasc,
		:new.nr_folha_cert_casamento,
		:new.nr_same,
		:new.ds_observacao,
		:new.qt_dependente,
		:new.nr_transplante,
		:new.dt_validade_rg,
		:new.dt_revisao,
		:new.dt_demissao_hosp,
		:new.cd_puericultura,
		:new.cd_cnes,
		:new.ds_apelido,
		:new.ie_endereco_correspondencia,
		:new.qt_peso_nasc,
		:new.uf_conselho,
		:new.nm_abreviado,
		:new.nr_ccm,
		:new.dt_fim_experiencia,
		:new.dt_validade_conselho,
		:new.ds_profissao,
		:new.ds_empresa_pf,
		:new.nr_passaporte,
		:new.nr_cnh,
		:new.nr_cert_militar,
		:new.nr_pront_ext,
		:new.ie_escolaridade_cns,
		:new.ie_situacao_conj_cns,
		:new.nr_folha_cert_div,
		:new.nr_cert_divorcio,
		:new.nr_seq_cartorio_divorcio,
		:new.dt_emissao_cert_divorcio,
		:new.nr_livro_cert_divorcio,
		:new.dt_alta_institucional,
		:new.dt_transplante,
		:new.nr_matricula_nasc,
		:new.ie_doador,
		:new.dt_vencimento_cnh,
		:new.ds_categoria_cnh,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null);
end if;

if	((:old.nr_cartao_nac_sus is null) and (:new.nr_cartao_nac_sus is not null)) or
	((:new.nr_cartao_nac_sus is not null) and (:old.nr_cartao_nac_sus <> :new.nr_cartao_nac_sus)) then

	open C01;
	loop
	fetch C01 into
		ds_email_w;
	exit when C01%notfound;
		begin
		if	(ds_email_w is not null) then
			ds_mensagem_w := OBTER_DESC_EXPRESSAO(327262) || ' ' || :new.cd_pessoa_fisica || chr(13) || chr(10) ||
					'Pessoa: ' || :new.nm_pessoa_fisica || chr(13) || chr(10) ||
					'CNS:'	|| :new.nr_cartao_nac_sus;
			enviar_email('Cadastro de Pessoa com CNS', ds_mensagem_w, ds_email_origem_w, ds_email_w, :new.nm_usuario, 'M');
		end if;
		end;
	end loop;
	close C01;

end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa fisica */
if (:new.nr_cpf is not null) then
	gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
	gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
end if;

if (updating) and (nvl(:old.nm_pessoa_fisica, :new.nm_pessoa_fisica) <> :new.nm_pessoa_fisica) then
	supplypoint_atual_inf_pac(:new.cd_pessoa_fisica, :new.nm_usuario);
	integracao_athena_disp_pck.atualiza_info_paciente(:new.cd_pessoa_fisica, :new.nm_usuario);
    gerar_int_dankia_pck.dankia_disp_atualiza_paciente(:new.cd_estabelecimento,
                                                       :new.nm_usuario,
                                                       :new.cd_pessoa_fisica,
                                                       :new.nm_pessoa_fisica,
                                                       :new.ie_sexo,
                                                       :new.dt_nascimento);
end if;

	select	max(ie_motivo_sem_cpf)
	into	ie_status_cpf_w
	from	pessoa_fisica_aux
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

  IF (IE_STATUS_CPF_W = '4'
  AND :NEW.NR_CARTAO_ESTRANGEIRO IS NULL
  AND :NEW.NR_REG_GERAL_ESTRANG IS NULL) THEN
    SELECT COUNT(*)
      INTO NR_QTD_REG_EST_W
      FROM PESSOA_FISICA_ESTRANGEIRO PFE
     WHERE PFE.CD_PESSOA_FISICA = :NEW.CD_PESSOA_FISICA;

    IF (NR_QTD_REG_EST_W = 0) THEN
      ATUALIZA_MOTIVO_SEM_CPF_PF(:NEW.CD_PESSOA_FISICA, NULL, :NEW.NM_USUARIO, 'N');

      IE_STATUS_CPF_W := NULL;
    END IF;
  END IF;

  SELECT CASE WHEN :NEW.NR_CPF IS NOT NULL THEN '7'
              WHEN :NEW.NR_CARTAO_ESTRANGEIRO IS NOT NULL
                OR :NEW.NR_REG_GERAL_ESTRANG IS NOT NULL
                OR (SELECT COUNT(*)
                      FROM PESSOA_FISICA_ESTRANGEIRO PFE
                     WHERE PFE.CD_PESSOA_FISICA = :NEW.CD_PESSOA_FISICA) > 0 THEN '4'
              WHEN OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') < 18
        and  (ie_status_cpf_w <> '1' or OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') > 0) THEN '2'
              WHEN OBTER_IDADE(:NEW.DT_NASCIMENTO, SYSDATE, 'A') > 60 THEN '3'
         ELSE '6' END
    INTO IE_STATUS_CPF_W
    FROM DUAL;

  ATUALIZA_MOTIVO_SEM_CPF_PF(:NEW.CD_PESSOA_FISICA, IE_STATUS_CPF_W, :NEW.NM_USUARIO, 'N');

if 	(updating) then
	ie_opcao_w	:= 'A';
end if;
reg_integracao_p.ie_operacao		:= ie_opcao_w;
reg_integracao_p.nr_prontuario		:= :new.nr_prontuario;
reg_integracao_p.cd_pessoa_fisica	:= :new.cd_pessoa_fisica;
reg_integracao_p.ie_funcionario 	:= nvl(:new.ie_funcionario,'N');
reg_integracao_p.nr_seq_conselho 	:= :new.nr_seq_conselho;

SELECT decode(COUNT(*), 0, 'N', 'S')
  INTO reg_integracao_p.ie_pessoa_atend
  FROM pessoa_fisica_aux
 WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
   AND nr_primeiro_atend IS NOT NULL;

   if :new.nr_cpf is not null then
      reg_integracao_p.ie_possui_cpf := 'S';
   end if;

if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
	gerar_int_padrao.gravar_integracao('12',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

if (:new.ie_funcionario = 'S') then
  gerar_int_padrao.gravar_integracao('304',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

if :new.dt_obito is not null then
  finaliza_evento_pj_obito(:new.cd_pessoa_fisica, :new.dt_obito,:new.nm_usuario, 'N');
end if;

<<Final>>
qt_reg_w  := 0;


end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PESSOA_FISICA_UPDATE
AFTER INSERT OR UPDATE ON TASY.PESSOA_FISICA FOR EACH ROW
DECLARE

ie_insere_registro_w		varchar2(10) := 'N';
qt_beneficiarios_repasse_w	number(10);
nr_seq_segurado_w		number(10);

begin

ie_insere_registro_w		:= 'N';
qt_beneficiarios_repasse_w	:= 0;

/*Essa trigger ira verificar apenas beneficiarios de repasse com responsabilidade transferida*/
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:old.nm_usuario is not null) then

		select	count(*)
		into	qt_beneficiarios_repasse_w
		from	pls_segurado
		where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
		and	ie_tipo_segurado	= 'R';

		if	(qt_beneficiarios_repasse_w	> 0) then
			select	max(nr_sequencia)
			into	nr_seq_segurado_w
			from	pls_segurado
			where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
			and	ie_tipo_segurado	= 'R';

			if	(:old.nm_pessoa_fisica <> :new.nm_pessoa_fisica) then
				ie_insere_registro_w := 'S';
			end if;

			if	(:old.dt_nascimento <> :new.dt_nascimento) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.ie_sexo,'0') <> nvl(:new.ie_sexo,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.NR_IDENTIDADE,'0') <> nvl(:new.NR_IDENTIDADE,'0')) then
				ie_insere_registro_w := 'S';
			end if;


			if	(nvl(:old.nr_cpf,'0') <> nvl(:new.nr_cpf,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.ie_estado_civil,'0') <> nvl(:new.ie_estado_civil,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(ie_insere_registro_w	= 'S') then
				begin
				insert into pls_segurado_alteracao
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						nr_seq_segurado)
				values	(	pls_segurado_alteracao_seq.nextval,trunc(sysdate,'dd'),:new.nm_usuario,trunc(sysdate,'dd'),:new.nm_usuario,
						nr_seq_segurado_w);
				exception
				when others then
					ie_insere_registro_w	:= 'N';
				end;

			end if;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_tp  after update ON TASY.PESSOA_FISICA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PESSOA_FISICA);  ds_c_w:=null; ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_SOCIAL,1,500);gravar_log_alteracao(substr(:old.NM_SOCIAL,1,4000),substr(:new.NM_SOCIAL,1,4000),:new.nm_usuario,nr_seq_w,'NM_SOCIAL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PERFIL,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PERFIL,1,4000),substr(:new.NR_SEQ_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PERFIL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SAME,1,4000),substr(:new.NR_SAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SAME',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBO_SAUDE,1,4000),substr(:new.NR_SEQ_CBO_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBO_SAUDE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INTEGRACAO_EXTERNA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_OBITO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_OBITO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_OBITO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SANGUE,1,4000),substr(:new.IE_TIPO_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SANGUE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PESSOA,1,4000),substr(:new.IE_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PESSOA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAU_INSTRUCAO,1,4000),substr(:new.IE_GRAU_INSTRUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_INSTRUCAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRONTUARIO,1,4000),substr(:new.NR_PRONTUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONTUARIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RELIGIAO,1,4000),substr(:new.CD_RELIGIAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_RELIGIAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PIS_PASEP,1,4000),substr(:new.NR_PIS_PASEP,1,4000),:new.nm_usuario,nr_seq_w,'NR_PIS_PASEP',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEPENDENCIA_SUS,1,4000),substr(:new.IE_DEPENDENCIA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEPENDENCIA_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CODIGO_PROF,1,4000),substr(:new.DS_CODIGO_PROF,1,4000),:new.nm_usuario,nr_seq_w,'DS_CODIGO_PROF',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FUNCIONARIO,1,4000),substr(:new.IE_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCIONARIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSS,1,4000),substr(:new.NR_INSS,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ISS,1,4000),substr(:new.NR_ISS,1,4000),:new.nm_usuario,nr_seq_w,'NR_ISS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NACIONALIDADE,1,4000),substr(:new.CD_NACIONALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_NACIONALIDADE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATOR_RH,1,4000),substr(:new.IE_FATOR_RH,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATOR_RH',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COR_PELE,1,4000),substr(:new.NR_SEQ_COR_PELE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COR_PELE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_EMISSOR_CI,1,4000),substr(:new.DS_ORGAO_EMISSOR_CI,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_EMISSOR_CI',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_NAC_SUS,1,4000),substr(:new.NR_CARTAO_NAC_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_NAC_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CBO_SUS,1,4000),substr(:new.CD_CBO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_CBO_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ATIVIDADE_SUS,1,4000),substr(:new.CD_ATIVIDADE_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_ATIVIDADE_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VINCULO_SUS,1,4000),substr(:new.IE_VINCULO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_VINCULO_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ALTURA_CM,1,4000),substr(:new.QT_ALTURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA_CM',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CEP_CIDADE_NASC,1,4000),substr(:new.NR_CEP_CIDADE_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_CEP_CIDADE_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_NASC,1,4000),substr(:new.NR_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCIONARIO,1,4000),substr(:new.CD_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCIONARIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PESSOA_PESQUISA,1,4000),substr(:new.NM_PESSOA_PESQUISA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_PESQUISA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CI,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CI,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CI',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONSELHO,1,4000),substr(:new.NR_SEQ_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONSELHO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PAGER_BIP,1,4000),substr(:new.NR_PAGER_BIP,1,4000),:new.nm_usuario,nr_seq_w,'NR_PAGER_BIP',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TRANSACAO_SUS,1,4000),substr(:new.NR_TRANSACAO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_TRANSACAO_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CHEGADA_BRASIL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CHEGADA_BRASIL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CHEGADA_BRASIL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FLUENCIA_PORTUGUES,1,4000),substr(:new.IE_FLUENCIA_PORTUGUES,1,4000),:new.nm_usuario,nr_seq_w,'IE_FLUENCIA_PORTUGUES',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TITULO_ELEITOR,1,4000),substr(:new.NR_TITULO_ELEITOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO_ELEITOR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ZONA,1,4000),substr(:new.NR_ZONA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ZONA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SECAO,1,4000),substr(:new.NR_SECAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SECAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_ESTRANGEIRO,1,4000),substr(:new.NR_CARTAO_ESTRANGEIRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_ESTRANGEIRO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REG_GERAL_ESTRANG,1,4000),substr(:new.NR_REG_GERAL_ESTRANG,1,4000),:new.nm_usuario,nr_seq_w,'NR_REG_GERAL_ESTRANG',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ADMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ADMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ADMISSAO_HOSP',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRONTUARIO,1,4000),substr(:new.IE_TIPO_PRONTUARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRONTUARIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HISTORICO,1,4000),substr(:new.DS_HISTORICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_HISTORICO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ORIGINAL,1,4000),substr(:new.NM_USUARIO_ORIGINAL,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_ORIGINAL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CADASTRO_ORIGINAL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REVISAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REVISAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REVISAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_REVISAO,1,4000),substr(:new.NM_USUARIO_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_REVISAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DEPENDENTE,1,4000),substr(:new.QT_DEPENDENTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_DEPENDENTE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_DEMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_DEMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_DEMISSAO_HOSP',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNES,1,4000),substr(:new.CD_CNES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNES',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTRA_REF_SUS,1,4000),substr(:new.NR_CONTRA_REF_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTRA_REF_SUS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TRANSPLANTE,1,4000),substr(:new.NR_TRANSPLANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TRANSPLANTE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PUERICULTURA,1,4000),substr(:new.CD_PUERICULTURA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PUERICULTURA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NATURALIZACAO_PF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NATURALIZACAO_PF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NATURALIZACAO_PF',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_EMISSORA_CI,1,4000),substr(:new.SG_EMISSORA_CI,1,4000),:new.nm_usuario,nr_seq_w,'SG_EMISSORA_CI',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CTPS,1,4000),substr(:new.NR_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CTPS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SERIE_CTPS,1,4000),substr(:new.NR_SERIE_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SERIE_CTPS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.UF_EMISSORA_CTPS,1,4000),substr(:new.UF_EMISSORA_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'UF_EMISSORA_CTPS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CTPS,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CTPS,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CTPS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONETICA,1,4000),substr(:new.DS_FONETICA,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONETICA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PORTARIA_NAT,1,4000),substr(:new.NR_PORTARIA_NAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTARIA_NAT',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_GERACAO_PRONT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_GERACAO_PRONT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_GERACAO_PRONT',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REVISAR,1,4000),substr(:new.IE_REVISAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REVISAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA_SEM_ACENTO,1,4000),substr(:new.NM_PESSOA_FISICA_SEM_ACENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA_SEM_ACENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_CASAMENTO,1,4000),substr(:new.NR_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_CASAMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_NASC,1,4000),substr(:new.NR_SEQ_CARTORIO_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_CASAMENTO,1,4000),substr(:new.NR_SEQ_CARTORIO_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_CASAMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_CASAMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_NASC,1,4000),substr(:new.NR_LIVRO_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_CASAMENTO,1,4000),substr(:new.NR_LIVRO_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_CASAMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_NASC,1,4000),substr(:new.NR_FOLHA_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_CASAMENTO,1,4000),substr(:new.NR_FOLHA_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_CASAMENTO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_NUT_PERFIL,1,4000),substr(:new.NR_SEQ_NUT_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_NUT_PERFIL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_APELIDO,1,4000),substr(:new.DS_APELIDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_APELIDO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONETICA_CNS,1,4000),substr(:new.DS_FONETICA_CNS,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONETICA_CNS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_RG,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_RG,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_RG',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_PLS,1,4000),substr(:new.NR_REGISTRO_PLS,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_PLS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRONT_DV,1,4000),substr(:new.NR_PRONT_DV,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONT_DV',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO_NASC,1,4000),substr(:new.QT_PESO_NASC,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO_NASC',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.UF_CONSELHO,1,4000),substr(:new.UF_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'UF_CONSELHO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_EXPORTAR,1,4000),substr(:new.IE_STATUS_EXPORTAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_EXPORTAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENDERECO_CORRESPONDENCIA,1,4000),substr(:new.IE_ENDERECO_CORRESPONDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENDERECO_CORRESPONDENCIA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ABREVIADO,1,4000),substr(:new.NM_ABREVIADO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ABREVIADO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_CELULAR,1,4000),substr(:new.NR_DDD_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_CELULAR,1,4000),substr(:new.NR_DDI_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FREQUENTA_ESCOLA,1,4000),substr(:new.IE_FREQUENTA_ESCOLA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FREQUENTA_ESCOLA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CCM,1,4000),substr(:new.NR_CCM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CCM',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CONSELHO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CONSELHO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CONSELHO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_EXPERIENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_EXPERIENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_EXPERIENCIA',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROFISSAO,1,4000),substr(:new.DS_PROFISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROFISSAO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMPRESA_PF,1,4000),substr(:new.DS_EMPRESA_PF,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMPRESA_PF',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_CORREIO,1,4000),substr(:new.IE_NF_CORREIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_CORREIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PJ,1,4000),substr(:new.CD_TIPO_PJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PJ',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CNH,1,4000),substr(:new.NR_CNH,1,4000),:new.nm_usuario,nr_seq_w,'NR_CNH',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_MILITAR,1,4000),substr(:new.NR_CERT_MILITAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_MILITAR',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PASSAPORTE,1,4000),substr(:new.NR_PASSAPORTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_PASSAPORTE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_DIVORCIO,1,4000),substr(:new.NR_CERT_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_DIVORCIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_DIVORCIO,1,4000),substr(:new.NR_SEQ_CARTORIO_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_DIVORCIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_DIVORCIO,1,4000),substr(:new.NR_LIVRO_CERT_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_DIVORCIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_DIVORCIO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_DIV,1,4000),substr(:new.NR_FOLHA_CERT_DIV,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_DIV',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESCOLARIDADE_CNS,1,4000),substr(:new.IE_ESCOLARIDADE_CNS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESCOLARIDADE_CNS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_CONJ_CNS,1,4000),substr(:new.IE_SITUACAO_CONJ_CNS,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_CONJ_CNS',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_OCUP_ATUAL',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRONT_EXT,1,4000),substr(:new.NR_PRONT_EXT,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONT_EXT',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DECLARACAO_NASC_VIVO,1,4000),substr(:new.CD_DECLARACAO_NASC_VIVO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DECLARACAO_NASC_VIVO',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEPENDENTE,1,4000),substr(:new.IE_DEPENDENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEPENDENTE',ie_log_w,ds_w,'PESSOA_FISICA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_pessoa_fisica_via_adic 
after update ON TASY.PESSOA_FISICA 
for each row
declare

vl_parametro_w			varchar2(20);
qt_beneficiarios_plano_w	number(10);
ie_lancar_via_adic		varchar2(20);
ie_possui_benef_w		varchar2(1) := 'N';

begin
ie_lancar_via_adic	:= 'N';
select	count(1)
into	qt_beneficiarios_plano_w
from	pls_segurado
where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
and	((dt_rescisao is null) or (dt_rescisao > sysdate));

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') and
	(qt_beneficiarios_plano_w > 0)	then
	pls_usuario_pck.set_ie_commit('S');
	if	(wheb_usuario_pck.get_cd_funcao in (1220,1268)) then
		Obter_Param_Usuario(1220, 39, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, vl_parametro_w);
	elsif	(wheb_usuario_pck.get_cd_funcao = 1286) then
		Obter_Param_Usuario(1286, 10, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, vl_parametro_w);
		if	(vl_parametro_w = 'P') then
			vl_parametro_w	:= 'S';
		end if;
	else 
		vl_parametro_w := 'N';
	end if;
	
	if	(vl_parametro_w = 'S') then
		null; 

	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Pessoa_Fisica_AfterInsert
AFTER INSERT ON TASY.PESSOA_FISICA FOR EACH ROW
DECLARE

cd_setor_atendimento_w	Number(5);
ie_setor_origem_w	varchar2(1); --Ivan em 10/08/2007 OS60862
nr_seq_set_origem_w	number(10);  --Ivan em 10/08/2007 OS60862
ie_regra_pront_w	varchar2(15);
sg_estado_w		compl_pessoa_fisica.sg_estado%type;

begin

if	(:new.nr_prontuario is not null) then

	select	nvl(max(vl_parametro),'BASE')
	into	ie_regra_pront_w
	from	funcao_parametro
	where	cd_funcao	= 0
	and	nr_sequencia	= 120;

	if	(ie_regra_pront_w <> 'ESTAB') then
		gerar_same_cpi_prontuario(	:new.nr_prontuario, :new.cd_pessoa_fisica, :new.cd_estabelecimento,:new.nm_usuario);
	end if;
end if;

gerar_regra_prontuario_gestao(null, :new.cd_estabelecimento, null, :new.cd_pessoa_fisica, :new.nm_usuario, null, null, null, null, null, null, null);

/* In�cio - Ivan em 10/08/2007 OS60862 */
Obter_Param_Usuario(32,19,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_setor_origem_w);

if	(ie_setor_origem_w = 'S') then
	begin

	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_atendimento_w
	from	usuario
	where	nm_usuario	= :new.nm_usuario;

	if	(cd_setor_atendimento_w > 0) then
		begin

		select	pf_setor_prontuario_seq.nextval
		into	nr_seq_set_origem_w
		from	dual;

		insert into pf_setor_prontuario (
			nr_sequencia,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			cd_setor_atendimento,
			dt_inicio_cinculo)
		values (
			nr_seq_set_origem_w,
			sysdate,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			:new.cd_pessoa_fisica,
			cd_setor_atendimento_w,
			sysdate
		);

		end;
	end if;

	end;
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa fisica */
select 	max(sg_estado)
into	sg_estado_w
from 	compl_pessoa_fisica
where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
and 	nvl(ie_tipo_complemento,1) = 1;
if (:new.nr_cpf is not null and sg_estado_w is not null) then
	gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
	gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
end if;
/* T�rmino - Ivan em 10/08/2007 OS60862 */

recent_patients_found_handler(null, :new.cd_pessoa_fisica, :new.nm_usuario, 'S');

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_insert_hl7
after insert ON TASY.PESSOA_FISICA for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin
ds_sep_bv_w := obter_separador_bv;

ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(71, ds_param_integ_hl7_w);

ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(144, ds_param_integ_hl7_w);

if (:new.cd_pessoa_fisica is not null) then
  call_bifrost_content('patient.create','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_BEFUPDATE_LOG
BEFORE UPDATE ON TASY.PESSOA_FISICA FOR EACH ROW
DECLARE

ie_alteracao_nome_w	varchar2(01);
cd_perfil_ativo_w		number(05,0) := obter_perfil_ativo;
cd_funcao_ativa_w		number(05,0) := obter_funcao_ativa;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	(:old.nm_pessoa_fisica <> :new.nm_pessoa_fisica) or
		(:old.dt_nascimento <> :new.dt_nascimento) or
		(:old.ie_sexo <> :new.ie_sexo) or
		(:old.ie_estado_civil <> :new.ie_estado_civil) or
		(:old.nr_cpf <> :new.nr_cpf) or
		(:old.nr_identidade <> :new.nr_identidade) or
		(:old.nr_telefone_celular <> :new.nr_telefone_celular) or
		(:old.ie_grau_instrucao <> :new.ie_grau_instrucao) or
		(:old.nr_cep_cidade_nasc <> :new.nr_cep_cidade_nasc) or
		(:old.nr_prontuario <> :new.nr_prontuario) or
		(:old.cd_religiao <> :new.cd_religiao) or
		(:old.nr_pis_pasep <> :new.nr_pis_pasep) or
		(:old.cd_nacionalidade <> :new.cd_nacionalidade) or
		(:old.ie_funcionario <> :new.ie_funcionario) or
		(:old.qt_altura_cm <> :new.qt_altura_cm) or
		(:old.nr_seq_cor_pele <> :new.nr_seq_cor_pele) then
		begin

		if	(nvl(:old.nm_pessoa_fisica, :new.nm_pessoa_fisica) <> :new.nm_pessoa_fisica) then
			ie_alteracao_nome_w	:= 'S';
		else
			ie_alteracao_nome_w	:= 'N';
		end if;

		insert into pessoa_fisica_alteracao(
			nr_sequencia,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo_pessoa,
			nm_pessoa_fisica,
			dt_nascimento,
			ie_sexo,
			ie_estado_civil,
			nr_cpf,
			nr_identidade,
			nr_telefone_celular,
			ie_grau_instrucao,
			nr_cep_cidade_nasc,
			nr_prontuario,
			cd_religiao,
			nr_pis_pasep,
			cd_nacionalidade,
			ie_dependencia_sus,
			qt_altura_cm,
			ie_tipo_sangue,
			ie_fator_rh,
			dt_obito,
			nr_iss,
			nr_inss,
			nr_cert_nasc,
			nr_cert_casamento,
			cd_cargo,
			ds_codigo_prof,
			cd_empresa,
			ie_funcionario,
			nr_seq_cor_pele,
			ds_orgao_emissor_ci,
			nr_cartao_nac_sus,
			cd_cbo_sus,
			cd_atividade_sus,
			ie_vinculo_sus,
			cd_estabelecimento,
			cd_sistema_ant,
			ie_frequenta_escola,
			cd_funcionario,
			nr_pager_bip,
			nr_transacao_sus,
			cd_medico,
			cd_perfil_ativo,
			ie_tipo_prontuario,
			nm_pessoa_pesquisa,
			dt_emissao_ci,
			nr_seq_conselho,
			dt_admissao_hosp,
			ie_fluencia_portugues,
			nr_titulo_eleitor,
			nr_zona,
			nr_secao,
			dt_emissao_ctps,
			nr_cartao_estrangeiro,
			nr_reg_geral_estrang,
			dt_chegada_brasil,
			nm_usuario_original,
			ds_historico,
			sg_emissora_ci,
			dt_naturalizacao_pf,
			nr_ctps,
			ie_status_exportar,
			nr_serie_ctps,
			uf_emissora_ctps,
			nr_portaria_nat,
			nr_seq_perfil,
			nr_seq_cartorio_nasc,
			nr_seq_cartorio_casamento,
			dt_emissao_cert_nasc,
			dt_emissao_cert_casamento,
			nr_livro_cert_nasc,
			nr_livro_cert_casamento,
			dt_cadastro_original,
			nr_folha_cert_nasc,
			nr_folha_cert_casamento,
			nr_same,
			ds_observacao,
			qt_dependente,
			nm_pessoa_fisica_sem_acento,
			dt_geracao_pront,
			nr_transplante,
			nr_registro_pls,
			dt_validade_rg,
			dt_revisao,
			nm_usuario_revisao,
			dt_demissao_hosp,
			nr_contra_ref_sus,
			cd_puericultura,
			ds_senha,
			ds_fonetica,
			ds_fonetica_cns,
			ie_revisar,
			cd_cnes,
			ds_apelido,
			nr_seq_cbo_saude,
			ie_endereco_correspondencia,
			ie_nf_correio,
			dt_integracao_externa,
			cd_municipio_ibge,
			nr_seq_pais,
			nr_seq_nut_perfil,
			qt_peso_nasc,
			uf_conselho,
			nr_pront_dv,
			nm_abreviado,
			nr_ddd_celular,
			nr_ddi_celular,
			nr_ccm,
			dt_fim_experiencia,
			dt_validade_conselho,
			ds_profissao,
			ds_empresa_pf,
			nr_passaporte,
			cd_tipo_pj,
			nr_cnh,
			nr_cert_militar,
			nr_pront_ext,
			dt_inicio_ocup_atual,
			ie_escolaridade_cns,
			ie_situacao_conj_cns,
			nr_folha_cert_div,
			nr_cert_divorcio,
			nr_seq_cartorio_divorcio,
			dt_emissao_cert_divorcio,
			nr_livro_cert_divorcio,
			dt_alta_institucional,
			dt_transplante,
			cd_familia,
			nr_matricula_nasc,
			nm_pessoa_atual,
			ie_alteracao_nome,
			dt_nascimento_atual,
			cd_perfil_alteracao,
			cd_funcao_ativa)
		values(	pessoa_fisica_alteracao_seq.nextval,
			:new.cd_pessoa_fisica,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:old.ie_tipo_pessoa,
			:old.nm_pessoa_fisica,
			:old.dt_nascimento,
			:old.ie_sexo,
			:old.ie_estado_civil,
			:old.nr_cpf,
			:old.nr_identidade,
			:old.nr_telefone_celular,
			:old.ie_grau_instrucao,
			:old.nr_cep_cidade_nasc,
			:old.nr_prontuario,
			:old.cd_religiao,
			:old.nr_pis_pasep,
			:old.cd_nacionalidade,
			:old.ie_dependencia_sus,
			:old.qt_altura_cm,
			:old.ie_tipo_sangue,
			:old.ie_fator_rh,
			:old.dt_obito,
			:old.nr_iss,
			:old.nr_inss,
			:old.nr_cert_nasc,
			:old.nr_cert_casamento,
			:old.cd_cargo,
			:old.ds_codigo_prof,
			:old.cd_empresa,
			:old.ie_funcionario,
			:old.nr_seq_cor_pele,
			:old.ds_orgao_emissor_ci,
			:old.nr_cartao_nac_sus,
			:old.cd_cbo_sus,
			:old.cd_atividade_sus,
			:old.ie_vinculo_sus,
			:old.cd_estabelecimento,
			:old.cd_sistema_ant,
			:old.ie_frequenta_escola,
			:old.cd_funcionario,
			:old.nr_pager_bip,
			:old.nr_transacao_sus,
			:old.cd_medico,
			:old.cd_perfil_ativo,
			:old.ie_tipo_prontuario,
			:old.nm_pessoa_pesquisa,
			:old.dt_emissao_ci,
			:old.nr_seq_conselho,
			:old.dt_admissao_hosp,
			:old.ie_fluencia_portugues,
			:old.nr_titulo_eleitor,
			:old.nr_zona,
			:old.nr_secao,
			:old.dt_emissao_ctps,
			:old.nr_cartao_estrangeiro,
			:old.nr_reg_geral_estrang,
			:old.dt_chegada_brasil,
			:old.nm_usuario_original,
			:old.ds_historico,
			:old.sg_emissora_ci,
			:old.dt_naturalizacao_pf,
			:old.nr_ctps,
			:old.ie_status_exportar,
			:old.nr_serie_ctps,
			:old.uf_emissora_ctps,
			:old.nr_portaria_nat,
			:old.nr_seq_perfil,
			:old.nr_seq_cartorio_nasc,
			:old.nr_seq_cartorio_casamento,
			:old.dt_emissao_cert_nasc,
			:old.dt_emissao_cert_casamento,
			:old.nr_livro_cert_nasc,
			:old.nr_livro_cert_casamento,
			:old.dt_cadastro_original,
			:old.nr_folha_cert_nasc,
			:old.nr_folha_cert_casamento,
			:old.nr_same,
			:old.ds_observacao,
			:old.qt_dependente,
			:old.nm_pessoa_fisica_sem_acento,
			:old.dt_geracao_pront,
			:old.nr_transplante,
			:old.nr_registro_pls,
			:old.dt_validade_rg,
			:old.dt_revisao,
			:old.nm_usuario_revisao,
			:old.dt_demissao_hosp,
			:old.nr_contra_ref_sus,
			:old.cd_puericultura,
			:old.ds_senha,
			:old.ds_fonetica,
			:old.ds_fonetica_cns,
			:old.ie_revisar,
			:old.cd_cnes,
			:old.ds_apelido,
			:old.nr_seq_cbo_saude,
			:old.ie_endereco_correspondencia,
			:old.ie_nf_correio,
			:old.dt_integracao_externa,
			:old.cd_municipio_ibge,
			:old.nr_seq_pais,
			:old.nr_seq_nut_perfil,
			:old.qt_peso_nasc,
			:old.uf_conselho,
			:old.nr_pront_dv,
			:old.nm_abreviado,
			:old.nr_ddd_celular,
			:old.nr_ddi_celular,
			:old.nr_ccm,
			:old.dt_fim_experiencia,
			:old.dt_validade_conselho,
			:old.ds_profissao,
			:old.ds_empresa_pf,
			:old.nr_passaporte,
			:old.cd_tipo_pj,
			:old.nr_cnh,
			:old.nr_cert_militar,
			:old.nr_pront_ext,
			:old.dt_inicio_ocup_atual,
			:old.ie_escolaridade_cns,
			:old.ie_situacao_conj_cns,
			:old.nr_folha_cert_div,
			:old.nr_cert_divorcio,
			:old.nr_seq_cartorio_divorcio,
			:old.dt_emissao_cert_divorcio,
			:old.nr_livro_cert_divorcio,
			:old.dt_alta_institucional,
			:old.dt_transplante,
			:old.cd_familia,
			:old.nr_matricula_nasc,
			:new.nm_pessoa_fisica,
			ie_alteracao_nome_w,
			:new.dt_nascimento,
			cd_perfil_ativo_w,
			cd_funcao_ativa_w);
		end;
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_insert
before insert ON TASY.PESSOA_FISICA for each row
declare

  nr_identidade_w           varchar2(20);
  nr_cartao_nac_sus_w       varchar2(20);
  ie_permite_pontuacao_rg_w varchar2(1);
  ds_given_name_w           person_name.ds_given_name%type;
  ds_component_name_1_w     person_name.ds_component_name_1%type;
  ds_family_name_w          person_name.ds_family_name%type;
  cd_pessoa_fisica_w        varchar2(10);
  nm_pessoa_montar_w        pessoa_fisica.nm_pessoa_fisica%type;
  NR_PARAM_277_W            NUMBER(10);

  ds_nls_territory_w		    varchar2(64);
  ds_hash_new_w             pessoa_fisica_exc.ds_hash%type;
  ds_hash_exc_w             pessoa_fisica_exc.ds_hash%type;
  nr_seq_hash_exc_w         pessoa_fisica_exc.nr_sequencia%type;
  ds_text_w                 varchar2(500);
begin

/* Esta deve ser sempre a primeira rotina da trigger */
if	(:new.nm_primeiro_nome is not null) or (:new.nr_seq_person_name is not null) then
	if	(:new.nr_seq_person_name is not null) then
		begin
		select 	max(ds_given_name),
				max(ds_component_name_1),
				max(ds_family_name)
		into	ds_given_name_w,
				ds_component_name_1_w,
				ds_family_name_w
		from	person_name
		where	nr_sequencia = :new.nr_seq_person_name
		and		ds_type	= 'main';

		:new.nm_primeiro_nome := nvl(ds_given_name_w,:new.nm_primeiro_nome);
		:new.nm_sobrenome_pai := nvl(ds_family_name_w,:new.nm_sobrenome_pai);
		:new.nm_sobrenome_mae := nvl(ds_component_name_1_w,:new.nm_sobrenome_mae);
		end;
	end if;
  nm_pessoa_montar_w := substr(montar_nm_pessoa_fisica(null,:new.nm_primeiro_nome,:new.nm_sobrenome_pai,:new.nm_sobrenome_mae, :new.nm_usuario, :new.nr_seq_person_name),1,60);
  if (trim(nm_pessoa_montar_w) is  not null) then
    :new.nm_pessoa_fisica	:= nm_pessoa_montar_w;
  end if;
end if;

ie_permite_pontuacao_rg_w	:= nvl(Obter_Valor_Param_Usuario(0, 184, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento), 'N');
  NR_PARAM_277_W := NVL(OBTER_SOMENTE_NUMERO(OBTER_VALOR_PARAM_USUARIO(5, 277, OBTER_PERFIL_ATIVO, :NEW.NM_USUARIO, :NEW.CD_ESTABELECIMENTO)), 0);
-- Edgar 14/01/2005, OS 14182, fiz a trigger

:new.nm_pessoa_fisica		:= trim(:new.nm_pessoa_fisica);
while	(instr(:new.nm_pessoa_fisica, '  ') > 0) loop
	:new.nm_pessoa_fisica	:= replace(:new.nm_pessoa_fisica, '  ', ' ');
end loop;

  IF (NR_PARAM_277_W > 0
  AND LENGTH(:NEW.NM_PESSOA_FISICA) < NR_PARAM_277_W) THEN
    WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(1112273);
  END IF;

nr_identidade_w		:= '';

if	(nvl(ie_permite_pontuacao_rg_w,'N') = 'N') then
	if	(:new.NR_IDENTIDADE is not null) then /* Elemar inclui o if por que dava erro */
		:new.NR_IDENTIDADE	:= upper(:new.NR_IDENTIDADE);
		for	i in 1..length(:new.NR_IDENTIDADE) loop		-- validar identidade, somente letras e numeros
			if	(instr('0123456789ABCDEFGHIJKLMNOPQRSTUVXWYV',substr(:new.NR_IDENTIDADE, i, 1)) > 0) then
				nr_identidade_w		:= nr_identidade_w || substr(:new.NR_IDENTIDADE, i, 1);
			end if;

		end loop;
		:new.NR_IDENTIDADE	:= nr_identidade_w;
	end if;
end if;

/* Marcus em 04/12/06 OS445904 */
if	(:new.ds_senha is null) then
	select	obter_senha_pf(:new.dt_nascimento)
	into	:new.ds_senha
	from	dual;
end if;

/* Coelho em 02/01/07 	OS45273 */
if ( (:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica) or
	(:old.ds_fonetica is null )) then
	:new.ds_fonetica := gera_fonetica(:new.nm_pessoa_fisica,'N');
end if;

/* Fernando 02/08/07 */
if	(:new.dt_nascimento > sysdate) then
	wheb_mensagem_pck.exibir_mensagem_abort(194547);
	/*Data de nascimento invalida!*/
end	if;

if	(:new.dt_nascimento is not null) then
	:new.dt_nascimento	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_nascimento);
end if;

/* Oraci em 26/12/2007 OS77737 */
if	(:new.nm_pessoa_fisica_sem_acento is null) or
	(:new.nm_pessoa_fisica <> :old.nm_pessoa_fisica) or
	(:new.nm_social <> :old.nm_social) then
  begin
       if (obter_se_nome_social <> 'N') then
		   :new.nm_pessoa_fisica_sem_acento	:= nvl(elimina_acentuacao(:new.nm_social), elimina_acentuacao(:new.nm_pessoa_fisica));
       else
           :new.nm_pessoa_fisica_sem_acento	:= elimina_acentuacao(:new.nm_pessoa_fisica);
      end if;
  end;
end if;

/* Oraci em 11/01/2008 OS78875*/
if	(:new.nr_prontuario is not null) and
	(:old.nr_prontuario is null) then
	:new.dt_geracao_pront :=  sysdate;
	/*insert into logxxx_tasy(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
	values		(	sysdate,
				:new.nm_usuario,
				55777,
				'PF: ' || :new.cd_pessoa_fisica || chr(13) ||
				'Pessoa_Fisica_Insert'  || chr(13) ||
				':old.nr_prontuario=' || :old.nr_prontuario || chr(13) ||
				':new.nr_prontuario=' || :new.nr_prontuario || chr(13) ||
				':old.dt_geracao_pront=' || :old.dt_geracao_pront || chr(13) ||
				':new.dt_geracao_pront=' || :new.dt_geracao_pront);*/
elsif	(:new.nr_prontuario is null) and
	(:old.nr_prontuario is not null) then
	:new.dt_geracao_pront :=  null;

	/*insert into logxxx_tasy(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
	values		(	sysdate,
				:new.nm_usuario,
				55777,
				'PF: ' || :new.cd_pessoa_fisica || chr(13) ||
				'Pessoa_Fisica_Insert'  || chr(13) ||
				':old.nr_prontuario=' || :old.nr_prontuario || chr(13) ||
				':new.nr_prontuario=' || :new.nr_prontuario || chr(13) ||
				':old.dt_geracao_pront=' || :old.dt_geracao_pront || chr(13) ||
				':new.dt_geracao_pront=' || :new.dt_geracao_pront);*/
end if;

if	(:new.nr_cartao_nac_sus is not null) then
	begin
	select	to_number(:new.nr_cartao_nac_sus)
	into	nr_cartao_nac_sus_w
	from	dual;
	exception when others then
		wheb_mensagem_pck.exibir_mensagem_abort(194548);
		/*Nao e permitido informar caracteres alfanumericos no numero do cartao SUS.*/
	end;
end if;

/*       Coelho 03/07/2008 OS95248 */
:new.nm_pessoa_pesquisa	:= padronizar_nome(:new.nm_pessoa_fisica);

if ('00/00/0000 00:00:00' = to_char(:new.dt_obito,'dd/mm/yyyy hh24:mi:ss')) then
	:new.dt_obito := null;
end if;

:new.nm_abreviado	:= pls_gerar_nome_abreviado(:new.nm_pessoa_fisica);

if(upper(:new.cd_pessoa_fisica) = '@SEQUENCE') then
	begin
		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;
		:new.cd_pessoa_fisica := cd_pessoa_fisica_w;
	end;
end if;

if (:new.nm_pessoa_fisica is not null ) then
  check_patient_deletion_hash(:new.nm_pessoa_fisica, :new.dt_nascimento, :new.cd_pessoa_mae, :new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ISCV_PESSOA_FISICA_UPDATE
after update or insert ON TASY.PESSOA_FISICA for each row
declare

ds_sep_bv_w		varchar2(100)	:= obter_separador_bv;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;

	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;

	begin
	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;


	function permiteIntegrar return boolean is	--Verificar se h� algum procedimento interno que deva integrar
	ds_retorno_w		varchar2(1);
	begin
	if	permiteIntegrarISCV() then
		nr_atendimento_w := obter_atendimento_paciente(:new.cd_pessoa_fisica, obter_estabelecimento_ativo);

		select	nvl(max(Obter_Se_Integr_Proc_Interno(pp.nr_seq_proc_interno, 17,null,pp.ie_lado,nvl(pm.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento))),'N') ie_permite_proc_integ
		into	ds_retorno_w
		from	prescr_procedimento pp,
				prescr_medica pm
		where	pm.nr_atendimento = nr_atendimento_w
		and		pp.nr_seq_proc_interno is not null
		and		pp.nr_sequencia = pm.nr_prescricao;

		if	(ds_retorno_w = 'N') then

			select	nvl(max(Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
			into	ds_retorno_w
			from	agenda_paciente
			where	nr_seq_proc_interno is not null
			and		nr_atendimento = nr_atendimento_w;

			if	(ds_retorno_w = 'N') then
				select	nvl(max(Obter_Se_Integr_Proc_Interno(app.nr_seq_proc_interno, 17,null,app.ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
				into	ds_retorno_w
				from	agenda_paciente ap,
						agenda_paciente_proc app
				where	ap.nr_seq_proc_interno is not null
				and		ap.nr_atendimento = nr_atendimento_w
				and		ap.nr_sequencia = app.nr_sequencia;
			end if;

			if	(ds_retorno_w = 'N') then
				select	decode(count(*),0,'N','S')
				into	ds_retorno_w
				from	prescr_medica a,
						prescr_procedimento b,
						atendimento_paciente c
				where	a.nr_prescricao = b.nr_prescricao
				and		a.nr_atendimento = c.nr_atendimento
				and		c.cd_pessoa_fisica = :new.cd_pessoa_fisica
				and exists(select	1
						from	regra_proc_interno_integra x
						where	x.nr_seq_proc_interno = b.nr_seq_proc_interno
						and	x.ie_tipo_integracao = 17
						and	nvl(x.ie_tipo_proc_integr,0) = 0
						and	NVL(x.cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = NVL(wheb_usuario_pck.get_cd_estabelecimento,0));

			end if;
		end if;

		return ds_retorno_w = 'S';
	else
		return false;
	end if;

	end;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	((:old.nm_pessoa_fisica <> :new.nm_pessoa_fisica)
	or	(:old.nr_seq_person_name   <> :new.nr_seq_person_name )
	or	(:old.cd_estabelecimento <> :new.cd_estabelecimento)
	or	(:old.nr_seq_cor_pele <> :new.nr_seq_cor_pele)
	or	(:old.dt_nascimento <> :new.dt_nascimento)
	or	(:old.ie_sexo <> :new.ie_sexo))
	and	permiteIntegrar() then
		gravar_agend_integracao(746, 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w, Obter_Setor_Atendimento(obter_atendimento_paciente(:new.cd_pessoa_fisica, obter_estabelecimento_ativo)));
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.GERINT_pessoa_fisica
after update ON TASY.PESSOA_FISICA for each row
declare

ie_integra_gerint_w		varchar2(1);
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ds_sep_bv_w				varchar2(50);

begin

if 	((wheb_usuario_pck.get_ie_executar_trigger = 'S' ) and
	(obter_dados_param_atend(:new.cd_estabelecimento,'GI') = 'S') and
	(((:old.NR_CPF is null) and (:new.NR_CPF is not null)) or
	((:old.NR_CARTAO_NAC_SUS is null) and (:new.NR_CARTAO_NAC_SUS is not null)))) then

	select 	decode(count(*),0,'N','S'),
			max(nr_atendimento)
	into	ie_integra_gerint_w,
			nr_atendimento_w
	from	gerint_solic_internacao
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica
	and		nr_atendimento is not null
	and		ie_situacao not in ('E','N','L');

	if (ie_integra_gerint_w = 'S') then

		ds_sep_bv_w	:=	obter_separador_bv;

		--Atualiza dados na Solicita��o de interna��o.
		update	GERINT_SOLIC_INTERNACAO
		set		NM_PESSOA_FISICA = :new.nm_pessoa_fisica,
				NR_CPF_PACIENTE = :new.nr_cpf,
				NR_CARTAO_SUS = :new.nr_cartao_nac_sus
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and		nr_atendimento = nr_atendimento_w;

		--Dispara o evento de integra��o.
		exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  							|| ds_sep_bv_w ||
																	'cd_estabelecimento_p=' 		|| :new.cd_estabelecimento				 		|| ds_sep_bv_w ||
																	'id_evento_p=' 					|| '13'				 							|| ds_sep_bv_w ||
																	'nr_atendimento_p=' 			|| nr_atendimento_w	 							|| ds_sep_bv_w ||
																	'ie_leito_extra_p=' 			|| null				 							|| ds_sep_bv_w ||
																	'dt_alta_p=' 					|| null											|| ds_sep_bv_w ||
																	'ds_motivo_alta_p=' 			|| null											|| ds_sep_bv_w ||
																	'ds_justif_transferencia_p='	|| null 										|| ds_sep_bv_w ||
																	'nr_seq_solic_internacao_p=' 	|| null 										|| ds_sep_bv_w ||
																	'nr_seq_leito_p=' 				|| null											|| ds_sep_bv_w ||
																	'ds_ident_leito_p='				|| null											|| ds_sep_bv_w ||
																	'nr_seq_classif_p=' 			|| null											|| ds_sep_bv_w ||
																	'ie_status_unidade_p=' 			|| null											|| ds_sep_bv_w ||
																	'nr_cpf_paciente_p=' 			|| :new.NR_CPF									|| ds_sep_bv_w ||
																	'nr_cartao_sus_p=' 				|| :new.NR_CARTAO_NAC_SUS						|| ds_sep_bv_w ||
																	'cd_cid_p='						|| null											|| ds_sep_bv_w ||
																	'cd_evolucao_p='				|| null);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_delete_hl7
after delete ON TASY.PESSOA_FISICA for each row
declare

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

  ger_pessoa_fisica_delete(:old.cd_pessoa_fisica);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_pls_rps
after update ON TASY.PESSOA_FISICA for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w		varchar2(1) := 'N';

begin
begin
	if	(nvl(:new.nr_cpf,'0') 		!= nvl(:old.nr_cpf,'0')) or
		(nvl(:new.nm_pessoa_fisica,'0')	!= nvl(:old.nm_pessoa_fisica,'0')) or
		(nvl(:new.cd_cnes,'0')		!= nvl(:old.cd_cnes,'0')) or
		(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) then
		ie_alterou_w := 'S';
	end if;

	if 	(ie_alterou_w = 'S') then
		pls_gerar_alt_prest_rps(null, :new.cd_pessoa_fisica, null, :old.nr_cpf, :old.cd_municipio_ibge, :old.cd_cnes, :new.nm_usuario, null, null, null);
	end if;
exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_delete
after delete ON TASY.PESSOA_FISICA for each row
declare
reg_integracao_p	gerar_int_padrao.reg_integracao;

begin
reg_integracao_p.ie_operacao	:=	'E';
reg_integracao_p.nr_prontuario	:=	:old.nr_prontuario;
reg_integracao_p.cd_pessoa_fisica	:=	:old.cd_pessoa_fisica;

SELECT DECODE(COUNT(*),0, 'N', 'S')
INTO reg_integracao_p.ie_pessoa_atend
FROM pessoa_fisica_aux
WHERE cd_pessoa_fisica = :old.cd_pessoa_fisica
AND NR_PRIMEIRO_ATEND IS NOT NULL;

if :old.nr_cpf is not null then
   reg_integracao_p.ie_possui_cpf := 'S';
end if;

if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
gerar_int_padrao.gravar_integracao('12',:old.cd_pessoa_fisica,:old.nm_usuario, reg_integracao_p);
end if;

delete pessoa_fisica_aux
WHERE cd_pessoa_fisica = :old.cd_pessoa_fisica;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_tie
after insert or update ON TASY.PESSOA_FISICA for each row
declare

json_w        philips_json;
json_data_w   clob;

pragma autonomous_transaction;

cursor c01 is
select 	a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        a.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        pkg_date_formaters.to_varchar((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)), 'shortTime', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_opme h where h.nr_seq_agenda = a.nr_sequencia and h.dt_exclusao is null), wheb_mensagem_pck.get_texto(94755)) OPSM,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7),
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        (select :new.cd_pessoa_fisica cd_pessoa_fisica,
                :new.nm_pessoa_fisica nm_pessoa_fisica,
                :new.dt_nascimento dt_nascimento,
                :new.nr_prontuario nr_prontuario,
                :new.ie_tipo_sangue ie_tipo_sangue,
                :new.ie_fator_rh ie_fator_rh,
                :new.nr_telefone_celular nr_telefone_celular,
                :new.ie_sexo ie_sexo,
                :new.cd_pessoa_mae cd_pessoa_mae
        from    dual) d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate
and     sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24
and     b.cd_tipo_agenda = 1
union
select  a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        j.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(j.cd_procedimento, j.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        pkg_date_formaters.to_varchar((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)), 'shortTime', wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_nm_usuario) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_opme h where h.nr_seq_agenda = a.nr_sequencia and h.dt_exclusao is null), wheb_mensagem_pck.get_texto(94755)) OPSM,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7),
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        (select :new.cd_pessoa_fisica cd_pessoa_fisica,
                :new.nm_pessoa_fisica nm_pessoa_fisica,
                :new.dt_nascimento dt_nascimento,
                :new.nr_prontuario nr_prontuario,
                :new.ie_tipo_sangue ie_tipo_sangue,
                :new.ie_fator_rh ie_fator_rh,
                :new.nr_telefone_celular nr_telefone_celular,
                :new.ie_sexo ie_sexo,
                :new.cd_pessoa_mae cd_pessoa_mae
        from    dual) d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g,
        agenda_paciente_proc j
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     j.nr_sequencia = a.nr_sequencia
and     a.dt_agenda >= sysdate
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate
and     sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24
and     b.cd_tipo_agenda = 1
order by schedule_date, start_time, patient;

begin

if (wheb_usuario_pck.get_nm_usuario is not null) then

	for c01_w in c01 loop

		json_w := philips_json();
		json_w.put('schedule_id', c01_w.schedule_id);
		json_w.put('schedule', c01_w.schedule);
		json_w.put('schedule_classification_id', c01_w.schedule_classification_id);
		json_w.put('schedule_classification', c01_w.schedule_classification);
		json_w.put('schedule_status_id', c01_w.schedule_status_id);
		json_w.put('schedule_status', c01_w.schedule_status);
		json_w.put('schedule_date', c01_w.schedule_date);
		json_w.put('scheduling_date', c01_w.scheduling_date);
		json_w.put('schedule_code', c01_w.schedule_code);
		json_w.put('procedure_id', c01_w.procedure_id);
		json_w.put('procedure_description', c01_w.procedure_description);
		json_w.put('procedure_status', c01_w.procedure_status);
		json_w.put('reservation', c01_w.reservation);
		json_w.put('admission_date', c01_w.admission_date);
		json_w.put('estimated_admission_date', c01_w.estimated_admission_date);
		json_w.put('authorization_status_id', c01_w.authorization_status_id);
		json_w.put('authorization_status', c01_w.authorization_status);
		json_w.put('start_time', c01_w.start_time);
		json_w.put('gestational_age', c01_w.gestational_age);
		json_w.put('age', c01_w.age);
		json_w.put('surgeon', c01_w.surgeon);
		json_w.put('encounter', c01_w.encounter);
		json_w.put('room', c01_w.room);
		json_w.put('insurance', c01_w.insurance);
		json_w.put('insurance_user', c01_w.insurance_user);
		json_w.put('admission', c01_w.admission);
		json_w.put('telephone', c01_w.telephone);
		json_w.put('procedure_estimated_duration', c01_w.procedure_estimated_duration);
		json_w.put('procedure_duration', c01_w.procedure_duration);
		json_w.put('patient_id', c01_w.patient_id);
		json_w.put('patient', c01_w.patient);
		json_w.put('birth', c01_w.birth);
		json_w.put('medical_record', c01_w.medical_record);
		json_w.put('blood', c01_w.blood);
		json_w.put('cellphone', c01_w.cellphone);
		json_w.put('mother', c01_w.mother);
		json_w.put('gender', c01_w.gender);
		json_w.put('anaesthesia', c01_w.anaesthesia);
		json_w.put('surgery_nature', c01_w.surgery_nature);
		json_w.put('preorder_blood', c01_w.preorder_blood);
		json_w.put('OPSM', c01_w.OPSM);
		json_w.put('recommendation', c01_w.recommendation);
		json_w.put('patient_classification', c01_w.patient_classification);
		json_w.put('surgery', c01_w.surgery);
		json_w.put('insurance_coverage', c01_w.insurance_coverage);
		json_w.put('notes', c01_w.notes);
		json_w.put('ICU', c01_w.ICU);
		json_w.put('NICU', c01_w.NICU);
		json_w.put('surgical_services_status', c01_w.surgical_services_status);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end loop;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_update
 before update ON TASY.PESSOA_FISICA  FOR EACH ROW
DECLARE
 nm_coluna_w        varchar2(50);
 ie_alterar_w        varchar2(10);
 ds_w            varchar2(50);
 vl_parametro_w    varchar2(20);
 qt_registros_w    number(10);
 ie_tipo_segurado_w    varchar2(10);
 ie_estipulante_w    varchar2(10);
 ie_pagador_w        varchar2(10);
 Cursor C01 is
    select    ie_tipo_segurado
    from    pls_segurado
    where    cd_pessoa_fisica    = :new.cd_pessoa_fisica
    and    ((dt_rescisao is null)  or (dt_rescisao > sysdate));

begin
/*ESSA TRIGGER IR� COLOCAR AS INFORMA��OES CONFORME GERADA NA PROCEDURE TASY_CRIAR_TRIGGER_PF_ALT*/

if    (wheb_usuario_pck.get_cd_funcao = 281) and (:new.dt_nascimento > sysdate) then
    wheb_mensagem_pck.exibir_mensagem_abort(194547);
end if;
 if    (wheb_usuario_pck.get_ie_executar_trigger    = 'S') and
    (pls_usuario_pck.get_ie_exec_trigger_solic_pf    = 'S') then
    pls_usuario_pck.set_ie_lancar_mensagem_alt('N');
    pls_usuario_pck.set_ie_commit('S');
     if    (wheb_usuario_pck.get_cd_funcao = 5) then
        Obter_Param_Usuario(5, 177, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
    elsif    (wheb_usuario_pck.get_cd_funcao = 32) then
        Obter_Param_Usuario(32, 46, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
    elsif    (wheb_usuario_pck.get_cd_funcao = 1220) then
        Obter_Param_Usuario(1220, 26, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
    else
        vl_parametro_w := 'N';
    end if;
    
    
    if    (vl_parametro_w = 'S') then
        null;
              gerar_solicitacao_alteracao(:old.IE_GRAU_INSTRUCAO,:new.IE_GRAU_INSTRUCAO,'IE_GRAU_INSTRUCAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CEP_CIDADE_NASC,:new.NR_CEP_CIDADE_NASC,'NR_CEP_CIDADE_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PRONTUARIO,:new.NR_PRONTUARIO,'NR_PRONTUARIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_RELIGIAO,:new.CD_RELIGIAO,'CD_RELIGIAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PIS_PASEP,:new.NR_PIS_PASEP,'NR_PIS_PASEP',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_NACIONALIDADE,:new.CD_NACIONALIDADE,'CD_NACIONALIDADE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_DEPENDENCIA_SUS,:new.IE_DEPENDENCIA_SUS,'IE_DEPENDENCIA_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_ALTURA_CM,:new.QT_ALTURA_CM,'QT_ALTURA_CM',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_TIPO_SANGUE,:new.IE_TIPO_SANGUE,'IE_TIPO_SANGUE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FATOR_RH,:new.IE_FATOR_RH,'IE_FATOR_RH',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_OBITO,'dd/mm/yyyy'),to_char(:new.DT_OBITO,'dd/mm/yyyy'),'DT_OBITO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_ISS,:new.NR_ISS,'NR_ISS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_INSS,:new.NR_INSS,'NR_INSS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CERT_NASC,:new.NR_CERT_NASC,'NR_CERT_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CARGO,:new.CD_CARGO,'CD_CARGO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_CODIGO_PROF,:new.DS_CODIGO_PROF,'DS_CODIGO_PROF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FUNCIONARIO,:new.IE_FUNCIONARIO,'IE_FUNCIONARIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_COR_PELE,:new.NR_SEQ_COR_PELE,'NR_SEQ_COR_PELE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_ORGAO_EMISSOR_CI,:new.DS_ORGAO_EMISSOR_CI,'DS_ORGAO_EMISSOR_CI',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              --gerar_solicitacao_alteracao(:old.NR_CARTAO_NAC_SUS_ANT,:new.NR_CARTAO_NAC_SUS_ANT,'NR_CARTAO_NAC_SUS_ANT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CBO_SUS,:new.CD_CBO_SUS,'CD_CBO_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_ATIVIDADE_SUS,:new.CD_ATIVIDADE_SUS,'CD_ATIVIDADE_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_VINCULO_SUS,:new.IE_VINCULO_SUS,'IE_VINCULO_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_ESTABELECIMENTO,:new.CD_ESTABELECIMENTO,'CD_ESTABELECIMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_SISTEMA_ANT,:new.CD_SISTEMA_ANT,'CD_SISTEMA_ANT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_FUNCIONARIO,:new.CD_FUNCIONARIO,'CD_FUNCIONARIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PAGER_BIP,:new.NR_PAGER_BIP,'NR_PAGER_BIP',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_TRANSACAO_SUS,:new.NR_TRANSACAO_SUS,'NR_TRANSACAO_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_MEDICO,:new.CD_MEDICO,'CD_MEDICO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_PESSOA_PESQUISA,:new.NM_PESSOA_PESQUISA,'NM_PESSOA_PESQUISA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_EMISSAO_CI,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CI,'dd/mm/yyyy'),'DT_EMISSAO_CI',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CONSELHO,:new.NR_SEQ_CONSELHO,'NR_SEQ_CONSELHO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_ADMISSAO_HOSP,'dd/mm/yyyy'),to_char(:new.DT_ADMISSAO_HOSP,'dd/mm/yyyy'),'DT_ADMISSAO_HOSP',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FLUENCIA_PORTUGUES,:new.IE_FLUENCIA_PORTUGUES,'IE_FLUENCIA_PORTUGUES',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_TITULO_ELEITOR,:new.NR_TITULO_ELEITOR,'NR_TITULO_ELEITOR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_ZONA,:new.NR_ZONA,'NR_ZONA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SECAO,:new.NR_SECAO,'NR_SECAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CARTAO_ESTRANGEIRO,:new.NR_CARTAO_ESTRANGEIRO,'NR_CARTAO_ESTRANGEIRO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_REG_GERAL_ESTRANG,:new.NR_REG_GERAL_ESTRANG,'NR_REG_GERAL_ESTRANG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_CHEGADA_BRASIL,'dd/mm/yyyy'),to_char(:new.DT_CHEGADA_BRASIL,'dd/mm/yyyy'),'DT_CHEGADA_BRASIL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_USUARIO_ORIGINAL,:new.NM_USUARIO_ORIGINAL,'NM_USUARIO_ORIGINAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy'),to_char(:new.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy'),'DT_CADASTRO_ORIGINAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy'),'DT_ATUALIZACAO_NREC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_USUARIO_NREC,:new.NM_USUARIO_NREC,'NM_USUARIO_NREC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_TIPO_PRONTUARIO,:new.IE_TIPO_PRONTUARIO,'IE_TIPO_PRONTUARIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_DEPENDENTE,:new.QT_DEPENDENTE,'QT_DEPENDENTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_TRANSPLANTE,:new.NR_TRANSPLANTE,'NR_TRANSPLANTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_PESSOA_FISICA_SEM_ACENTO,:new.NM_PESSOA_FISICA_SEM_ACENTO,'NM_PESSOA_FISICA_SEM_ACENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_EMPRESA,:new.CD_EMPRESA,'CD_EMPRESA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_REVISAO,'dd/mm/yyyy'),to_char(:new.DT_REVISAO,'dd/mm/yyyy'),'DT_REVISAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_USUARIO_REVISAO,:new.NM_USUARIO_REVISAO,'NM_USUARIO_REVISAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_DEMISSAO_HOSP,'dd/mm/yyyy'),to_char(:new.DT_DEMISSAO_HOSP,'dd/mm/yyyy'),'DT_DEMISSAO_HOSP',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CONTRA_REF_SUS,:new.NR_CONTRA_REF_SUS,'NR_CONTRA_REF_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_PUERICULTURA,:new.CD_PUERICULTURA,'CD_PUERICULTURA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.SG_EMISSORA_CI,:new.SG_EMISSORA_CI,'SG_EMISSORA_CI',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_NATURALIZACAO_PF,'dd/mm/yyyy'),to_char(:new.DT_NATURALIZACAO_PF,'dd/mm/yyyy'),'DT_NATURALIZACAO_PF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CTPS,:new.NR_CTPS,'NR_CTPS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SERIE_CTPS,:new.NR_SERIE_CTPS,'NR_SERIE_CTPS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.UF_EMISSORA_CTPS,:new.UF_EMISSORA_CTPS,'UF_EMISSORA_CTPS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_EMISSAO_CTPS,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CTPS,'dd/mm/yyyy'),'DT_EMISSAO_CTPS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PORTARIA_NAT,:new.NR_PORTARIA_NAT,'NR_PORTARIA_NAT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_SENHA,:new.DS_SENHA,'DS_SENHA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_FONETICA,:new.DS_FONETICA,'DS_FONETICA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_REVISAR,:new.IE_REVISAR,'IE_REVISAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CNES,:new.CD_CNES,'CD_CNES',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_PERFIL,:new.NR_SEQ_PERFIL,'NR_SEQ_PERFIL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SAME,:new.NR_SAME,'NR_SAME',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CBO_SAUDE,:new.NR_SEQ_CBO_SAUDE,'NR_SEQ_CBO_SAUDE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy'),to_char(:new.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy'),'DT_INTEGRACAO_EXTERNA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_FONETICA_CNS,:new.DS_FONETICA_CNS,'DS_FONETICA_CNS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_APELIDO,:new.DS_APELIDO,'DS_APELIDO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_GERACAO_PRONT,'dd/mm/yyyy'),to_char(:new.DT_GERACAO_PRONT,'dd/mm/yyyy'),'DT_GERACAO_PRONT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_MUNICIPIO_IBGE,:new.CD_MUNICIPIO_IBGE,'CD_MUNICIPIO_IBGE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_PAIS,:new.NR_SEQ_PAIS,'NR_SEQ_PAIS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CERT_CASAMENTO,:new.NR_CERT_CASAMENTO,'NR_CERT_CASAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CARTORIO_NASC,:new.NR_SEQ_CARTORIO_NASC,'NR_SEQ_CARTORIO_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CARTORIO_CASAMENTO,:new.NR_SEQ_CARTORIO_CASAMENTO,'NR_SEQ_CARTORIO_CASAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy'),'DT_EMISSAO_CERT_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy'),'DT_EMISSAO_CERT_CASAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_FOLHA_CERT_NASC,:new.NR_FOLHA_CERT_NASC,'NR_FOLHA_CERT_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_FOLHA_CERT_CASAMENTO,:new.NR_FOLHA_CERT_CASAMENTO,'NR_FOLHA_CERT_CASAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_LIVRO_CERT_NASC,:new.NR_LIVRO_CERT_NASC,'NR_LIVRO_CERT_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_LIVRO_CERT_CASAMENTO,:new.NR_LIVRO_CERT_CASAMENTO,'NR_LIVRO_CERT_CASAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_NUT_PERFIL,:new.NR_SEQ_NUT_PERFIL,'NR_SEQ_NUT_PERFIL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_REGISTRO_PLS,:new.NR_REGISTRO_PLS,'NR_REGISTRO_PLS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_VALIDADE_RG,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_RG,'dd/mm/yyyy'),'DT_VALIDADE_RG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_PESO_NASC,:new.QT_PESO_NASC,'QT_PESO_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.UF_CONSELHO,:new.UF_CONSELHO,'UF_CONSELHO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_STATUS_EXPORTAR,:new.IE_STATUS_EXPORTAR,'IE_STATUS_EXPORTAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_ENDERECO_CORRESPONDENCIA,:new.IE_ENDERECO_CORRESPONDENCIA,'IE_ENDERECO_CORRESPONDENCIA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PRONT_DV,:new.NR_PRONT_DV,'NR_PRONT_DV',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_ABREVIADO,:new.NM_ABREVIADO,'NM_ABREVIADO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_DDD_CELULAR,:new.NR_DDD_CELULAR,'NR_DDD_CELULAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_DDI_CELULAR,:new.NR_DDI_CELULAR,'NR_DDI_CELULAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FREQUENTA_ESCOLA,:new.IE_FREQUENTA_ESCOLA,'IE_FREQUENTA_ESCOLA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CCM,:new.NR_CCM,'NR_CCM',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_FIM_EXPERIENCIA,'dd/mm/yyyy'),to_char(:new.DT_FIM_EXPERIENCIA,'dd/mm/yyyy'),'DT_FIM_EXPERIENCIA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_VALIDADE_CONSELHO,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_CONSELHO,'dd/mm/yyyy'),'DT_VALIDADE_CONSELHO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_PROFISSAO,:new.DS_PROFISSAO,'DS_PROFISSAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_EMPRESA_PF,:new.DS_EMPRESA_PF,'DS_EMPRESA_PF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_NF_CORREIO,:new.IE_NF_CORREIO,'IE_NF_CORREIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PASSAPORTE,:new.NR_PASSAPORTE,'NR_PASSAPORTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_TIPO_PJ,:new.CD_TIPO_PJ,'CD_TIPO_PJ',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CNH,:new.NR_CNH,'NR_CNH',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CERT_MILITAR,:new.NR_CERT_MILITAR,'NR_CERT_MILITAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_PERFIL_ATIVO,:new.CD_PERFIL_ATIVO,'CD_PERFIL_ATIVO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_PRONT_EXT,:new.NR_PRONT_EXT,'NR_PRONT_EXT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_TIPO_DEFINITIVO_PROVISORIO,:new.IE_TIPO_DEFINITIVO_PROVISORIO,'IE_TIPO_DEFINITIVO_PROVISORIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_TIPO_PESSOA,:new.IE_TIPO_PESSOA,'IE_TIPO_PESSOA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_PESSOA_FISICA,:new.NM_PESSOA_FISICA,'NM_PESSOA_FISICA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy'),'DT_NASCIMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_SEXO,:new.IE_SEXO,'IE_SEXO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_ESTADO_CIVIL,:new.IE_ESTADO_CIVIL,'IE_ESTADO_CIVIL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CPF,:new.NR_CPF,'NR_CPF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_IDENTIDADE,:new.NR_IDENTIDADE,'NR_IDENTIDADE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_TELEFONE_CELULAR,:new.NR_TELEFONE_CELULAR,'NR_TELEFONE_CELULAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy'),to_char(:new.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy'),'DT_INICIO_OCUP_ATUAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_ESCOLARIDADE_CNS,:new.IE_ESCOLARIDADE_CNS,'IE_ESCOLARIDADE_CNS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_SITUACAO_CONJ_CNS,:new.IE_SITUACAO_CONJ_CNS,'IE_SITUACAO_CONJ_CNS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CERT_DIVORCIO,:new.NR_CERT_DIVORCIO,'NR_CERT_DIVORCIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CARTORIO_DIVORCIO,:new.NR_SEQ_CARTORIO_DIVORCIO,'NR_SEQ_CARTORIO_DIVORCIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy'),'DT_EMISSAO_CERT_DIVORCIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_LIVRO_CERT_DIVORCIO,:new.NR_LIVRO_CERT_DIVORCIO,'NR_LIVRO_CERT_DIVORCIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_FOLHA_CERT_DIV,:new.NR_FOLHA_CERT_DIV,'NR_FOLHA_CERT_DIV',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_ALTA_INSTITUCIONAL,'dd/mm/yyyy'),to_char(:new.DT_ALTA_INSTITUCIONAL,'dd/mm/yyyy'),'DT_ALTA_INSTITUCIONAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_TRANSPLANTE,'dd/mm/yyyy'),to_char(:new.DT_TRANSPLANTE,'dd/mm/yyyy'),'DT_TRANSPLANTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_FAMILIA,:new.CD_FAMILIA,'CD_FAMILIA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_MATRICULA_NASC,:new.NR_MATRICULA_NASC,'NR_MATRICULA_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_DOADOR,:new.IE_DOADOR,'IE_DOADOR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_VENCIMENTO_CNH,'dd/mm/yyyy'),to_char(:new.DT_VENCIMENTO_CNH,'dd/mm/yyyy'),'DT_VENCIMENTO_CNH',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_CATEGORIA_CNH,:new.DS_CATEGORIA_CNH,'DS_CATEGORIA_CNH',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_PESSOA_MAE,:new.CD_PESSOA_MAE,'CD_PESSOA_MAE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_PRIMEIRA_ADMISSAO,'dd/mm/yyyy'),to_char(:new.DT_PRIMEIRA_ADMISSAO,'dd/mm/yyyy'),'DT_PRIMEIRA_ADMISSAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_FIM_PRORROGACAO,'dd/mm/yyyy'),to_char(:new.DT_FIM_PRORROGACAO,'dd/mm/yyyy'),'DT_FIM_PRORROGACAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CERTIDAO_OBITO,:new.NR_CERTIDAO_OBITO,'NR_CERTIDAO_OBITO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_ETNIA,:new.NR_SEQ_ETNIA,'NR_SEQ_ETNIA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CARTAO_NAC_SUS,:new.NR_CARTAO_NAC_SUS,'NR_CARTAO_NAC_SUS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_PESO,:new.QT_PESO,'QT_PESO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_EMANCIPADO,:new.IE_EMANCIPADO,'IE_EMANCIPADO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_VINCULO_PROFISSIONAL,:new.IE_VINCULO_PROFISSIONAL,'IE_VINCULO_PROFISSIONAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_NIT,:new.CD_NIT,'CD_NIT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_DEPENDENTE,:new.IE_DEPENDENTE,'IE_DEPENDENTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_EMAIL_CCIH,:new.DS_EMAIL_CCIH,'DS_EMAIL_CCIH',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CGC_ORIG_TRANSPL,:new.CD_CGC_ORIG_TRANSPL,'CD_CGC_ORIG_TRANSPL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_AFASTAMENTO,'dd/mm/yyyy'),to_char(:new.DT_AFASTAMENTO,'dd/mm/yyyy'),'DT_AFASTAMENTO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_TRATAMENTO_PSIQUIATRICO,:new.IE_TRATAMENTO_PSIQUIATRICO,'IE_TRATAMENTO_PSIQUIATRICO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_TIPO_BENEFICIO,:new.NR_SEQ_TIPO_BENEFICIO,'NR_SEQ_TIPO_BENEFICIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_TIPO_INCAPACIDADE,:new.NR_SEQ_TIPO_INCAPACIDADE,'NR_SEQ_TIPO_INCAPACIDADE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_AGENCIA_INSS,:new.NR_SEQ_AGENCIA_INSS,'NR_SEQ_AGENCIA_INSS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_DIAS_IG,:new.QT_DIAS_IG,'QT_DIAS_IG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.QT_SEMANAS_IG,:new.QT_SEMANAS_IG,'QT_SEMANAS_IG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_REGRA_IG,:new.IE_REGRA_IG,'IE_REGRA_IG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_NASCIMENTO_IG,'dd/mm/yyyy'),to_char(:new.DT_NASCIMENTO_IG,'dd/mm/yyyy'),'DT_NASCIMENTO_IG',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_STATUS_USUARIO_EVENT,:new.IE_STATUS_USUARIO_EVENT,'IE_STATUS_USUARIO_EVENT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CID_DIRETA,:new.CD_CID_DIRETA,'CD_CID_DIRETA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_COREN,:new.IE_COREN,'IE_COREN',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_VALIDADE_COREN,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_COREN,'dd/mm/yyyy'),'DT_VALIDADE_COREN',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_SOCIAL,:new.NM_SOCIAL,'NM_SOCIAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_COR_OLHO,:new.NR_SEQ_COR_OLHO,'NR_SEQ_COR_OLHO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_COR_CABELO,:new.NR_SEQ_COR_CABELO,'NR_SEQ_COR_CABELO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_CAD_SISTEMA_ANT,'dd/mm/yyyy'),to_char(:new.DT_CAD_SISTEMA_ANT,'dd/mm/yyyy'),'DT_CAD_SISTEMA_ANT',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_PERM_SMS_EMAIL,:new.IE_PERM_SMS_EMAIL,'IE_PERM_SMS_EMAIL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CLASSIF_PAC_AGE,:new.NR_SEQ_CLASSIF_PAC_AGE,'NR_SEQ_CLASSIF_PAC_AGE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_INSCRICAO_ESTADUAL,:new.NR_INSCRICAO_ESTADUAL,'NR_INSCRICAO_ESTADUAL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_SOCIO,:new.IE_SOCIO,'IE_SOCIO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_ULT_PROFISSAO,:new.CD_ULT_PROFISSAO,'CD_ULT_PROFISSAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_VEGETARIANO,:new.IE_VEGETARIANO,'IE_VEGETARIANO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_CONSELHEIRO,:new.IE_CONSELHEIRO,'IE_CONSELHEIRO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FUMANTE,:new.IE_FUMANTE,'IE_FUMANTE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CODIGO_SERV_PREST,:new.NR_CODIGO_SERV_PREST,'NR_CODIGO_SERV_PREST',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_ADOCAO,'dd/mm/yyyy'),to_char(:new.DT_ADOCAO,'dd/mm/yyyy'),'DT_ADOCAO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_CELULAR_NUMEROS,:new.NR_CELULAR_NUMEROS,'NR_CELULAR_NUMEROS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_LAUDO_ANAT_PATOL,:new.DS_LAUDO_ANAT_PATOL,'DS_LAUDO_ANAT_PATOL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(to_char(:old.DT_LAUDO_ANAT_PATOL,'dd/mm/yyyy'),to_char(:new.DT_LAUDO_ANAT_PATOL,'dd/mm/yyyy'),'DT_LAUDO_ANAT_PATOL',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_RIC,:new.NR_RIC,'NR_RIC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_CONSISTE_NR_SERIE_NF,:new.IE_CONSISTE_NR_SERIE_NF,'IE_CONSISTE_NR_SERIE_NF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_RGA,:new.NR_RGA,'NR_RGA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_BARRAS_PESSOA,:new.CD_BARRAS_PESSOA,'CD_BARRAS_PESSOA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_PESSOA_CROSS,:new.CD_PESSOA_CROSS,'CD_PESSOA_CROSS',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_FUNCAO_PF,:new.NR_SEQ_FUNCAO_PF,'NR_SEQ_FUNCAO_PF',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_USUARIO_PRINC_CI,:new.NM_USUARIO_PRINC_CI,'NM_USUARIO_PRINC_CI',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_TURNO_TRABALHO,:new.NR_SEQ_TURNO_TRABALHO,'NR_SEQ_TURNO_TRABALHO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_CHEFIA,:new.NR_SEQ_CHEFIA,'NR_SEQ_CHEFIA',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_DECLARACAO_NASC_VIVO,:new.CD_DECLARACAO_NASC_VIVO,'CD_DECLARACAO_NASC_VIVO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_TERMO_CERT_NASC,:new.NR_TERMO_CERT_NASC,'NR_TERMO_CERT_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_CURP,:new.CD_CURP,'CD_CURP',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_RFC,:new.CD_RFC,'CD_RFC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.SG_ESTADO_NASC,:new.SG_ESTADO_NASC,'SG_ESTADO_NASC',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_FORNECEDOR,:new.IE_FORNECEDOR,'IE_FORNECEDOR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.CD_IFE,:new.CD_IFE,'CD_IFE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_PRIMEIRO_NOME,:new.NM_PRIMEIRO_NOME,'NM_PRIMEIRO_NOME',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_SOBRENOME_MAE,:new.NM_SOBRENOME_MAE,'NM_SOBRENOME_MAE',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NM_SOBRENOME_PAI,:new.NM_SOBRENOME_PAI,'NM_SOBRENOME_PAI',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_RH_FRACO,:new.IE_RH_FRACO,'IE_RH_FRACO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.DS_MUNICIPIO_NASC_ESTRANGEIRO,:new.DS_MUNICIPIO_NASC_ESTRANGEIRO,'DS_MUNICIPIO_NASC_ESTRANGEIRO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_GEMELAR,:new.IE_GEMELAR,'IE_GEMELAR',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.IE_SUBTIPO_SANGUINEO,:new.IE_SUBTIPO_SANGUINEO,'IE_SUBTIPO_SANGUINEO',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
              gerar_solicitacao_alteracao(:old.NR_SEQ_PERSON_NAME,:new.NR_SEQ_PERSON_NAME,'NR_SEQ_PERSON_NAME',:new.cd_pessoa_fisica,:new.cd_estabelecimento, :new.nm_usuario);
    elsif    (vl_parametro_w = 'D') then
        select    decode(count(1),0,'N','S')
        into    ie_estipulante_w
        from    pls_contrato
        where    cd_pf_estipulante    = :new.cd_pessoa_fisica
        and    ((dt_rescisao_contrato is null)  or (dt_rescisao_contrato > sysdate));

        select    decode(count(1),0,'N','S')
        into    ie_pagador_w
        from    pls_contrato_pagador
        where    cd_pessoa_fisica    = :new.cd_pessoa_fisica
        and    ((dt_rescisao is null)  or (dt_rescisao > sysdate));

        for r_c01_w in C01 loop
            null;
              gerar_solicitacao_alt_regra(:old.IE_GRAU_INSTRUCAO,:new.IE_GRAU_INSTRUCAO,'IE_GRAU_INSTRUCAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_GRAU_INSTRUCAO := :old.IE_GRAU_INSTRUCAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CEP_CIDADE_NASC,:new.NR_CEP_CIDADE_NASC,'NR_CEP_CIDADE_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CEP_CIDADE_NASC := :old.NR_CEP_CIDADE_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PRONTUARIO,:new.NR_PRONTUARIO,'NR_PRONTUARIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PRONTUARIO := :old.NR_PRONTUARIO;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_RELIGIAO,:new.CD_RELIGIAO,'CD_RELIGIAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_RELIGIAO := :old.CD_RELIGIAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PIS_PASEP,:new.NR_PIS_PASEP,'NR_PIS_PASEP',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PIS_PASEP := :old.NR_PIS_PASEP;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_NACIONALIDADE,:new.CD_NACIONALIDADE,'CD_NACIONALIDADE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_NACIONALIDADE := :old.CD_NACIONALIDADE;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_DEPENDENCIA_SUS,:new.IE_DEPENDENCIA_SUS,'IE_DEPENDENCIA_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_DEPENDENCIA_SUS := :old.IE_DEPENDENCIA_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_ALTURA_CM,:new.QT_ALTURA_CM,'QT_ALTURA_CM',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_ALTURA_CM := :old.QT_ALTURA_CM;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_TIPO_SANGUE,:new.IE_TIPO_SANGUE,'IE_TIPO_SANGUE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_TIPO_SANGUE := :old.IE_TIPO_SANGUE;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FATOR_RH,:new.IE_FATOR_RH,'IE_FATOR_RH',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FATOR_RH := :old.IE_FATOR_RH;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_OBITO,'dd/mm/yyyy'),to_char(:new.DT_OBITO,'dd/mm/yyyy'),'DT_OBITO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_OBITO := :old.DT_OBITO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_ISS,:new.NR_ISS,'NR_ISS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_ISS := :old.NR_ISS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_INSS,:new.NR_INSS,'NR_INSS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_INSS := :old.NR_INSS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CERT_NASC,:new.NR_CERT_NASC,'NR_CERT_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CERT_NASC := :old.NR_CERT_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_CARGO,:new.CD_CARGO,'CD_CARGO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CARGO := :old.CD_CARGO;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_CODIGO_PROF,:new.DS_CODIGO_PROF,'DS_CODIGO_PROF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_CODIGO_PROF := :old.DS_CODIGO_PROF;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FUNCIONARIO,:new.IE_FUNCIONARIO,'IE_FUNCIONARIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FUNCIONARIO := :old.IE_FUNCIONARIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_COR_PELE,:new.NR_SEQ_COR_PELE,'NR_SEQ_COR_PELE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_COR_PELE := :old.NR_SEQ_COR_PELE;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_ORGAO_EMISSOR_CI,:new.DS_ORGAO_EMISSOR_CI,'DS_ORGAO_EMISSOR_CI',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_ORGAO_EMISSOR_CI := :old.DS_ORGAO_EMISSOR_CI;
        end if;

              gerar_solicitacao_alt_regra(:old.CD_CBO_SUS,:new.CD_CBO_SUS,'CD_CBO_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CBO_SUS := :old.CD_CBO_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_ATIVIDADE_SUS,:new.CD_ATIVIDADE_SUS,'CD_ATIVIDADE_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_ATIVIDADE_SUS := :old.CD_ATIVIDADE_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_VINCULO_SUS,:new.IE_VINCULO_SUS,'IE_VINCULO_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_VINCULO_SUS := :old.IE_VINCULO_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_ESTABELECIMENTO,:new.CD_ESTABELECIMENTO,'CD_ESTABELECIMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_ESTABELECIMENTO := :old.CD_ESTABELECIMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_SISTEMA_ANT,:new.CD_SISTEMA_ANT,'CD_SISTEMA_ANT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_SISTEMA_ANT := :old.CD_SISTEMA_ANT;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_FUNCIONARIO,:new.CD_FUNCIONARIO,'CD_FUNCIONARIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_FUNCIONARIO := :old.CD_FUNCIONARIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PAGER_BIP,:new.NR_PAGER_BIP,'NR_PAGER_BIP',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PAGER_BIP := :old.NR_PAGER_BIP;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_TRANSACAO_SUS,:new.NR_TRANSACAO_SUS,'NR_TRANSACAO_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_TRANSACAO_SUS := :old.NR_TRANSACAO_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_MEDICO,:new.CD_MEDICO,'CD_MEDICO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_MEDICO := :old.CD_MEDICO;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_PESSOA_PESQUISA,:new.NM_PESSOA_PESQUISA,'NM_PESSOA_PESQUISA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_PESSOA_PESQUISA := :old.NM_PESSOA_PESQUISA;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_EMISSAO_CI,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CI,'dd/mm/yyyy'),'DT_EMISSAO_CI',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_EMISSAO_CI := :old.DT_EMISSAO_CI;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CONSELHO,:new.NR_SEQ_CONSELHO,'NR_SEQ_CONSELHO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CONSELHO := :old.NR_SEQ_CONSELHO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_ADMISSAO_HOSP,'dd/mm/yyyy'),to_char(:new.DT_ADMISSAO_HOSP,'dd/mm/yyyy'),'DT_ADMISSAO_HOSP',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_ADMISSAO_HOSP := :old.DT_ADMISSAO_HOSP;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FLUENCIA_PORTUGUES,:new.IE_FLUENCIA_PORTUGUES,'IE_FLUENCIA_PORTUGUES',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FLUENCIA_PORTUGUES := :old.IE_FLUENCIA_PORTUGUES;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_TITULO_ELEITOR,:new.NR_TITULO_ELEITOR,'NR_TITULO_ELEITOR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_TITULO_ELEITOR := :old.NR_TITULO_ELEITOR;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_ZONA,:new.NR_ZONA,'NR_ZONA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_ZONA := :old.NR_ZONA;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SECAO,:new.NR_SECAO,'NR_SECAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SECAO := :old.NR_SECAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CARTAO_ESTRANGEIRO,:new.NR_CARTAO_ESTRANGEIRO,'NR_CARTAO_ESTRANGEIRO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CARTAO_ESTRANGEIRO := :old.NR_CARTAO_ESTRANGEIRO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_REG_GERAL_ESTRANG,:new.NR_REG_GERAL_ESTRANG,'NR_REG_GERAL_ESTRANG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_REG_GERAL_ESTRANG := :old.NR_REG_GERAL_ESTRANG;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_CHEGADA_BRASIL,'dd/mm/yyyy'),to_char(:new.DT_CHEGADA_BRASIL,'dd/mm/yyyy'),'DT_CHEGADA_BRASIL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_CHEGADA_BRASIL := :old.DT_CHEGADA_BRASIL;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_USUARIO_ORIGINAL,:new.NM_USUARIO_ORIGINAL,'NM_USUARIO_ORIGINAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_USUARIO_ORIGINAL := :old.NM_USUARIO_ORIGINAL;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy'),to_char(:new.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy'),'DT_CADASTRO_ORIGINAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_CADASTRO_ORIGINAL := :old.DT_CADASTRO_ORIGINAL;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy'),'DT_ATUALIZACAO_NREC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_ATUALIZACAO_NREC := :old.DT_ATUALIZACAO_NREC;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_USUARIO_NREC,:new.NM_USUARIO_NREC,'NM_USUARIO_NREC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_USUARIO_NREC := :old.NM_USUARIO_NREC;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_TIPO_PRONTUARIO,:new.IE_TIPO_PRONTUARIO,'IE_TIPO_PRONTUARIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_TIPO_PRONTUARIO := :old.IE_TIPO_PRONTUARIO;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_DEPENDENTE,:new.QT_DEPENDENTE,'QT_DEPENDENTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_DEPENDENTE := :old.QT_DEPENDENTE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_TRANSPLANTE,:new.NR_TRANSPLANTE,'NR_TRANSPLANTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_TRANSPLANTE := :old.NR_TRANSPLANTE;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_PESSOA_FISICA_SEM_ACENTO,:new.NM_PESSOA_FISICA_SEM_ACENTO,'NM_PESSOA_FISICA_SEM_ACENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_PESSOA_FISICA_SEM_ACENTO := :old.NM_PESSOA_FISICA_SEM_ACENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_EMPRESA,:new.CD_EMPRESA,'CD_EMPRESA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_EMPRESA := :old.CD_EMPRESA;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_REVISAO,'dd/mm/yyyy'),to_char(:new.DT_REVISAO,'dd/mm/yyyy'),'DT_REVISAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_REVISAO := :old.DT_REVISAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_USUARIO_REVISAO,:new.NM_USUARIO_REVISAO,'NM_USUARIO_REVISAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_USUARIO_REVISAO := :old.NM_USUARIO_REVISAO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_DEMISSAO_HOSP,'dd/mm/yyyy'),to_char(:new.DT_DEMISSAO_HOSP,'dd/mm/yyyy'),'DT_DEMISSAO_HOSP',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_DEMISSAO_HOSP := :old.DT_DEMISSAO_HOSP;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CONTRA_REF_SUS,:new.NR_CONTRA_REF_SUS,'NR_CONTRA_REF_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CONTRA_REF_SUS := :old.NR_CONTRA_REF_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_PUERICULTURA,:new.CD_PUERICULTURA,'CD_PUERICULTURA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_PUERICULTURA := :old.CD_PUERICULTURA;
        end if;
              gerar_solicitacao_alt_regra(:old.SG_EMISSORA_CI,:new.SG_EMISSORA_CI,'SG_EMISSORA_CI',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.SG_EMISSORA_CI := :old.SG_EMISSORA_CI;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_NATURALIZACAO_PF,'dd/mm/yyyy'),to_char(:new.DT_NATURALIZACAO_PF,'dd/mm/yyyy'),'DT_NATURALIZACAO_PF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_NATURALIZACAO_PF := :old.DT_NATURALIZACAO_PF;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CTPS,:new.NR_CTPS,'NR_CTPS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CTPS := :old.NR_CTPS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SERIE_CTPS,:new.NR_SERIE_CTPS,'NR_SERIE_CTPS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SERIE_CTPS := :old.NR_SERIE_CTPS;
        end if;
              gerar_solicitacao_alt_regra(:old.UF_EMISSORA_CTPS,:new.UF_EMISSORA_CTPS,'UF_EMISSORA_CTPS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.UF_EMISSORA_CTPS := :old.UF_EMISSORA_CTPS;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_EMISSAO_CTPS,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CTPS,'dd/mm/yyyy'),'DT_EMISSAO_CTPS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_EMISSAO_CTPS := :old.DT_EMISSAO_CTPS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PORTARIA_NAT,:new.NR_PORTARIA_NAT,'NR_PORTARIA_NAT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PORTARIA_NAT := :old.NR_PORTARIA_NAT;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_SENHA,:new.DS_SENHA,'DS_SENHA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_SENHA := :old.DS_SENHA;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_FONETICA,:new.DS_FONETICA,'DS_FONETICA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_FONETICA := :old.DS_FONETICA;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_REVISAR,:new.IE_REVISAR,'IE_REVISAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_REVISAR := :old.IE_REVISAR;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_CNES,:new.CD_CNES,'CD_CNES',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CNES := :old.CD_CNES;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_PERFIL,:new.NR_SEQ_PERFIL,'NR_SEQ_PERFIL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_PERFIL := :old.NR_SEQ_PERFIL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SAME,:new.NR_SAME,'NR_SAME',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SAME := :old.NR_SAME;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CBO_SAUDE,:new.NR_SEQ_CBO_SAUDE,'NR_SEQ_CBO_SAUDE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CBO_SAUDE := :old.NR_SEQ_CBO_SAUDE;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy'),to_char(:new.DT_INTEGRACAO_EXTERNA,'dd/mm/yyyy'),'DT_INTEGRACAO_EXTERNA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_INTEGRACAO_EXTERNA := :old.DT_INTEGRACAO_EXTERNA;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_FONETICA_CNS,:new.DS_FONETICA_CNS,'DS_FONETICA_CNS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_FONETICA_CNS := :old.DS_FONETICA_CNS;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_APELIDO,:new.DS_APELIDO,'DS_APELIDO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_APELIDO := :old.DS_APELIDO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_GERACAO_PRONT,'dd/mm/yyyy'),to_char(:new.DT_GERACAO_PRONT,'dd/mm/yyyy'),'DT_GERACAO_PRONT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_GERACAO_PRONT := :old.DT_GERACAO_PRONT;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_MUNICIPIO_IBGE,:new.CD_MUNICIPIO_IBGE,'CD_MUNICIPIO_IBGE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_MUNICIPIO_IBGE := :old.CD_MUNICIPIO_IBGE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_PAIS,:new.NR_SEQ_PAIS,'NR_SEQ_PAIS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_PAIS := :old.NR_SEQ_PAIS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CERT_CASAMENTO,:new.NR_CERT_CASAMENTO,'NR_CERT_CASAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CERT_CASAMENTO := :old.NR_CERT_CASAMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CARTORIO_NASC,:new.NR_SEQ_CARTORIO_NASC,'NR_SEQ_CARTORIO_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CARTORIO_NASC := :old.NR_SEQ_CARTORIO_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CARTORIO_CASAMENTO,:new.NR_SEQ_CARTORIO_CASAMENTO,'NR_SEQ_CARTORIO_CASAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CARTORIO_CASAMENTO := :old.NR_SEQ_CARTORIO_CASAMENTO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy'),'DT_EMISSAO_CERT_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_EMISSAO_CERT_NASC := :old.DT_EMISSAO_CERT_NASC;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy'),'DT_EMISSAO_CERT_CASAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_EMISSAO_CERT_CASAMENTO := :old.DT_EMISSAO_CERT_CASAMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_FOLHA_CERT_NASC,:new.NR_FOLHA_CERT_NASC,'NR_FOLHA_CERT_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_FOLHA_CERT_NASC := :old.NR_FOLHA_CERT_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_FOLHA_CERT_CASAMENTO,:new.NR_FOLHA_CERT_CASAMENTO,'NR_FOLHA_CERT_CASAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_FOLHA_CERT_CASAMENTO := :old.NR_FOLHA_CERT_CASAMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_LIVRO_CERT_NASC,:new.NR_LIVRO_CERT_NASC,'NR_LIVRO_CERT_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_LIVRO_CERT_NASC := :old.NR_LIVRO_CERT_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_LIVRO_CERT_CASAMENTO,:new.NR_LIVRO_CERT_CASAMENTO,'NR_LIVRO_CERT_CASAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_LIVRO_CERT_CASAMENTO := :old.NR_LIVRO_CERT_CASAMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_NUT_PERFIL,:new.NR_SEQ_NUT_PERFIL,'NR_SEQ_NUT_PERFIL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_NUT_PERFIL := :old.NR_SEQ_NUT_PERFIL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_REGISTRO_PLS,:new.NR_REGISTRO_PLS,'NR_REGISTRO_PLS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_REGISTRO_PLS := :old.NR_REGISTRO_PLS;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_VALIDADE_RG,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_RG,'dd/mm/yyyy'),'DT_VALIDADE_RG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_VALIDADE_RG := :old.DT_VALIDADE_RG;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_PESO_NASC,:new.QT_PESO_NASC,'QT_PESO_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_PESO_NASC := :old.QT_PESO_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.UF_CONSELHO,:new.UF_CONSELHO,'UF_CONSELHO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.UF_CONSELHO := :old.UF_CONSELHO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_STATUS_EXPORTAR,:new.IE_STATUS_EXPORTAR,'IE_STATUS_EXPORTAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_STATUS_EXPORTAR := :old.IE_STATUS_EXPORTAR;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_ENDERECO_CORRESPONDENCIA,:new.IE_ENDERECO_CORRESPONDENCIA,'IE_ENDERECO_CORRESPONDENCIA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_ENDERECO_CORRESPONDENCIA := :old.IE_ENDERECO_CORRESPONDENCIA;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PRONT_DV,:new.NR_PRONT_DV,'NR_PRONT_DV',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PRONT_DV := :old.NR_PRONT_DV;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_ABREVIADO,:new.NM_ABREVIADO,'NM_ABREVIADO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_ABREVIADO := :old.NM_ABREVIADO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_DDD_CELULAR,:new.NR_DDD_CELULAR,'NR_DDD_CELULAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_DDD_CELULAR := :old.NR_DDD_CELULAR;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_DDI_CELULAR,:new.NR_DDI_CELULAR,'NR_DDI_CELULAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_DDI_CELULAR := :old.NR_DDI_CELULAR;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FREQUENTA_ESCOLA,:new.IE_FREQUENTA_ESCOLA,'IE_FREQUENTA_ESCOLA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FREQUENTA_ESCOLA := :old.IE_FREQUENTA_ESCOLA;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CCM,:new.NR_CCM,'NR_CCM',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CCM := :old.NR_CCM;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_FIM_EXPERIENCIA,'dd/mm/yyyy'),to_char(:new.DT_FIM_EXPERIENCIA,'dd/mm/yyyy'),'DT_FIM_EXPERIENCIA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_FIM_EXPERIENCIA := :old.DT_FIM_EXPERIENCIA;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_VALIDADE_CONSELHO,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_CONSELHO,'dd/mm/yyyy'),'DT_VALIDADE_CONSELHO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_VALIDADE_CONSELHO := :old.DT_VALIDADE_CONSELHO;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_PROFISSAO,:new.DS_PROFISSAO,'DS_PROFISSAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_PROFISSAO := :old.DS_PROFISSAO;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_EMPRESA_PF,:new.DS_EMPRESA_PF,'DS_EMPRESA_PF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_EMPRESA_PF := :old.DS_EMPRESA_PF;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_NF_CORREIO,:new.IE_NF_CORREIO,'IE_NF_CORREIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_NF_CORREIO := :old.IE_NF_CORREIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PASSAPORTE,:new.NR_PASSAPORTE,'NR_PASSAPORTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PASSAPORTE := :old.NR_PASSAPORTE;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_TIPO_PJ,:new.CD_TIPO_PJ,'CD_TIPO_PJ',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_TIPO_PJ := :old.CD_TIPO_PJ;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CNH,:new.NR_CNH,'NR_CNH',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CNH := :old.NR_CNH;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CERT_MILITAR,:new.NR_CERT_MILITAR,'NR_CERT_MILITAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CERT_MILITAR := :old.NR_CERT_MILITAR;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_PERFIL_ATIVO,:new.CD_PERFIL_ATIVO,'CD_PERFIL_ATIVO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_PERFIL_ATIVO := :old.CD_PERFIL_ATIVO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_PRONT_EXT,:new.NR_PRONT_EXT,'NR_PRONT_EXT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_PRONT_EXT := :old.NR_PRONT_EXT;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_TIPO_DEFINITIVO_PROVISORIO,:new.IE_TIPO_DEFINITIVO_PROVISORIO,'IE_TIPO_DEFINITIVO_PROVISORIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_TIPO_DEFINITIVO_PROVISORIO := :old.IE_TIPO_DEFINITIVO_PROVISORIO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_TIPO_PESSOA,:new.IE_TIPO_PESSOA,'IE_TIPO_PESSOA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_TIPO_PESSOA := :old.IE_TIPO_PESSOA;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_PESSOA_FISICA,:new.NM_PESSOA_FISICA,'NM_PESSOA_FISICA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_PESSOA_FISICA := :old.NM_PESSOA_FISICA;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy'),'DT_NASCIMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_NASCIMENTO := :old.DT_NASCIMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_SEXO,:new.IE_SEXO,'IE_SEXO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_SEXO := :old.IE_SEXO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_ESTADO_CIVIL,:new.IE_ESTADO_CIVIL,'IE_ESTADO_CIVIL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_ESTADO_CIVIL := :old.IE_ESTADO_CIVIL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CPF,:new.NR_CPF,'NR_CPF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CPF := :old.NR_CPF;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_IDENTIDADE,:new.NR_IDENTIDADE,'NR_IDENTIDADE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_IDENTIDADE := :old.NR_IDENTIDADE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_TELEFONE_CELULAR,:new.NR_TELEFONE_CELULAR,'NR_TELEFONE_CELULAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_TELEFONE_CELULAR := :old.NR_TELEFONE_CELULAR;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy'),to_char(:new.DT_INICIO_OCUP_ATUAL,'dd/mm/yyyy'),'DT_INICIO_OCUP_ATUAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_INICIO_OCUP_ATUAL := :old.DT_INICIO_OCUP_ATUAL;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_ESCOLARIDADE_CNS,:new.IE_ESCOLARIDADE_CNS,'IE_ESCOLARIDADE_CNS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_ESCOLARIDADE_CNS := :old.IE_ESCOLARIDADE_CNS;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_SITUACAO_CONJ_CNS,:new.IE_SITUACAO_CONJ_CNS,'IE_SITUACAO_CONJ_CNS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_SITUACAO_CONJ_CNS := :old.IE_SITUACAO_CONJ_CNS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CERT_DIVORCIO,:new.NR_CERT_DIVORCIO,'NR_CERT_DIVORCIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CERT_DIVORCIO := :old.NR_CERT_DIVORCIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CARTORIO_DIVORCIO,:new.NR_SEQ_CARTORIO_DIVORCIO,'NR_SEQ_CARTORIO_DIVORCIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CARTORIO_DIVORCIO := :old.NR_SEQ_CARTORIO_DIVORCIO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy'),to_char(:new.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy'),'DT_EMISSAO_CERT_DIVORCIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_EMISSAO_CERT_DIVORCIO := :old.DT_EMISSAO_CERT_DIVORCIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_LIVRO_CERT_DIVORCIO,:new.NR_LIVRO_CERT_DIVORCIO,'NR_LIVRO_CERT_DIVORCIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_LIVRO_CERT_DIVORCIO := :old.NR_LIVRO_CERT_DIVORCIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_FOLHA_CERT_DIV,:new.NR_FOLHA_CERT_DIV,'NR_FOLHA_CERT_DIV',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_FOLHA_CERT_DIV := :old.NR_FOLHA_CERT_DIV;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_ALTA_INSTITUCIONAL,'dd/mm/yyyy'),to_char(:new.DT_ALTA_INSTITUCIONAL,'dd/mm/yyyy'),'DT_ALTA_INSTITUCIONAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_ALTA_INSTITUCIONAL := :old.DT_ALTA_INSTITUCIONAL;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_TRANSPLANTE,'dd/mm/yyyy'),to_char(:new.DT_TRANSPLANTE,'dd/mm/yyyy'),'DT_TRANSPLANTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_TRANSPLANTE := :old.DT_TRANSPLANTE;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_FAMILIA,:new.CD_FAMILIA,'CD_FAMILIA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_FAMILIA := :old.CD_FAMILIA;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_MATRICULA_NASC,:new.NR_MATRICULA_NASC,'NR_MATRICULA_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_MATRICULA_NASC := :old.NR_MATRICULA_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_DOADOR,:new.IE_DOADOR,'IE_DOADOR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_DOADOR := :old.IE_DOADOR;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_VENCIMENTO_CNH,'dd/mm/yyyy'),to_char(:new.DT_VENCIMENTO_CNH,'dd/mm/yyyy'),'DT_VENCIMENTO_CNH',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_VENCIMENTO_CNH := :old.DT_VENCIMENTO_CNH;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_CATEGORIA_CNH,:new.DS_CATEGORIA_CNH,'DS_CATEGORIA_CNH',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_CATEGORIA_CNH := :old.DS_CATEGORIA_CNH;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_PESSOA_MAE,:new.CD_PESSOA_MAE,'CD_PESSOA_MAE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_PESSOA_MAE := :old.CD_PESSOA_MAE;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_PRIMEIRA_ADMISSAO,'dd/mm/yyyy'),to_char(:new.DT_PRIMEIRA_ADMISSAO,'dd/mm/yyyy'),'DT_PRIMEIRA_ADMISSAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_PRIMEIRA_ADMISSAO := :old.DT_PRIMEIRA_ADMISSAO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_FIM_PRORROGACAO,'dd/mm/yyyy'),to_char(:new.DT_FIM_PRORROGACAO,'dd/mm/yyyy'),'DT_FIM_PRORROGACAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_FIM_PRORROGACAO := :old.DT_FIM_PRORROGACAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CERTIDAO_OBITO,:new.NR_CERTIDAO_OBITO,'NR_CERTIDAO_OBITO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CERTIDAO_OBITO := :old.NR_CERTIDAO_OBITO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_ETNIA,:new.NR_SEQ_ETNIA,'NR_SEQ_ETNIA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_ETNIA := :old.NR_SEQ_ETNIA;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CARTAO_NAC_SUS,:new.NR_CARTAO_NAC_SUS,'NR_CARTAO_NAC_SUS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CARTAO_NAC_SUS := :old.NR_CARTAO_NAC_SUS;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_PESO,:new.QT_PESO,'QT_PESO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_PESO := :old.QT_PESO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_EMANCIPADO,:new.IE_EMANCIPADO,'IE_EMANCIPADO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_EMANCIPADO := :old.IE_EMANCIPADO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_VINCULO_PROFISSIONAL,:new.IE_VINCULO_PROFISSIONAL,'IE_VINCULO_PROFISSIONAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_VINCULO_PROFISSIONAL := :old.IE_VINCULO_PROFISSIONAL;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_NIT,:new.CD_NIT,'CD_NIT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_NIT := :old.CD_NIT;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_DEPENDENTE,:new.IE_DEPENDENTE,'IE_DEPENDENTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_DEPENDENTE := :old.IE_DEPENDENTE;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_EMAIL_CCIH,:new.DS_EMAIL_CCIH,'DS_EMAIL_CCIH',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_EMAIL_CCIH := :old.DS_EMAIL_CCIH;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_CGC_ORIG_TRANSPL,:new.CD_CGC_ORIG_TRANSPL,'CD_CGC_ORIG_TRANSPL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CGC_ORIG_TRANSPL := :old.CD_CGC_ORIG_TRANSPL;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_AFASTAMENTO,'dd/mm/yyyy'),to_char(:new.DT_AFASTAMENTO,'dd/mm/yyyy'),'DT_AFASTAMENTO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_AFASTAMENTO := :old.DT_AFASTAMENTO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_TRATAMENTO_PSIQUIATRICO,:new.IE_TRATAMENTO_PSIQUIATRICO,'IE_TRATAMENTO_PSIQUIATRICO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_TRATAMENTO_PSIQUIATRICO := :old.IE_TRATAMENTO_PSIQUIATRICO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_TIPO_BENEFICIO,:new.NR_SEQ_TIPO_BENEFICIO,'NR_SEQ_TIPO_BENEFICIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_TIPO_BENEFICIO := :old.NR_SEQ_TIPO_BENEFICIO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_TIPO_INCAPACIDADE,:new.NR_SEQ_TIPO_INCAPACIDADE,'NR_SEQ_TIPO_INCAPACIDADE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_TIPO_INCAPACIDADE := :old.NR_SEQ_TIPO_INCAPACIDADE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_AGENCIA_INSS,:new.NR_SEQ_AGENCIA_INSS,'NR_SEQ_AGENCIA_INSS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_AGENCIA_INSS := :old.NR_SEQ_AGENCIA_INSS;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_DIAS_IG,:new.QT_DIAS_IG,'QT_DIAS_IG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_DIAS_IG := :old.QT_DIAS_IG;
        end if;
              gerar_solicitacao_alt_regra(:old.QT_SEMANAS_IG,:new.QT_SEMANAS_IG,'QT_SEMANAS_IG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.QT_SEMANAS_IG := :old.QT_SEMANAS_IG;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_REGRA_IG,:new.IE_REGRA_IG,'IE_REGRA_IG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_REGRA_IG := :old.IE_REGRA_IG;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_NASCIMENTO_IG,'dd/mm/yyyy'),to_char(:new.DT_NASCIMENTO_IG,'dd/mm/yyyy'),'DT_NASCIMENTO_IG',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_NASCIMENTO_IG := :old.DT_NASCIMENTO_IG;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_STATUS_USUARIO_EVENT,:new.IE_STATUS_USUARIO_EVENT,'IE_STATUS_USUARIO_EVENT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_STATUS_USUARIO_EVENT := :old.IE_STATUS_USUARIO_EVENT;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_CID_DIRETA,:new.CD_CID_DIRETA,'CD_CID_DIRETA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CID_DIRETA := :old.CD_CID_DIRETA;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_COREN,:new.IE_COREN,'IE_COREN',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_COREN := :old.IE_COREN;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_VALIDADE_COREN,'dd/mm/yyyy'),to_char(:new.DT_VALIDADE_COREN,'dd/mm/yyyy'),'DT_VALIDADE_COREN',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_VALIDADE_COREN := :old.DT_VALIDADE_COREN;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_SOCIAL,:new.NM_SOCIAL,'NM_SOCIAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_SOCIAL := :old.NM_SOCIAL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_COR_OLHO,:new.NR_SEQ_COR_OLHO,'NR_SEQ_COR_OLHO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_COR_OLHO := :old.NR_SEQ_COR_OLHO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_COR_CABELO,:new.NR_SEQ_COR_CABELO,'NR_SEQ_COR_CABELO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_COR_CABELO := :old.NR_SEQ_COR_CABELO;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_CAD_SISTEMA_ANT,'dd/mm/yyyy'),to_char(:new.DT_CAD_SISTEMA_ANT,'dd/mm/yyyy'),'DT_CAD_SISTEMA_ANT',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_CAD_SISTEMA_ANT := :old.DT_CAD_SISTEMA_ANT;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_PERM_SMS_EMAIL,:new.IE_PERM_SMS_EMAIL,'IE_PERM_SMS_EMAIL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_PERM_SMS_EMAIL := :old.IE_PERM_SMS_EMAIL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CLASSIF_PAC_AGE,:new.NR_SEQ_CLASSIF_PAC_AGE,'NR_SEQ_CLASSIF_PAC_AGE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CLASSIF_PAC_AGE := :old.NR_SEQ_CLASSIF_PAC_AGE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_INSCRICAO_ESTADUAL,:new.NR_INSCRICAO_ESTADUAL,'NR_INSCRICAO_ESTADUAL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_INSCRICAO_ESTADUAL := :old.NR_INSCRICAO_ESTADUAL;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_SOCIO,:new.IE_SOCIO,'IE_SOCIO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_SOCIO := :old.IE_SOCIO;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_ULT_PROFISSAO,:new.CD_ULT_PROFISSAO,'CD_ULT_PROFISSAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_ULT_PROFISSAO := :old.CD_ULT_PROFISSAO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_VEGETARIANO,:new.IE_VEGETARIANO,'IE_VEGETARIANO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_VEGETARIANO := :old.IE_VEGETARIANO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_CONSELHEIRO,:new.IE_CONSELHEIRO,'IE_CONSELHEIRO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_CONSELHEIRO := :old.IE_CONSELHEIRO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FUMANTE,:new.IE_FUMANTE,'IE_FUMANTE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FUMANTE := :old.IE_FUMANTE;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CODIGO_SERV_PREST,:new.NR_CODIGO_SERV_PREST,'NR_CODIGO_SERV_PREST',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CODIGO_SERV_PREST := :old.NR_CODIGO_SERV_PREST;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_ADOCAO,'dd/mm/yyyy'),to_char(:new.DT_ADOCAO,'dd/mm/yyyy'),'DT_ADOCAO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_ADOCAO := :old.DT_ADOCAO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_CELULAR_NUMEROS,:new.NR_CELULAR_NUMEROS,'NR_CELULAR_NUMEROS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_CELULAR_NUMEROS := :old.NR_CELULAR_NUMEROS;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_LAUDO_ANAT_PATOL,:new.DS_LAUDO_ANAT_PATOL,'DS_LAUDO_ANAT_PATOL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_LAUDO_ANAT_PATOL := :old.DS_LAUDO_ANAT_PATOL;
        end if;
              gerar_solicitacao_alt_regra(to_char(:old.DT_LAUDO_ANAT_PATOL,'dd/mm/yyyy'),to_char(:new.DT_LAUDO_ANAT_PATOL,'dd/mm/yyyy'),'DT_LAUDO_ANAT_PATOL',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DT_LAUDO_ANAT_PATOL := :old.DT_LAUDO_ANAT_PATOL;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_RIC,:new.NR_RIC,'NR_RIC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_RIC := :old.NR_RIC;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_CONSISTE_NR_SERIE_NF,:new.IE_CONSISTE_NR_SERIE_NF,'IE_CONSISTE_NR_SERIE_NF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_CONSISTE_NR_SERIE_NF := :old.IE_CONSISTE_NR_SERIE_NF;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_RGA,:new.NR_RGA,'NR_RGA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_RGA := :old.NR_RGA;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_BARRAS_PESSOA,:new.CD_BARRAS_PESSOA,'CD_BARRAS_PESSOA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_BARRAS_PESSOA := :old.CD_BARRAS_PESSOA;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_PESSOA_CROSS,:new.CD_PESSOA_CROSS,'CD_PESSOA_CROSS',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_PESSOA_CROSS := :old.CD_PESSOA_CROSS;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_FUNCAO_PF,:new.NR_SEQ_FUNCAO_PF,'NR_SEQ_FUNCAO_PF',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_FUNCAO_PF := :old.NR_SEQ_FUNCAO_PF;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_USUARIO_PRINC_CI,:new.NM_USUARIO_PRINC_CI,'NM_USUARIO_PRINC_CI',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_USUARIO_PRINC_CI := :old.NM_USUARIO_PRINC_CI;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_TURNO_TRABALHO,:new.NR_SEQ_TURNO_TRABALHO,'NR_SEQ_TURNO_TRABALHO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_TURNO_TRABALHO := :old.NR_SEQ_TURNO_TRABALHO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_CHEFIA,:new.NR_SEQ_CHEFIA,'NR_SEQ_CHEFIA',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_CHEFIA := :old.NR_SEQ_CHEFIA;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_DECLARACAO_NASC_VIVO,:new.CD_DECLARACAO_NASC_VIVO,'CD_DECLARACAO_NASC_VIVO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_DECLARACAO_NASC_VIVO := :old.CD_DECLARACAO_NASC_VIVO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_TERMO_CERT_NASC,:new.NR_TERMO_CERT_NASC,'NR_TERMO_CERT_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_TERMO_CERT_NASC := :old.NR_TERMO_CERT_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_CURP,:new.CD_CURP,'CD_CURP',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_CURP := :old.CD_CURP;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_RFC,:new.CD_RFC,'CD_RFC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_RFC := :old.CD_RFC;
        end if;
              gerar_solicitacao_alt_regra(:old.SG_ESTADO_NASC,:new.SG_ESTADO_NASC,'SG_ESTADO_NASC',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.SG_ESTADO_NASC := :old.SG_ESTADO_NASC;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_FORNECEDOR,:new.IE_FORNECEDOR,'IE_FORNECEDOR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_FORNECEDOR := :old.IE_FORNECEDOR;
        end if;
              gerar_solicitacao_alt_regra(:old.CD_IFE,:new.CD_IFE,'CD_IFE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.CD_IFE := :old.CD_IFE;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_PRIMEIRO_NOME,:new.NM_PRIMEIRO_NOME,'NM_PRIMEIRO_NOME',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_PRIMEIRO_NOME := :old.NM_PRIMEIRO_NOME;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_SOBRENOME_MAE,:new.NM_SOBRENOME_MAE,'NM_SOBRENOME_MAE',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_SOBRENOME_MAE := :old.NM_SOBRENOME_MAE;
        end if;
              gerar_solicitacao_alt_regra(:old.NM_SOBRENOME_PAI,:new.NM_SOBRENOME_PAI,'NM_SOBRENOME_PAI',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NM_SOBRENOME_PAI := :old.NM_SOBRENOME_PAI;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_RH_FRACO,:new.IE_RH_FRACO,'IE_RH_FRACO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_RH_FRACO := :old.IE_RH_FRACO;
        end if;
              gerar_solicitacao_alt_regra(:old.DS_MUNICIPIO_NASC_ESTRANGEIRO,:new.DS_MUNICIPIO_NASC_ESTRANGEIRO,'DS_MUNICIPIO_NASC_ESTRANGEIRO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.DS_MUNICIPIO_NASC_ESTRANGEIRO := :old.DS_MUNICIPIO_NASC_ESTRANGEIRO;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_GEMELAR,:new.IE_GEMELAR,'IE_GEMELAR',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_GEMELAR := :old.IE_GEMELAR;
        end if;
              gerar_solicitacao_alt_regra(:old.IE_SUBTIPO_SANGUINEO,:new.IE_SUBTIPO_SANGUINEO,'IE_SUBTIPO_SANGUINEO',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.IE_SUBTIPO_SANGUINEO := :old.IE_SUBTIPO_SANGUINEO;
        end if;
              gerar_solicitacao_alt_regra(:old.NR_SEQ_PERSON_NAME,:new.NR_SEQ_PERSON_NAME,'NR_SEQ_PERSON_NAME',:new.cd_pessoa_fisica,'PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,null, r_c01_w.ie_tipo_segurado,

                                ie_estipulante_w,ie_pagador_w,:new.cd_estabelecimento, :new.nm_usuario,ie_alterar_w);
        if    (ie_alterar_w    = 'S') then
            :new.NR_SEQ_PERSON_NAME := :old.NR_SEQ_PERSON_NAME;
        end if;

        end loop;
    end if;
if    (vl_parametro_w <> 'N') then
    select    count(1)
    into    qt_registros_w
    from    pessoa_fisica_solic_alt
    where    cd_pessoa_fisica    = :new.cd_pessoa_fisica;
    if    (qt_registros_w > 0) then
        tasy_gerar_solicitacao(:new.cd_pessoa_fisica);
        if    (vl_parametro_w = 'S') then
            wheb_mensagem_pck.exibir_mensagem_abort(231273);
        end if;
        pls_usuario_pck.set_ie_lancar_mensagem_alt('S');
    end if;
end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_update_hl7
after update ON TASY.PESSOA_FISICA for each row
declare
ds_sep_bv_w				varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
ie_integrado_w			varchar2(1);
ie_existe_atend_w		number(10);
qt_atendimento_w		number(10);
ie_medico_w				varchar2(1);
ds_guampa_w		varchar2(10)	:= chr(39);
ie_condition_w varchar2(1) := 'N';

	PROCEDURE INTEGRA_BIFROST IS
	  JSON_ADT_A08_W			PHILIPS_JSON;
	  DS_EVENT_W				VARCHAR2(100) := 'patient.changeInformation_ADT_A08';
	  DS_MESSAGE_RESPONSE_W	CLOB;
	BEGIN
	  JSON_ADT_A08_W := PHILIPS_JSON();
	  JSON_ADT_A08_W.PUT('CD_PESSOA_FISICA', :NEW.CD_PESSOA_FISICA);
	  JSON_ADT_A08_W.PUT('NM_PESSOA_FISICA', :NEW.NM_PESSOA_FISICA);
	  JSON_ADT_A08_W.PUT('DT_NASCIMENTO',	:NEW.DT_NASCIMENTO);
	  JSON_ADT_A08_W.PUT('IE_SEXO', :NEW.IE_SEXO);

	  DBMS_LOB.CREATETEMPORARY(DS_MESSAGE_RESPONSE_W, TRUE);
	  JSON_ADT_A08_W.TO_CLOB(DS_MESSAGE_RESPONSE_W);

	  DS_MESSAGE_RESPONSE_W := BIFROST.SEND_INTEGRATION_CONTENT(DS_EVENT_W,
																DS_MESSAGE_RESPONSE_W,
																OBTER_USUARIO_ATIVO);
	END;

  PROCEDURE HL7_ADT_47(CD_PESSOA_FISICA_P PESSOA_FISICA.CD_PESSOA_FISICA%TYPE,
                       NR_PRONT_EXT_OLD_P PESSOA_FISICA.NR_PRONT_EXT%TYPE,
                       NM_PESSOA_FISICA_OLD_P PESSOA_FISICA.NM_PESSOA_FISICA%TYPE) IS
    ie_use_integration_w varchar2(10);
    ds_comando_w         varchar2(4000);
    ds_guampa_w          varchar2(10) := chr(39);
    jobno                number;

  begin
    obter_param_usuario(9041,
                        10,
                        obter_perfil_ativo,
                        :NEW.NM_USUARIO,
                        obter_estabelecimento_ativo,
                        ie_use_integration_w);

    if (ie_use_integration_w = 'S') then
      begin
        ds_comando_w	:= ' declare '||CHR(10)||
                         ' ds_result_w clob; '||CHR(10)||
                         ' ds_params_w clob; '||CHR(10)||
                         ' begin '||CHR(10)||
                         ' ds_params_w := person_json_pck.get_patient_message_clob('||CD_PESSOA_FISICA_P||');'|| CHR(10)||
                         ' ds_params_w := REPLACE(ds_params_w,'
                          ||CHR(39)||'"externalAccountNumber"'||CHR(39)
                          ||','||CHR(39)||'"externalMedicalRecordIdOld":"'
                          || NR_PRONT_EXT_OLD_P||'"'
                          ||',"patientNameOld":"'
                          || NM_PESSOA_FISICA_OLD_P||'"'
                          ||',"externalAccountNumber"'|| CHR(39)||');'||CHR(10)||

                    ' if (ds_params_w is not null) then '||
                    ' wheb_usuario_pck.set_cd_estabelecimento('||obter_estabelecimento_ativo||');' ||
                    ' wheb_usuario_pck.set_nm_usuario('||CHR(39)||:NEW.NM_USUARIO||CHR(39)||');' ||
                    ' ds_result_w := bifrost.send_integration_content('
                    ||CHR(39)||'patient.identifier.update'||CHR(39)
                    ||',ds_params_w,'
                    ||CHR(39)||:NEW.NM_USUARIO||CHR(39)||','
                    ||CHR(39)||'N'||CHR(39)||');'||
                    ' end if; '||
                    ' end;';

        dbms_job.submit(jobno, ds_comando_w);
      end;
    end if;

  end HL7_ADT_47;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

	ds_sep_bv_w := obter_separador_bv;

	if	(:old.dt_obito is null)
	and	(:new.dt_obito is null) then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
					'nr_atendimento=0'  || ds_sep_bv_w ||
					'nr_seq_interno=0'  || ds_sep_bv_w;
		gravar_agend_integracao(18, ds_param_integ_hl7_w);
		end;
	end if;

	if	(nvl(:old.nm_pessoa_fisica,'X') <> :new.nm_pessoa_fisica ) or
		(nvl(:old.ie_sexo,'X') 	<> :new.ie_sexo) 	or
		(:old.dt_nascimento 	<> :new.dt_nascimento) 	or
		(nvl(:old.nr_identidade,'X') 	<> :new.nr_identidade) 	or
		((:old.dt_obito is null) and(:new.dt_obito is null)) then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
		gravar_agend_integracao(145, ds_param_integ_hl7_w);

		Mirth_ADT_A08_atualizaao_pac(:new.cd_pessoa_fisica);

		end;
	end if;

	if	(nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) then

		select	nvl(max('S'),'N')
		into	ie_integrado_w
		from	pf_codigo_externo
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and	IE_TIPO_CODIGO_EXTERNO = 'DI';

		if	(ie_integrado_w = 'S') then

			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
			gravar_agend_integracao(191, ds_param_integ_hl7_w);

		end if;

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
		gravar_agend_integracao(214, ds_param_integ_hl7_w);

	end if;

	if	(nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) or
		(nvl(:old.dt_obito,sysdate) <> nvl(:new.dt_obito,sysdate)) then

		ger_pessoa_fisica_update(:new.cd_pessoa_fisica);

	end if;

	--HCOR ADT A08

   if (wheb_usuario_pck.is_evento_ativo(783) = 'S') then

      qt_atendimento_w  := OBTER_QTD_ATENDIMENTOS(:new.cd_pessoa_fisica);

      if (qt_atendimento_w > 0) then
         ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
         gravar_agend_integracao(783, ds_param_integ_hl7_w);
      end if;

   end if;

	if (wheb_usuario_pck.is_evento_ativo(853) = 'S') and
		((nvl(:old.nm_pessoa_fisica,'X') <> nvl(:new.nm_pessoa_fisica,'X')) or
		(nvl(:old.ie_sexo,'X') <> nvl(:new.ie_sexo,'X')) or
		(nvl(:old.dt_nascimento,sysdate) <> nvl(:new.dt_nascimento,sysdate)) or
		(nvl(:old.nr_seq_cor_pele,0) <> nvl(:new.nr_seq_cor_pele,0)) or
		(nvl(:old.ie_estado_civil,0) <> nvl(:new.ie_estado_civil,0)) or
		(nvl(:old.cd_religiao,0) <> nvl(:new.cd_religiao,0)) or
		(nvl(:old.nr_prontuario,0) <> nvl(:new.nr_prontuario,0)) or
		(nvl(:old.nr_cpf,'X') <> nvl(:new.nr_cpf,'X')) or
		(nvl(:old.nr_telefone_celular,'X') <> nvl(:new.nr_telefone_celular,'X')) or
		(nvl(:old.nr_ddd_celular,'X') <> nvl(:new.nr_ddd_celular,'X')) or
		(nvl(:old.nr_ddi_celular,'X') <> nvl(:new.nr_ddi_celular,'X')) or
		(nvl(:old.dt_obito,sysdate) <> nvl(:new.dt_obito,sysdate))) then

		enviar_pf_medico_hl7(:new.cd_pessoa_fisica,'MUP');

		INTEGRA_BIFROST;
	end if;
	if ((nvl(:old.IE_TIPO_SANGUE, 'X') <> nvl(:new.IE_TIPO_SANGUE, 'X')) or
       (nvl(:old.IE_FATOR_RH, 'X') <> nvl(:new.IE_FATOR_RH, 'X')) or
       (nvl(:old.QT_PESO, 0) <> nvl(:new.QT_PESO, 0)) or
       (nvl(:old.QT_ALTURA_CM, 0) <> nvl(:new.QT_ALTURA_CM, 0))) then
		call_interface_file(927, 'itech_pck.send_patient_attribute_info('||ds_guampa_w|| :new.cd_pessoa_fisica ||ds_guampa_w||','||ds_guampa_w|| :new.nm_usuario ||ds_guampa_w||','||0||','||ds_guampa_w||null||ds_guampa_w||','||obter_estabelecimento_ativo||');', :new.nm_usuario);

		call_interface_file(933, 'carestream_ris_japan_l10n_pck.patient_change_info( ' || :new.cd_pessoa_fisica || ', ''' || :new.nm_usuario || ''' , 0 , null, ' || obter_estabelecimento_ativo || ');' , :new.nm_usuario);
	end if;
	call_bifrost_content('patient.registration.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);

  begin
    -- INICIO ADT_A47
    IF (:NEW.NR_PRONT_EXT <> :OLD.NR_PRONT_EXT) THEN
      HL7_ADT_47(:OLD.CD_PESSOA_FISICA, :OLD.NR_PRONT_EXT, :OLD.NM_PESSOA_FISICA);
    END IF;
    -- FIM ADT_A47*/
  end;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.NATURAL_PERSON_TIE
    AFTER INSERT ON TASY.PESSOA_FISICA     FOR EACH ROW
DECLARE
    JSON_W          PHILIPS_JSON;
    JSON_DATOS_BASICOS_W            PHILIPS_JSON;
    JSON_DATOS_ADICIONALES_W            PHILIPS_JSON;
    JSON_EDIT_FORM_GROUP_2_W            PHILIPS_JSON;
    JSON_DATOS_CONTACTO_W           PHILIPS_JSON;
    JSON_DATOS_CONTACTO_LIST_W          PHILIPS_JSON_LIST;
    JSON_DIREC_PERSONA_W            PHILIPS_JSON;
    JSON_DIREC_PERSONA_LIST_W           PHILIPS_JSON_LIST;
    JSON_DATA_W    CLOB;
    CD_SEXO         NUMBER(1);
    DS_RETORNO_INTEGRACAO_W  VARCHAR2(4000);
BEGIN
    if    nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_AR' then

    JSON_W    := PHILIPS_JSON();
    JSON_DATOS_BASICOS_W := PHILIPS_JSON();
    JSON_DATOS_ADICIONALES_W := PHILIPS_JSON();
    JSON_EDIT_FORM_GROUP_2_W := PHILIPS_JSON();
    JSON_DATOS_CONTACTO_W := PHILIPS_JSON();
    JSON_DIREC_PERSONA_W := PHILIPS_JSON();
    JSON_DATOS_CONTACTO_LIST_W := PHILIPS_JSON_LIST();
    JSON_DIREC_PERSONA_LIST_W := PHILIPS_JSON_LIST();

    JSON_DATOS_BASICOS_W.PUT('id', :NEW.CD_PESSOA_FISICA);
    JSON_DATOS_BASICOS_W.PUT('nombre', obter_parte_nome(:NEW.NM_PESSOA_FISICA,'Nome'));
    JSON_DATOS_BASICOS_W.PUT('apellido', obter_parte_nome(:NEW.NM_PESSOA_FISICA,'RestoNome'));
    JSON_DATOS_BASICOS_W.PUT('fechaNacimiento', nvl(to_char(:NEW.DT_NASCIMENTO, 'yyyy-mm-dd'), to_char(SYSDATE, 'yyyy-mm-dd')));

    if (:NEW.IE_SEXO = 'M') then
        CD_SEXO := 1;
    elsif (:NEW.IE_SEXO = 'F') then
        CD_SEXO := 2;
    else
        CD_SEXO := 3;
    end if;

    JSON_DATOS_BASICOS_W.PUT('sexo', CD_SEXO);
    JSON_DATOS_BASICOS_W.PUT('tipoDoc', 7);
    JSON_DATOS_BASICOS_W.PUT('documento', :NEW.NR_IDENTIDADE);
    JSON_DATOS_BASICOS_W.PUT('emailPrincipal', '');
    JSON_DATOS_BASICOS_W.PUT('telefonoPrincipal', :NEW.NR_TELEFONE_CELULAR);
    JSON_DATOS_BASICOS_W.PUT('terminos', true);

    JSON_DATOS_CONTACTO_W.PUT('tipo', 1);
    JSON_DATOS_CONTACTO_W.PUT('valor', 0);
    JSON_DATOS_CONTACTO_W.PUT('uso', 1);
    JSON_DATOS_CONTACTO_LIST_W.APPEND(JSON_DATOS_CONTACTO_W.to_json_value());

    JSON_DIREC_PERSONA_W.PUT('uso', 1);
    JSON_DIREC_PERSONA_W.PUT('direccion', '');
    JSON_DIREC_PERSONA_W.PUT('localidad', '');
    JSON_DIREC_PERSONA_W.PUT('provincia', '');
    JSON_DIREC_PERSONA_W.PUT('pais', '');
    JSON_DIREC_PERSONA_W.PUT('cp', '');
    JSON_DIREC_PERSONA_W.PUT('piso', '');
    JSON_DIREC_PERSONA_W.PUT('dpto', '');
    JSON_DIREC_PERSONA_W.PUT('tipo', 1);
    JSON_DIREC_PERSONA_W.PUT('lat', 0);
    JSON_DIREC_PERSONA_W.PUT('lng', 0);
    JSON_DIREC_PERSONA_LIST_W.APPEND(JSON_DIREC_PERSONA_W.to_json_value());

    JSON_EDIT_FORM_GROUP_2_W.PUT('datosContacto', JSON_DATOS_CONTACTO_LIST_W);
    JSON_EDIT_FORM_GROUP_2_W.PUT('direccionesPersona', JSON_DIREC_PERSONA_LIST_W);
    JSON_EDIT_FORM_GROUP_2_W.PUT('fotoHd', 'data:image/png;base64, iVg==');

    JSON_DATOS_ADICIONALES_W.PUT('editarFormGroup2', JSON_EDIT_FORM_GROUP_2_W);

    JSON_W.PUT('datosBasicos', JSON_DATOS_BASICOS_W);
    JSON_W.PUT('datosAdicionales', JSON_DATOS_ADICIONALES_W);

    DBMS_LOB.CREATETEMPORARY(JSON_DATA_W, TRUE);
    JSON_W.TO_CLOB(JSON_DATA_W);

    SELECT  BIFROST.SEND_INTEGRATION_CONTENT('api.naturalperson.send',
                                               JSON_DATA_W,
                                               'WebServiceTIE')
    INTO  DS_RETORNO_INTEGRACAO_W
    FROM  DUAL;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_ATUAL
before update ON TASY.PESSOA_FISICA for each row
declare

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion?o [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat?s [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten?:
	Par?tro 5 - [45] - Consistir a informa? do org?emissor do documento de identifica? da pessoa f?ca com base no cadastro de ?os emissores
-------------------------------------------------------------------------------------------------------------------
Refer?ias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

  ie_altera_cadastro_w          varchar2(01);
  nr_identidade_w               varchar2(20);
  qt_anos_w                     number(15, 0);
  qt_existe_w                   number(10);
  cd_estabelecimento_w          number(4);
  nr_sequencia_w                number(10);
  qt_emp_w                      number(10);
  qt_solic_w                    number(10);
  nr_seq_emp_w                  number(10);
  cd_setor_atendimento_w        number(5);
  ie_emprestar_w                varchar2(1);
  nr_seq_pf_hist_w              number(10);
  ie_setor_origem_w             varchar2(10); -- Ivan em 10/08/2007 OS60862
  qt_setor_prontuario_w         number(5); -- Ivan em 10/08/2007 OS60862
  nr_seq_set_origem_w           number(10); --Ivan em 10/08/2007 OS60862
  nr_seq_duplic_w               number(10, 0);
  ie_orgao_emissor_w            varchar2(1);
  nm_pessoa_montar_w            pessoa_fisica.nm_pessoa_fisica%type;
  qt_registro_w                 number(10);
  ie_prontuario_w               varchar2(01);
  ie_cancelar_trat_w            varchar2(01);
  ie_cancelar_agen_cons_obito_w varchar2(01);
  ie_cancelar_agen_serv_obito_w varchar2(01);
  ie_cancelar_agen_exam_obito_w varchar2(01);
  ie_cancelar_agen_ciru_obito_w varchar2(01);
  ie_cancelar_agen_obito_w      varchar2(01);
  nr_cartao_nac_sus_w           varchar2(20);
  nr_cirurgia_w                 number(10,0);
  ds_motivo_status_w            agenda_paciente.ds_motivo_status%type := wheb_mensagem_pck.get_texto(795084);
  ie_atualiza_fim_validade_w    varchar2(1) := 'N';
  nm_pessoa_retorno_w           varchar2(60);
  nm_pessoa_fisica_w            varchar2(60);
  cd_tipo_agenda_w				number(1);

  ie_permite_pontuacao_rg_w     varchar2(1);
  ie_revisar_alt_w              varchar2(10);

  cd_agenda_w                   number(10);
  nr_seq_agenda_w               number(10);
  dt_agenda_w                   agenda_consulta.dt_agenda%type;
  nm_guerra_w                   medico.nm_guerra%type;
  qt_parametro_w                number(10);

  qt_hor_cancel_w               number(10);

  bb_genderID                   varchar2(32);
  bb_nr_atendimento             atendimento_paciente.nr_atendimento%type;
  bb_nr_seq_interno             atend_paciente_unidade.nr_seq_interno%type;

  json_demografia               philips_json;
  envio_integracao_bb           clob;
  retorno_integracao_bb         clob;

  IE_ATUAL_PAC_AGEINT_w         parametro_agenda.IE_ATUAL_PAC_AGEINT%type;
  IE_ATUAL_PAC_AGECON_AGESERV_w parametro_agenda.IE_ATUAL_PAC_AGECON_AGESERV%type;
  ie_cad_completo_w             FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
  ie_cad_simplif_w              FUNCAO_PARAMETRO.VL_PARAMETRO_PADRAO%type;
  nr_seq_prestador_w            pls_prestador.nr_sequencia%type;
  country_code_w                number(2);
  ds_given_name_w               person_name.ds_given_name%type;
  ds_component_name_1_w         person_name.ds_component_name_1%type;
  ds_family_name_w              person_name.ds_family_name%type;
  reg_integracao_p              gerar_int_padrao.reg_integracao;
  nr_seq_person_name_w          person_name.nr_sequencia%type;
  NR_PARAM_277_W                NUMBER(10);

  esocial_altera_cad_det_w      gerar_esocial_dados_pck.t_esocial_altera_cad_det;

  Cursor C01 is
    select b.cd_tipo_agenda, a.cd_agenda, a.nr_sequencia, a.dt_agenda
      from agenda_consulta a, agenda b
     where a.cd_agenda = b.cd_agenda
       and a.cd_pessoa_fisica = :new.cd_pessoa_fisica
       and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.dt_agenda) >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_obito)
       and a.ie_status_agenda <> 'C';

  Cursor C02 is
    select a.cd_agenda, a.nr_sequencia, a.hr_inicio
      from agenda_paciente a, agenda b
     where a.cd_agenda = b.cd_agenda
       and b.cd_tipo_agenda = 2
       and a.cd_pessoa_fisica = :new.cd_pessoa_fisica
       and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.hr_inicio) >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_obito)
       and a.ie_status_agenda <> 'C';

  Cursor C03 is
    select a.cd_agenda, a.nr_sequencia, a.hr_inicio
      from agenda_paciente a, agenda b
    where a.cd_agenda = b.cd_agenda
      and b.cd_tipo_agenda = 1
      and a.cd_pessoa_fisica = :new.cd_pessoa_fisica
      and ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.hr_inicio) >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_obito)
      and a.ie_status_agenda <> 'C';

  Cursor c_prestadores is
    select a.nr_sequencia nr_seq_prestador
      from pls_prestador a
     where a.cd_pessoa_fisica = :new.cd_pessoa_fisica;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	/* Esta deve ser sempre a primeira rotina da trigger */
	country_code_w := philips_param_pck.get_cd_pais();

	if	((:new.NM_PRIMEIRO_NOME is not null) or (:new.nr_seq_person_name is not null)) and (country_code_w <> 1) then
		begin
		if 	(country_code_w = 2) then
			begin
			if	((:new.nr_seq_person_name is not null) and
			(((:new.nm_primeiro_nome != :old.nm_primeiro_nome) or (:new.nm_primeiro_nome is null and :old.nm_primeiro_nome is not null) or (:new.nm_primeiro_nome is not null and :old.nm_primeiro_nome is null))or
			((:new.nm_sobrenome_pai != :old.nm_sobrenome_pai) or (:new.nm_sobrenome_pai is null and :old.nm_sobrenome_pai is not null) or (:new.nm_sobrenome_pai is not null and :old.nm_sobrenome_pai is null)) or
			((:new.nm_sobrenome_mae != :old.nm_sobrenome_mae) or (:new.nm_sobrenome_mae is null and :old.nm_sobrenome_mae is not null) or (:new.nm_sobrenome_mae is not null and :old.nm_sobrenome_mae is null))))then

				update	person_name
				set	ds_given_name = :new.nm_primeiro_nome,
					ds_component_name_1 = :new.nm_sobrenome_mae,
					ds_family_name = :new.nm_sobrenome_pai
				where	nr_sequencia = :new.nr_seq_person_name;

			elsif	((:new.nr_seq_person_name is not null) and
			(((:new.nm_primeiro_nome = :old.nm_primeiro_nome) or (:new.nm_primeiro_nome is null and :old.nm_primeiro_nome is null))and
			((:new.nm_sobrenome_pai = :old.nm_sobrenome_pai) or (:new.nm_sobrenome_pai is null and :old.nm_sobrenome_pai is null)) and
			((:new.nm_sobrenome_mae = :old.nm_sobrenome_mae) or (:new.nm_sobrenome_mae is null and :old.nm_sobrenome_mae is null))))then
				begin
				select 	max(ds_given_name),
						max(ds_component_name_1),
						max(ds_family_name)
				into	ds_given_name_w,
						ds_component_name_1_w,
						ds_family_name_w
				from	person_name
				where	nr_sequencia = :new.nr_seq_person_name
				and		ds_type	= 'main';

				:new.nm_primeiro_nome := ds_given_name_w;
				:new.nm_sobrenome_pai := ds_family_name_w;
				:new.nm_sobrenome_mae := ds_component_name_1_w;
				end;
			end if;
      nm_pessoa_montar_w	:= substr(montar_nm_pessoa_fisica(null,:new.nm_primeiro_nome,:new.nm_sobrenome_pai,:new.nm_sobrenome_mae, :new.nm_usuario, :new.nr_seq_person_name),1,60);
			if (trim(nm_pessoa_montar_w) is  not null) then
				:new.nm_pessoa_fisica	:= nm_pessoa_montar_w;
			end if;
			end;
		elsif (:new.NR_SEQ_PERSON_NAME is not null) then
			begin
				:new.nm_pessoa_fisica	:= substr(montar_nm_pessoa_fisica(null,null,null,null,:new.nm_usuario,:new.nr_seq_person_name),1,60);
			end;
		end if;
		end;
	end if;

	ie_permite_pontuacao_rg_w	:= nvl(Obter_Valor_Param_Usuario(0, 184, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento), 'N');
	Obter_param_Usuario(5, 164, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_completo_w);
	Obter_param_Usuario(32, 44, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_cad_simplif_w);
  NR_PARAM_277_W := NVL(OBTER_SOMENTE_NUMERO(OBTER_VALOR_PARAM_USUARIO(5, 277, OBTER_PERFIL_ATIVO, :NEW.NM_USUARIO, :NEW.CD_ESTABELECIMENTO)), 0);

	select	nvl(max(vl_parametro), max(vl_parametro_padrao))
	into	ie_prontuario_w
	from	funcao_parametro
	where	cd_funcao	= 5
	and	nr_sequencia	= 54;

	select  nvl(max(IE_ATUAL_PAC_AGEINT), 'N'),
		nvl(max(IE_ATUAL_PAC_AGECON_AGESERV), 'N')
	into	IE_ATUAL_PAC_AGEINT_w,
		IE_ATUAL_PAC_AGECON_AGESERV_w
	from 	PARAMETRO_AGENDA
	where 	cd_estabelecimento = :new.cd_estabelecimento;


	if	(:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica) or
		(:new.nr_cpf <> :old.nr_cpf) then
		begin

		select	nvl(max(vl_parametro), '0')
		into	ie_altera_cadastro_w
		from	funcao_param_usuario
		where	cd_funcao		= 0
		and	nr_sequencia		= 34
		and 	nm_usuario_param	= :new.nm_usuario;

		if	(ie_altera_cadastro_w = '0') then
			begin

			select	nvl(vl_parametro, vl_parametro_padrao)
			into	ie_altera_cadastro_w
			from	funcao_parametro
			where	cd_funcao	= 0
			and	nr_sequencia	= 34;

			end;
		end if;

		if	(ie_altera_cadastro_w	= 'N') then
			begin

			-- Voc??possui permiss?para alterar o nome e o CPF do paciente!
			Wheb_mensagem_pck.exibir_mensagem_abort(192964);
			end;
		end if;
		end;
	end if;

	IF (:NEW.NM_PESSOA_FISICA <> :OLD.NM_PESSOA_FISICA
  AND NR_PARAM_277_W > 0
  AND LENGTH(:NEW.NM_PESSOA_FISICA) < NR_PARAM_277_W) THEN
		WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(1112273);
	END IF;

	/* 	Rafael em 13/09/2006 -> gerava erro: "cannot update ("TASY","NM_PESSOA_FISICA") to null"
		criado novo campo NM_PESSOA_FISICA_SEM_ACENTO*/
	/*       Coelho 03/07/2008 OS95248 */

	nr_identidade_w		:= '';

	if	(nvl(ie_permite_pontuacao_rg_w,'N') = 'N') then
		if	(:new.NR_IDENTIDADE is not null) and
			(:new.NR_IDENTIDADE <> nvl(:Old.NR_IDENTIDADE,'X')) then
			:new.NR_IDENTIDADE	:= upper(:new.NR_IDENTIDADE);
			for	i in 1..length(:new.NR_IDENTIDADE) loop		-- validar identidade, somente letras e n?meros
				if	(instr('0123456789ABCDEFGHIJKLMNOPQRSTUVXWYV',substr(:new.NR_IDENTIDADE, i, 1)) > 0) then
					nr_identidade_w		:= nr_identidade_w || substr(:new.NR_IDENTIDADE, i, 1);
				end if;
			end loop;
			:new.NR_IDENTIDADE	:= nr_identidade_w;
		end if;
	end if;

	if	((:new.dt_nascimento is not null) and (:new.dt_nascimento <> :old.dt_nascimento)) or
		((:old.dt_nascimento is null)  and (:new.dt_nascimento is not null)) then
		begin
		qt_anos_w	:= obter_idade(:new.dt_nascimento,sysdate,'A');
		update	agenda_paciente
		set	qt_idade_paciente	= qt_anos_w,
			dt_nascimento_pac	= :new.dt_nascimento
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	dt_agenda		> sysdate - 30;
		exception
		when others then
		qt_anos_w	:= obter_idade(:new.dt_nascimento,sysdate,'A');
		end;

		begin
		wheb_usuario_pck.set_ie_executar_trigger('N');
		if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then
			if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
				update	agenda_consulta
				set	QT_IDADE_PAC	= qt_anos_w,
					DT_NASCIMENTO_PAC	= :new.dt_nascimento
				where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
				and	dt_agenda		> sysdate - 30;
			end if;
		end if;
		if (IE_ATUAL_PAC_AGEINT_w = 'S') then
			update	agenda_integrada
			set	QT_IDADE_PAC	= qt_anos_w,
				DT_NASCIMENTO	= :new.dt_nascimento
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
			and	DT_INICIO_AGENDAMENTO	> sysdate - 30;
		end if;
		wheb_usuario_pck.set_ie_executar_trigger('S');
		exception
		when others then
		wheb_usuario_pck.set_ie_executar_trigger('S');
		qt_anos_w	:= obter_idade(:new.dt_nascimento,sysdate,'A');
		end;
	end if;

	if	(:new.dt_nascimento is not null) and
		(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_nascimento) <> (:new.dt_nascimento)) then
		:new.dt_nascimento := ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_nascimento);
	end if;

	if	((:new.qt_altura_cm is not null) and (:new.qt_altura_cm <> :old.qt_altura_cm)) or
		((:old.qt_altura_cm is null)  and (:new.qt_altura_cm is not null)) then
		update	agenda_paciente
		set	qt_altura_cm		= :new.qt_altura_cm
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	dt_agenda		> sysdate - 30;

		wheb_usuario_pck.set_ie_executar_trigger('N');
		if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then

			if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
				update	agenda_consulta
				set	QT_ALTURA_CM	 = :new.qt_altura_cm
				where	cd_pessoa_fisica = :new.cd_pessoa_fisica
				and	dt_agenda	 > sysdate - 30;
			end if;
		end if;
		if (IE_ATUAL_PAC_AGEINT_w = 'S') then
			update	agenda_integrada
			set	QT_ALTURA_CM	 = 	:new.qt_altura_cm
			where	cd_pessoa_fisica = 	:new.cd_pessoa_fisica
			and	DT_INICIO_AGENDAMENTO	> sysdate - 30;
			wheb_usuario_pck.set_ie_executar_trigger('S');
		end if;
		wheb_usuario_pck.set_ie_executar_trigger('S');

	end if;

	if	((:new.qt_peso is not null) and (:new.qt_peso <> :old.qt_peso)) or
		((:old.qt_peso is null)  and (:new.qt_peso is not null)) then
		update	agenda_paciente
		set	qt_peso			= :new.qt_peso
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	dt_agenda		> sysdate - 30;

		wheb_usuario_pck.set_ie_executar_trigger('N');
		if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then
			if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
				update	agenda_consulta
				set	qt_peso			= :new.qt_peso
				where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
				and	dt_agenda		> sysdate - 30;
			end if;
		end if;
		if (IE_ATUAL_PAC_AGEINT_w = 'S') then
			update	agenda_integrada
			set	QT_PESO			= :new.qt_peso
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
			and	DT_INICIO_AGENDAMENTO	> sysdate - 30;
		end if;
		wheb_usuario_pck.set_ie_executar_trigger('S');

	end if;

	if	(:new.nr_telefone_celular is not null) and
		(nvl(:new.nr_telefone_celular,'X') <> nvl(:old.nr_telefone_celular,'X')) then
		begin

		update	agenda_paciente
		set	nr_telefone		= :new.nr_telefone_celular
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	dt_agenda		> sysdate - 30;

		wheb_usuario_pck.set_ie_executar_trigger('N');
		if (ie_cad_completo_w = 'N') and (ie_cad_simplif_w = 'N') then
			if (IE_ATUAL_PAC_AGECON_AGESERV_w = 'S') then
				update	agenda_consulta
				set	NR_TELEFONE		= :new.nr_telefone_celular
				where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
				and	dt_agenda		> sysdate - 30;
			end if;
		end if;
		if (IE_ATUAL_PAC_AGEINT_w = 'S') then
			update	agenda_integrada
			set	NR_TELEFONE		= :new.nr_telefone_celular
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
			and	DT_INICIO_AGENDAMENTO		> sysdate - 30;
		end if;
		wheb_usuario_pck.set_ie_executar_trigger('S');
		end;
	end if;

	if	(:old.nr_prontuario is not null) and
		(:new.nr_prontuario is not null) and
		(:old.nr_prontuario) <> (:new.nr_prontuario) and
		(ie_prontuario_w	= 'S') then

		select	count(*)
		into	qt_solic_w
		from	same_cpi_solic
		where	nr_prontuario = :old.nr_prontuario;

		select	count(*)
		into	qt_emp_W
		from	same_cpi_emp
		where	nr_prontuario = :old.nr_prontuario;

		update	same_cpi_prontuario
		set	nr_prontuario = :new.nr_prontuario
		where	nr_prontuario = :old.nr_prontuario;

			-- Voc??pode alterar o n?mero do prontu?o, pois existem solicita?s ou empr?imos do mesmo.
			--Wheb_mensagem_pck.exibir_mensagem_abort(192966);
		if	(qt_emp_w > 0) then
			update	same_cpi_emp
			set	nr_prontuario = :new.nr_prontuario
			where	nr_prontuario = :old.nr_prontuario;
		end if;
		if 	(qt_solic_w > 0) then
			update	same_cpi_solic
			set	nr_prontuario = :new.nr_prontuario
			where	nr_prontuario = :old.nr_prontuario;
		end	if;

		/* Rafael em 24/11/2007 OS74917 */
		begin
		select	pessoa_fisica_duplic_seq.nextval
		into	nr_seq_duplic_w
		from	dual;

		insert into pessoa_fisica_duplic	(
							nr_sequencia,
							cd_pessoa_fisica,
							dt_atualizacao,
							nm_usuario,
							cd_cadastro,
							nm_paciente,
							nr_prontuario,
							qt_atendimento,
							qt_atend_interno,
							ie_acerto
							)
		values					(
							nr_seq_duplic_w,
							:new.cd_pessoa_fisica,
							sysdate,
							:new.nm_usuario,
							:new.cd_pessoa_fisica,
							:new.nm_pessoa_fisica,
							:old.nr_prontuario,
							null,
							null,
							'P'
							);
		exception
			when others then
			nr_seq_duplic_w := 0;
		end;

	elsif	(:new.nr_prontuario is not null) then

		/* Rafael em 13/06/2007 OS59807 */
		select	nvl(:new.cd_estabelecimento,min(cd_estabelecimento))
		into	cd_estabelecimento_w
		from	estabelecimento;

		/* Rafael em 13/06/2007 OS59807
		select	count(*)
		into	qt_existe_w
		from	same_cpi_prontuario
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	nr_prontuario		= :new.nr_prontuario;
		*/

		/* Rafael em 13/06/2007 OS59807 */
		select	count(*)
		into	qt_existe_w
		from	same_cpi_prontuario
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	nr_prontuario		= :new.nr_prontuario;

		if	(qt_existe_w = 0) and
			(ie_prontuario_w = 'S') then

			/* Rafael em 13/06/2007 OS59807
			select	nvl(:new.cd_estabelecimento,min(cd_estabelecimento))
			into	cd_estabelecimento_w
			from	estabelecimento;
			*/

			select	same_cpi_prontuario_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into same_cpi_prontuario(
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				nr_prontuario,
				cd_pessoa_fisica,
				qt_volume,
				ds_localizacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_sequencia_w,
				cd_estabelecimento_w,
				sysdate,
				:new.nm_usuario,
				:new.nr_prontuario,
				:new.cd_pessoa_fisica,
				1,
				null,
				sysdate,
				:new.nm_usuario);

			Obter_Param_Usuario(1007,11,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_emprestar_w);
			if	(ie_emprestar_w = 'S') then
				begin
				select	nvl(max(cd_setor_atendimento),0)
				into	cd_setor_atendimento_w
				from	usuario
				where	nm_usuario	= :new.nm_usuario;

				if	(cd_setor_atendimento_w > 0) then
					begin
					select	same_cpi_emp_seq.nextval
					into	nr_seq_emp_w
					from	dual;

					insert into same_cpi_emp(
						nr_sequencia,
						cd_estabelecimento,
						nr_prontuario,
						dt_atualizacao,
						nm_usuario,
						cd_setor_atendimento,
						dt_emprestimo,
						nm_usuario_empr,
						ie_operacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec)
					values(	nr_seq_emp_w,
						cd_estabelecimento_w,
						:new.nr_prontuario,
						sysdate,
						:new.nm_usuario,
						cd_setor_atendimento_w,
						sysdate,
						:new.nm_usuario,
						'E',
						sysdate,
						:new.nm_usuario);
					end;
				end if;
				end;
			end if;

		end if;

	end if;

	if	(:old.nr_prontuario is not null) and
		(:new.nr_prontuario is null) then
		select	count(*)
		into	qt_solic_w
		from	same_cpi_solic
		where	nr_prontuario	= :old.nr_prontuario;

		select	count(*)
		into	qt_emp_w
		from	same_cpi_emp
		where	nr_prontuario	= :old.nr_prontuario;

		if	(qt_solic_w = 0) and
			(qt_emp_w = 0) then
			delete	same_cpi_prontuario
			where	nr_prontuario	= :old.nr_prontuario;
		end if;
	end if;

	/* Marcus em 04/12/06 OS445904 */

	if	(:new.ds_senha is null)then
		select	obter_senha_pf(:new.dt_nascimento)
		into	:new.ds_senha
		from	dual;

	end if;

	/* Coelho em 02/01/07 	OS45273 */
	if ( (:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica) or
		(:old.ds_fonetica is null )) then
		:new.ds_fonetica := gera_fonetica(:new.nm_pessoa_fisica,'N');
	end if;

	/* Oraci em 02/08/2007 OS63677 */
	if	(:old.nm_pessoa_fisica is not null) and
		(:new.nm_pessoa_fisica <> :old.nm_pessoa_fisica) then

		select	pessoa_fisica_historico_seq.nextval
		into	nr_seq_pf_hist_w
		from	dual;

		insert into pessoa_fisica_historico(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_pessoa_fisica,
				cd_pessoa_fisica,
				dt_liberacao)
		values		(nr_seq_pf_hist_w,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:old.nm_pessoa_fisica,
				:old.cd_pessoa_fisica,
				sysdate);
	end if;

	/* Ivan em 10/08/2007 OS60862 */
	Obter_Param_Usuario(32,19,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_setor_origem_w);

	if	(ie_setor_origem_w = 'S') then
		begin

		select	count(*)
		into	qt_setor_prontuario_w
		from	pf_setor_prontuario
		where	cd_pessoa_fisica = :old.cd_pessoa_fisica;

		select	nvl(max(cd_setor_atendimento),0)
		into	cd_setor_atendimento_w
		from	usuario
		where	nm_usuario	= :new.nm_usuario;

		if	((cd_setor_atendimento_w > 0) and (qt_setor_prontuario_w = 0)) then
			begin

			select	pf_setor_prontuario_seq.nextval
			into	nr_seq_set_origem_w
			from	dual;

			insert into pf_setor_prontuario (
				nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				cd_setor_atendimento,
				dt_inicio_cinculo)
			values (
				nr_seq_set_origem_w,
				sysdate,
				sysdate,
				:new.nm_usuario,
				:new.nm_usuario,
				:old.cd_pessoa_fisica,
				cd_setor_atendimento_w,
				sysdate
			);

			end;
		end if;

		end;
	end if;

	/* Oraci em 11/01/2008 OS78875*/
	if	(:new.nr_prontuario is not null) and
		(:old.nr_prontuario is null) then

		if	(:new.dt_geracao_pront is null) then
			:new.dt_geracao_pront :=  sysdate;
		end if;

		/*insert into logxxx_tasy(	DT_ATUALIZACAO,
					NM_USUARIO,
					CD_LOG,
					DS_LOG)
		values		(	sysdate,
					:new.nm_usuario,
					55777,
					'PF: ' || :new.cd_pessoa_fisica || chr(13) ||
					'Pessoa_Fisica_Atual'  || chr(13) ||
					':old.nr_prontuario=' || :old.nr_prontuario || chr(13) ||
					':new.nr_prontuario=' || :new.nr_prontuario || chr(13) ||
					':old.dt_geracao_pront=' || :old.dt_geracao_pront || chr(13) ||
					':new.dt_geracao_pront=' || :new.dt_geracao_pront);*/
	elsif	(:new.nr_prontuario is null) and
		(:old.nr_prontuario is not null) then
		:new.dt_geracao_pront :=  null;

		/*insert into logxxx_tasy(	DT_ATUALIZACAO,
					NM_USUARIO,
					CD_LOG,
					DS_LOG)
		values		(	sysdate,
					:new.nm_usuario,
					55777,
					'PF: ' || :new.cd_pessoa_fisica || chr(13) ||
					'Pessoa_Fisica_Atual'  || chr(13) ||
					':old.nr_prontuario=' || :old.nr_prontuario || chr(13) ||
					':new.nr_prontuario=' || :new.nr_prontuario || chr(13) ||
					':old.dt_geracao_pront=' || :old.dt_geracao_pront || chr(13) ||
					':new.dt_geracao_pront=' || :new.dt_geracao_pront);*/
	else
		-- Edgar 27/05/2009, OS 138561, s?terar o dt_geracao_pront se alterar o nr_prontuario
		:new.dt_geracao_pront	:= :old.dt_geracao_pront;
	end if;

	/* Dalcastagne em 14/01/2008 OS 79144 */


	/* Rafael em 17/1/8 OS79667 */
	select	nvl(max(obter_valor_param_usuario(5, 45, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento)), 'N')
	into	ie_orgao_emissor_w
	from	dual;

	if	(ie_orgao_emissor_w = 'S') and
		(:new.ds_orgao_emissor_ci is not null) and
		(consistir_orgao_emissor(:new.ds_orgao_emissor_ci) = 'N') then
		-- O org?emissor do documento informado n?confere com os definidos no cadastro, favor verificar.
		Wheb_mensagem_pck.exibir_mensagem_abort(192967);
	end if;
	/* Fim Rafael em 17/1/8 OS79667 */

	/* Francisco em 11/03/2008  - OS 82683 - Se ?o plano e foi alterado, deve mudar o campo IE_REVISAR */

	/*aaschlote 27/12/2012 OS 524353*/
	Obter_Param_Usuario(0,189,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_revisar_alt_w);

	ie_revisar_alt_w	:= nvl(ie_revisar_alt_w,'R');

	if	(:old.ie_revisar = :new.ie_revisar) and
		(ie_revisar_alt_w <> 'M') then
		select	count(*)
		into	qt_registro_w
		from	pls_contrato
		where	cd_pf_estipulante	= :new.cd_pessoa_fisica;

		if	(qt_registro_w > 0) then
			:new.ie_revisar	:= ie_revisar_alt_w;
		else
			select	count(*)
			into	qt_registro_w
			from	pls_contrato_pagador
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

			if	(qt_registro_w > 0) then
				:new.ie_revisar	:= ie_revisar_alt_w;
			else
				select	count(*)
				into	qt_registro_w
				from	pls_segurado
				where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

				if	(qt_registro_w > 0) then
					:new.ie_revisar	:= ie_revisar_alt_w;
				else
					select	count(*)
					into	qt_registro_w
					from	pls_prestador
					where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

					if	(qt_registro_w > 0) then
						:new.ie_revisar	:= ie_revisar_alt_w;
					end if;
				end if;
			end if;
		end if;

	end if;

	Obter_Param_Usuario(5,90,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_cancelar_trat_w);

	if  ((ie_cancelar_trat_w = 'S') or (ie_cancelar_trat_w = 'I')) and
		((:old.dt_obito <> :new.dt_obito) or ((:new.dt_obito is not null) and (:old.dt_obito is null)) or (:new.dt_obito is not null)) then
		cancelar_trat_onc_pf_obito(:new.cd_pessoa_fisica,:new.nm_usuario);
	end if;

	if  (ie_cancelar_trat_w = 'I') and
		((:old.dt_obito <> :new.dt_obito) or ((:new.dt_obito is not null) and (:old.dt_obito is null)) or (:new.dt_obito is not null)) then
		inativar_protoc_onco_pf_obito(:new.cd_pessoa_fisica,:new.nm_usuario);
	end if;

	Obter_Param_Usuario(821,461,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_cancelar_agen_cons_obito_w);
	Obter_Param_Usuario(866,319,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_cancelar_agen_serv_obito_w);

	if ((ie_cancelar_agen_cons_obito_w = 'S') or (ie_cancelar_agen_serv_obito_w = 'S')) and
		((:new.dt_obito is not null) and (:old.dt_obito is null)) then

		open C01;
		loop
		fetch C01 into
			cd_tipo_agenda_w,
			cd_agenda_w,
			nr_seq_agenda_w,
			dt_agenda_w;
		exit when C01%notfound;
			begin
			if (cd_agenda_w is not null) and (nr_seq_agenda_w is not null) then
				if (cd_tipo_agenda_w = 3 and ie_cancelar_agen_cons_obito_w = 'S') or (cd_tipo_agenda_w = 5 and ie_cancelar_agen_serv_obito_w = 'S') then
					select	nvl(max(PKG_DATE_UTILS.extract_field('SECOND', dt_Agenda)),0)+1
					into	qt_hor_cancel_w
					from	agenda_consulta
					where	cd_agenda = cd_agenda_w
					and	to_date(to_char(dt_agenda,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
					and	ie_status_agenda = 'C';

					update 	agenda_consulta
					set	ie_status_agenda = 'C',
						ds_motivo_status = wheb_mensagem_pck.get_texto(795084),
						nm_usuario = :new.nm_usuario,
						dt_Agenda = dt_agenda + qt_hor_cancel_w / 86400
					where	cd_agenda = cd_agenda_w
					and	nr_sequencia = nr_seq_agenda_w;
				end if;
			end if;
			end;
		end loop;
		close C01;

		if (ie_cancelar_agen_cons_obito_w = 'S') then
			update	agenda_lista_espera
			set	ie_status_espera = 'C',
				ds_observacao = wheb_mensagem_pck.get_texto(795084),
				nm_usuario = :new.nm_usuario,
				dt_atualizacao = SYSDATE
			where	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	ie_status_espera = 'A'
			and (cd_tipo_agenda = 3
			or cd_tipo_agenda is null);
		end if;

		if (ie_cancelar_agen_serv_obito_w = 'S') then
			update	agenda_lista_espera
			set	ie_status_espera = 'C',
				ds_observacao = wheb_mensagem_pck.get_texto(795084),
				nm_usuario = :new.nm_usuario,
				dt_atualizacao = SYSDATE
			where	cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	ie_status_espera = 'A'
			and (cd_tipo_agenda = 5
			or cd_tipo_agenda is null);
		end if;
	end if;

	Obter_Param_Usuario(820,464,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_cancelar_agen_exam_obito_w);

	if (ie_cancelar_agen_exam_obito_w = 'S') and
		((:new.dt_obito is not null) and (:old.dt_obito is null)) then

		open C02;
		loop
		fetch C02 into
			cd_agenda_w,
			nr_seq_agenda_w,
			dt_agenda_w;
		exit when C02%notfound;
			begin
			if (cd_agenda_w is not null) and (nr_seq_agenda_w is not null) then

				select	nvl(max(PKG_DATE_UTILS.extract_field('SECOND', hr_inicio)),0)+1
				into	qt_hor_cancel_w
				from	agenda_paciente
				where	cd_agenda = cd_agenda_w
				and	to_date(to_char(hr_inicio,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
				and	ie_status_agenda = 'C';

				update 	agenda_paciente
				set	ie_status_agenda = 'C',
					ds_motivo_status = wheb_mensagem_pck.get_texto(795084),
					nm_usuario = :new.nm_usuario,
					hr_inicio = hr_inicio + qt_hor_cancel_w / 86400
				where	cd_agenda = cd_agenda_w
				and	nr_sequencia = nr_seq_agenda_w;

			end if;
			end;
		end loop;
		close C02;

		update	agenda_lista_espera
		set	ie_status_espera = 'C',
			ds_observacao = wheb_mensagem_pck.get_texto(795084),
			nm_usuario = :new.nm_usuario,
			dt_atualizacao = SYSDATE
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and	ie_status_espera = 'A'
		and (cd_tipo_agenda = 2
		or cd_tipo_agenda is null);

	end if;

	Obter_Param_Usuario(871,850,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_cancelar_agen_ciru_obito_w);

	if (ie_cancelar_agen_ciru_obito_w = 'S') and
		((:new.dt_obito is not null) and (:old.dt_obito is null)) then

		open C03;
		loop
		fetch C03 into
			cd_agenda_w,
			nr_seq_agenda_w,
			dt_agenda_w;
		exit when C03%notfound;
			begin
			if (cd_agenda_w is not null) and (nr_seq_agenda_w is not null) then

				update 	agenda_paciente
				set	ie_status_agenda = 'C',
					ds_motivo_status = SUBSTR(ds_motivo_status_w, 1, 255),
					ds_observacao = DECODE(ds_observacao,NULL,SUBSTR(ds_motivo_status_w,1,1500),SUBSTR(SUBSTR(ds_observacao,1,1500)||CHR(13) || CHR(10)||SUBSTR(ds_motivo_status_w, 1, 255),1,3000)),
					nm_usuario = :new.nm_usuario,
					nm_usuario_cancel = :new.nm_usuario,
					hr_inicio = hr_inicio + 1 / 86400,
					dt_cancelamento = sysdate,
					dt_atualizacao = sysdate
				where	cd_agenda = cd_agenda_w
				and	nr_sequencia = nr_seq_agenda_w;

				select nvl(max(nr_cirurgia), 0)
				into nr_cirurgia_w
				from agenda_paciente
				where	nr_sequencia = nr_seq_agenda_w;

				if (nr_cirurgia_w > 0) then
					update cirurgia
					set	ie_status_cirurgia = 3,
						dt_cancelamento = sysdate
					where nr_cirurgia = nr_cirurgia_w;
				end if;

				update agenda_pac_equip
				set    dt_confirmacao = null
				where  nr_seq_agenda  = nr_seq_agenda_w;

			end if;
			end;
		end loop;
		close C03;
	end if;

	Obter_Param_Usuario(5,181,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_atualiza_fim_validade_w);
	if      (ie_atualiza_fim_validade_w = 'S') and
		((:new.dt_obito is not null) and (:old.dt_obito is null)) then

		update	PF_CARTAO_FIDELIDADE
		set 	DT_FIM_VALIDADE = :new.dt_obito
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	end if;

	/*if	(nvl(:new.ds_senha,'X') <> nvl(:old.ds_senha,'X')) then

		insert into logxxx_tasy	(
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log
				)
		values			(
				sysdate,
				:new.nm_usuario,
				41875,
				'Pessoa fisica' || :new.cd_pessoa_fisica || ' Senha antiga ' || :old.ds_senha || ' Senha atual' || :new.ds_senha);
	end if;*/

	if	(:new.nr_cartao_nac_sus is not null) then
		begin
		select	to_number(:new.nr_cartao_nac_sus)
		into	nr_cartao_nac_sus_w
		from	dual;
		exception when others then
			-- N??ermitido informar caracteres alfanum?cos no n?mero do cart?SUS.
			Wheb_mensagem_pck.exibir_mensagem_abort(192968);
		end;
	end if;

	/* Fim Francisco em 11/03/2008 */

	if	(inserting) or
		(nvl(:new.NR_SEQ_PERFIL,0)	<> nvl(:old.NR_SEQ_PERFIL,0)) or
		(nvl(:new.NR_PRONTUARIO,0)	<> nvl(:old.NR_PRONTUARIO,0)) then
		nm_pessoa_retorno_w	:= Obter_Nome_Notorio_Pac(:new.nm_pessoa_fisica,:new.cd_pessoa_fisica,:new.nr_prontuario,:new.nr_seq_perfil,:new.nm_usuario);

		if	(nm_pessoa_retorno_w is not null) then
			nm_pessoa_fisica_w	:= :new.nm_pessoa_fisica;
			nr_seq_person_name_w := :new.nr_seq_person_name;

			select	count(*)
			into	qt_registro_w
			from	PESSOA_FISICA_OCULTA
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
			and	IE_NOME_REAL		= 'S';

			if	(qt_registro_w	= 0) then
				insert into pessoa_fisica_oculta(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_pessoa_fisica,
					nm_oculto,
					cd_perfil,
					ie_nome_real,
					nr_seq_person_name)
				values	(
					PESSOA_FISICA_OCULTA_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.cd_pessoa_fisica,
					nm_pessoa_fisica_w,
					null,
					'S',
					nr_seq_person_name_w);
			end if;
			:new.nm_pessoa_fisica	:= substr(nm_pessoa_retorno_w,1,60);
			if (nr_seq_person_name_w is not null) then
				begin
				select	person_name_seq.nextval
				into	nr_seq_person_name_w
				from	dual
				where 	rownum < 2;
				insert into person_name(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_type,
					ds_given_name)
				values(
					nr_seq_person_name_w,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					'main',
					:new.nm_pessoa_fisica
				);
				:new.nr_seq_person_name := nr_seq_person_name_w;
				end;
			end if;
		end if;
	end if;
end if;

if	(:new.nm_pessoa_fisica	<> :old.nm_pessoa_fisica) or
	(:new.nm_pessoa_pesquisa is null) then
	:new.nm_pessoa_pesquisa	:= padronizar_nome(:new.nm_pessoa_fisica);
end if;

/* Rafael em 07/09/06 OS40311 */
if	(:new.nm_pessoa_fisica_sem_acento is null) or
	(:new.nm_pessoa_fisica <> :old.nm_pessoa_fisica) or
	(:new.nm_social <> :old.nm_social) then
  begin
       if (obter_se_nome_social <> 'N') then
		   :new.nm_pessoa_fisica_sem_acento	:= nvl(elimina_acentuacao(:new.nm_social), elimina_acentuacao(:new.nm_pessoa_fisica));
       else
           :new.nm_pessoa_fisica_sem_acento	:= elimina_acentuacao(:new.nm_pessoa_fisica);
      end if;
  end;
end if;

/*aaschlote 03/10/2013 OS 652624*/
if	(:new.NM_ABREVIADO is null) or
	(:new.nm_pessoa_fisica <> :old.nm_pessoa_fisica) then
	:new.nm_abreviado	:= pls_gerar_nome_abreviado(:new.nm_pessoa_fisica);
end if;

/* dgstihler 14/08/18  OS 1734662*/
if	(:new.dt_nascimento <> :old.dt_nascimento)
	and (:new.dt_nascimento > sysdate) then
	wheb_mensagem_pck.exibir_mensagem_abort(194547);
	/*Data de nascimento inv?da!*/
end	if;

/*Sandro Paulo 28/05/2015 OS 878225*/
if	(:old.dt_obito is null) and
	(:new.dt_obito is not null) then
	cd_estabelecimento_w	:= obter_estabelecimento_ativo;
	mprev_tratar_exclusao_part(	null,
					:new.cd_pessoa_fisica,
					:new.nm_pessoa_fisica,
					'S', -- (S) ?o
					null,
					:new.dt_obito,
					'I',
					cd_estabelecimento_w,
					:new.nm_usuario);
end if;

/*Jucimara 01/06/2015 OS 849879 */
if	(nvl(:new.nm_pessoa_fisica,'X') <> nvl(:old.nm_pessoa_fisica,'X')) then
	select 	count(1)
	into	qt_parametro_w
	from 	pls_web_param_guia_medico;

	if (qt_parametro_w > 0) then
	begin
		select 	nm_guerra
		into	nm_guerra_w
		from	medico
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	exception
	when others then
		nm_guerra_w	:= null;
	end;
		pls_atualiza_nome_busca_guia(	:new.cd_pessoa_fisica,
						null,
						null,
						null,
						:new.nm_pessoa_fisica,
						nm_guerra_w);
	end if;
end if;

begin
open c_prestadores;
loop
fetch c_prestadores into
	nr_seq_prestador_w;
exit when c_prestadores%notfound;
	begin

	/* Verifica se houve altera? em algum dos campos utilizados no E-Social */
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NM_PESSOA_FISICA',		:old.nm_pessoa_fisica,	:new.nm_pessoa_fisica,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DT_NASCIMENTO',		:old.dt_nascimento,	:new.dt_nascimento,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DT_EMISSAO_CI',		:old.dt_emissao_ci,	:new.dt_emissao_ci,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('IE_ESTADO_CIVIL',		:old.ie_estado_civil,	:new.ie_estado_civil,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_CARTAO_NAC_SUS',	:old.nr_cartao_nac_sus, :new.nr_cartao_nac_sus,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_CEP_CIDADE_NASC',	:old.nr_cep_cidade_nasc,:new.nr_cep_cidade_nasc,esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_CNH',			:old.nr_cnh,		:new.nr_cnh,		esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('CD_MUNICIPIO_IBGE',	:old.cd_municipio_ibge, :new.cd_municipio_ibge,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_PIS_PASEP',		:old.nr_pis_pasep,	:new.nr_pis_pasep,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_SEQ_CONSELHO',		:old.nr_seq_conselho,	:new.nr_seq_conselho,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_TITULO_ELEITOR',	:old.nr_titulo_eleitor,	:new.nr_titulo_eleitor,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('SG_EMISSORA_CI',		:old.sg_emissora_ci,	:new.sg_emissora_ci,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('CD_NACIONALIDADE',		:old.cd_nacionalidade,	:new.cd_nacionalidade,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('CD_PAIS',			:old.nr_seq_pais,	:new.nr_seq_pais,	esocial_altera_cad_det_w);
	gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DS_CODIGO_PROF',		:old.ds_profissao,	:new.ds_profissao,	esocial_altera_cad_det_w);

	if	(esocial_altera_cad_det_w.count > 0) then
		gerar_esocial_dados_pck.gravar_alteracao_cad_det(nr_seq_prestador_w, :new.nm_usuario, esocial_altera_cad_det_w);
	end if;

	end;
end loop;
close c_prestadores;
exception
when others then
	nr_seq_prestador_w	:= null;
end;

if ('00/00/0000 00:00:00' = to_char(:new.dt_obito,'dd/mm/yyyy hh24:mi:ss')) then
	:new.dt_obito := null;
end if;

if	(:new.ie_sexo <> :old.ie_sexo) or
	(:new.dt_nascimento <> :old.dt_nascimento) or
	(:new.cd_nacionalidade <> :old.cd_nacionalidade) or
	(:new.ie_estado_civil <> :old.ie_estado_civil) or
	(:new.cd_religiao <> :old.cd_religiao) or
	(:new.nr_spss <> :old.nr_spss) or
	(:new.cd_rfc <> :old.cd_rfc) or
	(:new.cd_curp <> :old.cd_curp) or
	(:new.nm_pessoa_fisica <> :old.nm_pessoa_fisica) or
	(:new.nr_cpf <> :old.nr_cpf) or
	(:new.nr_identidade <> :old.nr_identidade) or
	(:new.nr_seq_conselho <> :old.nr_seq_conselho) then
	begin
	send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'A');
	end;
end if;

if (upper(:old.nm_pessoa_fisica) <> upper(:new.nm_pessoa_fisica)) or
    (:old.dt_nascimento <> :new.dt_nascimento) or
    (:old.cd_pessoa_mae <> :new.cd_pessoa_mae) then
  check_patient_deletion_hash( :new.nm_pessoa_fisica, :new.dt_nascimento, :new.cd_pessoa_mae, :new.nm_usuario);
end if;

if (
     (NVL(:old.qt_altura_cm, -999) != NVL(:new.qt_altura_cm, -999)) or
     (NVL(:old.qt_peso, -999) != NVL(:new.qt_peso, -999)) or
     (NVL(:old.ie_sexo, 'NA') != NVL(:new.ie_sexo, 'NA')) or
     (NVL(:old.dt_nascimento, TO_DATE('01010001', 'ddmmyyyy')) != NVL(:new.dt_nascimento, TO_DATE('01010001', 'ddmmyyyy')))
) THEN

BEGIN

    select ap.nr_atendimento, apu.nr_seq_interno
    into bb_nr_atendimento, bb_nr_seq_interno
    from ATENDIMENTO_PACIENTE ap,
         ATEND_PACIENTE_UNIDADE apu,
         UNIDADE_ATENDIMENTO ua
        where ap.nr_atendimento = (
                select MAX(ap1.nr_atendimento) from ATENDIMENTO_PACIENTE ap1
                  where ap1.cd_pessoa_fisica = :new.cd_pessoa_fisica and ap1.dt_alta is null)
          and apu.nr_atendimento = ap.nr_atendimento
          and apu.dt_entrada_unidade = (
              SELECT MAX(dt_entrada_unidade) FROM ATEND_PACIENTE_UNIDADE
              WHERE NR_ATENDIMENTO = ap.nr_atendimento
              AND DT_SAIDA_UNIDADE IS NULL)
          and apu.nr_atendimento = ap.nr_atendimento
          and apu.dt_saida_unidade IS NULL
          and ua.cd_setor_atendimento = apu.cd_setor_atendimento
          and ua.cd_unidade_basica = apu.cd_unidade_basica
          and ua.cd_unidade_compl = apu.cd_unidade_compl
          and ua.nr_seq_classif in (1,3,4);

        IF (:new.ie_sexo = 'M') THEN
            bb_genderID := '4408493246f84a72b83e523e1bcb60b4';

        ELSIF (:new.ie_sexo = 'F') THEN
            bb_genderID := '32ea83413b314be698359feea4c65297';

        ELSIF (:new.ie_sexo = 'A') THEN
            bb_genderID := '259a3c67bc20409b858729c96716e30c';

        ELSE
            bb_genderID := '597b391357864a4697eea517205a2cb4';

        END IF;

        json_demografia := philips_json();
        json_demografia.put('typeID', 'ADT');
        json_demografia.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD HH24:MI:SS.SSSSS'));
        json_demografia.put('messageUsesUtcDateTimes', '1');
        json_demografia.put('patientHealthSystemStayID', LPAD(bb_nr_atendimento, 32, 0));
        json_demografia.put('mostRecentWardStayID', TO_CHAR(bb_nr_seq_interno));
        json_demografia.put('dateOfBirth', TO_CHAR(f_extract_utc_bb(:new.dt_nascimento), 'YYYY-MM-DD'));
        json_demografia.put('genderID', bb_genderID);
        json_demografia.put('admissionWeight', NVL(TO_CHAR(:new.qt_peso), ''));
        json_demografia.put('admissionHeight', NVL(TO_CHAR(:new.qt_altura_cm), ''));

        dbms_lob.createtemporary(envio_integracao_bb, TRUE);
        json_demografia.to_clob(envio_integracao_bb);
        SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Patient_Admission',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;

END IF;

end;
/


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_pf_atual
after update of nm_social, nm_pessoa_fisica, nr_seq_pais, nr_cpf ON TASY.PESSOA_FISICA for each row
declare
ie_efd_icmsipi_w	varchar2(1);
ds_valor_alterior_w varchar2(100);
ds_se_nome_social_w varchar2(1);

begin
obter_param_usuario(5500, 61, obter_perfil_ativo, obter_usuario_ativo , obter_estabelecimento_ativo, ie_efd_icmsipi_w);
ds_se_nome_social_w := pkg_name_utils.get_social_name_enabled(obter_estabelecimento_ativo);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if	(nvl(ie_efd_icmsipi_w,'N') = 'S') then
		ds_valor_alterior_w := '';
		-- REGISTRO 0150.NOME
		if	(ds_se_nome_social_w = 'S') then
			if  (nvl(:new.nm_social,nvl(:new.nm_pessoa_fisica,'XPTO')) <> nvl(:old.nm_social,nvl(:old.nm_pessoa_fisica,'XPTO'))) then
				ds_valor_alterior_w := substr(nvl(:old.nm_social, :old.nm_pessoa_fisica), 1, 100);
			end if;
		elsif (ds_se_nome_social_w = 'T') then
			if  (nvl(:new.nm_social,'XPTO') <> nvl(:old.nm_social,'XPTO'))
			or	(nvl(:new.nm_pessoa_fisica,'XPTO') <> nvl(:old.nm_pessoa_fisica,'XPTO')) then
				if	(:old.nm_social is not null) then
					ds_valor_alterior_w := substr(:old.nm_social || '(' || :old.nm_pessoa_fisica || ')', 1, 100);
				else
					ds_valor_alterior_w := substr(:old.nm_pessoa_fisica, 1, 100);
				end if;
			end if;
		else
			if  (nvl(:new.nm_pessoa_fisica,'XPTO') <> nvl(:old.nm_pessoa_fisica,'XPTO')) then
				ds_valor_alterior_w := substr(:old.nm_pessoa_fisica, 1, 100);
			end if;
		end if;
		if (ds_valor_alterior_w is not null or ds_valor_alterior_w <> '') then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'03', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				ds_valor_alterior_w,
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.COD_PAIS
		if  (nvl(:new.nr_seq_pais,0) <> nvl(:old.nr_seq_pais,0)) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'04', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				to_char(:old.nr_seq_pais),
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.CPF
		if  (nvl(:new.nr_cpf,'XPTO') <> nvl(:old.nr_cpf,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'06', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				elimina_caracteres_especiais(:old.nr_cpf),
				:new.cd_pessoa_fisica);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_SIB_INSERT
AFTER INSERT OR UPDATE ON TASY.PESSOA_FISICA FOR EACH ROW
DECLARE

qt_beneficiarios_plano_w	number(10);
cd_estabelecimento_w		number(10);

/*
IE_ATRIBUTO

1 - NM_PESSOA_FISICA
2 - DT_NASCIMENTO
3 - IE_SEXO
4 - NR_CARTAO_NAC_SUS
6 - CD_NACIONALIDADE
7 - NR_PIS_PASEP
8 - NR_CPF
18-CD_DECLARACAO_NASC_VIVO

*/
begin

qt_beneficiarios_plano_w	:= 0;
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;

if	(cd_estabelecimento_w is null) then
	cd_estabelecimento_w	:= :old.cd_estabelecimento;
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if	(:old.nm_usuario is not null) and (cd_estabelecimento_w is not null) then
		select	count(1)
		into	qt_beneficiarios_plano_w
		from	pls_segurado
		where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
		and	ie_tipo_segurado	in ('A','B','R');

		if	(qt_beneficiarios_plano_w > 0) then
			--Nome da Pessoa
			if	(upper(trim(:old.nm_pessoa_fisica)) <> upper(trim(:new.nm_pessoa_fisica))) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'1');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Data de nascimento
			if	(:old.dt_nascimento <> :new.dt_nascimento) or
				(:old.dt_nascimento is null and :new.dt_nascimento is not null) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'2');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Sexo
			if	(nvl(:old.ie_sexo,'0') <> nvl(:new.ie_sexo,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'3');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Numero do cartao SUS
			if	(nvl(:old.nr_cartao_nac_sus,'0') <> nvl(:new.nr_cartao_nac_sus,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'4');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Nacionalidade
			if	(nvl(:old.cd_nacionalidade,'0') <> nvl(:new.cd_nacionalidade,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'6');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Pis/Pasep
			if	(nvl(:old.nr_pis_pasep,'0') <> nvl(:new.nr_pis_pasep,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'7');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--CPF
			if	(nvl(:old.nr_cpf,'0') <> nvl(:new.nr_cpf,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'8');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
			--Declaracao de nascido vivo
			if	(nvl(:old.cd_declaracao_nasc_vivo,'0') <> nvl(:new.cd_declaracao_nasc_vivo,'0')) then
				begin
				insert into pls_pessoa_fisica_sib
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
				values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
						cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'18');
				exception
				when others then
					cd_estabelecimento_w := cd_estabelecimento_w;
				end;
			end if;
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.PESSOA_FISICA ADD (
  CHECK ( ie_sexo IN ( 'M' , 'F','I' )  ),
  CONSTRAINT PESFISI_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          9M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESFISI_U1
 UNIQUE (NR_PRONTUARIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          9M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESFISI_U2
 UNIQUE (NR_TRANSPLANTE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          512K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESFISI_U3
 UNIQUE (NR_CONTRA_REF_SUS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          448K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA ADD (
  CONSTRAINT PESFISI_CATLINI_FK 
 FOREIGN KEY (NR_SEQ_LINGUA_INDIGENA) 
 REFERENCES TASY.CAT_LINGUA_INDIGENA (NR_SEQUENCIA),
  CONSTRAINT PESFISI_PFFORTRAT_FK 
 FOREIGN KEY (NR_SEQ_FORMA_TRAT) 
 REFERENCES TASY.PF_FORMA_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_SUBSAN_FK 
 FOREIGN KEY (IE_SUBTIPO_SANGUINEO) 
 REFERENCES TASY.SUBTIPO_SANGUINEO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_CONPROF_FK 
 FOREIGN KEY (NR_SEQ_CONSELHO) 
 REFERENCES TASY.CONSELHO_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT PESFISI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESFISI_PEPPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERFIL) 
 REFERENCES TASY.PEP_PERFIL_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PESFISI_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PESFISI_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT PESFISI_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PESFISI_CARTORI_FK 
 FOREIGN KEY (NR_SEQ_CARTORIO_NASC) 
 REFERENCES TASY.CARTORIO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_CARTORI_FK2 
 FOREIGN KEY (NR_SEQ_CARTORIO_CASAMENTO) 
 REFERENCES TASY.CARTORIO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_NUTPERF_FK 
 FOREIGN KEY (NR_SEQ_NUT_PERFIL) 
 REFERENCES TASY.NUT_PERFIL (NR_SEQUENCIA),
  CONSTRAINT PESFISI_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PJ) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA),
  CONSTRAINT PESFISI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PESFISI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_MAE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESFISI_SUSETNI_FK 
 FOREIGN KEY (NR_SEQ_ETNIA) 
 REFERENCES TASY.SUS_ETNIA (NR_SEQUENCIA),
  CONSTRAINT PESFISI_PESJURI_FK 
 FOREIGN KEY (CD_CGC_ORIG_TRANSPL) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESFISI_AGEINSS_FK 
 FOREIGN KEY (NR_SEQ_AGENCIA_INSS) 
 REFERENCES TASY.AGENCIA_INSS (NR_SEQUENCIA),
  CONSTRAINT PESFISI_TIPOBEN_FK 
 FOREIGN KEY (NR_SEQ_TIPO_BENEFICIO) 
 REFERENCES TASY.TIPO_BENEFICIO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_TPINCAP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_INCAPACIDADE) 
 REFERENCES TASY.TIPO_INCAPACIDADE (NR_SEQUENCIA),
  CONSTRAINT PESFISI_FUNPESF_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO_PF) 
 REFERENCES TASY.FUNCAO_PESSOA_FISICA (NR_SEQUENCIA),
  CONSTRAINT PESFISI_TURTRAB_FK 
 FOREIGN KEY (NR_SEQ_TURNO_TRABALHO) 
 REFERENCES TASY.TURNO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_DIRETA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PESFISI_CORCABE_FK 
 FOREIGN KEY (NR_SEQ_COR_CABELO) 
 REFERENCES TASY.COR_CABELO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_COROLHO_FK 
 FOREIGN KEY (NR_SEQ_COR_OLHO) 
 REFERENCES TASY.COR_OLHO (NR_SEQUENCIA),
  CONSTRAINT PESFISI_AGCLSPC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PAC_AGE) 
 REFERENCES TASY.AGENDA_CLASSIF_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PESFISI_PROFISS_FK 
 FOREIGN KEY (CD_ULT_PROFISSAO) 
 REFERENCES TASY.PROFISSAO (CD_PROFISSAO),
  CONSTRAINT PESFISI_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT PESFISI_NACIONA_FK 
 FOREIGN KEY (CD_NACIONALIDADE) 
 REFERENCES TASY.NACIONALIDADE (CD_NACIONALIDADE),
  CONSTRAINT PESFISI_RELIGIA_FK 
 FOREIGN KEY (CD_RELIGIAO) 
 REFERENCES TASY.RELIGIAO (CD_RELIGIAO),
  CONSTRAINT PESFISI_CORPELE_FK 
 FOREIGN KEY (NR_SEQ_COR_PELE) 
 REFERENCES TASY.COR_PELE (NR_SEQUENCIA),
  CONSTRAINT PESFISI_CBORED_FK 
 FOREIGN KEY (CD_CBO_SUS) 
 REFERENCES TASY.CBO_RED (CD_CBO),
  CONSTRAINT PESFISI_ATCNAER_FK 
 FOREIGN KEY (CD_ATIVIDADE_SUS) 
 REFERENCES TASY.ATIVIDADE_CNAER (CD_ATIVIDADE),
  CONSTRAINT PESFISI_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PESFISI_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PESFISI_CHEFIPF_FK 
 FOREIGN KEY (NR_SEQ_CHEFIA) 
 REFERENCES TASY.CHEFIA_PF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_FISICA TO APPSSJ;

GRANT SELECT ON TASY.PESSOA_FISICA TO DWONATAM;

GRANT SELECT ON TASY.PESSOA_FISICA TO NIVEL_1;

GRANT INSERT, SELECT ON TASY.PESSOA_FISICA TO ROBOCMTECNOLOGIA;
GRANT UPDATE (NR_DDD_CELULAR) ON TASY.PESSOA_FISICA TO ROBOCMTECNOLOGIA;
GRANT UPDATE (NR_DDI_CELULAR) ON TASY.PESSOA_FISICA TO ROBOCMTECNOLOGIA;
GRANT UPDATE (NR_TELEFONE_CELULAR) ON TASY.PESSOA_FISICA TO ROBOCMTECNOLOGIA;

GRANT INSERT, UPDATE ON TASY.PESSOA_FISICA TO SAOJOSE;

GRANT SELECT ON TASY.PESSOA_FISICA TO TASY_CONSULTA;

GRANT SELECT ON TASY.PESSOA_FISICA TO WESLLEN;

