ALTER TABLE TASY.INFORM_ADIC_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INFORM_ADIC_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INFORM_ADIC_USUARIO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_USUARIO_INF_ADIC    VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_JURISDICAO      NUMBER(10),
  IE_NIVEL_USUARIO_SEED  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INADUS_CTJURI_FK_I ON TASY.INFORM_ADIC_USUARIO
(NR_SEQ_JURISDICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INADUS_PK ON TASY.INFORM_ADIC_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INADUS_USUARIO_FK_I ON TASY.INFORM_ADIC_USUARIO
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INADUS_USUARIO_FK1_I ON TASY.INFORM_ADIC_USUARIO
(NM_USUARIO_INF_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INFORM_ADIC_USUARIO ADD (
  CONSTRAINT INADUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INFORM_ADIC_USUARIO ADD (
  CONSTRAINT INADUS_CTJURI_FK 
 FOREIGN KEY (NR_SEQ_JURISDICAO) 
 REFERENCES TASY.CAT_JURISDICAO (NR_SEQUENCIA),
  CONSTRAINT INADUS_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT INADUS_USUARIO_FK1 
 FOREIGN KEY (NM_USUARIO_INF_ADIC) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.INFORM_ADIC_USUARIO TO NIVEL_1;

