ALTER TABLE TASY.TITULO_REC_LIQ_CC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_REC_LIQ_CC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_REC_LIQ_CC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_TITULO               NUMBER(10)            NOT NULL,
  NR_SEQ_BAIXA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_CENTRO_CUSTO         NUMBER(15),
  VL_BAIXA                NUMBER(15,2),
  VL_AMAIOR               NUMBER(15,2),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CTA_CTB_ORIGEM       VARCHAR2(20 BYTE),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  VL_RECEBIDO             NUMBER(15,2),
  CD_CONTA_DEB_PLS        VARCHAR2(20 BYTE),
  CD_CONTA_REC_PLS        VARCHAR2(20 BYTE),
  NR_SEQ_MENS_SEG_ITEM    NUMBER(10),
  CD_HISTORICO_PLS        NUMBER(10),
  NR_SEQ_CONTA_PLS        NUMBER(10),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  NR_SEQ_PRODUTO          NUMBER(10),
  VL_ANTECIPACAO_MENS     NUMBER(15,2),
  VL_PRO_RATA             NUMBER(15,2),
  CD_CONTA_ANTEC_PLS      VARCHAR2(20 BYTE),
  CD_HISTORICO_ANTEC_PLS  NUMBER(10),
  IE_LOTE_PRO_RATA        VARCHAR2(3 BYTE),
  VL_CONTAB_PRO_RATA      NUMBER(15,2),
  CD_HISTORICO_REV_ANTEC  NUMBER(10),
  CD_CONTA_REC_ANTECIP    VARCHAR2(20 BYTE),
  CD_CONTA_DEB_ANTECIP    VARCHAR2(20 BYTE),
  IE_ORIGEM_CLASSIF       VARCHAR2(15 BYTE),
  CD_ESTAB_CENTRO_CUSTO   NUMBER(4),
  VL_PERDAS               NUMBER(15,2),
  VL_RECEBIDO_ESTRANG     NUMBER(15,2),
  VL_COTACAO              NUMBER(21,10),
  CD_MOEDA                NUMBER(5),
  VL_COMPLEMENTO          NUMBER(15,2),
  VL_DESCONTO             NUMBER(15,2),
  VL_MULTA                NUMBER(15,2),
  VL_JUROS                NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRELC_CENCUST_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_CONCONT_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_CTA_CTB_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_CONCONT_FK2_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_CONCONT_FK3_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_DEB_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELC_CONCONT_FK4_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_REC_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELC_CONCONT_FK5_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_ANTEC_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CONCONT_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_CONCONT_FK6_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_REC_ANTECIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CONCONT_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_CONCONT_FK7_I ON TASY.TITULO_REC_LIQ_CC
(CD_CONTA_DEB_ANTECIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_CONCONT_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_ESTABEL_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_ESTAB_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELC_HISPADR_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_HISTORICO_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_HISPADR_FK2_I ON TASY.TITULO_REC_LIQ_CC
(CD_HISTORICO_ANTEC_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_HISPADR_FK3_I ON TASY.TITULO_REC_LIQ_CC
(CD_HISTORICO_REV_ANTEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_HISPADR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_MOEDA_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELC_PESFISI_FK_I ON TASY.TITULO_REC_LIQ_CC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITRELC_PK ON TASY.TITULO_REC_LIQ_CC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELC_PLSCOME_FK_I ON TASY.TITULO_REC_LIQ_CC
(NR_SEQ_CONTA_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_PLSMSI_FK_I ON TASY.TITULO_REC_LIQ_CC
(NR_SEQ_MENS_SEG_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_PLSMSI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_PRODFIN_FK_I ON TASY.TITULO_REC_LIQ_CC
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELC_PRODFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELC_TIRELIQ_FK_I ON TASY.TITULO_REC_LIQ_CC
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.conexao3 
BEFORE insert or update ON TASY.TITULO_REC_LIQ_CC FOR EACH ROW
DECLARE

ie_tipo_w varchar2(1);


begin

IF  (inserting) then
 ie_tipo_w := 'I';
else
 ie_tipo_w := 'A';
end if;


 insert into conexao3(dt_atualizacao,
        nm_usuario,
        ds_log,
        ds_stack)
      values(sysdate,
        nvl(wheb_usuario_pck.get_nm_usuario,'Via Banco'),
        'Tipo: '||ie_tipo_w,
        substr(dbms_utility.format_call_stack,1,2000));

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_rec_liq_cc_after_ins_up
after insert or update ON TASY.TITULO_REC_LIQ_CC for each row
declare

ie_contab_baixas_pro_rata_w     pls_parametros_cr.ie_contab_baixas_pro_rata%type;
vl_movimento_w                  ctb_documento.vl_movimento%type;
nr_seq_trans_fin_w              ctb_documento.nr_seq_trans_financ%type;
dt_referencia_w                 pls_mensalidade.dt_referencia%type;
nr_seq_mensalidade_w            pls_mensalidade.nr_sequencia%type;
dt_recebimento_w                titulo_receber_liq.dt_recebimento%type;
dt_referencia_pls_mens_w        titulo_receber_liq.dt_referencia_pls_mens%type;
cd_estabelecimento_w            titulo_receber.cd_estabelecimento%type;
ie_contab_cr_w                  parametro_tesouraria.ie_contab_cr%type;
ie_acao_w                       titulo_receber_liq.ie_acao%type;
nr_multiplicador_w              number(1);
nr_seq_baixa_w                  titulo_receber_liq.nr_sequencia%type;
ie_contab_rec_classif_w         parametro_contas_receber.ie_contab_rec_classif%type;
ie_cc_glosa_w                   parametro_contas_receber.ie_cc_glosa%type;
ie_origem_titulo_w              titulo_receber.ie_origem_titulo%type;
ie_contab_tit_interc_cancel_w	pls_parametro_contabil.ie_contab_tit_interc_cancel%type;
qt_pls_fatura_w                 number(1):= 0;
nr_seq_caixa_rec_w              titulo_receber_liq.nr_seq_caixa_rec%type;
qt_movto_trans_financ_w         number(1):= 0;
nr_seq_trans_caixa_w            titulo_receber_liq.nr_seq_trans_caixa%type;
nr_seq_movto_trans_fin_w        titulo_receber_liq.nr_seq_movto_trans_fin%type;
nr_seq_info_w                   ctb_documento.nr_seq_info%type;
nr_documento_w                  ctb_documento.nr_documento%type;
nr_seq_doc_compl_w              ctb_documento.nr_seq_doc_compl%type;
nm_tabela_w                     ctb_documento.nm_tabela%type;
dt_movimento_w                  ctb_documento.dt_competencia%type;
ie_contab_classif_mens_w        pls_parametro_contabil.ie_contab_classif_mens%type;
nm_atributo_w                   ctb_documento.nm_atributo%type;
qt_ctb_documento_w              number(10);
nr_doc_analitico_w              ctb_documento.nr_doc_analitico%type;
reg_integracao_p                gerar_int_padrao.reg_integracao;
qt_reg_w                        number(10);
ie_concil_contab_w              pls_visible_false.ie_concil_contab%type;
ie_contab_glosa_detalhe_w	ctb_param_lote_contas_rec.ie_contab_glosa_detalhe%type;
ie_contab_tit_cancelado_w	parametro_contas_receber.ie_contab_tit_cancelado%type;

cursor c01 is
        select  a.nm_atributo,
                10 cd_tipo_lote_contab
        from    atributo_contab a
        where   ((a.cd_tipo_lote_contab = 10
                and     a.nm_atributo in ('VL_RECEBIDO_TESOURARIA', 'VL_FATURAMENTO_OPS'))
        or(a.cd_tipo_lote_contab = 5
                and     a.nm_atributo in (      'VL_ORIGINAL_BAIXA','VL_DESCONTOS', 'VL_JUROS',
                                                'VL_MULTA', 'VL_GLOSA', 'VL_REC_MAIOR')))
        union all
        select  a.nm_atributo,
                18 cd_tipo_lote_contab
        from    atributo_contab a
        where   (a.cd_tipo_lote_contab = 18
                and     a.nm_atributo in (      'VL_FATURAMENTO_OPS'))
	union all
	select	a.nm_atributo,
		36 cd_tipo_lote_contab
	from	atributo_contab a
	where	a.cd_tipo_lote_contab = 36
	and	a.nm_atributo = 'VL_RECEBIDO'
	and	ie_concil_contab_w = 'S'
	and	exists	(select	1
			from	titulo_receber	x,
				titulo_receber_liq y
			where	:new.nr_titulo		= x.nr_titulo
			and	:new.nr_seq_baixa	= y.nr_sequencia
			and	x.nr_titulo		= y.nr_titulo
			and	x.ie_origem_titulo	= '8'
			and	x.nr_seq_ptu_fatura is not null
			and	nvl(y.ie_lib_caixa,'N')	= 'S'
			and	((x.ie_situacao		!= '3') or
				(ie_contab_tit_interc_cancel_w = 'S')))
        union all
        select  a.nm_atributo,
                39 cd_tipo_lote_contab
        from    atributo_contab a
        where   (a.cd_tipo_lote_contab = 39
	and     a.nm_atributo in ( 'VL_RECEBIDO', 'VL_PERDAS', 'VL_GLOSA'))
	and	ie_concil_contab_w = 'S'
	and	exists ( select	1
			 from 	titulo_receber_liq y
			 where  y.nr_seq_lote_enc_contas is null
			 and    y.nr_seq_pls_lote_camara is null
			 and	nvl(y.ie_lib_caixa, 'S') = 'S'
			 and	y.nr_titulo 	= :new.nr_titulo
			 and	y.nr_sequencia	= :new.nr_seq_baixa
			 and	exists	(select	1
					from	titulo_receber x
					where	x.nr_titulo	= y.nr_titulo
					and	x.ie_pls	= 'S'
					and	x.ie_origem_titulo = '3'
					and	((x.ie_situacao	<> '3' or ie_contab_tit_cancelado_w = 'S')
					or 	(x.ie_situacao = '3' and y.vl_rec_maior <> 0))))
        union all
        select  a.nm_atributo,
		a.cd_tipo_lote_contab
	from    atributo_contab a
	where   a.cd_tipo_lote_contab = 5
	and     a.nm_atributo in (      'VL_DESCONTOS', 'VL_JUROS', 'VL_MULTA', 'VL_RECEBIDO',
					'VL_GLOSA', 'VL_GLOSA_DETALHE', 'VL_REC_MAIOR',
					'VL_PERDAS', 'VL_RECEBIDO_ESTRANG', 'VL_MOEDA_COMPLEMENTAR');

c01_w           c01%rowtype;

begin

if 	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if (:new.nr_seq_baixa is not null) and (:new.nr_titulo is not null) then

        /*Esse select existe para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
        select  count(*)
        into    qt_reg_w
        from    intpd_fila_transmissao
        where   nr_seq_documento	= to_char(:new.nr_titulo)
        and	nr_seq_item_documento   = to_char(:new.nr_seq_baixa)
        and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

        if (qt_reg_w = 0) then
                reg_integracao_p.nr_seq_item_documento_p        :=      :new.nr_seq_baixa;
                gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
        end if;

	end if;

	begin

	select  a.cd_estabelecimento,
		b.nr_seq_trans_fin,
		a.nr_seq_mensalidade,
		b.dt_recebimento,
		b.dt_referencia_pls_mens,
		b.ie_acao,
		b.nr_sequencia,
		a.ie_origem_titulo,
		b.nr_seq_caixa_rec,
		b.nr_seq_trans_caixa,
		b.nr_seq_movto_trans_fin
	into    cd_estabelecimento_w,
		nr_seq_trans_fin_w,
		nr_seq_mensalidade_w,
		dt_recebimento_w,
		dt_referencia_pls_mens_w,
		ie_acao_w,
		nr_seq_baixa_w,
		ie_origem_titulo_w,
		nr_seq_caixa_rec_w,
		nr_seq_trans_caixa_w,
		nr_seq_movto_trans_fin_w
	from    titulo_receber a,
		titulo_receber_liq b
	where   a.nr_titulo = b.nr_titulo
	and     b.nr_sequencia 	= :new.nr_seq_baixa
	and     a.nr_titulo 	= :new.nr_titulo;

	select	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from	pls_visible_false
	where	cd_estabelecimento = cd_estabelecimento_w;

	if      (inserting) then
			begin

			begin

			select  nvl(ie_contab_cr, 'N')
			into    ie_contab_cr_w
			from    parametro_tesouraria
			where   cd_estabelecimento      = cd_estabelecimento_w;

			exception
			when others then
				ie_contab_cr_w:= 'N';
			end;

			begin

			select  dt_referencia
			into    dt_referencia_w
			from    pls_mensalidade
			where   nr_sequencia = nr_seq_mensalidade_w;

		exception
		when others then
			dt_referencia_w:= sysdate;
		end;

		begin

		select  nvl(ie_contab_rec_classif,'N'),
			nvl(ie_cc_glosa,'N'),
			nvl(ie_contab_tit_cancelado, 'S')
		into    ie_contab_rec_classif_w,
			ie_cc_glosa_w,
			ie_contab_tit_cancelado_w
		from    parametro_contas_receber
		where   cd_estabelecimento      = cd_estabelecimento_w;

		exception
		when others then
			ie_contab_rec_classif_w := 'N';
			ie_cc_glosa_w := 'N';
			ie_contab_tit_cancelado_w := 'S';
		end;

		begin

		select  nvl(max(a.ie_contab_classif_mens),'N')
		into    ie_contab_classif_mens_w
		from    pls_parametro_contabil a
		where   a.cd_estabelecimento    = cd_estabelecimento_w;

		exception
		when others then
			ie_contab_classif_mens_w:= 'N';
		end;

		begin
		select  x.*
		into    ie_contab_rec_classif_w,
				ie_cc_glosa_w,
				ie_contab_glosa_detalhe_w
		from    (
				select  nvl(a.ie_contab_rec_classif, 'N'),
					nvl(a.ie_cc_glosa, 'N'),
					nvl(a.ie_contab_glosa_detalhe, 'N')
				from    ctb_param_lote_contas_rec a
				where   a.cd_empresa    = obter_empresa_estab(cd_estabelecimento_w)
				and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
				and	ie_ctb_online = 'S'
				order   by nvl(a.cd_estab_exclusivo, 0) desc
				) x
		where   rownum = 1;
		exception when others then
			null;
		end;

		nr_multiplicador_w := -1;
		if      (ie_acao_w = 'I') then
			begin
			nr_multiplicador_w := 1;
			end;
		end if;

		select	nvl(max(ie_contab_tit_interc_cancel), 'S')
		into	ie_contab_tit_interc_cancel_w
		from	pls_parametro_contabil
		where 	cd_estabelecimento = cd_estabelecimento_w;

		open c01;
		loop
		fetch c01 into
			c01_w;
		exit when c01%notfound;
				begin

				vl_movimento_w:= 0;

				begin
				nm_atributo_w:= c01_w.nm_atributo;

				if      (((ie_contab_cr_w = 'S') and (c01_w.cd_tipo_lote_contab = 10))
					or ((c01_w.cd_tipo_lote_contab = 18) and (c01_w.nm_atributo = 'VL_FATURAMENTO_OPS'))
					or (c01_w.cd_tipo_lote_contab = 39)
					or (c01_w.cd_tipo_lote_contab = 5))     then
					begin

					vl_movimento_w  := 0;
					vl_movimento_w  :=      case c01_w.nm_atributo
								when 'VL_DEB_ANTECIP' then :new.vl_recebido * nr_multiplicador_w
								when 'VL_DESCONTOS' then :new.vl_desconto * nr_multiplicador_w
								when 'VL_GLOSA' then :new.vl_baixa * nr_multiplicador_w
								when 'VL_GLOSA_DETALHE' then :new.vl_baixa * nr_multiplicador_w
								when 'VL_JUROS' then :new.vl_juros * nr_multiplicador_w
								when 'VL_MOEDA_COMPLEMENTAR' then :new.vl_complemento * nr_multiplicador_w
								when 'VL_MULTA'  then :new.vl_multa * nr_multiplicador_w
								when 'VL_ORIGINAL_BAIXA'  then :new.vl_recebido + :new.vl_juros + :new.vl_multa + :new.vl_desconto
								when 'VL_PERDAS' then :new.vl_perdas * nr_multiplicador_w
								when 'VL_RECEBIDO'  then :new.vl_recebido * nr_multiplicador_w
								when 'VL_RECEBIDO_ESTRANG' then :new.vl_recebido_estrang * nr_multiplicador_w
								when 'VL_RECEBIDO_TESOURARIA'  then :new.vl_recebido * nr_multiplicador_w
								when 'VL_REC_ANTECIP' then :new.vl_recebido * nr_multiplicador_w
								when 'VL_REC_MAIOR' then :new.vl_amaior * nr_multiplicador_w
								when 'VL_PERDAS' then :new.vl_perdas * nr_multiplicador_w
								end;

					if      (c01_w.nm_atributo = 'VL_FATURAMENTO_OPS') then
							begin


							begin

							select  count(1)
							into    qt_pls_fatura_w
							from    pls_fatura w
							where   (w.nr_titulo = :new.nr_titulo or w.nr_titulo_ndc = :new.nr_titulo)
							and     rownum = 1;

							exception
							when others then
									qt_pls_fatura_w:= 0;
							end;


							if      (c01_w.cd_tipo_lote_contab = 10) then
									begin

									begin

									select  count(1)
									into    qt_movto_trans_financ_w
									from    movto_trans_financ x
									where   x.nr_sequencia          = nr_seq_movto_trans_fin_w
									and     x.nr_seq_saldo_caixa    is not null
									and     rownum = 1;

									exception
									when others then
											qt_movto_trans_financ_w:= 0;
									end;


									if      (ie_cc_glosa_w <> 'N') and
											((:new.ie_origem_classif = 'FO') or (ie_origem_titulo_w = 13) and (qt_pls_fatura_w = 0)) and
											((nvl(nr_seq_caixa_rec_w, 0) <> 0) or (qt_movto_trans_financ_w > 0) or (nvl(nr_seq_trans_caixa_w, 0) <> 0))then
											begin
													vl_movimento_w:= :new.vl_amaior;

											end;
									end if;

									end;
							elsif   (c01_w.cd_tipo_lote_contab = 18) then
									begin

									if      ((:new.ie_origem_classif = 'FO') or (ie_origem_titulo_w = 13) and (qt_pls_fatura_w = 0)) then
											begin

											vl_movimento_w:= :new.vl_recebido * nr_multiplicador_w;

											end;
									end if;

									end;
							elsif   (c01_w.cd_tipo_lote_contab = 5) then
									begin

									if      ((:new.ie_origem_classif = 'FO')  or ((ie_origem_titulo_w = 13) and (qt_pls_fatura_w  = 0))) then
											begin

											vl_movimento_w:= :new.vl_recebido * nr_multiplicador_w;

											end;
									end if;

									end;
							end if;


							end;
					end if;

					nr_seq_info_w := 14;
					dt_movimento_w := dt_recebimento_w;

					if      (c01_w.cd_tipo_lote_contab = 10)  then
							begin

							delete
							from    ctb_documento
							where   nm_atributo = c01_w.nm_atributo
							and     nr_documento = :new.nr_titulo
							and     dt_competencia = dt_recebimento_w
							and     nr_seq_doc_compl = nr_seq_baixa_w;

							nr_documento_w := :new.nr_titulo;
							nr_seq_doc_compl_w := :new.nr_seq_baixa;
							nr_doc_analitico_w := :new.nr_sequencia;
							nm_tabela_w := 'TITULO_RECEBER_LIQ_CONTAB_V2';

							end;
					elsif   ((c01_w.cd_tipo_lote_contab = 18) or (c01_w.cd_tipo_lote_contab = 39)) then
						begin

						nr_documento_w := :new.nr_titulo;
						nr_seq_doc_compl_w := :new.nr_seq_baixa;
						nr_doc_analitico_w := :new.nr_sequencia;
						nm_tabela_w := 'TITULO_REC_LIQ_CC';

						if	(c01_w.nm_atributo = 'VL_GLOSA' and ie_cc_glosa_w = 'N') then
							vl_movimento_w := null;
						end if;

						if 	(c01_w.cd_tipo_lote_contab = 39
							and ie_contab_rec_classif_w = 'N'
							and c01_w.nm_atributo = 'VL_RECEBIDO') then
							vl_movimento_w := null;
						end if;
						end;

					elsif   (c01_w.cd_tipo_lote_contab = 5) then
							begin

							nr_documento_w 		:= :new.nr_titulo;
							nr_seq_doc_compl_w 	:= :new.nr_seq_baixa;
							nr_doc_analitico_w 	:= :new.nr_sequencia;

							nm_tabela_w 		:=  case c01_w.nm_atributo
													when 'VL_DEB_ANTECIP' then 'TITULO_RECEBER_LIQ_CC'
													when 'VL_DESCONTOS' then 'TITULO_RECEBER_LIQ_CONTAB_V2'
													when 'VL_FATURAMENTO_OPS' then 'TITULO_RECEBER_LIQ_CC'
													when 'VL_GLOSA' then 'TITULO_REC_LIQ_CC'
													when 'VL_GLOSA_DETALHE' then 'TITULO_RECEBER_LIQ_CC'
													when 'VL_JUROS' then 'TITULO_RECEBER_LIQ_CONTAB_V2'
													when 'VL_MOEDA_COMPLEMENTAR' then 'TITULO_REC_LIQ_CC'
													when 'VL_MULTA'  then 'TITULO_RECEBER_LIQ_CONTAB_V2'
													when 'VL_PERDAS' then 'TITULO_REC_LIQ_CC'
													when 'VL_RECEBIDO' then 'TITULO_REC_LIQ_CC'
													when 'VL_RECEBIDO_ESTRANG' then 'TITULO_REC_LIQ_CC'
													when 'VL_REC_ANTECIP' then 'TITULO_RECEBER_LIQ_CC'
													when 'VL_REC_MAIOR' then 'TITULO_RECEBER_LIQ_CC'
													end;

							if      (c01_w.nm_atributo in ('VL_DESCONTOS', 'VL_JUROS', 'VL_MOEDA_COMPLEMENTAR', 'VL_MULTA', 'VL_PERDAS', 'VL_RECEBIDO_ESTRANG')) then
									begin
									delete
									from    ctb_documento
									where   nm_atributo 			= c01_w.nm_atributo
									and     nr_documento 			= nr_documento_w
									and     nr_seq_doc_compl 		= nr_seq_doc_compl_w
									and	nvl(nr_doc_analitico,0)		= 0
									and     dt_competencia 			= trunc(dt_recebimento_w)
									and	ie_situacao_ctb			= 'N'
									and	nvl(nr_lote_contabil,0)	= 0;
									end;
							end if;

							if      (c01_w.nm_atributo  = 'VL_DEB_ANTECIP') then
									begin
									nr_seq_info_w:= 50;
									end;
							elsif   (c01_w.nm_atributo in ('VL_REC_ANTECIP', 'VL_MOEDA_COMPLEMENTAR', 'VL_RECEBIDO_ESTRANG')) then
									begin
									nr_seq_info_w:= 49;
									end;
							elsif   (c01_w.nm_atributo = 'VL_RECEBIDO') then
									begin
									if      (((:new.ie_lote_pro_rata is not null) and (ie_contab_rec_classif_w = 'S')) or (ie_contab_rec_classif_w = 'N')) then
											begin

											vl_movimento_w := null;

											end;
									else
											begin
											delete
											from    ctb_documento
											where   nm_atributo 			= c01_w.nm_atributo
											and     nr_documento 			= nr_documento_w
											and     nr_seq_doc_compl 		= nr_seq_doc_compl_w
											and	nvl(nr_doc_analitico,0)	= 0
											and     dt_competencia 			= trunc(dt_recebimento_w)
											and	ie_situacao_ctb			= 'N'
											and	nm_tabela				= 'TITULO_RECEBER_LIQ'
											and	nvl(nr_lote_contabil,0)	= 0;
											end;
									end if;
									end;
							elsif	(c01_w.nm_atributo in ('VL_GLOSA', 'VL_REC_MAIOR') and ie_cc_glosa_w = 'N') then
									begin
									vl_movimento_w := null;
									end;
							elsif	(c01_w.nm_atributo = 'VL_GLOSA_DETALHE' and (ie_cc_glosa_w = 'N' or ie_contab_glosa_detalhe_w = 'N')) then
									begin
									vl_movimento_w := null;
									end;
							end if;

							end;
					end if;

					end;
				end if;

				if      (ie_contab_rec_classif_w = 'S') and
							((c01_w.cd_tipo_lote_contab = 10) or
							((c01_w.cd_tipo_lote_contab = 39) and (c01_w.nm_atributo = 'VL_RECEBIDO'))) then
						begin

						delete  from    ctb_documento
						where   nm_atributo = c01_w.nm_atributo
						and     nr_documento = :new.nr_titulo
						and     dt_competencia = dt_recebimento_w
						and     nr_seq_doc_compl = nr_seq_baixa_w;


						nr_seq_info_w := 14;
						nr_documento_w := :new.nr_titulo;
						nr_seq_doc_compl_w := :new.nr_seq_baixa;
						nr_doc_analitico_w := :new.nr_sequencia;
						nm_tabela_w := 'TITULO_REC_LIQ_CC';
						dt_movimento_w := dt_recebimento_w;
						vl_movimento_w:= :new.vl_recebido * nr_multiplicador_w;

						if      (nvl(vl_movimento_w, 0) <> 0) then
								begin

								ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
															dt_movimento_w,
															c01_w.cd_tipo_lote_contab,
															nr_seq_trans_fin_w,
															nr_seq_info_w,
															nr_documento_w,
															nr_seq_doc_compl_w,
															nr_doc_analitico_w,
															vl_movimento_w,
															nm_tabela_w,
															nm_atributo_w,
															:new.nm_usuario);
								end;
						end if;
						vl_movimento_w:= null;
						end;
				end if;

				if 	(c01_w.cd_tipo_lote_contab = 36) then
					nr_seq_info_w 		:= 14;
					nm_tabela_w 		:= 'TITULO_REC_LIQ_CC';
					dt_movimento_w 		:= dt_recebimento_w;
					nm_atributo_w		:= c01_w.nm_atributo;
					nr_documento_w 		:= :new.nr_titulo;
					nr_seq_doc_compl_w 	:= :new.nr_seq_baixa;
					nr_doc_analitico_w 	:= :new.nr_sequencia;
					vl_movimento_w 		:= :new.vl_recebido * nr_multiplicador_w;
				end if;
				end;

				if      (nvl(vl_movimento_w, 0) <> 0) then
					begin

					ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
												dt_movimento_w,
												c01_w.cd_tipo_lote_contab,
												nr_seq_trans_fin_w,
												nr_seq_info_w,
												nr_documento_w,
												nr_seq_doc_compl_w,
												nr_doc_analitico_w,
												vl_movimento_w,
												nm_tabela_w,
												nm_atributo_w,
												:new.nm_usuario);
					end;
				end if;


				end;
		end loop;
		close c01;
		end;
	end if;

	if      (updating) then
			begin

				if      (:new.vl_amaior <> :old.vl_amaior) then
						begin

								update  ctb_documento
								set     vl_movimento = :new.vl_amaior
								where   nr_documento = :new.nr_titulo
								and     nr_seq_doc_compl = :new.nr_sequencia;

						end;
				end if;

				open c01;
				loop
				fetch c01 into
						c01_w;
				exit when c01%notfound;
						begin
						qt_ctb_documento_w:= 0;

						if      (c01_w.nm_atributo = 'VL_PERDAS') and (nvl(:new.vl_perdas, 0) <> nvl(:old.vl_perdas, 0))then
								begin


								select  count(1)
								into    qt_ctb_documento_w
								from    ctb_documento
								where   nm_atributo = c01_w.nm_atributo
								and     nm_tabela = 'TITULO_REC_LIQ_CC'
								and     dt_competencia = dt_recebimento_w
								and     nr_Seq_info = 14
								and     nr_documento = :new.nr_titulo
								and     nr_seq_doc_compl = nr_seq_baixa_w
								and     nr_doc_analitico = :new.nr_sequencia
								and     cd_estabelecimento = cd_estabelecimento_w;

								if      (nvl(qt_ctb_documento_w, 0) = 0) then
										begin


										ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
																	dt_recebimento_w,
																	c01_w.cd_tipo_lote_contab,
																	nr_seq_trans_fin_w,
																	14,
																	:new.nr_sequencia,
																	nr_seq_baixa_w,
																	nr_doc_analitico_w,
																	:new.vl_perdas,
																	'TITULO_REC_LIQ_CC',
																	c01_w.nm_atributo,
																	:new.nm_usuario);

										end;
								else
										begin

										update  ctb_documento
										set     vl_movimento = :new.vl_perdas
										where   nm_atributo = c01_w.nm_atributo
										and     nm_tabela = 'TITULO_REC_LIQ_CC'
										and     dt_competencia = dt_recebimento_w
										and     nr_Seq_info = 14
										and     nr_documento = :new.nr_titulo
										and     nr_seq_doc_compl = nr_seq_baixa_w
										and     nr_doc_analitico = :new.nr_sequencia
										and     cd_estabelecimento = cd_estabelecimento_w;

										end;
								end if;

								end;
						elsif   ((c01_w.nm_atributo = 'VL_DESCONTOS') and (nvl(:new.vl_desconto, 0) <> nvl(:old.vl_desconto, 0)))
								or ((c01_w.nm_atributo = 'VL_MULTA')and (nvl(:new.vl_multa, 0) <> nvl(:old.vl_multa, 0)))
								or ((c01_w.nm_atributo = 'VL_JUROS') and (nvl(:new.vl_juros, 0) <> nvl(:old.vl_juros, 0))) then
								begin

								vl_movimento_w:=        case    c01_w.nm_atributo
														when    'VL_DESCONTOS' then :new.vl_desconto
														when    'VL_MULTA' then :new.vl_multa
														when    'VL_JUROS' then :new.vl_juros
														end;

								update  ctb_documento
								set     vl_movimento = vl_movimento_w
								where   nm_atributo = c01_w.nm_atributo
								and     nm_tabela = 'TITULO_RECEBER_LIQ_CONTAB_V2'
								and     dt_competencia = dt_recebimento_w
								and     nr_Seq_info = 14
								and     nr_documento = :new.nr_titulo
								and     nr_seq_doc_compl = nr_seq_baixa_w
								and     nr_doc_analitico = :new.nr_sequencia
								and     cd_estabelecimento = cd_estabelecimento_w;

								end;
						elsif   (((c01_w.nm_atributo = 'VL_RECEBIDO') or (c01_w.nm_atributo = 'VL_REC_ANTECIP')) and (nvl(:new.vl_recebido, 0) <> nvl(:old.vl_recebido, 0))) then
								begin

								update  ctb_documento
								set     vl_movimento = :new.vl_recebido
								where   nm_atributo = c01_w.nm_atributo
								and     nm_tabela = 'TITULO_REC_LIQ_CC'
								and     dt_competencia = dt_recebimento_w
								and     nr_Seq_info = 14
								and     nr_documento = :new.nr_titulo
								and     nr_seq_doc_compl = nr_seq_baixa_w
								and     nr_doc_analitico = :new.nr_sequencia
								and     cd_estabelecimento = cd_estabelecimento_w;

								end;
						end if;

						end;
				end loop;
				close c01;

			end;
	end if;
	end;
end if;
end;
/


ALTER TABLE TASY.TITULO_REC_LIQ_CC ADD (
  CONSTRAINT TITRELC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_REC_LIQ_CC ADD (
  CONSTRAINT TITRELC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_CENTRO_CUSTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TITRELC_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITRELC_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TITRELC_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_ANTEC_PLS) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TITRELC_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_ANTEC_PLS) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_REV_ANTEC) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TITRELC_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_REC_ANTECIP) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_CONCONT_FK7 
 FOREIGN KEY (CD_CONTA_DEB_ANTECIP) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITRELC_CONCONT_FK 
 FOREIGN KEY (CD_CTA_CTB_ORIGEM) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_CONCONT_FK3 
 FOREIGN KEY (CD_CONTA_DEB_PLS) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_CONCONT_FK4 
 FOREIGN KEY (CD_CONTA_REC_PLS) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITRELC_PLSMSI_FK 
 FOREIGN KEY (NR_SEQ_MENS_SEG_ITEM) 
 REFERENCES TASY.PLS_MENSALIDADE_SEG_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITRELC_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO_PLS) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TITRELC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TITRELC_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PLS) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT TITRELC_PRODFIN_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_FINANCEIRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_REC_LIQ_CC TO NIVEL_1;

