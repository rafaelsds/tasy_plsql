ALTER TABLE TASY.PLS_CONV_SERV_EXTRAT_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONV_SERV_EXTRAT_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONV_SERV_EXTRAT_MENS
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL              NUMBER(10),
  CD_SERVICO_DEST              VARCHAR2(30 BYTE),
  DS_SERVICO_DEST              VARCHAR2(255 BYTE) NOT NULL,
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  IE_TIPO_DESPESA              VARCHAR2(1 BYTE),
  CD_AREA_PROCEDIMENTO         NUMBER(15),
  CD_ESPECIALIDADE             NUMBER(15),
  CD_GRUPO_PROC                NUMBER(15),
  IE_ATEND_INTERNACAO          VARCHAR2(1 BYTE),
  IE_BENEF_COOPERADO           VARCHAR2(1 BYTE),
  IE_PRESTADOR_EXEC_COOPERADO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCSEM_AREPROC_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCSEM_ESPPROC_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCSEM_ESTABEL_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCSEM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCSEM_GRUPROC_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCSEM_I1 ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(NVL("NR_SEQ_MATERIAL",1))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCSEM_I2 ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(NVL("CD_PROCEDIMENTO",1), NVL("IE_ORIGEM_PROCED",1))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCSEM_PK ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCSEM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCSEM_PLSMAT_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCSEM_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCSEM_PROCEDI_FK_I ON TASY.PLS_CONV_SERV_EXTRAT_MENS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCSEM_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_conv_serv_extrat_mensatual
before insert or update ON TASY.PLS_CONV_SERV_EXTRAT_MENS for each row
declare

begin

if	((:new.cd_procedimento 		is not null) or
	(:new.cd_area_procedimento 	is not null) or
	(:new.cd_especialidade 		is not null) or
	(:new.cd_grupo_proc 		is not null) or
	(:new.ie_tipo_despesa 		is not null)) and
	(:new.nr_seq_material 		is not null) then
	-- N�o pode ser criada uma regra com material e procedimento
	wheb_mensagem_pck.exibir_mensagem_abort(266790);
end if;

if	(:new.cd_procedimento 		is null) and
	(:new.cd_area_procedimento 	is null) and
	(:new.cd_especialidade 		is null) and
	(:new.cd_grupo_proc 		is null) and
	(:new.ie_tipo_despesa 		is null) and
	(:new.nr_seq_material 		is null) and
	(nvl(:new.ie_atend_internacao,'N') = 'N') then
	-- N�o pode ser criada uma regra vazia.
	wheb_mensagem_pck.exibir_mensagem_abort(266791);
end if;

if	(:new.ie_atend_internacao = 'S') and
	((:new.cd_procedimento 		is not null) or
	(:new.cd_area_procedimento 	is not null) or
	(:new.cd_especialidade 		is not null) or
	(:new.cd_grupo_proc 		is not null) or
	(:new.ie_tipo_despesa 		is not null) or
	(:new.nr_seq_material 		is not null) or
	(:new.ie_benef_cooperado <> 'A') or
	(:new.ie_prestador_exec_cooperado <> 'A')) then
	--N�o � possivel criar uma regra com mais de um campo marcado al�m do campo "Identificar atendimento interna��o"
	wheb_mensagem_pck.exibir_mensagem_abort(388483);
end if;

end;
/


ALTER TABLE TASY.PLS_CONV_SERV_EXTRAT_MENS ADD (
  CONSTRAINT PLSCSEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONV_SERV_EXTRAT_MENS ADD (
  CONSTRAINT PLSCSEM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCSEM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSCSEM_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCSEM_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSCSEM_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSCSEM_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC));

GRANT SELECT ON TASY.PLS_CONV_SERV_EXTRAT_MENS TO NIVEL_1;

