ALTER TABLE TASY.FORMA_PAGTO_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMA_PAGTO_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMA_PAGTO_REGRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_FORMA          NUMBER(10)              NOT NULL,
  NR_SEQ_BANDEIRA       NUMBER(10)              NOT NULL,
  IE_TIPO_CARTAO        VARCHAR2(1 BYTE)        NOT NULL,
  TX_ADMINISTRACAO      NUMBER(7,4)             NOT NULL,
  QT_DIAS_VENC          NUMBER(4)               NOT NULL,
  IE_ARREDONDAMENTO     VARCHAR2(1 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA    DATE                    NOT NULL,
  IE_DIAS_UTEIS         VARCHAR2(1 BYTE)        NOT NULL,
  NR_PARCELA_INICIO     NUMBER(5)               NOT NULL,
  NR_PARCELA_FIM        NUMBER(5)               NOT NULL,
  VL_TRANSACAO_INICIO   NUMBER(15,2),
  VL_TRANSACAO_FIM      NUMBER(15,2),
  VL_MINIMO_PARCELA     NUMBER(15,2),
  IE_PARCELA_INF        VARCHAR2(1 BYTE)        NOT NULL,
  QT_PARCELA            NUMBER(10),
  NR_DIA_FIXO           NUMBER(2),
  IE_PROXIMO_MES        VARCHAR2(1 BYTE),
  IE_REGRA_POR_PARCELA  VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  VL_MINIMO_TAXA        NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FPAGREG_BANCACR_FK_I ON TASY.FORMA_PAGTO_REGRA
(NR_SEQ_BANDEIRA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FPAGREG_ESTABEL_FK_I ON TASY.FORMA_PAGTO_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FPAGREG_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FPAGREG_FOPAGCR_FK_I ON TASY.FORMA_PAGTO_REGRA
(NR_SEQ_FORMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FPAGREG_FOPAGCR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FPAGREG_PK ON TASY.FORMA_PAGTO_REGRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FPAGREG_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.FORMA_PAGTO_REGRA_tp  after update ON TASY.FORMA_PAGTO_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_BANDEIRA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_BANDEIRA,1,4000),substr(:new.NR_SEQ_BANDEIRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_BANDEIRA',ie_log_w,ds_w,'FORMA_PAGTO_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.TX_ADMINISTRACAO,1,500);gravar_log_alteracao(substr(:old.TX_ADMINISTRACAO,1,4000),substr(:new.TX_ADMINISTRACAO,1,4000),:new.nm_usuario,nr_seq_w,'TX_ADMINISTRACAO',ie_log_w,ds_w,'FORMA_PAGTO_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_CARTAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_CARTAO,1,4000),substr(:new.IE_TIPO_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CARTAO',ie_log_w,ds_w,'FORMA_PAGTO_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FORMA_PAGTO_REGRA ADD (
  CONSTRAINT FPAGREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMA_PAGTO_REGRA ADD (
  CONSTRAINT FPAGREG_FOPAGCR_FK 
 FOREIGN KEY (NR_SEQ_FORMA) 
 REFERENCES TASY.FORMA_PAGTO_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT FPAGREG_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT FPAGREG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.FORMA_PAGTO_REGRA TO NIVEL_1;

