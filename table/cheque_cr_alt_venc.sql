ALTER TABLE TASY.CHEQUE_CR_ALT_VENC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE_CR_ALT_VENC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE_CR_ALT_VENC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CHEQUE        NUMBER(10)               NOT NULL,
  DT_ANTERIOR          DATE                     NOT NULL,
  DT_VENCIMENTO_ATUAL  DATE                     NOT NULL,
  CD_MOTIVO            VARCHAR2(5 BYTE)         NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHALTVE_CHEQUES_FK_I ON TASY.CHEQUE_CR_ALT_VENC
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHALTVE_PK ON TASY.CHEQUE_CR_ALT_VENC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHALTVE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cheque_cr_alt_venc_insert
after insert ON TASY.CHEQUE_CR_ALT_VENC for each row
declare

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
update	cheque_cr
set    	dt_vencimento_atual = :new.dt_vencimento_atual,
	dt_atualizacao	= sysdate,
	nm_usuario	= :new.nm_usuario
where  	nr_seq_cheque	= :new.nr_seq_cheque;

end if;
end cheque_cr_alt_venc_insert;
/


ALTER TABLE TASY.CHEQUE_CR_ALT_VENC ADD (
  CONSTRAINT CHALTVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE_CR_ALT_VENC ADD (
  CONSTRAINT CHALTVE_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CHEQUE_CR_ALT_VENC TO NIVEL_1;

