ALTER TABLE TASY.SIP_NV_TEMPO_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIP_NV_TEMPO_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIP_NV_TEMPO_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  DT_INICIO            DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_FIM               DATE,
  DS_SQL               CLOB                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_SQL) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SINTREG_PK ON TASY.SIP_NV_TEMPO_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SINTREG_PLSLSIP_FK_I ON TASY.SIP_NV_TEMPO_REGRA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SINTREG_SIASRNV_FK_I ON TASY.SIP_NV_TEMPO_REGRA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SIP_NV_TEMPO_REGRA ADD (
  CONSTRAINT SINTREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIP_NV_TEMPO_REGRA ADD (
  CONSTRAINT SINTREG_PLSLSIP_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_SIP (NR_SEQUENCIA),
  CONSTRAINT SINTREG_SIASRNV_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.SIP_ITEM_ASSIST_REGRA_NV (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIP_NV_TEMPO_REGRA TO NIVEL_1;

