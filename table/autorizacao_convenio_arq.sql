ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONVENIO_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONVENIO_ARQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(110 BYTE),
  NR_SEQUENCIA_AUTOR   NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  IE_ANEXAR_EMAIL      VARCHAR2(1 BYTE)         NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUCOARQ_AUTCONV_FK_I ON TASY.AUTORIZACAO_CONVENIO_ARQ
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCOARQ_AUTCONV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUCOARQ_PK ON TASY.AUTORIZACAO_CONVENIO_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUCOARQ_TPANAUCO_FK_I ON TASY.AUTORIZACAO_CONVENIO_ARQ
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUCOARQ_TPANAUCO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.autorizacao_convenio_arq_aftup
after update ON TASY.AUTORIZACAO_CONVENIO_ARQ for each row
declare
ds_titulo_w			varchar2(255)	:= wheb_mensagem_pck.get_texto(312534);
ds_historico_w			varchar2(4000)	:= '';
ds_texto_campo_w		varchar2(20) := wheb_mensagem_pck.get_texto(182370);
ds_texto_antes_w		varchar2(20) := wheb_mensagem_pck.get_texto(312535);
ds_texto_depois_w		varchar2(20) := wheb_mensagem_pck.get_texto(182372);
begin

begin
if	(nvl(:new.ds_arquivo,0) <> nvl(:old.ds_arquivo,0)) then
	ds_historico_w := substr(ds_historico_w||ds_texto_campo_w|| ' DS_ARQUIVO - '||  ds_texto_antes_w || :old.ds_arquivo ||' '||ds_texto_depois_w||' '|| :new.ds_arquivo ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.ie_anexar_email,0) <> nvl(:old.ie_anexar_email,0)) then
	ds_historico_w := substr(ds_historico_w||ds_texto_campo_w|| ' IE_ANEXAR_EMAIL - ' || ds_texto_antes_w || :old.ie_anexar_email ||' '||ds_texto_Depois_w||' '|| :new.ie_anexar_email ||chr(13)||chr(10),1,4000);
end if;
exception
when others then
	ds_historico_w := 'X';
end;

if	(nvl(ds_historico_w,'X') <> 'X') then
	gravar_autor_conv_log_alter(:new.nr_sequencia_autor,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ARQ ADD (
  CONSTRAINT AUCOARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONVENIO_ARQ ADD (
  CONSTRAINT AUCOARQ_TPANAUCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ANEXO_AUTOR_CONV (NR_SEQUENCIA),
  CONSTRAINT AUCOARQ_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AUTORIZACAO_CONVENIO_ARQ TO NIVEL_1;

