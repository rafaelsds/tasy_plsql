ALTER TABLE TASY.SL_ITEM_CHECK_LIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SL_ITEM_CHECK_LIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SL_ITEM_CHECK_LIST
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_ITEM                  VARCHAR2(255 BYTE)   NOT NULL,
  NR_SEQ_GRUPO_CHECK_LIST  NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_MATERIAL              NUMBER(10),
  NR_SEQ_APRESENTACAO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SLITCHL_MATERIA_FK_I ON TASY.SL_ITEM_CHECK_LIST
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SLITCHL_PK ON TASY.SL_ITEM_CHECK_LIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLITCHL_SLGRCHL_FK_I ON TASY.SL_ITEM_CHECK_LIST
(NR_SEQ_GRUPO_CHECK_LIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SL_ITEM_CHECK_LIST ADD (
  CONSTRAINT SLITCHL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SL_ITEM_CHECK_LIST ADD (
  CONSTRAINT SLITCHL_SLGRCHL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CHECK_LIST) 
 REFERENCES TASY.SL_GRUPO_CHECK_LIST (NR_SEQUENCIA),
  CONSTRAINT SLITCHL_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.SL_ITEM_CHECK_LIST TO NIVEL_1;

