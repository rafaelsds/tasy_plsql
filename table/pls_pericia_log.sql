ALTER TABLE TASY.PLS_PERICIA_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PERICIA_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PERICIA_LOG
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_SEQ_PERICIA  NUMBER(10)                    NOT NULL,
  DS_LOG          VARCHAR2(4000 BYTE)           NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPELO_PK ON TASY.PLS_PERICIA_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PERICIA_LOG ADD (
  CONSTRAINT PLSPELO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_PERICIA_LOG TO NIVEL_1;

