ALTER TABLE TASY.DIS_REGRA_LOCAL_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIS_REGRA_LOCAL_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIS_REGRA_LOCAL_USUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_USUARIO_REGRA     VARCHAR2(15 BYTE)        NOT NULL,
  CD_LOCAL_ESTOQUE     NUMBER(4)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DISREGUSU_DISPAIN_FK_I ON TASY.DIS_REGRA_LOCAL_USUARIO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DISREGUSU_ESTABEL_FK_I ON TASY.DIS_REGRA_LOCAL_USUARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DISREGUSU_LOCESTO_FK_I ON TASY.DIS_REGRA_LOCAL_USUARIO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DISREGUSU_PK ON TASY.DIS_REGRA_LOCAL_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.dis_local_usuario_atual
before insert or update or delete ON TASY.DIS_REGRA_LOCAL_USUARIO for each row
declare

qt_existe_w		number(10);

pragma autonomous_transaction;

begin
if	(inserting) and
	(gerar_int_dankia_pck.get_ie_int_dankia = 'S') then
	gerar_int_dankia_pck.dankia_permissao_usuario_local(:new.nm_usuario_regra, :new.cd_local_estoque, 'I');
	select	count(1)
	into	qt_existe_w
	from	dis_regra_local_usuario
	where	nm_usuario_regra = :new.nm_usuario_regra
	and	nr_sequencia <> :new.nr_sequencia;
	if	(qt_existe_w = 0) then
		gerar_int_dankia_pck.dankia_inserir_funcionario(:new.nm_usuario_regra, 'I');
	end if;
elsif	(deleting) and
	(gerar_int_dankia_pck.get_ie_int_dankia = 'S') then
	gerar_int_dankia_pck.dankia_permissao_usuario_local(:old.nm_usuario_regra, :old.cd_local_estoque, 'E');
	select	count(1)
	into	qt_existe_w
	from	dis_regra_local_usuario
	where	nm_usuario_regra = :old.nm_usuario_regra
	and		nr_sequencia <> :old.nr_sequencia;
	if	(qt_existe_w = 0) then
		gerar_int_dankia_pck.dankia_inserir_funcionario(:old.nm_usuario_regra, 'E');
	end if;
elsif	(updating) then
	wheb_mensagem_pck.exibir_mensagem_abort(403431);
end if;

commit;

end;
/


ALTER TABLE TASY.DIS_REGRA_LOCAL_USUARIO ADD (
  CONSTRAINT DISREGUSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIS_REGRA_LOCAL_USUARIO ADD (
  CONSTRAINT DISREGUSU_DISPAIN_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.DIS_PARAMETROS_INT (NR_SEQUENCIA),
  CONSTRAINT DISREGUSU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DISREGUSU_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE));

GRANT SELECT ON TASY.DIS_REGRA_LOCAL_USUARIO TO NIVEL_1;

