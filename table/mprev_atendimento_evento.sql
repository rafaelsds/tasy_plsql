ALTER TABLE TASY.MPREV_ATENDIMENTO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_ATENDIMENTO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_ATENDIMENTO_EVENTO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_PACIENTE  NUMBER(10),
  NR_SEQ_TIPO_EVENTO     NUMBER(10),
  NR_SEQ_STATUS          NUMBER(10),
  DT_INICIO_EVENTO       DATE                   NOT NULL,
  DT_FIM_EVENTO          DATE,
  CD_PROCEDIMENTO        NUMBER(15)             NOT NULL,
  IE_ORIGEM_PROCED       NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  IE_PROF_ADIC           VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_ATENDIMENTO     NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRATEV_MPRATEN_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEV_MPRATPA_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(NR_SEQ_ATEND_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEV_MPRSTAT_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEV_MPRTIEA_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(NR_SEQ_TIPO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEV_PESFISI_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRATEV_PK ON TASY.MPREV_ATENDIMENTO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEV_PROCEDI_FK_I ON TASY.MPREV_ATENDIMENTO_EVENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.mprev_atend_evento_insert
before insert or update ON TASY.MPREV_ATENDIMENTO_EVENTO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Apenas verificar se para o atendimento do tipo Coletivo foi informado uma turma.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	mprev_atendimento a
where	a.nr_sequencia = :new.nr_seq_atendimento;

if	(:new.ie_origem_proced is null) then
	:new.ie_origem_proced	:= PLS_OBTER_ORIGEM_PROCED(cd_estabelecimento_w,null,null,:new.dt_inicio_evento,null);
end if;

end;
/


ALTER TABLE TASY.MPREV_ATENDIMENTO_EVENTO ADD (
  CONSTRAINT MPRATEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_ATENDIMENTO_EVENTO ADD (
  CONSTRAINT MPRATEV_MPRATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.MPREV_ATENDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRATEV_MPRATPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PACIENTE) 
 REFERENCES TASY.MPREV_ATEND_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT MPRATEV_MPRSTAT_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.MPREV_STATUS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT MPRATEV_MPRTIEA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EVENTO) 
 REFERENCES TASY.MPREV_TIPO_EVENTO_ATEND (NR_SEQUENCIA),
  CONSTRAINT MPRATEV_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MPRATEV_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.MPREV_ATENDIMENTO_EVENTO TO NIVEL_1;

