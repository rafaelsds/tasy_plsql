ALTER TABLE TASY.PLS_EXEC_CIRURGICA_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EXEC_CIRURGICA_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EXEC_CIRURGICA_LOG
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_LOG                 VARCHAR2(4000 BYTE),
  NR_SEQ_EXEC_CIRURGICA  NUMBER(10)             NOT NULL,
  IE_TIPO_LOG            VARCHAR2(1 BYTE),
  NR_SEQ_EXEC_CIR_GUIA   NUMBER(20),
  NR_SEQ_CONTA           NUMBER(20)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSECL_ESTABEL_FK_I ON TASY.PLS_EXEC_CIRURGICA_LOG
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSECL_PK ON TASY.PLS_EXEC_CIRURGICA_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSECL_PLSCOME_FK_I ON TASY.PLS_EXEC_CIRURGICA_LOG
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSECL_PLSECGUIA_FK_I ON TASY.PLS_EXEC_CIRURGICA_LOG
(NR_SEQ_EXEC_CIR_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSECL_PLSEXECC_FK_I ON TASY.PLS_EXEC_CIRURGICA_LOG
(NR_SEQ_EXEC_CIRURGICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_EXEC_CIRURGICA_LOG ADD (
  CONSTRAINT PLSECL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_EXEC_CIRURGICA_LOG ADD (
  CONSTRAINT PLSECL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSECL_PLSEXECC_FK 
 FOREIGN KEY (NR_SEQ_EXEC_CIRURGICA) 
 REFERENCES TASY.PLS_EXECUCAO_CIRURGICA (NR_SEQUENCIA),
  CONSTRAINT PLSECL_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSECL_PLSECGUIA_FK 
 FOREIGN KEY (NR_SEQ_EXEC_CIR_GUIA) 
 REFERENCES TASY.PLS_EXEC_CIRURGICA_GUIA (NR_SEQUENCIA));

