ALTER TABLE TASY.IMP_MATERIAL_REACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMP_MATERIAL_REACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_MATERIAL_REACAO
(
  NR_SEQUENCIA         NUMBER(10),
  CD_MATERIAL          NUMBER(10)               DEFAULT null,
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_REACAO            VARCHAR2(4000 BYTE),
  IE_DIRTY_CHECK       NUMBER(1),
  NR_SEQ_MIMS_VERSION  NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPMR_IMPMATE_FK_I ON TASY.IMP_MATERIAL_REACAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMPMR_PK ON TASY.IMP_MATERIAL_REACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_MATERIAL_REACAO ADD (
  CONSTRAINT IMPMR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMP_MATERIAL_REACAO ADD (
  CONSTRAINT IMPMR_IMPMATE_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.IMP_MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMP_MATERIAL_REACAO TO NIVEL_1;

