ALTER TABLE TASY.LOG_QTD_REGISTROS_GRID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_QTD_REGISTROS_GRID CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_QTD_REGISTROS_GRID
(
  CD_FUNCAO            NUMBER(5)                NOT NULL,
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_REGISTRO          NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOGQTRGRID_PK ON TASY.LOG_QTD_REGISTROS_GRID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOG_QTD_REGISTROS_GRID_tp  after update ON TASY.LOG_QTD_REGISTROS_GRID FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'LOG_QTD_REGISTROS_GRID',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOG_QTD_REGISTROS_GRID ADD (
  CONSTRAINT LOGQTRGRID_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LOG_QTD_REGISTROS_GRID TO NIVEL_1;

