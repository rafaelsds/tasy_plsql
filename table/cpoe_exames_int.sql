ALTER TABLE TASY.CPOE_EXAMES_INT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_EXAMES_INT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_EXAMES_INT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10)               NOT NULL,
  NR_SEQ_PROC_CPOE     NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  QT_EXAME_INT         NUMBER(8,3),
  CD_INTERVALO         VARCHAR2(7 BYTE),
  DT_EXAME_INT         DATE,
  IE_URGENCIA          VARCHAR2(3 BYTE),
  DS_HORARIOS          VARCHAR2(2000 BYTE),
  NR_ATENDIMENTO       NUMBER(10),
  DT_LIBERACAO         DATE,
  IE_TIPO_ASSOC        VARCHAR2(1 BYTE),
  NR_SEQ_PROC_EDIT     NUMBER(10),
  CD_FUNCAO_ORIGEM     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPEXAIN_ATEPACI_FK_I ON TASY.CPOE_EXAMES_INT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPEXAIN_CPOEPRO_FK_I ON TASY.CPOE_EXAMES_INT
(NR_SEQ_PROC_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPEXAIN_CPOEPRO_FK2_I ON TASY.CPOE_EXAMES_INT
(NR_SEQ_PROC_EDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPEXAIN_INTPRES_FK_I ON TASY.CPOE_EXAMES_INT
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPEXAIN_PESFISI_FK_I ON TASY.CPOE_EXAMES_INT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPEXAIN_PK ON TASY.CPOE_EXAMES_INT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPEXAIN_PROINTE_FK_I ON TASY.CPOE_EXAMES_INT
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_EXAMES_INT ADD (
  CONSTRAINT CPEXAIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_EXAMES_INT ADD (
  CONSTRAINT CPEXAIN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CPEXAIN_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROC_CPOE) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CPEXAIN_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT CPEXAIN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPEXAIN_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CPOE_EXAMES_INT TO NIVEL_1;

