ALTER TABLE TASY.OCORRENCIA_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OCORRENCIA_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.OCORRENCIA_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_OCORRENCIA        NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OCOANEX_OCORREN_FK_I ON TASY.OCORRENCIA_ANEXO
(NR_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OCOANEX_OCORREN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.OCOANEX_PK ON TASY.OCORRENCIA_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OCORRENCIA_ANEXO ADD (
  CONSTRAINT OCOANEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OCORRENCIA_ANEXO ADD (
  CONSTRAINT OCOANEX_OCORREN_FK 
 FOREIGN KEY (NR_OCORRENCIA) 
 REFERENCES TASY.OCORRENCIA (NR_OCORRENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OCORRENCIA_ANEXO TO NIVEL_1;

