ALTER TABLE TASY.HISTORICO_SAUDE_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HISTORICO_SAUDE_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.HISTORICO_SAUDE_CIRURGIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE               NOT NULL,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_CIRURGIA                DATE,
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  DS_PROCEDIMENTO_INF        VARCHAR2(180 BYTE),
  IE_TIPO_ANESTESIA          NUMBER(3),
  DS_LOCAL                   VARCHAR2(120 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DS_COMPLICACAO             VARCHAR2(255 BYTE),
  IE_INTENSIDADE             VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  NR_SEQ_SAEP                NUMBER(10),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  IE_NEGA_CIRURGIAS          VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DS_JUST_TERMINO            VARCHAR2(255 BYTE),
  NM_USUARIO_TERMINO         VARCHAR2(15 BYTE),
  CD_DOENCA_CID              VARCHAR2(10 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HISSACI_ATCONSPEPA_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_ATEPACI_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_CIDDOEN_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_EHRREEL_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_HISTSAU_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_I1 ON TASY.HISTORICO_SAUDE_CIRURGIA
(DT_ATUALIZACAO_NREC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_I1
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_NISEALE_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_PESFISI_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_PESFISI_FK2_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HISSACI_PK ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_PROCEDI_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_PROINTE_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISSACI_SAEPERO_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_TASASDI_FK_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISSACI_TASASDI_FK2_I ON TASY.HISTORICO_SAUDE_CIRURGIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISSACI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.HISTORICO_SAUDE_CIRURGIA_ATUAL
before update or insert ON TASY.HISTORICO_SAUDE_CIRURGIA for each row
declare
begin

if	(NVL(:new.ie_nega_cirurgias,'N') = 'N') and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO_NREC, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hist_saude_cirurgia_pend_atual 
after insert or update ON TASY.HISTORICO_SAUDE_CIRURGIA 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSCI'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSCI'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.HIST_SAUDE_CIRURGIA_SBIS_IN
before insert or update ON TASY.HISTORICO_SAUDE_CIRURGIA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       :new.cd_pessoa_fisica,
                       obter_desc_expressao(308038),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       :new.cd_pessoa_fisica,
                       obter_desc_expressao(308038),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.HISTORICO_SAUDE_CIRURGIA_tp  after update ON TASY.HISTORICO_SAUDE_CIRURGIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_PROCEDIMENTO_INF,1,500);gravar_log_alteracao(substr(:old.DS_PROCEDIMENTO_INF,1,4000),substr(:new.DS_PROCEDIMENTO_INF,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROCEDIMENTO_INF',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NEGA_CIRURGIAS,1,500);gravar_log_alteracao(substr(:old.IE_NEGA_CIRURGIAS,1,4000),substr(:new.IE_NEGA_CIRURGIAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGA_CIRURGIAS',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_COMPLICACAO,1,500);gravar_log_alteracao(substr(:old.DS_COMPLICACAO,1,4000),substr(:new.DS_COMPLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLICACAO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ANESTESIA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ANESTESIA,1,4000),substr(:new.IE_TIPO_ANESTESIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ANESTESIA',ie_log_w,ds_w,'HISTORICO_SAUDE_CIRURGIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.HIST_SAUDE_CIRURGIA_SBIS_DEL
before delete ON TASY.HISTORICO_SAUDE_CIRURGIA for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(308038),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));


end;
/


ALTER TABLE TASY.HISTORICO_SAUDE_CIRURGIA ADD (
  CONSTRAINT HISSACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HISTORICO_SAUDE_CIRURGIA ADD (
  CONSTRAINT HISSACI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT HISSACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HISSACI_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT HISSACI_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT HISSACI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HISSACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISSACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISSACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT HISSACI_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT HISSACI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HISSACI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT HISSACI_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT HISSACI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.HISTORICO_SAUDE_CIRURGIA TO NIVEL_1;

