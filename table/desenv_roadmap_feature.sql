ALTER TABLE TASY.DESENV_ROADMAP_FEATURE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_ROADMAP_FEATURE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_ROADMAP_FEATURE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ART               NUMBER(10),
  NR_FEATURE           NUMBER(10),
  DT_INICIAL           DATE,
  DT_FINAL             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DEROFEA_DESARTR_FK_I ON TASY.DESENV_ROADMAP_FEATURE
(NR_ART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEROFEA_DESFEAT_FK_I ON TASY.DESENV_ROADMAP_FEATURE
(NR_FEATURE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DEROFEA_PK ON TASY.DESENV_ROADMAP_FEATURE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DESENV_ROADMAP_FEATURE ADD (
  CONSTRAINT DEROFEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_ROADMAP_FEATURE ADD (
  CONSTRAINT DEROFEA_DESARTR_FK 
 FOREIGN KEY (NR_ART) 
 REFERENCES TASY.DESENV_ART (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DEROFEA_DESFEAT_FK 
 FOREIGN KEY (NR_FEATURE) 
 REFERENCES TASY.DESENV_FEATURE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DESENV_ROADMAP_FEATURE TO NIVEL_1;

