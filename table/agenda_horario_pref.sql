ALTER TABLE TASY.AGENDA_HORARIO_PREF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_HORARIO_PREF CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_HORARIO_PREF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_HORARIO       NUMBER(10),
  NR_SEQ_TURNO         NUMBER(10),
  NR_SEQ_PREFERENCIA   NUMBER(10)               NOT NULL,
  HR_INICIAL           DATE                     NOT NULL,
  HR_FINAL             DATE                     NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FINAL_VIGENCIA    DATE,
  CD_PERFIL            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGHOPRE_AGEHORA_FK_I ON TASY.AGENDA_HORARIO_PREF
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGHOPRE_AGEPREF_FK_I ON TASY.AGENDA_HORARIO_PREF
(NR_SEQ_PREFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGHOPRE_AGEPREF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGHOPRE_AGETURN_FK_I ON TASY.AGENDA_HORARIO_PREF
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGHOPRE_PERFIL_FK_I ON TASY.AGENDA_HORARIO_PREF
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGHOPRE_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGHOPRE_PK ON TASY.AGENDA_HORARIO_PREF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.agenda_horario_pref_Atual
before insert or update ON TASY.AGENDA_HORARIO_PREF FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
cd_agenda_w	number(10);
BEGIN
begin

select	max(cd_agenda)
into	cd_agenda_w
from 	agenda_turno
where	nr_sequencia = :new.nr_seq_turno;

if (nvl(cd_agenda_w,0) > 0) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	cd_agenda	 = cd_agenda_w;
end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_horario_pref_Delete
before delete ON TASY.AGENDA_HORARIO_PREF FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
cd_agenda_w	number(10);
BEGIN
begin


select	max(cd_agenda)
into	cd_agenda_w
from 	agenda_turno
where	nr_sequencia = :old.nr_seq_turno;

if (nvl(cd_agenda_w,0) > 0) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	cd_agenda	 = cd_agenda_w;
end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.AGENDA_HORARIO_PREF ADD (
  CONSTRAINT AGHOPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_HORARIO_PREF ADD (
  CONSTRAINT AGHOPRE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT AGHOPRE_AGEPREF_FK 
 FOREIGN KEY (NR_SEQ_PREFERENCIA) 
 REFERENCES TASY.AGENDA_PREFERENCIA (NR_SEQUENCIA),
  CONSTRAINT AGHOPRE_AGETURN_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.AGENDA_TURNO (NR_SEQUENCIA),
  CONSTRAINT AGHOPRE_AGEHORA_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.AGENDA_HORARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_HORARIO_PREF TO NIVEL_1;

