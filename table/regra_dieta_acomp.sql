ALTER TABLE TASY.REGRA_DIETA_ACOMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DIETA_ACOMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DIETA_ACOMP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CONVENIO           NUMBER(5)               NOT NULL,
  CD_CATEGORIA          VARCHAR2(10 BYTE),
  CD_PLANO              VARCHAR2(10 BYTE),
  QT_DIETA_ACOMP        NUMBER(3)               NOT NULL,
  IE_LIB_DIETA          VARCHAR2(15 BYTE),
  QT_IDADE_MENORES      NUMBER(3),
  QT_IDADE_MAIORES      NUMBER(3),
  QT_ACOMPANHANTES      NUMBER(3)               NOT NULL,
  NR_SEQ_REGRA_ACOMP    NUMBER(10),
  QT_IDADE_DE           NUMBER(3),
  QT_IDADE_ATE          NUMBER(3),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  IE_TIPO_ATENDIMENTO   NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGDIEA_CATCONV_FK_I ON TASY.REGRA_DIETA_ACOMP
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGDIEA_CLAATEN_FK_I ON TASY.REGRA_DIETA_ACOMP
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIEA_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIEA_CONPLAN_FK_I ON TASY.REGRA_DIETA_ACOMP
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIEA_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIEA_CONVENI_FK_I ON TASY.REGRA_DIETA_ACOMP
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIEA_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIEA_ESTABEL_FK_I ON TASY.REGRA_DIETA_ACOMP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIEA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIEA_NUTRECA_FK_I ON TASY.REGRA_DIETA_ACOMP
(NR_SEQ_REGRA_ACOMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIEA_NUTRECA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGDIEA_PK ON TASY.REGRA_DIETA_ACOMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_DIETA_ACOMP ADD (
  CONSTRAINT REGDIEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DIETA_ACOMP ADD (
  CONSTRAINT REGDIEA_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT REGDIEA_NUTRECA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ACOMP) 
 REFERENCES TASY.NUT_REGRA_CONSISTE_ACOMP (NR_SEQUENCIA),
  CONSTRAINT REGDIEA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGDIEA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REGDIEA_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGDIEA_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO));

GRANT SELECT ON TASY.REGRA_DIETA_ACOMP TO NIVEL_1;

