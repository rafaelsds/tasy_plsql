ALTER TABLE TASY.PLS_TIPO_ITEM_FAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_ITEM_FAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_ITEM_FAT
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  CD_MATERIAL              NUMBER(6),
  IE_ATO_COOPERADO         VARCHAR2(1 BYTE),
  IE_TIPO_RELACAO          VARCHAR2(2 BYTE),
  NR_SEQ_REGRA_FAT         NUMBER(10),
  NR_SEQ_EVENTO            NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR   NUMBER(10),
  IE_VALOR                 VARCHAR2(1 BYTE),
  IE_TIPO_GUIA             VARCHAR2(2 BYTE),
  IE_ORIGEM_CONTA          VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_ATENDIMENTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTIFA_ESTABEL_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIFA_MATERIA_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTIFA_PK ON TASY.PLS_TIPO_ITEM_FAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIFA_PLSEVFA_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_PLSEVFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIFA_PLSPRGP_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIFA_PLSRFAT_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(NR_SEQ_REGRA_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_PLSRFAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIFA_PLSTIAT_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTIFA_PROCEDI_FK_I ON TASY.PLS_TIPO_ITEM_FAT
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIFA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_TIPO_ITEM_FAT_tp  after update ON TASY.PLS_TIPO_ITEM_FAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_RELACAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_TIPO_ITEM_FAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_TIPO_ITEM_FAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TIPO_ITEM_FAT ADD (
  CONSTRAINT PLSTIFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_ITEM_FAT ADD (
  CONSTRAINT PLSTIFA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTIFA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLSTIFA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSTIFA_PLSEVFA_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSTIFA_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTIFA_PLSRFAT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_FAT) 
 REFERENCES TASY.PLS_REGRA_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSTIFA_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_ITEM_FAT TO NIVEL_1;

