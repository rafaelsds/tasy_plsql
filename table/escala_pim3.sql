ALTER TABLE TASY.ESCALA_PIM3
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PIM3 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PIM3
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_ADM_ELETIVA           VARCHAR2(1 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  IE_CIRURGIA_CARDIACA     VARCHAR2(1 BYTE),
  IE_DIAG_ALTO_RISCO       VARCHAR2(1 BYTE),
  IE_DIAG_ALTISSIMO_RISCO  VARCHAR2(1 BYTE),
  IE_RESPOSTA_PUPILAS      VARCHAR2(1 BYTE),
  IE_VENTILACAO_MECANICA   VARCHAR2(1 BYTE),
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  QT_PA_SITOLICA           NUMBER(3),
  DT_AVALIACAO             DATE                 NOT NULL,
  QT_TX_MORTALIDADE        NUMBER(5,1)          NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_DIAG_BAIXO_RISCO      VARCHAR2(1 BYTE),
  QT_REL_PAO2              NUMBER(5,1),
  QT_REL_FIO2              NUMBER(5,1),
  QT_BASE_EXCESS           NUMBER(2),
  QT_PTO_ADM_ELETIVA       NUMBER(15,4)         NOT NULL,
  IE_PROC_RECUPERACAO      VARCHAR2(1 BYTE),
  QT_PTO_PROC_RECUP        NUMBER(15,4)         NOT NULL,
  QT_PTO_CIRUR_CARD        NUMBER(15,4)         NOT NULL,
  QT_PTO_DIAG_ALTO_RISCO   NUMBER(15,4)         NOT NULL,
  QT_PTO_RESP_PUPILAS      NUMBER(15,4)         NOT NULL,
  QT_PTO_VENT_MEC          NUMBER(15,4)         NOT NULL,
  QT_PTO_PA_SISTOLICA      NUMBER(16,5)         NOT NULL,
  QT_PTO_PAO2_FIO2         NUMBER(15,4)         NOT NULL,
  QT_PTO_PH_SANGUE         NUMBER(15,4)         NOT NULL,
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPIM3_ATEPACI_FK_I ON TASY.ESCALA_PIM3
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPIM3_PESFISI_FK_I ON TASY.ESCALA_PIM3
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPIM3_PK ON TASY.ESCALA_PIM3
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_pim3_atual
before insert or update ON TASY.ESCALA_PIM3 for each row
declare
logit_w					number(16,5) := 0;
qt_pto_valores_w		number(10,0) := 0;
qt_pontuacao_w			number(16,5) := 0;
qt_fio2_pao2_w			number(16,5) := 0;
qt_fio2_w				number(16,5) := 0;
qt_pao2_w				number(16,5) := 0;
qt_pto_pa_sistolica_2_w	number(16,5) := 0;
qt_reg_w	number(1);
qt_pa_sitolica_w		number(3);
begin

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

/* Pontua��o das pupilas */
if	(:new.ie_resposta_pupilas = 'N') then
	:new.qt_pto_resp_pupilas	:= 0;
elsif	(:new.ie_resposta_pupilas = 'S') then
	:new.qt_pto_resp_pupilas	:= 3.8233;
else
	:new.qt_pto_resp_pupilas	:= 0;
end if;

/* Pontua��o da admiss�o eletiva */
if	(:new.ie_adm_eletiva = 'N') then
	:new.qt_pto_adm_eletiva	:= 0;
elsif	(:new.ie_adm_eletiva = 'S') then
	:new.qt_pto_adm_eletiva := -0.5378;
else
	:new.qt_pto_adm_eletiva := 0;
end if;

/* Pontua��o da ventila��o mec�nica */
if	(:new.ie_ventilacao_mecanica = 'N') then
	:new.qt_pto_vent_mec	:= 0;
elsif	(:new.ie_ventilacao_mecanica = 'S') then
	:new.qt_pto_vent_mec	:= 0.9763;
else
	:new.qt_pto_vent_mec	:= 0;
end if;

/* Base excess (mEq/L ou mmol/L)*/
if	(:new.qt_base_excess is not null) then
	:new.qt_pto_ph_sangue := (:new.qt_base_excess * 0.0671);
else
	:new.qt_pto_ph_sangue := 0;
end if;


/* PA m�x (mmHg) */
if	(:new.qt_pa_sitolica is null) then

	qt_pa_sitolica_w := 120;

else

	qt_pa_sitolica_w := :new.qt_pa_sitolica;

end if;

:new.qt_pto_pa_sistolica := (qt_pa_sitolica_w * -0.0431);

qt_pto_pa_sistolica_2_w := (0.1716 * (dividir((qt_pa_sitolica_w * qt_pa_sitolica_w),1000)));


/*FiO2x100/PaO2  */
if	(:new.qt_rel_fio2 is null) or
	(:new.qt_rel_pao2 is null) then
	:new.qt_pto_pao2_fio2  := 0.23;
else
	qt_fio2_pao2_w := dividir(dividir(:new.qt_rel_fio2,100),:new.qt_rel_pao2)*100;
	:new.qt_pto_pao2_fio2 := qt_fio2_pao2_w * 0.4214;
end if;


/* Pontua��o do procedimento de recupera��o */
if	(:new.ie_proc_recuperacao = 'N') then
	:new.qt_pto_proc_recup := 0;
elsif	(:new.ie_proc_recuperacao = 'S') and
		(:new.ie_cirurgia_cardiaca = 'S')then
	:new.qt_pto_proc_recup := -1.2246;
elsif	(:new.ie_proc_recuperacao = 'S') and
		(:new.ie_cirurgia_cardiaca = 'N')then
	:new.qt_pto_proc_recup := -0.8762;
else
	:new.qt_pto_proc_recup := -1.5164;
end if;

/* Pontua��o de diagn�stico de alto risco */
if	(nvl(:new.ie_diag_altissimo_risco,0) = 0 ) and
	(nvl(:new.ie_diag_alto_risco,0) = 0) and
	(nvl(:new.ie_diag_baixo_risco,0) = 0) then
	:new.qt_pto_diag_alto_risco	 := 0;
elsif	(nvl(:new.ie_diag_altissimo_risco,0) = 0) and
		(nvl(:new.ie_diag_alto_risco,0) = 0) and
		((:new.ie_diag_baixo_risco = '1') or
		(:new.ie_diag_baixo_risco = '2') or
		(:new.ie_diag_baixo_risco = '3') or
		(:new.ie_diag_baixo_risco = '4') or
		(:new.ie_diag_baixo_risco = '5') or
		(:new.ie_diag_baixo_risco = '6')) then
		:new.qt_pto_diag_alto_risco := -2.1766;
elsif	(nvl(:new.ie_diag_altissimo_risco,0) = 0) and
		((:new.ie_diag_alto_risco = '1') or
		(:new.ie_diag_alto_risco = '2') or
		(:new.ie_diag_alto_risco = '3') or
		(:new.ie_diag_alto_risco = '4') or
		(:new.ie_diag_alto_risco = '5')) then
		:new.qt_pto_diag_alto_risco := 1.0725;
elsif	((:new.ie_diag_altissimo_risco = '1') or
		(:new.ie_diag_altissimo_risco = '2') or
		(:new.ie_diag_altissimo_risco = '3') or
		(:new.ie_diag_altissimo_risco = '4') or
		(:new.ie_diag_altissimo_risco = '5')) then
		:new.qt_pto_diag_alto_risco := 1.6225;
else
	:new.qt_pto_diag_alto_risco := 0;
end if;

qt_pontuacao_w		:=	nvl(:new.qt_pto_resp_pupilas,0)	+	nvl(:new.qt_pto_adm_eletiva,0)	+
				nvl(:new.qt_pto_vent_mec,0)	+	nvl(:new.qt_pto_ph_sangue,0)	+
				nvl(:new.qt_pto_pa_sistolica,0)	+	nvl(:new.qt_pto_pao2_fio2,0) +
				nvl(:new.qt_pto_proc_recup,0) + nvl(:new.qt_pto_diag_alto_risco,0) +
				nvl(qt_pto_pa_sistolica_2_w,0);


logit_w :=  qt_pontuacao_w - 1.7928;

:new.qt_tx_mortalidade := dividir(exp(logit_w), 1 + exp(logit_w)) * 100;

<<Final>>
qt_reg_w	:= 0;

exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(206726);
end;

end;
/


ALTER TABLE TASY.ESCALA_PIM3 ADD (
  CONSTRAINT ESCPIM3_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PIM3 ADD (
  CONSTRAINT ESCPIM3_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPIM3_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_PIM3 TO NIVEL_1;

