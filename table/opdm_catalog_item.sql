ALTER TABLE TASY.OPDM_CATALOG_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_CATALOG_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_CATALOG_ITEM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_OPDM_TASY         NUMBER(10),
  NM_COMMERCIAL            VARCHAR2(50 BYTE),
  DS_COMMERCIAL            VARCHAR2(50 BYTE),
  NM_PRODUCT_BRAND         VARCHAR2(80 BYTE),
  IE_PRODUCT_BRAND_OPDM    VARCHAR2(1 BYTE),
  NM_ENGINEERING_RESP_ORG  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPDMCATIT_OPDMTASY_FK_I ON TASY.OPDM_CATALOG_ITEM
(NR_SEQ_OPDM_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPDMCATIT_PK ON TASY.OPDM_CATALOG_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_CATALOG_ITEM_tp  after update ON TASY.OPDM_CATALOG_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPDM_TASY,1,4000),substr(:new.NR_SEQ_OPDM_TASY,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPDM_TASY',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_COMMERCIAL,1,4000),substr(:new.NM_COMMERCIAL,1,4000),:new.nm_usuario,nr_seq_w,'NM_COMMERCIAL',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ENGINEERING_RESP_ORG,1,4000),substr(:new.NM_ENGINEERING_RESP_ORG,1,4000),:new.nm_usuario,nr_seq_w,'NM_ENGINEERING_RESP_ORG',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PRODUCT_BRAND,1,4000),substr(:new.NM_PRODUCT_BRAND,1,4000),:new.nm_usuario,nr_seq_w,'NM_PRODUCT_BRAND',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRODUCT_BRAND_OPDM,1,4000),substr(:new.IE_PRODUCT_BRAND_OPDM,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRODUCT_BRAND_OPDM',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMMERCIAL,1,4000),substr(:new.DS_COMMERCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMMERCIAL',ie_log_w,ds_w,'OPDM_CATALOG_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_CATALOG_ITEM ADD (
  CONSTRAINT OPDMCATIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OPDM_CATALOG_ITEM ADD (
  CONSTRAINT OPDMCATIT_OPDMTASY_FK 
 FOREIGN KEY (NR_SEQ_OPDM_TASY) 
 REFERENCES TASY.OPDM_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_CATALOG_ITEM TO NIVEL_1;

