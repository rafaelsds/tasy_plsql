ALTER TABLE TASY.TECNICA_ANESTESICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TECNICA_ANESTESICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TECNICA_ANESTESICA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_TECNICA             VARCHAR2(255 BYTE)     NOT NULL,
  NR_SEQ_APRESENTACAO    NUMBER(4),
  DS_COR                 VARCHAR2(15 BYTE),
  DS_TEXTO_PADRAO        VARCHAR2(4000 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4),
  DS_TEXTO_PADRAO_CLOB   CLOB,
  NR_SEQ_TIPO_AVALIACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_TEXTO_PADRAO_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CITEANE_ESTABEL_FK_I ON TASY.TECNICA_ANESTESICA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CITEANE_MEDTIAV_FK_I ON TASY.TECNICA_ANESTESICA
(NR_SEQ_TIPO_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CITEANE_PK ON TASY.TECNICA_ANESTESICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TECNICA_ANESTESICA_AFT_UPD
before insert or update ON TASY.TECNICA_ANESTESICA FOR EACH ROW
DECLARE
/* OBS: Ao editar pelo Java/HTML (tipo CLOB), so ira sobrescrever o conteudo do campo em Delphi (tipo Varchar2) quando o conteudo do campo CLOB for igual ou inferior a 4000 contando os caracteres do RTF .
** Quando editar via Delphi, sempre ira sobrescrever o conteudo do campo usado em Java/HTML para nao haver impactos quando houver migracao de plataforma ou quando utilizado em contingencia
*/
begin
if (updating) then
	if ((nvl(:old.ds_texto_padrao_clob, 'XPTO') <> nvl(:new.ds_texto_padrao_clob, 'XPTO'))
		and DBMS_LOB.getLength(nvl(:new.ds_texto_padrao_clob, 'XPTO')) <= 4000)then /*quando alterado pelo Java ou HTML*/
		:new.ds_texto_padrao := SUBSTR(:new.ds_texto_padrao_clob,1,4000);
	elsif (:old.ds_texto_padrao <> :new.ds_texto_padrao) then /*quando alterado pelo Delphi*/
		:new.ds_texto_padrao_clob := :new.ds_texto_padrao;
	end if;
elsif (inserting) then
	if ((nvl(:new.ds_texto_padrao_clob,'XPTO') <> 'XPTO' or :new.ds_texto_padrao_clob is not null)
		and DBMS_LOB.getLength(:new.ds_texto_padrao_clob) <= 4000) then /*quando alterado pelo Java ou HTML*/
		:new.ds_texto_padrao := SUBSTR(:new.ds_texto_padrao_clob,1,4000);
	elsif (nvl(:new.ds_texto_padrao,'XPTO') <> 'XPTO') then /*quando alterado pelo Delphi*/
		:new.ds_texto_padrao_clob := :new.ds_texto_padrao;
	end if;
end if;

end;
/


ALTER TABLE TASY.TECNICA_ANESTESICA ADD (
  CONSTRAINT CITEANE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TECNICA_ANESTESICA ADD (
  CONSTRAINT CITEANE_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA),
  CONSTRAINT CITEANE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TECNICA_ANESTESICA TO NIVEL_1;

