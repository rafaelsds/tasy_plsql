ALTER TABLE TASY.CTB_ORCAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_ORCAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_ORCAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_MES_REF        NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE)       NOT NULL,
  CD_CENTRO_CUSTO       NUMBER(8)               NOT NULL,
  VL_ORIGINAL           NUMBER(15,2)            NOT NULL,
  VL_ORCADO             NUMBER(15,2)            NOT NULL,
  VL_REALIZADO          NUMBER(15,2)            NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DS_JUSTIFICATIVA      VARCHAR2(2000 BYTE),
  VL_EMPENHO            NUMBER(15,2),
  QT_METRICA_ORC        NUMBER(15,2),
  VL_TICKET_MEDIO_ORC   NUMBER(15,2),
  QT_METRICA_REAL       NUMBER(15,2),
  VL_TICKET_MEDIO_REAL  NUMBER(15,2),
  NR_SEQ_METRICA        NUMBER(10),
  IE_CENARIO            VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIB        VARCHAR2(15 BYTE),
  CD_CLASSIF_CONTA      VARCHAR2(40 BYTE),
  IE_ORIGEM_ORC         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_JUS    DATE,
  NM_USUARIO_JUS        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBORCA_CENCUST_FK_I ON TASY.CTB_ORCAMENTO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBORCA_CONCONT_FK_I ON TASY.CTB_ORCAMENTO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBORCA_CTBMERE_FK_I ON TASY.CTB_ORCAMENTO
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBORCA_ESTABEL_FK_I ON TASY.CTB_ORCAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBORCA_PK ON TASY.CTB_ORCAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBORCA_UK ON TASY.CTB_ORCAMENTO
(NR_SEQ_MES_REF, CD_ESTABELECIMENTO, CD_CONTA_CONTABIL, CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CTB_ORCAMENTO_INSERT
before INSERT ON TASY.CTB_ORCAMENTO for each row
declare

cd_empresa_w			number(4);
ds_conta_contabil_w		varchar2(255);
ie_liberacao_w		varchar2(1);
ie_conta_vigente_w		varchar2(1);

BEGIN
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	select	obter_empresa_estab(:new.cd_estabelecimento)
	into	cd_empresa_w
	from	dual;

	select	ds_conta_contabil
	into	ds_conta_contabil_w
	from	conta_contabil
	where	cd_conta_contabil = :new.cd_conta_contabil;

	/*Consistir se o usu�rio pode utilizar a conta contabil */

	select	ctb_obter_se_conta_usuario(cd_empresa_w,:new.cd_conta_contabil,:new.nm_usuario)
	into	ie_liberacao_w
	from	dual;

	if	(ie_liberacao_w = 'N') then
		/*'Usu�rio sem permiss�o para atualizar o or�amento desta conta cont�bil:' || chr(13) ||
						:new.cd_conta_contabil ||' - ' || ds_conta_contabil_w || chr(13) ||
						'Verifique o cadastro: Libera��o do Or�amento por conta.'*/
		wheb_mensagem_pck.exibir_mensagem_abort(237679,'CD_CONTA_CONTABIL_W='||:new.cd_conta_contabil);
	end if;

	select	substr(obter_se_conta_vigente(:new.cd_conta_contabil, dt_referencia),1,1)
	into	ie_conta_vigente_w
	from	ctb_mes_ref
	where	nr_sequencia = :new.nr_seq_mes_ref;
	if	(ie_conta_vigente_w = 'N') then
		/*'Esta conta cont�bil esta fora da data de vig�ncia.' || chr(13) || chr(10) || ds_conta_contabil_w*/
		wheb_mensagem_pck.exibir_mensagem_abort(237680,'CD_CONTA_CONTABIL_W='||:new.cd_conta_contabil);
	end if;
end if;
END;
/


ALTER TABLE TASY.CTB_ORCAMENTO ADD (
  CONSTRAINT CTBORCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CTBORCA_UK
 UNIQUE (NR_SEQ_MES_REF, CD_ESTABELECIMENTO, CD_CONTA_CONTABIL, CD_CENTRO_CUSTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_ORCAMENTO ADD (
  CONSTRAINT CTBORCA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBORCA_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBORCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBORCA_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_ORCAMENTO TO NIVEL_1;

