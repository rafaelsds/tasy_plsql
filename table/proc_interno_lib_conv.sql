ALTER TABLE TASY.PROC_INTERNO_LIB_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INTERNO_LIB_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INTERNO_LIB_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PROC_INTERNO  NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONVENIO          NUMBER(5),
  IE_LIBERAR           VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRINLIC_CONVENI_FK_I ON TASY.PROC_INTERNO_LIB_CONV
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRINLIC_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRINLIC_PK ON TASY.PROC_INTERNO_LIB_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRINLIC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRINLIC_PROINTE_FK_I ON TASY.PROC_INTERNO_LIB_CONV
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRINLIC_PROINTE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROC_INTERNO_LIB_CONV ADD (
  CONSTRAINT PRINLIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INTERNO_LIB_CONV ADD (
  CONSTRAINT PRINLIC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PRINLIC_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROC_INTERNO_LIB_CONV TO NIVEL_1;

