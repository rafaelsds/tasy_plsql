ALTER TABLE TASY.LAB_SORO_ARMAZENA_RACK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_SORO_ARMAZENA_RACK CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_SORO_ARMAZENA_RACK
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_MOVIMENTACAO        NUMBER(10),
  NR_SEQ_DESCARTE            NUMBER(10),
  NR_SEQ_ARMAZENAMENTO_PRIM  NUMBER(10),
  NR_SEQ_ARMAZENAMENTO_SEC   NUMBER(10),
  NR_SEQ_RACK                NUMBER(10),
  DS_RACK_GENERICO           VARCHAR2(255 BYTE),
  IE_STATUS                  VARCHAR2(1 BYTE),
  NR_SEQ_AMOSTRA_RACK        NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LASOARMRAC_LASOAMORAC_FK_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_AMOSTRA_RACK)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOARMRAC_LASOARM_FK_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_ARMAZENAMENTO_PRIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOARMRAC_LASOARM_FK_2_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_ARMAZENAMENTO_SEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOARMRAC_LASOFODESC_FK_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_DESCARTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOARMRAC_LASOMOMOVI_FK_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_MOVIMENTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOARMRAC_LASORACK_FK_I ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQ_RACK)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LASOARMRAC_PK ON TASY.LAB_SORO_ARMAZENA_RACK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAB_SORO_ARMAZENA_RACK ADD (
  CONSTRAINT LASOARMRAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_SORO_ARMAZENA_RACK ADD (
  CONSTRAINT LASOARMRAC_LASOAMORAC_FK 
 FOREIGN KEY (NR_SEQ_AMOSTRA_RACK) 
 REFERENCES TASY.LAB_SORO_AMOSTRA_RACK (NR_SEQUENCIA),
  CONSTRAINT LASOARMRAC_LASOARM_FK 
 FOREIGN KEY (NR_SEQ_ARMAZENAMENTO_PRIM) 
 REFERENCES TASY.LAB_SORO_ARMAZENAMENTO (NR_SEQUENCIA),
  CONSTRAINT LASOARMRAC_LASOARM_FK_2 
 FOREIGN KEY (NR_SEQ_ARMAZENAMENTO_SEC) 
 REFERENCES TASY.LAB_SORO_ARMAZENAMENTO (NR_SEQUENCIA),
  CONSTRAINT LASOARMRAC_LASOFODESC_FK 
 FOREIGN KEY (NR_SEQ_DESCARTE) 
 REFERENCES TASY.LAB_SORO_FORMA_DESCARTE (NR_SEQUENCIA),
  CONSTRAINT LASOARMRAC_LASOMOMOVI_FK 
 FOREIGN KEY (NR_SEQ_MOVIMENTACAO) 
 REFERENCES TASY.LAB_SORO_MOT_MOVIMENTACAO (NR_SEQUENCIA),
  CONSTRAINT LASOARMRAC_LASORACK_FK 
 FOREIGN KEY (NR_SEQ_RACK) 
 REFERENCES TASY.LAB_SORO_RACK (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_SORO_ARMAZENA_RACK TO NIVEL_1;

