ALTER TABLE TASY.AGEINT_EXAME_ADIC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_EXAME_ADIC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_EXAME_ADIC_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO   NUMBER(10)              NOT NULL,
  NR_SEQ_ITEM           NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO_SELEC    NUMBER(10),
  IE_LADO               VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  IE_REGRA              NUMBER(5),
  IE_GLOSA              VARCHAR2(1 BYTE),
  NR_SEQ_REGRA          NUMBER(10),
  IE_AUTORIZACAO        VARCHAR2(3 BYTE),
  VL_AUXILIARES         NUMBER(15,2),
  VL_MATERIAIS          NUMBER(15,2),
  VL_MEDICO             NUMBER(15,2),
  VL_ANESTESISTA        NUMBER(15,2),
  VL_CUSTO_OPERACIONAL  NUMBER(15,2),
  VL_LANC_AUTO          NUMBER(15,2),
  NR_SEQ_ORIGEM         NUMBER(10),
  DT_CANCELAMENTO       DATE,
  NM_USUARIO_CANCEL     VARCHAR2(15 BYTE),
  DS_CIRURGIA           VARCHAR2(255 BYTE),
  VL_ITEM               NUMBER(15,2),
  VL_ITEM_PARTICULAR    NUMBER(15,2),
  NR_SEQ_TOPOGRAFIA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIEXAI_AGEINGR_FK_I ON TASY.AGEINT_EXAME_ADIC_ITEM
(NR_SEQ_GRUPO_SELEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAI_AGEINGR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAI_AGEINIT_FK_I ON TASY.AGEINT_EXAME_ADIC_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAI_AGEINIT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGIEXAI_PK ON TASY.AGEINT_EXAME_ADIC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAI_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAI_PROCEDI_FK_I ON TASY.AGEINT_EXAME_ADIC_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAI_PROINTE_FK_I ON TASY.AGEINT_EXAME_ADIC_ITEM
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAI_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAI_TOPDOR_FK_I ON TASY.AGEINT_EXAME_ADIC_ITEM
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ageint_exame_adic_item_atual
before insert or update ON TASY.AGEINT_EXAME_ADIC_ITEM FOR EACH ROW
DECLARE

qt_pontos_w			preco_amb.qt_pontuacao%type;
cd_estabelecimento_w	Number(4);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
dt_inicio_agendamento_w	Date;
cd_plano_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_convenio_ww		Number(5);
cd_categoria_ww		Varchar2(10);
cd_plano_ww		Varchar2(10);
ie_tipo_convenio_w	Number(3);
ie_regra_w		Number(5);
ie_glosa_w		Varchar2(1);
cd_usuario_convenio_w	Varchar2(30);
dt_nascimento_w		Date;
ie_Sexo_w		Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
ie_tipo_Atendimento_w	Number(3);
nr_seq_ageint_w		number(10);
cd_conv_item_adic_w	number(5);
cd_categ_item_adic_w	varchar2(10);
cd_plano_item_adic_w	varchar2(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
nr_seq_regra_w		number(10);
ie_resp_autor_w		varchar2(10);
cd_medico_solicitante_w varchar2(10);

ie_edicao_w                  varchar2(1);
cd_edicao_ajuste_w      number(10);
qt_item_edicao_w         number(10);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
ie_estab_usuario_w	varchar2(1);
nm_paciente_w		varchar2(60);
ie_pacote_w		varchar2(1);
ds_erro_w		varchar2(255);
dt_nascimento_Ww	date;
ie_calcula_lanc_auto_w	varchar2(1);
ie_consiste_regra_w	varchar2(1);
ie_bloq_glosa_part_w	varchar2(1);
ie_gerado_orc_w 	varchar2(1);
vl_custo_operacional_w	Number(15,2);
vl_anestesista_w	Number(15,2);
vl_medico_w		Number(15,2);
vl_auxiliares_w		Number(15,2);
vl_materiais_w		Number(15,2);
vl_aux_w		Number(15,4);
CD_EDICAO_AMB_w         Number(6);
ds_aux_w		varchar2(10);
ds_irrelevante_w	varchar2(255);
nr_seq_cobertura_w	number(10,0);
cd_paciente_w		varchar2(10);
nr_seq_ajuste_proc_w	number(10,0);
ie_vl_particular_w	varchar2(1);
vl_lanc_autom_w         number(15,2);
qt_idade_w		number(3);
vl_procedimento_part_w	number(15,2);
cd_convenio_part_w	number(5,0);
cd_categoria_part_w	varchar2(10);
pr_glosa_w				number(15,4);
vl_glosa_w				number(15,4);
ie_regra_arredondamento_tx_w	varchar2(1):= 'N';
ie_tipo_rounded_w		varchar2(1);
ie_regra_arred_IPE_w		varchar2(1):= 'N';
ie_calc_glosa_atend_w		varchar2(1);
IE_CALCULA_GLOSA_w			parametro_Agenda_integrada.ie_calcula_glosa%type;
cd_medico_w					agenda_integrada_item.cd_medico%type;
cd_especialidade_w			agenda_integrada_item.cd_especialidade%type;
ie_param_444_w               VARCHAR2(1) := '';
ie_excluir_valor             VARCHAR2(1) := 'N';


BEGIN

select	nvl(max(IE_CALCULA_GLOSA),'N')
into	IE_CALCULA_GLOSA_w
from	parametro_Agenda_integrada
where	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento)	= wheb_usuario_pck.get_cd_estabelecimento;

begin
select	a.cd_estabelecimento,
	a.cd_convenio,
	a.cd_categoria,
	a.dt_inicio_agendamento,
	a.cd_plano,
	a.cd_usuario_convenio,
	a.cd_pessoa_fisica,
	a.ie_tipo_Atendimento,
	a.dt_nascimento,
	a.nr_sequencia,
	a.nm_paciente,
	a.cd_medico_solicitante,
	a.nr_seq_cobertura,
	a.cd_pessoa_fisica,
	ie_gerado_orc,
	cd_medico,
	cd_especialidade
into	cd_estabelecimento_w,
	cd_convenio_ww,
	cd_categoria_ww,
	dt_inicio_agendamento_w,
	cd_plano_ww,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	ie_tipo_Atendimento_w,
	dt_nascimento_w,
	nr_seq_ageint_w,
	nm_paciente_w,
	cd_medico_solicitante_w,
	nr_seq_cobertura_w,
	cd_paciente_w,
	ie_gerado_orc_w,
	cd_medico_w,
	cd_especialidade_w
from	agenda_integrada a,
	agenda_integrada_item b
where	b.nr_seq_agenda_int = a.nr_sequencia
and	b.nr_sequencia = :new.nr_seq_item;
exception
when others then
	nr_seq_ageint_w	:= null;
end;

ie_consiste_regra_w	:= nvl(Obter_Valor_Param_Usuario(869, 177, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'N');

obter_param_usuario(869, 187, obter_perfil_ativo, :new.nm_usuario, 0, ie_bloq_glosa_part_w);
obter_param_usuario(869, 331, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_vl_particular_w);
obter_param_usuario(869, 262, obter_perfil_ativo, :new.nm_usuario, 0, ie_calc_glosa_atend_w);
obter_param_usuario(869, 444, obter_perfil_ativo, :new.nm_usuario, 0, ie_param_444_w);

select	nvl(max(Obter_Valor_Param_Usuario(869, 7, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'S')
into	ie_calcula_lanc_auto_w
from 	dual;


if	(ie_consiste_regra_w = 'S') then
	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_conv_item_w,
		cd_categ_item_w,
		cd_plano_item_w
	from	agenda_integrada_conv_item
	where	nr_seq_agenda_item	= :new.nr_seq_item;

	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_conv_item_adic_w,
		cd_categ_item_adic_w,
		cd_plano_item_adic_w
	from	ageint_adic_item_conv_item
	where	nr_seq_adic_item	= :new.nr_sequencia;

	if	(cd_conv_item_adic_w is not null) then
		cd_convenio_ww	:= cd_conv_item_adic_w;
		cd_categoria_ww	:= cd_categ_item_adic_w;
		cd_plano_ww	:= cd_plano_item_adic_w;
	elsif	(cd_conv_item_w is not null) then
		cd_convenio_ww	:= cd_conv_item_w;
		cd_categoria_ww	:= cd_categ_item_w;
		cd_plano_ww	:= cd_plano_item_w;
	end if;



	cd_procedimento_w	:= :new.cd_procedimento;
	ie_origem_proced_w	:= :new.ie_origem_proced;

	if	(nr_seq_ageint_w is not null) then
		if	(cd_pessoa_fisica_w is not null) then
			select	max(ie_Sexo),
				max(dt_nascimento)
			into	ie_Sexo_w,
				dt_nascimento_ww
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		end if;

		if	(dt_nascimento_W is null) then
			dt_nascimento_W	:= dt_nascimento_Ww;
		end if;

		qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');

		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio	= cd_convenio_ww;


		if      (:new.nr_seq_proc_interno is not null) and
			((:old.nr_seq_proc_interno is null) or
			((:old.nr_seq_proc_interno is not null) and
			(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) then

			obter_proc_tab_interno_conv(
							:new.nr_seq_proc_interno,
							cd_estabelecimento_w,
							cd_convenio_ww,
							cd_categoria_ww,
							cd_plano_ww,
							null,
							cd_procedimento_w,
							ie_origem_proced_w,
							null,
							sysdate,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);

			:new.cd_procedimento	:= cd_procedimento_w;
			:new.ie_origem_proced	:= ie_origem_proced_w;

		end if;

		if      (:new.nr_seq_proc_interno is not null) and
			((:old.nr_seq_proc_interno is null) or
			((:old.nr_seq_proc_interno is not null) and
			(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) then

			ageint_consiste_plano_conv(
						null,
						cd_convenio_ww,
						cd_procedimento_w,
						ie_origem_proced_w,
						sysdate,
						1,
						nvl(ie_tipo_Atendimento_w,0),
						cd_plano_ww,
						null,
						ds_erro_w,
						0,
						null,
						ie_regra_w,
						null,
						nr_seq_regra_w,
						:new.nr_Seq_proc_interno,
						cd_categoria_ww,
						cd_estabelecimento_w,
						null,
						ie_Sexo_w,
						ie_glosa_w,
						cd_edicao_ajuste_w,
						nr_seq_cobertura_w,
						ds_irrelevante_w,
						ds_irrelevante_w,
						cd_paciente_w,
						null,
						pr_glosa_w,
						vl_glosa_w,
            cd_medico_w);


				ie_edicao_w	:= ageint_obter_se_proc_conv(cd_estabelecimento_w, cd_convenio_ww, cd_categoria_ww, sysdate, cd_procedimento_W, ie_origem_proced_w, :new.nr_Seq_proc_interno, ie_tipo_atendimento_w);

				ie_pacote_w	:= obter_Se_pacote_convenio(cd_procedimento_w, ie_origem_proced_w, cd_convenio_ww, cd_estabelecimento_w);

				if	(ie_edicao_w 			= 'N') and
					(nvl(cd_edicao_ajuste_w,0) 	= 0) and
					(ie_pacote_w			= 'N') then
					ie_glosa_w        := 'T';
				end if;

				if 	ie_glosa_w = 'E' and
					nvl(ie_param_444_w,'N') = 'N' and
					ie_tipo_convenio_w = '1' then
					ie_excluir_valor := 'S';
				end if;

				if	(ie_edicao_w 			= 'N') and
					(nvl(cd_edicao_ajuste_w,0) 	> 0) and
					(ie_pacote_w			= 'N') then

					select   count(*)
					into     qt_item_edicao_w
					from     preco_amb
					where    cd_edicao_amb = cd_edicao_ajuste_w
					and      cd_procedimento = cd_procedimento_w
					and      ie_origem_proced = ie_origem_proced_w;

					if          (qt_item_edicao_w = 0) then
						    ie_glosa_w :=    'G';
					end if;

				end if;

				:new.ie_autorizacao	:= 'L';
				if	((ie_Regra_w in (1,2,5)) or
					((ie_Regra_w = 8) and (ie_bloq_glosa_part_w = 'N'))) then
					:new.ie_autorizacao	:= 'B';
				elsif	(ie_Regra_w in (3,6,7)) then
					select 	nvl(max(ie_resp_autor),'H')
					into	ie_resp_autor_w
					from 	regra_convenio_plano
					where 	nr_sequencia = nr_seq_regra_w;
					if	(ie_resp_autor_w	= 'H') then
						:new.ie_autorizacao	:= 'PAH';
					elsif	(ie_resp_autor_w	= 'P') then
						:new.ie_autorizacao	:= 'PAP';
					end if;
				end if;

				if	(ie_glosa_w in ('G','T','D','F')) then
					:new.ie_autorizacao	:= 'B';
				end if;

				if	(ie_excluir_valor = 'S') or
					(((((nvl(ie_Regra_w,0) not in (1,2,5)) or
					(nvl(ie_glosa_w,'') not in ('T','E','R','B','H','Z',''))) and
					(ie_tipo_convenio_w <> 1) and
					(ie_calc_glosa_atend_w = 'N')) or
					((ie_calc_glosa_atend_w = 'S') 	and
					(ie_tipo_convenio_w <> 1) 	and
					((nvl(ie_glosa_w,'') not in ('T','G','R')) or (ie_glosa_w is null)))) and
					(IE_CALCULA_GLOSA_w = 'N' or
					(IE_CALCULA_GLOSA_w = 'S' and
					(nvl(ie_glosa_w,'') not in ('P','R') or (ie_glosa_w is null))))) then
					vl_procedimento_w	:= 0;
				else
					if	(ie_glosa_w	in ('P','R') and
						IE_CALCULA_GLOSA_w = 'S') then
						Define_Preco_Procedimento(
							CD_ESTABELECIMENTO_w,
							cd_convenio_ww,
							cd_categoria_ww,
							dt_inicio_agendamento_w,
							CD_PROCEDIMENTO_w,
							0,
							nvl(ie_tipo_Atendimento_w,0),
							0,
							cd_medico_w,--medico
							0,
							0,
							0,
							:new.nr_seq_proc_interno,
							null,--usuario convenio
							cd_plano_ww,
							0,
							0,
							null,
							VL_PROCEDIMENTO_w,
							vl_custo_operacional_w,
							vl_anestesista_w,
							vl_medico_w,
							vl_auxiliares_w,
							vl_materiais_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							qt_pontos_w,
							CD_EDICAO_AMB_w,
							ds_aux_w,
							nr_seq_ajuste_proc_w,
							0,
							null,
							0,
							'N',
							null,
							null,
							null,
							null,
							null,
							cd_especialidade_w,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);


						if	(ie_glosa_w = 'P') then
							vl_glosa_w:= vl_procedimento_w * pr_glosa_w / 100;

							/* ROTINA DE ARREDONDAMENTO, USADO PELO CONVENIO IPE   --->>    INICIO  <<----- */
							begin
							select 	nvl(max(ie_regra_arredondamento_tx),'N')
							into	ie_regra_arredondamento_tx_w
							from 	parametro_faturamento
							where 	cd_estabelecimento = cd_estabelecimento_w;
							exception
								when others then
									ie_regra_arredondamento_tx_w:= 'N';
							end;

							if	(ie_regra_arredondamento_tx_w = 'S')then

								select	max(ie_arredondamento)
								into	ie_tipo_rounded_w
								from	convenio_estabelecimento
								where	cd_convenio	  	= cd_convenio_w
								and	cd_estabelecimento	= cd_estabelecimento_w;

								if	(ie_tipo_rounded_w = 'R') then

									select 	obter_regra_arredondamento(cd_convenio_ww, cd_categoria_w, cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w,
											nvl(sysdate,sysdate), 'P', 1)
									into	ie_tipo_rounded_w
									from 	dual;

									ie_regra_arred_IPE_w:= 'S';

								end if;

								if	(ie_tipo_rounded_w is not null) and
									(ie_regra_arred_IPE_w = 'S') then

									arredondamento(vl_glosa_w, 2, ie_tipo_rounded_w);

								else
									ie_regra_arred_IPE_w:= 'N';
								end if;

							end if;
							if	(vl_glosa_w	> 0)  then
								vl_procedimento_w	:= vl_procedimento_w - vl_glosa_w;
							end if;
						else
							vl_procedimento_w:= vl_glosa_w;
						end if;
					else
						if	(ie_tipo_convenio_w <> 1) then
							select	max(cd_convenio_partic),
								max(cd_categoria_partic)
							into	cd_convenio_w,
								cd_categoria_w
							from	parametro_faturamento
							where	cd_estabelecimento	= cd_estabelecimento_w;

							if	(cd_convenio_ww is not null) then
								select	max(cd_plano)
								into	cd_plano_w
								from	convenio_plano
								where	cd_convenio	= cd_convenio_w
								and	ie_situacao	= 'A';
							end if;
						end if;

						if	(cd_convenio_w is null) then
							cd_convenio_w	:= cd_convenio_ww;
							cd_Categoria_w	:= cd_categoria_ww;
							cd_plano_w	:= cd_plano_ww;
						end if;

							Define_Preco_Procedimento(
								cd_estabelecimento_w,
								cd_convenio_w,
								cd_categoria_w,
								dt_inicio_agendamento_w,
								cd_procedimento_w,
								0,
								nvl(ie_tipo_Atendimento_w,0),
								0,
								cd_medico_w,--medico
								0,
								0,
								0,
								:new.nr_seq_proc_interno,
								null,--usuario convenio
								cd_plano_w,
								0,
								0,
								null,
								vl_procedimento_w,
								vl_custo_operacional_w,
								vl_anestesista_w,
								vl_medico_w,
								vl_auxiliares_w,
								vl_materiais_w,
								vl_aux_w,
								vl_aux_w,
								vl_aux_w,
								vl_aux_w,
								vl_aux_w,
								vl_aux_w,
								vl_aux_w,
								qt_pontos_w,
								cd_edicao_amb_w,
								ds_aux_w,
								nr_seq_ajuste_proc_w,
								0,
								null,
								0,
								'N',
								null,
								null,
								null,
								null,
								null,
								cd_especialidade_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null);

						if	(ie_calcula_lanc_auto_w	<> 'N') then
							ageint_Calcular_Lanc_Aut(cd_estabelecimento_w,
										cd_convenio_w,
										cd_categoria_w,
										sysdate,
										null,
										cd_procedimento_w,
										ie_origem_proced_w,
										null,
										null,
										qt_idade_w,
										null,
										null,
										:new.nr_seq_proc_interno,
										ie_tipo_atendimento_w,
										null,
										cd_usuario_convenio_w,
										cd_plano_w,
										null,
										null,
										4,
										null,
										cd_edicao_amb_w,
										vl_lanc_autom_w);
						end if;
					end if;
				end if;

			if	(nvl(ie_gerado_orc_w,'N') = 'N') then
				if	(ie_calcula_lanc_auto_w = 'S') then
					:new.vl_item			:= vl_procedimento_w + nvl(VL_LANC_AUTOM_W,0);
				else
					:new.vl_item			:= vl_procedimento_w;
				end if;
			end if;

			:new.ie_Regra			:= ie_Regra_w;
			:new.ie_glosa			:= ie_glosa_w;
			:new.nr_seq_regra		:= nr_seq_regra_w;
			:new.vl_custo_operacional	:= vl_custo_operacional_w;
			:new.vl_anestesista		:= vl_anestesista_w;
			:new.vl_medico			:= vl_medico_w;
			:new.vl_auxiliares		:= vl_auxiliares_w;
			:new.vl_materiais		:= vl_materiais_w;

			if	(ie_vl_particular_w = 'S') then

				obter_convenio_particular(cd_estabelecimento_w, cd_convenio_part_w, cd_categoria_part_w);

				if ie_excluir_valor = 'S' then
				   VL_PROCEDIMENTO_part_w := 0;
				else
					Define_Preco_Procedimento(
						CD_ESTABELECIMENTO_w,
						cd_convenio_part_w,
						cd_categoria_part_w,
						dt_inicio_agendamento_w,
						CD_PROCEDIMENTO_w,
						0,
						nvl(ie_tipo_Atendimento_w,0),
						0,
						cd_medico_w,--medico
						0,
						0,
						0,
						:new.nr_seq_proc_interno,
						null,--usuario convenio
						null,
						0,
						0,
						null,
						VL_PROCEDIMENTO_part_w,
						vl_custo_operacional_w,
						vl_anestesista_w,
						vl_medico_w,
						vl_auxiliares_w,
						vl_materiais_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						qt_pontos_w,
						CD_EDICAO_AMB_w,
						ds_aux_w,
						nr_seq_ajuste_proc_w,
						0,
						null,
						0,
						'N',
						null,
						null,
						null,
						null,
						null,
						cd_especialidade_w,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);
				end if;

				:new.vl_item_particular	:= VL_PROCEDIMENTO_part_w;

			end if;

		end if;

	end if;
end if;
END;
/


ALTER TABLE TASY.AGEINT_EXAME_ADIC_ITEM ADD (
  CONSTRAINT AGIEXAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_EXAME_ADIC_ITEM ADD (
  CONSTRAINT AGIEXAI_AGEINIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.AGENDA_INTEGRADA_ITEM (NR_SEQUENCIA),
  CONSTRAINT AGIEXAI_AGEINGR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SELEC) 
 REFERENCES TASY.AGENDA_INT_GRUPO (NR_SEQUENCIA),
  CONSTRAINT AGIEXAI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGIEXAI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGIEXAI_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGEINT_EXAME_ADIC_ITEM TO NIVEL_1;

