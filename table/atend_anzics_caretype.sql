ALTER TABLE TASY.ATEND_ANZICS_CARETYPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_ANZICS_CARETYPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_ANZICS_CARETYPE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_CARE_TYPE         NUMBER(1)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATNDANZCT_ATEPACI_FK_I ON TASY.ATEND_ANZICS_CARETYPE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATNDANZCT_PK ON TASY.ATEND_ANZICS_CARETYPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATEND_ANZICS_CARETYPE ADD (
  CONSTRAINT ATNDANZCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_ANZICS_CARETYPE ADD (
  CONSTRAINT ATNDANZCT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ATEND_ANZICS_CARETYPE TO NIVEL_1;

