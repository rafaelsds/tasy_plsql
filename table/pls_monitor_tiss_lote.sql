ALTER TABLE TASY.PLS_MONITOR_TISS_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_LOTE              VARCHAR2(12 BYTE)        NOT NULL,
  DS_LOTE              VARCHAR2(255 BYTE)       NOT NULL,
  DT_GERACAO_LOTE      DATE,
  DT_MES_COMPETENCIA   DATE                     NOT NULL,
  CD_ANS               VARCHAR2(6 BYTE)         NOT NULL,
  CD_VERSAO_TISS       VARCHAR2(7 BYTE),
  DT_LOTE              DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_STATUS            VARCHAR2(3 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  QT_CONTA             NUMBER(10),
  QT_PROCEDIMENTO      NUMBER(10),
  QT_MATERIAL          NUMBER(10),
  VL_TOTAL_COBRANCA    NUMBER(18,2),
  VL_TOTAL_PAGO        NUMBER(18,2),
  IE_TIPO_LOTE         VARCHAR2(1 BYTE),
  IE_ORIGEM_LOTE       VARCHAR2(10 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  QT_FRANQUIA          NUMBER(10),
  VL_TOTAL_GLOSA       NUMBER(15,2),
  VL_TOTAL_COPART      NUMBER(18,2),
  IE_FORNEC_DIRETO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMOTL_ESTABEL_FK_I ON TASY.PLS_MONITOR_TISS_LOTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMOTL_I1 ON TASY.PLS_MONITOR_TISS_LOTE
(NR_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMOTL_PK ON TASY.PLS_MONITOR_TISS_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMOTL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_lote_atual
after insert or update ON TASY.PLS_MONITOR_TISS_LOTE for each row
declare

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S' and pls_se_aplicacao_tasy = 'N')  then
	if	(:new.ie_origem_lote is null) then
		insert into pls_monitor_log_lote(nr_sequencia, ds_log, dt_atualizacao , dt_atualizacao_nrec,
						nm_usuario, nm_usuario_nrec, nr_seq_lote  )
			values			(pls_monitor_log_lote_seq.nextval, wheb_usuario_pck.get_nm_maquina||' maquina '||dbms_utility.format_call_stack, sysdate, sysdate,
						nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario), nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario), :new.nr_sequencia);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_MONITOR_TISS_LOTE_tp  after update ON TASY.PLS_MONITOR_TISS_LOTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_FORNEC_DIRETO,1,4000),substr(:new.IE_FORNEC_DIRETO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORNEC_DIRETO',ie_log_w,ds_w,'PLS_MONITOR_TISS_LOTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MONITOR_TISS_LOTE ADD (
  CONSTRAINT PLSMOTL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_LOTE ADD (
  CONSTRAINT PLSMOTL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_LOTE TO NIVEL_1;

