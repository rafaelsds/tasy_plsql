DROP TABLE TASY.MATERIAL_VINC_RLA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_VINC_RLA
(
  NR_SEQ_MAT_ORIGEM  NUMBER(10)                 NOT NULL,
  NR_SEQ_PROC_RLA    NUMBER(10),
  NR_SEQ_MAT_RLA     NUMBER(10),
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.MATERIAL_VINC_RLA TO NIVEL_1;

