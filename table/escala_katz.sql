ALTER TABLE TASY.ESCALA_KATZ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_KATZ CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_KATZ
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_BANHO                   VARCHAR2(1 BYTE)   NOT NULL,
  IE_VESTIR                  VARCHAR2(1 BYTE)   NOT NULL,
  IE_HIGIENE_PESSOAL         VARCHAR2(1 BYTE)   NOT NULL,
  IE_TRANSFERENCIA           VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONTINENCIA             VARCHAR2(1 BYTE)   NOT NULL,
  IE_ALIMENTACAO             VARCHAR2(1 BYTE)   NOT NULL,
  QT_PONTUACAO               NUMBER(3),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_PRESCR              NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCKATZ_ATEPACI_FK_I ON TASY.ESCALA_KATZ
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKATZ_EHRREEL_FK_I ON TASY.ESCALA_KATZ
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCKATZ_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCKATZ_PEPRESC_FK_I ON TASY.ESCALA_KATZ
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKATZ_PESFISI_FK_I ON TASY.ESCALA_KATZ
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCKATZ_PK ON TASY.ESCALA_KATZ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKATZ_TASASDI_FK_I ON TASY.ESCALA_KATZ
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCKATZ_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCKATZ_TASASDI_FK2_I ON TASY.ESCALA_KATZ
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCKATZ_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_katz_tr
before insert or update ON TASY.ESCALA_KATZ for each row
declare
qt_ponto_w	number(3) := 0;
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if  (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;
qt_ponto_w	:= 6;
if (nvl(:new.ie_banho,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;
if (nvl(:new.ie_vestir,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;
if (nvl(:new.ie_higiene_pessoal,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;
if (nvl(:new.ie_transferencia,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;
if (nvl(:new.ie_continencia,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;
if (nvl(:new.ie_alimentacao,'0') in ('1','2')) then
	qt_ponto_w := qt_ponto_w - 1;
end if;

:new.qt_pontuacao := qt_ponto_w;

<<Final>>
qt_reg_w := 0;
end escala_katz_tr;
/


CREATE OR REPLACE TRIGGER TASY.escala_katz_pend_atual
after insert or update ON TASY.ESCALA_KATZ for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'79');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_KATZ_delete
after delete ON TASY.ESCALA_KATZ for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '79'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_KATZ ADD (
  CONSTRAINT ESCKATZ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_KATZ ADD (
  CONSTRAINT ESCKATZ_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCKATZ_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCKATZ_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCKATZ_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCKATZ_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCKATZ_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_KATZ TO NIVEL_1;

