ALTER TABLE TASY.IMC_RESPONSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMC_RESPONSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMC_RESPONSE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CLAIM          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_TRANSACTION        VARCHAR2(24 BYTE),
  CD_STATUS             VARCHAR2(10 BYTE),
  NR_ACCOUNT            VARCHAR2(20 BYTE),
  CD_CLM_ASSES_FUND     VARCHAR2(1 BYTE),
  CD_CLAIM_EXP          NUMBER(10),
  DS_CLAIM_EXP          VARCHAR2(80 BYTE),
  CD_CLAIM              VARCHAR2(10 BYTE),
  NM_FIRST              VARCHAR2(40 BYTE),
  CD_MEDICARE_CARD      NUMBER(10),
  NR_PATIENT_REF        NUMBER(10),
  CD_FUND_STATUS        NUMBER(4),
  IE_MEDICARE_CODE      VARCHAR2(10 BYTE),
  CD_MEDICARE_STATUS    NUMBER(10),
  CD_PROCESS_STATUS     NUMBER(10),
  CD_PAT_MEDICARE_CARD  NUMBER(10),
  NM_PAT_FIRST          VARCHAR2(40 BYTE),
  NM_PAT_FAMILY         VARCHAR2(40 BYTE),
  NR_PAT_REF_NUM        NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMCRESP_IMCCLAI_FK_I ON TASY.IMC_RESPONSE
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMCRESP_PK ON TASY.IMC_RESPONSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMC_RESPONSE ADD (
  CONSTRAINT IMCRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMC_RESPONSE ADD (
  CONSTRAINT IMCRESP_IMCCLAI_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.IMC_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.IMC_RESPONSE TO NIVEL_1;

