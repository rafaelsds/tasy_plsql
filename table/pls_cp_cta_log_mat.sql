ALTER TABLE TASY.PLS_CP_CTA_LOG_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_LOG_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_LOG_MAT
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DS_LOG            VARCHAR2(4000 BYTE)         NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  VL_MATERIAL       NUMBER(15,2),
  NR_SEQ_CONTA_MAT  NUMBER(10)                  NOT NULL,
  IE_DESTINO_REGRA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCPTLMA_I1 ON TASY.PLS_CP_CTA_LOG_MAT
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCPTLMA_I2 ON TASY.PLS_CP_CTA_LOG_MAT
(NR_SEQ_CONTA_MAT, IE_DESTINO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCPTLMA_PK ON TASY.PLS_CP_CTA_LOG_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCPTLMA_PLSCOMAT_FK_I ON TASY.PLS_CP_CTA_LOG_MAT
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_log_mat_delete
before delete ON TASY.PLS_CP_CTA_LOG_MAT for each row
declare

begin

-- ao deletar um registro na tabela de log principal o mesmo � salvo em uma tabela de
-- hist�rico, desta forma na tabela principal guardamos apenas o ultimo recalculo feito
-- todos os demais ficam armazenados na tabela de hist�rico
insert into pls_cp_cta_log_mat_hist (
	nr_sequencia, ds_log, dt_atualizacao,
	ie_destino_regra, nm_usuario, nr_seq_conta_mat,
	vl_material, dt_atualizacao_nrec, nm_usuario_nrec
) values (
	pls_cp_cta_log_mat_hist_seq.nextval, :old.ds_log, :old.dt_atualizacao,
	:old.ie_destino_regra, :old.nm_usuario, :old.nr_seq_conta_mat,
	:old.vl_material, sysdate, nvl(wheb_usuario_pck.get_nm_usuario, 'naoidentificado')
	);

end pls_cp_cta_log_mat_delete;
/


ALTER TABLE TASY.PLS_CP_CTA_LOG_MAT ADD (
  CONSTRAINT PCPTLMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_LOG_MAT ADD (
  CONSTRAINT PCPTLMA_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CP_CTA_LOG_MAT TO NIVEL_1;

