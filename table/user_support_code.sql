ALTER TABLE TASY.USER_SUPPORT_CODE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USER_SUPPORT_CODE CASCADE CONSTRAINTS;

CREATE TABLE TASY.USER_SUPPORT_CODE
(
  NM_USER_REQUEST           VARCHAR2(50 BYTE)   NOT NULL,
  NR_SEQUENCE               NUMBER(10)          NOT NULL,
  CD_SERVICE_ORDER          NUMBER(18),
  NM_USER_SUPPORT           VARCHAR2(50 BYTE),
  OTP_CHALLENGE             NUMBER(18)          NOT NULL,
  CD_OTP                    NUMBER(18)          NOT NULL,
  OTP_LENGTH                NUMBER(2)           NOT NULL,
  DT_GENERATION             TIMESTAMP(6)        NOT NULL,
  DT_VALIDITY_START         TIMESTAMP(6),
  DT_VALIDITY_END           TIMESTAMP(6),
  DT_FIRST_LOGIN            TIMESTAMP(6),
  IE_CODE_STATUS            VARCHAR2(1 BYTE)    NOT NULL,
  HIDE_SENSITIVE_DATA       NUMBER(1),
  HIDE_IDENTIFICATION_DATA  NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_USER_SUPPORT_CODE ON TASY.USER_SUPPORT_CODE
(NM_USER_REQUEST, NR_SEQUENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.USER_SUPPORT_CODE ADD (
  CONSTRAINT PK_USER_SUPPORT_CODE
 PRIMARY KEY
 (NM_USER_REQUEST, NR_SEQUENCE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.USER_SUPPORT_CODE TO NIVEL_1;

