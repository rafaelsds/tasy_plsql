ALTER TABLE TASY.PRESCR_PROC_BOL_AUX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_BOL_AUX CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_BOL_AUX
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_HORARIO       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_BOLSA             VARCHAR2(50 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRE_PRO_AU_PK ON TASY.PRESCR_PROC_BOL_AUX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRESCR_PROC_BOL_AUX ADD (
  CONSTRAINT PRE_PRO_AU_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROC_BOL_AUX TO NIVEL_1;

