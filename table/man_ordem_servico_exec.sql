ALTER TABLE TASY.MAN_ORDEM_SERVICO_EXEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERVICO_EXEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERVICO_EXEC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ORDEM          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NM_USUARIO_EXEC       VARCHAR2(15 BYTE)       NOT NULL,
  QT_MIN_PREV           NUMBER(15),
  DT_ULT_VISAO          DATE,
  NR_SEQ_FUNCAO         NUMBER(10),
  DT_RECEBIMENTO        DATE,
  NR_SEQ_TIPO_EXEC      NUMBER(10),
  DT_FIM_EXECUCAO       DATE,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_USUARIO_PREV       VARCHAR2(15 BYTE),
  IE_CONSIDERA_HPI      VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA_HPI  VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSX_I1 ON TASY.MAN_ORDEM_SERVICO_EXEC
(NR_SEQ_ORDEM, NM_USUARIO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSX_I2 ON TASY.MAN_ORDEM_SERVICO_EXEC
(NM_USUARIO_EXEC, NR_SEQ_TIPO_EXEC, DT_FIM_EXECUCAO, NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSX_MANORSE_FK_I ON TASY.MAN_ORDEM_SERVICO_EXEC
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSX_MANTIFU_FK_I ON TASY.MAN_ORDEM_SERVICO_EXEC
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSX_MANTIFU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANOSX_MANTPEX_FK_I ON TASY.MAN_ORDEM_SERVICO_EXEC
(NR_SEQ_TIPO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANOSX_PK ON TASY.MAN_ORDEM_SERVICO_EXEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERVICO_EXEC_ATUAL
BEFORE INSERT OR UPDATE ON TASY.MAN_ORDEM_SERVICO_EXEC FOR EACH ROW
DECLARE

qt_existe_w		number(10,0);
nr_seq_classif_w	 	number(10);

BEGIN
if	(user = 'CORP') then
	if	(:new.dt_recebimento is null) then
		:new.dt_recebimento := sysdate;
	end if;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERVICO_EXEC_BEFIN
BEFORE INSERT ON TASY.MAN_ORDEM_SERVICO_EXEC FOR EACH ROW
BEGIN
if	(user = 'CORP') then
	if	(:new.dt_atualizacao_nrec is null) then
		:new.dt_atualizacao_nrec := sysdate;
	end if;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERVICO_EXEC_tp  after update ON TASY.MAN_ORDEM_SERVICO_EXEC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_USUARIO_EXEC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_EXEC,1,4000),substr(:new.NM_USUARIO_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_EXEC',ie_log_w,ds_w,'MAN_ORDEM_SERVICO_EXEC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MAN_ORDEM_SERVICO_EXEC ADD (
  CONSTRAINT MANOSX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERVICO_EXEC ADD (
  CONSTRAINT MANOSX_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANOSX_MANTIFU_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.MAN_TIPO_FUNCAO (NR_SEQUENCIA),
  CONSTRAINT MANOSX_MANTPEX_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EXEC) 
 REFERENCES TASY.MAN_TIPO_EXECUTOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERVICO_EXEC TO NIVEL_1;

