ALTER TABLE TASY.PLS_ITENS_REGRA_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ITENS_REGRA_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ITENS_REGRA_PARTIC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_REGRA               NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR           NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR     NUMBER(10),
  NR_SEQ_PREST_PARTIC        NUMBER(10),
  NR_SEQ_GRUPO_PREST_PARTIC  NUMBER(10),
  IE_OCORRENCIA              VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_GRAU_PARTIC         NUMBER(10),
  IE_PARTICIPANTE_DUPLIC     VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_REGRA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSITRP_PK ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSGRPA_FK_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSGRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSOCRP_FK_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSOCRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSORGP_FK_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_GRUPO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSORGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSPRES_FK_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSPRES_FK2_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_PREST_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSPRGP_FK_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSITRP_PLSPRGP_FK2_I ON TASY.PLS_ITENS_REGRA_PARTIC
(NR_SEQ_GRUPO_PREST_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSITRP_PLSPRGP_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_itens_regra_partic_atual
before insert or update or delete ON TASY.PLS_ITENS_REGRA_PARTIC for each row
declare
nr_seq_regra_w		pls_oc_regra_participante.nr_sequencia%type;
nr_seq_grupo_regra_w	pls_oc_regra_participante.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_grupo_regra_w := :old.nr_seq_grupo_regra;
else
	-- inser��o ou atualiza��o
	nr_seq_grupo_regra_w := :new.nr_seq_grupo_regra;
end if;

-- se tiver grupo de regra
if	(nr_seq_grupo_regra_w is not null) then

	-- busca a regra do grupo
	select	max(nr_seq_regra_partic)
	into	nr_seq_regra_w
	from	pls_oc_regra_grupo_partic
	where	nr_sequencia = nr_seq_grupo_regra_w;

	-- se tiver a regra sinaliza que � necess�rio atualizar a tabela
	if	(nr_seq_regra_w is not null) then

		pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_PARTIC_TM', wheb_usuario_pck.get_nm_usuario,
									'PLS_ITENS_REGRA_PARTIC_ATUAL',
									'nr_seq_regra_p=' || nr_seq_regra_w);
	end if;
end if;

end pls_itens_regra_partic_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_ITENS_REGRA_PARTIC_tp  after update ON TASY.PLS_ITENS_REGRA_PARTIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PREST_PARTIC,1,4000),substr(:new.NR_SEQ_GRUPO_PREST_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PREST_PARTIC',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OCORRENCIA,1,4000),substr(:new.IE_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OCORRENCIA',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REGRA,1,4000),substr(:new.NR_SEQ_GRUPO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REGRA',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_GRUPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRESTADOR',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PREST_PARTIC,1,4000),substr(:new.NR_SEQ_PREST_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PREST_PARTIC',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRAU_PARTIC,1,4000),substr(:new.NR_SEQ_GRAU_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRAU_PARTIC',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PARTICIPANTE_DUPLIC,1,4000),substr(:new.IE_PARTICIPANTE_DUPLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARTICIPANTE_DUPLIC',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_ITENS_REGRA_PARTIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_ITENS_REGRA_PARTIC ADD (
  CONSTRAINT PLSITRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ITENS_REGRA_PARTIC ADD (
  CONSTRAINT PLSITRP_PLSOCRP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_OC_REGRA_PARTICIPANTE (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PREST_PARTIC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSPRGP_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_PREST_PARTIC) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSITRP_PLSORGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REGRA) 
 REFERENCES TASY.PLS_OC_REGRA_GRUPO_PARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ITENS_REGRA_PARTIC TO NIVEL_1;

