ALTER TABLE TASY.MAN_ORDEM_SERVICO_PARADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERVICO_PARADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERVICO_PARADA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM_SERV    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO            DATE                     NOT NULL,
  DT_FIM               DATE,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSPA_MANORSE_FK_I ON TASY.MAN_ORDEM_SERVICO_PARADA
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANOSPA_PK ON TASY.MAN_ORDEM_SERVICO_PARADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANOSPA_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_ORDEM_SERVICO_PARADA ADD (
  CONSTRAINT MANOSPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERVICO_PARADA ADD (
  CONSTRAINT MANOSPA_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MAN_ORDEM_SERVICO_PARADA TO NIVEL_1;

