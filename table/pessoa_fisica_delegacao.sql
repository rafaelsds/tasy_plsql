ALTER TABLE TASY.PESSOA_FISICA_DELEGACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_DELEGACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_DELEGACAO
(
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  CD_PESSOA_SUBSTITUTA  VARCHAR2(10 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_LIMITE             DATE                    NOT NULL,
  IE_OBJETIVO           VARCHAR2(10 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(10),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  IE_CONSIGNADO         VARCHAR2(1 BYTE),
  IE_MATERIAL_ESTOQUE   VARCHAR2(1 BYTE),
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_INICIO_LIMITE      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFIDE_CLAMATE_FK_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIDE_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFIDE_GRUMATE_FK_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIDE_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFIDE_MATERIA_FK_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIDE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFIDE_PESFISI_FK_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFIDE_PESFISI_FK1_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_PESSOA_SUBSTITUTA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFIDE_PK ON TASY.PESSOA_FISICA_DELEGACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFIDE_SUBMATE_FK_I ON TASY.PESSOA_FISICA_DELEGACAO
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIDE_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_DELEGACAO_ATUAL
BEFORE UPDATE ON  PESSOA_FISICA_DELEGACAO
FOR EACH ROW
DECLARE

ds_log_w	varchar2(4000);
ds_enter_w	varchar2(255);

begin
ds_enter_w	:= chr(13) || chr(10);
ds_log_w 	:= wheb_mensagem_pck.get_texto(796350) || ds_enter_w || ds_enter_w;-- Alterado delega��o abaixo.

if	(:new.ie_objetivo <> :old.ie_objetivo) then
	ds_log_w := substr(ds_log_w ||	wheb_mensagem_pck.get_texto(796352) || obter_valor_dominio(2106,:old.ie_objetivo) || ds_enter_w || --'OBJETIVO ANTES:  '
				wheb_mensagem_pck.get_texto(796357) || obter_valor_dominio(2106,:new.ie_objetivo) || ds_enter_w || ds_enter_w,1,4000); --  'OBJETIVO ATUAL: '

end if;

if	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) then
	ds_log_w := substr(ds_log_w ||	wheb_mensagem_pck.get_texto(796358) || :old.cd_pessoa_fisica || ' - ' || obter_nome_pf(:old.cd_pessoa_fisica) || ds_enter_w || --PESSOA F�SICA ANTES:
				wheb_mensagem_pck.get_texto(796363) || :new.cd_pessoa_fisica || ' - ' || obter_nome_pf(:new.cd_pessoa_fisica) || ds_enter_w || ds_enter_w,1,4000);--'PESSOA F�SICA ATUAL: '
end if;

if	(:new.cd_pessoa_substituta <> :old.cd_pessoa_substituta) then
	ds_log_w := substr(ds_log_w ||	wheb_mensagem_pck.get_texto(796442) || :old.cd_pessoa_substituta || ' - ' || obter_nome_pf(:old.cd_pessoa_substituta) || ds_enter_w || --PESSOA SUBST. ANTES:
				wheb_mensagem_pck.get_texto(796443) || :new.cd_pessoa_substituta || ' - ' || obter_nome_pf(:new.cd_pessoa_substituta) || ds_enter_w || ds_enter_w,1,4000);--PESSOA SUBST. ATUAL:
end if;

if	(:new.dt_limite <> :old.dt_limite) then
	ds_log_w := substr(ds_log_w ||	wheb_mensagem_pck.get_texto(796444) || :old.dt_limite || ds_enter_w || --'DT. LIMITE ANTES: '
				wheb_mensagem_pck.get_texto(796445) || :new.dt_limite || ds_enter_w || ds_enter_w,1,4000);--'DT. LIMITE ATUAL: '
end if;

insert into pessoa_fisica_deleg_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log,
	cd_pessoa_substituida,
	cd_pessoa_delegada)
values(	pessoa_fisica_deleg_log_seq.nextval,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	wheb_mensagem_pck.get_texto(796446),--'Altera��o delega��o',
	ds_log_w,
	:new.cd_pessoa_fisica,
	:new.cd_pessoa_substituta);
end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_DELEGACAO_INSERT
BEFORE INSERT ON TASY.PESSOA_FISICA_DELEGACAO FOR EACH ROW
DECLARE

ds_log_w		varchar2(4000);

begin

ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(795921,'DS_OBJETIVO_W=' || obter_valor_dominio(2106,:new.ie_objetivo) || ';'
				|| 'DS_PESSOA_FISICA_W=' || :new.cd_pessoa_fisica 	|| ' - ' || obter_nome_pf(:new.cd_pessoa_fisica) || ';'
				|| 'DS_SUBSTITUTO_W=' || :new.cd_pessoa_substituta 	|| ' - ' || obter_nome_pf(:new.cd_pessoa_substituta) || ';'
				|| 'DT_LIMITE_W=' || :new.dt_limite),1,4000);

insert into pessoa_fisica_deleg_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log,
	cd_pessoa_substituida,
	cd_pessoa_delegada)
values(	pessoa_fisica_deleg_log_seq.nextval,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	wheb_mensagem_pck.get_texto(795926),
	ds_log_w,
	:new.cd_pessoa_fisica,
	:new.cd_pessoa_substituta);

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_DELEGACAO_tp  after update ON TASY.PESSOA_FISICA_DELEGACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_INICIO_LIMITE,1,500);gravar_log_alteracao(substr(:old.DT_INICIO_LIMITE,1,4000),substr(:new.DT_INICIO_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_LIMITE',ie_log_w,ds_w,'PESSOA_FISICA_DELEGACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIMITE,1,500);gravar_log_alteracao(substr(:old.DT_LIMITE,1,4000),substr(:new.DT_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIMITE',ie_log_w,ds_w,'PESSOA_FISICA_DELEGACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_OBJETIVO,1,500);gravar_log_alteracao(substr(:old.IE_OBJETIVO,1,4000),substr(:new.IE_OBJETIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBJETIVO',ie_log_w,ds_w,'PESSOA_FISICA_DELEGACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PESSOA_FISICA_DELEGACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_SUBSTITUTA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_SUBSTITUTA,1,4000),substr(:new.CD_PESSOA_SUBSTITUTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SUBSTITUTA',ie_log_w,ds_w,'PESSOA_FISICA_DELEGACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_DELEGACAO_DELETE
BEFORE DELETE ON TASY.PESSOA_FISICA_DELEGACAO FOR EACH ROW
DECLARE

ds_log_w	varchar2(4000);

begin

ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(796662,'DS_OBJETIVO_W=' || obter_valor_dominio(2106,:old.ie_objetivo) || ';'
				|| 'DS_PESSOA_FISICA_W=' || :old.cd_pessoa_fisica || ' - ' || obter_nome_pf(:old.cd_pessoa_fisica) || ';'
				|| 'DS_SUBSTITUTO_W=' || :old.cd_pessoa_substituta || ' - ' || obter_nome_pf(:old.cd_pessoa_substituta) || ';'
				|| 'DT_LIMITE_W=' || :old.dt_limite),1,4000);


insert into pessoa_fisica_deleg_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log,
	cd_pessoa_substituida,
	cd_pessoa_delegada)
values(	pessoa_fisica_deleg_log_seq.nextval,
	sysdate,
	WHEB_USUARIO_PCK.get_nm_usuario,
	sysdate,
	WHEB_USUARIO_PCK.get_nm_usuario,
	wheb_mensagem_pck.get_texto(796663),--'Exclus�o delega��o',
	ds_log_w,
	:old.cd_pessoa_fisica,
	:old.cd_pessoa_substituta);

end;
/


ALTER TABLE TASY.PESSOA_FISICA_DELEGACAO ADD (
  CONSTRAINT PESFIDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_DELEGACAO ADD (
  CONSTRAINT PESFIDE_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_SUBSTITUTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESFIDE_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT PESFIDE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PESFIDE_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT PESFIDE_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT PESFIDE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_FISICA_DELEGACAO TO NIVEL_1;

