ALTER TABLE TASY.BANCO_OCORR_MOTIVO_RET
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_OCORR_MOTIVO_RET CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_OCORR_MOTIVO_RET
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESCRIT_RET    NUMBER(10)               NOT NULL,
  CD_MOTIVO            VARCHAR2(20 BYTE)        NOT NULL,
  DS_MOTIVO            VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BOCMERE_BOESRET_FK_I ON TASY.BANCO_OCORR_MOTIVO_RET
(NR_SEQ_ESCRIT_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOCMERE_PK ON TASY.BANCO_OCORR_MOTIVO_RET
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BOCMERE_PK
  MONITORING USAGE;


ALTER TABLE TASY.BANCO_OCORR_MOTIVO_RET ADD (
  CONSTRAINT BOCMERE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_OCORR_MOTIVO_RET ADD (
  CONSTRAINT BOCMERE_BOESRET_FK 
 FOREIGN KEY (NR_SEQ_ESCRIT_RET) 
 REFERENCES TASY.BANCO_OCORR_ESCRIT_RET (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.BANCO_OCORR_MOTIVO_RET TO NIVEL_1;

