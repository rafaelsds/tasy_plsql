ALTER TABLE TASY.W_XDOK_LKFKA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_XDOK_LKFKA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_XDOK_LKFKA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  KURZBEZEICHNUNG      VARCHAR2(20 BYTE),
  KRANKENANSTALT       VARCHAR2(4 BYTE),
  TRAGERNUMMER         VARCHAR2(4 BYTE),
  TYP                  VARCHAR2(1 BYTE),
  LANGBEZEICHNUG       VARCHAR2(150 BYTE),
  PLZ                  VARCHAR2(6 BYTE),
  ORT                  VARCHAR2(50 BYTE),
  STRASSE              VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WXLKFK_I1 ON TASY.W_XDOK_LKFKA
(KRANKENANSTALT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WXLKFK_PK ON TASY.W_XDOK_LKFKA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WXLKFK_WXIMPT_FK_I ON TASY.W_XDOK_LKFKA
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_XDOK_LKFKA ADD (
  CONSTRAINT WXLKFK_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

