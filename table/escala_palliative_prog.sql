ALTER TABLE TASY.ESCALA_PALLIATIVE_PROG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PALLIATIVE_PROG CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PALLIATIVE_PROG
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_KARNOFSKY           NUMBER(3)              NOT NULL,
  IE_INGESTAO_ORAL       NUMBER(5,2)            NOT NULL,
  IE_EDEMA               VARCHAR2(2 BYTE)       NOT NULL,
  IE_DISPNEIA            VARCHAR2(2 BYTE)       NOT NULL,
  IE_DELIRIUM            VARCHAR2(2 BYTE)       NOT NULL,
  QT_PONTOS              NUMBER(5,2),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESPAPRO_ATEPACI_FK_I ON TASY.ESCALA_PALLIATIVE_PROG
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESPAPRO_PESFISI_FK_I ON TASY.ESCALA_PALLIATIVE_PROG
(CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESPAPRO_PK ON TASY.ESCALA_PALLIATIVE_PROG
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Escala_palliative_prog_atual
before insert or update ON TASY.ESCALA_PALLIATIVE_PROG for each row
declare

qt_pontuacao_karnofsky_w		number(2,1) := 0;
qt_pontuacao_ingestao_oral_w	number(2,1) := 0;
qt_pontuacao_edema_w			number(2,1) := 0;
qt_pontuacao_dispneia_w			number(2,1) := 0;
qt_pontuacao_delirium_w			number(2,1) := 0;
qt_reg_w				number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.ie_karnofsky <= 20) then
	qt_pontuacao_karnofsky_w	:=	4;
elsif	(:new.ie_karnofsky <= 50) then
	qt_pontuacao_karnofsky_w	:=	2.5;
end if;

if	(:new.ie_ingestao_oral = 1) then
	qt_pontuacao_ingestao_oral_w	:=	2.5;
elsif	(:new.ie_ingestao_oral = 2) then
	qt_pontuacao_ingestao_oral_w	:=	1;
end if;

if	(:new.ie_edema = 'S') then
	qt_pontuacao_edema_w	:=	1;
end if;

if	(:new.ie_dispneia = 'S') then
	qt_pontuacao_dispneia_w	:=	3.5;
end if;

if	(:new.ie_delirium = 'S') then
	qt_pontuacao_delirium_w	:=	4;
end if;

:new.qt_pontos	:=	qt_pontuacao_karnofsky_w +
					qt_pontuacao_ingestao_oral_w +
					qt_pontuacao_edema_w +
					qt_pontuacao_dispneia_w +
					qt_pontuacao_delirium_w;

<<Final>>
qt_reg_w	:= 0;

end Escala_palliative_prog_atual;
/


ALTER TABLE TASY.ESCALA_PALLIATIVE_PROG ADD (
  CONSTRAINT ESPAPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PALLIATIVE_PROG ADD (
  CONSTRAINT ESPAPRO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESPAPRO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_PALLIATIVE_PROG TO NIVEL_1;

