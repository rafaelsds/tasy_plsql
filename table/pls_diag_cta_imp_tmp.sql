ALTER TABLE TASY.PLS_DIAG_CTA_IMP_TMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_DIAG_CTA_IMP_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_DIAG_CTA_IMP_TMP
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NR_SEQ_CONTA  NUMBER(10)                      NOT NULL,
  CD_DOENCA     VARCHAR2(4 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE UNIQUE INDEX TASY.PDICTIT_PK ON TASY.PLS_DIAG_CTA_IMP_TMP
(NR_SEQUENCIA);


CREATE INDEX TASY.PDICTIT_PLSCTIM_FK_I ON TASY.PLS_DIAG_CTA_IMP_TMP
(NR_SEQ_CONTA);


ALTER TABLE TASY.PLS_DIAG_CTA_IMP_TMP ADD (
  CONSTRAINT PDICTIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_DIAG_CTA_IMP_TMP TO NIVEL_1;

