ALTER TABLE TASY.PTU_MOV_BENEF_PF_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_MOV_BENEF_PF_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_MOV_BENEF_PF_COMPL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MOV_PESSOA    NUMBER(10)               NOT NULL,
  CD_TIPO_LOGRADOURO   VARCHAR2(3 BYTE),
  DS_ENDERECO          VARCHAR2(40 BYTE),
  CD_CEP               NUMBER(8),
  NM_MUNICIPIO         VARCHAR2(30 BYTE),
  SG_UF                VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  DS_BAIRRO            VARCHAR2(30 BYTE),
  NR_DDD               NUMBER(4),
  NR_FONE              NUMBER(8),
  NR_RAMAL             NUMBER(4),
  DS_COMPLEMENTO       VARCHAR2(255 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  NR_ENDERECO          VARCHAR2(6 BYTE),
  IE_TIPO_ENDERECO     VARCHAR2(2 BYTE),
  NR_DDD_2             NUMBER(4),
  NR_FONE_2            NUMBER(9),
  NR_RAMAL_2           NUMBER(9),
  DS_EMAIL_2           VARCHAR2(50 BYTE),
  NR_DDD_3             NUMBER(4),
  NR_FONE_3            NUMBER(9),
  NR_RAMAL_3           NUMBER(9),
  DS_EMAIL_3           VARCHAR2(50 BYTE),
  NR_DDI               VARCHAR2(3 BYTE),
  IE_TIPO_EMAIL        VARCHAR2(2 BYTE),
  IE_TIPO_TELEFONE     VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUMBPC_PK ON TASY.PTU_MOV_BENEF_PF_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBPC_PTUMBPL_FK_I ON TASY.PTU_MOV_BENEF_PF_COMPL
(NR_SEQ_MOV_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBPC_SUSMUNI_FK_I ON TASY.PTU_MOV_BENEF_PF_COMPL
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PTU_MOV_BENEF_PF_COMPL_tp  after update ON TASY.PTU_MOV_BENEF_PF_COMPL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PTU_MOV_BENEF_PF_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PTU_MOV_BENEF_PF_COMPL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PTU_MOV_BENEF_PF_COMPL ADD (
  CONSTRAINT PTUMBPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_MOV_BENEF_PF_COMPL ADD (
  CONSTRAINT PTUMBPC_PTUMBPL_FK 
 FOREIGN KEY (NR_SEQ_MOV_PESSOA) 
 REFERENCES TASY.PTU_MOV_BENEF_PF_LOTE (NR_SEQUENCIA),
  CONSTRAINT PTUMBPC_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE));

GRANT SELECT ON TASY.PTU_MOV_BENEF_PF_COMPL TO NIVEL_1;

