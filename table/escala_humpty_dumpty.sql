ALTER TABLE TASY.ESCALA_HUMPTY_DUMPTY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_HUMPTY_DUMPTY CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_HUMPTY_DUMPTY
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  DT_AVALIACAO            DATE                  NOT NULL,
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_LIBERACAO            DATE,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  QT_PONTUACAO            NUMBER(3),
  IE_DIAGNOSTICO          VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEFICIENCIA          VARCHAR2(1 BYTE)      NOT NULL,
  IE_FAT_AMBIENT          VARCHAR2(1 BYTE)      NOT NULL,
  IE_RESP_CIRURGIA        VARCHAR2(1 BYTE)      NOT NULL,
  IE_USO_MEDIC            VARCHAR2(1 BYTE)      NOT NULL,
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESHUMDU_ATEPACI_FK_I ON TASY.ESCALA_HUMPTY_DUMPTY
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESHUMDU_PESFISI_FK_I ON TASY.ESCALA_HUMPTY_DUMPTY
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESHUMDU_PK ON TASY.ESCALA_HUMPTY_DUMPTY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_humpty_dumpty_atual
before insert or update ON TASY.ESCALA_HUMPTY_DUMPTY for each row
declare
qt_ponto_w	number(10)	:= 0;
qt_idade_w	number(10);
ie_sexo_w	varchar2(1);

begin

if (nvl(:new.nr_atendimento,0) > 0) then

	select  to_number(nvl(trim(obter_idade(dt_nascimento, nvl(dt_obito,sysdate), 'A')),0)),
			ie_sexo
	into	qt_idade_w,
			ie_sexo_w
	from	Pessoa_fisica
	where 	cd_pessoa_fisica = obter_pessoa_Atendimento(:new.nr_atendimento,'C')
	and		rownum < 2;

	if (qt_idade_w >= 13) then
		qt_ponto_w	:= qt_ponto_w + 1;
	elsif (qt_idade_w >= 7) then
		qt_ponto_w	:= qt_ponto_w + 2;
	elsif (qt_idade_w >= 3) then
		qt_ponto_w	:= qt_ponto_w + 3;
	else
		qt_ponto_w	:= qt_ponto_w + 4;
	end if;


	if (ie_sexo_w = 'M') then
		qt_ponto_w	:= qt_ponto_w + 2;
	elsif (ie_sexo_w = 'F') then
		qt_ponto_w	:= qt_ponto_w + 1;
	end if;
end if;


if	(:new.IE_DIAGNOSTICO	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.IE_DIAGNOSTICO,0);
end if;
if	(:new.IE_DEFICIENCIA	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.IE_DEFICIENCIA,0);
end if;
if	(:new.IE_FAT_AMBIENT	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.IE_FAT_AMBIENT,0);
end if;
if	(:new.IE_RESP_CIRURGIA	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.IE_RESP_CIRURGIA,0);
end if;
if	(:new.IE_USO_MEDIC	is not null) then
	qt_ponto_w	:= qt_ponto_w + nvl(:new.IE_USO_MEDIC,0);
end if;

:new.QT_PONTUACAO	:= qt_ponto_w;

end;
/


ALTER TABLE TASY.ESCALA_HUMPTY_DUMPTY ADD (
  CONSTRAINT ESHUMDU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_HUMPTY_DUMPTY ADD (
  CONSTRAINT ESHUMDU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESHUMDU_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_HUMPTY_DUMPTY TO NIVEL_1;

