ALTER TABLE TASY.PLS_INSTRUMENTO_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_INSTRUMENTO_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_INSTRUMENTO_GUIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_INSTRUMENTO   NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITULO            VARCHAR2(255 BYTE),
  DS_GUIA_LEITURA      VARCHAR2(4000 BYTE),
  NR_PAGINA            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSIGIA_PK ON TASY.PLS_INSTRUMENTO_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSIGIA_PLSINSTR_FK_I ON TASY.PLS_INSTRUMENTO_GUIA
(NR_SEQ_INSTRUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_INSTRUMENTO_GUIA ADD (
  CONSTRAINT PLSIGIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_INSTRUMENTO_GUIA ADD (
  CONSTRAINT PLSIGIA_PLSINSTR_FK 
 FOREIGN KEY (NR_SEQ_INSTRUMENTO) 
 REFERENCES TASY.PLS_INSTRUMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_INSTRUMENTO_GUIA TO NIVEL_1;

