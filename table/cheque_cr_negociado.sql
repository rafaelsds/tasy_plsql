ALTER TABLE TASY.CHEQUE_CR_NEGOCIADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE_CR_NEGOCIADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE_CR_NEGOCIADO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CHEQUE        NUMBER(10)               NOT NULL,
  NR_SEQ_CAIXA_REC     NUMBER(10)               NOT NULL,
  VL_NEGOCIADO         NUMBER(15,2)             NOT NULL,
  VL_DESCONTO          NUMBER(15,2)             NOT NULL,
  VL_ACRESCIMO         NUMBER(15,2)             NOT NULL,
  DT_NEGOCIACAO        DATE                     NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)               NOT NULL,
  VL_TERCEIRO          NUMBER(15,2),
  VL_JUROS             NUMBER(15,2),
  VL_MULTA             NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHCRNEG_CAIREC_FK_I ON TASY.CHEQUE_CR_NEGOCIADO
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHCRNEG_CHEQUES_FK_I ON TASY.CHEQUE_CR_NEGOCIADO
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHCRNEG_PK ON TASY.CHEQUE_CR_NEGOCIADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHCRNEG_PK
  MONITORING USAGE;


CREATE INDEX TASY.CHCRNEG_TRAFINA_FK_I ON TASY.CHEQUE_CR_NEGOCIADO
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHCRNEG_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CHEQUE_CR_NEGOCIADO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.CHEQUE_CR_NEGOCIADO FOR EACH ROW
DECLARE

vl_saldo_negociado_w	number(15,2);
cd_estabelecimento_w	cheque_cr.cd_estabelecimento%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
begin
select	nvl(vl_saldo_negociado,vl_cheque)
into	vl_saldo_negociado_w
from	cheque_cr
where	nr_seq_cheque	= :new.nr_seq_cheque;
exception when others then
	vl_saldo_negociado_w	:= 0;
end;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	cheque_cr
where	nr_seq_cheque	= :new.nr_seq_cheque;


if	(:new.vl_negociado > vl_saldo_negociado_w) then
	/*O valor a ser negociado nao pode ser maior que o saldo a negociar do cheque!*/
	wheb_mensagem_pck.exibir_mensagem_abort(237887);
end if;

if	(:new.dt_negociacao <> :old.dt_negociacao) then
	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_negociacao);
end if;
end if;

end;
/


ALTER TABLE TASY.CHEQUE_CR_NEGOCIADO ADD (
  CONSTRAINT CHCRNEG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE_CR_NEGOCIADO ADD (
  CONSTRAINT CHCRNEG_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE),
  CONSTRAINT CHCRNEG_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT CHCRNEG_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CHEQUE_CR_NEGOCIADO TO NIVEL_1;

