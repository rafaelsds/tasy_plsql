ALTER TABLE TASY.PLS_CP_CTA_FILTRO_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_FILTRO_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_FILTRO_BENEF
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_CP_CTA_FILTRO         NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_CONGENERE             NUMBER(10),
  IE_TIPO_VINCULO              VARCHAR2(2 BYTE),
  IE_TIPO_SEGURADO             VARCHAR2(3 BYTE),
  SG_UF_OPERADORA_INTERCAMBIO  VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_OPERADORA       NUMBER(10),
  NR_SEQ_GRUPO_INTERCAMBIO     NUMBER(10),
  NR_SEQ_INTERCAMBIO           NUMBER(10),
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_PCMSO                     VARCHAR2(1 BYTE),
  QT_IDADE_INICIAL             NUMBER(5),
  QT_IDADE_FINAL               NUMBER(5),
  NR_SEQ_REGRA_ATEND_CART      NUMBER(10),
  CD_CATEGORIA                 VARCHAR2(10 BYTE),
  CD_CONVENIO                  NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCPFB_CATCONV_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCPFB_PK ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLCPCTF_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_CP_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLSCONG_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLSINCA_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLSOACA_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_REGRA_ATEND_CART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLSPGOP_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_GRUPO_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPFB_PLSRGIN_FK_I ON TASY.PLS_CP_CTA_FILTRO_BENEF
(NR_SEQ_GRUPO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_FILTRO_BENEF_tp  after update ON TASY.PLS_CP_CTA_FILTRO_BENEF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CP_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_CP_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_CTA_FILTRO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SEGURADO,1,4000),substr(:new.IE_TIPO_SEGURADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SEGURADO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_ATEND_CART,1,4000),substr(:new.NR_SEQ_REGRA_ATEND_CART,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_ATEND_CART',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_INICIAL,1,4000),substr(:new.QT_IDADE_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_INICIAL',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PCMSO,1,4000),substr(:new.IE_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PCMSO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_UF_OPERADORA_INTERCAMBIO,1,4000),substr(:new.SG_UF_OPERADORA_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'SG_UF_OPERADORA_INTERCAMBIO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_OPERADORA,1,4000),substr(:new.NR_SEQ_GRUPO_OPERADORA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_OPERADORA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_FINAL,1,4000),substr(:new.QT_IDADE_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_FINAL',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_BENEF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_FILTRO_BENEF ADD (
  CONSTRAINT PLSCPFB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_FILTRO_BENEF ADD (
  CONSTRAINT PLSCPFB_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT PLSCPFB_PLCPCTF_FK 
 FOREIGN KEY (NR_SEQ_CP_CTA_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSCPFB_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCPFB_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSCPFB_PLSOACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ATEND_CART) 
 REFERENCES TASY.PLS_OC_ATEND_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSCPFB_PLSPGOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_OPERADORA) 
 REFERENCES TASY.PLS_PRECO_GRUPO_OPERADORA (NR_SEQUENCIA),
  CONSTRAINT PLSCPFB_PLSRGIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_GRUPO_INTER (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_FILTRO_BENEF TO NIVEL_1;

