ALTER TABLE TASY.PLS_PLANO_ACOMODACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PLANO_ACOMODACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PLANO_ACOMODACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PLANO            NUMBER(10)            NOT NULL,
  NR_SEQ_TIPO_ACOMODACAO  NUMBER(10),
  NR_SEQ_CATEGORIA        NUMBER(10),
  IE_ACOMOD_PADRAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPLAC_PK ON TASY.PLS_PLANO_ACOMODACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLAC_PLSCATE_FK_I ON TASY.PLS_PLANO_ACOMODACAO
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLAC_PLSCATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPLAC_PLSPLAN_FK_I ON TASY.PLS_PLANO_ACOMODACAO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLAC_PLSTIAC_FK_I ON TASY.PLS_PLANO_ACOMODACAO
(NR_SEQ_TIPO_ACOMODACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLAC_PLSTIAC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PLANO_ACOMODACAO ADD (
  CONSTRAINT PLSPLAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PLANO_ACOMODACAO ADD (
  CONSTRAINT PLSPLAC_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSPLAC_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSPLAC_PLSCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.PLS_CATEGORIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PLANO_ACOMODACAO TO NIVEL_1;

