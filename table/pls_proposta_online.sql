ALTER TABLE TASY.PLS_PROPOSTA_ONLINE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROPOSTA_ONLINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROPOSTA_ONLINE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_CRIACAO                 DATE               NOT NULL,
  IE_STATUS                  VARCHAR2(5 BYTE)   NOT NULL,
  IE_ESTAGIO                 VARCHAR2(2 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_TIPO_CONTRATACAO        VARCHAR2(2 BYTE),
  DT_CONFIRMACAO             DATE,
  NR_DIA_VENCIMENTO          NUMBER(2),
  ID_SUBJECT                 VARCHAR2(36 BYTE),
  VL_SIMULACAO               NUMBER(15,2),
  DS_EMAIL                   VARCHAR2(255 BYTE),
  NR_SEQ_PLANO               NUMBER(10),
  CD_MUNICIPIO_IBGE          VARCHAR2(6 BYTE),
  NR_SEQ_TABELA              NUMBER(10),
  NR_DDD_TELEFONE            VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE            VARCHAR2(3 BYTE),
  NR_TELEFONE                VARCHAR2(40 BYTE),
  NR_SEQ_SIMULACAO           NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4),
  DT_VIGENCIA_PREV           DATE,
  NR_SEQ_VENDEDOR_CANAL      NUMBER(10),
  NR_SEQ_VENDEDOR_PF         NUMBER(10),
  IE_STATUS_PLANO            VARCHAR2(1 BYTE),
  DS_URL_RESPOSTA_PAGAMENTO  VARCHAR2(500 BYTE),
  NR_SEQ_FORMA_COBRANCA      VARCHAR2(2 BYTE),
  DS_PAGAMENTO_ID            VARCHAR2(4000 BYTE),
  DS_PAGAMENTO_KEY           VARCHAR2(4000 BYTE),
  NR_SEQ_GRUPO_PLANO         NUMBER(10),
  DT_VALIDACAO               DATE,
  IE_BOLETO_EMAIL            VARCHAR2(1 BYTE),
  CD_BANCO                   NUMBER(5),
  CD_CONTA                   VARCHAR2(20 BYTE),
  IE_DIGITO_CONTA            VARCHAR2(1 BYTE),
  CD_AGENCIA_BANCARIA        VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRON_BANCO_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_ESTABEL_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRON_PK ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSGPON_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_GRUPO_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSPLAN_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSSIPR_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_SIMULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSTAPR_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSVEND_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_VENDEDOR_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_PLSVEVI_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(NR_SEQ_VENDEDOR_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRON_SUSMUNI_FK_I ON TASY.PLS_PROPOSTA_ONLINE
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PROPOSTA_ONLINE ADD (
  CONSTRAINT PLSPRON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROPOSTA_ONLINE ADD (
  CONSTRAINT PLSPRON_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_PLSSIPR_FK 
 FOREIGN KEY (NR_SEQ_SIMULACAO) 
 REFERENCES TASY.PLS_SIMULACAO_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PLSPRON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPRON_PLSVEVI_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_PF) 
 REFERENCES TASY.PLS_VENDEDOR_VINCULADO (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_CANAL) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_PLSGPON_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANO) 
 REFERENCES TASY.PLS_GRUPO_PLANO_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSPRON_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO));

GRANT SELECT ON TASY.PLS_PROPOSTA_ONLINE TO NIVEL_1;

