ALTER TABLE TASY.PTU_QUESTIONAMENTO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_QUESTIONAMENTO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_QUESTIONAMENTO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MOTIVO        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_PROC    NUMBER(10),
  NR_SEQ_CONTA_MAT     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUQUCO_PK ON TASY.PTU_QUESTIONAMENTO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUQUCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUQUCO_PTUMOQU_FK_I ON TASY.PTU_QUESTIONAMENTO_ITEM
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUQUCO_PTUMOQU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_QUESTIONAMENTO_ITEM ADD (
  CONSTRAINT PTUQUCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_QUESTIONAMENTO_ITEM ADD (
  CONSTRAINT PTUQUCO_PTUMOQU_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.PTU_MOTIVO_QUESTIONAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_QUESTIONAMENTO_ITEM TO NIVEL_1;

