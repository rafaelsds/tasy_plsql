ALTER TABLE TASY.ESCALA_SAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SAD
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_ATENDIMENTO                NUMBER(10),
  CD_PROFISSIONAL               VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                  DATE            NOT NULL,
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                  DATE,
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  QT_SCORE                      NUMBER(2),
  IE_NIVEL_ATENCAO              VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  NR_SEQ_ASSINATURA             NUMBER(10),
  NR_SEQ_ASSINATURA_INATIVACAO  NUMBER(10),
  IE_SEXO                       VARCHAR2(1 BYTE),
  IE_IDADE                      VARCHAR2(1 BYTE),
  IE_DEPRESSAO                  VARCHAR2(1 BYTE),
  IE_DOENCA                     VARCHAR2(1 BYTE),
  IE_ABUSO_ETILICO              VARCHAR2(1 BYTE),
  IE_PERDA_RACIONALIDADE        VARCHAR2(1 BYTE),
  IE_FALTA_SUPORTE              VARCHAR2(1 BYTE),
  IE_PLANO_ORGANIZADO           VARCHAR2(1 BYTE),
  IE_SOLTEIRO                   VARCHAR2(1 BYTE),
  IE_TENTATIVA_PREVIA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSAD_ATEPACI_FK_I ON TASY.ESCALA_SAD
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAD_PESFISI_FK_I ON TASY.ESCALA_SAD
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAD_PESFISI_FK2_I ON TASY.ESCALA_SAD
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSAD_PK ON TASY.ESCALA_SAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAD_TASASDI_FK_I ON TASY.ESCALA_SAD
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAD_TASASDI_FK2_I ON TASY.ESCALA_SAD
(NR_SEQ_ASSINATURA_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_SAD_ATUAL
before insert or update ON TASY.ESCALA_SAD for each row
declare

begin

:new.QT_SCORE := 0;

if (:new.IE_SEXO = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_IDADE = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_DEPRESSAO = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_TENTATIVA_PREVIA = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_ABUSO_ETILICO = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_PERDA_RACIONALIDADE = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_FALTA_SUPORTE = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_PLANO_ORGANIZADO  = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_SOLTEIRO = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

if (:new.IE_DOENCA = 'S') then
	:new.QT_SCORE := :new.QT_SCORE + 1;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_SAD_DELETE
after delete ON TASY.ESCALA_SAD for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '253'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_SAD_PEND_ATUAL
after insert or update ON TASY.ESCALA_SAD for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
cd_pessoa_fisica_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	cd_pessoa_fisica_w := substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255);
	if (cd_pessoa_fisica_w is null) then
		cd_pessoa_fisica_w := :new.cd_pessoa_fisica;
	end if;

	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'253');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_SAD ADD (
  CONSTRAINT ESCSAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_SAD ADD (
  CONSTRAINT ESCSAD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSAD_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSAD_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSAD_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCSAD_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINATURA_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_SAD TO NIVEL_1;

