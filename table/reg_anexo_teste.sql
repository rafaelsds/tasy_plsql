ALTER TABLE TASY.REG_ANEXO_TESTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_ANEXO_TESTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_ANEXO_TESTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_ANEXO             VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CASO_TESTE    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REANTE_PK ON TASY.REG_ANEXO_TESTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REANTE_RECATE_FK_I ON TASY.REG_ANEXO_TESTE
(NR_SEQ_CASO_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_ANEXO_TESTE ADD (
  CONSTRAINT REANTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_ANEXO_TESTE ADD (
  CONSTRAINT REANTE_RECATE_FK 
 FOREIGN KEY (NR_SEQ_CASO_TESTE) 
 REFERENCES TASY.REG_CASO_TESTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_ANEXO_TESTE TO NIVEL_1;

