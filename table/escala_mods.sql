ALTER TABLE TASY.ESCALA_MODS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MODS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MODS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_LIBERACAO           DATE,
  QT_REL_PAO2_FIO2       NUMBER(3),
  QT_PLAQUETAS           NUMBER(15),
  QT_BILIRRUBINA_SERICA  NUMBER(15,2),
  QT_GLASGOW             NUMBER(2),
  QT_CREATININA_SERICA   NUMBER(3,1),
  QT_FREQ_CARDIACA       NUMBER(3),
  QT_PVC                 NUMBER(4,1),
  QT_PA_SISTOLICA        NUMBER(3),
  QT_PA_DIASTOLICA       NUMBER(3),
  QT_PAM                 NUMBER(3),
  QT_PA_AJUSTADA         NUMBER(15,2),
  QT_PTO_RESP            NUMBER(1)              NOT NULL,
  QT_PTO_COAG            NUMBER(1)              NOT NULL,
  QT_PTO_HEPATICA        NUMBER(1)              NOT NULL,
  QT_PTO_CARDIO          NUMBER(1)              NOT NULL,
  QT_PTO_SNC             NUMBER(1)              NOT NULL,
  QT_PTO_RENAL           NUMBER(1)              NOT NULL,
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  PR_MORTALIDADE         NUMBER(3)              NOT NULL,
  PR_MORTALIDADE_UTI     NUMBER(3)              NOT NULL,
  QT_DIAS                NUMBER(5),
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMODS_ATEPACI_FK_I ON TASY.ESCALA_MODS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMODS_I1 ON TASY.ESCALA_MODS
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMODS_PERFIL_FK_I ON TASY.ESCALA_MODS
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMODS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMODS_PESFISI_FK_I ON TASY.ESCALA_MODS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMODS_PK ON TASY.ESCALA_MODS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMODS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_mods_befins
before insert or update ON TASY.ESCALA_MODS for each row
DECLARE
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Pontua��o rela��o Pa02/FiO2 */
if	(:new.qt_rel_pao2_fio2 is null) then
	:new.qt_pto_resp:= 0;
elsif	(:new.qt_rel_pao2_fio2 > 300) then
	:new.qt_pto_resp:= 0;
elsif	(:new.qt_rel_pao2_fio2 > 225) and
	(:new.qt_rel_pao2_fio2 <= 300) then
	:new.qt_pto_resp:= 1;
elsif	(:new.qt_rel_pao2_fio2 > 150) and
	(:new.qt_rel_pao2_fio2 <= 225) then
	:new.qt_pto_resp:= 2;
elsif	(:new.qt_rel_pao2_fio2 > 75) and
	(:new.qt_rel_pao2_fio2 <= 150) then
	:new.qt_pto_resp:= 3;
elsif	(:new.qt_rel_pao2_fio2 <= 75) then
	:new.qt_pto_resp:= 4;
end if;

/* Pontua��o Contagem de plaquetas multiplicar por 1000 */
if	(:new.qt_plaquetas is null) then
	:new.qt_pto_coag:= 0;
elsif	(:new.qt_plaquetas > 120) then
	:new.qt_pto_coag:= 0;
elsif	(:new.qt_plaquetas >= 81) and
	(:new.qt_plaquetas <= 120) then
	:new.qt_pto_coag:= 1;
elsif	(:new.qt_plaquetas >= 51) and
	(:new.qt_plaquetas < 81) then
	:new.qt_pto_coag:= 2;
elsif	(:new.qt_plaquetas >= 21) and
	(:new.qt_plaquetas < 51) then
	:new.qt_pto_coag:= 3;
elsif	(:new.qt_plaquetas <= 20) then
	:new.qt_pto_coag:= 4;
end if;

/* Pontua��o Bilirrubina s�rica */
if	(:new.qt_bilirrubina_serica is null) then
	:new.qt_pto_hepatica:= 0;
elsif	(:new.qt_bilirrubina_serica > 14) then
	:new.qt_pto_hepatica:= 4;
elsif	(:new.qt_bilirrubina_serica > 7) and
	(:new.qt_bilirrubina_serica <= 14) then
	:new.qt_pto_hepatica:= 3;
elsif	(:new.qt_bilirrubina_serica > 3.5) and
	(:new.qt_bilirrubina_serica <= 7) then
	:new.qt_pto_hepatica:= 2;
elsif	(:new.qt_bilirrubina_serica > 1.2) and
	(:new.qt_bilirrubina_serica <= 3.5) then
	:new.qt_pto_hepatica:= 1;
elsif	(:new.qt_bilirrubina_serica < 1.2) then
	:new.qt_pto_hepatica:= 0;
end if;


/* Pontua��o para Cardiovascular */
if	(:new.qt_pa_diastolica is not null) and
	(:new.qt_pa_sistolica is not null) then
	:new.qt_pam:= nvl(dividir((:new.qt_pa_sistolica + (:new.qt_pa_diastolica * 2)), 3),0);
end if;

if	(:new.qt_freq_cardiaca is not null) and
	(:new.qt_pvc is not null) and
	(:new.qt_pam is not null) then
	:new.qt_pa_ajustada:= nvl(dividir((:new.qt_freq_cardiaca * :new.qt_pvc), :new.qt_pam),0);
end if;

if	(:new.qt_pa_ajustada is null) then
	:new.qt_pto_cardio:= 0;
elsif	(:new.qt_pa_ajustada > 30) then
	:new.qt_pto_cardio:= 4;
elsif	(:new.qt_pa_ajustada > 20) and
	(:new.qt_pa_ajustada <= 30) then
	:new.qt_pto_cardio:= 3;
elsif	(:new.qt_pa_ajustada > 15) and
	(:new.qt_pa_ajustada <= 20) then
	:new.qt_pto_cardio:= 2;
elsif	(:new.qt_pa_ajustada > 10) and
	(:new.qt_pa_ajustada <= 15) then
	:new.qt_pto_cardio:= 1;
elsif	(:new.qt_pa_ajustada <= 10) then
	:new.qt_pto_cardio:= 0;
end if;

/* Pontua��o Glasgow */
if	(:new.qt_glasgow is null) then
	:new.qt_pto_snc:= 0;
elsif	(:new.qt_glasgow >= 15) then
	:new.qt_pto_snc:= 0;
elsif	(:new.qt_glasgow >= 13) and
	(:new.qt_glasgow < 15) then
	:new.qt_pto_snc:= 1;
elsif	(:new.qt_glasgow >= 10) and
	(:new.qt_glasgow < 13) then
	:new.qt_pto_snc:= 2;
elsif	(:new.qt_glasgow >= 7) and
	(:new.qt_glasgow < 10) then
	:new.qt_pto_snc:= 3;
elsif	(:new.qt_glasgow <= 6) then
	:new.qt_pto_snc:= 4;
end if;

/* Pontua��o Creatinina S�rica */
if	(:new.qt_creatinina_serica is null) then
	:new.qt_pto_renal:= 0;
elsif	(:new.qt_creatinina_serica > 5.7) then
	:new.qt_pto_renal:= 4;
elsif	(:new.qt_creatinina_serica > 4) and
	(:new.qt_creatinina_serica <= 5.7) then
	:new.qt_pto_renal:= 3;
elsif	(:new.qt_creatinina_serica > 2.3) and
	(:new.qt_creatinina_serica <= 4) then
	:new.qt_pto_renal:= 2;
elsif	(:new.qt_creatinina_serica > 1.1) and
	(:new.qt_creatinina_serica <= 2.3) then
	:new.qt_pto_renal:= 1;
elsif	(:new.qt_creatinina_serica <= 1.1) then
	:new.qt_pto_renal:= 0;
end if;

/* Obter a pontua��o total */
:new.qt_pontuacao:= nvl(:new.qt_pto_resp,0)	+ nvl(:new.qt_pto_coag,0)	+ nvl(:new.qt_pto_hepatica,0) +
		    nvl(:new.qt_pto_cardio,0)	+ nvl(:new.qt_pto_snc,0)	+ nvl(:new.qt_pto_renal,0);

/* Percentual de mortalidade */
if	(:new.qt_pontuacao = 0) then
	:new.pr_mortalidade_uti:= 0;
	:new.pr_mortalidade:= 0;
	:new.qt_dias:= 2;
elsif	(:new.qt_pontuacao >= 1) and
	(:new.qt_pontuacao <= 2) then
	:new.pr_mortalidade_uti:= 1;
	:new.pr_mortalidade:= 7;
	:new.qt_dias:= 3;
elsif	(:new.qt_pontuacao >= 3) and
	(:new.qt_pontuacao <= 4) then
	:new.pr_mortalidade_uti:= 2;
	:new.pr_mortalidade:= 7;
	:new.qt_dias:= 3;
elsif	(:new.qt_pontuacao = 5) then
	:new.pr_mortalidade_uti:= 3;
	:new.pr_mortalidade:= 16;
	:new.qt_dias:= 6;
elsif	(:new.qt_pontuacao >= 6) and
	(:new.qt_pontuacao <= 7) then
	:new.pr_mortalidade_uti:= 4;
	:new.pr_mortalidade:= 16;
	:new.qt_dias:= 6;
elsif	(:new.qt_pontuacao = 8) then
	:new.pr_mortalidade_uti:= 5;
	:new.pr_mortalidade:= 16;
	:new.qt_dias:= 6;
elsif	(:new.qt_pontuacao >= 9) and
	(:new.qt_pontuacao <= 12) then
	:new.pr_mortalidade_uti:= 25;
	:new.pr_mortalidade:= 50;
	:new.qt_dias:= 10;
elsif	(:new.qt_pontuacao >= 13) and
	(:new.qt_pontuacao <= 16) then
	:new.pr_mortalidade_uti:= 50;
	:new.pr_mortalidade:= 70;
	:new.qt_dias:= 17;
elsif	(:new.qt_pontuacao >= 17) and
	(:new.qt_pontuacao <= 20) then
	:new.pr_mortalidade_uti:= 75;
	:new.pr_mortalidade:= 82;
	:new.qt_dias:= 21;
elsif	(:new.qt_pontuacao >= 21) and
	(:new.qt_pontuacao <= 24) then
	:new.pr_mortalidade_uti:= 100;
	:new.pr_mortalidade:= 100;
	:new.qt_dias:= null;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.ESCALA_MODS ADD (
  CONSTRAINT ESCMODS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MODS ADD (
  CONSTRAINT ESCMODS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMODS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMODS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_MODS TO NIVEL_1;

