ALTER TABLE TASY.PLS_AR_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AR_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AR_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_MES_COMPETENCIA   DATE                     NOT NULL,
  DT_INICIO            DATE                     NOT NULL,
  DT_FIM               DATE                     NOT NULL,
  IE_PRO_RATA_DIA      VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  IE_FORMA_PAGAMENTO   VARCHAR2(3 BYTE)         NOT NULL,
  DT_GERACAO           DATE,
  IE_SITUACAO          VARCHAR2(2 BYTE),
  DS_LOG               VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARLOTE_ESTABEL_FK_I ON TASY.PLS_AR_LOTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARLOTE_PK ON TASY.PLS_AR_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ar_lote_atual
before update or insert ON TASY.PLS_AR_LOTE for each row
declare

begin
if	(:new.dt_mes_competencia <> :old.dt_mes_competencia) or
	(:old.dt_mes_competencia is null) then
	:new.dt_inicio 	:= trunc(:new.dt_mes_competencia, 'mm');
	:new.dt_fim 	:= last_day(trunc(:new.dt_mes_competencia)) + 0.99999;
end if;

if	(:new.cd_estabelecimento <> :old.cd_estabelecimento) or
	(:old.cd_estabelecimento is null) then
	select	nvl(max(ie_forma_pagamento), 'P')
	into	:new.ie_forma_pagamento
	from	pls_parametro_pagamento
	where	cd_estabelecimento = :new.cd_estabelecimento;

	select	nvl(max(ie_pro_rata_dia), 'S')
	into	:new.ie_pro_rata_dia
	from	pls_parametros
	where	cd_estabelecimento = :new.cd_estabelecimento;
end if;

end;
/


ALTER TABLE TASY.PLS_AR_LOTE ADD (
  CONSTRAINT PARLOTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AR_LOTE ADD (
  CONSTRAINT PARLOTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_AR_LOTE TO NIVEL_1;

