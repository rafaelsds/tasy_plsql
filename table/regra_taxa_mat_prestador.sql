ALTER TABLE TASY.REGRA_TAXA_MAT_PRESTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TAXA_MAT_PRESTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TAXA_MAT_PRESTADOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FINAL_VIGENCIA       DATE,
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_GRUPO_MATERIAL       NUMBER(3),
  CD_SUBGRUPO_MATERIAL    NUMBER(3),
  CD_CLASSE_MATERIAL      NUMBER(5),
  CD_MATERIAL             NUMBER(6),
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_CONVENIO_PRESTADOR   NUMBER(5)             NOT NULL,
  CD_CATEGORIA_PRESTADOR  VARCHAR2(10 BYTE)     NOT NULL,
  PR_PRESTADOR            NUMBER(9,4),
  VL_PRESTADOR            NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGTXMP_CATCONV_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_CONVENIO_PRESTADOR, CD_CATEGORIA_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_CLAMATE_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_CONVENI_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_CONVENIO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_ESTABEL_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_GRUMATE_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_MATERIA_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_PESJURI_FK2_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGTXMP_PK ON TASY.REGRA_TAXA_MAT_PRESTADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTXMP_SUBMATE_FK_I ON TASY.REGRA_TAXA_MAT_PRESTADOR
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_TAXA_MAT_PRESTADOR_tp  after update ON TASY.REGRA_TAXA_MAT_PRESTADOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'REGRA_TAXA_MAT_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'REGRA_TAXA_MAT_PRESTADOR',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'REGRA_TAXA_MAT_PRESTADOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_TAXA_MAT_PRESTADOR ADD (
  CONSTRAINT REGTXMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TAXA_MAT_PRESTADOR ADD (
  CONSTRAINT REGTXMP_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_PRESTADOR, CD_CATEGORIA_PRESTADOR) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGTXMP_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT REGTXMP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_PRESTADOR) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REGTXMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGTXMP_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT REGTXMP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REGTXMP_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REGTXMP_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.REGRA_TAXA_MAT_PRESTADOR TO NIVEL_1;

