ALTER TABLE TASY.CONV_REGRA_RETORNO_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_REGRA_RETORNO_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_REGRA_RETORNO_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_CONVENIO          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CORETAD_CONVENI_FK_I ON TASY.CONV_REGRA_RETORNO_ADIC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORETAD_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CORETAD_ESTABEL_FK_I ON TASY.CONV_REGRA_RETORNO_ADIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORETAD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CORETAD_PK ON TASY.CONV_REGRA_RETORNO_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CORETAD_PROCEDI_FK_I ON TASY.CONV_REGRA_RETORNO_ADIC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORETAD_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CORETAD_TRAFINA_FK_I ON TASY.CONV_REGRA_RETORNO_ADIC
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORETAD_TRAFINA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CONV_REGRA_RETORNO_ADIC ADD (
  CONSTRAINT CORETAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_REGRA_RETORNO_ADIC ADD (
  CONSTRAINT CORETAD_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT CORETAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CORETAD_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT CORETAD_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONV_REGRA_RETORNO_ADIC TO NIVEL_1;

