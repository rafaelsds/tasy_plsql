ALTER TABLE TASY.MAN_PLANEJ_DATA_PREV_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_PLANEJ_DATA_PREV_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_PLANEJ_DATA_PREV_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PLANEJ        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ANTERIOR          DATE,
  DT_ATUAL             DATE,
  NR_SEQ_EQUIPAMENTO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANPDPL_MANEQUI_FK_I ON TASY.MAN_PLANEJ_DATA_PREV_LOG
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANPDPL_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANPDPL_MANPLPR_FK_I ON TASY.MAN_PLANEJ_DATA_PREV_LOG
(NR_SEQ_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANPDPL_MANPLPR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANPDPL_PK ON TASY.MAN_PLANEJ_DATA_PREV_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANPDPL_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_PLANEJ_DATA_PREV_LOG ADD (
  CONSTRAINT MANPDPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_PLANEJ_DATA_PREV_LOG ADD (
  CONSTRAINT MANPDPL_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT MANPDPL_MANPLPR_FK 
 FOREIGN KEY (NR_SEQ_PLANEJ) 
 REFERENCES TASY.MAN_PLANEJ_PREV (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_PLANEJ_DATA_PREV_LOG TO NIVEL_1;

