ALTER TABLE TASY.PROJ_ORC_DESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_ORC_DESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_ORC_DESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORC           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF_DESP  NUMBER(10)               NOT NULL,
  QT_UNITARIA          NUMBER(15,2)             NOT NULL,
  QT_SEMANA            NUMBER(15)               NOT NULL,
  VL_CUSTO_TOTAL       NUMBER(15,2)             NOT NULL,
  VL_RECEITA_TOTAL     NUMBER(15,2)             NOT NULL,
  VL_UNITARIO          NUMBER(15,2)             NOT NULL,
  VL_IMPOSTO           NUMBER(15,2)             NOT NULL,
  VL_MARGEM            NUMBER(15,2)             NOT NULL,
  IE_RESP_DESP         VARCHAR2(1 BYTE)         NOT NULL,
  IE_RESP_PAGTO        VARCHAR2(15 BYTE)        NOT NULL,
  IE_REEMBOLSO         VARCHAR2(15 BYTE)        NOT NULL,
  DT_INICIO_DESP       DATE                     NOT NULL,
  DT_FIM_DESP          DATE                     NOT NULL,
  QT_CONSULTORES       NUMBER(15)               NOT NULL,
  PR_IMPOSTO           NUMBER(15,4)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROORDE_PK ON TASY.PROJ_ORC_DESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROORDE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROORDE_PROJORC_FK_I ON TASY.PROJ_ORC_DESP
(NR_SEQ_ORC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROORDE_PROJORC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROORDE_VIACLDE_FK_I ON TASY.PROJ_ORC_DESP
(NR_SEQ_CLASSIF_DESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROORDE_VIACLDE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Proj_Orc_Desp_Atual
before insert or update ON TASY.PROJ_ORC_DESP for each row
declare

vl_receita_total_w	number(15,2);
vl_imposto_w		number(15,2);
vl_margem_w		number(15,2);
vl_custo_total_w	number(15,2);

begin

if (1=2) then

	vl_receita_total_w := :new.vl_unitario * :new.qt_unitaria;

	select	vl_receita_total_w,
		(vl_receita_total_w * 0.37),
		(vl_receita_total_w * 0.5),
		vl_receita_total_w
	into	vl_receita_total_w,
		vl_imposto_w,
		vl_margem_w,
		vl_custo_total_w
	from	dual;

	:new.vl_custo_total	:= vl_custo_total_w;
	:new.vl_receita_total	:= vl_receita_total_w;
	:new.vl_imposto		:= vl_imposto_w;
	:new.vl_margem		:= vl_margem_w;
end if;
end;
/


ALTER TABLE TASY.PROJ_ORC_DESP ADD (
  CONSTRAINT PROORDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_ORC_DESP ADD (
  CONSTRAINT PROORDE_PROJORC_FK 
 FOREIGN KEY (NR_SEQ_ORC) 
 REFERENCES TASY.PROJ_ORCAMENTO (NR_SEQUENCIA),
  CONSTRAINT PROORDE_VIACLDE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_DESP) 
 REFERENCES TASY.VIA_CLASSIF_DESP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_ORC_DESP TO NIVEL_1;

