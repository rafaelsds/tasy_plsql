ALTER TABLE TASY.EXP_DADO_PCT_ATRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXP_DADO_PCT_ATRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXP_DADO_PCT_ATRIBUTO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NR_SEQ_HL7_SEGMENT             NUMBER(10),
  NR_SEQ_RES_CADASTRO_ONTOL_PHI  NUMBER(10),
  CD_EXPRESSAO                   NUMBER(10),
  NM_TABELA                      VARCHAR2(30 BYTE),
  NM_ATRIBUTO                    VARCHAR2(30 BYTE),
  NR_SEQ_EXP_DADO_PCT_SQL        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXPDAPCATR_DICEXPR_FK_I ON TASY.EXP_DADO_PCT_ATRIBUTO
(CD_EXPRESSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXPDAPCATR_EXPDAPCSQL_FK_I ON TASY.EXP_DADO_PCT_ATRIBUTO
(NR_SEQ_EXP_DADO_PCT_SQL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXPDAPCATR_HL7SEGMENT_FK_I ON TASY.EXP_DADO_PCT_ATRIBUTO
(NR_SEQ_HL7_SEGMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EXPDAPCATR_PK ON TASY.EXP_DADO_PCT_ATRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXPDAPCATR_RECAONP_FK_I ON TASY.EXP_DADO_PCT_ATRIBUTO
(NR_SEQ_RES_CADASTRO_ONTOL_PHI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXPDAPCATR_5 ON TASY.EXP_DADO_PCT_ATRIBUTO
(NM_TABELA, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EXP_DADO_PCT_ATRIBUTO ADD (
  CONSTRAINT EXPDAPCATR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.EXP_DADO_PCT_ATRIBUTO ADD (
  CONSTRAINT EXPDAPCATR_DICEXPR_FK 
 FOREIGN KEY (CD_EXPRESSAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO)
    ON DELETE CASCADE,
  CONSTRAINT EXPDAPCATR_EXPDAPCSQL_FK 
 FOREIGN KEY (NR_SEQ_EXP_DADO_PCT_SQL) 
 REFERENCES TASY.EXP_DADO_PCT_SQL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EXPDAPCATR_HL7SEGMENT_FK 
 FOREIGN KEY (NR_SEQ_HL7_SEGMENT) 
 REFERENCES TASY.HL7_SEGMENT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EXPDAPCATR_RECAONP_FK 
 FOREIGN KEY (NR_SEQ_RES_CADASTRO_ONTOL_PHI) 
 REFERENCES TASY.RES_CADASTRO_ONTOLOGIA_PHI (NR_SEQUENCIA)
    ON DELETE CASCADE);

