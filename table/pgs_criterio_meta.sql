ALTER TABLE TASY.PGS_CRITERIO_META
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PGS_CRITERIO_META CASCADE CONSTRAINTS;

CREATE TABLE TASY.PGS_CRITERIO_META
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_META           NUMBER(10),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  NR_SEQ_SUS_GRUPO      NUMBER(10),
  NR_SEQ_SUS_SUBGRUPO   NUMBER(10),
  NR_SEQ_SUS_FORMA_ORG  NUMBER(10),
  QT_PRONTO_CRITERIO    NUMBER(10),
  CD_CBO_CRITERIO       VARCHAR2(6 BYTE),
  IE_ORIGEM_VALOR_CRIT  VARCHAR2(15 BYTE),
  PR_VALOR_CRITERIO     NUMBER(15,2),
  IE_COMPLEXIDADE       VARCHAR2(2 BYTE),
  QT_PONTO_CRITERIO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PGSCRME_PGSMETA_FK_I ON TASY.PGS_CRITERIO_META
(NR_SEQ_META)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PGSCRME_PK ON TASY.PGS_CRITERIO_META
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PGSCRME_PROCEDI_FK_I ON TASY.PGS_CRITERIO_META
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PGSCRME_SUSCBOU_FK_I ON TASY.PGS_CRITERIO_META
(CD_CBO_CRITERIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PGSCRME_SUSFORG_FK_I ON TASY.PGS_CRITERIO_META
(NR_SEQ_SUS_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PGSCRME_SUSGRUP_FK_I ON TASY.PGS_CRITERIO_META
(NR_SEQ_SUS_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PGSCRME_SUSSUBG_FK_I ON TASY.PGS_CRITERIO_META
(NR_SEQ_SUS_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PGS_CRITERIO_META ADD (
  CONSTRAINT PGSCRME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PGS_CRITERIO_META ADD (
  CONSTRAINT PGSCRME_PGSMETA_FK 
 FOREIGN KEY (NR_SEQ_META) 
 REFERENCES TASY.PGS_META (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PGSCRME_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PGSCRME_SUSCBOU_FK 
 FOREIGN KEY (CD_CBO_CRITERIO) 
 REFERENCES TASY.SUS_CBO (CD_CBO),
  CONSTRAINT PGSCRME_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_SUS_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT PGSCRME_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_SUS_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT PGSCRME_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUS_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PGS_CRITERIO_META TO NIVEL_1;

