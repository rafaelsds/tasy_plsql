ALTER TABLE TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(80 BYTE)        NOT NULL,
  DS_ARQUIVO_BACKUP    VARCHAR2(80 BYTE),
  DS_TITULO            VARCHAR2(40 BYTE),
  NR_SEQ_CAMPO_TRAT    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RXTTCRI_PK ON TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTTCRI_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTTCRI_RXTCPTR_FK_I ON TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM
(NR_SEQ_CAMPO_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTTCRI_RXTCPTR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM ADD (
  CONSTRAINT RXTTCRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM ADD (
  CONSTRAINT RXTTCRI_RXTCPTR_FK 
 FOREIGN KEY (NR_SEQ_CAMPO_TRAT) 
 REFERENCES TASY.RXT_CAMPO_TRAT_ROENTGEN (NR_SEQUENCIA));

GRANT SELECT ON TASY.RXT_TRAT_CAMPO_ROEN_IMAGEM TO NIVEL_1;

