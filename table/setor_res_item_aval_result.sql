ALTER TABLE TASY.SETOR_RES_ITEM_AVAL_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SETOR_RES_ITEM_AVAL_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.SETOR_RES_ITEM_AVAL_RESULT
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AVALIACAO     NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  QT_RESULTADO         NUMBER(15,4),
  DS_RESULTADO         VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SETRESITAR_MEDITAV_FK_I ON TASY.SETOR_RES_ITEM_AVAL_RESULT
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETRESITAR_MEDITAV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SETRESITAR_PK ON TASY.SETOR_RES_ITEM_AVAL_RESULT
(NR_SEQ_ITEM, NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETRESITAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SETRESITAR_SETRESITAV_FK_I ON TASY.SETOR_RES_ITEM_AVAL_RESULT
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETRESITAR_SETRESITAV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SETOR_RES_ITEM_AVAL_RESULT ADD (
  CONSTRAINT SETRESITAR_PK
 PRIMARY KEY
 (NR_SEQ_ITEM, NR_SEQ_AVALIACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SETOR_RES_ITEM_AVAL_RESULT ADD (
  CONSTRAINT SETRESITAR_MEDITAV_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.MED_ITEM_AVALIAR (NR_SEQUENCIA),
  CONSTRAINT SETRESITAR_SETRESITAV_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.SETOR_RESIDUO_ITEM_AVAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SETOR_RES_ITEM_AVAL_RESULT TO NIVEL_1;

