ALTER TABLE TASY.CONVENIO_TAXA_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_TAXA_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_TAXA_CIRURGIA
(
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_CONVENIO           NUMBER(5)               NOT NULL,
  CD_CATEGORIA          VARCHAR2(10 BYTE)       NOT NULL,
  NR_PORTE_ANESTESICO   NUMBER(2)               NOT NULL,
  CD_TAXA_CIRURGIA      NUMBER(15),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_TIPO_ATENDIMENTO   NUMBER(3),
  NR_SEQUENCIA          NUMBER(10),
  IE_TIPO_REGRA         VARCHAR2(3 BYTE)        NOT NULL,
  CD_TIPO_ACOMODACAO    NUMBER(4),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  QT_PROCEDIMENTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONTACI_PROINTE_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTACI_TIPACID_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTXCI_CATCONV_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTXCI_ESTABEL_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTXCI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONTXCI_PK ON TASY.CONVENIO_TAXA_CIRURGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTXCI_PK
  MONITORING USAGE;


CREATE INDEX TASY.CONTXCI_PROCEDI_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(CD_TAXA_CIRURGIA, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTXCI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTXCI_SETATEN_FK_I ON TASY.CONVENIO_TAXA_CIRURGIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONVENIO_TAXA_CIRURGIA ADD (
  CONSTRAINT CONTXCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_TAXA_CIRURGIA ADD (
  CONSTRAINT CONTACI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT CONTACI_TIPACID_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT CONTXCI_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONTXCI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONTXCI_PROCEDI_FK 
 FOREIGN KEY (CD_TAXA_CIRURGIA, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT CONTXCI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CONVENIO_TAXA_CIRURGIA TO NIVEL_1;

