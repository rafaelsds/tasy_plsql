ALTER TABLE TASY.GPI_INF_RESP_APROV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_INF_RESP_APROV CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_INF_RESP_APROV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_INFORMACAO    NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_APROVACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPIRESAP_GPIINFO_FK_I ON TASY.GPI_INF_RESP_APROV
(NR_SEQ_INFORMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIRESAP_GPIINFO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIRESAP_PESFISI_FK_I ON TASY.GPI_INF_RESP_APROV
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GPIRESAP_PK ON TASY.GPI_INF_RESP_APROV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIRESAP_PK
  MONITORING USAGE;


ALTER TABLE TASY.GPI_INF_RESP_APROV ADD (
  CONSTRAINT GPIRESAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_INF_RESP_APROV ADD (
  CONSTRAINT GPIRESAP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GPIRESAP_GPIINFO_FK 
 FOREIGN KEY (NR_SEQ_INFORMACAO) 
 REFERENCES TASY.GPI_INFORMACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.GPI_INF_RESP_APROV TO NIVEL_1;

