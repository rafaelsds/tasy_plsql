ALTER TABLE TASY.MAN_EQUIP_PERIODO_PARADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_EQUIP_PERIODO_PARADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_EQUIP_PERIODO_PARADO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_PERIODO_INICIAL    DATE,
  DT_PERIODO_FINAL      DATE,
  NR_SEQ_EQUIPAMENTO    NUMBER(10)              NOT NULL,
  NR_SEQ_MOTIVO         NUMBER(10),
  NR_SEQ_REG_PARADA_OS  NUMBER(10),
  NR_EXTERNO_INT        VARCHAR2(40 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MAEQPEP_MANEQUI_FK_I ON TASY.MAN_EQUIP_PERIODO_PARADO
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAEQPEP_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MAEQPEP_MANMEPA_FK_I ON TASY.MAN_EQUIP_PERIODO_PARADO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAEQPEP_MANMEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MAEQPEP_MANOSPA_FK_I ON TASY.MAN_EQUIP_PERIODO_PARADO
(NR_SEQ_REG_PARADA_OS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MAEQPEP_PK ON TASY.MAN_EQUIP_PERIODO_PARADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAEQPEP_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_EQUIP_PERIODO_PARADO ADD (
  CONSTRAINT MAEQPEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_EQUIP_PERIODO_PARADO ADD (
  CONSTRAINT MAEQPEP_MANOSPA_FK 
 FOREIGN KEY (NR_SEQ_REG_PARADA_OS) 
 REFERENCES TASY.MAN_ORDEM_SERVICO_PARADA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MAEQPEP_MANMEPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MAN_MOTIVO_EQUIP_PARADO (NR_SEQUENCIA),
  CONSTRAINT MAEQPEP_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_EQUIP_PERIODO_PARADO TO NIVEL_1;

