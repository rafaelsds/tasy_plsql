ALTER TABLE TASY.AGENDA_TURNO_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_TURNO_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_TURNO_ESP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_AGENDA             NUMBER(10)              NOT NULL,
  DT_AGENDA             DATE                    NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  HR_INICIAL            DATE                    NOT NULL,
  HR_FINAL              DATE                    NOT NULL,
  NR_MINUTO_INTERVALO   NUMBER(10)              NOT NULL,
  IE_CLASSIF_AGENDA     VARCHAR2(5 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_HORARIO_ADICIONAL  VARCHAR2(1 BYTE)        NOT NULL,
  DT_AGENDA_FIM         DATE,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_SALA           NUMBER(10),
  CD_MEDICO_REQ         VARCHAR2(10 BYTE),
  IE_DIA_SEMANA         NUMBER(1),
  QT_IDADE_MIN          NUMBER(3),
  QT_IDADE_MAX          NUMBER(3),
  DS_OBS_INTERNA        VARCHAR2(255 BYTE),
  IE_UTILIZA_AGEWEB     VARCHAR2(1 BYTE),
  IE_ENCAIXE            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGETUES_AGENDA_FK_I ON TASY.AGENDA_TURNO_ESP
(CD_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETUES_AGSACON_FK_I ON TASY.AGENDA_TURNO_ESP
(NR_SEQ_SALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETUES_AGSACON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETUES_I1 ON TASY.AGENDA_TURNO_ESP
(CD_AGENDA, DT_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETUES_PESFISI_FK_I ON TASY.AGENDA_TURNO_ESP
(CD_MEDICO_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGETUES_PK ON TASY.AGENDA_TURNO_ESP
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETUES_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Agenda_Turno_Esp_Atual
before insert or update ON TASY.AGENDA_TURNO_ESP FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
ie_manter_livres_w	varchar2(3);
BEGIN
begin

if	(:new.ie_horario_adicional is null) then
	:new.ie_horario_adicional	:= 'N';
end if;

select	nvl(max(ie_forma_excluir_consulta),'N')
into	ie_manter_livres_w
from	parametro_agenda;

if	(ie_manter_livres_w	= 'N') then
	delete	from agenda_consulta
	where	dt_agenda between :new.hr_inicial and :new.hr_final
	and	ie_status_agenda = 'L'
	and	cd_agenda	 = :new.cd_agenda;
end if;

if	(:new.dt_Agenda_fim is not null) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	dt_agenda between trunc(:new.dt_Agenda) and trunc(:new.dt_Agenda_fim) + 86399 / 86400
	and	cd_agenda	 = :new.cd_agenda;
else
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	dt_agenda >= trunc(:new.dt_Agenda)
	and	cd_agenda	 = :new.cd_agenda;
end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Turno_Esp_Delete
before delete ON TASY.AGENDA_TURNO_ESP FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
ie_manter_livres_w	varchar2(3);
BEGIN
begin

select	nvl(max(ie_forma_excluir_consulta),'N')
into	ie_manter_livres_w
from	parametro_agenda;

if	(ie_manter_livres_w	= 'N') then
	delete	from agenda_consulta
	where	dt_agenda between :old.hr_inicial and :old.hr_final
	and	cd_agenda = :old.cd_agenda
	and	ie_status_agenda = 'L';
end if;

if	(:old.dt_Agenda_fim is not null) then
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	dt_agenda between trunc(:old.dt_Agenda) and trunc(:old.dt_Agenda_fim) + 86399 / 86400
	and	cd_agenda	 = :old.cd_agenda;
else
	delete	from agenda_controle_horario
	where	dt_agenda >= trunc(sysdate)
	and	dt_agenda >= trunc(:old.dt_Agenda)
	and	cd_agenda	 = :old.cd_agenda;
end if;
exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.AGENDA_TURNO_ESP ADD (
  CONSTRAINT AGETUES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_TURNO_ESP ADD (
  CONSTRAINT AGETUES_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGETUES_AGSACON_FK 
 FOREIGN KEY (NR_SEQ_SALA) 
 REFERENCES TASY.AGENDA_SALA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGETUES_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_REQ) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGENDA_TURNO_ESP TO NIVEL_1;

