ALTER TABLE TASY.CONVERSAO_MEDIC_ONCO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVERSAO_MEDIC_ONCO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVERSAO_MEDIC_ONCO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_MATERIAL_GENERICO   NUMBER(6)              NOT NULL,
  CD_MATERIAL_COMERCIAL  NUMBER(6)              NOT NULL,
  IE_TIPO_CONVENIO       NUMBER(2),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_CONVENIO            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONMEDO_CONVENI_FK_I ON TASY.CONVERSAO_MEDIC_ONCO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONMEDO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONMEDO_MATERIA_FK_I ON TASY.CONVERSAO_MEDIC_ONCO
(CD_MATERIAL_COMERCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONMEDO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONMEDO_MATERIA_FK2_I ON TASY.CONVERSAO_MEDIC_ONCO
(CD_MATERIAL_GENERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONMEDO_PK ON TASY.CONVERSAO_MEDIC_ONCO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONVERSAO_MEDIC_ONCO ADD (
  CONSTRAINT CONMEDO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVERSAO_MEDIC_ONCO ADD (
  CONSTRAINT CONMEDO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL_COMERCIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CONMEDO_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_GENERICO) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CONMEDO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CONVERSAO_MEDIC_ONCO TO NIVEL_1;

