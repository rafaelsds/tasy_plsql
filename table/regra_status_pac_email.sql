ALTER TABLE TASY.REGRA_STATUS_PAC_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_STATUS_PAC_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_STATUS_PAC_EMAIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITULO            VARCHAR2(255 BYTE)       NOT NULL,
  DS_REMETENTE         VARCHAR2(255 BYTE)       NOT NULL,
  DS_EMAIL             VARCHAR2(4000 BYTE),
  DS_ANEXO             VARCHAR2(255 BYTE),
  NR_SEQ_STATUS_PAC    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RGSTPCE_PK ON TASY.REGRA_STATUS_PAC_EMAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RGSTPCE_STAPAAG_FK_I ON TASY.REGRA_STATUS_PAC_EMAIL
(NR_SEQ_STATUS_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_STATUS_PAC_EMAIL ADD (
  CONSTRAINT RGSTPCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_STATUS_PAC_EMAIL ADD (
  CONSTRAINT RGSTPCE_STAPAAG_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PAC) 
 REFERENCES TASY.STATUS_PACIENTE_AGENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_STATUS_PAC_EMAIL TO NIVEL_1;

