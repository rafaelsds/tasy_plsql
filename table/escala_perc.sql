ALTER TABLE TASY.ESCALA_PERC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PERC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PERC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_LEG_SWELLING        VARCHAR2(1 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SATURATION          VARCHAR2(1 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_HEART_RATE          VARCHAR2(1 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_HEMOPTYSIS          VARCHAR2(1 BYTE),
  IE_HORMONE_USE         VARCHAR2(1 BYTE),
  IE_RECENT_SURGERY      VARCHAR2(1 BYTE),
  IE_PRIOR_PE            VARCHAR2(1 BYTE),
  IE_AGE                 VARCHAR2(1 BYTE),
  QT_PONTUACAO           NUMBER(3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPERC_ATEPACI_FK_I ON TASY.ESCALA_PERC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPERC_PESFISI_FK_I ON TASY.ESCALA_PERC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPERC_PK ON TASY.ESCALA_PERC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_perc_atual
before insert or update ON TASY.ESCALA_PERC for each row
declare

qt_valor_w	number(3) := 0;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
if	(:new.IE_HEART_RATE = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_HEMOPTYSIS = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_HORMONE_USE  = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_AGE = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_LEG_SWELLING  = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_PRIOR_PE  = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_RECENT_SURGERY = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

if	(:new.IE_SATURATION  = 'S') then
	qt_valor_w := qt_valor_w + 1;
end if;

:new.QT_PONTUACAO := qt_valor_w;

end if;

end;
/


ALTER TABLE TASY.ESCALA_PERC ADD (
  CONSTRAINT ESCPERC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_PERC ADD (
  CONSTRAINT ESCPERC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPERC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_PERC TO NIVEL_1;

