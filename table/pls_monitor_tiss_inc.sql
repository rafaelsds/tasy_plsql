ALTER TABLE TASY.PLS_MONITOR_TISS_INC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_INC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_INC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_INCONSISTENCIA       VARCHAR2(255 BYTE)    NOT NULL,
  CD_INCONSISTENCIA       VARCHAR2(10 BYTE)     NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_INCONSISTENCIA  VARCHAR2(3 BYTE)      NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMOTI_I1 ON TASY.PLS_MONITOR_TISS_INC
(CD_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMOTI_PK ON TASY.PLS_MONITOR_TISS_INC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_MONITOR_TISS_INC ADD (
  CONSTRAINT PLSMOTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_INC TO NIVEL_1;

