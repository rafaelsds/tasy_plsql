ALTER TABLE TASY.RECEP_MOTIVO_DESISTENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RECEP_MOTIVO_DESISTENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.RECEP_MOTIVO_DESISTENCIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_EMPRESA              NUMBER(4),
  CD_ESTABELECIMENTO      NUMBER(4),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DS_MOTIVO               VARCHAR2(150 BYTE)    NOT NULL,
  IE_EXIGE_JUSTIFICATIVA  VARCHAR2(1 BYTE),
  CD_ETAPA_DESISTENCIA    NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECMODES_EMPRESA_FK_I ON TASY.RECEP_MOTIVO_DESISTENCIA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECMODES_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RECMODES_ESTABEL_FK_I ON TASY.RECEP_MOTIVO_DESISTENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECMODES_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RECMODES_PK ON TASY.RECEP_MOTIVO_DESISTENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RECEP_MOTIVO_DESISTENCIA ADD (
  CONSTRAINT RECMODES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RECEP_MOTIVO_DESISTENCIA ADD (
  CONSTRAINT RECMODES_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT RECMODES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.RECEP_MOTIVO_DESISTENCIA TO NIVEL_1;

