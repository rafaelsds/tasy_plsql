ALTER TABLE TASY.CONV_REGRA_VENC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_REGRA_VENC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_REGRA_VENC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ENTREGA_INICIAL   DATE                     NOT NULL,
  DT_ENTREGA_FINAL     DATE                     NOT NULL,
  DT_VENCIMENTO        DATE                     NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREGVE_CONVENI_FK_I ON TASY.CONV_REGRA_VENC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREGVE_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COREGVE_PK ON TASY.CONV_REGRA_VENC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONV_REGRA_VENC ADD (
  CONSTRAINT COREGVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_REGRA_VENC ADD (
  CONSTRAINT COREGVE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CONV_REGRA_VENC TO NIVEL_1;

