ALTER TABLE TASY.PRECO_TUSS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_TUSS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_TUSS
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_EDICAO_AMB           NUMBER(6)             NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15)            NOT NULL,
  IE_ORIGEM_PROCED        NUMBER(10)            NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  VL_PROCEDIMENTO         NUMBER(17,4),
  VL_MEDICO               NUMBER(17,4),
  VL_ANESTESISTA          NUMBER(17,4),
  VL_AUXILIARES           NUMBER(17,4),
  VL_CUSTO_OPERACIONAL    NUMBER(17,4),
  VL_FILME                NUMBER(17,4),
  QT_FILME_AMB            NUMBER(15,4),
  NR_AUXILIARES_AMB       NUMBER(2),
  QT_INCIDENCIA_AMB       NUMBER(2),
  NR_PORTE_ANEST_AMB      NUMBER(2),
  DT_FINAL_VIGENCIA       DATE,
  CD_PORTE_CBHPM          VARCHAR2(10 BYTE),
  TX_PORTE                NUMBER(15,4),
  QT_UCO                  NUMBER(15,4),
  NR_PORTE_ANEST_CBHPM    NUMBER(2),
  NR_AUXILIARES_CBHPM     NUMBER(2),
  QT_FILME_CBHPM          NUMBER(15,4),
  QT_INCIDENCIA_CBHPM     NUMBER(2),
  CD_MOEDA                NUMBER(3),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRETUSS_EDIAMB_FK_I ON TASY.PRECO_TUSS
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRETUSS_EDIAMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRETUSS_I1 ON TASY.PRECO_TUSS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRETUSS_MOEDA_FK_I ON TASY.PRECO_TUSS
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRETUSS_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRETUSS_PK ON TASY.PRECO_TUSS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRETUSS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRETUSS_PROCEDI_FK_I ON TASY.PRECO_TUSS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRETUSS_PROCEDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRETUSS_UK ON TASY.PRECO_TUSS
(CD_EDICAO_AMB, CD_PROCEDIMENTO, IE_ORIGEM_PROCED, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRETUSS_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PRECO_TUSS_tp  after update ON TASY.PRECO_TUSS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PROCEDIMENTO,1,4000),substr(:new.VL_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PROCEDIMENTO',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MEDICO,1,4000),substr(:new.VL_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MEDICO',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_AUXILIARES,1,4000),substr(:new.VL_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUXILIARES',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUXILIARES_CBHPM,1,4000),substr(:new.NR_AUXILIARES_CBHPM,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUXILIARES_CBHPM',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PORTE_ANEST_AMB,1,4000),substr(:new.NR_PORTE_ANEST_AMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTE_ANEST_AMB',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CUSTO_OPERACIONAL,1,4000),substr(:new.VL_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_CUSTO_OPERACIONAL',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUXILIARES_AMB,1,4000),substr(:new.NR_AUXILIARES_AMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUXILIARES_AMB',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PORTE_ANEST_CBHPM,1,4000),substr(:new.NR_PORTE_ANEST_CBHPM,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTE_ANEST_CBHPM',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ANESTESISTA,1,4000),substr(:new.VL_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ANESTESISTA',ie_log_w,ds_w,'PRECO_TUSS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.preco_tuss_atual
before insert or update ON TASY.PRECO_TUSS for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_final_vigencia, 'F');

end preco_tuss_atual;
/


ALTER TABLE TASY.PRECO_TUSS ADD (
  CONSTRAINT PRETUSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRETUSS_UK
 UNIQUE (CD_EDICAO_AMB, CD_PROCEDIMENTO, IE_ORIGEM_PROCED, DT_INICIO_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_TUSS ADD (
  CONSTRAINT PRETUSS_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PRETUSS_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT PRETUSS_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PRECO_TUSS TO NIVEL_1;

