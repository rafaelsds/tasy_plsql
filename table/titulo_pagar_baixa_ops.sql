ALTER TABLE TASY.TITULO_PAGAR_BAIXA_OPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_BAIXA_OPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_BAIXA_OPS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_BAIXA         NUMBER(5)                NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  IE_DEBITO_CREDITO    VARCHAR2(1 BYTE)         NOT NULL,
  CD_HISTORICO         NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10)               NOT NULL,
  VL_MOVIMENTO         NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORIGEM            VARCHAR2(15 BYTE)        NOT NULL,
  NR_CODIGO_CONTROLE   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPAGBOPS_CONCONT_FK_I ON TASY.TITULO_PAGAR_BAIXA_OPS
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGBOPS_LOTCONT_FK_I ON TASY.TITULO_PAGAR_BAIXA_OPS
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPAGBOPS_PK ON TASY.TITULO_PAGAR_BAIXA_OPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGBOPS_TITPABA_FK_I ON TASY.TITULO_PAGAR_BAIXA_OPS
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGBOPS_TITPAGA_FK_I ON TASY.TITULO_PAGAR_BAIXA_OPS
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_baixa_ops_insert
after insert ON TASY.TITULO_PAGAR_BAIXA_OPS for each row
declare

ie_contab_tit_interc_cancel_w		pls_parametro_contabil.ie_contab_tit_interc_cancel%type;
cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
nr_seq_baixa_w				titulo_pagar_baixa.nr_sequencia%type;
dt_baixa_w				titulo_pagar_baixa.dt_baixa%type;
vl_movimento_w				titulo_pagar_baixa_ops.vl_movimento%type;
nr_seq_trans_financ_w			titulo_pagar_baixa.nr_seq_trans_fin%type;
ie_concil_contab_w			pls_visible_false.ie_concil_contab%type;

cursor c01 is
	select 	a.nm_atributo,
		36 cd_tipo_lote_contab
	from	atributo_contab a
	where	((a.cd_tipo_lote_contab = 36
		and	a.nm_atributo = 'VL_INTERCAMBIO'
		and	nvl(:new.ie_origem, 'X') = 'I')
	or 	(a.cd_tipo_Lote_contab = 7
		and	((a.nm_atributo = 'VL_COPARTICIPACAO'
		and	nvl(:new.ie_origem, 'X') = 'C')
		or	(a.nm_atributo = 'VL_EVENTO_OPS'
		and	nvl(:new.ie_origem, 'X') = 'E')
		or	(a.nm_atributo = 'VL_PAGAMENTO_OPS'
		and	nvl(:new.ie_origem, 'X') = 'P')
		or	(a.nm_atributo = 'VL_REEMBOLSO'
		and	nvl(:new.ie_origem, 'X') = 'R'))))
	and	exists	(select	1
			from	titulo_pagar	x,
				ptu_fatura	y
			where	x.nr_titulo		= y.nr_titulo
			and	x.nr_titulo		= :new.nr_titulo
			and	x.ie_origem_titulo	= '16'
			and	nvl(x.ie_status,'D')	= 'D'
			and	((x.ie_situacao		!= 'C') or
				(ie_contab_tit_interc_cancel_w = 'S'))
			union all
			select	1
			from	titulo_pagar	x,
				ptu_fatura	y
			where	x.nr_titulo		= y.nr_titulo_ndc
			and	x.nr_titulo		= :new.nr_titulo
			and	x.ie_origem_titulo	= '16'
			and	nvl(x.ie_status,'D')	= 'D'
			and	((x.ie_situacao		!= 'C') or
				(ie_contab_tit_interc_cancel_w = 'S')))
	and	ie_concil_contab_w = 'S';

c01_w		c01%rowtype;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

	select  max(cd_estabelecimento)
	into    cd_estabelecimento_w
	from    titulo_pagar
	where   nr_titulo = :new.nr_titulo;

	select	nvl(max(ie_contab_tit_interc_cancel), 'S')
	into	ie_contab_tit_interc_cancel_w
	from	pls_parametro_contabil
	where 	cd_estabelecimento = cd_estabelecimento_w;

	select 	nr_sequencia,
		dt_baixa,
		nr_seq_trans_fin
	into	nr_seq_baixa_w,
		dt_baixa_w,
		nr_seq_trans_financ_w
	from	titulo_pagar_baixa
	where	nr_sequencia 	= :new.nr_seq_baixa
	and	nr_titulo	= :new.nr_titulo;

	begin
	select	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from	pls_visible_false
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception when others then
		ie_concil_contab_w := 'N';
	end;

	open c01;
	loop
	fetch c01 into
		c01_w;
	exit when c01%notfound;
	begin
	vl_movimento_w := :new.vl_movimento;

	if	(nvl(vl_movimento_w, 0) <> 0) then
		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									dt_baixa_w,
									c01_w.cd_tipo_lote_contab,
									nr_seq_trans_financ_w,
									13,
									:new.nr_titulo,
									nr_seq_baixa_w,
									:new.nr_sequencia,
									vl_movimento_w,
									'TITULO_PAGAR_BAIXA_OPS',
									c01_w.nm_atributo,
									:new.nm_usuario);
	end if;
	end;
	end loop;
	close c01;
end if;
end;
/


ALTER TABLE TASY.TITULO_PAGAR_BAIXA_OPS ADD (
  CONSTRAINT TITPAGBOPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_BAIXA_OPS ADD (
  CONSTRAINT TITPAGBOPS_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITPAGBOPS_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAGBOPS_TITPABA_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_PAGAR_BAIXA (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TITULO_PAGAR_BAIXA_OPS TO NIVEL_1;

