ALTER TABLE TASY.ESCALA_PIPER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PIPER CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PIPER
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  CD_PROFISSIONAL             VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                DATE              NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_LIBERACAO                DATE,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  QT_ESTRESSE_FADIGA          NUMBER(3),
  IE_TEMPO_FADIGA             VARCHAR2(3 BYTE),
  IE_OUTRO_SINTOMA            VARCHAR2(1 BYTE),
  DS_OUTRO_SINTOMA            VARCHAR2(255 BYTE),
  QT_TEMPO_FADIGA             NUMBER(3),
  QT_FADIGA_INTEF_CAPACIDADE  NUMBER(3),
  QT_FADIGA_INTEF_HABILIDADE  NUMBER(3),
  QT_FADIGA_INTEF_HAB_SEXO    NUMBER(3),
  QT_FADIGA_CAP_ATIV          NUMBER(3),
  QT_INTENSIDADE              NUMBER(3),
  QT_FADIGA_AGORA_AGRADAVEL   NUMBER(3),
  QT_FADIGA_AGORA_ACEITAVEL   NUMBER(3),
  QT_FADIGA_AGORA_PROTETORA   NUMBER(3),
  QT_FADIGA_AGORA_POSITIVA    NUMBER(3),
  QT_FADIGA_AGORA_NORMAL      NUMBER(3),
  QT_SENTINDO_FORTE           NUMBER(3),
  QT_SENTINDO_ACORDADO        NUMBER(3),
  QT_SENTINDO_VIDA            NUMBER(3),
  QT_SENTINDO_VIGOR           NUMBER(3),
  QT_SENTINDO_ENERGIA         NUMBER(3),
  QT_SENTINDO_PACIENTE        NUMBER(3),
  QT_SENTINDO_RELAXADO        NUMBER(3),
  QT_SENTINDO_FELIZ           NUMBER(3),
  QT_SENTINDO_CONCENTRAR      NUMBER(3),
  QT_SENTINDO_LEMBRAR         NUMBER(3),
  QT_SENTINDO_PENSAR          NUMBER(3),
  DS_CONTRIBUI_FADIGA         VARCHAR2(2000 BYTE),
  DS_ALIVIA_FADIGA            VARCHAR2(2000 BYTE),
  DS_MELHORAR_FADIGA          VARCHAR2(2000 BYTE),
  QT_SCORE_COMPORTAMENTAL     NUMBER(10,2),
  QT_SCORE_AFETIVO            NUMBER(10,2),
  QT_SCORE_SENSORIAL          NUMBER(10,2),
  QT_SCORE_COGNITIVO          NUMBER(10,2),
  QT_SCORE                    NUMBER(10,2),
  NR_SEQ_REG_ELEMENTO         NUMBER(10),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPIPE_ATEPACI_FK_I ON TASY.ESCALA_PIPER
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPIPE_EHRREEL_FK_I ON TASY.ESCALA_PIPER
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPIPE_PESFISI_FK_I ON TASY.ESCALA_PIPER
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPIPE_PK ON TASY.ESCALA_PIPER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPIPE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_PIPER_ATUAL
before insert or update ON TASY.ESCALA_PIPER for each row
declare
--Subgrupo Comportamental
qt_reg_comport_w		number(5) := 0;
qt_result_comport_w	number(10,2) := 0;
--Subgrupo Afetivo
qt_reg_significado_w	number(5) := 0;
qt_result_significado_w	number(10,2) := 0;
--Subgrupo Sensorial
qt_reg_sensorial_w		number(5) := 0;
qt_result_sensorial		number(10,2) := 0;
--Subgrupo Cognitivo
qt_reg_cognitivo_w		number(5) := 0;
qt_result_cognitivo_w	number(10,2) := 0;

--Verifica se 75% dos subgrupos est�o preenchidos
qt_percent_subgrupo_w	number(5) := 0;
qt_quantidade_total_w	number(5) := 0;
qt_result_total_w		number(10,2) := 0;


begin

if	(:new.qt_estresse_fadiga is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_estresse_fadiga;
end if;
if	(:new.qt_fadiga_intef_capacidade is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_fadiga_intef_capacidade;
end if;
if	(:new.qt_fadiga_intef_habilidade is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_fadiga_intef_habilidade;
end if;
if	(:new.qt_fadiga_intef_hab_sexo is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_fadiga_intef_hab_sexo;
end if;
if	(:new.qt_fadiga_cap_ativ is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_fadiga_cap_ativ;
end if;
if	(:new.qt_intensidade is not null) then
	qt_reg_comport_w	:= 	qt_reg_comport_w + 1;
	qt_result_comport_w	:= 	qt_result_comport_w + :new.qt_intensidade;
end if;
if	(qt_reg_comport_w >= 5) then
	qt_percent_subgrupo_w		:= 	qt_percent_subgrupo_w + 1;
	:new.QT_SCORE_COMPORTAMENTAL	:= 	( qt_result_comport_w / qt_reg_comport_w );
end if;

if	(:new.qt_fadiga_agora_agradavel is not null) then
	qt_reg_significado_w	:= 	qt_reg_significado_w + 1;
	qt_result_significado_w := 	qt_result_significado_w +  :new.qt_fadiga_agora_agradavel;
end if;
if	(:new.qt_fadiga_agora_aceitavel is not null) then
	qt_reg_significado_w	:= 	qt_reg_significado_w + 1;
	qt_result_significado_w := 	qt_result_significado_w +  :new.qt_fadiga_agora_aceitavel;
end if;
if	(:new.qt_fadiga_agora_protetora is not null) then
	qt_reg_significado_w	:= 	qt_reg_significado_w + 1;
	qt_result_significado_w := 	qt_result_significado_w +  :new.qt_fadiga_agora_protetora;
end if;
if	(:new.qt_fadiga_agora_positiva is not null) then
	qt_reg_significado_w	:= 	qt_reg_significado_w + 1;
	qt_result_significado_w := 	qt_result_significado_w +  :new.qt_fadiga_agora_positiva;
end if;
if	(:new.qt_fadiga_agora_normal is not null) then
	qt_reg_significado_w	:= 	qt_reg_significado_w + 1;
	qt_result_significado_w := 	qt_result_significado_w +  :new.qt_fadiga_agora_normal;
end if;
if	(qt_reg_significado_w >= 4) then
	qt_percent_subgrupo_w	:= 	qt_percent_subgrupo_w + 1;
	:new.QT_SCORE_AFETIVO	:= 	( qt_result_significado_w / qt_reg_significado_w );
end if;

if	(:new.QT_SENTINDO_FORTE is not null) then
	qt_reg_sensorial_w	:= 	qt_reg_sensorial_w + 1;
	qt_result_sensorial 	:= 	qt_result_sensorial +  :new.QT_SENTINDO_FORTE;
end if;
if	(:new.QT_SENTINDO_ACORDADO is not null) then
	qt_reg_sensorial_w	:= 	qt_reg_sensorial_w + 1;
	qt_result_sensorial 	:= 	qt_result_sensorial +  :new.QT_SENTINDO_ACORDADO;
end if;
if	(:new.QT_SENTINDO_VIDA is not null) then
	qt_reg_sensorial_w	:= 	qt_reg_sensorial_w + 1;
	qt_result_sensorial 	:= 	qt_result_sensorial +  :new.QT_SENTINDO_VIDA;
end if;
if	(:new.QT_SENTINDO_VIGOR is not null) then
	qt_reg_sensorial_w	:= 	qt_reg_sensorial_w + 1;
	qt_result_sensorial 	:= 	qt_result_sensorial +  :new.QT_SENTINDO_VIGOR;
end if;
if	(:new.QT_SENTINDO_ENERGIA is not null) then
	qt_reg_sensorial_w	:= 	qt_reg_sensorial_w + 1;
	qt_result_sensorial 	:= 	qt_result_sensorial +  :new.QT_SENTINDO_ENERGIA;
end if;
if	(qt_reg_sensorial_w >= 4) then
	qt_percent_subgrupo_w	:=	qt_percent_subgrupo_w + 1;
	:new.QT_SCORE_SENSORIAL	:= 	( qt_result_sensorial / qt_reg_sensorial_w );
end if;

if	(:new.QT_SENTINDO_PACIENTE is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_PACIENTE;
end if;
if	(:new.QT_SENTINDO_RELAXADO is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_RELAXADO;
end if;
if	(:new.QT_SENTINDO_FELIZ is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_FELIZ;
end if;
if	(:new.QT_SENTINDO_CONCENTRAR is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_CONCENTRAR;
end if;
if	(:new.QT_SENTINDO_LEMBRAR is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_LEMBRAR;
end if;
if	(:new.QT_SENTINDO_PENSAR is not null) then
	qt_reg_cognitivo_w	:= 	qt_reg_cognitivo_w + 1;
	qt_result_cognitivo_w 	:= 	qt_result_cognitivo_w +  :new.QT_SENTINDO_PENSAR;
end if;
if	(qt_reg_cognitivo_w >= 5) then
	qt_percent_subgrupo_w	:= 	qt_percent_subgrupo_w + 1;
	:new.QT_SCORE_COGNITIVO	:= 	( qt_result_cognitivo_w / qt_reg_cognitivo_w );
end if;

qt_quantidade_total_w	:= qt_reg_comport_w + qt_reg_significado_w + qt_reg_sensorial_w + qt_reg_cognitivo_w;

if	(qt_quantidade_total_w >=  17) then
	qt_result_total_w	:= qt_result_comport_w + qt_result_significado_w + qt_result_sensorial + qt_result_cognitivo_w;
	:new.QT_SCORE		:= (qt_result_total_w / qt_quantidade_total_w);
end if;

end;
/


ALTER TABLE TASY.ESCALA_PIPER ADD (
  CONSTRAINT ESCPIPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PIPER ADD (
  CONSTRAINT ESCPIPE_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCPIPE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPIPE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_PIPER TO NIVEL_1;

