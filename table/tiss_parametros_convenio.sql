ALTER TABLE TASY.TISS_PARAMETROS_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_PARAMETROS_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_PARAMETROS_CONVENIO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_CONVENIO                    NUMBER(5)      NOT NULL,
  IE_DATA_DESPESA                VARCHAR2(2 BYTE) NOT NULL,
  IE_MEDICO_SOLIC_ATEND          VARCHAR2(2 BYTE) NOT NULL,
  IE_AGRUPAR_PROC                VARCHAR2(1 BYTE) NOT NULL,
  IE_FECHAR_CONTA_INCONS         VARCHAR2(2 BYTE) NOT NULL,
  IE_CID_SPSADT                  VARCHAR2(1 BYTE) NOT NULL,
  IE_PARTIC_CONVENIADO           VARCHAR2(1 BYTE) NOT NULL,
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  IE_CONSISTE_CODIGO             VARCHAR2(2 BYTE) NOT NULL,
  IE_ATUALIZAR_ALTA              VARCHAR2(2 BYTE) NOT NULL,
  IE_EXEC_SPSADT                 VARCHAR2(3 BYTE) NOT NULL,
  IE_TOTALIZAR_OPM               VARCHAR2(2 BYTE) NOT NULL,
  IE_CONSISTE_TISS               VARCHAR2(2 BYTE) NOT NULL,
  IE_SPSADT_ZERADO               VARCHAR2(2 BYTE) NOT NULL,
  IE_RI_ZERADO                   VARCHAR2(2 BYTE) NOT NULL,
  IE_GUIA_PROTOCOLO              VARCHAR2(3 BYTE) NOT NULL,
  IE_DESPESA_ZERADA              VARCHAR2(3 BYTE) NOT NULL,
  DS_MASCARA_USUARIO             VARCHAR2(140 BYTE),
  IE_ARREDONDAMENTO_DESP         VARCHAR2(3 BYTE) NOT NULL,
  IE_PROC_PARTIC                 VARCHAR2(3 BYTE) NOT NULL,
  IE_PROC_SOLIC_SPSADT           VARCHAR2(3 BYTE),
  IE_CONSISTE_AUTOR_SPSADT       VARCHAR2(3 BYTE) NOT NULL,
  IE_SOLICITANTE                 VARCHAR2(3 BYTE) NOT NULL,
  IE_NUMERO_LOTE                 VARCHAR2(3 BYTE),
  IE_CRM                         VARCHAR2(3 BYTE) NOT NULL,
  IE_GUIA_PRESTADOR              VARCHAR2(3 BYTE),
  IE_DESP_TISS_INTERNADO         VARCHAR2(3 BYTE) NOT NULL,
  IE_CONSISTE_DESP_NEG           VARCHAR2(3 BYTE) NOT NULL,
  IE_CONSISTE_VIA_ACESSO         VARCHAR2(3 BYTE) NOT NULL,
  IE_COD_TISS_BRASINDICE         VARCHAR2(3 BYTE) NOT NULL,
  IE_SAIDA_INTERNACAO            VARCHAR2(3 BYTE) NOT NULL,
  IE_TOTAL_GUIA_DESP             VARCHAR2(3 BYTE) NOT NULL,
  IE_DATA_PROC                   VARCHAR2(3 BYTE) NOT NULL,
  IE_GERAR_CTA_DIF_TISS          VARCHAR2(3 BYTE),
  IE_SENHA_GUIA                  VARCHAR2(3 BYTE) NOT NULL,
  IE_GUIA_HONORARIO              VARCHAR2(3 BYTE) NOT NULL,
  IE_FECHAR_PROT_INCONS          VARCHAR2(2 BYTE) NOT NULL,
  IE_CAMPO_BOOLEAN               VARCHAR2(3 BYTE) NOT NULL,
  IE_GUIA_PRINC_HONOR            VARCHAR2(3 BYTE) NOT NULL,
  IE_MASCARA_CID                 VARCHAR2(3 BYTE) NOT NULL,
  DS_FORMATO_CARTEIRA            VARCHAR2(50 BYTE),
  IE_ENVIO_CARTEIRA              VARCHAR2(10 BYTE),
  DS_MASCARA_LOTE                VARCHAR2(20 BYTE),
  IE_DESC_PROC_INTERNO           VARCHAR2(3 BYTE) NOT NULL,
  IE_ORDENACAO_OD                VARCHAR2(5 BYTE),
  IE_HONOR_PARTIC                VARCHAR2(3 BYTE) NOT NULL,
  IE_CONSISTIR_OBS               VARCHAR2(3 BYTE) NOT NULL,
  QT_DIAS_VALIDADE_GUIA          NUMBER(5),
  DS_EMAIL_AUTOR                 VARCHAR2(255 BYTE),
  NR_SEQ_SAIDA_SPSADT            NUMBER(10),
  IE_PARTIC_SPSADT               VARCHAR2(1 BYTE) NOT NULL,
  IE_ASSINATURA_SOLIC            VARCHAR2(2 BYTE) NOT NULL,
  IE_EMITE_OPM_ZERADO            VARCHAR2(2 BYTE) NOT NULL,
  IE_DIGITOS_DESP                VARCHAR2(1 BYTE) NOT NULL,
  IE_ABORTO_NASC                 VARCHAR2(1 BYTE) NOT NULL,
  IE_CONSISTE_INDICACAO          VARCHAR2(1 BYTE) NOT NULL,
  IE_REDUCAO_ACRESC              VARCHAR2(3 BYTE),
  IE_TOTAL_OPM                   VARCHAR2(5 BYTE) NOT NULL,
  IE_CONTRATADO_CONSULTA         VARCHAR2(5 BYTE) NOT NULL,
  QT_GUIAS_PROTOCOLO             NUMBER(10),
  IE_DATA_ENTRADA_RI             VARCHAR2(3 BYTE) NOT NULL,
  QT_MAX_DESPESA                 NUMBER(10),
  IE_CONSISTIR_PROC              VARCHAR2(1 BYTE) NOT NULL,
  IE_CONTRAT_EXEC_HI             VARCHAR2(5 BYTE) NOT NULL,
  IE_CONSISTIR_DECL_OBITO        VARCHAR2(1 BYTE),
  IE_DT_CONTRAT_RI               VARCHAR2(3 BYTE),
  IE_PERIODO_DESP                VARCHAR2(5 BYTE),
  IE_CONSISTE_TEC_UTIL           VARCHAR2(1 BYTE),
  IE_PARTICIPANTE_RI             VARCHAR2(1 BYTE),
  IE_CONSISTIR_GUIA_SENHA        VARCHAR2(1 BYTE),
  IE_RELAT_AUTOR                 VARCHAR2(1 BYTE),
  IE_REGRA_MASCARA               VARCHAR2(1 BYTE),
  IE_SEPARAR_SPSADT_HONOR        VARCHAR2(1 BYTE),
  IE_VALOR_DESCONTO              VARCHAR2(1 BYTE),
  IE_TIPO_ACOMOD                 VARCHAR2(5 BYTE),
  IE_EXEC_RI                     VARCHAR2(5 BYTE),
  IE_FORMATO_RED_ACRES           VARCHAR2(1 BYTE),
  IE_EMITE_HONOR_SADT_PEP        VARCHAR2(5 BYTE),
  IE_ORDENACAO_XML               VARCHAR2(5 BYTE),
  IE_DATA_TRANS_XML              VARCHAR2(2 BYTE),
  IE_AGRUPAR_DESP_PAC            VARCHAR2(2 BYTE),
  IE_MEDICO_REP                  VARCHAR2(5 BYTE),
  IE_HONOR_ZERADO                VARCHAR2(5 BYTE),
  IE_NOME_ARQUIVO                VARCHAR2(2 BYTE),
  IE_CONSISTE_TIPO_SAIDA         VARCHAR2(2 BYTE),
  IE_CONSISTE_CID_AUTOR          VARCHAR2(2 BYTE),
  IE_CID_SOLIC_EXTERNO           VARCHAR2(2 BYTE),
  IE_ESPECIALIDADE_EXEC          VARCHAR2(2 BYTE),
  IE_OBSERVACAO                  VARCHAR2(1 BYTE),
  IE_VALOR_UNITARIO              VARCHAR2(3 BYTE),
  IE_PROTOCOLO_RET               VARCHAR2(3 BYTE),
  IE_ORDENACAO_SPSADT            VARCHAR2(3 BYTE),
  IE_CARTEIRA_MED                VARCHAR2(2 BYTE),
  IE_GUIA_DESP_MED               VARCHAR2(1 BYTE),
  IE_DT_EMISSAO_GUIA             VARCHAR2(5 BYTE),
  IE_TOTAL_GERAL_OPM             VARCHAR2(1 BYTE),
  IE_VALOR_UNITARIO_OPM          VARCHAR2(5 BYTE),
  IE_OBS_URGENTE                 VARCHAR2(1 BYTE),
  IE_LADO_REP                    VARCHAR2(1 BYTE),
  IE_QUEBRA_PROC_PEP             VARCHAR2(1 BYTE),
  IE_CONVERSAO_QTDE_MAT          VARCHAR2(5 BYTE),
  DS_DIR_PADRAO                  VARCHAR2(255 BYTE),
  DS_DIR_PADRAO_RET              VARCHAR2(255 BYTE),
  IE_AGRUP_RESP_CRED             VARCHAR2(1 BYTE),
  IE_DATA_ENTRADA_ALTA           VARCHAR2(5 BYTE),
  NR_SEQ_SAIDA_INT               NUMBER(10),
  IE_PACOTE                      VARCHAR2(5 BYTE),
  IE_PROC_TUSS                   VARCHAR2(5 BYTE),
  IE_PRESCR_UNICA                VARCHAR2(1 BYTE),
  IE_DATA_ASS_PREST              VARCHAR2(1 BYTE),
  IE_NOME_ABREV                  VARCHAR2(1 BYTE),
  IE_SENHA_GUIA_PRINC            VARCHAR2(1 BYTE),
  IE_DESC_PLANO                  VARCHAR2(1 BYTE),
  IE_DESC_CONVENIO               VARCHAR2(5 BYTE),
  IE_PROT_SOLIC_STATUS           VARCHAR2(5 BYTE),
  IE_DATA_REF_TAB_MAT            VARCHAR2(5 BYTE),
  IE_CONSISTIR_CPF               VARCHAR2(1 BYTE),
  IE_CONSISTIR_UF_CRM_SOLIC      VARCHAR2(1 BYTE) NOT NULL,
  DS_ARQUIVO_LOGO_COMP           VARCHAR2(255 BYTE),
  IE_CONSISTIR_GUIA_DUP          VARCHAR2(1 BYTE),
  IE_DT_PROC_TASYMED             VARCHAR2(5 BYTE),
  IE_CONSISTIR_PROC_ZERADO       VARCHAR2(1 BYTE),
  IE_ATUAL_CONTA                 VARCHAR2(1 BYTE),
  IE_CONS_TEMPO_DOENCA           VARCHAR2(1 BYTE),
  IE_AGRUP_EXAME                 VARCHAR2(1 BYTE),
  IE_DT_AUTOR_SADT               VARCHAR2(1 BYTE),
  IE_FORCAR_AGRUP                VARCHAR2(1 BYTE),
  IE_TIPO_FATUR_TISS             VARCHAR2(1 BYTE),
  NR_SEQ_SAIDA_CONSULTA          NUMBER(10),
  IE_GERAR_TISS                  VARCHAR2(1 BYTE),
  DS_FUNCTION_OBS_SPSADT         VARCHAR2(255 BYTE),
  IE_CONSISTE_CBO                VARCHAR2(1 BYTE),
  IE_SENHA_AUTOR                 VARCHAR2(5 BYTE),
  DS_ADICIONAL_GUIA_PREST        VARCHAR2(20 BYTE),
  CD_ADIC_CRM                    VARCHAR2(5 BYTE),
  IE_CONSISTE_SENHA_AUTOR        VARCHAR2(1 BYTE),
  IE_DT_AUTOR_RI                 VARCHAR2(5 BYTE),
  IE_DT_VALIDADE_SENHA_RI        VARCHAR2(5 BYTE),
  IE_MEMBRO_EXEC                 VARCHAR2(1 BYTE),
  IE_DT_ASSIN_SOLIC              VARCHAR2(2 BYTE),
  DS_FUNCTION_OBS_CONSULTA       VARCHAR2(255 BYTE),
  IE_ALT_TIPO_ATEND_CONTA        VARCHAR2(1 BYTE),
  IE_GRAVAR_LOG_CONTA            VARCHAR2(1 BYTE),
  IE_GESTACAO_NASC_VIVOS         VARCHAR2(2 BYTE),
  IE_GESTACAO_DIF_ABORTO         VARCHAR2(1 BYTE),
  IE_CONSISTE_QT_PROC            VARCHAR2(1 BYTE),
  IE_ATUALIZA_SENHA_CONTA        VARCHAR2(2 BYTE),
  IE_TRANSTORNO_ABORTO           VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_TISS              NUMBER(10),
  DS_MASCARA_CRM                 VARCHAR2(140 BYTE),
  IE_CID_RI                      VARCHAR2(1 BYTE) NOT NULL,
  DS_NAMESPACE                   VARCHAR2(255 BYTE),
  IE_CODIGO_INTERNO_DESP         VARCHAR2(1 BYTE),
  IE_CD_AUTORIZACAO_RELAT        VARCHAR2(5 BYTE),
  IE_DESC_EXAME_EXT              VARCHAR2(5 BYTE),
  IE_PREST_SPSADT_TASYMED        VARCHAR2(5 BYTE),
  IE_FONTE_PAGADORA              VARCHAR2(5 BYTE),
  NR_LOTE_ATUAL                  NUMBER(10),
  IE_TOPO_REP                    VARCHAR2(1 BYTE),
  IE_MASCARA_DNV                 VARCHAR2(3 BYTE),
  IE_DATA_ATECACO                VARCHAR2(5 BYTE),
  IE_GUIA_NUMERICA               VARCHAR2(2 BYTE),
  IE_FORCAR_AGRUP_PARTIC         VARCHAR2(1 BYTE),
  IE_MEDICO_CONVENIO             VARCHAR2(1 BYTE),
  IE_TIPO_FATUR_PERIODO          VARCHAR2(2 BYTE),
  IE_SPSADT_ATEND_RETORNO        VARCHAR2(2 BYTE),
  IE_CONS_ATEND_RETORNO          VARCHAR2(2 BYTE),
  IE_ORDENACAO_RI                VARCHAR2(3 BYTE),
  IE_ATUALIZA_NR_GUIA            VARCHAR2(2 BYTE),
  IE_COD_TISS_SIMPRO             VARCHAR2(3 BYTE),
  IE_CRM_CONTRAT_SOLIC           VARCHAR2(1 BYTE),
  IE_LADO_CONTA                  VARCHAR2(1 BYTE),
  IE_CONSISTE_GRAU_PARTIC        VARCHAR2(1 BYTE),
  IE_SOLIC_DEM_PAGTO             VARCHAR2(2 BYTE),
  IE_NAO_HONORARIO_SADT          VARCHAR2(2 BYTE),
  IE_GUIA_DESP_PROC_PRINC        VARCHAR2(1 BYTE),
  IE_SADT_AUTOR_SEM_PROC         VARCHAR2(1 BYTE),
  IE_MEDICO_SOLIC_EXAME          VARCHAR2(2 BYTE),
  IE_DIAG_TIPO_ATEND             VARCHAR2(1 BYTE),
  IE_ASSINATURA_EXEC             VARCHAR2(2 BYTE),
  IE_EMITIR_HEMOCOMPONENTE       VARCHAR2(1 BYTE),
  IE_QUEBRA_GRUPO_EXAME          VARCHAR2(1 BYTE),
  IE_AGRUPAR_HONOR               VARCHAR2(1 BYTE),
  IE_GUIA_PRINC_AUTOR            VARCHAR2(2 BYTE),
  IE_ULTIMA_PRESCRICAO           VARCHAR2(1 BYTE),
  IE_GUIA_OPERADORA_AUTOR        VARCHAR2(2 BYTE),
  IE_CONVERSAO_SADT_SOLIC        VARCHAR2(2 BYTE),
  IE_ARQUIVOS_ZIP                VARCHAR2(1 BYTE),
  IE_CONSISTE_CNS                VARCHAR2(1 BYTE),
  IE_MATERIAL_EXEC               VARCHAR2(1 BYTE),
  IE_QTD_MATERIAL                VARCHAR2(1 BYTE),
  IE_SENHA_PROC_PRESCR           VARCHAR2(1 BYTE),
  IE_GUIA_SOLIC_INTE_PROC        VARCHAR2(1 BYTE),
  IE_CONSISTE_CODIGO_ANS         VARCHAR2(1 BYTE),
  IE_TIPO_SAIDA_PARCIAL          VARCHAR2(1 BYTE),
  IE_OBITO_PARCIAL               VARCHAR2(1 BYTE),
  DS_CARACTERES_XML              VARCHAR2(255 BYTE),
  IE_AGRUPAR_OPME                VARCHAR2(1 BYTE),
  IE_QUEBRA_HORARIOS_PRESCR      VARCHAR2(1 BYTE),
  IE_LOTE_XML_DESPESA            VARCHAR2(1 BYTE),
  IE_DESC_LAUDO_FATUR_MED        VARCHAR2(1 BYTE),
  IE_CONSISTE_VALOR_GUIA         VARCHAR2(1 BYTE),
  IE_DATA_ASSIN_AUTOR            VARCHAR2(2 BYTE),
  IE_QUEBRA_GUIA_PRESCR_ASSOC    VARCHAR2(1 BYTE),
  IE_GUIA_PEDIDO_EXAME           VARCHAR2(1 BYTE),
  IE_OBS_PRESCR_AUTOR            VARCHAR2(1 BYTE),
  IE_GUIA_REF_PRORROG            VARCHAR2(1 BYTE),
  IE_SENHA_MAT_GUIA_PROC         VARCHAR2(1 BYTE),
  IE_PREST_EXEC_PEDIDO_EXAME     VARCHAR2(1 BYTE),
  IE_DATA_LOTE_ANEXO             VARCHAR2(1 BYTE),
  IE_QT_DOSE_SOLIC_QUIMIO        VARCHAR2(1 BYTE),
  IE_REF_SOLIC_DEM_PAGTO         VARCHAR2(1 BYTE),
  IE_CBO_AUTOR_ESPEC             VARCHAR2(1 BYTE),
  IE_SETOR_EXEC_PROC             VARCHAR2(1 BYTE),
  IE_CONSISTE_TUSS_AUTOR         VARCHAR2(1 BYTE),
  IE_SENHA_TAXA_GUIA_PROC        VARCHAR2(1 BYTE),
  IE_DOSE_QUIMIO                 VARCHAR2(1 BYTE),
  IE_GUIA_REF_ANEXO              VARCHAR2(2 BYTE),
  IE_CONSISTE_JUST_ESPEC_OPM     VARCHAR2(1 BYTE),
  IE_INDICACAO_PROC_PRESCR       VARCHAR2(1 BYTE),
  IE_PREST_SOLIC_ODONTO          VARCHAR2(2 BYTE),
  IE_CONTRAT_EXEC_ODONTO         VARCHAR2(2 BYTE),
  IE_PROF_EXEC_ODONTO            VARCHAR2(2 BYTE),
  NM_USUARIO_ASSINATURA_CONTRAT  VARCHAR2(15 BYTE),
  QT_DIAS_INT_TITULAR_CONV       NUMBER(3),
  IE_QT_PROC_PRESCR_INTERV       VARCHAR2(1 BYTE),
  NR_GUIA_OPER_REC_GLOSA         VARCHAR2(1 BYTE),
  IE_ATUALIZA_VINCULO_TUSS       VARCHAR2(1 BYTE),
  IE_DESP_AGRUP_VALOR            VARCHAR2(1 BYTE),
  IE_IMPORT_ITENS_DEM_GRG        VARCHAR2(1 BYTE),
  IE_DATA_REC_GLOSA_TISS         VARCHAR2(1 BYTE),
  IE_IMP_VALIDADE_GUIA_SENHA     VARCHAR2(1 BYTE),
  IE_GERAR_LOTE_REC_TISS         VARCHAR2(1 BYTE),
  IE_XML_PEDIDO_EXAME_EXT        VARCHAR2(1 BYTE),
  IE_SETOR_EXEC_MAT              VARCHAR2(2 BYTE),
  IE_BUSCAR_PRIORIDADE_TAB_TISS  VARCHAR2(1 BYTE),
  IE_AGRUP_RESPOSTA_REC_GLOSA    VARCHAR2(1 BYTE),
  IE_HOSPITAL_DIA                VARCHAR2(1 BYTE),
  IM_LOGO_CONVENIO               LONG RAW,
  IE_GUIA_INTEGRAL_DEM           VARCHAR2(1 BYTE),
  IE_LOTE_PREST_SEQ_PROTOC       VARCHAR2(1 BYTE),
  IE_CONS_DIAS_SOLIC_INT         VARCHAR2(1 BYTE),
  IE_DIGITOS_PROC                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSPCO_CONVENI_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSPCO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSPCO_ESTABEL_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSPCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSPCO_PK ON TASY.TISS_PARAMETROS_CONVENIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSPCO_TISSMSI_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(NR_SEQ_SAIDA_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSPCO_TISSTSC_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(NR_SEQ_SAIDA_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSPCO_TISSTSS_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(NR_SEQ_SAIDA_SPSADT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSPCO_UK ON TASY.TISS_PARAMETROS_CONVENIO
(CD_CONVENIO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSPCO_USUARIO_FK_I ON TASY.TISS_PARAMETROS_CONVENIO
(NM_USUARIO_ASSINATURA_CONTRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_PARAMETROS_CONVENIO_tp  after update ON TASY.TISS_PARAMETROS_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_CID_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_CID_SPSADT,1,4000),substr(:new.IE_CID_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CID_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PARTIC_CONVENIADO,1,500);gravar_log_alteracao(substr(:old.IE_PARTIC_CONVENIADO,1,4000),substr(:new.IE_PARTIC_CONVENIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARTIC_CONVENIADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AGRUPAR_PROC,1,500);gravar_log_alteracao(substr(:old.IE_AGRUPAR_PROC,1,4000),substr(:new.IE_AGRUPAR_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUPAR_PROC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESP_TISS_INTERNADO,1,500);gravar_log_alteracao(substr(:old.IE_DESP_TISS_INTERNADO,1,4000),substr(:new.IE_DESP_TISS_INTERNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESP_TISS_INTERNADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_EXEC_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_EXEC_SPSADT,1,4000),substr(:new.IE_EXEC_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXEC_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CRM,1,500);gravar_log_alteracao(substr(:old.IE_CRM,1,4000),substr(:new.IE_CRM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_CODIGO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_CODIGO,1,4000),substr(:new.IE_CONSISTE_CODIGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_CODIGO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESPESA_ZERADA,1,500);gravar_log_alteracao(substr(:old.IE_DESPESA_ZERADA,1,4000),substr(:new.IE_DESPESA_ZERADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESPESA_ZERADA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FECHAR_CONTA_INCONS,1,500);gravar_log_alteracao(substr(:old.IE_FECHAR_CONTA_INCONS,1,4000),substr(:new.IE_FECHAR_CONTA_INCONS,1,4000),:new.nm_usuario,nr_seq_w,'IE_FECHAR_CONTA_INCONS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TOTALIZAR_OPM,1,500);gravar_log_alteracao(substr(:old.IE_TOTALIZAR_OPM,1,4000),substr(:new.IE_TOTALIZAR_OPM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOTALIZAR_OPM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_INDICACAO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_INDICACAO,1,4000),substr(:new.IE_CONSISTE_INDICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_INDICACAO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DIAS_VALIDADE_GUIA,1,500);gravar_log_alteracao(substr(:old.QT_DIAS_VALIDADE_GUIA,1,4000),substr(:new.QT_DIAS_VALIDADE_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_VALIDADE_GUIA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EMAIL_AUTOR,1,500);gravar_log_alteracao(substr(:old.DS_EMAIL_AUTOR,1,4000),substr(:new.DS_EMAIL_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL_AUTOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_OBS,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_OBS,1,4000),substr(:new.IE_CONSISTIR_OBS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_OBS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ABORTO_NASC,1,500);gravar_log_alteracao(substr(:old.IE_ABORTO_NASC,1,4000),substr(:new.IE_ABORTO_NASC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABORTO_NASC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_HONOR_PARTIC,1,500);gravar_log_alteracao(substr(:old.IE_HONOR_PARTIC,1,4000),substr(:new.IE_HONOR_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_HONOR_PARTIC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_EMITE_OPM_ZERADO,1,500);gravar_log_alteracao(substr(:old.IE_EMITE_OPM_ZERADO,1,4000),substr(:new.IE_EMITE_OPM_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMITE_OPM_ZERADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DIGITOS_DESP,1,500);gravar_log_alteracao(substr(:old.IE_DIGITOS_DESP,1,4000),substr(:new.IE_DIGITOS_DESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIGITOS_DESP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REDUCAO_ACRESC,1,500);gravar_log_alteracao(substr(:old.IE_REDUCAO_ACRESC,1,4000),substr(:new.IE_REDUCAO_ACRESC,1,4000),:new.nm_usuario,nr_seq_w,'IE_REDUCAO_ACRESC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_SAIDA_SPSADT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_SAIDA_SPSADT,1,4000),substr(:new.NR_SEQ_SAIDA_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SAIDA_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_USUARIO,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_USUARIO,1,4000),substr(:new.DS_MASCARA_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_USUARIO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_DESPESA,1,500);gravar_log_alteracao(substr(:old.IE_DATA_DESPESA,1,4000),substr(:new.IE_DATA_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_DESPESA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PARTIC_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_PARTIC_SPSADT,1,4000),substr(:new.IE_PARTIC_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARTIC_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROC_PARTIC,1,500);gravar_log_alteracao(substr(:old.IE_PROC_PARTIC,1,4000),substr(:new.IE_PROC_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_PARTIC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ASSINATURA_SOLIC,1,500);gravar_log_alteracao(substr(:old.IE_ASSINATURA_SOLIC,1,4000),substr(:new.IE_ASSINATURA_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ASSINATURA_SOLIC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SPSADT_ZERADO,1,500);gravar_log_alteracao(substr(:old.IE_SPSADT_ZERADO,1,4000),substr(:new.IE_SPSADT_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SPSADT_ZERADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RI_ZERADO,1,500);gravar_log_alteracao(substr(:old.IE_RI_ZERADO,1,4000),substr(:new.IE_RI_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RI_ZERADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SOLICITANTE,1,500);gravar_log_alteracao(substr(:old.IE_SOLICITANTE,1,4000),substr(:new.IE_SOLICITANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITANTE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TOTAL_GUIA_DESP,1,500);gravar_log_alteracao(substr(:old.IE_TOTAL_GUIA_DESP,1,4000),substr(:new.IE_TOTAL_GUIA_DESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOTAL_GUIA_DESP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESC_PROC_INTERNO,1,500);gravar_log_alteracao(substr(:old.IE_DESC_PROC_INTERNO,1,4000),substr(:new.IE_DESC_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_PROC_INTERNO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PROTOCOLO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PROTOCOLO,1,4000),substr(:new.IE_GUIA_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PROTOCOLO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_COD_TISS_BRASINDICE,1,500);gravar_log_alteracao(substr(:old.IE_COD_TISS_BRASINDICE,1,4000),substr(:new.IE_COD_TISS_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'IE_COD_TISS_BRASINDICE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_HONORARIO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_HONORARIO,1,4000),substr(:new.IE_GUIA_HONORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_HONORARIO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_FORMATO_CARTEIRA,1,500);gravar_log_alteracao(substr(:old.DS_FORMATO_CARTEIRA,1,4000),substr(:new.DS_FORMATO_CARTEIRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORMATO_CARTEIRA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ENVIO_CARTEIRA,1,500);gravar_log_alteracao(substr(:old.IE_ENVIO_CARTEIRA,1,4000),substr(:new.IE_ENVIO_CARTEIRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENVIO_CARTEIRA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_TISS,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_TISS,1,4000),substr(:new.IE_CONSISTE_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_TISS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ARREDONDAMENTO_DESP,1,500);gravar_log_alteracao(substr(:old.IE_ARREDONDAMENTO_DESP,1,4000),substr(:new.IE_ARREDONDAMENTO_DESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_ARREDONDAMENTO_DESP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEDICO_SOLIC_ATEND,1,500);gravar_log_alteracao(substr(:old.IE_MEDICO_SOLIC_ATEND,1,4000),substr(:new.IE_MEDICO_SOLIC_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_SOLIC_ATEND',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ATUALIZAR_ALTA,1,500);gravar_log_alteracao(substr(:old.IE_ATUALIZAR_ALTA,1,4000),substr(:new.IE_ATUALIZAR_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_ALTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_AUTOR_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_AUTOR_SPSADT,1,4000),substr(:new.IE_CONSISTE_AUTOR_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_AUTOR_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PRESTADOR,1,4000),substr(:new.IE_GUIA_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PRESTADOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_VIA_ACESSO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_VIA_ACESSO,1,4000),substr(:new.IE_CONSISTE_VIA_ACESSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_VIA_ACESSO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORDENACAO_OD,1,500);gravar_log_alteracao(substr(:old.IE_ORDENACAO_OD,1,4000),substr(:new.IE_ORDENACAO_OD,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_OD',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NUMERO_LOTE,1,500);gravar_log_alteracao(substr(:old.IE_NUMERO_LOTE,1,4000),substr(:new.IE_NUMERO_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_NUMERO_LOTE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_DESP_NEG,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_DESP_NEG,1,4000),substr(:new.IE_CONSISTE_DESP_NEG,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_DESP_NEG',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROC_SOLIC_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_PROC_SOLIC_SPSADT,1,4000),substr(:new.IE_PROC_SOLIC_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_SOLIC_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SAIDA_INTERNACAO,1,500);gravar_log_alteracao(substr(:old.IE_SAIDA_INTERNACAO,1,4000),substr(:new.IE_SAIDA_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SAIDA_INTERNACAO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FECHAR_PROT_INCONS,1,500);gravar_log_alteracao(substr(:old.IE_FECHAR_PROT_INCONS,1,4000),substr(:new.IE_FECHAR_PROT_INCONS,1,4000),:new.nm_usuario,nr_seq_w,'IE_FECHAR_PROT_INCONS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CAMPO_BOOLEAN,1,500);gravar_log_alteracao(substr(:old.IE_CAMPO_BOOLEAN,1,4000),substr(:new.IE_CAMPO_BOOLEAN,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAMPO_BOOLEAN',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_LOTE,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_LOTE,1,4000),substr(:new.DS_MASCARA_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_LOTE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_PROC,1,500);gravar_log_alteracao(substr(:old.IE_DATA_PROC,1,4000),substr(:new.IE_DATA_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_PROC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GERAR_CTA_DIF_TISS,1,500);gravar_log_alteracao(substr(:old.IE_GERAR_CTA_DIF_TISS,1,4000),substr(:new.IE_GERAR_CTA_DIF_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_CTA_DIF_TISS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENHA_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_SENHA_GUIA,1,4000),substr(:new.IE_SENHA_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_GUIA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MASCARA_CID,1,500);gravar_log_alteracao(substr(:old.IE_MASCARA_CID,1,4000),substr(:new.IE_MASCARA_CID,1,4000),:new.nm_usuario,nr_seq_w,'IE_MASCARA_CID',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PRINC_HONOR,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PRINC_HONOR,1,4000),substr(:new.IE_GUIA_PRINC_HONOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PRINC_HONOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_CONTRAT_RI,1,500);gravar_log_alteracao(substr(:old.IE_DT_CONTRAT_RI,1,4000),substr(:new.IE_DT_CONTRAT_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_CONTRAT_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_ENTRADA_RI,1,500);gravar_log_alteracao(substr(:old.IE_DATA_ENTRADA_RI,1,4000),substr(:new.IE_DATA_ENTRADA_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_ENTRADA_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_MAX_DESPESA,1,500);gravar_log_alteracao(substr(:old.QT_MAX_DESPESA,1,4000),substr(:new.QT_MAX_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_DESPESA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONTRATADO_CONSULTA,1,500);gravar_log_alteracao(substr(:old.IE_CONTRATADO_CONSULTA,1,4000),substr(:new.IE_CONTRATADO_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTRATADO_CONSULTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_GUIAS_PROTOCOLO,1,500);gravar_log_alteracao(substr(:old.QT_GUIAS_PROTOCOLO,1,4000),substr(:new.QT_GUIAS_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'QT_GUIAS_PROTOCOLO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TOTAL_OPM,1,500);gravar_log_alteracao(substr(:old.IE_TOTAL_OPM,1,4000),substr(:new.IE_TOTAL_OPM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOTAL_OPM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONTRAT_EXEC_HI,1,500);gravar_log_alteracao(substr(:old.IE_CONTRAT_EXEC_HI,1,4000),substr(:new.IE_CONTRAT_EXEC_HI,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTRAT_EXEC_HI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_PROC,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_PROC,1,4000),substr(:new.IE_CONSISTIR_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_PROC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PERIODO_DESP,1,500);gravar_log_alteracao(substr(:old.IE_PERIODO_DESP,1,4000),substr(:new.IE_PERIODO_DESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERIODO_DESP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SEPARAR_SPSADT_HONOR,1,500);gravar_log_alteracao(substr(:old.IE_SEPARAR_SPSADT_HONOR,1,4000),substr(:new.IE_SEPARAR_SPSADT_HONOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEPARAR_SPSADT_HONOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_GUIA_SENHA,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_GUIA_SENHA,1,4000),substr(:new.IE_CONSISTIR_GUIA_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_GUIA_SENHA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PARTICIPANTE_RI,1,500);gravar_log_alteracao(substr(:old.IE_PARTICIPANTE_RI,1,4000),substr(:new.IE_PARTICIPANTE_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARTICIPANTE_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_DECL_OBITO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_DECL_OBITO,1,4000),substr(:new.IE_CONSISTIR_DECL_OBITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_DECL_OBITO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_EMITE_HONOR_SADT_PEP,1,500);gravar_log_alteracao(substr(:old.IE_EMITE_HONOR_SADT_PEP,1,4000),substr(:new.IE_EMITE_HONOR_SADT_PEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMITE_HONOR_SADT_PEP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_EXEC_RI,1,500);gravar_log_alteracao(substr(:old.IE_EXEC_RI,1,4000),substr(:new.IE_EXEC_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXEC_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FORMATO_RED_ACRES,1,500);gravar_log_alteracao(substr(:old.IE_FORMATO_RED_ACRES,1,4000),substr(:new.IE_FORMATO_RED_ACRES,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMATO_RED_ACRES',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RELAT_AUTOR,1,500);gravar_log_alteracao(substr(:old.IE_RELAT_AUTOR,1,4000),substr(:new.IE_RELAT_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_RELAT_AUTOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_TEC_UTIL,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_TEC_UTIL,1,4000),substr(:new.IE_CONSISTE_TEC_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_TEC_UTIL',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_MASCARA,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_MASCARA,1,4000),substr(:new.IE_REGRA_MASCARA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_MASCARA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ACOMOD,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ACOMOD,1,4000),substr(:new.IE_TIPO_ACOMOD,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ACOMOD',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AGRUPAR_DESP_PAC,1,500);gravar_log_alteracao(substr(:old.IE_AGRUPAR_DESP_PAC,1,4000),substr(:new.IE_AGRUPAR_DESP_PAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUPAR_DESP_PAC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_HONOR_ZERADO,1,500);gravar_log_alteracao(substr(:old.IE_HONOR_ZERADO,1,4000),substr(:new.IE_HONOR_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_HONOR_ZERADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_VALOR_DESCONTO,1,500);gravar_log_alteracao(substr(:old.IE_VALOR_DESCONTO,1,4000),substr(:new.IE_VALOR_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_DESCONTO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORDENACAO_XML,1,500);gravar_log_alteracao(substr(:old.IE_ORDENACAO_XML,1,4000),substr(:new.IE_ORDENACAO_XML,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_XML',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_CID_AUTOR,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_CID_AUTOR,1,4000),substr(:new.IE_CONSISTE_CID_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_CID_AUTOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_TIPO_SAIDA,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_TIPO_SAIDA,1,4000),substr(:new.IE_CONSISTE_TIPO_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_TIPO_SAIDA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ESPECIALIDADE_EXEC,1,500);gravar_log_alteracao(substr(:old.IE_ESPECIALIDADE_EXEC,1,4000),substr(:new.IE_ESPECIALIDADE_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESPECIALIDADE_EXEC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEDICO_REP,1,500);gravar_log_alteracao(substr(:old.IE_MEDICO_REP,1,4000),substr(:new.IE_MEDICO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_REP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_TRANS_XML,1,500);gravar_log_alteracao(substr(:old.IE_DATA_TRANS_XML,1,4000),substr(:new.IE_DATA_TRANS_XML,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_TRANS_XML',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CID_SOLIC_EXTERNO,1,500);gravar_log_alteracao(substr(:old.IE_CID_SOLIC_EXTERNO,1,4000),substr(:new.IE_CID_SOLIC_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CID_SOLIC_EXTERNO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NOME_ARQUIVO,1,500);gravar_log_alteracao(substr(:old.IE_NOME_ARQUIVO,1,4000),substr(:new.IE_NOME_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOME_ARQUIVO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_VALOR_UNITARIO,1,500);gravar_log_alteracao(substr(:old.IE_VALOR_UNITARIO,1,4000),substr(:new.IE_VALOR_UNITARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_UNITARIO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.IE_OBSERVACAO,1,4000),substr(:new.IE_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBSERVACAO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_DESP_MED,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_DESP_MED,1,4000),substr(:new.IE_GUIA_DESP_MED,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_DESP_MED',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROTOCOLO_RET,1,500);gravar_log_alteracao(substr(:old.IE_PROTOCOLO_RET,1,4000),substr(:new.IE_PROTOCOLO_RET,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROTOCOLO_RET',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORDENACAO_SPSADT,1,500);gravar_log_alteracao(substr(:old.IE_ORDENACAO_SPSADT,1,4000),substr(:new.IE_ORDENACAO_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_EMISSAO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_DT_EMISSAO_GUIA,1,4000),substr(:new.IE_DT_EMISSAO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_EMISSAO_GUIA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CARTEIRA_MED,1,500);gravar_log_alteracao(substr(:old.IE_CARTEIRA_MED,1,4000),substr(:new.IE_CARTEIRA_MED,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARTEIRA_MED',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_QUEBRA_PROC_PEP,1,500);gravar_log_alteracao(substr(:old.IE_QUEBRA_PROC_PEP,1,4000),substr(:new.IE_QUEBRA_PROC_PEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUEBRA_PROC_PEP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_SENHA_AUTOR,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_SENHA_AUTOR,1,4000),substr(:new.IE_CONSISTE_SENHA_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_SENHA_AUTOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_VALOR_UNITARIO_OPM,1,500);gravar_log_alteracao(substr(:old.IE_VALOR_UNITARIO_OPM,1,4000),substr(:new.IE_VALOR_UNITARIO_OPM,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_UNITARIO_OPM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TOTAL_GERAL_OPM,1,500);gravar_log_alteracao(substr(:old.IE_TOTAL_GERAL_OPM,1,4000),substr(:new.IE_TOTAL_GERAL_OPM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOTAL_GERAL_OPM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LADO_REP,1,500);gravar_log_alteracao(substr(:old.IE_LADO_REP,1,4000),substr(:new.IE_LADO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_LADO_REP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_OBS_URGENTE,1,500);gravar_log_alteracao(substr(:old.IE_OBS_URGENTE,1,4000),substr(:new.IE_OBS_URGENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBS_URGENTE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AGRUP_RESP_CRED,1,500);gravar_log_alteracao(substr(:old.IE_AGRUP_RESP_CRED,1,4000),substr(:new.IE_AGRUP_RESP_CRED,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUP_RESP_CRED',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_SAIDA_INT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_SAIDA_INT,1,4000),substr(:new.NR_SEQ_SAIDA_INT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SAIDA_INT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_ENTRADA_ALTA,1,500);gravar_log_alteracao(substr(:old.IE_DATA_ENTRADA_ALTA,1,4000),substr(:new.IE_DATA_ENTRADA_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_ENTRADA_ALTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PACOTE,1,500);gravar_log_alteracao(substr(:old.IE_PACOTE,1,4000),substr(:new.IE_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PACOTE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONVERSAO_QTDE_MAT,1,500);gravar_log_alteracao(substr(:old.IE_CONVERSAO_QTDE_MAT,1,4000),substr(:new.IE_CONVERSAO_QTDE_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONVERSAO_QTDE_MAT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROC_TUSS,1,500);gravar_log_alteracao(substr(:old.IE_PROC_TUSS,1,4000),substr(:new.IE_PROC_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_TUSS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DIR_PADRAO,1,500);gravar_log_alteracao(substr(:old.DS_DIR_PADRAO,1,4000),substr(:new.DS_DIR_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_PADRAO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DIR_PADRAO_RET,1,500);gravar_log_alteracao(substr(:old.DS_DIR_PADRAO_RET,1,4000),substr(:new.DS_DIR_PADRAO_RET,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_PADRAO_RET',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESC_CONVENIO,1,500);gravar_log_alteracao(substr(:old.IE_DESC_CONVENIO,1,4000),substr(:new.IE_DESC_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_CONVENIO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NOME_ABREV,1,500);gravar_log_alteracao(substr(:old.IE_NOME_ABREV,1,4000),substr(:new.IE_NOME_ABREV,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOME_ABREV',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_ASS_PREST,1,500);gravar_log_alteracao(substr(:old.IE_DATA_ASS_PREST,1,4000),substr(:new.IE_DATA_ASS_PREST,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_ASS_PREST',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PRESCR_UNICA,1,500);gravar_log_alteracao(substr(:old.IE_PRESCR_UNICA,1,4000),substr(:new.IE_PRESCR_UNICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESCR_UNICA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESC_PLANO,1,500);gravar_log_alteracao(substr(:old.IE_DESC_PLANO,1,4000),substr(:new.IE_DESC_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_PLANO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_REF_TAB_MAT,1,500);gravar_log_alteracao(substr(:old.IE_DATA_REF_TAB_MAT,1,4000),substr(:new.IE_DATA_REF_TAB_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_REF_TAB_MAT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENHA_GUIA_PRINC,1,500);gravar_log_alteracao(substr(:old.IE_SENHA_GUIA_PRINC,1,4000),substr(:new.IE_SENHA_GUIA_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_GUIA_PRINC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROT_SOLIC_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_PROT_SOLIC_STATUS,1,4000),substr(:new.IE_PROT_SOLIC_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROT_SOLIC_STATUS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_PROC_ZERADO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_PROC_ZERADO,1,4000),substr(:new.IE_CONSISTIR_PROC_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_PROC_ZERADO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_CPF,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_CPF,1,4000),substr(:new.IE_CONSISTIR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_CPF',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_GUIA_DUP,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_GUIA_DUP,1,4000),substr(:new.IE_CONSISTIR_GUIA_DUP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_GUIA_DUP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ARQUIVO_LOGO_COMP,1,500);gravar_log_alteracao(substr(:old.DS_ARQUIVO_LOGO_COMP,1,4000),substr(:new.DS_ARQUIVO_LOGO_COMP,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO_LOGO_COMP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTIR_UF_CRM_SOLIC,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTIR_UF_CRM_SOLIC,1,4000),substr(:new.IE_CONSISTIR_UF_CRM_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTIR_UF_CRM_SOLIC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ATUAL_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_ATUAL_CONTA,1,4000),substr(:new.IE_ATUAL_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUAL_CONTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONS_TEMPO_DOENCA,1,500);gravar_log_alteracao(substr(:old.IE_CONS_TEMPO_DOENCA,1,4000),substr(:new.IE_CONS_TEMPO_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_TEMPO_DOENCA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_PROC_TASYMED,1,500);gravar_log_alteracao(substr(:old.IE_DT_PROC_TASYMED,1,4000),substr(:new.IE_DT_PROC_TASYMED,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_PROC_TASYMED',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FORCAR_AGRUP,1,500);gravar_log_alteracao(substr(:old.IE_FORCAR_AGRUP,1,4000),substr(:new.IE_FORCAR_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORCAR_AGRUP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AGRUP_EXAME,1,500);gravar_log_alteracao(substr(:old.IE_AGRUP_EXAME,1,4000),substr(:new.IE_AGRUP_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUP_EXAME',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_AUTOR_SADT,1,500);gravar_log_alteracao(substr(:old.IE_DT_AUTOR_SADT,1,4000),substr(:new.IE_DT_AUTOR_SADT,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_AUTOR_SADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_SAIDA_CONSULTA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_SAIDA_CONSULTA,1,4000),substr(:new.NR_SEQ_SAIDA_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SAIDA_CONSULTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_FATUR_TISS,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_FATUR_TISS,1,4000),substr(:new.IE_TIPO_FATUR_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FATUR_TISS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_CBO,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_CBO,1,4000),substr(:new.IE_CONSISTE_CBO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_CBO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_FUNCTION_OBS_SPSADT,1,500);gravar_log_alteracao(substr(:old.DS_FUNCTION_OBS_SPSADT,1,4000),substr(:new.DS_FUNCTION_OBS_SPSADT,1,4000),:new.nm_usuario,nr_seq_w,'DS_FUNCTION_OBS_SPSADT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GERAR_TISS,1,500);gravar_log_alteracao(substr(:old.IE_GERAR_TISS,1,4000),substr(:new.IE_GERAR_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_TISS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ADIC_CRM,1,500);gravar_log_alteracao(substr(:old.CD_ADIC_CRM,1,4000),substr(:new.CD_ADIC_CRM,1,4000),:new.nm_usuario,nr_seq_w,'CD_ADIC_CRM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ADICIONAL_GUIA_PREST,1,500);gravar_log_alteracao(substr(:old.DS_ADICIONAL_GUIA_PREST,1,4000),substr(:new.DS_ADICIONAL_GUIA_PREST,1,4000),:new.nm_usuario,nr_seq_w,'DS_ADICIONAL_GUIA_PREST',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENHA_AUTOR,1,500);gravar_log_alteracao(substr(:old.IE_SENHA_AUTOR,1,4000),substr(:new.IE_SENHA_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_AUTOR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEMBRO_EXEC,1,500);gravar_log_alteracao(substr(:old.IE_MEMBRO_EXEC,1,4000),substr(:new.IE_MEMBRO_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEMBRO_EXEC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_AUTOR_RI,1,500);gravar_log_alteracao(substr(:old.IE_DT_AUTOR_RI,1,4000),substr(:new.IE_DT_AUTOR_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_AUTOR_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_VALIDADE_SENHA_RI,1,500);gravar_log_alteracao(substr(:old.IE_DT_VALIDADE_SENHA_RI,1,4000),substr(:new.IE_DT_VALIDADE_SENHA_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_VALIDADE_SENHA_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ALT_TIPO_ATEND_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_ALT_TIPO_ATEND_CONTA,1,4000),substr(:new.IE_ALT_TIPO_ATEND_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALT_TIPO_ATEND_CONTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_FUNCTION_OBS_CONSULTA,1,500);gravar_log_alteracao(substr(:old.DS_FUNCTION_OBS_CONSULTA,1,4000),substr(:new.DS_FUNCTION_OBS_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'DS_FUNCTION_OBS_CONSULTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DT_ASSIN_SOLIC,1,500);gravar_log_alteracao(substr(:old.IE_DT_ASSIN_SOLIC,1,4000),substr(:new.IE_DT_ASSIN_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_ASSIN_SOLIC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GESTACAO_DIF_ABORTO,1,500);gravar_log_alteracao(substr(:old.IE_GESTACAO_DIF_ABORTO,1,4000),substr(:new.IE_GESTACAO_DIF_ABORTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GESTACAO_DIF_ABORTO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSISTE_QT_PROC,1,500);gravar_log_alteracao(substr(:old.IE_CONSISTE_QT_PROC,1,4000),substr(:new.IE_CONSISTE_QT_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_QT_PROC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ATUALIZA_SENHA_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_ATUALIZA_SENHA_CONTA,1,4000),substr(:new.IE_ATUALIZA_SENHA_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZA_SENHA_CONTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TRANSTORNO_ABORTO,1,500);gravar_log_alteracao(substr(:old.IE_TRANSTORNO_ABORTO,1,4000),substr(:new.IE_TRANSTORNO_ABORTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSTORNO_ABORTO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GESTACAO_NASC_VIVOS,1,500);gravar_log_alteracao(substr(:old.IE_GESTACAO_NASC_VIVOS,1,4000),substr(:new.IE_GESTACAO_NASC_VIVOS,1,4000),:new.nm_usuario,nr_seq_w,'IE_GESTACAO_NASC_VIVOS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GRAVAR_LOG_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_GRAVAR_LOG_CONTA,1,4000),substr(:new.IE_GRAVAR_LOG_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAVAR_LOG_CONTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CID_RI,1,500);gravar_log_alteracao(substr(:old.IE_CID_RI,1,4000),substr(:new.IE_CID_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_CID_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_CRM,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_CRM,1,4000),substr(:new.DS_MASCARA_CRM,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_CRM',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TRANS_TISS,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_TISS,1,4000),substr(:new.NR_SEQ_TRANS_TISS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_TISS',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PREST_SPSADT_TASYMED,1,500);gravar_log_alteracao(substr(:old.IE_PREST_SPSADT_TASYMED,1,4000),substr(:new.IE_PREST_SPSADT_TASYMED,1,4000),:new.nm_usuario,nr_seq_w,'IE_PREST_SPSADT_TASYMED',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DESC_EXAME_EXT,1,500);gravar_log_alteracao(substr(:old.IE_DESC_EXAME_EXT,1,4000),substr(:new.IE_DESC_EXAME_EXT,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_EXAME_EXT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CD_AUTORIZACAO_RELAT,1,500);gravar_log_alteracao(substr(:old.IE_CD_AUTORIZACAO_RELAT,1,4000),substr(:new.IE_CD_AUTORIZACAO_RELAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CD_AUTORIZACAO_RELAT',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_NAMESPACE,1,500);gravar_log_alteracao(substr(:old.DS_NAMESPACE,1,4000),substr(:new.DS_NAMESPACE,1,4000),:new.nm_usuario,nr_seq_w,'DS_NAMESPACE',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CODIGO_INTERNO_DESP,1,500);gravar_log_alteracao(substr(:old.IE_CODIGO_INTERNO_DESP,1,4000),substr(:new.IE_CODIGO_INTERNO_DESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CODIGO_INTERNO_DESP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TOPO_REP,1,500);gravar_log_alteracao(substr(:old.IE_TOPO_REP,1,4000),substr(:new.IE_TOPO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOPO_REP',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_ATECACO,1,500);gravar_log_alteracao(substr(:old.IE_DATA_ATECACO,1,4000),substr(:new.IE_DATA_ATECACO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_ATECACO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MASCARA_DNV,1,500);gravar_log_alteracao(substr(:old.IE_MASCARA_DNV,1,4000),substr(:new.IE_MASCARA_DNV,1,4000),:new.nm_usuario,nr_seq_w,'IE_MASCARA_DNV',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORDENACAO_RI,1,500);gravar_log_alteracao(substr(:old.IE_ORDENACAO_RI,1,4000),substr(:new.IE_ORDENACAO_RI,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_RI',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SPSADT_ATEND_RETORNO,1,500);gravar_log_alteracao(substr(:old.IE_SPSADT_ATEND_RETORNO,1,4000),substr(:new.IE_SPSADT_ATEND_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SPSADT_ATEND_RETORNO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONS_ATEND_RETORNO,1,500);gravar_log_alteracao(substr(:old.IE_CONS_ATEND_RETORNO,1,4000),substr(:new.IE_CONS_ATEND_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_ATEND_RETORNO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_COD_TISS_SIMPRO,1,500);gravar_log_alteracao(substr(:old.IE_COD_TISS_SIMPRO,1,4000),substr(:new.IE_COD_TISS_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COD_TISS_SIMPRO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ATUALIZA_NR_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_ATUALIZA_NR_GUIA,1,4000),substr(:new.IE_ATUALIZA_NR_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZA_NR_GUIA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SOLIC_DEM_PAGTO,1,500);gravar_log_alteracao(substr(:old.IE_SOLIC_DEM_PAGTO,1,4000),substr(:new.IE_SOLIC_DEM_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLIC_DEM_PAGTO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LADO_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_LADO_CONTA,1,4000),substr(:new.IE_LADO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_LADO_CONTA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ASSINATURA_EXEC,1,500);gravar_log_alteracao(substr(:old.IE_ASSINATURA_EXEC,1,4000),substr(:new.IE_ASSINATURA_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ASSINATURA_EXEC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_QTD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.IE_QTD_MATERIAL,1,4000),substr(:new.IE_QTD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_QTD_MATERIAL',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MATERIAL_EXEC,1,500);gravar_log_alteracao(substr(:old.IE_MATERIAL_EXEC,1,4000),substr(:new.IE_MATERIAL_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_EXEC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENHA_PROC_PRESCR,1,500);gravar_log_alteracao(substr(:old.IE_SENHA_PROC_PRESCR,1,4000),substr(:new.IE_SENHA_PROC_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_PROC_PRESCR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_SOLIC_INTE_PROC,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_SOLIC_INTE_PROC,1,4000),substr(:new.IE_GUIA_SOLIC_INTE_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_SOLIC_INTE_PROC',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_OBITO_PARCIAL,1,500);gravar_log_alteracao(substr(:old.IE_OBITO_PARCIAL,1,4000),substr(:new.IE_OBITO_PARCIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBITO_PARCIAL',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_SAIDA_PARCIAL,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_SAIDA_PARCIAL,1,4000),substr(:new.IE_TIPO_SAIDA_PARCIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SAIDA_PARCIAL',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_QUEBRA_HORARIOS_PRESCR,1,500);gravar_log_alteracao(substr(:old.IE_QUEBRA_HORARIOS_PRESCR,1,4000),substr(:new.IE_QUEBRA_HORARIOS_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUEBRA_HORARIOS_PRESCR',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_FATUR_PERIODO,1,4000),substr(:new.IE_TIPO_FATUR_PERIODO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FATUR_PERIODO',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GUIA_NUMERICA,1,4000),substr(:new.IE_GUIA_NUMERICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_NUMERICA',ie_log_w,ds_w,'TISS_PARAMETROS_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_PARAMETROS_CONVENIO ADD (
  CONSTRAINT TISSPCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TISSPCO_UK
 UNIQUE (CD_CONVENIO, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_PARAMETROS_CONVENIO ADD (
  CONSTRAINT TISSPCO_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_ASSINATURA_CONTRAT) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT TISSPCO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT TISSPCO_TISSTSS_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_SPSADT) 
 REFERENCES TASY.TISS_TIPO_SAIDA_SPSADT (NR_SEQUENCIA),
  CONSTRAINT TISSPCO_TISSMSI_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_INT) 
 REFERENCES TASY.TISS_MOTIVO_SAIDA_INT (NR_SEQUENCIA),
  CONSTRAINT TISSPCO_TISSTSC_FK 
 FOREIGN KEY (NR_SEQ_SAIDA_CONSULTA) 
 REFERENCES TASY.TISS_TIPO_SAIDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT TISSPCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TISS_PARAMETROS_CONVENIO TO NIVEL_1;

