ALTER TABLE TASY.W_PESSOA_FISICA_LOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PESSOA_FISICA_LOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PESSOA_FISICA_LOC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_PESSOA_FISICA     VARCHAR2(255 BYTE)       NOT NULL,
  DT_NASCIMENTO        DATE,
  NR_CPF               VARCHAR2(11 BYTE),
  NR_IDENTIDADE        VARCHAR2(15 BYTE),
  DS_BAIRRO            VARCHAR2(40 BYTE),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  SG_ESTADO            VARCHAR2(15 BYTE),
  NR_SEQ_COR_PELE      NUMBER(10),
  IE_SEXO              VARCHAR2(1 BYTE),
  DS_ORGAO_EMISSOR_CI  VARCHAR2(40 BYTE),
  IE_ESTADO_CIVIL      VARCHAR2(2 BYTE),
  CD_NACIONALIDADE     VARCHAR2(8 BYTE),
  NR_CEP_CIDADE_NASC   VARCHAR2(15 BYTE),
  DS_ENDERECO          VARCHAR2(100 BYTE),
  IE_TIPO_COMPLEMENTO  NUMBER(2),
  DS_MUNICIPIO         VARCHAR2(40 BYTE),
  CD_CEP               VARCHAR2(15 BYTE),
  NM_PAI               VARCHAR2(60 BYTE),
  NM_MAE               VARCHAR2(60 BYTE),
  NR_TELEFONE_RES      VARCHAR2(40 BYTE),
  NR_TELEFONE_COM      VARCHAR2(40 BYTE),
  NR_TELEFONE_CELULAR  VARCHAR2(40 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  NR_CARTAO_NAC_SUS    NUMBER(20),
  IE_GRAU_INSTRUCAO    NUMBER(2),
  NR_SEQ_RELIGIAO      NUMBER(5),
  NR_ENDERECO          NUMBER(5),
  NR_SEQ_PAIS          NUMBER(10),
  CD_SISTEMA_ANT       VARCHAR2(20 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  CD_MUNICIPIO_END     VARCHAR2(6 BYTE),
  DS_GIVEN_NAME        VARCHAR2(128 BYTE),
  DS_FAMILY_NAME       VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_1  VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_2  VARCHAR2(128 BYTE),
  DS_COMPONENT_NAME_3  VARCHAR2(128 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPFLOC_PK ON TASY.W_PESSOA_FISICA_LOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPFLOC_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PESSOA_FISICA_LOC ADD (
  CONSTRAINT WPFLOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PESSOA_FISICA_LOC TO NIVEL_1;

