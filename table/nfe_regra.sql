ALTER TABLE TASY.NFE_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NFE_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.NFE_REGRA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  SG_ESTADO                  VARCHAR2(15 BYTE),
  DS_ENVIO_HOMOL             VARCHAR2(255 BYTE),
  DS_RETENVIO_HOMOL          VARCHAR2(255 BYTE),
  DS_CANCELAMENTO_HOMOL      VARCHAR2(255 BYTE),
  DS_INUTILIZACAO_HOMOL      VARCHAR2(255 BYTE),
  DS_CONSULTA_HOMOL          VARCHAR2(255 BYTE),
  DS_STATUSSERV_HOMOL        VARCHAR2(255 BYTE),
  DS_RECEPCAO_HOMOL          VARCHAR2(255 BYTE),
  DS_ENVIO_PROD              VARCHAR2(255 BYTE),
  DS_RETENVIO_PROD           VARCHAR2(255 BYTE),
  DS_CANCELAMENTO_PROD       VARCHAR2(255 BYTE),
  DS_INUTILIZACAO_PROD       VARCHAR2(255 BYTE),
  DS_CONSULTA_PROD           VARCHAR2(255 BYTE),
  DS_STATUSSERV_PROD         VARCHAR2(255 BYTE),
  DS_RECEPCAO_PROD           VARCHAR2(255 BYTE),
  IE_OPERACAO_ENVIO          VARCHAR2(255 BYTE),
  IE_OPERACAO_RETENVIO       VARCHAR2(255 BYTE),
  IE_OPERACAO_CANCEL         VARCHAR2(255 BYTE),
  IE_OPERACAO_INUTILIZA      VARCHAR2(255 BYTE),
  IE_OPERACAO_CONSULTA       VARCHAR2(255 BYTE),
  IE_OPERACAO_STATUS         VARCHAR2(255 BYTE),
  IE_OPERACAO_RECEPCAO       VARCHAR2(255 BYTE),
  IE_VERSAO_ENVIO_HOMOL      NUMBER(10),
  IE_VERSAO_RETENVIO_HOMOL   NUMBER(10),
  IE_VERSAO_CANCEL_HOMOL     NUMBER(10),
  IE_VERSAO_INUTILIZA_HOMOL  NUMBER(10),
  IE_VERSAO_CONSULTA_HOMOL   NUMBER(10),
  IE_VERSAO_STATUS_HOMOL     NUMBER(10),
  IE_VERSAO_RECEPCAO_HOMOL   NUMBER(10),
  IE_VERSAO_ENVIO_PROD       NUMBER(10),
  IE_VERSAO_RETENVIO_PROD    NUMBER(10),
  IE_VERSAO_CANCEL_PROD      NUMBER(10),
  IE_VERSAO_INUTILIZA_PROD   NUMBER(10),
  IE_VERSAO_CONSULTA_PROD    NUMBER(10),
  IE_VERSAO_STATUS_PROD      NUMBER(10),
  IE_VERSAO_RECEPCAO_PROD    NUMBER(10),
  IE_TIPO_URL                NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFEREGR_ESTABEL_FK_I ON TASY.NFE_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFEREGR_PK ON TASY.NFE_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.NFE_REGRA_tp  after update ON TASY.NFE_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_VERSAO_CONSULTA_HOMOL,1,4000),substr(:new.IE_VERSAO_CONSULTA_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_CONSULTA_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_INUTILIZA_HOMOL,1,4000),substr(:new.IE_VERSAO_INUTILIZA_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_INUTILIZA_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_RECEPCAO_HOMOL,1,4000),substr(:new.IE_VERSAO_RECEPCAO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_RECEPCAO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_ENVIO_PROD,1,4000),substr(:new.IE_VERSAO_ENVIO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_ENVIO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_RETENVIO_PROD,1,4000),substr(:new.IE_VERSAO_RETENVIO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_RETENVIO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_CANCEL_PROD,1,4000),substr(:new.IE_VERSAO_CANCEL_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_CANCEL_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_INUTILIZA_PROD,1,4000),substr(:new.IE_VERSAO_INUTILIZA_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_INUTILIZA_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_CONSULTA_PROD,1,4000),substr(:new.IE_VERSAO_CONSULTA_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_CONSULTA_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_STATUS_PROD,1,4000),substr(:new.IE_VERSAO_STATUS_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_STATUS_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_RECEPCAO_PROD,1,4000),substr(:new.IE_VERSAO_RECEPCAO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_RECEPCAO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENVIO_HOMOL,1,4000),substr(:new.DS_ENVIO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENVIO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RETENVIO_HOMOL,1,4000),substr(:new.DS_RETENVIO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RETENVIO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CANCELAMENTO_HOMOL,1,4000),substr(:new.DS_CANCELAMENTO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_CANCELAMENTO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INUTILIZACAO_HOMOL,1,4000),substr(:new.DS_INUTILIZACAO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_INUTILIZACAO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONSULTA_HOMOL,1,4000),substr(:new.DS_CONSULTA_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONSULTA_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_STATUSSERV_HOMOL,1,4000),substr(:new.DS_STATUSSERV_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_STATUSSERV_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RECEPCAO_HOMOL,1,4000),substr(:new.DS_RECEPCAO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RECEPCAO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENVIO_PROD,1,4000),substr(:new.DS_ENVIO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENVIO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RETENVIO_PROD,1,4000),substr(:new.DS_RETENVIO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_RETENVIO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CANCELAMENTO_PROD,1,4000),substr(:new.DS_CANCELAMENTO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_CANCELAMENTO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INUTILIZACAO_PROD,1,4000),substr(:new.DS_INUTILIZACAO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_INUTILIZACAO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONSULTA_PROD,1,4000),substr(:new.DS_CONSULTA_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONSULTA_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_STATUSSERV_PROD,1,4000),substr(:new.DS_STATUSSERV_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_STATUSSERV_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RECEPCAO_PROD,1,4000),substr(:new.DS_RECEPCAO_PROD,1,4000),:new.nm_usuario,nr_seq_w,'DS_RECEPCAO_PROD',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_ENVIO,1,4000),substr(:new.IE_OPERACAO_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_ENVIO',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_RETENVIO,1,4000),substr(:new.IE_OPERACAO_RETENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_RETENVIO',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_CANCEL,1,4000),substr(:new.IE_OPERACAO_CANCEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_CANCEL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_INUTILIZA,1,4000),substr(:new.IE_OPERACAO_INUTILIZA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_INUTILIZA',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_CONSULTA,1,4000),substr(:new.IE_OPERACAO_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_CONSULTA',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_STATUS,1,4000),substr(:new.IE_OPERACAO_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_STATUS',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO_RECEPCAO,1,4000),substr(:new.IE_OPERACAO_RECEPCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO_RECEPCAO',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_ENVIO_HOMOL,1,4000),substr(:new.IE_VERSAO_ENVIO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_ENVIO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_RETENVIO_HOMOL,1,4000),substr(:new.IE_VERSAO_RETENVIO_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_RETENVIO_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_CANCEL_HOMOL,1,4000),substr(:new.IE_VERSAO_CANCEL_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_CANCEL_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_STATUS_HOMOL,1,4000),substr(:new.IE_VERSAO_STATUS_HOMOL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_STATUS_HOMOL',ie_log_w,ds_w,'NFE_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.NFE_REGRA ADD (
  CONSTRAINT NFEREGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NFE_REGRA ADD (
  CONSTRAINT NFEREGR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.NFE_REGRA TO NIVEL_1;

