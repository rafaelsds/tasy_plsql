ALTER TABLE TASY.PLS_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EVENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DS_EVENTO                  VARCHAR2(100 BYTE) NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_NATUREZA                VARCHAR2(1 BYTE)   NOT NULL,
  IE_TIPO_EVENTO             VARCHAR2(3 BYTE)   NOT NULL,
  IE_TAXA_DIFELIDADE         VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO           VARCHAR2(3 BYTE),
  IE_COOPERADO               VARCHAR2(1 BYTE),
  IE_INCIDENCIA_LANC_PROG    VARCHAR2(1 BYTE),
  IE_SALDO_NEGATIVO          VARCHAR2(3 BYTE)   NOT NULL,
  IE_DESC_TRIB_TIPO_CONTRAT  VARCHAR2(1 BYTE),
  NR_PRIOR_DESC              NUMBER(10),
  IE_CONSISTE_CTB            VARCHAR2(1 BYTE),
  IE_CONSISTE_PERIODO_PAG    VARCHAR2(1 BYTE),
  NR_PRIOR_DESC_TRIB         NUMBER(10),
  IE_EVENTO_ZERADO           VARCHAR2(1 BYTE),
  NR_SEQ_TERMO_CRED_DEB      NUMBER(10),
  IE_FORMA_APLICACAO_REGRA   VARCHAR2(2 BYTE),
  IE_APLICAR_REGRA_PREST     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEVEN_ESTABEL_FK_I ON TASY.PLS_EVENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVEN_I1 ON TASY.PLS_EVENTO
(IE_NATUREZA, IE_TIPO_EVENTO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSEVEN_PK ON TASY.PLS_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVEN_PTTCDEB_FK_I ON TASY.PLS_EVENTO
(NR_SEQ_TERMO_CRED_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_EVENTO_tp  after update ON TASY.PLS_EVENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONSISTE_CTB,1,4000),substr(:new.IE_CONSISTE_CTB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_CTB',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRIOR_DESC,1,4000),substr(:new.NR_PRIOR_DESC,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRIOR_DESC',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TERMO_CRED_DEB,1,4000),substr(:new.NR_SEQ_TERMO_CRED_DEB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TERMO_CRED_DEB',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_APLICACAO_REGRA,1,4000),substr(:new.IE_FORMA_APLICACAO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_APLICACAO_REGRA',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ZERADO,1,4000),substr(:new.IE_EVENTO_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EVENTO_ZERADO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRIOR_DESC_TRIB,1,4000),substr(:new.NR_PRIOR_DESC_TRIB,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRIOR_DESC_TRIB',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NATUREZA,1,4000),substr(:new.IE_NATUREZA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NATUREZA',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EVENTO,1,4000),substr(:new.DS_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_EVENTO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_EVENTO,1,4000),substr(:new.IE_TIPO_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_EVENTO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TAXA_DIFELIDADE,1,4000),substr(:new.IE_TAXA_DIFELIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAXA_DIFELIDADE',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COOPERADO,1,4000),substr(:new.IE_COOPERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COOPERADO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO,1,4000),substr(:new.IE_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INCIDENCIA_LANC_PROG,1,4000),substr(:new.IE_INCIDENCIA_LANC_PROG,1,4000),:new.nm_usuario,nr_seq_w,'IE_INCIDENCIA_LANC_PROG',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALDO_NEGATIVO,1,4000),substr(:new.IE_SALDO_NEGATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALDO_NEGATIVO',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESC_TRIB_TIPO_CONTRAT,1,4000),substr(:new.IE_DESC_TRIB_TIPO_CONTRAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_TRIB_TIPO_CONTRAT',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_PERIODO_PAG,1,4000),substr(:new.IE_CONSISTE_PERIODO_PAG,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_PERIODO_PAG',ie_log_w,ds_w,'PLS_EVENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_EVENTO ADD (
  CONSTRAINT PLSEVEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EVENTO ADD (
  CONSTRAINT PLSEVEN_PTTCDEB_FK 
 FOREIGN KEY (NR_SEQ_TERMO_CRED_DEB) 
 REFERENCES TASY.PLS_TISS_TERMO_CRED_DEB (NR_SEQUENCIA),
  CONSTRAINT PLSEVEN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_EVENTO TO NIVEL_1;

