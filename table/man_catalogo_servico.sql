ALTER TABLE TASY.MAN_CATALOGO_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_CATALOGO_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_CATALOGO_SERVICO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_SERVICO             VARCHAR2(80 BYTE)      NOT NULL,
  NR_SEQ_TIPO            NUMBER(10)             NOT NULL,
  DS_SERVICO             VARCHAR2(255 BYTE)     NOT NULL,
  QT_HORA_UTIL           NUMBER(7,2),
  NR_SEQ_GRUPO_PLANEJ    NUMBER(10),
  NR_SEQ_GRUPO_TRABALHO  NUMBER(10),
  IE_OS_DEPARTAMENTO     VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MACATSE_ESTABEL_FK_I ON TASY.MAN_CATALOGO_SERVICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACATSE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MACATSE_MANGRPL_FK_I ON TASY.MAN_CATALOGO_SERVICO
(NR_SEQ_GRUPO_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACATSE_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MACATSE_MANGRTR_FK_I ON TASY.MAN_CATALOGO_SERVICO
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACATSE_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MACATSE_MATICAT_FK_I ON TASY.MAN_CATALOGO_SERVICO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACATSE_MATICAT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MACATSE_PK ON TASY.MAN_CATALOGO_SERVICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_CATALOGO_SERVICO ADD (
  CONSTRAINT MACATSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_CATALOGO_SERVICO ADD (
  CONSTRAINT MACATSE_MATICAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MAN_TIPO_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT MACATSE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MACATSE_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT MACATSE_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_CATALOGO_SERVICO TO NIVEL_1;

