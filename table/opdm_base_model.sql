ALTER TABLE TASY.OPDM_BASE_MODEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_BASE_MODEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_BASE_MODEL
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_OPDM_TASY              NUMBER(10),
  DS_MODEL_DESCRIPTION          VARCHAR2(255 BYTE),
  DS_COMPANY_PREFIX             VARCHAR2(255 BYTE),
  CD_GS1_GLOBAL_MODEL           VARCHAR2(50 BYTE),
  DS_TYPE_DEVICE                VARCHAR2(50 BYTE),
  NM_ENGINEERING_RESP_ORG       VARCHAR2(255 BYTE),
  DS_LEGAL_MANUFACTURER         VARCHAR2(255 BYTE),
  DS_AUTHORIZED_REPRESENTATIVE  VARCHAR2(255 BYTE),
  IE_SOFTWARE_PRODUCT           VARCHAR2(1 BYTE),
  IE_ACTIVE_DEVICE              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPDMBAMO_OPDMTASY_FK_I ON TASY.OPDM_BASE_MODEL
(NR_SEQ_OPDM_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPDMBAMO_PK ON TASY.OPDM_BASE_MODEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_BASE_MODEL_tp  after update ON TASY.OPDM_BASE_MODEL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPDM_TASY,1,4000),substr(:new.NR_SEQ_OPDM_TASY,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPDM_TASY',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MODEL_DESCRIPTION,1,4000),substr(:new.DS_MODEL_DESCRIPTION,1,4000),:new.nm_usuario,nr_seq_w,'DS_MODEL_DESCRIPTION',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPANY_PREFIX,1,4000),substr(:new.DS_COMPANY_PREFIX,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPANY_PREFIX',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GS1_GLOBAL_MODEL,1,4000),substr(:new.CD_GS1_GLOBAL_MODEL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GS1_GLOBAL_MODEL',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACTIVE_DEVICE,1,4000),substr(:new.IE_ACTIVE_DEVICE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACTIVE_DEVICE',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ENGINEERING_RESP_ORG,1,4000),substr(:new.NM_ENGINEERING_RESP_ORG,1,4000),:new.nm_usuario,nr_seq_w,'NM_ENGINEERING_RESP_ORG',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LEGAL_MANUFACTURER,1,4000),substr(:new.DS_LEGAL_MANUFACTURER,1,4000),:new.nm_usuario,nr_seq_w,'DS_LEGAL_MANUFACTURER',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_AUTHORIZED_REPRESENTATIVE,1,4000),substr(:new.DS_AUTHORIZED_REPRESENTATIVE,1,4000),:new.nm_usuario,nr_seq_w,'DS_AUTHORIZED_REPRESENTATIVE',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOFTWARE_PRODUCT,1,4000),substr(:new.IE_SOFTWARE_PRODUCT,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOFTWARE_PRODUCT',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TYPE_DEVICE,1,4000),substr(:new.DS_TYPE_DEVICE,1,4000),:new.nm_usuario,nr_seq_w,'DS_TYPE_DEVICE',ie_log_w,ds_w,'OPDM_BASE_MODEL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_BASE_MODEL ADD (
  CONSTRAINT OPDMBAMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OPDM_BASE_MODEL ADD (
  CONSTRAINT OPDMBAMO_OPDMTASY_FK 
 FOREIGN KEY (NR_SEQ_OPDM_TASY) 
 REFERENCES TASY.OPDM_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_BASE_MODEL TO NIVEL_1;

