ALTER TABLE TASY.PLS_ATU_PLANO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATU_PLANO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATU_PLANO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  NR_SEQ_PLANO         NUMBER(10),
  IE_TIPO_CONTRATACAO  VARCHAR2(2 BYTE),
  IE_COPARTICIPACAO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_ABRANGENCIA       VARCHAR2(3 BYTE),
  IE_ACOMODACAO        VARCHAR2(1 BYTE),
  IE_SEGMENTACAO       VARCHAR2(3 BYTE),
  NR_PROTOCOLO_ANS     VARCHAR2(20 BYTE),
  IE_FRANQUIA          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAALI_PK ON TASY.PLS_ATU_PLANO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAALI_PLSAALL_FK_I ON TASY.PLS_ATU_PLANO_ITEM
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAALI_PLSPLAN_FK_I ON TASY.PLS_ATU_PLANO_ITEM
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ATU_PLANO_ITEM ADD (
  CONSTRAINT PLSAALI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATU_PLANO_ITEM ADD (
  CONSTRAINT PLSAALI_PLSAALL_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_ATUARIAL_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSAALI_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATU_PLANO_ITEM TO NIVEL_1;

