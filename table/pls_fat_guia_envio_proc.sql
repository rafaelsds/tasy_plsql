ALTER TABLE TASY.PLS_FAT_GUIA_ENVIO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FAT_GUIA_ENVIO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FAT_GUIA_ENVIO_PROC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CONTA_PROC           NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_GUIA_ENVIO           NUMBER(10)        NOT NULL,
  CD_PROCEDIMENTO             NUMBER(15),
  IE_ORIGEM_PROCED            NUMBER(10),
  CD_PROCED_CONVERSAO         NUMBER(15),
  IE_ORIGEM_PROCED_CONVERSAO  NUMBER(10),
  DT_ATENDIMENTO              DATE,
  DT_PROCEDIMENTO             DATE,
  IE_VIA_ACESSO               VARCHAR2(1 BYTE),
  VL_UNITARIO                 NUMBER(15,2),
  VL_PROCEDIMENTO             NUMBER(15,2),
  DT_INICIO_PROC              DATE,
  DT_FIM_PROC                 DATE,
  IE_TECNICA_UTILIZADA        VARCHAR2(1 BYTE),
  CD_TIPO_TABELA_IMP          VARCHAR2(10 BYTE),
  CD_TIPO_TABELA              VARCHAR2(10 BYTE),
  NR_SEQ_TISS_TABELA          NUMBER(10),
  VL_BENEFICIARIO             NUMBER(15,2),
  VL_BENEFICIARIO_UNIT        NUMBER(15,2),
  NR_SEQ_TISS_TAB_CONVERSAO   NUMBER(10),
  DS_PROC_CONVERSAO_REGRA     VARCHAR2(255 BYTE),
  TX_REDUCAO_ACRESCIMO        NUMBER(7,4),
  NR_SEQ_CONTA_POS_ESTAB      NUMBER(10)        NOT NULL,
  QT_PROCEDIMENTO             NUMBER(15,4),
  IE_TIPO_DESPESA             VARCHAR2(1 BYTE),
  NR_SEQ_POS_PROC             NUMBER(10),
  CD_DENTE                    VARCHAR2(20 BYTE),
  CD_REGIAO_BOCA              VARCHAR2(20 BYTE),
  CD_FACE_DENTE               VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSFGEP_PK ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_PLSCOPRO_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_PLSCOVB_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_CONTA_POS_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_PLSFAGE_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_GUIA_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_PLSPPROC_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_POS_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_TISSDEN_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(CD_DENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_TISSRGBO_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(CD_REGIAO_BOCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_TISSTTA_FK_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_TISS_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFGEP_TISSTTA_FK2_I ON TASY.PLS_FAT_GUIA_ENVIO_PROC
(NR_SEQ_TISS_TAB_CONVERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_FAT_GUIA_ENVIO_PROC_tp  after update ON TASY.PLS_FAT_GUIA_ENVIO_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_TISS_TABELA,1,4000),substr(:new.NR_SEQ_TISS_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TISS_TABELA',ie_log_w,ds_w,'PLS_FAT_GUIA_ENVIO_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_FAT_GUIA_ENVIO_PROC ADD (
  CONSTRAINT PLSFGEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FAT_GUIA_ENVIO_PROC ADD (
  CONSTRAINT PLSFGEP_PLSPPROC_FK 
 FOREIGN KEY (NR_SEQ_POS_PROC) 
 REFERENCES TASY.PLS_CONTA_POS_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSFGEP_TISSDEN_FK 
 FOREIGN KEY (CD_DENTE) 
 REFERENCES TASY.TISS_DENTE (CD_DENTE),
  CONSTRAINT PLSFGEP_TISSRGBO_FK 
 FOREIGN KEY (CD_REGIAO_BOCA) 
 REFERENCES TASY.TISS_REGIAO_BOCA (CD_REGIAO),
  CONSTRAINT PLSFGEP_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSFGEP_PLSFAGE_FK 
 FOREIGN KEY (NR_SEQ_GUIA_ENVIO) 
 REFERENCES TASY.PLS_FATURA_GUIA_ENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSFGEP_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TABELA) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA),
  CONSTRAINT PLSFGEP_TISSTTA_FK2 
 FOREIGN KEY (NR_SEQ_TISS_TAB_CONVERSAO) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA),
  CONSTRAINT PLSFGEP_PLSCOVB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS_ESTAB) 
 REFERENCES TASY.PLS_CONTA_POS_ESTABELECIDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FAT_GUIA_ENVIO_PROC TO NIVEL_1;

