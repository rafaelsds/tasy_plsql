ALTER TABLE TASY.CARTA_MEDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARTA_MEDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARTA_MEDICA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE                NOT NULL,
  NM_USUARIO_ORIGINAL       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  CD_MEDICO                 VARCHAR2(10 BYTE),
  DS_CARTA                  CLOB,
  DT_LIBERACAO_PRELIMINAR   DATE,
  DT_LIBERACAO              DATE,
  NR_ATENDIMENTO            NUMBER(10),
  IE_TRANSCRICAO            VARCHAR2(1 BYTE),
  IE_LOG                    VARCHAR2(1 BYTE),
  NR_VERSAO                 NUMBER(5),
  NR_SEQ_ASSINATURA         NUMBER(10),
  IE_REVISAO                VARCHAR2(1 BYTE),
  NR_SEQ_CARTA_MAE          NUMBER(10),
  NM_USUARIO_PRE_LIB        VARCHAR2(15 BYTE),
  IE_VISUALIZACAO           VARCHAR2(1 BYTE),
  DT_LIBERACAO_RESPONSAVEL  DATE,
  CD_MEDICO_RESPONSAVEL     VARCHAR2(10 BYTE),
  IE_LIBERACAO_PRELIMINAR   VARCHAR2(1 BYTE),
  IE_LIBERACAO_RESPONSAVEL  VARCHAR2(1 BYTE),
  DT_INTEGRACAO             DATE,
  NR_SEQ_MODELO             NUMBER(10),
  IE_PRELIMINAR             VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  NR_EPISODIO               NUMBER(10),
  NR_SEQ_EPISODIO           NUMBER(10),
  IE_CARTA_EM_EDICAO        VARCHAR2(1 BYTE),
  NR_CIRURGIA               NUMBER(10),
  IE_STATUS_APROVACAO       VARCHAR2(2 BYTE),
  DS_PDF_SERIAL             LONG,
  NR_SEQ_MOVIMENTACAO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_CARTA) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CARMED_ATEPACI_FK_I ON TASY.CARTA_MEDICA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_ATEPACU_FK_I ON TASY.CARTA_MEDICA
(NR_SEQ_MOVIMENTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_CAEPPA_FK_I ON TASY.CARTA_MEDICA
(NR_SEQ_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_CAMEMOD_FK_I ON TASY.CARTA_MEDICA
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_CARMED_FK_I ON TASY.CARTA_MEDICA
(NR_SEQ_CARTA_MAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_CIRURGI_FK_I ON TASY.CARTA_MEDICA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_ESTABEL_FK_I ON TASY.CARTA_MEDICA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_PESFISI_FK_I ON TASY.CARTA_MEDICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_PESFISI_FK2_I ON TASY.CARTA_MEDICA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_PESFISI_FK3_I ON TASY.CARTA_MEDICA
(CD_MEDICO_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CARMED_PK ON TASY.CARTA_MEDICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARMED_TASASDI_FK_I ON TASY.CARTA_MEDICA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CARTA_MEDICA_BEFORE_INSERT
BEFORE INSERT ON TASY.CARTA_MEDICA FOR EACH ROW
DECLARE

BEGIN
	if (:new.nr_seq_carta_mae is null) then
		:new.nr_seq_carta_mae := :new.nr_sequencia;
	end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.CARTA_MEDICA_AFTER_INSERT
AFTER insert ON TASY.CARTA_MEDICA FOR EACH ROW
DECLARE
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
ie_modelo_carta_w   carta_medica_modelo.ie_modelo_cirurgia%type;

BEGIN
        if    (wheb_usuario_pck.get_ie_executar_trigger    = 'S') then
                select	max(a.nr_seq_regra)
                into	nr_seq_regra_w
                from	wl_worklist a,
                        wl_item b,
                        wl_regra_item c
                where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
                and	a.nr_atendimento = :new.nr_atendimento
                and	b.nr_sequencia = a.nr_seq_item
                and	c.nr_sequencia = a.nr_seq_regra
                and	a.dt_final_real is null
                and	b.cd_categoria = 'ML'
                and	c.ie_tipo_pend_carta = 'N'
                and	b.ie_situacao = 'A'
                and	c.ie_situacao = 'A';

                select  max(cmm.ie_modelo_cirurgia)
                into    ie_modelo_carta_w
                from    carta_medica_modelo cmm,
                        wl_regra_item wri
                where   cmm.nr_sequencia = :new.nr_seq_modelo
                and     wri.nr_sequencia = to_number(to_char(nr_seq_regra_w))
                and     wri.ie_modelo_carta = cmm.ie_modelo_cirurgia;

                wl_gerar_finalizar_tarefa('ML','F',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,null,'N',null,null,null,null,null,nvl(:new.nr_seq_carta_mae,:new.nr_sequencia),null,null,null,nr_seq_regra_w,null,null,null,
                null,null,null,null,null,null,null,null,null,null,null,null,null,ie_modelo_carta_w);
        end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.CARTA_MEDICA_AFTER_INS_UPDATE
AFTER INSERT or UPDATE ON TASY.CARTA_MEDICA FOR EACH ROW
DECLARE
pragma autonomous_transaction;
CONST_ML_TASK_LIST_W			constant varchar2(2) := 'ML';

ie_participante_w				number(10)	:= 0;
ie_aprovado_w					number(10)	:= 0;
qt_pendencias_assinatura_w		number(10)	:= 0;
cd_profissional_w				number(10)	:= 0;
CD_PESSOA_USUARIO_W				number(10)	:= 0;
nr_seq_tipo_adm_fat_atd_w		atendimento_paciente.nr_seq_tipo_admissao_fat%type;
nr_seq_episodio_w				atendimento_paciente.nr_seq_episodio%type;

cursor C01 is
	select	nvl(b.qt_tempo_normal, 0) qt_tempo_normal,
			nvl(b.nr_sequencia, 0) nr_sequencia
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = CONST_ML_TASK_LIST_W
								and		x.ie_situacao = 'A')
	and		nvl(b.ie_tipo_pend_carta,'A') = 'A';

cursor C02 is
	select  a.nm_usuario_resp nm_usuario_w
    from    PARTICIPANTE_CARTA_MEDICA a
    where   NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
    and     a.IE_DEVE_ASSINAR = 'S';

cursor C03 is
    select  a.nm_usuario_resp nm_usuario_w
    from    PARTICIPANTE_CARTA_MEDICA a
    where   NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
    and     NR_SEQ_APRESENT =   (   select  min(NR_SEQ_APRESENT)
                                    from    PARTICIPANTE_CARTA_MEDICA b
                                    where   b.NR_SEQ_CARTA_MAE = a.NR_SEQ_CARTA
                                    and     b.IE_DEVE_ASSINAR = 'S'
									and		b.DT_ASSINATURA is null);

cursor C04 is
    select  a.nm_usuario_resp nm_usuario_w
	from    PARTICIPANTE_CARTA_MEDICA a
	where   a.NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
	and     a.IE_DEVE_ASSINAR = 'S'
	and     a.NR_SEQ_APRESENT in   (    select  NR_SEQ_APRESENT
										from    PARTICIPANTE_CARTA_MEDICA b
										where   b.NR_SEQ_CARTA_MAE = a.NR_SEQ_CARTA_MAE
										and     b.IE_DEVE_ASSINAR = 'S'
										and     b.NR_SEQ_APRESENT > (	select	c.NR_SEQ_APRESENT
																		from 	PARTICIPANTE_CARTA_MEDICA c
																		where 	c.NR_SEQ_CARTA_MAE = b.NR_SEQ_CARTA_MAE
																		and 	c.nm_usuario_resp = :new.nm_usuario
																		and 	c.DT_ASSINATURA is not null ));

cursor C05 is
    select  a.nm_usuario_resp nm_usuario_w
	from    PARTICIPANTE_CARTA_MEDICA a
	where   a.NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
	and     a.IE_DEVE_ASSINAR = 'S'
	and     a.NR_SEQ_APRESENT =   (     select  min(NR_SEQ_APRESENT)
										from    PARTICIPANTE_CARTA_MEDICA b
										where   b.NR_SEQ_CARTA_MAE = a.NR_SEQ_CARTA_MAE
										and     b.IE_DEVE_ASSINAR = 'S'
										and     b.NR_SEQ_APRESENT > (	select	c.NR_SEQ_APRESENT
																		from 	PARTICIPANTE_CARTA_MEDICA c
																		where 	c.NR_SEQ_CARTA_MAE = b.NR_SEQ_CARTA_MAE
																		and 	c.nm_usuario_resp = :new.nm_usuario
																		and 	c.DT_ASSINATURA is not null ));

BEGIN
	if (:old.ds_carta <> :new.ds_carta and
		:old.nm_usuario <> :new.nm_usuario and
		:new.IE_STATUS_APROVACAO in ('EE', 'AP')) then
		--cd_profissional_atual_w = obter_pf_usuario(:new,'C');

		select	count(*)
		into	ie_participante_w
		from	PARTICIPANTE_CARTA_MEDICA
		where	NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
		and 	nm_usuario_resp = :new.nm_usuario;

		select	count(*)
		into	qt_pendencias_assinatura_w
		from	wl_worklist a,
				wl_item b,
				wl_regra_item c
		where	a.nr_seq_carta_mae = :new.nr_seq_carta_mae
		and		a.dt_final_real is null
		and		b.nr_sequencia = a.nr_seq_item
		and		c.nr_sequencia = a.nr_seq_regra
		and		b.cd_categoria = CONST_ML_TASK_LIST_W
		and		c.ie_tipo_pend_carta = 'A'
		and		b.ie_situacao = 'A'
		and		c.ie_situacao = 'A'
		and		a.cd_profissional <> :new.CD_MEDICO;

		select	max(nr_seq_tipo_admissao_fat),
				max(nr_seq_episodio)
		into	nr_seq_tipo_adm_fat_atd_w,
				nr_seq_episodio_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		select	count(*)
		into	ie_aprovado_w
		from	PARTICIPANTE_CARTA_MEDICA
		where	NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
		and 	nvl(ie_deve_assinar, 'S') = 'S'
		and		dt_assinatura is null
		and		NM_USUARIO_ASSINAT is null;

		if (ie_aprovado_w = 0) then
			atualizar_status_aprovacao(:new.nr_seq_carta_mae, 'AP');
		else
			if (ie_participante_w > 0) then
				for c01_w in C01 loop
					for c04_w in C04 loop
						cd_profissional_w := OBTER_PESSOA_FISICA_USUARIO(c04_w.nm_usuario_w, 'C');
						wl_gerar_finalizar_tarefa(	CONST_ML_TASK_LIST_W,
													'F',
													null,
													null,
													wheb_usuario_pck.get_nm_usuario,
													null,
													'S',
													null,
													null,
													null,
													null,
													null,
													:new.nr_seq_carta_mae,
													null,
													null,
													null,
													c01_w.nr_sequencia,
													null,
													null,
													null,
													null,
													null,
													null,
													null,
													sysdate,
													null,
													null,
													null);
						update	PARTICIPANTE_CARTA_MEDICA
						set		dt_assinatura = null,
								NM_USUARIO_ASSINAT = null
						where 	NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
						and		ie_deve_assinar = 'S'
						and 	nm_usuario_resp = c04_w.nm_usuario_w;

						commit;

						for c05_w in C05 loop
							begin
								wl_gerar_finalizar_tarefa(	CONST_ML_TASK_LIST_W,
															'I',
															:new.nr_atendimento,
															:new.cd_pessoa_fisica,
															wheb_usuario_pck.get_nm_usuario,
															(sysdate+(c01_w.qt_tempo_normal/24)),
															'S',
															null,
															null,
															null,
															null,
															null,
															:new.nr_seq_carta_mae,
															null,
															null,
															null,
															c01_w.nr_sequencia,
															null,
															null,
															null,
															null,
															null,
															null,
															null,
															sysdate,
															nr_seq_episodio_w,
															null,
															obter_pf_usuario(c05_w.nm_usuario_w,'C'));
							end;
						end loop;

					end loop;
				end loop;
			else
				update	PARTICIPANTE_CARTA_MEDICA
				set		dt_assinatura = null,
						NM_USUARIO_ASSINAT = null
				where 	NR_SEQ_CARTA_MAE = :new.nr_seq_carta_mae
				and		ie_deve_assinar = 'S';

				commit;

				for c01_w in C01 loop
				begin
					wl_gerar_finalizar_tarefa(	CONST_ML_TASK_LIST_W,
												'F',
												null,
												null,
												wheb_usuario_pck.get_nm_usuario,
												null,
												'S',
												null,
												null,
												null,
												null,
												null,
												:new.nr_seq_carta_mae,
												null,
												null,
												null,
												c01_w.nr_sequencia,
												null,
												null,
												null,
												null,
												null,
												null,
												null,
												sysdate,
												null,
												null,
												null);

					if	(c01_w.qt_tempo_normal > 0 and obter_se_regra_geracao(c01_w.nr_sequencia, nr_seq_episodio_w, nr_seq_tipo_adm_fat_atd_w) = 'S') then
						for c03_w in C03 loop
							cd_pessoa_usuario_w := OBTER_PESSOA_FISICA_USUARIO(c03_w.nm_usuario_w, 'C');
							wl_gerar_finalizar_tarefa(	CONST_ML_TASK_LIST_W,
														'I',
														:new.nr_atendimento,
														:new.cd_pessoa_fisica,
														wheb_usuario_pck.get_nm_usuario,
														(sysdate+(c01_w.qt_tempo_normal/24)),
														'S',
														null,
														null,
														null,
														null,
														null,
														:new.nr_seq_carta_mae,
														null,
														null,
														null,
														c01_w.nr_sequencia,
														null,
														null,
														null,
														null,
														null,
														null,
														null,
														sysdate,
														nr_seq_episodio_w,
														null,
														cd_pessoa_usuario_w);
							atualizar_status_aprovacao(:new.nr_seq_carta_mae, 'EE');
						end loop;
					end if;
				end;
				end loop;
			end if;
		end if;
	end if;
END;
/


ALTER TABLE TASY.CARTA_MEDICA ADD (
  CONSTRAINT CARMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CARTA_MEDICA ADD (
  CONSTRAINT CARMED_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CARMED_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_MOVIMENTACAO) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO),
  CONSTRAINT CARMED_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CARMED_CARMED_FK 
 FOREIGN KEY (NR_SEQ_CARTA_MAE) 
 REFERENCES TASY.CARTA_MEDICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CARMED_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CARMED_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CARMED_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CARMED_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CARMED_CAMEMOD_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.CARTA_MEDICA_MODELO (NR_SEQUENCIA),
  CONSTRAINT CARMED_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CARMED_CAEPPA_FK 
 FOREIGN KEY (NR_SEQ_EPISODIO) 
 REFERENCES TASY.EPISODIO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CARTA_MEDICA TO NIVEL_1;

