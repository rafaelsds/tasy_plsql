ALTER TABLE TASY.DUV_VER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DUV_VER CASCADE CONSTRAINTS;

CREATE TABLE TASY.DUV_VER
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_MENSAGEM        NUMBER(10)             NOT NULL,
  PR_CABECA              NUMBER(15,4),
  PR_PESCOCO             NUMBER(15,4),
  PR_QUEIMADO_FRENTE     NUMBER(15,4),
  PR_QUEIMADO_COSTAS     NUMBER(15,4),
  PR_NADEGA_DIREITA      NUMBER(15,4),
  PR_NADEGA_ESQUERDA     NUMBER(15,2),
  PR_GENITALIA           NUMBER(15,4),
  PR_BRACO_DIREITO       NUMBER(15,4),
  PR_BRACO_ESQUERDO      NUMBER(15,4),
  PR_ANTEBRACO_DIREITO   NUMBER(15,2),
  PR_ANTEBRACO_ESQUERDO  NUMBER(15,4),
  PR_MAO_DIREITA         NUMBER(15,4),
  PR_MAO_ESQUERDA        NUMBER(15,4),
  PR_COXA_DIREITA        NUMBER(15,4),
  PR_COXA_ESQUERDA       NUMBER(15,4),
  PR_PERNA_DIREITA       NUMBER(15,4),
  PR_PERNA_ESQUERDA      NUMBER(15,4),
  PR_PE_DIREITO          NUMBER(15,4),
  PR_PE_ESQUERDO         NUMBER(15,4),
  IE_GRAU_SEVERIDADE     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DUVVER_DUVMENS_FK_I ON TASY.DUV_VER
(NR_SEQ_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DUVVER_PK ON TASY.DUV_VER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DUV_VER_AFTER_UPDATE
after insert or update ON TASY.DUV_VER for each row
declare

nr_sequencia_w	number(10);
vl_soma_w	number(15,4);
pr_total_w	DUV_SVB.pr_total%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

select	max(nr_sequencia)
into	nr_sequencia_w
from	duv_svb
where	nr_seq_mensagem	= :new.nr_seq_mensagem;

vl_soma_w	:=	nvl(:new.PR_ANTEBRACO_DIREITO,0) + nvl(:new.PR_ANTEBRACO_ESQUERDO,0) + nvl(:new.PR_BRACO_DIREITO,0) + nvl(:new.PR_BRACO_ESQUERDO,0) + nvl(:new.PR_CABECA,0) + nvl(:new.PR_COXA_DIREITA,0) + nvl(:new.PR_COXA_ESQUERDA,0) +
			nvl(:new.PR_GENITALIA,0) + nvl(:new.PR_MAO_DIREITA,0) + nvl(:new.PR_MAO_ESQUERDA,0) + nvl(:new.PR_NADEGA_DIREITA,0) + nvl(:new.PR_NADEGA_ESQUERDA,0) + nvl(:new.PR_PE_DIREITO,0) + nvl(:new.PR_PE_ESQUERDO,0) +
			nvl(:new.PR_PERNA_DIREITA,0) + nvl(:new.PR_PERNA_ESQUERDA,0) + nvl(:new.PR_PESCOCO,0) + nvl(:new.PR_QUEIMADO_COSTAS,0) + nvl(:new.PR_QUEIMADO_FRENTE,0);

if	(:new.IE_GRAU_SEVERIDADE = '1') then
	update	duv_svb
	set	pr_2grau_a 	= vl_soma_w
	where	nr_sequencia	= nr_sequencia_w;

elsif	(:new.IE_GRAU_SEVERIDADE = '2') then
	update	duv_svb
	set	pr_2grau_b 	= vl_soma_w
	where	nr_sequencia	= nr_sequencia_w;

elsif	(:new.IE_GRAU_SEVERIDADE = '3') then
	update	duv_svb
	set	pr_3grau 	= vl_soma_w
	where	nr_sequencia	= nr_sequencia_w;

elsif	(:new.IE_GRAU_SEVERIDADE = '4') then
	update	duv_svb
	set	pr_4grau 	= vl_soma_w
	where	nr_sequencia	= nr_sequencia_w;
end if;

select	nvl(sum(pr_2grau_a),0) +
	nvl(sum(pr_2grau_b),0) +
	nvl(sum(pr_3grau),0) +
	nvl(sum(pr_4grau),0)
into	pr_total_w
from	duv_svb
where	nr_sequencia	= nr_sequencia_w;

update	duv_svb
set	pr_total 	= pr_total_w
where	nr_sequencia	= nr_sequencia_w;

end;
/


ALTER TABLE TASY.DUV_VER ADD (
  CONSTRAINT DUVVER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DUV_VER ADD (
  CONSTRAINT DUVVER_DUVMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSAGEM) 
 REFERENCES TASY.DUV_MENSAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DUV_VER TO NIVEL_1;

