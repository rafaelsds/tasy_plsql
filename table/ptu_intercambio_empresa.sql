ALTER TABLE TASY.PTU_INTERCAMBIO_EMPRESA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_INTERCAMBIO_EMPRESA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_INTERCAMBIO_EMPRESA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_INTERCAMBIO   NUMBER(10)               NOT NULL,
  DS_RAZAO_SOCIAL      VARCHAR2(40 BYTE)        NOT NULL,
  NM_EMPR_ABREV        VARCHAR2(30 BYTE)        NOT NULL,
  IE_TIPO_PESSOA       NUMBER(1)                NOT NULL,
  CD_CGC_CPF           VARCHAR2(14 BYTE)        NOT NULL,
  DS_ENDERECO          VARCHAR2(40 BYTE)        NOT NULL,
  CD_CEP               VARCHAR2(8 BYTE)         NOT NULL,
  CD_EMPRESA_ORIGEM    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FILIAL            NUMBER(4),
  NR_INSC_ESTADUAL     NUMBER(20),
  DS_COMPLEMENTO       VARCHAR2(20 BYTE),
  DS_BAIRRO            VARCHAR2(30 BYTE),
  NM_CIDADE            VARCHAR2(100 BYTE),
  SG_UF                VARCHAR2(2 BYTE),
  NR_DDD               NUMBER(4),
  NR_TELEFONE          NUMBER(9)                DEFAULT null,
  NR_FAX               NUMBER(9),
  DT_INCLUSAO          DATE,
  DT_EXCLUSAO          DATE,
  CD_MUNICIPIO_IBGE    VARCHAR2(7 BYTE),
  NR_SEQ_CONTRATO      NUMBER(10),
  NR_SEQ_EMISSOR       NUMBER(10),
  IE_TIPO_CONTRATO     VARCHAR2(2 BYTE),
  NR_SEQ_CONGENERE     NUMBER(10),
  NR_ENDERECO          VARCHAR2(6 BYTE),
  NR_DDI               VARCHAR2(3 BYTE),
  IE_CONTRATO_LOCAL    VARCHAR2(2 BYTE),
  CD_CAEPF             VARCHAR2(14 BYTE),
  CD_TIPO_LOGRADOURO   VARCHAR2(4 BYTE),
  NM_FANTASIA_EMPR     VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUINEM_PK ON TASY.PTU_INTERCAMBIO_EMPRESA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUINEM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUINEM_PLSCONG_FK_I ON TASY.PTU_INTERCAMBIO_EMPRESA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUINEM_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUINEM_PLSEMC_FK_I ON TASY.PTU_INTERCAMBIO_EMPRESA
(NR_SEQ_EMISSOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUINEM_PLSEMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUINEM_PTUINTE_FK_I ON TASY.PTU_INTERCAMBIO_EMPRESA
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_INTERCAMBIO_EMPRESA ADD (
  CONSTRAINT PTUINEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_INTERCAMBIO_EMPRESA ADD (
  CONSTRAINT PTUINEM_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PTUINEM_PTUINTE_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PTU_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PTUINEM_PLSEMC_FK 
 FOREIGN KEY (NR_SEQ_EMISSOR) 
 REFERENCES TASY.PLS_EMISSOR_CARTEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_INTERCAMBIO_EMPRESA TO NIVEL_1;

