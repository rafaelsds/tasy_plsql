ALTER TABLE TASY.MARCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MARCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MARCA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DS_MARCA               VARCHAR2(80 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_TIPO            NUMBER(10),
  QT_DIA_RESSUP          NUMBER(3),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_REPROVACAO          DATE,
  NM_USUARIO_REPROVACAO  VARCHAR2(15 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_SISTEMA_ANT         VARCHAR2(80 BYTE),
  CD_CNPJ_FABRICANTE     VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MARCA_PESJURI_FK_I ON TASY.MARCA
(CD_CNPJ_FABRICANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MARCA_PK ON TASY.MARCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MARCA_TIPMARC_FK_I ON TASY.MARCA
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MARCA_ATUAL
before insert or update ON TASY.MARCA for each row
DECLARE
PRAGMA AUTONOMOUS_TRANSACTION;
ie_bloqueia_marca_igual_w		varchar2(1);
qt_existe_w			number(10);
cd_estabelecimento_w		number(5);

begin

select	wheb_usuario_pck.get_Cd_estabelecimento
into	cd_estabelecimento_w
from	dual;

select	nvl(max(IE_CAD_MARCA_IGUAL),'N')
into	ie_bloqueia_marca_igual_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_bloqueia_marca_igual_w = 'S') then
	begin

	select	count(*)
	into	qt_existe_w
	from	marca
	where	nr_sequencia <> :new.nr_sequencia
	and	upper(elimina_acentuacao(ds_marca)) = upper(elimina_acentuacao(:new.ds_marca));

	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(266244);
		--'J� existe marca cadastrada com esta descri��o!' || chr(10) ||
		--'Verifique.');
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.MARCA ADD (
  CONSTRAINT MARCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MARCA ADD (
  CONSTRAINT MARCA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_FABRICANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MARCA_TIPMARC_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_MARCA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MARCA TO NIVEL_1;

