ALTER TABLE TASY.RULE_INDEXED_SELECT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RULE_INDEXED_SELECT CASCADE CONSTRAINTS;

CREATE TABLE TASY.RULE_INDEXED_SELECT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_SQL               VARCHAR2(4000 BYTE),
  DS_CORE              VARCHAR2(100 BYTE),
  DS_TITULO            VARCHAR2(150 BYTE),
  IE_TIPO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RUINSQL_PK ON TASY.RULE_INDEXED_SELECT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RULE_INDEXED_SELECT_tp  after update ON TASY.RULE_INDEXED_SELECT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_TITULO,1,500);gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'RULE_INDEXED_SELECT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'RULE_INDEXED_SELECT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CORE,1,4000),substr(:new.DS_CORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CORE',ie_log_w,ds_w,'RULE_INDEXED_SELECT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'RULE_INDEXED_SELECT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RULE_INDEXED_SELECT ADD (
  CONSTRAINT RUINSQL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.RULE_INDEXED_SELECT TO NIVEL_1;

