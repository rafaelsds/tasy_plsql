ALTER TABLE TASY.RESULT_LABORATORIO_INTEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_LABORATORIO_INTEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_LABORATORIO_INTEG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(14),
  NR_SEQ_PRESCRICAO    NUMBER(6)                DEFAULT null,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EXAME_INTEGRACAO  VARCHAR2(20 BYTE),
  DS_EXAME_INTEGRACAO  VARCHAR2(80 BYTE),
  NR_SEQ_RESULT_LAB    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RESLABINT_PK ON TASY.RESULT_LABORATORIO_INTEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESLABINT_RESLABO_FK_I ON TASY.RESULT_LABORATORIO_INTEG
(NR_SEQ_RESULT_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RESULT_LABORATORIO_INTEG ADD (
  CONSTRAINT RESLABINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULT_LABORATORIO_INTEG ADD (
  CONSTRAINT RESLABINT_RESLABO_FK 
 FOREIGN KEY (NR_SEQ_RESULT_LAB) 
 REFERENCES TASY.RESULT_LABORATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RESULT_LABORATORIO_INTEG TO NIVEL_1;

