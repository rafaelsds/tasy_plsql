ALTER TABLE TASY.ESCALA_AUDIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_AUDIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_AUDIT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  QT_FREQUENCIA          NUMBER(1)              NOT NULL,
  QT_DOSE_DIA            NUMBER(1)              NOT NULL,
  QT_FREQ_CINCO_DOSE     NUMBER(1)              NOT NULL,
  QT_NAO_CONSEGUE_PARAR  NUMBER(1)              NOT NULL,
  QT_NAO_CONSEGUE_FAZER  NUMBER(1)              NOT NULL,
  QT_BEBER_MANHA         NUMBER(1)              NOT NULL,
  QT_CULPADO_BEBER       NUMBER(1)              NOT NULL,
  QT_NAO_LEMBRAR         NUMBER(1)              NOT NULL,
  QT_FERIMENTO_PREJ      NUMBER(1)              NOT NULL,
  QT_SUGERIU_PARAR       NUMBER(1)              NOT NULL,
  QT_PONTOS              NUMBER(3)              NOT NULL,
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAIDI_ATEPACI_FK_I ON TASY.ESCALA_AUDIT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAIDI_I1 ON TASY.ESCALA_AUDIT
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAIDI_PERFIL_FK_I ON TASY.ESCALA_AUDIT
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAIDI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAIDI_PESFISI_FK_I ON TASY.ESCALA_AUDIT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAIDI_PESFISI_FK2_I ON TASY.ESCALA_AUDIT
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAIDI_PK ON TASY.ESCALA_AUDIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAIDI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_AUDIT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_AUDIT FOR EACH ROW
DECLARE
qt_reg_w	number(1);

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.cd_pessoa_fisica is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_pessoa_fisica	:= obter_pessoa_atendimento(:new.nr_atendimento,'C');
end if;

:new.qt_pontos	:= 	nvl(:new.qt_frequencia,0) +
			nvl(:new.qt_dose_dia,0) +
			nvl(:new.qt_freq_cinco_dose,0) +
			nvl(:new.qt_nao_consegue_fazer,0) +
			nvl(:new.qt_beber_manha,0) +
			nvl(:new.qt_culpado_beber,0) +
			nvl(:new.qt_nao_lembrar,0) +
			nvl(:new.qt_ferimento_prej,0) +
			nvl(:new.qt_sugeriu_parar,0) +
			nvl(:new.qt_nao_consegue_parar,0);

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ESCALA_AUDIT ADD (
  CONSTRAINT ESCAIDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_AUDIT ADD (
  CONSTRAINT ESCAIDI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAIDI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAIDI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAIDI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_AUDIT TO NIVEL_1;

