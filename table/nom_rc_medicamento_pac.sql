ALTER TABLE TASY.NOM_RC_MEDICAMENTO_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOM_RC_MEDICAMENTO_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOM_RC_MEDICAMENTO_PAC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CABECALHO       NUMBER(10)             NOT NULL,
  CD_MATERIAL            NUMBER(10)             NOT NULL,
  IE_VIA_APLICACAO       VARCHAR2(15 BYTE),
  IE_DESC_VIA_APLICACAO  VARCHAR2(255 BYTE)     NOT NULL,
  QT_DOSE                NUMBER(15,4)           NOT NULL,
  DT_INICIO              DATE,
  DT_FINAL               DATE,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DS_HTML                CLOB,
  DS_MATERIAL            VARCHAR2(255 BYTE),
  CD_SISTEMA_ANT         VARCHAR2(20 BYTE),
  DS_DOSE                VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.NOMRCMP_NOMRCCA_FK_I ON TASY.NOM_RC_MEDICAMENTO_PAC
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.NOMRCMP_PK ON TASY.NOM_RC_MEDICAMENTO_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.NOM_RC_MEDICAMENTO_PAC_tp  after update ON TASY.NOM_RC_MEDICAMENTO_PAC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_MATERIAL,1,4000),substr(:new.DS_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL',ie_log_w,ds_w,'NOM_RC_MEDICAMENTO_PAC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'NOM_RC_MEDICAMENTO_PAC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.NOM_RC_MEDICAMENTO_PAC ADD (
  CONSTRAINT NOMRCMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NOM_RC_MEDICAMENTO_PAC ADD (
  CONSTRAINT NOMRCMP_NOMRCCA_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.NOM_RC_CABECALHO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NOM_RC_MEDICAMENTO_PAC TO NIVEL_1;

