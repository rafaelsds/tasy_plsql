ALTER TABLE TASY.PROFISSIONAL_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROFISSIONAL_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROFISSIONAL_AGENDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PROFISSIONAL      VARCHAR2(10 BYTE),
  CD_FUNCAO            NUMBER(3),
  NR_SEQ_AGENDA        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_INDICACAO         VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_EQUIPE        NUMBER(10),
  CD_ESPECIALIDADE     NUMBER(5),
  NR_SEQ_EQUIPE_LISTA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROAGEN_AGEPACI_FK_I ON TASY.PROFISSIONAL_AGENDA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAGEN_EQUILISESP_FK_I ON TASY.PROFISSIONAL_AGENDA
(NR_SEQ_EQUIPE_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAGEN_ESPMEDI_FK_I ON TASY.PROFISSIONAL_AGENDA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAGEN_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROAGEN_FUNCAO_FK_I ON TASY.PROFISSIONAL_AGENDA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAGEN_FUNCAO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROAGEN_PESFISI_FK_I ON TASY.PROFISSIONAL_AGENDA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAGEN_PFEQUIP_FK_I ON TASY.PROFISSIONAL_AGENDA
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAGEN_PFEQUIP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROAGEN_PK ON TASY.PROFISSIONAL_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAGEN_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PROFISSIONAL_AGENDA_ATUAL
before insert or update ON TASY.PROFISSIONAL_AGENDA FOR EACH ROW
DECLARE

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);

pragma autonomous_transaction;

BEGIN
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);


select  max(a.ie_status_agenda),
	max(b.cd_tipo_agenda)
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :new.nr_seq_agenda;

exception
	when others then
      	null;
end;

if	(ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E') then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.PROFISSIONAL_AGENDA_DELETE
before delete ON TASY.PROFISSIONAL_AGENDA FOR EACH ROW
DECLARE

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);

pragma autonomous_transaction;

BEGIN
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);

select  max(a.ie_status_agenda),
	max(b.cd_tipo_agenda)
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :old.nr_seq_agenda;

exception
	when others then
      	null;
end;

if	((ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E')) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

end if;
END;
/


ALTER TABLE TASY.PROFISSIONAL_AGENDA ADD (
  CONSTRAINT PROAGEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROFISSIONAL_AGENDA ADD (
  CONSTRAINT PROAGEN_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PROAGEN_EQUILISESP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_LISTA) 
 REFERENCES TASY.EQUIPE_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT PROAGEN_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROAGEN_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO_MEDICO (CD_FUNCAO),
  CONSTRAINT PROAGEN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROAGEN_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROFISSIONAL_AGENDA TO NIVEL_1;

