ALTER TABLE TASY.W_IPASGO_DADOS_PROC_TRAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_IPASGO_DADOS_PROC_TRAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_IPASGO_DADOS_PROC_TRAT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_LINHA              NUMBER(7),
  TP_REGISTRO           NUMBER(2),
  NR_LINHA_ATEND        NUMBER(5),
  NR_LINHA_ATO          NUMBER(2),
  NR_LINHA_PROC         NUMBER(2),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  QT_PROCEDIMENTO       NUMBER(3),
  IE_VIA_ACESSO         VARCHAR2(1 BYTE),
  DT_MESANO_REFERENCIA  DATE,
  NR_INTERNO_CONTA      NUMBER(10),
  DS_LINHA              VARCHAR2(2000 BYTE),
  NR_SEQ_TIPO_FATURA    NUMBER(10),
  DT_PROCEDIMENTO       DATE,
  VL_CH_MEDICO          NUMBER(15,2),
  DT_EXEC_PROC_CONTA    DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WIPDPTR_PK ON TASY.W_IPASGO_DADOS_PROC_TRAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WIPDPTR_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_IPASGO_DADOS_PROC_TRAT ADD (
  CONSTRAINT WIPDPTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_IPASGO_DADOS_PROC_TRAT TO NIVEL_1;

