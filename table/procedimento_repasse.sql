ALTER TABLE TASY.PROCEDIMENTO_REPASSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO_REPASSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO_REPASSE
(
  NR_SEQ_PROCEDIMENTO         NUMBER(10)        NOT NULL,
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  VL_REPASSE                  NUMBER(15,2)      NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_SEQ_TERCEIRO             NUMBER(10)        NOT NULL,
  NR_LOTE_CONTABIL            NUMBER(10),
  NR_REPASSE_TERCEIRO         NUMBER(10),
  CD_CONTA_CONTABIL           VARCHAR2(20 BYTE),
  NR_SEQ_TRANS_FIN            NUMBER(10),
  VL_LIBERADO                 NUMBER(15,2),
  NR_SEQ_ITEM_RETORNO         NUMBER(10),
  IE_STATUS                   VARCHAR2(1 BYTE),
  NR_SEQ_ORIGEM               NUMBER(10),
  CD_REGRA                    NUMBER(5)         NOT NULL,
  DT_LIBERACAO                DATE,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  DT_CONTABIL_TITULO          DATE,
  NR_SEQ_RET_GLOSA            NUMBER(10),
  DT_CONTABIL                 DATE,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PARTIC               NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NR_PROCESSO_AIH             NUMBER(10),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  NR_INTERNO_CONTA_EST        NUMBER(10),
  NR_SEQ_CRITERIO             NUMBER(10),
  NR_SEQ_TRANS_FIN_REP_MAIOR  NUMBER(10),
  IE_ESTORNO                  VARCHAR2(1 BYTE),
  IE_REPASSE_CALC             VARCHAR2(1 BYTE),
  VL_ORIGINAL_REPASSE         NUMBER(15,2),
  NR_SEQ_REGRA_ITEM           NUMBER(10),
  IE_ANALISADO                VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_AUDIT_HIST      NUMBER(10),
  NR_SEQ_MOTIVO_DES           NUMBER(10),
  IE_DESC_CAIXA               VARCHAR2(1 BYTE),
  VL_DESP_CARTAO              NUMBER(15,2),
  NR_SEQ_PARCELA              NUMBER(10),
  VL_CUSTO_ITEM               NUMBER(15,2),
  VL_DESCONTO                 NUMBER(15,2),
  CD_MEDICO_LAUDO             VARCHAR2(10 BYTE) DEFAULT null,
  IE_TIPO_ATENDIMENTO         NUMBER(3)         DEFAULT null,
  IE_TIPO_CONVENIO            NUMBER(2)         DEFAULT null,
  CD_CONVENIO                 NUMBER(5)         DEFAULT null,
  DS_CLASSIF_ATUA_MEDICO      VARCHAR2(255 BYTE) DEFAULT null,
  NR_SEQ_PROC_CRIT_REPASSE    NUMBER(10)        DEFAULT null,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) DEFAULT null,
  IE_ORIGEM_PROCED            NUMBER(10)        DEFAULT null,
  QT_PROCEDIMENTO             NUMBER(9,3)       DEFAULT null,
  DS_MEDICO_LAUDO             VARCHAR2(255 BYTE) DEFAULT null,
  DS_CARATER                  VARCHAR2(255 BYTE) DEFAULT null,
  DS_MEDICO_RESP              VARCHAR2(255 BYTE) DEFAULT null,
  DS_VIA_ACESSO               VARCHAR2(255 BYTE) DEFAULT null,
  NR_PRESCRICAO               NUMBER(14)        DEFAULT null,
  DS_CATEGORIA                VARCHAR2(255 BYTE) DEFAULT null,
  DS_OBSERVACAO_CRITERIO      VARCHAR2(4000 BYTE) DEFAULT null,
  NM_MEDICO_REQ               VARCHAR2(255 BYTE) DEFAULT null,
  DS_FUNCAO_MED               VARCHAR2(255 BYTE) DEFAULT null,
  DS_SETOR_ATENDIMENTO        VARCHAR2(255 BYTE) DEFAULT null,
  DS_OBS_FORMA_REPASSE        VARCHAR2(255 BYTE) DEFAULT null,
  DS_CONTA_CONTABIL           VARCHAR2(255 BYTE) DEFAULT null,
  DT_ENTRADA_ATEND            DATE              DEFAULT null,
  NM_USUARIO_ORIGINAL         VARCHAR2(255 BYTE) DEFAULT null,
  DS_CENTRO_CUSTO             VARCHAR2(255 BYTE) DEFAULT null,
  DT_MESANO_REFERENCIA        DATE              DEFAULT null,
  DS_TIPO_ATENDIMENTO         VARCHAR2(255 BYTE) DEFAULT null,
  DT_PROCEDIMENTO             DATE              DEFAULT null,
  DS_REGRA                    VARCHAR2(255 BYTE) DEFAULT null,
  DS_CONVENIO                 VARCHAR2(255 BYTE) DEFAULT null,
  DS_TRANSACAO                VARCHAR2(255 BYTE) DEFAULT null,
  NM_MEDICO_EXECUTOR          VARCHAR2(255 BYTE) DEFAULT null,
  NM_MEDICO                   VARCHAR2(255 BYTE) DEFAULT null,
  DS_STATUS                   VARCHAR2(255 BYTE) DEFAULT null,
  DS_PROCEDIMENTO             VARCHAR2(255 BYTE) DEFAULT null,
  NM_PESSOA_FISICA            VARCHAR2(255 BYTE) DEFAULT null,
  CD_PROCEDIMENTO             NUMBER(15)        DEFAULT null,
  NR_ATENDIMENTO              NUMBER(10)        DEFAULT null,
  NR_INTERNO_CONTA            NUMBER(10)        DEFAULT null,
  NR_LAUDO                    NUMBER(10),
  IE_BLOQ_LAUDO_LIBERADO      VARCHAR2(1 BYTE),
  VL_CUSTO                    NUMBER(15,2),
  VL_IMPOSTO                  NUMBER(15,2),
  DT_ENTREGA_CONVENIO         DATE,
  DS_TIPO_CONV                VARCHAR2(255 BYTE),
  IE_ALTA_COMPLEXIDADE        VARCHAR2(1 BYTE),
  DS_LOG                      VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          15M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROREPA_CONCONT_FK_I ON TASY.PROCEDIMENTO_REPASSE
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_CONPACI_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_INTERNO_CONTA_EST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_CONREIT_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_ITEM_RETORNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_CONREIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_CORETGL_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_RET_GLOSA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_I1 ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_TERCEIRO, IE_STATUS, NR_REPASSE_TERCEIRO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_I2 ON TASY.PROCEDIMENTO_REPASSE
(DT_CONTABIL, DT_CONTABIL_TITULO, NR_SEQ_TERCEIRO, NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_I3 ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_PARTIC, NR_REPASSE_TERCEIRO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_LOTAUHI_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_LOTCONT_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_LOTE_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_MOALREP_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_MOTIVO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_MOALREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_MOCRPAR_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_PARCELA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_PESFISI_FK_I ON TASY.PROCEDIMENTO_REPASSE
(CD_MEDICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROREPA_PK ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_PROPACI_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_PROPART_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_PROREPA_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_REGRETE_FK_I ON TASY.PROCEDIMENTO_REPASSE
(CD_REGRA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_REGRETE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_REPTERC_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_REPASSE_TERCEIRO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_SUSAIHP_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_PROCESSO_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_SUSAIHP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_TERCEIR_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_TERCEIRO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROREPA_TRAFINA_FK_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_TRANS_FIN)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREPA_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROREPA_TRAFINA_FK2_I ON TASY.PROCEDIMENTO_REPASSE
(NR_SEQ_TRANS_FIN_REP_MAIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROCEDIMENTO_REPASSE_tp  after update ON TASY.PROCEDIMENTO_REPASSE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PROCEDIMENTO_REPASSE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PROCEDIMENTO_REPASSE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Procedimento_Repasse_Delete
BEFORE DELETE ON TASY.PROCEDIMENTO_REPASSE FOR EACH ROW
declare

ie_status_w	varchar2(255);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:old.nr_repasse_terceiro is not null) then

		select 	ie_status
		into 	ie_status_w
		from 	repasse_terceiro
		where 	nr_repasse_terceiro = :old.nr_repasse_terceiro;

		if 	(ie_status_w = 'F') then
			/* Este procedimento j� est� vinculado � um repasse!
			N�o � poss�vel excluir este procedimento.
			Repasse: #@NR_REPASSE_TERCEIRO#@ */
			wheb_mensagem_pck.exibir_mensagem_abort(266950, 'NR_REPASSE_TERCEIRO=' || :old.nr_repasse_terceiro);
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Procedimento_Repasse_Atual
BEFORE UPDATE ON TASY.PROCEDIMENTO_REPASSE FOR EACH ROW
declare

cont_w				number(1, 0);
nr_seq_repasse_w		Number(10,0);
qt_reg_w			number(1);
ie_vincular_rep_proc_pos_w	varchar2(1);
ie_vinc_repasse_ret_w		varchar2(1);
ie_conta_repasse_w		parametro_faturamento.ie_conta_repasse%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin

:new.ds_log := substr(:old.ds_log || chr(13) || chr(10) || ' Atualizacao no repasse: ' || substr(dbms_utility.format_call_stack,1,4000),1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

cd_estabelecimento_w	:= obter_estabelecimento_ativo;

select	max(IE_CONTA_REPASSE)
into	ie_conta_repasse_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

CONSISTIR_VENC_REPASSE(
	:old.nr_repasse_terceiro,
	:new.nr_repasse_terceiro,
	:old.vl_repasse,
	:new.vl_repasse,
	:old.vl_liberado,
	:new.vl_liberado);


-- se esta desvinculando
if	(:new.nr_repasse_terceiro is null) and
	(:old.nr_repasse_terceiro is not null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :old.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(220250,'NR_REPASSE_P='||:old.nr_repasse_terceiro);
		/*'O repasse deste procedimento ja esta fechado!' || chr(13) ||
						'Repasse: ' || :old.nr_repasse_terceiro);*/
	end if;
end if;

-- se esta vinculando
if	(:new.nr_repasse_terceiro is not null) and
	(:old.nr_repasse_terceiro is null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :new.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(220250,'NR_REPASSE_P='||:old.nr_repasse_terceiro);
		/*'O repasse deste procedimento ja esta fechado!' || chr(13) ||
						'Repasse: ' || :old.nr_repasse_terceiro);*/
	end if;
end if;

if	(:new.ie_status <> :old.ie_status) and
	(((ie_conta_repasse_w = 'S') and
	((:new.ie_status = 'S') or
	(:new.ie_status = 'L'))) or
	((ie_conta_repasse_w = 'R') and
	(:new.ie_status = 'R'))) then
	begin
	if	(:new.dt_liberacao is null) then
		:new.dt_liberacao	:= sysdate;
	end if;

	if	(:new.nr_repasse_terceiro is null) and
		(:old.nr_repasse_terceiro is null) then

		obter_repasse_terceiro(
			:new.dt_liberacao,
			:new.nr_seq_terceiro,
			:new.nm_usuario,
			:new.nr_seq_procedimento,
			'P',
			nr_seq_repasse_w,
			null,
			null);

		select	nvl(max(b.ie_vincular_rep_proc_pos),'S'),
			nvl(max(b.ie_vinc_repasse_ret),'S')
		into	ie_vincular_rep_proc_pos_w,
			ie_vinc_repasse_ret_w
		from	parametro_faturamento b,
			terceiro a
		where	a.cd_estabelecimento	= b.cd_estabelecimento
		and	a.nr_sequencia		= :new.nr_seq_terceiro;

		if	(nvl(nr_seq_repasse_w,0) > 0) then
			if	((ie_vincular_rep_proc_pos_w = 'N') and
				 (trunc(:new.dt_liberacao, 'dd') > trunc(sysdate, 'dd'))) or
				(ie_vinc_repasse_ret_w = 'N') then
				:new.nr_repasse_terceiro	:= null;
			else
				:new.nr_repasse_terceiro	:= nr_seq_repasse_w;
			end if;
		end if;

	end if;
	end;
end if;

if	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'R') or
	 (:new.ie_status = 'S') or
	 (:new.ie_status = 'L')) then

	CONSISTIR_ITEM_REPASSE_TIT(:new.nr_seq_procedimento,null);

end if;

if  (:new.ie_status <> :old.ie_status) then
	:new.ds_status := substr(obter_valor_dominio(1129, :new.ie_status),1,50);
end if;

if  (:new.nr_seq_trans_fin <> :old.nr_seq_trans_fin) then
    :new.ds_transacao := substr(obter_desc_trans_financ(:new.nr_seq_trans_fin),1,100);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.procedimento_repasse_insert
before insert ON TASY.PROCEDIMENTO_REPASSE for each row
declare

qt_reg_w			number(1);

begin

:new.ds_log := substr('Processo de geracao do Repasse: ' || dbms_utility.format_call_stack,1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_atualizacao_nrec is null) then
	begin
	:new.dt_atualizacao_nrec := sysdate;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.PROCEDIMENTO_REPASSE ADD (
  CONSTRAINT PROREPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO_REPASSE ADD (
  CONSTRAINT PROREPA_MOCRPAR_FK 
 FOREIGN KEY (NR_SEQ_PARCELA) 
 REFERENCES TASY.MOVTO_CARTAO_CR_PARCELA (NR_SEQUENCIA),
  CONSTRAINT PROREPA_PROREPA_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.PROCEDIMENTO_REPASSE (NR_SEQUENCIA),
  CONSTRAINT PROREPA_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT PROREPA_MOALREP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DES) 
 REFERENCES TASY.MOTIVO_ALT_REPASSE (NR_SEQUENCIA),
  CONSTRAINT PROREPA_PROPART_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO, NR_SEQ_PARTIC) 
 REFERENCES TASY.PROCEDIMENTO_PARTICIPANTE (NR_SEQUENCIA,NR_SEQ_PARTIC),
  CONSTRAINT PROREPA_SUSAIHP_FK 
 FOREIGN KEY (NR_PROCESSO_AIH) 
 REFERENCES TASY.SUS_AIH_PROCESSO (NR_PROCESSO),
  CONSTRAINT PROREPA_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROREPA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PROREPA_REPTERC_FK 
 FOREIGN KEY (NR_REPASSE_TERCEIRO) 
 REFERENCES TASY.REPASSE_TERCEIRO (NR_REPASSE_TERCEIRO),
  CONSTRAINT PROREPA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PROREPA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PROREPA_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT PROREPA_REGRETE_FK 
 FOREIGN KEY (CD_REGRA) 
 REFERENCES TASY.REGRA_REPASSE_TERCEIRO (CD_REGRA),
  CONSTRAINT PROREPA_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PROREPA_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROREPA_CORETGL_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT PROREPA_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA_EST) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT PROREPA_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_REP_MAIOR) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROCEDIMENTO_REPASSE TO NIVEL_1;

