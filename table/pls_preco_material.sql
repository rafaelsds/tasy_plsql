ALTER TABLE TASY.PLS_PRECO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRECO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRECO_MATERIAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO          NUMBER(10)              NOT NULL,
  NR_SEQ_MATERIAL       NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA_MAT  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPREM_I1 ON TASY.PLS_PRECO_MATERIAL
(NR_SEQ_GRUPO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPREM_I2 ON TASY.PLS_PRECO_MATERIAL
(NR_SEQ_GRUPO, NVL("NR_SEQ_MATERIAL",(-1)))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPREM_PK ON TASY.PLS_PRECO_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPREM_PLSESMAT_FK_I ON TASY.PLS_PRECO_MATERIAL
(NR_SEQ_ESTRUTURA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPREM_PLSMAT_FK_I ON TASY.PLS_PRECO_MATERIAL
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPREM_PLSPRGM_FK_I ON TASY.PLS_PRECO_MATERIAL
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREM_PLSPRGM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_preco_material_atual_tot
before insert or update or delete ON TASY.PLS_PRECO_MATERIAL for each row
declare

begin
-- qualquer coisa que acontecer manda atualizar o grupo na tabela
if	(deleting) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_MATERIAL_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_PRECO_MATERIAL_ATUAL_TOT',
								'nr_seq_grupo_p=' || :old.nr_seq_grupo);
else
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_MATERIAL_TM',
								wheb_usuario_pck.get_nm_usuario,
								'PLS_PRECO_MATERIAL_ATUAL_TOT',
								'nr_seq_grupo_p=' || :new.nr_seq_grupo);
end if;

end pls_preco_material_atual_tot;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PRECO_MATERIAL_tp  after update ON TASY.PLS_PRECO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA_MAT,1,4000),substr(:new.NR_SEQ_ESTRUTURA_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA_MAT',ie_log_w,ds_w,'PLS_PRECO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PRECO_MATERIAL ADD (
  CONSTRAINT PLSPREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRECO_MATERIAL ADD (
  CONSTRAINT PLSPREM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSPREM_PLSPRGM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSPREM_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRECO_MATERIAL TO NIVEL_1;

