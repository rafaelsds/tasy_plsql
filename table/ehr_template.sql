ALTER TABLE TASY.EHR_TEMPLATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_TEMPLATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_TEMPLATE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DS_TEMPLATE               VARCHAR2(80 BYTE)   NOT NULL,
  NR_SEQ_APRES              NUMBER(15)          NOT NULL,
  QT_POS_ESQUERDA           NUMBER(5)           NOT NULL,
  QT_TOPO_CAMPO             NUMBER(5)           NOT NULL,
  QT_INTERVALO_CAMPO        NUMBER(3)           NOT NULL,
  DT_LIBERACAO              DATE,
  IE_EXPORTAR               VARCHAR2(1 BYTE),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIV         VARCHAR2(15 BYTE),
  NR_SEQ_RELATORIO          NUMBER(10),
  IE_CLUSTER                VARCHAR2(1 BYTE),
  IE_FINALIZAR_ATENDIMENTO  VARCHAR2(1 BYTE),
  IE_EVOLUCAO_CLINICA       VARCHAR2(3 BYTE),
  IE_GERAR_EVOLUCAO         VARCHAR2(1 BYTE),
  DS_MSG_CAMPO_OBRIGATORIO  VARCHAR2(255 BYTE),
  IE_FICAR_GRID_CLUSTER     VARCHAR2(1 BYTE),
  CD_UNIDADE                VARCHAR2(100 BYTE),
  IE_PREV_ALTA              VARCHAR2(1 BYTE),
  IE_USUARIO_RESTRITO       VARCHAR2(1 BYTE),
  IE_REGRA_CLUSTER          VARCHAR2(1 BYTE),
  IE_CONSISTIR_CAMPO_OBRIG  VARCHAR2(1 BYTE),
  IE_ALTA_MED               VARCHAR2(1 BYTE),
  NR_SEQ_PHILIPS            NUMBER(15),
  CD_GRUPO                  NUMBER(3),
  IE_TEMPLATE_UNICO         VARCHAR2(1 BYTE),
  IE_BLOQ_DUPLICACAO_USUAR  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRTEMP_EHRGRUTEMP_FK_I ON TASY.EHR_TEMPLATE
(CD_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRTEMP_I1 ON TASY.EHR_TEMPLATE
(DT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE UNIQUE INDEX TASY.EHRTEMP_PK ON TASY.EHR_TEMPLATE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRTEMP_RELATOR_FK_I ON TASY.EHR_TEMPLATE
(NR_SEQ_RELATORIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRTEMP_TIPEVOL_FK_I ON TASY.EHR_TEMPLATE
(IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRTEMP_UK ON TASY.EHR_TEMPLATE
(NR_SEQ_PHILIPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EHR_TEMPLATE_tp  after update ON TASY.EHR_TEMPLATE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_TEMPLATE,1,500);gravar_log_alteracao(substr(:old.DS_TEMPLATE,1,4000),substr(:new.DS_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'DS_TEMPLATE',ie_log_w,ds_w,'EHR_TEMPLATE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'EHR_TEMPLATE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ehr_template_delete
before delete ON TASY.EHR_TEMPLATE for each row
declare
ie_registro_filho_w  	varchar2(1);
pragma autonomous_transaction;

begin

select 	nvl(max('S'),'N')
into	ie_registro_filho_w
from	ehr_template_conteudo
where	nr_seq_template = :old.nr_sequencia;

if  (ie_registro_filho_w = 'S') then
	Wheb_mensagem_pck.exibir_mensagem_abort(687932);
end	if;

end;
/


ALTER TABLE TASY.EHR_TEMPLATE ADD (
  CONSTRAINT EHRTEMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT EHRTEMP_UK
 UNIQUE (NR_SEQ_PHILIPS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_TEMPLATE ADD (
  CONSTRAINT EHRTEMP_EHRGRUTEMP_FK 
 FOREIGN KEY (CD_GRUPO) 
 REFERENCES TASY.EHR_GRUPO_TEMPLATE (CD_GRUPO),
  CONSTRAINT EHRTEMP_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA),
  CONSTRAINT EHRTEMP_TIPEVOL_FK 
 FOREIGN KEY (IE_EVOLUCAO_CLINICA) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO));

GRANT SELECT ON TASY.EHR_TEMPLATE TO NIVEL_1;

