ALTER TABLE TASY.PLS_RPS_PREST_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RPS_PREST_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RPS_PREST_PLANO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR_RPS  NUMBER(10)              NOT NULL,
  NR_SEQ_PLANO          NUMBER(10)              NOT NULL,
  NR_REGISTRO_PLANO     VARCHAR2(10 BYTE),
  CD_PLANO              VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPPP_PK ON TASY.PLS_RPS_PREST_PLANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPPP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPPP_PLSPLAN_FK_I ON TASY.PLS_RPS_PREST_PLANO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPPP_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPPP_PLSRPPR_FK_I ON TASY.PLS_RPS_PREST_PLANO
(NR_SEQ_PRESTADOR_RPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPPP_PLSRPPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_RPS_PREST_PLANO ADD (
  CONSTRAINT PLSRPPP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RPS_PREST_PLANO ADD (
  CONSTRAINT PLSRPPP_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSRPPP_PLSRPPR_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_RPS) 
 REFERENCES TASY.PLS_RPS_PRESTADOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_RPS_PREST_PLANO TO NIVEL_1;

