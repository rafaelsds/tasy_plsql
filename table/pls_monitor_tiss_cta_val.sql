ALTER TABLE TASY.PLS_MONITOR_TISS_CTA_VAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_CTA_VAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_CTA_VAL
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_LOTE_MONITOR          NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR             NUMBER(10),
  NR_SEQ_SEGURADO              NUMBER(10),
  CD_CNES_PREST_EXEC           VARCHAR2(7 BYTE),
  IE_IDENTIF_PREST_EXEC        VARCHAR2(1 BYTE),
  CD_CPF_CGC_PREST_EXEC        VARCHAR2(14 BYTE),
  CD_MUNICIPIO_PREST_EXEC      VARCHAR2(7 BYTE),
  NR_CARTAO_NAC_SUS            VARCHAR2(20 BYTE),
  CD_MUNICIPIO_BENEF           VARCHAR2(7 BYTE),
  IE_SEXO                      VARCHAR2(1 BYTE),
  DT_NASCIMENTO                DATE,
  NR_PROTOCOLO_ANS             VARCHAR2(20 BYTE),
  IE_ORIGEM_EVENTO             VARCHAR2(1 BYTE),
  CD_GUIA_PRESTADOR            VARCHAR2(20 BYTE),
  CD_GUIA_OPERADORA            VARCHAR2(20 BYTE),
  IE_REEMBOLSO                 VARCHAR2(20 BYTE),
  DT_SOLICITACAO               DATE,
  DT_AUTORIZACAO               DATE,
  DT_REALIZACAO                DATE,
  DT_INICIO_FATURAMENTO        DATE,
  DT_FIM_INTERNACAO            DATE,
  DT_PROTOCOLO                 DATE,
  DT_PAGAMENTO_PREVISTO        DATE,
  IE_TIPO_CONSULTA             VARCHAR2(1 BYTE),
  CD_CBO_EXECUTANTE            VARCHAR2(15 BYTE),
  IE_RECEM_NASC                VARCHAR2(1 BYTE),
  IE_INDICACAO_ACIDENTE        VARCHAR2(1 BYTE),
  IE_CARATER_ATENDIMENTO       VARCHAR2(1 BYTE),
  IE_TIPO_INTERNACAO           VARCHAR2(20 BYTE),
  IE_REGIME_INTERNACAO         VARCHAR2(1 BYTE),
  IE_TIPO_ATENDIMENTO          VARCHAR2(20 BYTE),
  IE_TIPO_FATURAMENTO          VARCHAR2(1 BYTE),
  NR_DIARIAS_ACOMPANHANTE      NUMBER(3),
  NR_DIARIAS_UTI               NUMBER(3),
  IE_MOTIVO_ENCERRAMENTO       VARCHAR2(2 BYTE),
  VL_COBRANCA_GUIA             NUMBER(15,2),
  VL_TOTAL_PROCEDIMENTO        NUMBER(10,2),
  VL_TOTAL_DIARIA              NUMBER(10,2),
  VL_TOTAL_TAXA                NUMBER(10,2),
  VL_TOTAL_MATERIAL            NUMBER(10,2),
  VL_TOTAL_OPM                 NUMBER(10,2),
  VL_TOTAL_MEDICAMENTOS        NUMBER(10,2),
  VL_TOTAL_PAGO                NUMBER(10,2),
  VL_TOTAL_TABELA_PROPRIA      NUMBER(10,2),
  VL_TOTAL_GLOSA               NUMBER(15,2),
  IE_STATUS                    VARCHAR2(3 BYTE),
  NR_SEQ_CONTA                 NUMBER(10)       NOT NULL,
  IE_CONTA_ATUALIZADA          VARCHAR2(1 BYTE),
  NR_SEQ_CBO_SAUDE             NUMBER(10),
  NR_SEQ_TIPO_ATENDIMENTO      NUMBER(10),
  NR_SEQ_CLINICA               NUMBER(10),
  DT_CONTA_FECHADA             DATE,
  DT_CONTA_FECHADA_RECURSO     DATE,
  DT_PAGAMENTO_RECURSO         DATE,
  DT_CANCELAMENTO_CONTA        DATE,
  IE_TIPO_ENVIO                VARCHAR2(1 BYTE),
  CD_GUIA_PRINCIPAL            VARCHAR2(20 BYTE),
  IE_TIPO_GUIA                 VARCHAR2(2 BYTE),
  CD_VERSAO_TISS               VARCHAR2(8 BYTE),
  DT_EVENTO                    DATE,
  IE_TIPO_EVENTO               VARCHAR2(3 BYTE),
  IE_GLOSA                     VARCHAR2(1 BYTE),
  NR_SEQ_CTA_ALT               NUMBER(10)       NOT NULL,
  VL_TOTAL_FORNECEDOR          NUMBER(10,2),
  NR_SEQ_CONTA_REC             NUMBER(10),
  NR_SEQ_MONITOR_GUIA          NUMBER(10),
  DT_EXCLUSAO_CONTA            DATE,
  NR_REGISTRO_OPERADORA_INTER  VARCHAR2(20 BYTE),
  NR_SEQ_LOTE_FRANQUIA         NUMBER(10),
  CD_GUIA_SADT_PRINC           VARCHAR2(20 BYTE),
  VL_TOTAL_COPART              NUMBER(18,2),
  NR_SEQ_PREST_FRANQUIA        NUMBER(10),
  NR_SEQ_PREST_INTER           NUMBER(10),
  IE_TIPO_ATEND_OPER           VARCHAR2(1 BYTE),
  NR_IDENT_PRE_ESTAB           NUMBER(15),
  NR_SEQ_CONTA_DISC            NUMBER(10),
  IE_FORNEC_DIRETO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTCV_CBOSAUD_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_I1 ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_LOTE_MONITOR, NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_I2 ON TASY.PLS_MONITOR_TISS_CTA_VAL
(CD_GUIA_OPERADORA, CD_GUIA_PRESTADOR, NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_I3 ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_CTA_ALT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTCV_PK ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSCLIN_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSCOME_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSFPPR_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_PREST_FRANQUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSFRPA_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_LOTE_FRANQUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSMOTG_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_MONITOR_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSMOTL_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_LOTE_MONITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSPRES_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSRGCO_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_CONTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSSEGU_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTCV_PLSTIAT_FK_I ON TASY.PLS_MONITOR_TISS_CTA_VAL
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_cta_val_upd
before update ON TASY.PLS_MONITOR_TISS_CTA_VAL for each row
declare

ds_observacao_w	varchar2(4000);

begin


	if (pls_se_aplicacao_tasy = 'N') then

		ds_observacao_w := 'Modifica��o de registro via fora do tasy - Modifica��o registrada na tabela pls_monitor_tiss_cta_val'||chr(13)||chr(10);

		if	(nvl(:old.ie_carater_atendimento,'X') <> nvl(:new.ie_carater_atendimento,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					' Car�ter atendimento: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_carater_atendimento||' - Modificada: '||:new.ie_carater_atendimento;
		end if;

		if	(nvl(:old.ie_conta_atualizada,'X') <> nvl(:new.ie_conta_atualizada,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta atualizada: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_conta_atualizada||' - Modificada: '||:new.ie_conta_atualizada;
		end if;

		if	(nvl(:old.ie_glosa,'X') <> nvl(:new.ie_glosa,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Glosa: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_glosa||' - Modificada: '||:new.ie_glosa;
		end if;

		if	(nvl(:old.ie_identif_prest_exec,'X') <> nvl(:new.ie_identif_prest_exec,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Identif prest: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_identif_prest_exec ||' - Modificada: '||:new.ie_identif_prest_exec;
		end if;

		if	(nvl(:old.ie_origem_evento,'X') <> nvl(:new.ie_origem_evento,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Origem evento: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_origem_evento||' - Modificada: '||:new.ie_origem_evento;
		end if;

		if	(nvl(:old.ie_recem_nasc,'X') <> nvl(:new.ie_recem_nasc,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Rec�m nascido: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_recem_nasc||' - Modificada: '||:new.ie_recem_nasc;
		end if;

		if	(nvl(:old.ie_reembolso,'X') <> nvl(:new.ie_reembolso,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						' ie_reembolso: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_reembolso||' - Modificada: '||:new.ie_reembolso;
		end if;

		if	(nvl(:old.ie_status,'X') <> nvl(:new.ie_status,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'ie_status: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.ie_status||' - Modificada: '||:new.ie_status;
		end if;

		if	(nvl(:old.cd_guia_operadora,'X') <> nvl(:new.cd_guia_operadora,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Guia operadora: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.cd_guia_operadora||' - Modificada: '||:new.cd_guia_operadora;
		end if;

		if	(nvl(:old.cd_guia_prestador,'X') <> nvl(:new.cd_guia_prestador,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Guia prestador: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.cd_guia_prestador||' - Modificada: '||:new.cd_guia_prestador;
		end if;

		if	(nvl(:old.cd_guia_principal,'X') <> nvl(:new.cd_guia_principal,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Guia principal: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.cd_guia_principal||' - Modificada: '||:new.cd_guia_principal;
		end if;

		if	(nvl(:old.cd_guia_sadt_princ,'X') <> nvl(:new.cd_guia_sadt_princ,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Guia SADT princ: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.cd_guia_sadt_princ||' - Modificada: '||:new.cd_guia_sadt_princ;
		end if;

		if	(:old.nr_seq_conta <> :new.nr_seq_conta) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Seq conta: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '|| :old.nr_seq_conta||' - Modificada: '||:new.nr_seq_conta;
		end if;


		insert into pls_monitor_log_lote(nr_sequencia, ds_log, dt_atualizacao , dt_atualizacao_nrec,
											nm_usuario, nm_usuario_nrec, nr_seq_lote  )
		values(pls_monitor_log_lote_seq.nextval, ds_observacao_w, sysdate, sysdate,
				:new.nm_usuario, :new.nm_usuario, :new.nr_seq_conta);


	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_cta_val_atual
before update ON TASY.PLS_MONITOR_TISS_CTA_VAL for each row
declare

begin
-- Caso tenha alguma altera��o no valor apresentado ou pago, zera os valores totais do lote
if	(:new.vl_cobranca_guia <> :old.vl_cobranca_guia) or
	(:new.vl_total_pago <> :old.vl_total_pago) then
	pls_gerencia_envio_ans_pck.atualiza_valores_totais_lote(:new.nr_seq_lote_monitor, :new.nm_usuario, 'Z');
end if;

end;
/


ALTER TABLE TASY.PLS_MONITOR_TISS_CTA_VAL ADD (
  CONSTRAINT PLSMTCV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_CTA_VAL ADD (
  CONSTRAINT PLSMTCV_PLSMOTL_FK 
 FOREIGN KEY (NR_SEQ_LOTE_MONITOR) 
 REFERENCES TASY.PLS_MONITOR_TISS_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSFPPR_FK 
 FOREIGN KEY (NR_SEQ_PREST_FRANQUIA) 
 REFERENCES TASY.PLS_FRANQ_PAG_PREST (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSRGCO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSFRPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FRANQUIA) 
 REFERENCES TASY.PLS_FRANQ_PAG (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSMOTG_FK 
 FOREIGN KEY (NR_SEQ_MONITOR_GUIA) 
 REFERENCES TASY.PLS_MONITOR_TISS_GUIA (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSMTCV_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_CTA_VAL TO NIVEL_1;

