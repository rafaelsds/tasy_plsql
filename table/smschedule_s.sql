ALTER TABLE TASY.SMSCHEDULE_S
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SMSCHEDULE_S CASCADE CONSTRAINTS;

CREATE TABLE TASY.SMSCHEDULE_S
(
  NAMEDOBJECT_ID_SEQUENCEID_    NUMBER(10),
  NAMEDOBJECT_ID_OBJECTTYPE_    NUMBER(10),
  SCHEDULE_HASEXPIRATION_       NUMBER(10),
  SCHEDULE_EXP_DATE_            DATE,
  SCHEDULE_EXP_TOD_SPM_         NUMBER(10),
  SCHEDULE_EXP_TOD_ZONE_        NUMBER(10),
  REPEATED_ISIMMEDIATE_         NUMBER(10),
  REPEATED_STARTTIME_DATE_      DATE,
  REPEATED_STARTTIME_TOD_SPM_   NUMBER(10),
  REPEATED_STARTTIME_TOD_ZONE_  NUMBER(10),
  REPEATED_SECONDSINPERIOD_     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.SMSCHEDULE_S ADD (
  PRIMARY KEY
 (NAMEDOBJECT_ID_SEQUENCEID_)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SMSCHEDULE_S TO NIVEL_1;

