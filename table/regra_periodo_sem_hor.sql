ALTER TABLE TASY.REGRA_PERIODO_SEM_HOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PERIODO_SEM_HOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PERIODO_SEM_HOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PERFIL             NUMBER(5),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_TIPO_ITEM          VARCHAR2(5 BYTE)        NOT NULL,
  HR_INICIO             VARCHAR2(5 BYTE)        NOT NULL,
  HR_FIM                VARCHAR2(5 BYTE)        NOT NULL,
  DS_MENSAGEM           VARCHAR2(2000 BYTE),
  DT_INICIO             DATE,
  DT_FIM                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGPESH_I1 ON TASY.REGRA_PERIODO_SEM_HOR
(IE_TIPO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPESH_PERFIL_FK_I ON TASY.REGRA_PERIODO_SEM_HOR
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPESH_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGPESH_PK ON TASY.REGRA_PERIODO_SEM_HOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPESH_SETATEN_FK_I ON TASY.REGRA_PERIODO_SEM_HOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGPESH_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atual_regra_periodo_sem_hor
before insert or update ON TASY.REGRA_PERIODO_SEM_HOR for each row
declare

begin

if (:new.hr_inicio is not null) and
   ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
	:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_fim is not null) and
   ((:new.hr_fim <> :old.hr_fim) or (:old.dt_fim is null)) then
	:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fim || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/


ALTER TABLE TASY.REGRA_PERIODO_SEM_HOR ADD (
  CONSTRAINT REGPESH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PERIODO_SEM_HOR ADD (
  CONSTRAINT REGPESH_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REGPESH_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.REGRA_PERIODO_SEM_HOR TO NIVEL_1;

