ALTER TABLE TASY.ESCALA_FORREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_FORREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_FORREST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_FORREST             VARCHAR2(3 BYTE)       NOT NULL,
  PR_REAVALIACAO         NUMBER(10),
  PR_MORTALIDADE         NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCFORR_ATEPACI_FK_I ON TASY.ESCALA_FORREST
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFORR_PESFISI_FK_I ON TASY.ESCALA_FORREST
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCFORR_PK ON TASY.ESCALA_FORREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_forrest_atual
before update or insert ON TASY.ESCALA_FORREST for each row
declare

begin
if	(:new.ie_forrest = '1A') then
	:new.pr_reavaliacao	:= 55;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '1B') then
	:new.pr_reavaliacao	:= 55;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '2A') then
	:new.pr_reavaliacao	:= 43;
	:new.pr_mortalidade	:= 11;
elsif	(:new.ie_forrest = '2B') then
	:new.pr_reavaliacao	:= 22;
	:new.pr_mortalidade	:= 7;
elsif	(:new.ie_forrest = '2C') then
	:new.pr_reavaliacao	:= 10;
	:new.pr_mortalidade	:= 3;
elsif	(:new.ie_forrest = '3') then
	:new.pr_reavaliacao	:= 5;
	:new.pr_mortalidade	:= 2;
end if;

end;
/


ALTER TABLE TASY.ESCALA_FORREST ADD (
  CONSTRAINT ESCFORR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_FORREST ADD (
  CONSTRAINT ESCFORR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCFORR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_FORREST TO NIVEL_1;

