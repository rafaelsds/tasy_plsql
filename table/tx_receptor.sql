ALTER TABLE TASY.TX_RECEPTOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_RECEPTOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_RECEPTOR
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  NR_SEQ_ORGAO                 NUMBER(10)       NOT NULL,
  DT_TRANSPLANTE               DATE,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DT_INICIO_DIALISE            DATE,
  NR_SESSOES_DIALISE           NUMBER(5),
  IE_RETRANSPLANTE             VARCHAR2(1 BYTE),
  DT_TRANSPLANTE_ANTERIOR      DATE,
  IE_PRIORIDADE                VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_RETRANSPLANTE  NUMBER(10),
  IE_TIPO_DOADOR               VARCHAR2(15 BYTE),
  IE_HIPERT_PRE_TRANSPLANTE    VARCHAR2(1 BYTE),
  IE_HIPERT_POS_TRANSPLANTE    VARCHAR2(1 BYTE),
  IE_TRATAMENTO_DIALISE        VARCHAR2(15 BYTE),
  NR_DROGAS_PRE_TRANSPLANTE    NUMBER(5),
  NR_DROGAS_POS_TRANSPLANTE    NUMBER(5),
  QT_CREATININA                NUMBER(5,2),
  NR_CEP_CIDADE_ORIGEM         VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  CD_EMPRESA                   NUMBER(4)        NOT NULL,
  IE_PACIENTE_EXTERNO          VARCHAR2(1 BYTE),
  CD_CGC_EXTERNO               VARCHAR2(14 BYTE),
  CD_MEDICO_RESP               VARCHAR2(10 BYTE),
  IE_DIABETE_POS_TRANSPLANTE   VARCHAR2(1 BYTE),
  IE_DIABETE_PRE_TRANSPLANTE   VARCHAR2(1 BYTE),
  QT_PESO_PRE_TRANSP           NUMBER(10,3),
  NR_GESTACAO                  NUMBER(10),
  QT_TRANSFUSOES               NUMBER(5),
  DT_ULTIMA_TRANSFUSAO         DATE,
  QT_TRANSPLANTE               NUMBER(10),
  DT_PERDA_ORGAO               DATE,
  NR_SEQ_CAUSA_MORTE           NUMBER(10),
  NR_SEQ_CAUSA_PERDA           NUMBER(10),
  NR_TRANSPLANTE               VARCHAR2(20 BYTE),
  IE_TABAGISMO                 VARCHAR2(1 BYTE),
  IE_ETILISTA                  VARCHAR2(1 BYTE),
  IE_HEPATITE_B                VARCHAR2(1 BYTE),
  IE_CMV                       VARCHAR2(1 BYTE),
  IE_HIV                       VARCHAR2(1 BYTE),
  IE_PERITONEAL                VARCHAR2(1 BYTE),
  IE_CATETER                   VARCHAR2(1 BYTE),
  IE_FISTULA                   VARCHAR2(1 BYTE),
  IE_HEPATITE_C                VARCHAR2(1 BYTE),
  CD_MUNICIPIO_IBGE            VARCHAR2(6 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TXRECE_CAPEORG_FK_I ON TASY.TX_RECEPTOR
(NR_SEQ_CAUSA_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_CAPEORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECE_CAUSAMO_FK_I ON TASY.TX_RECEPTOR
(NR_SEQ_CAUSA_MORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_CAUSAMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECE_EMPRESA_FK_I ON TASY.TX_RECEPTOR
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECE_ESTABEL_FK_I ON TASY.TX_RECEPTOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECE_PESFISI_FK_I ON TASY.TX_RECEPTOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXRECE_PESFISI_FK2_I ON TASY.TX_RECEPTOR
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXRECE_PESJURI_FK_I ON TASY.TX_RECEPTOR
(CD_CGC_EXTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXRECE_PK ON TASY.TX_RECEPTOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXRECE_SUSMUNI_FK_I ON TASY.TX_RECEPTOR
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXRECE_TXMORET_FK_I ON TASY.TX_RECEPTOR
(NR_SEQ_MOTIVO_RETRANSPLANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_TXMORET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECE_TXORGA_FK_I ON TASY.TX_RECEPTOR
(NR_SEQ_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECE_TXORGA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_receptor_insert
after insert on tx_receptor
for each row
declare

begin

insert into paciente_tratamento (
	nr_sequencia,
	cd_pessoa_fisica,
	ie_tratamento,
	dt_inicio_tratamento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	NR_SEQ_RECEPTOR
) values (
	paciente_tratamento_seq.nextval,
	:new.cd_pessoa_fisica,
	'TX',
	sysdate,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_sequencia
);

end;
/


CREATE OR REPLACE TRIGGER TASY.tx_receptor_update
before update ON TASY.TX_RECEPTOR for each row
declare
qt_registro_w			number(5);
nr_seq_tratamento_w		number(10);
cd_estabelecimento_w		number(5);
nr_seq_motivo_fim_tratamento_w	number(10);
nr_seq_motivo_fim_transp_w 	number(10);
nr_seq_tranp_transpl_w		number(10);
qt_reg_w	number(1);
ie_finaliza_pre_transp_w	varchar2(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

obter_param_usuario(7006,24,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_finaliza_pre_transp_w);

if 	(:new.dt_transplante is not null) and
	(:old.dt_transplante is null) then

	cd_estabelecimento_w	:= tx_obter_estab_receptor(:new.nr_sequencia);

	select 	nvl(max(nr_seq_motivo_fim_tratamento),0),
		nvl(max(nr_seq_motivo_fim_trat_transp),0)
	into	nr_seq_motivo_fim_tratamento_w,
		nr_seq_motivo_fim_transp_w
	from 	tx_parametros
	where	cd_estabelecimento = nvl(cd_estabelecimento_w,cd_estabelecimento);

	select 	nvl(max(nr_sequencia),0)
	into	nr_seq_tratamento_w
	from   	paciente_tratamento
	where  	ie_tratamento = 'TX'
	and	nr_seq_receptor = :new.nr_sequencia;

	if 	(nr_seq_tratamento_w > 0) and
		(nr_seq_motivo_fim_tratamento_w > 0) then

		update 	paciente_tratamento
		set	dt_final_tratamento = sysdate,
			nr_seq_motivo_fim = nr_seq_motivo_fim_tratamento_w
		where	nr_sequencia  = nr_seq_tratamento_w;
	end if;


	if	(ie_finaliza_pre_transp_w = 'N') then

		select  paciente_tratamento_seq.nextval
		into	nr_seq_tranp_transpl_w
		from	dual;

		insert into paciente_tratamento (nr_sequencia,
						cd_pessoa_fisica,
						ie_tratamento,
						dt_inicio_tratamento,
						dt_final_tratamento,
						ds_motivo_fim,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ds_observacao,
						nr_seq_motivo_fim,
						nr_seq_receptor)
				values		(nr_seq_tranp_transpl_w,
						:new.cd_pessoa_fisica,
						'TR',
						:new.dt_transplante,
						null,
						null,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						null,
						null,
						:new.nr_sequencia);

		if 	(nr_seq_motivo_fim_transp_w > 0) then

			update 	paciente_tratamento
			set	dt_final_tratamento = sysdate,
				nr_seq_motivo_fim = nr_seq_motivo_fim_transp_w
			where	nr_sequencia  = nr_seq_tranp_transpl_w;

		end if;
	end if;

	insert into paciente_tratamento (nr_sequencia,
					cd_pessoa_fisica,
					ie_tratamento,
					dt_inicio_tratamento,
					dt_final_tratamento,
					ds_motivo_fim,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_observacao,
					nr_seq_motivo_fim,
					nr_seq_receptor)
			values		(paciente_tratamento_seq.nextval,
					:new.cd_pessoa_fisica,
					'PT',
					:new.dt_transplante,
					null,
					null,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					null,
					null,
					:new.nr_sequencia);

end if;

if 	(:new.dt_transplante is null) and
	(:old.dt_transplante is not null) then

	select 	count(*)
	into	qt_registro_w
	from 	paciente_tratamento
	where 	ie_tratamento in ('PT','TR')
	and	nr_seq_receptor	= :new.nr_sequencia;

	if 	(qt_registro_w > 0) then
		--rase_application_error(-20011,'Existem tratamentos de transplante e p�s transplante para o receptor. ' || chr(10) || 'A data de transplante n�o pode ser retirada #@#@');
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(232949);
	end if;

end if;
<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.TX_RECEPTOR ADD (
  CONSTRAINT TXRECE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_RECEPTOR ADD (
  CONSTRAINT TXRECE_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT TXRECE_CAPEORG_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_PERDA) 
 REFERENCES TASY.CAUSA_PERDA_ORGAO (NR_SEQUENCIA),
  CONSTRAINT TXRECE_TXORGA_FK 
 FOREIGN KEY (NR_SEQ_ORGAO) 
 REFERENCES TASY.TX_ORGAO (NR_SEQUENCIA),
  CONSTRAINT TXRECE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TXRECE_TXMORET_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RETRANSPLANTE) 
 REFERENCES TASY.TX_MOTIVO_RETRANSPLANTE (NR_SEQUENCIA),
  CONSTRAINT TXRECE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TXRECE_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT TXRECE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_EXTERNO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TXRECE_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TXRECE_CAUSAMO_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_MORTE) 
 REFERENCES TASY.CAUSA_MORTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_RECEPTOR TO NIVEL_1;

