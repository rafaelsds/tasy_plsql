ALTER TABLE TASY.SIP_LOTE_ITEM_ASSISTENCIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIP_LOTE_ITEM_ASSISTENCIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  SG_UF                 VARCHAR2(2 BYTE)        NOT NULL,
  QT_EVENTO             NUMBER(15,3)            NOT NULL,
  QT_BENEFICIARIO       NUMBER(10)              NOT NULL,
  VL_DESPESA            NUMBER(15,2)            NOT NULL,
  NR_SEQ_ITEM_SIP       NUMBER(10)              NOT NULL,
  NR_SEQ_APRES          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_EVENTO             VARCHAR2(1 BYTE),
  IE_BENEF_CARENCIA     VARCHAR2(1 BYTE),
  IE_DESPESA            VARCHAR2(1 BYTE),
  IE_TIPO_CONTRATACAO   VARCHAR2(2 BYTE),
  IE_SEGMENTACAO_SIP    NUMBER(3),
  CD_CLASSIFICACAO_SIP  VARCHAR2(10 BYTE),
  NR_SEQ_SUPERIOR       NUMBER(10),
  DT_OCORRENCIA         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIPLOIA_I ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(CD_CLASSIFICACAO_SIP, NR_SEQ_LOTE, IE_TIPO_CONTRATACAO, IE_SEGMENTACAO_SIP)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIPLOIA_I2 ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(NR_SEQ_LOTE, IE_TIPO_CONTRATACAO, IE_SEGMENTACAO_SIP, SG_UF, NR_SEQ_ITEM_SIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIPLOIA_I3 ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(NR_SEQ_LOTE, NR_SEQ_ITEM_SIP, IE_SEGMENTACAO_SIP, IE_TIPO_CONTRATACAO, SG_UF, 
DT_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SIPLOIA_PK ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIPLOIA_PLSLSIP_FK_I ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIPLOIA_SIPITAS_FK_I ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL
(NR_SEQ_ITEM_SIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPLOIA_SIPITAS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sip_lote_item_assist_insert
before insert ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL for each row
declare

begin

select	nvl(ie_evento, 'S'),
	nvl(ie_benef_carencia, 'S'),
	nvl(ie_despesa, 'S'),
	nr_seq_apres
into	:new.ie_evento,
	:new.ie_benef_carencia,
	:new.ie_despesa,
	:new.nr_seq_apres
from	sip_item_assistencial
where	nr_sequencia	= :new.nr_seq_item_sip;

end;
/


CREATE OR REPLACE TRIGGER TASY.sip_lote_item_assist_atual
before update or delete ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL for each row
declare
ie_grava_log	varchar2(1);

begin

ie_grava_log := 'N';

-- se for a��o de delete ou se algum valor dos campos pertinentes a altera��o for modificado
if	(deleting) then
	ie_grava_log := 'S';

elsif	((nvl(:new.cd_classificacao_sip, 'A') != nvl(:old.cd_classificacao_sip, 'A')) or
	 (nvl(:new.dt_ocorrencia, sysdate) != nvl(:old.dt_ocorrencia, sysdate)) or
	 (nvl(:new.ie_benef_carencia, 'A') != nvl(:old.ie_benef_carencia, 'A')) or
	 (nvl(:new.ie_despesa, 'A') != nvl(:old.ie_despesa, 'A')) or
	 (nvl(:new.ie_evento, 'A') != nvl(:old.ie_evento, 'A')) or
	 (nvl(:new.ie_segmentacao_sip, 0) != nvl(:old.ie_segmentacao_sip, 0)) or
	 (nvl(:new.ie_tipo_contratacao, 'A') != nvl(:old.ie_tipo_contratacao, 'A')) or
	 (nvl(:new.nr_seq_apres, 0) != nvl(:old.nr_seq_apres, 0)) or
	 (nvl(:new.nr_seq_item_sip, 0) != nvl(:old.nr_seq_item_sip, 0)) or
	 (nvl(:new.nr_seq_lote, 0) != nvl(:old.nr_seq_lote, 0)) or
	 (nvl(:new.nr_seq_superior, 0) != nvl(:old.nr_seq_superior, 0)) or
	 (nvl(:new.qt_beneficiario, 0) != nvl(:old.qt_beneficiario, 0)) or
	 (nvl(:new.qt_evento, 0) != nvl(:old.qt_evento, 0)) or
	 (nvl(:new.sg_uf, 'A') != nvl(:old.sg_uf, 'A')) or
	 (nvl(:new.vl_despesa, 0) != nvl(:old.vl_despesa, 0))) then
	ie_grava_log := 'S';
end if;

-- grava o log
if	(ie_grava_log = 'S') then

	insert into sip_lote_item_assist_log
		( 	nr_sequencia, dt_atualizacao_nrec, nm_usuario_nrec,
			cd_classificacao_sip, dt_atualizacao_log, dt_ocorrencia,
			ie_benef_carencia, ie_despesa, ie_evento,
			ie_segmentacao_sip, ie_tipo_contratacao, nm_usuario_log,
			nr_seq_apres, nr_seq_item_sip, nr_seq_lote,
			nr_seq_superior, nr_sequencia_log, qt_beneficiario,
			qt_evento, sg_uf, vl_despesa)
	values	( 	sip_lote_item_assist_log_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado'),1,14),
			:old.cd_classificacao_sip, :old.dt_atualizacao, :old.dt_ocorrencia,
			:old.ie_benef_carencia, :old.ie_despesa, :old.ie_evento,
			:old.ie_segmentacao_sip, :old.ie_tipo_contratacao, :old.nm_usuario,
			:old.nr_seq_apres, :old.nr_seq_item_sip, :old.nr_seq_lote,
			:old.nr_seq_superior, :old.nr_sequencia, :old.qt_beneficiario,
			:old.qt_evento, :old.sg_uf, :old.vl_despesa);
end if;

end sip_lote_item_assist_atual;
/


CREATE OR REPLACE TRIGGER TASY.sip_lote_item_ass_atual
before insert or update ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL for each row
--Esta trigger foi criada pois na litoral eles cadastram os eventos odontol�gicos manualmente.
--E por este motivo precisa preencher a classifica��o se n�o o item n�o � agrupado  e geraria linha a mais no XML causando erro
--Prefirir n�o fazer o tratamento na SIP_LOTE_ITEM_ASSIST_ATUAL, pois nela h� tratamento para Delete e Update enquanto esta faz para Insert e  Update
declare
cd_classificacao_sip_w 		sip_lote_item_assistencial.cd_classificacao_sip%type;
begin

if 	(((:new.cd_classificacao_sip is null) or --insert
	  (:old.nr_seq_item_sip <> :new.nr_seq_item_sip))and --update
	 (:new.nr_seq_item_sip is not null))	then
	select	max(cd_classificacao)
	into	cd_classificacao_sip_w
	from	sip_item_assistencial
	where	nr_sequencia = :new.nr_seq_item_sip;

	:new.cd_classificacao_sip := cd_classificacao_sip_w;

end if;

end;
/


ALTER TABLE TASY.SIP_LOTE_ITEM_ASSISTENCIAL ADD (
  CONSTRAINT SIPLOIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIP_LOTE_ITEM_ASSISTENCIAL ADD (
  CONSTRAINT SIPLOIA_PLSLSIP_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_SIP (NR_SEQUENCIA),
  CONSTRAINT SIPLOIA_SIPITAS_FK 
 FOREIGN KEY (NR_SEQ_ITEM_SIP) 
 REFERENCES TASY.SIP_ITEM_ASSISTENCIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIP_LOTE_ITEM_ASSISTENCIAL TO NIVEL_1;

