ALTER TABLE TASY.W_GRUPO_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_GRUPO_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_GRUPO_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_CRACHA            NUMBER(15),
  CD_GRUPO             NUMBER(5),
  CD_ENTRADA_SAIDA     VARCHAR2(15 BYTE),
  QT_ACESSO            NUMBER(5),
  DT_EVENTO            DATE,
  NR_SEQ_ATEND         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WGRUACE_ATEVISG_FK_I ON TASY.W_GRUPO_ACESSO
(NR_SEQ_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WGRUACE_ATEVISG_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WGRUACE_PK ON TASY.W_GRUPO_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WGRUACE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.W_GRUPO_ACESSO_UPDATE
before update ON TASY.W_GRUPO_ACESSO for each row
declare

cont_w			number(10,0);
dt_entrada_w		date;
nr_atendimento_w	number(10,0);
nr_sequencia_w		number(10,0);
dt_acompanhante_w	date;
nr_seq_atend_visita_w	number(10,0);

begin

if	(:new.dt_evento <> nvl(:old.dt_evento,:new.dt_evento+1)) and
	(:new.dt_evento is not null) then
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	controle_acesso_visita
	where	cd_grupo = :new.cd_grupo;

	select	max(dt_entrada),
		max(nr_atendimento),
		max(dt_acompanhante),
		max(nr_seq_atend_visita)
	into	dt_entrada_w,
		nr_atendimento_w,
		dt_acompanhante_w,
		nr_seq_atend_visita_w
	from	atendimento_visita_grupo
	where	nr_sequencia	= :new.nr_seq_atend;

	if	(dt_entrada_w is null) then
		update	atendimento_visita_grupo
		set	dt_entrada	= :new.dt_evento
		where	nr_sequencia	= :new.nr_seq_atend;
	else
		update	atendimento_visita_grupo
		set	dt_saida	= :new.dt_evento
		where	nr_sequencia	= :new.nr_seq_atend;
	end if;

	if	(nr_seq_atend_visita_w is not null) then
		select	count(*)
		into	cont_w
		from	atendimento_visita_grupo
		where	nvl(nr_seq_atend_visita,0) = nvl(nr_seq_atend_visita_w,0)
		and	dt_saida is null;

		if	(cont_w = 0) then
			update	atendimento_visita
			set	dt_saida	= :new.dt_evento
			where	nr_sequencia	= nr_seq_atend_visita_w;
		end if;

	elsif	(dt_acompanhante_w is not null) then
		select	count(*)
		into	cont_w
		from	atendimento_visita_grupo
		where	nr_atendimento	= nr_atendimento_w
		and	dt_acompanhante	= dt_acompanhante_w
		and	dt_saida is null;

		if	(cont_w = 0) then
			update	atendimento_acompanhante
			set	dt_saida	= :new.dt_evento
			where	nr_atendimento	= nr_atendimento_w
			and	dt_acompanhante	= dt_acompanhante_w
			and	dt_saida	is null;
		end if;

	elsif 	(nr_atendimento_w is not null) then
		select	count(*)
		into	cont_w
		from	atendimento_visita_grupo
		where	nr_atendimento	= nr_atendimento_w
		and	dt_saida 	is null;

		if	(cont_w = 0) then
			update	atendimento_visita
			set	dt_saida	= :new.dt_evento
			where	nr_atendimento	= nr_atendimento_w
			and	dt_saida	is null;
		end if;

	end if;

end if;

end;
/


ALTER TABLE TASY.W_GRUPO_ACESSO ADD (
  CONSTRAINT WGRUACE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_GRUPO_ACESSO ADD (
  CONSTRAINT WGRUACE_ATEVISG_FK 
 FOREIGN KEY (NR_SEQ_ATEND) 
 REFERENCES TASY.ATENDIMENTO_VISITA_GRUPO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_GRUPO_ACESSO TO NIVEL_1;

