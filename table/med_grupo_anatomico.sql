ALTER TABLE TASY.MED_GRUPO_ANATOMICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_GRUPO_ANATOMICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_GRUPO_ANATOMICO
(
  CD_GRUPO_ANATOMICO   VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_GRUPO_ANATOMICO   VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MEDGRAN_PK ON TASY.MED_GRUPO_ANATOMICO
(CD_GRUPO_ANATOMICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDGRAN_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_GRUPO_ANATOMICO ADD (
  CONSTRAINT MEDGRAN_PK
 PRIMARY KEY
 (CD_GRUPO_ANATOMICO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MED_GRUPO_ANATOMICO TO NIVEL_1;

