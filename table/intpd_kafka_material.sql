ALTER TABLE TASY.INTPD_KAFKA_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_KAFKA_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_KAFKA_MATERIAL
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  IE_EVENTO           VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  DS_JSON             CLOB,
  NR_SEQ_FILA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INTKTPMAT_PK ON TASY.INTPD_KAFKA_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTPD_KAFKA_MATERIAL ADD (
  CONSTRAINT INTKTPMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTPD_KAFKA_MATERIAL TO NIVEL_1;

