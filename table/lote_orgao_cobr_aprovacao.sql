ALTER TABLE TASY.LOTE_ORGAO_COBR_APROVACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ORGAO_COBR_APROVACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ORGAO_COBR_APROVACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE          NUMBER(5)                NOT NULL,
  NM_USUARIO_APROV     VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_APROVACAO         DATE,
  QT_NIVEL_LIBERACAO   NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOTORGAPR_LOTORGCO_FK_I ON TASY.LOTE_ORGAO_COBR_APROVACAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOTORGAPR_PK ON TASY.LOTE_ORGAO_COBR_APROVACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTORGAPR_USUARIO_FK_I ON TASY.LOTE_ORGAO_COBR_APROVACAO
(NM_USUARIO_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOTE_ORGAO_COBR_APROVACAO ADD (
  CONSTRAINT LOTORGAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ORGAO_COBR_APROVACAO ADD (
  CONSTRAINT LOTORGAPR_LOTORGCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_ORGAO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT LOTORGAPR_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_APROV) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.LOTE_ORGAO_COBR_APROVACAO TO NIVEL_1;

