ALTER TABLE TASY.CUR_EVOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CUR_EVOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CUR_EVOLUCAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_FERIDA              NUMBER(10)         NOT NULL,
  DT_EVOLUCAO                DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_EVOLUCAO_CLINICA        VARCHAR2(3 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DS_EVOLUCAO                LONG               NOT NULL,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CUREVOL_ATEPACI_FK_I ON TASY.CUR_EVOLUCAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUREVOL_CURFERI_FK_I ON TASY.CUR_EVOLUCAO
(NR_SEQ_FERIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CUREVOL_PK ON TASY.CUR_EVOLUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUREVOL_PK
  MONITORING USAGE;


CREATE INDEX TASY.CUREVOL_TASASDI_FK_I ON TASY.CUR_EVOLUCAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUREVOL_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CUREVOL_TASASDI_FK2_I ON TASY.CUR_EVOLUCAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUREVOL_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CUREVOL_TIPEVOL_FK_I ON TASY.CUR_EVOLUCAO
(IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUREVOL_TIPEVOL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cur_evolucao_pend_atual
after insert or update ON TASY.CUR_EVOLUCAO for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(Obter_Valor_Param_Usuario(0,37, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') then

if (:new.dt_liberacao is null) then
		ie_tipo_w := 'CE';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCE';
	end if;

	if	(ie_tipo_w	is not null) or
		(ie_tipo_w <> '' )then

		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;


<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.Cur_Evolucao_Insert
AFTER INSERT ON TASY.CUR_EVOLUCAO FOR EACH ROW
DECLARE

ie_lacto_w		varchar2(1);

BEGIN

if	(:new.dt_evolucao > sysdate) and
	(:new.nr_atendimento is not null) then
	--A data da evolu��o n�o pode ser maior que a data atual.#@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(262188);
end if;

begin
gerar_lancamento_automatico(:new.nr_atendimento, null, 545, :new.nm_usuario, :new.nr_sequencia, :new.dt_evolucao, :new.ie_evolucao_clinica, null, null, null);
exception
	when others then
	ie_lacto_w:= 'N';
end;


END;
/


CREATE OR REPLACE TRIGGER TASY.CUR_EVOLUCAO_ATUAL
before insert or update ON TASY.CUR_EVOLUCAO for each row
declare


begin
if	(nvl(:old.DT_EVOLUCAO,sysdate+10) <> :new.DT_EVOLUCAO) and
	(:new.DT_EVOLUCAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_EVOLUCAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CUR_EVOLUCAO_DELETE
after delete ON TASY.CUR_EVOLUCAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	nr_seq_item_pront = 53
and		nr_seq_registro  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.CUR_EVOLUCAO ADD (
  CONSTRAINT CUREVOL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CUR_EVOLUCAO ADD (
  CONSTRAINT CUREVOL_CURFERI_FK 
 FOREIGN KEY (NR_SEQ_FERIDA) 
 REFERENCES TASY.CUR_FERIDA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CUREVOL_TIPEVOL_FK 
 FOREIGN KEY (IE_EVOLUCAO_CLINICA) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO),
  CONSTRAINT CUREVOL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CUREVOL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CUREVOL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.CUR_EVOLUCAO TO NIVEL_1;

