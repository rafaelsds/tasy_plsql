ALTER TABLE TASY.IMPNPAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMPNPAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMPNPAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CARGA         NUMBER(10)               NOT NULL,
  DT_INICIO_PROC       DATE,
  DT_FIM_PROC          DATE,
  NR_SEQ_CARGA_ARQ     NUMBER(10)               NOT NULL,
  MANDT                VARCHAR2(4 BYTE),
  PATNR                VARCHAR2(10 BYTE)        NOT NULL,
  EINRI                VARCHAR2(5 BYTE),
  GSCHL                VARCHAR2(1 BYTE),
  NNAME                VARCHAR2(31 BYTE),
  NNAMS                VARCHAR2(81 BYTE),
  VNAME                VARCHAR2(31 BYTE),
  VNAMS                VARCHAR2(81 BYTE),
  TITEL                VARCHAR2(16 BYTE),
  NAMZU                VARCHAR2(16 BYTE),
  VORSW                VARCHAR2(16 BYTE),
  NAME2                VARCHAR2(31 BYTE),
  GBDAT                VARCHAR2(30 BYTE),
  GBNAM                VARCHAR2(31 BYTE),
  GBNAS                VARCHAR2(81 BYTE),
  GLAND                VARCHAR2(4 BYTE),
  TODKZ                VARCHAR2(1 BYTE),
  TODDT                VARCHAR2(30 BYTE),
  TODZT                VARCHAR2(30 BYTE),
  TODDB                VARCHAR2(30 BYTE),
  TODZB                VARCHAR2(30 BYTE),
  TODUR                VARCHAR2(4 BYTE),
  ANRED                VARCHAR2(3 BYTE),
  FAMST                VARCHAR2(2 BYTE),
  KONFE                VARCHAR2(4 BYTE),
  NATIO                VARCHAR2(4 BYTE),
  SPRAS                VARCHAR2(2 BYTE),
  LAND                 VARCHAR2(4 BYTE),
  PSTLZ                VARCHAR2(11 BYTE),
  ORT                  VARCHAR2(26 BYTE),
  ORT2                 VARCHAR2(26 BYTE),
  STRAS                VARCHAR2(31 BYTE),
  BLAND                VARCHAR2(4 BYTE),
  GEBIE                VARCHAR2(10 BYTE),
  TELF1                VARCHAR2(17 BYTE),
  SPEND                VARCHAR2(2 BYTE),
  SPENT                VARCHAR2(51 BYTE),
  SPELT                VARCHAR2(2 BYTE),
  VIPKZ                VARCHAR2(2 BYTE),
  ARCHV                VARCHAR2(2 BYTE),
  STATU                VARCHAR2(2 BYTE),
  EXTNR                VARCHAR2(21 BYTE),
  NOTAN                VARCHAR2(2 BYTE),
  KRZAN                VARCHAR2(2 BYTE),
  RFPAT                VARCHAR2(11 BYTE),
  RFZIF                VARCHAR2(2 BYTE),
  BERUF                VARCHAR2(26 BYTE),
  AGNUM                VARCHAR2(11 BYTE),
  AGNAM                VARCHAR2(31 BYTE),
  AGLAN                VARCHAR2(4 BYTE),
  AGPLZ                VARCHAR2(11 BYTE),
  AGORT                VARCHAR2(26 BYTE),
  AGSTR                VARCHAR2(31 BYTE),
  AGTEL                VARCHAR2(17 BYTE),
  RVNUM                VARCHAR2(21 BYTE),
  ANNA1                VARCHAR2(31 BYTE),
  ANVN1                VARCHAR2(31 BYTE),
  ANLA1                VARCHAR2(4 BYTE),
  ANPL1                VARCHAR2(11 BYTE),
  ANOR1                VARCHAR2(26 BYTE),
  ANST1                VARCHAR2(31 BYTE),
  ANTE1                VARCHAR2(17 BYTE),
  ANVV1                VARCHAR2(2 BYTE),
  VMKZ1                VARCHAR2(2 BYTE),
  ANNA2                VARCHAR2(31 BYTE),
  ANVN2                VARCHAR2(31 BYTE),
  ANLA2                VARCHAR2(4 BYTE),
  ANPL2                VARCHAR2(11 BYTE),
  ANOR2                VARCHAR2(26 BYTE),
  ANST2                VARCHAR2(31 BYTE),
  ANTE2                VARCHAR2(17 BYTE),
  ANVV2                VARCHAR2(2 BYTE),
  VMKZ2                VARCHAR2(2 BYTE),
  KZTXT                VARCHAR2(51 BYTE),
  LGTXT                VARCHAR2(2 BYTE),
  BEGDT                VARCHAR2(30 BYTE),
  ENDDT                VARCHAR2(30 BYTE),
  HISTK                VARCHAR2(2 BYTE),
  RISKF                VARCHAR2(2 BYTE),
  TESTP                VARCHAR2(2 BYTE),
  PZIFF                VARCHAR2(2 BYTE),
  ERDAT                VARCHAR2(30 BYTE),
  ERUSR                VARCHAR2(13 BYTE),
  UPDAT                VARCHAR2(30 BYTE),
  UPUSR                VARCHAR2(13 BYTE),
  STORN                VARCHAR2(2 BYTE),
  STUSR                VARCHAR2(13 BYTE),
  STDAT                VARCHAR2(30 BYTE),
  GBORT                VARCHAR2(26 BYTE),
  HARNR                VARCHAR2(11 BYTE),
  EARNR                VARCHAR2(11 BYTE),
  UARNR                VARCHAR2(11 BYTE),
  RACE                 VARCHAR2(3 BYTE),
  RESID                VARCHAR2(2 BYTE),
  PASSTY               VARCHAR2(3 BYTE),
  PASSNR               VARCHAR2(16 BYTE),
  ADRNR                VARCHAR2(11 BYTE),
  ADROB                VARCHAR2(5 BYTE),
  ADNAG                VARCHAR2(11 BYTE),
  ADOAG                VARCHAR2(5 BYTE),
  ADNN1                VARCHAR2(11 BYTE),
  ADON1                VARCHAR2(5 BYTE),
  ADNN2                VARCHAR2(11 BYTE),
  ADON2                VARCHAR2(5 BYTE),
  ANEX1                VARCHAR2(21 BYTE),
  ANEX2                VARCHAR2(21 BYTE),
  ADRN2                VARCHAR2(11 BYTE),
  ADRO2                VARCHAR2(5 BYTE),
  INACT                VARCHAR2(2 BYTE),
  USER1                VARCHAR2(21 BYTE),
  USER2                VARCHAR2(21 BYTE),
  USER3                VARCHAR2(30 BYTE),
  USER4                VARCHAR2(30 BYTE),
  USER5                VARCHAR2(51 BYTE),
  USER6                VARCHAR2(2 BYTE),
  EXTAUFG              VARCHAR2(2 BYTE),
  EXTAUFGA             VARCHAR2(2 BYTE),
  EMPSC                VARCHAR2(30 BYTE),
  UNKNOWN_GBDAT        VARCHAR2(2 BYTE),
  MIG_DONE             VARCHAR2(2 BYTE),
  INSID                VARCHAR2(11 BYTE),
  ISTAT_BPL            VARCHAR2(7 BYTE),
  TAXNUM               VARCHAR2(21 BYTE),
  TAXNUM_IND           VARCHAR2(2 BYTE),
  STP                  VARCHAR2(17 BYTE),
  STP_ENDDT            VARCHAR2(30 BYTE),
  CONS_PERSDATA        VARCHAR2(2 BYTE),
  NARNR_S              VARCHAR2(11 BYTE),
  NARNR_A              VARCHAR2(11 BYTE),
  VNAME_LONG           VARCHAR2(71 BYTE),
  NNAME_LONG           VARCHAR2(71 BYTE),
  GBNAM_LONG           VARCHAR2(71 BYTE),
  TITLE_ACA2           VARCHAR2(5 BYTE),
  SEX_SPECIAL          VARCHAR2(2 BYTE),
  DS_CHAVE_TASY        VARCHAR2(255 BYTE),
  IE_STATUS            VARCHAR2(10 BYTE),
  NR_LINHA             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPNPAT_GERCARI_FK_I ON TASY.IMPNPAT
(NR_SEQ_CARGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPNPAT_GERCGARQ_FK_I ON TASY.IMPNPAT
(NR_SEQ_CARGA_ARQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMPNPAT_PK ON TASY.IMPNPAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMPNPAT ADD (
  CONSTRAINT IMPNPAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMPNPAT ADD (
  CONSTRAINT IMPNPAT_GERCARI_FK 
 FOREIGN KEY (NR_SEQ_CARGA) 
 REFERENCES TASY.GER_CARGA_INICIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT IMPNPAT_GERCGARQ_FK 
 FOREIGN KEY (NR_SEQ_CARGA_ARQ) 
 REFERENCES TASY.GER_CARGA_ARQ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMPNPAT TO NIVEL_1;

