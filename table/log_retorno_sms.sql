ALTER TABLE TASY.LOG_RETORNO_SMS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_RETORNO_SMS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_RETORNO_SMS
(
  NR_CELULAR          VARCHAR2(50 BYTE)         NOT NULL,
  DS_RESPOSTA         VARCHAR2(512 BYTE)        NOT NULL,
  DT_RESPOSTA         DATE                      NOT NULL,
  IE_PROCESSADO       VARCHAR2(1 BYTE)          NOT NULL,
  NR_SEQUENCIA        NUMBER(10),
  NR_SEQ_AGEINT       NUMBER(10),
  DT_AGENDAMENTO      DATE,
  DT_CONTATO          DATE,
  DS_OBS_CONTATO      VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA_CONS  NUMBER(10),
  NM_SMS_SENT_USER    VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE),
  NR_SEQ_AGENDA_PAC   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LORESMS_I1 ON TASY.LOG_RETORNO_SMS
(IE_PROCESSADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LORESMS_I1
  MONITORING USAGE;


CREATE INDEX TASY.LORESMS_I2 ON TASY.LOG_RETORNO_SMS
(NR_SEQ_AGEINT, DT_AGENDAMENTO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LORESMS_I3 ON TASY.LOG_RETORNO_SMS
(NR_CELULAR, DT_RESPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LORESMS_PK ON TASY.LOG_RETORNO_SMS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LORESMS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.log_retorno_sms_before_ins
before insert ON TASY.LOG_RETORNO_SMS for each row
declare

	nr_seq_agenda_w			agenda_consulta.nr_sequencia%type;
	cd_pessoa_fisica_w		varchar2(20);
	nr_seq_forma_confirmacao_w	number(10);
	nr_celular_w			varchar2(50);
	ie_tipo_retorno_w		varchar2(1);
	nr_atendimento_w                number(10);
	ds_texto_w			varchar2(5000);
	id_sms_w			number(10);
	ds_remetente_p			varchar2(255);
	nm_pessoa_fisica_w		varchar2(255);
	dt_agenda_w			date;
	dt_min_exame_w			date;
	dt_min_consulta_w		date;
	dt_primeiro_hor_w		varchar2(100);
	ds_remetente_sms_w		varchar2(255);
	ds_item_agenda_w		varchar2(255);
	cd_agenda_w			number(20);
	cd_estab_logado_w		number(10);
	nr_seq_regra_sms_w              number(10);
	cd_estab_agenda_w		number(10);
	nr_ddd_celular_w		varchar2(50);
	nr_telefone_celular_w		varchar2(50);
	ds_mensagem_sms_w               regra_sms_alta.ds_mensagem%type;
	ie_consist_destinatario_w	varchar2(1);
	nr_tamanho_mensagem_w		number(5)	:= 0;
	ie_retorno_DDI_w		varchar2(1);
	nr_celular_new_w		varchar(50);

begin

	cd_pessoa_fisica_w	:= :new.cd_pessoa_fisica;
	nr_seq_agenda_w		:= nvl(:new.nr_seq_agenda_pac, :new.nr_seq_agenda_cons);

	SELECT MAX(cd_estabelecimento), MAX(cd_agenda)
	INTO cd_estab_agenda_w, cd_agenda_w
	FROM (
		SELECT a.cd_estabelecimento, a.cd_agenda
		FROM agenda a,
		agenda_paciente b
		WHERE a.cd_agenda = b.cd_agenda
		AND ((NVL(:new.nr_seq_agenda_pac, 0) = 0 AND 1 = 2) OR b.nr_sequencia = :new.nr_seq_agenda_pac)

		UNION

		SELECT a.cd_estabelecimento, a.cd_agenda
		FROM agenda a,
		agenda_consulta b
		WHERE a.cd_agenda = b.cd_agenda
		AND ((NVL(:new.nr_seq_agenda_cons, 0) > 0 AND 1 = 2) OR b.nr_sequencia = :new.nr_seq_agenda_cons));

	cd_estab_logado_w	:= nvl(obter_estabelecimento_ativo,1);
	ds_remetente_p		:= Obter_Valor_Param_Usuario(0, 63, obter_perfil_ativo, obter_usuario_ativo, nvl(cd_estab_agenda_w,cd_estab_logado_w));
	Obter_param_Usuario(821, 501, obter_perfil_ativo, obter_usuario_ativo, nvl(cd_estab_agenda_w,cd_estab_logado_w), ie_retorno_DDI_w);
	Obter_Param_Usuario(0, 214, obter_perfil_ativo, obter_usuario_ativo, nvl(cd_estab_agenda_w,cd_estab_logado_w), ie_consist_destinatario_w);

	nr_celular_w		:= trim(:new.nr_celular);
	nr_celular_new_w	:= nr_celular_w;
	nr_ddd_celular_w	:= trim(substr(nr_celular_w,3,2));
	nr_telefone_celular_w	:= trim(substr(nr_celular_w,5,50));

	if (nvl(ie_retorno_DDI_w,'S') = 'N') then

		if	(substr(nr_celular_w,1,2) = '55') then
			select 	substr(:new.nr_celular,3,12)
			into 	nr_celular_w
			from 	dual;
		end if;

		nr_ddd_celular_w	:= trim(substr(nr_celular_w,1,2));
		nr_telefone_celular_w	:= trim(substr(nr_celular_w,3,50));

	else

		if	(substr(nr_celular_w,1,2) <> '55') then
			nr_celular_w :=  '55'||:new.nr_celular;
			nr_ddd_celular_w	:= trim(substr(nr_celular_w,3,2));
			nr_telefone_celular_w	:= trim(substr(nr_celular_w,5,50));
		end if;

	end if;

	if (ie_consist_destinatario_w = 'I') then
		nr_celular_w := nr_celular_new_w;
	else
		if	((ie_consist_destinatario_w <> 'N')
			and (substr(nr_celular_new_w,1,2) = '55')
			and (Length(nr_celular_new_w) > 11))	then
			nr_celular_w := substr(nr_celular_new_w,3,length(nr_celular_new_w));
		else
			begin
				if	(((substr(nr_celular_new_w,1,3) = '055')
					OR (substr(nr_celular_new_w,1,3) = '+55')
					OR (substr(nr_celular_new_w,1,2) = '55'))
					and (Length(nr_celular_new_w) > 11)) then

					if	((substr(nr_celular_new_w,1,3) = '055')
						OR (substr(nr_celular_new_w,1,3) = '+55')) then
						nr_celular_w := substr(nr_celular_new_w, 4, Length(nr_celular_new_w));
					else
						nr_celular_w := substr(nr_celular_new_w, 3, Length(nr_celular_new_w));
					end if;
				end if;
			end;
		end if;

		nr_telefone_celular_w	:= trim(substr(nr_celular_w,3,50));
	end if;

	if (to_char(:new.dt_resposta,'yyyy')) <> (to_char(SYSDATE,'yyyy')) then
		if ((to_char(:new.dt_resposta,'hh24:mi:ss') = '00:00:00') or (to_char(:new.dt_resposta,'hh24:mi:ss') is null)) then
			:new.dt_resposta := to_date(to_char(:new.dt_resposta,'dd/mm/')||to_char(SYSDATE,'yyyy')||' '||to_char(sysdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		else
			:new.dt_resposta := to_date(to_char(:new.dt_resposta,'dd/mm/')||to_char(SYSDATE,'yyyy')||' '||to_char(:new.dt_resposta,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		end if;
	end if;

	if	(cd_pessoa_fisica_w is not null) then

		select  max(a.nr_atendimento),
			max(b.nr_seq_regra_envio)
		into    nr_atendimento_w,
			nr_seq_regra_sms_w
		from    atendimento_paciente a,
			atend_regra_sms b
		where   a.nr_atendimento	= b.nr_atendimento
		and     a.cd_pessoa_fisica	= cd_pessoa_fisica_w
		and     b.dt_atualizacao_nrec	> sysdate -30
		and     b.ie_envio_retorno	= 'E'
		and     2	> (	select  count(*)
					from    atend_regra_sms c
					where   c.nr_atendimento = a.nr_atendimento
					and     c.ie_envio_retorno = 'R');

		select	VALIDATE_SMS_RESPONSE_TEXT(:new.ds_resposta)
		into	ie_tipo_retorno_w
		from	dual;

		if	((nvl(nr_atendimento_w,0) > 0) and (:new.ds_resposta is not null)) then
			insert into atend_regra_sms (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				ds_mensagem,
				nr_seq_regra_envio,
				nr_atendimento,
				ie_envio_retorno
			) values (
				atend_regra_sms_seq.nextval,
				sysdate,
				'Tasy',
				sysdate,
				'Tasy',
				substr(:new.ds_resposta,1,140),
				nr_seq_regra_sms_w,
				nr_atendimento_w,
				'R'
			);

			select  max(ds_mensagem)
			into    ds_mensagem_sms_w
			from    REGRA_SMS_ALTA_RETORNO
			where   nr_seq_regra = nr_seq_regra_sms_w
			and	ie_tipo_resposta	= ie_tipo_retorno_w;

			if      (ds_mensagem_sms_w is not null) then
				if (ds_remetente_p is null) then
					ds_remetente_p := substr(obter_nome_estabelecimento(cd_estab_logado_w),1,255);
				end if;

				ds_mensagem_sms_w	:= substr(replace_macro(ds_mensagem_sms_w, '@paciente', obter_nome_pf(cd_pessoa_fisica_w)),1,140);
				enviar_sms_atend(ds_remetente_p, nr_ddd_celular_w||nr_telefone_celular_w, ds_mensagem_sms_w, nr_seq_regra_sms_w, nr_atendimento_w, 'E', 'Tasy', 'N');
			end if;
		end if;

		select	obter_primeiro_nome(obter_nome_pf(cd_pessoa_fisica_w))
		into	nm_pessoa_fisica_w
		from	dual;

		IF (nvl(:new.nr_seq_agenda_cons, 0) > 0) THEN

			SELECT	nm
			INTO	:new.nm_sms_sent_user
			FROM	(SELECT	NVL(NVL(MAX(b.nm_usuario), MAX(a.nm_usuario)), 'Tasy') nm
				FROM	agenda_consulta a,
					log_envio_sms	b
				WHERE	a.nr_sequencia	= b.nr_seq_agenda
				AND	somente_numero(b.nr_telefone)	= somente_numero(nr_celular_w)
				AND	b.cd_agenda	= a.cd_agenda
				AND	a.nr_sequencia	= :new.nr_seq_agenda_cons
				AND	b.dt_envio	<= sysdate
				AND	'TASY'		!= upper(b.nm_usuario)
				ORDER BY	b.dt_atualizacao DESC)
			WHERE	rownum	= 1;

		END IF;

		select 	nvl(max(ie_tipo_retorno),'O')
		into	ie_tipo_retorno_w
		from	ageint_texto_retorno_sms
		where	trim(upper(ds_texto)) = trim(upper(:new.ds_resposta));

		--and	cd_estabelecimento    = cd_estab_logado_w; -- Patricia solicitou que considere apenas do estabelecimento

		select 	nvl(max(ds_texto),'')
		into	ds_texto_w
		from	ageint_texto_confirm_sms
		where	ie_tipo_retorno    = ie_tipo_retorno_w
		and	cd_estabelecimento = nvl(cd_estab_agenda_w,cd_estab_logado_w); -- Patricia solicitou que considere apenas do estabelecimento

		ds_remetente_p := nvl(Obter_Valor_Param_Usuario(0, 63, obter_perfil_ativo, obter_usuario_ativo, nvl(cd_estab_agenda_w,cd_estab_logado_w)),'Tasy');

		begin

			select 	max(substr(to_char(nvl(Obter_Horario_item_Ageint(nr_seq_agenda_cons, nr_Seq_Agenda_exame,nr_sequencia),qt_obter_horario_agendado(nr_sequencia)),'dd/mm/yyyy hh24:mi'),1,20))
			into	dt_agenda_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int = :new.nr_seq_ageint;

		exception
		when others then
			dt_agenda_w	:= '';
		end;

		begin
			select	max(substr(nvl(obter_desc_espec_medica(b.cd_especialidade), obter_nome_medico_combo_agcons(b.cd_estabelecimento, cd_agenda_w, 3, 'N')),1,255))
			into	ds_item_agenda_w
			from	agenda_integrada_item b
			where	b.nr_seq_agenda_int = :new.nr_seq_ageint;
		exception
		when others then
			ds_item_agenda_w := '';
		end;

		begin
			select	max(substr(a.ds_remetente_sms,1,255))
			into	ds_remetente_sms_w
			from	parametro_agenda a
			where	a.cd_estabelecimento	= (	select	max(x.cd_estabelecimento)
								from	agenda_integrada_item x
								where	x.nr_seq_agenda_int = :new.nr_seq_ageint);
		exception
		when others then
			ds_remetente_sms_w := '';
		end;

		begin

			select	min(a.hr_inicio)
			into	dt_min_exame_w
			from	agenda_paciente a,
				agenda_integrada_item b
			where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	a.nr_sequencia 	   	= b.nr_seq_agenda_exame
			and	trunc(a.hr_inicio)	>= trunc(sysdate)
			and	nvl(b.cd_estabelecimento, nvl(cd_estab_agenda_w,cd_estab_logado_w)) = nvl(cd_estab_agenda_w,cd_estab_logado_w)
			and	a.ie_status_agenda	not in ('C','B','L');

			select	min(a.dt_agenda)
			into	dt_min_consulta_w
			from	agenda_consulta a,
				agenda_integrada_item b
			where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	a.nr_sequencia		= b.nr_seq_agenda_cons
			and	nvl(b.cd_estabelecimento, nvl(cd_estab_agenda_w,cd_estab_logado_w)) = nvl(cd_estab_agenda_w,cd_estab_logado_w)
			and	trunc(a.dt_agenda)	>= trunc(sysdate)
			and	a.ie_status_agenda	not in ('C','B','L');

			if	(dt_min_exame_w is not null) and
				(dt_min_consulta_w is not null) then
				if	(dt_min_exame_w	< dt_min_consulta_w) then
					dt_primeiro_hor_w	:= to_char(dt_min_exame_w, 'dd/mm/yyyy hh24:mi');
				else
					dt_primeiro_hor_w	:= to_char(dt_min_consulta_w, 'dd/mm/yyyy hh24:mi');
				end if;

			elsif	(dt_min_exame_w is null) then
				dt_primeiro_hor_w	:= to_char(dt_min_consulta_w, 'dd/mm/yyyy hh24:mi');
			elsif	(dt_min_consulta_w is null) then
				dt_primeiro_hor_w	:= to_char(dt_min_exame_w, 'dd/mm/yyyy hh24:mi');
			end if;

		exception
		when others then
			dt_primeiro_hor_w := '';
		end;

		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@paciente', nm_pessoa_fisica_w),1,5000);
		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@protocoloagenda', :new.nr_seq_ageint),1,5000);
		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@horario', dt_agenda_w),1,5000);
		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@item', ds_item_agenda_w),1,5000);
		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@primeiro_horario', dt_primeiro_hor_w),1,5000);
		ds_texto_w	:= substr(replace_macro(ds_texto_w, '@estab', ds_remetente_sms_w),1,5000);

		if	(ds_remetente_p is not null) and
			(to_char(nr_celular_w) is not null) and
			(ds_texto_w is not null)	then

			select 	length(ds_texto_w)
			into	nr_tamanho_mensagem_w
			from 	dual;

			insert into log_envio_sms (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				dt_envio,
				cd_agenda,
				nr_seq_agenda,
				nr_telefone,
				ds_mensagem,
				id_sms,
				nr_seq_ageint
			) values (
				log_envio_sms_seq.nextval,
				sysdate,
				'Tasy',
				sysdate,
				'Tasy',
				sysdate,
				null,
				nr_seq_agenda_w,
				nvl(nr_celular_w, wheb_mensagem_pck.get_texto(793111)),
				substr(ds_texto_w,1,nr_tamanho_mensagem_w),
				1,
				:new.nr_seq_ageint
			);

			wheb_usuario_pck.set_cd_estabelecimento(nvl(cd_estab_agenda_w,cd_estab_logado_w));
			wheb_sms.enviar_sms(ds_remetente_p, to_char(nr_celular_w), substr(ds_texto_w,1,nr_tamanho_mensagem_w), obter_usuario_ativo,id_sms_w);
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.log_retorno_sms_after_ins
after insert ON TASY.LOG_RETORNO_SMS for each row
declare

nr_seq_agenda_w			number(10);
nr_seq_agenda_pac_w		number(10);
cd_pessoa_fisica_w		varchar2(20);
nr_seq_forma_confirmacao_w		number(10);
cd_estabelecimento_w		number(10);
nr_celular_w			varchar2(50);
nr_seq_ageint_w			number(10);

nr_ddd_celular_w			varchar2(50);
nr_telefone_celular_w		varchar2(50);
ie_retorno_canc_w			varchar2(1);
qt_hor_cancel_w			number(10,0);

cd_agenda_w			agenda_consulta.cd_agenda%type;
dt_agenda_w			date;
ie_classif_agenda_w agenda_consulta.ie_classif_agenda%type;
ie_tipo_retorno_w		varchar2(1);
qt_dias_confirmacao_w	ageint_texto_confirm_sms.qt_dias_confirmacao%type;
qt_dias_confirmacao_ww	ageint_texto_confirm_sms.qt_dias_confirmacao%type;
ds_motivo_cancelamento_w ageint_texto_confirm_sms.ds_motivo_cancelamento%type;
ie_altera_status_confirmada_w	parametro_agenda.ie_altera_status_confirmada%TYPE;
ie_confirm_ret_sms_w				parametro_agenda.ie_confirm_ret_sms%type;
ie_status_confirmado_w			VARCHAR2(2) := 'CN';

cursor c01 is
	select	nr_sequencia,
			cd_agenda,
			dt_agenda,
			ie_classif_agenda
	from	agenda_consulta
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		ie_status_agenda = 'N'
	and		dt_confirmacao is null
	and		dt_agenda >= sysdate
	and 	((qt_dias_confirmacao_w = 0) or (trunc(dt_agenda) <= trunc(sysdate + qt_dias_confirmacao_w)))
	order 	by nr_sequencia;

Cursor C02 is
	select	nr_sequencia,
			cd_agenda,
			hr_inicio
	from	agenda_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and		ie_status_agenda = 'N'
	and		dt_confirmacao is null
	and		hr_inicio >= sysdate
	and 	((qt_dias_confirmacao_w = 0) or (trunc(hr_inicio) <= trunc(sysdate + qt_dias_confirmacao_w)))
	order 	by nr_sequencia;

BEGIN

  wheb_usuario_pck.set_ie_commit('N');

	select 	nvl(max(ie_tipo_retorno),'O')
	into	ie_tipo_retorno_w
	from	ageint_texto_retorno_sms
	where	trim(upper(ds_texto)) = trim(upper(:new.ds_resposta));


	select 	nvl(max(qt_dias_confirmacao),0) , max(ds_motivo_cancelamento)
	into	qt_dias_confirmacao_w,
  ds_motivo_cancelamento_w
	from	ageint_texto_confirm_sms
	where	ie_tipo_retorno    = ie_tipo_retorno_w
	and		cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

	if (nvl(qt_dias_confirmacao_w,0) = 0) then
		qt_dias_confirmacao_ww := 30;
	else
		qt_dias_confirmacao_ww := qt_dias_confirmacao_w+1;
	end if;

if	(instr(upper(:new.ds_resposta), 'SIM') > 0) or
	(obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S'
	or obter_tipo_resposta_sms(:new.ds_resposta) = 'N') then

	nr_celular_w := :new.nr_celular;

	if	(nvl(pkg_i18n.get_user_locale, 'pt_BR')	 <> 'en_AU') then
		if	(substr(nr_celular_w,1,2) <> '55') then
			nr_celular_w :=  '55'||:new.nr_celular;
		end if;
	end if;

	nr_ddd_celular_w		:= trim(substr(nr_celular_w,3,2));
	nr_telefone_celular_w	:= trim(substr(nr_celular_w,5,50));

	begin
		select	cd_pessoa_fisica,
				cd_estabelecimento
		into	cd_pessoa_fisica_w,
				cd_estabelecimento_w
		from	pessoa_fisica a
		where	nr_telefone_celular	= nr_celular_w
		and 	dt_obito is null
		and	(exists (select 1
				from agenda_consulta x
				where a.cd_pessoa_fisica = x.cd_pessoa_fisica
				and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
				and rownum = 1)
		or 	exists (select 1
				from agenda_paciente x
				where a.cd_pessoa_fisica = x.cd_pessoa_fisica
				and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
				and rownum = 1))
		and	rownum = 1;

	exception
	when no_data_found then

		begin
			select	cd_pessoa_fisica,
					cd_estabelecimento
			into	cd_pessoa_fisica_w,
					cd_estabelecimento_w
			from	pessoa_fisica a
			where	nr_ddd_celular		= nr_ddd_celular_w
			and		nr_telefone_celular = nr_telefone_celular_w
			and 	dt_obito is null
			and	(exists (select 1
					from agenda_consulta x
					where a.cd_pessoa_fisica = x.cd_pessoa_fisica
					and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
					and rownum = 1)
			or 	exists (select 1
					from agenda_paciente x
					where a.cd_pessoa_fisica = x.cd_pessoa_fisica
					and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
					and rownum = 1))
			and		rownum = 1;

		exception
		when no_data_found then

			begin
				select	cd_pessoa_fisica,
						cd_estabelecimento
				into	cd_pessoa_fisica_w,
						cd_estabelecimento_w
				from	pessoa_fisica a
				where	somente_numero(nr_telefone_celular)	= somente_numero(nr_ddd_celular_w||nr_telefone_celular_w)
				and 	dt_obito is null
				and	(exists (select 1
						from agenda_consulta x
						where a.cd_pessoa_fisica = x.cd_pessoa_fisica
						and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
						and rownum = 1)
				or 	exists (select 1
						from agenda_paciente x
						where a.cd_pessoa_fisica = x.cd_pessoa_fisica
						and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
						and rownum = 1))
				and		rownum = 1;

				exception
				when no_data_found then
					begin
						select	max(cd_pessoa_fisica),
								max(cd_estabelecimento)
						into	cd_pessoa_fisica_w,
								cd_estabelecimento_w
						from	pessoa_fisica a
						where	somente_numero(nr_telefone_celular)	= somente_numero(nr_celular_w)
						and 	dt_obito is null
						and	(exists (select 1
								from agenda_consulta x
								where a.cd_pessoa_fisica = x.cd_pessoa_fisica
								and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
								and rownum = 1)
						or 	exists (select 1
								from agenda_paciente x
								where a.cd_pessoa_fisica = x.cd_pessoa_fisica
								and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
								and rownum = 1));

						exception
						when no_data_found then
						begin
							select	cd_pessoa_fisica,
									cd_estabelecimento
							into	cd_pessoa_fisica_w,
									cd_estabelecimento_w
							from	pessoa_fisica a
							where	nr_ddd_celular		= nr_ddd_celular_w
							and		nr_telefone_celular = substr(trim(nr_telefone_celular_w),2,50)  -- older number format with 8 digits
							and 	dt_obito is null
							and	(exists (select 1
									from agenda_consulta x
									where a.cd_pessoa_fisica = x.cd_pessoa_fisica
									and x.dt_agenda between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
									and rownum = 1)
							or 	exists (select 1
									from agenda_paciente x
									where a.cd_pessoa_fisica = x.cd_pessoa_fisica
									and x.hr_inicio between trunc(sysdate) and trunc(sysdate) + qt_dias_confirmacao_ww
									and rownum = 1))
							and		rownum = 1;
						end;
					end;
			end;
		end;
	end;

	if	(cd_pessoa_fisica_w is not null) then

		select	nvl(max(a.nr_sequencia),0)
		into	nr_seq_ageint_w
		from 	agenda_integrada a,
				agenda_integrada_item b,
				agenda_paciente c
		where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
		and		c.nr_sequencia 	   	= b.nr_seq_agenda_exame
		and		a.nr_sequencia 	   	= b.nr_seq_agenda_int
		and		c.hr_inicio	  		>= trunc(sysdate)
		and		c.ie_status_agenda 	= 'N'
		and		c.dt_confirmacao   	is null;

		if (nvl(nr_seq_ageint_w,0) = 0) then

			select	nvl(max(a.nr_sequencia),0)
			into	nr_seq_ageint_w
			from 	agenda_integrada a,
					agenda_integrada_item b,
					agenda_consulta c
			where	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
			and		c.nr_sequencia 	   	= b.nr_seq_agenda_cons
			and		a.nr_sequencia 	   	= b.nr_seq_agenda_int
			and		c.dt_agenda	  		>= trunc(sysdate)
			and		c.ie_status_agenda 	= 'N'
			and		c.dt_confirmacao   	is null;
		end if;

		/*if (nvl(nr_seq_ageint_w,0) > 0) then
			:new.nr_seq_ageint := nr_seq_ageint_w;
		end if;*/

		SELECT MAX(nr_seq_forma_confirmacao_sms),
		       MAX(NVL(ie_altera_status_confirmada,'N')),
		       MAX(NVL(IE_CONFIRM_RET_SMS,'N'))
		  INTO nr_seq_forma_confirmacao_w,
		       ie_altera_status_confirmada_w,
		       ie_confirm_ret_sms_w
		  FROM parametro_agenda
		 WHERE cd_estabelecimento = nvl(cd_estabelecimento_w, cd_estabelecimento);

		open c01;
		loop
		fetch c01 into
			nr_seq_agenda_w,
			cd_agenda_w,
			dt_agenda_w,
         ie_classif_agenda_w;
		exit when c01%notfound;
			begin

        IF ((IE_CONFIRM_RET_SMS_w = 'N')  OR
            (IE_CONFIRM_RET_SMS_w = 'S' AND
             valida_sms_agenda_regra(ie_classif_agenda_p   => ie_classif_agenda_w,
                                     cd_agenda_p           => cd_agenda_w) = 'S')) THEN
			if (instr(upper(:new.ds_resposta), 'SIM') > 0) or
				(obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S') then
				update	agenda_consulta
				set		dt_confirmacao			= sysdate,
						nm_usuario_confirm		= 'TASY',
						nm_usuario				= 'TASY',
						nr_seq_forma_confirmacao= nr_seq_forma_confirmacao_w,
						ds_confirmacao			= wheb_mensagem_pck.get_texto(795424),
						ie_status_agenda         = decode(ie_altera_status_confirmada_w,
						                            'S',
						                            ie_status_confirmado_w,
						                            ie_status_agenda)
				where	nr_sequencia			= nr_seq_agenda_w;
      elsif((instr(upper(:new.ds_resposta), 'NAO') > 0) or (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'N')) then
         if(length(ds_motivo_cancelamento_w) > 0 ) then
           wheb_usuario_pck.set_ie_commit('N');
           begin
             alterar_status_agecons(cd_agenda_w,
                                    nr_seq_agenda_w,
                                    'C',
                                    null,
                                    ds_motivo_cancelamento_w,
                                    'N',
                                    'TASY',
                                    '');
           exception
             when others then
               null;
           end;

           wheb_usuario_pck.set_ie_commit('S');
         end if;
        insert into AGENDA_RETORNO_SMS
          (NR_SEQUENCIA,
           DT_ATUALIZACAO_NREC,
           DT_ATUALIZACAO,
           NM_USUARIO_NREC,
           NM_USUARIO,
           IE_STATUS,
           NR_SEQ_AGEPAC)
        values
          (AGENDA_RETORNO_SMS_seq.nextval,
           sysdate,
           sysdate,
           'TASY',
           'TASY',
           'N',
           nr_seq_agenda_pac_w);
			else

				insert into AGENDA_RETORNO_SMS (
					NR_SEQUENCIA,
					DT_ATUALIZACAO_NREC,
					DT_ATUALIZACAO,
					NM_USUARIO_NREC,
					NM_USUARIO,
					IE_STATUS,
					NR_SEQ_AGECONS
				) values (
					agenda_retorno_sms_seq.nextval,
					sysdate,
					sysdate,
					'TASY',
					'TASY',
					'N',
					nr_seq_agenda_w
				);
			end if;

        END IF;
			end;
		end loop;
		close c01;


		open C02;
		loop
		fetch C02 into
			nr_seq_agenda_pac_w,
			cd_agenda_w,
			dt_agenda_w;
		exit when C02%notfound;

      BEGIN
        IF ((IE_CONFIRM_RET_SMS_w = 'N')  OR
            (IE_CONFIRM_RET_SMS_w = 'S' AND
             valida_sms_agenda_regra(ie_classif_agenda_p   => ie_classif_agenda_w,
                                     cd_agenda_p           => cd_agenda_w) = 'S')) THEN

			if (instr(upper(:new.ds_resposta), 'SIM') > 0) or
        (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'S') then
        update  agenda_paciente
        set    dt_confirmacao        = sysdate,
            nm_usuario_confirm      = 'TASY',
            nm_usuario          = 'TASY',
            nr_seq_forma_confirmacao  = nr_seq_forma_confirmacao_w,
            ds_confirmacao        = wheb_mensagem_pck.get_texto(795424),
            ie_status_agenda         = decode(ie_altera_status_confirmada_w,
                                        'S',
                                        ie_status_confirmado_w,
                                        ie_status_agenda)

        where  nr_sequencia        = nr_seq_agenda_pac_w;
      elsif((instr(upper(:new.ds_resposta), 'NAO') > 0) or (obter_se_resposta_Sim_cadastro(upper(:new.ds_resposta)) = 'N')) then
         if(length(ds_motivo_cancelamento_w) > 0 ) then
           wheb_usuario_pck.set_ie_commit('N');
           begin
             alterar_status_agenda(cd_agenda_w,
                                   nr_seq_agenda_pac_w,
                                   'C',
                                   null,
                                   ds_motivo_cancelamento_w,
                                   'N',
                                   'TASY',
                                   '');
           exception
             when others then
               null;
           end;
           wheb_usuario_pck.set_ie_commit('S');
         end if;
        insert into AGENDA_RETORNO_SMS
          (NR_SEQUENCIA,
           DT_ATUALIZACAO_NREC,
           DT_ATUALIZACAO,
           NM_USUARIO_NREC,
           NM_USUARIO,
           IE_STATUS,
           NR_SEQ_AGEPAC)
        values
          (AGENDA_RETORNO_SMS_seq.nextval,
           sysdate,
           sysdate,
           'TASY',
           'TASY',
           'N',
           nr_seq_agenda_pac_w);
      else
        insert into AGENDA_RETORNO_SMS (
          NR_SEQUENCIA,
          DT_ATUALIZACAO_NREC,
          DT_ATUALIZACAO,
          NM_USUARIO_NREC,
          NM_USUARIO,
          IE_STATUS,
          NR_SEQ_AGEPAC
        ) values (
          AGENDA_RETORNO_SMS_seq.nextval,
          sysdate,
          sysdate,
          'TASY',
          'TASY',
          'N',
          nr_seq_agenda_pac_w
        );
      end if;

        END IF;
      end;
    end loop;
    close C02;

  end if;
end if;
wheb_usuario_pck.set_ie_commit('S');
end;
/


ALTER TABLE TASY.LOG_RETORNO_SMS ADD (
  CONSTRAINT LORESMS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LOG_RETORNO_SMS TO NIVEL_1;

