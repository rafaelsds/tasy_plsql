ALTER TABLE TASY.COT_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA
(
  NR_COT_COMPRA             NUMBER(10)          NOT NULL,
  DT_COT_COMPRA             DATE                NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  CD_COMPRADOR              VARCHAR2(10 BYTE)   NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  CD_PESSOA_SOLICITANTE     VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  DT_GERACAO_ORDEM_COMPRA   DATE,
  DT_RETORNO_PREV           DATE,
  DT_ENTREGA                DATE,
  NR_DOCUMENTO_EXTERNO      VARCHAR2(10 BYTE),
  IE_TIPO_INTEGRACAO_ENVIO  VARCHAR2(15 BYTE),
  IE_TIPO_INTEGRACAO_RECEB  VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_CANCEL      NUMBER(10),
  NR_SEQ_REG_LICITACAO      NUMBER(10),
  NR_DOCUMENTO_INTERNO      VARCHAR2(100 BYTE),
  NR_SEQ_TIPO_COMPRA        NUMBER(10),
  NR_SEQ_MOD_COMPRA         NUMBER(10),
  DT_FECHAMENTO_LIC         DATE,
  NR_SEQ_REG_COMPRA         NUMBER(10),
  NR_CLASSIF_INTERNO        VARCHAR2(100 BYTE),
  NR_SEQ_SUBGRUPO_COMPRA    NUMBER(10),
  NR_SEQ_AGENDA_PAC         NUMBER(10),
  DS_TITULO                 VARCHAR2(255 BYTE),
  IE_SISTEMA_COTACAO        VARCHAR2(15 BYTE),
  DT_CALCULO_COTACAO        DATE,
  DT_APROVACAO              DATE,
  NR_ORCAMENTO              VARCHAR2(20 BYTE),
  NM_USUARIO_APROV          VARCHAR2(15 BYTE),
  IE_FINALIDADE_COTACAO     VARCHAR2(15 BYTE),
  IE_STATUS_ENVIO           VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO           NUMBER(10),
  DS_JUSTIF_DIVERGENCIA     VARCHAR2(4000 BYTE),
  NR_PROCESSO               NUMBER(10),
  IE_FORMA_VENC_COTACAO     VARCHAR2(15 BYTE),
  IE_OPERACAO_ENVIO         VARCHAR2(15 BYTE),
  IE_ORCADO                 VARCHAR2(1 BYTE),
  NR_ATENDIMENTO            NUMBER(10),
  NR_SEQ_AUTOR_CIR          NUMBER(10),
  CD_CONDICAO_PAGAMENTO     NUMBER(10),
  DT_ENVIO_INTEGR_PADRAO    DATE,
  IE_ENVIADO_INTEGRACAO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COTCOMP_AGEPACI_FK_I ON TASY.COT_COMPRA
(NR_SEQ_AGENDA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_ATEPACI_FK_I ON TASY.COT_COMPRA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOMP_COMPRAD_FK1_I ON TASY.COT_COMPRA
(CD_COMPRADOR, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOMP_CONPAGA_FK_I ON TASY.COT_COMPRA
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_CONTRAT_FK_I ON TASY.COT_COMPRA
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_MOTCASO_FK_I ON TASY.COT_COMPRA
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_MOTCASO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_PESFISI_FK2_I ON TASY.COT_COMPRA
(CD_PESSOA_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COTCOMP_PK ON TASY.COT_COMPRA
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOMP_REGLIMC_FK_I ON TASY.COT_COMPRA
(NR_SEQ_MOD_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_REGLIMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_REGLITC_FK_I ON TASY.COT_COMPRA
(NR_SEQ_TIPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_REGLITC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_RELICIT_FK_I ON TASY.COT_COMPRA
(NR_SEQ_REG_LICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_RELICIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOMP_SUSUCOM_FK_I ON TASY.COT_COMPRA
(NR_SEQ_SUBGRUPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOMP_SUSUCOM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cot_compra_Insert
BEFORE INSERT OR UPDATE ON TASY.COT_COMPRA FOR EACH ROW
DECLARE


ie_seq_proc_cotacao_w		varchar2(1);
nr_processo_w			number(10);
nr_processo_ww			number(10);
ie_status_w  number;

BEGIN

select count(*)
into ie_status_w
from cot_compra_item
where nr_cot_compra = :new.nr_cot_compra and obter_bloq_canc_proj_rec(nr_seq_proj_rec) > 0;

if (ie_status_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

select  max(ie_seq_proc_cotacao)
into    ie_seq_proc_cotacao_w
from 	parametro_compras
where	cd_estabelecimento = :new.cd_estabelecimento;

if	(:new.ds_titulo is null) then
	 :new.ds_titulo := obter_titulo_cotacao(:new.nr_cot_compra, :new.cd_estabelecimento, :new.nm_usuario);
end if;

if 	(ie_seq_proc_cotacao_w = 'S') and
	(:new.nr_processo is null) then

	select	max(rownum) + 1
	into	nr_processo_ww
	from	w_inserir_processo
	where	nvl(cd_estabelecimento,:new.cd_estabelecimento) = :new.cd_estabelecimento;

	nr_processo_w := nr_processo_ww||to_char(sysdate,'yyyy');

	inserir_numero_processo(:new.nr_cot_compra, nr_processo_w, :new.cd_estabelecimento);

	:new.nr_processo := nr_processo_w;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_beforeupdate
before update ON TASY.COT_COMPRA for each row
declare

reg_integracao_p			gerar_int_padrao.reg_integracao;

begin

if (updating) then
      	if	(:old.nr_documento_externo is not null) then
		:new.nr_documento_externo	:= :old.nr_documento_externo;
	end if;
end if;

if	(:old.dt_envio_integr_padrao is null) and
	(:new.dt_envio_integr_padrao is not null) then

	reg_integracao_p.ie_operacao		:=	'I';
	reg_integracao_p.cd_estab_documento	:=	:new.cd_estabelecimento;
	reg_integracao_p.ie_finalidade_cotacao	:=	:new.ie_finalidade_cotacao;
	gerar_int_padrao.gravar_integracao('57', :new.nr_cot_compra, :new.nm_usuario, reg_integracao_p);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_afinsert
after insert or update ON TASY.COT_COMPRA for each row
declare
ds_retorno_integracao_w clob;

begin

if (:old.dt_aprovacao is null and :new.dt_aprovacao is not null) then

    select bifrost.send_integration(
        'purchase.quotation.send.request',
        'com.philips.tasy.integration.purchasequotation.outbound.PurchaseQuotationCallback',
        ''||:new.nr_cot_compra||'',
        :new.nm_usuario)
    into ds_retorno_integracao_w
    from dual;

end if;

end cot_compra_afinsert;
/


CREATE OR REPLACE TRIGGER TASY.COT_COMPRA_DELETE 
BEFORE DELETE ON TASY.COT_COMPRA 
for each row
declare 
nr_solic_compra_w		number(10); 
nr_item_cot_compra_w		number(10); 
 
cursor c01 is 
select	nr_solic_compra, 
	nr_item_cot_compra 
from	cot_compra_item 
where	nr_cot_compra = :old.nr_cot_compra 
and	nr_solic_compra > 0; 
 
pragma autonomous_transaction; 
 
BEGIN 
 
 
open C01; 
loop 
fetch C01 into	 
	nr_solic_compra_w, 
	nr_item_cot_compra_w; 
exit when C01%notfound; 
	begin 
	 
	if	(:old.nr_cot_compra > 0) and 
		(nr_item_cot_compra_w > 0) then 
		 
		update	solic_compra_item a 
		set	a.nr_cot_compra		= null, 
			a.nr_item_cot_compra	= null 
		where	a.nr_cot_compra		= :old.nr_cot_compra 
		and	a.nr_item_cot_compra	= nr_item_cot_compra_w; 
 
	end if; 
	end; 
end loop; 
close C01; 
 
commit; 
 
END;
/


ALTER TABLE TASY.COT_COMPRA ADD (
  CONSTRAINT COTCOMP_PK
 PRIMARY KEY
 (NR_COT_COMPRA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          576K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA ADD (
  CONSTRAINT COTCOMP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT COTCOMP_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT COTCOMP_REGLITC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPRA) 
 REFERENCES TASY.REG_LIC_TIPO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOMP_REGLIMC_FK 
 FOREIGN KEY (NR_SEQ_MOD_COMPRA) 
 REFERENCES TASY.REG_LIC_MOD_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOMP_SUSUCOM_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO_COMPRA) 
 REFERENCES TASY.SUP_SUBGRUPO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOMP_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_PAC) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COTCOMP_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT COTCOMP_COMPRAD_FK1 
 FOREIGN KEY (CD_COMPRADOR, CD_ESTABELECIMENTO) 
 REFERENCES TASY.COMPRADOR (CD_PESSOA_FISICA,CD_ESTABELECIMENTO),
  CONSTRAINT COTCOMP_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COTCOMP_MOTCASO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.MOTIVO_CANCEL_SC_OC (NR_SEQUENCIA),
  CONSTRAINT COTCOMP_RELICIT_FK 
 FOREIGN KEY (NR_SEQ_REG_LICITACAO) 
 REFERENCES TASY.REG_LICITACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COT_COMPRA TO NIVEL_1;

