ALTER TABLE TASY.TX_REGISTRO_SNT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_REGISTRO_SNT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_REGISTRO_SNT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_RGCT               NUMBER(10),
  NR_RGCT_DIG           NUMBER(5),
  NR_SEQ_RECEPTOR       NUMBER(10)              NOT NULL,
  NR_SEQ_CENTRAL        NUMBER(10)              NOT NULL,
  DT_INSCRICAO          DATE,
  IE_STATUS             VARCHAR2(15 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_EQUIPE_CENTRO  NUMBER(10)              NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  IE_STATUS_RGCT        VARCHAR2(3 BYTE),
  IE_MOTIVO_STATUS      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXRESN_PK ON TASY.TX_REGISTRO_SNT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESN_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXRESN_TXCETR_FK_I ON TASY.TX_REGISTRO_SNT
(NR_SEQ_CENTRAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESN_TXCETR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRESN_TXRECE_FK_I ON TASY.TX_REGISTRO_SNT
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESN_TXRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_registro_snt_befupdate
before insert or update ON TASY.TX_REGISTRO_SNT for each row
declare
ie_tipo_orgao_receptor_w	varchar2(15);
qt_registro_w			number(10,0);
nr_seq_equipe_w		number(10,0);
ie_permite_lib_w	varchar2(1);
ie_liberado_w 		varchar2(1);
cd_estabelecimento_w	number(5);
begin


select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from 	tx_central_trAnspl
where	nr_sequencia = :new.nr_seq_central;

Obter_Param_Usuario(7006,8,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_permite_lib_w);

if	(ie_permite_lib_w = 'S') and (:new.ie_status_rgct = 'A') then

	ie_liberado_w	:= substr(TX_Obter_se_lista_servico(:new.nr_seq_receptor),1,1);

	if	(ie_liberado_w = 'N') then

		--O paciente n�o est� liberado na lista de servi�o.
		wheb_mensagem_pck.exibir_mensagem_abort(264146);

	end if;

end if;

/* Verifica compatibilidade entre �rg�os */
select	substr(tx_obter_dados_receptor(:new.nr_seq_receptor, 'TO'),1,15)
into	ie_tipo_orgao_receptor_w
from	dual;

/* Verifica compatibilidade com o centro */
select	count(*)
into	qt_registro_w
from	tx_central_orgao
where	ie_orgao		= ie_tipo_orgao_receptor_w
and	nr_seq_central	= :new.nr_seq_central;

if	(qt_registro_w = 0) then
	--Os �rg�os do receptor e autorizados para o centro de transplantes s�o incompat�veis.
	wheb_mensagem_pck.exibir_mensagem_abort(264149);
end if;

/* Verifica compatibilidade com a equipe */
select	max(nr_seq_equipe)
into	nr_seq_equipe_w
from	tx_central_equipe
where	nr_sequencia		= :new.nr_seq_equipe_centro;

select	count(*)
into	qt_registro_w
from	tx_equipe_orgao
where	ie_orgao		= ie_tipo_orgao_receptor_w
and	nr_seq_equipe		= nr_seq_equipe_w;

if	(qt_registro_w = 0) then
	--Os �rg�os do receptor e autorizados para a equipe de transplantes s�o incompat�veis.
	wheb_mensagem_pck.exibir_mensagem_abort(264150);
end if;

/* Registra a altera��o no hist�rico  */
insert into tx_registro_snt_hist (
	nr_sequencia,
	nr_seq_registro,
	ie_status,
	ie_status_rgct,
	ie_motivo_status,
	dt_historico,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_observacao
) values (
	tx_registro_snt_hist_seq.nextval,
	:new.nr_sequencia,
	:new.ie_status,
	:new.ie_status_rgct,
	:new.ie_motivo_status,
	sysdate,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.ds_observacao
);
end;
/


ALTER TABLE TASY.TX_REGISTRO_SNT ADD (
  CONSTRAINT TXRESN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_REGISTRO_SNT ADD (
  CONSTRAINT TXRESN_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA),
  CONSTRAINT TXRESN_TXCETR_FK 
 FOREIGN KEY (NR_SEQ_CENTRAL) 
 REFERENCES TASY.TX_CENTRAL_TRANSPL (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_REGISTRO_SNT TO NIVEL_1;

