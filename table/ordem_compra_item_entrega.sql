ALTER TABLE TASY.ORDEM_COMPRA_ITEM_ENTREGA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ITEM_ENTREGA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ITEM_ENTREGA
(
  NR_ORDEM_COMPRA          NUMBER(10)           NOT NULL,
  NR_ITEM_OCI              NUMBER(5)            NOT NULL,
  DT_PREVISTA_ENTREGA      DATE                 NOT NULL,
  DT_REAL_ENTREGA          DATE,
  QT_PREVISTA_ENTREGA      NUMBER(13,4)         NOT NULL,
  QT_REAL_ENTREGA          NUMBER(13,4),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_CANCELAMENTO          DATE,
  NR_SEQUENCIA             NUMBER(10),
  IE_STATUS_EXPORTAR       VARCHAR2(1 BYTE),
  DT_ENTREGA_ORIGINAL      DATE,
  DT_ENTREGA_LIMITE        DATE,
  DT_BAIXA                 DATE,
  DT_CONFIRMA_ENTREGA      DATE,
  HR_PREVISTA_ENTREGA      DATE,
  DT_INICIO_PENDENCIA      DATE,
  NM_USUARIO_INI_PEND      VARCHAR2(15 BYTE),
  DT_FIM_PENDENCIA         DATE,
  NM_USUARIO_FIM_PEND      VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_PEND       NUMBER(10),
  IE_STATUS                VARCHAR2(15 BYTE),
  IE_CANCELADA_INSPECAO    VARCHAR2(1 BYTE),
  NM_USUARIO_CONF_ENTREGA  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCOENT_ORCOIEE_FK_I ON TASY.ORDEM_COMPRA_ITEM_ENTREGA
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOENT_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_ITEM_ENTREGA
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOIE_MOPEDEN_FK_I ON TASY.ORDEM_COMPRA_ITEM_ENTREGA
(NR_SEQ_MOTIVO_PEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOIE_MOPEDEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOIE_ORCENT_FK_I ON TASY.ORDEM_COMPRA_ITEM_ENTREGA
(NR_ORDEM_COMPRA, DT_PREVISTA_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORDCOIE_PK ON TASY.ORDEM_COMPRA_ITEM_ENTREGA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_ENTREGA_tp  after update ON TASY.ORDEM_COMPRA_ITEM_ENTREGA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CANCELADA_INSPECAO,1,4000),substr(:new.IE_CANCELADA_INSPECAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CANCELADA_INSPECAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_EXPORTAR,1,4000),substr(:new.IE_STATUS_EXPORTAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_EXPORTAR',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_REAL_ENTREGA,1,4000),substr(:new.QT_REAL_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_REAL_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_OCI,1,4000),substr(:new.NR_ITEM_OCI,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_OCI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PREVISTA_ENTREGA,1,4000),substr(:new.DT_PREVISTA_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'DT_PREVISTA_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REAL_ENTREGA,1,4000),substr(:new.DT_REAL_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'DT_REAL_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PREVISTA_ENTREGA,1,4000),substr(:new.QT_PREVISTA_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_PREVISTA_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CANCELAMENTO,1,4000),substr(:new.DT_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CANCELAMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ENTREGA_ORIGINAL,1,4000),substr(:new.DT_ENTREGA_ORIGINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTREGA_ORIGINAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ENTREGA_LIMITE,1,4000),substr(:new.DT_ENTREGA_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTREGA_LIMITE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_BAIXA,1,4000),substr(:new.DT_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'DT_BAIXA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CONFIRMA_ENTREGA,1,4000),substr(:new.DT_CONFIRMA_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'DT_CONFIRMA_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.HR_PREVISTA_ENTREGA,1,4000),substr(:new.HR_PREVISTA_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'HR_PREVISTA_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_INI_PEND,1,4000),substr(:new.NM_USUARIO_INI_PEND,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_INI_PEND',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_FIM_PEND,1,4000),substr(:new.NM_USUARIO_FIM_PEND,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_FIM_PEND',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_PENDENCIA,1,4000),substr(:new.DT_INICIO_PENDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_PENDENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_PENDENCIA,1,4000),substr(:new.DT_FIM_PENDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_PENDENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_PEND,1,4000),substr(:new.NR_SEQ_MOTIVO_PEND,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_PEND',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_CONF_ENTREGA,1,4000),substr(:new.NM_USUARIO_CONF_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_CONF_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_ENTREGA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.DELETE_OC_ITEM_ENTREGA
before delete ON TASY.ORDEM_COMPRA_ITEM_ENTREGA for each row
declare

nr_sequencia_w		inspecao_recebimento.nr_sequencia%type;

begin
select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	inspecao_recebimento
where	nr_seq_entrega = :old.nr_sequencia;

if	(nr_sequencia_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(311804,'NR_SEQ_INSP_W='||nr_sequencia_w||';DS_STACK_W='||substr(dbms_utility.format_call_stack,1,2000));
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ordem_compra_item_entr_update
before update ON TASY.ORDEM_COMPRA_ITEM_ENTREGA for each row
declare
qt_reg_w			number(10);
nr_seq_nf_w		number(10);
nr_item_nf_w		number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.dt_prevista_entrega is not null) and
	(:old.dt_prevista_entrega <> :new.dt_prevista_entrega) then
	begin

	select	nvl(max(a.nr_sequencia),0)
	into	nr_seq_nf_w
	from	nota_fiscal_item a,
		nota_fiscal b
	where	a.nr_sequencia = b.nr_sequencia
	and	b.dt_atualizacao_estoque is null
	and	a.nr_ordem_compra = :new.nr_ordem_compra
	and	a.nr_item_oci = :new.nr_item_oci
	and	trunc(a.dt_entrega_ordem) = trunc(:old.dt_prevista_entrega);

	if	(nr_seq_nf_w > 0) then
		begin

		select	nvl(max(a.nr_item_nf),0)
		into	nr_item_nf_w
		from	nota_fiscal_item a,
			nota_fiscal b
		where	a.nr_sequencia = b.nr_sequencia
		and	b.nr_sequencia = nr_seq_nf_w
		and	a.nr_ordem_compra = :new.nr_ordem_compra
		and	a.nr_item_oci = :new.nr_item_oci
		and	trunc(a.dt_entrega_ordem) = trunc(:old.dt_prevista_entrega);

		if	(nr_item_nf_w > 0) then
			begin

			update	nota_fiscal_item
			set	dt_entrega_ordem = :new.dt_prevista_entrega
			where	nr_sequencia = nr_seq_nf_w
			and	nr_item_nf = nr_item_nf_w;

			end;
		end if;

		end;
	end if;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_ENTREGA_DEL
BEFORE DELETE ON TASY.ORDEM_COMPRA_ITEM_ENTREGA FOR EACH ROW
DECLARE

qt_registros_w		number(10);

begin

select	count(*)
into	qt_registros_w
from	inspecao_recebimento
where	nr_seq_entrega = :new.nr_Sequencia;

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(237554);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.INTPD_ORDEM_COMPRA_ITEM_EN
before insert or update or delete ON TASY.ORDEM_COMPRA_ITEM_ENTREGA 
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_registros_w			number(10);
dt_aprovacao_w			ordem_compra.dt_aprovacao%type;

pragma autonomous_transaction;
begin

reg_integracao_p.cd_estab_documento	:= null;
reg_integracao_p.ie_tipo_ordem		:= null;
reg_integracao_p.nr_seq_tipo_compra	:= null;
reg_integracao_p.nr_seq_mod_compra	:= null;
reg_integracao_p.ds_id_origin		:= null;

select 	max(dt_aprovacao) 
into	dt_aprovacao_w
from 	ordem_compra 
where 	nr_ordem_compra = :new.nr_ordem_compra;

if	(inserting or updating) and
	(dt_aprovacao_w is null) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :new.nr_ordem_compra
	and	ie_evento = '2'
	and	nvl(ie_status,'P') not in ('P','R')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then	
		reg_integracao_p.ie_operacao		:= 'A';			
		gerar_int_padrao.gravar_integracao('2',:new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);
	end if;
	
	if(inserting) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
		gravar_agend_fluxo_caixa(:new.nr_ordem_compra,:new.nr_sequencia,'OC', :new.DT_PREVISTA_ENTREGA,'I',:new.nm_usuario,'S');
	elsif (updating) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
		gravar_agend_fluxo_caixa(:new.nr_ordem_compra,:new.nr_sequencia,'OC', :new.DT_PREVISTA_ENTREGA,'A',:new.nm_usuario,'S');
	end if;

elsif	(deleting) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :old.nr_ordem_compra
	and	ie_evento = '2'
	and	ie_status in ('S')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;	

	if	(qt_registros_w > 0) then
		reg_integracao_p.ie_operacao		:= 'E';
		gerar_int_padrao.gravar_integracao('2',:old.nr_ordem_compra,:old.nm_usuario, reg_integracao_p);
	end if;
	
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:old.nr_ordem_compra,:old.nr_sequencia,'OC', :old.DT_PREVISTA_ENTREGA,'E',:old.nm_usuario,'S');
	
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_ENTREGA_UPD
BEFORE UPDATE ON TASY.ORDEM_COMPRA_ITEM_ENTREGA FOR EACH ROW
DECLARE

ds_log_w	varchar2(2000);
dt_liberacao_w	date;

begin

select	max(dt_liberacao)
into	dt_liberacao_w
from	ordem_compra
where	nr_ordem_compra = :new.nr_ordem_compra;

if	(dt_liberacao_w is not null) and
	(:old.qt_prevista_entrega <> :new.qt_prevista_entrega) then
	gravar_log_tasy(27, substr(dbms_utility.format_call_stack,1,2000), :new.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.ORDEM_COMPRA_ITEM_ENTREGA ADD (
  CONSTRAINT ORDCOIE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ITEM_ENTREGA ADD (
  CONSTRAINT ORDCOIE_MOPEDEN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PEND) 
 REFERENCES TASY.MOTIVO_PENDENCIA_ENTREGA (NR_SEQUENCIA),
  CONSTRAINT ORCOENT_ORCOIEE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_ITEM_ENTREGA TO NIVEL_1;

