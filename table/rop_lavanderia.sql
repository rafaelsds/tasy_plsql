ALTER TABLE TASY.ROP_LAVANDERIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_LAVANDERIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_LAVANDERIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_EVENTO             VARCHAR2(15 BYTE)       NOT NULL,
  IE_STATUS             VARCHAR2(15 BYTE),
  CD_CNPJ               VARCHAR2(14 BYTE),
  CD_PESSOA_TRANSPORTE  VARCHAR2(10 BYTE),
  DS_LACRE              VARCHAR2(80 BYTE),
  QT_PESO               NUMBER(13,4),
  DT_ENVIO_RECEB        DATE                    NOT NULL,
  DT_LIBERACAO          DATE,
  CD_PESSOA_RESP        VARCHAR2(10 BYTE),
  DT_CONFIRMACAO        DATE,
  NR_SEQ_MOTIVO_DEV     NUMBER(10),
  DS_JUSTIFICATIVA_DEV  VARCHAR2(4000 BYTE),
  NR_CESTO              NUMBER(10),
  NR_SEQ_LAVANDERIA     NUMBER(10),
  NR_SEQ_TIPO_ROUPA     NUMBER(10),
  NR_SEQ_SAIDA_CORRESP  NUMBER(10),
  NR_SEQ_OPERACAO       NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPLAVA_ESTABEL_FK_I ON TASY.ROP_LAVANDERIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLAVA_PESFISI_FK_I ON TASY.ROP_LAVANDERIA
(CD_PESSOA_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLAVA_PESFISI_FK2_I ON TASY.ROP_LAVANDERIA
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLAVA_PESJURI_FK_I ON TASY.ROP_LAVANDERIA
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ROPLAVA_PK ON TASY.ROP_LAVANDERIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLAVA_ROPLAVN_FK_I ON TASY.ROP_LAVANDERIA
(NR_SEQ_LAVANDERIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_ROPLAVN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLAVA_ROPMODE_FK_I ON TASY.ROP_LAVANDERIA
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_ROPMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLAVA_ROPTOLA_FK_I ON TASY.ROP_LAVANDERIA
(NR_SEQ_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_ROPTOLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLAVA_ROTIROU_FK_I ON TASY.ROP_LAVANDERIA
(NR_SEQ_TIPO_ROUPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_ROTIROU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLAVA_SETATEN_FK_I ON TASY.ROP_LAVANDERIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLAVA_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ROP_LAVANDERIA ADD (
  CONSTRAINT ROPLAVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_LAVANDERIA ADD (
  CONSTRAINT ROPLAVA_ROPTOLA_FK 
 FOREIGN KEY (NR_SEQ_OPERACAO) 
 REFERENCES TASY.ROP_TIPO_OPERACAO_LAV (NR_SEQUENCIA),
  CONSTRAINT ROPLAVA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPLAVA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ROPLAVA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ROPLAVA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_TRANSPORTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ROPLAVA_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ROPLAVA_ROPMODE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.ROP_MOTIVO_DEVOLUCAO (NR_SEQUENCIA),
  CONSTRAINT ROPLAVA_ROPLAVN_FK 
 FOREIGN KEY (NR_SEQ_LAVANDERIA) 
 REFERENCES TASY.ROP_CADASTRO_LAVANDERIA (NR_SEQUENCIA),
  CONSTRAINT ROPLAVA_ROTIROU_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ROUPA) 
 REFERENCES TASY.ROP_TIPO_ROUPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_LAVANDERIA TO NIVEL_1;

