ALTER TABLE TASY.PF_MEDICO_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_MEDICO_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_MEDICO_EXTERNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MEDICO            VARCHAR2(10 BYTE)        NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_TIPO_MEDICO   NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  IE_BENEFIT_AUTH      VARCHAR2(1 BYTE),
  NM_CLINICA_MEDICA    VARCHAR2(255 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFME_PESFISI_FK_I ON TASY.PF_MEDICO_EXTERNO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFME_PESFISIMED_FK_I ON TASY.PF_MEDICO_EXTERNO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFME_PK ON TASY.PF_MEDICO_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFME_TIMEDEX_FK_I ON TASY.PF_MEDICO_EXTERNO
(NR_SEQ_TIPO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pf_medico_externo_atual
before insert or update ON TASY.PF_MEDICO_EXTERNO for each row
declare
qt_reg_w	number(10);
validate_date varchar2(10);
dt_end_date_null date;
dt_max_end_date date;
is_both_dates_null varchar2(10);
ie_medico_familia_w varchar2(1 char);
ie_existe_medico_familia_w varchar2(1 char);

pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

  validate_date := 'N';
  if	(((inserting) or (updating)) and
	nvl(gerar_int_padrao.get_executando_recebimento,'N') = 'N') then

    select	count(nr_sequencia)
    into	qt_reg_w
    from	pf_medico_externo
    where	cd_pessoa_fisica = :new.cd_pessoa_fisica
    and   cd_medico = :new.cd_medico
    and  ((dt_inicio_vigencia between :new.dt_inicio_vigencia and :new.dt_fim_vigencia)
        or(dt_fim_vigencia between :new.dt_inicio_vigencia and :new.dt_fim_vigencia)
        or (:new.dt_inicio_vigencia between dt_inicio_vigencia and dt_fim_vigencia)
        or(:new.dt_fim_vigencia between dt_inicio_vigencia and dt_fim_vigencia))
    and	nr_sequencia <> :new.nr_sequencia;

    if	(qt_reg_w = 0) then
      select	count(nr_sequencia)
      into	qt_reg_w
      from	pf_medico_externo
      where	cd_pessoa_fisica = :new.cd_pessoa_fisica
      and   cd_medico = :new.cd_medico
      and
      ((dt_inicio_vigencia between :new.dt_inicio_vigencia and :new.dt_fim_vigencia)
            or(dt_fim_vigencia between :new.dt_inicio_vigencia and :new.dt_fim_vigencia)
          or (:new.dt_inicio_vigencia between dt_inicio_vigencia and dt_fim_vigencia)
            or(:new.dt_fim_vigencia between dt_inicio_vigencia and dt_fim_vigencia))
      and	nr_sequencia <> :new.nr_sequencia;
    end if;

    if	(qt_reg_w > 0) then
      begin
      wheb_mensagem_pck.exibir_mensagem_abort(1099887);
      end;
    end if;

    select max(dt_inicio_vigencia)
    into	dt_end_date_null
    from  PF_MEDICO_EXTERNO
    where cd_pessoa_fisica = :new.cd_pessoa_fisica
    and   cd_medico = :new.cd_medico
    and   nr_sequencia <> :new.nr_sequencia
    and   dt_fim_vigencia is null;

    select nvl(max('S'),'N')
    into	is_both_dates_null
    from  PF_MEDICO_EXTERNO
    where cd_pessoa_fisica = :new.cd_pessoa_fisica
    and   cd_medico = :new.cd_medico
    and   nr_sequencia <> :new.nr_sequencia
    and   dt_fim_vigencia is null
    and   dt_inicio_vigencia is null;

    select max(dt_fim_vigencia)
    into	dt_max_end_date
    from  PF_MEDICO_EXTERNO
    where cd_pessoa_fisica = :new.cd_pessoa_fisica
    and   cd_medico = :new.cd_medico
    and   nr_sequencia <> :new.nr_sequencia
    and   dt_fim_vigencia is not null
    and   dt_inicio_vigencia is not null;

    if(dt_end_date_null is not null) then
      if (:new.dt_fim_vigencia is null) then
        begin
          wheb_mensagem_pck.exibir_mensagem_abort(1099887);
        end;
      elsif (dt_end_date_null < :new.dt_inicio_vigencia) then
        wheb_mensagem_pck.exibir_mensagem_abort(1099887);
      end if;
    end if;

    if   (dt_end_date_null is not null and :new.dt_fim_vigencia is null)
      or (dt_end_date_null < :new.dt_inicio_vigencia)
      or (:new.dt_inicio_vigencia < dt_max_end_date and :new.dt_fim_vigencia is null)
      or (is_both_dates_null = 'S')then
        begin
          wheb_mensagem_pck.exibir_mensagem_abort(1099887);
        end;
    end if;

    if(:new.dt_inicio_vigencia > :new.dt_fim_vigencia) then
      validate_date	:=	'S';
    end if;

    if(validate_date = 'S') then
      begin
      wheb_mensagem_pck.exibir_mensagem_abort(307642);
      end;
    end if;

  end if;

  commit;

end if;

end pf_medico_externo_atual;
/


CREATE OR REPLACE TRIGGER TASY.pf_medico_externo_after
after insert or update or delete ON TASY.PF_MEDICO_EXTERNO for each row
declare
qt_reg_w		number(10);
reg_integracao_w  gerar_int_padrao.reg_integracao;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_cpf_w pessoa_fisica.nr_cpf %type;

pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	cd_pessoa_fisica_w := nvl(:new.cd_pessoa_fisica,:old.cd_pessoa_fisica);

	begin
	select  cd_pessoa_fisica,
		nr_prontuario,
		nvl(ie_funcionario,'N'),
    nr_cpf
	into  	reg_integracao_w.cd_pessoa_fisica,
		reg_integracao_w.nr_prontuario,
		reg_integracao_w.ie_funcionario,
    nr_cpf_w
	from  	pessoa_fisica
	where  	cd_pessoa_fisica = cd_pessoa_fisica_w;
	exception
		when others then
		reg_integracao_w.cd_pessoa_fisica  	:= null;
		reg_integracao_w.nr_prontuario    	:= null;
		reg_integracao_w.ie_funcionario    	:= null;
	end;

	reg_integracao_w.ie_operacao	:= 'A';

	select 	decode(count(*), 0, 'N', 'S')
	into 	reg_integracao_w.ie_pessoa_atend
	from 	pessoa_fisica_aux
	where 	cd_pessoa_fisica 	= cd_pessoa_fisica_w
	and 	nr_primeiro_atend 	is not null;

  if nr_cpf_w is not null then
     reg_integracao_w.ie_possui_cpf := 'S';
  end if;

	if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
		gerar_int_padrao.gravar_integracao('12', cd_pessoa_fisica_w,nvl(:new.nm_usuario,:old.nm_usuario), reg_integracao_w);
	end if;

	commit;

end if;

end pf_medico_externo_after;
/


ALTER TABLE TASY.PF_MEDICO_EXTERNO ADD (
  CONSTRAINT PFME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_MEDICO_EXTERNO ADD (
  CONSTRAINT PFME_PESFISIMED_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFME_TIMEDEX_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MEDICO) 
 REFERENCES TASY.TIPO_MEDICO_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT PFME_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PF_MEDICO_EXTERNO TO NIVEL_1;

