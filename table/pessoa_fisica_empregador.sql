ALTER TABLE TASY.PESSOA_FISICA_EMPREGADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_EMPREGADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_EMPREGADOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_INICIO_TRABALHO   DATE,
  DT_FIM_TRABALHO      DATE,
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  CD_PROFISSAO         NUMBER(10),
  DS_PROFISSAO         VARCHAR2(255 BYTE),
  DS_EMPRESA_TEXTO     VARCHAR2(80 BYTE),
  DS_ENDERECO_EMPREG   VARCHAR2(40 BYTE),
  NR_ENDERECO_EMPREG   VARCHAR2(10 BYTE),
  DS_PAIS_EMPREG       VARCHAR2(80 BYTE),
  CD_CEP_EMPREG        VARCHAR2(15 BYTE),
  DS_MUNICIPIO_EMPREG  VARCHAR2(40 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFEMPREG_PESFISI_FK_I ON TASY.PESSOA_FISICA_EMPREGADOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFEMPREG_PESJURI_FK_I ON TASY.PESSOA_FISICA_EMPREGADOR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFEMPREG_PK ON TASY.PESSOA_FISICA_EMPREGADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_emp_aftupd
after insert or update or delete ON TASY.PESSOA_FISICA_EMPREGADOR for each row
declare
reg_integracao_w		gerar_int_padrao.reg_integracao;
nr_prontuario_w		pessoa_fisica.nr_prontuario%type;
ie_funcionario_w		pessoa_fisica.ie_funcionario%type;
nr_seq_conselho_w	pessoa_fisica.nr_seq_conselho%type;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_cpf_w pessoa_fisica.nr_cpf %type;

begin

if	(:new.dt_inicio_trabalho is not null and :new.dt_inicio_trabalho > sysdate) then
	wheb_mensagem_pck.exibir_mensagem_abort(1117614);
end if;

cd_pessoa_fisica_w	:=	nvl(:new.cd_pessoa_fisica, :old.cd_pessoa_fisica);

if	((inserting or updating) and
	(nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S'))	then
	insere_pessoa_fisica_emp(	:new.cd_cgc,
							:new.dt_inicio_trabalho,
							:new.nm_usuario,
							cd_pessoa_fisica_w,
							:new.dt_fim_trabalho,
							:new.cd_profissao,
							:new.ds_profissao,
							nvl(:old.cd_cgc, :new.cd_cgc),
							'S',
							:new.ds_empresa_texto,
							nvl(:old.ds_empresa_texto, :new.ds_empresa_texto),
                            :new.ds_endereco_empreg,
							:new.nr_endereco_empreg,
							:new.ds_pais_empreg,
							:new.cd_cep_empreg,
							:new.ds_municipio_empreg,
							:new.nr_telefone
                            );
end if;

select	nr_prontuario,
	ie_funcionario,
	nr_seq_conselho,
  nr_cpf
into	nr_prontuario_w,
	ie_funcionario_w,
	nr_seq_conselho_w,
  nr_cpf_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

if nr_cpf_w is not null then
   reg_integracao_w.ie_possui_cpf := 'S';
end if;

reg_integracao_w.ie_operacao		:=  'A';
reg_integracao_w.nr_prontuario		:=  nr_prontuario_w;
reg_integracao_w.cd_pessoa_fisica	:=  cd_pessoa_fisica_w;
reg_integracao_w.ie_funcionario		:=  nvl(ie_funcionario_w,'N');
reg_integracao_w.nr_seq_conselho	:=  nr_seq_conselho_w;

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	wheb_usuario_pck.set_ie_executar_trigger('S');
end if;

if  (wheb_usuario_pck.get_ie_lote_contabil = 'N') then
  gerar_int_padrao.gravar_integracao('12', cd_pessoa_fisica_w,nvl(:new.nm_usuario,:old.nm_usuario), reg_integracao_w);
end if;

end;
/


ALTER TABLE TASY.PESSOA_FISICA_EMPREGADOR ADD (
  CONSTRAINT PFEMPREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PESSOA_FISICA_EMPREGADOR ADD (
  CONSTRAINT PFEMPREG_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFEMPREG_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PESSOA_FISICA_EMPREGADOR TO NIVEL_1;

