ALTER TABLE TASY.QUA_EVENTO_SEPSE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_EVENTO_SEPSE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_EVENTO_SEPSE_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  CD_INTERVALO         VARCHAR2(7 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO              VARCHAR2(3 BYTE)         NOT NULL,
  NR_SEQ_EVENTO_SEPSE  NUMBER(10)               NOT NULL,
  DT_ITEM              DATE                     NOT NULL,
  QT_DOSE              NUMBER(18,6),
  CD_MATERIAL          NUMBER(10),
  NR_SEQ_EXAME         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAEVSI_EXALABO_FK_I ON TASY.QUA_EVENTO_SEPSE_ITEM
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVSI_INTPRES_FK_I ON TASY.QUA_EVENTO_SEPSE_ITEM
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVSI_MATERIA_FK_I ON TASY.QUA_EVENTO_SEPSE_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUAEVSI_PK ON TASY.QUA_EVENTO_SEPSE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVSI_QUAEVSE_FK_I ON TASY.QUA_EVENTO_SEPSE_ITEM
(NR_SEQ_EVENTO_SEPSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUA_EVENTO_SEPSE_ITEM ADD (
  CONSTRAINT QUAEVSI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_EVENTO_SEPSE_ITEM ADD (
  CONSTRAINT QUAEVSI_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT QUAEVSI_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT QUAEVSI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT QUAEVSI_QUAEVSE_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_SEPSE) 
 REFERENCES TASY.QUA_EVENTO_SEPSE (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_EVENTO_SEPSE_ITEM TO NIVEL_1;

