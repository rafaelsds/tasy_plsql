ALTER TABLE TASY.SUP_PESSOA_UNITARIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_PESSOA_UNITARIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_PESSOA_UNITARIZACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  IE_MANIPULACAO       VARCHAR2(1 BYTE)         NOT NULL,
  IE_TRANSPORTE        VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_FRACIONADOR       VARCHAR2(1 BYTE)         NOT NULL,
  IE_CONFERENTE        VARCHAR2(1 BYTE)         NOT NULL,
  IE_REVISOR           VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESUNIT_ESTABEL_FK_I ON TASY.SUP_PESSOA_UNITARIZACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESUNIT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESUNIT_PESFISI_FK_I ON TASY.SUP_PESSOA_UNITARIZACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESUNIT_PK ON TASY.SUP_PESSOA_UNITARIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_PESSOA_UNITARIZACAO_tp  after update ON TASY.SUP_PESSOA_UNITARIZACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MANIPULACAO,1,4000),substr(:new.IE_MANIPULACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MANIPULACAO',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REVISOR,1,4000),substr(:new.IE_REVISOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REVISOR',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRACIONADOR,1,4000),substr(:new.IE_FRACIONADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRACIONADOR',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONFERENTE,1,4000),substr(:new.IE_CONFERENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONFERENTE',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRANSPORTE,1,4000),substr(:new.IE_TRANSPORTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSPORTE',ie_log_w,ds_w,'SUP_PESSOA_UNITARIZACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_PESSOA_UNITARIZACAO ADD (
  CONSTRAINT PESUNIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_PESSOA_UNITARIZACAO ADD (
  CONSTRAINT PESUNIT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESUNIT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.SUP_PESSOA_UNITARIZACAO TO NIVEL_1;

