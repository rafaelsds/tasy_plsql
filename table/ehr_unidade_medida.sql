ALTER TABLE TASY.EHR_UNIDADE_MEDIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_UNIDADE_MEDIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_UNIDADE_MEDIDA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA      VARCHAR2(30 BYTE)      NOT NULL,
  DS_UNIDADE_MEDIDA      VARCHAR2(60 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_APRES           NUMBER(3)              NOT NULL,
  CD_EXP_UNIDADE_MEDIDA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRUNME_DICEXPR_FK_I ON TASY.EHR_UNIDADE_MEDIDA
(CD_EXP_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRUNME_PK ON TASY.EHR_UNIDADE_MEDIDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EHR_UNIDADE_MEDIDA ADD (
  CONSTRAINT EHRUNME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_UNIDADE_MEDIDA ADD (
  CONSTRAINT EHRUNME_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_UNIDADE_MEDIDA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.EHR_UNIDADE_MEDIDA TO NIVEL_1;

