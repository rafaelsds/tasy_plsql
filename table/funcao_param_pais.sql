ALTER TABLE TASY.FUNCAO_PARAM_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_PARAM_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_PARAM_PAIS
(
  CD_PAIS         NUMBER(5)                     NOT NULL,
  NR_SEQ_PARAM    NUMBER(5)                     NOT NULL,
  CD_FUNCAO       NUMBER(5)                     NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUPAPAI_FUNPARA_FK_I ON TASY.FUNCAO_PARAM_PAIS
(CD_FUNCAO, NR_SEQ_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUPAPAI_PK ON TASY.FUNCAO_PARAM_PAIS
(CD_FUNCAO, NR_SEQ_PARAM, CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUPAPAI_TASYPAI_FK_I ON TASY.FUNCAO_PARAM_PAIS
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FUNCAO_PARAM_PAIS ADD (
  CONSTRAINT FUPAPAI_PK
 PRIMARY KEY
 (CD_FUNCAO, NR_SEQ_PARAM, CD_PAIS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_PARAM_PAIS ADD (
  CONSTRAINT FUPAPAI_FUNPARA_FK 
 FOREIGN KEY (CD_FUNCAO, NR_SEQ_PARAM) 
 REFERENCES TASY.FUNCAO_PARAMETRO (CD_FUNCAO,NR_SEQUENCIA),
  CONSTRAINT FUPAPAI_TASYPAI_FK 
 FOREIGN KEY (CD_PAIS) 
 REFERENCES TASY.TASY_PAIS (CD_PAIS));

GRANT SELECT ON TASY.FUNCAO_PARAM_PAIS TO NIVEL_1;

