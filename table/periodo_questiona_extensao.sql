ALTER TABLE TASY.PERIODO_QUESTIONA_EXTENSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERIODO_QUESTIONA_EXTENSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERIODO_QUESTIONA_EXTENSAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  HR_INICIO             VARCHAR2(5 BYTE)        NOT NULL,
  HR_FIM                VARCHAR2(5 BYTE)        NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PERFIL             NUMBER(5),
  DT_FIM                DATE,
  DT_INICIO             DATE,
  IE_QUESTIONAR         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERQUEXT_PERFIL_FK_I ON TASY.PERIODO_QUESTIONA_EXTENSAO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PERQUEXT_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PERQUEXT_PK ON TASY.PERIODO_QUESTIONA_EXTENSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERQUEXT_SETATEN_FK_I ON TASY.PERIODO_QUESTIONA_EXTENSAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PERQUEXT_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.periodo_questiona_extensao_atl
before insert or update ON TASY.PERIODO_QUESTIONA_EXTENSAO for each row
declare

begin
begin
	if (:new.hr_fim is not null) and ((:new.hr_fim <> :old.hr_fim) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fim,'dd/mm/yyyy hh24:mi');
	end if;
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.PERIODO_QUESTIONA_EXTENSAO ADD (
  CONSTRAINT PERQUEXT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERIODO_QUESTIONA_EXTENSAO ADD (
  CONSTRAINT PERQUEXT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PERQUEXT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PERIODO_QUESTIONA_EXTENSAO TO NIVEL_1;

