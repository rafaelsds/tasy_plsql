ALTER TABLE TASY.TX_LOCUS_HLA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_LOCUS_HLA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_LOCUS_HLA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_LOCUS_HLA         VARCHAR2(20 BYTE)        NOT NULL,
  IE_CLASSE_LOCUS      VARCHAR2(2 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXLOCUS_PK ON TASY.TX_LOCUS_HLA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXLOCUS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TX_LOCUS_HLA_tp  after update ON TASY.TX_LOCUS_HLA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_LOCUS_HLA,1,500);gravar_log_alteracao(substr(:old.DS_LOCUS_HLA,1,4000),substr(:new.DS_LOCUS_HLA,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOCUS_HLA',ie_log_w,ds_w,'TX_LOCUS_HLA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TX_LOCUS_HLA ADD (
  CONSTRAINT TXLOCUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TX_LOCUS_HLA TO NIVEL_1;

