ALTER TABLE TASY.NEGOCIACAO_CR_CARTAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NEGOCIACAO_CR_CARTAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NEGOCIACAO_CR_CARTAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_NEGOCIACAO    NUMBER(10)               NOT NULL,
  IE_TIPO_CARTAO       VARCHAR2(1 BYTE)         NOT NULL,
  VL_TRANSACAO         NUMBER(15,2)             NOT NULL,
  QT_PARCELA           NUMBER(4)                NOT NULL,
  NR_SEQ_BANDEIRA      NUMBER(10),
  NR_SEQ_FORMA_PAGTO   NUMBER(10),
  DS_COMPROVANTE       VARCHAR2(100 BYTE),
  NR_AUTORIZACAO       VARCHAR2(40 BYTE),
  NR_SEQ_MOVTO_CARTAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NEGCRCAR_BANCACR_FK_I ON TASY.NEGOCIACAO_CR_CARTAO
(NR_SEQ_BANDEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRCAR_BANCACR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NEGCRCAR_MOVCACR_FK_I ON TASY.NEGOCIACAO_CR_CARTAO
(NR_SEQ_MOVTO_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRCAR_MOVCACR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NEGCRCAR_NEGOCCR_FK_I ON TASY.NEGOCIACAO_CR_CARTAO
(NR_SEQ_NEGOCIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRCAR_NEGOCCR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NEGCRCAR_PK ON TASY.NEGOCIACAO_CR_CARTAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRCAR_PK
  MONITORING USAGE;


ALTER TABLE TASY.NEGOCIACAO_CR_CARTAO ADD (
  CONSTRAINT NEGCRCAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NEGOCIACAO_CR_CARTAO ADD (
  CONSTRAINT NEGCRCAR_NEGOCCR_FK 
 FOREIGN KEY (NR_SEQ_NEGOCIACAO) 
 REFERENCES TASY.NEGOCIACAO_CR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NEGCRCAR_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT NEGCRCAR_MOVCACR_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_CARTAO) 
 REFERENCES TASY.MOVTO_CARTAO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.NEGOCIACAO_CR_CARTAO TO NIVEL_1;

