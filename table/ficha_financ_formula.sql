ALTER TABLE TASY.FICHA_FINANC_FORMULA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FICHA_FINANC_FORMULA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FICHA_FINANC_FORMULA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_FORMULA           VARCHAR2(255 BYTE)       NOT NULL,
  DS_MACRO             VARCHAR2(50 BYTE)        NOT NULL,
  DS_FORMULA           VARCHAR2(4000 BYTE),
  NR_SEQ_CALCULO       NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.FIFIFOR_PK ON TASY.FICHA_FINANC_FORMULA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FIFIFOR_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FIFIFOR_UK ON TASY.FICHA_FINANC_FORMULA
(DS_MACRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FIFIFOR_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ficha_financ_formula_delete
before delete ON TASY.FICHA_FINANC_FORMULA for each row
declare

ds_formula_w	varchar2(255);

pragma autonomous_transaction;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

select	max(nm_formula)
into 	ds_formula_w
from	ficha_financ_formula
where	ds_formula like '%#F_'||:old.ds_macro||'@';

if	(ds_formula_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(231488,'NM_FORMULA='||ds_formula_w);
end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ficha_financ_formula_update
after update ON TASY.FICHA_FINANC_FORMULA for each row
declare

pragma autonomous_transaction;
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

if	(:old.ds_macro <> :new.ds_macro) then
	ficha_financ_altera_macro_for(:old.ds_macro, :new.ds_macro);
end if;
end if;
end;
/


ALTER TABLE TASY.FICHA_FINANC_FORMULA ADD (
  CONSTRAINT FIFIFOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT FIFIFOR_UK
 UNIQUE (DS_MACRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.FICHA_FINANC_FORMULA TO NIVEL_1;

