ALTER TABLE TASY.MP_PROCESSO_OBJ_REC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MP_PROCESSO_OBJ_REC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MP_PROCESSO_OBJ_REC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_OBJ_PROC      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RECURSO       NUMBER(10)               NOT NULL,
  CD_CARGO             NUMBER(10),
  NR_SEQ_TIPO_EQUIP    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPPRORE_CARGO_FK_I ON TASY.MP_PROCESSO_OBJ_REC
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPRORE_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPRORE_MANTPEQ_FK_I ON TASY.MP_PROCESSO_OBJ_REC
(NR_SEQ_TIPO_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPRORE_MANTPEQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPRORE_MPPROOB_FK_I ON TASY.MP_PROCESSO_OBJ_REC
(NR_SEQ_OBJ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPRORE_MPPROOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPRORE_MPTIREC_FK_I ON TASY.MP_PROCESSO_OBJ_REC
(NR_SEQ_RECURSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPRORE_MPTIREC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPPRORE_PK ON TASY.MP_PROCESSO_OBJ_REC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPRORE_PK
  MONITORING USAGE;


ALTER TABLE TASY.MP_PROCESSO_OBJ_REC ADD (
  CONSTRAINT MPPRORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MP_PROCESSO_OBJ_REC ADD (
  CONSTRAINT MPPRORE_MPPROOB_FK 
 FOREIGN KEY (NR_SEQ_OBJ_PROC) 
 REFERENCES TASY.MP_PROCESSO_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPPRORE_MPTIREC_FK 
 FOREIGN KEY (NR_SEQ_RECURSO) 
 REFERENCES TASY.MP_TIPO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT MPPRORE_MANTPEQ_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EQUIP) 
 REFERENCES TASY.MAN_TIPO_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT MPPRORE_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO));

GRANT SELECT ON TASY.MP_PROCESSO_OBJ_REC TO NIVEL_1;

