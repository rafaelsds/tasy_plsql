ALTER TABLE TASY.ROP_INVENTARIO_AJUSTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_INVENTARIO_AJUSTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_INVENTARIO_AJUSTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_RESP_INVENTARIO   VARCHAR2(10 BYTE)        NOT NULL,
  DT_INVENTARIO        DATE                     NOT NULL,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  DT_LIBERACAO         DATE,
  DT_FECHAMENTO        DATE,
  NM_USUARIO_FECHA     VARCHAR2(15 BYTE),
  DT_AJUSTE            DATE,
  NM_USUARIO_AJUSTE    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPINAJ_ESTABEL_FK_I ON TASY.ROP_INVENTARIO_AJUSTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPINAJ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPINAJ_PESFISI_FK_I ON TASY.ROP_INVENTARIO_AJUSTE
(CD_RESP_INVENTARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ROPINAJ_PK ON TASY.ROP_INVENTARIO_AJUSTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ROP_INVENTARIO_AJUSTE ADD (
  CONSTRAINT ROPINAJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_INVENTARIO_AJUSTE ADD (
  CONSTRAINT ROPINAJ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPINAJ_PESFISI_FK 
 FOREIGN KEY (CD_RESP_INVENTARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ROP_INVENTARIO_AJUSTE TO NIVEL_1;

