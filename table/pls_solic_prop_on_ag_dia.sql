ALTER TABLE TASY.PLS_SOLIC_PROP_ON_AG_DIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLIC_PROP_ON_AG_DIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLIC_PROP_ON_AG_DIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_SOLIC_PROP_ON_AG  NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_DIA_SEMANA            NUMBER(1),
  DS_PERIODO               VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSSPAD_PK ON TASY.PLS_SOLIC_PROP_ON_AG_DIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSPAD_PLSSPOA_FK_I ON TASY.PLS_SOLIC_PROP_ON_AG_DIA
(NR_SEQ_SOLIC_PROP_ON_AG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SOLIC_PROP_ON_AG_DIA ADD (
  CONSTRAINT PLSSPAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_SOLIC_PROP_ON_AG_DIA ADD (
  CONSTRAINT PLSSPAD_PLSSPOA_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_PROP_ON_AG) 
 REFERENCES TASY.PLS_SOLIC_PROP_ON_AGENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SOLIC_PROP_ON_AG_DIA TO NIVEL_1;

