ALTER TABLE TASY.FLUXO_CAIXA_NOTIF_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FLUXO_CAIXA_NOTIF_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.FLUXO_CAIXA_NOTIF_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PERFIL_REGRA      NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FLUCXNOPE_PERFIL_FK_I ON TASY.FLUXO_CAIXA_NOTIF_PERFIL
(CD_PERFIL_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FLUCXNOPE_PK ON TASY.FLUXO_CAIXA_NOTIF_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FLUXO_CAIXA_NOTIF_PERFIL ADD (
  CONSTRAINT FLUCXNOPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FLUXO_CAIXA_NOTIF_PERFIL ADD (
  CONSTRAINT FLUCXNOPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_REGRA) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FLUXO_CAIXA_NOTIF_PERFIL TO NIVEL_1;

