ALTER TABLE TASY.CENTRO_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CENTRO_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CENTRO_CUSTO
(
  CD_CENTRO_CUSTO          NUMBER(8)            NOT NULL,
  DS_CENTRO_CUSTO          VARCHAR2(80 BYTE)    NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_TERCEIRO          NUMBER(10),
  CD_GRUPO                 NUMBER(10),
  CD_CLASSIFICACAO         VARCHAR2(40 BYTE),
  IE_TIPO                  VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_CENTRO_CUSTO_CONSUMO  NUMBER(8),
  CD_SISTEMA_CONTABIL      VARCHAR2(20 BYTE),
  IE_TIPO_CUSTO            VARCHAR2(3 BYTE),
  IE_PERIODO_PROJ_RECEITA  VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_FIM_CONTABIL          DATE,
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  CD_CENTRO_CUSTO_REF      NUMBER(8)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.CENTRO_CUSTO.CD_CENTRO_CUSTO IS 'Codigo do Centro de Custo';

COMMENT ON COLUMN TASY.CENTRO_CUSTO.DS_CENTRO_CUSTO IS 'Descri�ao do Centro de Custo';

COMMENT ON COLUMN TASY.CENTRO_CUSTO.IE_SITUACAO IS 'Situacao do Centro de Custo';


CREATE INDEX TASY.CENCUST_CENCUST_FK_I ON TASY.CENTRO_CUSTO
(CD_CENTRO_CUSTO_CONSUMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CENCUST_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CENCUST_CENCUST_FK2_I ON TASY.CENTRO_CUSTO
(CD_CENTRO_CUSTO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCUST_CTBGRCE_FK_I ON TASY.CENTRO_CUSTO
(CD_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CENCUST_CTBGRCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CENCUST_ESTABEL_FK_I ON TASY.CENTRO_CUSTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCUST_I1 ON TASY.CENTRO_CUSTO
(CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CENCUST_PK ON TASY.CENTRO_CUSTO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCUST_TERCEIR_FK_I ON TASY.CENTRO_CUSTO
(NR_SEQ_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CENCUST_TERCEIR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.centro_custo_atual
before insert or update ON TASY.CENTRO_CUSTO for each row
declare

qt_registro_w   number(10);

BEGIN
if      (wheb_usuario_pck.get_ie_executar_trigger       = 'S')  then

if (:new.dt_fim_contabil is not null) then
begin
        select  count(*)
        into    qt_registro_w
        from    ctb_saldo a,
                ctb_mes_ref b
        where   a.nr_seq_mes_ref = b.nr_sequencia
        and     b.dt_referencia >= :new.dt_fim_contabil
        and     a.cd_centro_custo = :new.cd_centro_custo
        and     a.vl_saldo <> 0;

        if (qt_registro_w <> 0) then
                wheb_mensagem_pck.exibir_mensagem_abort(1160571, 'DT_FIM_CONTABIL ='||:new.dt_fim_contabil);
        end if;
end;
end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.CENTRO_CUSTO_tp  after update ON TASY.CENTRO_CUSTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CENTRO_CUSTO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CENTRO_CUSTO,1,4000),substr(:new.DS_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CENTRO_CUSTO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TERCEIRO,1,4000),substr(:new.NR_SEQ_TERCEIRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TERCEIRO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO_CONSUMO,1,4000),substr(:new.CD_CENTRO_CUSTO_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO_CONSUMO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIFICACAO,1,4000),substr(:new.CD_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIFICACAO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_CONTABIL,1,4000),substr(:new.DT_FIM_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_CONTABIL',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_CONTABIL,1,4000),substr(:new.CD_SISTEMA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_CONTABIL',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CUSTO,1,4000),substr(:new.IE_TIPO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CUSTO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERIODO_PROJ_RECEITA,1,4000),substr(:new.IE_PERIODO_PROJ_RECEITA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERIODO_PROJ_RECEITA',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO,1,4000),substr(:new.CD_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO',ie_log_w,ds_w,'CENTRO_CUSTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CENTRO_CUSTO ADD (
  CHECK ( ie_situacao IN ( 'A' , 'I' )  ),
  CONSTRAINT CENCUST_PK
 PRIMARY KEY
 (CD_CENTRO_CUSTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CENTRO_CUSTO ADD (
  CONSTRAINT CENCUST_CENCUST_FK2 
 FOREIGN KEY (CD_CENTRO_CUSTO_REF) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CENCUST_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT CENCUST_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CENCUST_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO_CONSUMO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CENCUST_CTBGRCE_FK 
 FOREIGN KEY (CD_GRUPO) 
 REFERENCES TASY.CTB_GRUPO_CENTRO (CD_GRUPO));

GRANT SELECT ON TASY.CENTRO_CUSTO TO NIVEL_1;

