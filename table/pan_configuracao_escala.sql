ALTER TABLE TASY.PAN_CONFIGURACAO_ESCALA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAN_CONFIGURACAO_ESCALA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAN_CONFIGURACAO_ESCALA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESCALA        NUMBER(10),
  IE_QUEDA             VARCHAR2(1 BYTE),
  NR_SEQ_SETOR         NUMBER(10)               NOT NULL,
  NR_SEQ_SCORE_FLEX    NUMBER(10),
  QT_HORA              NUMBER(10),
  IE_ESCALA            VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PACOESC_PK ON TASY.PAN_CONFIGURACAO_ESCALA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PAN_CONFIGURACAO_ESCALA_tp  after update ON TASY.PAN_CONFIGURACAO_ESCALA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_QUEDA,1,500);gravar_log_alteracao(substr(:old.IE_QUEDA,1,4000),substr(:new.IE_QUEDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUEDA',ie_log_w,ds_w,'PAN_CONFIGURACAO_ESCALA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PAN_CONFIGURACAO_ESCALA ADD (
  CONSTRAINT PACOESC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PAN_CONFIGURACAO_ESCALA TO NIVEL_1;

