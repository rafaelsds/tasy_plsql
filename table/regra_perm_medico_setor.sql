ALTER TABLE TASY.REGRA_PERM_MEDICO_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PERM_MEDICO_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PERM_MEDICO_SETOR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_MED_CONV  NUMBER(10),
  CD_SETOR_ATENDIMENTO   NUMBER(5)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPEMES_ESTABEL_FK_I ON TASY.REGRA_PERM_MEDICO_SETOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEMES_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RPEMES_PK ON TASY.REGRA_PERM_MEDICO_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEMES_PK
  MONITORING USAGE;


CREATE INDEX TASY.RPEMES_RPEMECO_FK_I ON TASY.REGRA_PERM_MEDICO_SETOR
(NR_SEQ_REGRA_MED_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEMES_RPEMECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPEMES_SETATEN_FK_I ON TASY.REGRA_PERM_MEDICO_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEMES_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_PERM_MEDICO_SETOR ADD (
  CONSTRAINT RPEMES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PERM_MEDICO_SETOR ADD (
  CONSTRAINT RPEMES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RPEMES_RPEMECO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_MED_CONV) 
 REFERENCES TASY.REGRA_PERM_MEDICO_CONV (NR_SEQUENCIA),
  CONSTRAINT RPEMES_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.REGRA_PERM_MEDICO_SETOR TO NIVEL_1;

