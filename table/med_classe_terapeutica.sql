ALTER TABLE TASY.MED_CLASSE_TERAPEUTICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_CLASSE_TERAPEUTICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_CLASSE_TERAPEUTICA
(
  CD_CLASSE_TERAPEUTICA  VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_CLASSE_TERAPEUTICA  VARCHAR2(255 BYTE)     NOT NULL,
  CD_GRUPO_ANATOMICO     VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDCLTE_MEDGRAN_FK_I ON TASY.MED_CLASSE_TERAPEUTICA
(CD_GRUPO_ANATOMICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLTE_MEDGRAN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDCLTE_PK ON TASY.MED_CLASSE_TERAPEUTICA
(CD_CLASSE_TERAPEUTICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLTE_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_CLASSE_TERAPEUTICA ADD (
  CONSTRAINT MEDCLTE_PK
 PRIMARY KEY
 (CD_CLASSE_TERAPEUTICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_CLASSE_TERAPEUTICA ADD (
  CONSTRAINT MEDCLTE_MEDGRAN_FK 
 FOREIGN KEY (CD_GRUPO_ANATOMICO) 
 REFERENCES TASY.MED_GRUPO_ANATOMICO (CD_GRUPO_ANATOMICO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MED_CLASSE_TERAPEUTICA TO NIVEL_1;

