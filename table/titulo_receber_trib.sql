ALTER TABLE TASY.TITULO_RECEBER_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  TX_TRIBUTO           NUMBER(15,4),
  VL_TRIBUTO           NUMBER(15,2)             NOT NULL,
  VL_BASE_CALCULO      NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_TITULO            NUMBER(10)               NOT NULL,
  VL_SALDO             NUMBER(15,2),
  VL_TRIB_NAO_RETIDO   NUMBER(15,2)             NOT NULL,
  VL_BASE_NAO_RETIDO   NUMBER(15,2)             NOT NULL,
  VL_TRIB_ADIC         NUMBER(15,2)             NOT NULL,
  VL_BASE_ADIC         NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_NOTA_FISCAL   NUMBER(10),
  IE_ORIGEM_TRIBUTO    VARCHAR2(3 BYTE),
  DT_TRIBUTO           DATE,
  NR_SEQ_NF_TRIB       NUMBER(10),
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10),
  NR_SEQ_MENS_TRIB     NUMBER(10),
  DT_CONTABIL          DATE,
  NR_CODIGO_CONTROLE   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRETR_I1 ON TASY.TITULO_RECEBER_TRIB
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRETR_I1
  MONITORING USAGE;


CREATE INDEX TASY.TITRETR_NOTFITR_FK_I ON TASY.TITULO_RECEBER_TRIB
(NR_SEQ_NF_TRIB)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRETR_NOTFITR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITRETR_PK ON TASY.TITULO_RECEBER_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRETR_PLSMENT_FK_I ON TASY.TITULO_RECEBER_TRIB
(NR_SEQ_MENS_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRETR_PLSMENT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRETR_TITRECE_FK_I ON TASY.TITULO_RECEBER_TRIB
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRETR_TRAFINA_FK_I ON TASY.TITULO_RECEBER_TRIB
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRETR_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRETR_TRIBUTO_FK_I ON TASY.TITULO_RECEBER_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_trib_delete
before delete ON TASY.TITULO_RECEBER_TRIB for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TRT',:old.dt_tributo,'E',:old.nm_usuario);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_trib_integr
after insert or update ON TASY.TITULO_RECEBER_TRIB for each row
declare

qt_reg_w	number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_titulo is not null) then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		gerar_int_padrao.gravar_integracao('26', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
	end if;

end if;

if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TRT',:new.dt_tributo,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TRT',:new.dt_tributo,'A',:new.nm_usuario);
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Titulo_Receber_Trib_Update
BEFORE INSERT OR UPDATE ON TASY.TITULO_RECEBER_TRIB FOR EACH ROW
DECLARE
nr_interno_conta_w	number(10,0);
nr_seq_protocolo_w	number(10,0);
ie_digitar_imposto_w	varchar2(1) := 'N';


BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	nvl(max(nr_interno_conta),0),
	nvl(max(nr_seq_protocolo),0)
into	nr_interno_conta_w,
	nr_seq_protocolo_w
from	titulo_receber
where	nr_titulo	= :new.NR_TITULO;

Obter_Param_Usuario(801,93,wheb_usuario_pck.get_cd_perfil,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_digitar_imposto_w);

if	((nvl(:new.ie_origem_tributo,'C')  = 'D') and
	((nr_interno_conta_w > 0) or (nr_seq_protocolo_w > 0)) and
	(ie_digitar_imposto_w = 'N')) and (wheb_usuario_pck.get_cd_funcao <> 135) then
	-- Este tributo nao pode ser digitado pois o titulo e originado de uma conta ou um protocolo!
	wheb_mensagem_pck.exibir_mensagem_abort(267077);
end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_trib_after
after insert ON TASY.TITULO_RECEBER_TRIB for each row
declare

cd_estabelecimento_w	ctb_documento.cd_estabelecimento%type;
dt_movimento_w		ctb_documento.dt_competencia%type;

cursor c01 is
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 5
	and 	a.nm_atributo in ( 'VL_TRIB_RETIDO');

c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	cd_estabelecimento
into	cd_estabelecimento_w
from	titulo_receber
where	nr_titulo = :new.nr_titulo;

dt_movimento_w:= nvl(:new.dt_tributo,:new.dt_contabil);

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	if	(nvl(:new.vl_tributo, 0) <> 0) and (nvl(:new.nr_seq_nota_fiscal, 0) <> 0) then
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									trunc(dt_movimento_w),
									c01_w.cd_tipo_lote_contab,
									:new.nr_seq_trans_financ,
									45,
									:new.nr_titulo,
									:new.nr_sequencia,
									0,
									:new.vl_tributo,
									'TITULO_RECEBER_TRIB',
									c01_w.nm_atributo,
									:new.nm_usuario);

		end;
	end if;

	end;
end loop;
close c01;

end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_TRIB ADD (
  CONSTRAINT TITRETR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_TRIB ADD (
  CONSTRAINT TITRETR_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRETR_PLSMENT_FK 
 FOREIGN KEY (NR_SEQ_MENS_TRIB) 
 REFERENCES TASY.PLS_MENSALIDADE_TRIB (NR_SEQUENCIA),
  CONSTRAINT TITRETR_NOTFITR_FK 
 FOREIGN KEY (NR_SEQ_NF_TRIB) 
 REFERENCES TASY.NOTA_FISCAL_TRIB (NR_SEQ_INTERNO),
  CONSTRAINT TITRETR_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT TITRETR_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.TITULO_RECEBER_TRIB TO NIVEL_1;

