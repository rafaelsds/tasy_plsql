ALTER TABLE TASY.PRESCR_PROC_HOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_HOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_HOR
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_PRESCRICAO             NUMBER(14)          NOT NULL,
  NR_SEQ_PROCEDIMENTO       NUMBER(6)           NOT NULL,
  CD_PROCEDIMENTO           NUMBER(15)          NOT NULL,
  IE_ORIGEM_PROCED          NUMBER(10)          NOT NULL,
  NR_SEQ_PROC_INTERNO       NUMBER(10),
  DS_HORARIO                VARCHAR2(15 BYTE),
  DT_HORARIO                DATE,
  NR_OCORRENCIA             NUMBER(15,4),
  IE_URGENTE                VARCHAR2(1 BYTE)    NOT NULL,
  DT_FIM_HORARIO            DATE,
  DT_SUSPENSAO              DATE,
  IE_HORARIO_ESPECIAL       VARCHAR2(1 BYTE),
  CD_MATERIAL_EXAME         VARCHAR2(20 BYTE),
  QT_HOR_REAPRAZAMENTO      NUMBER(10),
  NM_USUARIO_REAPRAZAMENTO  VARCHAR2(15 BYTE),
  IE_APRAZADO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE),
  IE_CHECAGEM               VARCHAR2(1 BYTE),
  NM_USUARIO_CHECAGEM       VARCHAR2(15 BYTE),
  IE_DOSE_ESPECIAL          VARCHAR2(1 BYTE),
  NM_USUARIO_DOSE_ESP       VARCHAR2(15 BYTE),
  DT_CHECAGEM               DATE,
  CD_SETOR_EXEC             NUMBER(5),
  NM_USUARIO_ADM            VARCHAR2(15 BYTE),
  NM_USUARIO_SUSP           VARCHAR2(15 BYTE),
  NR_SEQ_PROCESSO           NUMBER(10),
  DT_INICIO_PROCED          DATE,
  DT_LIB_HORARIO            DATE,
  IE_PENDENTE               VARCHAR2(1 BYTE),
  NR_SEQ_PROC_ORIGEM        NUMBER(6),
  IE_STATUS_IVC             VARCHAR2(5 BYTE),
  NR_ETAPA                  NUMBER(5),
  DT_INICIO_HORARIO         DATE,
  DT_INTERRUPCAO            DATE,
  NM_USUARIO_INICIO         VARCHAR2(50 BYTE),
  NM_USUARIO_INTER          VARCHAR2(50 BYTE),
  NR_SEQ_HOR_GLIC           NUMBER(15),
  DS_STACK                  VARCHAR2(2000 BYTE),
  DT_PRIMEIRA_CHECAGEM      DATE,
  QT_VOL_HEMOCOMP           NUMBER(15),
  NR_GRUPO_INTEGRACAO       NUMBER(10),
  DT_INTEGRACAO             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREPROHO_ADEPROC_FK_I ON TASY.PRESCR_PROC_HOR
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_ADEPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPROHO_I1 ON TASY.PRESCR_PROC_HOR
(DT_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROHO_I2 ON TASY.PRESCR_PROC_HOR
(DT_HORARIO, CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_I2
  MONITORING USAGE;


CREATE INDEX TASY.PREPROHO_I3 ON TASY.PRESCR_PROC_HOR
(IE_PENDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_I3
  MONITORING USAGE;


CREATE INDEX TASY.PREPROHO_I4 ON TASY.PRESCR_PROC_HOR
(NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO, DT_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROHO_MATEXLA_FK_I ON TASY.PRESCR_PROC_HOR
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_MATEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREPROHO_PK ON TASY.PRESCR_PROC_HOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROHO_PRESPRO_FK_I ON TASY.PRESCR_PROC_HOR
(NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROHO_PROCEDI_FK_I ON TASY.PRESCR_PROC_HOR
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROHO_PROINTE_FK_I ON TASY.PRESCR_PROC_HOR
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPROHO_SETATEN_FK_I ON TASY.PRESCR_PROC_HOR
(CD_SETOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROHO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Prescr_proc_hor_Insert
BEFORE INSERT ON TASY.PRESCR_PROC_HOR FOR EACH ROW
DECLARE

BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_proc_hor_update
before update ON TASY.PRESCR_PROC_HOR for each row
declare

c_zero_date constant date := to_date('01-01-01 00:00:00', 'dd-mm-yy hh24:mi:ss');

cd_perfil_w						perfil.cd_perfil%type;
ie_log_dt_prev_execucao_w		varchar2(1 char);
ds_log_dt_prev_execucao_w	varchar2(1800 char);

begin

if	(nvl(:new.dt_horario,c_zero_date) <> nvl(:old.dt_horario,c_zero_date)) then
	cd_perfil_w := obter_perfil_ativo;

	ie_log_dt_prev_execucao_w := obter_se_info_rastre_prescr('D', wheb_usuario_pck.get_nm_usuario, cd_perfil_w, wheb_usuario_pck.get_cd_estabelecimento);

	if (ie_log_dt_prev_execucao_w = 'S') then
		ds_log_dt_prev_execucao_w := 'Rastreabilidade DT_PREV_EXECUCAO Alteracoes(old/new)=  NR_SEQ_PROCEDIMENTO('||:new.nr_seq_procedimento||')';

		ds_log_dt_prev_execucao_w := substr(ds_log_dt_prev_execucao_w || ' DT_HORARIO(' || to_char(:old.dt_horario, 'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_horario, 'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);

		ds_log_dt_prev_execucao_w := substr(ds_log_dt_prev_execucao_w || ' FUNCAO('||to_char(wheb_usuario_pck.get_cd_funcao)||'); PERFIL('||to_char(cd_perfil_w)||')',1,1800);

		gerar_log_prescricao(
			nr_prescricao_p		=> :new.nr_prescricao,
			nr_seq_item_p		=> :new.nr_seq_procedimento,
			ie_agrupador_p		=> null,
			nr_seq_horario_p	=> :new.nr_sequencia,
			ie_tipo_item_p		=> 'P',
			ds_log_p			=> ds_log_dt_prev_execucao_w,
			nm_usuario_p		=> :new.nm_usuario,
			nr_seq_objeto_p		=> 42283,
			ie_commit_p			=> 'N'
		);
	end if;
end if;

if	(:new.dt_horario <> :old.dt_horario) and
	(:old.dt_horario is not null) then
	:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SMART_PRESCR_PROC_HOR
after update or insert ON TASY.PRESCR_PROC_HOR FOR EACH ROW
declare
	REG_INTEGRACAO_W		  gerar_int_padrao.reg_integracao;
	NR_SEQ_PROT_GLIC_W        PRESCR_PROCEDIMENTO.NR_SEQ_PROT_GLIC%type;
	NR_SEQ_PROC_INTERNO_W     PRESCR_PROCEDIMENTO.NR_SEQ_PROC_INTERNO%type;
	NR_SEQ_EXAME_W            PRESCR_PROCEDIMENTO.NR_SEQ_EXAME%type;

BEGIN
begin
	select max(nr_seq_prot_glic),
		   max(nr_seq_proc_interno),
		   max(nr_seq_exame)
	into
		nr_seq_prot_glic_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w
	from prescr_procedimento
	where nr_sequencia = :new.nr_seq_procedimento
	and nr_prescricao  = :new.nr_prescricao;


	if 	(((((:old.DT_SUSPENSAO is null)
	       and(:new.DT_SUSPENSAO is not null))
		   or (:new.dt_horario 	<> :old.dt_horario))
		   and (UPDATING))
		   or (((NVL(:NEW.IE_APRAZADO,'N') = 'S')
		   and (INSERTING))))  then
		begin
			reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
			If (nr_seq_prot_glic_w is not null) then -- Glicemia
				gerar_int_padrao.gravar_integracao('328', :new.NR_PRESCRICAO , :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQ_PROCEDIMENTO || ';DS_OPERACAO=UPDATE');
			ELSIF ((Obter_dados_proc_interno(nr_seq_proc_interno_w,'TU') = 'E') or (obter_cod_exame_lab(nr_seq_exame_w) IS NOT NULL)) then  -- Exames
				gerar_int_padrao.gravar_integracao('310', :new.NR_PRESCRICAO , :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQ_PROCEDIMENTO || ';DS_OPERACAO=UPDATE');
			end if;
		END;
	end if;


exception when others then
	null;
end;

END;
/


ALTER TABLE TASY.PRESCR_PROC_HOR ADD (
  CONSTRAINT PREPROHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_HOR ADD (
  CONSTRAINT PREPROHO_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT PREPROHO_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREPROHO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PREPROHO_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PREPROHO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PREPROHO_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROC_HOR TO NIVEL_1;

