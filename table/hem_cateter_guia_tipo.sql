ALTER TABLE TASY.HEM_CATETER_GUIA_TIPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_CATETER_GUIA_TIPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_CATETER_GUIA_TIPO
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  DS_TIPO_CATETER  VARCHAR2(80 BYTE)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.HEMCATGT_PK ON TASY.HEM_CATETER_GUIA_TIPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMCATGT_PK
  MONITORING USAGE;


ALTER TABLE TASY.HEM_CATETER_GUIA_TIPO ADD (
  CONSTRAINT HEMCATGT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.HEM_CATETER_GUIA_TIPO TO NIVEL_1;

