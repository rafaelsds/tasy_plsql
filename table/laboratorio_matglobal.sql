ALTER TABLE TASY.LABORATORIO_MATGLOBAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LABORATORIO_MATGLOBAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.LABORATORIO_MATGLOBAL
(
  CD_LABORATORIO       VARCHAR2(10 BYTE)        NOT NULL,
  DS_LABORATORIO       VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LABOMGL_PK ON TASY.LABORATORIO_MATGLOBAL
(CD_LABORATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LABORATORIO_MATGLOBAL ADD (
  CONSTRAINT LABOMGL_PK
 PRIMARY KEY
 (CD_LABORATORIO));

GRANT SELECT ON TASY.LABORATORIO_MATGLOBAL TO NIVEL_1;

