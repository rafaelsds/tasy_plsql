ALTER TABLE TASY.PLS_REGRA_ICMS_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ICMS_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ICMS_PREST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  TX_AJUSTE            NUMBER(15,4)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  DT_FIM_VIGENCIA_REF  DATE,
  IE_TIPO_DESPESA      VARCHAR2(2 BYTE),
  IE_GENERICO          VARCHAR2(3 BYTE),
  IE_BRASINDICE        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRICMP_PK ON TASY.PLS_REGRA_ICMS_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_icms_prest_dt_ref
before insert or update ON TASY.PLS_REGRA_ICMS_PREST for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_ICMS_PREST_tp  after update ON TASY.PLS_REGRA_ICMS_PREST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_GENERICO,1,4000),substr(:new.IE_GENERICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GENERICO',ie_log_w,ds_w,'PLS_REGRA_ICMS_PREST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA,1,4000),substr(:new.IE_TIPO_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA',ie_log_w,ds_w,'PLS_REGRA_ICMS_PREST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE,1,4000),substr(:new.TX_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE',ie_log_w,ds_w,'PLS_REGRA_ICMS_PREST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_ICMS_PREST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_ICMS_PREST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_ICMS_PREST ADD (
  CONSTRAINT PLSRICMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_ICMS_PREST TO NIVEL_1;

