ALTER TABLE TASY.PLS_ATENDIMENTO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATENDIMENTO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATENDIMENTO_EVENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_ATENDIMENTO      NUMBER(10)            NOT NULL,
  IE_TIPO_OCORRENCIA      VARCHAR2(3 BYTE),
  NR_SEQ_EVENTO           NUMBER(10)            NOT NULL,
  DT_INICIO               DATE                  NOT NULL,
  IE_STATUS               VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_EVENTO           DATE,
  DT_CONCLUSAO            DATE,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO_OCORRENCIA  NUMBER(10)            NOT NULL,
  QT_EVENTO_REALIZADO     NUMBER(5),
  NR_SEQ_REGRA_TEMPO      NUMBER(10),
  NR_SEQ_CAPTACAO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSASEV_MPRCAPT_FK_I ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSASEV_PK ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSASEV_PLSATEN_FK_I ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSASEV_PLSEVOC_FK_I ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSASEV_PLSEVOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSASEV_PLSRTEO_FK_I ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQ_REGRA_TEMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSASEV_PLSRTEO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSASEV_PLSTIOC_FK_I ON TASY.PLS_ATENDIMENTO_EVENTO
(NR_SEQ_TIPO_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSASEV_PLSTIOC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_atendimento_evento_insert
before insert ON TASY.PLS_ATENDIMENTO_EVENTO for each row
declare

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_perfil_w			perfil.cd_perfil%type;
dt_conclusao_w			pls_atendimento.dt_conclusao%type;
ie_status_w			pls_atendimento.ie_status%type;
ie_param_21_w			varchar2(2);
ds_historico_w			varchar2(255);
nr_seq_tipo_historico_w		pls_tipo_historico_atend.nr_sequencia%type;

begin

begin
	select	dt_conclusao,
		ie_status
	into	dt_conclusao_w,
		ie_status_w
	from	pls_atendimento
	where	nr_sequencia = :new.nr_seq_atendimento;
exception
when others then
	dt_conclusao_w	:= null;
	ie_status_w	:= null;
end;

cd_perfil_w		:= somente_numero(wheb_usuario_pck.get_cd_perfil);
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
obter_param_usuario(1263, 21, cd_perfil_w, :new.nm_usuario, cd_estabelecimento_w, ie_param_21_w);

if	((dt_conclusao_w is not null) or (ie_status_w = 'C')) and -- "C" = Atendimento conclu�do
	(ie_param_21_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort('N�o � poss�vel inserir evento em um atendimento conclu�do!');
end if;

if	((dt_conclusao_w is not null) or (ie_status_w = 'C')) and -- "C" = Atendimento conclu�do
	(ie_param_21_w = 'S') then
	begin
		select	ds_valor_dominio
		into	ds_historico_w
		from	valor_dominio_v
		where	cd_dominio	= 3394
		and	vl_dominio	= 10
		and	ie_situacao	= 'A';
	exception
	when others then
		ds_historico_w	:= 'Inseriu ocorr�ncia no atendimento finalizado.';
	end;

	ds_historico_w	:= ds_historico_w || chr(13) || 'Usu�rio: ' || :new.nm_usuario || chr(13) || 'Data\hora: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if	(pls_obter_se_controle_estab('GA') = 'S') then
		select	max(nr_sequencia)
		into	nr_seq_tipo_historico_w
		from	pls_tipo_historico_atend
		where	ie_gerado_sistema	= 'S'
		and	ie_situacao		= 'A'
		and	(cd_estabelecimento = cd_estabelecimento_w );
	else
		select	max(nr_sequencia)
		into	nr_seq_tipo_historico_w
		from	pls_tipo_historico_atend
		where	ie_gerado_sistema	= 'S'
		and	ie_situacao		= 'A';
	end if;

	insert	into pls_atendimento_historico
		(nr_sequencia, nr_seq_atendimento, ds_historico_long,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, nr_seq_tipo_historico, dt_historico,
		ie_origem_historico, ie_gerado_sistema)
	values	(pls_atendimento_historico_seq.nextval, :new.nr_seq_atendimento, ds_historico_w,
		sysdate, :new.nm_usuario, sysdate,
		:new.nm_usuario, nr_seq_tipo_historico_w, sysdate,
		10, 'S');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_atendimento_evento_atual
after insert or update ON TASY.PLS_ATENDIMENTO_EVENTO for each row
declare
ds_historico_w			Varchar2(4000)	:= 'X';
nm_evento_new_w			Varchar2(255);
nm_evento_old_w			Varchar2(255);
nr_seq_tipo_historico_w		pls_tipo_historico_atend.nr_sequencia%type;

begin
select	max(nr_sequencia)
into	nr_seq_tipo_historico_w
from	pls_tipo_historico_atend
where	ie_gerado_sistema	= 'S'
and	ie_situacao		= 'A';

if 	(nvl(nr_seq_tipo_historico_w,0)	> 0) then
	if 	(inserting) then
		select	ds_valor_dominio
		into	ds_historico_w
		from	valor_dominio
		where	cd_dominio	= 3394
		and	vl_dominio	= '2' --Novo evento
		and	ie_situacao	= 'A';

		select	nm_evento
		into	nm_evento_new_w
		from	pls_evento_ocorrencia
		where   nr_sequencia 	= :new.nr_seq_evento;

		ds_historico_w	:= ds_historico_w||': '||nm_evento_new_w;
	elsif 	(updating) then
		if 	(nvl(:old.nr_seq_evento, 0) <> nvl(:new.nr_seq_evento, 0)) then
			select	ds_valor_dominio
			into	ds_historico_w
			from	valor_dominio
			where	cd_dominio	= 3394
			and	vl_dominio	= '12' --Altera��o de evento
			and	ie_situacao	= 'A';

			select	nm_evento
			into	nm_evento_new_w
			from	pls_evento_ocorrencia
			where   nr_sequencia 	= :new.nr_seq_evento;

			select	nm_evento
			into	nm_evento_old_w
			from	pls_evento_ocorrencia
			where   nr_sequencia 	= :old.nr_seq_evento;

			ds_historico_w	:= ds_historico_w||': o evento '||nm_evento_old_w||' foi alterado para '||nm_evento_new_w;
		end if;
	end if;
	if 	(ds_historico_w <> 'X') then
		insert	into	pls_atendimento_historico(	nr_sequencia, nr_seq_atendimento, ds_historico_long,
								dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
								nm_usuario_nrec, nr_seq_tipo_historico, dt_historico,
								ie_origem_historico, ie_gerado_sistema)
			values				(	pls_atendimento_historico_seq.nextval, :new.nr_seq_atendimento, ds_historico_w,
								sysdate, :new.nm_usuario, sysdate,
								:new.nm_usuario, nr_seq_tipo_historico_w, :new.dt_atualizacao,
								'12', 'S');
	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_ATENDIMENTO_EVENTO ADD (
  CONSTRAINT PLSASEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATENDIMENTO_EVENTO ADD (
  CONSTRAINT PLSASEV_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSASEV_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSASEV_PLSEVOC_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSASEV_PLSTIOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_OCORRENCIA) 
 REFERENCES TASY.PLS_TIPO_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSASEV_PLSRTEO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TEMPO) 
 REFERENCES TASY.PLS_REGRA_TEMPO_EVENT_OCOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATENDIMENTO_EVENTO TO NIVEL_1;

