ALTER TABLE TASY.REG_AREA_CUSTOMER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_AREA_CUSTOMER CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_AREA_CUSTOMER
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_AREA                VARCHAR2(255 BYTE),
  DT_APROVACAO           DATE,
  NR_SEQ_APRESENTACAO    NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_OBERVACAO           VARCHAR2(4000 BYTE),
  NR_SEQ_INTENCAO_USO    NUMBER(10),
  DS_DESCRICAO           VARCHAR2(4000 BYTE),
  NR_SEQ_ESTAGIO         NUMBER(10),
  CD_VERSAO              VARCHAR2(255 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO   VARCHAR2(255 BYTE),
  NM_USUARIO_APROVACAO   VARCHAR2(15 BYTE),
  DT_LIBERACAO           DATE,
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGARC_PK ON TASY.REG_AREA_CUSTOMER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGARC_REGINU_FK_I ON TASY.REG_AREA_CUSTOMER
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_area_customer_after
after insert or update ON TASY.REG_AREA_CUSTOMER for each row
declare

ie_tipo_alteracao_w		reg_area_customer_hist.ie_tipo_alteracao%type;

begin

if (inserting) then
	ie_tipo_alteracao_w := 'I';
elsif	(updating) and
	(:new.ie_situacao = 'I') then
	ie_tipo_alteracao_w := 'E';
else
	ie_tipo_alteracao_w := 'A';
end if;

insert into reg_area_customer_hist (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_area,
	dt_aprovacao,
	nr_seq_apresentacao,
	ie_situacao,
	cd_versao,
	ds_obervacao,
	nr_seq_reg_version_rev,
	ds_descricao,
	nr_seq_intencao_uso,
	nr_seq_area_customer,
	nr_seq_estagio,
	ie_tipo_alteracao)
values (
	reg_area_customer_hist_seq.nextval,
	:new.dt_atualizacao,
	:new.nm_usuario,
	:new.dt_atualizacao_nrec,
	:new.nm_usuario_nrec,
	:new.ds_area,
	:new.dt_aprovacao,
	:new.nr_seq_apresentacao,
	:new.ie_situacao,
	:new.cd_versao,
	:new.ds_obervacao,
	null,
	:new.ds_descricao,
	:new.nr_seq_intencao_uso,
	:new.nr_sequencia,
	:new.nr_seq_estagio,
	ie_tipo_alteracao_w);

end;
/


ALTER TABLE TASY.REG_AREA_CUSTOMER ADD (
  CONSTRAINT REGARC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_AREA_CUSTOMER ADD (
  CONSTRAINT REGARC_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_AREA_CUSTOMER TO NIVEL_1;

