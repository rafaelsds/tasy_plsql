DROP TABLE TASY.COMPL_PESSOA_FISICA_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPL_PESSOA_FISICA_SETOR
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_COMPL_PESSOA_FISICA  NUMBER(3),
  NR_SEQ_EMPRESA_REF_SETOR    NUMBER(10),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPEFISE_COMPEFI_FK_I ON TASY.COMPL_PESSOA_FISICA_SETOR
(CD_PESSOA_FISICA, NR_SEQ_COMPL_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPEFISE_EMPREFSET_FK_I ON TASY.COMPL_PESSOA_FISICA_SETOR
(NR_SEQ_EMPRESA_REF_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.COMPL_PESSOA_FISICA_SETOR ADD (
  CONSTRAINT COPEFISE_COMPEFI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA, NR_SEQ_COMPL_PESSOA_FISICA) 
 REFERENCES TASY.COMPL_PESSOA_FISICA (CD_PESSOA_FISICA,NR_SEQUENCIA),
  CONSTRAINT COPEFISE_EMPREFSET_FK 
 FOREIGN KEY (NR_SEQ_EMPRESA_REF_SETOR) 
 REFERENCES TASY.EMPRESA_REF_SETOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMPL_PESSOA_FISICA_SETOR TO NIVEL_1;

