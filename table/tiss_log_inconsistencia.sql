DROP TABLE TASY.TISS_LOG_INCONSISTENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_LOG_INCONSISTENCIA
(
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_INCONSISTENCIA  NUMBER(10)             NOT NULL,
  NR_SEQUENCIA_AUTOR     NUMBER(10),
  NR_INTERNO_CONTA       NUMBER(10),
  NR_SEQ_PROTOCOLO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSLOIN_ESTABEL_FK_I ON TASY.TISS_LOG_INCONSISTENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSLOIN_TISSINC_FK_I ON TASY.TISS_LOG_INCONSISTENCIA
(NR_SEQ_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TISS_LOG_INCONSISTENCIA ADD (
  CONSTRAINT TISSLOIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSLOIN_TISSINC_FK 
 FOREIGN KEY (NR_SEQ_INCONSISTENCIA) 
 REFERENCES TASY.TISS_INCONSISTENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TISS_LOG_INCONSISTENCIA TO NIVEL_1;

