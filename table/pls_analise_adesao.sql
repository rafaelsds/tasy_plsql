ALTER TABLE TASY.PLS_ANALISE_ADESAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ANALISE_ADESAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ANALISE_ADESAO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  NR_SEQ_PESSOA_PROPOSTA    NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  DT_ANALISE                DATE                NOT NULL,
  IE_STATUS                 VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_PROPOSTA           NUMBER(10),
  DT_ENCERRAMENTO           DATE,
  NM_USUARIO_ANALISE        VARCHAR2(15 BYTE),
  NR_SEQ_PERITO             NUMBER(10),
  DT_ATENDIMENTO            DATE,
  DT_SOLICITACAO_INCLUSAO   DATE,
  NR_SEQ_SEGURADO           NUMBER(10),
  NR_SEQ_PROP_BENEF_ONLINE  NUMBER(10),
  NR_SEQ_PROP_ONLINE        NUMBER(10),
  NR_SEQ_STATUS_INTERNO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSAADE_ESTABEL_FK_I ON TASY.PLS_ANALISE_ADESAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAADE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAADE_PESFISI_FK_I ON TASY.PLS_ANALISE_ADESAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSAADE_PK ON TASY.PLS_ANALISE_ADESAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAADE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAADE_PLSANPE_FK_I ON TASY.PLS_ANALISE_ADESAO
(NR_SEQ_PERITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAADE_PLSANPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAADE_PLSPBON_FK_I ON TASY.PLS_ANALISE_ADESAO
(NR_SEQ_PROP_BENEF_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAADE_PLSPROB_FK_I ON TASY.PLS_ANALISE_ADESAO
(NR_SEQ_PESSOA_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAADE_PLSPROB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAADE_PLSPRON_FK_I ON TASY.PLS_ANALISE_ADESAO
(NR_SEQ_PROP_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAADE_PLSSIAD_FK_I ON TASY.PLS_ANALISE_ADESAO
(NR_SEQ_STATUS_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SSJ_INSERIR_HIST_ANALISE
AFTER INSERT
ON TASY.PLS_ANALISE_ADESAO 

FOR EACH ROW
DECLARE

nr_seq_proposta_w number(10);
nr_seq_analise_w number(10);

cursor c_historico is
    select  DS_HISTORICO,
            nm_usuario
    from PLS_PROPOSTA_HISTORICO
    where nr_seq_proposta = nr_seq_proposta_w;
v_historico c_historico%rowtype;    



BEGIN

    nr_seq_proposta_w:= :new.nr_seq_proposta;
    nr_seq_analise_w:= :new.nr_sequencia;
    
    
    if(nr_seq_proposta_w is not null and nr_seq_analise_w is not null)then
        
        open c_historico;
        
            loop
            fetch c_historico into v_historico;
            exit when c_historico%notfound;
            
                insert into PLS_ANALISE_HISTORICO
                (
                    nr_sequencia,
                    nr_seq_analise,
                    cd_estabelecimento,
                    dt_historico,
                    nm_usuario_historico,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    ds_titulo,
                    ds_historico
                )
                values
                (
                    
                    PLS_ANALISE_HISTORICO_SEQ.nextval,  --nr_sequencia,
                    nr_seq_analise_w,                   --nr_seq_analise,
                    2,                                  --cd_estabelecimento,
                    sysdate,                            --dt_historico,
                    v_historico.nm_usuario,             --nm_usuario_historico,
                    sysdate,                            --dt_atualizacao,
                    v_historico.nm_usuario,             --nm_usuario,
                    sysdate,                            --dt_atualizacao_nrec,
                    v_historico.nm_usuario,             --nm_usuario_nrec,
                    'Histórico da Proposta',            --ds_titulo,
                    v_historico.ds_historico            --ds_historico,
                );

            
            end loop;
        
        close c_historico;
    end if;

 
END SSJ_INSERIR_HIST_ANALISE;
/


ALTER TABLE TASY.PLS_ANALISE_ADESAO ADD (
  CONSTRAINT PLSAADE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ANALISE_ADESAO ADD (
  CONSTRAINT PLSAADE_PLSSIAD_FK 
 FOREIGN KEY (NR_SEQ_STATUS_INTERNO) 
 REFERENCES TASY.PLS_STATUS_INTERNO_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSAADE_PLSPRON_FK 
 FOREIGN KEY (NR_SEQ_PROP_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSAADE_PLSPBON_FK 
 FOREIGN KEY (NR_SEQ_PROP_BENEF_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_BENEF_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSAADE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSAADE_PLSPROB_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSAADE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSAADE_PLSANPE_FK 
 FOREIGN KEY (NR_SEQ_PERITO) 
 REFERENCES TASY.PLS_ANALISE_PERITO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ANALISE_ADESAO TO NIVEL_1;

