ALTER TABLE TASY.GCA_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GCA_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GCA_ATENDIMENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  NR_SEQ_GRADACAO            NUMBER(10),
  QT_PONTUACAO               NUMBER(3)          NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  CD_PERFIL_ATIVO            NUMBER(5),
  CD_SETOR_PACIENTE          NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_AUTOR                   VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_PRESCR              NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GCAATEN_ATEPACI_FK_I ON TASY.GCA_ATENDIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_GCAGRAD_FK_I ON TASY.GCA_ATENDIMENTO
(NR_SEQ_GRADACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_I1 ON TASY.GCA_ATENDIMENTO
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_PEPRESC_FK_I ON TASY.GCA_ATENDIMENTO
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_PERFIL_FK_I ON TASY.GCA_ATENDIMENTO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GCAATEN_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GCAATEN_PESFISI_FK_I ON TASY.GCA_ATENDIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GCAATEN_PK ON TASY.GCA_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_SETATEN_FK_I ON TASY.GCA_ATENDIMENTO
(CD_SETOR_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAATEN_TASASDI_FK_I ON TASY.GCA_ATENDIMENTO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GCAATEN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GCAATEN_TASASDI_FK2_I ON TASY.GCA_ATENDIMENTO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GCAATEN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.gca_atendimento_aftupdate
after update ON TASY.GCA_ATENDIMENTO for each row
declare

qt_reg_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin
	Gerar_Evolucao_Escala_ECA(:new.nr_sequencia, :new.nm_usuario);
	exception
		when others then
		null;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.GCA_ATENDIMENTO_ATUAL
before insert or update ON TASY.GCA_ATENDIMENTO for each row
declare

cd_setor_atendimento_w	Number(05,0);
begin
-- OS 209826 - Wilbert
--if	(:new.cd_setor_paciente is  null) then
--	:new.cd_setor_paciente	:= obter_setor_Atendimento(:new.nr_atendimento);
--end if;
if	(:new.cd_setor_paciente is  null) then
	select	cd_setor_atendimento
	into	cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno = (
		select Obter_Atepacu_paciente(:new.nr_atendimento, 'IAA')
		from dual);
	:new.cd_setor_paciente	:= cd_setor_atendimento_w;
end if;



end;
/


CREATE OR REPLACE TRIGGER TASY.gca_atendimento_pend_atual
after insert or update ON TASY.GCA_ATENDIMENTO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'4');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.GCA_ATENDIMENTO_delete
after delete ON TASY.GCA_ATENDIMENTO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '4'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.gca_atendimento_update
before update ON TASY.GCA_ATENDIMENTO for each row
declare
qt_reg_w	number(1);
cd_setor_atendimento_w	number(10);
qt_idade_alerta_w		number(15);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
begin



begin
cd_setor_atendimento_w	:= nvl(obter_setor_atendimento(:new.nr_atendimento),0);
exception
	when others then
	cd_setor_atendimento_w	:= 0;

end;

qt_idade_alerta_w	:= nvl(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),sysdate,'DIA'),0);

select	nr_sequencia
into	:new.nr_seq_gradacao
from	gca_gradacao
where	:new.qt_pontuacao between vl_inicial and vl_final
and		nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
and		ie_situacao = 'A'
and		qt_idade_alerta_w	between nvl(OBTER_IDADE_GCA_GRAD(nr_sequencia,'MIN'),0) 	and	nvl(OBTER_IDADE_GCA_GRAD(nr_sequencia,'MAX'),9999999)
and		(((ie_autor is null) or (:new.ie_autor is null)) or (:new.ie_autor = ie_autor));
exception
	when others then
		:new.nr_seq_gradacao	:= null;

end;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.GCA_ATENDIMENTO ADD (
  CONSTRAINT GCAATEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GCA_ATENDIMENTO ADD (
  CONSTRAINT GCAATEN_GCAGRAD_FK 
 FOREIGN KEY (NR_SEQ_GRADACAO) 
 REFERENCES TASY.GCA_GRADACAO (NR_SEQUENCIA),
  CONSTRAINT GCAATEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT GCAATEN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GCAATEN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT GCAATEN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_PACIENTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT GCAATEN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT GCAATEN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT GCAATEN_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.GCA_ATENDIMENTO TO NIVEL_1;

