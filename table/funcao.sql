ALTER TABLE TASY.FUNCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO
(
  CD_FUNCAO                  NUMBER(5)          NOT NULL,
  DS_FUNCAO                  VARCHAR2(80 BYTE)  NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_HINT                    VARCHAR2(120 BYTE),
  DS_CAPTION                 VARCHAR2(15 BYTE),
  DS_FORM                    VARCHAR2(10 BYTE),
  DS_APLICACAO               VARCHAR2(10 BYTE),
  NR_SEQ_MODULO              NUMBER(10)         NOT NULL,
  NR_SEQ_IMPLANTACAO         NUMBER(5),
  NR_SEQ_MOD_IMPL            NUMBER(10),
  IE_REGRA_FUNCIONAMENTO     VARCHAR2(1 BYTE)   NOT NULL,
  DS_ACTION                  VARCHAR2(255 BYTE),
  PR_FUNCAO_MODULO           NUMBER(15,2)       NOT NULL,
  DS_MODULO_WEB              VARCHAR2(30 BYTE),
  NR_INDICE_IMAGEM           NUMBER(5),
  IE_SITUACAO_WEB            VARCHAR2(1 BYTE),
  IE_LIB_CLIENTE             VARCHAR2(1 BYTE),
  IE_MAXIMIZAR               VARCHAR2(1 BYTE)   NOT NULL,
  IE_SITUACAO_SWING          VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_FUNCAO_SWING            NUMBER(5),
  IE_DIC_OBJETO              VARCHAR2(1 BYTE),
  IE_RELEVANTE_SBIS          VARCHAR2(1 BYTE),
  IE_BLOQUEADA               VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  IE_CLASSIF_PRODUTO         VARCHAR2(1 BYTE),
  QT_TEMPO_TREINAMENTO_EAD   NUMBER(5),
  QT_TEMPO_TREINAMENTO_PRES  NUMBER(5),
  QT_TEMPO_PARAMETRIZACAO    VARCHAR2(2 BYTE),
  QT_TEMPO_PARAM_PEQ         NUMBER(5),
  QT_TEMPO_PARAM_MED         NUMBER(5),
  QT_TEMPO_PARAM_GRA         NUMBER(5),
  QT_TEMPO_PARAM_MOD         NUMBER(5),
  IE_CONFIRMA_FECHAR         VARCHAR2(1 BYTE),
  NR_SEQ_TREINAMENTO         NUMBER(10),
  QT_TEMPO_PARAM_GG          NUMBER(5),
  DS_CAMINHO_ICONE           VARCHAR2(255 BYTE),
  IE_SITUACAO_IOS            VARCHAR2(1 BYTE),
  DS_JUST_MIGRACAO           VARCHAR2(255 BYTE),
  IE_VERSAO_USO              VARCHAR2(2 BYTE),
  DT_ATUALIZACAO_PARAM       DATE,
  NR_SEQ_ORDEM_SERV          NUMBER(10),
  IE_TIPO_FUNCAO_IMPL        VARCHAR2(15 BYTE),
  DS_DESCRICAO_FUNCAO        VARCHAR2(2000 BYTE),
  CD_EXP_FUNCAO              NUMBER(10),
  IE_COMPLEXIDADE            VARCHAR2(1 BYTE),
  IE_REV_DESIGN_FLUXO        VARCHAR2(1 BYTE),
  IE_MOBILE                  VARCHAR2(1 BYTE),
  IE_CLASSIF_COMERCIAL       VARCHAR2(5 BYTE),
  IE_FORMA_APRESENTACAO      VARCHAR2(1 BYTE),
  IE_UTILITARIO_HTML         VARCHAR2(1 BYTE),
  DS_ROTA                    VARCHAR2(255 BYTE),
  IE_UTILIZA_MACRO           VARCHAR2(1 BYTE),
  IE_GESTAO_CONHECIMENTO     VARCHAR2(1 BYTE),
  IE_LAZY_LOAD               VARCHAR2(1 BYTE),
  IE_STATUS_HTML             VARCHAR2(1 BYTE),
  IE_EXCLUSIVA_PHILIPS       VARCHAR2(15 BYTE),
  IE_SCHEMATIC_ARQUIVO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.FUNCAO.CD_FUNCAO IS 'Codico da Funcao';

COMMENT ON COLUMN TASY.FUNCAO.DS_FUNCAO IS 'Descricao da Funcao';

COMMENT ON COLUMN TASY.FUNCAO.IE_SITUACAO IS 'Indicador da Situacao da Funcao';

COMMENT ON COLUMN TASY.FUNCAO.DT_ATUALIZACAO IS 'Data Atualizacao';

COMMENT ON COLUMN TASY.FUNCAO.NM_USUARIO IS 'Nome do Usuario Liberador';


CREATE INDEX TASY.FUNCAO_APLTASY_FK_I ON TASY.FUNCAO
(DS_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCAO_DICEXPR_FK_I ON TASY.FUNCAO
(CD_EXP_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCAO_I2 ON TASY.FUNCAO
(DS_FORM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCAO_I2_I ON TASY.FUNCAO
(CD_FUNCAO_SWING)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCAO_MODIMPL_FK_I ON TASY.FUNCAO
(NR_SEQ_MOD_IMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCAO_MODTASY_FK_I ON TASY.FUNCAO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNCAO_PK ON TASY.FUNCAO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNCAO_UK ON TASY.FUNCAO
(DS_CAMINHO_ICONE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNCAO_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.funcao_insert 
AFTER INSERT ON funcao 
FOR EACH ROW
DECLARE 
 
ie_tipo_cliente_w		Varchar2(1)	:= 'P'; 
 
BEGIN 
 
/* Fun��es da OPS */ 
if	(:new.nr_seq_modulo in (721, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1032)) then 
	ie_tipo_cliente_w	:= 'O'; 
end if; 
 
insert into funcao_net 
	(nr_sequencia, cd_funcao, ie_tipo_cliente, 
	dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
	nm_usuario_nrec) 
values(	funcao_net_seq.nextval, :new.cd_funcao, ie_tipo_cliente_w, 
	sysdate, :new.nm_usuario, sysdate, 
	:new.nm_usuario); 
 
END;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.FUNCAO FOR EACH ROW
DECLARE

ds_user_w varchar2(100);

qt_porcentagem_w	number(15,2);

BEGIN
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then

if	(nvl(:new.pr_funcao_modulo,0) <> nvl(:old.pr_funcao_modulo,0)) then
	begin
	qt_porcentagem_w	:= (nvl(:new.pr_funcao_modulo,0) - nvl(:old.pr_funcao_modulo,0));

	update	com_cliente_mod_impl a
	set	a.pr_utilizacao	= a.pr_utilizacao + qt_porcentagem_w
	where	exists (	select	1
				from	com_cliente_fun_impl b
				where	b.nr_seq_mod_impl	= a.nr_sequencia
				and	b.cd_funcao		= :new.cd_funcao);
	end;
end if;

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.funcao_atual_historico
before insert or update ON TASY.FUNCAO for each row
declare

ds_expressao_br_w	DIC_EXPRESSAO.ds_expressao_br%type;
cd_expressao_w		DIC_EXPRESSAO.cd_expressao%type;
nm_coluna_desc_w	varchar2(50);
nm_coluna_exp_w		varchar2(50);

begin

begin
if	(:new.CD_EXP_FUNCAO is not null or
	 :old.CD_EXP_FUNCAO is not null) then

	nm_coluna_desc_w	:= 'DS_FUNCAO';
	nm_coluna_exp_w		:= 'CD_EXP_FUNCAO';
	cd_expressao_w		:= :new.CD_EXP_FUNCAO;
	ds_expressao_br_w	:= obter_desc_expressao_br(cd_expressao_w);
	:new.ds_funcao 	:= ds_expressao_br_w;
end if;
exception when others then
	-- Caso a expressao nao couber no campo, avisa usu�rio
	if	(sqlcode = -6502) then
		wheb_mensagem_pck.exibir_mensagem_abort(290787, 'CD_EXPRESSAO_P='||cd_expressao_w||
								';DS_ATRIBUTO_P='||nm_coluna_desc_w||
								';NM_ATRIBUTO_EXP_P='||nm_coluna_exp_w);
	end if;
end;

if	(inserting) then
	begin
	insert into funcao_historico (
		cd_funcao,
		ds_funcao,
		ie_situacao_old,
		ie_situacao_new,
		ie_situacao_swing_old,
		ie_situacao_swing_new,
		ie_acao,
		nm_usuario,
		dt_atualizacao)
	values (
		:new.cd_funcao,
		:new.ds_funcao,
		null,
		:new.ie_situacao,
		null,
		:new.ie_situacao_swing,
		'I',
		:new.nm_usuario,
		sysdate);
	end;
elsif	(updating) then
	begin
	insert into funcao_historico (
		cd_funcao,
		ds_funcao,
		ie_situacao_old,
		ie_situacao_new,
		ie_situacao_swing_old,
		ie_situacao_swing_new,
		ie_acao,
		nm_usuario,
		dt_atualizacao)
	values (
		:new.cd_funcao,
		:new.ds_funcao,
		:old.ie_situacao,
		:new.ie_situacao,
		:old.ie_situacao_swing,
		:new.ie_situacao_swing,
		'U',
		:new.nm_usuario,
		sysdate);
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_tp  after update ON TASY.FUNCAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_FUNCAO);  ds_c_w:=null; ds_w:=substr(:new.DS_FUNCAO,1,500);gravar_log_alteracao(substr(:old.DS_FUNCAO,1,4000),substr(:new.DS_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_FUNCAO',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INDICE_IMAGEM,1,4000),substr(:new.NR_INDICE_IMAGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_INDICE_IMAGEM',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIB_CLIENTE,1,4000),substr(:new.IE_LIB_CLIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIB_CLIENTE',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_WEB,1,4000),substr(:new.IE_SITUACAO_WEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_WEB',ie_log_w,ds_w,'FUNCAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FUNCAO ADD (
  CONSTRAINT FUNCAO_PK
 PRIMARY KEY
 (CD_FUNCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT FUNCAO_UK
 UNIQUE (DS_CAMINHO_ICONE)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO ADD (
  CONSTRAINT FUNCAO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_FUNCAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT FUNCAO_MODTASY_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.MODULO_TASY (NR_SEQUENCIA),
  CONSTRAINT FUNCAO_MODIMPL_FK 
 FOREIGN KEY (NR_SEQ_MOD_IMPL) 
 REFERENCES TASY.MODULO_IMPLANTACAO (NR_SEQUENCIA),
  CONSTRAINT FUNCAO_APLTASY_FK 
 FOREIGN KEY (DS_APLICACAO) 
 REFERENCES TASY.APLICACAO_TASY (CD_APLICACAO_TASY),
  CONSTRAINT FUNCAO_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO_SWING) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO));

GRANT SELECT ON TASY.FUNCAO TO NIVEL_1;

