ALTER TABLE TASY.LOTE_AUDIT_HIST_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_AUDIT_HIST_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_AUDIT_HIST_ITEM
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_LOTE                 NUMBER(10)        NOT NULL,
  NR_SEQ_CONPACI_RET          NUMBER(10),
  DT_HISTORICO                DATE              NOT NULL,
  VL_HISTORICO                NUMBER(15,2),
  NR_SEQ_HIST_AUDIT           NUMBER(10),
  NR_SEQ_RET_ITEM             NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DT_REAPRESENTACAO           DATE,
  DT_RECEB_PREV               DATE,
  CD_MOTIVO_GLOSA             NUMBER(5),
  CD_PROCESSO_RECEB           VARCHAR2(20 BYTE),
  CD_RESPOSTA                 NUMBER(10),
  NR_SEQ_RET_GLOSA            NUMBER(10),
  IE_STATUS                   VARCHAR2(5 BYTE),
  VL_PAGO                     NUMBER(15,2),
  VL_SALDO                    NUMBER(15,2),
  NR_SEQ_GUIA                 NUMBER(10)        NOT NULL,
  NR_SEQ_PROPACI              NUMBER(10),
  NR_SEQ_MATPACI              NUMBER(10),
  VL_GLOSA                    NUMBER(15,2),
  VL_AMENOR                   NUMBER(15,2),
  QT_ITEM                     NUMBER(9,3)       NOT NULL,
  NR_SEQ_PROPACI_PARTIC       NUMBER(10),
  NR_SEQ_MATPACI_PARTIC       NUMBER(10),
  CD_SETOR_RESPONSAVEL        NUMBER(5),
  IE_ACAO_GLOSA               VARCHAR2(1 BYTE),
  VL_SALDO_AMENOR             NUMBER(15,4),
  VL_GLOSA_INFORMADA          NUMBER(15,2),
  DS_COMPLEMENTO              VARCHAR2(4000 BYTE),
  VL_ADICIONAL                NUMBER(15,2),
  NR_SEQ_PARTIC               NUMBER(10),
  IE_AJUSTADO                 VARCHAR2(1 BYTE),
  IE_TIPO_GLOSA               VARCHAR2(1 BYTE),
  IE_PAGO_CONVENIO            VARCHAR2(1 BYTE),
  CD_MOTIVO_GLOSA_TISS        VARCHAR2(10 BYTE),
  CD_MATERIAL_ATEND_PACIENTE  NUMBER(10),
  CD_MATERIAL_ATEND_TISS      VARCHAR2(20 BYTE),
  CD_ITEM_CONVERSAO           VARCHAR2(20 BYTE),
  CD_PROCEDIMENTO_PAC         NUMBER(15),
  IE_ORIGEM_PROCED_PAC        NUMBER(10),
  DT_ITEM                     DATE,
  DT_CONTA_ITEM               DATE,
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_AUTORIZACAO              VARCHAR2(20 BYTE),
  NR_INTERNO_CONTA            NUMBER(10),
  CD_ITEM_TUSS                NUMBER(15),
  IE_FORMA_AGRUPAMENTO        VARCHAR2(1 BYTE),
  VL_ACATADO                  NUMBER(15,2),
  DS_JUSTIFICATIVA_OPER       VARCHAR2(255 BYTE),
  NR_DOC_CONV_ITEM_CONTA      VARCHAR2(20 BYTE),
  IE_ITEM_SEM_RESPOSTA        VARCHAR2(2 BYTE),
  NR_ATENDIMENTO_ITEM         NUMBER(10),
  CD_PESSOA_FISICA_ITEM       VARCHAR2(10 BYTE),
  NM_PESSOA_FISICA_ITEM       VARCHAR2(60 BYTE),
  DS_ITEM_CONVENIO            VARCHAR2(255 BYTE),
  DS_ITEM_TISS                VARCHAR2(255 BYTE),
  DS_ITEM_TUSS                VARCHAR2(255 BYTE),
  DT_INTEGRACAO               DATE,
  NM_USUARIO_RESP             VARCHAR2(15 BYTE),
  IE_FUNCAO_MEDICO            VARCHAR2(10 BYTE),
  NR_SEQ_JUSTIFICATIVA        NUMBER(10),
  NM_USUARIO_PREVISTO         VARCHAR2(15 BYTE),
  DT_ANALISE                  DATE,
  NR_SEQUENCIA_ITEM           NUMBER(4)         DEFAULT null,
  NR_PROTOCOLO_OPERADORA      VARCHAR2(20 BYTE),
  DT_ATUAL_PROTOC_OPERADORA   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOAUHIT_ATEND_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_ATENDIMENTO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_CDTUSS_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_ITEM_TUSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_CODPF_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_PESSOA_FISICA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_CONPACI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_CONPAGU_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_INTERNO_CONTA, CD_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_CONREIT_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_RET_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_CONREIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_COPARET_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_CONPACI_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_COPARET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_CORETGL_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_RET_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_CORETGL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_DSTISS_I ON TASY.LOTE_AUDIT_HIST_ITEM
(UPPER("DS_ITEM_TISS"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_DSTUSS_I ON TASY.LOTE_AUDIT_HIST_ITEM
(UPPER("DS_ITEM_TUSS"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_GUIA_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_HIAUCOPA_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_HIST_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_HIAUCOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_HISTITJUST_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_ITEMCONV_I ON TASY.LOTE_AUDIT_HIST_ITEM
(UPPER("DS_ITEM_CONVENIO"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_LOTAUDGUI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_LOTAUHI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_MATERIA_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_MATERIAL_ATEND_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_MATPACI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_MATPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_MATPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_MATPACI_FK2_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_MATPACI_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_MATPACI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_MOGLOSA_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_MOGLOSA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_MOREGLO_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_RESPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_MOREGLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_NMPAC_I ON TASY.LOTE_AUDIT_HIST_ITEM
(UPPER("NM_PESSOA_FISICA_ITEM"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOAUHIT_PK ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_PROCEDI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_PROCEDIMENTO_PAC, IE_ORIGEM_PROCED_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_PROPACI_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_PROPACI_FK2_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_PROPACI_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_PROPACI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_PROPART_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NR_SEQ_PROPACI, NR_SEQ_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_PROPART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_SETATEN_FK_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_SETOR_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUHIT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUHIT_SETATEN_FK2_I ON TASY.LOTE_AUDIT_HIST_ITEM
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_USUARIO_PREVISTO_I ON TASY.LOTE_AUDIT_HIST_ITEM
(NM_USUARIO_PREVISTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOAUHIT_USUARIO_RESP ON TASY.LOTE_AUDIT_HIST_ITEM
(NM_USUARIO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOTE_AUDIT_HIST_ITEM_DELETE
BEFORE DELETE ON TASY.LOTE_AUDIT_HIST_ITEM FOR EACH ROW
DECLARE

begin
/* 	s� permite excluir se os itens n�o estiverem revertidos, para n�o perder o v�nvulo com as contas revertidas, por causa dos relat�rios */
if	(:old.nr_seq_propaci_partic is not null or :old.nr_seq_matpaci_partic is not null) then
	--R.aise_application_error(-20011, 'Este item est� em revers�o e n�o pode ser exclu�do!');
	wheb_mensagem_pck.exibir_mensagem_abort(263409);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.lote_audit_hist_item_update
before update ON TASY.LOTE_AUDIT_HIST_ITEM for each row
declare

ie_possui_partic_w	varchar2(1);
var_consistir_pago	varchar2(1);
vl_pago_w		lote_audit_hist_item.vl_pago%type;

begin

	if	((Nvl(:new.vl_acatado,0) > 0) and
		((nvl(:new.vl_glosa_informada,0) <> nvl(:old.vl_glosa_informada,0)) or
		(nvl(:new.vl_amenor,0) <> nvl(:old.vl_amenor,0)) or
		(nvl(:new.vl_glosa,0) <> nvl(:old.vl_glosa,0)) or
		(nvl(:new.vl_acatado,0) <> nvl(:old.vl_acatado,0)))) then
		if (nvl(:new.vl_glosa_informada,0) <> (nvl(:new.vl_amenor,0) + nvl(:new.vl_glosa,0) + nvl(:new.vl_acatado,0))) then
			wheb_mensagem_pck.exibir_mensagem_abort(1041992);
		end if;
	end if;

	if (nvl(:old.ie_acao_glosa, 0) <> nvl(:new.ie_acao_glosa, 0)) then

		vl_pago_w := 0;

		obter_param_usuario(69, 266, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, var_consistir_pago);

		select	max(ie_possui)
		into	ie_possui_partic_w
		from	(
				select 	'S' ie_possui
				from	grg_proc_partic a
				where	a.nr_seq_hist_item = :new.nr_sequencia
				union
				select  'N' ie_possui
				from	grg_proc_partic a
				where	:new.nr_sequencia not in (a.nr_seq_hist_item)
			);

		if	var_consistir_pago = 'N' and nvl(:new.nr_seq_ret_glosa, 0) > 0 then
			vl_pago_w := :new.vl_pago;
		end if;

		if ((:new.vl_glosa = 0 and :new.vl_amenor = 0) or ie_possui_partic_w = 'N') then
			if :new.ie_acao_glosa = 'R' then
				:new.vl_amenor 	:=  :new.vl_glosa_informada - vl_pago_w;
				:new.vl_glosa 	:=  0;
			elsif :new.ie_acao_glosa = 'A' then
				:new.vl_amenor 	:=  0;
				:new.vl_glosa 	:=  :new.vl_glosa_informada - vl_pago_w;
			end if;
		end if;

	end if;

	if 	(nvl(:old.ie_acao_glosa, 0) <> :new.ie_acao_glosa)
		or (nvl(:old.ds_observacao, '') <> :new.ds_observacao)
		or (nvl(:old.ds_justificativa_oper, '') <> :new.ds_justificativa_oper) then
		:new.nm_usuario_resp := nvl(:new.nm_usuario, wheb_usuario_pck.get_nm_usuario);
	end if;

	if	(nvl(:new.NR_PROTOCOLO_OPERADORA, '0') <> nvl(:old.NR_PROTOCOLO_OPERADORA, '0')) then
		:new.DT_ATUAL_PROTOC_OPERADORA := sysdate;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_AUDIT_HIST_ITEM_INSERT
before insert ON TASY.LOTE_AUDIT_HIST_ITEM for each row
declare

ie_tipo_glosa_w		varchar2(10);
cd_convenio_w		number(10);
cd_estabelecimento_w	number(10);
cd_motivo_glosa_w	number(10);
ie_acao_glosa_w		varchar2(1);
cd_setor_resp_w		number(5);
cd_resposta_w		number(10);


begin

/*lhalves OS 585937 em 11/05/2013 - Buscar motivo de glosa conforme regra, baseado no valor de glosa informado*/
if	(:new.cd_motivo_glosa is null) and
	(nvl(:new.vl_glosa_informada,0) <> 0) then

	select	max(a.cd_convenio),
		max(a.cd_estabelecimento)
	into	cd_convenio_w,
		cd_estabelecimento_w
	from	lote_auditoria a,
		lote_audit_hist b,
		lote_audit_hist_guia c
	where	a.nr_sequencia	= b.nr_seq_lote_audit
	and	b.nr_sequencia	= c.nr_seq_lote_hist
	and	c.nr_sequencia	= :new.nr_seq_guia;

	OBTER_MOTIVO_GLOSA_RECURSO(cd_estabelecimento_w,cd_convenio_w,:new.vl_glosa_informada,cd_motivo_glosa_w);

	if	(cd_motivo_glosa_w is not null) then

		select	max(a.ie_acao_glosa) ie_acao_glosa,
			max(a.cd_setor_resp) cd_setor_resp,
			max(a.cd_resposta) cd_resposta,
			max(a.ie_tipo_glosa) ie_tipo_glosa
		into	ie_acao_glosa_w,
			cd_setor_resp_w,
			cd_resposta_w,
			ie_tipo_glosa_w
		from	motivo_glosa a
		where	a.cd_motivo_glosa = cd_motivo_glosa_w;

		:new.ie_acao_glosa		:= ie_acao_glosa_w;
		:new.cd_setor_responsavel	:= cd_setor_resp_w;
		:new.cd_resposta		:= cd_resposta_w;
		:new.ie_tipo_glosa		:= ie_tipo_glosa_w;
		:new.cd_motivo_glosa		:= cd_motivo_glosa_w;

	end if;

end if;
/*FIM lhalves OS 585937*/

if	(:new.cd_motivo_glosa is not null) then

	select	max(ie_tipo_glosa)
	into	ie_tipo_glosa_w
	from	motivo_glosa
	where	cd_motivo_glosa	= :new.cd_motivo_glosa;

	:new.ie_tipo_glosa	:= ie_tipo_glosa_w;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_AUDIT_HIST_ITEM_tp  after update ON TASY.LOTE_AUDIT_HIST_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_GLOSA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_GLOSA,1,4000),substr(:new.IE_TIPO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GLOSA',ie_log_w,ds_w,'LOTE_AUDIT_HIST_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOTE_AUDIT_HIST_ITEM ADD (
  CONSTRAINT LOAUHIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_AUDIT_HIST_ITEM ADD (
  CONSTRAINT LOAUHIT_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT LOAUHIT_CONPAGU_FK 
 FOREIGN KEY (NR_INTERNO_CONTA, CD_AUTORIZACAO) 
 REFERENCES TASY.CONTA_PACIENTE_GUIA (NR_INTERNO_CONTA,CD_AUTORIZACAO),
  CONSTRAINT LOAUHIT_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LOAUHIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL_ATEND_PACIENTE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT LOAUHIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_PAC, IE_ORIGEM_PROCED_PAC) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT LOAUHIT_HISTITJUST_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.LOTE_HIST_ITEM_JUSTIF (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_LOTAUDGUI_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.LOTE_AUDIT_HIST_GUIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LOAUHIT_PROPART_FK 
 FOREIGN KEY (NR_SEQ_PROPACI, NR_SEQ_PARTIC) 
 REFERENCES TASY.PROCEDIMENTO_PARTICIPANTE (NR_SEQUENCIA,NR_SEQ_PARTIC),
  CONSTRAINT LOAUHIT_MATPACI_FK2 
 FOREIGN KEY (NR_SEQ_MATPACI_PARTIC) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_PROPACI_FK2 
 FOREIGN KEY (NR_SEQ_PROPACI_PARTIC) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_RESPONSAVEL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LOAUHIT_CORETGL_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_HIAUCOPA_FK 
 FOREIGN KEY (NR_SEQ_HIST_AUDIT) 
 REFERENCES TASY.HIST_AUDIT_CONTA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_COPARET_FK 
 FOREIGN KEY (NR_SEQ_CONPACI_RET) 
 REFERENCES TASY.CONTA_PACIENTE_RETORNO (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_RET_ITEM) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_MOGLOSA_FK 
 FOREIGN KEY (CD_MOTIVO_GLOSA) 
 REFERENCES TASY.MOTIVO_GLOSA (CD_MOTIVO_GLOSA),
  CONSTRAINT LOAUHIT_MOREGLO_FK 
 FOREIGN KEY (CD_RESPOSTA) 
 REFERENCES TASY.MOTIVO_RESP_GLOSA (CD_RESPOSTA),
  CONSTRAINT LOAUHIT_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LOAUHIT_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT LOAUHIT_MATPACI_FK 
 FOREIGN KEY (NR_SEQ_MATPACI) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_AUDIT_HIST_ITEM TO NIVEL_1;

