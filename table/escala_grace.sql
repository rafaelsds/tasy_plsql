ALTER TABLE TASY.ESCALA_GRACE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_GRACE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_GRACE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  QT_IDADE                  NUMBER(3)           NOT NULL,
  QT_FREQ_CARDIACA          NUMBER(3)           NOT NULL,
  QT_PA_SISTOLICA           NUMBER(3)           NOT NULL,
  QT_CREATININA             NUMBER(6,2)         NOT NULL,
  IE_KILLIP                 VARCHAR2(3 BYTE)    NOT NULL,
  QT_PONTUACAO              NUMBER(3),
  IE_MARCADOR_CARDIACO_ELE  VARCHAR2(1 BYTE)    NOT NULL,
  IE_PARADA_CARDIACA_ADM    VARCHAR2(1 BYTE)    NOT NULL,
  IE_DESVIO_SEG_ST          VARCHAR2(1 BYTE)    NOT NULL,
  NR_HORA                   NUMBER(2),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCGRAC_ATEPACI_FK_I ON TASY.ESCALA_GRACE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCGRAC_I1 ON TASY.ESCALA_GRACE
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCGRAC_PESFISI_FK_I ON TASY.ESCALA_GRACE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCGRAC_PK ON TASY.ESCALA_GRACE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_grace_atual
before insert or update ON TASY.ESCALA_GRACE for each row
declare

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

:new.qt_pontuacao	:= 0;


if	(:new.qt_idade	<30) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  0;
elsif	(:new.qt_idade	>=30) and
	(:new.qt_idade	< 40) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  8;
elsif	(:new.qt_idade	>=40) and
	(:new.qt_idade	< 50) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  25;
elsif	(:new.qt_idade	>=50) and
	(:new.qt_idade	< 60) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  41;
elsif	(:new.qt_idade	>=60) and
	(:new.qt_idade	< 70) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  58;
elsif	(:new.qt_idade	>=70) and
	(:new.qt_idade	< 80) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  75;
elsif	(:new.qt_idade	>=80) and
	(:new.qt_idade	< 90) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  91;
elsif	(:new.qt_idade	>=90) then
	:new.qt_pontuacao	:=:new.qt_pontuacao +  100;
end if;

if	(:new.ie_killip	= 'I') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 0;
elsif	(:new.ie_killip	= 'II') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 20;
elsif	(:new.ie_killip	= 'III') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 39;
elsif	(:new.ie_killip	= 'IV') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 59;
end if;


if	(:new.QT_PA_SISTOLICA	<=80) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 58;
elsif	(:new.QT_PA_SISTOLICA	>=80) and
	(:new.qt_pa_sistolica	<=99)then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 53;
elsif	(:new.QT_PA_SISTOLICA	>=100) and
	(:new.qt_pa_sistolica	<=119)then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 43;
elsif	(:new.QT_PA_SISTOLICA	>=120) and
	(:new.qt_pa_sistolica	<=139)then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 34;
elsif	(:new.QT_PA_SISTOLICA	>=140) and
	(:new.qt_pa_sistolica	<=159)then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 24;
elsif	(:new.QT_PA_SISTOLICA	>=160) and
	(:new.qt_pa_sistolica	<=199)then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 10;
end if;


if	(:new.QT_FREQ_CARDIACA	<= 50) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 0;
elsif	(:new.QT_FREQ_CARDIACA	>=50) and
	(:new.qt_freq_cardiaca <=69) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
elsif	(:new.QT_FREQ_CARDIACA	>=70) and
	(:new.qt_freq_cardiaca <=89) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 9;
elsif	(:new.QT_FREQ_CARDIACA	>=90) and
	(:new.qt_freq_cardiaca <=109) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 15;
elsif	(:new.QT_FREQ_CARDIACA	>=110) and
	(:new.qt_freq_cardiaca <=149) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 24;
elsif	(:new.QT_FREQ_CARDIACA	>=150) and
	(:new.qt_freq_cardiaca <=199) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 38;
elsif	(:new.QT_FREQ_CARDIACA	>=200)  then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 46;
end if;

if	(:new.QT_CREATININA	>=0) and
	(:new.QT_CREATININA	<= 0.39) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
elsif	(:new.QT_CREATININA	>=0.40) and
	(:new.QT_CREATININA	<= 0.79) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 4;
elsif	(:new.QT_CREATININA	>=0.80) and
	(:new.QT_CREATININA	<= 1.19) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 7;
elsif	(:new.QT_CREATININA	>=1.20) and
	(:new.QT_CREATININA	<= 1.59) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 10;
elsif	(:new.QT_CREATININA	>=1.60) and
	(:new.QT_CREATININA	<= 1.99) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 13;
elsif	(:new.QT_CREATININA	>=2.00) and
	(:new.QT_CREATININA	<= 3.99) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 21;
elsif	(:new.QT_CREATININA	>=4) then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 28;

end if;

if	(:new.ie_marcador_cardiaco_ele	= 'S') then
	:new.qt_pontuacao	:=:new.qt_pontuacao	+ 14;
end if;

if	(:new.IE_PARADA_CARDIACA_ADM	= 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 39;
end if;

if	(:new.IE_DESVIO_SEG_ST	= 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 28;
end if;
end;
/


ALTER TABLE TASY.ESCALA_GRACE ADD (
  CONSTRAINT ESCGRAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_GRACE ADD (
  CONSTRAINT ESCGRAC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCGRAC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_GRACE TO NIVEL_1;

