ALTER TABLE TASY.PROJ_RAT_ATIV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_RAT_ATIV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_RAT_ATIV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RAT           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_INICIO_ATIV       DATE                     NOT NULL,
  QT_MIN_ATIV          NUMBER(15),
  DS_ATIVIDADE         VARCHAR2(4000 BYTE)      NOT NULL,
  NR_SEQ_ETAPA_CRON    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_FIM_ATIV          DATE,
  IE_ATIVIDADE_EXTRA   VARCHAR2(1 BYTE),
  QT_MIN_INTERVALO     NUMBER(15)               NOT NULL,
  IE_CLASSIFICACAO     VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_ACORDO_ADIC   NUMBER(10),
  IE_TIPO_ATIVIDADE    VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(2000 BYTE),
  IE_RESP_ATIV_EXTRA   VARCHAR2(15 BYTE),
  IE_TEMPO_EXCEDIDO    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRORAAT_PK ON TASY.PROJ_RAT_ATIV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRORAAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRORAAT_PROACAD_FK_I ON TASY.PROJ_RAT_ATIV
(NR_SEQ_ACORDO_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRORAAT_PROACAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRORAAT_PROCRET_FK_I ON TASY.PROJ_RAT_ATIV
(NR_SEQ_ETAPA_CRON)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRORAAT_PROCRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRORAAT_PROJRAT_FK_I ON TASY.PROJ_RAT_ATIV
(NR_SEQ_RAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRORAAT_PROJRAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_rat_ativ_valor_delete
before delete ON TASY.PROJ_RAT_ATIV for each row
declare
qt_hora_old_w		number(15,2);
qt_hora_adic_old_w	number(15,2);
qt_hora_w		number(15,2);
qt_hora_adic_w		number(15,2);

begin
if	(:old.nr_seq_etapa_cron is not null) then
	begin
	select	nvl(qt_hora_real,0),
		nvl(qt_horas_etapa_adic,0)
	into	qt_hora_old_w,
		qt_hora_adic_old_w
	from	proj_cron_etapa
	where	nr_sequencia = :old.nr_seq_etapa_cron;

	select	nvl((nvl(qt_hora_real,0) - dividir(nvl(:old.qt_min_ativ,0),60)),0)
	into	qt_hora_w
	from	proj_cron_etapa
	where	nr_sequencia = :old.nr_seq_etapa_cron;

	if	(:old.ie_atividade_extra = 'S') then
		qt_hora_adic_w := nvl(round(dividir(:old.qt_min_ativ,60),2),0);
	end if;

	if(nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
		update	proj_cron_etapa
		set		qt_hora_real = nvl(qt_hora_w,0),
				qt_horas_etapa_adic = qt_hora_adic_w - nvl(qt_hora_adic_w,0)
		where	nr_sequencia = :old.nr_seq_etapa_cron;
	end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.proj_rat_ativ_atual
before insert or update ON TASY.PROJ_RAT_ATIV for each row
declare
qt_existe_w		number(10);
dt_inicio_rat_w		date;
dt_final_rat_w		date;

begin
qt_existe_w	:= 0;

select	trunc(dt_inicio),
	trunc(dt_final)
into	dt_inicio_rat_w,
	dt_final_rat_w
from	proj_rat
where	nr_sequencia = :new.nr_seq_rat;

if	(:new.dt_inicio_ativ is not null) and
	(trunc(:new.dt_inicio_ativ) < dt_inicio_rat_w) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262015); -- A data in�cio da atividade n�o pode ser menor que a data de in�cio do RAT !
end if;

if	(:new.dt_fim_ativ is not null) and
	(trunc(:new.dt_fim_ativ) > dt_final_rat_w) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262016); -- A data final da atividade n�o pode ser maior que a data final do RAT !
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.proj_rat_ativ_valor
BEFORE INSERT or UPDATE ON TASY.PROJ_RAT_ATIV FOR EACH ROW
declare
qt_hora_w 		number(15,2) := 0;
qt_hora_adic_w		number(15,2) := 0;
qt_hora_old_w		number(15,2) := 0;
qt_hora_adic_old_w	number(15,2) := 0;

begin
if	(:new.nr_seq_etapa_cron is not null) then
	begin
	if	(updating) and
		(:old.qt_min_ativ <> :new.qt_min_ativ) then
		begin
		select	nvl(qt_hora_real,0),
			nvl(qt_horas_etapa_adic,0)
		into	qt_hora_old_w,
			qt_hora_adic_old_w
		from	proj_cron_etapa
		where	nr_sequencia = :old.nr_seq_etapa_cron;

		select	nvl((nvl(qt_hora_real,0) - dividir(nvl(:new.qt_min_ativ,0) - nvl(:old.qt_min_ativ,0),60)),0)
		into	qt_hora_w
		from	proj_cron_etapa
		where	nr_sequencia = :old.nr_seq_etapa_cron;

		if	(:new.ie_atividade_extra = 'S') then
			qt_hora_adic_w := nvl(round(dividir(:new.qt_min_ativ,60),2),0);
		end if;

		update	proj_cron_etapa
		set	qt_hora_real = (qt_hora_old_w - nvl(qt_hora_w,0)) + qt_hora_old_w,
			qt_horas_etapa_adic = qt_hora_adic_w - nvl(qt_hora_adic_w,0)
		where	nr_sequencia = :new.nr_seq_etapa_cron;
		end;
	elsif	(inserting) then
		begin
		select	nvl((nvl(qt_hora_real,0) + dividir(nvl(:new.qt_min_ativ,0),60)),0)
		into	qt_hora_w
		from	proj_cron_etapa
		where	nr_sequencia = :new.nr_seq_etapa_cron;

		if	(:new.ie_atividade_extra = 'S') then
			qt_hora_adic_w := nvl(round(dividir(:new.qt_min_ativ,60),2),0);
		end if;

		update	proj_cron_etapa
		set	qt_hora_real = qt_hora_w,
			qt_horas_etapa_adic = nvl(qt_hora_adic_w,0)
		where	nr_sequencia = :new.nr_seq_etapa_cron;
		end;
	end if;
	end;

        -- Palliative solution to zero the activities seconds
        :new.dt_fim_ativ := :new.dt_fim_ativ + NUMTODSINTERVAL(0 - EXTRACT(SECOND FROM CAST(:new.dt_fim_ativ AS TIMESTAMP)),'SECOND');
        :new.dt_inicio_ativ := :new.dt_inicio_ativ + NUMTODSINTERVAL(0 - EXTRACT(SECOND FROM CAST(:new.dt_inicio_ativ AS TIMESTAMP)),'SECOND');

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PROJ_RAT_ATIV_tp  after update ON TASY.PROJ_RAT_ATIV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_RAT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_RAT,1,4000),substr(:new.NR_SEQ_RAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RAT',ie_log_w,ds_w,'PROJ_RAT_ATIV',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PROJ_RAT_ATIV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROJ_RAT_ATIV ADD (
  CONSTRAINT PRORAAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_RAT_ATIV ADD (
  CONSTRAINT PRORAAT_PROJRAT_FK 
 FOREIGN KEY (NR_SEQ_RAT) 
 REFERENCES TASY.PROJ_RAT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRORAAT_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_CRON) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT PRORAAT_PROACAD_FK 
 FOREIGN KEY (NR_SEQ_ACORDO_ADIC) 
 REFERENCES TASY.PROJ_ACORDO_ADICIONAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_RAT_ATIV TO NIVEL_1;

