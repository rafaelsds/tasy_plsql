ALTER TABLE TASY.PLS_FRANQ_PAG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FRANQ_PAG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FRANQ_PAG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_REFERENCIA        DATE                     NOT NULL,
  DT_GERACAO_LOTE      DATE,
  DT_LIBERACAO_LOTE    DATE,
  DT_FECHAMENTO_LOTE   DATE,
  DS_LOTE              VARCHAR2(255 BYTE),
  NR_SEQ_LOTE_PGTO     NUMBER(10),
  NR_SEQ_PP_LOTE_PGTO  NUMBER(10),
  IE_FUNCAO_PAGAMENTO  VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSFRPA_ESTABEL_FK_I ON TASY.PLS_FRANQ_PAG
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSFRPA_PK ON TASY.PLS_FRANQ_PAG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFRPA_PLSLOPA_FK_I ON TASY.PLS_FRANQ_PAG
(NR_SEQ_LOTE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFRPA_PPPLOTE_FK_I ON TASY.PLS_FRANQ_PAG
(NR_SEQ_PP_LOTE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_franq_pag_atual
before insert or update ON TASY.PLS_FRANQ_PAG for each row
declare

ie_status_w	pls_lote_pagamento.ie_status%type;

begin

if	(:new.nr_seq_lote_pgto is not null) and
	(nvl(:old.nr_seq_lote_pgto, -1) <> :new.nr_seq_lote_pgto) then
	select	max(ie_status)
	into	ie_status_w
	from	pls_lote_pagamento
	where	nr_sequencia	= :new.nr_seq_lote_pgto;

	if	(ie_status_w <> 'P') then
		wheb_mensagem_pck.exibir_mensagem_abort(329060); --  O lote de pagamento de produ��o m�dica informado deve estar com status provis�rio!
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_FRANQ_PAG ADD (
  CONSTRAINT PLSFRPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FRANQ_PAG ADD (
  CONSTRAINT PLSFRPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSFRPA_PLSLOPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PGTO) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSFRPA_PPPLOTE_FK 
 FOREIGN KEY (NR_SEQ_PP_LOTE_PGTO) 
 REFERENCES TASY.PLS_PP_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FRANQ_PAG TO NIVEL_1;

