ALTER TABLE TASY.PFCS_LOCATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_LOCATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_LOCATION
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  ID_LOCATION             VARCHAR2(255 BYTE),
  DS_NAME                 VARCHAR2(255 BYTE),
  DS_LOCATION             VARCHAR2(4000 BYTE),
  CD_PHYSICAL_TYPE        VARCHAR2(30 BYTE),
  CD_OPERATIONAL_STATUS   VARCHAR2(30 BYTE),
  SI_STATUS               VARCHAR2(30 BYTE),
  NR_SEQ_LOCATION_SUP     NUMBER(10),
  NR_SEQ_ORGANIZATION     NUMBER(10),
  DS_DEPARTMENT           VARCHAR2(255 BYTE),
  CD_DEPARTMENT           VARCHAR2(5 BYTE),
  UNIT                    VARCHAR2(255 BYTE),
  DS_ROOM                 VARCHAR2(255 BYTE),
  DS_BED                  VARCHAR2(255 BYTE),
  DS_PHYSICAL_TYPE        VARCHAR2(255 BYTE),
  SYS_PHYSICAL_TYPE       VARCHAR2(255 BYTE),
  DS_OPERATIONAL_STATUS   VARCHAR2(255 BYTE),
  SYS_OPERATIONAL_STATUS  VARCHAR2(255 BYTE),
  NR_SEQ_ADDRESS          NUMBER(10,1)          DEFAULT null,
  CD_TYPE                 VARCHAR2(15 BYTE),
  DS_TYPE                 VARCHAR2(255 BYTE),
  NR_ESI                  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCSLOCAL_PFCSADDR_FK_I ON TASY.PFCS_LOCATION
(NR_SEQ_ADDRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSLOCAL_PFCSLOCAL_FK_I ON TASY.PFCS_LOCATION
(NR_SEQ_LOCATION_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSLOCAL_PFCSORGA_FK_I ON TASY.PFCS_LOCATION
(NR_SEQ_ORGANIZATION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSLOCAL_PK ON TASY.PFCS_LOCATION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PFCS_UPDATE_SETOR_UNIDADE
BEFORE INSERT OR UPDATE ON TASY.PFCS_LOCATION FOR EACH ROW
DECLARE
-- Constants
nm_usuario_w                unidade_atendimento.nm_usuario%type := 'PFCS';

-- Dynamic variables
cd_setor_atendimento_w      setor_atendimento.cd_setor_atendimento%type;
cd_estabelecimento_w        setor_atendimento.cd_estabelecimento%type;
cd_classif_setor_w          setor_atendimento.cd_classif_setor%type;
ie_semi_intensiva_w         setor_atendimento.ie_semi_intensiva%type := null;
cd_tipo_acomodacao_w        tipo_acomodacao.cd_tipo_acomodacao%type;
nr_seq_interno_w            unidade_atendimento.nr_seq_interno%type;
ie_status_unidade_w         unidade_atendimento.ie_status_unidade%type;
ie_status_unidade_ant_w     unidade_atendimento.ie_status_ant_unidade%type;
ie_situacao_w               unidade_atendimento.ie_situacao%type := 'A';
dt_sanitization_w           unidade_atendimento.dt_higienizacao%type := null;
ds_log_w                    pfcs_org_structure_log.ds_log%type;
qt_reg_w                    number(1);

/*
    Trigger responsible for inserting departments and beds when needed.
    Information will come from PFCS_LOCATION table.
*/

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    -- http://hl7.org/fhir/valueset-location-status.html
    if (upper(:new.si_status) in ('ACTIVE', 'INACTIVE')) then
        ie_situacao_w := substr(upper(:new.si_status), 1, 1);
    elsif (upper(:new.si_status) = 'SUSPENDED') then
        :new.si_status := 'ACTIVE';
        :new.cd_operational_status := 'C';
    end if;

    ie_status_unidade_w := pfcs_get_bed_mapping(:new.cd_operational_status, 'F');

    if (ie_status_unidade_w = 'H') then
        dt_sanitization_w := sysdate;
    end if;

    IF INSERTING THEN
        -- Getting the establishment code
        select max(cd_estabelecimento)
        into cd_estabelecimento_w
        from pfcs_organization
        where nr_sequencia = :new.nr_seq_organization;

        if (cd_estabelecimento_w is not null) then
            -- Checking if the department already exists
            select nvl(max(cd_setor_atendimento),0)
            into cd_setor_atendimento_w
            from setor_atendimento
            where upper(ds_setor_atendimento) = upper(:new.ds_department)
                and cd_estabelecimento_base = cd_estabelecimento_w
                and ie_situacao = 'A';

            if (cd_setor_atendimento_w = 0) then
                /************* SETOR_ATENDIMENTO *************/
                select (nvl(max(cd_setor_atendimento),0)+1)
                into cd_setor_atendimento_w
                from setor_atendimento;

                cd_classif_setor_w :=
                    -- http://hl7.org/fhir/v3/ServiceDeliveryLocationRoleType/vs.html
                    -- Table: valor_dominio / cd_dominio = 1
                    -- 1 = Emergency Department / 2 = Surgical Center
                    -- 3 = Normal Unit (inpatient) / 4 = Critical Care (ICU)
                    -- 9 = Pediatric Unit
                    CASE
                        WHEN upper(:new.cd_type) IN ('ER', 'ETU') THEN '1'
                        WHEN upper(:new.cd_type) IN ('NS', 'CRS', 'OMS', 'SU', 'PLS', 'OR') THEN '2'
                        --WHEN upper(:new.cd_type) IN ('HU', 'RHU') THEN '3'
                        WHEN upper(:new.cd_type) IN ('MEDSURG', 'ICU', 'PEDICU', 'PEDNICU') THEN '4'
                        WHEN upper(:new.cd_type) IN ('PEDU') THEN '9'
                        ELSE '3'
                    END;

                if (upper(:new.cd_type) = 'MEDSURG') then
                    ie_semi_intensiva_w := 'S';
                end if;

                insert into
                setor_atendimento (
                    CD_ESTABELECIMENTO_BASE, -- FK ESTABELECIMENTO
                    CD_ESTABELECIMENTO,
                    CD_SETOR_ATENDIMENTO,
                    CD_CLASSIF_SETOR, -- Domain
                    DS_SETOR_ATENDIMENTO,
                    DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    IE_ADEP,
                    IE_CIH,
                    IE_CONTA,
                    IE_ESTERILIZACAO,
                    IE_LOC_PEP,
                    IE_OCUP_HOSPITALAR,
                    IE_PROPRIO,
                    IE_SITUACAO,
                    NM_UNIDADE_BASICA,
                    NM_USUARIO,
                    NM_USUARIO_NREC,
                    DS_DESCRICAO,
                    IE_SEMI_INTENSIVA
                ) values (
                    cd_estabelecimento_w,
                    cd_estabelecimento_w,
                    cd_setor_atendimento_w,
                    cd_classif_setor_w,
                    :new.ds_department,
                    SYSDATE,
                    SYSDATE,
                    'N',
                    'S',
                    'S',
                    'N',
                    'S',
                    'S',
                    'S',
                    'A',
                    ' ',
                    nm_usuario_w,
                    nm_usuario_w,
                    upper(:new.cd_type),
                    ie_semi_intensiva_w
                );

                ds_log_w := (
                    concat('cd_setor_atendimento: ',    cd_setor_atendimento_w)     || CHR(10) ||
                    concat('ds_setor_atendimento: ',    :new.ds_department)         || CHR(10) ||
                    concat('ds_classif_setor: ',        cd_classif_setor_w)         || CHR(10) ||
                    concat('cd_estabelecimento_base: ', cd_estabelecimento_w)       || CHR(10) ||
                    concat('cd_estabelecimento: ',      cd_estabelecimento_w)       || CHR(10) ||
                    concat('ie_situacao: ',             'A')                        || CHR(10) ||
                    concat('ie_semi_intensiva: ',       nvl(ie_semi_intensiva_w, 'null'))
                );
                pfcs_log_pck.pfcs_insert_org_struc_log(
                    ie_type_p => 'INSERT',
                    nm_table_p => 'SETOR_ATENDIMENTO',
                    ds_log_p => ds_log_w,
                    nm_usuario_p => nm_usuario_w
                );
            end if;

            /************* TIPO_ACOMODACAO *************/
            select
            nvl(max(cd_tipo_acomodacao),0)
            into cd_tipo_acomodacao_w
            from tipo_acomodacao;

            if (cd_tipo_acomodacao_w = 0) then
                cd_tipo_acomodacao_w := cd_tipo_acomodacao_w + 1;

                insert into
                tipo_acomodacao(
                    CD_TIPO_ACOMODACAO,
                    DS_TIPO_ACOMODACAO,
                    NM_USUARIO,
                    NM_USUARIO_NREC,
                    IE_SITUACAO,
                    DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    CD_NIVEL_ACOMODACAO -- Enter the type of accommodation released for the insurance, from 0 to 99. Ex: 3 (nursing), 4 (apartment), 5 (suite)
                ) values (
                    cd_tipo_acomodacao_w,
                    'PFCS',
                    nm_usuario_w,
                    nm_usuario_w,
                    'A',
                    SYSDATE,
                    SYSDATE,
                    4
                );
            end if;

            /************* UNIDADE_ATENDIMENTO *************/
            select (nvl(max(nr_seq_interno), 0) + 1)
            into nr_seq_interno_w
            from unidade_atendimento;

            insert into
            unidade_atendimento (
                CD_UNIDADE_BASICA,
                CD_UNIDADE_COMPL,
                CD_SETOR_ATENDIMENTO, -- FK SETOR_ATENDIMENTO
                CD_TIPO_ACOMODACAO, -- FK TIPO_ACOMODACAO
                DT_CRIACAO,
                IE_SITUACAO,
                DT_ATUALIZACAO,
                DT_ATUALIZACAO_NREC,
                NM_USUARIO,
                NM_USUARIO_NREC,
                IE_TEMPORARIO,
                NR_SEQ_LOCATION,
                NR_SEQ_INTERNO,
                IE_STATUS_UNIDADE,
                DT_HIGIENIZACAO
            ) values (
                :new.ds_room,
                :new.ds_bed,
                cd_setor_atendimento_w,
                cd_tipo_acomodacao_w,
                SYSDATE,
                ie_situacao_w,
                SYSDATE,
                SYSDATE,
                nm_usuario_w,
                nm_usuario_w,
                'N',
                :new.nr_sequencia,
                nr_seq_interno_w,
                ie_status_unidade_w,
                dt_sanitization_w
            );

            ds_log_w := (
                concat('nr_seq_location: ',         :new.nr_sequencia)          || CHR(10) ||
                concat('nr_seq_interno: ',          nr_seq_interno_w)           || CHR(10) ||
                concat('cd_unidade_basica: ',       :new.ds_room)               || CHR(10) ||
                concat('cd_unidade_compl: ',        :new.ds_bed)                || CHR(10) ||
                concat('cd_setor_atendimento: ',    cd_setor_atendimento_w)     || CHR(10) ||
                concat('cd_tipo_acomodacao: ',      cd_tipo_acomodacao_w)       || CHR(10) ||
                concat('ie_situacao: ',             ie_situacao_w)              || CHR(10) ||
                concat('ie_temporario: ',           'N')                        || CHR(10) ||
                concat('ie_status_unidade: ',       nvl(ie_status_unidade_w, 'null'))
            );
            pfcs_log_pck.pfcs_insert_org_struc_log(
                ie_type_p => 'INSERT',
                nm_table_p => 'UNIDADE_ATENDIMENTO',
                ds_log_p => ds_log_w,
                nm_usuario_p => nm_usuario_w
            );
        end if;
    END IF;


    IF UPDATING THEN
        select max(ie_status_unidade)
        into ie_status_unidade_ant_w
        from unidade_atendimento
        where nr_seq_location = :new.nr_sequencia;

        if (ie_situacao_w = 'H' and ie_status_unidade_ant_w = 'H') then
            select max(dt_higienizacao)
            into dt_sanitization_w
            from unidade_atendimento
            where nr_seq_location = :new.nr_sequencia;
        end if;

        update unidade_atendimento set
            DT_ATUALIZACAO = SYSDATE,
            IE_STATUS_UNIDADE = ie_status_unidade_w,
            IE_SITUACAO = ie_situacao_w,
            NM_USUARIO = nm_usuario_w,
            DT_HIGIENIZACAO = dt_sanitization_w
        where nr_seq_location = :new.nr_sequencia;

        ds_log_w := (
            concat('nr_seq_location: ',             :new.nr_sequencia || CHR(10))
            || concat('old ie_status_unidade: ',    nvl(ie_status_unidade_ant_w, 'null'))
            || concat(' | new ie_status_unidade: ', nvl(ie_status_unidade_w, 'null') || CHR(10))
            || concat('old ie_situacao: ',          substr(upper(:old.si_status), 1, 1))
            || concat(' | new ie_situacao: ',       ie_situacao_w)
        );
        pfcs_log_pck.pfcs_insert_org_struc_log(
            ie_type_p => 'UPDATE',
            nm_table_p => 'UNIDADE_ATENDIMENTO',
            ds_log_p => ds_log_w,
            nm_usuario_p => nm_usuario_w
        );
    END IF;
<<Final>>
qt_reg_w := 0;
END;
/


ALTER TABLE TASY.PFCS_LOCATION ADD (
  CONSTRAINT PFCSLOCAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PFCS_LOCATION ADD (
  CONSTRAINT PFCSLOCAL_PFCSADDR_FK 
 FOREIGN KEY (NR_SEQ_ADDRESS) 
 REFERENCES TASY.PFCS_ADDRESS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSLOCAL_PFCSLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCATION_SUP) 
 REFERENCES TASY.PFCS_LOCATION (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSLOCAL_PFCSORGA_FK 
 FOREIGN KEY (NR_SEQ_ORGANIZATION) 
 REFERENCES TASY.PFCS_ORGANIZATION (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PFCS_LOCATION TO NIVEL_1;

