ALTER TABLE TASY.PLS_CONVERSAO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONVERSAO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONVERSAO_PROC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_PROC_CONVERSAO          NUMBER(15),
  IE_ORIGEM_CONVERSAO        NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  CD_AREA_PROCEDIMENTO       NUMBER(15),
  CD_ESPECIALIDADE           NUMBER(15),
  CD_GRUPO_PROC              NUMBER(15),
  NR_SEQ_PRESTADOR           NUMBER(10),
  NR_SEQ_CONGENERE           NUMBER(10),
  NR_SEQ_ANTERIOR            NUMBER(10),
  IE_PTU                     VARCHAR2(1 BYTE),
  IE_IMPORTACAO_CONTA        VARCHAR2(1 BYTE),
  IE_REQUISICAO              VARCHAR2(1 BYTE),
  IE_PTU_A700                VARCHAR2(1 BYTE),
  IE_TIPO_CONVERSAO_SCS      NUMBER(2),
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  CD_PROC_ENVIO              VARCHAR2(30 BYTE),
  DS_PROC_ENVIO              VARCHAR2(255 BYTE),
  IE_ENVIO_RECEB             VARCHAR2(1 BYTE),
  DS_PROC_RETORNO            VARCHAR2(255 BYTE),
  NR_SEQ_INTERCAMBIO         NUMBER(10),
  IE_FATURAMENTO             VARCHAR2(1 BYTE),
  NR_SEQ_OPS_CONGENERE       NUMBER(10),
  NR_SEQ_CONTRATO            NUMBER(10),
  IE_ENVIO_LOTE_GUIAS        VARCHAR2(1 BYTE),
  CD_MATERIAL_IMP            VARCHAR2(255 BYTE),
  NR_SEQ_TISS_TAB_CONVERSAO  NUMBER(10),
  IE_TIPO_INTERCAMBIO        VARCHAR2(10 BYTE),
  IE_SOMENTE_CODIGO          VARCHAR2(1 BYTE),
  IE_TIPO_TABELA             VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_PRESTADOR      NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR     NUMBER(10),
  NM_REGRA                   VARCHAR2(100 BYTE),
  NR_ORDEM_EXEC_REGRA        NUMBER(10),
  QT_MULT_QUANTIDADE         NUMBER(9,4),
  NR_SEQ_GRUPO_CONTRATO      NUMBER(10),
  DT_INICIO_VIGENCIA_REF     DATE,
  DT_FIM_VIGENCIA_REF        DATE,
  IE_TIPO_TABELA_SCS         VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_ATENDIMENTO    NUMBER(10),
  IE_PTU_A1200               VARCHAR2(3 BYTE),
  IE_COMPLEMENTO             VARCHAR2(3 BYTE),
  IE_DIGITACAO_PORTAL        VARCHAR2(3 BYTE),
  IE_MONITORAMENTO_ANS       VARCHAR2(3 BYTE),
  NR_SEQ_GRAU_PARTIC         NUMBER(10),
  NR_SEQ_PRESTADOR_PARTIC    NUMBER(10),
  IE_SERVICO_PROPRIO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCONP_AREPROC_FK_I ON TASY.PLS_CONVERSAO_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_ESPPROC_FK_I ON TASY.PLS_CONVERSAO_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_ESTABEL_FK_I ON TASY.PLS_CONVERSAO_PROC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_GRUPROC_FK_I ON TASY.PLS_CONVERSAO_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_I1 ON TASY.PLS_CONVERSAO_PROC
(IE_IMPORTACAO_CONTA, IE_ENVIO_RECEB, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_I2 ON TASY.PLS_CONVERSAO_PROC
(DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF, IE_ENVIO_RECEB, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCONP_PK ON TASY.PLS_CONVERSAO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSCONG_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_PLSCONG_FK2_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_OPS_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_PLSCONG_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_PLSCONT_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_PLSGRPA_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSPRES_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_PLSPRES_FK2_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_PRESTADOR_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSPRGC_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSPRGP_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSTIAT_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PLSTIPR_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONP_PROCEDI_FK2_I ON TASY.PLS_CONVERSAO_PROC
(CD_PROC_CONVERSAO, IE_ORIGEM_CONVERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONP_PROCEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONP_TISSTTA_FK_I ON TASY.PLS_CONVERSAO_PROC
(NR_SEQ_TISS_TAB_CONVERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CONVERSAO_PROC_tp  after update ON TASY.PLS_CONVERSAO_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_GRUPO_PROC,1,500);gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ESPECIALIDADE,1,500);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_AREA_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONTRATO,1,4000),substr(:new.NR_SEQ_GRUPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONTRATO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_GRUPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRESTADOR',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_EXEC_REGRA,1,4000),substr(:new.NR_ORDEM_EXEC_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXEC_REGRA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MULT_QUANTIDADE,1,4000),substr(:new.QT_MULT_QUANTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_MULT_QUANTIDADE',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_A1200,1,4000),substr(:new.IE_PTU_A1200,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A1200',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPLEMENTO,1,4000),substr(:new.IE_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPLEMENTO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIGITACAO_PORTAL,1,4000),substr(:new.IE_DIGITACAO_PORTAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIGITACAO_PORTAL',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATENDIMENTO,1,4000),substr(:new.NR_SEQ_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MONITORAMENTO_ANS,1,4000),substr(:new.IE_MONITORAMENTO_ANS,1,4000),:new.nm_usuario,nr_seq_w,'IE_MONITORAMENTO_ANS',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_CONVERSAO,1,4000),substr(:new.CD_PROC_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_CONVERSAO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_CONVERSAO,1,4000),substr(:new.IE_ORIGEM_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_CONVERSAO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ANTERIOR,1,4000),substr(:new.NR_SEQ_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ANTERIOR',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU,1,4000),substr(:new.IE_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPORTACAO_CONTA,1,4000),substr(:new.IE_IMPORTACAO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPORTACAO_CONTA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONVERSAO_SCS,1,4000),substr(:new.IE_TIPO_CONVERSAO_SCS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONVERSAO_SCS',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PTU_A700,1,4000),substr(:new.IE_PTU_A700,1,4000),:new.nm_usuario,nr_seq_w,'IE_PTU_A700',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQUISICAO,1,4000),substr(:new.IE_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQUISICAO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROC_RETORNO,1,4000),substr(:new.DS_PROC_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROC_RETORNO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_ENVIO,1,4000),substr(:new.CD_PROC_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_ENVIO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROC_ENVIO,1,4000),substr(:new.DS_PROC_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROC_ENVIO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENVIO_RECEB,1,4000),substr(:new.IE_ENVIO_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENVIO_RECEB',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPS_CONGENERE,1,4000),substr(:new.NR_SEQ_OPS_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPS_CONGENERE',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INTERCAMBIO,1,4000),substr(:new.NR_SEQ_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INTERCAMBIO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATURAMENTO,1,4000),substr(:new.IE_FATURAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATURAMENTO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENVIO_LOTE_GUIAS,1,4000),substr(:new.IE_ENVIO_LOTE_GUIAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENVIO_LOTE_GUIAS',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TISS_TAB_CONVERSAO,1,4000),substr(:new.NR_SEQ_TISS_TAB_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TISS_TAB_CONVERSAO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TABELA_SCS,1,4000),substr(:new.IE_TIPO_TABELA_SCS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TABELA_SCS',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TABELA,1,4000),substr(:new.IE_TIPO_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TABELA',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_IMP,1,4000),substr(:new.CD_MATERIAL_IMP,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_IMP',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_CODIGO,1,4000),substr(:new.IE_SOMENTE_CODIGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_CODIGO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_INTERCAMBIO,1,4000),substr(:new.IE_TIPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_CONVERSAO_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conversao_proc_atual
before insert or update ON TASY.PLS_CONVERSAO_PROC for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia, 'F');

end pls_conversao_proc_atual;
/


ALTER TABLE TASY.PLS_CONVERSAO_PROC ADD (
  CONSTRAINT PLSCONP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONVERSAO_PROC ADD (
  CONSTRAINT PLSCONP_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TAB_CONVERSAO) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PARTIC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCONP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSCONP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSCONP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSCONP_PROCEDI_FK2 
 FOREIGN KEY (CD_PROC_CONVERSAO, IE_ORIGEM_CONVERSAO) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCONP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_OPS_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCONP_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONVERSAO_PROC TO NIVEL_1;

