ALTER TABLE TASY.PLS_OCORRENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCORRENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCORRENCIA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_OCORRENCIA             VARCHAR2(10 BYTE)   NOT NULL,
  DS_OCORRENCIA             VARCHAR2(255 BYTE)  NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DS_DOCUMENTACAO           VARCHAR2(4000 BYTE),
  IE_REQUISICAO             VARCHAR2(1 BYTE),
  IE_CONTA_MEDICA           VARCHAR2(1 BYTE),
  DS_MENSAGEM_EXTERNA       VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_GLOSA       NUMBER(10),
  NR_NIVEL_LIBERACAO        NUMBER(2),
  IE_AUDITORIA              VARCHAR2(1 BYTE),
  IE_FECHAR_CONTA           VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF            NUMBER(10),
  IE_GUIA_INTERCAMBIO       VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_PTU         NUMBER(10),
  IE_GLOSA                  VARCHAR2(1 BYTE),
  IE_ACAO_OCORRENCIA        VARCHAR2(3 BYTE),
  NR_SEQ_NIVEL_LIB          NUMBER(10),
  IE_AUDITORIA_CONTA        VARCHAR2(1 BYTE),
  IE_TIPO_GLOSA             VARCHAR2(10 BYTE),
  IE_PRE_ANALISE            VARCHAR2(1 BYTE),
  IE_DOCUMENTO_FISICO       VARCHAR2(1 BYTE),
  CD_SISTEMA_ANTERIOR       VARCHAR2(20 BYTE),
  IE_FINALIZAR_ANALISE      VARCHAR2(1 BYTE),
  IE_GLOSAR_PAGAMENTO       VARCHAR2(1 BYTE),
  IE_GLOSAR_FATURAMENTO     VARCHAR2(1 BYTE),
  IE_ENCAMINHAMENTO         VARCHAR2(1 BYTE),
  IE_REGRA_COMBINADA        VARCHAR2(1 BYTE),
  IE_LIB_WEB_GUIA           VARCHAR2(1 BYTE),
  IE_PROC_MAT               VARCHAR2(1 BYTE),
  IE_OCORRENCIA_MANUAL      VARCHAR2(1 BYTE),
  IE_RESPEITA_PADRAO_GLOSA  VARCHAR2(1 BYTE),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  IE_AUTORIZADO             VARCHAR2(2 BYTE),
  IE_GLOSA_AUTOMATICA       VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCOR_ESTABEL_FK_I ON TASY.PLS_OCORRENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCOR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSOCOR_PK ON TASY.PLS_OCORRENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCOR_PLSNILI_FK_I ON TASY.PLS_OCORRENCIA
(NR_SEQ_NIVEL_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCOR_PLSNILI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCOR_PLSOCCL_FK_I ON TASY.PLS_OCORRENCIA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCOR_PLSOCCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCOR_PTUMOQU_FK_I ON TASY.PLS_OCORRENCIA
(NR_SEQ_MOTIVO_PTU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCOR_PTUMOQU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCOR_TISSMGL_FK_I ON TASY.PLS_OCORRENCIA
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCOR_UK ON TASY.PLS_OCORRENCIA
(CD_OCORRENCIA, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_tp  after update ON TASY.PLS_OCORRENCIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_OCORRENCIA,1,500);gravar_log_alteracao(substr(:old.CD_OCORRENCIA,1,4000),substr(:new.CD_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_OCORRENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_GLOSA,1,4000),substr(:new.NR_SEQ_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_GLOSA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_PTU,1,4000),substr(:new.NR_SEQ_MOTIVO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_PTU',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FECHAR_CONTA,1,4000),substr(:new.IE_FECHAR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FECHAR_CONTA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITORIA,1,4000),substr(:new.IE_AUDITORIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITORIA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GLOSA,1,4000),substr(:new.IE_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GLOSA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GLOSAR_FATURAMENTO,1,4000),substr(:new.IE_GLOSAR_FATURAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GLOSAR_FATURAMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITORIA_CONTA,1,4000),substr(:new.IE_AUDITORIA_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITORIA_CONTA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOCUMENTO_FISICO,1,4000),substr(:new.IE_DOCUMENTO_FISICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOCUMENTO_FISICO',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRE_ANALISE,1,4000),substr(:new.IE_PRE_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRE_ANALISE',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GLOSA,1,4000),substr(:new.IE_TIPO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GLOSA',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FINALIZAR_ANALISE,1,4000),substr(:new.IE_FINALIZAR_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FINALIZAR_ANALISE',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENCAMINHAMENTO,1,4000),substr(:new.IE_ENCAMINHAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENCAMINHAMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GLOSAR_PAGAMENTO,1,4000),substr(:new.IE_GLOSAR_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GLOSAR_PAGAMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_NIVEL_LIB,1,4000),substr(:new.NR_SEQ_NIVEL_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_NIVEL_LIB',ie_log_w,ds_w,'PLS_OCORRENCIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_OCORRENCIA FOR EACH ROW
declare
ie_analise_cm_nova_w	pls_parametros.ie_analise_cm_nova%type;
begin
--aloterado o tratamento pois clientes que n�o fazem uso da an�lise por�m querem utilizar o processo de ocorr�ncia n�o conseguiam fazer uso do mesmo corretamente dgkorz

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	ie_analise_cm_nova
	into	ie_analise_cm_nova_w
	from	table(pls_parametros_pck.f_retorna_param(:new.cd_estabelecimento));

	if	(:new.ie_auditoria_conta = 'N') and
		(:new.nr_seq_motivo_glosa is not null) and
		(ie_analise_cm_nova_w = 'S')then
		wheb_mensagem_pck.exibir_mensagem_abort(210985);
	end if;
end if;

END;
/


ALTER TABLE TASY.PLS_OCORRENCIA ADD (
  CONSTRAINT PLSOCOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSOCOR_UK
 UNIQUE (CD_OCORRENCIA, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCORRENCIA ADD (
  CONSTRAINT PLSOCOR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSOCOR_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT PLSOCOR_PLSOCCL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PLS_OCORRENCIA_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PLSOCOR_PTUMOQU_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PTU) 
 REFERENCES TASY.PTU_MOTIVO_QUESTIONAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSOCOR_PLSNILI_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_LIB) 
 REFERENCES TASY.PLS_NIVEL_LIBERACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OCORRENCIA TO NIVEL_1;

