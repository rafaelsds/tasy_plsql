ALTER TABLE TASY.FUNCAO_CHAMADA_EXT_PARAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_CHAMADA_EXT_PARAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_CHAMADA_EXT_PARAM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CHAM_EXTERNA  NUMBER(10)               NOT NULL,
  NM_PARAMETRO         VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_APRES         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUCHEP_FUNCHE_FK_I ON TASY.FUNCAO_CHAMADA_EXT_PARAM
(NR_SEQ_CHAM_EXTERNA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUCHEP_PK ON TASY.FUNCAO_CHAMADA_EXT_PARAM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FUNCAO_CHAMADA_EXT_PARAM ADD (
  CONSTRAINT FUCHEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_CHAMADA_EXT_PARAM ADD (
  CONSTRAINT FUCHEP_FUNCHE_FK 
 FOREIGN KEY (NR_SEQ_CHAM_EXTERNA) 
 REFERENCES TASY.FUNCAO_CHAMADA_EXTERNA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FUNCAO_CHAMADA_EXT_PARAM TO NIVEL_1;

