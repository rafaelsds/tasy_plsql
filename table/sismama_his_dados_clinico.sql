ALTER TABLE TASY.SISMAMA_HIS_DADOS_CLINICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SISMAMA_HIS_DADOS_CLINICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SISMAMA_HIS_DADOS_CLINICO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_SISMAMA           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_TIPO_EXAME            VARCHAR2(2 BYTE),
  IE_RISCO_ELEVADO         VARCHAR2(1 BYTE),
  IE_GRAVIDA_AMAMENTANDO   VARCHAR2(1 BYTE),
  IE_TRATAMENTO_ANTERIOR   VARCHAR2(1 BYTE),
  IE_TRAT_MESMA_MAMA       VARCHAR2(1 BYTE),
  IE_TRAT_OUTRA_MAMA       VARCHAR2(1 BYTE),
  IE_TRAT_QUIMIOTERAPIA    VARCHAR2(1 BYTE),
  IE_TRAT_RAD_MESMA_MAMA   VARCHAR2(1 BYTE),
  IE_TRAT_RAD_OUTRA_MAMA   VARCHAR2(1 BYTE),
  IE_TRAT_HORMONIO         VARCHAR2(1 BYTE),
  IE_DETECCAO_LESAO        VARCHAR2(1 BYTE),
  IE_DIAGNOSTICO_IMAGEM    VARCHAR2(1 BYTE),
  IE_CARACTERISTICA_LESAO  VARCHAR2(1 BYTE),
  IE_LOCALIZACAO           VARCHAR2(10 BYTE),
  IE_TAMANHO               NUMBER(3),
  IE_LINFONODO_AXILAR      VARCHAR2(1 BYTE),
  IE_MATERIAL_ENVIADO      NUMBER(3),
  DT_COLETA                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SISHIDC_PK ON TASY.SISMAMA_HIS_DADOS_CLINICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISHIDC_SISMATE_FK_I ON TASY.SISMAMA_HIS_DADOS_CLINICO
(NR_SEQ_SISMAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SISMAMA_HIS_DADOS_CLINICO ADD (
  CONSTRAINT SISHIDC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SISMAMA_HIS_DADOS_CLINICO ADD (
  CONSTRAINT SISHIDC_SISMATE_FK 
 FOREIGN KEY (NR_SEQ_SISMAMA) 
 REFERENCES TASY.SISMAMA_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SISMAMA_HIS_DADOS_CLINICO TO NIVEL_1;

