ALTER TABLE TASY.GPI_TIPO_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_TIPO_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_TIPO_DOCUMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO              VARCHAR2(255 BYTE)       NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_ESTAB_EXCLUSIVO   NUMBER(4),
  DS_COR_FONTE         VARCHAR2(100 BYTE),
  DS_COR_FUNDO         VARCHAR2(100 BYTE),
  DS_COR_HTML          VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPITPDOC_EMPRESA_FK_I ON TASY.GPI_TIPO_DOCUMENTO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPITPDOC_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPITPDOC_ESTABEL_FK_I ON TASY.GPI_TIPO_DOCUMENTO
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPITPDOC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GPITPDOC_PK ON TASY.GPI_TIPO_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GPI_TIPO_DOCUMENTO ADD (
  CONSTRAINT GPITPDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_TIPO_DOCUMENTO ADD (
  CONSTRAINT GPITPDOC_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT GPITPDOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.GPI_TIPO_DOCUMENTO TO NIVEL_1;

