ALTER TABLE TASY.PROJ_ORC_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_ORC_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_ORC_PROF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORC           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_SEMANA            NUMBER(15)               NOT NULL,
  QT_HORA_SEM          NUMBER(15)               NOT NULL,
  VL_HORA_REC          NUMBER(15,2)             NOT NULL,
  NR_SEQ_NIVEL_CONS    NUMBER(10),
  IE_COORDENADOR       VARCHAR2(1 BYTE)         NOT NULL,
  QT_PROFISSIONAL      NUMBER(3)                NOT NULL,
  VL_CUSTO_HORA        NUMBER(15,2)             NOT NULL,
  VL_CUSTO_TOTAL       NUMBER(15,2)             NOT NULL,
  VL_RECEITA_TOTAL     NUMBER(15,2)             NOT NULL,
  VL_IMPOSTO           NUMBER(15,2)             NOT NULL,
  VL_MARGEM            NUMBER(15,2)             NOT NULL,
  IE_TIPO_VALOR        VARCHAR2(1 BYTE)         NOT NULL,
  CD_EXECUTOR          VARCHAR2(10 BYTE),
  VL_ADICIONAL_CLT     NUMBER(15,2)             NOT NULL,
  VL_CUSTO_INST        NUMBER(15,2)             NOT NULL,
  QT_HORAS             NUMBER(15)               NOT NULL,
  VL_FIXO              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJORP_PESFISI_FK_I ON TASY.PROJ_ORC_PROF
(CD_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJORP_PK ON TASY.PROJ_ORC_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJORP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJORP_PROJNICO_FK_I ON TASY.PROJ_ORC_PROF
(NR_SEQ_NIVEL_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJORP_PROJNICO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJORP_PROJORC_FK_I ON TASY.PROJ_ORC_PROF
(NR_SEQ_ORC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJORP_PROJORC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Proj_Orc_Prof_Atual
before insert or update ON Proj_Orc_Prof
for each row
declare

vl_custo_hora_w		number(15,2);
vl_custo_total_w	number(15,2);
vl_receita_total_w	number(15,2);
vl_margem_w		number(15,2);
qt_reg_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
vl_custo_hora_w 	:= :new.vl_custo_hora;
/*
vl_custo_total_w	:= ((vl_custo_hora_w * :new.qt_hora_sem) * :new.qt_semana) * :new.qt_profissional;
vl_receita_total_w	:= ((:new.vl_hora_rec * :new.qt_hora_sem) * :new.qt_semana) * :new.qt_profissional;

:new.vl_custo_hora	:= vl_custo_hora_w;
:new.vl_custo_total	:= vl_custo_total_w;
:new.vl_receita_total	:= vl_receita_total_w;
*/
<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.PROJ_ORC_PROF ADD (
  CONSTRAINT PROJORP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_ORC_PROF ADD (
  CONSTRAINT PROJORP_PROJORC_FK 
 FOREIGN KEY (NR_SEQ_ORC) 
 REFERENCES TASY.PROJ_ORCAMENTO (NR_SEQUENCIA),
  CONSTRAINT PROJORP_PROJNICO_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_CONS) 
 REFERENCES TASY.PROJ_NIVEL_CONSULTOR (NR_SEQUENCIA),
  CONSTRAINT PROJORP_PESFISI_FK 
 FOREIGN KEY (CD_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PROJ_ORC_PROF TO NIVEL_1;

