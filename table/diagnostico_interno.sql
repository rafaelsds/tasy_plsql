ALTER TABLE TASY.DIAGNOSTICO_INTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIAGNOSTICO_INTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIAGNOSTICO_INTERNO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_DIAGNOSTICO        VARCHAR2(240 BYTE)      NOT NULL,
  CD_DOENCA_CID         VARCHAR2(10 BYTE)       NOT NULL,
  IE_PEDE_DATA          VARCHAR2(1 BYTE)        NOT NULL,
  IE_PEDE_OBS           VARCHAR2(1 BYTE)        NOT NULL,
  IE_PEDE_EVIDENCIA     VARCHAR2(1 BYTE)        NOT NULL,
  IE_PEDE_CLASSIF_DIAG  VARCHAR2(1 BYTE)        NOT NULL,
  IE_PEDE_LADO          VARCHAR2(1 BYTE)        NOT NULL,
  IE_TIPO_DOENCA        VARCHAR2(15 BYTE),
  IE_UTILIZACAO         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIAINTE_CIDDOEN_FK_I ON TASY.DIAGNOSTICO_INTERNO
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIAINTE_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DIAINTE_PK ON TASY.DIAGNOSTICO_INTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIAGNOSTICO_INTERNO ADD (
  CONSTRAINT DIAINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIAGNOSTICO_INTERNO ADD (
  CONSTRAINT DIAINTE_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.DIAGNOSTICO_INTERNO TO NIVEL_1;

