ALTER TABLE TASY.SAN_OPCAO_IMP_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_OPCAO_IMP_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_OPCAO_IMP_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OPCAO             VARCHAR2(60 BYTE)        NOT NULL,
  CD_INTERFACE         NUMBER(5),
  NR_ATALHO            NUMBER(5),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_PROJ_XML      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SAOPLOT_INTERFA_FK_I ON TASY.SAN_OPCAO_IMP_LOTE
(CD_INTERFACE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SAOPLOT_PK ON TASY.SAN_OPCAO_IMP_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAOPLOT_XMLPROJ_FK_I ON TASY.SAN_OPCAO_IMP_LOTE
(NR_SEQ_PROJ_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.san_opcao_imp_lote_atual
before insert or update ON TASY.SAN_OPCAO_IMP_LOTE for each row
declare

begin
	if	((:new.cd_interface is not null) and
		(:new.nr_seq_proj_xml is not null)) or
		((:new.cd_interface is null) and
		(:new.nr_seq_proj_xml is null)) then

		wheb_mensagem_pck.exibir_mensagem_abort(729793);

	end if;
end;
/


ALTER TABLE TASY.SAN_OPCAO_IMP_LOTE ADD (
  CONSTRAINT SAOPLOT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_OPCAO_IMP_LOTE ADD (
  CONSTRAINT SAOPLOT_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT SAOPLOT_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_XML) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_OPCAO_IMP_LOTE TO NIVEL_1;

