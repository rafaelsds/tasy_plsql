ALTER TABLE TASY.SUP_INT_ATEND_REQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_ATEND_REQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_ATEND_REQ
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  QT_MATERIAL_ATENDIDA  NUMBER(13,4),
  CD_PESSOA_ATENDE      VARCHAR2(10 BYTE),
  IE_ACAO               VARCHAR2(1 BYTE),
  DT_ATENDIMENTO        DATE,
  DT_RECEBIMENTO        DATE,
  NR_REQUISICAO         NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  CD_ESTABELECIMENTO    NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINTAT_PK ON TASY.SUP_INT_ATEND_REQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUP_INT_ATEND_REQ ADD (
  CONSTRAINT SUINTAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_ATEND_REQ TO NIVEL_1;

