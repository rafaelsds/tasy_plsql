ALTER TABLE TASY.PLS_PP_CTA_COMBINADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PP_CTA_COMBINADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PP_CTA_COMBINADA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_TIPO_REGRA           VARCHAR2(5 BYTE)      NOT NULL,
  NR_SEQ_EVENTO           NUMBER(10)            NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NM_REGRA                VARCHAR2(200 BYTE)    NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_ORDEM_EXECUCAO       NUMBER(20)            NOT NULL,
  IE_ORDENACAO_SUGERIDA   VARCHAR2(1 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PPPCCOM_PK ON TASY.PLS_PP_CTA_COMBINADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PPPCCOM_PLSEVEN_FK_I ON TASY.PLS_PP_CTA_COMBINADA
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_pp_cta_combinada_atual
before insert or update ON TASY.PLS_PP_CTA_COMBINADA for each row
declare

qt_registro_w	pls_integer;

cursor c01(	nr_seq_pp_combinada_pc	pls_pp_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia
	from	pls_pp_cta_filtro
	where	nr_seq_pp_combinada = nr_seq_pp_combinada_pc;

begin

-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;

if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

-- percorre todos os filtros filhos
for r_c01_w in c01(:new.nr_sequencia) loop

	case (:new.ie_tipo_regra)
		when 'P' then -- procedimento

			update 	pls_pp_cta_filtro set
				ie_filtro_material = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		when 'M' then -- material

			update 	pls_pp_cta_filtro set
				ie_filtro_procedimento = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		when 'C' then -- material

			update 	pls_pp_cta_filtro set
				ie_filtro_material = 'N',
				ie_filtro_procedimento = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		else
			null;
	end case;
end loop;

end pls_pp_cta_combinada_atual;
/


ALTER TABLE TASY.PLS_PP_CTA_COMBINADA ADD (
  CONSTRAINT PPPCCOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PP_CTA_COMBINADA ADD (
  CONSTRAINT PPPCCOM_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_PP_CTA_COMBINADA TO NIVEL_1;

