ALTER TABLE TASY.INTPD_FILA_TRANSMISSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_FILA_TRANSMISSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_FILA_TRANSMISSAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_EVENTO              VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_DOCUMENTO       VARCHAR2(80 BYTE)      NOT NULL,
  NR_SEQ_EVENTO_SISTEMA  NUMBER(10),
  IE_STATUS              VARCHAR2(15 BYTE),
  DS_LOG                 VARCHAR2(4000 BYTE),
  IE_OPERACAO            VARCHAR2(15 BYTE),
  NR_DOC_EXTERNO         VARCHAR2(80 BYTE),
  DS_XML                 CLOB,
  DS_XML_RETORNO         CLOB                   DEFAULT null,
  NR_SEQ_ITEM_DOCUMENTO  NUMBER(10),
  IE_ENVIO_RECEBE        VARCHAR2(1 BYTE),
  CD_ESTAB_DOCUMENTO     NUMBER(4),
  DT_STATUS              DATE,
  DS_PARAMETROS_ADIC     VARCHAR2(2000 BYTE),
  DS_MESSAGE             CLOB,
  IE_STATUS_HTTP         NUMBER(10),
  DS_MESSAGE_RESPONSE    CLOB,
  NR_SEQ_AGRUPADOR       NUMBER(10),
  NR_SEQ_DEPENDENCIA     NUMBER(10),
  NR_SEQ_ORIGEM          NUMBER(10),
  IE_RESPONSE_PROCEDURE  VARCHAR2(1 BYTE),
  DS_STACK               VARCHAR2(4000 BYTE),
  CD_PERFIL              NUMBER(5),
  CD_SETOR_USUARIO       NUMBER(5),
  IE_TIPO_ERRO           VARCHAR2(1 BYTE),
  DT_PROCESSAMENTO       DATE,
  QT_TENTATIVA           NUMBER(5),
  NR_SEQ_LOG_INTEGRACAO  NUMBER(10),
  NR_SEQ_SERVIDOR        NUMBER(10),
  IE_CONTROLE_TAG        VARCHAR2(15 BYTE),
  CD_DEFAULT_MESSAGE     VARCHAR2(15 BYTE),
  NR_SEQ_SUPERIOR        NUMBER(10),
  IE_GERACAO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_XML) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_MESSAGE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_MESSAGE_RESPONSE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_XML_RETORNO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPDFTR_INTPDESI_FK_I ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_EVENTO_SISTEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_INTPDFTR_FK_I ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_DEPENDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_INTPDFTR_FK2_I ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_INTPDFTR_FK3_I ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I1 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I2 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_DOCUMENTO, NR_SEQ_ITEM_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I3 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_AGRUPADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I4 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_DOCUMENTO, IE_EVENTO, IE_STATUS, IE_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I5 ON TASY.INTPD_FILA_TRANSMISSAO
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I6 ON TASY.INTPD_FILA_TRANSMISSAO
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I7 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_DOC_EXTERNO, IE_EVENTO, IE_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I8 ON TASY.INTPD_FILA_TRANSMISSAO
(IE_STATUS, IE_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_I9 ON TASY.INTPD_FILA_TRANSMISSAO
(NR_DOC_EXTERNO, IE_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_LOGINTE_FK_I ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQ_LOG_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_PERFIL_FK_I ON TASY.INTPD_FILA_TRANSMISSAO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPDFTR_PK ON TASY.INTPD_FILA_TRANSMISSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDFTR_SETATEN_FK_I ON TASY.INTPD_FILA_TRANSMISSAO
(CD_SETOR_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.intpd_fila_transmissao_after
after update ON TASY.INTPD_FILA_TRANSMISSAO for each row
declare

jobno number;
qt_evento_superior_w number(10);

begin

select 	count(*)
into	qt_evento_superior_w
from 	intpd_evento_superior
where	nr_seq_evento_sup = (	select	max(nr_sequencia)
				from	intpd_eventos
				where	ie_evento = :new.ie_evento);

if	(:new.ie_evento = '54' and qt_evento_superior_w = 0) then /*Enviar procedimento paciente*/

	if	(:new.nr_seq_agrupador is not null) and
		(:new.nr_seq_dependencia is null) and
		(:old.ie_status <> :new.ie_status) then

		if	(:new.ie_status in ('E','S')) then

			dbms_job.submit(jobno, 'intpd_atualiza_reg_agrupador('|| to_char(:new.nr_sequencia) || ', ' || chr(39)|| :new.ie_status || chr(39)||');');

		elsif	(:new.ie_status in ('R','P')) then

			dbms_job.submit(jobno, 'intpd_atualiza_reg_agrupador('|| to_char(:new.nr_sequencia) || ', ' || chr(39)|| 'AP' || chr(39)||');');

		end if;
	end if;

end if;

if	((qt_evento_superior_w > 0) and
	(:old.ie_status <> :new.ie_status) and
	(:new.ie_status = 'S')) then
	dbms_job.submit(jobno, 'intpd_atualiza_reg_dependente('|| to_char(:new.nr_sequencia) || ', ' || chr(39)|| 'P' || chr(39)||');');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.intpd_fila_transmissao_atual
before insert or update ON TASY.INTPD_FILA_TRANSMISSAO for each row
declare

begin
if	(nvl(:old.ie_status,'NULL') <> nvl(:new.ie_status,'NULL')) then
	:new.dt_status	:=	sysdate;

	/*'Tratamento para erro fake, ou seja, sistema externo retorna erro, mas deve ser tratado como sucesso'*/
	if	(:new.ie_status = 'E') and
		(nvl(:old.ie_tipo_erro, 'NULL') <> nvl(:new.ie_tipo_erro, 'NULL')) and
		(:new.ie_tipo_erro = 'K') then
		:new.ie_status	:=	'S';
	end if;

	if	(:new.ie_status <> 'E') then
		:new.ie_tipo_erro	:=	null;
	end if;
end if;

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,4000);
end if;

if	(:new.nr_seq_documento is null) then
	:new.nr_seq_documento	:=	0;
end if;

if	(nvl(:new.ie_geracao,'X') = 'X') then
	:new.ie_geracao	:=	'S';
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.intpd_fila_transmissao_hist
after insert or update ON TASY.INTPD_FILA_TRANSMISSAO for each row
declare

begin
if	(inserting) then
	begin
	gerar_reg_alter_fila(:new.nr_sequencia, :new.nr_seq_evento_sistema);
	end;
end if;

if	(nvl(:old.ie_status,'NULL') <> nvl(:new.ie_status,'NULL')) THEN

  INSERT INTO intpd_historico_status (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_fila,
        ie_status,
        ds_log)
  VALUES (
        intpd_historico_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario,
        SYSDATE,
        :new.nm_usuario,
        :new.nr_sequencia,
        :new.ie_status,
        :new.ds_log);

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.intpd_fila_transmissao_delete
before delete ON TASY.INTPD_FILA_TRANSMISSAO for each row
declare

ds_host_w		varchar2(255);
nr_ip_address_w		varchar2(255);
ds_user_w		varchar2(255);
ds_session_user_w	varchar2(255);
ds_stack_w 		varchar2(4000);
ie_baixa_encontro_contas_w number(10) := 0;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin

	begin
	select	sys_context('USERENV', 'HOST') DS_HOST,
		sys_context('USERENV', 'IP_ADDRESS') NR_IP_ADDRESS,
		sys_context('USERENV', 'OS_USER') DS_USER,
		sys_context('USERENV', 'SESSION_USER') DS_SESSION_USER
	into	ds_host_w,
		nr_ip_address_w,
		ds_user_w,
		ds_session_user_w
	from	dual;
	exception
	when others then
		ds_host_w 	:= null;
		nr_ip_address_w 	:= null;
		ds_user_w 	:= null;
		ds_session_user_w 	:= null;
	end;

	ds_stack_w 		:= substr(dbms_utility.format_call_stack,1,4000);

	/*select	count(*)
	into	ie_baixa_encontro_contas_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual)
	and		upper(action) = 'INTPD_LOG_CLEANING';

	if (ie_baixa_encontro_contas_w = 0) then
	*/
		insert into intpd_log_exclusao(
			dt_exclusao,
			dt_registro,
			nr_seq_fila,
			nr_seq_documento,
			nr_seq_item_documento,
			nr_doc_externo,
			ie_evento,
			ie_operacao,
			ie_status,
			ds_xml,
			ds_xml_retorno,
			ds_stack,
			ds_host,
			nr_ip_address,
			ds_user,
			ds_session_user)
		values(	sysdate,
			:old.dt_atualizacao,
			:old.nr_sequencia,
			:old.nr_seq_documento,
			:old.nr_seq_item_documento,
			:old.nr_doc_externo,
			:old.ie_evento,
			:old.ie_operacao,
			:old.ie_status,
			:old.ds_xml,
			:old.ds_xml_retorno,
			ds_stack_w,
			ds_host_w,
			nr_ip_address_w,
			ds_user_w,
			ds_session_user_w);

	--end if;

	end;
end if;

end;
/


ALTER TABLE TASY.INTPD_FILA_TRANSMISSAO ADD (
  CONSTRAINT INTPDFTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTPD_FILA_TRANSMISSAO ADD (
  CONSTRAINT INTPDFTR_INTPDFTR_FK3 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.INTPD_FILA_TRANSMISSAO (NR_SEQUENCIA),
  CONSTRAINT INTPDFTR_INTPDFTR_FK 
 FOREIGN KEY (NR_SEQ_DEPENDENCIA) 
 REFERENCES TASY.INTPD_FILA_TRANSMISSAO (NR_SEQUENCIA),
  CONSTRAINT INTPDFTR_INTPDFTR_FK2 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.INTPD_FILA_TRANSMISSAO (NR_SEQUENCIA),
  CONSTRAINT INTPDFTR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT INTPDFTR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_USUARIO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT INTPDFTR_LOGINTE_FK 
 FOREIGN KEY (NR_SEQ_LOG_INTEGRACAO) 
 REFERENCES TASY.LOG_INTEGRACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INTPDFTR_INTPDESI_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_SISTEMA) 
 REFERENCES TASY.INTPD_EVENTOS_SISTEMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTPD_FILA_TRANSMISSAO TO NIVEL_1;

