ALTER TABLE TASY.EPISODIO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EPISODIO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EPISODIO_PACIENTE
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_EPISODIO                 NUMBER(10),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_EPISODIO        NUMBER(6),
  NR_SEQ_SUBTIPO_EPISODIO     NUMBER(6),
  DT_EPISODIO                 DATE,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  IE_STATUS                   VARCHAR2(1 BYTE),
  DT_CANCELAMENTO             DATE,
  NM_USUARIO_CANCELAMENTO     VARCHAR2(15 BYTE),
  CD_MEDICO_REFERIDO          VARCHAR2(10 BYTE),
  NM_USUARIO_ENCERRAMENTO     VARCHAR2(15 BYTE),
  DS_MOTIVO_CANCELAMENTO      VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  DT_FIM_EPISODIO             DATE,
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  DT_REABERTURA               DATE,
  NR_SEQ_MOTIVO_REABERTURA    NUMBER(10),
  NM_USUARIO_REABERTURA       VARCHAR2(15 BYTE),
  DS_MOTIVO_REABERTURA        VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO          NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAEPPA_CASUEP_FK_I ON TASY.EPISODIO_PACIENTE
(NR_SEQ_SUBTIPO_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAEPPA_CATPEP_FK_I ON TASY.EPISODIO_PACIENTE
(NR_SEQ_TIPO_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAEPPA_ESTABEL_FK_I ON TASY.EPISODIO_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAEPPA_I1 ON TASY.EPISODIO_PACIENTE
(NR_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAEPPA_MOTREACA_FK_I ON TASY.EPISODIO_PACIENTE
(NR_SEQ_MOTIVO_REABERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAEPPA_PESFISI_FK_I ON TASY.EPISODIO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAEPPA_PK ON TASY.EPISODIO_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.episodio_paciente_after
after insert or update or delete ON TASY.EPISODIO_PACIENTE for each row
declare
reg_integracao_p			gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);
ie_inpatient_w			varchar2(1);
qt_eventos_w			number(10);
ie_status_w  			varchar2(1);
nm_usuario_w 			varchar2(200);
qt_evt_case_status_w		number(10)	:= 0;
nr_sequencia_w			varchar2(35);
ie_tipo_old_w			tipo_episodio.ie_tipo%type;
ie_tipo_new_w			tipo_episodio.ie_tipo%type;
ie_relevante_drg_w		diagnostico_doenca.ie_relevante_drg%type default 'N';

begin

begin
select	count(1) qt_movement,
	nvl(max(decode(get_case_encounter_type(a.nr_seq_episodio, :new.nr_seq_tipo_episodio, a.nr_atendimento, a.ie_tipo_atendimento), '1', 'S')), 'N') ie_inpatient
into	qt_reg_w,
	ie_inpatient_w
from	atendimento_paciente a,
	atend_paciente_unidade b
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_seq_episodio = nvl(:new.nr_sequencia, :old.nr_sequencia);
exception
when others then
	qt_reg_w	:=	0;
end;

if	(qt_reg_w > 0) then
	if	(ie_inpatient_w = 'S') then
		begin
		if	((deleting) or ((updating) and ((:old.dt_cancelamento is null) and (:new.dt_cancelamento is not null)))) then
			reg_integracao_p.ie_operacao		:=	'E';
		elsif	(updating) then
			reg_integracao_p.ie_operacao		:=	'A';
		end if;

		reg_integracao_p.ie_status		:=	'P';
		reg_integracao_p.cd_pessoa_fisica	:=	nvl(:new.cd_pessoa_fisica,:old.cd_pessoa_fisica);

		gerar_int_padrao.gravar_integracao('106', nvl(:new.nr_sequencia,:old.nr_sequencia), nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end;
	else
		reg_integracao_p.ie_operacao		:=	'A';
		reg_integracao_p.ie_status		:=	'P';
		reg_integracao_p.cd_pessoa_fisica	:=	nvl(:new.cd_pessoa_fisica,:old.cd_pessoa_fisica);

		if	(nvl(:new.ds_observacao,'NULL') <> nvl(:old.ds_observacao,'NULL')) then
			gerar_int_padrao.gravar_integracao('120', nvl(:new.nr_sequencia,:old.nr_sequencia),nvl(obter_usuario_ativo,nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;
	end if;
end if;

if	(inserting) then
	ie_status_w := 'P';
	nm_usuario_w:= :new.nm_usuario;
elsif 	(updating) then
	begin
	if	(:old.dt_fim_episodio is not null and :new.dt_fim_episodio is null) then
		ie_status_w := 'P';
		nm_usuario_w:= :new.nm_usuario;
	elsif	(:old.dt_fim_episodio is null and :new.dt_fim_episodio is not null) then
		ie_status_w := 'F';
		nm_usuario_w:= :new.nm_usuario_encerramento;
	elsif	(:old.dt_cancelamento is null and :new.dt_cancelamento is not null) then
		ie_status_w := 'C';
		nm_usuario_w:= :new.nm_usuario_cancelamento;
	end if;

	if	(nvl(:new.nr_seq_tipo_episodio, 0) <> nvl(:old.nr_seq_tipo_episodio,0)) then
		begin
			select	nvl(max(ie_tipo),'0')
			into	ie_tipo_new_w
			from	tipo_episodio
			where	nr_sequencia = :new.nr_seq_tipo_episodio;

			select	nvl(max(ie_tipo),'0')
			into	ie_tipo_old_w
			from	tipo_episodio
			where	nr_sequencia = :old.nr_seq_tipo_episodio;

			if	((ie_tipo_new_w = '1') and (ie_tipo_old_w <> '1')) then
				begin
					ie_relevante_drg_w := 'S';
					reg_integracao_p.ie_operacao	:=	'I';
					reg_integracao_p.ie_status		:=	'A';
					reg_integracao_p.cd_pessoa_fisica	:=	:new.cd_pessoa_fisica;

					gerar_int_padrao.gravar_integracao('220', :new.nr_sequencia, nvl(obter_usuario_ativo,:new.nm_usuario), reg_integracao_p);
				end;
			elsif	((ie_tipo_new_w <> '1') and (ie_tipo_old_w = '1')) then
				update	intpd_fila_transmissao
				set	ie_status = 'S',
					dt_atualizacao = sysdate,
					ds_log = 'CANCELED'
				where	ie_evento = '220'
				and	nvl(ie_status, 'X') not in ('S')
				and	nr_seq_documento = :new.nr_sequencia;
			end if;

			update 	diagnostico_doenca
			set 	ie_diag_princ_episodio = 'N',
					ie_diag_princ_depart = 'N',
					ie_diag_obito = 'N',
					ie_diag_alta = 'N',
					ie_diag_cirurgia = 'N',
					ie_diag_pre_cir = 'N',
					ie_diag_trat_cert = 'N',
					ie_diag_admissao = 'N',
					ie_status_diag = 'N',
					ie_diag_referencia = 'N',
					ie_diag_cronico = 'N',
					ie_diag_trat_especial = 'N',
					ie_relevante_drg = ie_relevante_drg_w
			where 	nr_atendimento in (	select 	a.nr_atendimento
										from 	atendimento_paciente a
										where 	a.nr_seq_episodio = :new.nr_sequencia)
			and		dt_inativacao is null;

			update 	diagnostico_doenca_depart
			set 	ie_diag_princ_episodio = 'N',
					ie_diag_princ_depart = 'N',
					ie_diag_obito = 'N',
					ie_diag_alta = 'N',
					ie_diag_cirurgia = 'N',
					ie_diag_pre_cir = 'N',
					ie_diag_trat_cert = 'N',
					ie_diag_admissao = 'N',
					ie_diag_referencia = 'N',
					ie_diag_cronico = 'N',
					ie_diag_trat_especial = 'N'
			where 	nr_atendimento in (	select 	a.nr_atendimento
										from 	atendimento_paciente a
										where 	a.nr_seq_episodio = :new.nr_sequencia);
		end;
	end if;

	end;
end if;

if	((inserting) or	(updating)) then
	if (ie_status_w is not null) then
		insert into episodio_paciente_status(
			nr_sequencia,
			nr_seq_episodio,
			dt_atualizacao,
			nm_usuario,
			ie_status,
			dt_status)
		values(	episodio_paciente_status_seq.nextval,
			:new.nr_sequencia,
			sysdate,
			nvl(nm_usuario_w, :new.nm_usuario),
			ie_status_w,
			sysdate);
		end if;

	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') and
		((:old.dt_fim_episodio is null 		and :new.dt_fim_episodio is not null) or
		 (:old.dt_fim_episodio is not null 	and :new.dt_fim_episodio is null) or
		 (:old.dt_cancelamento is null 		and :new.dt_cancelamento is not null) or
		 (:old.dt_cancelamento is not null 	and :new.dt_cancelamento is null)) then
		begin
		select	1
		into	qt_eventos_w
		from	intpd_eventos
		where	ie_evento	= '401'
		and	ie_situacao	= 'A'
		and	rownum		= 1;
		exception
		when others then
			qt_eventos_w	:= 0;
		end;

		if	(qt_eventos_w > 0) and (obter_se_atend_finalizado(:new.nr_sequencia, null, null) = 'S') then
			reg_integracao_p.nr_seq_agrupador	:= :new.cd_pessoa_fisica;
			reg_integracao_p.cd_pessoa_fisica	:= :new.cd_pessoa_fisica;
			reg_integracao_p.ie_status		:= 'P';
			reg_integracao_p.ie_operacao	:= 'A';
			nr_sequencia_w			:= :new.nr_sequencia;

			gerar_int_padrao.gravar_integracao('401', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);
		end if;
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.EPISODIO_PACIENTE_BEFORE
before insert ON TASY.EPISODIO_PACIENTE for each row
begin
if(nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE', 'de_AT'))then
   cancelar_atend_controle(:new.cd_pessoa_fisica, 'S');
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.UPDATE_BILLABLE_CONV_PROCEDURE
after update ON TASY.EPISODIO_PACIENTE for each row
declare

nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
cd_procedimento_w	procedimento_paciente.cd_procedimento%type;
ie_origem_proced_w      procedimento_paciente.ie_origem_proced%type;

ie_alt_procedimento_w varchar(2);

pragma autonomous_transaction;

cursor c01 is
	select	nr_sequencia,
		dt_procedimento,
		nr_seq_proc_interno,
		nr_prescricao,
		cd_procedimento,
		ie_origem_proced,
		cd_setor_atendimento,
		dt_conta,
		cd_convenio,
		nr_interno_conta
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_w;

begin
    begin
		select 	nr_interno_conta into nr_interno_conta_w
		from 	conta_paciente
		where 	nr_atendimento = obter_atendimento_episodio(:new.nr_sequencia)
		and	ie_status_acerto   = 1;
		exception
		when others then
			nr_interno_conta_w:= null;
    end;


	if nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S' then
	begin
		obter_param_usuario(916,1236,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.Get_cd_estabelecimento,ie_alt_procedimento_w);


		if (:old.NR_SEQ_TIPO_EPISODIO <> :new.NR_SEQ_TIPO_EPISODIO) and (nvl(ie_alt_procedimento_w,'N') = 'S') Then
			begin
				for r_C01 in c01 loop
					begin
						obter_proc_tab_interno(
									r_C01.nr_seq_proc_interno,
									r_C01.nr_prescricao,
									obter_atendimento_episodio(:new.nr_sequencia),
									r_C01.nr_interno_conta,
									cd_procedimento_w,
									ie_origem_proced_w,
									r_C01.cd_setor_atendimento,
									r_C01.dt_conta);

						if	(cd_procedimento_w <> 0) then

							update	procedimento_paciente
							set	cd_procedimento			= cd_procedimento_w,
								ie_origem_proced		= ie_origem_proced_w
							where	nr_sequencia		= r_C01.nr_sequencia;

							atualiza_preco_procedimento(r_C01.nr_sequencia ,r_C01.cd_convenio,:new.nm_usuario);

						end if;
					end;
				end loop;
			end;
		end if;
		commit;
	end;
	end if;
end;
/


ALTER TABLE TASY.EPISODIO_PACIENTE ADD (
  CONSTRAINT CAEPPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EPISODIO_PACIENTE ADD (
  CONSTRAINT CAEPPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CAEPPA_CATPEP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EPISODIO) 
 REFERENCES TASY.TIPO_EPISODIO (NR_SEQUENCIA),
  CONSTRAINT CAEPPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CAEPPA_MOTREACA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REABERTURA) 
 REFERENCES TASY.MOTIVO_REABERTURA_CASE (NR_SEQUENCIA),
  CONSTRAINT CAEPPA_CASUEP_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_EPISODIO) 
 REFERENCES TASY.SUBTIPO_EPISODIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.EPISODIO_PACIENTE TO NIVEL_1;

