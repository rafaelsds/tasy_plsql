ALTER TABLE TASY.REGRA_EMISSAO_RELAT_ADEP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_EMISSAO_RELAT_ADEP CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_EMISSAO_RELAT_ADEP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO_NREC    DATE                   NOT NULL,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_DIETA_ORAL          VARCHAR2(1 BYTE)       NOT NULL,
  IE_SUPLEMENTO_ORAL     VARCHAR2(1 BYTE)       NOT NULL,
  IE_MEDICAMENTO         VARCHAR2(1 BYTE)       NOT NULL,
  IE_MATERIAL            VARCHAR2(1 BYTE)       NOT NULL,
  IE_PROCEDIMENTO        VARCHAR2(1 BYTE)       NOT NULL,
  IE_RECOMENDACAO        VARCHAR2(1 BYTE)       NOT NULL,
  IE_SAE                 VARCHAR2(1 BYTE)       NOT NULL,
  IE_LABORATORIO         VARCHAR2(1 BYTE)       NOT NULL,
  IE_SOLUCAO             VARCHAR2(1 BYTE),
  IE_GASOTERAPIA         VARCHAR2(1 BYTE)       NOT NULL,
  IE_SUPLEMENTO_ENTERAL  VARCHAR2(1 BYTE)       NOT NULL,
  IE_DIALISE             VARCHAR2(1 BYTE)       NOT NULL,
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  CD_PERFIL              NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REEMREA_PERFIL_FK_I ON TASY.REGRA_EMISSAO_RELAT_ADEP
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REEMREA_PK ON TASY.REGRA_EMISSAO_RELAT_ADEP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REEMREA_PK
  MONITORING USAGE;


CREATE INDEX TASY.REEMREA_SETATEN_FK_I ON TASY.REGRA_EMISSAO_RELAT_ADEP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REEMREA_UK ON TASY.REGRA_EMISSAO_RELAT_ADEP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REEMREA_UK
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_EMISSAO_RELAT_ADEP ADD (
  CONSTRAINT REEMREA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT REEMREA_UK
 UNIQUE (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_EMISSAO_RELAT_ADEP ADD (
  CONSTRAINT REEMREA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REEMREA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REEMREA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_EMISSAO_RELAT_ADEP TO NIVEL_1;

