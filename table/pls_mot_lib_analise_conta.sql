ALTER TABLE TASY.PLS_MOT_LIB_ANALISE_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOT_LIB_ANALISE_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOT_LIB_ANALISE_CONTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DS_MOTIVO_LIBERACAO    VARCHAR2(255 BYTE)     NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIPO_MOTIVO         VARCHAR2(3 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_PARECER             VARCHAR2(1 BYTE),
  IE_SUBSTITUI_ITEM      VARCHAR2(1 BYTE),
  IE_DOCUMENTO_FISICO    VARCHAR2(1 BYTE),
  IE_PRE_ANALISE         VARCHAR2(1 BYTE),
  DS_PARECER             VARCHAR2(4000 BYTE),
  IE_RECONSISTENCIA      VARCHAR2(1 BYTE),
  IE_GLOSA_MANUAL        VARCHAR2(1 BYTE),
  IE_FATURAMENTO         VARCHAR2(1 BYTE),
  IE_POS_ESTAB           VARCHAR2(1 BYTE),
  IE_ANALISE_CONTA_MED   VARCHAR2(1 BYTE),
  IE_LIBERACAO_ESPECIAL  VARCHAR2(1 BYTE),
  IE_TIPO_CONTA          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMLAC_ESTABEL_FK_I ON TASY.PLS_MOT_LIB_ANALISE_CONTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMLAC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSMLAC_PK ON TASY.PLS_MOT_LIB_ANALISE_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMLAC_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_MOT_LIB_ANALISE_CONTA ADD (
  CONSTRAINT PLSMLAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MOT_LIB_ANALISE_CONTA ADD (
  CONSTRAINT PLSMLAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MOT_LIB_ANALISE_CONTA TO NIVEL_1;

