ALTER TABLE TASY.PLS_MIGRACAO_BENEFICIARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MIGRACAO_BENEFICIARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MIGRACAO_BENEFICIARIO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CONTRATO_ATUAL       NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO_MIG         NUMBER(10),
  NR_SEQ_PAGADOR              NUMBER(10),
  NR_SEQ_TITULAR              NUMBER(10),
  NR_SEQ_PLANO                NUMBER(10),
  NR_SEQ_TABELA_PRECO         NUMBER(10),
  NR_SEQ_CANAL_VENDA          NUMBER(10),
  NR_SEQ_VENDEDOR             NUMBER(10),
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  NR_SEQ_CAUSA_RESCISAO       NUMBER(10),
  DT_MIGRACAO                 DATE,
  DT_LIMITE_UTILIZACAO        DATE,
  NR_SEQ_MOTIVO_ALTERACAO     NUMBER(10),
  NR_SEQ_SEGURADO             NUMBER(10),
  DT_SOLICITACAO              DATE,
  IE_STATUS                   NUMBER(1),
  NR_SEQ_PROPOSTA             NUMBER(10),
  IE_MIGRAR_DEPENDENTE        VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_REJEICAO      NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMIBE_PK ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLCONPAG_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSCARS_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_CAUSA_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSCONT_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_CONTRATO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSCONT_FK1_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_CONTRATO_MIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSINBR_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_MOTIVO_REJEICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSMALP_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_MOTIVO_ALTERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSMOCA_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSPLAN_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSPRAD_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSSEGU_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSSEGU_FK2_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSTAPR_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_TABELA_PRECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSVEND_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_CANAL_VENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMIBE_PLSVEVI_FK_I ON TASY.PLS_MIGRACAO_BENEFICIARIO
(NR_SEQ_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_MIGRACAO_BENEFICIARIO ADD (
  CONSTRAINT PLSMIBE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MIGRACAO_BENEFICIARIO ADD (
  CONSTRAINT PLSMIBE_PLSINBR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REJEICAO) 
 REFERENCES TASY.PLS_INCLUSAO_BENEF_REPROV (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSCARS_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_RESCISAO) 
 REFERENCES TASY.PLS_CAUSA_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_ATUAL) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSCONT_FK1 
 FOREIGN KEY (NR_SEQ_CONTRATO_MIG) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSMALP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ALTERACAO) 
 REFERENCES TASY.PLS_MOTIVO_ALT_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSSEGU_FK2 
 FOREIGN KEY (NR_SEQ_TITULAR) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA_PRECO) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_CANAL_VENDA) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSVEVI_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR) 
 REFERENCES TASY.PLS_VENDEDOR_VINCULADO (NR_SEQUENCIA),
  CONSTRAINT PLSMIBE_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MIGRACAO_BENEFICIARIO TO NIVEL_1;

