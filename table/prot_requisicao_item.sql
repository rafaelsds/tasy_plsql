ALTER TABLE TASY.PROT_REQUISICAO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROT_REQUISICAO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROT_REQUISICAO_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PROT_REQUISICAO  NUMBER(10)            NOT NULL,
  CD_MATERIAL             NUMBER(6)             NOT NULL,
  QT_MATERIAL             NUMBER(13,4)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROTREI_MATERIA_FK_I ON TASY.PROT_REQUISICAO_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROTREI_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROTREI_PK ON TASY.PROT_REQUISICAO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROTREI_PROTREQ_FK_I ON TASY.PROT_REQUISICAO_ITEM
(NR_SEQ_PROT_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROTREI_PROTREQ_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROT_REQUISICAO_ITEM ADD (
  CONSTRAINT PROTREI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROT_REQUISICAO_ITEM ADD (
  CONSTRAINT PROTREI_PROTREQ_FK 
 FOREIGN KEY (NR_SEQ_PROT_REQUISICAO) 
 REFERENCES TASY.PROT_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROTREI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PROT_REQUISICAO_ITEM TO NIVEL_1;

