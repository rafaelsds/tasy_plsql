ALTER TABLE TASY.CML_PROSPECT_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CML_PROSPECT_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.CML_PROSPECT_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROSPECT      NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_HIST     NUMBER(10)               NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  DT_LIBERACAO         DATE,
  DS_HISTORICO         LONG                     NOT NULL,
  NR_SEQ_PRODUTO       NUMBER(10),
  NR_SEQ_HIST_SUP      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMLPRHI_CMLPRHI_FK_I ON TASY.CML_PROSPECT_HIST
(NR_SEQ_HIST_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPRHI_CMLPRHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPRHI_CMLPROD_FK_I ON TASY.CML_PROSPECT_HIST
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPRHI_CMLPROD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMLPRHI_CMLPROS_FK_I ON TASY.CML_PROSPECT_HIST
(NR_SEQ_PROSPECT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMLPRHI_CMLTHIS_FK_I ON TASY.CML_PROSPECT_HIST
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPRHI_CMLTHIS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMLPRHI_PK ON TASY.CML_PROSPECT_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMLPRHI_PK
  MONITORING USAGE;


ALTER TABLE TASY.CML_PROSPECT_HIST ADD (
  CONSTRAINT CMLPRHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CML_PROSPECT_HIST ADD (
  CONSTRAINT CMLPRHI_CMLPROS_FK 
 FOREIGN KEY (NR_SEQ_PROSPECT) 
 REFERENCES TASY.CML_PROSPECT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMLPRHI_CMLTHIS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.CML_TIPO_HISTORICO (NR_SEQUENCIA),
  CONSTRAINT CMLPRHI_CMLPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.CML_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT CMLPRHI_CMLPRHI_FK 
 FOREIGN KEY (NR_SEQ_HIST_SUP) 
 REFERENCES TASY.CML_PROSPECT_HIST (NR_SEQUENCIA));

GRANT SELECT ON TASY.CML_PROSPECT_HIST TO NIVEL_1;

