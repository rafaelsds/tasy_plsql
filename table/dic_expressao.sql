ALTER TABLE TASY.DIC_EXPRESSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIC_EXPRESSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIC_EXPRESSAO
(
  CD_EXPRESSAO         NUMBER(10)               NOT NULL,
  DS_EXPRESSAO_BR      VARCHAR2(4000 BYTE),
  DS_EXPRESSAO_MX      VARCHAR2(4000 BYTE),
  DS_EXPRESSAO_US      VARCHAR2(4000 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DS_EXPRESSAO_AR      VARCHAR2(4000 BYTE),
  DS_EXPRESSAO_CO      VARCHAR2(4000 BYTE),
  DS_GLOSSARIO         VARCHAR2(1000 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  DS_REVISAO           VARCHAR2(2000 BYTE),
  DS_EXPRESSAO_DE      VARCHAR2(4000 BYTE),
  DS_EXPRESSAO_SA      VARCHAR2(4000 BYTE),
  DT_APROVACAO         DATE,
  NM_USUARIO_APROV     VARCHAR2(15 BYTE),
  DS_EXPRESSAO_PL      VARCHAR2(4000 BYTE),
  NR_SEQ_ORDEM_SERV    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DICEXPR_I1 ON TASY.DIC_EXPRESSAO
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DICEXPR_PK ON TASY.DIC_EXPRESSAO
(CD_EXPRESSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIC_EXPRESSAO_tp  after update ON TASY.DIC_EXPRESSAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_EXPRESSAO);  ds_c_w:=null; ds_w:=substr(:new.CD_EXPRESSAO,1,500);gravar_log_alteracao(substr(:old.CD_EXPRESSAO,1,4000),substr(:new.CD_EXPRESSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXPRESSAO',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EXPRESSAO_BR,1,500);gravar_log_alteracao(substr(:old.DS_EXPRESSAO_BR,1,4000),substr(:new.DS_EXPRESSAO_BR,1,4000),:new.nm_usuario,nr_seq_w,'DS_EXPRESSAO_BR',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EXPRESSAO_DE,1,500);gravar_log_alteracao(substr(:old.DS_EXPRESSAO_DE,1,4000),substr(:new.DS_EXPRESSAO_DE,1,4000),:new.nm_usuario,nr_seq_w,'DS_EXPRESSAO_DE',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EXPRESSAO_US,1,500);gravar_log_alteracao(substr(:old.DS_EXPRESSAO_US,1,4000),substr(:new.DS_EXPRESSAO_US,1,4000),:new.nm_usuario,nr_seq_w,'DS_EXPRESSAO_US',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_GLOSSARIO,1,500);gravar_log_alteracao(substr(:old.DS_GLOSSARIO,1,4000),substr(:new.DS_GLOSSARIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GLOSSARIO',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_REVISAO,1,500);gravar_log_alteracao(substr(:old.DS_REVISAO,1,4000),substr(:new.DS_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_REVISAO',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EXPRESSAO_MX,1,500);gravar_log_alteracao(substr(:old.DS_EXPRESSAO_MX,1,4000),substr(:new.DS_EXPRESSAO_MX,1,4000),:new.nm_usuario,nr_seq_w,'DS_EXPRESSAO_MX',ie_log_w,ds_w,'DIC_EXPRESSAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.DIC_EXPRESSAO ADD (
  CONSTRAINT DICEXPR_PK
 PRIMARY KEY
 (CD_EXPRESSAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.DIC_EXPRESSAO TO NIVEL_1;

