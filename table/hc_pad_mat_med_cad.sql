ALTER TABLE TASY.HC_PAD_MAT_MED_CAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_PAD_MAT_MED_CAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_PAD_MAT_MED_CAD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PAD_CAD       NUMBER(10),
  CD_MATERIAL          NUMBER(10),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HCPADMAME_HCPADCAD_FK_I ON TASY.HC_PAD_MAT_MED_CAD
(NR_SEQ_PAD_CAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCPADMAME_MATERIA_FK_I ON TASY.HC_PAD_MAT_MED_CAD
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HCPADMAME_PK ON TASY.HC_PAD_MAT_MED_CAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCPADMAME_UNIMEDI_FK_I ON TASY.HC_PAD_MAT_MED_CAD
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCPADMAME_VIAAPLI_FK_I ON TASY.HC_PAD_MAT_MED_CAD
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HC_PAD_MAT_MED_CAD ADD (
  CONSTRAINT HCPADMAME_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.HC_PAD_MAT_MED_CAD ADD (
  CONSTRAINT HCPADMAME_HCPADCAD_FK 
 FOREIGN KEY (NR_SEQ_PAD_CAD) 
 REFERENCES TASY.HC_PAD_CAD (NR_SEQUENCIA),
  CONSTRAINT HCPADMAME_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT HCPADMAME_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT HCPADMAME_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO));

GRANT SELECT ON TASY.HC_PAD_MAT_MED_CAD TO NIVEL_1;

