ALTER TABLE TASY.PTU_FATURA_PRE_SACADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_FATURA_PRE_SACADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_FATURA_PRE_SACADO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_FATURA          NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_GERACAO         NUMBER(10),
  TP_REGISTRO            VARCHAR2(3 BYTE),
  IE_CEDENTE_SACADO      VARCHAR2(1 BYTE),
  CD_OPERADORA_ANS       VARCHAR2(10 BYTE),
  NM_CEDENTE_SACADO      VARCHAR2(255 BYTE),
  DS_ENDERECO            VARCHAR2(255 BYTE),
  DS_ENDERECO_COMPL      VARCHAR2(255 BYTE),
  DS_BAIRRO              VARCHAR2(255 BYTE),
  CD_CEP                 VARCHAR2(8 BYTE),
  DS_CIDADE              VARCHAR2(255 BYTE),
  SG_UF                  VARCHAR2(2 BYTE),
  CD_CNPJ_CPF            VARCHAR2(14 BYTE),
  CD_INSCRICAO_ESTADUAL  VARCHAR2(20 BYTE),
  NR_DDD                 NUMBER(4),
  NR_TELEFONE            NUMBER(9),
  NR_FAX                 NUMBER(9),
  NR_ENDERECO            VARCHAR2(6 BYTE),
  NR_DDI                 VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUFAPS_PK ON TASY.PTU_FATURA_PRE_SACADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFAPS_PK
  MONITORING USAGE;


ALTER TABLE TASY.PTU_FATURA_PRE_SACADO ADD (
  CONSTRAINT PTUFAPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PTU_FATURA_PRE_SACADO TO NIVEL_1;

