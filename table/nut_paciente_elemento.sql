ALTER TABLE TASY.NUT_PACIENTE_ELEMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_PACIENTE_ELEMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_PACIENTE_ELEMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_NUT_PAC        NUMBER(10)              NOT NULL,
  NR_SEQ_ELEMENTO       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  QT_KCAL               NUMBER(7,1),
  QT_ELEMENTO           NUMBER(15,4)            NOT NULL,
  NR_SEQ_ELEM_UNID_MED  NUMBER(10),
  NR_SEQ_ELEM_MAT       NUMBER(10),
  QT_VOLUME             NUMBER(15,4),
  CD_UNID_MED_PRESCR    VARCHAR2(30 BYTE),
  QT_PRESCRICAO         NUMBER(9,3),
  QT_DISPENSAR          NUMBER(9,3),
  QT_VOL_1_FASE         NUMBER(15,4),
  QT_VOL_2_FASE         NUMBER(15,4),
  QT_VOL_3_FASE         NUMBER(15,4),
  CD_PERFIL_ATIVO       NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTPAEL_NUTELCA_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(NR_SEQ_ELEM_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPAEL_NUTELEM_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(NR_SEQ_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPAEL_NUTELEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPAEL_NUTELUM_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(NR_SEQ_ELEM_UNID_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPAEL_NUTELUM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPAEL_NUTPACI_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(NR_SEQ_NUT_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPAEL_PERFIL_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPAEL_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NUTPAEL_PK ON TASY.NUT_PACIENTE_ELEMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPAEL_UNIMEDI_FK_I ON TASY.NUT_PACIENTE_ELEMENTO
(CD_UNID_MED_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPAEL_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.NUT_PACIENTE_ELEMENTO ADD (
  CONSTRAINT NUTPAEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_PACIENTE_ELEMENTO ADD (
  CONSTRAINT NUTPAEL_NUTPACI_FK 
 FOREIGN KEY (NR_SEQ_NUT_PAC) 
 REFERENCES TASY.NUT_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NUTPAEL_NUTELEM_FK 
 FOREIGN KEY (NR_SEQ_ELEMENTO) 
 REFERENCES TASY.NUT_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NUTPAEL_NUTELCA_FK 
 FOREIGN KEY (NR_SEQ_ELEM_MAT) 
 REFERENCES TASY.NUT_ELEMENTO_CADASTRO (NR_SEQUENCIA),
  CONSTRAINT NUTPAEL_UNIMEDI_FK 
 FOREIGN KEY (CD_UNID_MED_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT NUTPAEL_NUTELUM_FK 
 FOREIGN KEY (NR_SEQ_ELEM_UNID_MED) 
 REFERENCES TASY.NUT_ELEMENTO_UNID_MED (NR_SEQUENCIA),
  CONSTRAINT NUTPAEL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.NUT_PACIENTE_ELEMENTO TO NIVEL_1;

