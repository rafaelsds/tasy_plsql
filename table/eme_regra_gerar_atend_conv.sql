ALTER TABLE TASY.EME_REGRA_GERAR_ATEND_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EME_REGRA_GERAR_ATEND_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.EME_REGRA_GERAR_ATEND_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONVENIO          NUMBER(5),
  CD_CATEGORIA         VARCHAR2(10 BYTE),
  CD_TIPO_ACOMODACAO   NUMBER(4),
  NR_SEQ_TIPO_SERVICO  NUMBER(10),
  NR_SEQ_REGRA_ATEND   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMREGEC_CATCONV_FK_I ON TASY.EME_REGRA_GERAR_ATEND_CONV
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMREGEC_CONVENI_FK_I ON TASY.EME_REGRA_GERAR_ATEND_CONV
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMREGEC_EMETISE_FK_I ON TASY.EME_REGRA_GERAR_ATEND_CONV
(NR_SEQ_TIPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_EMETISE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMREGEC_EMREGEA_FK_I ON TASY.EME_REGRA_GERAR_ATEND_CONV
(NR_SEQ_REGRA_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_EMREGEA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EMREGEC_PK ON TASY.EME_REGRA_GERAR_ATEND_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_PK
  MONITORING USAGE;


CREATE INDEX TASY.EMREGEC_TIPACOM_FK_I ON TASY.EME_REGRA_GERAR_ATEND_CONV
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMREGEC_TIPACOM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EME_REGRA_GERAR_ATEND_CONV ADD (
  CONSTRAINT EMREGEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EME_REGRA_GERAR_ATEND_CONV ADD (
  CONSTRAINT EMREGEC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT EMREGEC_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT EMREGEC_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT EMREGEC_EMETISE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_SERVICO) 
 REFERENCES TASY.EME_TIPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT EMREGEC_EMREGEA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ATEND) 
 REFERENCES TASY.EME_REGRA_GERAR_ATEND_REG (NR_SEQUENCIA));

GRANT SELECT ON TASY.EME_REGRA_GERAR_ATEND_CONV TO NIVEL_1;

