ALTER TABLE TASY.MAN_AVAL_EFICACIA_TRE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_AVAL_EFICACIA_TRE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_AVAL_EFICACIA_TRE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  NR_SEQ_ORDEM_TRE  NUMBER(10),
  NR_SEQ_AGENDA     NUMBER(10),
  NR_SEQ_ORDEM      NUMBER(10),
  IE_GERADO_ORDEM   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANAET_MANORSE_FK_I ON TASY.MAN_AVAL_EFICACIA_TRE
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANAET_MANORSE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANAET_PK ON TASY.MAN_AVAL_EFICACIA_TRE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANAET_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_AVAL_EFICACIA_TRE ADD (
  CONSTRAINT MANAET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_AVAL_EFICACIA_TRE ADD (
  CONSTRAINT MANAET_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_AVAL_EFICACIA_TRE TO NIVEL_1;

