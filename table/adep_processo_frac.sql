ALTER TABLE TASY.ADEP_PROCESSO_FRAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADEP_PROCESSO_FRAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADEP_PROCESSO_FRAC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_PROCESSO          NUMBER(10)           NOT NULL,
  NR_SEQ_DIGITO            NUMBER(10),
  DT_GERACAO               DATE                 NOT NULL,
  DT_PREPARO               DATE,
  DT_FIM_PREPARO           DATE,
  NM_USUARIO_PREPARO       VARCHAR2(15 BYTE),
  NM_USUARIO_FIM_PREPARO   VARCHAR2(15 BYTE),
  IE_STATUS_FRAC           VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_PROCESSO              DATE,
  NM_USUARIO_PROCESSO      VARCHAR2(15 BYTE),
  DS_OBS_PREPARO           VARCHAR2(255 BYTE),
  DS_LOTE                  VARCHAR2(25 BYTE),
  NM_FABRICANTE            VARCHAR2(255 BYTE),
  DT_VALIDADE              DATE,
  IE_IMPRESSO              VARCHAR2(1 BYTE),
  NM_USUARIO_CONF_PREPARO  VARCHAR2(15 BYTE),
  DT_CONFERENCIA_PREP      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADEPRFR_ADEPROC_FK_I ON TASY.ADEP_PROCESSO_FRAC
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPRFR_ADEPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ADEPRFR_PK ON TASY.ADEP_PROCESSO_FRAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.adep_processo_frac_atual
before insert or update ON TASY.ADEP_PROCESSO_FRAC for each row
declare
ie_status_frac_w		varchar2(15) := 'X';
ie_grava_log_gedipa_w	varchar2(15);

begin

select	nvl(max(ie_grava_log_gedipa),'S')
into	ie_grava_log_gedipa_w
from	parametros_farmacia
where	cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

if	(:old.dt_processo is null) and
	(:new.dt_processo is not null) then
	ie_status_frac_w	:= 'P';
elsif	(:old.dt_cancelamento is null) and
	(:new.dt_cancelamento is not null) then
	ie_status_frac_w	:= 'C';
elsif	(:old.dt_fim_preparo is null) and
	(:new.dt_fim_preparo is not null) then
	ie_status_frac_w	:= 'F';
elsif	(:old.dt_preparo is null) and
	(:new.dt_preparo is not null) then
	ie_status_frac_w	:= 'I';
elsif	(:old.dt_geracao is null) and
	(:new.dt_geracao is not null) then
	ie_status_frac_w	:= 'G';
end if;

if	(ie_status_frac_w <> 'X') then
	:new.ie_status_frac	:= ie_status_frac_w;
end if;

if	(ie_grava_log_gedipa_w = 'S') then
	insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
	values (obter_nextval_sequence('log_gedipa'), sysdate, 17, 'ADEP_PROCESSO_FRAC_ATUAL', null,null,
	substr('NR_SEQ_PROCESSO= '||:new.nr_seq_processo || ' - IE_STATUS_FRAC_W= ' || ie_status_frac_w ||
			' - NM_USUARIO= ' || wheb_usuario_pck.get_nm_usuario || ' - FUNCAO= ' || obter_funcao_ativa,1,200));
end if;

end;
/


ALTER TABLE TASY.ADEP_PROCESSO_FRAC ADD (
  CONSTRAINT ADEPRFR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADEP_PROCESSO_FRAC ADD (
  CONSTRAINT ADEPRFR_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ADEP_PROCESSO_FRAC TO NIVEL_1;

