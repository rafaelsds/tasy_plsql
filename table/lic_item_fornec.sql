ALTER TABLE TASY.LIC_ITEM_FORNEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LIC_ITEM_FORNEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.LIC_ITEM_FORNEC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_FORNEC        NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_ITEM              NUMBER(15,4)             NOT NULL,
  IE_VENCEDOR          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_PARECER       NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LICITFO_LICFORN_FK_I ON TASY.LIC_ITEM_FORNEC
(NR_SEQ_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LICITFO_LICFORN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LICITFO_LICITEM_FK_I ON TASY.LIC_ITEM_FORNEC
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LICITFO_LICITEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LICITFO_LICPAIT_FK_I ON TASY.LIC_ITEM_FORNEC
(NR_SEQ_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LICITFO_LICPAIT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LICITFO_PK ON TASY.LIC_ITEM_FORNEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LICITFO_PK
  MONITORING USAGE;


ALTER TABLE TASY.LIC_ITEM_FORNEC ADD (
  CONSTRAINT LICITFO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LIC_ITEM_FORNEC ADD (
  CONSTRAINT LICITFO_LICFORN_FK 
 FOREIGN KEY (NR_SEQ_FORNEC) 
 REFERENCES TASY.LIC_FORNEC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LICITFO_LICITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.LIC_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LICITFO_LICPAIT_FK 
 FOREIGN KEY (NR_SEQ_PARECER) 
 REFERENCES TASY.LIC_PARECER_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.LIC_ITEM_FORNEC TO NIVEL_1;

