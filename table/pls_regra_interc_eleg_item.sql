ALTER TABLE TASY.PLS_REGRA_INTERC_ELEG_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_INTERC_ELEG_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_INTERC_ELEG_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_REGRA            NUMBER(10)            NOT NULL,
  IE_TIPO_REGRA           VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_RESIDE_FORA_ATUACAO  VARCHAR2(1 BYTE),
  IE_CARATER_ATENDIMENTO  VARCHAR2(1 BYTE),
  QT_MESES_ANT            NUMBER(10),
  QT_UTILIZACAO           NUMBER(10),
  IE_AREA_ABRANGENCIA     VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGEI_PK ON TASY.PLS_REGRA_INTERC_ELEG_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGEI_PLSRIEG_FK_I ON TASY.PLS_REGRA_INTERC_ELEG_ITEM
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_INTERC_ELEG_ITEM ADD (
  CONSTRAINT PLSRGEI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_REGRA_INTERC_ELEG_ITEM ADD (
  CONSTRAINT PLSRGEI_PLSRIEG_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_INTERC_ELEGIVEL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_INTERC_ELEG_ITEM TO NIVEL_1;

