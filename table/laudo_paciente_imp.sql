ALTER TABLE TASY.LAUDO_PACIENTE_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE_IMP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LAUDO         NUMBER(10),
  NM_SOLICITANTE       VARCHAR2(255 BYTE)       NOT NULL,
  DT_SOLICITACAO       DATE                     NOT NULL,
  NR_SEQ_PRIORIDADE    NUMBER(10),
  IE_STATUS            VARCHAR2(15 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  DS_CONTATOS          VARCHAR2(255 BYTE),
  NR_ATENDIMENTO       NUMBER(10),
  NR_SEQ_LOTE          NUMBER(10),
  NR_PRESCRICAO        NUMBER(14),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  DT_PREVISAO_ENTREGA  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUPIMP_ATEPACI_FK_I ON TASY.LAUDO_PACIENTE_IMP
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPIMP_LAUPACI_FK_I ON TASY.LAUDO_PACIENTE_IMP
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPIMP_LAUPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPIMP_LAUPACP_FK_I ON TASY.LAUDO_PACIENTE_IMP
(NR_SEQ_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPIMP_LAUPACP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPIMP_PESFISI_FK_I ON TASY.LAUDO_PACIENTE_IMP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAUPIMP_PK ON TASY.LAUDO_PACIENTE_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPIMP_PK
  MONITORING USAGE;


CREATE INDEX TASY.LAUPIMP_PRESMED_FK_I ON TASY.LAUDO_PACIENTE_IMP
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPIMP_PRESMED_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_imp_trigger
after insert ON TASY.LAUDO_PACIENTE_IMP for each row
declare
cd_setor_registro		number(10);
nr_laudo_w			number(10);
ds_email_origem_w		varchar2(1000);
ds_email_destino_w		varchar2(1000);
nm_pessoa_fisica_w		varchar2(255);
nm_usuario_envio_email_w	varchar2(255);


Cursor C01 is
	select b.cd_setor_registro
	from laudo_paciente a,
	     laudo_paciente_loc b
	where a.nr_sequencia = :new.nr_seq_laudo
	and   a.nr_sequencia = b.nr_sequencia
	and   b.dt_retirada is null
	order by 1;

Cursor C02  is
	select 	ds_email,
		nm_usuario_envio_email
	from   	regra_laudo_loc_email
	where  	cd_setor_atendimento = cd_setor_registro
	order by 1;

begin
nm_pessoa_fisica_w := obter_nome_pf(:new.cd_pessoa_fisica);

select 	max(nr_laudo)
into	nr_laudo_w
from 	laudo_paciente
where 	nr_sequencia = :new.nr_seq_laudo;


if	(nr_laudo_w is not null) then

	open C01;
	loop
	fetch C01 into
		cd_setor_registro;
	exit when C01%notfound;
		begin

		open C02;
		loop
		fetch C02 into
			ds_email_destino_w,
			nm_usuario_envio_email_w;
		exit when C02%notfound;
			begin

			select 	ds_email
			into	ds_email_origem_w
			from 	usuario
			where 	nm_usuario like nm_usuario_envio_email_w;

			enviar_email(	wheb_mensagem_pck.get_texto(305917,'NR_LAUDO_W='||nr_laudo_w), --'Nova solicitação para o laudo '  || nr_laudo_w,
					wheb_mensagem_pck.get_texto(305922,'NR_LAUDO_W='||nr_laudo_w||';NM_PESSOA_FISICA_W='||nm_pessoa_fisica_w), /*'Para o laudo '  || nr_laudo_w || ' do paceinte '|| nm_pessoa_fisica_w || ' foi gerado uma nova solicitação de impressão.',*/
					ds_email_origem_w,
					ds_email_destino_w,
					nm_usuario_envio_email_w,
					'M');
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;



end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.up_laudo_paciente_imp_trigger
after update ON TASY.LAUDO_PACIENTE_IMP for each row
declare
cd_setor_registro_w		number(10);
nr_laudo_w			number(10);
ds_email_origem_w		varchar2(1000);
ds_email_destino_w		varchar2(1000);
nm_pessoa_fisica_w		varchar2(255);
nm_usuario_envio_email_w	varchar2(255);


Cursor C01 is
	select 	cd_setor_registro
	from 	laudo_paciente_loc
	where 	:new.nr_seq_laudo = nr_sequencia
	and   	dt_retirada is null
	order by 1;

Cursor C02  is
	select 	ds_email,
		nm_usuario_envio_email
	from   	regra_laudo_loc_email
	where  	cd_setor_atendimento = cd_setor_registro_w
	order by 1;

begin

if 	(:old.ie_status <> :new.ie_status) and
	(:new.ie_status = 'I') then
	begin
	nm_pessoa_fisica_w := obter_nome_pf(:new.cd_pessoa_fisica);

	select 	nr_laudo
	into	nr_laudo_w
	from 	laudo_paciente
	where 	nr_sequencia = :new.nr_seq_laudo;

	open C01;
	loop
	fetch C01 into
		cd_setor_registro_w;
	exit when C01%notfound;
		begin
		open C02;
		loop
		fetch C02 into
			ds_email_destino_w,
			nm_usuario_envio_email_w;
		exit when C02%notfound;
			begin
			select 	ds_email
			into	ds_email_origem_w
			from 	usuario
			where 	nm_usuario like nm_usuario_envio_email_w;

			enviar_email(	wheb_mensagem_pck.get_texto(305935,'NR_LAUDO_W='||nr_laudo_w), --'Alterado o Status da Solicitação para o laudo '  || nr_laudo_w,
					wheb_mensagem_pck.get_texto(305937,'NR_LAUDO_W='||nr_laudo_w||';NM_PESSOA_FISICA_W='||nm_pessoa_fisica_w), --'Para o laudo '  || nr_laudo_w || ' do paceinte '|| nm_pessoa_fisica_w || ' foi alterado o status da solicitação de impressão para Impresso.',
					ds_email_origem_w,
					ds_email_destino_w,
					nm_usuario_envio_email_w,
					'M');
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
	end;
end if;

end;
/


ALTER TABLE TASY.LAUDO_PACIENTE_IMP ADD (
  CONSTRAINT LAUPIMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE_IMP ADD (
  CONSTRAINT LAUPIMP_LAUPACI_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LAUPIMP_LAUPACP_FK 
 FOREIGN KEY (NR_SEQ_PRIORIDADE) 
 REFERENCES TASY.LAUDO_PACIENTE_PRIORIDADE (NR_SEQUENCIA),
  CONSTRAINT LAUPIMP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT LAUPIMP_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT LAUPIMP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.LAUDO_PACIENTE_IMP TO NIVEL_1;

