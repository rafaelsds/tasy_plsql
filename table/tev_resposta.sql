ALTER TABLE TASY.TEV_RESPOSTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEV_RESPOSTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEV_RESPOSTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_TITULO             VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_CLINICO_CIRURGICO  VARCHAR2(1 BYTE)        NOT NULL,
  DS_RESPOSTA           VARCHAR2(4000 BYTE)     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TEVRESP_PK ON TASY.TEV_RESPOSTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TEV_RESPOSTA_ATUAL
before insert or update or delete ON TASY.TEV_RESPOSTA for each row
declare

begin
	if (obter_se_base_wheb = 'N') then
		if (inserting) then
			wheb_mensagem_pck.exibir_mensagem_abort(494671);
		elsif (updating) then
			wheb_mensagem_pck.exibir_mensagem_abort(494672);
		elsif (deleting) then
			wheb_mensagem_pck.exibir_mensagem_abort(494675);
		end if;
	end if;
end;
/


ALTER TABLE TASY.TEV_RESPOSTA ADD (
  CONSTRAINT TEVRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TEV_RESPOSTA TO NIVEL_1;

