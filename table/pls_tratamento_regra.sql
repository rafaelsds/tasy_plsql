ALTER TABLE TASY.PLS_TRATAMENTO_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TRATAMENTO_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TRATAMENTO_REGRA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  NR_SEQ_TRATAMENTO        NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_UNID_TEMPO            VARCHAR2(1 BYTE),
  DT_FIM_VIGENCIA          DATE,
  QT_IDADE_BENEF           NUMBER(3),
  IE_PRECO                 VARCHAR2(2 BYTE),
  IE_TIPO_SEGURADO         VARCHAR2(3 BYTE),
  CD_ESPECIALIDADE_MEDICA  NUMBER(5),
  NR_SEQ_TIPO_PRESTADOR    NUMBER(10),
  IE_SEXO                  VARCHAR2(1 BYTE),
  QT_TEMPO_TRATAMENTO      NUMBER(5)            NOT NULL,
  QT_IDADE_MAX             NUMBER(3),
  IE_TIPO_GUIA             VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTRRE_ESPMEDI_FK_I ON TASY.PLS_TRATAMENTO_REGRA
(CD_ESPECIALIDADE_MEDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRE_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTRRE_PK ON TASY.PLS_TRATAMENTO_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRRE_PLSTIPR_FK_I ON TASY.PLS_TRATAMENTO_REGRA
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRE_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRRE_PLSTRAT_FK_I ON TASY.PLS_TRATAMENTO_REGRA
(NR_SEQ_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRE_PLSTRAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_TRATAMENTO_REGRA_tp  after update ON TASY.PLS_TRATAMENTO_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_TRATAMENTO_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PLS_TRATAMENTO_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TRATAMENTO_REGRA ADD (
  CONSTRAINT PLSTRRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TRATAMENTO_REGRA ADD (
  CONSTRAINT PLSTRRE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICA) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PLSTRRE_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTRRE_PLSTRAT_FK 
 FOREIGN KEY (NR_SEQ_TRATAMENTO) 
 REFERENCES TASY.PLS_TRATAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TRATAMENTO_REGRA TO NIVEL_1;

