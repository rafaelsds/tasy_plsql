ALTER TABLE TASY.INTERROGATORIO_APARELHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERROGATORIO_APARELHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERROGATORIO_APARELHO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_MEDICO               VARCHAR2(10 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  DT_REGISTRO             DATE                  NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DS_SINAIS_SINTOMAS      VARCHAR2(4000 BYTE),
  DT_LIBERACAO            DATE,
  DS_APARELHO_CARDIO      VARCHAR2(4000 BYTE),
  DT_INATIVACAO           DATE,
  DS_APARELHO_RESP        VARCHAR2(4000 BYTE),
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_APARELHO_DIGES       VARCHAR2(4000 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  DS_SISTEMA_NEUFRO       VARCHAR2(4000 BYTE),
  DS_SISTEMA_ENDOCRINO    VARCHAR2(4000 BYTE),
  DS_SISTEMA_HEMATO       VARCHAR2(4000 BYTE),
  DS_SISTEMA_NERVOSO      VARCHAR2(4000 BYTE),
  DS_SISTEMA_ESQUELETICO  VARCHAR2(4000 BYTE),
  DS_PELE                 VARCHAR2(4000 BYTE),
  DS_SENTIDO_ORGAO        VARCHAR2(4000 BYTE),
  DS_ESFERA               VARCHAR2(4000 BYTE),
  DS_CONDICAO_ATUAL       VARCHAR2(4000 BYTE),
  DS_PROGRNOSTICO         VARCHAR2(4000 BYTE),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTAPAR_ATCONSPEPA_FK_I ON TASY.INTERROGATORIO_APARELHO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTAPAR_ATEPACI_FK_I ON TASY.INTERROGATORIO_APARELHO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTAPAR_PESFISI_FK_I ON TASY.INTERROGATORIO_APARELHO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTAPAR_PK ON TASY.INTERROGATORIO_APARELHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTAPAR_PROFISS_FK_I ON TASY.INTERROGATORIO_APARELHO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTERROGATORIO_APARELHO ADD (
  CONSTRAINT INTAPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERROGATORIO_APARELHO ADD (
  CONSTRAINT INTAPAR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT INTAPAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT INTAPAR_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT INTAPAR_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTERROGATORIO_APARELHO TO NIVEL_1;

