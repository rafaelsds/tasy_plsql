ALTER TABLE TASY.CEP_ESPECIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CEP_ESPECIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CEP_ESPECIAL
(
  NR_CEP_ESPECIAL       NUMBER(9)               NOT NULL,
  CD_CEP_ESPECIAL       NUMBER(8)               NOT NULL,
  NM_CEP_ESPECIAL       VARCHAR2(55 BYTE)       NOT NULL,
  DS_ENDERECO_ESP       VARCHAR2(55 BYTE)       NOT NULL,
  NM_LOCALIDADE_ESP     VARCHAR2(55 BYTE)       NOT NULL,
  NM_BAIRRO_ESP         VARCHAR2(30 BYTE)       NOT NULL,
  CD_UNIDADE_FEDERACAO  VARCHAR2(2 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CEPESPE_CODIGO_I ON TASY.CEP_ESPECIAL
(CD_CEP_ESPECIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CEPESPE_CODIGO_I
  MONITORING USAGE;


CREATE INDEX TASY.CEPESPE_I2 ON TASY.CEP_ESPECIAL
(CD_UNIDADE_FEDERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CEPESPE_NOME_I ON TASY.CEP_ESPECIAL
(NM_CEP_ESPECIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CEPESPE_NOME_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CEPESPE_PK ON TASY.CEP_ESPECIAL
(NR_CEP_ESPECIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CEP_ESPECIAL ADD (
  CONSTRAINT CEPESPE_PK
 PRIMARY KEY
 (NR_CEP_ESPECIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CEP_ESPECIAL TO NIVEL_1;

