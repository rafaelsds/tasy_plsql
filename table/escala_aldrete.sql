ALTER TABLE TASY.ESCALA_ALDRETE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ALDRETE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ALDRETE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CIRURGIA                NUMBER(10),
  IE_ATIVIDADE               NUMBER(2)          NOT NULL,
  IE_RESPIRACAO              NUMBER(2)          NOT NULL,
  IE_PRESSAO_ARTERIAL        NUMBER(2)          NOT NULL,
  IE_CONSCIENCIA             NUMBER(2)          NOT NULL,
  IE_COLORACAO               NUMBER(2)          NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  QT_ALDRETE                 NUMBER(2),
  NR_SEQ_SAEP                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  NR_ATENDIMENTO             NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  IE_SATURATION_O2           NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCALDR_ATEPACI_FK_I ON TASY.ESCALA_ALDRETE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALDR_CIRURGI_FK_I ON TASY.ESCALA_ALDRETE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALDR_I1 ON TASY.ESCALA_ALDRETE
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALDR_PEPOCIR_FK_I ON TASY.ESCALA_ALDRETE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALDR_PESFISI_FK_I ON TASY.ESCALA_ALDRETE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCALDR_PK ON TASY.ESCALA_ALDRETE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALDR_SAEPERO_FK_I ON TASY.ESCALA_ALDRETE
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCALDR_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCALDR_TASASDI_FK_I ON TASY.ESCALA_ALDRETE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCALDR_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCALDR_TASASDI_FK2_I ON TASY.ESCALA_ALDRETE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCALDR_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_aldrete_atual
before insert or update ON TASY.ESCALA_ALDRETE for each row
declare

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if	(:new.nr_atendimento is null) and
		(:new.nr_cirurgia is not null) then

		select	nr_atendimento
		into	:new.nr_atendimento
		from	cirurgia
		where	nr_cirurgia = :new.nr_cirurgia;
	end if;


	:new.QT_ALDRETE :=      :new.ie_atividade +
				:new.ie_respiracao +
				:new.ie_pressao_arterial +
				:new.ie_consciencia +
				:new.ie_coloracao +
				nvl(:new.ie_saturation_o2,0);



end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.escala_aldrete_aftupdate
after update ON TASY.ESCALA_ALDRETE for each row
declare

qt_reg_w	number(1);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin
	Gerar_Evolucao_Aldrete(:new.nr_sequencia, :new.nm_usuario);
	exception
		when others then
		null;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_aldrete_pend_atual
after insert or update ON TASY.ESCALA_ALDRETE for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
cd_pessoa_fisica_w 		varchar2(10);
nr_atendimento_w   		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if 	(cd_pessoa_fisica_w is null) and (:new.nr_seq_pepo is not null) then
	select	max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	pepo_cirurgia c
	where	c.nr_sequencia = :new.nr_seq_pepo;
elsif	(:new.nr_atendimento is not null)then
	cd_pessoa_fisica_w := obter_pessoa_atendimento(:new.nr_atendimento,'C');
	nr_atendimento_w :=   :new.nr_atendimento;
end if;

if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'AD';
elsif	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XAD';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, null,null,null,null,null,null,'97');
end if;


<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_ALDRETE_delete
after delete ON TASY.ESCALA_ALDRETE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

goto Final;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_ALDRETE ADD (
  CONSTRAINT ESCALDR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ALDRETE ADD (
  CONSTRAINT ESCALDR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCALDR_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ESCALDR_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT ESCALDR_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT ESCALDR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCALDR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCALDR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_ALDRETE TO NIVEL_1;

