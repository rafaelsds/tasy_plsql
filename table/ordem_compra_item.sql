ALTER TABLE TASY.ORDEM_COMPRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ITEM
(
  NR_ORDEM_COMPRA           NUMBER(10)          NOT NULL,
  NR_ITEM_OCI               NUMBER(5)           NOT NULL,
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  CD_UNIDADE_MEDIDA_COMPRA  VARCHAR2(30 BYTE)   NOT NULL,
  VL_UNITARIO_MATERIAL      NUMBER(13,4)        NOT NULL,
  QT_MATERIAL               NUMBER(13,4)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_PESSOA_SOLICITANTE     VARCHAR2(10 BYTE),
  QT_MATERIAL_ENTREGUE      NUMBER(13,4),
  PR_DESCONTOS              NUMBER(13,4),
  CD_LOCAL_ESTOQUE          NUMBER(4),
  DS_MATERIAL_DIRETO        VARCHAR2(255 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_MOTIVO_ALTERACAO       NUMBER(10),
  NR_COT_COMPRA             NUMBER(10),
  NR_ITEM_COT_COMPRA        NUMBER(5),
  DS_MARCA                  VARCHAR2(30 BYTE),
  VL_ITEM_LIQUIDO           NUMBER(15,2),
  NR_SEQ_APROVACAO          NUMBER(10),
  DT_APROVACAO              DATE,
  CD_CENTRO_CUSTO           NUMBER(8),
  CD_CONTA_CONTABIL         VARCHAR2(20 BYTE),
  NR_SOLIC_COMPRA           NUMBER(10),
  NR_ITEM_SOLIC_COMPRA      NUMBER(5),
  QT_CONV_UNID_FORNEC       NUMBER(9,4),
  IE_GERACAO_SOLIC          VARCHAR2(1 BYTE),
  NR_SEQ_PROJ_REC           NUMBER(10),
  PR_DESC_FINANC            NUMBER(7,4),
  NR_SEQ_LIC_ITEM           NUMBER(10),
  NR_SEQ_CONTA_FINANC       NUMBER(10),
  QT_ORIGINAL               NUMBER(15,4),
  DT_VALIDADE               DATE,
  NR_SEQ_MARCA              NUMBER(10),
  DT_REPROVACAO             DATE,
  NR_SEQ_ORDEM_SERV         NUMBER(10),
  VL_ULTIMA_COMPRA          NUMBER(15,4),
  VL_DIF_ULTIMA_COMPRA      NUMBER(15,4),
  PR_DIF_ULTIMA_COMPRA      NUMBER(13,2),
  NR_SEQ_UNIDADE_ADIC       NUMBER(10),
  NR_SEQ_CRITERIO_RATEIO    NUMBER(10),
  NR_SERIE_MATERIAL         VARCHAR2(80 BYTE),
  NR_SOLIC_COMPRA_CANCEL    NUMBER(10),
  VL_DESCONTO               NUMBER(13,2),
  NR_SEQ_LOTE_FORNEC        NUMBER(10),
  NR_SEQ_PROJ_GPI           NUMBER(10),
  NR_SEQ_ETAPA_GPI          NUMBER(10),
  NR_SEQ_CONTA_GPI          NUMBER(10),
  DT_APROVACAO_ORIG         DATE,
  DT_REPROVACAO_ORIG        DATE,
  NR_CONTRATO               NUMBER(10),
  DT_INICIO_GARANTIA        DATE,
  DT_FIM_GARANTIA           DATE,
  NR_ID_INTEGRACAO          VARCHAR2(255 BYTE),
  NR_SEQ_REG_LIC_ITEM       NUMBER(10),
  NR_SEQ_CONTA_BCO          NUMBER(10),
  NR_SEQ_PRESCR_ITEM        NUMBER(6),
  NR_EMPENHO                VARCHAR2(255 BYTE),
  DT_EMPENHO                DATE,
  NR_ORDEM_AGRUP            NUMBER(10),
  NR_SEQ_ORC_ITEM_GPI       NUMBER(10),
  VL_UNIT_MAT_ORIGINAL      NUMBER(13,4),
  NR_ATENDIMENTO            NUMBER(10),
  QT_DIAS_GARANTIA          NUMBER(5),
  DT_DESDOBR_APROV          DATE,
  DT_LIB_ESTRUT_PJ          DATE,
  NM_USUARIO_LIB            VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_LIB      VARCHAR2(255 BYTE),
  CD_OPERACAO_NF            NUMBER(4),
  CD_NATUREZA_OPERACAO      NUMBER(4),
  NR_SEQ_PENDENCIA_TRANSF   NUMBER(10),
  IE_MOTIVO_REPROVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_REPROV   VARCHAR2(255 BYTE),
  DS_OBS_ITEM_FORN          VARCHAR2(255 BYTE),
  NR_SEQ_REGRA_CONTRATO     NUMBER(10),
  NR_SEQ_PRECO_PJ           NUMBER(10),
  NR_SEQ_AGRUP              NUMBER(10),
  DS_JUSTIF_DIVER           VARCHAR2(4000 BYTE),
  DS_LOTE                   VARCHAR2(25 BYTE),
  NR_DOCUMENTO_EXTERNO      VARCHAR2(100 BYTE),
  DT_LIB_CONFERENCIA        DATE,
  VL_TOTAL_ITEM             NUMBER(13,4),
  NR_SEQ_PROC_APROV         NUMBER(10),
  IE_SERVICO_REALIZADO      VARCHAR2(15 BYTE),
  NR_SEQ_MATPACI            NUMBER(10),
  NR_SEQ_ORDEM_PROD         NUMBER(10),
  DS_MARCA_FORNEC           VARCHAR2(30 BYTE),
  NR_SEQ_REG_PCS            NUMBER(10),
  NR_SEQ_OP_COMP_OPM        NUMBER(10),
  IE_SISTEMA_ORIGEM         VARCHAR2(15 BYTE),
  VL_FRETE_RATEIO           NUMBER(15,2),
  DS_JUSTIF_ESCOLHA_FORNEC  VARCHAR2(255 BYTE),
  CD_FEDERAL_VOUCHER        VARCHAR2(4000 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          25M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCOITE_ATEPACI_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_BANESTA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_CONTA_BCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_CANORPR_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_ORDEM_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_CANORPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_COCOITE_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_COCOSAG_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_COCOSAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_CONFINA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_CONRENF_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_REGRA_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_CONTRAT_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_CRITRAT_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_CRITERIO_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_CRITRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_GPICRETA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_GPICRETA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_GPIORIT_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_GPIORIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_GPIPLAN_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_CONTA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_GPIPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_GPIPROJ_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_GPIPROJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_I1 ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_MATPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_I1
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_LICITEM_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_LIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_LICITEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_LOCESTO_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_MANORSE_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_MARCA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_MATERIA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_MATERIAL, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_NATOPER_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_OPENOTA_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_OPENOTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_PCSREAN_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_REG_PCS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_PESFISI_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_PESSOA_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCOITE_PK ON TASY.ORDEM_COMPRA_ITEM
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_PRORECU_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_SOLCOMP_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_SUPETES_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_PENDENCIA_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_SUPETES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOITE_UNIMEDI_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITE_UNMACOM_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_UNIDADE_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOITE_UNMACOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOIT_CENCUST_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOIT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOIT_CONCONT_FK_I ON TASY.ORDEM_COMPRA_ITEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOIT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOIT_PROCOMP_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SEQ_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOIT_SOLCOIT_FK_I ON TASY.ORDEM_COMPRA_ITEM
(NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTPD_ORDEM_COMPRA_ITEM
before insert or update or delete ON TASY.ORDEM_COMPRA_ITEM 
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_registros_w			number(10);
dt_aprovacao_w			ordem_compra.dt_aprovacao%type;

pragma autonomous_transaction;
begin

reg_integracao_p.cd_estab_documento	:= null;
reg_integracao_p.ie_tipo_ordem		:= null;
reg_integracao_p.nr_seq_tipo_compra	:= null;
reg_integracao_p.nr_seq_mod_compra	:= null;
reg_integracao_p.ds_id_origin		:= null;

select 	max(dt_aprovacao) 
into	dt_aprovacao_w
from 	ordem_compra 
where 	nr_ordem_compra = :new.nr_ordem_compra;


if	(inserting or updating) and
	(dt_aprovacao_w is null) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :new.nr_ordem_compra
	and	ie_evento = '2'
	and	nvl(ie_status,'P') not in ('P','R')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then	
		reg_integracao_p.ie_operacao		:= 'A';			
		gerar_int_padrao.gravar_integracao('2',:new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);
	end if;

elsif	(deleting) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :old.nr_ordem_compra
	and	ie_evento = '2'
	and	ie_status in ('S')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then	
		reg_integracao_p.ie_operacao		:= 'E';
		gerar_int_padrao.gravar_integracao('2',:old.nr_ordem_compra,:old.nm_usuario, reg_integracao_p);
	end if;
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_tp  after update ON TASY.ORDEM_COMPRA_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ORDEM_COMPRA='||to_char(:old.NR_ORDEM_COMPRA)||'#@#@NR_ITEM_OCI='||to_char(:old.NR_ITEM_OCI);gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_PROD,1,4000),substr(:new.NR_SEQ_ORDEM_PROD,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_PROD',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REG_PCS,1,4000),substr(:new.NR_SEQ_REG_PCS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REG_PCS',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MARCA_FORNEC,1,4000),substr(:new.DS_MARCA_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'DS_MARCA_FORNEC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MARCA,1,4000),substr(:new.DS_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MARCA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_COMPRA,1,4000),substr(:new.CD_UNIDADE_MEDIDA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_SOLICITANTE,1,4000),substr(:new.CD_PESSOA_SOLICITANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SOLICITANTE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL_ENTREGUE,1,4000),substr(:new.QT_MATERIAL_ENTREGUE,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL_ENTREGUE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_ALTERACAO,1,4000),substr(:new.CD_MOTIVO_ALTERACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_ALTERACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_COT_COMPRA,1,4000),substr(:new.NR_COT_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_COT_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_COT_COMPRA,1,4000),substr(:new.NR_ITEM_COT_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_COT_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_APROVACAO,1,4000),substr(:new.DT_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_APROVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APROVACAO,1,4000),substr(:new.NR_SEQ_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APROVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SOLIC_COMPRA,1,4000),substr(:new.NR_SOLIC_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SOLIC_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_SOLIC_COMPRA,1,4000),substr(:new.NR_ITEM_SOLIC_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_SOLIC_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESCONTOS,1,4000),substr(:new.PR_DESCONTOS,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESCONTOS',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_UNITARIO_MATERIAL,1,4000),substr(:new.VL_UNITARIO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNITARIO_MATERIAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ITEM_LIQUIDO,1,4000),substr(:new.VL_ITEM_LIQUIDO,1,4000),:new.nm_usuario,nr_seq_w,'VL_ITEM_LIQUIDO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_OCI,1,4000),substr(:new.NR_ITEM_OCI,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_OCI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROJ_REC,1,4000),substr(:new.NR_SEQ_PROJ_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJ_REC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATERIAL_DIRETO,1,4000),substr(:new.DS_MATERIAL_DIRETO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL_DIRETO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESC_FINANC,1,4000),substr(:new.PR_DESC_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESC_FINANC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_FINANC,1,4000),substr(:new.NR_SEQ_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_FINANC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ORIGINAL,1,4000),substr(:new.QT_ORIGINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_ORIGINAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERACAO_SOLIC,1,4000),substr(:new.IE_GERACAO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERACAO_SOLIC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VALIDADE,1,4000),substr(:new.DT_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_VALIDADE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REPROVACAO,1,4000),substr(:new.DT_REPROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_REPROVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LIC_ITEM,1,4000),substr(:new.NR_SEQ_LIC_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LIC_ITEM',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_SERV,1,4000),substr(:new.NR_SEQ_ORDEM_SERV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_SERV',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ULTIMA_COMPRA,1,4000),substr(:new.VL_ULTIMA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ULTIMA_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DIF_ULTIMA_COMPRA,1,4000),substr(:new.VL_DIF_ULTIMA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'VL_DIF_ULTIMA_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DIF_ULTIMA_COMPRA,1,4000),substr(:new.PR_DIF_ULTIMA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'PR_DIF_ULTIMA_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_TOTAL_ITEM,1,4000),substr(:new.VL_TOTAL_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_TOTAL_ITEM',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SERIE_MATERIAL,1,4000),substr(:new.NR_SERIE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SERIE_MATERIAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CRITERIO_RATEIO,1,4000),substr(:new.NR_SEQ_CRITERIO_RATEIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CRITERIO_RATEIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_UNIDADE_ADIC,1,4000),substr(:new.NR_SEQ_UNIDADE_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_UNIDADE_ADIC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_FORNEC,1,4000),substr(:new.NR_SEQ_LOTE_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_FORNEC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SOLIC_COMPRA_CANCEL,1,4000),substr(:new.NR_SOLIC_COMPRA_CANCEL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SOLIC_COMPRA_CANCEL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESCONTO,1,4000),substr(:new.VL_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROJ_GPI,1,4000),substr(:new.NR_SEQ_PROJ_GPI,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJ_GPI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ETAPA_GPI,1,4000),substr(:new.NR_SEQ_ETAPA_GPI,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ETAPA_GPI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_GPI,1,4000),substr(:new.NR_SEQ_CONTA_GPI,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_GPI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_APROVACAO_ORIG,1,4000),substr(:new.DT_APROVACAO_ORIG,1,4000),:new.nm_usuario,nr_seq_w,'DT_APROVACAO_ORIG',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REPROVACAO_ORIG,1,4000),substr(:new.DT_REPROVACAO_ORIG,1,4000),:new.nm_usuario,nr_seq_w,'DT_REPROVACAO_ORIG',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTRATO,1,4000),substr(:new.NR_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTRATO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_GARANTIA,1,4000),substr(:new.DT_INICIO_GARANTIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_GARANTIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_GARANTIA,1,4000),substr(:new.DT_FIM_GARANTIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_GARANTIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ID_INTEGRACAO,1,4000),substr(:new.NR_ID_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ID_INTEGRACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REG_LIC_ITEM,1,4000),substr(:new.NR_SEQ_REG_LIC_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REG_LIC_ITEM',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_BCO,1,4000),substr(:new.NR_SEQ_CONTA_BCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_BCO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EMPENHO,1,4000),substr(:new.DT_EMPENHO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EMPENHO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_EMPENHO,1,4000),substr(:new.NR_EMPENHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_EMPENHO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESCR_ITEM,1,4000),substr(:new.NR_SEQ_PRESCR_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESCR_ITEM',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_AGRUP,1,4000),substr(:new.NR_ORDEM_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_AGRUP',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_UNIT_MAT_ORIGINAL,1,4000),substr(:new.VL_UNIT_MAT_ORIGINAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNIT_MAT_ORIGINAL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORC_ITEM_GPI,1,4000),substr(:new.NR_SEQ_ORC_ITEM_GPI,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORC_ITEM_GPI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_GARANTIA,1,4000),substr(:new.QT_DIAS_GARANTIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_GARANTIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DESDOBR_APROV,1,4000),substr(:new.DT_DESDOBR_APROV,1,4000),:new.nm_usuario,nr_seq_w,'DT_DESDOBR_APROV',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIB_ESTRUT_PJ,1,4000),substr(:new.DT_LIB_ESTRUT_PJ,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIB_ESTRUT_PJ',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIB,1,4000),substr(:new.NM_USUARIO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIB',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA_LIB,1,4000),substr(:new.DS_JUSTIFICATIVA_LIB,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA_LIB',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NATUREZA_OPERACAO,1,4000),substr(:new.CD_NATUREZA_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_NATUREZA_OPERACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_OPERACAO_NF,1,4000),substr(:new.CD_OPERACAO_NF,1,4000),:new.nm_usuario,nr_seq_w,'CD_OPERACAO_NF',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PENDENCIA_TRANSF,1,4000),substr(:new.NR_SEQ_PENDENCIA_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PENDENCIA_TRANSF',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOTIVO_REPROVACAO,1,4000),substr(:new.IE_MOTIVO_REPROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOTIVO_REPROVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA_REPROV,1,4000),substr(:new.DS_JUSTIFICATIVA_REPROV,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA_REPROV',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_CONTRATO,1,4000),substr(:new.NR_SEQ_REGRA_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_CONTRATO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBS_ITEM_FORN,1,4000),substr(:new.DS_OBS_ITEM_FORN,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBS_ITEM_FORN',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRECO_PJ,1,4000),substr(:new.NR_SEQ_PRECO_PJ,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRECO_PJ',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AGRUP,1,4000),substr(:new.NR_SEQ_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGRUP',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIF_DIVER,1,4000),substr(:new.DS_JUSTIF_DIVER,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIF_DIVER',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOTE,1,4000),substr(:new.DS_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOTE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIB_CONFERENCIA,1,4000),substr(:new.DT_LIB_CONFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIB_CONFERENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DOCUMENTO_EXTERNO,1,4000),substr(:new.NR_DOCUMENTO_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOCUMENTO_EXTERNO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SERVICO_REALIZADO,1,4000),substr(:new.IE_SERVICO_REALIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SERVICO_REALIZADO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_APROV,1,4000),substr(:new.NR_SEQ_PROC_APROV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_APROV',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATPACI,1,4000),substr(:new.NR_SEQ_MATPACI,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATPACI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ordem_compra_item_delete
before delete ON TASY.ORDEM_COMPRA_ITEM for each row
declare

pragma autonomous_transaction;

ie_frete_w				ordem_compra.ie_frete%type;
vl_frete_w				ordem_compra.vl_frete%type;
vl_frete_rateado_w		ordem_compra.vl_frete%type;
qt_itens_oc_w			ordem_compra_item.nr_item_oci%type;
nr_ordem_compra_w		ordem_compra_item_trib.nr_ordem_compra%type;
nr_item_oci_w			ordem_compra_item_trib.nr_item_oci%type;
cd_tributo_w			ordem_compra_item_trib.cd_tributo%type;
pr_tributo_w			ordem_compra_item_trib.pr_tributo%type;
qt_material_w			ordem_compra_item.qt_material%type;
vl_unitario_material_w	ordem_compra_item.vl_unitario_material%type;
vl_desconto_w			ordem_compra_item.vl_desconto%type;
ie_desc_nf_w			tributo.ie_desc_nf%type;
vl_tributo_w			ordem_compra_item_trib.vl_tributo%type;

cursor c02 is
select	ocit.nr_ordem_compra,
		ocit.nr_item_oci,
		ocit.cd_tributo,
		ocit.pr_tributo,
		decode(:old.nr_item_oci, ocit.nr_item_oci, :old.qt_material, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 1)),
		decode(:old.nr_item_oci, ocit.nr_item_oci, :old.vl_unitario_material, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 2)),
		decode(:old.nr_item_oci, ocit.nr_item_oci, :old.vl_desconto, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 3))
from	ordem_compra_item_trib ocit,
		tributo t
where	ocit.nr_ordem_compra = :old.nr_ordem_compra
and		ocit.cd_tributo = t.cd_tributo
and		t.ie_tipo_tributo = 'IPI'
and		nvl(decode(:old.nr_item_oci, ocit.nr_item_oci, :old.vl_frete_rateio, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 4)),0) = 0;

begin
if (compras_pck.get_is_oci_delete = 'S') then
	select	max(ie_frete),
			max(vl_frete)
	into	ie_frete_w,
			vl_frete_w
	from	ordem_compra
	where	nr_ordem_compra = :old.nr_ordem_compra;

	if (ie_frete_w = 'F') and (vl_frete_w > 0) then
		begin
		qt_itens_oc_w := obter_inf_itens_oc(:old.nr_ordem_compra, null, null) - 1;

		vl_frete_rateado_w := DIVIDIR_SEM_ROUND(vl_frete_w, qt_itens_oc_w);

		open c02;
		loop
		fetch c02 into
			nr_ordem_compra_w,
			nr_item_oci_w,
			cd_tributo_w,
			pr_tributo_w,
			qt_material_w,
			vl_unitario_material_w,
			vl_desconto_w;
		exit when c02%notfound;
			begin
			select	nvl(ie_desc_nf,'N')
			into	ie_desc_nf_w
			from	tributo
			where	cd_tributo = cd_tributo_w;

			if (ie_desc_nf_w = 'S') then
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) - vl_desconto_w + vl_frete_rateado_w);
			else
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) + vl_frete_rateado_w);
			end if;

			if (vl_tributo_w is not null) then
				update	ordem_compra_item_trib
				set		vl_tributo = vl_tributo_w
				where	nr_ordem_compra = nr_ordem_compra_w
				and		nr_item_oci = nr_item_oci_w
				and		cd_tributo = cd_tributo_w;
			end if;
			end;
		end loop;
		close c02;
		end;
	end if;

	commit;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Ordem_Compra_Item_Atual
BEFORE INSERT OR UPDATE ON TASY.ORDEM_COMPRA_ITEM FOR EACH ROW
DECLARE


cd_tributo_w			number(5);
pr_tributo_w			number(7,4);
qt_registro_w			number(5);
vl_tributo_w			number(15,2);
qt_reg_w				number(1);
nr_seq_reg_lic_compra_w		number(10);
nr_solic_compra_w			number(10);
nr_item_solic_compra_w		number(10);
nr_seq_licitacao_w			number(10);
nr_sequencia_w			number(10);
qt_mat_solicitacao_w		number(13,4);
qt_diferenca_w			number(13,4);
qt_alterada_w			number(13,4);
vl_ultima_compra_w		Number(15,4);
vl_dif_ultima_compra_w		Number(15,4);
pr_dif_ultima_compra_w		Number(13,4);
cd_estabelecimento_w		Number(10);
ie_sistema_origem_w		varchar2(15);
ie_tipo_tributo_w		tributo.ie_tipo_tributo%type;
ie_considera_rateio_trib_w	varchar2(15);
ie_frete_w				ordem_compra.ie_frete%type;
vl_frete_w				ordem_compra.vl_frete%type;
vl_frete_rateado_w		ordem_compra.vl_frete%type;
qt_itens_oc_w			ordem_compra_item.nr_item_oci%type;
nr_ordem_compra_w		ordem_compra_item_trib.nr_ordem_compra%type;
nr_item_oci_w			ordem_compra_item_trib.nr_item_oci%type;
qt_material_w			ordem_compra_item.qt_material%type;
vl_unitario_material_w	ordem_compra_item.vl_unitario_material%type;
vl_desconto_w			ordem_compra_item.vl_desconto%type;

ie_desc_nf_w			varchar2(15);

cursor C01 IS
select	a.cd_tributo,
	a.pr_tributo,
	b.ie_tipo_tributo
from	ordem_compra_item_trib a,
	tributo b
where	a.cd_tributo = b.cd_tributo
and	nr_ordem_compra	= :new.nr_ordem_compra
and	nr_item_oci	= :new.nr_item_oci;

cursor c02 is
select	ocit.nr_ordem_compra,
		ocit.nr_item_oci,
		ocit.cd_tributo,
		ocit.pr_tributo,
		decode(:new.nr_item_oci, ocit.nr_item_oci, :new.qt_material, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 1)),
		decode(:new.nr_item_oci, ocit.nr_item_oci, :new.vl_unitario_material, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 2)),
		decode(:new.nr_item_oci, ocit.nr_item_oci, :new.vl_desconto, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 3))
from	ordem_compra_item_trib ocit,
		tributo t
where	ocit.nr_ordem_compra = :new.nr_ordem_compra
and		ocit.cd_tributo = t.cd_tributo
and		t.ie_tipo_tributo = 'IPI'
and		nvl(decode(:new.nr_item_oci, ocit.nr_item_oci, :new.vl_frete_rateio, obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 4)),0) = 0;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

:new.pr_descontos		:= nvl(:new.pr_descontos,0);
:new.vl_unitario_material	:= nvl(:new.vl_unitario_material,0);
:new.qt_material		:= nvl(:new.qt_material,0);

if (nvl(:new.vl_desconto,0) > 0) then
	:new.vl_item_liquido		:= 	round(:new.vl_total_item - nvl(:new.vl_desconto,0),2);
else
	:new.vl_item_liquido		:= 	round(:new.vl_total_item - ((:new.vl_total_item * :new.pr_descontos) / 100),2);
end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = :new.nr_ordem_compra;

if	(inserting) then

	vl_ultima_compra_w		:= nvl(obter_dados_ult_compra_data(cd_estabelecimento_w,:new.cd_material, null, trunc(sysdate,'dd'), 0, 'VU'),0);
	vl_dif_ultima_compra_w		:= vl_ultima_compra_w - nvl(:new.vl_unitario_material,0);
	begin
		pr_dif_ultima_compra_w	:= round((dividir(vl_dif_ultima_compra_w, vl_ultima_compra_w) * 100),2);
	exception
	when others then
		pr_dif_ultima_compra_w	:= 99999999;
	end;

	:new.vl_ultima_compra		:= vl_ultima_compra_w;
	:new.vl_dif_ultima_compra	:= vl_dif_ultima_compra_w;
	:new.pr_dif_ultima_compra	:= pr_dif_ultima_compra_w;
end if;


select	obter_valor_param_usuario(915, 190, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w)
into	ie_considera_rateio_trib_w
from	dual;

select	count(*)
into	qt_registro_w
from	ordem_compra_item_trib
where	nr_ordem_compra	= :new.nr_ordem_compra
and	nr_item_oci	= :new.nr_item_oci;

if	(qt_registro_w > 0) then
	begin
	open c01;
	loop
	fetch c01 into
		cd_tributo_w,
		pr_tributo_w,
		ie_tipo_tributo_w;
	exit when c01%notfound;
		begin

		select	nvl(ie_desc_nf,'N')
		into	ie_desc_nf_w
		from	tributo
		where	cd_tributo = cd_tributo_w;

		/* - Alterado por Lssilva em 19/09/2017 - OS1494616 */
		if (ie_desc_nf_w = 'S') then
			vl_tributo_w	:= (pr_tributo_w / 100) * ((:new.qt_material * :new.vl_unitario_material) - nvl(:new.vl_desconto,0));
		else
			vl_tributo_w	:= (pr_tributo_w / 100) * ((:new.qt_material * :new.vl_unitario_material));
			/* - (:new.qt_material * :new.vl_unitario_material) * (:new.pr_descontos / 100)); Alterado por Anderson em 20/07/2009 - OS154749 */
		end if;



		if	(:new.vl_frete_rateio > 0) and
			(ie_considera_rateio_trib_w = 'S') and
			(ie_tipo_tributo_w in ('IPI','ICMS','ICMSST')) then

			vl_tributo_w	:= (pr_tributo_w / 100) * ((:new.qt_material * :new.vl_unitario_material)+:new.vl_frete_rateio);
		end if;

		update	ordem_compra_item_trib
		set	vl_tributo			= vl_tributo_w
		where	nr_ordem_compra		= :new.nr_ordem_compra
		and	nr_item_oci		= :new.nr_item_oci
		and	cd_tributo			= cd_tributo_w;
		end;
	end loop;
	close c01;
	end;
end if;

if	(updating) then

	if (nvl(:old.nr_seq_marca,0) <> nvl(:new.nr_seq_marca,0)) then
		:new.ds_marca := SUBSTR(obter_desc_material_marca(:new.nr_seq_marca, :new.cd_material),1,100);
	end if;

	if	(:new.ie_sistema_origem = 'SGH') and
		(:new.cd_centro_custo <> :old.cd_centro_custo) and
		(:old.dt_atualizacao between sysdate - 0.0050 and sysdate) then
		:new.cd_centro_custo := :old.cd_centro_custo;
	end if;


end if;


if	(:old.qt_material <> :new.qt_material) then

	select	nvl(max(nr_seq_reg_lic_compra),0),
		nvl(max(nr_seq_licitacao),0)
	into	nr_seq_reg_lic_compra_w,
		nr_seq_licitacao_w
	from	ordem_compra
	where	nr_ordem_compra = :new.nr_ordem_compra;

	if	(nr_seq_licitacao_w > 0) and
		(nr_seq_reg_lic_compra_w > 0) and
		(nvl(:new.nr_seq_reg_lic_item,0) > 0) then

		select	nvl(max(nr_solic_compra),0),
			nvl(max(nr_item_solic_compra),0)
		into	nr_solic_compra_w,
			nr_item_solic_compra_w
		from	reg_lic_compra_item
		where	nr_seq_reg_lic_compra = nr_seq_reg_lic_compra_w
		and	nr_seq_lic_item = :new.nr_seq_reg_lic_item
		and	nr_seq_licitacao = nr_seq_licitacao_w;

		/*UPDATE DA SOLICITACAO DE COMPRAS*/
		if	(nr_solic_compra_w > 0) and
			(nr_item_solic_compra_w > 0) then

			select	nvl(max(nr_sequencia),0)
			into	nr_sequencia_w
			from	reg_lic_item_solic
			where	nr_solic_compra = nr_solic_compra_w
			and	nr_item_solic_compra = nr_item_solic_compra_w
			and	nr_seq_licitacao = nr_seq_licitacao_w;

			if	(nr_sequencia_w > 0) then
				update	reg_lic_item_solic
				set	qt_item = :new.qt_material
				where	nr_sequencia = nr_sequencia_w;
			end if;

			select	nvl(qt_material,0)
			into	qt_mat_solicitacao_w
			from	solic_compra_item
			where	nr_solic_compra = nr_solic_compra_w
			and	nr_item_solic_compra = nr_item_solic_compra_w;

			qt_diferenca_w 	:= :old.qt_material - :new.qt_material;
			qt_alterada_w	:= qt_mat_solicitacao_w - qt_diferenca_w;

			update	solic_compra_item
			set	qt_material = qt_alterada_w
			where	nr_solic_compra = nr_solic_compra_w
			and	nr_item_solic_compra = nr_item_solic_compra_w;

			update	solic_compra_item_entrega
			set	qt_entrega_solicitada = qt_alterada_w
			where	nr_solic_compra = nr_solic_compra_w
			and	nr_item_solic_compra = nr_item_solic_compra_w;

			/*Foi alterado a quantidade do material #@CD_MATERIAL#@, pois foi alterado a quantidade do item na ordem de compra (#@NR_ORDEM_COMPRA#@) gerada pelo registro de precos (#@NR_SEQ_REG_LIC_COMPRA#@).
			Qt. Original: #@QT_MATERIAL_SOLIC#@ Qt. Atual: #@QT_MATERI*/

			gerar_hist_solic_sem_commit(
				nr_solic_compra_w,
				/*'Alteracao na quantidade do material #@CD_MATERIAL#@*/
				wheb_mensagem_pck.get_texto(306365, 'CD_MATERIAL=' || :new.cd_material),
				substr(wheb_mensagem_pck.get_texto(306367,
					'CD_MATERIAL=' || :new.cd_material ||
					';NR_ORDEM_COMPRA=' || :new.nr_ordem_compra ||
					';NR_SEQ_REG_LIC_COMPRA=' || nr_seq_reg_lic_compra_w ||
					';QT_MATERIAL_SOLIC=' || campo_mascara_virgula(qt_mat_solicitacao_w) ||
					';QT_MATERIAL_ALTERADO=' || campo_mascara_virgula(qt_alterada_w)),1,255),
				'Q',
				:new.nm_usuario);
		end if;
		/* FIM DO UPDATE DA SOLICITACAO DE COMPRAS*/

		/*UPDATE NO REGISTRO DE COMPRA*/
		update	reg_lic_compra_item
		set	qt_material = :new.qt_material
		where	nr_seq_reg_lic_compra = nr_seq_reg_lic_compra_w
		and	nr_seq_lic_item = :new.nr_seq_reg_lic_item
		and	nr_seq_licitacao = nr_seq_licitacao_w;

		/*FIM DO UPDATE DO REGISTRO DE COMPRA*/
	end if;
end if;

select	max(ie_frete),
		max(vl_frete)
into	ie_frete_w,
		vl_frete_w
from	ordem_compra
where	nr_ordem_compra = :new.nr_ordem_compra;

if (ie_frete_w = 'F') and (vl_frete_w > 0) then
	begin
	qt_itens_oc_w := obter_inf_itens_oc(:new.nr_ordem_compra, null, null);

	if	(inserting) then
		qt_itens_oc_w := qt_itens_oc_w + 1;
	end if;

	begin
		vl_frete_rateado_w := vl_frete_w / qt_itens_oc_w;
	exception
	when others then
		vl_frete_rateado_w := vl_frete_w / 1;
	end;

	open c02;
	loop
	fetch c02 into
		nr_ordem_compra_w,
		nr_item_oci_w,
		cd_tributo_w,
		pr_tributo_w,
		qt_material_w,
		vl_unitario_material_w,
		vl_desconto_w;
	exit when c02%notfound;
		begin
		select	nvl(ie_desc_nf,'N')
		into	ie_desc_nf_w
		from	tributo
		where	cd_tributo = cd_tributo_w;

		if (ie_desc_nf_w = 'S') then
			vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) - vl_desconto_w + vl_frete_rateado_w);
		else
			vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) + vl_frete_rateado_w);
		end if;

		if (vl_tributo_w is not null) then
			update	ordem_compra_item_trib
			set		vl_tributo = vl_tributo_w
			where	nr_ordem_compra = nr_ordem_compra_w
			and		nr_item_oci = nr_item_oci_w
			and		cd_tributo = cd_tributo_w;
		end if;
		end;
	end loop;
	close c02;
	end;
end if;

--OS 1296310
--:new.vl_total_item := :new.vl_unitario_material * :new.qt_material;

<<Final>>

qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_BEF_INS_UPD
BEFORE INSERT OR UPDATE ON TASY.ORDEM_COMPRA_ITEM FOR EACH ROW
DECLARE

qt_items_w		number(10);
qt_items_voucher_w		number(10);

pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S' AND nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_AR')  then
  
  SELECT count(*)
  INTO qt_items_w
  FROM ORDEM_COMPRA_ITEM
  WHERE NR_ORDEM_COMPRA = :new.NR_ORDEM_COMPRA;

  SELECT count(*)
  INTO qt_items_voucher_w
  FROM ORDEM_COMPRA_ITEM
  WHERE NR_ORDEM_COMPRA = :new.NR_ORDEM_COMPRA
  AND CD_FEDERAL_VOUCHER is not null;

  if (inserting and qt_items_voucher_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1166198);
  end if;
  if ((inserting and (:new.CD_FEDERAL_VOUCHER is not null AND qt_items_w > 0)) or (updating and :new.CD_FEDERAL_VOUCHER is not null AND qt_items_w > 1)) then
    wheb_mensagem_pck.exibir_mensagem_abort(1166199);
  end if;

end if;

end;
/


ALTER TABLE TASY.ORDEM_COMPRA_ITEM ADD (
  CONSTRAINT ORCOITE_PK
 PRIMARY KEY
 (NR_ORDEM_COMPRA, NR_ITEM_OCI)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ITEM ADD (
  CONSTRAINT ORCOITE_PCSREAN_FK 
 FOREIGN KEY (NR_SEQ_REG_PCS) 
 REFERENCES TASY.PCS_REG_ANALISE (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_CANORPR_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_PROD) 
 REFERENCES TASY.CAN_ORDEM_PROD (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_GPIPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_GPI) 
 REFERENCES TASY.GPI_PLANO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ORCOITE_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT ORCOITE_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT ORCOITE_SUPETES_FK 
 FOREIGN KEY (NR_SEQ_PENDENCIA_TRANSF) 
 REFERENCES TASY.SUP_PENDENCIA_TRANSF_ESTAB (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTRATO) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_COCOSAG_FK 
 FOREIGN KEY (NR_SEQ_AGRUP) 
 REFERENCES TASY.COT_COMPRA_SOLIC_AGRUP (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_UNMACOM_FK 
 FOREIGN KEY (NR_SEQ_UNIDADE_ADIC) 
 REFERENCES TASY.UNIDADE_MEDIDA_ADIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_CRITRAT_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO_RATEIO) 
 REFERENCES TASY.CTB_CRITERIO_RATEIO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT ORCOITE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ORCOITE_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT ORCOITE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORCOITE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT ORDCOIT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT ORDCOIT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT ORDCOIT_PROCOMP_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO) 
 REFERENCES TASY.PROCESSO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOIT_SOLCOIT_FK_I 
 FOREIGN KEY (NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA_ITEM (NR_SOLIC_COMPRA,NR_ITEM_SOLIC_COMPRA),
  CONSTRAINT ORCOITE_COCOITE_FK 
 FOREIGN KEY (NR_COT_COMPRA, NR_ITEM_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA_ITEM (NR_COT_COMPRA,NR_ITEM_COT_COMPRA),
  CONSTRAINT ORCOITE_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_LICITEM_FK 
 FOREIGN KEY (NR_SEQ_LIC_ITEM) 
 REFERENCES TASY.LIC_ITEM (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_CONFINA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT ORCOITE_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT ORCOITE_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ORDEM_COMPRA_ITEM TO NIVEL_1;

