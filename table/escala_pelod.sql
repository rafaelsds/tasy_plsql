ALTER TABLE TASY.ESCALA_PELOD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PELOD CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PELOD
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  DT_LIBERACAO              DATE,
  QT_REL_PAO2_FIO2          NUMBER(3),
  QT_CREATININA_SERICA      NUMBER(15,2),
  QT_PA_SISTOLICA           NUMBER(3),
  QT_PTO_HEPATICA           NUMBER(3)           NOT NULL,
  QT_PTO_CARDIO             NUMBER(3)           NOT NULL,
  QT_PTO_SNC                NUMBER(3)           NOT NULL,
  QT_PTO_RENAL              NUMBER(3)           NOT NULL,
  QT_PONTUACAO              NUMBER(3)           NOT NULL,
  PR_MORTALIDADE            NUMBER(7,1)         NOT NULL,
  QT_PTO_REL_PAO2_FIO2      NUMBER(3)           NOT NULL,
  QT_FREQ_CARDIACA          NUMBER(3),
  QT_PTO_FREQ_CARDIACA      NUMBER(3)           NOT NULL,
  QT_PACO2                  NUMBER(15,2),
  QT_PTO_PACO2              NUMBER(3)           NOT NULL,
  IE_VENTILACAO_MECANICA    VARCHAR2(1 BYTE),
  QT_PTO_VENT_MECANICA      NUMBER(3)           NOT NULL,
  QT_GLASGOW                NUMBER(2),
  QT_PTO_GLASGOW            NUMBER(3)           NOT NULL,
  IE_PUPILA_FIXA            VARCHAR2(1 BYTE),
  QT_PTO_PUPILA_FIXA        NUMBER(3)           NOT NULL,
  QT_GLOBULOS_BRANCOS       NUMBER(15,1),
  QT_PTO_GLOB_BRANCO        NUMBER(3)           NOT NULL,
  QT_PTO_CREATININA_SERICA  NUMBER(3)           NOT NULL,
  QT_PTO_PA_SISTOLICA       NUMBER(3)           NOT NULL,
  QT_TGO                    NUMBER(15),
  QT_PTO_TGO                NUMBER(3)           NOT NULL,
  QT_PLAQUETAS              NUMBER(15),
  QT_PTO_PLAQUETAS          NUMBER(3)           NOT NULL,
  QT_TAP_INR                NUMBER(15,2),
  QT_PTO_TAP_INR            NUMBER(3)           NOT NULL,
  QT_PTO_HEMATOLOGICA       NUMBER(3)           NOT NULL,
  QT_PTO_PULMONAR           NUMBER(3)           NOT NULL,
  CD_PERFIL_ATIVO           NUMBER(5),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  NR_HORA                   NUMBER(2),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPELO_ATEPACI_FK_I ON TASY.ESCALA_PELOD
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPELO_I1 ON TASY.ESCALA_PELOD
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPELO_PERFIL_FK_I ON TASY.ESCALA_PELOD
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPELO_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCPELO_PESFISI_FK_I ON TASY.ESCALA_PELOD
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPELO_PK ON TASY.ESCALA_PELOD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPELO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_pelod_atual
before insert or update ON TASY.ESCALA_PELOD for each row
declare

cd_pessoa_fisica_w	varchar2(10);
nr_idade_dias_w		number(10,0);
nr_idade_meses_w	number(10,0);
logit_w			number(15,4):= 0;
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Obter pessoa f�sica */
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

/* Obter a idade em meses */
select	nvl(obter_idade_pf(cd_pessoa_fisica_w, :new.dt_avaliacao, 'M'),0)
into	nr_idade_meses_w
from	dual;

/* Pontua��o  da frequ�ncia card�aca */
/* 12 anos = 144 meses */
if	(:new.qt_freq_cardiaca is null) then
	:new.qt_pto_freq_cardiaca := 0;
elsif	(nr_idade_meses_w < 144) then
	if	(:new.qt_freq_cardiaca <= 195)	then
		:new.qt_pto_freq_cardiaca := 0;
	elsif	(:new.qt_freq_cardiaca > 195)	then
		:new.qt_pto_freq_cardiaca := 10;
	end if;

elsif	(nr_idade_meses_w >= 144) then
	if	(:new.qt_freq_cardiaca <= 150)	then
		:new.qt_pto_freq_cardiaca := 0;
	elsif	(:new.qt_freq_cardiaca > 150)	then
		:new.qt_pto_freq_cardiaca := 10;
	end if;

end if;

/* Pontua��o da press�o arterial sist�lica */
if	(:new.qt_pa_sistolica is null) then
	:new.qt_pto_pa_sistolica := 0;
elsif	(nr_idade_meses_w < 1) then
	if	(:new.qt_pa_sistolica > 65) then
		:new.qt_pto_pa_sistolica := 0;
	elsif	(:new.qt_pa_sistolica >= 35) and
		(:new.qt_pa_sistolica <= 65) then
		:new.qt_pto_pa_sistolica := 10;
	elsif	(:new.qt_pa_sistolica < 35) then
		:new.qt_pto_pa_sistolica := 20;
	end if;

elsif	(nr_idade_meses_w >= 1) and
	(nr_idade_meses_w < 12) then
	if	(:new.qt_pa_sistolica > 75) then
		:new.qt_pto_pa_sistolica := 0;
	elsif	(:new.qt_pa_sistolica >= 35) and
		(:new.qt_pa_sistolica <= 75) then
		:new.qt_pto_pa_sistolica := 10;
	elsif	(:new.qt_pa_sistolica < 35) then
		:new.qt_pto_pa_sistolica := 20;
	end if;

elsif	(nr_idade_meses_w >= 12) and
	(nr_idade_meses_w < 144) then
	if	(:new.qt_pa_sistolica > 85) then
		:new.qt_pto_pa_sistolica := 0;
	elsif	(:new.qt_pa_sistolica >= 45) and
		(:new.qt_pa_sistolica <= 85) then
		:new.qt_pto_pa_sistolica := 10;
	elsif	(:new.qt_pa_sistolica < 45) then
		:new.qt_pto_pa_sistolica := 20;
	end if;

elsif	(nr_idade_meses_w >= 144) then
	if	(:new.qt_pa_sistolica > 95) then
		:new.qt_pto_pa_sistolica := 0;
	elsif	(:new.qt_pa_sistolica >= 55) and
		(:new.qt_pa_sistolica <= 95) then
		:new.qt_pto_pa_sistolica := 10;
	elsif	(:new.qt_pa_sistolica < 55) then
		:new.qt_pto_pa_sistolica := 20;
	end if;

end if;

/* Pontua��o da rela��o PaO2/FiO2 */
if	(:new.qt_rel_pao2_fio2 is null) then
	:new.qt_pto_rel_pao2_fio2 := 0;
elsif	(:new.qt_rel_pao2_fio2 > 70) then
	:new.qt_pto_rel_pao2_fio2 := 0;
elsif	(:new.qt_rel_pao2_fio2 <= 70) then
	:new.qt_pto_rel_pao2_fio2 := 10;
end if;

/* Pontua��o do PaCO2*/
if	(:new.qt_paco2 is null) then
	:new.qt_pto_paco2 := 0;
elsif	(:new.qt_paco2 <= 90) then
	:new.qt_pto_paco2 := 0;
elsif	(:new.qt_paco2 > 90) then
	:new.qt_pto_paco2 := 10;
end if;

/* Pontua��o da ventila��o mec�nica */
if	(:new.ie_ventilacao_mecanica = 'S') then
	:new.qt_pto_vent_mecanica := 1;
elsif	(:new.ie_ventilacao_mecanica = 'N') then
	:new.qt_pto_vent_mecanica := 0;
end if;

/* Pontua��o creatinina s�rica */
if	(:new.qt_creatinina_serica is null) then
	:new.qt_pto_creatinina_serica := 0;
else
	/* Obter idade em dias */

	select	obter_dias_entre_datas(to_date(obter_dados_pf(cd_pessoa_fisica_w, 'DN'),'dd/mm/yyyy'), :new.dt_avaliacao)
	into	nr_idade_dias_w
	from	dual;

	if	(nr_idade_dias_w < 7) then
		if	(:new.qt_creatinina_serica < 1.59) then
			:new.qt_pto_creatinina_serica := 0;
		elsif	(:new.qt_creatinina_serica >= 1.59) then
			:new.qt_pto_creatinina_serica := 10;
		end if;

	elsif	(nr_idade_dias_w >= 7) and
		(nr_idade_meses_w < 12) then
		if	(:new.qt_creatinina_serica < 0.62) then
			:new.qt_pto_creatinina_serica := 0;
		elsif	(:new.qt_creatinina_serica >= 0.62) then
			:new.qt_pto_creatinina_serica := 10;
		end if;

	elsif	(nr_idade_meses_w >= 12) and
		(nr_idade_meses_w < 144) then
		if	(:new.qt_creatinina_serica < 1.13) then
			:new.qt_pto_creatinina_serica := 0;
		elsif	(:new.qt_creatinina_serica >= 1.13) then
			:new.qt_pto_creatinina_serica := 10;
		end if;

	elsif	(nr_idade_meses_w >= 144) then
		if	(:new.qt_creatinina_serica < 1.59) then
			:new.qt_pto_creatinina_serica := 0;
		elsif	(:new.qt_creatinina_serica >= 1.59) then
			:new.qt_pto_creatinina_serica := 10;
		end if;

	end if;
end if;

/* Pontua��o da escala de glasgow */
if	(:new.qt_glasgow is null) then
	:new.qt_pto_glasgow := 0;
elsif	(:new.qt_glasgow >= 12) and
	(:new.qt_glasgow <= 15) then
	:new.qt_pto_glasgow := 0;
elsif	(:new.qt_glasgow >= 7) and
	(:new.qt_glasgow <= 11) then
	:new.qt_pto_glasgow := 1;
elsif	(:new.qt_glasgow >= 4) and
	(:new.qt_glasgow <= 6) then
	:new.qt_pto_glasgow := 10;
elsif	(:new.qt_glasgow = 3) then
	:new.qt_pto_glasgow := 20;
else
	:new.qt_pto_glasgow := 0;
end if;

/* Pontua��o das rea��es pupilares */
if	(:new.ie_pupila_fixa = 'S') then
	:new.qt_pto_pupila_fixa := 10;
elsif	(:new.ie_pupila_fixa = 'N') then
	:new.qt_pto_pupila_fixa := 0;
end if;

/* Pontua��o de gl�bulos brancos */
if	(:new.qt_globulos_brancos is null) then
	:new.qt_pto_glob_branco := 0;
elsif	(:new.qt_globulos_brancos >= 4500) then
	:new.qt_pto_glob_branco := 0;
elsif	(:new.qt_globulos_brancos >= 1500) and
	(:new.qt_globulos_brancos <= 4400) then
	:new.qt_pto_glob_branco := 1;
elsif	(:new.qt_globulos_brancos < 1500) then
	:new.qt_pto_glob_branco := 10;
end if;

/* Pontua��o das plaquetas */
if	(:new.qt_plaquetas is null) then
	:new.qt_pto_plaquetas := 0;
elsif	(:new.qt_plaquetas >= 35000) then
	:new.qt_pto_plaquetas := 0;
elsif	(:new.qt_plaquetas < 35000) then
	:new.qt_pto_plaquetas := 1;
end if;

/* Pontua��o da TGO */
if	(:new.qt_tgo is null) then
	:new.qt_pto_tgo := 0;
elsif	(:new.qt_tgo < 950) then
	:new.qt_pto_tgo := 0;
elsif	(:new.qt_tgo >= 950) then
	:new.qt_pto_tgo := 1;
end if;

/* Pontua��o de tempo de protrombina */
if	(:new.qt_tap_inr is null) then
	:new.qt_pto_tap_inr := 0;
elsif	(:new.qt_tap_inr < 1.4) then
	:new.qt_pto_tap_inr := 0;
elsif	(:new.qt_tap_inr >= 1.4) then
	:new.qt_pto_tap_inr := 1;
end if;


/* Soma das pontua��es por �rea */
/* Cardiovascular */
if	((:new.qt_pto_freq_cardiaca = 10 and :new.qt_pto_pa_sistolica = 0) or
	(:new.qt_pto_freq_cardiaca = 0 and :new.qt_pto_pa_sistolica = 10) or
	(:new.qt_pto_freq_cardiaca = 10 and :new.qt_pto_pa_sistolica = 10)) then
	:new.qt_pto_cardio := 10;
elsif	(:new.qt_pto_pa_sistolica = 20) then
	:new.qt_pto_cardio := 20;
else
	:new.qt_pto_cardio := 0;
end if;

/* Pulmonar */
if	(:new.qt_pto_rel_pao2_fio2 = 10 and :new.qt_pto_paco2 = 0) or
	(:new.qt_pto_rel_pao2_fio2 = 0 and :new.qt_pto_paco2 = 10) or
	(:new.qt_pto_rel_pao2_fio2 = 10 and :new.qt_pto_paco2 = 10) then
	:new.qt_pto_pulmonar := 10;
elsif	(:new.qt_pto_vent_mecanica = 1) and
	(:new.qt_pto_rel_pao2_fio2 = 0) and
	(:new.qt_pto_paco2 = 0) then
	:new.qt_pto_pulmonar := 1;
else
	:new.qt_pto_pulmonar := 0;
end if;

/* Hep�tica */
if	((:new.qt_pto_tgo = 0 and :new.qt_pto_tap_inr = 1) or
	(:new.qt_pto_tgo = 1 and :new.qt_pto_tap_inr = 0) or
	(:new.qt_pto_tgo = 1 and :new.qt_pto_tap_inr = 1)) then
	:new.qt_pto_hepatica := 1;
else
	:new.qt_pto_hepatica := 0;
end if;

/* Sistema nervoso central */
if	(:new.qt_pto_glasgow = 20) then
	:new.qt_pto_snc	:= 20;
elsif	(:new.qt_pto_glasgow = 10 and :new.qt_pto_pupila_fixa = 0) or
	(:new.qt_pto_glasgow = 1 and :new.qt_pto_pupila_fixa = 10) or
	(:new.qt_pto_glasgow = 0 and :new.qt_pto_pupila_fixa = 10) then
	:new.qt_pto_snc	:= 10;
elsif	(:new.qt_pto_glasgow = 1) and
	(:new.qt_pto_pupila_fixa = 0) then
	:new.qt_pto_snc	:= 1;
else
	:new.qt_pto_snc	:= 0;
end if;

/* Renal */
:new.qt_pto_renal := :new.qt_pto_creatinina_serica;

/* Hematol�gica */
if	(:new.qt_pto_glob_branco = 10) then
	:new.qt_pto_hematologica := 10;
elsif	(:new.qt_pto_glob_branco = 1 and :new.qt_pto_plaquetas = 0) or
	(:new.qt_pto_glob_branco = 0 and :new.qt_pto_plaquetas = 1) or
	(:new.qt_pto_glob_branco = 1 and :new.qt_pto_plaquetas = 1) then
	:new.qt_pto_hematologica := 1;
else
	:new.qt_pto_hematologica := 0;
end if;

/* Pontua��o total */
:new.qt_pontuacao :=	nvl(:new.qt_pto_cardio,0)	+	nvl(:new.qt_pto_pulmonar,0)	+
			nvl(:new.qt_pto_hepatica,0)	+	nvl(:new.qt_pto_snc,0)		+
			nvl(:new.qt_pto_renal,0)	+	nvl(:new.qt_pto_hematologica,0);

/* Percentual de mortalidade -7.7631 */
logit_w := -7.64 + 0.30 * (:new.qt_pontuacao);

:new.pr_mortalidade	:= trunc(dividir(exp(logit_w), 1 + exp(logit_w)) * 100,1);

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_PELOD_tp  after update ON TASY.ESCALA_PELOD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_FREQ_CARDIACA,1,4000),substr(:new.QT_FREQ_CARDIACA,1,4000),:new.nm_usuario,nr_seq_w,'QT_FREQ_CARDIACA',ie_log_w,ds_w,'ESCALA_PELOD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ESCALA_PELOD ADD (
  CONSTRAINT ESCPELO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PELOD ADD (
  CONSTRAINT ESCPELO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPELO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCPELO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_PELOD TO NIVEL_1;

