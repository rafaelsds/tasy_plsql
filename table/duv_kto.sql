ALTER TABLE TASY.DUV_KTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DUV_KTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DUV_KTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MENSAGEM      NUMBER(10)               NOT NULL,
  VL_TOTAL_CONTA       NUMBER(15,2),
  NR_CONTA             VARCHAR2(15 BYTE),
  CD_BENEFICIARIO      VARCHAR2(9 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DUVKTO_DUVMENS_FK_I ON TASY.DUV_KTO
(NR_SEQ_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DUVKTO_PK ON TASY.DUV_KTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DUV_KTO ADD (
  CONSTRAINT DUVKTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DUV_KTO ADD (
  CONSTRAINT DUVKTO_DUVMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSAGEM) 
 REFERENCES TASY.DUV_MENSAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DUV_KTO TO NIVEL_1;

