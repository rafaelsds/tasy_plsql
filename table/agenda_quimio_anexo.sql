ALTER TABLE TASY.AGENDA_QUIMIO_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_QUIMIO_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_QUIMIO_ANEXO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_ARQUIVO            VARCHAR2(255 BYTE)      NOT NULL,
  NR_SEQ_AGENDA_QUIMIO  NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEQANX_AGEQUIM_FK_I ON TASY.AGENDA_QUIMIO_ANEXO
(NR_SEQ_AGENDA_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQANX_AGEQUIM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGEQANX_PK ON TASY.AGENDA_QUIMIO_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQANX_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGENDA_QUIMIO_ANEXO_AFTINS
after insert ON TASY.AGENDA_QUIMIO_ANEXO for each row
declare

ds_texto_w				varchar2(255);
nr_sequencia_autor_w	autorizacao_convenio.nr_sequencia%type;
nr_seq_paciente_w		paciente_atendimento.nr_seq_paciente%type;
ie_gerar_anexo_w		parametro_agenda_quimio.ie_gerar_anexo_aut_conv%type;

Cursor C01 is
	select	a.nr_sequencia
	from	autorizacao_convenio a
	where	a.nr_seq_paciente_setor = nr_seq_paciente_w
	and	not exists(	select 1
				from	autorizacao_convenio_arq x
				where	x.nr_sequencia_autor = a.nr_sequencia
				and		x.ds_arquivo	     = :new.ds_arquivo);

begin

select	nvl(max(ie_gerar_anexo_aut_conv),'N')
into	ie_gerar_anexo_w
from	parametro_agenda_quimio
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (ie_gerar_anexo_w = 'S') then
	begin

	ds_texto_w := substr(wheb_mensagem_pck.get_texto(333914),1,255); --Registro gerado pela Agenda de Quimioterapia.

	select	max(b.nr_seq_paciente)
	into	nr_seq_paciente_w
	from	agenda_quimio a,
			paciente_atendimento b
	where 	a.nr_seq_atendimento = b.nr_seq_atendimento
	and		a.nr_sequencia = :new.nr_seq_agenda_quimio;

	if (nr_seq_paciente_w is not null) then
		begin

		open C01;
		loop
		fetch C01 into
			nr_sequencia_autor_w;
		exit when C01%notfound;
			begin

			insert	into autorizacao_convenio_arq (
				ds_arquivo,
				ds_observacao,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_anexar_email,
				nm_usuario,
				nm_usuario_nrec,
				nr_sequencia,
				nr_sequencia_autor)
			values (
				substr(:new.ds_arquivo,1,254),
				ds_texto_w,
				sysdate,
				sysdate,
				'N',
				wheb_usuario_pck.get_nm_usuario,
				wheb_usuario_pck.get_nm_usuario,
				autorizacao_convenio_arq_seq.nextval,
				nr_sequencia_autor_w);

			end;
		end loop;
		close C01;

		end;
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.AGENDA_QUIMIO_ANEXO ADD (
  CONSTRAINT AGEQANX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_QUIMIO_ANEXO ADD (
  CONSTRAINT AGEQANX_AGEQUIM_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_QUIMIO) 
 REFERENCES TASY.AGENDA_QUIMIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_QUIMIO_ANEXO TO NIVEL_1;

