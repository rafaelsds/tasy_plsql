ALTER TABLE TASY.PLS_SOLIC_RESC_CONTATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLIC_RESC_CONTATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLIC_RESC_CONTATO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SOLICITACAO   NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_CONTATO  NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  IE_FORMA_ENVIO       VARCHAR2(1 BYTE),
  CD_CEP               VARCHAR2(15 BYTE),
  DS_ENDERECO          VARCHAR2(100 BYTE),
  NR_ENDERECO          NUMBER(5),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  DS_BAIRRO            VARCHAR2(80 BYTE),
  SG_ESTADO            VARCHAR2(15 BYTE),
  CD_BANCO             NUMBER(5),
  CD_AGENCIA_BANCARIA  VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA    VARCHAR2(1 BYTE),
  CD_CONTA             VARCHAR2(20 BYTE),
  IE_DIGITO_CONTA      VARCHAR2(1 BYTE),
  NR_DDD_TELEFONE      VARCHAR2(3 BYTE),
  NR_DDD_CELULAR       VARCHAR2(3 BYTE),
  NR_DDI_CELULAR       VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE      VARCHAR2(3 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE),
  NR_TELEFONE_CELULAR  VARCHAR2(40 BYTE),
  NM_PESSOA_DEVOLUCAO  VARCHAR2(255 BYTE),
  NR_CPF_DEVOLUCAO     VARCHAR2(11 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSRCC_BANCO_FK_I ON TASY.PLS_SOLIC_RESC_CONTATO
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRCC_PESFISI_FK_I ON TASY.PLS_SOLIC_RESC_CONTATO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSRCC_PK ON TASY.PLS_SOLIC_RESC_CONTATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRCC_PLSSRES_FK_I ON TASY.PLS_SOLIC_RESC_CONTATO
(NR_SEQ_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRCC_PLSTCSR_FK_I ON TASY.PLS_SOLIC_RESC_CONTATO
(NR_SEQ_TIPO_CONTATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRCC_SUSMUNI_FK_I ON TASY.PLS_SOLIC_RESC_CONTATO
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_SOLIC_RESC_CONTATO_tp  after update ON TASY.PLS_SOLIC_RESC_CONTATO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_CELULAR,1,4000),substr(:new.NR_DDI_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_CELULAR',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_CELULAR,1,4000),substr(:new.NR_DDD_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_CELULAR',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'PLS_SOLIC_RESC_CONTATO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_SOLIC_RESC_CONTATO ADD (
  CONSTRAINT PLSSRCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SOLIC_RESC_CONTATO ADD (
  CONSTRAINT PLSSRCC_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT PLSSRCC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSSRCC_PLSSRES_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO) 
 REFERENCES TASY.PLS_SOLICITACAO_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSSRCC_PLSTCSR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONTATO) 
 REFERENCES TASY.PLS_TP_CONTATO_SOLIC_RESC (NR_SEQUENCIA),
  CONSTRAINT PLSSRCC_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE));

GRANT SELECT ON TASY.PLS_SOLIC_RESC_CONTATO TO NIVEL_1;

