ALTER TABLE TASY.TASY_PADRAO_COR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_PADRAO_COR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_PADRAO_COR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_PADRAO            VARCHAR2(60 BYTE)        NOT NULL,
  DS_COR_FUNDO         VARCHAR2(15 BYTE),
  DS_COR_FONTE         VARCHAR2(15 BYTE),
  DS_COR_SELECAO       VARCHAR2(15 BYTE),
  NR_SEQ_APRES         NUMBER(5)                NOT NULL,
  DS_ITEM              VARCHAR2(40 BYTE),
  NR_SEQ_LEGENDA       NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_HINT              VARCHAR2(255 BYTE),
  CD_EXP_ITEM          NUMBER(10),
  IE_JAVA              VARCHAR2(1 BYTE),
  DS_COR_HTML          VARCHAR2(15 BYTE),
  NR_SEQ_COR_HTML      NUMBER(10),
  NR_SEQ_CONCEITO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASPACO_DICEXPR_FK_I ON TASY.TASY_PADRAO_COR
(CD_EXP_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASPACO_PK ON TASY.TASY_PADRAO_COR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASPACO_TAPACOH_FK_I ON TASY.TASY_PADRAO_COR
(NR_SEQ_COR_HTML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASPACO_TASLEGE_FK_I ON TASY.TASY_PADRAO_COR
(NR_SEQ_LEGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_PADRAO_COR_tp  after update ON TASY.TASY_PADRAO_COR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EXP_ITEM,1,4000),substr(:new.CD_EXP_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_ITEM',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JAVA,1,4000),substr(:new.IE_JAVA,1,4000),:new.nm_usuario,nr_seq_w,'IE_JAVA',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PADRAO,1,4000),substr(:new.DS_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PADRAO',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR_FUNDO,1,4000),substr(:new.DS_COR_FUNDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR_FUNDO',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR_HTML,1,4000),substr(:new.DS_COR_HTML,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR_HTML',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR_SELECAO,1,4000),substr(:new.DS_COR_SELECAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR_SELECAO',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM,1,4000),substr(:new.DS_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LEGENDA,1,4000),substr(:new.NR_SEQ_LEGENDA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LEGENDA',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HINT,1,4000),substr(:new.DS_HINT,1,4000),:new.nm_usuario,nr_seq_w,'DS_HINT',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR_FONTE,1,4000),substr(:new.DS_COR_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR_FONTE',ie_log_w,ds_w,'TASY_PADRAO_COR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TASY_PADRAO_COR ADD (
  CONSTRAINT TASPACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_PADRAO_COR ADD (
  CONSTRAINT TASPACO_TASLEGE_FK 
 FOREIGN KEY (NR_SEQ_LEGENDA) 
 REFERENCES TASY.TASY_LEGENDA (NR_SEQUENCIA),
  CONSTRAINT TASPACO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_ITEM) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TASPACO_TAPACOH_FK 
 FOREIGN KEY (NR_SEQ_COR_HTML) 
 REFERENCES TASY.TASY_PADRAO_COR_HTML (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_PADRAO_COR TO NIVEL_1;

