ALTER TABLE TASY.PROJ_VINC_CLASSIF_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_VINC_CLASSIF_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_VINC_CLASSIF_ETAPA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_CLASSIFICACAO     VARCHAR2(15 BYTE)        NOT NULL,
  IE_TESTE_PILOTO      VARCHAR2(1 BYTE),
  IE_TREINAMENTO       VARCHAR2(1 BYTE),
  IE_PARAMETRIZACAO    VARCHAR2(1 BYTE),
  IE_CADASTROS         VARCHAR2(1 BYTE),
  IE_ADERENCIA         VARCHAR2(1 BYTE),
  IE_MPO               VARCHAR2(1 BYTE),
  IE_MODULO            VARCHAR2(1 BYTE),
  IE_FASE              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJVINCLA_PK ON TASY.PROJ_VINC_CLASSIF_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJVINCLA_PK
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_VINC_CLASSIF_ETAPA ADD (
  CONSTRAINT PROJVINCLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PROJ_VINC_CLASSIF_ETAPA TO NIVEL_1;

