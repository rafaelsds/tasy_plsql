ALTER TABLE TASY.PLS_COMPETENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMPETENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMPETENCIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_MES_COMPETENCIA      DATE,
  DT_LIBERACAO            DATE,
  NM_USUARIO_LIBERACAO    VARCHAR2(40 BYTE),
  DT_FECHAMENTO           DATE,
  NM_USUARIO_FECHAMENTO   VARCHAR2(40 BYTE),
  DS_MAQUINA_LIBERACAO    VARCHAR2(80 BYTE),
  DS_MAQUINA_FECHAMENTO   VARCHAR2(80 BYTE),
  VL_TOTAL_DESPESA        NUMBER(15,2),
  VL_TOTAL_IMPOSTO        NUMBER(15,2),
  NR_SEQ_ANTERIOR         NUMBER(10),
  TX_ADMINISTRATIVA       NUMBER(7,4),
  DT_MES_COMPETENCIA_AUX  VARCHAR2(7 BYTE),
  DT_ATUALIZACAO_ATO      DATE,
  DT_ATUALIZACAO_GRUPO    DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOMP_ESTABEL_FK_I ON TASY.PLS_COMPETENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMP_I1 ON TASY.PLS_COMPETENCIA
(DT_MES_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOMP_PK ON TASY.PLS_COMPETENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_competencia_atual
before insert or update ON TASY.PLS_COMPETENCIA for each row
declare
qt_registro_w		pls_integer;

pragma autonomous_transaction;

begin
if (:new.dt_mes_competencia is not null) then
	:new.dt_mes_competencia := trunc(:new.dt_mes_competencia, 'month');
end if;
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then
	if	(inserting) or
		(updating and :old.dt_mes_competencia_aux != :new.dt_mes_competencia_aux) then
		select	count(1)
		into	qt_registro_w
		from	pls_competencia
		where	cd_estabelecimento	= :new.cd_estabelecimento
		and	dt_mes_competencia_aux	= :new.dt_mes_competencia_aux;

		if	(qt_registro_w > 0) then
			-- A competencia #@DT_COMPETENCIA#@ ja foi cadastrada para este estabelecimento, favor informar outra competencia.
			wheb_mensagem_pck.exibir_mensagem_abort( 1031372,'DT_COMPETENCIA=' || :new.dt_mes_competencia_aux);
		end if;
	end if;
end if;

end pls_competencia_atual;
/


ALTER TABLE TASY.PLS_COMPETENCIA ADD (
  CONSTRAINT PLSCOMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMPETENCIA ADD (
  CONSTRAINT PLSCOMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_COMPETENCIA TO NIVEL_1;

