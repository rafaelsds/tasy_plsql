DROP TABLE TASY.W_CONSISTENCIA_CIRURGIA CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_CONSISTENCIA_CIRURGIA
(
  NR_CIRURGIA      NUMBER(10),
  NR_PRESCRICAO    NUMBER(14),
  DS_CONSISTENCIA  VARCHAR2(2000 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_CONSISTENCIA_CIRURGIA TO NIVEL_1;

