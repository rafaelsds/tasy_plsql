ALTER TABLE TASY.PTU_AVISO_DECLARACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_AVISO_DECLARACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_AVISO_DECLARACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_AVISO_CONTA    NUMBER(10)              NOT NULL,
  NR_DECLARACAO_NASC    VARCHAR2(11 BYTE),
  NR_DIAGNOSTICO_OBITO  VARCHAR2(4 BYTE),
  NR_DECLARAO_OBITO     VARCHAR2(11 BYTE),
  IE_INDICADOR_DORN     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUAVDC_PK ON TASY.PTU_AVISO_DECLARACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUAVDC_PTUAVCT_FK_I ON TASY.PTU_AVISO_DECLARACAO
(NR_SEQ_AVISO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_AVISO_DECLARACAO ADD (
  CONSTRAINT PTUAVDC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_AVISO_DECLARACAO ADD (
  CONSTRAINT PTUAVDC_PTUAVCT_FK 
 FOREIGN KEY (NR_SEQ_AVISO_CONTA) 
 REFERENCES TASY.PTU_AVISO_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_AVISO_DECLARACAO TO NIVEL_1;

