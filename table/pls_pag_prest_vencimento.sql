ALTER TABLE TASY.PLS_PAG_PREST_VENCIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PAG_PREST_VENCIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PAG_PREST_VENCIMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PAG_PRESTADOR  NUMBER(10)              NOT NULL,
  DT_VENCIMENTO         DATE                    NOT NULL,
  VL_VENCIMENTO         NUMBER(15,2)            NOT NULL,
  NR_TITULO             NUMBER(10),
  VL_LIQUIDO            NUMBER(15,2)            NOT NULL,
  VL_IR                 NUMBER(15,2),
  VL_IMPOSTO_MUNIC      NUMBER(15,2),
  NR_NOTA_FISCAL        NUMBER(20),
  NR_SEQ_EVENTO_MOVTO   NUMBER(10),
  IE_PROXIMO_PGTO       VARCHAR2(1 BYTE),
  NR_TITULO_RECEBER     NUMBER(10),
  DT_VENC_ORIGINAL      DATE,
  VL_GLOSA              NUMBER(15,2),
  IE_SALDO_NEGATIVO     VARCHAR2(3 BYTE),
  NR_SEQ_PAG_ITEM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPPVE_PK ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVE_PLSEVMO_FK_I ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_SEQ_EVENTO_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVE_PLSEVMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVE_PLSPAGP_FK_I ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_SEQ_PAG_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVE_PLSPAIT_FK_I ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_SEQ_PAG_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVE_PLSPAIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVE_TITPAGA_FK_I ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVE_TITRECE_FK_I ON TASY.PLS_PAG_PREST_VENCIMENTO
(NR_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVE_TITRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_pag_prest_vencimento_atual
before update ON TASY.PLS_PAG_PREST_VENCIMENTO for each row
declare

nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
nr_seq_lote_w			pls_lote_pagamento.nr_sequencia%type;

begin

if	(:new.nr_titulo is null) and
	(:old.nr_titulo is not null) then
	select	max(c.nr_seq_protocolo),
		max(b.nr_seq_prestador),
		max(b.nr_seq_lote)
	into	nr_seq_protocolo_w,
		nr_seq_prestador_w,
		nr_seq_lote_w
	from	pls_pagamento_prestador		b,
		pls_conta_medica_resumo		c
	where	b.nr_seq_prestador	= c.nr_seq_prestador_pgto
	and	b.nr_seq_lote		= c.nr_seq_lote_pgto
	and	((c.ie_situacao = 'A') or (c.ie_situacao is null))
	and	b.nr_sequencia		= :new.nr_seq_pag_prestador;

	if	(nr_seq_protocolo_w is not null) then
		insert into log_tasy
			(dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values	(sysdate,
			'Trigger',
			99601,
			' Lote pgto prod m�dica: ' || nr_seq_lote_w || chr(13) ||
			' Prestador: ' || nr_seq_prestador_w || chr(13) ||
			' Protocolo ref: ' || nr_seq_protocolo_w);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_PAG_PREST_VENCIMENTO ADD (
  CONSTRAINT PLSPPVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSPPVE_UK
 UNIQUE (NR_SEQ_PAG_PRESTADOR));

ALTER TABLE TASY.PLS_PAG_PREST_VENCIMENTO ADD (
  CONSTRAINT PLSPPVE_PLSPAGP_FK 
 FOREIGN KEY (NR_SEQ_PAG_PRESTADOR) 
 REFERENCES TASY.PLS_PAGAMENTO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPPVE_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PLSPPVE_PLSEVMO_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_MOVTO) 
 REFERENCES TASY.PLS_EVENTO_MOVIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPPVE_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT PLSPPVE_PLSPAIT_FK 
 FOREIGN KEY (NR_SEQ_PAG_ITEM) 
 REFERENCES TASY.PLS_PAGAMENTO_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PAG_PREST_VENCIMENTO TO NIVEL_1;

