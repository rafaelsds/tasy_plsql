ALTER TABLE TASY.ESCALA_COGNITIVE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_COGNITIVE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_COGNITIVE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  DT_REGISTRO            DATE,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  IE_AGE                 VARCHAR2(1 BYTE),
  IE_TIME                VARCHAR2(1 BYTE),
  IE_ADDRESS             VARCHAR2(1 BYTE),
  IE_YEAR                VARCHAR2(1 BYTE),
  IE_LOCATION            VARCHAR2(1 BYTE),
  IE_RECOGNIZE_OBJECT    VARCHAR2(1 BYTE),
  IE_DOB                 VARCHAR2(1 BYTE),
  IE_SECOND_WAR          VARCHAR2(1 BYTE),
  IE_PRIME_MINISTER      VARCHAR2(1 BYTE),
  IE_COUNT_DOWN          VARCHAR2(1 BYTE),
  QT_SCORE               NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCOGN_ATEPACI_FK_I ON TASY.ESCALA_COGNITIVE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCOGN_PK ON TASY.ESCALA_COGNITIVE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_cognitive_atual

before insert or update ON TASY.ESCALA_COGNITIVE 
for each row
declare

qt_age_w number (1) := 0;

qt_DOB_w  number (1) := 0;

qt_address_w number (1) := 0;

qt_year_w number (1) := 0;

qt_location_w number (1) := 0;

qt_recognize_object_w number (1) := 0;

qt_TIME_w number (1) := 0;

qt_SECOND_WAR_w number (1) := 0;

qt_prime_minister_w number (1) := 0;

qt_count_down_w number (1) := 0;


begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

select decode(:new.ie_age,'S',1,0),

				decode(:new.IE_DOB ,'S',1,0),

				decode(:new.ie_address ,'S',1,0),

				decode(:new.ie_year,'S',1,0),

				decode(:new.ie_location ,'S',1,0),

				decode(:new.ie_recognize_object ,'S',1,0),

				decode(:new.IE_TIME,'S',1,0),

				decode(:new.IE_SECOND_WAR,'S',1,0),

				decode(:new.ie_prime_minister,'S',1,0),

				decode(:new.ie_count_down,'S',1,0)

        into qt_age_w,

        qt_DOB_w  ,

        qt_address_w ,

        qt_year_w ,

        qt_location_w,

        qt_recognize_object_w ,

        qt_TIME_w,

        qt_SECOND_WAR_w ,

        qt_prime_minister_w ,

        qt_count_down_w

  from dual;

	:new.qt_score :=      qt_age_w + qt_DOB_w +	qt_address_w +qt_year_w +qt_location_w +qt_recognize_object_w
                        +qt_TIME_w +	qt_SECOND_WAR_w + qt_prime_minister_w+ qt_count_down_w;

end if;

end;
/


ALTER TABLE TASY.ESCALA_COGNITIVE ADD (
  CONSTRAINT ESCCOGN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_COGNITIVE ADD (
  CONSTRAINT ESCCOGN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_COGNITIVE TO NIVEL_1;

