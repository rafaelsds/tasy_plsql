ALTER TABLE TASY.ESCALA_GOLDMAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_GOLDMAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_GOLDMAN
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  QT_IDADE                  NUMBER(3)           NOT NULL,
  IE_IAM                    VARCHAR2(1 BYTE)    NOT NULL,
  IE_GALOPE_B3              VARCHAR2(1 BYTE)    NOT NULL,
  IE_DISTENSAO              VARCHAR2(1 BYTE)    NOT NULL,
  IE_ESTENOSE               VARCHAR2(1 BYTE)    NOT NULL,
  IE_DISRITMIA_SINUSAL      VARCHAR2(1 BYTE)    NOT NULL,
  IE_DISRITMIA_VENTRICULAR  VARCHAR2(1 BYTE)    NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  QT_PAO2                   NUMBER(6,2)         NOT NULL,
  QT_PACO2                  NUMBER(6,2)         NOT NULL,
  QT_HCO3                   NUMBER(6,2)         NOT NULL,
  QT_POTASSIO               NUMBER(6,2)         NOT NULL,
  QT_UREIA                  NUMBER(6,2)         NOT NULL,
  QT_CREATININA             NUMBER(6,2)         NOT NULL,
  IE_TGO                    VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOENCA_HEPATICA        VARCHAR2(1 BYTE)    NOT NULL,
  IE_PAC_ACAMADO            VARCHAR2(1 BYTE)    NOT NULL,
  IE_CIR_ABDOMINAL          VARCHAR2(1 BYTE)    NOT NULL,
  IE_CIR_TORACICA           VARCHAR2(1 BYTE)    NOT NULL,
  IE_CIR_AORTICA            VARCHAR2(1 BYTE)    NOT NULL,
  IE_CIR_EMERGENCIA         VARCHAR2(1 BYTE)    NOT NULL,
  QT_PONTUACAO              NUMBER(3),
  NR_HORA                   NUMBER(2),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCGOLD_ATEPACI_FK_I ON TASY.ESCALA_GOLDMAN
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCGOLD_PESFISI_FK_I ON TASY.ESCALA_GOLDMAN
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCGOLD_PK ON TASY.ESCALA_GOLDMAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCGOLD_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_goldman_atual
before insert or update ON TASY.ESCALA_GOLDMAN for each row
declare
qt_reg_w	number(1);
qt_pontos_w	number(5);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

qt_pontos_w	:= 0;

if	(:new.qt_idade > 70) then
	qt_pontos_w	:= qt_pontos_w + 5;
end if;

if	(nvl(:new.ie_iam,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 10;
end if;

if	(nvl(:new.ie_galope_b3,'N') = 'S') or
	(nvl(:new.ie_distensao,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 11;
end if;

if	(nvl(:new.ie_estenose,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 3;
end if;

if	(nvl(:new.ie_disritmia_sinusal,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 7;
end if;

if	(nvl(:new.ie_disritmia_ventricular,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 7;
end if;

if	(:new.qt_pao2 < 60) or
	(:new.qt_paco2 > 50) or
	(:new.qt_potassio < 3) or
	(:new.qt_hco3 < 20) or
	(:new.qt_ureia > 50) or
	(:new.qt_creatinina > 3) or
	(nvl(:new.ie_tgo,'N') = 'S') or
	(nvl(:new.ie_doenca_hepatica,'N') = 'S') or
	(nvl(:new.ie_pac_acamado,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 3;
end if;

if	(nvl(:new.ie_cir_abdominal,'N') = 'S') or
	(nvl(:new.ie_cir_toracica,'N') = 'S') or
	(nvl(:new.ie_cir_aortica,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 3;
end if;

if	(nvl(:new.ie_cir_emergencia,'N') = 'S') then
	qt_pontos_w	:= qt_pontos_w + 4;
end if;

:new.qt_pontuacao	:= qt_pontos_w;

<<Final>>
qt_reg_w	:= 0;
end Escala_goldman_atual;
/


ALTER TABLE TASY.ESCALA_GOLDMAN ADD (
  CONSTRAINT ESCGOLD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_GOLDMAN ADD (
  CONSTRAINT ESCGOLD_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCGOLD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_GOLDMAN TO NIVEL_1;

