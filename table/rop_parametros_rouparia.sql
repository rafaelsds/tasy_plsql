ALTER TABLE TASY.ROP_PARAMETROS_ROUPARIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_PARAMETROS_ROUPARIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_PARAMETROS_ROUPARIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_DIAS_BAIXA_PECA   NUMBER(5),
  CD_MOTIVO_BAIXA      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPAROU_ESTABEL_FK_I ON TASY.ROP_PARAMETROS_ROUPARIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPAROU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ROPAROU_PK ON TASY.ROP_PARAMETROS_ROUPARIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPAROU_PK
  MONITORING USAGE;


CREATE INDEX TASY.ROPAROU_ROPMOBA_FK_I ON TASY.ROP_PARAMETROS_ROUPARIA
(CD_MOTIVO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPAROU_ROPMOBA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_PARAMETROS_ROUPARIA_INSERT
before insert ON TASY.ROP_PARAMETROS_ROUPARIA for each row
declare

qt_registro_w		number(10);
PRAGMA AUTONOMOUS_TRANSACTION;
begin

select	count(*)
into	qt_registro_w
from	rop_parametros_rouparia
where	cd_estabelecimento = :new.cd_estabelecimento;

if	(qt_registro_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265562);
	--'J� existe um cadastro dos par�metros da rouparia para esse estabelecimento. N�o � permitido par�metros duplicadas.'
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ROP_PARAMETROS_ROUPARIA_tp  after update ON TASY.ROP_PARAMETROS_ROUPARIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'ROP_PARAMETROS_ROUPARIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_BAIXA,1,4000),substr(:new.CD_MOTIVO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_BAIXA',ie_log_w,ds_w,'ROP_PARAMETROS_ROUPARIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_BAIXA_PECA,1,4000),substr(:new.QT_DIAS_BAIXA_PECA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_BAIXA_PECA',ie_log_w,ds_w,'ROP_PARAMETROS_ROUPARIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_PARAMETROS_ROUPARIA ADD (
  CONSTRAINT ROPAROU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_PARAMETROS_ROUPARIA ADD (
  CONSTRAINT ROPAROU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPAROU_ROPMOBA_FK 
 FOREIGN KEY (CD_MOTIVO_BAIXA) 
 REFERENCES TASY.ROP_MOTIVO_BAIXA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_PARAMETROS_ROUPARIA TO NIVEL_1;

