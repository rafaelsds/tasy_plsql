ALTER TABLE TASY.VIA_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VIA_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.VIA_GRUPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.VIAGRP_GRPROU_FK_I ON TASY.VIA_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.VIAGRP_PK ON TASY.VIA_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VIAGRP_VIAAPLI_FK_I ON TASY.VIA_GRUPO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.VIA_GRUPO ADD (
  CONSTRAINT VIAGRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.VIA_GRUPO ADD (
  CONSTRAINT VIAGRP_GRPROU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GROUP_ROUTE (NR_SEQUENCIA),
  CONSTRAINT VIAGRP_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO)
    ON DELETE CASCADE);

