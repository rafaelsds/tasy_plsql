ALTER TABLE TASY.TESTE_UNITARIO_PL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TESTE_UNITARIO_PL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TESTE_UNITARIO_PL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MACRO             VARCHAR2(200 BYTE),
  DS_COMANDO           CLOB,
  NR_SEQ_SUP           NUMBER(10),
  NR_ORDEM             NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TESTUNITPL_PK ON TASY.TESTE_UNITARIO_PL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TESTE_UNITARIO_PL ADD (
  CONSTRAINT TESTUNITPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.TESTE_UNITARIO_PL TO NIVEL_1;

