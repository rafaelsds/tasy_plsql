DROP TABLE TASY.SALDO_JULHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SALDO_JULHO
(
  CD_LOCAL_ESTOQUE  NUMBER(4)                   NOT NULL,
  CD_MATERIAL       NUMBER(6)                   NOT NULL,
  QT_ESTOQUE_JUN    NUMBER(13,4)                NOT NULL,
  QT_ESTOQUE_JUL    NUMBER(13,4)                NOT NULL,
  QT_ESTOQUE_JUNA   NUMBER(13,4)                NOT NULL,
  QT_INVENTARIO     NUMBER(13,4)                NOT NULL,
  VL_ESTOQUE        NUMBER(15,2)                NOT NULL,
  VL_INVENTARIO     NUMBER(15,2)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.SALDO_JULHO TO NIVEL_1;

