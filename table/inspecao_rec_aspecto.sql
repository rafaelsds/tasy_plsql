ALTER TABLE TASY.INSPECAO_REC_ASPECTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INSPECAO_REC_ASPECTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INSPECAO_REC_ASPECTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_GRUPO_MATERIAL     NUMBER(3),
  DS_ASPECTO            VARCHAR2(255 BYTE)      NOT NULL,
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_MATERIAL           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INSREAS_CLAMATE_FK_I ON TASY.INSPECAO_REC_ASPECTO
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSREAS_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INSREAS_ESTABEL_FK_I ON TASY.INSPECAO_REC_ASPECTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INSREAS_GRUMATE_FK_I ON TASY.INSPECAO_REC_ASPECTO
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSREAS_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INSREAS_MATERIA_FK_I ON TASY.INSPECAO_REC_ASPECTO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSREAS_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INSREAS_PK ON TASY.INSPECAO_REC_ASPECTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSREAS_PK
  MONITORING USAGE;


CREATE INDEX TASY.INSREAS_SUBMATE_FK_I ON TASY.INSPECAO_REC_ASPECTO
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INSREAS_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.INSPECAO_REC_ASPECTO_ATUAL
before insert or update ON TASY.INSPECAO_REC_ASPECTO for each row
declare

begin

if	(:new.cd_grupo_material is not null) then
	begin
	if	(:new.cd_subgrupo_material is not null) or
		(:new.cd_classe_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
elsif	(:new.cd_subgrupo_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_classe_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
elsif	(:new.cd_classe_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_subgrupo_material is not null) or
		(:new.cd_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
elsif	(:new.cd_material is not null) then
	begin
	if	(:new.cd_grupo_material is not null) or
		(:new.cd_subgrupo_material is not null) or
		(:new.cd_classe_material is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266075);
		--'Favor informar somente uma informa��o (S� o Grupo, ou s� o Subgrupo, ou s� a Classe, ou s� o Material).');
	end if;
	end;
end if;

end;
/


ALTER TABLE TASY.INSPECAO_REC_ASPECTO ADD (
  CONSTRAINT INSREAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INSPECAO_REC_ASPECTO ADD (
  CONSTRAINT INSREAS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT INSREAS_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT INSREAS_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT INSREAS_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT INSREAS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.INSPECAO_REC_ASPECTO TO NIVEL_1;

