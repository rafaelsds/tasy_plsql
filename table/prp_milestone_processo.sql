ALTER TABLE TASY.PRP_MILESTONE_PROCESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRP_MILESTONE_PROCESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRP_MILESTONE_PROCESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_MILESTONE         VARCHAR2(255 BYTE)       NOT NULL,
  NM_MILESTONE_INGLES  VARCHAR2(255 BYTE)       NOT NULL,
  NM_MILESTONE_AVREV   VARCHAR2(4 BYTE)         NOT NULL,
  NR_SEQ_FASE_ANT      NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NM_MILESTONE_ABREV   VARCHAR2(4 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPMILE_PK ON TASY.PRP_MILESTONE_PROCESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPMILE_PRPFASE_FK_I ON TASY.PRP_MILESTONE_PROCESSO
(NR_SEQ_FASE_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPMILE_PRPFASE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRP_MILESTONE_PROCESSO ADD (
  CONSTRAINT PRPMILE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRP_MILESTONE_PROCESSO ADD (
  CONSTRAINT PRPMILE_PRPFASE_FK 
 FOREIGN KEY (NR_SEQ_FASE_ANT) 
 REFERENCES TASY.PRP_FASE_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRP_MILESTONE_PROCESSO TO NIVEL_1;

