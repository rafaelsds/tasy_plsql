ALTER TABLE TASY.CTB_PARAM_LOTE_CONTAS_REC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_PARAM_LOTE_CONTAS_REC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_PARAM_LOTE_CONTAS_REC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_EMPRESA                 NUMBER(4)          NOT NULL,
  CD_ESTAB_EXCLUSIVO         NUMBER(4),
  IE_CTB_ONLINE              VARCHAR2(1 BYTE)   NOT NULL,
  IE_CC_GLOSA                VARCHAR2(15 BYTE)  NOT NULL,
  IE_GERAR_CONTAB_REG_TIT    VARCHAR2(1 BYTE)   NOT NULL,
  IE_DT_CONTAB_CR            VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONTAB_TIT_CANCELADO    VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONTAB_REC_CLASSIF      VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONTAB_CARTAO_CR        VARCHAR2(2 BYTE)   NOT NULL,
  IE_CONTAB_CLASSIF_TIT_REC  VARCHAR2(1 BYTE)   NOT NULL,
  IE_DT_CONTAB_GLOSA         VARCHAR2(1 BYTE),
  IE_CONTAB_PROV_CONTRATO    VARCHAR2(1 BYTE),
  IE_CONTAB_VL_ESTORNO       VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONTAB_GLOSA_DETALHE    VARCHAR2(1 BYTE)   NOT NULL,
  IE_PROD_CLASSIF_BAIXA      VARCHAR2(1 BYTE),
  IE_CONTAB_GLOSA_CRIT       VARCHAR2(1 BYTE),
  IE_MOVTO_CARTAO_ESTAB      VARCHAR2(1 BYTE),
  IE_CONTAB_CURTO_PRAZO      VARCHAR2(1 BYTE),
  IE_CONTAB_TIT_DESDOB       VARCHAR2(1 BYTE),
  IE_CONTAB_TRANS_FIN_BAIXA  VARCHAR2(1 BYTE),
  IE_CONTAB_DESP_CARTAO      VARCHAR2(1 BYTE),
  IE_CONTAB_PJ_CONVENIO      VARCHAR2(1 BYTE)   DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBPARCRE_EMPRESA_FK_I ON TASY.CTB_PARAM_LOTE_CONTAS_REC
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBPARCRE_ESTABEL_FK_I ON TASY.CTB_PARAM_LOTE_CONTAS_REC
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBPARCRE_PK ON TASY.CTB_PARAM_LOTE_CONTAS_REC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CTB_PARAM_LOTE_CONTAS_REC_tp  after update ON TASY.CTB_PARAM_LOTE_CONTAS_REC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONTAB_PJ_CONVENIO,1,4000),substr(:new.IE_CONTAB_PJ_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_PJ_CONVENIO',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DT_CONTAB_GLOSA,1,4000),substr(:new.IE_DT_CONTAB_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_CONTAB_GLOSA',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_CARTAO_CR,1,4000),substr(:new.IE_CONTAB_CARTAO_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_CARTAO_CR',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_CLASSIF_TIT_REC,1,4000),substr(:new.IE_CONTAB_CLASSIF_TIT_REC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_CLASSIF_TIT_REC',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_VL_ESTORNO,1,4000),substr(:new.IE_CONTAB_VL_ESTORNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_VL_ESTORNO',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_TIT_DESDOB,1,4000),substr(:new.IE_CONTAB_TIT_DESDOB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_TIT_DESDOB',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROD_CLASSIF_BAIXA,1,4000),substr(:new.IE_PROD_CLASSIF_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROD_CLASSIF_BAIXA',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOVTO_CARTAO_ESTAB,1,4000),substr(:new.IE_MOVTO_CARTAO_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOVTO_CARTAO_ESTAB',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_CURTO_PRAZO,1,4000),substr(:new.IE_CONTAB_CURTO_PRAZO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_CURTO_PRAZO',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_PROV_CONTRATO,1,4000),substr(:new.IE_CONTAB_PROV_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_PROV_CONTRATO',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_REC_CLASSIF,1,4000),substr(:new.IE_CONTAB_REC_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_REC_CLASSIF',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTAB_TIT_CANCELADO,1,4000),substr(:new.IE_CONTAB_TIT_CANCELADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTAB_TIT_CANCELADO',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DT_CONTAB_CR,1,4000),substr(:new.IE_DT_CONTAB_CR,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_CONTAB_CR',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_CONTAB_REG_TIT,1,4000),substr(:new.IE_GERAR_CONTAB_REG_TIT,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_CONTAB_REG_TIT',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CC_GLOSA,1,4000),substr(:new.IE_CC_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CC_GLOSA',ie_log_w,ds_w,'CTB_PARAM_LOTE_CONTAS_REC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CTB_PARAM_LOTE_CONTAS_REC ADD (
  CONSTRAINT CTBPARCRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CTB_PARAM_LOTE_CONTAS_REC ADD (
  CONSTRAINT CTBPARCRE_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CTBPARCRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CTB_PARAM_LOTE_CONTAS_REC TO NIVEL_1;

