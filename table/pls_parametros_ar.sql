ALTER TABLE TASY.PLS_PARAMETROS_AR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAMETROS_AR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAMETROS_AR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_NOVO_RESULTADO       VARCHAR2(1 BYTE),
  IE_DATA_CONTA_MEDICA    VARCHAR2(2 BYTE),
  IE_PRO_RATA             VARCHAR2(1 BYTE),
  IE_DATA_COPARTICIPACAO  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPAAR_ESTABEL_FK_I ON TASY.PLS_PARAMETROS_AR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPAAR_PK ON TASY.PLS_PARAMETROS_AR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_parametros_ar_afterupdate
after update ON TASY.PLS_PARAMETROS_AR 
for each row
declare

begin

if	(:new.ie_novo_resultado <> :old.ie_novo_resultado) then
	pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_AR', 'IE_NOVO_RESULTADO', :old.ie_novo_resultado, 
					:new.ie_novo_resultado, :new.nm_usuario, :new.cd_estabelecimento);
end if;

if	(:new.ie_data_conta_medica <> :old.ie_data_conta_medica) then
	pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_AR', 'IE_DATA_CONTA_MEDICA', :old.ie_data_conta_medica, 
					:new.ie_data_conta_medica, :new.nm_usuario, :new.cd_estabelecimento);
end if;

if	(:new.ie_pro_rata <> :old.ie_pro_rata) then
	pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_AR', 'IE_PRO_RATA', :old.ie_pro_rata, 
					:new.ie_pro_rata, :new.nm_usuario, :new.cd_estabelecimento);
end if;

if	(:new.ie_data_coparticipacao <> :old.ie_data_coparticipacao) then
	pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_AR', 'IE_DATA_COPARTICIPACAO', :old.ie_data_coparticipacao, 
					:new.ie_data_coparticipacao, :new.nm_usuario, :new.cd_estabelecimento);
end if;

end;
/


ALTER TABLE TASY.PLS_PARAMETROS_AR ADD (
  CONSTRAINT PLSPAAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PARAMETROS_AR ADD (
  CONSTRAINT PLSPAAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_PARAMETROS_AR TO NIVEL_1;

