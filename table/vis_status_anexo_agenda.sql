ALTER TABLE TASY.VIS_STATUS_ANEXO_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VIS_STATUS_ANEXO_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.VIS_STATUS_ANEXO_AGENDA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_STATUS_AGENDA  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.VISTANA_PESFISI_FK_I ON TASY.VIS_STATUS_ANEXO_AGENDA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.VISTANA_PK ON TASY.VIS_STATUS_ANEXO_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VISTANA_PK
  MONITORING USAGE;


CREATE INDEX TASY.VISTANA_SETATEN_FK_I ON TASY.VIS_STATUS_ANEXO_AGENDA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VISTANA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.VISTANA_STANAGE_FK_I ON TASY.VIS_STATUS_ANEXO_AGENDA
(NR_SEQ_STATUS_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VISTANA_STANAGE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.VIS_STATUS_ANEXO_AGENDA ADD (
  CONSTRAINT VISTANA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VIS_STATUS_ANEXO_AGENDA ADD (
  CONSTRAINT VISTANA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT VISTANA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT VISTANA_STANAGE_FK 
 FOREIGN KEY (NR_SEQ_STATUS_AGENDA) 
 REFERENCES TASY.STATUS_ANEXO_AGENDA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.VIS_STATUS_ANEXO_AGENDA TO NIVEL_1;

