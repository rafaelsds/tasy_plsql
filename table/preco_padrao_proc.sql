ALTER TABLE TASY.PRECO_PADRAO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_PADRAO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_PADRAO_PROC
(
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_TABELA_CUSTO           NUMBER(5)           NOT NULL,
  IE_ORIGEM_PROCED          NUMBER(10),
  CD_PROCEDIMENTO           NUMBER(15),
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  QT_PARAMETRO              NUMBER(15,4),
  PR_DVV                    NUMBER(15,4),
  PR_DESPESA_FIXA           NUMBER(15,4),
  PR_CUSTO_INDIRETO_FIXO    NUMBER(15,4),
  PR_MARGEM_LUCRO           NUMBER(15,4),
  VL_CUSTO_VARIAVEL         NUMBER(15,4),
  VL_CUSTO_DIRETO_FIXO      NUMBER(15,4),
  VL_PRECO_CALCULADO        NUMBER(15,4),
  VL_PRECO_TABELA           NUMBER(15,4),
  VL_PRECO_TABELA_ANTERIOR  NUMBER(15,4),
  CD_CENTRO_CONTROLE        NUMBER(8),
  VL_CUSTO_DIR_APOIO        NUMBER(15,2)        NOT NULL,
  VL_CUSTO_MAO_OBRA         NUMBER(15,2)        NOT NULL,
  VL_CUSTO_INDIRETO         NUMBER(15,2)        NOT NULL,
  VL_DESPESA                NUMBER(15,2)        NOT NULL,
  IE_CALCULA_CONTA          VARCHAR2(1 BYTE)    NOT NULL,
  VL_CUSTO_HM               NUMBER(15,2)        NOT NULL,
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_PROC_INTERNO       NUMBER(10),
  NR_SEQ_EXAME              NUMBER(10),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  NR_SEQ_TABELA             NUMBER(10),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_FICHA_TECNICA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CUSPRPP_CENCONT_FK_I ON TASY.PRECO_PADRAO_PROC
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_EXALABO_FK_I ON TASY.PRECO_PADRAO_PROC
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_FICTECN_FK_I ON TASY.PRECO_PADRAO_PROC
(NR_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CUSPRPP_PK ON TASY.PRECO_PADRAO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_PROCEDI_FK_I ON TASY.PRECO_PADRAO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_PROINTE_FK_I ON TASY.PRECO_PADRAO_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_SETATEN_FK_I ON TASY.PRECO_PADRAO_PROC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUSPRPP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CUSPRPP_TABCUST_FK_I ON TASY.PRECO_PADRAO_PROC
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSPRPP_TABCUST_FK_I2 ON TASY.PRECO_PADRAO_PROC
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRECO_PADRAO_PROC ADD (
  CONSTRAINT CUSPRPP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          832K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_PADRAO_PROC ADD (
  CONSTRAINT CUSPRPP_FICTECN_FK 
 FOREIGN KEY (NR_FICHA_TECNICA) 
 REFERENCES TASY.FICHA_TECNICA (NR_FICHA_TECNICA)
    ON DELETE CASCADE,
  CONSTRAINT CUSPRPP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CUSPRPP_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE)
    ON DELETE CASCADE,
  CONSTRAINT CUSPRPP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT CUSPRPP_TABCUST_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.TABELA_CUSTO (NR_SEQUENCIA),
  CONSTRAINT CUSPRPP_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT CUSPRPP_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME));

GRANT SELECT ON TASY.PRECO_PADRAO_PROC TO NIVEL_1;

