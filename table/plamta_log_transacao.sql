ALTER TABLE TASY.PLAMTA_LOG_TRANSACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLAMTA_LOG_TRANSACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLAMTA_LOG_TRANSACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO       NUMBER(10)             NOT NULL,
  NR_INTERNO_CONTA       NUMBER(10)             NOT NULL,
  NR_DOC_CONV_PRINCIPAL  VARCHAR2(20 BYTE),
  CD_CGC_LOGIN           VARCHAR2(14 BYTE),
  CD_SENHA_PLAMTA        VARCHAR2(20 BYTE),
  DT_ENTRADA             DATE,
  DS_METODO              VARCHAR2(150 BYTE)     NOT NULL,
  DS_MENSAGEM            VARCHAR2(4000 BYTE),
  IE_TIPO_MENSAGEM       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLAMLOT_CONPACI_FK_I ON TASY.PLAMTA_LOG_TRANSACAO
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLAMLOT_CONPACI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLAMLOT_PK ON TASY.PLAMTA_LOG_TRANSACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLAMLOT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLAMLOT_PROCONV_FK_I ON TASY.PLAMTA_LOG_TRANSACAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLAMLOT_PROCONV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLAMTA_LOG_TRANSACAO ADD (
  CONSTRAINT PLAMLOT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLAMTA_LOG_TRANSACAO ADD (
  CONSTRAINT PLAMLOT_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT PLAMLOT_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLAMTA_LOG_TRANSACAO TO NIVEL_1;

