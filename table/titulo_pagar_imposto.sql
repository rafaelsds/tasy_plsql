ALTER TABLE TASY.TITULO_PAGAR_IMPOSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_IMPOSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_IMPOSTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_TITULO             NUMBER(10)              NOT NULL,
  CD_TRIBUTO            NUMBER(10)              NOT NULL,
  IE_PAGO_PREV          VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_IMPOSTO            DATE                    NOT NULL,
  VL_BASE_CALCULO       NUMBER(15,2)            NOT NULL,
  VL_IMPOSTO            NUMBER(15,2)            NOT NULL,
  DS_EMP_RETENCAO       VARCHAR2(255 BYTE),
  PR_IMPOSTO            NUMBER(7,4),
  CD_BENEFICIARIO       VARCHAR2(14 BYTE),
  CD_CONTA_FINANC       NUMBER(10),
  NR_SEQ_TRANS_REG      NUMBER(10),
  NR_SEQ_TRANS_BAIXA    NUMBER(10),
  CD_COND_PAGTO         NUMBER(10),
  VL_NAO_RETIDO         NUMBER(15,2)            NOT NULL,
  IE_VENCIMENTO         VARCHAR2(1 BYTE)        NOT NULL,
  VL_BASE_NAO_RETIDO    NUMBER(15,2)            NOT NULL,
  VL_TRIB_ADIC          NUMBER(15,2)            NOT NULL,
  VL_BASE_ADIC          NUMBER(15,2)            NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  VL_REDUCAO            NUMBER(15,2),
  VL_DESC_BASE          NUMBER(15,2),
  CD_DARF               VARCHAR2(10 BYTE),
  CD_VARIACAO           VARCHAR2(2 BYTE),
  IE_PERIODICIDADE      VARCHAR2(1 BYTE),
  NR_SEQ_BAIXA          NUMBER(10),
  NR_SEQ_TIT_ADIANT     NUMBER(10),
  IE_INSS_IR_SEMANAL    VARCHAR2(1 BYTE),
  NR_LOTE_CONTABIL      NUMBER(10),
  NR_SEQ_PLS_VENC_TRIB  NUMBER(10),
  NR_SEQ_TRIB_CP        NUMBER(10),
  DT_CONTABIL           DATE,
  DS_STACK              VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITTEIM_CONFINA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITTEIM_CONPAGA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(CD_COND_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITTEIM_I1 ON TASY.TITULO_PAGAR_IMPOSTO
(NR_TITULO, CD_TRIBUTO, IE_PAGO_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_LOTCONT_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_PESJURI_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(CD_BENEFICIARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITTEIM_PK ON TASY.TITULO_PAGAR_IMPOSTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_PLSPPVT_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_SEQ_PLS_VENC_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_TITPAAD_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_TITULO, NR_SEQ_TIT_ADIANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_TITPAAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITTEIM_TITPABA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_TITPAGA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_TRAFINA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_SEQ_TRANS_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITTEIM_TRAFINA_FK2_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_SEQ_TRANS_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITTEIM_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITTEIM_TRIBUTO_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITTEIM_TRICOPA_FK_I ON TASY.TITULO_PAGAR_IMPOSTO
(NR_SEQ_TRIB_CP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_afatual
after insert ON TASY.TITULO_PAGAR_IMPOSTO for each row
declare

qt_reg_w 		number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin

/*Esse select � para tentar evitar a duplicidade de envio para intpd_fila_transmissao*/
select  count(*)
into    qt_reg_w
from    intpd_fila_transmissao
where   nr_seq_documento                = to_char(:new.nr_titulo)
and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
and		ie_evento in ('53');

/*OS 1430325 - Envio de classifica��o de t�tulos a pagar*/
if (qt_reg_w = 0) then
	select  max (cd_estabelecimento),
			max (ie_tipo_titulo)
			into reg_integracao_w.cd_estab_documento,
			     reg_integracao_w.ie_tipo_titulo_cpa
			from titulo_pagar
			where nr_titulo = :new.nr_titulo;
	gerar_int_padrao.gravar_integracao('53', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_bfupdate
before update ON TASY.TITULO_PAGAR_IMPOSTO FOR EACH ROW
declare
cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	titulo_pagar
where	nr_titulo = :new.nr_titulo;

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_contabil);

if	(:old.ds_stack is null) then
	:new.ds_stack := substr('CallStack Update: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
end if;

if	(:new.nr_seq_pls_venc_trib is not null) and
	(:old.dt_imposto <> :new.dt_imposto) then
	update	pls_pag_prest_venc_trib
	set	dt_imposto	= :new.dt_imposto
	where	nr_sequencia	= :new.nr_seq_pls_venc_trib;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_update
after update ON TASY.TITULO_PAGAR_IMPOSTO FOR EACH ROW
declare

qt_reg_w 		number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin

select  count(*)
into    qt_reg_w
from    intpd_fila_transmissao
where   nr_seq_documento                = to_char(:new.nr_titulo)
and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
and		ie_evento in ('53');

if (qt_reg_w = 0) then
	/*OS 1430325 - Envio de classifica��o de t�tulos a pagar*/
	select  max (cd_estabelecimento),
		max (ie_tipo_titulo)
		into reg_integracao_w.cd_estab_documento,
		     reg_integracao_w.ie_tipo_titulo_cpa
		from titulo_pagar
		where nr_titulo = :new.nr_titulo;
	gerar_int_padrao.gravar_integracao('53', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
end if;

if	(:new.VL_IMPOSTO <> :old.VL_IMPOSTO) or
	(:new.VL_BASE_CALCULO <> :old.VL_BASE_CALCULO) or
	(:new.PR_IMPOSTO <> :old.PR_IMPOSTO) or
	(:new.VL_DESC_BASE <> :old.VL_DESC_BASE) or
	(:new.VL_NAO_RETIDO <> :old.VL_NAO_RETIDO) or
	(:new.VL_BASE_NAO_RETIDO <> :old.VL_BASE_NAO_RETIDO) or
	(:new.VL_TRIB_ADIC <> :old.VL_TRIB_ADIC) or
	(:new.VL_BASE_ADIC <> :old.VL_BASE_ADIC) or
	(:new.VL_REDUCAO <> :old.VL_REDUCAO) then
	null;
	/*insert into logxxxxxx_tasy
			(DT_ATUALIZACAO,
			NM_USUARIO,
			CD_LOG,
			DS_LOG)
	values		(sysdate,
			 'SQLPLUS',
			 55712,
			'T�tulo: ' || :new.nr_titulo || chr(13) ||
			'Tributo: ' || :new.CD_TRIBUTO || chr(13) ||
			':new.VL_IMPOSTO = ' || 	:new.VL_IMPOSTO 	|| ' :old.VL_IMPOSTO = ' 	|| :old.VL_IMPOSTO || chr(13) ||
			':new.VL_BASE_CALCULO = ' || 	:new.VL_BASE_CALCULO 	|| ' :old.VL_BASE_CALCULO = ' 	|| :old.VL_BASE_CALCULO || chr(13) ||
			':new.PR_IMPOSTO = ' || 	:new.PR_IMPOSTO		|| ' :old.PR_IMPOSTO = ' 	|| :old.PR_IMPOSTO || chr(13) ||
			':new.VL_DESC_BASE = ' || 	:new.VL_DESC_BASE 	|| ' :old.VL_DESC_BASE = ' 	|| :old.VL_DESC_BASE || chr(13) ||
			':new.VL_NAO_RETIDO = ' || :new.VL_NAO_RETIDO	|| ' :old.VL_NAO_RETIDO = '|| :old.VL_NAO_RETIDO || chr(13) ||
			':new.VL_BASE_NAO_RETIDO = ' || :new.VL_BASE_NAO_RETIDO	|| ' :old.VL_BASE_NAO_RETIDO = '|| :old.VL_BASE_NAO_RETIDO || chr(13) ||
			':new.VL_TRIB_ADIC = ' || 	:new.VL_TRIB_ADIC	|| ' :old.VL_TRIB_ADIC = ' 	|| :old.VL_TRIB_ADIC || chr(13) ||
			':new.VL_BASE_ADIC = ' ||	:new.VL_BASE_ADIC	|| ' :old.VL_BASE_ADIC = ' 	|| :old.VL_BASE_ADIC || chr(13) ||
			':new.VL_REDUCAO = ' ||		:new.VL_REDUCAO		|| ' :old.VL_REDUCAO = ' 	|| :old.VL_REDUCAO);*/

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_delete
before delete ON TASY.TITULO_PAGAR_IMPOSTO FOR EACH ROW
declare
qt_registro_w	pls_integer;
begin

select	count(1)
into	qt_registro_w
from	pls_pp_lr_base_trib
where	nr_seq_vl_tit_trib = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457543);
end if;

select	count(1)
into	qt_registro_w
from	pls_pp_base_acum_trib
where	nr_seq_vl_tit_trib = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457543);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_after
after insert ON TASY.TITULO_PAGAR_IMPOSTO 
for each row
declare

vl_movimento_w			ctb_documento.vl_movimento%type;
cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;
ie_contab_prov_tributo_w	parametros_contas_pagar.ie_contab_prov_tributo%type;
vl_titulo_w			titulo_pagar.vl_titulo%type;
nr_seq_trans_financ_w		ctb_documento.nr_seq_trans_financ%type;
nr_seq_trans_fin_contab_w	titulo_pagar.nr_seq_trans_fin_contab%type;
nr_seq_info_w			ctb_documento.nr_seq_info%type;	
nm_tabela_w			ctb_documento.nm_tabela%type;	
nr_documento_w			ctb_documento.nr_documento%type;
nr_seq_doc_compl_w		ctb_documento.nr_seq_doc_compl%type;
nr_seq_baixa_w			titulo_pagar_baixa.nr_sequencia%type;
dt_movimento_w			ctb_documento.dt_competencia%type;
ie_contab_prov_cp_w		tributo.ie_contab_prov_cp%type;
nr_doc_analitico_w		ctb_documento.nr_doc_analitico%type;

cursor c01 is
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 7
	and 	a.nm_atributo in ( 'VL_TRANSF_TRIBUTO', 'VL_PROV_TRIBUTO')
	union all
	select	a.nm_atributo,
		18 cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 7
	and 	a.nm_atributo in ( 'VL_PROV_TRIBUTO');

c01_w		c01%rowtype;

begin

select	cd_estabelecimento,
	vl_titulo,
	nr_seq_trans_fin_contab
into	cd_estabelecimento_w,
	vl_titulo_w,
	nr_seq_trans_fin_contab_w
from	titulo_pagar
where	nr_titulo = :new.nr_titulo;

begin
select  nvl(ie_contab_prov_tributo, 'N')
into    ie_contab_prov_tributo_w
from    parametros_contas_pagar
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
        ie_contab_prov_tributo_w := 'N';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_prov_tributo, 'N'), ie_contab_prov_tributo_w)
into    ie_contab_prov_tributo_w
from    ctb_param_lote_contas_pag a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

if	(ie_contab_prov_tributo_w = 'S') then
	begin
	
	open c01;
	loop
	fetch c01 into	
		c01_w;
	exit when c01%notfound;
		begin
		vl_movimento_w:= 0;
		if	(c01_w.cd_tipo_lote_contab = 7) then
			begin
			
			begin
			select	a.dt_baixa
			into	dt_movimento_w
			from	titulo_pagar_baixa a
			where	a.nr_titulo = :new.nr_titulo
			and	a.nr_sequencia = :new.nr_seq_baixa;
			
			exception
			when others then
				nr_seq_baixa_w:= null;
			end;

			if	(c01_w.nm_atributo = 'VL_TRANSF_TRIBUTO') then
				begin
						
				if	(nvl(nr_seq_baixa_w,0) <> 0) then
					begin
					
					begin
					
					select	nr_seq_trans_imp
					into	nr_seq_trans_fin_contab_w
					from	tributo
					where 	cd_tributo = :new.cd_tributo;
					
					exception
					when others then
						nr_seq_trans_fin_contab_w:= null;
					end;
					
					vl_movimento_w			:= vl_titulo_w;
					nr_seq_trans_financ_w		:= nr_seq_trans_fin_contab_w;
					nr_seq_info_w			:= 13;
					nm_tabela_w			:= 'TITULO_PAGAR_BAIXA';
					nr_documento_w			:= :new.nr_titulo;
					nr_seq_doc_compl_w		:= :new.nr_seq_baixa;
					nr_doc_analitico_w		:= :new.nr_sequencia;			
					
					end;
				end if;
				
				end;
			elsif	(c01_w.nm_atributo = 'VL_PROV_TRIBUTO') then
				begin
				
				vl_movimento_w		:= :new.vl_imposto;
				nr_seq_trans_financ_w	:= nr_seq_trans_fin_contab_w;
				nr_seq_info_w		:= 59;
				nm_tabela_w		:= 'TITULO_PAGAR_IMPOSTO';
				nr_documento_w		:= :new.nr_titulo;
				nr_seq_doc_compl_w	:= :new.nr_seq_baixa;
				nr_doc_analitico_w	:= :new.nr_sequencia;
				dt_movimento_w		:= :new.dt_imposto;
				
				end;
			end if;
			end;
		elsif	(c01_w.cd_tipo_lote_contab = 18) then
			begin
			
			if	(c01_w.nm_atributo = 'VL_PROV_TRIBUTO') then
				begin
				
				begin
					
				select	ie_contab_prov_cp
				into	ie_contab_prov_cp_w
				from	tributo
				where 	cd_tributo = :new.cd_tributo;
				
				exception
				when others then
					ie_contab_prov_cp_w:= null;
				end;
				
				if	(nvl(ie_contab_prov_cp_w, 'X') = 'S') then
					begin
					
					begin
					
					select	nr_seq_trans_fin,
						dt_baixa
					into	nr_seq_trans_financ_w,
						dt_movimento_w
					from	titulo_pagar_baixa
					where	nr_titulo = :new.nr_titulo
					and	nr_sequencia = :new.nr_seq_baixa;
					
					exception
					when others then
						nr_seq_trans_financ_w:= null;
						dt_movimento_w:= null;
					end;
					
					vl_movimento_w		:= :new.vl_imposto;
					nr_seq_info_w		:= 59;
					nm_tabela_w		:= 'TITULO_PAGAR_IMPOSTO';
					nr_documento_w		:= :new.nr_titulo;
					nr_seq_doc_compl_w	:= :new.nr_seq_baixa;
					nr_doc_analitico_w	:= :new.nr_sequencia;
					
					
					end;
				end if;
				
				end;
			end if;
			
			end;
		end if;
		
		if	(nvl(vl_movimento_w, 0) <> 0) then
			begin
			
			ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
										dt_movimento_w,
										c01_w.cd_tipo_lote_contab,
										nr_seq_trans_financ_w,
										nr_seq_info_w,
										nr_documento_w,
										nr_seq_doc_compl_w,
										nr_doc_analitico_w,
										vl_movimento_w,
										nm_tabela_w,
										c01_w.nm_atributo,
										:new.nm_usuario);	
			
			end;
		end if;
		
		end;
	end loop;
	close c01;
	
	
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_imposto_insert
BEFORE INSERT ON TASY.TITULO_PAGAR_IMPOSTO FOR EACH ROW
declare
qt_pag_prest_venc_w	number(5)	:= 0;
cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;

BEGIN
/* ebcabral - 30/01/2014 - Se for pagamento de produ��o m�dica, pode gerar imposto negativo */
select	count(1)
into	qt_pag_prest_venc_w
from	pls_pag_prest_vencimento 	b,
	pls_pag_prest_venc_trib		a
where	a.nr_seq_vencimento 	= b.nr_sequencia
and	b.nr_titulo 		= :new.nr_titulo
and	a.vl_imposto 		< 0
and	rownum 			= 1;

if	(qt_pag_prest_venc_w = 0) and
	(:new.vl_imposto < 0) and
	(:new.nr_seq_baixa is null) then		-- Edgar 13/02/2008, inclui este if
	wheb_mensagem_pck.exibir_mensagem_abort(263383,'VL_TRIBUTO=' || :new.vl_imposto);
end if;


select	cd_estabelecimento
into	cd_estabelecimento_w
from	titulo_pagar
where	nr_titulo = :new.nr_titulo;
			/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
			philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_imposto);
:new.ds_stack := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);

END;
/


ALTER TABLE TASY.TITULO_PAGAR_IMPOSTO ADD (
  CONSTRAINT TITTEIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_IMPOSTO ADD (
  CONSTRAINT TITTEIM_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITTEIM_PLSPPVT_FK 
 FOREIGN KEY (NR_SEQ_PLS_VENC_TRIB) 
 REFERENCES TASY.PLS_PAG_PREST_VENC_TRIB (NR_SEQUENCIA),
  CONSTRAINT TITTEIM_TRICOPA_FK 
 FOREIGN KEY (NR_SEQ_TRIB_CP) 
 REFERENCES TASY.TRIBUTO_CONTA_PAGAR (NR_SEQUENCIA),
  CONSTRAINT TITTEIM_TITPAAD_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_TIT_ADIANT) 
 REFERENCES TASY.TITULO_PAGAR_ADIANT (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT TITTEIM_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITTEIM_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT TITTEIM_PESJURI_FK 
 FOREIGN KEY (CD_BENEFICIARIO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TITTEIM_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TITTEIM_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_REG) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITTEIM_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITTEIM_CONPAGA_FK 
 FOREIGN KEY (CD_COND_PAGTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT TITTEIM_TITPABA_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_PAGAR_BAIXA (NR_TITULO,NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_PAGAR_IMPOSTO TO NIVEL_1;

