ALTER TABLE TASY.PLS_PAG_PREST_VENC_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PAG_PREST_VENC_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PAG_PREST_VENC_TRIB
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_VENCIMENTO           NUMBER(10),
  CD_TRIBUTO                  NUMBER(10)        NOT NULL,
  DT_IMPOSTO                  DATE              NOT NULL,
  VL_BASE_CALCULO             NUMBER(15,2)      NOT NULL,
  VL_IMPOSTO                  NUMBER(15,2)      NOT NULL,
  PR_TRIBUTO                  NUMBER(7,4)       NOT NULL,
  VL_NAO_RETIDO               NUMBER(15,2)      NOT NULL,
  VL_BASE_NAO_RETIDO          NUMBER(15,2)      NOT NULL,
  VL_TRIB_ADIC                NUMBER(15,2)      NOT NULL,
  VL_BASE_ADIC                NUMBER(15,2)      NOT NULL,
  IE_PAGO_PREV                VARCHAR2(1 BYTE)  NOT NULL,
  VL_DESC_BASE                NUMBER(15,2),
  VL_REDUCAO                  NUMBER(15,2),
  CD_DARF                     VARCHAR2(10 BYTE),
  CD_VARIACAO                 VARCHAR2(2 BYTE),
  IE_PERIODICIDADE            VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_REG            NUMBER(10),
  NR_SEQ_TRANS_BAIXA          NUMBER(10),
  IE_TIPO_CONTRATACAO         VARCHAR2(2 BYTE),
  IE_FILANTROPIA              VARCHAR2(3 BYTE),
  NR_SEQ_TRIB_ESTORNADO       NUMBER(10),
  NR_SEQ_LOTE_TRIB_PREST      NUMBER(10),
  CD_BENEFICIARIO             VARCHAR2(14 BYTE),
  NR_LOTE_CONTABIL            NUMBER(10),
  CD_CONTA_DEB                VARCHAR2(20 BYTE),
  CD_CONTA_CRED               VARCHAR2(20 BYTE),
  CD_HISTORICO                NUMBER(10),
  NR_SEQ_ESQUEMA              NUMBER(10),
  CD_CLASSIF_CRED             VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB              VARCHAR2(40 BYTE),
  NM_USUARIO_DIGITACAO        VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_RET_TRIB_VALOR  NUMBER(10),
  NR_SEQ_REGRA                NUMBER(10),
  NR_SEQ_REGRA_CALCULO        NUMBER(10),
  VL_BASE_PRODUCAO            NUMBER(15,2),
  NR_TITULO_PAGAR_RET         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPPVT_CONCONT_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_CONCONT_FK2_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_HISPADR_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_I1 ON TASY.PLS_PAG_PREST_VENC_TRIB
(IE_PAGO_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVT_LOTCONT_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_PESJURI_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(CD_BENEFICIARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPPVT_PK ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_PLSESCO_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_PLSESCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_PLSLRTV_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_LOTE_RET_TRIB_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_PLSLRTV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_PLSPPVE_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_VENCIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVT_REGCAIR_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_REGRA_CALCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVT_TITPAGA_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_TITULO_PAGAR_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVT_TRAFINA_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_TRANS_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_TRAFINA_FK2_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_TRANS_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPPVT_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPPVT_TRIBUTO_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPPVT_TRICOPA_FK_I ON TASY.PLS_PAG_PREST_VENC_TRIB
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_pag_prest_venc_trib_del
before delete ON TASY.PLS_PAG_PREST_VENC_TRIB for each row
declare

begin
if	(:old.nr_seq_lote_ret_trib_valor is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265465, '');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_pag_prest_venc_trib_atual
before insert or update ON TASY.PLS_PAG_PREST_VENC_TRIB for each row
declare
ie_forma_arred_ops_w		varchar2(15);

begin
/*if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	select	nvl(max(ie_forma_arred_ops), 'N')
	into	ie_forma_arred_ops_w
	from	tributo
	where	cd_tributo = :new.cd_tributo;

	if	(ie_forma_arred_ops_w = 'S') then
		if	(:new.vl_base_calculo is not null) and (:new.pr_tributo is not null) and (:new.vl_imposto != 0) and (:new.ie_pago_prev != 'R') then
			begin
			:new.vl_imposto	:= trunc(dividir_sem_round(:new.vl_base_calculo * :new.pr_tributo, 100),2);
			exception
			when others then
				null;
			end;
		end if;
	end if;
end if;*/

null;

end;
/


ALTER TABLE TASY.PLS_PAG_PREST_VENC_TRIB ADD (
  CONSTRAINT PLSPPVT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PAG_PREST_VENC_TRIB ADD (
  CONSTRAINT PLSPPVT_TRICOPA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.TRIBUTO_CONTA_PAGAR (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR_RET) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PLSPPVT_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT PLSPPVT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_REG) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_PLSPPVE_FK 
 FOREIGN KEY (NR_SEQ_VENCIMENTO) 
 REFERENCES TASY.PLS_PAG_PREST_VENCIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_PESJURI_FK 
 FOREIGN KEY (CD_BENEFICIARIO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSPPVT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSPPVT_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSPPVT_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSPPVT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSPPVT_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_PLSLRTV_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RET_TRIB_VALOR) 
 REFERENCES TASY.PLS_LOTE_RET_TRIB_VALOR (NR_SEQUENCIA),
  CONSTRAINT PLSPPVT_REGCAIR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CALCULO) 
 REFERENCES TASY.REGRA_CALCULO_IRPF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PAG_PREST_VENC_TRIB TO NIVEL_1;

