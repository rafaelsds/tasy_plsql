ALTER TABLE TASY.SUS_LAUDO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_LAUDO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_LAUDO_PACIENTE
(
  NR_ATENDIMENTO             NUMBER(10),
  IE_TIPO_LAUDO_SUS          NUMBER(2)          NOT NULL,
  NR_LAUDO_SUS               NUMBER(3)          NOT NULL,
  DT_EMISSAO                 DATE               NOT NULL,
  CD_PROCEDIMENTO_SOLIC      NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  QT_PROCEDIMENTO_SOLIC      NUMBER(5),
  CD_MEDICO_REQUISITANTE     VARCHAR2(10 BYTE),
  CD_MEDICO_RESPONSAVEL      VARCHAR2(10 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_DIAGNOSTICO             DATE,
  DS_MOTIVO_INTERNACAO       VARCHAR2(2000 BYTE),
  DS_MOTIVO_ALTERACAO        VARCHAR2(2000 BYTE),
  NR_AIH                     NUMBER(13),
  DS_SINAL_SINTOMA           VARCHAR2(2000 BYTE),
  DS_CONDICAO_JUSTIFICA      VARCHAR2(2000 BYTE),
  DS_RESULT_PROVA            VARCHAR2(2000 BYTE),
  CD_CID_PRINCIPAL           VARCHAR2(4 BYTE),
  IE_LIFONODOS_REG_INVAL     VARCHAR2(1 BYTE),
  DS_LOCALIZACAO_METASTASE   VARCHAR2(80 BYTE),
  DS_ESTADIO_UICC            VARCHAR2(40 BYTE),
  DS_ESTADIO_OUTRO_SIST      VARCHAR2(20 BYTE),
  CD_GRAU_HISTOPAT           NUMBER(5),
  CD_DIAG_CITO_HIST          VARCHAR2(10 BYTE),
  DT_DIAG_CITO_HIST          DATE,
  IE_TRATAMENTO_ANT          VARCHAR2(1 BYTE),
  DS_TRATAMENTO_ANT          VARCHAR2(255 BYTE),
  DT_PRI_TRATAMENTO          DATE,
  DT_SEG_TRATAMENTO          DATE,
  DT_TER_TRATAMENTO          DATE,
  IE_CONTINUIDADE_TRAT       VARCHAR2(1 BYTE),
  DT_INICIO_TRAT_SOLIC       DATE,
  IE_VIA_TRATAMENTO          VARCHAR2(5 BYTE),
  IE_FINALIDADE              VARCHAR2(3 BYTE),
  DS_SIGLA_ESQUEMA           VARCHAR2(40 BYTE),
  QT_MESES_PREV              NUMBER(3),
  QT_MESES_AUTORIZADO        NUMBER(5),
  CD_TIPO_TRATAMENTO         VARCHAR2(3 BYTE),
  CD_ASSOCIACAO              VARCHAR2(3 BYTE),
  DS_COMPLEMENTO             VARCHAR2(2000 BYTE),
  NR_SEQ_EQUIP               NUMBER(10),
  QT_DOSE_AREA_DIA           NUMBER(7,2),
  QT_CAMPO_AREA_DIA          NUMBER(5),
  QT_TOTAL_DIA_AREA          NUMBER(5),
  QT_CHECK_FILMS             NUMBER(3),
  QT_INSERCOES               NUMBER(3),
  DT_INICIO_TRAT             DATE,
  DT_FIM_TRAT                DATE,
  DS_MARCARA                 VARCHAR2(1 BYTE),
  QT_BLOCO_PRE               NUMBER(3),
  QT_TOTAL_CAMPO_PREV        NUMBER(5),
  QT_CAMPO_PAGO              NUMBER(5),
  NR_SEQ_INTERNO             NUMBER(10)         NOT NULL,
  QT_PREV_MES1               NUMBER(3),
  QT_PREV_MES2               NUMBER(3),
  QT_PREV_MES3               NUMBER(3),
  NR_ATENDIMENTO_ORIGEM      NUMBER(10),
  IE_STATUS_PROCESSO         NUMBER(3),
  IE_CLASSIFICACAO           NUMBER(3),
  NR_INTERNO_CONTA           NUMBER(10),
  NR_SEQ_AIH                 NUMBER(10),
  NR_APAC                    NUMBER(13),
  NR_SEQ_LOTE                NUMBER(10),
  DT_INICIO_VAL_APAC         DATE,
  DT_FIM_VAL_APAC            DATE,
  DT_RETORNO_SECR            DATE,
  DS_INCONSISTENCIA          VARCHAR2(255 BYTE),
  IE_TIPO_LAUDO_APAC         NUMBER(2),
  DS_JUSTIFICATIVA           VARCHAR2(1000 BYTE),
  IE_ORIGEM_LAUDO_APAC       NUMBER(3),
  IE_METASTASE               VARCHAR2(1 BYTE),
  CD_CID_MORFOLOGIA          VARCHAR2(6 BYTE),
  QT_RADIACAO                NUMBER(15,4),
  CD_CID_SECUNDARIO          VARCHAR2(4 BYTE),
  DS_TRATAMENTO_ANT2         VARCHAR2(255 BYTE),
  DS_TRATAMENTO_ANT3         VARCHAR2(255 BYTE),
  DS_HIPOTESE_DIAG           VARCHAR2(2000 BYTE),
  DS_RESUMO_EXAME_FIS        VARCHAR2(2000 BYTE),
  DS_EXAME_RESULTADO         VARCHAR2(2000 BYTE),
  IE_RECIDIVADO              VARCHAR2(1 BYTE),
  IE_VIA_IV                  VARCHAR2(1 BYTE),
  IE_VIA_SC                  VARCHAR2(1 BYTE),
  IE_VIA_IM                  VARCHAR2(1 BYTE),
  IE_VIA_VO                  VARCHAR2(1 BYTE),
  IE_VIA_IT                  VARCHAR2(1 BYTE),
  IE_VIA_IVES                VARCHAR2(1 BYTE),
  IE_VIA_OUTROS              VARCHAR2(1 BYTE),
  IE_TIPO_TUMOR              NUMBER(2),
  IE_URGENTE                 VARCHAR2(1 BYTE),
  QT_MESES_CANC              NUMBER(3),
  QT_MESES_AUTOR_LV          NUMBER(3),
  DT_LIBERACAO               DATE,
  DT_CANCELAMENTO            DATE,
  IE_CLASSIF_HEMOFILIA       VARCHAR2(15 BYTE),
  IE_INIBIDOR                VARCHAR2(15 BYTE),
  IE_GESTANTE                VARCHAR2(1 BYTE),
  IE_TRANSPLANTE             VARCHAR2(1 BYTE),
  QT_TRANSPLANTE             NUMBER(5),
  QT_PESO                    NUMBER(6,3),
  QT_ALTURA                  NUMBER(6,3),
  IE_PROVA_DIAG              VARCHAR2(15 BYTE),
  CD_CID_CAUSA_ASSOC         VARCHAR2(4 BYTE),
  IE_DIARIA_ACOMP            VARCHAR2(1 BYTE)   NOT NULL,
  IE_DIARIA_UTI              VARCHAR2(4 BYTE),
  DS_COMPLEMENTO_LONGO       LONG,
  CD_CID_TOPOGRAFIA          VARCHAR2(10 BYTE),
  DT_PRI_DIALISE             DATE,
  QT_ALTURA_CM               NUMBER(4),
  QT_DIURESE                 NUMBER(4),
  QT_GLICOSE                 NUMBER(4),
  PR_ALBUMINA                NUMBER(4,2),
  PR_HB                      NUMBER(4,2),
  IE_ACESSO_VASCULAR         VARCHAR2(1 BYTE),
  IE_HIV                     VARCHAR2(1 BYTE),
  IE_HCV                     VARCHAR2(1 BYTE),
  IE_HB_SANGUE               VARCHAR2(1 BYTE),
  IE_ULTRA_ABDOMEN           VARCHAR2(1 BYTE),
  IE_INSCRITO_CNCDO          VARCHAR2(1 BYTE),
  NR_TRU                     NUMBER(7,2),
  CD_CID_PRIM_TRAT           VARCHAR2(4 BYTE),
  CD_CID_SEG_TRAT            VARCHAR2(4 BYTE),
  CD_CID_TERC_TRAT           VARCHAR2(4 BYTE),
  QT_NARC_CRIANCA            NUMBER(5),
  QT_SIST_IMOB               NUMBER(5),
  NR_SEQ_PRI_TRAT            NUMBER(10),
  NR_SEQ_SEG_TRAT            NUMBER(10),
  NR_SEQ_TER_TRAT            NUMBER(10),
  DS_OUTROS_TRAT_ANT         VARCHAR2(2000 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_ORDEM_LAUDO             NUMBER(15),
  NR_SEQ_INT_PROT_MEDIC      NUMBER(10),
  DS_ANAMNESE                VARCHAR2(2000 BYTE),
  DS_ALTERACAO_LABORAT       VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  IE_FORMA_TRATAMENTO        VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_FUTURO        NUMBER(10),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  DT_METASTASE               DATE,
  NR_BPA                     NUMBER(13),
  CD_CNPJ_EXECUTOR           VARCHAR2(14 BYTE),
  DT_TRANSPLANTE             DATE,
  NR_SEQ_MOTIVO_NEXEC_PROC   NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SEQ_LOCALIZACAO_LAUDO   NUMBER(10),
  DS_OBSERVACAO_PRI_TRAT     VARCHAR2(2000 BYTE),
  DS_OBSERVACAO_SEG_TRAT     VARCHAR2(2000 BYTE),
  DS_OBSERVACAO_TER_TRAT     VARCHAR2(2000 BYTE),
  NR_PRESCRICAO              NUMBER(15),
  IE_CARATER_INTER_SUS       VARCHAR2(2 BYTE),
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NR_PROTOCOLO_SISREG        VARCHAR2(100 BYTE),
  IE_STATUS_SISREG           VARCHAR2(15 BYTE),
  DT_ENVIO_SISREG            DATE,
  NM_USUARIO_SISREG          VARCHAR2(15 BYTE),
  DT_INICIO_DIALISE_CLI      DATE,
  IE_CARACT_TRATAMENTO       VARCHAR2(1 BYTE),
  IE_ACESSO_VASC_DIAL        VARCHAR2(1 BYTE),
  IE_ACOMP_NEFROL            VARCHAR2(1 BYTE),
  IE_SITUACAO_USU_INI        VARCHAR2(1 BYTE),
  IE_SITUACAO_TRASP          VARCHAR2(1 BYTE),
  IE_DADOS_APTO              VARCHAR2(1 BYTE),
  QT_FOSFORO                 NUMBER(6,2),
  QT_KTV_SEMANAL             NUMBER(6,2),
  QT_PTH                     NUMBER(6,2),
  IE_INTER_CLINICA           VARCHAR2(1 BYTE),
  IE_PERITONITE_DIAG         VARCHAR2(1 BYTE),
  IE_ENCAMINHADO_FAV         VARCHAR2(1 BYTE),
  IE_ENCAM_IMP_CATETER       VARCHAR2(1 BYTE),
  IE_SITUACAO_VACINA         VARCHAR2(1 BYTE),
  IE_ANTI_HBS                VARCHAR2(1 BYTE),
  IE_INFLUENZA               VARCHAR2(1 BYTE),
  IE_DIFTERIA_TETANO         VARCHAR2(1 BYTE),
  IE_PNEUMOCOCICA            VARCHAR2(1 BYTE),
  IE_IECA                    VARCHAR2(1 BYTE),
  IE_BRA                     VARCHAR2(1 BYTE),
  IE_DUPLEX_PREVIO           VARCHAR2(1 BYTE),
  IE_CATETER_OUTROS          VARCHAR2(1 BYTE),
  IE_FAV_PREVIAS             VARCHAR2(1 BYTE),
  IE_FLEBITES                VARCHAR2(1 BYTE),
  IE_HEMATOMAS               VARCHAR2(1 BYTE),
  IE_VEIA_VISIVEL            VARCHAR2(1 BYTE),
  IE_PRESENCA_PULSO          VARCHAR2(1 BYTE),
  QT_DIAMETRO_VEIA           NUMBER(6,2),
  QT_DIAMETRO_ARTERIA        NUMBER(6,2),
  IE_FREMITO_TRAJ_FAV        VARCHAR2(1 BYTE),
  IE_PULSO_FREMITO           VARCHAR2(1 BYTE),
  IE_MOTIVO_SOLICITACAO      VARCHAR2(2 BYTE),
  CD_CNES                    NUMBER(7),
  DT_INTERNACAO              DATE,
  DT_ASSINATURA_LAUDO        DATE,
  IE_CLINICA                 NUMBER(5),
  NR_SEQ_STATUS_LAUDO        NUMBER(10),
  IE_TIPO_INT_PSIQUIATRIA    VARCHAR2(15 BYTE),
  IE_TIPO_LEITO              VARCHAR2(2 BYTE),
  NR_SEQ_PEDIDO              NUMBER(10),
  DT_PRI_AVALIACAO           DATE,
  QT_IMC_PRI_AVALIACAO       NUMBER(3),
  DT_AVALIACAO_ATUAL         DATE,
  IE_PERDA_PESO_PRE_OP       VARCHAR2(1 BYTE),
  IE_AVAL_NUTRICIONISTA      VARCHAR2(1 BYTE),
  IE_AVAL_PSICOLOGO          VARCHAR2(1 BYTE),
  IE_AVAL_MED_CLINICO        VARCHAR2(1 BYTE),
  IE_AVAL_MED_PSIQUIATR      VARCHAR2(1 BYTE),
  IE_AVAL_ENDOCRINO          VARCHAR2(1 BYTE),
  IE_AVAL_CIRUR_DIGESTI      VARCHAR2(1 BYTE),
  IE_AVAL_CIRUR_GERAL        VARCHAR2(1 BYTE),
  IE_GRUPO_MULTIPROFIS       VARCHAR2(1 BYTE),
  IE_AVAL_RISCO_CIRURG       VARCHAR2(1 BYTE),
  IE_REAL_EXAM_LABORAT       VARCHAR2(1 BYTE),
  IE_ESOFAGOGASTRODUO        VARCHAR2(1 BYTE),
  IE_ULTR_ABDOMEN_TOT        VARCHAR2(1 BYTE),
  IE_ECOCARDIO_TRANST        VARCHAR2(1 BYTE),
  IE_ULTRAS_DOPPL_COL        VARCHAR2(1 BYTE),
  IE_PROVA_PULMON_BRO        VARCHAR2(1 BYTE),
  IE_APTO_PROC_CIRURG        VARCHAR2(1 BYTE),
  DT_ENVIO_WEBSERVICE        DATE,
  CD_LAUDO_INTERN_WS         VARCHAR2(100 BYTE),
  TP_SITUACAO                VARCHAR2(10 BYTE),
  NR_SEQ_MOT_CANC_WS         NUMBER(10),
  DS_OBSERV_CANCEL_WS        VARCHAR2(2000 BYTE),
  NR_SEQ_MOT_TROC_WS         NUMBER(10),
  NM_PESSOA_RESP_LME         VARCHAR2(60 BYTE),
  NR_SEQ_MORF_DESC_ADIC      NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  DT_ASSINATURA_INATIVACAO   DATE,
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DT_ASSINATURA              DATE,
  CD_PROFIS_REQUISITANTE     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          44M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSLAPA_ATEPACFU_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_ATEND_FUTURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_ATEPACI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_ATEPACI_FK2_I ON TASY.SUS_LAUDO_PACIENTE
(NR_ATENDIMENTO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_SECUNDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK1_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK2_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_CAUSA_ASSOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK3_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_PRIM_TRAT)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK4_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_SEG_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK5_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_TERC_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDDOEN_FK6_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDDOEN_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDMORF_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CID_MORFOLOGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDMORF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CIDOMOR_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_DIAG_CITO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_CIDOMOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_CONPACI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_DIAMEDI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_ATENDIMENTO, DT_DIAGNOSTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_DIAMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_I1 ON TASY.SUS_LAUDO_PACIENTE
(DT_DIAGNOSTICO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_I1
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_I2 ON TASY.SUS_LAUDO_PACIENTE
(NR_ATENDIMENTO, IE_CLASSIFICACAO, IE_TIPO_LAUDO_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_MEDIREQ_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_MEDICO_REQUISITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_MEDIRES_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_MEDICO_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_MODEADI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_MORF_DESC_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_PEDEXEX_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_PEDIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_PERFIL_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_PESFISI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_PESFISI_FK2_I ON TASY.SUS_LAUDO_PACIENTE
(CD_PROFIS_REQUISITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_PESJURI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_CNPJ_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUSLAPA_PK ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_PRESMED_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_PROCEDI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_PROINTE_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_RXTEQUI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_RXTEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_SMMPLWS_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_MOT_TROC_WS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_SUSAIH_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_SUSLOAU_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_SUSLOLA_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_LOCALIZACAO_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_SUSLOLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_SUSMCRL_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_MOT_CANC_WS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_SUSMNEL_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_MOTIVO_NEXEC_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_SUSMNEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_SUSSTLA_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_STATUS_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_SUSTRAN_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_PRI_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_SUSTRAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_SUSTRAN_FK2_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_SEG_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_SUSTRAN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_SUSTRAN_FK3_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_TER_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLAPA_SUSTRAN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLAPA_TASASDI_FK_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLAPA_TASASDI_FK_2_I ON TASY.SUS_LAUDO_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSLAPA_UK ON TASY.SUS_LAUDO_PACIENTE
(NR_ATENDIMENTO, IE_TIPO_LAUDO_SUS, NR_LAUDO_SUS, NR_SEQ_ATEND_FUTURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.sus_laudo_paciente_aftins
after insert ON TASY.SUS_LAUDO_PACIENTE for each row
declare

cd_cgc_prestador_w		varchar2(14);
cd_setor_atendimento_w		number(5);
dt_entrada_unidade_w		date;
nr_seq_atepacu_w			number(10);
cd_medico_responsavel_w		varchar2(10);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
dt_periodo_inicial_w		date;
dt_entrada_w			date;
dt_alta_w				date;
nr_seq_propaci_w			number(10);
cd_estabelecimento_w		number(4);
nr_interno_conta_w			number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
qt_procedimento_w			number(5);
qt_regra_laudo_w			number(5);
ie_proc_laudo_w			varchar2(15) := 'N';


begin

select	count(*)
into	qt_regra_laudo_w
from	sus_regra_gerar_proc_laudo;

if	(qt_regra_laudo_w > 0) then

	begin
	select	nvl(sus_obter_se_gerar_proc_laudo(:new.cd_procedimento_solic,:new.ie_origem_proced,:new.ie_classificacao),'N')
	into	ie_proc_laudo_w
	from 	dual;
	exception
	when others then
		ie_proc_laudo_w := 'N';
	end;

else
	ie_proc_laudo_w := 'N';
end if;

if	(ie_proc_laudo_w = 'S') then
	begin

	begin
	select	a.cd_convenio,
		a.cd_categoria,
		b.cd_estabelecimento,
		b.dt_entrada,
		b.dt_alta
	into	cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w,
		dt_entrada_w,
		dt_alta_w
	from	atend_categoria_convenio a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.nr_atendimento = :new.nr_atendimento
	and	rownum = 1;

	select	max(cd_cgc)
	into	cd_cgc_prestador_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	exception
	when others then
		cd_convenio_w		:= null;
		cd_categoria_w		:= null;
		cd_estabelecimento_w	:= null;
		dt_entrada_w		:= null;
		dt_alta_w 		:= null;
		cd_cgc_prestador_w	:= null;
	end;

	begin
	select	a.cd_setor_atendimento,
		a.dt_entrada_unidade,
		a.nr_seq_interno
	into	cd_setor_atendimento_w,
		dt_entrada_unidade_w,
		nr_seq_atepacu_w
	from	atend_paciente_unidade a
	where	a.nr_atendimento	= :new.nr_atendimento
	and	a.dt_entrada_unidade 	= (	select max(x.dt_entrada_unidade)
						from atend_paciente_unidade x
						where x.nr_atendimento = a.nr_atendimento);
	exception
	when others then
		cd_setor_atendimento_w	:= null;
		dt_entrada_unidade_w	:= null;
		nr_seq_atepacu_w	:= null;
	end;

	if	(nr_seq_atepacu_w is not null)  then

		select	procedimento_paciente_seq.nextval
		into	nr_seq_propaci_w
		from	dual;

		begin
		insert into procedimento_paciente(
			nr_sequencia, nr_atendimento, dt_entrada_unidade, cd_procedimento,
			dt_procedimento, qt_procedimento, dt_atualizacao, nm_usuario,
			cd_medico, cd_convenio, cd_categoria, cd_pessoa_fisica,
			dt_prescricao, ds_observacao, vl_procedimento, 	vl_medico,
			vl_anestesista, vl_materiais, cd_edicao_amb, cd_tabela_servico,
			dt_vigencia_preco, cd_procedimento_princ, dt_procedimento_princ, dt_acerto_conta,
			dt_acerto_convenio, dt_acerto_medico, vl_auxiliares, vl_custo_operacional,
			tx_medico, tx_anestesia, nr_prescricao, nr_sequencia_prescricao,
			cd_motivo_exc_conta, ds_compl_motivo_excon, cd_acao, qt_devolvida,
			cd_motivo_devolucao, nr_cirurgia, nr_doc_convenio, cd_medico_executor,
			ie_cobra_pf_pj, nr_laudo, dt_conta, cd_setor_atendimento,
			cd_conta_contabil, cd_procedimento_aih, ie_origem_proced, nr_aih,
			ie_responsavel_credito, tx_procedimento, cd_equipamento, ie_valor_informado,
			cd_estabelecimento_custo, cd_tabela_custo, cd_situacao_glosa, nr_lote_contabil,
			cd_procedimento_convenio, nr_seq_autorizacao, 	ie_tipo_servico_sus, ie_tipo_ato_sus,
			cd_cgc_prestador, nr_nf_prestador, cd_atividade_prof_bpa, nr_interno_conta,
			nr_seq_proc_princ, ie_guia_informada, dt_inicio_procedimento, ie_emite_conta,
			ie_funcao_medico, ie_classif_sus, 	cd_especialidade, nm_usuario_original,
			nr_seq_proc_pacote, ie_tipo_proc_sus, cd_setor_receita, vl_adic_plant,
			nr_seq_atepacu, ie_auditoria)
		values	(nr_seq_propaci_w, :new.nr_atendimento,dt_entrada_unidade_w,:new.cd_procedimento_solic,
			dt_entrada_unidade_w, nvl(:new.qt_procedimento_solic,1), sysdate, :new.nm_usuario,
			null, cd_convenio_w, cd_categoria_w, null,
			null,'Regra geracao proc laudo', 0, 0,
			0, 0,null,null,
			null, null, null, null,
			null, null, 0, 0,
			1, 1, null, null,
			null, null, null, null,
			null, null, null, coalesce(:new.cd_medico_requisitante,:new.cd_profis_requisitante),
			null, null, null, cd_setor_atendimento_w,
			null, null, :new.ie_origem_proced, null,
			null, 100, null, 'N',
			cd_estabelecimento_w, null, null, null,
			null, null, null, null,
			cd_cgc_prestador_w, null, null, null,
			null, null, null, null,
			null, null, null, null,
			null, null, cd_setor_atendimento_w, 0,
			nr_seq_atepacu_w,null);

		atualiza_preco_procedimento(nr_seq_propaci_w,cd_convenio_w,:new.nm_usuario);


		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(188082,'SQL_ERRO='||substr(sqlerrm,1,255));
			/* Ocorreu o seguinte problema ao inserir registro na procedimento paciente:#@SQL_ERRO#@ */
		end;

	end if;

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sus_laudo_paciente_insert
before insert ON TASY.SUS_LAUDO_PACIENTE for each row
declare

ie_diagnostico_w			varchar2(1) := 'N';
cd_doenca_cid_w			varchar2(10);
cd_cid_secundario_w		varchar2(10);
nr_interno_conta_w			number(10);
qt_meses_val_laudo_w		number(3);
cd_estabelecimento_w		number(4);
ie_proc_sus_antigo_w		varchar2(1);
ie_gerar_diag_w			varchar2(1);
ie_gerar_diag_atend_atual_w		varchar2(1);
cd_cid_atualizar_w			varchar2(10);
cd_estab_ativo_w			number(5);
nm_usuario_ativo_w		varchar2(20);
ie_dt_emissao_dt_inicial_w		varchar2(15) := 'N';
ie_tipo_laudo_apac_w		sus_procedimento.ie_tipo_laudo_apac%type;
cd_procedimento_solic_w 		sus_laudo_paciente.cd_procedimento_solic%type;
ie_origem_proced_w			sus_laudo_paciente.ie_origem_proced%type;

begin

cd_estab_ativo_w		:= wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_ativo_w	:= wheb_usuario_pck.get_nm_usuario;

obter_param_usuario(916,653,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_w);
obter_param_usuario(916,654,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_atend_atual_w);
obter_param_usuario(1124,76,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_dt_emissao_dt_inicial_w);

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

select	nvl(max(ie_proc_sus_antigo),'N')
into	ie_proc_sus_antigo_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_proc_sus_antigo_w = 'S') and
	(:new.ie_origem_proced in (2,3)) then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(187969);
	/* Este procedimento n�o pode ser lan�ado pois � de origem antiga do SUS. */
	end;
end if;

select	nvl(max('S'),'N')
into	ie_diagnostico_w
from	diagnostico_medico
where	nr_atendimento 	= :new.nr_atendimento
  and	ie_tipo_diagnostico 	= 2;

if	(:new.dt_fim_val_apac is null) then
	select	nvl(max(b.qt_meses_val_laudo - 1),0)
	into	qt_meses_val_laudo_w
	from	parametro_medico b,
		atendimento_paciente a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.nr_atendimento	= :new.nr_atendimento;

	if	(qt_meses_val_laudo_w	> 0) then
		if	(:new.dt_inicio_val_apac is null) then
			begin
			if	(ie_dt_emissao_dt_inicial_w = 'S') then
				:new.dt_inicio_val_apac 	:= :new.dt_emissao;
			else
				:new.dt_inicio_val_apac	:= sysdate;
			end if;
			end;
		end if;
		if	(:new.dt_fim_val_apac is null) then
			:new.dt_fim_val_apac	:= last_day(add_months(:new.dt_emissao,qt_meses_val_laudo_w));
		end if;
	end if;
end if;

if	(obter_funcao_ativa = 916)
and	(ie_gerar_diag_atend_atual_w = 'S') then
	select	max(cd_doenca)
	into	cd_cid_atualizar_w
	from	diagnostico_doenca
	where	nr_atendimento 	= :new.nr_atendimento
	and	dt_diagnostico 	= (	select	min(dt_diagnostico)
					from	diagnostico_doenca
					where	nr_atendimento = :new.nr_atendimento);

	if	(cd_cid_atualizar_w is not null) then
		:new.cd_cid_principal := cd_cid_atualizar_w;
	end if;
end if;

if	(:new.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is null) then
	begin
	:new.cd_pessoa_fisica	:= obter_dados_atendimento(:new.nr_atendimento,'CP');
	end;
end if;

if	(:new.ie_tipo_laudo_sus = 0) and
	(:new.cd_procedimento_solic is not null) then

	select	max(cd_doenca_cid),
		max(cd_cid_secundario)
	into	cd_doenca_cid_w,
		cd_cid_secundario_w
	from 	procedimento
	where 	cd_procedimento 	= :new.cd_procedimento_solic
	and	ie_origem_proced	= :new.ie_origem_proced;

	if	((obter_funcao_ativa <> 916) and
		 (ie_diagnostico_w	= 'N'))
	or	((obter_funcao_ativa = 916) and
		 (ie_gerar_diag_w = 'S')) then
		begin
		insert into diagnostico_medico(
			nr_atendimento, dt_diagnostico, ie_tipo_diagnostico,
			cd_medico, dt_atualizacao, nm_usuario, ds_diagnostico)
		values(
			:new.nr_atendimento, :new.dt_emissao, 2,
			:new.cd_medico_requisitante, sysdate, :new.nm_usuario, null);
		if	(cd_doenca_cid_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_doenca_cid_w,
				sysdate, :new.nm_usuario, null, 'P', 2);
		end if;
		if	(cd_cid_secundario_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO
)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_cid_secundario_w, sysdate,  :new.nm_usuario, null, 'S', 2);
		end if;
		exception
			when others then
				ie_diagnostico_w := 'S';
		end;
	end if;
end if;

/*if	(:new.nr_aih <> nvl(:old.nr_aih,0)) then
	select	nvl(max(nr_interno_conta),null)
	into	nr_interno_conta_w
	from	sus_aih
	where	nr_aih = :new.nr_aih;

	:new.nr_interno_conta := nr_interno_conta_w;
end if;*/

if	((:new.nr_seq_aih is null) or
	(:new.nr_seq_aih = 0)) and
	(:new.nr_aih is not null) then
	begin
	select	nr_sequencia
	into	:new.nr_seq_aih
	from	sus_aih
	where	nr_aih	= :new.nr_aih;
	exception
	when others then
		:new.nr_seq_aih := :new.nr_seq_aih;
	end;
end if;

if	(:new.nr_apac is not null) then
	:new.ie_status_processo	:= 2;
end if;

if	(:new.ie_classificacao = 13) and
	(sus_obter_tiporeg_proc(:new.cd_procedimento_solic,:new.ie_origem_proced,'C',12) = 2) then
	:new.ie_classificacao := 15;
end if;

if	(nvl(:new.ie_tipo_laudo_apac,0) = 0) and
	(nvl(:new.ie_classificacao,0) in (11,12,13,14)) then
	begin

	select	nvl(max(ie_tipo_laudo_apac),'0')
	into	ie_tipo_laudo_apac_w
 	from	sus_procedimento
	where	ie_origem_proced = :new.ie_origem_proced
	and	cd_procedimento = :new.cd_procedimento_solic;

	if	(nvl(ie_tipo_laudo_apac_w,'0') > '0') then
		begin
		if	(ie_tipo_laudo_apac_w = '01') then
			:new.ie_tipo_laudo_apac := 12;
		elsif	(ie_tipo_laudo_apac_w = '02') then
			:new.ie_tipo_laudo_apac := 8;
		elsif	(ie_tipo_laudo_apac_w = '03') then
			:new.ie_tipo_laudo_apac := 1;
		elsif	(ie_tipo_laudo_apac_w = '04') then
			:new.ie_tipo_laudo_apac := 2;
		elsif	(ie_tipo_laudo_apac_w = '06') then
			:new.ie_tipo_laudo_apac := 12;
		elsif	(ie_tipo_laudo_apac_w = '07') then
			:new.ie_tipo_laudo_apac := 22;
		elsif	(ie_tipo_laudo_apac_w = '10') then
			:new.ie_tipo_laudo_apac := 8;
		elsif	(ie_tipo_laudo_apac_w = '12') then
			:new.ie_tipo_laudo_apac := 19;
		end if;
		end;
	end if;
	end;
end if;

if 	(:new.cd_procedimento_solic is null) and
	(:new.nr_seq_proc_interno is not null) then
	obter_proc_tab_interno(:new.nr_seq_proc_interno, null, :new.nr_atendimento, null, cd_procedimento_solic_w, ie_origem_proced_w);
	if 	(cd_procedimento_solic_w is not null) then
		:new.cd_procedimento_solic := cd_procedimento_solic_w;
		:new.ie_origem_proced := ie_origem_proced_w;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SUS_LAUDO_PACIENTE_tp  after update ON TASY.SUS_LAUDO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_INTERNO);  ds_c_w:=null; ds_w:=substr(:new.DS_ESTADIO_OUTRO_SIST,1,500);gravar_log_alteracao(substr(:old.DS_ESTADIO_OUTRO_SIST,1,4000),substr(:new.DS_ESTADIO_OUTRO_SIST,1,4000),:new.nm_usuario,nr_seq_w,'DS_ESTADIO_OUTRO_SIST',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_LOCALIZACAO_METASTASE,1,500);gravar_log_alteracao(substr(:old.DS_LOCALIZACAO_METASTASE,1,4000),substr(:new.DS_LOCALIZACAO_METASTASE,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOCALIZACAO_METASTASE',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ESTADIO_UICC,1,500);gravar_log_alteracao(substr(:old.DS_ESTADIO_UICC,1,4000),substr(:new.DS_ESTADIO_UICC,1,4000),:new.nm_usuario,nr_seq_w,'DS_ESTADIO_UICC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_RESULT_PROVA,1,500);gravar_log_alteracao(substr(:old.DS_RESULT_PROVA,1,4000),substr(:new.DS_RESULT_PROVA,1,4000),:new.nm_usuario,nr_seq_w,'DS_RESULT_PROVA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CONDICAO_JUSTIFICA,1,500);gravar_log_alteracao(substr(:old.DS_CONDICAO_JUSTIFICA,1,4000),substr(:new.DS_CONDICAO_JUSTIFICA,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONDICAO_JUSTIFICA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SINAL_SINTOMA,1,500);gravar_log_alteracao(substr(:old.DS_SINAL_SINTOMA,1,4000),substr(:new.DS_SINAL_SINTOMA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SINAL_SINTOMA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AIH,1,4000),substr(:new.NR_AIH,1,4000),:new.nm_usuario,nr_seq_w,'NR_AIH',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EMISSAO,1,4000),substr(:new.DT_EMISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EMISSAO',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_REQUISITANTE,1,4000),substr(:new.CD_MEDICO_REQUISITANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_REQUISITANTE',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CID_PRINCIPAL,1,4000),substr(:new.CD_CID_PRINCIPAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CID_PRINCIPAL',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIFONODOS_REG_INVAL,1,4000),substr(:new.IE_LIFONODOS_REG_INVAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIFONODOS_REG_INVAL',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRAU_HISTOPAT,1,4000),substr(:new.CD_GRAU_HISTOPAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRAU_HISTOPAT',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DIAG_CITO_HIST,1,4000),substr(:new.DT_DIAG_CITO_HIST,1,4000),:new.nm_usuario,nr_seq_w,'DT_DIAG_CITO_HIST',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DIAG_CITO_HIST,1,4000),substr(:new.CD_DIAG_CITO_HIST,1,4000),:new.nm_usuario,nr_seq_w,'CD_DIAG_CITO_HIST',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRATAMENTO_ANT,1,4000),substr(:new.IE_TRATAMENTO_ANT,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRATAMENTO_ANT',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INTERNO_CONTA,1,4000),substr(:new.NR_INTERNO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_INTERNO_CONTA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VAL_APAC,1,4000),substr(:new.DT_INICIO_VAL_APAC,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VAL_APAC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VAL_APAC,1,4000),substr(:new.DT_FIM_VAL_APAC,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VAL_APAC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_RETORNO_SECR,1,4000),substr(:new.DT_RETORNO_SECR,1,4000),:new.nm_usuario,nr_seq_w,'DT_RETORNO_SECR',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_APAC,1,4000),substr(:new.NR_APAC,1,4000),:new.nm_usuario,nr_seq_w,'NR_APAC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOTE,1,4000),substr(:new.NR_SEQ_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_METASTASE,1,4000),substr(:new.IE_METASTASE,1,4000),:new.nm_usuario,nr_seq_w,'IE_METASTASE',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CID_SECUNDARIO,1,4000),substr(:new.CD_CID_SECUNDARIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CID_SECUNDARIO',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECIDIVADO,1,4000),substr(:new.IE_RECIDIVADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECIDIVADO',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CID_TOPOGRAFIA,1,4000),substr(:new.CD_CID_TOPOGRAFIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CID_TOPOGRAFIA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MESES_AUTOR_LV,1,4000),substr(:new.QT_MESES_AUTOR_LV,1,4000),:new.nm_usuario,nr_seq_w,'QT_MESES_AUTOR_LV',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DIAGNOSTICO,1,4000),substr(:new.DT_DIAGNOSTICO,1,4000),:new.nm_usuario,nr_seq_w,'DT_DIAGNOSTICO',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_RESPONSAVEL,1,4000),substr(:new.CD_MEDICO_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_RESPONSAVEL',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PROCEDIMENTO_SOLIC,1,4000),substr(:new.QT_PROCEDIMENTO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'QT_PROCEDIMENTO_SOLIC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO_SOLIC,1,4000),substr(:new.CD_PROCEDIMENTO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO_SOLIC',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_BPA,1,4000),substr(:new.NR_BPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_BPA',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LAUDO_SUS,1,4000),substr(:new.NR_LAUDO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_LAUDO_SUS',ie_log_w,ds_w,'SUS_LAUDO_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.GERINT_sus_laudo_paciente
after insert or update ON TASY.SUS_LAUDO_PACIENTE for each row
declare

nr_seq_forma_chegada_w			atendimento_paciente.NR_SEQ_FORMA_CHEGADA%type;
frequencia_cardiaca_w			atendimento_sinal_vital.qt_freq_cardiaca%type;
frequencia_respiratoria_w		atendimento_sinal_vital.qt_freq_resp%type;
pressao_arterial_maxima_w		atendimento_sinal_vital.qt_pa_sistolica%type;
pressao_arterial_minima_w		atendimento_sinal_vital.qt_pa_diastolica%type;
temperatura_w					atendimento_sinal_vital.qt_temp%type;
saturacao_o2_w					atendimento_sinal_vital.qt_saturacao_o2%type;
ie_nivel_consciencia_w			atendimento_sinal_vital.ie_nivel_consciencia%type;
fluxo_w							atendimento_monit_resp.qt_fluxo_insp%type;
fiO2_w							atendimento_monit_resp.qt_fio2%type;
satO2_w							atendimento_monit_resp.qt_saturacao_o2%type;
peep_w							atendimento_monit_resp.qt_peep%type;
ie_disp_resp_esp_w				atendimento_monit_resp.ie_disp_resp_esp%type;
tipo_internacao_w 				conversao_meio_externo.cd_externo%type;
tipo_acesso_w					conversao_meio_externo.cd_externo%type;
tipo_leito_w					conversao_meio_externo.cd_externo%type;
sensorio_w						conversao_meio_externo.cd_externo%type;
suporteO2_w						conversao_meio_externo.cd_externo%type;
cartao_sus_w 					pessoa_fisica.nr_cartao_nac_sus%type;
cpf_profissional_solicitante_w	pessoa_fisica.nr_cpf%type;
tipo_protocolo_origem_w			varchar2(255);
numero_protocolo_origem_w		varchar2(12);
cor_w							varchar2(255);
debito_urinario_w				varchar2(60);
dialise_w						varchar2(3);
internacao_propria_w			varchar2(3);
ie_regulacao_gerint_w			parametro_atendimento.ie_regulacao_gerint%type;

ie_condicao_w					gerint_solic_internacao.ie_condicao%type;
ie_carater_inter_sus_origem_w	atendimento_paciente.ie_carater_inter_sus%type;

nr_seq_solic_internacao_w		GERINT_SOLIC_INTERNACAO.nr_sequencia%type;
ds_sep_bv_w						varchar2(50);

--variaveis do evento autom�tico para integra��o
seq_gerint_evento_w		GERINT_EVENTO_INTEGRACAO.nr_sequencia%type;
nr_cpf_paciente_w		pessoa_fisica.nr_cpf%type;
begin

	--Verifica se utiliza regula��o de leitos para o estado do Rio Grande do Sul.
	select	nvl(max(ie_regulacao_gerint),'N')
	into	ie_regulacao_gerint_w
	from	parametro_atendimento
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if (((:old.dt_liberacao is null) and (:new.dt_liberacao is not null)) and (ie_regulacao_gerint_w = 'S') and (:new.ie_classificacao = 1)) then

		--Obter valores da ATENDIMENTO_PACIENTE
		select	max(NR_SEQ_FORMA_CHEGADA),
				max(IE_CARATER_INTER_SUS)
		into	nr_seq_forma_chegada_w,
				ie_carater_inter_sus_origem_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		--Obter valores da tabela ATENDIMENTO_SINAL_VITAL
		select	max(qt_freq_cardiaca),
				max(qt_freq_resp),
				max(qt_pa_sistolica),
				max(qt_pa_diastolica),
				max(qt_temp),
				max(qt_saturacao_o2),
				max(ie_nivel_consciencia)
		into	frequencia_cardiaca_w,
				frequencia_respiratoria_w,
				pressao_arterial_maxima_w,
				pressao_arterial_minima_w,
				temperatura_w,
				saturacao_o2_w,
				ie_nivel_consciencia_w
		from	atendimento_sinal_vital
		where	nr_sequencia = (select 	max(nr_sequencia)
								from	atendimento_sinal_vital
								where	nr_atendimento = :new.nr_atendimento
								and		dt_liberacao is not null
								and		dt_inativacao is null);

		--Obter valores da tabela ATENDIMENTO_MONIT_RESP
		select	max(qt_fluxo_insp),
				max(qt_fio2),
				max(qt_saturacao_o2),
				max(qt_peep),
				max(ie_disp_resp_esp)
		into	fluxo_w,
				fiO2_w,
				satO2_w,
				peep_w,
				ie_disp_resp_esp_w
		from	atendimento_monit_resp
		where	nr_sequencia = (select 	max(nr_sequencia)
								from	atendimento_monit_resp
								where	nr_atendimento = :new.nr_atendimento
								and		dt_liberacao is not null
								and		dt_inativacao is null);

		--Obter valores da tabela HD_PRESCRICAO
		SELECT 	DECODE(COUNT(*),0,'N','S')
		into	dialise_w
		FROM   	prescr_medica p,
				hd_prescricao h
		WHERE	p.nr_prescricao = h.nr_prescricao
		and		p.nr_atendimento = :new.nr_atendimento
		and		dt_liberacao is not null
		and		dt_suspensao is null;

		--Obter valores da convers�o meio externo.
		tipo_internacao_w 	:= obter_conversao_externa_int(null, 'GERINT_SOLIC_INTERNACAO',	'IE_CARATER_INTER_SUS', nvl(:new.ie_carater_inter_sus,ie_carater_inter_sus_origem_w), 	'GERINT');
		tipo_acesso_w		:= obter_conversao_externa_int(null, 'GERINT_SOLIC_INTERNACAO',	'NR_SEQ_FORMA_CHEGADA', nr_seq_forma_chegada_w, 	'GERINT');
		tipo_leito_w		:= obter_conversao_externa_int(null, 'GERINT_SOLIC_INTERNACAO',	'IE_TIPO_LEITO', 		:new.ie_tipo_leito, 		'GERINT');
		sensorio_w			:= obter_conversao_externa_int(null, 'GERINT_SOLIC_INTERNACAO',	'IE_NIVEL_CONSCIENCIA', ie_nivel_consciencia_w, 	'GERINT');
		suporteO2_w			:= obter_conversao_externa_int(null, 'GERINT_SOLIC_INTERNACAO',	'IE_DISP_RESP_ESP', 	ie_disp_resp_esp_w, 		'GERINT');

		--Obter valores de function
		cartao_sus_w 					:= substr(obter_dados_pf(:new.cd_pessoa_fisica,'CNS'),1,15);
		cpf_profissional_solicitante_w 	:= obter_dados_pf(:new.cd_medico_requisitante,'CPF');
		tipo_protocolo_origem_w 		:= '';
		numero_protocolo_origem_w 		:= '';
		cor_w 							:= ''; --N�o ser� informado;
		debito_urinario_w 				:= 'NAO_AVALIADO';
		internacao_propria_w			:= 'S'; -- sempre que gerado pela Laudo SUS, vai gerar como interna��o pr�pria;

		if (cartao_sus_w is null) and (cpf_profissional_solicitante_w is null) then
			ie_condicao_w := 'P';
		end if;

		select 	gerint_solic_internacao_seq.nextval
		into	nr_seq_solic_internacao_w
		from 	dual;

		insert into GERINT_SOLIC_INTERNACAO (
											nr_sequencia,
											cd_estabelecimento,
											dt_atualizacao,
											nm_usuario,
											dt_atualizacao_nrec,
											nm_usuario_nrec,
											cd_pessoa_fisica,
											nr_cartao_sus,
											ds_tipo_internacao,
											ie_protocolo_origem,
											nr_protocolo_origem,
											nr_seq_forma_chegada,
											ds_tipo_acesso,
											ie_internacao_propria,
											ie_tipo_leito,
											ds_tipo_leito,
											cd_cid_principal,
											cd_medico_requisitante,
											nr_cpf_medico_req,
											ds_sinal_sintoma,
											ds_condicao_justifica,
											ds_cor,
											qt_freq_cardiaca,
											qt_freq_resp,
											qt_pa_sistolica,
											qt_pa_diastolica,
											qt_temp,
											qt_saturacao_o2,
											ie_nivel_consciencia,
											ds_debito_urinario,
											cd_procedimento,
											ie_origem_proced,
											ie_dialise,
											ie_disp_resp_esp,
											qt_fluxo_insp,
											qt_fio2,
											qt_sat_o2,
											qt_peep,
											ie_situacao,
											nr_atendimento_origem,
											ie_carater_inter_sus,
											nr_cpf_paciente,
											nm_pessoa_fisica,
											ie_condicao,
											ie_sexo,
											qt_idade,
											ds_endereco_pf,
											ds_municipio_pf,
											CD_MUNICIPIO_IBGE
										) values (
											nr_seq_solic_internacao_w,
											wheb_usuario_pck.get_cd_estabelecimento,
											sysdate,
											wheb_usuario_pck.get_nm_usuario,
											sysdate,
											wheb_usuario_pck.get_nm_usuario,
											nvl(:new.cd_pessoa_fisica, obter_pessoa_atendimento(:new.nr_atendimento_origem,'C')),
											cartao_sus_w,
											tipo_internacao_w,
											tipo_protocolo_origem_w,
											numero_protocolo_origem_w,
											nr_seq_forma_chegada_w,
											tipo_acesso_w,
											internacao_propria_w,
											:new.ie_tipo_leito,
											tipo_leito_w,
											:new.cd_cid_principal,
											:new.cd_medico_requisitante,
											cpf_profissional_solicitante_w,
											substr(:new.ds_sinal_sintoma,1,2000),
											substr(:new.ds_condicao_justifica,1,2000),
											cor_w,
											frequencia_cardiaca_w,
											frequencia_respiratoria_w,
											pressao_arterial_maxima_w,
											pressao_arterial_minima_w,
											temperatura_w,
											saturacao_o2_w,
											sensorio_w,
											debito_urinario_w,
											:new.cd_procedimento_solic,
											:new.ie_origem_proced,
											dialise_w,
											suporteO2_w,
											fluxo_w,
											fiO2_w,
											satO2_w,
											peep_w,
											'E',
											:new.nr_atendimento_origem,
											nvl(:new.ie_carater_inter_sus,ie_carater_inter_sus_origem_w),
											obter_dados_pf(:new.cd_pessoa_fisica,'CPF'),
											obter_nome_pf(:new.cd_pessoa_fisica),
											ie_condicao_w,
											obter_dados_pf(:new.cd_pessoa_fisica,'SE'),
											obter_dados_pf(:new.cd_pessoa_fisica,'I'),
											obter_endereco_pf(:new.cd_pessoa_fisica,'END'),
											obter_municipio_pf(:new.cd_pessoa_fisica),
											substr(OBTER_DADOS_COMPL_PF(:new.cd_pessoa_fisica,'M'),1,6));

		--Dispara o evento automaticamente para integra��o.
		select	GERINT_EVENTO_INTEGRACAO_seq.nextval
		into	seq_gerint_evento_w
		from	dual;

		nr_cpf_paciente_w := obter_dados_pf(:new.cd_pessoa_fisica,'CPF');

		insert into GERINT_EVENTO_INTEGRACAO(	NR_SEQUENCIA,
												NM_USUARIO,
												NM_PESSOA_FISICA,
												IE_SITUACAO,
												ID_EVENTO,
												DT_ATUALIZACAO_NREC,
												DT_ATUALIZACAO,
												CD_ESTABELECIMENTO,
												CD_PESSOA_FISICA,
												NR_SEQ_SOLIC_INTER
											) values (
												seq_gerint_evento_w,
												wheb_usuario_pck.get_nm_usuario,
												obter_nome_pf(:new.cd_pessoa_fisica),
												'N',
												9,
												sysdate,
												sysdate,
												wheb_usuario_pck.get_cd_estabelecimento,
												:new.cd_pessoa_fisica,
												nr_seq_solic_internacao_w);

		insert into GERINT_EVENTO_INT_DADOS(	NR_SEQ_EVENTO,
												NR_CARTAO_SUS,
												DS_TIPO_INTERNACAO,
												DS_TIPO_PROTOCOLO_ORIGEM,
												NR_PROTOCOLO_ORIGEM,
												DS_TIPO_ACESSO,
												IE_INTERNACAO_PROPRIA,
												DS_TIPO_LEITO,
												CD_CID_PRINCIPAL,
												NR_CPF_PROF_SOLICITANTE,
												DS_SINAIS_SINTOMAS,
												DS_JUSTIFICATIVA_INTERNACAO,
												DS_COR,
												QT_FREQ_CARDIACA,
												QT_FREQ_RESPIRATORIA,
												QT_PRESSAO_ART_MAXIMA,
												QT_PRESSAO_ART_MINIMA,
												QT_TEMPERATURA,
												QT_SATURACAO_O2,
												DS_NIVEL_CONSCIENCIA,
												DS_DEBITO_URINARIO,
												CD_PROCEDIMENTO,
												IE_DIALISE,
												DS_SUPORTE_O2,
												QT_FLUXO,
												QT_FIO2,
												QT_SAT_O2,
												QT_PEEP,
												NR_CPF_PACIENTE,
												DS_CONDICAO,
												NM_PESSOA_FISICA,
												DS_SEXO_PF,
												QT_IDADE_PF,
												DS_ENDERECO_PF,
												DS_MUNICIPIO_RESIDENCIA_PF,
												IE_TIPO_LEITO,
												CD_MUNICIPIO_IBGE
											)
											select
												seq_gerint_evento_w,
												cartao_sus_w,
												obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','IE_CARATER_INTER_SUS',nvl(:new.ie_carater_inter_sus,ie_carater_inter_sus_origem_w),'GERINT'),
												Gerint_desc_de_para(tipo_protocolo_origem_w),
												numero_protocolo_origem_w,
												Obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','NR_SEQ_FORMA_CHEGADA',nr_seq_forma_chegada_w,'GERINT'),
												internacao_propria_w,
												obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','IE_TIPO_LEITO',:new.ie_tipo_leito,'GERINT'),
												:new.cd_cid_principal,
												cpf_profissional_solicitante_w,
												substr(:new.ds_sinal_sintoma,1,2000),
												substr(:new.ds_condicao_justifica,1,2000),
												cor_w,
												frequencia_cardiaca_w,
												frequencia_respiratoria_w,
												pressao_arterial_maxima_w,
												pressao_arterial_minima_w,
												temperatura_w,
												saturacao_o2_w,
												obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','IE_NIVEL_CONSCIENCIA',sensorio_w,'GERINT'),
												debito_urinario_w,
												:new.cd_procedimento_solic,
												dialise_w,
												obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','IE_DISP_RESP_ESP',suporteO2_w,'GERINT'),
												fluxo_w,
												fiO2_w,
												satO2_w,
												peep_w,
												decode(cartao_sus_w,null,nr_cpf_paciente_w,null), --n�o enviar se possuir CNS
												decode(cartao_sus_w,null,obter_conversao_externa_int(null,'GERINT_SOLIC_INTERNACAO','IE_CONDICAO',ie_condicao_w,'GERINT'),null), --enviar somente se n�o possuir CNS
												decode(ie_condicao_w,'R',obter_nome_pf(:new.cd_pessoa_fisica),decode(cartao_sus_w,null,decode(nr_cpf_paciente_w,null,obter_nome_pf(:new.cd_pessoa_fisica),null),null)), --n�o enviar se possuir CNS/CPF
												decode(cartao_sus_w,null,decode(nr_cpf_paciente_w,null,decode(obter_dados_pf(:new.cd_pessoa_fisica,'SE'),'F','FEMININO','M','MASCULINO', null),null),null), --n�o enviar se possuir CNS/CPF
												decode(cartao_sus_w,null,decode(nr_cpf_paciente_w,null,obter_dados_pf(:new.cd_pessoa_fisica,'I'),null),null), --n�o enviar se possuir CNS/CPF
												decode(cartao_sus_w,null,decode(nr_cpf_paciente_w,null,obter_endereco_pf(:new.cd_pessoa_fisica,'END'),null),null), --n�o enviar se possuir CNS/CPF
												decode(cartao_sus_w,null,decode(nr_cpf_paciente_w,null,obter_municipio_pf(:new.cd_pessoa_fisica),null),null), --n�o enviar se possuir CNS/CPF
												:new.IE_TIPO_LEITO,
												substr(OBTER_DADOS_COMPL_PF(:new.cd_pessoa_fisica,'M'),1,6)
											from dual;

		update	GERINT_SOLIC_INTERNACAO
		set		IE_SITUACAO	 		= 'O',
				DT_GERACAO_EVENTO	= sysdate,
				NM_USUARIO 			= wheb_usuario_pck.get_nm_usuario,
				DT_ATUALIZACAO 		= sysdate
		where	nr_sequencia 		= nr_seq_solic_internacao_w;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_gest_aut_sus_laudo_pac_upd
before update ON TASY.SUS_LAUDO_PACIENTE for each row
declare

nm_usuario_w varchar2(15);

begin
        
        
    if obter_funcao_ativa = 1006 then
        :new.dt_atualizacao := :old.dt_atualizacao;
        :new.nm_usuario := :old.nm_usuario;
    end if;
    
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_sus_laudo_paciente_insupd
after insert or update ON TASY.SUS_LAUDO_PACIENTE for each row
declare

nm_usuario_w varchar2(15);

begin
 
        DELETE FROM PEP_ITEM_PENDENTE
        WHERE IE_TIPO_REGISTRO = 'HSJL'
        AND NR_SEQ_REGISTRO = :new.nr_seq_interno
        AND NVL(IE_TIPO_PENDENCIA,'L') = 'L';
    
        if(:new.dt_liberacao is null)then

            nm_usuario_w := substr(obter_usuario_pessoa(:new.cd_medico_requisitante),1,15);

            INSERT INTO PEP_ITEM_PENDENTE
            (
                NR_SEQUENCIA,
                DT_ATUALIZACAO,
                NM_USUARIO,
                DT_ATUALIZACAO_NREC,
                NM_USUARIO_NREC,
                DT_REGISTRO,
                CD_PESSOA_FISICA,
                NR_ATENDIMENTO,
                NR_SEQ_REGISTRO,
                IE_TIPO_REGISTRO,
                NR_SEQ_ITEM_PRONT,
                IE_TIPO_PENDENCIA,
                IE_PENDENTE_ASSINAT_USUARIO,
                IE_GERADO_FUNCAO_EHR,
                IE_GERADO_FUNCAO_PEPO
            )
            VALUES(
                PEP_ITEM_PENDENTE_SEQ.NEXTVAL,
                SYSDATE,
                nm_usuario_w,
                SYSDATE,
                nm_usuario_w,
                SYSDATE,
                nvl(:new.cd_pessoa_fisica, obter_dados_atendimento(:new.nr_atendimento, 'CP')),
                :new.nr_atendimento,
                :new.nr_seq_interno,
                'HSJL',
                365,
                'L',
                'N',
                'N',
                'N'
            );
            
        end if;
       
end;
/


CREATE OR REPLACE TRIGGER TASY.sus_laudo_paciente_update
before update ON TASY.SUS_LAUDO_PACIENTE for each row
declare

Cursor C01 is
	select	nr_sequencia
	from	sus_apac_unif
	where	cd_procedimento = :new.cd_procedimento_solic
	and	dt_inicio_validade = :new.dt_inicio_val_apac
	and	nr_atendimento = :new.nr_atendimento;

ie_diagnostico_w			varchar2(1 char)	:= 'N';
cd_doenca_cid_w			procedimento.cd_doenca_cid%type;
cd_cid_secundario_w		procedimento.cd_cid_secundario%type;
--nr_interno_conta_w			number(10);
ie_permite_w			varchar2(1 char)	:= 'S';
cd_estabelecimento_w		number(4);
ie_proc_sus_antigo_w		parametro_faturamento.ie_proc_sus_antigo%type;
ie_gerar_diag_w			varchar2(1 char);
ie_gerar_diag_atend_atual_w		varchar2(1 char);
cd_cid_atualizar_w			diagnostico_doenca.cd_doenca%type;
cd_estab_ativo_w			number(5);
nm_usuario_ativo_w		varchar2(20 char);
ie_atualizar_periodo_apac_w		varchar2(15 char)	:= 'N';
nr_seq_apac_w			C01%rowtype;
qt_meses_val_laudo_w		number(10);
dt_inicio_val_apac_w		date;
dt_fim_val_apac_w			date;
cd_procedimento_solic_w		sus_laudo_paciente.cd_procedimento_solic%type;
ie_origem_proced_w		sus_laudo_paciente.ie_origem_proced%type;
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_activated_w			varchar2(1);
qt_laudo_medicamento_w		number(5);
ds_retorno_integracao_w		varchar2(4000);

begin

begin
select	cd_estabelecimento
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;
exception
when others then
	cd_estabelecimento_w := 0;
end;

select	nvl(max(ie_proc_sus_antigo),'N')
into	ie_proc_sus_antigo_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_proc_sus_antigo_w = 'S') and
	(:old.ie_origem_proced not in (2,3)) and (:new.ie_origem_proced in (2,3)) then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(186393);
	/* Este procedimento nao pode ser lancado pois e de origem antiga do SUS. */
	end;
end if;

cd_estab_ativo_w   := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_ativo_w := wheb_usuario_pck.get_nm_usuario;

obter_param_usuario(916,653,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_w);
obter_param_usuario(916,654,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_gerar_diag_atend_atual_w);
obter_param_usuario(1124,123,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_atualizar_periodo_apac_w);
obter_param_usuario(9041,10,obter_perfil_ativo,nm_usuario_ativo_w,cd_estab_ativo_w,ie_activated_w);

If	(Obter_funcao_ativa = 916)
And	(ie_gerar_diag_atend_Atual_w = 'S') then
	select	max(b.cd_doenca)
	into	cd_cid_atualizar_w
	from	diagnostico_doenca b
	where	b.nr_atendimento = :new.nr_atendimento
	and	b.dt_diagnostico = (	select	min(a.dt_diagnostico)
					from	diagnostico_doenca a
					where	a.nr_atendimento = :new.nr_Atendimento);

	If	(cd_cid_atualizar_w is not null) then
		:new.cd_cid_principal := cd_cid_atualizar_w;
	End if;
End if;

if	(:new.ie_tipo_laudo_sus = 0) and
	(:new.cd_procedimento_solic is not null) then

	select	max(cd_doenca_cid),
		max(cd_cid_secundario)
	into	cd_doenca_cid_w,
		cd_cid_secundario_w
	from 	procedimento
	where 	cd_procedimento 	= :new.cd_procedimento_solic
	and	ie_origem_proced	= :new.ie_origem_proced;

	If	((Obter_funcao_ativa <> 916) and
		 (ie_diagnostico_w	= 'N'))
	or	((Obter_funcao_ativa = 916) and
		 (ie_gerar_diag_w = 'S')) then
		begin
		insert into diagnostico_medico(
			nr_atendimento, dt_diagnostico, ie_tipo_diagnostico,
			cd_medico, dt_atualizacao, nm_usuario, ds_diagnostico)
		values(
			:new.nr_atendimento, :new.dt_emissao, 2,
			:new.cd_medico_requisitante, sysdate, :new.nm_usuario, null);
		if	(cd_doenca_cid_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_doenca_cid_w,
				sysdate, :new.nm_usuario, null, 'P', 2);
		end if;
		if	(cd_cid_secundario_w is not null) then
			insert into diagnostico_doenca(
				nr_atendimento, dt_diagnostico, cd_doenca, dt_atualizacao,
				nm_usuario, ds_diagnostico, ie_classificacao_doenca, IE_TIPO_DIAGNOSTICO)
			values(
				:new.nr_atendimento, :new.dt_emissao, cd_cid_secundario_w, sysdate,  :new.nm_usuario, null, 'S', 2);
		end if;
		exception
			when others then
				ie_diagnostico_w := 'S';
		end;
	End if;
end if;

/*if	(nvl(:new.nr_aih,0) <> nvl(:old.nr_aih,1)) then
	select	nvl(max(nr_interno_conta),null)
	into	nr_interno_conta_w
	from	sus_aih
	where	nr_aih = nvl(:new.nr_aih,0);

	:new.nr_interno_conta := nr_interno_conta_w;
end if;*/

if	((:new.nr_seq_aih is null) or
	(:new.nr_seq_aih = 0)) and
	(:new.nr_aih is not null) then
	begin
	select	nr_sequencia
	into	:new.nr_seq_aih
	from	sus_aih
	where	nr_aih	= :new.nr_aih;
	exception
	when others then
		:new.nr_seq_aih := :new.nr_seq_aih;
	end;
end if;

if	(:new.nr_apac is not null) and
	(:old.nr_apac is null) then
	begin
	:new.ie_status_processo	:= 2;

	reg_integracao_p.nr_atendimento		:=	:new.nr_atendimento;

	gerar_int_padrao.gravar_integracao(ie_evento_p => '395',nr_seq_documento_p => :new.nr_seq_interno, nm_usuario_p => :new.nm_usuario,reg_integracao_p => reg_integracao_p);

	if	(ie_activated_w = 'S') then
		begin

		begin
		select	count(1)
		into	qt_laudo_medicamento_w
		from	sus_laudo_medicamento
		where	nr_seq_laudo_sus = :new.nr_seq_interno;
		exception
		when no_data_found then
			qt_laudo_medicamento_w := 0;
		end;

		if	(nvl(qt_laudo_medicamento_w,0) > 0) then
			begin

			select	bifrost.send_integration(
			'authorizationManagement.send.request',
			'com.philips.tasy.integration.sus.authorizationmanagementsus.outbound.AuthorizationManagementSusCallback',
			'{"encounter" : '||:new.nr_atendimento||', "internalSequenceReport": ' || :new.nr_seq_interno || ', "event": 395, "userName": "' || :new.nm_usuario || '" }',
			:new.nm_usuario)
			into	ds_retorno_integracao_w
			from	dual;

			end;
		end if;

		end;
	end if;

	end;
end if;

select	Obter_Se_solic_laudo(:new.nr_atendimento,:new.ie_classificacao,:new.cd_medico_requisitante)
into	ie_permite_w
from	dual;

if	(ie_permite_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(186392);
	/* O medico requisitante nao tem permissao para gerar laudo.
	Verificar regra solicitantes de laudos nos Cadastros gerais/Aplicacao principal/Cadastros SUS. */
end if;

if	(nvl(ie_atualizar_periodo_apac_w,'N') = 'S') and
	(:new.dt_emissao is not null) and
	(nvl(:old.dt_emissao,:new.dt_emissao) <> :new.dt_emissao) then
	begin

	select	nvl(max(b.qt_meses_val_laudo - 1),0)
	into	qt_meses_val_laudo_w
	from	parametro_medico b,
		atendimento_paciente a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	and	a.nr_atendimento	= :new.nr_atendimento;

	dt_inicio_val_apac_w	:= :new.dt_emissao;

	if	(qt_meses_val_laudo_w	> 0) then
		dt_fim_val_apac_w	:= last_day(add_months(:new.dt_emissao,qt_meses_val_laudo_w));
	end if;


    for nr_seq_apac_w in C01 loop
			begin

            update 	sus_apac_unif
            set 	dt_inicio_validade	= nvl(dt_inicio_val_apac_w,dt_inicio_validade),
                dt_fim_validade	= nvl(dt_fim_val_apac_w,dt_fim_validade)
            where 	nr_sequencia	= nr_seq_apac_w.nr_sequencia;

            end;
	end loop;


	:new.dt_inicio_val_apac	:= dt_inicio_val_apac_w;
	:new.dt_fim_val_apac	:= dt_fim_val_apac_w;

	end;
end if;

if 	(:new.nr_seq_proc_interno is not null) then
	begin
	if	((:new.cd_procedimento_solic is null) or
		(:new.nr_seq_proc_interno <> :old.nr_seq_proc_interno)) then
		begin
		obter_proc_tab_interno(:new.nr_seq_proc_interno, null, :new.nr_atendimento, null, cd_procedimento_solic_w, ie_origem_proced_w);
		if	(cd_procedimento_solic_w is not null) then
			:new.cd_procedimento_solic := cd_procedimento_solic_w;
			:new.ie_origem_proced := ie_origem_proced_w;
		end if;
		end;
	end if;
	end;
end if;

if	(:old.ie_classificacao = 15) and
	(:new.ie_classificacao <> 15) and
	(sus_obter_tiporeg_proc(:new.cd_procedimento_solic,:new.ie_origem_proced,'C',12) = 2) then
	:new.ie_classificacao := :old.ie_classificacao;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SUS_LAUDO_PACIENTE_BEFINS
BEFORE INSERT ON TASY.SUS_LAUDO_PACIENTE FOR EACH ROW
DECLARE

ie_permite_w		Varchar2(1) := 'S';
ds_retorno_w		varchar2(255)	:= '';
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

BEGIN

if	(:new.nr_seq_interno is null) then
	select	sus_laudo_paciente_seq.nextVal
	into	:new.nr_seq_interno
	from	dual;
end if;

select	Obter_Se_solic_laudo(:new.nr_atendimento,:new.ie_classificacao,coalesce(:new.cd_medico_requisitante,:new.cd_profis_requisitante))
into	ie_permite_w
from	dual;

if	(ie_permite_w = 'N') then
	--'O medico requisitante nao tem permissao para gerar laudo. Verificar regra solicitantes de laudos nos Cadastros gerais/Aplicacao principal/Cadastros SUS.');
	wheb_mensagem_pck.exibir_mensagem_abort(263527);
end if;

begin
cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estabelecimento_w := 0;
end;

sus_consiste_regra_laudo(cd_estabelecimento_w, :new.cd_procedimento_solic, :new.ie_origem_proced, ds_retorno_w);

if	(nvl(ds_retorno_w,'X') <> 'X') then
	ds_retorno_w := substr(substr(ds_retorno_w,1,220) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(454285),1,255);
	Wheb_mensagem_pck.exibir_mensagem_abort(262328 , 'DS_MENSAGEM='||ds_retorno_w);
end if;

END;
/


ALTER TABLE TASY.SUS_LAUDO_PACIENTE ADD (
  CONSTRAINT SUSLAPA_PK
 PRIMARY KEY
 (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT SUSLAPA_UK
 UNIQUE (NR_ATENDIMENTO, IE_TIPO_LAUDO_SUS, NR_LAUDO_SUS, NR_SEQ_ATEND_FUTURO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_LAUDO_PACIENTE ADD (
  CONSTRAINT SUSLAPA_SUSMCRL_FK 
 FOREIGN KEY (NR_SEQ_MOT_CANC_WS) 
 REFERENCES TASY.SUS_MOT_CAN_REJ_LAUDO_WS (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_MODEADI_FK 
 FOREIGN KEY (NR_SEQ_MORF_DESC_ADIC) 
 REFERENCES TASY.CIDO_MORFOLOGIA_DESC_ADIC (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_TASASDI_FK_2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFIS_REQUISITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT SUSLAPA_PEDEXEX_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO) 
 REFERENCES TASY.PEDIDO_EXAME_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT SUSLAPA_SUSSTLA_FK 
 FOREIGN KEY (NR_SEQ_STATUS_LAUDO) 
 REFERENCES TASY.SUS_STATUS_LAUDO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SUSLAPA_MEDIREQ_FK 
 FOREIGN KEY (CD_MEDICO_REQUISITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSLAPA_MEDIRES_FK 
 FOREIGN KEY (CD_MEDICO_RESPONSAVEL) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSLAPA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSLAPA_CIDDOEN_FK1 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDOMOR_FK 
 FOREIGN KEY (CD_DIAG_CITO_HIST) 
 REFERENCES TASY.CIDO_MORFOLOGIA (CD_MORFOLOGIA),
  CONSTRAINT SUSLAPA_RXTEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIP) 
 REFERENCES TASY.RXT_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_ATEPACI_FK2 
 FOREIGN KEY (NR_ATENDIMENTO_ORIGEM) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SUSLAPA_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT SUSLAPA_SUSLOAU_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.SUS_LOTE_AUTOR (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_CIDMORF_FK 
 FOREIGN KEY (CD_CID_MORFOLOGIA) 
 REFERENCES TASY.CID_MORFOLOGIA (CD_CID_MORFOLOGIA),
  CONSTRAINT SUSLAPA_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_SECUNDARIO) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDDOEN_FK2 
 FOREIGN KEY (CD_CID_CAUSA_ASSOC) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDDOEN_FK6 
 FOREIGN KEY (CD_CID_TOPOGRAFIA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDDOEN_FK3 
 FOREIGN KEY (CD_CID_PRIM_TRAT) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDDOEN_FK4 
 FOREIGN KEY (CD_CID_SEG_TRAT) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_CIDDOEN_FK5 
 FOREIGN KEY (CD_CID_TERC_TRAT) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSLAPA_SUSTRAN_FK 
 FOREIGN KEY (NR_SEQ_PRI_TRAT) 
 REFERENCES TASY.SUS_TRAT_ANTERIOR (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_SUSTRAN_FK2 
 FOREIGN KEY (NR_SEQ_SEG_TRAT) 
 REFERENCES TASY.SUS_TRAT_ANTERIOR (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_SUSTRAN_FK3 
 FOREIGN KEY (NR_SEQ_TER_TRAT) 
 REFERENCES TASY.SUS_TRAT_ANTERIOR (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_EXECUTOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SUSLAPA_SUSMNEL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_NEXEC_PROC) 
 REFERENCES TASY.SUS_MOTIVO_NEXEC_LAUDO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_SUSLOLA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO_LAUDO) 
 REFERENCES TASY.SUS_LOCALIZACAO_LAUDO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT SUSLAPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSLAPA_ATEPACFU_FK 
 FOREIGN KEY (NR_SEQ_ATEND_FUTURO) 
 REFERENCES TASY.ATEND_PAC_FUTURO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT SUSLAPA_SMMPLWS_FK 
 FOREIGN KEY (NR_SEQ_MOT_TROC_WS) 
 REFERENCES TASY.SUS_MOT_MUD_PROC_LAUDO_WS (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUS_LAUDO_PACIENTE TO NIVEL_1;

