ALTER TABLE TASY.USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO
(
  NM_USUARIO                    VARCHAR2(15 BYTE),
  DS_USUARIO                    VARCHAR2(100 BYTE) NOT NULL,
  DS_SENHA                      VARCHAR2(64 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO_ATUAL              VARCHAR2(15 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO          NUMBER(5),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  CD_BARRAS                     VARCHAR2(40 BYTE),
  DT_ALTERACAO_SENHA            DATE,
  DS_EMAIL                      VARCHAR2(255 BYTE),
  NR_SEQUENCIA                  NUMBER(10),
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  NR_RAMAL                      VARCHAR2(20 BYTE),
  IE_MENSAGEM_REC               VARCHAR2(1 BYTE),
  IE_MENSAGEM_ENVIO             VARCHAR2(1 BYTE),
  QT_DIA_SENHA                  NUMBER(5)       NOT NULL,
  DS_CONTA_EMAIL                VARCHAR2(50 BYTE),
  IE_ANEXAR_ARQUIVO             VARCHAR2(1 BYTE) NOT NULL,
  DS_SENHA_LAUDO                VARCHAR2(15 BYTE),
  IE_COMUNIC_INTERNA            VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_EVOLUCAO              VARCHAR2(3 BYTE),
  IE_LOCALIZAR_MEDICO           VARCHAR2(1 BYTE),
  CD_FUNCAO                     NUMBER(5),
  IE_VERSAO_ANTERIOR            VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_CLASSIF                NUMBER(10),
  IE_CHAMAR_TASYMON             VARCHAR2(1 BYTE),
  CD_PERFIL_INICIAL             NUMBER(5),
  IE_EVENTO_AGENDA              VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_SAC                 VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_PROCESSO            VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_PRESCR              VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_PROC_AGENDA         VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_COMUNIC             VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_ALERTA              VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_APROV_COMPRA        VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_EXAME_URG           VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_APROV_DOC           VARCHAR2(1 BYTE) NOT NULL,
  IE_FECHAR_TASYMON             VARCHAR2(1 BYTE) NOT NULL,
  DS_SENHA_EMAIL                VARCHAR2(100 BYTE),
  IE_EVENTO_LIB_TELEFONE        VARCHAR2(1 BYTE) NOT NULL,
  NM_USUARIO_ORIG               VARCHAR2(15 BYTE),
  IE_MOSTRAR_ANEXO_COMUNIC      VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_ORDEM_SERV          VARCHAR2(1 BYTE) NOT NULL,
  IE_EVENTO_OBJ_INV             VARCHAR2(1 BYTE) NOT NULL,
  DS_COMUNIC_PADRAO             VARCHAR2(4000 BYTE),
  IE_EVENTO_RECOLETA            VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_IMPRESSORA             NUMBER(10),
  IE_ATUALIZAR_DIC              VARCHAR2(1 BYTE),
  CD_CLASSIF_LOCALIZADOR        VARCHAR2(2 BYTE),
  NR_SEQ_PERFIL                 NUMBER(10),
  DT_VALIDADE_USUARIO           DATE,
  NR_RAMAL_TEMP                 VARCHAR2(8 BYTE),
  IE_PROFISSIONAL               VARCHAR2(15 BYTE),
  QT_ACESSO_INVALIDO            NUMBER(10),
  IE_OS_PE_BSC                  VARCHAR2(15 BYTE),
  DS_LOGIN                      VARCHAR2(50 BYTE),
  IE_EVENTO_SUSP_EXAME          VARCHAR2(1 BYTE),
  IE_EVENTO_MED_CIH             VARCHAR2(1 BYTE),
  IE_EVENTO_SOLIC_PRONT         VARCHAR2(1 BYTE),
  DS_HIST_PADRAO                VARCHAR2(4000 BYTE),
  IE_EVENTO_INT_AGENDA_SAN      VARCHAR2(1 BYTE),
  IE_EVENTO_VINC_AGENDA_SAN     VARCHAR2(1 BYTE),
  IE_EVENTO_CANC_CIR_SAN        VARCHAR2(1 BYTE),
  IE_EVENTO_CANC_AGENDA_SAN     VARCHAR2(1 BYTE),
  IE_EVENTO_TRANSF_AGENDA_SAN   VARCHAR2(1 BYTE),
  IE_EVENTO_ENC_AGENDA_SAN      VARCHAR2(1 BYTE),
  IE_EVENTO_ALTERACAO_PARAM     VARCHAR2(1 BYTE),
  CD_CERTIFICADO                VARCHAR2(100 BYTE),
  NR_SEQ_JUSTIFIC_ACESSO        NUMBER(10),
  IE_EVENTO_PACIENTE            VARCHAR2(1 BYTE),
  DS_TEXTO_EMAIL                VARCHAR2(4000 BYTE),
  IE_EVENTO_ESTRANGEIRO         VARCHAR2(1 BYTE),
  IE_EVENTO_ALTA_PARTIC         VARCHAR2(1 BYTE),
  IE_EVENTO_ALERTA_MEDICO       VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_RECEBER_COPIA_EMAIL        VARCHAR2(1 BYTE),
  IE_EVENTO_PREV_ALTA           VARCHAR2(1 BYTE),
  IE_EVENTO_ALTA_PARTIC_EXT     VARCHAR2(1 BYTE),
  IE_EVENTO_ALTA_REGRA          VARCHAR2(1 BYTE),
  NR_SEQ_IDIOMA                 NUMBER(10),
  NM_USUARIO_PESQUISA           VARCHAR2(15 BYTE) NOT NULL,
  IE_SAIDA_LOGOFF               VARCHAR2(1 BYTE),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE),
  IE_EVENTO_OS_PROGRAMAR        VARCHAR2(1 BYTE),
  IE_DIETA_PRESCRITA            VARCHAR2(1 BYTE),
  NR_NIVEL_APROVACAO            NUMBER(10),
  IE_EVENTO_AGENDA_COMERCIAL    VARCHAR2(1 BYTE),
  IE_EVENTO_REQUISICAO_LIB      VARCHAR2(1 BYTE),
  IE_EVENTO_ALT_STATUS_SAN      VARCHAR2(1 BYTE),
  NR_SEQ_APRES_PROF             NUMBER(3),
  IE_OCORRENCIA_RETORNO         VARCHAR2(1 BYTE),
  IE_EVENTO_SOLIC_COMPRA        VARCHAR2(1 BYTE),
  IE_TERCEIRO                   VARCHAR2(2 BYTE),
  IE_ALERTA_FORMA_ENTREGA       VARCHAR2(1 BYTE),
  DT_VALIDADE_CERTIFICADO       DATE,
  IE_EVENTO_HEMO                VARCHAR2(1 BYTE),
  IE_EVENTO_SC_URGENTE          VARCHAR2(1 BYTE),
  IE_EVENTO_AGENDA_CARATER      VARCHAR2(1 BYTE),
  DT_EMISSAO_CERTIFICADO        DATE,
  IE_RESP_AUDITORIA             VARCHAR2(1 BYTE),
  IE_EVENTO_LIB_RECEB_PP        VARCHAR2(1 BYTE),
  IE_RENOVACAO_CERTIFICADO      VARCHAR2(3 BYTE),
  IE_EVENTO_HIGIENIZACAO_LEITO  VARCHAR2(1 BYTE),
  CD_BARRAS_NUMERICO            NUMBER(38),
  DT_INATIVACAO                 DATE,
  NR_SEQ_ORDEM                  NUMBER(10),
  IE_EVENTO_RETORNO_LIGACAO     VARCHAR2(1 BYTE),
  DS_TEC                        VARCHAR2(15 BYTE),
  IE_EVENTO_VAGA_PEND_APROV     VARCHAR2(1 BYTE),
  IE_TERMO_USO                  VARCHAR2(1 BYTE),
  NM_USUARIO_GIT                VARCHAR2(50 BYTE),
  NM_USUARIO_GITHUB             VARCHAR2(50 BYTE),
  DS_UTC                        VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO              VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO            VARCHAR2(50 BYTE),
  DT_ACEITE_TERMO_USO           DATE,
  IE_CAN_VIEW_SENSITIVE_INFO    VARCHAR2(1 BYTE) DEFAULT null,
  IE_CAN_VIEW_PATIENT_INFO      VARCHAR2(1 BYTE) DEFAULT null,
  USER_2FA_STATUS               VARCHAR2(1 BYTE),
  USER_2FA_SECRET               VARCHAR2(1000 BYTE),
  IE_CAN_DELETE_PATIENT         VARCHAR2(1 BYTE) DEFAULT null,
  IE_CAN_AUTH_PATIENT_DELETION  VARCHAR2(1 BYTE) DEFAULT null,
  IE_IMPLANTADOR                VARCHAR2(1 BYTE),
  IE_SENHA_CASE_SENSITIVE       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.USUARIO.NM_USUARIO IS 'Nome Abreviado do Usuario';

COMMENT ON COLUMN TASY.USUARIO.DS_USUARIO IS 'Nome Completo do Usuario';

COMMENT ON COLUMN TASY.USUARIO.DS_SENHA IS 'Senha do Usuario';

COMMENT ON COLUMN TASY.USUARIO.IE_SITUACAO IS 'Situacao do Usuario';

COMMENT ON COLUMN TASY.USUARIO.DT_ATUALIZACAO IS 'Data de Atualizacao';

COMMENT ON COLUMN TASY.USUARIO.NM_USUARIO_ATUAL IS 'Nome do Usuario';


CREATE INDEX TASY.USUARIO_COMINCL_FK_I ON TASY.USUARIO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_COMINCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUARIO_ESTABEL_FK_I ON TASY.USUARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_FUNCAO_FK_I ON TASY.USUARIO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_I1 ON TASY.USUARIO
(CD_BARRAS, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUARIO_I2 ON TASY.USUARIO
(NM_USUARIO_PESQUISA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_I3 ON TASY.USUARIO
(IE_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_I4 ON TASY.USUARIO
(UPPER("NM_USUARIO"), IE_CHAMAR_TASYMON)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_I5 ON TASY.USUARIO
("TASY"."CAMPO_NUMERICO_PARAMETRO"("CD_BARRAS",'999999999999999999999999999D9999'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_JUACPEP_FK_I ON TASY.USUARIO
(NR_SEQ_JUSTIFIC_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_JUACPEP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUARIO_MANORSE_FK_I ON TASY.USUARIO
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_PEPPEUS_FK_I ON TASY.USUARIO
(NR_SEQ_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_PEPPEUS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUARIO_PERFIL_FK_I ON TASY.USUARIO
(CD_PERFIL_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUARIO_PESFISI_FK_I ON TASY.USUARIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUARIO_PK ON TASY.USUARIO
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_SETATEN_FK_I ON TASY.USUARIO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUARIO_SETIMPR_FK_I ON TASY.USUARIO
(NR_SEQ_IMPRESSORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_SETIMPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUARIO_TASYIDI_FK_I ON TASY.USUARIO
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUARIO_TASYIDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.USUARIO_UK ON TASY.USUARIO
(CD_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUARIO_UK2 ON TASY.USUARIO
(DS_LOGIN)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.USUARIO_AFTUPD
AFTER UPDATE OF IE_TIPO_EVOLUCAO,
                DS_LOGIN,
                DS_EMAIL,
                NM_USUARIO,
                IE_SITUACAO ON TASY.USUARIO FOR EACH ROW
DECLARE
  REG_INTEGRACAO_P GERAR_INT_PADRAO.REG_INTEGRACAO;
BEGIN
  IF ((:NEW.CD_PESSOA_FISICA IS NOT NULL)
  AND (((:NEW.IE_TIPO_EVOLUCAO IS NOT NULL AND :OLD.IE_TIPO_EVOLUCAO IS NOT NULL
  AND :NEW.IE_TIPO_EVOLUCAO <> :OLD.IE_TIPO_EVOLUCAO)
  OR (:OLD.IE_TIPO_EVOLUCAO IS NULL AND :NEW.IE_TIPO_EVOLUCAO IS NOT NULL)
  OR (:OLD.IE_TIPO_EVOLUCAO IS NOT NULL AND :NEW.IE_TIPO_EVOLUCAO IS NULL))

  OR (nvl(:new.cd_pessoa_fisica, '-1') <> nvl(:old.cd_pessoa_fisica, '-1'))

  OR ((:NEW.DS_LOGIN IS NOT NULL AND :OLD.DS_LOGIN IS NOT NULL
  AND :NEW.DS_LOGIN <> :OLD.DS_LOGIN)
  OR (:OLD.DS_LOGIN IS NULL AND :NEW.DS_LOGIN IS NOT NULL)
  OR (:OLD.DS_LOGIN IS NOT NULL AND :NEW.DS_LOGIN IS NULL))

  OR ((:NEW.DS_EMAIL IS NOT NULL AND :OLD.DS_EMAIL IS NOT NULL
  AND :NEW.DS_EMAIL <> :OLD.DS_EMAIL)
  OR (:OLD.DS_EMAIL IS NULL AND :NEW.DS_EMAIL IS NOT NULL)
  OR (:OLD.DS_EMAIL IS NOT NULL AND :NEW.DS_EMAIL IS NULL))

  OR ((:NEW.NM_USUARIO IS NOT NULL AND :OLD.NM_USUARIO IS NOT NULL
  AND :NEW.NM_USUARIO <> :OLD.NM_USUARIO)
  OR (:OLD.NM_USUARIO IS NULL AND :NEW.NM_USUARIO IS NOT NULL)
  OR (:OLD.NM_USUARIO IS NOT NULL AND :NEW.NM_USUARIO IS NULL))

  OR ((:NEW.IE_SITUACAO IS NOT NULL AND :OLD.IE_SITUACAO IS NOT NULL
  AND :NEW.IE_SITUACAO <> :OLD.IE_SITUACAO)
  OR (:OLD.IE_SITUACAO IS NULL AND :NEW.IE_SITUACAO IS NOT NULL)
  OR (:OLD.IE_SITUACAO IS NOT NULL AND :NEW.IE_SITUACAO IS NULL)))) THEN
	GERAR_INT_PADRAO.GRAVAR_INTEGRACAO('313',
                                       :NEW.CD_PESSOA_FISICA,
                                       :NEW.NM_USUARIO_ATUAL,
                                       REG_INTEGRACAO_P);
  END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.USUARIO_tp  after update ON TASY.USUARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NM_USUARIO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_IMPRESSORA,1,4000),substr(:new.NR_SEQ_IMPRESSORA,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_SEQ_IMPRESSORA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIF_LOCALIZADOR,1,4000),substr(:new.CD_CLASSIF_LOCALIZADOR,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_CLASSIF_LOCALIZADOR',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario_atual,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MENSAGEM_ENVIO,1,4000),substr(:new.IE_MENSAGEM_ENVIO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_MENSAGEM_ENVIO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ATUAL,1,4000),substr(:new.NM_USUARIO_ATUAL,1,4000),:new.nm_usuario_atual,nr_seq_w,'NM_USUARIO_ATUAL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ALTERACAO_SENHA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ALTERACAO_SENHA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario_atual,nr_seq_w,'DT_ALTERACAO_SENHA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONTA_EMAIL,1,4000),substr(:new.DS_CONTA_EMAIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_CONTA_EMAIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_USUARIO,1,4000),substr(:new.DS_USUARIO,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_USUARIO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario_atual,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL,1,4000),substr(:new.NR_RAMAL,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_RAMAL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_BARRAS,1,4000),substr(:new.CD_BARRAS,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_BARRAS',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MENSAGEM_REC,1,4000),substr(:new.IE_MENSAGEM_REC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_MENSAGEM_REC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANEXAR_ARQUIVO,1,4000),substr(:new.IE_ANEXAR_ARQUIVO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_ANEXAR_ARQUIVO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_SENHA,1,4000),substr(:new.QT_DIA_SENHA,1,4000),:new.nm_usuario_atual,nr_seq_w,'QT_DIA_SENHA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_LAUDO,1,4000),substr(:new.DS_SENHA_LAUDO,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_SENHA_LAUDO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMUNIC_INTERNA,1,4000),substr(:new.IE_COMUNIC_INTERNA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_COMUNIC_INTERNA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOCALIZAR_MEDICO,1,4000),substr(:new.IE_LOCALIZAR_MEDICO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_LOCALIZAR_MEDICO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_EVOLUCAO,1,4000),substr(:new.IE_TIPO_EVOLUCAO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_TIPO_EVOLUCAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CHAMAR_TASYMON,1,4000),substr(:new.IE_CHAMAR_TASYMON,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_CHAMAR_TASYMON',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_ANTERIOR,1,4000),substr(:new.IE_VERSAO_ANTERIOR,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_VERSAO_ANTERIOR',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_EXAME_URG,1,4000),substr(:new.IE_EVENTO_EXAME_URG,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_EXAME_URG',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL_INICIAL,1,4000),substr(:new.CD_PERFIL_INICIAL,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_PERFIL_INICIAL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_AGENDA,1,4000),substr(:new.IE_EVENTO_AGENDA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_AGENDA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_SAC,1,4000),substr(:new.IE_EVENTO_SAC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_SAC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_PROCESSO,1,4000),substr(:new.IE_EVENTO_PROCESSO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_PROCESSO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_PRESCR,1,4000),substr(:new.IE_EVENTO_PRESCR,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_PRESCR',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_PROC_AGENDA,1,4000),substr(:new.IE_EVENTO_PROC_AGENDA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_PROC_AGENDA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_COMUNIC,1,4000),substr(:new.IE_EVENTO_COMUNIC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_COMUNIC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALERTA,1,4000),substr(:new.IE_EVENTO_ALERTA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALERTA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_APROV_COMPRA,1,4000),substr(:new.IE_EVENTO_APROV_COMPRA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_APROV_COMPRA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FECHAR_TASYMON,1,4000),substr(:new.IE_FECHAR_TASYMON,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_FECHAR_TASYMON',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_LIB_TELEFONE,1,4000),substr(:new.IE_EVENTO_LIB_TELEFONE,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_LIB_TELEFONE',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_APROV_DOC,1,4000),substr(:new.IE_EVENTO_APROV_DOC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_APROV_DOC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ORIG,1,4000),substr(:new.NM_USUARIO_ORIG,1,4000),:new.nm_usuario_atual,nr_seq_w,'NM_USUARIO_ORIG',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_EMAIL,1,4000),substr(:new.DS_SENHA_EMAIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_SENHA_EMAIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_OBJ_INV,1,4000),substr(:new.IE_EVENTO_OBJ_INV,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_OBJ_INV',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_RECOLETA,1,4000),substr(:new.IE_EVENTO_RECOLETA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_RECOLETA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ORDEM_SERV,1,4000),substr(:new.IE_EVENTO_ORDEM_SERV,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ORDEM_SERV',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PERFIL,1,4000),substr(:new.NR_SEQ_PERFIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_SEQ_PERFIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOSTRAR_ANEXO_COMUNIC,1,4000),substr(:new.IE_MOSTRAR_ANEXO_COMUNIC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_MOSTRAR_ANEXO_COMUNIC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_DIC,1,4000),substr(:new.IE_ATUALIZAR_DIC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_ATUALIZAR_DIC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMUNIC_PADRAO,1,4000),substr(:new.DS_COMUNIC_PADRAO,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_COMUNIC_PADRAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OS_PE_BSC,1,4000),substr(:new.IE_OS_PE_BSC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_OS_PE_BSC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOGIN,1,4000),substr(:new.DS_LOGIN,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_LOGIN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROFISSIONAL,1,4000),substr(:new.IE_PROFISSIONAL,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_PROFISSIONAL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_USUARIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_USUARIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario_atual,nr_seq_w,'DT_VALIDADE_USUARIO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_SUSP_EXAME,1,4000),substr(:new.IE_EVENTO_SUSP_EXAME,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_SUSP_EXAME',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_SOLIC_PRONT,1,4000),substr(:new.IE_EVENTO_SOLIC_PRONT,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_SOLIC_PRONT',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_MED_CIH,1,4000),substr(:new.IE_EVENTO_MED_CIH,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_MED_CIH',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_INT_AGENDA_SAN,1,4000),substr(:new.IE_EVENTO_INT_AGENDA_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_INT_AGENDA_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_VINC_AGENDA_SAN,1,4000),substr(:new.IE_EVENTO_VINC_AGENDA_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_VINC_AGENDA_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_CANC_CIR_SAN,1,4000),substr(:new.IE_EVENTO_CANC_CIR_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_CANC_CIR_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_CANC_AGENDA_SAN,1,4000),substr(:new.IE_EVENTO_CANC_AGENDA_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_CANC_AGENDA_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ENC_AGENDA_SAN,1,4000),substr(:new.IE_EVENTO_ENC_AGENDA_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ENC_AGENDA_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_TRANSF_AGENDA_SAN,1,4000),substr(:new.IE_EVENTO_TRANSF_AGENDA_SAN,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_TRANSF_AGENDA_SAN',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALTERACAO_PARAM,1,4000),substr(:new.IE_EVENTO_ALTERACAO_PARAM,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALTERACAO_PARAM',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HIST_PADRAO,1,4000),substr(:new.DS_HIST_PADRAO,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_HIST_PADRAO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CERTIFICADO,1,4000),substr(:new.CD_CERTIFICADO,1,4000),:new.nm_usuario_atual,nr_seq_w,'CD_CERTIFICADO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAN_VIEW_SENSITIVE_INFO,1,4000),substr(:new.IE_CAN_VIEW_SENSITIVE_INFO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_CAN_VIEW_SENSITIVE_INFO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAN_VIEW_PATIENT_INFO,1,4000),substr(:new.IE_CAN_VIEW_PATIENT_INFO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_CAN_VIEW_PATIENT_INFO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAN_DELETE_PATIENT,1,4000),substr(:new.IE_CAN_DELETE_PATIENT,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_CAN_DELETE_PATIENT',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAN_AUTH_PATIENT_DELETION,1,4000),substr(:new.IE_CAN_AUTH_PATIENT_DELETION,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_CAN_AUTH_PATIENT_DELETION',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_JUSTIFIC_ACESSO,1,4000),substr(:new.NR_SEQ_JUSTIFIC_ACESSO,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_SEQ_JUSTIFIC_ACESSO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_PACIENTE,1,4000),substr(:new.IE_EVENTO_PACIENTE,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_PACIENTE',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TEXTO_EMAIL,1,4000),substr(:new.DS_TEXTO_EMAIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'DS_TEXTO_EMAIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ESTRANGEIRO,1,4000),substr(:new.IE_EVENTO_ESTRANGEIRO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ESTRANGEIRO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALERTA_MEDICO,1,4000),substr(:new.IE_EVENTO_ALERTA_MEDICO,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALERTA_MEDICO',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALTA_PARTIC,1,4000),substr(:new.IE_EVENTO_ALTA_PARTIC,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALTA_PARTIC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario_atual,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALTA_REGRA,1,4000),substr(:new.IE_EVENTO_ALTA_REGRA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALTA_REGRA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_ALTA_PARTIC_EXT,1,4000),substr(:new.IE_EVENTO_ALTA_PARTIC_EXT,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_ALTA_PARTIC_EXT',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEBER_COPIA_EMAIL,1,4000),substr(:new.IE_RECEBER_COPIA_EMAIL,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_RECEBER_COPIA_EMAIL',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_PREV_ALTA,1,4000),substr(:new.IE_EVENTO_PREV_ALTA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_PREV_ALTA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_IDIOMA,1,4000),substr(:new.NR_SEQ_IDIOMA,1,4000),:new.nm_usuario_atual,nr_seq_w,'NR_SEQ_IDIOMA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_PESQUISA,1,4000),substr(:new.NM_USUARIO_PESQUISA,1,4000),:new.nm_usuario_atual,nr_seq_w,'NM_USUARIO_PESQUISA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_SOLIC_COMPRA,1,4000),substr(:new.IE_EVENTO_SOLIC_COMPRA,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_SOLIC_COMPRA',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO_SC_URGENTE,1,4000),substr(:new.IE_EVENTO_SC_URGENTE,1,4000),:new.nm_usuario_atual,nr_seq_w,'IE_EVENTO_SC_URGENTE',ie_log_w,ds_w,'USUARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.usuario_aftins
after insert ON TASY.USUARIO for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
begin
if	(:new.cd_pessoa_fisica is not null) then
	gerar_int_padrao.gravar_integracao('313',:new.cd_pessoa_fisica,:new.nm_usuario_atual, reg_integracao_p);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.usuario_delete
before delete ON TASY.USUARIO FOR EACH ROW
DECLARE

BEGIN

if	(gerar_int_dankia_pck.get_ie_int_dankia = 'S') then
	gerar_int_dankia_pck.dankia_atualizar_funcionario('E', :old.ie_profissional, :old.nm_usuario, :old.ds_usuario, :old.cd_barras, :old.ie_situacao, :old.cd_estabelecimento);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.usuario_atual_subject BEFORE
    DELETE OR INSERT OR UPDATE OF dt_atualizacao,ds_email,nm_usuario,ds_usuario,ds_senha,ds_tec,ds_login,dt_alteracao_senha,cd_estabelecimento
,qt_acesso_invalido,user_2fa_secret,user_2fa_status,ie_situacao,cd_barras ON TASY.USUARIO     FOR EACH ROW
DECLARE
    rc_usuario         usuario%rowtype;
    v_id_application   application.id%TYPE;
    v_id_datasource    datasource.id%TYPE;
    v_id_client        client.id%TYPE;
BEGIN
    v_id_application := psa_is_configured;
    v_id_datasource := psa_is_configured_datasource;
    v_id_client := psa_is_configured_client;
    IF
        v_id_application IS NOT NULL AND v_id_client IS NOT NULL AND v_id_datasource IS NOT NULL
    THEN
        IF
            inserting OR updating
        THEN
            rc_usuario.dt_atualizacao :=:new.dt_atualizacao;
            rc_usuario.ds_email :=:new.ds_email;
            rc_usuario.nm_usuario :=:new.nm_usuario;
            rc_usuario.ds_usuario :=:new.ds_usuario;
            rc_usuario.ds_senha :=:new.ds_senha;
            rc_usuario.ds_tec :=:new.ds_tec;
            rc_usuario.ds_login :=:new.ds_login;
            rc_usuario.dt_alteracao_senha :=:new.dt_alteracao_senha;
            rc_usuario.cd_estabelecimento :=:new.cd_estabelecimento;
            rc_usuario.ie_situacao :=:new.ie_situacao;
            rc_usuario.qt_acesso_invalido :=:new.qt_acesso_invalido;
            rc_usuario.user_2fa_secret :=:new.user_2fa_secret;
            rc_usuario.user_2fa_status :=:new.user_2fa_status;
            rc_usuario.cd_barras :=:new.cd_barras;
            IF
                inserting
            THEN
                psa_subject_insert(rc_usuario);
            ELSE
                psa_subject_update(:old.nm_usuario,rc_usuario,:new.ds_senha <>:old.ds_senha);
            END IF;

        ELSE
            psa_subject_delete(:old.nm_usuario);
        END IF;
    END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.USUARIO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.USUARIO FOR EACH ROW
DECLARE

qt_senha_w	number(10);
qt_reg_w	number(10);
qt_usuario_w number(5);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.qt_dia_senha is null) then
	:new.qt_dia_senha	:= 180;
end if;

if	((:new.qt_dia_senha > 180) or (:new.qt_dia_senha < 1)) and
	(:new.qt_dia_senha <> nvl(:old.qt_dia_senha,99999)) then
	--197506
	wheb_mensagem_pck.exibir_mensagem_abort(197506);
	--rise_application_error(-20011,'O N�mero de dias deve estar entre 1 e 180. (CFM/SBIS)');
end if;

if	(( :old.nm_usuario is null) or (upper(:old.nm_usuario) <> upper(:new.nm_usuario) )) and (existe_login_usuario(:new.nm_usuario) = 'S') then
	--197545
	wheb_mensagem_pck.exibir_mensagem_abort(197545);
	--rise_application_error(-20011,'Nome de usu�rio j� sendo utilizado. Registro n�o gravado!');
end if;
if	((:old.ds_login is null) or (upper(:old.ds_login) <> upper(:new.ds_login))) and ( existe_login_usuario(:new.ds_login) = 'S') then
	--197546
	wheb_mensagem_pck.exibir_mensagem_abort(197546);
	--rise_application_error(-20011,'Login alternativo j� est� sendo utilizado. Registro n�o gravado!#@#@');
end if;

if	(:new.ds_login is not null) and (upper(:new.nm_usuario) <> upper(:old.ds_login)) and (upper(:new.nm_usuario)  = upper(:new.ds_login)) then
	 --197547
	 wheb_mensagem_pck.exibir_mensagem_abort(197547);
	--rise_application_error(-20011,'O nome do usu�rio e login alternativo n�o podem ser iguais. Registro n�o gravado!#@#@');
end if;

if	( :new.ds_senha_email <> :old.ds_senha_email ) or
	( :old.ds_senha_email is null and :new.ds_senha_email is not null) then
	:new.ds_senha_email := WHEB_SEGURANCA.ENCRYPT(:new.ds_senha_email);
end if;

if	(:new.ds_senha <> :old.ds_senha) then
	insert	into usuario_hist_senha(
		NR_SEQUENCIA,
		DS_SENHA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		ds_tec
	)values(
		usuario_hist_senha_seq.nextval,
		:new.ds_senha,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.ds_tec
	);
	:new.qt_acesso_invalido := 0;

	if	(:new.dt_alteracao_senha is null) then
		delete from usuario_pergunta_senha where Upper(nm_usuario_pergunta) = Upper(:old.nm_usuario);
	end	if;

	select	to_number(obter_valor_param_usuario(6001,30,null,null, wheb_usuario_pck.get_cd_Estabelecimento))
	into	qt_senha_w
	from	dual;

	qt_reg_w := 0;
	for linha in (	SELECT	nr_sequencia
			FROM	USUARIO_HIST_SENHA
			WHERE	UPPER(nm_usuario) = UPPER(:new.nm_usuario)
			ORDER BY dt_atualizacao DESC) loop

		qt_reg_w	:= qt_reg_w + 1;

		if	(qt_reg_w > qt_senha_w) then
			delete from usuario_hist_senha where nr_sequencia = linha.nr_sequencia;
		end	if;

	end loop;
end if;


if	(:old.cd_certificado is null) and (:new.cd_certificado is not null) then
	update	pep_item_pendente
	set	IE_PENDENTE_ASSINAT_USUARIO = 'N'
	where	IE_PENDENTE_ASSINAT_USUARIO = 'S'
	and	nm_usuario	= :new.nm_usuario;
end if;

if	(gerar_int_dankia_pck.get_ie_int_dankia = 'S') and
	(updating) and
	(gerar_int_dankia_pck.get_ie_find_user_dankia(:new.nm_usuario) = 'S') then
	if ((nvl(:new.ie_profissional,'X') <> nvl(:old.ie_profissional,'X')) or
		(nvl(:new.nm_usuario,'X') <> nvl(:old.nm_usuario, 'X')) or
		(nvl(:new.ds_usuario,'X') <> nvl(:old.ds_usuario, 'X')) or
		(nvl(:new.cd_barras, 'X') <> nvl(:old.cd_barras, 'X')) or
		(nvl(:new.ie_situacao, 'X') <> nvl(:old.ie_situacao, 'X')) or
		(nvl(:new.cd_estabelecimento,9999) <> nvl(:old.cd_estabelecimento,9999))) then
		gerar_int_dankia_pck.dankia_atualizar_funcionario('A', :new.ie_profissional, :new.nm_usuario, :new.ds_usuario, :new.cd_barras, :new.ie_situacao, :new.cd_estabelecimento);
	end if;
end if;

:new.nm_usuario_pesquisa	:= upper(:new.nm_usuario);
:new.ds_utc_atualizacao := obter_data_utc(sysdate,'HV');
:new.ds_utc := OBTER_DATA_UTC(:new.dt_atualizacao,'HV');
:new.ie_horario_verao := obter_se_horario_verao(:new.dt_atualizacao);

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.USU_LOCALIDADE_AFTINS
after insert ON TASY.USUARIO for each row
begin
    gerar_localidade_usuario(:new.nm_usuario);
end USU_LOCALIDADE_AFTINS;
/


ALTER TABLE TASY.USUARIO ADD (
  CONSTRAINT USUARIO_PK
 PRIMARY KEY
 (NM_USUARIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT USUARIO_UK
 UNIQUE (CD_BARRAS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT USUARIO_UK2
 UNIQUE (DS_LOGIN)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO ADD (
  CONSTRAINT USUARIO_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT USUARIO_SETIMPR_FK 
 FOREIGN KEY (NR_SEQ_IMPRESSORA) 
 REFERENCES TASY.SETOR_IMPRESSORA (NR_SEQUENCIA),
  CONSTRAINT USUARIO_PEPPEUS_FK 
 FOREIGN KEY (NR_SEQ_PERFIL) 
 REFERENCES TASY.PEP_PERFIL_USUARIO (NR_SEQUENCIA),
  CONSTRAINT USUARIO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT USUARIO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT USUARIO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT USUARIO_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT USUARIO_COMINCL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.COMUNIC_INTERNA_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT USUARIO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_INICIAL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT USUARIO_JUACPEP_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFIC_ACESSO) 
 REFERENCES TASY.JUSTIFICATIVA_ACESSO_PEP (NR_SEQUENCIA),
  CONSTRAINT USUARIO_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.USUARIO TO NIVEL_1;

