ALTER TABLE TASY.PLS_LOTE_REEMB_REC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_REEMB_REC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_REEMB_REC_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  VL_RECUPERAR         NUMBER(15,2)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLRRI_PK ON TASY.PLS_LOTE_REEMB_REC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLRRI_PLSLRRE_FK_I ON TASY.PLS_LOTE_REEMB_REC_ITEM
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLRRI_PLSPRCO_FK_I ON TASY.PLS_LOTE_REEMB_REC_ITEM
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOTE_REEMB_REC_ITEM ADD (
  CONSTRAINT PLSLRRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_REEMB_REC_ITEM ADD (
  CONSTRAINT PLSLRRI_PLSLRRE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_REEMB_RECUPERACAO (NR_SEQUENCIA),
  CONSTRAINT PLSLRRI_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_REEMB_REC_ITEM TO NIVEL_1;

