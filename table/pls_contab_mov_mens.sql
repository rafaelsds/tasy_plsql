ALTER TABLE TASY.PLS_CONTAB_MOV_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTAB_MOV_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTAB_MOV_MENS
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_LOTE             NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_ITEM            VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_LANCAMENTO  NUMBER(10),
  NR_SEQ_TIPO_ACRESCIMO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOMM_ESTABEL_FK_I ON TASY.PLS_CONTAB_MOV_MENS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMM_I1 ON TASY.PLS_CONTAB_MOV_MENS
(IE_TIPO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOMM_PK ON TASY.PLS_CONTAB_MOV_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMM_PLSCOML_FK_I ON TASY.PLS_CONTAB_MOV_MENS
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMM_PLSCOML_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMM_PLSTACR_FK_I ON TASY.PLS_CONTAB_MOV_MENS
(NR_SEQ_TIPO_ACRESCIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMM_PLSTACR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CONTAB_MOV_MENS ADD (
  CONSTRAINT PLSCOMM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTAB_MOV_MENS ADD (
  CONSTRAINT PLSCOMM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCOMM_PLSCOML_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_CONTAB_MOV_MENS_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSCOMM_PLSTACR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACRESCIMO) 
 REFERENCES TASY.PLS_TIPO_ACRESCIMO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTAB_MOV_MENS TO NIVEL_1;

