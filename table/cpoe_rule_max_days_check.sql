ALTER TABLE TASY.CPOE_RULE_MAX_DAYS_CHECK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_RULE_MAX_DAYS_CHECK CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_RULE_MAX_DAYS_CHECK
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_MATERIAL                 NUMBER(10)        NOT NULL,
  NR_MAX_DAYS                 NUMBER(5)         NOT NULL,
  SI_ENABLE_ORDER_SUBMISSION  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CP_RM_DC_MATERIA_FK_I ON TASY.CPOE_RULE_MAX_DAYS_CHECK
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CP_RM_DC_PK ON TASY.CPOE_RULE_MAX_DAYS_CHECK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_RULE_MAX_DAYS_CHECK ADD (
  CONSTRAINT CP_RM_DC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_RULE_MAX_DAYS_CHECK ADD (
  CONSTRAINT CP_RM_DC_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.CPOE_RULE_MAX_DAYS_CHECK TO NIVEL_1;

