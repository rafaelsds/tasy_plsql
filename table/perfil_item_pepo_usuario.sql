ALTER TABLE TASY.PERFIL_ITEM_PEPO_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ITEM_PEPO_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ITEM_PEPO_USUARIO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PERFIL_ITEM    NUMBER(10)              NOT NULL,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE)       NOT NULL,
  IE_ATUALIZAR          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEITPEU_PERITEP_FK_I ON TASY.PERFIL_ITEM_PEPO_USUARIO
(NR_SEQ_PERFIL_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEITPEU_PK ON TASY.PERFIL_ITEM_PEPO_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PERFIL_ITEM_PEPO_USUARIO ADD (
  CONSTRAINT PEITPEU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ITEM_PEPO_USUARIO ADD (
  CONSTRAINT PEITPEU_PERITEP_FK 
 FOREIGN KEY (NR_SEQ_PERFIL_ITEM) 
 REFERENCES TASY.PERFIL_ITEM_PEPO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PERFIL_ITEM_PEPO_USUARIO TO NIVEL_1;

