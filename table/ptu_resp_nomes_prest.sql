ALTER TABLE TASY.PTU_RESP_NOMES_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_RESP_NOMES_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_RESP_NOMES_PREST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_UNIMED_PRESTADOR  NUMBER(4)                NOT NULL,
  IE_ALTO_CUSTO        VARCHAR2(1 BYTE),
  NM_PRESTADOR         VARCHAR2(40 BYTE)        NOT NULL,
  CD_PRESTADOR         NUMBER(8)                NOT NULL,
  NR_SEQ_RESP_PREST    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESPECIALIDADE     NUMBER(2),
  IE_TIPO_REDE_MIN     NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTURNPR_PK ON TASY.PTU_RESP_NOMES_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTURNPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTURNPR_PTURCPR_FK_I ON TASY.PTU_RESP_NOMES_PREST
(NR_SEQ_RESP_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTURNPR_PTURCPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_RESP_NOMES_PREST ADD (
  CONSTRAINT PTURNPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_RESP_NOMES_PREST ADD (
  CONSTRAINT PTURNPR_PTURCPR_FK 
 FOREIGN KEY (NR_SEQ_RESP_PREST) 
 REFERENCES TASY.PTU_RESP_CONSULTA_PREST (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_RESP_NOMES_PREST TO NIVEL_1;

