ALTER TABLE TASY.INTERF_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERF_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERF_CAMPO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_BANDA            NUMBER(10)            NOT NULL,
  NM_CAMPO_ORIG           VARCHAR2(255 BYTE),
  NM_CAMPO_PORT           VARCHAR2(255 BYTE),
  NM_CAMPO_ING            VARCHAR2(255 BYTE),
  NR_SEQ_DOMINIO          NUMBER(10),
  IE_PENDENTE             VARCHAR2(1 BYTE)      NOT NULL,
  NM_CAMPO                VARCHAR2(255 BYTE),
  NM_CAMPO_BASE           VARCHAR2(255 BYTE),
  IE_CONFIRM_TELA         VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  NM_ATRIBUTO_DESTINO     VARCHAR2(50 BYTE),
  NM_ATRIBUTO_DEST_LONGO  VARCHAR2(50 BYTE),
  IE_TIPO_ATRIBUTO        VARCHAR2(10 BYTE),
  QT_TAMANHO              NUMBER(10),
  QT_DECIMAIS             NUMBER(10),
  IE_OBRIGATORIO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTCAMP_INTADOM_FK_I ON TASY.INTERF_CAMPO
(NR_SEQ_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTCAMP_INTBAND_FK_I ON TASY.INTERF_CAMPO
(NR_SEQ_BANDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTCAMP_PK ON TASY.INTERF_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTERF_CAMPO_tp  after update ON TASY.INTERF_CAMPO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_ATRIBUTO,1,4000),substr(:new.IE_TIPO_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATRIBUTO',ie_log_w,ds_w,'INTERF_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'INTERF_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DECIMAIS,1,4000),substr(:new.QT_DECIMAIS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DECIMAIS',ie_log_w,ds_w,'INTERF_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAMANHO,1,4000),substr(:new.QT_TAMANHO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAMANHO',ie_log_w,ds_w,'INTERF_CAMPO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.INTERF_CAMPO ADD (
  CONSTRAINT INTCAMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERF_CAMPO ADD (
  CONSTRAINT INTCAMP_INTADOM_FK 
 FOREIGN KEY (NR_SEQ_DOMINIO) 
 REFERENCES TASY.INTERF_TAB_DOMINIO (NR_SEQUENCIA),
  CONSTRAINT INTCAMP_INTBAND_FK 
 FOREIGN KEY (NR_SEQ_BANDA) 
 REFERENCES TASY.INTERF_BANDA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INTERF_CAMPO TO NIVEL_1;

