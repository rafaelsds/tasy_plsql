ALTER TABLE TASY.CARGO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARGO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARGO
(
  CD_CARGO                 NUMBER(10)           NOT NULL,
  DS_CARGO                 VARCHAR2(80 BYTE)    NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF           NUMBER(10),
  IE_TIPO_EVOLUCAO         VARCHAR2(3 BYTE),
  IE_MEDICO                VARCHAR2(1 BYTE)     NOT NULL,
  IE_EXIGE_CONSELHO        VARCHAR2(1 BYTE)     NOT NULL,
  IE_ALTERAR_CARGO         VARCHAR2(1 BYTE),
  CD_EXTERNO               VARCHAR2(255 BYTE),
  NR_SEQ_PERFIL            NUMBER(10),
  NR_CONSELHO              VARCHAR2(20 BYTE),
  NR_SEQ_PEP_PERF_USU      NUMBER(10),
  IE_GERAR_USUARIO         VARCHAR2(1 BYTE),
  NR_NIVEL_APROVACAO       NUMBER(10),
  CD_CBO_SUS               NUMBER(6),
  CD_EXP_CARGO             NUMBER(10),
  IE_ENCAMINHA_PROC_APROV  VARCHAR2(1 BYTE),
  CD_EXT_MORTE_FETAL       VARCHAR2(15 BYTE),
  IE_TIPO_PROF_SAUDE       VARCHAR2(2 BYTE),
  IE_TYPE                  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CARGO_CBORED_FK_I ON TASY.CARGO
(CD_CBO_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CARGO_CBORED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CARGO_COMINCL_FK_I ON TASY.CARGO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CARGO_COMINCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CARGO_DICEXPR_FK_I ON TASY.CARGO
(CD_EXP_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARGO_PEPPEPA_FK_I ON TASY.CARGO
(NR_SEQ_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CARGO_PEPPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CARGO_PEPPEUS_FK_I ON TASY.CARGO
(NR_SEQ_PEP_PERF_USU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CARGO_PEPPEUS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CARGO_PK ON TASY.CARGO
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CARGO_tp  after update ON TASY.CARGO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CARGO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CBO_SUS,1,4000),substr(:new.CD_CBO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_CBO_SUS',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CARGO,1,4000),substr(:new.DS_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CARGO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_EVOLUCAO,1,4000),substr(:new.IE_TIPO_EVOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_EVOLUCAO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDICO,1,4000),substr(:new.IE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_CONSELHO,1,4000),substr(:new.IE_EXIGE_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_CONSELHO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTERAR_CARGO,1,4000),substr(:new.IE_ALTERAR_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTERAR_CARGO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PEP_PERF_USU,1,4000),substr(:new.NR_SEQ_PEP_PERF_USU,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PEP_PERF_USU',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PERFIL,1,4000),substr(:new.NR_SEQ_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PERFIL',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONSELHO,1,4000),substr(:new.NR_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONSELHO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXTERNO,1,4000),substr(:new.CD_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXTERNO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_USUARIO,1,4000),substr(:new.IE_GERAR_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_USUARIO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_NIVEL_APROVACAO,1,4000),substr(:new.NR_NIVEL_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_NIVEL_APROVACAO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'CARGO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CARGO ADD (
  CONSTRAINT CARGO_PK
 PRIMARY KEY
 (CD_CARGO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CARGO ADD (
  CONSTRAINT CARGO_PEPPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERFIL) 
 REFERENCES TASY.PEP_PERFIL_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT CARGO_PEPPEUS_FK 
 FOREIGN KEY (NR_SEQ_PEP_PERF_USU) 
 REFERENCES TASY.PEP_PERFIL_USUARIO (NR_SEQUENCIA),
  CONSTRAINT CARGO_CBORED_FK 
 FOREIGN KEY (CD_CBO_SUS) 
 REFERENCES TASY.CBO_RED (CD_CBO),
  CONSTRAINT CARGO_COMINCL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.COMUNIC_INTERNA_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT CARGO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CARGO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.CARGO TO NIVEL_1;

GRANT INSERT, SELECT, UPDATE ON TASY.CARGO TO SAOJOSE;

