ALTER TABLE TASY.MOVIMENTO_CONTABIL_DOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVIMENTO_CONTABIL_DOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVIMENTO_CONTABIL_DOC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_MOVIMENTO        NUMBER(10),
  NR_LOTE_CONTABIL        NUMBER(10)            NOT NULL,
  NR_SEQ_INFO             NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_DOCUMENTO            NUMBER(14),
  VL_MOVIMENTO            NUMBER(15,2)          NOT NULL,
  NR_SEQ_DOC_COMPL        NUMBER(10),
  NM_TABELA               VARCHAR2(50 BYTE),
  NM_ATRIBUTO             VARCHAR2(255 BYTE),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  IE_DEBITO_CREDITO       VARCHAR2(1 BYTE),
  DT_MOVIMENTO            DATE,
  CD_HISTORICO            NUMBER(10),
  DS_COMPL_HISTORICO      VARCHAR2(255 BYTE),
  NR_SEQ_REGRA_TF         NUMBER(10),
  NR_SEQ_CTB_MOVTO        NUMBER(10),
  CD_CNPJ                 VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  NR_SEQ_CTB_DOCUMENTO    NUMBER(14),
  NR_DOC_ANALITICO        NUMBER(14),
  NR_SEQ_TRANS_FIN        NUMBER(10),
  CD_CENTRO_CUSTO         NUMBER(8)             DEFAULT null,
  CD_SEQUENCIA_PARAMETRO  NUMBER(10),
  NR_NFE_IMP              VARCHAR2(255 BYTE),
  NR_CODIGO_CONTROLE      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOVCDOC_CTBDOC_I1 ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_SEQ_CTB_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCDOC_I ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_SEQ_CTB_MOVTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCDOC_INFOCTB_FK_I ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_SEQ_INFO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCDOC_LOTCONT_FK_I ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCDOC_MOVCONT_FK_I ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_LOTE_CONTABIL, NR_SEQ_MOVIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOVCDOC_PK ON TASY.MOVIMENTO_CONTABIL_DOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.movimento_contabil_doc_before
before insert or delete ON TASY.MOVIMENTO_CONTABIL_DOC for each row
declare
nr_seq_ctb_documento_w		ctb_documento.nr_sequencia%type;
cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;
cd_tipo_lote_contabil_w		ctb_documento.cd_tipo_lote_contabil%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;
nm_atributo_w			ctb_documento.nm_atributo%type;
nm_atributo_ret_w		varchar2(255);
id_indx_aux_w			integer:= 0;
ie_atributo_unico_w		boolean:= false;


begin

/*Vari�vel recebendo N como valor padr�o pois a funcionalidade ainda n�o est� disponibilizada aos clientes para utiliza��o*/
ie_concil_contab_w := 'N';

if	(ie_concil_contab_w = 'S') then
	begin

	if	(inserting) then
		begin

		begin

		select	a.cd_estabelecimento,
			b.cd_tipo_lote_contabil
		into	cd_estabelecimento_w,
			cd_tipo_lote_contabil_w
		from	movimento_contabil a,
			lote_contabil b
		where 	a.nr_lote_contabil 	= b.nr_lote_contabil
		and 	a.nr_sequencia 		= :new.nr_seq_movimento
		and	a.nr_lote_contabil 	= :new.nr_lote_contabil;

		exception
		when others then
		cd_estabelecimento_w	:= 0;
		cd_tipo_lote_contabil_w	:= 0;
		end;

		nm_atributo_ret_w:= ctb_concil_financeira_pck.formata_atributos_conciliacao(:new.nm_atributo);

		for i in 1..length(nm_atributo_ret_w)loop
			begin

			if ( substr(nm_atributo_ret_w,i,1) = '|') then
				id_indx_aux_w := id_indx_aux_w + 1;
			end if;

			end;
		end loop;

		if	(id_indx_aux_w = 0) and (length(nm_atributo_ret_w) > 0) then
			begin
				id_indx_aux_w		:= 1;
				ie_atributo_unico_w	:= true;
			end;
		end if;

		for i in 1..id_indx_aux_w loop
			begin
			if	(id_indx_aux_w = 1) and (ie_atributo_unico_w) then
				begin

				nm_atributo_w		:= nm_atributo_ret_w;

				end;
			else
				begin

				nm_atributo_w		:= substr(nm_atributo_ret_w,1 , instr(nm_atributo_ret_w, '|') - 1);
				nm_atributo_ret_w	:= substr(nm_atributo_ret_w, instr(nm_atributo_ret_w, '|') + 1, length(nm_atributo_ret_w));

				end;
			end if;

			begin

			select	nr_sequencia
			into	nr_seq_ctb_documento_w
			from	ctb_documento a
			where	trunc(a.dt_competencia, 'dd')	= trunc(:new.dt_movimento, 'dd')
			and 	a.cd_tipo_lote_contabil		= cd_tipo_lote_contabil_w
			and 	a.nr_seq_info			= :new.nr_seq_info
			and 	a.nr_documento			= :new.nr_documento
			and 	nvl(a.nr_seq_doc_compl, 0)	= nvl(:new.nr_seq_doc_compl,0)
			and 	nvl(a.nr_doc_analitico, 0)	= nvl(:new.nr_doc_analitico, 0)
			and 	a.nm_tabela			= :new.nm_tabela
			and 	a.nm_atributo			= nm_atributo_w
			and 	((a.vl_movimento		= :new.vl_movimento)
				or a.cd_tipo_lote_contabil in (2, 51));

			exception
			when others then
				nr_seq_ctb_documento_w:= 0;

			end;

			if	(nvl(nr_seq_ctb_documento_w, 0) <> 0) then
				begin

				:new.nr_seq_ctb_documento	:= nr_seq_ctb_documento_w;
				:new.nm_usuario			:= :new.nm_usuario;
				:new.dt_atualizacao		:= sysdate;

				update	ctb_documento
				set	nr_lote_contabil	= :new.nr_lote_contabil,
					ie_status		= 'P',
					nm_usuario		= :new.nm_usuario,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= nr_seq_ctb_documento_w
				and 	cd_tipo_lote_contabil 	= cd_tipo_lote_contabil_w;

				end;
			end if;

			end;
		end loop;

		end;
	else
		begin

		update	ctb_documento
		set 	nr_lote_contabil	= null
		where 	nr_lote_contabil	= :old.nr_lote_contabil;

		end;
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.MOVIMENTO_CONTABIL_DOC ADD (
  CONSTRAINT MOVCDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVIMENTO_CONTABIL_DOC ADD (
  CONSTRAINT MOVCDOC_INFOCTB_FK 
 FOREIGN KEY (NR_SEQ_INFO) 
 REFERENCES TASY.INFORMACAO_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT MOVCDOC_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT MOVCDOC_MOVCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL, NR_SEQ_MOVIMENTO) 
 REFERENCES TASY.MOVIMENTO_CONTABIL (NR_LOTE_CONTABIL,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MOVIMENTO_CONTABIL_DOC TO NIVEL_1;

