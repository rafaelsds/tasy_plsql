ALTER TABLE TASY.PLS_OCORRENCIA_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCORRENCIA_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCORRENCIA_GRUPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_OCORRENCIA    NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_FLUXO         NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_USUARIO_EXEC      VARCHAR2(15 BYTE),
  IE_ORIGEM_CONTA      VARCHAR2(1 BYTE),
  IE_AUTORIZACAO       VARCHAR2(1 BYTE),
  IE_CONTA_MEDICA      VARCHAR2(1 BYTE),
  IE_REQUISICAO        VARCHAR2(1 BYTE),
  IE_INTERCAMBIO       VARCHAR2(1 BYTE),
  IE_TIPO_ANALISE      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCGR_PK ON TASY.PLS_OCORRENCIA_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCGR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCGR_PLSGRAU_FK_I ON TASY.PLS_OCORRENCIA_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCGR_PLSOCOR_FK_I ON TASY.PLS_OCORRENCIA_GRUPO
(NR_SEQ_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ocorrencia_grupo_atual
before insert or update ON TASY.PLS_OCORRENCIA_GRUPO for each row
declare
ie_alerta_w			varchar2(255)	:= 'N';
nr_seq_fluxo_w			number(10);

begin
if	(nvl(:new.ie_autorizacao,'S') = 'S') and
	(:new.nr_seq_fluxo is null) then
	ie_alerta_w	:= 'S';
end if;

if	(nvl(:new.ie_conta_medica,'S') = 'S') then
	select	max(a.nr_seq_fluxo_padrao)
	into	nr_seq_fluxo_w
	from	pls_grupo_auditor	a
	where	a.nr_sequencia	= :new.nr_seq_grupo;

	if	(nr_seq_fluxo_w is null) and
		(:new.nr_seq_fluxo is null) then
		ie_alerta_w	:= 'S';
	end if;
end if;

if	(ie_alerta_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(214142);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_GRUPO_tp  after update ON TASY.PLS_OCORRENCIA_GRUPO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_ANALISE,1,4000),substr(:new.IE_TIPO_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ANALISE',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO,1,4000),substr(:new.NR_SEQ_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTERCAMBIO,1,4000),substr(:new.IE_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTERCAMBIO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FLUXO,1,4000),substr(:new.NR_SEQ_FLUXO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FLUXO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTORIZACAO,1,4000),substr(:new.IE_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTORIZACAO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_MEDICA,1,4000),substr(:new.IE_CONTA_MEDICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_MEDICA',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_CONTA,1,4000),substr(:new.IE_ORIGEM_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_CONTA',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQUISICAO,1,4000),substr(:new.IE_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQUISICAO',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_EXEC,1,4000),substr(:new.NM_USUARIO_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_EXEC',ie_log_w,ds_w,'PLS_OCORRENCIA_GRUPO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OCORRENCIA_GRUPO ADD (
  CONSTRAINT PLSOCGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCORRENCIA_GRUPO ADD (
  CONSTRAINT PLSOCGR_PLSOCOR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA) 
 REFERENCES TASY.PLS_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSOCGR_PLSGRAU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OCORRENCIA_GRUPO TO NIVEL_1;

