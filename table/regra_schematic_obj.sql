ALTER TABLE TASY.REGRA_SCHEMATIC_OBJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_SCHEMATIC_OBJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_SCHEMATIC_OBJ
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_TIPO_OBJETO         VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_REGRA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_SCHEMATIC  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGSCHO_PK ON TASY.REGRA_SCHEMATIC_OBJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGSCHO_REGSCHE_FK_I ON TASY.REGRA_SCHEMATIC_OBJ
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGSCHO_TPSCHE_FK_I ON TASY.REGRA_SCHEMATIC_OBJ
(NR_SEQ_TIPO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGSCHO_UK ON TASY.REGRA_SCHEMATIC_OBJ
(IE_TIPO_OBJETO, NR_SEQ_TIPO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_SCHEMATIC_OBJ ADD (
  CONSTRAINT REGSCHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT REGSCHO_UK
 UNIQUE (IE_TIPO_OBJETO, NR_SEQ_TIPO_SCHEMATIC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_SCHEMATIC_OBJ ADD (
  CONSTRAINT REGSCHO_REGSCHE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_SCHEMATIC (NR_SEQUENCIA),
  CONSTRAINT REGSCHO_TPSCHE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_SCHEMATIC) 
 REFERENCES TASY.TIPO_SCHEMATIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_SCHEMATIC_OBJ TO NIVEL_1;

