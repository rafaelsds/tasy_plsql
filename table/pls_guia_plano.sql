ALTER TABLE TASY.PLS_GUIA_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GUIA_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GUIA_PLANO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_SEGURADO               NUMBER(10),
  DT_VALIDADE_SENHA             DATE,
  CD_GUIA                       VARCHAR2(20 BYTE),
  IE_TIPO_GUIA                  VARCHAR2(2 BYTE) NOT NULL,
  DT_SOLICITACAO                DATE            NOT NULL,
  CD_MEDICO_SOLICITANTE         VARCHAR2(10 BYTE),
  QT_DIA_SOLICITADO             NUMBER(3),
  QT_DIA_AUTORIZADO             NUMBER(3),
  IE_STATUS                     VARCHAR2(2 BYTE) NOT NULL,
  NR_SEQ_PRESTADOR              NUMBER(10),
  NR_SEQ_PRESTADOR_IMP          VARCHAR2(20 BYTE),
  IE_CARATER_INTERNACAO         VARCHAR2(1 BYTE),
  IE_CARATER_INTERNACAO_IMP     VARCHAR2(1 BYTE),
  DT_AUTORIZACAO                DATE,
  NM_PRESTADOR_IMP              VARCHAR2(255 BYTE),
  CD_SENHA                      VARCHAR2(20 BYTE),
  DT_EMISSAO                    DATE,
  DT_EMISSAO_IMP                DATE,
  DS_INDICACAO_CLINICA          VARCHAR2(4000 BYTE),
  IE_REGIME_INTERNACAO          VARCHAR2(1 BYTE),
  IE_REGIME_INTERNACAO_IMP      VARCHAR2(1 BYTE),
  NR_SEQ_PLANO                  NUMBER(10),
  DS_CONSELHO_PROFISSIONAL_IMP  VARCHAR2(255 BYTE),
  NR_CRM_IMP                    VARCHAR2(20 BYTE),
  UF_CRM_IMP                    VARCHAR2(2 BYTE),
  DT_ADMISSAO_HOSP              DATE,
  CD_CNES_IMP                   VARCHAR2(20 BYTE),
  CD_GUIA_IMP                   VARCHAR2(20 BYTE),
  NM_MEDICO_SOLICITANTE_IMP     VARCHAR2(255 BYTE),
  CD_USUARIO_PLANO_IMP          VARCHAR2(30 BYTE),
  DS_PLANO_IMP                  VARCHAR2(255 BYTE),
  DT_VALIDADE_CARTAO_IMP        DATE,
  NM_SEGURADO_IMP               VARCHAR2(255 BYTE),
  CD_ANS_IMP                    VARCHAR2(20 BYTE),
  DT_SOLICITACAO_IMP            DATE,
  DS_INDICACAO_CLINICA_IMP      VARCHAR2(4000 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ACOMODACAO        NUMBER(10),
  NR_SEQ_CLINICA                NUMBER(10),
  NR_SEQ_CLINICA_IMP            VARCHAR2(10 BYTE),
  NR_SEQ_AUTORIZACAO_IMP        NUMBER(10),
  DT_LIBERACAO                  DATE,
  NM_USUARIO_LIBERACAO          VARCHAR2(15 BYTE),
  NR_CARTAO_NAC_SUS_IMP         VARCHAR2(30 BYTE),
  CD_CGC_SOLICITANTE_IMP        VARCHAR2(14 BYTE),
  CD_CPF_SOLICITANTE_IMP        VARCHAR2(11 BYTE),
  CD_INTERNO_SOLICITANTE_IMP    VARCHAR2(20 BYTE),
  CD_CNES_MEDICO_IMP            VARCHAR2(20 BYTE),
  CD_CGC_PRESTADOR_IMP          VARCHAR2(14 BYTE),
  NR_CPF_PRESTADOR_IMP          VARCHAR2(11 BYTE),
  QT_DIA_AUTORIZADO_IMP         VARCHAR2(5 BYTE),
  DT_ADMISSAO_HOSP_IMP          DATE,
  DT_VALIDADE_SENHA_IMP         DATE,
  CD_TIPO_ACOMOD_AUTOR_IMP      VARCHAR2(10 BYTE),
  CD_SENHA_IMP                  VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  CD_GUIA_PRESTADOR             VARCHAR2(20 BYTE),
  IE_TIPO_PROCESSO              VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_TIPO_LIMITACAO         NUMBER(10),
  CD_GUIA_PRINCIPAL             VARCHAR2(20 BYTE),
  CD_CPF_PRESTADOR_IMP          VARCHAR2(15 BYTE),
  CD_CBO_SAUDE_IMP              VARCHAR2(30 BYTE),
  CD_GUIA_PRINCIPAL_IMP         VARCHAR2(20 BYTE),
  CD_CGC_SOLICITADO_IMP         VARCHAR2(14 BYTE),
  NR_CPF_SOLICITADO_IMP         VARCHAR2(15 BYTE),
  CD_INTERNO_SOLICITADO_IMP     VARCHAR2(20 BYTE),
  NM_SOLICITADO_IMP             VARCHAR2(255 BYTE),
  DS_OBSERVACAO_IMP             VARCHAR2(4000 BYTE),
  NM_RESPONSAVEL_AUTORIZ_IMP    VARCHAR2(255 BYTE),
  NR_SEQ_GUIA_PRINCIPAL         NUMBER(10),
  DT_CANCELAMENTO               DATE,
  IE_FORMA_IMP                  VARCHAR2(1 BYTE),
  IE_TIPO_ATEND_TISS            VARCHAR2(2 BYTE),
  IE_UTILIZADO                  VARCHAR2(2 BYTE),
  IE_TIPO_SEGURADO              VARCHAR2(3 BYTE),
  NR_SEQ_PERICIA                NUMBER(10),
  IE_COBRANCA_PREVISTA          VARCHAR2(1 BYTE),
  IE_ESTAGIO                    NUMBER(2)       NOT NULL,
  NR_SEQ_REGRA_LIBERACAO        NUMBER(10),
  DS_LOG                        VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_CANCEL          NUMBER(10),
  NR_SEQ_MOTIVO_LIB             NUMBER(10),
  DS_OBSERVACAO                 VARCHAR2(4000 BYTE),
  NR_SEQ_ATEND_PLS              NUMBER(10),
  NR_SEQ_EVENTO_ATEND           NUMBER(10),
  IE_TIPO_CONSULTA              NUMBER(1),
  NR_SEQ_GUIA_PLANO_ANT         NUMBER(10),
  CD_VERSAO                     VARCHAR2(70 BYTE),
  DS_APLICATIVO                 VARCHAR2(70 BYTE),
  DS_FABRICANTE                 VARCHAR2(70 BYTE),
  CD_GUIA_MANUAL                VARCHAR2(20 BYTE),
  CD_SENHA_EXTERNA              VARCHAR2(20 BYTE),
  NM_USUARIO_ATENDIMENTO        VARCHAR2(15 BYTE),
  NR_SEQ_UNI_EXEC               NUMBER(10),
  IE_CONSULTA_URGENCIA          VARCHAR2(1 BYTE),
  IE_ESTAGIO_COMPLEMENTO        NUMBER(1),
  DS_BIOMETRIA                  VARCHAR2(4000 BYTE),
  VL_AUTORIZACAO                NUMBER(15,2),
  IE_TIPO_INTERCAMBIO           VARCHAR2(10 BYTE),
  IE_TIPO_SAIDA                 VARCHAR2(1 BYTE),
  NR_SEQ_PRESTADOR_WEB          NUMBER(10),
  CD_GUIA_MANUAL_IMP            VARCHAR2(20 BYTE),
  DT_VALID_SENHA_EXT            DATE,
  IE_PAGAMENTO_AUTOMATICO       VARCHAR2(3 BYTE),
  NR_SEQ_PGTO_AUT               NUMBER(10),
  NR_SEQ_CONSELHO               NUMBER(10),
  IE_RECEM_NASCIDO              VARCHAR2(1 BYTE),
  NM_RECEM_NASCIDO              VARCHAR2(60 BYTE),
  DT_NASC_RECEM_NASCIDO         DATE,
  IE_TIPO_GAT                   VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE              NUMBER(5),
  NR_SEQ_REGRA_COMPL            NUMBER(10),
  IE_ORIGEM_SOLIC               VARCHAR2(2 BYTE),
  NM_USUARIO_SOLIC              VARCHAR2(255 BYTE),
  DT_DUPLICACAO_GUIA            DATE,
  CD_MATRICULA_ESTIPULANTE      VARCHAR2(30 BYTE),
  CD_GUIA_PESQUISA              VARCHAR2(20 BYTE),
  NR_SEQ_CAT                    NUMBER(10),
  NR_SEQ_GUIA_ORIGEM            NUMBER(10),
  IE_PCMSO                      VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_PRORROGACAO     NUMBER(10),
  NR_SEQ_PREST_SOLIC            NUMBER(10),
  IE_RECEM_NASCIDO_IMP          VARCHAR2(1 BYTE),
  NM_SOLICITANTE_IMP            VARCHAR2(70 BYTE),
  DT_INTERNACAO_IMP             DATE,
  IE_INDICACAO_QUIMIO_IMP       VARCHAR2(1 BYTE),
  IE_INDICACAO_OPME_IMP         VARCHAR2(1 BYTE),
  CD_SENHA_REGRA                VARCHAR2(20 BYTE),
  CD_TIPO_ACOMODACAO            VARCHAR2(2 BYTE),
  NR_SEQ_LOTE_ANEXO_GUIA        NUMBER(10),
  IE_ANEXO_GUIA                 VARCHAR2(1 BYTE),
  DT_RECEBIMENTO_XML            DATE,
  IE_ANEXO_OPME                 VARCHAR2(1 BYTE),
  IE_ANEXO_QUIMIOTERAPIA        VARCHAR2(1 BYTE),
  IE_ANEXO_RADIOTERAPIA         VARCHAR2(1 BYTE),
  DT_INTERNACAO_SUGERIDA        DATE,
  IE_AGUARDA_ANEXO_GUIA         VARCHAR2(1 BYTE),
  IE_MOTIVO_ENCERRAMENTO        VARCHAR2(2 BYTE),
  NR_SEQ_CBO_SAUDE              NUMBER(10),
  NR_SEQ_GUIA_OK                NUMBER(10),
  DS_TOKEN                      VARCHAR2(10 BYTE),
  ID_APP_EXTERNO                NUMBER(10),
  IE_APP_EXTERNO                VARCHAR2(2 BYTE),
  IE_TIPO_IDENT_BENEF           VARCHAR2(2 BYTE),
  CD_VALIDACAO_BENEF_TISS       VARCHAR2(10 BYTE),
  CD_AUSENCIA_VAL_BENEF_TISS    VARCHAR2(2 BYTE),
  CD_IDENT_BIOMETRIA_BENEF      VARCHAR2(4000 BYTE),
  CD_TEMPLATE_BIOMET_BENEF      VARCHAR2(4000 BYTE),
  IE_ETAPA_AUTORIZACAO          VARCHAR2(2 BYTE),
  DS_QRCODE                     VARCHAR2(4000 BYTE),
  IE_ORIGEM_EXECUCAO            VARCHAR2(1 BYTE),
  IE_TIPO_VALIDACAO_BIOMETRIA   NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSGUIA_CBOSAUD_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_CONPROF_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_CONSELHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_CONPROF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_ESPMEDI_FK_I ON TASY.PLS_GUIA_PLANO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_ESTABEL_FK_I ON TASY.PLS_GUIA_PLANO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I1 ON TASY.PLS_GUIA_PLANO
(IE_ESTAGIO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I10 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PRESTADOR_WEB, IE_TIPO_GUIA, IE_STATUS, DT_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I11 ON TASY.PLS_GUIA_PLANO
(CD_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I12 ON TASY.PLS_GUIA_PLANO
(CD_GUIA_MANUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I13 ON TASY.PLS_GUIA_PLANO
(CD_GUIA_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I14 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO, CD_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I15 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO, CD_GUIA_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I2 ON TASY.PLS_GUIA_PLANO
(CD_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I20 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO, CD_GUIA_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I21 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO, NR_SEQ_UNI_EXEC, CD_SENHA, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I22 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO, NR_SEQ_UNI_EXEC, CD_SENHA_EXTERNA, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I23 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PRESTADOR, DT_AUTORIZACAO, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I24 ON TASY.PLS_GUIA_PLANO
(NR_SEQ_GUIA_OK, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I25 ON TASY.PLS_GUIA_PLANO
(CD_SENHA, NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I3 ON TASY.PLS_GUIA_PLANO
(CD_GUIA_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I4 ON TASY.PLS_GUIA_PLANO
(DT_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I5 ON TASY.PLS_GUIA_PLANO
(IE_STATUS, IE_ESTAGIO, NR_SEQ_GUIA_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I6 ON TASY.PLS_GUIA_PLANO
(DT_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I7 ON TASY.PLS_GUIA_PLANO
(IE_ESTAGIO_COMPLEMENTO, IE_UTILIZADO, IE_STATUS, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I8 ON TASY.PLS_GUIA_PLANO
(CD_GUIA, NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_I9 ON TASY.PLS_GUIA_PLANO
(IE_PAGAMENTO_AUTOMATICO, NR_SEQ_PRESTADOR, IE_STATUS, DT_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_MEDICO_FK_I ON TASY.PLS_GUIA_PLANO
(CD_MEDICO_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGUIA_PK ON TASY.PLS_GUIA_PLANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_PLSASEV_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_EVENTO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSASEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSATEN_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_ATEND_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSCATS_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_CAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSCATS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSCLIN_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSCLIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSCONG_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_UNI_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSGUIA_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_GUIA_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_PLSGUIA_FK2_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_GUIA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSGUIA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSGUMC_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSGUMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSLAGA_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_LOTE_ANEXO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSLAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSMOLIB_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_MOTIVO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSMOLIB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSMPRA_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_MOTIVO_PRORROGACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSMPRA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSPLAN_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSPRES_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_PLSPRES_FK2_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PREST_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSRCOC_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_REGRA_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSRCOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSSEGU_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUIA_PLSTIAC_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGUIA_PLSUSWE_FK_I ON TASY.PLS_GUIA_PLANO
(NR_SEQ_PRESTADOR_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGUIA_PLSUSWE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_guia_Delete
before delete ON pls_guia_plano
FOR EACH ROW
DECLARE

BEGIN

insert into log_exclusao
	(nm_tabela,
	dt_atualizacao,
	nm_usuario,
	ds_chave)
values	('PLS_GUIA_PLANO',
	sysdate,
	:old.nm_usuario,
	' NR_SEQUENCIA= '		|| :old.nr_sequencia			||
	' MAQUINA= '			|| substr(obter_inf_sessao(0),1,80)	||
	' OUSER= '			|| substr(obter_inf_sessao(1),1,30));
END;
/


CREATE OR REPLACE TRIGGER TASY.pls_alt_autor_log_bd
before update or delete ON TASY.PLS_GUIA_PLANO for each row
declare

nm_machine_w		varchar2(255);
ie_tipo_alteracao_w	varchar2(1)	:= null;
ds_log_w		varchar2(2000)	:= null;
qt_rotina_w		number(10);

begin

if	(pls_se_aplicacao_tasy = 'N') then
	begin
	SELECT	machine
	INTO	nm_machine_w
	FROM	v$session
	WHERE	audsid = USERENV('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;

	/* Verifica��o de SCS */
	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'TASY_SCS%';

	if	(qt_rotina_w = 0) then

		if	(updating) then
			if	(nvl(:new.ie_estagio,0) <>  nvl(:old.ie_estagio,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'ie_estagio_old= ' || :old.ie_estagio || ' ie_estagio_new= ' || :new.ie_estagio;
			end if;

			if	(nvl(:new.ie_status,0) <>  nvl(:old.ie_status,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'ie_status_old= ' || :old.ie_status || ' ie_estagio_new= ' || :new.ie_status;
			end if;

			if	(ds_log_w is not null) then
				ie_tipo_alteracao_w	:= 'A';
			end if;
		elsif	(deleting) then
			ie_tipo_alteracao_w	:= 'E';

			ds_log_w	:= 'Guia: ' || :new.cd_guia;
		end if;

		if	(ie_tipo_alteracao_w is not null) then
			insert into pls_log_alt_autor_bd
				(nr_sequencia,
				dt_alteracao,
				nr_seq_guia_plano,
				nr_seq_guia_plano_proc,
				nr_seq_guia_plano_mat,
				ie_tipo_alteracao,
				ds_log,
				nm_maquina)
			values	(pls_log_alt_autor_bd_seq.nextval,
				sysdate,
				:new.nr_sequencia,
				null,
				null,
				ie_tipo_alteracao_w,
				ds_log_w,
				nm_machine_w);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_GUIA_PLANO_tp  after update ON TASY.PLS_GUIA_PLANO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CONSELHO,1,4000),substr(:new.NR_SEQ_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONSELHO',ie_log_w,ds_w,'PLS_GUIA_PLANO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_GUIA_PLANO_UPDATE
before update ON TASY.PLS_GUIA_PLANO for each row
declare

--pragma autonomous_transaction;

qt_item_invalido_w	number(10)	:= 0;
ds_estagio_w		varchar2(255)	:= null;
ds_observacao_w		varchar2(4000);
detalhes_exec_w		varchar2(2000);
nr_seq_prot_atend_w	pls_protocolo_atendimento.nr_sequencia%type;
qt_proc_diarias_w	number(10);

begin

if	(:new.ie_pagamento_automatico = 'PA' and :new.nr_seq_pgto_aut is null) then
	ds_observacao_w	:= 'Guia setada para pagamento autom�tico sem regra de pagamento informada.'|| chr(13) || chr(10) ||
			   'Aplica��o Tasy: '||pls_se_aplicacao_tasy || chr(13) || chr(10) ||
			   'Fun��o ativa: '||obter_funcao_ativa || chr(13) || chr(10) ||
			   'Nome m�quina: '||wheb_usuario_pck.get_machine|| chr(13) || chr(10) ||
			   'Usu�rio: '||nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado ');

	insert into 	pls_guia_plano_historico(nr_sequencia, nm_usuario, dt_atualizacao,
						 nr_seq_guia, dt_historico, ie_tipo_historico,
						 ie_origem_historico, ds_observacao)
					values	(pls_guia_plano_historico_seq.nextval, :new.nm_usuario, sysdate,
						 :new.nr_sequencia, sysdate, 'L',
						 'A', ds_observacao_w);
end if;


if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	/* Consistir o est�gio da guia com o status dos itens da guia */
	if	(nvl(:old.ie_estagio,0) <> 9 and (nvl(:new.ie_estagio,0) <> nvl(:old.ie_estagio,0))) then

		if	(pls_se_estagio_aut_valido(:new.nr_sequencia,:new.ie_estagio) = 'N') then
			/* Retorna fun��o que est� sendo executada e Call Stack*/
			ds_estagio_w	:= obter_valor_dominio(2055,:new.ie_estagio);

			detalhes_exec_w	:= substr(pls_obter_detalhe_exec(true),1,2000);

			ds_observacao_w	:= 	'Mudan�a de est�gio n�o permitida ' || chr(13) || chr(10) ||
						'Est�gio anterior: ' || obter_valor_dominio(2055,:old.ie_estagio) || chr(13) || chr(10) ||
						'Est�gio novo: ' || obter_valor_dominio(2055,:new.ie_estagio)|| chr(13) || chr(10) ||
						'Detalhes t�cnicos: ' || chr(13) || chr(10) ||
						detalhes_exec_w;

			/* Gravar em log, qual a guia, est�gio que foi alterado e stack de execu��o */
			insert into pls_guia_plano_historico
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nr_seq_guia,
				dt_historico,
				ie_tipo_historico,
				ie_origem_historico,
				ds_observacao)
			values	(pls_guia_plano_historico_seq.nextval,
				:new.nm_usuario,
				sysdate,
				:new.nr_sequencia,
				sysdate,
				'EI',
				'A',
				ds_observacao_w);

		end if;
	end if;
end if;

if	(nvl(:old.ie_status,0) <> 1 and nvl(:new.ie_status,0) = 1) then
	select	nvl(sum(a.qt_autorizada), 0)
	into	qt_proc_diarias_w
	from	pls_guia_plano_proc a,
		procedimento b
	where	a.nr_seq_guia		= :new.nr_sequencia
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced 	= b.ie_origem_proced
	and	b.ie_classificacao 	= '3'; 			--Di�rias

	if (qt_proc_diarias_w > 0) then
		:new.qt_dia_autorizado	:= qt_proc_diarias_w;
	end if;
end if;

begin
	select	nr_sequencia
	into	nr_seq_prot_atend_w
	from	pls_protocolo_atendimento
	where 	nr_seq_guia	= :new.nr_sequencia;
exception
when others then
	nr_seq_prot_atend_w	:= null;
end;
if (nr_seq_prot_atend_w is not null) then
	if 	(nvl(:old.ie_estagio, 0) <> nvl(:new.ie_estagio, 0)) and
		(:old.ie_estagio in (1,2,3,9,11,12)) and
		(:new.ie_estagio in (4,5,6,10)) then

		update	pls_protocolo_atendimento
		set	ie_status	= 3,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao 	= sysdate
		where	nr_sequencia	= nr_seq_prot_atend_w;

	elsif	(nvl(:old.ie_estagio, 0) <> nvl(:new.ie_estagio, 0)) and
		(:new.ie_estagio = 8) then

		update	pls_protocolo_atendimento
		set	ie_status	= 4,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao 	= sysdate
		where	nr_sequencia	= nr_seq_prot_atend_w;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_guia_plano_delete
before delete ON TASY.PLS_GUIA_PLANO for each row
declare

begin

if	(:old.nr_seq_evento_atend is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1042262);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_guia_plano_atual
before insert or update ON TASY.PLS_GUIA_PLANO for each row
declare

vl_procedimentos_w	Number(15,2);
vl_materiais_w		Number(15,4);

begin

if	(:new.nr_seq_segurado is not null) then

	select	nvl(max(ie_tipo_segurado),'B')
	into	:new.ie_tipo_segurado
	from	pls_segurado
	where	nr_sequencia	= :new.nr_seq_segurado;

	/* Francisco - 23/05/2012 - Performance */
	if	(nvl(:new.cd_guia, 'X') <> nvl(:old.cd_guia, 'X')) then
		:new.cd_guia_pesquisa	:= pls_converte_cd_guia_pesquisa(:new.cd_guia);
	end if;
end if;

-- se for guia de SP/SADT e prorroga��o de interna��o verifica se tem uma guia principal
-- se tiver ela deve ser considerada
if	(:new.ie_tipo_guia in ('2', '8')) then
	if	(:new.nr_seq_guia_principal is not null) then
		:new.nr_seq_guia_ok := :new.nr_seq_guia_principal;
	end if;
end if;
-- se acima n�o atribuiu valor lan�a o valor da sequencia
if	(:new.nr_seq_guia_ok is null) then
	:new.nr_seq_guia_ok := :new.nr_sequencia;
end if;

select	nvl(sum(vl_procedimento*qt_autorizada),0)
into	vl_procedimentos_w
from	pls_guia_plano_proc
where	nr_seq_guia	= :new.nr_sequencia;

if (vl_procedimentos_w = 0) then
	select	nvl(sum(vl_procedimento*qt_solicitada),0)
	into	vl_procedimentos_w
	from	pls_guia_plano_proc
	where	nr_seq_guia	= :new.nr_sequencia;
end if;

select	nvl(sum(vl_material*qt_autorizada),0)
into	vl_materiais_w
from	pls_guia_plano_mat
where	nr_seq_guia	= :new.nr_sequencia;

if (vl_materiais_w = 0) then
	select	nvl(sum(vl_material*qt_solicitada),0)
	into	vl_materiais_w
	from	pls_guia_plano_mat
	where	nr_seq_guia	= :new.nr_sequencia;
end if;

:new.vl_autorizacao	:= vl_procedimentos_w + vl_materiais_w;

end pls_guia_plano_atual;
/


ALTER TABLE TASY.PLS_GUIA_PLANO ADD (
  CONSTRAINT PLSGUIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GUIA_PLANO ADD (
  CONSTRAINT PLSGUIA_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PREST_SOLIC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSLAGA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ANEXO_GUIA) 
 REFERENCES TASY.PLS_LOTE_ANEXO_GUIAS_AUT (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_CONPROF_FK 
 FOREIGN KEY (NR_SEQ_CONSELHO) 
 REFERENCES TASY.CONSELHO_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PLSGUIA_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_WEB) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSCATS_FK 
 FOREIGN KEY (NR_SEQ_CAT) 
 REFERENCES TASY.PLS_CAT (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSGUIA_FK2 
 FOREIGN KEY (NR_SEQ_GUIA_ORIGEM) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSRCOC_FK 
 FOREIGN KEY (NR_SEQ_REGRA_COMPL) 
 REFERENCES TASY.PLS_REGRA_CONTA_COMPL (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSMPRA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PRORROGACAO) 
 REFERENCES TASY.PLS_MOTIVO_PRORROGACAO_AUT (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_UNI_EXEC) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLICITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSGUIA_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSGUIA_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PRINCIPAL) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSMOLIB_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_LIB) 
 REFERENCES TASY.PLS_MOTIVO_LIBERACAO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSGUMC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_GUIA_MOTIVO_CANCEL (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PLS) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSGUIA_PLSASEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_ATEND) 
 REFERENCES TASY.PLS_ATENDIMENTO_EVENTO (NR_SEQUENCIA));

GRANT UPDATE ON TASY.PLS_GUIA_PLANO TO DWONATAM;

GRANT SELECT ON TASY.PLS_GUIA_PLANO TO NIVEL_1;

GRANT UPDATE ON TASY.PLS_GUIA_PLANO TO WESLLEN;

