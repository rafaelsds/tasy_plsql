ALTER TABLE TASY.SAN_RESERVA_PROD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_RESERVA_PROD CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_RESERVA_PROD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RESERVA       NUMBER(10)               NOT NULL,
  NR_SEQ_PRODUCAO      NUMBER(10)               NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROPACI       NUMBER(10),
  DT_RECEBIMENTO       DATE,
  NR_AGRUPAMENTO       NUMBER(10),
  IE_STATUS_ADEP       VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DS_FENOTIPO_PADRAO   VARCHAR2(255 BYTE),
  QT_UNIDADE           NUMBER(8)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANREPR_PK ON TASY.SAN_RESERVA_PROD
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANREPR_PROPACI_FK_I ON TASY.SAN_RESERVA_PROD
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANREPR_SANPROD_FK_I ON TASY.SAN_RESERVA_PROD
(NR_SEQ_PRODUCAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANREPR_SANRESE_FK_I ON TASY.SAN_RESERVA_PROD
(NR_SEQ_RESERVA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANREPR_UK ON TASY.SAN_RESERVA_PROD
(NR_SEQ_RESERVA, NR_SEQ_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.San_reserva_prod_Update
BEFORE UPDATE ON TASY.SAN_RESERVA_PROD FOR EACH ROW
declare
IE_RESERVA_NAO_UTILIZADA_W	varchar2(1);
qt_procedimento_w		number(5);
begin

select 	max(nvl(IE_RESERVA_NAO_UTILIZADA,'N'))
into 	IE_RESERVA_NAO_UTILIZADA_w
from 	san_parametro;

if	(:new.ie_status = 'N') and
	(:new.ie_status <> :old.ie_status) and
	(IE_RESERVA_NAO_UTILIZADA_w <> 'N')then

	:new.nr_seq_propaci := null;

	if 	(IE_RESERVA_NAO_UTILIZADA_w = 'C') and
		(:old.nr_seq_propaci is not null) then

		select nvl(max(qt_procedimento),0)
		into	qt_procedimento_w
		from procedimento_paciente
		where nr_sequencia = :old.nr_seq_propaci;

		if	(qt_procedimento_w > 1) then
			update procedimento_paciente
			set qt_procedimento = qt_procedimento - 1
			where nr_sequencia = :old.nr_seq_propaci;

			update procedimento_paciente
			set qt_procedimento = qt_procedimento - 1
			where nr_seq_proc_princ = :old.nr_seq_propaci;

			update material_atend_paciente
			set qt_material = qt_material - 1
			where nr_seq_proc_princ = :old.nr_seq_propaci;
		else
			delete procedimento_paciente
			where nr_seq_proc_princ = :old.nr_seq_propaci;

			delete material_atend_paciente
			where nr_seq_proc_princ = :old.nr_seq_propaci;

			delete procedimento_paciente
			where nr_sequencia = :old.nr_seq_propaci;
		end if;
	end if;
end if;

if 	(:new.ie_status <> 'N') and
	(:old.nr_seq_propaci is not null) and
	(:new.nr_seq_propaci is null) then

	select nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from procedimento_paciente
	where nr_sequencia = :old.nr_seq_propaci;

	if	(qt_procedimento_w > 1) then
		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_sequencia = :old.nr_seq_propaci;

		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		update material_atend_paciente
		set qt_material = qt_material - 1
		where nr_seq_proc_princ = :old.nr_seq_propaci;
	else
		delete procedimento_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		delete material_atend_paciente
		where nr_seq_proc_princ = :old.nr_seq_propaci;

		delete procedimento_paciente
		where nr_sequencia = :old.nr_seq_propaci;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.San_reserva_prod_Delete
before delete ON TASY.SAN_RESERVA_PROD for each row
declare
nr_prescricao_w san_reserva_item.nr_prescricao%type;
nr_seq_procedimento_w san_reserva_item.nr_seq_prescr%type;
nr_seq_horario_w prescr_proc_bolsa.nr_seq_horario%type;
nm_usuario_w san_reserva_item.nm_usuario%type;
pragma autonomous_transaction;

begin

select  max(b.nr_prescricao),
		max(b.nr_seq_prescr),
		max(b.nm_usuario)
into	nr_prescricao_w,
		nr_seq_procedimento_w,
		nm_usuario_w
from 	san_reserva a,
		san_reserva_item b
where 	a.nr_sequencia = b.nr_seq_reserva
and 	a.nr_sequencia = :old.nr_seq_reserva;

select 	max(nr_sequencia)
into	nr_seq_horario_w
from 	prescr_proc_hor
where 	nr_prescricao = nr_prescricao_w
and		nr_seq_procedimento = nr_seq_procedimento_w
and		nvl(ie_aprazado, 'N') = 'S';

Deletar_registro_ISBT(nr_prescricao_w, nr_seq_procedimento_w, null, nm_usuario_w, nr_seq_horario_w);

if (nr_seq_horario_w is not null) then
	suspender_prescr_proc_hor(nr_seq_horario_w, nm_usuario_w);
end if;

end;
/


ALTER TABLE TASY.SAN_RESERVA_PROD ADD (
  CONSTRAINT SANREPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT SANREPR_UK
 UNIQUE (NR_SEQ_RESERVA, NR_SEQ_PRODUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_RESERVA_PROD ADD (
  CONSTRAINT SANREPR_SANPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUCAO) 
 REFERENCES TASY.SAN_PRODUCAO (NR_SEQUENCIA),
  CONSTRAINT SANREPR_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT SANREPR_SANRESE_FK 
 FOREIGN KEY (NR_SEQ_RESERVA) 
 REFERENCES TASY.SAN_RESERVA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_RESERVA_PROD TO NIVEL_1;

