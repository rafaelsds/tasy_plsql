ALTER TABLE TASY.W_IMP_PAT_BEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_IMP_PAT_BEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_IMP_PAT_BEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_BEM               VARCHAR2(20 BYTE)        NOT NULL,
  DS_BEM               VARCHAR2(255 BYTE)       NOT NULL,
  DT_AQUISICAO         DATE                     NOT NULL,
  DT_INICIO_USO        DATE,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  IE_IMOBILIZADO       VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  CD_CENTRO_CUSTO      NUMBER(8),
  IE_TIPO_VALOR        VARCHAR2(3 BYTE)         NOT NULL,
  TX_DEPREC            NUMBER(15,4)             NOT NULL,
  TX_FISCAL            NUMBER(15,4),
  CD_CGC               VARCHAR2(14 BYTE),
  NR_NOTA_FISCAL       VARCHAR2(255 BYTE),
  NR_SEQ_ADICAO        NUMBER(5),
  DS_SERIE             VARCHAR2(40 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  VL_ORIGINAL          NUMBER(15,2)             NOT NULL,
  NR_SEQ_MARCA         NUMBER(10),
  IE_SUBVENCAO         VARCHAR2(1 BYTE),
  DT_GARANTIA          DATE,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_CRIT_RATEIO   NUMBER(10),
  CD_BEM_EXTERNO       VARCHAR2(20 BYTE),
  VL_RESIDUAL          NUMBER(15,2),
  IE_PROPRIEDADE       VARCHAR2(15 BYTE),
  VL_MERCADO           NUMBER(15,2),
  NR_SEQ_REGRA_CONTA   NUMBER(10),
  QT_TEMPO_VIDA_UTIL   NUMBER(10),
  DT_VALOR             DATE,
  VL_BASE_DEPREC       NUMBER(22,4),
  VL_CONTABIL          NUMBER(22,4),
  VL_DEPREC_ACUM       NUMBER(22,4),
  VL_DEPREC_MES        NUMBER(22,4),
  DS_CONSISTENCIA      VARCHAR2(4000 BYTE),
  IE_STATUS_IMP        VARCHAR2(15 BYTE),
  NR_SEQ_BEM           NUMBER(10),
  CD_MOEDA             NUMBER(5)                NOT NULL,
  CD_BEM_SUPERIOR      VARCHAR2(20 BYTE),
  NR_LINHA             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WIMPATBEM_MOEDA_FK_I ON TASY.W_IMP_PAT_BEM
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WIMPATBEM_PK ON TASY.W_IMP_PAT_BEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WIMPATBEM_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.W_IMP_PAT_BEM_tp  after update ON TASY.W_IMP_PAT_BEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_BEM,1,500);gravar_log_alteracao(substr(:old.CD_BEM,1,4000),substr(:new.CD_BEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_BEM',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_BEM,1,500);gravar_log_alteracao(substr(:old.DS_BEM,1,4000),substr(:new.DS_BEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_BEM',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROPRIEDADE,1,500);gravar_log_alteracao(substr(:old.IE_PROPRIEDADE,1,4000),substr(:new.IE_PROPRIEDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROPRIEDADE',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONTA_CONTABIL,1,500);gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'W_IMP_PAT_BEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.W_IMP_PAT_BEM ADD (
  CONSTRAINT WIMPATBEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_IMP_PAT_BEM ADD (
  CONSTRAINT WIMPATBEM_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.W_IMP_PAT_BEM TO NIVEL_1;

