ALTER TABLE TASY.EIS_SALDO_ESTOQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIS_SALDO_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_SALDO_ESTOQUE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_REFERENCIA         DATE                    NOT NULL,
  CD_LOCAL_ESTOQUE      NUMBER(4)               NOT NULL,
  IE_PERIODO            VARCHAR2(1 BYTE)        NOT NULL,
  VL_ESTOQUE            NUMBER(20,2),
  CD_SUBGRUPO_MATERIAL  NUMBER(3)               NOT NULL,
  CD_GRUPO_MATERIAL     NUMBER(3)               NOT NULL,
  CD_CLASSE_MATERIAL    NUMBER(5)               NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  IE_PADRONIZADO        VARCHAR2(1 BYTE)        NOT NULL,
  IE_CURVA_ABC          VARCHAR2(1 BYTE)        NOT NULL,
  QT_ESTOQUE            NUMBER(22,4),
  VL_ESTOQUE_MEDIO      NUMBER(20,2),
  VL_CONSUMO            NUMBER(20,2),
  VL_ESTOQUE_MAXIMO     NUMBER(20,2),
  VL_ESTOQUE_CALC_MAX   NUMBER(20,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISSAES_CLAMATE_FK_I ON TASY.EIS_SALDO_ESTOQUE
(CD_CLASSE_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISSAES_GRUMATE_FK_I ON TASY.EIS_SALDO_ESTOQUE
(CD_GRUPO_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISSAES_I1 ON TASY.EIS_SALDO_ESTOQUE
(DT_REFERENCIA, CD_ESTABELECIMENTO, CD_LOCAL_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISSAES_I2 ON TASY.EIS_SALDO_ESTOQUE
(CD_ESTABELECIMENTO, IE_PERIODO, DT_REFERENCIA, IE_CURVA_ABC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISSAES_LOCESTO_FK_I ON TASY.EIS_SALDO_ESTOQUE
(CD_LOCAL_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISSAES_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EISSAES_MATERIA_FK_I ON TASY.EIS_SALDO_ESTOQUE
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EISSAES_PK ON TASY.EIS_SALDO_ESTOQUE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISSAES_PK
  MONITORING USAGE;


CREATE INDEX TASY.EISSAES_SUBMATE_FK_I ON TASY.EIS_SALDO_ESTOQUE
(CD_SUBGRUPO_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EIS_SALDO_ESTOQUE ADD (
  CONSTRAINT EISSAES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          896K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EIS_SALDO_ESTOQUE ADD (
  CONSTRAINT EISSAES_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT EISSAES_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT EISSAES_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT EISSAES_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT EISSAES_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.EIS_SALDO_ESTOQUE TO NIVEL_1;

