ALTER TABLE TASY.REGRA_CID_PRE_AGEND_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CID_PRE_AGEND_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CID_PRE_AGEND_ESP
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_PRE_AGEND  NUMBER(10),
  CD_ESPECIALIDADE        NUMBER(10)            NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECIREE_ESPMEDI_FK_I ON TASY.REGRA_CID_PRE_AGEND_ESP
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECIREE_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RECIREE_PK ON TASY.REGRA_CID_PRE_AGEND_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECIREE_PK
  MONITORING USAGE;


CREATE INDEX TASY.RECIREE_REGCIAG_FK_I ON TASY.REGRA_CID_PRE_AGEND_ESP
(NR_SEQ_REGRA_PRE_AGEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RECIREE_REGCIAG_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_CID_PRE_AGEND_ESP ADD (
  CONSTRAINT RECIREE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CID_PRE_AGEND_ESP ADD (
  CONSTRAINT RECIREE_REGCIAG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRE_AGEND) 
 REFERENCES TASY.REGRA_CID_PRE_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT RECIREE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.REGRA_CID_PRE_AGEND_ESP TO NIVEL_1;

