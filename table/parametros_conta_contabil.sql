ALTER TABLE TASY.PARAMETROS_CONTA_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETROS_CONTA_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETROS_CONTA_CONTABIL
(
  CD_SEQUENCIA_PARAMETRO      NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  CD_CONTA_RECEITA            VARCHAR2(20 BYTE),
  CD_CONTA_ESTOQUE            VARCHAR2(20 BYTE),
  CD_CONTA_PASSAG_DIRETA      VARCHAR2(20 BYTE),
  CD_GRUPO_MATERIAL           NUMBER(3),
  CD_SUBGRUPO_MATERIAL        NUMBER(5),
  CD_CLASSE_MATERIAL          NUMBER(5),
  CD_MATERIAL                 NUMBER(6),
  CD_AREA_PROCED              NUMBER(8),
  CD_ESPECIAL_PROCED          NUMBER(8),
  CD_GRUPO_PROCED             NUMBER(15),
  CD_PROCEDIMENTO             NUMBER(15),
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  IE_ORIGEM_PROCED            NUMBER(10),
  CD_CONTA_DESPESA            VARCHAR2(20 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  IE_TIPO_ATENDIMENTO         NUMBER(3),
  IE_CLASSIF_CONVENIO         VARCHAR2(3 BYTE),
  CD_CONVENIO                 NUMBER(5),
  IE_CLINICA                  NUMBER(5),
  CD_LOCAL_ESTOQUE            NUMBER(4),
  CD_OPERACAO_ESTOQUE         NUMBER(10),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_CENTRO_CUSTO             NUMBER(15),
  DT_INICIO_VIGENCIA          DATE,
  DT_FIM_VIGENCIA             DATE,
  NR_SEQ_GRUPO                NUMBER(10),
  NR_SEQ_SUBGRUPO             NUMBER(10),
  NR_SEQ_FORMA_ORG            NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  CD_CATEGORIA_CONVENIO       VARCHAR2(10 BYTE),
  IE_TIPO_CONVENIO            NUMBER(2),
  CD_PLANO                    VARCHAR2(10 BYTE),
  CD_CENTRO_CUSTO_RECEITA     NUMBER(10),
  CD_CONTA_REC_PACOTE         VARCHAR2(20 BYTE),
  IE_TIPO_CENTRO              NUMBER(3),
  IE_TIPO_TRIBUTO_ITEM        VARCHAR2(15 BYTE),
  IE_COMPLEXIDADE_SUS         VARCHAR2(2 BYTE),
  IE_TIPO_FINANC_SUS          VARCHAR2(4 BYTE),
  CD_CONTA_AJUSTE_PROD        VARCHAR2(20 BYTE),
  CD_HISTORICO                NUMBER(10),
  CD_CONTA_DESP_PRE_FATUR     VARCHAR2(20 BYTE),
  CD_EMPRESA                  NUMBER(4)         NOT NULL,
  CD_CONTA_REDUT_RECEITA      VARCHAR2(20 BYTE),
  CD_CONTA_GRATUIDADE         VARCHAR2(20 BYTE),
  NR_SEQ_MOTIVO_SOLIC         NUMBER(10),
  IE_CONSISTE_PROC_PRINC_AIH  VARCHAR2(1 BYTE),
  CD_OPERACAO_NF              NUMBER(4),
  CD_CONTA_PERDA_PRE_FAT      VARCHAR2(20 BYTE),
  IE_SITUACAO_CONTA_PAC       VARCHAR2(15 BYTE),
  CD_CONTA_SUBVENCAO          VARCHAR2(20 BYTE),
  CD_CONTA_ESTOQUE_TERC       VARCHAR2(20 BYTE),
  NR_SEQ_FAMILIA              NUMBER(10),
  CD_NATUREZA_OPERACAO        NUMBER(15),
  NR_SEQ_PROC_INTERNO         NUMBER(10),
  CD_SEQUENCIA_PARAM_REF      NUMBER(10),
  NR_SEQ_REGRA_VALOR          NUMBER(10),
  NR_SEQ_PROJ_REC             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARCOCO_AREPROC_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_AREA_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CATCONV_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONVENIO, CD_CATEGORIA_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CENCUST_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CENCUST_FK1_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CENTRO_CUSTO_RECEITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CENCUST_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CLAMATE_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONE_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONP_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_PASSAG_DIRETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONR_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_RECEITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_DESP_PRE_FATUR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK1_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_REC_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONT_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK2_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_AJUSTE_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK3_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_REDUT_RECEITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONT_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK4_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_GRATUIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONCONT_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONCONT_FK5_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_PERDA_PRE_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_CONCONT_FK6_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_SUBVENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_CONDESP_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_DESPESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONDESP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONPLAN_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_CONTEST_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONTA_ESTOQUE_TERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_CONVENI_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_EMPRESA_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_ESPPROC_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_ESPECIAL_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_ESTABEL_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_GRUMATE_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_GRUPROC_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_GRUPO_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_HISPADR_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_I1 ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_GRUPO_MATERIAL, CD_SUBGRUPO_MATERIAL, CD_CLASSE_MATERIAL, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_I1
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_I2 ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_AREA_PROCED, CD_ESPECIAL_PROCED, CD_GRUPO_PROCED, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_I2
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_LOCESTO_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_LOCAL_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_MATERIA_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_MATFAMI_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_MOSOCOM_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_MOTIVO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_NATOPER_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_OPEESTO_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_OPERACAO_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_OPEESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_OPENOTA_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_PARCOCO_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_SEQUENCIA_PARAM_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_PATRVC_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_REGRA_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARCOCO_PK ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_SEQUENCIA_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_PROCEDI_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_PROINTE_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_PRORECU_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARCOCO_SETATEN_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_SUBGRMA_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_SUBGRMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_SUSFORG_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_SUSGRUP_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCOCO_SUSSUBG_FK_I ON TASY.PARAMETROS_CONTA_CONTABIL
(NR_SEQ_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCOCO_SUSSUBG_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.param_conta_contabil_atual
before insert or update ON TASY.PARAMETROS_CONTA_CONTABIL for each row
declare

ds_erro_w				varchar2(2000);
cd_estabelecimento_w	number(4);
cd_empresa_w			number(4);

ds_receita_w			varchar2(40);
ds_estoque_w			varchar2(40);
ds_despesa_w			varchar2(40);
ds_gratuidade_w			varchar2(40);
ds_desp_pre_fatur_w		varchar2(40);
ds_redut_receita_w 		varchar2(40);
ds_passag_direta_w 		varchar2(40);
ds_rec_pacote_w	   		varchar2(40);

begin

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

ds_receita_w		:= substr(wheb_mensagem_pck.get_texto(304171,''),1,40);
ds_estoque_w		:= substr(wheb_mensagem_pck.get_texto(304174,''),1,40);
ds_despesa_w		:= substr(wheb_mensagem_pck.get_texto(304175,''),1,40);
ds_gratuidade_w		:= substr(wheb_mensagem_pck.get_texto(354047,''),1,40);
ds_desp_pre_fatur_w := substr(wheb_mensagem_pck.get_texto(354048,''),1,40);
ds_redut_receita_w  := substr(wheb_mensagem_pck.get_texto(354049,''),1,40);
ds_passag_direta_w  := substr(wheb_mensagem_pck.get_texto(354050,''),1,40);
ds_rec_pacote_w	    := substr(wheb_mensagem_pck.get_texto(354051,''),1,40);



if	(:new.cd_empresa is null) then
	begin
	cd_estabelecimento_w	:= nvl(:new.cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento);

	begin
	select	cd_empresa
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception when others then
		cd_empresa_w	:= null;
	end;

	:new.cd_empresa	:= cd_empresa_w;
	end;
end if;

ctb_consistir_conta_titulo(:new.cd_conta_receita, 			ds_receita_w);
ctb_consistir_conta_titulo(:new.cd_conta_estoque, 			ds_estoque_w);
ctb_consistir_conta_titulo(:new.cd_conta_despesa, 			ds_despesa_w);
ctb_consistir_conta_titulo(:new.cd_conta_gratuidade, 		ds_gratuidade_w);
ctb_consistir_conta_titulo(:new.cd_conta_desp_pre_fatur, 	ds_desp_pre_fatur_w);
ctb_consistir_conta_titulo(:new.cd_conta_redut_receita, 	ds_redut_receita_w);
ctb_consistir_conta_titulo(:new.cd_conta_passag_direta, 	ds_passag_direta_w);
ctb_consistir_conta_titulo(:new.cd_conta_rec_pacote, 		ds_rec_pacote_w);

ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_receita, 			ds_receita_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_estoque, 			ds_estoque_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_despesa, 			ds_despesa_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_gratuidade, 		ds_gratuidade_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_desp_pre_fatur, 	ds_desp_pre_fatur_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_redut_receita, 	ds_redut_receita_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_passag_direta, 	ds_passag_direta_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_rec_pacote, 		ds_rec_pacote_w);


con_consiste_vigencia_conta(:new.cd_conta_receita, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_estoque, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_despesa, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_gratuidade, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_desp_pre_fatur, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_redut_receita, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_passag_direta, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_rec_pacote, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

end;
/


ALTER TABLE TASY.PARAMETROS_CONTA_CONTABIL ADD (
  CONSTRAINT PARCOCO_PK
 PRIMARY KEY
 (CD_SEQUENCIA_PARAMETRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETROS_CONTA_CONTABIL ADD (
  CONSTRAINT PARCOCO_CONCONT_FK3 
 FOREIGN KEY (CD_CONTA_REDUT_RECEITA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CONCONT_FK4 
 FOREIGN KEY (CD_CONTA_GRATUIDADE) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_MOSOCOM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SOLIC) 
 REFERENCES TASY.MOTIVO_SOLIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT PARCOCO_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_PERDA_PRE_FAT) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_SUBVENCAO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CONTEST_FK 
 FOREIGN KEY (CD_CONTA_ESTOQUE_TERC) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT PARCOCO_PARCOCO_FK 
 FOREIGN KEY (CD_SEQUENCIA_PARAM_REF) 
 REFERENCES TASY.PARAMETROS_CONTA_CONTABIL (CD_SEQUENCIA_PARAMETRO),
  CONSTRAINT PARCOCO_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_PATRVC_FK 
 FOREIGN KEY (NR_SEQ_REGRA_VALOR) 
 REFERENCES TASY.PAT_REGRA_VALOR_CONTA (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_DESP_PRE_FATUR) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PARCOCO_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT PARCOCO_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_AJUSTE_PROD) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PARCOCO_CONCONT_FK1 
 FOREIGN KEY (CD_CONTA_REC_PACOTE) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PARCOCO_CENCUST_FK1 
 FOREIGN KEY (CD_CENTRO_CUSTO_RECEITA) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PARCOCO_CONDESP_FK 
 FOREIGN KEY (CD_CONTA_DESPESA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PARCOCO_CONCONE_FK 
 FOREIGN KEY (CD_CONTA_ESTOQUE) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT PARCOCO_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCED) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PARCOCO_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT PARCOCO_CONCONP_FK 
 FOREIGN KEY (CD_CONTA_PASSAG_DIRETA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_CONCONR_FK 
 FOREIGN KEY (CD_CONTA_RECEITA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PARCOCO_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIAL_PROCED) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PARCOCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PARCOCO_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT PARCOCO_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROCED) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PARCOCO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PARCOCO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PARCOCO_SUBGRMA_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT PARCOCO_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT PARCOCO_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_ESTOQUE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE),
  CONSTRAINT PARCOCO_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA),
  CONSTRAINT PARCOCO_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA_CONVENIO) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA));

GRANT SELECT ON TASY.PARAMETROS_CONTA_CONTABIL TO NIVEL_1;

