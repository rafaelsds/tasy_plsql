DROP TABLE TASY.W_CRITERIO_DISTR_ORC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CRITERIO_DISTR_ORC
(
  NR_SEQ_DISTRIBUICAO      NUMBER(10),
  CD_SEQUENCIA_CRITERIO    NUMBER(10),
  CD_CENTRO_CONTROLE       NUMBER(8),
  CD_CENTRO_CONTROLE_DEST  NUMBER(8),
  QT_DISTRIBUICAO          NUMBER(15,4),
  QT_MESES_QTDADE          NUMBER(3),
  QT_JAN                   NUMBER(15,4),
  QT_FEV                   NUMBER(15,4),
  QT_MAR                   NUMBER(15,4),
  QT_ABR                   NUMBER(15,4),
  QT_MAI                   NUMBER(15,4),
  QT_JUN                   NUMBER(15,4),
  QT_JUL                   NUMBER(15,4),
  QT_AGO                   NUMBER(15,4),
  QT_SET                   NUMBER(15,4),
  QT_OUT                   NUMBER(15,4),
  QT_NOV                   NUMBER(15,4),
  QT_DEZ                   NUMBER(15,4),
  PR_DISTRIBUICAO          NUMBER(7,4),
  QT_MESES_PERCENT         NUMBER(3),
  PR_JAN                   NUMBER(7,4),
  PR_FEV                   NUMBER(7,4),
  PR_MAR                   NUMBER(7,4),
  PR_ABR                   NUMBER(7,4),
  PR_MAI                   NUMBER(7,4),
  PR_JUN                   NUMBER(7,4),
  PR_JUL                   NUMBER(7,4),
  PR_AGO                   NUMBER(7,4),
  PR_SET                   NUMBER(7,4),
  PR_OUT                   NUMBER(7,4),
  PR_NOV                   NUMBER(7,4),
  PR_DEZ                   NUMBER(7,4),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_CRITERIO_DISTR_ORC TO NIVEL_1;

