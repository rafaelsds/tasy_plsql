ALTER TABLE TASY.TITULO_RECEBER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER
(
  NR_TITULO                     NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_EMISSAO                    DATE            NOT NULL,
  DT_VENCIMENTO                 DATE            NOT NULL,
  DT_PAGAMENTO_PREVISTO         DATE            NOT NULL,
  VL_TITULO                     NUMBER(15,2)    NOT NULL,
  VL_SALDO_TITULO               NUMBER(15,2)    NOT NULL,
  VL_SALDO_JUROS                NUMBER(15,2)    NOT NULL,
  VL_SALDO_MULTA                NUMBER(15,2)    NOT NULL,
  CD_MOEDA                      NUMBER(5)       NOT NULL,
  CD_PORTADOR                   NUMBER(10)      NOT NULL,
  CD_TIPO_PORTADOR              NUMBER(5)       NOT NULL,
  TX_JUROS                      NUMBER(7,4)     NOT NULL,
  TX_MULTA                      NUMBER(7,4)     NOT NULL,
  CD_TIPO_TAXA_JURO             NUMBER(10)      NOT NULL,
  CD_TIPO_TAXA_MULTA            NUMBER(10)      NOT NULL,
  TX_DESC_ANTECIPACAO           NUMBER(7,4),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_EMISSAO_TITULO        NUMBER(5)       NOT NULL,
  IE_ORIGEM_TITULO              VARCHAR2(10 BYTE),
  IE_TIPO_TITULO                VARCHAR2(2 BYTE) NOT NULL,
  IE_TIPO_INCLUSAO              VARCHAR2(1 BYTE) NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  NR_INTERNO_CONTA              NUMBER(10),
  CD_CGC                        VARCHAR2(14 BYTE),
  CD_SERIE                      VARCHAR2(255 BYTE),
  NR_DOCUMENTO                  NUMBER(22),
  NR_SEQUENCIA_DOC              NUMBER(5),
  CD_BANCO                      NUMBER(5),
  CD_AGENCIA_BANCARIA           VARCHAR2(8 BYTE),
  NR_BLOQUETO                   VARCHAR2(44 BYTE),
  DT_LIQUIDACAO                 DATE,
  NR_LOTE_CONTABIL              NUMBER(10),
  DS_OBSERVACAO_TITULO          VARCHAR2(4000 BYTE),
  NR_ATENDIMENTO                NUMBER(10),
  NR_SEQ_PROTOCOLO              NUMBER(10),
  DT_CONTABIL                   DATE,
  NR_SEQ_CONTA_BANCO            NUMBER(10),
  DT_EMISSAO_BLOQUETO           DATE,
  NR_SEQ_CLASSE                 NUMBER(10),
  NR_GUIA                       VARCHAR2(20 BYTE),
  NR_NOSSO_NUMERO               VARCHAR2(20 BYTE),
  DT_REF_CONTA                  DATE,
  CD_CONVENIO_CONTA             NUMBER(5),
  NR_SEQ_TERC_CONTA             NUMBER(10),
  NR_SEQ_NF_SAIDA               NUMBER(10),
  NR_PROCESSO_AIH               NUMBER(10),
  CD_TIPO_RECEBIMENTO           NUMBER(5),
  NR_SEQ_CONTROLE_PESSOA        NUMBER(10),
  NR_SEQ_TRANS_FIN_CONTAB       NUMBER(10),
  NR_SEQ_CONTRATO               NUMBER(10),
  VL_DESC_PREVISTO              NUMBER(15,2),
  NR_SEQ_CARTEIRA_COBR          NUMBER(10),
  DT_INTEGRACAO_EXTERNA         DATE,
  CD_ESTAB_FINANCEIRO           NUMBER(4),
  NR_TITULO_EXTERNO             VARCHAR2(255 BYTE),
  NR_SEQ_MENSALIDADE            NUMBER(10),
  NR_SEQ_MENS_SEGURADO          NUMBER(10),
  IE_PLS                        VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_FIN_BAIXA        NUMBER(10),
  NR_NOTA_FISCAL                VARCHAR2(255 BYTE),
  NM_USUARIO_ORIG               VARCHAR2(15 BYTE),
  DT_INCLUSAO                   DATE,
  IE_TIPO_CARTEIRA              VARCHAR2(5 BYTE),
  NR_SEQ_EMPRESA                NUMBER(10),
  NR_SEQ_PROJ_REC               NUMBER(10),
  NR_SEQ_LOTE_EMPRESA           NUMBER(10),
  NR_SEQ_NEGOCIACAO_ORIGEM      NUMBER(10),
  NR_SEQ_PLS_LOTE_CAMARA        NUMBER(10),
  NR_SEQ_PTU_FATURA             NUMBER(10),
  DT_ULTIMA_DEVOLUCAO           DATE,
  NR_SEQ_LOTE_ENC_CONTAS        NUMBER(10),
  IE_LIQ_INADIMPLENCIA          VARCHAR2(1 BYTE),
  IE_NEGOCIADO                  VARCHAR2(1 BYTE),
  NR_SEQ_GUIA                   NUMBER(15),
  NR_TITULO_DEST                NUMBER(10),
  NR_SEQ_PAG_CONTA_EVENTO       NUMBER(10),
  VL_OUTRAS_DESPESAS            NUMBER(15,2),
  IE_DIG_NOSSO_NUMERO           VARCHAR2(2 BYTE),
  NR_SEQ_SEGURADO_MENS          NUMBER(10),
  NR_SEQ_EME_FATURA             NUMBER(10),
  NR_SEQ_PLS_LOTE_CONTEST       NUMBER(10),
  NR_SEQ_PLS_LOTE_DISC          NUMBER(10),
  NR_SEQ_GRUPO_PROD             NUMBER(10),
  NR_SEQ_COB_PREVIA             NUMBER(10),
  NR_SEQ_PAGADOR                NUMBER(10),
  VL_JUROS_BOLETO               NUMBER(15,2),
  VL_MULTA_BOLETO               NUMBER(15,2),
  NR_RPS                        NUMBER(15),
  NR_LIVRO                      VARCHAR2(255 BYTE),
  NR_SEQ_AGRUPAMENTO            NUMBER(10),
  NR_DUPLICATA                  NUMBER(15),
  NR_LOTE_CONTABIL_CURTO_PRAZO  NUMBER(10),
  NR_SEQ_TF_CURTO_PRAZO         NUMBER(10),
  NR_TITULO_EXT_NUMERICO        VARCHAR2(20 BYTE),
  DT_CREDITO_BANCARIO           DATE,
  IE_PRORROGACAO_PORTAL         VARCHAR2(1 BYTE),
  NR_SISTEMA_ANT                VARCHAR2(255 BYTE),
  VL_MULTA_FIXO                 NUMBER(15,2),
  NR_SEQ_LOTE_PROT              NUMBER(10),
  NR_SEQ_NOTA_DEB_CONCLUSAO     NUMBER(10),
  IE_DATA_JUROS_MULTA           VARCHAR2(50 BYTE),
  DT_ENVIO_SERASA               DATE,
  VL_ABATIMENTO                 NUMBER(15,2),
  NR_SEQ_CLIENTE                NUMBER(10),
  DT_ULTIMA_ALTERACAO           DATE,
  DT_LIMITE_DESCONTO            DATE,
  VL_TITULO_ESTRANG             NUMBER(15,2),
  VL_COTACAO                    NUMBER(21,10),
  NR_SEQ_PTU_FATURA_OLD         NUMBER(10),
  IE_INTEGRA_UNIMED             VARCHAR2(1 BYTE),
  IE_ENTRADA_CONFIRMADA         VARCHAR2(1 BYTE),
  IE_NEGOCIACAO_IMPRESSA        VARCHAR2(1 BYTE),
  NR_SEQ_APORTE_VENC            NUMBER(10),
  NR_SEQ_PLS_FATURA             NUMBER(10),
  NR_SEQ_CARTA                  NUMBER(10),
  DS_QRCODE                     VARCHAR2(4000 BYTE),
  DS_STACK                      VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRECE_AGEBANC_FK_I ON TASY.TITULO_RECEBER
(CD_BANCO, CD_AGENCIA_BANCARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_AGEBANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_ATEPACI_FK_I ON TASY.TITULO_RECEBER
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_BANCART_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CARTEIRA_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_BANESTAB_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_CLATIRE_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CLASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_CLATIRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_CONPACI_FK_I ON TASY.TITULO_RECEBER
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_CONPESS_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CONTROLE_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_CONPESS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_CONTRAT_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_CONVENI_FK_I ON TASY.TITULO_RECEBER
(CD_CONVENIO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_CRTCOMP_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_CARTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_DTPREVPAG_ESTAB ON TASY.TITULO_RECEBER
(DT_PAGAMENTO_PREVISTO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_EMEFATU_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_EME_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_EMEFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_ESTABEL_FK_I ON TASY.TITULO_RECEBER
(CD_ESTABELECIMENTO, IE_SITUACAO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_ESTABEL_FK2_I ON TASY.TITULO_RECEBER
(CD_ESTAB_FINANCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_ESTABEL_FK3_I ON TASY.TITULO_RECEBER
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_GRPRFIN_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_I1 ON TASY.TITULO_RECEBER
(DT_VENCIMENTO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I10 ON TASY.TITULO_RECEBER
(NR_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I11 ON TASY.TITULO_RECEBER
(NR_INTERNO_CONTA, NR_SEQ_PROTOCOLO, NR_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13432K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I12 ON TASY.TITULO_RECEBER
(NR_NOSSO_NUMERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          984K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_I12
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_I13 ON TASY.TITULO_RECEBER
(IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I2 ON TASY.TITULO_RECEBER
(DT_PAGAMENTO_PREVISTO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I3 ON TASY.TITULO_RECEBER
(DT_EMISSAO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I4 ON TASY.TITULO_RECEBER
(DT_LIQUIDACAO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I5 ON TASY.TITULO_RECEBER
(DT_CONTABIL, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I6 ON TASY.TITULO_RECEBER
(NR_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I7 ON TASY.TITULO_RECEBER
(IE_SITUACAO, CD_ESTABELECIMENTO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I8 ON TASY.TITULO_RECEBER
(CD_ESTABELECIMENTO, IE_SITUACAO, DT_VENCIMENTO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_I9 ON TASY.TITULO_RECEBER
(IE_SITUACAO, CD_ESTABELECIMENTO, DT_LIQUIDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_LOTCONT_FK_I ON TASY.TITULO_RECEBER
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_LOTCONT_FK2_I ON TASY.TITULO_RECEBER
(NR_LOTE_CONTABIL_CURTO_PRAZO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_LOTCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_LOTENCO_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_LOTE_ENC_CONTAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_LOTENCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_LOTEPRO_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_LOTE_PROT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_MOEDA_FK_I ON TASY.TITULO_RECEBER
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_NEGOCCR_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_NEGOCIACAO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_NEGOCCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_NOTFISC_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_NF_SAIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PESFISI_FK_I ON TASY.TITULO_RECEBER
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PESJURI_FK_I ON TASY.TITULO_RECEBER
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITRECE_PK ON TASY.TITULO_RECEBER
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLCONPAG_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLSCPSE_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_COB_PREVIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSCPSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSDELC_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_LOTE_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSDELC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSFATU_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PLS_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLSGUIA_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSGUIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSLOCC_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PLS_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSLOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSLOCO_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PLS_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSLODI_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PLS_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSMENS_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLSMESE_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_MENS_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSMESE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PLSPAIT_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PAG_CONTA_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLSRJAV_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_APORTE_VENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PLSSEME_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_SEGURADO_MENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PLSSEME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PORTADO_FK_I ON TASY.TITULO_RECEBER
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PROCONV_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_PRORECU_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PRORECU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PTUFATU_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_PTU_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PTUFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_PTUNOCC_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_NOTA_DEB_CONCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_PTUNOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_SUSAIHP_FK_I ON TASY.TITULO_RECEBER
(NR_PROCESSO_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_SUSAIHP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TERCONT_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_TERC_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECE_TIPRECE_FK_I ON TASY.TITULO_RECEBER
(CD_TIPO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TIPRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TITEXN_I ON TASY.TITULO_RECEBER
(NR_TITULO_EXT_NUMERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TITEXN_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TITRECE_FK_I ON TASY.TITULO_RECEBER
(NR_TITULO_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TRAFINA_FK_I ON TASY.TITULO_RECEBER
(NR_SEQ_TRANS_FIN_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TRAFINA_FK2_I ON TASY.TITULO_RECEBER
(NR_SEQ_TRANS_FIN_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECE_TRAFINA_FK3_I ON TASY.TITULO_RECEBER
(NR_SEQ_TF_CURTO_PRAZO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECE_TRAFINA_FK3_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_befdelete
BEFORE DELETE ON TASY.TITULO_RECEBER FOR EACH ROW
BEGIN

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then
/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TR',nvl(:old.dt_pagamento_previsto,:old.dt_vencimento),'E',:old.nm_usuario);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Titulo_Receber_Update
AFTER UPDATE ON TASY.TITULO_RECEBER FOR EACH ROW
DECLARE

nr_sequencia_w			number(10);
nm_atributo_w			varchar2(50);
ds_valor_ant_w			varchar2(50);
ds_valor_atual_w		varchar2(50);

ie_motivo_w				number(5);
ie_tipo_consistencia_w	number(5,0);
nr_seq_liq_w			number(10,0);
ie_tipo_convenio_w		number(2);
qt_reg_w				number(1);

nr_seq_pls_fatura_w		number(10);
nr_seq_fatura_w			number(10);
ie_tipo_ocorrencia_w	number(10);

cd_tipo_portador_w				portador.cd_tipo_portador%type;
nr_seq_conta_banco_w			portador.nr_seq_conta_banco%type;
ie_recalcula_nosso_num_w		varchar2(1);
vl_valor_altera_venc_w		number(15,2);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'S') or
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'S') then
	goto Final;
end if;

select nvl(max(nr_sequencia),0) + 1
into nr_sequencia_w
from titulo_receber_alt;

if	(:old.dt_emissao <> :new.dt_emissao) or
	(:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica) or
	(:old.cd_cgc <> :new.cd_cgc) then
	if (:old.dt_emissao <> :new.dt_emissao) then
		nm_atributo_w := 'DT_EMISSAO';
		ds_valor_ant_w := to_char(:old.dt_emissao,'dd/mm/yyyy hh24:mi:ss');
		ds_valor_atual_w := to_char(:new.dt_emissao,'dd/mm/yyyy hh24:mi:ss');
	elsif (:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica) then
		nm_atributo_w := 'CD_PESSOA_FISICA';
		ds_valor_ant_w := :old.cd_pessoa_fisica;
		ds_valor_atual_w := :new.cd_pessoa_fisica;
	elsif (:old.cd_cgc <> :new.cd_cgc) then
		nm_atributo_w := 'CD_CGC';
		ds_valor_ant_w := :old.cd_cgc;
		ds_valor_atual_w := :new.cd_cgc;
	end if;

	insert into Titulo_Receber_Alt (nr_titulo, nr_sequencia, nm_atributo, nm_usuario,
						  dt_atualizacao, ds_valor_ant, ds_valor_atual)
	values (:new.nr_titulo, nr_sequencia_w, nm_atributo_w, :new.nm_usuario,
		  sysdate, ds_valor_ant_w, ds_valor_atual_w);
end if;

/* Rotina criada por Marcus em 30/12/2006 para aprovar ordens de repasse a distribuidores Wheb */
if	(:new.nr_seq_nf_saida is not null) and
	(:new.vl_saldo_titulo <> :old.vl_saldo_titulo) then
	FIN_Aprovar_OC_Repasse(
		:new.nr_titulo,
		:new.nr_seq_nf_saida,
		:new.vl_titulo,
		:new.vl_saldo_titulo,
		:new.nm_usuario);
end if;

if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null))
	or
	((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(232927);
end if;

/* Ao alterar o vencimento do t�tulo, altera o vencimento da fatura que originou o t�tulo */
if	(trunc(:new.dt_pagamento_previsto) <> trunc(:old.dt_pagamento_previsto)) and
	(:old.dt_pagamento_previsto is not null) then
	begin
	select	nr_sequencia
	into	nr_seq_pls_fatura_w
	from	pls_fatura
	where	nr_titulo = :old.nr_titulo;
	exception
		when others then
		nr_seq_pls_fatura_w := null;
	end;

	if	(nr_seq_pls_fatura_w is not null) then
		update	pls_fatura
		set	dt_vencimento 	= :new.dt_pagamento_previsto,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_pls_fatura_w;

		begin
		select	nr_sequencia
		into	nr_seq_fatura_w
		from	ptu_fatura
		where	nr_seq_pls_fatura = nr_seq_pls_fatura_w;
		exception
			when others then
			nr_seq_fatura_w := null;
		end;

		if	(nr_seq_fatura_w is not null) then
			update	ptu_fatura
			set	dt_vencimento_fatura	= :new.dt_pagamento_previsto,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
			where	nr_sequencia		= nr_seq_fatura_w;
		end if;
	end if;
end if;
/* OS 1782010 - retirei daqui desta trigger pois estava dando mutante. Coloquei no afterpost do programa.
if (:new.cd_portador <> :old.cd_portador) then

	obter_param_usuario(801,211,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_recalcula_nosso_num_w);

	if (ie_recalcula_nosso_num_w in ('B','A')) then
		select 	cd_tipo_portador,
			nr_seq_conta_banco
		into	cd_tipo_portador_w,
			nr_seq_conta_banco_w
		from	portador
		where	cd_portador = :new.cd_portador;
		if (cd_tipo_portador_w = 1) and (nr_seq_conta_banco_w is not null) and (:new.nr_bloqueto is not null) then
			gerar_bloqueto_tit_rec(:new.nr_titulo, 'MTR', 'S');
		end if;
	end if;
end if;*/

/*Inicio tratamento para titulos de cobran�a que foram alterados*/
if (:new.ie_entrada_confirmada = 'C') then

	/*Altera��o de conta banc�ria*/
	if (:old.nr_seq_conta_banco <> :new.nr_seq_conta_banco) then

		ie_tipo_ocorrencia_w := 7;

		gerar_titulo_receber_instr ( :new.nr_titulo,
									 :new.vl_saldo_titulo,
									 :new.nm_usuario,
									 :new.cd_moeda,
									 0,
									 0,
									 :new.vl_saldo_titulo,
									 ie_tipo_ocorrencia_w,
									 :new.cd_pessoa_fisica,
									 :new.cd_cgc,
									 :old.nr_seq_conta_banco,
									 'TITULO_RECEBER_UPDATE');


	end if;

end if;

/*OS 1555588 - Como � um valor, fiz da mesma forma que o parametro 148 na proc consistir_calcular_cot_compra*/
begin
	select	nvl(max(obter_valor_param_usuario(801,214, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo)),0)
	into	vl_valor_altera_venc_w
	from	dual;
exception when others then
	vl_valor_altera_venc_w := 0;
end;

if (nvl(vl_valor_altera_venc_w,0) > 0) and (:new.vl_saldo_titulo > nvl(vl_valor_altera_venc_w,0)) and (:new.dt_pagamento_previsto <> :old.dt_pagamento_previsto) then
	--N�o ser� poss�vel alterar o vencimento do t�tulo. O saldo � maior que o valor m�mino informado no par�metro 214. Vl saldo: #@vl_saldo_titulo_w#@ Vl par�metro: #@vl_valor_altera_venc_w#@
	wheb_mensagem_pck.exibir_mensagem_abort(995290,	'vl_saldo_titulo_w=' || campo_mascara_virgula(:new.vl_saldo_titulo) ||
									';VL_VALOR_ALTERA_VENC_W=' || campo_mascara_virgula(vl_valor_altera_venc_w));
end if;
/*FIM OS 1555588 */

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.Titulo_Receber_Insert
BEFORE INSERT ON TASY.TITULO_RECEBER FOR EACH ROW
DECLARE

ie_titulo_prot_zerado_w		varchar2(20);
ie_historico_w			varchar2(2);
nr_sequencia_w			number(15);
cd_convenio_parametro_w		number(5);
nr_nivel_anterior_w		number(5);
ie_valor_tit_protocolo_w		varchar2(2);
cd_convenio_w			number(5);
tx_juros_w			number(7,4);
tx_multa_w			number(7,4);
ie_dt_contabil_ops_w	varchar2(1);
dt_contabil_ops_w		date;
qt_provisorio_w			number(10) := 0;

ds_module_log_w conta_paciente_hist.ds_module%type;
ds_call_stack_w conta_paciente_hist.ds_call_stack%type;

BEGIN

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

select	max(a.cd_convenio_parametro)
into	cd_convenio_w
from	conta_paciente a
where	a.nr_interno_conta	= :new.nr_interno_conta;

if 	(:new.vl_outras_despesas = 0) then

	OBTER_ACRESC_CR (:new.cd_estabelecimento,:new.cd_cgc,:new.vl_outras_despesas);

end if;

if	(cd_convenio_w	is null) then

	select	max(a.cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio a
	where	a.nr_seq_protocolo	= :new.nr_seq_protocolo;

end if;

select	nvl(max(ie_historico_conta),'N')
into	ie_historico_w
from	parametro_faturamento
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_emissao,'CR',null,cd_convenio_w,null,null) = 'N') then
	--O mes/dia financeiro de emissao do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes.
	Wheb_mensagem_pck.exibir_mensagem_abort(222067);
end if;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, nvl(:new.dt_contabil,:new.dt_emissao),'CR',null,cd_convenio_w,null,null) = 'N') then
	--O mes/dia financeiro da data contabil do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes. Data: #@DT_CONTABIL#@
	Wheb_mensagem_pck.exibir_mensagem_abort(222068,'DT_CONTABIL='||nvl(:new.dt_contabil,:new.dt_emissao));
end if;

if	(:new.vl_saldo_titulo < 0) or (:new.vl_titulo < 0) then
	--O valor do titulo nao pode ser negativo!
	Wheb_mensagem_pck.exibir_mensagem_abort(222069);
end if;

select	nvl(max(ie_titulo_prot_zerado),'N'),
	nvl(max(ie_valor_tit_protocolo),'S')
into	ie_titulo_prot_zerado_w,
	ie_valor_tit_protocolo_w
from	parametro_contas_receber
where	cd_estabelecimento		= :new.cd_estabelecimento;

-- Edgar 31/01/2008, OS 81434, coloquei consistencia abaixo ate achar o problema de geracao de titulos do protooclo com valor zerado
if	(ie_titulo_prot_zerado_w = 'P') and
	(:new.vl_titulo = 0)  and
	(:new.nr_seq_protocolo is not null) then
	--O valor do titulo de protocolo nao pode ser zero!
	Wheb_mensagem_pck.exibir_mensagem_abort(222070);
end if;

if	(ie_titulo_prot_zerado_w = 'N') and (:new.vl_titulo = 0) and (:new.ie_origem_titulo <> '3')  then
	--O valor do titulo nao pode ser zero!
	Wheb_mensagem_pck.exibir_mensagem_abort(222071);
end if;

--lhalves 19/10/2010, OS 259145.
if	(ie_valor_tit_protocolo_w = 'N') and
	(:new.nr_seq_protocolo is not null) and
	(:new.nr_interno_conta is null) then
	if	(:new.vl_titulo <> obter_total_protocolo(:new.nr_seq_protocolo)) then
		--O valor do titulo nao pode ser diferente do protocolo!
		Wheb_mensagem_pck.exibir_mensagem_abort(222072);
	end if;
end if;


if	(:new.dt_vencimento < to_date('01/01/1900', 'dd/mm/yyyy')) then
	--Nao e possivel incluir um titulo com vencimento inferior a 01/01/1900!
	Wheb_mensagem_pck.exibir_mensagem_abort(222073);
end if;

if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null))
	or
	((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
	--O titulo deve ser de uma pessoa fisica ou juridica!
	Wheb_mensagem_pck.exibir_mensagem_abort(222074);
end if;

qt_provisorio_w := 0;

if	(:new.nr_seq_protocolo is not null) then

	select	count(*)
	into	qt_provisorio_w
	from	protocolo_convenio x
	where	x.nr_seq_protocolo = :new.nr_seq_protocolo
	and	x.ie_status_protocolo = 1;

elsif	 (:new.nr_seq_lote_prot is not null) then

	select	count(*)
	into	qt_provisorio_w
	from	protocolo_convenio w
	where	w.nr_seq_lote_protocolo = :new.nr_seq_lote_prot
	and	w.ie_status_protocolo = 1;

end if;

if	(qt_provisorio_w > 0) then

	--Nao e permitido gerar o titulo para um protocolo com status Provisorio.
	wheb_mensagem_pck.exibir_mensagem_abort(338789);
end if;

:new.dt_contabil := nvl(:new.dt_contabil, :new.dt_emissao);

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_contabil);


if	(ie_historico_w		= 'S') and
	( :new.NR_INTERNO_CONTA  is not null) then
	begin
	select	conta_paciente_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	select	cd_convenio_parametro,
		decode(nr_seq_protocolo,null,6,8)
	into	cd_convenio_parametro_w,
		nr_nivel_anterior_w
	from	conta_paciente
	where 	nr_interno_conta	= :new.NR_INTERNO_CONTA;

	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into conta_paciente_hist(
		nr_sequencia, dt_atualizacao, nm_usuario,
		vl_conta, nr_seq_protocolo, nr_interno_conta,
		nr_nivel_anterior, nr_nivel_atual, dt_referencia,
		nr_atendimento, cd_convenio, dt_conta_protocolo,
		cd_funcao, ds_module, ds_call_stack)
	values	(
		nr_sequencia_w, sysdate, :new.nm_usuario,
		obter_valor_conta(:new.nr_interno_conta,0), null, :new.nr_interno_conta,
		nr_nivel_anterior_w,
		nr_nivel_anterior_w , sysdate,
		obter_atendimento_conta(:new.nr_interno_conta),
		cd_convenio_parametro_w,
		null,
		obter_funcao_Ativa, ds_module_log_w, ds_call_stack_w);
	exception
		when others then
		nr_nivel_anterior_w	:=nr_nivel_anterior_w;
	end;

end if;

if	(Obter_Valor_Param_Usuario(85,89,obter_Perfil_Ativo,:new.nm_usuario,0)	='S') and
	(:new.nr_seq_protocolo is not null) then
	update	protocolo_convenio a
	set	a.dt_vencimento		= :new.dt_vencimento
	where	nr_seq_protocolo	= :new.nr_seq_protocolo;
end if;

if	(trunc(:new.dt_pagamento_previsto, 'dd') < trunc(:new.dt_emissao, 'dd')) then
	--A data de vencimento nao pode ser menor que a data de emissao do titulo!
	--Dt emissao: #@DT_EMISSAO#@
	--Dt vencimento: #@DT_VENCIMENTO#@
	--Seq protocolo: #@NR_SEQ_PROTOCOLO#@
	--Pessoa: #@DS_PESSOA#@
	Wheb_mensagem_pck.exibir_mensagem_abort(222075,	'DT_EMISSAO='||:new.dt_emissao||
							';DT_VENCIMENTO='||trunc(:new.dt_pagamento_previsto, 'dd')||
							';NR_SEQ_PROTOCOLO='||:new.nr_seq_protocolo||
							';DS_PESSOA='||obter_nome_pf_pj(:new.cd_pessoa_fisica, :new.cd_cgc));
end if;

obter_tx_juros_multa_cre(:new.cd_estabelecimento,
			:new.ie_origem_titulo,
			:new.ie_tipo_titulo,
			tx_juros_w,
			tx_multa_w);

if	(tx_juros_w is not null) then
	:new.tx_juros	:= tx_juros_w;
end if;

if	(tx_multa_w is not null) then
	:new.tx_multa	:= tx_multa_w;
end if;

/*AAMFIRMO OS 925991 - Para titulos originados de OPS Mensalidade(3)  e OPS Faturamento(13), a data contabil do titulo deve ser a data contabil no OPS*/
Obter_Param_Usuario(801, 204, obter_perfil_ativo, :new.nm_usuario,:new.cd_estabelecimento, ie_dt_contabil_ops_w);

if ( nvl(ie_dt_contabil_ops_w,'N') = 'S' ) and ( :new.ie_origem_titulo = '3' ) then /*Aqui somente titulo com origem OPS - Mensalidade. Para origem OPS Faturamento foi tratado na proc pls_gerar_titulos_fatura*/

	/*Esse select para buscar a data e o mesmo utilizado na funcao Titulo_receber, na function que busca o valor para o campo  DT_MENSALIDADE no titulo*/
	begin

	select 	substr(pls_obter_dados_mensalidade(:new.nr_seq_mensalidade,'D'),1,10)
	into	dt_contabil_ops_w
	from 	dual;
	if (dt_contabil_ops_w is null) then

		select 	max(x.dt_mes_competencia)
		into	dt_contabil_ops_w
		from 	pls_fatura  x
		where 	x.nr_titulo = :new.nr_titulo;

		if (dt_contabil_ops_w is null) then

			select 	max(x.dt_mes_competencia)
			into	dt_contabil_ops_w
			from 	pls_fatura  x
			where  	x.nr_titulo_ndc = :new.nr_titulo;

		end if;

	end if;

	if (dt_contabil_ops_w is not null) then
		:new.dt_contabil := nvl(dt_contabil_ops_w,nvl(:new.dt_contabil, :new.dt_emissao));
	end if;
	exception when others then
		:new.dt_contabil := nvl(:new.dt_contabil, :new.dt_emissao);
	end;

end if;
/*AAMFIRMO Fim tratativa OS 925991*/
:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_afterinsert
after insert or update ON TASY.TITULO_RECEBER for each row
declare

dt_documento_w			ctb_documento.dt_competencia%type;
ie_contab_classif_tit_rec_w	parametro_contas_receber.ie_contab_classif_tit_rec%type;
ie_contab_curto_prazo_w		parametro_contas_receber.ie_contab_curto_prazo%type;
nr_seq_trans_financ_w		ctb_documento.nr_seq_trans_financ%type;
qt_titulo_rec_classif_w		number(10);
vl_movimento_w			ctb_documento.vl_movimento%type;
vl_tributo_w			titulo_receber_trib.vl_tributo%type;
ie_contab_tit_interc_cancel_w	pls_parametro_contabil.ie_contab_tit_interc_cancel%type;
nr_seq_info_ctb_w		ctb_documento.nr_seq_info%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;

cursor c01 is
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 5
	and 	a.nm_atributo in ( 'VL_TITULO_RECEBER', 'VL_LIQUIDO_TIT', 'VL_CURTO_PRAZO', 'VL_LONGO_PRAZO')
	union all
	select 	a.nm_atributo,
		a.cd_tipo_Lote_contab
	from	atributo_contab a
	where	a.cd_tipo_lote_contab = 36
	and	a.nm_atributo = 'VL_TITULO_RECEBER'
	and	:new.ie_origem_titulo	= '8'
	and	:new.nr_seq_ptu_fatura is not null
	and	ie_concil_contab_w = 'S'
	and	((:new.ie_situacao != '3') or (ie_contab_tit_interc_cancel_w = 'S'))
	union all
	select 	a.nm_atributo,
		a.cd_tipo_Lote_contab
	from	atributo_contab a
	where	a.cd_tipo_lote_contab = 37
	and	a.nm_atributo = 'VL_TITULO_RECEBER'
	and	:new.ie_origem_titulo	= '11'
	and	ie_concil_contab_w = 'S'
	and	((:new.nr_seq_pls_lote_contest is not null) or (:new.nr_seq_pls_lote_disc is not null))
	and 	:new.ie_situacao <> 3;

c01_w		c01%rowtype;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

if      (inserting) then

        /* Projeto MXM (7077)  - Exportar t�tulo a receber
        Dispara somente quando houver nota fiscal vinculada */
        if (nvl(:new.nr_seq_nf_saida,0) <> 0) then
                if (:new.cd_pessoa_fisica is not null) then
                        gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
                        gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
                else
                        gravar_agend_integracao(556,'CD_PESSOA_JURIDICA='||:new.cd_cgc||';CD_PESSOA_FISICA='||null||';'); --Fornecedor
                end if;
                gravar_agend_integracao(559,'NR_TITULO='||:new.nr_titulo||';');
        end if;

        /* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',nvl(:new.dt_pagamento_previsto,:new.dt_vencimento),'I',:new.nm_usuario);

end if;

begin
select	a.ie_contab_classif_tit_rec,
	a.ie_contab_curto_prazo
into	ie_contab_classif_tit_rec_w,
	ie_contab_curto_prazo_w
from	parametro_contas_receber a
where	a.cd_estabelecimento    = :new.cd_estabelecimento;
exception when others then
        ie_contab_classif_tit_rec_w := 'N';
        ie_contab_curto_prazo_w := 'N';
end;

begin
select  x.*

into    ie_contab_classif_tit_rec_w,
		ie_contab_curto_prazo_w
from    (
        select  nvl(a.ie_contab_classif_tit_rec, 'N'),
				nvl(a.ie_contab_curto_prazo, 'N')
        from    ctb_param_lote_contas_rec a

        where   a.cd_empresa    = obter_empresa_estab(:new.cd_estabelecimento)
        and     nvl(a.cd_estab_exclusivo, :new.cd_estabelecimento) = :new.cd_estabelecimento
		and		ie_ctb_online = 'S'
        order   by nvl(a.cd_estab_exclusivo, 0) desc
        ) x
where   rownum = 1;
exception when others then
	null;
end;

select	nvl(max(ie_contab_tit_interc_cancel), 'S')
into	ie_contab_tit_interc_cancel_w
from	pls_parametro_contabil
where 	cd_estabelecimento = :new.cd_estabelecimento;

begin
select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = :new.cd_estabelecimento;
exception when others then
	ie_concil_contab_w := 'N';
end;

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	vl_movimento_w := 0;
	if 	(c01_w.cd_tipo_lote_contab = 5) then
		nr_seq_info_ctb_w	:= 3;
		if	(c01_w.nm_atributo = 'VL_LIQUIDO_TIT') then
			begin

			select	nvl(sum(decode(b.ie_soma_diminui, 'S' , a.vl_tributo, 'D' , a.vl_tributo * -1,0)),0)
			into	vl_tributo_w
			from	titulo_receber_trib	a,
				tributo			b
			where	a.cd_tributo		= b.cd_tributo
			and	a.nr_seq_nota_fiscal    is null
			and	a.nr_seq_mens_trib      is null
			and	a.nr_titulo		= :new.nr_titulo
			and	(a.ie_origem_tributo = 'D' or nvl(b.ie_incide_conta,'N') = 'N');

			vl_movimento_w	        := :new.vl_titulo + vl_tributo_w;
			dt_documento_w          := :new.dt_contabil;
			nr_seq_trans_financ_w   := :new.nr_seq_trans_fin_contab;

			end;
		elsif	(c01_w.nm_atributo = 'VL_TITULO_RECEBER') then
			begin

			begin
			select	count(1)
			into	qt_titulo_rec_classif_w
			from	titulo_receber_classif
			where	nr_titulo = :new.nr_titulo;
			exception
			when others then
				qt_titulo_rec_classif_w:= 0;
			end;

			if	(ie_contab_classif_tit_rec_w = 'N') or (qt_titulo_rec_classif_w = 0) then
				begin

				vl_movimento_w:= :new.vl_titulo;
				dt_documento_w:= :new.dt_contabil;
				nr_seq_trans_financ_w:= :new.nr_seq_trans_fin_contab;

				end;
			end if;


			end;
		elsif	(c01_w.nm_atributo = 'VL_CURTO_PRAZO') or (c01_w.nm_atributo = 'VL_LONGO_PRAZO') then
			begin

			if	(ie_contab_curto_prazo_w = 'S') then
				begin

				vl_movimento_w:= :new.vl_titulo;
				dt_documento_w:= :new.dt_contabil;

				if	(c01_w.nm_atributo = 'VL_CURTO_PRAZO') then
					begin
					dt_documento_w:= pkg_date_utils.add_month(:new.dt_vencimento,-12,0);
					end;
				end if;


				if	(pkg_date_utils.add_month(:new.dt_vencimento,-12,0) >= :new.dt_contabil) and (c01_w.nm_atributo = 'VL_CURTO_PRAZO')
					or (:new.dt_vencimento > pkg_date_utils.add_month(:new.dt_contabil,12,0)) and (c01_w.nm_atributo = 'VL_LONGO_PRAZO') then
					begin

					if		(nvl(:new.nr_seq_tf_curto_prazo, 0) <> 0) then
						begin

						nr_seq_trans_financ_w:= :new.nr_seq_tf_curto_prazo;
						end;
									else
											begin
											vl_movimento_w := null;
											end;
					end if;

					end;
				end if;

				end;
			end if;
			end;
		end if;
	elsif 	(c01_w.cd_tipo_lote_contab = 36) then
		vl_movimento_w		:= :new.vl_titulo;
		dt_documento_w 		:= :new.dt_contabil;
		nr_seq_trans_financ_w 	:= :new.nr_seq_trans_fin_contab;
		nr_seq_info_ctb_w	:= 46;
	elsif	(c01_w.cd_tipo_lote_contab = 37) then
		vl_movimento_w 		:= :new.vl_titulo;
		dt_documento_w		:= :new.dt_contabil;
		nr_seq_trans_financ_w 	:= :new.nr_seq_trans_fin_contab;
		nr_seq_info_ctb_w	:= 46;
	end if;

	if	(inserting) then
		begin

		if	(nvl(vl_movimento_w, 0) <> 0) then
			begin

			ctb_concil_financeira_pck.ctb_gravar_documento	(	:new.cd_estabelecimento,
										trunc(dt_documento_w),
										c01_w.cd_tipo_lote_contab,
										nr_seq_trans_financ_w,
										nr_seq_info_ctb_w,
										:new.nr_titulo,
										null,
										null,
										vl_movimento_w,
										'TITULO_RECEBER',
										c01_w.nm_atributo,
										:new.nm_usuario);

			end;
		end if;

		end;
	end if;

	end;
end loop;
close c01;

if	(updating) then
	begin

	update	ctb_documento
	set	nr_seq_trans_financ = :new.nr_seq_trans_fin_contab
	where	nr_documento = :new.nr_titulo
	and 	nvl(nr_seq_doc_compl, 0) = 0
	and	nm_tabela = 'TITULO_RECEBER'
	and 	nm_atributo in ('VL_TITULO_RECEBER', 'VL_LIQUIDO_TIT');


	update	ctb_documento
	set	nr_seq_trans_financ = :new.nr_seq_tf_curto_prazo
	where	nr_documento = :new.nr_titulo
	and 	nvl(nr_seq_doc_compl, 0) = 0
	and	nm_tabela = 'TITULO_RECEBER'
	and 	nm_atributo in ('VL_CURTO_PRAZO', 'VL_LONGO_PRAZO');

	end;
end if;

end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_rec_bef_ins_update
before update or insert ON TASY.TITULO_RECEBER for each row
declare

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(:old.nr_titulo_externo is null) and
		(:new.nr_titulo_externo is not null) then
		:new.nr_titulo_ext_numerico	:= somente_numero(:new.nr_titulo_externo);

	elsif	(:old.nr_titulo_externo <> :new.nr_titulo_externo) then
		:new.nr_titulo_ext_numerico	:= somente_numero(:new.nr_titulo_externo);
	end if;
end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_after_update
after update ON TASY.TITULO_RECEBER for each row
declare

cd_banco_w					banco_estabelecimento.cd_banco%type;
cd_agencia_bancaria_w		banco_estabelecimento.cd_agencia_bancaria%type;
nr_conta_w					banco_estabelecimento.cd_conta%type;
ie_digito_conta_w			banco_estabelecimento.ie_digito_conta%type;
cd_camara_compensacao_w		pessoa_fisica_conta.cd_camara_compensacao%type;
ie_retorno_w				number(10);
ie_tipo_ocorrencia_w		number(10);
ie_desc_previsto_w			varchar2(1);
ie_juros_multa_w			varchar2(1);
nr_seq_motivo_desc_w		titulo_receber_liq_desc.nr_seq_motivo_desc%type;
cd_centro_custo_desc_w		titulo_receber_liq_desc.cd_centro_custo%type;

cursor c01 is
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_fisica_conta
	where	cd_pessoa_fisica 	= :new.cd_pessoa_fisica
	union
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_juridica_conta
	where	cd_cgc 	= :new.cd_cgc;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

/* Projeto MXM (7077)  - Exportar t�tulo a receber
 Dispara somente quando houver nota fiscal vinculada */
if (nvl(:new.nr_seq_nf_saida,0) <> 0 and nvl(:old.nr_seq_nf_saida,0) <> nvl(:new.nr_seq_nf_saida,0)) then
	if (:new.cd_pessoa_fisica is not null) then
		gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
		gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
	else
		gravar_agend_integracao(556,'CD_PESSOA_JURIDICA='||:new.cd_cgc||';CD_PESSOA_FISICA='||null||';'); --Fornecedor
	end if;
	gravar_agend_integracao(559,'NR_TITULO='||:new.nr_titulo||';');
end if;

if (:new.ie_entrada_confirmada = 'C') and (:old.vl_desc_previsto <> :new.vl_desc_previsto) then
	/*Tipo de ocorrencia 4 pois se trata de concess�o de desconto.*/
	ie_tipo_ocorrencia_w := 4;

	begin
	select	a.cd_banco,
			a.cd_agencia_bancaria,
			a.cd_conta,
			a.ie_digito_conta
	into	cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w
	from	banco_estabelecimento a
	where	a.nr_sequencia = :new.nr_seq_conta_banco;
	exception when others then
			ie_retorno_w	:= 0;
	end;

	if	(ie_retorno_w = 0) then
		open c01;
		loop
		fetch c01 into
			cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w,
			cd_camara_compensacao_w;
		exit when c01%notfound;
			cd_banco_w	:= cd_banco_w;
		end loop;
		close c01;
	end if;

	obter_param_usuario(815, 9, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_desc_previsto_w);
	obter_param_usuario(815, 16, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_juros_multa_w);

	select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc)
	into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w
	from	titulo_receber_liq_desc a
	where	a.nr_titulo		= :new.nr_titulo
	and		a.nr_bordero	is null
	and		a.nr_seq_liq	is null;

	insert into titulo_receber_instr (	nr_seq_cobranca,
										nr_titulo,
										vl_cobranca,
										dt_atualizacao,
										nm_usuario,
										cd_banco,
										cd_agencia_bancaria,
										nr_conta,
										cd_moeda,
										ie_digito_conta,
										cd_camara_compensacao,
										vl_desconto,
										vl_desc_previsto,
										vl_acrescimo,
										nr_sequencia,
										vl_despesa_bancaria,
										vl_saldo_inclusao,
										qt_dias_instrucao,
										cd_ocorrencia,
										ie_instrucao_enviada,
										nr_seq_motivo_desc,
										cd_centro_custo_desc,
										vl_juros,
										vl_multa,
										ie_selecionado)
							values	(	null,
										:new.nr_titulo,
										:new.vl_titulo,
										sysdate,
										:new.nm_usuario,
										cd_banco_w,
										cd_agencia_bancaria_w,
										nr_conta_w,
										:new.cd_moeda,
										ie_digito_conta_w,
										cd_camara_compensacao_w,
										decode(ie_desc_previsto_w,'S', nvl(:new.vl_desc_previsto,0)) + nvl(to_number(obter_dados_titulo_receber(:new.nr_titulo,'VNC')),0),
										decode(ie_desc_previsto_w,'S', nvl(:new.vl_desc_previsto,0),0),
										0,
										titulo_receber_instr_seq.nextval,
										0,
										:new.vl_saldo_titulo,
										null,
										substr(obter_ocorrencia_envio_cre(ie_tipo_ocorrencia_w,cd_banco_w),1,3),
										'N',
										nr_seq_motivo_desc_w,
										cd_centro_custo_desc_w,
										decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','J')),null),
										decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','M')),null),
										'N');

end if;

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',nvl(:new.dt_pagamento_previsto,:new.dt_vencimento),'A',:new.nm_usuario);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_integr
after insert or update ON TASY.TITULO_RECEBER for each row
declare

qt_reg_w	number(10);
reg_integracao_p		gerar_int_padrao.reg_integracao;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

if (:new.nr_titulo is not null) then

	/*Esse select � para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
	    /*Envio de cadastros financeiros - Gera��o de t�tulo*/
		gerar_int_padrao.gravar_integracao('31', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);

		reg_integracao_p.cd_estab_documento    := :new.cd_estabelecimento;
		reg_integracao_p.ie_origem_titulo_cre  := :new.ie_origem_titulo;
		reg_integracao_p.ie_tipo_titulo_cre    := :new.ie_tipo_titulo;
		gerar_int_padrao.gravar_integracao('26', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);

	end if;

end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Titulo_Receber_Bef_update
BEFORE UPDATE ON TASY.TITULO_RECEBER FOR EACH ROW
DECLARE

ie_titulo_prot_zerado_w		varchar2(100);
qt_reg_w			number(1);
ie_fechamento_atual_w		varchar2(1);
cd_convenio_w			number(5);
ie_cancel_tit_a600_w		pls_parametros_camara.ie_permite_canc_tit_vinc_a600%type;
nr_seq_lote_camara_w		pls_lote_camara_comp.nr_sequencia%type;
nr_seq_ptu_fatura_w		ptu_fatura.nr_sequencia%type;
dt_ultima_baixa_banc_w		titulo_receber_liq.dt_recebimento%type;
vl_glosa_w			titulo_receber_liq.vl_glosa%type;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'S') or
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'S') then
	goto Final;
end if;

cd_convenio_w	:= :new.cd_convenio_conta;

if	(cd_convenio_w	is null) then

	select	max(a.cd_convenio)
	into	cd_convenio_w
	from	protocolo_convenio a
	where	a.nr_seq_protocolo	= :new.nr_seq_protocolo;

end if;

/* Altera��o da data emiss�o */
if	((fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_emissao,'CR',null,cd_convenio_w,null,null) = 'N') or
	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, :old.dt_emissao,'CR',null,cd_convenio_w,null,null) = 'N'))and
	(:old.dt_emissao <> :new.dt_emissao) then
	--O m�s/dia financeiro do t�tulo j� est� fechado! N�o � poss�vel alterar a data de emiss�o.
	Wheb_mensagem_pck.exibir_mensagem_abort(204424);
end if;

/* Altera��o da data cont�bil */
if	(((:new.dt_contabil is not null) and
	  (fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_contabil,'CR',null,cd_convenio_w,null,null) = 'N')) or
	 ((:old.dt_emissao is not null) and
	  (fin_obter_se_mes_aberto(:new.cd_estabelecimento, :old.dt_emissao,'CR',null,cd_convenio_w,null,null) = 'N'))) and
	(:old.dt_contabil <> :new.dt_contabil) then
	--O m�s/dia financeiro do t�tulo (#@NR_TITULO#@) j� est� fechado! N�o � poss�vel alterar a data de cont�biliza��o.
	--Dt cont�bil: #@DT_CONTABIL#@. Dt emiss�o: #@DT_EMISSAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(204425,	'NR_TITULO=' || :new.nr_titulo ||
							';DT_CONTABIL=' || to_char(:new.dt_contabil,'dd/mm/yyyy') ||
							';DT_EMISSAO=' || to_char(:old.dt_emissao,'dd/mm/yyyy'));
end if;

/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
if	(:old.dt_contabil <> :new.dt_contabil) then
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_contabil);
end if;

if	(:new.nr_lote_contabil > 0) and
	(:old.dt_contabil is not null) and
	(:old.dt_contabil <> :new.dt_contabil) then
	--O t�tulo #@NR_TITULO#@  j� est� contabilizado no lote #@NR_LOTE#@ ! N�o � poss�vel alterar a data cont�bil.
	Wheb_mensagem_pck.exibir_mensagem_abort(204426, 'NR_TITULO=' || :new.nr_titulo ||
							';NR_LOTE=' || :new.nr_lote_contabil);
end if;

/* Cancelamento */
obter_param_usuario(801,115,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_fechamento_atual_w);

if	(ie_fechamento_atual_w	= 'S') and
	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, sysdate,'CR',null,cd_convenio_w,:new.nr_interno_conta,null) = 'N') and
	(:new.ie_situacao = '3') and
	(:old.ie_situacao <> :new.ie_situacao) then
	--O m�s/dia financeiro do t�tulo j� est� fechado! N�o � poss�vel cancelar o mesmo.
	Wheb_mensagem_pck.exibir_mensagem_abort(204423);

elsif	(nvl(ie_fechamento_atual_w,'N') = 'N') and
	((fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_contabil,'CR',null,cd_convenio_w,:new.nr_interno_conta,null) = 'N') or
	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, :old.dt_emissao,'CR',null,cd_convenio_w,:new.nr_interno_conta,null) = 'N')) and
	(:new.ie_situacao = '3') and
	(:old.ie_situacao <> :new.ie_situacao) then
	--O m�s/dia financeiro do t�tulo j� est� fechado! N�o � poss�vel cancelar o mesmo.
	Wheb_mensagem_pck.exibir_mensagem_abort(204422);
end if;

select	nvl(max(ie_titulo_prot_zerado),'N')
into	ie_titulo_prot_zerado_w
from	parametro_contas_receber
where	cd_estabelecimento		= :new.cd_estabelecimento;

-- Edgar 18/04/2008, OS 89930
if	(ie_titulo_prot_zerado_w = 'N') and
	(:new.vl_titulo = 0) and (:new.vl_titulo <> :old.vl_titulo) then -- dsantos em 15/03/2011, alterado para OS 297538 conforme sugestao do Egdar
	--O valor do t�tulo n�o pode ser zero!
	Wheb_mensagem_pck.exibir_mensagem_abort(204420);
end if;

if	((:new.dt_pagamento_previsto <> :old.dt_pagamento_previsto) or
	 (:new.dt_emissao <> :old.dt_emissao)) and
	(trunc(:new.dt_pagamento_previsto, 'dd') < trunc(:new.dt_emissao, 'dd')) then
	--A data de vencimento n�o pode ser menor que a data de emiss�o do t�tulo!
	Wheb_mensagem_pck.exibir_mensagem_abort(204421);
end if;

--aldellandrea 01/12/2015 OS962185 -- Validar se permite cancelar titulo de A600
if	((:new.ie_origem_titulo = '13') or   --OPS - Faturamento
	 (:new.ie_origem_titulo = '11')) and
	(:new.ie_situacao = '3')	then --contesta��o
	--verifica o campo na regra se permite cancelar t�tulo de a600
	select	max(nvl(ie_permite_canc_tit_vinc_a600, 'S'))
	into	ie_cancel_tit_a600_w
	from	pls_parametros_camara
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if	(ie_cancel_tit_a600_w = 'N') then

		select  max(y.nr_sequencia)
		into	nr_seq_lote_camara_w
		from 	pls_titulo_lote_camara z,
			pls_lote_camara_comp x,
			ptu_camara_compensacao y
		where 	x.nr_sequencia 		= y.nr_seq_lote_camara
		and 	x.nr_sequencia 		= z.nr_seq_lote_camara
		and 	z.nr_titulo_receber  	= :new.nr_titulo;

		if	(nr_seq_lote_camara_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort(373291, 'NR_TITULO=' || :new.nr_titulo ||
									';NR_LOTE=' || nr_seq_lote_camara_w);
		end if;

	end if;
end if;

-- Gerar registro 510
if	(:new.ie_origem_titulo = '13') and -- OPS - Faturamento
	(:old.ie_situacao != '2') and -- N�o podia estar liquidado
	(:new.ie_situacao = '2') and -- Estar sendo liquidado
	(:new.dt_liquidacao is not null) then -- Ter data de liquida��o

	select	max(u.nr_sequencia)
	into	nr_seq_ptu_fatura_w
	from	ptu_fatura	u,
		pls_fatura	s
	where	s.nr_sequencia	= u.nr_seq_pls_fatura
	and	s.nr_titulo	= :new.nr_titulo;


	if	(nr_seq_ptu_fatura_w is null) then
		select	max(u.nr_sequencia)
		into	nr_seq_ptu_fatura_w
		from	ptu_fatura	u,
			pls_fatura	s
		where	s.nr_sequencia	= u.nr_seq_pls_fatura
		and	s.nr_titulo_ndc	= :new.nr_titulo;
	end if;

	-- Gerar A510
	if	(nr_seq_ptu_fatura_w is not null) then

		-- Busca a ultima data de recebimento do titulo, que n�o tenha valor de glosa (indicando baixa bancaria)
		select	max(dt_recebimento)
		into	dt_ultima_baixa_banc_w
		from	titulo_receber_liq
		where	nr_titulo	= :new.nr_titulo
		and	vl_glosa	= 0;

		select	sum(nvl(vl_glosa,0))
		into	vl_glosa_w
		from	titulo_receber_liq
		where	nr_titulo	= :new.nr_titulo;

		-- se a data de baixa bancaria n�o existir ou for maior ou igual ( teoricamente s� pode ser menor ou igual) que a data de liquida��o,  ent�o utiliza a data de baixa bancaria,
		if	(dt_ultima_baixa_banc_w is null) or
			(dt_ultima_baixa_banc_w >= :new.dt_liquidacao)then

			dt_ultima_baixa_banc_w := :new.dt_liquidacao;
		end if;

		ptu_gerar_fat_baixa_interc( nr_seq_ptu_fatura_w, dt_ultima_baixa_banc_w, :new.cd_estabelecimento, :new.nm_usuario, 'N', null, null, vl_glosa_w, :new.dt_pagamento_previsto);
	end if;
end if;

--fim aldellandrea
<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.TITULO_RECEBER ADD (
  CONSTRAINT TITRECE_PK
 PRIMARY KEY
 (NR_TITULO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          832K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER ADD (
  CONSTRAINT TITRECE_PTUNOCC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_DEB_CONCLUSAO) 
 REFERENCES TASY.PTU_NOTA_DEB_CONCLUSAO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSPAIT_FK 
 FOREIGN KEY (NR_SEQ_PAG_CONTA_EVENTO) 
 REFERENCES TASY.PLS_PAGAMENTO_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSRJAV_FK 
 FOREIGN KEY (NR_SEQ_APORTE_VENC) 
 REFERENCES TASY.PLS_PROG_REAJ_APORTE_VENC (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSFATU_FK 
 FOREIGN KEY (NR_SEQ_PLS_FATURA) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_CRTCOMP_FK 
 FOREIGN KEY (NR_SEQ_CARTA) 
 REFERENCES TASY.CARTA_COMPROMISSO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_LOTEPRO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PROT) 
 REFERENCES TASY.LOTE_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT TITRECE_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_NEGOCCR_FK 
 FOREIGN KEY (NR_SEQ_NEGOCIACAO_ORIGEM) 
 REFERENCES TASY.NEGOCIACAO_CR (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSDELC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_EMPRESA) 
 REFERENCES TASY.PLS_DESC_LOTE_COMISSAO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PTUFATU_FK 
 FOREIGN KEY (NR_SEQ_PTU_FATURA) 
 REFERENCES TASY.PTU_FATURA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA),
  CONSTRAINT TITRECE_LOTENCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENC_CONTAS) 
 REFERENCES TASY.LOTE_ENCONTRO_CONTAS (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_DEST) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT TITRECE_PLSSEME_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO_MENS) 
 REFERENCES TASY.PLS_SEGURADO_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT TITRECE_EMEFATU_FK 
 FOREIGN KEY (NR_SEQ_EME_FATURA) 
 REFERENCES TASY.EME_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSCPSE_FK 
 FOREIGN KEY (NR_SEQ_COB_PREVIA) 
 REFERENCES TASY.PLS_COBRANCA_PREVIA_SERV (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT TITRECE_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_CURTO_PRAZO) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITRECE_TRAFINA_FK3 
 FOREIGN KEY (NR_SEQ_TF_CURTO_PRAZO) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_CONPESS_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_PESSOA) 
 REFERENCES TASY.CONTROLE_PESSOA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_CONTAB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_BANCART_FK 
 FOREIGN KEY (NR_SEQ_CARTEIRA_COBR) 
 REFERENCES TASY.BANCO_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEBIMENTO) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO),
  CONSTRAINT TITRECE_CLATIRE_FK 
 FOREIGN KEY (NR_SEQ_CLASSE) 
 REFERENCES TASY.CLASSE_TITULO_RECEBER (NR_SEQUENCIA),
  CONSTRAINT TITRECE_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT TITRECE_AGEBANC_FK 
 FOREIGN KEY (CD_BANCO, CD_AGENCIA_BANCARIA) 
 REFERENCES TASY.AGENCIA_BANCARIA (CD_BANCO,CD_AGENCIA_BANCARIA),
  CONSTRAINT TITRECE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT TITRECE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TITRECE_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITRECE_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITRECE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TITRECE_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TITRECE_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT TITRECE_BANESTAB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_CONTA) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TITRECE_TERCONT_FK 
 FOREIGN KEY (NR_SEQ_TERC_CONTA) 
 REFERENCES TASY.TERCEIRO_CONTA (NR_SEQUENCIA),
  CONSTRAINT TITRECE_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NF_SAIDA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT TITRECE_SUSAIHP_FK 
 FOREIGN KEY (NR_PROCESSO_AIH) 
 REFERENCES TASY.SUS_AIH_PROCESSO (NR_PROCESSO),
  CONSTRAINT TITRECE_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_FINANCEIRO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TITRECE_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT TITRECE_PLSMESE_FK 
 FOREIGN KEY (NR_SEQ_MENS_SEGURADO) 
 REFERENCES TASY.PLS_MENSALIDADE_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT TITRECE_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_RECEBER TO NIVEL_1;

