ALTER TABLE TASY.PLS_CONTA_MAT_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_MAT_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_MAT_REGRA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CP_COMB_FILTRO      NUMBER(10),
  NR_SEQ_CP_COMB_FILTRO_COP  NUMBER(10),
  NR_SEQ_CP_COMB_ENTAO       NUMBER(10),
  NR_SEQ_CP_COMB_COP_ENTAO   NUMBER(10),
  VL_MATERIAL_BASE           NUMBER(15,4),
  DT_VALORIZACAO             DATE,
  DS_ITEM_PTU                VARCHAR2(255 BYTE),
  VL_MAT_COPARTIC_REGRA      NUMBER(15,2),
  VL_RECALCULO               NUMBER(15,2),
  TP_REDE_MIN                NUMBER(1),
  NR_SEQ_REGRA_TX_ADM        NUMBER(10),
  IE_A520                    VARCHAR2(3 BYTE),
  IE_ACRES_URG_EMER          VARCHAR2(1 BYTE),
  NR_SEQ_ITEM_TISS           NUMBER(6),
  NR_SEQ_ITEM_TISS_VINCULO   NUMBER(6),
  IE_TP_TRANSACAO            VARCHAR2(2 BYTE),
  IE_VL_APRESENTADO_SISTEMA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCMAREG_PK ON TASY.PLS_CONTA_MAT_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCMAREG_PLCPMAT_FK_I ON TASY.PLS_CONTA_MAT_REGRA
(NR_SEQ_CP_COMB_ENTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCMAREG_PLCPMAT1_FK_I ON TASY.PLS_CONTA_MAT_REGRA
(NR_SEQ_CP_COMB_COP_ENTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCMAREG_PLSCPCC_FK_I ON TASY.PLS_CONTA_MAT_REGRA
(NR_SEQ_CP_COMB_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCMAREG_PLSCPCC1_FK_I ON TASY.PLS_CONTA_MAT_REGRA
(NR_SEQ_CP_COMB_FILTRO_COP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_CONTA_MAT_REGRA ADD (
  CONSTRAINT PCMAREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_MAT_REGRA ADD (
  CONSTRAINT PCMAREG_PLCPMAT1_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_COP_ENTAO) 
 REFERENCES TASY.PLS_CP_CTA_EMAT (NR_SEQUENCIA),
  CONSTRAINT PCMAREG_PLCPMAT_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_ENTAO) 
 REFERENCES TASY.PLS_CP_CTA_EMAT (NR_SEQUENCIA),
  CONSTRAINT PCMAREG_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PCMAREG_PLSCPCC1_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_FILTRO_COP) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PCMAREG_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_MAT_REGRA TO NIVEL_1;

