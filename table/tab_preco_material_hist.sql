ALTER TABLE TASY.TAB_PRECO_MATERIAL_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAB_PRECO_MATERIAL_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAB_PRECO_MATERIAL_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TAB_PRECO_MAT     NUMBER(4)                NOT NULL,
  NR_DOCUMENTO         VARCHAR2(50 BYTE),
  IE_TIPO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TB_PR_HIST_ESTABEL_FK_I ON TASY.TAB_PRECO_MATERIAL_HIST
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TB_PR_HIST_PK ON TASY.TAB_PRECO_MATERIAL_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TB_PR_HIST_TABPRMA_FK_I ON TASY.TAB_PRECO_MATERIAL_HIST
(CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TAB_PRECO_MATERIAL_HIST ADD (
  CONSTRAINT TB_PR_HIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.TAB_PRECO_MATERIAL_HIST ADD (
  CONSTRAINT TB_PR_HIST_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TB_PR_HIST_TABPRMA_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT) 
 REFERENCES TASY.TABELA_PRECO_MATERIAL (CD_ESTABELECIMENTO,CD_TAB_PRECO_MAT));

GRANT SELECT ON TASY.TAB_PRECO_MATERIAL_HIST TO NIVEL_1;

