ALTER TABLE TASY.SLINE_XML_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SLINE_XML_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SLINE_XML_LAUDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  SL_TIPO              VARCHAR2(2 BYTE),
  SL_TITULO            VARCHAR2(70 BYTE),
  SL_SN                VARCHAR2(1 BYTE),
  SL_CHAVE             VARCHAR2(255 BYTE),
  SL_SENHA             VARCHAR2(255 BYTE),
  SL_COD_DOC           VARCHAR2(255 BYTE),
  SL_FORMATO           VARCHAR2(5 BYTE),
  SL_CHAVE_ADICIONAL   VARCHAR2(255 BYTE),
  SL_DATA_REALIZACAO   DATE,
  SL_SMS               VARCHAR2(20 BYTE),
  SL_OPER              VARCHAR2(255 BYTE),
  NR_PRESCRICAO        NUMBER(10),
  CD_MEDICO            VARCHAR2(10 BYTE),
  SL_TEXTO             VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SLXMLAU_PK ON TASY.SLINE_XML_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLXMLAU_PK
  MONITORING USAGE;


ALTER TABLE TASY.SLINE_XML_LAUDO ADD (
  CONSTRAINT SLXMLAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SLINE_XML_LAUDO TO NIVEL_1;

