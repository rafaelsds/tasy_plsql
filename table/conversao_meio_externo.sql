ALTER TABLE TASY.CONVERSAO_MEIO_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVERSAO_MEIO_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVERSAO_MEIO_EXTERNO
(
  CD_CGC                  VARCHAR2(14 BYTE),
  NM_TABELA               VARCHAR2(50 BYTE)     NOT NULL,
  NM_ATRIBUTO             VARCHAR2(50 BYTE)     NOT NULL,
  CD_INTERNO              VARCHAR2(40 BYTE)     NOT NULL,
  CD_EXTERNO              VARCHAR2(40 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQUENCIA            NUMBER(10),
  IE_SISTEMA_EXTERNO      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA            NUMBER(10),
  IE_ENVIO_RECEB          VARCHAR2(1 BYTE)      NOT NULL,
  NM_APRESENTACAO_EXT     VARCHAR2(255 BYTE),
  CD_DOMINIO              NUMBER(5),
  IE_TIPO                 VARCHAR2(1 BYTE),
  DS_CONV_LONGA           VARCHAR2(4000 BYTE),
  CD_SISTEMA_CODIFICACAO  VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONMEEX_DOMINIO_FK_I ON TASY.CONVERSAO_MEIO_EXTERNO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONMEEX_I1 ON TASY.CONVERSAO_MEIO_EXTERNO
(NM_TABELA, NM_ATRIBUTO, CD_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONMEEX_I2 ON TASY.CONVERSAO_MEIO_EXTERNO
(NM_TABELA, NM_ATRIBUTO, CD_EXTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONMEEX_I3 ON TASY.CONVERSAO_MEIO_EXTERNO
(NR_SEQ_REGRA, NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONMEEX_PESJURI_FK_I ON TASY.CONVERSAO_MEIO_EXTERNO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONMEEX_PK ON TASY.CONVERSAO_MEIO_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONMEEX_RECOMEX_FK_I ON TASY.CONVERSAO_MEIO_EXTERNO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Smart_meio_ext_aftInsUpdDel
after insert or update or delete ON TASY.CONVERSAO_MEIO_EXTERNO for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ie_integra_w			varchar2(1) := 'N';
nr_seq_conversao_w		conversao_meio_externo.nr_sequencia%type;
nm_usuario_w			conversao_meio_externo.nm_usuario%type;
ie_evento_w				varchar2(10) := '327';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

If (nvl(:new.ie_sistema_externo,:old.ie_sistema_externo) = 'SMART') and
	(nvl(:new.nm_tabela,:old.nm_tabela) = 'PE_PROCEDIMENTO')
	then


	Select	OBTER_ESTABELECIMENTO_ATIVO
	into	cd_estabelecimento_w
	from 	dual;

	if (deleting) then

		nr_seq_conversao_w := :old.CD_INTERNO;
		nm_usuario_w 		:= :old.nm_usuario;

	else

		nr_seq_conversao_w := :new.CD_INTERNO;
		nm_usuario_w 		:= :new.nm_usuario;

	end if;

	Select 	Smart_obter_se_sae(nr_seq_conversao_w, cd_estabelecimento_w, ie_evento_w,'RC',nm_usuario_w)
	into	ie_integra_w
	from 	dual;

	if (nvl( ie_integra_w,'N') = 'S') then

		reg_integracao_w.cd_estab_documento	:=	cd_estabelecimento_w;

		if (inserting) then
			BEGIN
				gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_conversao_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=CREATE');
			END;
		elsif  (updating) then
			BEGIN
				gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_conversao_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=UPDATE');
			END;
		elsif  (deleting) then
			BEGIN
				gerar_int_padrao.gravar_integracao(ie_evento_w, nr_seq_conversao_w, nm_usuario_w, reg_integracao_w,'DS_OPERACAO_P=DELETE');
			END;
		end if;

	end if;

end if;

<<Final>>
ie_integra_w	:= 'N';

end;
/


ALTER TABLE TASY.CONVERSAO_MEIO_EXTERNO ADD (
  CONSTRAINT CONMEEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVERSAO_MEIO_EXTERNO ADD (
  CONSTRAINT CONMEEX_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO),
  CONSTRAINT CONMEEX_RECOMEX_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_CONV_MEIO_EXT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONMEEX_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.CONVERSAO_MEIO_EXTERNO TO NIVEL_1;

