ALTER TABLE TASY.QT_CONVENIO_ESTAB_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_CONVENIO_ESTAB_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_CONVENIO_ESTAB_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  IE_PERMISSAO         VARCHAR2(1 BYTE),
  CD_CATEGORIA         VARCHAR2(10 BYTE),
  CD_PLANO             VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QTCVESR_CATCONV_FK_I ON TASY.QT_CONVENIO_ESTAB_REGRA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTCVESR_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTCVESR_CONPLAN_FK_I ON TASY.QT_CONVENIO_ESTAB_REGRA
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTCVESR_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTCVESR_CONVENI_FK_I ON TASY.QT_CONVENIO_ESTAB_REGRA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTCVESR_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTCVESR_ESTABEL_FK_I ON TASY.QT_CONVENIO_ESTAB_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTCVESR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QTCVESR_PK ON TASY.QT_CONVENIO_ESTAB_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTCVESR_PK
  MONITORING USAGE;


ALTER TABLE TASY.QT_CONVENIO_ESTAB_REGRA ADD (
  CONSTRAINT QTCVESR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QT_CONVENIO_ESTAB_REGRA ADD (
  CONSTRAINT QTCVESR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT QTCVESR_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT QTCVESR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT QTCVESR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.QT_CONVENIO_ESTAB_REGRA TO NIVEL_1;

