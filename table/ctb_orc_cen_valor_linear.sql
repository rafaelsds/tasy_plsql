ALTER TABLE TASY.CTB_ORC_CEN_VALOR_LINEAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_ORC_CEN_VALOR_LINEAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_ORC_CEN_VALOR_LINEAR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CENARIO       NUMBER(10)               NOT NULL,
  CD_CENTRO_CUSTO      NUMBER(10),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  DS_DETALHE           VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_CNPJ              VARCHAR2(14 BYTE),
  VL_ORCADO_1          NUMBER(15,2),
  VL_ORCADO_2          NUMBER(15,2),
  VL_ORCADO_3          NUMBER(15,2),
  VL_ORCADO_4          NUMBER(15,2),
  VL_ORCADO_5          NUMBER(15,2),
  VL_ORCADO_6          NUMBER(15,2),
  VL_ORCADO_7          NUMBER(15,2),
  VL_ORCADO_8          NUMBER(15,2),
  VL_ORCADO_9          NUMBER(15,2),
  VL_ORCADO_10         NUMBER(15,2),
  VL_ORCADO_11         NUMBER(15,2),
  VL_ORCADO_12         NUMBER(15,2),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBCENLIN_CENCUST_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBCENLIN_CONCONT_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBCENLIN_CTBORCE_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(NR_SEQ_CENARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_CTBORCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBCENLIN_ESTABEL_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBCENLIN_PESFISI_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBCENLIN_PESJURI_FK_I ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBCENLIN_PK ON TASY.CTB_ORC_CEN_VALOR_LINEAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBCENLIN_PK
  MONITORING USAGE;


ALTER TABLE TASY.CTB_ORC_CEN_VALOR_LINEAR ADD (
  CONSTRAINT CTBCENLIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_ORC_CEN_VALOR_LINEAR ADD (
  CONSTRAINT CTBCENLIN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CTBCENLIN_CTBORCE_FK 
 FOREIGN KEY (NR_SEQ_CENARIO) 
 REFERENCES TASY.CTB_ORC_CENARIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CTBCENLIN_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CTBCENLIN_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBCENLIN_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBCENLIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CTB_ORC_CEN_VALOR_LINEAR TO NIVEL_1;

