ALTER TABLE TASY.HD_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10),
  NR_SEQ_TECNICA       NUMBER(10)               NOT NULL,
  DT_INSTALACAO        DATE,
  DT_PERDA_RETIRADA    DATE,
  NR_SEQ_MOTIVO_FIM    NUMBER(10),
  IE_ADEQUADO          VARCHAR2(1 BYTE),
  CD_MEDICO            VARCHAR2(10 BYTE),
  CNPJ_LOCAL_INCLUSAO  VARCHAR2(14 BYTE),
  DT_PRIM_UTILIZACAO   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDACESS_HDLOCAC_FK_I ON TASY.HD_ACESSO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDACESS_HDLOCAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDACESS_HDPAREC_FK_I ON TASY.HD_ACESSO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDACESS_HDTECAC_FK_I ON TASY.HD_ACESSO
(NR_SEQ_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDACESS_MEDICO_FK_I ON TASY.HD_ACESSO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDACESS_MOTIFIM_FK_I ON TASY.HD_ACESSO
(NR_SEQ_MOTIVO_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDACESS_MOTIFIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDACESS_PESJURI_FK_I ON TASY.HD_ACESSO
(CNPJ_LOCAL_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDACESS_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDACESS_PK ON TASY.HD_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_acesso_befinsupd
before insert or update ON TASY.HD_ACESSO for each row
declare

begin

if	(:new.dt_perda_retirada is not null) and (:new.nr_seq_motivo_fim is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(264191);
end if;

end;
/


ALTER TABLE TASY.HD_ACESSO ADD (
  CONSTRAINT HDACESS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_ACESSO ADD (
  CONSTRAINT HDACESS_HDLOCAC_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.HD_LOCAL_ACESSO (NR_SEQUENCIA),
  CONSTRAINT HDACESS_HDPAREC_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.HD_PAC_RENAL_CRONICO (CD_PESSOA_FISICA),
  CONSTRAINT HDACESS_HDTECAC_FK 
 FOREIGN KEY (NR_SEQ_TECNICA) 
 REFERENCES TASY.HD_TECNICA_ACESSO (NR_SEQUENCIA),
  CONSTRAINT HDACESS_MOTIFIM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM) 
 REFERENCES TASY.MOTIVO_FIM (NR_SEQUENCIA),
  CONSTRAINT HDACESS_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT HDACESS_PESJURI_FK 
 FOREIGN KEY (CNPJ_LOCAL_INCLUSAO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.HD_ACESSO TO NIVEL_1;

