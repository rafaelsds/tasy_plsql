ALTER TABLE TASY.PLS_REGRA_ACESSO_GP_SERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ACESSO_GP_SERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ACESSO_GP_SERV
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_REGRA_PRESTADOR       VARCHAR2(1 BYTE),
  IE_REGRA_REEMBOLSO       VARCHAR2(1 BYTE),
  IE_REGRA_FATURA          VARCHAR2(1 BYTE),
  IE_REGRA_COPARTICIPACAO  VARCHAR2(1 BYTE),
  IE_REGRA_INTERCAMBIO     VARCHAR2(1 BYTE),
  IE_REGRA_AUTORIZACAO     VARCHAR2(1 BYTE),
  IE_REGRA_FRANQUIA        VARCHAR2(1 BYTE),
  DS_REGRA                 VARCHAR2(255 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSACECP_PK ON TASY.PLS_REGRA_ACESSO_GP_SERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_ACESSO_GP_SERV_tp  after update ON TASY.PLS_REGRA_ACESSO_GP_SERV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_PRESTADOR,1,4000),substr(:new.IE_REGRA_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_REEMBOLSO,1,4000),substr(:new.IE_REGRA_REEMBOLSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_REEMBOLSO',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_FATURA,1,4000),substr(:new.IE_REGRA_FATURA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_FATURA',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_INTERCAMBIO,1,4000),substr(:new.IE_REGRA_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_AUTORIZACAO,1,4000),substr(:new.IE_REGRA_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_AUTORIZACAO',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_FRANQUIA,1,4000),substr(:new.IE_REGRA_FRANQUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_FRANQUIA',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_COPARTICIPACAO,1,4000),substr(:new.IE_REGRA_COPARTICIPACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_COPARTICIPACAO',ie_log_w,ds_w,'PLS_REGRA_ACESSO_GP_SERV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_ACESSO_GP_SERV ADD (
  CONSTRAINT PLSACECP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_ACESSO_GP_SERV TO NIVEL_1;

