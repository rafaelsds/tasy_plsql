ALTER TABLE TASY.AVAL_NUTRICAO_PED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AVAL_NUTRICAO_PED CASCADE CONSTRAINTS;

CREATE TABLE TASY.AVAL_NUTRICAO_PED
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  QT_PERDA                      NUMBER(7,3),
  QT_PESO_HABIT                 NUMBER(7,3)     NOT NULL,
  QT_PESO_ATUAL                 NUMBER(7,3)     NOT NULL,
  PR_PERDA                      NUMBER(7,3),
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  QT_IG_SEMANA                  NUMBER(2),
  CD_PROFISSIONAL               VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                  DATE            NOT NULL,
  NR_SEQ_FATOR_ATIV             NUMBER(15),
  NR_SEQ_FATOR_STRESS           NUMBER(15),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DS_OBSERVACAO                 VARCHAR2(4000 BYTE),
  IE_AVAL_NUT                   VARCHAR2(15 BYTE),
  NR_SEQ_REG_ELEMENTO           NUMBER(10),
  DT_LIBERACAO                  DATE,
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  QT_ALTURA                     NUMBER(5,2),
  QT_IDADE                      NUMBER(3),
  QT_IMC                        NUMBER(6,1),
  QT_TEMPO_PERDA                NUMBER(3),
  QT_ALTURA_JOELHO              NUMBER(5,2),
  QT_ALTURA_ESTIMADA            NUMBER(3),
  QT_CIRC_PANTURRILHA           NUMBER(5,2),
  QT_CIRC_QUADRIL               NUMBER(5,2),
  QT_CIRC_ABDOMEN               NUMBER(5,2),
  QT_CIRC_BRACO                 NUMBER(5,2),
  QT_CIRC_CINTURA               NUMBER(5,2),
  QT_CIRC_PULSO                 NUMBER(5,2),
  PR_CIRC_BRACO                 NUMBER(7,3),
  QT_PERC_CIRC_BRACO            NUMBER(5,2),
  QT_PREGA_CUT_TRICIP           NUMBER(5,2),
  QT_PREGA_CUT_SUBESC           NUMBER(5,2),
  QT_PERC_PREGA_CUT_TRICIP      NUMBER(5,2),
  PR_PREGA_CUT_TRICIP           NUMBER(7,3),
  QT_PREGA_CUT_SUPRAILIACA      NUMBER(5,2),
  QT_PREGA_CUT_BICIPAL          NUMBER(5,2),
  QT_PREGA_ADUT_POLEGAR         NUMBER(5,2),
  QT_CIRC_MUSC_BRACO            NUMBER(5,2),
  QT_PERC_CIRC_MUSC_BRACO       NUMBER(5,2),
  PR_CIRC_MUSC_BRACO            NUMBER(7,3),
  QT_PROTEINA                   NUMBER(10,2),
  QT_NEC_PROTEICA               NUMBER(10,2),
  QT_RAZAO_CINTURA_QUADRIL      NUMBER(15,4),
  IE_RESPIRACAO                 VARCHAR2(5 BYTE),
  IE_QUEIMADURA                 VARCHAR2(1 BYTE),
  IE_FORMULA_BOLSO              VARCHAR2(1 BYTE),
  IE_TEMPO_PERDA                VARCHAR2(3 BYTE),
  QT_KCAL_RECOMENDADO           NUMBER(15,4),
  QT_GASTO_ENER_REPOUSO         NUMBER(5,1),
  QT_GASTO_GEB_MIFFLIN          NUMBER(5,1),
  IE_TRAUMA                     VARCHAR2(1 BYTE),
  QT_FATOR_ATIVIDADE            NUMBER(4,1),
  QT_FATOR_ESTRESSE             NUMBER(4,1),
  QT_GASTO_ENER_TOTAL           NUMBER(5,1),
  QT_GAST_ENER_TOTAL_IRETON     NUMBER(5,1),
  QT_IDADE_MES                  NUMBER(5),
  QT_IDADE_DIAS                 NUMBER(5),
  PR_PERDA_PESO                 NUMBER(7,3),
  QT_PESO_AJUSTADO              NUMBER(7,3),
  QT_NEC_CALORICA               NUMBER(10,2),
  QT_CALORIA                    NUMBER(6,1),
  QT_PREGA_CUT_ABD              NUMBER(5,2),
  QT_OSSEA                      NUMBER(7,3),
  PR_GORDURA                    NUMBER(7,3),
  IE_TIPO                       VARCHAR2(1 BYTE),
  QT_PREGA_CUT_PANTUR           NUMBER(5,2),
  QT_DENSIDADE_CORPORAL         NUMBER(5,2),
  QT_PESO_GORDO                 NUMBER(7,3),
  QT_MCM                        NUMBER(7,3),
  QT_IND_FLEXIBILIDADE          NUMBER(5,2),
  QT_EXEC_TESTE_FORCA           NUMBER(5),
  QT_FORCA_MAO_ESQ_1            NUMBER(7,3),
  QT_FORCA_MAO_ESQ_2            NUMBER(7,3),
  QT_FORCA_MAO_ESQ_3            NUMBER(7,3),
  QT_FORCA_MAO_DIR_1            NUMBER(7,3),
  QT_FORCA_MAO_DIR_2            NUMBER(7,3),
  QT_TEMPO_POS_ERETA            NUMBER(7,2),
  PR_GORDURA_IDEAL              NUMBER(7,3),
  PR_RESULT_ADEQ                NUMBER(7,3),
  QT_FORCA_MAO_DIR_3            NUMBER(7,3),
  QT_NEC_ENERGETICA             NUMBER(7,3),
  QT_NECESSIDADE_ENERGETICA     NUMBER(10,3),
  QT_UREIA                      NUMBER(7,3),
  QT_BAL_NITROGENADO            NUMBER(7,3),
  IE_MEMBRO_AMPUTADO            VARCHAR2(1 BYTE),
  IE_TIPO_ATEND_AVALIACAO       VARCHAR2(1 BYTE),
  QT_PESO_CORRIGIDO_AMP         NUMBER(7,3),
  IE_PACIENTE_ACAMADO           VARCHAR2(1 BYTE),
  IE_FATOR_ATIV_FISICA          VARCHAR2(3 BYTE),
  IE_ASCITE                     VARCHAR2(3 BYTE),
  IE_EDEMA                      VARCHAR2(3 BYTE),
  QT_PESO_AJUSTADO_AMP          NUMBER(7,3),
  QT_PESO_HIDRICO               NUMBER(7,3),
  QT_PESO_HIDRICO_ASCITE        NUMBER(7,3),
  IE_PARAPLEGICO                VARCHAR2(1 BYTE),
  QT_PESO_PARAPLEGICO           NUMBER(4,2),
  QT_PESO_TETRAPLEGICO          NUMBER(4,2),
  IE_TETRAPLEGICO               VARCHAR2(1 BYTE),
  IE_ORIGEM_PESO                VARCHAR2(2 BYTE),
  IE_ORIGEM_ALTURA              VARCHAR2(2 BYTE),
  QT_PESO_HABITUAL              NUMBER(7,3),
  IE_LADO_CIRC_BRACO            VARCHAR2(1 BYTE),
  IE_LADO_CIRC_PULSO            VARCHAR2(1 BYTE),
  IE_LADO_PREGA_CUT_TRI         VARCHAR2(1 BYTE),
  IE_LADO_CIRC_MUSC_BRA         VARCHAR2(1 BYTE),
  IE_TCTH                       VARCHAR2(3 BYTE),
  QT_AMB_C                      NUMBER(5,2),
  QT_PCT_AMB                    NUMBER(10,3),
  QT_AMB                        NUMBER(5,2),
  QT_GASTO_ENER_REPOUSO_KJ      NUMBER(5,1),
  QT_GASTO_GEB_MIFFLIN_KJ       NUMBER(5,1),
  QT_GASTO_ENER_TOTAL_KJ        NUMBER(5,1),
  QT_GAST_ENER_TOTAL_IRETON_KJ  NUMBER(5,1),
  IE_NIVEL_ATENCAO              VARCHAR2(1 BYTE),
  QT_CALORIA_KJ                 NUMBER(6,1),
  QT_NEC_CALORICA_KJ            NUMBER(10,2),
  NR_SEQ_ATEND_CONS_PEPA        NUMBER(10),
  IE_ALEITAMENTO_MATERNO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AVNUPED_ATCONSPEPA_FK_I ON TASY.AVAL_NUTRICAO_PED
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUPED_ATEPACI_FK_I ON TASY.AVAL_NUTRICAO_PED
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUPED_EHRREEL_FK_I ON TASY.AVAL_NUTRICAO_PED
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUPED_NUTFAAT_FK_I ON TASY.AVAL_NUTRICAO_PED
(NR_SEQ_FATOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUPED_NUTFAST_FK_I ON TASY.AVAL_NUTRICAO_PED
(NR_SEQ_FATOR_STRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUPED_PESFISI_FK_I ON TASY.AVAL_NUTRICAO_PED
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AVNUPED_PK ON TASY.AVAL_NUTRICAO_PED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.aval_nutricao_ped_atual
before insert or update ON TASY.AVAL_NUTRICAO_PED for each row
declare
cd_pessoa_fisica_w	varchar2(10);
ie_sexo_w		varchar2(1);
qt_fator_ativ_w		number(15,4);
qt_fator_stress_w	number(15,4);
qt_obesidade		number(1);
qt_pont_sexo		number(1);
qt_pont_queimadura	number(1);
qt_pont_trauma		number(1);
ie_raca_negra_w			varchar2(1);
qt_nec_proteica_w	number(10,2);
qt_meses_w			varchar2(255);
qt_50_w				number(5,2);
qt_fator_ativ_fis_w number(10,2);
idade_ano_w number(10);

begin

/* *************************************************************************************
 *WHEN MAKING CHANGES IN THIS TRIGGER, ALSO CHANGE IN PROCEDURE OBTER_VALORES_AVAL_PED *
 ************************************************************************************ */


:new.pr_perda		:= 	trunc(dividir_sem_round((:new.qt_peso_atual * 100) , :new.qt_peso_habit),3);
if (:new.qt_peso_atual < :new.qt_peso_habit) then
	:new.QT_PERDA		:= (:new.qt_peso_habit - :new.qt_peso_atual);
	:new.PR_PERDA_PESO  	:=   trunc(dividir_sem_round(((:new.qt_peso_habit - :new.qt_peso_atual) * 100), :new.qt_peso_habit),3);
else
	:new.QT_PERDA := 0;
	:new.PR_PERDA_PESO := 0;
end if;

if	(:new.qt_altura is not null) then
	:new.qt_imc	:= 	trunc(dividir_sem_round(:new.qt_peso_atual , ((dividir_sem_round(:new.qt_altura , 100)) * (dividir_sem_round(:new.qt_altura , 100)))),1);
end if;


if	(:new.QT_CIRC_QUADRIL	is not null) and
	(:new.QT_CIRC_CINTURA is not null)then
	:new.QT_RAZAO_CINTURA_QUADRIL	:= dividir(:new.QT_CIRC_CINTURA,:new.QT_CIRC_QUADRIL);
end if;

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

select	obter_sexo_pf(cd_pessoa_fisica_w,'C')
into	ie_sexo_w
from	dual;

qt_meses_w := obter_idade_pf(cd_pessoa_fisica_w,:new.DT_AVALIACAO,'M');
idade_ano_w := Obter_Idade_PF(cd_pessoa_fisica_w, sysdate, 'A');

select max(qt_50)
into   qt_50_w
from   w_aval_who
where  ie_tipo = 'IMC'
and    ie_sexo = ie_sexo_w
and    qt_meses = qt_meses_w;

if (:new.qt_altura > 0) and (qt_50_w > 0) then
	:new.QT_PESO_HABIT := nvl(trunc(((dividir_sem_round(:new.qt_altura , 100)) * (dividir_sem_round(:new.qt_altura , 100))) * qt_50_w,3),0);
end if;

/* Get your black race */
select	nvl(max(ie_negro),'N')
into	ie_raca_negra_w
from	cor_pele a,
	pessoa_fisica b
	where	b.nr_seq_cor_pele 	= a.nr_sequencia
	and	b.cd_pessoa_fisica 	= cd_pessoa_fisica_w;

if	(ie_sexo_w = 'M') then
	:new.qt_altura_estimada	:= 	(64.19 - (0.04 * :new.qt_idade) + (2.02 * :new.qt_altura_joelho));
else
	:new.qt_altura_estimada	:= 	(84.88 - (0.24 * :new.qt_idade) + (1.83 * :new.qt_altura_joelho));
end if;

/*Chumlea - 1985		- 		Arthur - OS 823502*/
if	(:new.ie_origem_peso = 'F') and
	(:new.qt_altura_joelho is not null) then
	if	(ie_sexo_w	= 	'M') then
		:new.qt_peso_atual	:= 	(60.65 + (:new.qt_altura_joelho * 2.04));
	else
		:new.qt_peso_atual	:= 	(84.88 + (:new.qt_altura_joelho * 1.83) - (:new.qt_idade * 0.24));
	end if;
end if;

/*Chumlea - 1988 		- 		Arthur - OS 823502*/
if	(:new.ie_origem_peso = 'C') and
	(:new.qt_prega_cut_subesc is not null) and
	(:new.qt_circ_panturrilha is not null) and
	(:new.qt_circ_braco is not null) and
	(:new.qt_altura_joelho is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
		:new.qt_peso_atual	:= ((0.98 * :new.qt_circ_panturrilha) + (1.16 * :new.qt_altura_joelho) + (1.73 * :new.qt_circ_braco) + (0.37 * :new.qt_prega_cut_subesc) - 81.69);
	else
		:new.qt_peso_atual	:= ((1.27 * :new.qt_circ_panturrilha) + (0.87 * :new.qt_altura_joelho) + (0.98 * :new.qt_circ_braco) + (0.4 * :new.qt_prega_cut_subesc) - 62.35);

	end if;
	end;
end if;

if	(:new.ie_origem_peso = 'R') and
	(:new.qt_circ_abdomen is not null) and
	(:new.qt_circ_panturrilha is not null) and
	(:new.qt_circ_braco is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
		:new.qt_peso_atual	:= ((0.5759 * :new.qt_circ_braco) + (0.5263 * :new.qt_circ_abdomen) + (1.2452 * :new.qt_circ_panturrilha) - (4.8689 * 1) - 32.9241);
	else
		:new.qt_peso_atual	:= ((0.5759 * :new.qt_circ_braco) + (0.5263 * :new.qt_circ_abdomen) + (1.2452 * :new.qt_circ_panturrilha) - (4.8689 * 2) - 32.9241);

	end if;
	end;
end if;

/*The formula below was documented as Chumlea, but it does not match the references
Changed to a non-existing type*/
if	(:new.ie_origem_peso = 'J')and
	(:new.qt_circ_braco is not null) and
	(:new.qt_altura_joelho is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
			if (ie_raca_negra_w	=	'N') then
				if	(:new.qt_idade between 19 and 59) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.19) + (:new.qt_circ_braco * 3.14) - (83.72));
				elsif 	(:new.qt_idade between 60 and 80) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.10) + (:new.qt_circ_braco * 3.07) - (75.81));
				end if;
			else
				if (:new.qt_idade between 19 and 59) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.09) + (:new.qt_circ_braco * 3.14) - (83.72));
				elsif	(:new.qt_idade between 60 and 80) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 0.44) + (:new.qt_circ_braco * 2.86) - (39.21));
				end if;
			end if;
	elsif (ie_sexo_w	= 	'F') then
		if(ie_raca_negra_w	=	'N') then
			if	(:new.qt_idade between 19 and 59) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.01) + (:new.qt_circ_braco * 2.81) - (66.04));
			elsif	(:new.qt_idade between 60 and 80) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.09) + (:new.qt_circ_braco * 2.68) - (65.51));
			end if;
		else
			if	(:new.qt_idade between 19 and 59) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.24) + (:new.qt_circ_braco * 2.97) - (82.48));
			elsif	(:new.qt_idade between 60 and 80) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.50) + (:new.qt_circ_braco * 2.58) - (84.22));
			end if;
		end if;
	end if;
	end;
end if ;

/*Chumlea*/
:new.QT_PERC_CIRC_BRACO 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'CB');
:new.QT_PERC_PREGA_CUT_TRICIP 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'PCT');
:new.QT_PERC_CIRC_MUSC_BRACO 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'CMB');

if	(:new.QT_CIRC_BRACO is not null) and
	(:new.QT_PREGA_CUT_TRICIP is not null) then
	:new.QT_CIRC_MUSC_BRACO	:= :new.QT_CIRC_BRACO - 3.14 * (dividir(:new.QT_PREGA_CUT_TRICIP , 10));
end if;

if	(:new.QT_PERC_CIRC_BRACO is not null) then
	:new.PR_CIRC_BRACO	:= dividir((:new.QT_CIRC_BRACO * 100) , :new.QT_PERC_CIRC_BRACO);
end if;

if	(:new.QT_PERC_PREGA_CUT_TRICIP is not null) then
	:new.PR_PREGA_CUT_TRICIP	:= dividir((:new.QT_PREGA_CUT_TRICIP * 100) , :new.QT_PERC_PREGA_CUT_TRICIP);
end if;

if	(:new.QT_PERC_CIRC_MUSC_BRACO is not null) then
	:new.PR_CIRC_MUSC_BRACO	:= dividir((:new.QT_CIRC_MUSC_BRACO * 100) , :new.QT_PERC_CIRC_MUSC_BRACO);
end if;

if	(ie_sexo_w = 'M') then
	:new.QT_GASTO_ENER_REPOUSO	:= 66.5 + (13.7 * :new.QT_PESO_ATUAL) + (5 * nvl(:new.qt_altura,:new.qt_altura_estimada)) - (6.8 * :new.qt_idade);
  :new.QT_GASTO_ENER_REPOUSO_KJ := converter_kcal_kjoules(:new.QT_GASTO_ENER_REPOUSO,'KJ');

elsif	(ie_sexo_w = 'F') then
	:new.QT_GASTO_ENER_REPOUSO	:= 665 + (9.6 * :new.QT_PESO_ATUAL) + (1.9 * nvl(:new.qt_altura,:new.qt_altura_estimada)) - (4.7 * :new.qt_idade);
  :new.QT_GASTO_ENER_REPOUSO_KJ := converter_kcal_kjoules(:new.QT_GASTO_ENER_REPOUSO,'KJ');

end if;

if	((:new.QT_PROTEINA is not null) and (:new.QT_PESO_ATUAL is not null)) then
	:new.QT_NEC_PROTEICA := :new.QT_PROTEINA * :new.QT_PESO_ATUAL;
else
	:new.QT_NEC_PROTEICA := null;
end if;

if	(:new.QT_CALORIA is not null or  :new.QT_CALORIA_KJ is not null) and (:new.QT_PESO_ATUAL is not null) then

if	(:new.QT_CALORIA is not null and  :new.QT_CALORIA_KJ is  null and (:old.QT_CALORIA <> :new.QT_CALORIA)) then
      :new.QT_CALORIA_KJ := converter_kcal_kjoules (:new.QT_CALORIA,'KJ');

elsif (:new.QT_CALORIA is not null and  :new.QT_CALORIA_KJ is  null ) then
      :new.QT_CALORIA_KJ := converter_kcal_kjoules (:new.QT_CALORIA,'KJ');
end if;

if 	(:new.QT_CALORIA_KJ is null and :new.QT_CALORIA is not null and (:old.QT_CALORIA_KJ <> :new.QT_CALORIA_KJ)) then
	:new.QT_CALORIA := converter_kcal_kjoules (:new.QT_CALORIA_KJ,'KCAL');

elsif (:new.QT_CALORIA is null and :new.QT_CALORIA_KJ is not null) then
	:new.QT_CALORIA := converter_kcal_kjoules (:new.QT_CALORIA_KJ,'KCAL');

end if;

  :new.QT_NEC_CALORICA := :new.QT_CALORIA * :new.QT_PESO_ATUAL;
  :new.QT_NEC_CALORICA_KJ := :new.QT_CALORIA_KJ * :new.QT_PESO_ATUAL;

end if;
begin
if	(:new.QT_GASTO_ENER_REPOUSO is not null or :new.QT_GASTO_ENER_REPOUSO_KJ is not null) and
	(:new.NR_SEQ_FATOR_ATIV is not null) and
	(:new.NR_SEQ_FATOR_STRESS is not null) then

	select	nvl(max(qt_fator),1)
	into	qt_fator_stress_w
	from	nut_fator_stress
	where	nr_sequencia	= :new.NR_SEQ_FATOR_STRESS;

	select	nvl(max(qt_fator),1)
	into	qt_fator_ativ_w
	from	nut_fator_ativ
	where	nr_sequencia	= :new.NR_SEQ_FATOR_ATIV ;


	:new.QT_GASTO_ENER_TOTAL := (:new.QT_GASTO_ENER_REPOUSO * qt_fator_ativ_w * qt_fator_stress_w);
  :new.QT_GASTO_ENER_TOTAL_KJ := (:new.QT_GASTO_ENER_REPOUSO_KJ * qt_fator_ativ_w * qt_fator_stress_w);

end if;
exception
when others then
	null;
end;

begin
if	(:new.qt_altura is not null) then
	if (ie_sexo_w = 'M') then
		:new.QT_GASTO_GEB_MIFFLIN := (10 * :new.qt_peso_atual) + (6.25 * :new.qt_altura) - (5 * :new.qt_idade) + 5;
    :new.QT_GASTO_GEB_MIFFLIN_KJ := converter_kcal_kjoules(:new.QT_GASTO_GEB_MIFFLIN,'KJ');
	else
		:new.QT_GASTO_GEB_MIFFLIN := (10 * :new.qt_peso_atual) + (6.25 * :new.qt_altura) - (5 * :new.qt_idade) - 161;
    :new.QT_GASTO_GEB_MIFFLIN_KJ := converter_kcal_kjoules(:new.QT_GASTO_GEB_MIFFLIN,'KJ');
	end if;
end if;

if	(:new.QT_PREGA_CUT_TRICIP is not null) and
	(:new.QT_PREGA_CUT_SUPRAILIACA is not null) and
	(:new.QT_PREGA_CUT_SUBESC is not null) and
	(:new.QT_PREGA_CUT_ABD is not null) then
	:new.PR_GORDURA := ((:new.QT_PREGA_CUT_TRICIP + :new.QT_PREGA_CUT_SUPRAILIACA + :new.QT_PREGA_CUT_SUBESC + :new.QT_PREGA_CUT_ABD ) * 0.153) + 5.783;
     --                                                 ((:new.QT_PREGA_CUT_TRICIP + :new.QT_PREGA_CUT_SUPRAILIACA + :new.QT_PREGA_CUT_SUBESC + (:new.QT_CIRC_ABDOMEN * 10) ) * 0.153) + 5.783;
end if;


if	(:new.IE_RESPIRACAO is not null) then
	if	(:new.IE_RESPIRACAO = 'ESP') then

		if	(:new.QT_IMC is not null) then
			if	(:new.QT_IMC > 27) then
				qt_obesidade	:= 1;
			else
				qt_obesidade	:= 0;
			end if;
		end if;

		:new.QT_GAST_ENER_TOTAL_IRETON := 629 - (11 * :new.qt_idade) + (25 * :new.qt_peso_atual) - (609 * qt_obesidade);
    :new.QT_GAST_ENER_TOTAL_IRETON_KJ := converter_kcal_kjoules(:new.QT_GAST_ENER_TOTAL_IRETON,'KJ');

	end if;

	if	(:new.IE_RESPIRACAO = 'MEC') then

		if	(:new.IE_QUEIMADURA = 'S') then
			qt_pont_queimadura := 1;
		else
			qt_pont_queimadura := 0;
		end if;

		if	(:new.IE_TRAUMA = 'S') then
			qt_pont_trauma := 1;
		else
			qt_pont_trauma := 0;
		end if;

		if	(ie_sexo_w = 'M') then
			qt_pont_sexo := 1;
		else
			qt_pont_sexo := 0;
		end if;

		:new.QT_GAST_ENER_TOTAL_IRETON := 1784 - (11 * :new.qt_idade) + (5 * :new.qt_peso_atual) + (244 * qt_pont_sexo) + (239 * qt_pont_trauma) + (840 * qt_pont_queimadura);
    :new.QT_GAST_ENER_TOTAL_IRETON_KJ := converter_kcal_kjoules(:new.QT_GAST_ENER_TOTAL_IRETON,'KJ');


	end if;

end if;

if (:new.IE_TCTH = 'PR') then
	if	(ie_sexo_w = 'M') then
		if (idade_ano_w >= 0) and (idade_ano_w < 3) then
			:new.qt_necessidade_energetica := ((0.249 * :new.QT_PESO_ATUAL) - 0.127) * 238.85;
		elsif (idade_ano_w >= 3) and (idade_ano_w < 10) then
			:new.qt_necessidade_energetica := ((0.095 * :new.QT_PESO_ATUAL) + 2.110) * 238.85;
		elsif (idade_ano_w >= 10) and (idade_ano_w <= 18) then
			:new.qt_necessidade_energetica := ((0.074 * :new.QT_PESO_ATUAL) + 2.754) * 238.85;
		end if;
	elsif (idade_ano_w >= 0) and (idade_ano_w < 3) then
		:new.qt_necessidade_energetica := ((0.244 * :new.QT_PESO_ATUAL) - 0.130) * 238.85;
	elsif (idade_ano_w >= 3) and (idade_ano_w <= 10) then
		:new.qt_necessidade_energetica := ((0.085 * :new.QT_PESO_ATUAL) + 2.033) * 238.85;
	elsif (idade_ano_w >= 10) and (idade_ano_w < 18) then
		:new.qt_necessidade_energetica := ((0.056 * :new.QT_PESO_ATUAL) + 2.898) * 238.85;
	end if;
elsif (:new.IE_TCTH = 'PO') then
	if (qt_meses_w > 3) and (qt_meses_w <= 35) then
		:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 22;
	elsif (qt_meses_w <= 3) then
		:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 175;
	elsif (qt_meses_w >= 4) and (qt_meses_w <= 6) then
		:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 56;
	elsif (qt_meses_w >= 7) and (qt_meses_w <= 12) then
		:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 22;
	elsif (qt_meses_w >= 13) and (qt_meses_w <= 35) then
		:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 20;
	else
		qt_fator_ativ_fis_w := to_number(obter_qt_fator_ativ_fisica(:new.ie_fator_ativ_fisica, :new.nr_atendimento));

		if (qt_fator_ativ_fis_w is not null) and (qt_fator_ativ_fis_w > 0) then
			:new.qt_necessidade_energetica := obter_nec_energetica(:new.QT_PESO_ATUAL,:new.QT_ALTURA,qt_fator_ativ_fis_w,:new.nr_atendimento);
		else
			:new.qt_necessidade_energetica := null;
		end if;
	end if;
elsif (qt_meses_w <= 35) then
	:new.qt_necessidade_energetica := (89 * :new.QT_PESO_ATUAL) - 100 + 22;
else
	qt_fator_ativ_fis_w := to_number(obter_qt_fator_ativ_fisica(:new.ie_fator_ativ_fisica, :new.nr_atendimento));
	if (qt_fator_ativ_fis_w is not null) and (qt_fator_ativ_fis_w > 0) then
			:new.qt_necessidade_energetica := obter_nec_energetica(:new.QT_PESO_ATUAL,:new.QT_ALTURA,qt_fator_ativ_fis_w,:new.nr_atendimento);
		else
			:new.qt_necessidade_energetica := null;
		end if;
end if;
exception
when others then
	null;
end;

end;
/


ALTER TABLE TASY.AVAL_NUTRICAO_PED ADD (
  CONSTRAINT AVNUPED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AVAL_NUTRICAO_PED ADD (
  CONSTRAINT AVNUPED_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT AVNUPED_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AVNUPED_NUTFAAT_FK 
 FOREIGN KEY (NR_SEQ_FATOR_ATIV) 
 REFERENCES TASY.NUT_FATOR_ATIV (NR_SEQUENCIA),
  CONSTRAINT AVNUPED_NUTFAST_FK 
 FOREIGN KEY (NR_SEQ_FATOR_STRESS) 
 REFERENCES TASY.NUT_FATOR_STRESS (NR_SEQUENCIA),
  CONSTRAINT AVNUPED_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVNUPED_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.AVAL_NUTRICAO_PED TO NIVEL_1;

