ALTER TABLE TASY.PLS_OC_CTA_FILTRO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_FILTRO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_FILTRO_PROC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_FILTRO     NUMBER(10)           NOT NULL,
  NR_SEQ_ESTRUTURA         NUMBER(10),
  NR_SEQ_GRUPO_SERVICO     NUMBER(10),
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  CD_AREA_PROCEDIMENTO     NUMBER(15),
  CD_ESPECIALIDADE         NUMBER(15),
  CD_GRUPO_PROC            NUMBER(15),
  IE_FERIADO               VARCHAR2(1 BYTE),
  DT_DIA_SEMANA            NUMBER(2),
  HR_INICIAL               DATE,
  HR_FINAL                 DATE,
  IE_TIPO_FERIADO          VARCHAR2(10 BYTE),
  IE_TIPO_DATA             VARCHAR2(3 BYTE),
  IE_CONSISTENCIA_VALOR    VARCHAR2(10 BYTE),
  IE_TIPO_DESP_PROC        VARCHAR2(1 BYTE),
  VL_MAX_ITEM              NUMBER(15,4),
  VL_MINIMO_ITEM           NUMBER(15,4),
  NR_SEQ_GRUPO_REC         NUMBER(10),
  IE_VIA_ACESSO            VARCHAR2(1 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  IE_PRESTADOR_ALTO_CUSTO  VARCHAR2(1 BYTE)     NOT NULL,
  VL_MINIMO_ITEM_IMP       NUMBER(15,4),
  VL_MAX_ITEM_IMP          NUMBER(15,4),
  ID_PACOTE_PTU            VARCHAR2(1 BYTE),
  TP_REDE_MIN              NUMBER(1),
  IE_ACRES_URG_EMER        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCPC_AREPROC_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_ESPPROC_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_GRUPROC_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_GRURECE_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_GRURECE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSOCPC_PK ON TASY.PLS_OC_CTA_FILTRO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_PLSOCCF_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(NR_SEQ_OC_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_PLSOCCF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_PLSOCES_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_PLSOCES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_PLSPRGS_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCPC_PROCEDI_FK_I ON TASY.PLS_OC_CTA_FILTRO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCPC_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_FILTRO_PROC_tp  after update ON TASY.PLS_OC_CTA_FILTRO_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PRESTADOR_ALTO_CUSTO,1,4000),substr(:new.IE_PRESTADOR_ALTO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_ALTO_CUSTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.ID_PACOTE_PTU,1,4000),substr(:new.ID_PACOTE_PTU,1,4000),:new.nm_usuario,nr_seq_w,'ID_PACOTE_PTU',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAX_ITEM_IMP,1,4000),substr(:new.VL_MAX_ITEM_IMP,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAX_ITEM_IMP',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO_ITEM_IMP,1,4000),substr(:new.VL_MINIMO_ITEM_IMP,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO_ITEM_IMP',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TP_REDE_MIN,1,4000),substr(:new.TP_REDE_MIN,1,4000),:new.nm_usuario,nr_seq_w,'TP_REDE_MIN',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACRES_URG_EMER,1,4000),substr(:new.IE_ACRES_URG_EMER,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACRES_URG_EMER',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_ACESSO,1,4000),substr(:new.IE_VIA_ACESSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_ACESSO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESP_PROC,1,4000),substr(:new.IE_TIPO_DESP_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESP_PROC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DATA,1,4000),substr(:new.IE_TIPO_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DATA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_INICIAL,'hh24:mi:ss'),to_char(:new.HR_INICIAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_INICIAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_FINAL,'hh24:mi:ss'),to_char(:new.HR_FINAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_FINAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DIA_SEMANA,1,4000),substr(:new.DT_DIA_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'DT_DIA_SEMANA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FERIADO,1,4000),substr(:new.IE_FERIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FERIADO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_FERIADO,1,4000),substr(:new.IE_TIPO_FERIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FERIADO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTENCIA_VALOR,1,4000),substr(:new.IE_CONSISTENCIA_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTENCIA_VALOR',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO_ITEM,1,4000),substr(:new.VL_MINIMO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAX_ITEM,1,4000),substr(:new.VL_MAX_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAX_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_OC_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_FILTRO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERVICO,1,4000),substr(:new.NR_SEQ_GRUPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERVICO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_FILTRO_PROC ADD (
  CONSTRAINT PLSOCPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_FILTRO_PROC ADD (
  CONSTRAINT PLSOCPC_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSOCPC_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSOCPC_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSOCPC_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSOCPC_PLSOCCF_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_FILTRO) 
 REFERENCES TASY.PLS_OC_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSOCPC_PLSOCES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_OCORRENCIA_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSOCPC_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSOCPC_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_OC_CTA_FILTRO_PROC TO NIVEL_1;

