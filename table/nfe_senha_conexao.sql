ALTER TABLE TASY.NFE_SENHA_CONEXAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NFE_SENHA_CONEXAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NFE_SENHA_CONEXAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_DIR_ENTRADA          VARCHAR2(255 BYTE),
  DS_DIR_SAIDA            VARCHAR2(255 BYTE),
  NM_USUARIO_SENHA        VARCHAR2(15 BYTE),
  DS_SENHA                VARCHAR2(20 BYTE),
  DS_CAMINHO_CERTIFICADO  VARCHAR2(255 BYTE),
  DS_SENHA_CERTIFICADO    VARCHAR2(255 BYTE),
  DS_CAMINHO_KEYSTORE     VARCHAR2(255 BYTE),
  DS_SENHA_KEYSTORE       VARCHAR2(255 BYTE),
  DS_DESTINO_TRANSMISSAO  VARCHAR2(255 BYTE),
  DS_METODO_TRANSMISSAO   VARCHAR2(255 BYTE),
  DS_URL_WSDL             VARCHAR2(255 BYTE),
  DS_IP_PORTA_PROXY       VARCHAR2(255 BYTE),
  DS_METODO_CONSULTA      VARCHAR2(255 BYTE),
  DS_METODO_CANCELAMENTO  VARCHAR2(255 BYTE),
  IE_TIPO_NOTA            VARCHAR2(5 BYTE),
  DS_LOGIN_SITE           VARCHAR2(255 BYTE),
  DS_SENHA_SITE           VARCHAR2(255 BYTE),
  CD_ACESSO_DEISS         VARCHAR2(14 BYTE),
  DS_ARQUIVOS_TRANSF      VARCHAR2(255 BYTE),
  IE_UTILIZA_SAMBA        VARCHAR2(2 BYTE),
  IE_UTILIZAR_CFDI        VARCHAR2(1 BYTE),
  DS_PROTOCOLO            VARCHAR2(20 BYTE)     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFESECO_ESTABEL_FK_I ON TASY.NFE_SENHA_CONEXAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFESECO_PK ON TASY.NFE_SENHA_CONEXAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFESECO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.NFE_SENHA_CONEXAO_tp  after update ON TASY.NFE_SENHA_CONEXAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOGIN_SITE,1,4000),substr(:new.DS_LOGIN_SITE,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOGIN_SITE',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_METODO_TRANSMISSAO,1,4000),substr(:new.DS_METODO_TRANSMISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_METODO_TRANSMISSAO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_SENHA,1,4000),substr(:new.NM_USUARIO_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_SENHA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_URL_WSDL,1,4000),substr(:new.DS_URL_WSDL,1,4000),:new.nm_usuario,nr_seq_w,'DS_URL_WSDL',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DIR_ENTRADA,1,4000),substr(:new.DS_DIR_ENTRADA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_ENTRADA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DIR_SAIDA,1,4000),substr(:new.DS_DIR_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIR_SAIDA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CAMINHO_CERTIFICADO,1,4000),substr(:new.DS_CAMINHO_CERTIFICADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_CERTIFICADO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_CERTIFICADO,1,4000),substr(:new.DS_SENHA_CERTIFICADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_CERTIFICADO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CAMINHO_KEYSTORE,1,4000),substr(:new.DS_CAMINHO_KEYSTORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_KEYSTORE',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_KEYSTORE,1,4000),substr(:new.DS_SENHA_KEYSTORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_KEYSTORE',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_IP_PORTA_PROXY,1,4000),substr(:new.DS_IP_PORTA_PROXY,1,4000),:new.nm_usuario,nr_seq_w,'DS_IP_PORTA_PROXY',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_METODO_CONSULTA,1,4000),substr(:new.DS_METODO_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'DS_METODO_CONSULTA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_METODO_CANCELAMENTO,1,4000),substr(:new.DS_METODO_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_METODO_CANCELAMENTO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_SITE,1,4000),substr(:new.DS_SENHA_SITE,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_SITE',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_NOTA,1,4000),substr(:new.IE_TIPO_NOTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_NOTA',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DESTINO_TRANSMISSAO,1,4000),substr(:new.DS_DESTINO_TRANSMISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DESTINO_TRANSMISSAO',ie_log_w,ds_w,'NFE_SENHA_CONEXAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.NFE_SENHA_CONEXAO ADD (
  CONSTRAINT NFESECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NFE_SENHA_CONEXAO ADD (
  CONSTRAINT NFESECO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.NFE_SENHA_CONEXAO TO NIVEL_1;

