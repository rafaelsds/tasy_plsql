ALTER TABLE TASY.PLS_MONITOR_TISS_MAT_VAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_MAT_VAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_MAT_VAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CTA_VAL         NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA           NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA_MAT       NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_TABELA_REF          VARCHAR2(2 BYTE),
  CD_GRUPO_PROC          VARCHAR2(3 BYTE),
  NR_SEQ_MATERIAL        NUMBER(10),
  QT_MATERIAL            NUMBER(12,4),
  VL_MATERIAL            NUMBER(15,2),
  VL_LIBERADO            NUMBER(15,2),
  QT_LIBERADO            NUMBER(12,4),
  CD_MATERIAL            VARCHAR2(15 BYTE),
  VL_GLOSA               NUMBER(15,2),
  IE_TIPO_DESPESA        VARCHAR2(1 BYTE),
  NR_SEQ_PACOTE          NUMBER(10),
  DT_EVENTO              DATE,
  IE_TIPO_EVENTO         VARCHAR2(3 BYTE),
  IE_GLOSA               VARCHAR2(1 BYTE),
  IE_ITEM_ATUALIZADO     VARCHAR2(1 BYTE),
  NR_SEQ_CTA_ALT         NUMBER(10)             NOT NULL,
  CD_CGC_FORNECEDOR      VARCHAR2(14 BYTE),
  VL_PAGO_FORNECEDOR     NUMBER(10,2),
  NR_SEQ_PREST_FORNEC    NUMBER(10),
  NR_SEQ_MAT_REC         NUMBER(10),
  NR_SEQ_CONTA_REC       NUMBER(10),
  CD_MATERIAL_TUSS       VARCHAR2(20 BYTE),
  VL_COPARTICIPACAO      NUMBER(18,2),
  IE_ORIGEM_TAB_REF      VARCHAR2(15 BYTE),
  IE_ORIGEM_GRUPO_PROC   VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TAB_REF   NUMBER(10),
  NR_SEQ_REGRA_GPO_PROC  NUMBER(10),
  NR_SEQ_CONTA_DISC      NUMBER(10),
  NR_SEQ_DISC_MAT        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTMV_MATERIA_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PESJURI_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTMV_PK ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSCOMAT_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_CONTA, NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSDIMA_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_DISC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSMAT_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSMTCV_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_CTA_VAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSMTRG_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_REGRA_GPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSPRES_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_PREST_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSRETT_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_REGRA_TAB_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PLSRGCO_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_CONTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTMV_PRGLMAT_FK_I ON TASY.PLS_MONITOR_TISS_MAT_VAL
(NR_SEQ_MAT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_mat_val_del
before delete ON TASY.PLS_MONITOR_TISS_MAT_VAL for each row
declare

vl_total_pago_w			pls_monitor_tiss_cta_val.vl_total_pago%type;
vl_total_apres_w		pls_monitor_tiss_cta_val.vl_cobranca_guia%type;
vl_total_item_w			pls_monitor_tiss_cta_val.vl_total_pago%type;
vl_total_glosa_w		pls_monitor_tiss_cta_val.vl_total_glosa%type;
vl_total_fornecedor_w		pls_monitor_tiss_cta_val.vl_total_fornecedor%type;
vl_item_pago_w			pls_monitor_tiss_mat_val.vl_liberado%type;
vl_item_apres_w			pls_monitor_tiss_mat_val.vl_material%type;
vl_item_glosa_w			pls_monitor_tiss_mat_val.vl_glosa%type;
vl_item_fornecedor_w		pls_monitor_tiss_mat_val.vl_pago_fornecedor%type;
ie_tipo_despesa_w		pls_conta_mat.ie_tipo_despesa%type;

begin
vl_item_pago_w := nvl(:old.vl_liberado,0);
vl_item_apres_w := nvl(:old.vl_material,0);
vl_item_glosa_w := nvl(:old.vl_glosa,0);
vl_item_fornecedor_w := nvl(:old.vl_pago_fornecedor,0);


select	nvl(vl_cobranca_guia,0),
	nvl(vl_total_pago,0),
	nvl(vl_total_glosa,0),
	nvl(vl_total_fornecedor,0)
into	vl_total_apres_w,
	vl_total_pago_w,
	vl_total_glosa_w,
	vl_total_fornecedor_w
from	pls_monitor_tiss_cta_val
where	nr_sequencia = :old.nr_seq_cta_val;

select	ie_tipo_despesa
into	ie_tipo_despesa_w
from	pls_conta_mat
where	nr_sequencia = :old.nr_seq_conta_mat;

if	(:old.cd_tabela_ref in ('00','98')) then
	select	vl_total_tabela_propria
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_tabela_propria = vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w,
		vl_total_fornecedor 	= vl_total_fornecedor_w - vl_item_fornecedor_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	((:old.cd_tabela_ref = '18') and (ie_tipo_despesa_w = '1')) or
	((:old.cd_tabela_ref in ('19','20')) and (ie_tipo_despesa_w = '2')) then
	select	vl_total_medicamentos
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_medicamentos 	= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w,
		vl_total_fornecedor 	= vl_total_fornecedor_w - vl_item_fornecedor_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	(:old.cd_tabela_ref = '19') and (ie_tipo_despesa_w = '3') then
	select	vl_total_material
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_material 	= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w,
		vl_total_fornecedor	= vl_total_fornecedor_w - vl_item_fornecedor_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	(:old.cd_tabela_ref = '19') and (ie_tipo_despesa_w = '7') then
	select	vl_total_opm
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_opm 		= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w,
		vl_total_fornecedor 	= vl_total_fornecedor_w - vl_item_fornecedor_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;
end if;
end;
/


ALTER TABLE TASY.PLS_MONITOR_TISS_MAT_VAL ADD (
  CONSTRAINT PLSMTMV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_MAT_VAL ADD (
  CONSTRAINT PLSMTMV_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA, NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQ_CONTA,NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSMTCV_FK 
 FOREIGN KEY (NR_SEQ_CTA_VAL) 
 REFERENCES TASY.PLS_MONITOR_TISS_CTA_VAL (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSDIMA_FK 
 FOREIGN KEY (NR_SEQ_DISC_MAT) 
 REFERENCES TASY.PLS_DISCUSSAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PRGLMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSRGCO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSMTRG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GPO_PROC) 
 REFERENCES TASY.PLS_MONITOR_TISS_REG_GPO (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSRETT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TAB_REF) 
 REFERENCES TASY.PLS_REGRA_TABELA_TISS (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSMTMV_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSMTMV_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PREST_FORNEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_MAT_VAL TO NIVEL_1;

