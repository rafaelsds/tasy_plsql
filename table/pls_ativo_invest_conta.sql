ALTER TABLE TASY.PLS_ATIVO_INVEST_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATIVO_INVEST_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATIVO_INVEST_CONTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATIVO_INVEST  NUMBER(10)               NOT NULL,
  DS_CONTA             VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAINC_PK ON TASY.PLS_ATIVO_INVEST_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAINC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAINC_PLSATIN_FK_I ON TASY.PLS_ATIVO_INVEST_CONTA
(NR_SEQ_ATIVO_INVEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAINC_PLSATIN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ATIVO_INVEST_CONTA ADD (
  CONSTRAINT PLSAINC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATIVO_INVEST_CONTA ADD (
  CONSTRAINT PLSAINC_PLSATIN_FK 
 FOREIGN KEY (NR_SEQ_ATIVO_INVEST) 
 REFERENCES TASY.PLS_ATIVO_INVESTIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_ATIVO_INVEST_CONTA TO NIVEL_1;

