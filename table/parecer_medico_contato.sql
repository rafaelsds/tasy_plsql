ALTER TABLE TASY.PARECER_MEDICO_CONTATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARECER_MEDICO_CONTATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARECER_MEDICO_CONTATO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  NR_PARECER               NUMBER(10)           NOT NULL,
  DT_CONTATO               DATE                 NOT NULL,
  CD_PESSOA_CONTATO        VARCHAR2(10 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  NM_PROFISSIONAL_EXTERNO  VARCHAR2(80 BYTE),
  IE_STATUS_CONTATO        VARCHAR2(3 BYTE),
  NR_SEQ_ATEND_CONS_PEPA   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARMEDC_ATCONSPEPA_FK_I ON TASY.PARECER_MEDICO_CONTATO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDC_PARMEDI_FK_I ON TASY.PARECER_MEDICO_CONTATO
(NR_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMEDC_PARMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMEDC_PESFISI_FK_I ON TASY.PARECER_MEDICO_CONTATO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDC_PESFISI_FK2_I ON TASY.PARECER_MEDICO_CONTATO
(CD_PESSOA_CONTATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARMEDC_PK ON TASY.PARECER_MEDICO_CONTATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMEDC_PK
  MONITORING USAGE;


ALTER TABLE TASY.PARECER_MEDICO_CONTATO ADD (
  CONSTRAINT PARMEDC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARECER_MEDICO_CONTATO ADD (
  CONSTRAINT PARMEDC_PARMEDI_FK 
 FOREIGN KEY (NR_PARECER) 
 REFERENCES TASY.PARECER_MEDICO_REQ (NR_PARECER)
    ON DELETE CASCADE,
  CONSTRAINT PARMEDC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMEDC_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_CONTATO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMEDC_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PARECER_MEDICO_CONTATO TO NIVEL_1;

