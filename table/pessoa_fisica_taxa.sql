ALTER TABLE TASY.PESSOA_FISICA_TAXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_TAXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_TAXA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  QT_DIAS_PAGAMENTO        NUMBER(4),
  NR_SEQ_JUSTIFICATIVA     NUMBER(10),
  DT_PAGAMENTO             DATE,
  NR_ATENDIMENTO           NUMBER(10),
  IE_OBRIGA_PAG_ADICIONAL  VARCHAR2(3 BYTE),
  NR_SEQ_ATECACO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PF_TAXA_ATECACO_FK_I ON TASY.PESSOA_FISICA_TAXA
(NR_SEQ_ATECACO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PF_TAXA_ATEPACI_FK_I ON TASY.PESSOA_FISICA_TAXA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PF_TAXA_PESFISI_FK_I ON TASY.PESSOA_FISICA_TAXA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PF_TAXA_PES_TX_JUS_FK_I ON TASY.PESSOA_FISICA_TAXA
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PF_TAXA_PK ON TASY.PESSOA_FISICA_TAXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_taxa_after
after insert or update ON TASY.PESSOA_FISICA_TAXA for each row
declare
is_changing_obriga_pag_w boolean    := false;
is_changing_seq_justif_w boolean    := false;
ds_log_w    varchar2(2000);
begin
is_changing_obriga_pag_w := (updating and nvl(:new.ie_obriga_pag_adicional, 'NULL') <> nvl(:old.ie_obriga_pag_adicional, 'NULL'));
is_changing_seq_justif_w := (updating and nvl(:new.nr_seq_justificativa, -1) <> nvl(:old.nr_seq_justificativa, -1));
if  (inserting or is_changing_obriga_pag_w or is_changing_seq_justif_w) then
    if  (nvl(:new.ie_obriga_pag_adicional, 'N') = 'S' and :new.nr_seq_justificativa is not null) then
        if  (inserting) then
            ds_log_w    :=  'Insert' ||
                        '|New IOPA=#' || :new.ie_obriga_pag_adicional || '#' ||
                        '|New NSJ=#' || :new.nr_seq_justificativa || '#' ||
                        '|New nr_seq_atecaco=#' || :new.nr_seq_atecaco;
        elsif   (updating) then
            ds_log_w    :=  'Update' ||
                        '|New IOPA=#' || :new.ie_obriga_pag_adicional || '#' ||
                        '|Old IOPA=#' || :old.ie_obriga_pag_adicional || '#' ||
                        '|New NSJ=#' || :new.nr_seq_justificativa || '#' ||
                        '|Old NSJ=#' || :old.nr_seq_justificativa || '#' ||
                        '|New nr_seq_atecaco=#' || :new.nr_seq_atecaco || '#' ||
                        '|Old nr_seq_atecaco=#' || :old.nr_seq_atecaco;
        end if;

        ds_log_w    :=  substr(ds_log_w || chr(13) || chr(10) || dbms_utility.format_call_stack,1, 2000);

        gravar_log_tasy(1515, ds_log_w, :new.nm_usuario);
    end if;
end if;
end pessoa_fisica_taxa_after;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_taxa_atual
after insert or update or delete ON TASY.PESSOA_FISICA_TAXA for each row
declare

qt_eventos_w		number(10)	:= 0;
nr_seq_interno_w	number(10);
ie_cascade_w		varchar2(1);
ie_operacao_w		varchar2(1);
ds_stack_w		varchar2(4000);

procedure agendar_envio_convenio(
		nr_seq_interno_p	number,
		ie_operacao_p		varchar2,
		nm_usuario_p		varchar2) is

reg_integracao_p	gerar_int_padrao.reg_integracao;
nr_sequencia_w		varchar2(255);

cursor c01 is
select	a.cd_pessoa_fisica,
	b.nr_seq_episodio,
	c.nr_atendimento,
	c.cd_convenio,
	c.cd_categoria,
	c.nr_seq_interno
from	episodio_paciente a,
	atendimento_paciente b,
	atend_categoria_convenio c
where	a.nr_sequencia = b.nr_seq_episodio
and	b.nr_atendimento = c.nr_atendimento
and	obter_se_atendimento_fechado(b.nr_atendimento) = 'N'
and	obter_se_atendimento_alta(b.nr_atendimento) = 'N'
and	a.dt_fim_episodio is null
and	a.dt_cancelamento is null
and	b.dt_cancelamento is null
and	b.dt_fim_conta is null
and	b.dt_alta is null
and	exists(	select	1
		from	atend_paciente_unidade x
		where	x.nr_atendimento = b.nr_atendimento)
and	c.nr_seq_interno = nr_seq_interno_p;

c01_w	c01%rowtype;

pragma autonomous_transaction;
begin
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin
	reg_integracao_p.nr_atendimento		:= c01_w.nr_atendimento;
	reg_integracao_p.cd_pessoa_fisica	:= c01_w.cd_pessoa_fisica;
	reg_integracao_p.ie_status		:= 'P';

	reg_integracao_p.ie_operacao		:= ie_operacao_p;

	nr_sequencia_w			:= 	c01_w.nr_atendimento || ish_param_pck.get_separador ||
						c01_w.nr_seq_interno || ish_param_pck.get_separador ||
						c01_w.nr_seq_episodio;

	gerar_int_padrao.gravar_integracao('135', nr_sequencia_w, nm_usuario_p, reg_integracao_p);
	end;
end loop;
close c01;
commit;
end agendar_envio_convenio;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	select	dbms_utility.format_call_stack
	into	ds_stack_w
	from	dual;


	if	(instr(ds_stack_w, 'ATEND_CATEG_CONV_DELETE_TAXA') = 0) and
		(instr(ds_stack_w, 'CANCELAR_ATENDIMENTO_PACIENTE') = 0) then
		begin
		select	1
		into	qt_eventos_w
		from	intpd_eventos
		where	ie_evento	= '135'
		and	ie_situacao	= 'A'
		and	rownum		= 1;
		exception
		when others then
			qt_eventos_w	:= 0;
		end;

		nr_seq_interno_w	:=	nvl(:new.nr_seq_atecaco, :old.nr_seq_atecaco);

		begin
		select	'N'
		into	ie_cascade_w
		from	atend_categoria_convenio
		where	nr_seq_interno = nr_seq_interno_w;
		exception
		when others then
			ie_cascade_w	:=	'S';
		end;

		if	(deleting) and (ie_cascade_w = 'S') then
			ie_operacao_w	:= 'E';
		else
			ie_operacao_w	:= 'A';
		end if;

		agendar_envio_convenio(nr_seq_interno_w, ie_operacao_w, nvl(:new.nm_usuario, :old.nm_usuario));
	end if;
	end;
end if;

end pessoa_fisica_taxa_atual;
/


ALTER TABLE TASY.PESSOA_FISICA_TAXA ADD (
  CONSTRAINT PF_TAXA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_TAXA ADD (
  CONSTRAINT PF_TAXA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PF_TAXA_PES_TX_JUS_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.PESSOA_TAXA_JUSTIFICATIVA (NR_SEQUENCIA),
  CONSTRAINT PF_TAXA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PF_TAXA_ATECACO_FK 
 FOREIGN KEY (NR_SEQ_ATECACO) 
 REFERENCES TASY.ATEND_CATEGORIA_CONVENIO (NR_SEQ_INTERNO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_FISICA_TAXA TO NIVEL_1;

