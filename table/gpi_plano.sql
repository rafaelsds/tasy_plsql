ALTER TABLE TASY.GPI_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_PLANO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DS_CONTA             VARCHAR2(80 BYTE)        NOT NULL,
  IE_TIPO              VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_SUPERIOR      NUMBER(10),
  NR_SEQ_APRES         NUMBER(10)               NOT NULL,
  IE_MATERIAL          VARCHAR2(15 BYTE)        NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  IE_OPERACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPIPLAN_CONCONT_FK_I ON TASY.GPI_PLANO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIPLAN_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIPLAN_EMPRESA_FK_I ON TASY.GPI_PLANO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GPIPLAN_PK ON TASY.GPI_PLANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GPI_PLANO ADD (
  CONSTRAINT GPIPLAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_PLANO ADD (
  CONSTRAINT GPIPLAN_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT GPIPLAN_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.GPI_PLANO TO NIVEL_1;

