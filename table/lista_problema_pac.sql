ALTER TABLE TASY.LISTA_PROBLEMA_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LISTA_PROBLEMA_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.LISTA_PROBLEMA_PAC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_STATUS                   VARCHAR2(10 BYTE),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_INTENSIDADE              VARCHAR2(3 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  DT_INICIO                   DATE,
  DT_FIM                      DATE,
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  DT_REVISAO                  DATE,
  NM_USUARIO_STATUS           VARCHAR2(15 BYTE) DEFAULT null,
  NM_USUARIO_FINALIZACAO      VARCHAR2(15 BYTE),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  DS_MOTIVO_FINALIZACAO       VARCHAR2(255 BYTE),
  NR_SEQ_CAD_LIST_PROB        NUMBER(10),
  DT_FINALIZACAO              DATE,
  CD_DOENCA                   VARCHAR2(10 BYTE),
  IE_TIPO_DIAGNOSTICO         NUMBER(3),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  DT_LIBERACAO                DATE,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  DS_PROBLEMA                 VARCHAR2(144 BYTE),
  CD_CIAP                     VARCHAR2(5 BYTE),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE),
  NR_SEQ_ESCUTA               NUMBER(10),
  NR_SEQ_SINTOMA              NUMBER(10),
  CD_MEDICO                   VARCHAR2(10 BYTE),
  NR_SEQ_PROBLEMA_SUP         NUMBER(10),
  NR_SEQ_SOAP                 NUMBER(10),
  IE_SUBJETIVO                VARCHAR2(1 BYTE),
  IE_AVALIACAO                VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_HIST            NUMBER(10),
  NR_SEQ_CAD_PROBLEMA         NUMBER(10),
  NR_SEQ_HISTORICO            NUMBER(10),
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  IE_IMPORTADO_HISTORICO      VARCHAR2(1 BYTE),
  NR_SEQ_PROBLEMA_INF         NUMBER(10),
  DS_FATOS_RELEVANTES         VARCHAR2(4000 BYTE),
  NR_SEQ_APRESEN_ESP          NUMBER(10),
  IE_TRATAMENTO_FAMACOLOGICO  VARCHAR2(1 BYTE),
  IE_TRATAMENTO_NAO_FAMACO    VARCHAR2(1 BYTE),
  IE_TIPO_DETECCAO            VARCHAR2(1 BYTE),
  IE_COMPLICACAO              VARCHAR2(1 BYTE),
  IE_CONSTAR_DOENCA_PREVIA    VARCHAR2(5 BYTE),
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  CD_DISEASE_MEDIS            NUMBER(8),
  IE_MAIN_ENC_PROBL           VARCHAR2(1 BYTE),
  NR_SEQ_CATEGORIA            NUMBER(10),
  CD_EVOLUCAO                 NUMBER(10),
  NR_SEQ_VINC_PAI             NUMBER(10),
  IE_TIPO_CAT_DIAGNOSTICO     VARCHAR2(1 BYTE),
  NR_SEQ_CATALOGO             NUMBER(10),
  CD_ITEM_CATALOGO            VARCHAR2(10 BYTE),
  QT_DURACAO                  NUMBER(10),
  IE_PERIODO                  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CD_MEDIS_DISEASE_FK_I ON TASY.LISTA_PROBLEMA_PAC
(CD_DISEASE_MEDIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_ATCONSPEPA_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_ATEPACI_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_ATESINI_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_ESCUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_CADPROB_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_CAD_PROBLEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_CALIPRO_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_CAD_LIST_PROB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_CATPROB_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_CIDDOEN_FK_I ON TASY.LISTA_PROBLEMA_PAC
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_EVOPACI_FK_I ON TASY.LISTA_PROBLEMA_PAC
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_HISTSAU_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_PESFISI_FK_I ON TASY.LISTA_PROBLEMA_PAC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LIPRPAC_PK ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_PROCIAP_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_SINTOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_PROFISS_FK_I ON TASY.LISTA_PROBLEMA_PAC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIPRPAC_TIHISSA_FK_I ON TASY.LISTA_PROBLEMA_PAC
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LISTA_PROBLEMA_PAC_DELETE
before delete ON TASY.LISTA_PROBLEMA_PAC for each row
declare

qt_reg_w		number(10);
pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if ( :old.nr_sequencia is not null) then

	select	count(1)
	into	qt_reg_w
	from	LISTA_PROBLEMA_PAC_ITEM
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from LISTA_PROBLEMA_PAC_ITEM
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;

	select	count(1)
	into	qt_reg_w
	from	lista_problema_status
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from lista_problema_status
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;

	select	count(1)
	into	qt_reg_w
	from	lista_problema_pac_incid
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from lista_problema_pac_incid
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;


	select	count(1)
	into	qt_reg_w
	from	prescr_item_sintoma
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from prescr_item_sintoma
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;

	select	count(1)
	into	qt_reg_w
	from	soap_item_sintoma
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from SOAP_ITEM_SINTOMA
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;

	select	count(1)
	into	qt_reg_w
	from	lista_problema_pac_prog
	where	nr_seq_problema	= :old.nr_sequencia;

	if	(qt_reg_w > 0) then

		delete from lista_problema_pac_prog
		where  nr_seq_problema	= :old.nr_sequencia;

		commit;

	end if;


end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.LISTA_PROBLEMA_PAC_tp  after update ON TASY.LISTA_PROBLEMA_PAC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'LISTA_PROBLEMA_PAC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.LISTA_PROBLEMA_PAC_Atual
before insert or update ON TASY.LISTA_PROBLEMA_PAC for each row
declare

nr_seq_sintoma_w  	 problema_ciap.nr_sequencia%type;

begin
if    (wheb_usuario_pck.get_ie_executar_trigger    = 'S')  then
if	(nvl(:new.cd_ciap,0) <> nvl(:old.cd_ciap,0)) then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_sintoma_w
	from	problema_ciap
	where	cd_ciap	= :new.cd_ciap;

	if	(nr_seq_sintoma_w > 0) then
		:new.nr_seq_sintoma := nr_seq_sintoma_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(851899);
	end if;
end if;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

     update evolucao_paciente
     set dt_inativacao = sysdate,
     ie_situacao ='I',
     dt_atualizacao = sysdate ,
     nm_usuario = wheb_usuario_pck.get_nm_usuario,
     nm_usuario_inativacao =  wheb_usuario_pck.get_nm_usuario,
     DS_JUSTIFICATIVA = :new.DS_JUSTIFICATIVA
     where cd_evolucao = :new.cd_evolucao;

     delete from clinical_note_soap_data
	 where cd_evolucao = :new.cd_evolucao and IE_MED_REC_TYPE = 'PROBLEM' and IE_STAGE = 1  and IE_SOAP_TYPE = 'A' and NR_SEQ_MED_ITEM = :new.nr_sequencia;

end if;
end if;

end;
/


ALTER TABLE TASY.LISTA_PROBLEMA_PAC ADD (
  CONSTRAINT LIPRPAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LISTA_PROBLEMA_PAC ADD (
  CONSTRAINT LIPRPAC_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT CD_MEDIS_DISEASE_FK 
 FOREIGN KEY (CD_DISEASE_MEDIS) 
 REFERENCES TASY.MEDIS_DECEASE (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_CATPROB_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_PROBLEMA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LIPRPAC_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT LIPRPAC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT LIPRPAC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LIPRPAC_CALIPRO_FK 
 FOREIGN KEY (NR_SEQ_CAD_LIST_PROB) 
 REFERENCES TASY.CAD_LISTA_PROBLEMA (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT LIPRPAC_ATESINI_FK 
 FOREIGN KEY (NR_SEQ_ESCUTA) 
 REFERENCES TASY.ATEND_ESCUTA_INICIAL (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_PROCIAP_FK 
 FOREIGN KEY (NR_SEQ_SINTOMA) 
 REFERENCES TASY.PROBLEMA_CIAP (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LIPRPAC_CADPROB_FK 
 FOREIGN KEY (NR_SEQ_CAD_PROBLEMA) 
 REFERENCES TASY.CADASTRO_PROBLEMA (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HISTORICO) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT LIPRPAC_TIHISSA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.TIPO_HISTORICO_SAUDE (NR_SEQUENCIA));

GRANT SELECT ON TASY.LISTA_PROBLEMA_PAC TO NIVEL_1;

