ALTER TABLE TASY.NOTA_FISCAL_CONSIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_CONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_CONSIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_NOTA            NUMBER(10)             NOT NULL,
  IE_FORMA_CONSISTENCIA  VARCHAR2(15 BYTE)      NOT NULL,
  DS_CONSISTENCIA        VARCHAR2(255 BYTE)     NOT NULL,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  IE_NOTA_ITEM           VARCHAR2(1 BYTE)       NOT NULL,
  NR_ITEM_NF             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOFICON_NOTFISC_FK_I ON TASY.NOTA_FISCAL_CONSIST
(NR_SEQ_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NOFICON_PK ON TASY.NOTA_FISCAL_CONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOFICON_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_cons_afterinsert
after insert ON TASY.NOTA_FISCAL_CONSIST for each row
declare

cd_funcao_ativa_w	funcao.cd_funcao%type;
cd_material_w		material.cd_material%type;

begin

cd_funcao_ativa_w := obter_funcao_ativa;

cd_material_w	:= null;

if	(cd_funcao_ativa_w = 146) then
	begin

	if	(:new.nr_item_nf > 0) then

		select	nvl(max(cd_material),0)
		into	cd_material_w
		from	nota_fiscal_item
		where	nr_sequencia = :new.nr_seq_nota
		and	nr_item_nf = :new.nr_item_nf;

		if	(cd_material_w = 0) then
			cd_material_w := null;
		end if;
	end if;

	insert into w_nota_fiscal_consist(
		nr_sequencia,
		nr_seq_nota,
		ds_titulo,
		ds_log,
		dt_atualizacao,
		nm_usuario,
		nr_item_nf,
		cd_material)
	values( w_nota_fiscal_consist_seq.nextval,
		:new.nr_seq_nota,
		substr(:new.ds_consistencia,1,255),
		substr(:new.ds_observacao,1,4000),
		sysdate,
		:new.nm_usuario,
		:new.nr_item_nf,
		cd_material_w);

	end;
end if;

end;
/


ALTER TABLE TASY.NOTA_FISCAL_CONSIST ADD (
  CONSTRAINT NOFICON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_CONSIST ADD (
  CONSTRAINT NOFICON_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NOTA_FISCAL_CONSIST TO NIVEL_1;

