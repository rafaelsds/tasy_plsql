ALTER TABLE TASY.PLS_EMAIL_PARAMETROS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EMAIL_PARAMETROS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EMAIL_PARAMETROS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  QT_ENVIO                    NUMBER(10),
  IE_LIMPAR_ANEXO             VARCHAR2(1 BYTE),
  DS_CAMINHO_ANEXO_SERV       VARCHAR2(255 BYTE),
  DS_CAMINHO_ANEXO_REDE       VARCHAR2(255 BYTE),
  NM_SERVIDOR_ENVIO           VARCHAR2(255 BYTE),
  IE_ORIGEM                   VARCHAR2(15 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  NM_USERID_ENVIO             VARCHAR2(255 BYTE),
  DS_SENHA_AUT_ENVIO          VARCHAR2(255 BYTE),
  DS_PORTA_ENVIO              VARCHAR2(50 BYTE),
  IE_PROTOCOLO_SSL            VARCHAR2(2 BYTE),
  IE_EMAIL_RESPOSTA           VARCHAR2(2 BYTE),
  IE_METODO_AUT_SSL           VARCHAR2(10 BYTE),
  CD_PRIORIDADE               NUMBER(2),
  DS_REDE_AUT                 VARCHAR2(255 BYTE),
  DS_USUARIO_REDE             VARCHAR2(255 BYTE),
  DS_SENHA_REDE               VARCHAR2(255 BYTE),
  IE_UTILIZA_AUT_REDE         VARCHAR2(1 BYTE),
  IE_SENHA_ANEXO_PF           VARCHAR2(2 BYTE),
  IE_SENHA_ANEXO_PJ           VARCHAR2(2 BYTE),
  QT_CARACTER_SENHA_ANEXO_PF  NUMBER(3),
  QT_CARACTER_SENHA_ANEXO_PJ  NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEMPAR_ESTABEL_FK_I ON TASY.PLS_EMAIL_PARAMETROS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMPAR_IE_ORIGEMI ON TASY.PLS_EMAIL_PARAMETROS
(IE_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSEMPAR_PK ON TASY.PLS_EMAIL_PARAMETROS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_email_parametros_before
before insert or update ON TASY.PLS_EMAIL_PARAMETROS for each row
begin

if	(:new.ds_senha_aut_envio <> :old.ds_senha_aut_envio) or
	((:old.ds_senha_aut_envio is null) and (:new.ds_senha_aut_envio is not null)) then
	:new.ds_senha_aut_envio := wheb_seguranca.encrypt(:new.ds_senha_aut_envio);
end if;

if	(:new.ds_senha_rede <> :old.ds_senha_rede) or
	((:old.ds_senha_rede is null) and (:new.ds_senha_rede is not null)) then
	:new.ds_senha_rede := wheb_seguranca.encrypt(:new.ds_senha_rede);
end if;

end pls_email_parametros_before;
/


CREATE OR REPLACE TRIGGER TASY.PLS_EMAIL_PARAMETROS_tp  after update ON TASY.PLS_EMAIL_PARAMETROS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_SERVIDOR_ENVIO,1,4000),substr(:new.NM_SERVIDOR_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_SERVIDOR_ENVIO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USERID_ENVIO,1,4000),substr(:new.NM_USERID_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USERID_ENVIO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_AUT_ENVIO,1,4000),substr(:new.DS_SENHA_AUT_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_AUT_ENVIO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PORTA_ENVIO,1,4000),substr(:new.DS_PORTA_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PORTA_ENVIO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROTOCOLO_SSL,1,4000),substr(:new.IE_PROTOCOLO_SSL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROTOCOLO_SSL',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRIORIDADE,1,4000),substr(:new.CD_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRIORIDADE',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM,1,4000),substr(:new.IE_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIMPAR_ANEXO,1,4000),substr(:new.IE_LIMPAR_ANEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIMPAR_ANEXO',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CAMINHO_ANEXO_SERV,1,4000),substr(:new.DS_CAMINHO_ANEXO_SERV,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_ANEXO_SERV',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CAMINHO_ANEXO_REDE,1,4000),substr(:new.DS_CAMINHO_ANEXO_REDE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMINHO_ANEXO_REDE',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_METODO_AUT_SSL,1,4000),substr(:new.IE_METODO_AUT_SSL,1,4000),:new.nm_usuario,nr_seq_w,'IE_METODO_AUT_SSL',ie_log_w,ds_w,'PLS_EMAIL_PARAMETROS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_EMAIL_PARAMETROS ADD (
  CONSTRAINT PLSEMPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EMAIL_PARAMETROS ADD (
  CONSTRAINT PLSEMPAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_EMAIL_PARAMETROS TO NIVEL_1;

