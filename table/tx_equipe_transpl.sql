ALTER TABLE TASY.TX_EQUIPE_TRANSPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_EQUIPE_TRANSPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_EQUIPE_TRANSPL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DS_EQUIPE                  VARCHAR2(80 BYTE)  NOT NULL,
  NR_SEQ_EQUIPE              NUMBER(10)         NOT NULL,
  CD_CGC                     VARCHAR2(14 BYTE)  NOT NULL,
  NR_SNT                     VARCHAR2(20 BYTE)  NOT NULL,
  NR_PORTARIA_CRED           NUMBER(5)          NOT NULL,
  DT_CREDENCIAMENTO          DATE               NOT NULL,
  NR_PORTARIA_EXCLUSAO       NUMBER(5),
  DT_EXCLUSAO                DATE,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  IE_FORMA_RECREDENCIAMENTO  VARCHAR2(15 BYTE),
  QT_DIAS_ANTECEDENCIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TXEQTR_ESTABEL_FK_I ON TASY.TX_EQUIPE_TRANSPL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQTR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXEQTR_PFEQUIP_FK_I ON TASY.TX_EQUIPE_TRANSPL
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQTR_PFEQUIP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXEQTR_PK ON TASY.TX_EQUIPE_TRANSPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQTR_PK
  MONITORING USAGE;


ALTER TABLE TASY.TX_EQUIPE_TRANSPL ADD (
  CONSTRAINT TXEQTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_EQUIPE_TRANSPL ADD (
  CONSTRAINT TXEQTR_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT TXEQTR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TX_EQUIPE_TRANSPL TO NIVEL_1;

