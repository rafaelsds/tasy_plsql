ALTER TABLE TASY.PLS_EVENTO_REGRA_TIT_PAGAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EVENTO_REGRA_TIT_PAGAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  DT_FIM_VIGENCIA          DATE,
  IE_ORIGEM_TITULO         VARCHAR2(10 BYTE),
  NR_SEQ_TRANS_FIN_CONTAB  NUMBER(10),
  IE_TIPO_PESSOA           VARCHAR2(3 BYTE)     NOT NULL,
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  CD_CGC                   VARCHAR2(14 BYTE),
  IE_TIPO_TITULO           VARCHAR2(2 BYTE),
  NR_SEQ_EVENTO            NUMBER(10)           NOT NULL,
  IE_INCLUI_EVENTO         VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_PRESTADOR_DEST    NUMBER(10),
  NR_SEQ_CLASSE_TIT_PAGAR  NUMBER(10),
  CD_CENTRO_CUSTO          NUMBER(8),
  IE_ORDENACAO_SUGERIDA    VARCHAR2(1 BYTE),
  NR_ORDEM_EXECUCAO        NUMBER(20),
  DT_INICIO_VIGENCIA_REF   DATE,
  DT_FIM_VIGENCIA_REF      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEVTP_CENCUST_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVTP_CLTITPA_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(NR_SEQ_CLASSE_TIT_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVTP_CLTITPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVTP_I1 ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVTP_PESFISI_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVTP_PESJURI_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVTP_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSEVTP_PK ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVTP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVTP_PLSEVEN_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVTP_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVTP_PLSPRES_FK_I ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR
(NR_SEQ_PRESTADOR_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVTP_PLSPRES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_EVENTO_REGRA_TIT_PAGAR_tp  after update ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ORDEM_EXECUCAO,1,4000),substr(:new.NR_ORDEM_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXECUCAO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_TIT_PAGAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDENACAO_SUGERIDA,1,4000),substr(:new.IE_ORDENACAO_SUGERIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SUGERIDA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_TIT_PAGAR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_evento_regra_tit_pag_atual
before insert or update ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR for each row
declare

begin

-- previnir que seja gravada a hora junto na regra, essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;

if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia, 'F');

end pls_evento_regra_tit_pagar;
/


ALTER TABLE TASY.PLS_EVENTO_REGRA_TIT_PAGAR ADD (
  CONSTRAINT PLSEVTP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EVENTO_REGRA_TIT_PAGAR ADD (
  CONSTRAINT PLSEVTP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSEVTP_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSEVTP_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSEVTP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_DEST) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSEVTP_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_TIT_PAGAR) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT PLSEVTP_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.PLS_EVENTO_REGRA_TIT_PAGAR TO NIVEL_1;

