ALTER TABLE TASY.FUNCAO_PARAM_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_PARAM_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_PARAM_USUARIO
(
  CD_FUNCAO           NUMBER(5)                 NOT NULL,
  NR_SEQUENCIA        NUMBER(5)                 NOT NULL,
  NM_USUARIO_PARAM    VARCHAR2(15 BYTE)         NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  VL_PARAMETRO        VARCHAR2(255 BYTE),
  DS_OBSERVACAO       VARCHAR2(4000 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  NR_SEQ_INTERNO      NUMBER(10)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNPAUS_ESTABEL_FK_I ON TASY.FUNCAO_PARAM_USUARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNPAUS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FUNPAUS_PK ON TASY.FUNCAO_PARAM_USUARIO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNPAUS_UK ON TASY.FUNCAO_PARAM_USUARIO
(CD_FUNCAO, NR_SEQUENCIA, NM_USUARIO_PARAM, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPAUS_USUARIO_FK_I ON TASY.FUNCAO_PARAM_USUARIO
(NM_USUARIO_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.funcao_param_usuario_dt_param
after INSERT OR DELETE OR UPDATE ON TASY.FUNCAO_PARAM_USUARIO for each row
declare
	ie_consistir_valor_w	varchar2(1);
	vl_parametro_padrao_w	varchar2(255);
begin

update	funcao
set	dt_atualizacao_param = sysdate
where	cd_funcao = nvl(:new.cd_funcao,:old.cd_funcao);

if (inserting or updating) then
	select	ie_consistir_valor,
		vl_parametro_padrao
	into	ie_consistir_valor_w,
		vl_parametro_padrao_w
	from	funcao_parametro
	where	cd_funcao = nvl(:new.cd_funcao, :old.cd_funcao)
	and	nr_sequencia = nvl(:new.nr_sequencia, :old.nr_sequencia);

	if	(ie_consistir_valor_w = 'S') and
		(somente_numero(:new.vl_parametro) < somente_numero(vl_parametro_padrao_w)) then
		wheb_mensagem_pck.exibir_mensagem_abort(55208);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_usuario_insert
BEFORE INSERT ON TASY.FUNCAO_PARAM_USUARIO FOR EACH ROW
DECLARE
nr_seq_interno_w NUMBER(10);

BEGIN

IF (:NEW.nr_seq_interno IS NULL) THEN
 SELECT funcao_param_usuario_seq.NEXTVAL
 INTO nr_seq_interno_w
 FROM dual;

 :NEW.nr_seq_interno := nr_seq_interno_w;
END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_usuario_subject FOR
    DELETE OR INSERT OR UPDATE ON TASY.FUNCAO_PARAM_USUARIO 
COMPOUND TRIGGER
    apply_sync BOOLEAN := false;

    TYPE changed_subjects_type IS TABLE OF funcao_param_usuario.nm_usuario_param%TYPE;
    changed_subjects changed_subjects_type := changed_subjects_type ();

    AFTER EACH ROW IS BEGIN
        apply_sync := psa_is_auth_parameter(:new.cd_funcao,:new.nr_sequencia) = 'TRUE' OR apply_sync;

        changed_subjects.extend;
        IF
            inserting OR updating
        THEN
            changed_subjects(changed_subjects.last) :=:new.nm_usuario_param;
        ELSE
            changed_subjects(changed_subjects.last) :=:old.nm_usuario_param;
        END IF;
    END AFTER EACH ROW;

    AFTER STATEMENT IS BEGIN
        IF
            apply_sync
        THEN
            FOR i IN 1..changed_subjects.count LOOP
                psa_synchronize_subject(changed_subjects(i));
            END LOOP;

        END IF;
    END AFTER STATEMENT;
END;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAM_USUARIO_tp  after update ON TASY.FUNCAO_PARAM_USUARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_INTERNO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'FUNCAO_PARAM_USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO_PARAM_USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'FUNCAO_PARAM_USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_PARAM,1,4000),substr(:new.NM_USUARIO_PARAM,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_PARAM',ie_log_w,ds_w,'FUNCAO_PARAM_USUARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'FUNCAO_PARAM_USUARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.update_param_usuario
before update or insert ON TASY.FUNCAO_PARAM_USUARIO for each row
declare

pragma autonomous_transaction;

begin

IF INSTR(:NEW.VL_PARAMETRO, '#@') > 0 THEN
  wheb_mensagem_pck.exibir_mensagem_abort(1040016);
END IF;

atualizar_log_alt_parametros(
			:new.nm_usuario,
			:new.cd_estabelecimento,
			null,
			null,
			null,
			null,
			:new.NM_USUARIO_PARAM,
			:old.NM_USUARIO_PARAM,
			:new.nr_sequencia,
			:new.vl_parametro,
			:old.vl_parametro,
			'U',
			:new.cd_funcao);

commit;

end;
/


ALTER TABLE TASY.FUNCAO_PARAM_USUARIO ADD (
  CONSTRAINT FUNPAUS_PK
 PRIMARY KEY
 (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT FUNPAUS_UK
 UNIQUE (CD_FUNCAO, NR_SEQUENCIA, NM_USUARIO_PARAM, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_PARAM_USUARIO ADD (
  CONSTRAINT FUNPAUS_FUNPARA_FK 
 FOREIGN KEY (CD_FUNCAO, NR_SEQUENCIA) 
 REFERENCES TASY.FUNCAO_PARAMETRO (CD_FUNCAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT FUNPAUS_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_PARAM) 
 REFERENCES TASY.USUARIO (NM_USUARIO)
    ON DELETE CASCADE,
  CONSTRAINT FUNPAUS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.FUNCAO_PARAM_USUARIO TO NIVEL_1;

