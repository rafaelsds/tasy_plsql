ALTER TABLE TASY.PLS_CAMARA_CALENDARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CAMARA_CALENDARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CAMARA_CALENDARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ANO               NUMBER(5)                NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_CAMARA        NUMBER(10)               NOT NULL,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCCAL_ESTABEL_FK_I ON TASY.PLS_CAMARA_CALENDARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCAL_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCCAL_PK ON TASY.PLS_CAMARA_CALENDARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCAL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCCAL_PLSCACO_FK_I ON TASY.PLS_CAMARA_CALENDARIO
(NR_SEQ_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCAL_PLSCACO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCCAL_UK ON TASY.PLS_CAMARA_CALENDARIO
(NR_ANO, NR_SEQ_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCAL_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_camara_calend_after_update
after update ON TASY.PLS_CAMARA_CALENDARIO for each row
declare

procedure inserir_log_calendario(	nm_campo_p		varchar2,
					vl_antigo_p		varchar2,
					vl_novo_p		varchar2) is

ds_historico_w	varchar2(4000);
nm_campo_w	varchar2(200);

begin

	select 	nvl(ds_label, ds_label_grid)
	into	nm_campo_w
	from	tabela_atributo
	where 	nm_tabela = 'PLS_CAMARA_CALENDARIO'
	and	nm_atributo = nm_campo_p;

ds_historico_w := wheb_mensagem_pck.get_texto(1111922, 	'NM_CAMPO=' 	|| nm_campo_w 			|| ';' ||
							'VL_ANTIGO=' 	|| nvl(vl_antigo_p, 'null') 	|| ';' ||
							'VL_NOVO=' 	|| nvl(vl_novo_p, 'null'));

insert into pls_hist_camara_calend
	(nr_sequencia,
	cd_estabelecimento,
	ds_historico,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_tabela,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_camara_calend)
values (pls_hist_camara_calend_seq.nextval,
	wheb_usuario_pck.get_cd_estabelecimento,
	ds_historico_w,
	sysdate,
	sysdate,
	'PLS_CAMARA_CALENDARIO',
	wheb_usuario_pck.get_nm_usuario,
	wheb_usuario_pck.get_nm_usuario,
	:new.nr_sequencia);

end inserir_log_calendario;

begin

if	(nvl(:new.nm_usuario_lib, 'X') <> nvl(:old.nm_usuario_lib, 'X')) then
	inserir_log_calendario('NM_USUARIO_LIB', :old.nm_usuario_lib, :new.nm_usuario_lib);
end if;

if	(nvl(:new.dt_liberacao, to_date('30/12/1899', 'dd/mm/yyyy')) <> nvl(:old.dt_liberacao, to_date('30/12/1899', 'dd/mm/yyyy'))) then
	inserir_log_calendario('DT_LIBERACAO', to_char(:old.dt_liberacao,'dd/mm/rrrr'), to_char(:new.dt_liberacao,'dd/mm/rrrr'));
end if;

if	(nvl(:new.ds_observacao, '') <> nvl(:old.ds_observacao, '')) then
	inserir_log_calendario('DS_OBSERVACAO', :old.ds_observacao, :new.ds_observacao);
end if;

if	(nvl(:new.nr_ano, 0) <> nvl(:old.nr_ano, 0)) then
	inserir_log_calendario('NR_ANO', :old.nr_ano, :new.nr_ano);
end if;

if	(nvl(:new.nr_seq_camara, 0) <> nvl(:old.nr_seq_camara, 0)) then
	inserir_log_calendario('NR_SEQ_CAMARA', :old.nr_seq_camara, :new.nr_seq_camara);
end if;

end;
/


ALTER TABLE TASY.PLS_CAMARA_CALENDARIO ADD (
  CONSTRAINT PLSCCAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSCCAL_UK
 UNIQUE (NR_ANO, NR_SEQ_CAMARA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CAMARA_CALENDARIO ADD (
  CONSTRAINT PLSCCAL_PLSCACO_FK 
 FOREIGN KEY (NR_SEQ_CAMARA) 
 REFERENCES TASY.PLS_CAMARA_COMPENSACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCCAL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_CAMARA_CALENDARIO TO NIVEL_1;

