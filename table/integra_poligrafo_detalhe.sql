ALTER TABLE TASY.INTEGRA_POLIGRAFO_DETALHE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTEGRA_POLIGRAFO_DETALHE CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTEGRA_POLIGRAFO_DETALHE
(
  OBX1                 VARCHAR2(15 BYTE),
  OBX2                 NUMBER(10),
  OBX3                 VARCHAR2(60 BYTE),
  OBX4                 VARCHAR2(60 BYTE),
  OBX5                 VARCHAR2(60 BYTE),
  OBX6                 NUMBER(6),
  OBX7                 NUMBER(4),
  OBX8                 VARCHAR2(10 BYTE),
  OBX9                 DATE,
  OBX10                VARCHAR2(10 BYTE),
  OBX11                DATE,
  OBX12                VARCHAR2(10 BYTE),
  OBX13                VARCHAR2(10 BYTE),
  OBX14                DATE,
  OBX15                VARCHAR2(10 BYTE),
  OBX16                VARCHAR2(10 BYTE),
  OBX17                VARCHAR2(10 BYTE),
  OBX18                VARCHAR2(10 BYTE),
  OBX19                VARCHAR2(10 BYTE),
  OBX20                VARCHAR2(10 BYTE),
  OBX21                VARCHAR2(10 BYTE),
  OBX22                VARCHAR2(10 BYTE),
  OBX23                VARCHAR2(10 BYTE),
  OBX24                VARCHAR2(10 BYTE),
  OBX25                VARCHAR2(10 BYTE),
  OBX26                VARCHAR2(10 BYTE),
  OBX27                VARCHAR2(10 BYTE),
  OBX28                VARCHAR2(10 BYTE),
  OBX29                VARCHAR2(10 BYTE),
  OBX30                VARCHAR2(10 BYTE),
  OBX31                VARCHAR2(10 BYTE),
  OBX32                VARCHAR2(10 BYTE),
  OBX33                VARCHAR2(10 BYTE),
  OBX34                VARCHAR2(10 BYTE),
  OBX35                VARCHAR2(10 BYTE),
  OBX36                VARCHAR2(10 BYTE),
  OBX37                VARCHAR2(10 BYTE),
  OBX38                VARCHAR2(10 BYTE),
  OBX39                VARCHAR2(10 BYTE),
  OBX40                VARCHAR2(10 BYTE),
  OBX41                VARCHAR2(10 BYTE),
  OBX42                VARCHAR2(10 BYTE),
  OBX43                VARCHAR2(10 BYTE),
  OBX44                VARCHAR2(10 BYTE),
  OBX45                VARCHAR2(10 BYTE),
  OBX46                VARCHAR2(10 BYTE),
  OBX47                VARCHAR2(10 BYTE),
  OBX48                VARCHAR2(10 BYTE),
  OBX49                VARCHAR2(10 BYTE),
  OBX50                VARCHAR2(10 BYTE),
  OBX51                VARCHAR2(10 BYTE),
  OBX52                VARCHAR2(10 BYTE),
  OBX53                VARCHAR2(10 BYTE),
  OBX54                VARCHAR2(10 BYTE),
  OBX55                VARCHAR2(10 BYTE),
  OBX56                VARCHAR2(10 BYTE),
  OBX57                VARCHAR2(10 BYTE),
  OBX58                VARCHAR2(10 BYTE),
  OBX59                VARCHAR2(10 BYTE),
  OBX60                VARCHAR2(10 BYTE),
  OBX61                VARCHAR2(10 BYTE),
  OBX62                VARCHAR2(10 BYTE),
  OBX63                VARCHAR2(10 BYTE),
  OBX64                VARCHAR2(10 BYTE),
  OBX65                VARCHAR2(10 BYTE),
  OBX66                VARCHAR2(10 BYTE),
  OBX67                VARCHAR2(10 BYTE),
  OBX68                VARCHAR2(10 BYTE),
  OBX69                VARCHAR2(10 BYTE),
  OBX70                VARCHAR2(10 BYTE),
  OBX71                VARCHAR2(10 BYTE),
  OBX72                VARCHAR2(10 BYTE),
  OBX73                VARCHAR2(10 BYTE),
  OBX74                VARCHAR2(10 BYTE),
  OBX75                VARCHAR2(10 BYTE),
  OBX76                VARCHAR2(10 BYTE),
  OBX77                VARCHAR2(10 BYTE),
  OBX78                VARCHAR2(10 BYTE),
  OBX79                VARCHAR2(10 BYTE),
  OBX80                VARCHAR2(10 BYTE),
  OBX81                VARCHAR2(10 BYTE),
  OBX82                VARCHAR2(10 BYTE),
  OBX83                VARCHAR2(10 BYTE),
  OBX84                VARCHAR2(10 BYTE),
  OBX85                VARCHAR2(10 BYTE),
  OBX86                VARCHAR2(10 BYTE),
  OBX87                VARCHAR2(10 BYTE),
  OBX88                VARCHAR2(10 BYTE),
  OBX89                VARCHAR2(10 BYTE),
  OBX90                VARCHAR2(10 BYTE),
  OBX91                VARCHAR2(10 BYTE),
  OBX92                VARCHAR2(10 BYTE),
  OBX93                VARCHAR2(10 BYTE),
  OBX94                VARCHAR2(10 BYTE),
  OBX95                VARCHAR2(10 BYTE),
  OBX96                VARCHAR2(10 BYTE),
  OBX97                VARCHAR2(10 BYTE),
  OBX98                VARCHAR2(10 BYTE),
  OBX99                VARCHAR2(10 BYTE),
  OBX100               VARCHAR2(10 BYTE),
  OBX101               VARCHAR2(10 BYTE),
  OBX102               VARCHAR2(10 BYTE),
  OBX103               VARCHAR2(10 BYTE),
  OBX104               VARCHAR2(10 BYTE),
  OBX105               VARCHAR2(10 BYTE),
  OBX106               VARCHAR2(10 BYTE),
  OBX107               VARCHAR2(10 BYTE),
  OBX108               VARCHAR2(10 BYTE),
  OBX109               VARCHAR2(10 BYTE),
  OBX110               VARCHAR2(10 BYTE),
  OBX111               VARCHAR2(10 BYTE),
  OBX112               VARCHAR2(10 BYTE),
  OBX113               VARCHAR2(10 BYTE),
  OBX114               VARCHAR2(10 BYTE),
  OBX115               VARCHAR2(10 BYTE),
  OBX116               VARCHAR2(10 BYTE),
  OBX117               VARCHAR2(10 BYTE),
  OBX118               VARCHAR2(10 BYTE),
  OBX119               VARCHAR2(10 BYTE),
  OBX120               VARCHAR2(10 BYTE),
  OBX121               VARCHAR2(10 BYTE),
  OBX122               VARCHAR2(10 BYTE),
  OBX123               VARCHAR2(10 BYTE),
  OBX124               VARCHAR2(10 BYTE),
  OBX125               VARCHAR2(10 BYTE),
  OBX126               VARCHAR2(10 BYTE),
  OBX127               VARCHAR2(10 BYTE),
  OBX128               VARCHAR2(10 BYTE),
  OBX129               VARCHAR2(10 BYTE),
  OBX130               VARCHAR2(10 BYTE),
  OBX131               VARCHAR2(10 BYTE),
  OBX132               VARCHAR2(10 BYTE),
  OBX133               VARCHAR2(10 BYTE),
  OBX134               VARCHAR2(10 BYTE),
  OBX135               VARCHAR2(10 BYTE),
  OBX136               VARCHAR2(10 BYTE),
  OBX138               VARCHAR2(10 BYTE),
  OBX137               VARCHAR2(10 BYTE),
  OBX139               VARCHAR2(10 BYTE),
  OBX140               VARCHAR2(10 BYTE),
  OBX141               VARCHAR2(10 BYTE),
  OBX142               VARCHAR2(10 BYTE),
  OBX143               VARCHAR2(10 BYTE),
  OBX144               VARCHAR2(10 BYTE),
  OBX145               VARCHAR2(10 BYTE),
  OBX146               VARCHAR2(10 BYTE),
  OBX148               VARCHAR2(10 BYTE),
  OBX149               VARCHAR2(10 BYTE),
  OBX150               VARCHAR2(10 BYTE),
  OBX151               VARCHAR2(10 BYTE),
  OBX152               VARCHAR2(10 BYTE),
  OBX162               VARCHAR2(10 BYTE),
  OBX153               VARCHAR2(10 BYTE),
  OBX154               VARCHAR2(10 BYTE),
  OBX155               VARCHAR2(10 BYTE),
  OBX156               VARCHAR2(10 BYTE),
  OBX157               VARCHAR2(10 BYTE),
  OBX147               VARCHAR2(10 BYTE),
  OBX158               VARCHAR2(10 BYTE),
  OBX159               VARCHAR2(10 BYTE),
  OBX160               VARCHAR2(10 BYTE),
  OBX161               VARCHAR2(10 BYTE),
  OBX163               VARCHAR2(10 BYTE),
  OBX164               VARCHAR2(10 BYTE),
  OBX165               VARCHAR2(10 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NR_SEQUENCIA         NUMBER(10),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  OBX166               VARCHAR2(400 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ITPODE_PESFISI_FK_I ON TASY.INTEGRA_POLIGRAFO_DETALHE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ITPODE_PK ON TASY.INTEGRA_POLIGRAFO_DETALHE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTEGRA_POLIGRAFO_DETALHE ADD (
  CONSTRAINT ITPODE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTEGRA_POLIGRAFO_DETALHE ADD (
  CONSTRAINT ITPODE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.INTEGRA_POLIGRAFO_DETALHE TO NIVEL_1;

