ALTER TABLE TASY.PRESCR_OPM_COMPONENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_OPM_COMPONENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_OPM_COMPONENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  NR_SEQ_OPM           NUMBER(10),
  CD_MATERIAL_PADRAO   NUMBER(10)               NOT NULL,
  IE_STATUS_SALDO      VARCHAR2(15 BYTE),
  QT_MATERIAL          NUMBER(15,6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPCOM_MATERIA_FK_I ON TASY.PRESCR_OPM_COMPONENTE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPCOM_MATERIA_FK2_I ON TASY.PRESCR_OPM_COMPONENTE
(CD_MATERIAL_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROPCOM_PK ON TASY.PRESCR_OPM_COMPONENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPCOM_PRESOPM_FK_I ON TASY.PRESCR_OPM_COMPONENTE
(NR_SEQ_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRESCR_OPM_COMPONENTE ADD (
  CONSTRAINT PROPCOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_OPM_COMPONENTE ADD (
  CONSTRAINT PROPCOM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PROPCOM_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_PADRAO) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PROPCOM_PRESOPM_FK 
 FOREIGN KEY (NR_SEQ_OPM) 
 REFERENCES TASY.PRESCR_OPM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_OPM_COMPONENTE TO NIVEL_1;

