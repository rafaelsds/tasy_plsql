ALTER TABLE TASY.SETOR_TEMPERATURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SETOR_TEMPERATURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SETOR_TEMPERATURA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO   NUMBER(10)             NOT NULL,
  DT_HORA_INICIO         DATE                   NOT NULL,
  DT_HORA_FIM            DATE,
  QT_TEMP_INICIAL        NUMBER(7,2),
  QT_TEMP_MIN            NUMBER(7,2),
  QT_TEMP_MAX            NUMBER(7,2),
  NM_USUARIO_FECHAMENTO  VARCHAR2(15 BYTE),
  QT_UMIDADE_FINAL       NUMBER(7,2),
  NR_SEQ_TURNO           NUMBER(10),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DT_MEDICAO             DATE,
  QT_UMIDADE_MAX         NUMBER(4,2),
  QT_UMIDADE_MIN         NUMBER(4,2),
  VL_PRESSAO_COMP        NUMBER(15,2),
  PR_NIVEL_HELIO         NUMBER(15,2),
  QT_TEMP_AGUA_ENT       NUMBER(15,2),
  QT_TEMP_AGUA_SAI       NUMBER(15,2),
  VL_HORIMETRO_COMP      NUMBER(15,2),
  NR_SEQ_AMBIENTE        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SETTEMP_AMBTEMP_FK_I ON TASY.SETOR_TEMPERATURA
(NR_SEQ_AMBIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SETTEMP_PK ON TASY.SETOR_TEMPERATURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SETTEMP_SETATEN_FK_I ON TASY.SETOR_TEMPERATURA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SETTEMP_TURTEMP_FK_I ON TASY.SETOR_TEMPERATURA
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SETTEMP_USUARIO_FK_I ON TASY.SETOR_TEMPERATURA
(NM_USUARIO_FECHAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SETOR_TEMPERATURA ADD (
  CONSTRAINT SETTEMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SETOR_TEMPERATURA ADD (
  CONSTRAINT SETTEMP_AMBTEMP_FK 
 FOREIGN KEY (NR_SEQ_AMBIENTE) 
 REFERENCES TASY.AMBIENTE_TEMPERATURA (NR_SEQUENCIA),
  CONSTRAINT SETTEMP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SETTEMP_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_FECHAMENTO) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT SETTEMP_TURTEMP_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.TURNO_TEMPERATURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SETOR_TEMPERATURA TO NIVEL_1;

