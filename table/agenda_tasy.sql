ALTER TABLE TASY.AGENDA_TASY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_TASY
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_AGENDA               VARCHAR2(100 BYTE),
  DT_AGENDA               DATE                  NOT NULL,
  NR_SEQ_TIPO             NUMBER(10)            NOT NULL,
  NR_MINUTO_DURACAO       NUMBER(10)            NOT NULL,
  NM_USUARIO_AGENDA       VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DS_LOCAL                VARCHAR2(50 BYTE),
  IE_STATUS               VARCHAR2(1 BYTE)      NOT NULL,
  DT_FINAL                DATE,
  NR_SEQ_LOCAL            NUMBER(10),
  NR_SEQ_AGENDA_CONVITE   NUMBER(10),
  NR_ORDEM_SERVICO        NUMBER(10),
  DT_CONFIRMACAO          DATE,
  NM_USUARIO_CONFIRMACAO  VARCHAR2(15 BYTE),
  DS_OBS_CONFIRMACAO      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGETASY_AGETASY_FK_I ON TASY.AGENDA_TASY
(NR_SEQ_AGENDA_CONVITE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETASY_AGETASY_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETASY_I1 ON TASY.AGENDA_TASY
(DT_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETASY_I2 ON TASY.AGENDA_TASY
(NM_USUARIO_AGENDA, IE_STATUS, DT_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETASY_LOAGTAS_FK_I ON TASY.AGENDA_TASY
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETASY_LOAGTAS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETASY_MANORSE_FK_I ON TASY.AGENDA_TASY
(NR_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGETASY_PK ON TASY.AGENDA_TASY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETASY_TIPAGTA_FK_I ON TASY.AGENDA_TASY
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETASY_TIPAGTA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_tasy_delete
before delete ON TASY.AGENDA_TASY for each row
declare

ds_id_google_w	varchar2(255);

pragma autonomous_transaction;
begin

select	max(ds_id_google)
into	ds_id_google_w
from	agenda_google_cal_hist
where	nr_seq_agenda = :old.nr_sequencia;

if	( ds_id_google_w is not null ) then
	insert into agenda_google_cal_del (
		nr_sequencia,
		ds_id_google,
		nm_usuario_agenda,
		nr_seq_agenda,
		dt_atualizacao,
		nm_usuario
	) values (
		agenda_google_cal_del_seq.nextval,
		ds_id_google_w,
		:old.nm_usuario_agenda,
		:old.nr_sequencia,
		sysdate,
		:old.nm_usuario_agenda
	);

	commit;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Tasy_Update
AFTER UPDATE ON TASY.AGENDA_TASY FOR EACH ROW
DECLARE

nr_seq_comunicacao_w	number(10,0);
nm_usuario_conv_w	varchar2(15);
nm_usuario_destino_w	varchar2(4000);
nr_seq_agenda_google_w	number(10);
ds_id_google_w		varchar2(255);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(773957, null, wheb_usuario_pck.get_nr_seq_idioma);--Cancelamento de Reuni�o
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(732872, null, wheb_usuario_pck.get_nr_seq_idioma);--do dia

cursor	c01 is
	select	nm_usuario_convidado
	from	agenda_tasy_convite
	where	nr_seq_agenda	= :new.nr_sequencia;

begin

if	(:new.ie_status = 'C') then
	begin

	nm_usuario_destino_w	:= '';

	open	c01;
	loop
	fetch	c01 into
		nm_usuario_conv_w;
	exit	when c01%notfound;
		begin

		nm_usuario_destino_w	:= nm_usuario_destino_w ||
						nm_usuario_conv_w || ', ';

		end;
	end loop;
	close c01;

	if	(length(nm_usuario_destino_w) > 0) then
		begin

		select	comunic_interna_seq.nextval
		into	nr_seq_comunicacao_w
		from	dual;

		insert	into comunic_interna
			(dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			cd_perfil,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			ds_perfil_adicional,
			cd_setor_destino,
			cd_estab_destino,
			ds_setor_adicional,dt_liberacao)
		values	(sysdate,
			expressao1_w || ' ' || :new.ds_agenda,
			expressao1_w || ' ' || :new.ds_agenda || ' ' ||expressao2_w|| ' ' || to_char(:new.dt_agenda, 'dd/mm/yyyy hh24:mi:ss'),
			:new.nm_usuario,
			sysdate,
			'N',
			nm_usuario_destino_w,
			null,
			nr_seq_comunicacao_w,
			'N', null, null, null, null, null,
			sysdate);

		end;
	end if;

	select	nvl(max(nr_sequencia), 0),
		max(ds_id_google)
	into	nr_seq_agenda_google_w,
		ds_id_google_w
	from	agenda_google_cal_hist
	where	nr_seq_agenda = :new.nr_sequencia;

	if	(nr_seq_agenda_google_w > 0) then
		insert into agenda_google_cal_del (
			nr_sequencia,
			ds_id_google,
			nm_usuario_agenda,
			nr_seq_agenda,
			dt_atualizacao,
			nm_usuario
		) values (
			agenda_google_cal_del_seq.nextval,
			ds_id_google_w,
			:new.nm_usuario_agenda,
			:new.nr_sequencia,
			sysdate,
			:new.nm_usuario_agenda
		);
	end if;

	end;

end if;

end;
/


ALTER TABLE TASY.AGENDA_TASY ADD (
  CONSTRAINT AGETASY_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_TASY ADD (
  CONSTRAINT AGETASY_MANORSE_FK 
 FOREIGN KEY (NR_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT AGETASY_AGETASY_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONVITE) 
 REFERENCES TASY.AGENDA_TASY (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGETASY_LOAGTAS_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.LOCAL_AGENDA_TASY (NR_SEQUENCIA),
  CONSTRAINT AGETASY_TIPAGTA_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_AGENDA_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_TASY TO NIVEL_1;

