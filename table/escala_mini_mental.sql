ALTER TABLE TASY.ESCALA_MINI_MENTAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MINI_MENTAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MINI_MENTAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_DIA_SEMANA              VARCHAR2(1 BYTE),
  IE_DIA_MES                 VARCHAR2(1 BYTE),
  IE_MES                     VARCHAR2(1 BYTE),
  IE_ANO                     VARCHAR2(1 BYTE),
  IE_HORA_APROXIMADA         VARCHAR2(1 BYTE),
  IE_LOCAL_ESPEC             VARCHAR2(1 BYTE),
  IE_INSTITUICAO             VARCHAR2(1 BYTE),
  IE_BAIRRO                  VARCHAR2(1 BYTE),
  IE_CIDADE                  VARCHAR2(1 BYTE),
  IE_ESTADO                  VARCHAR2(1 BYTE),
  QT_PALAVRA                 NUMBER(1),
  QT_CALCULO                 NUMBER(1),
  QT_EVOCACAO                NUMBER(1),
  IE_NOMEAR                  VARCHAR2(1 BYTE)   NOT NULL,
  QT_SCORE                   NUMBER(2),
  IE_REPETIR                 VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMANDO                 VARCHAR2(1 BYTE)   NOT NULL,
  IE_LER_OBEDECER            VARCHAR2(1 BYTE)   NOT NULL,
  IE_ESCREVER                VARCHAR2(1 BYTE)   NOT NULL,
  IE_COPIAR_DESENHO          VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMANDO_DOBRE           VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMANDO_COLOQUE         VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMIME_ATEPACI_FK_I ON TASY.ESCALA_MINI_MENTAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMIME_PESFISI_FK_I ON TASY.ESCALA_MINI_MENTAL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMIME_PK ON TASY.ESCALA_MINI_MENTAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMIME_TASASDI_FK_I ON TASY.ESCALA_MINI_MENTAL
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMIME_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMIME_TASASDI_FK2_I ON TASY.ESCALA_MINI_MENTAL
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMIME_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_MINI_MENTAL_INSERT
before insert or update ON TASY.ESCALA_MINI_MENTAL for each row
declare
qt_pontuacao_total_w	number(10) := 0;

begin

Consiste_Liberacao_Escala(101);


if	(:new.ie_dia_semana = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_dia_mes = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_mes = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_ano = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_hora_aproximada = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_local_espec = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_instituicao = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_bairro = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_cidade = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_estado = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

qt_pontuacao_total_w 	:= qt_pontuacao_total_w + :new.qt_palavra;

qt_pontuacao_total_w	:= qt_pontuacao_total_w + :new.qt_calculo;

qt_pontuacao_total_w	:= qt_pontuacao_total_w + :new.qt_evocacao;

if	(:new.ie_nomear = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 2;
end if;

if	(:new.ie_repetir = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_comando = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;
if	(:new.IE_COMANDO_DOBRE = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;
if	(:new.IE_COMANDO_COLOQUE = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_ler_obedecer = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_copiar_desenho = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

if	(:new.ie_escrever = 'S') then
	qt_pontuacao_total_w := qt_pontuacao_total_w + 1;
end if;

:new.qt_score := nvl(qt_pontuacao_total_w,0);

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_mini_ment_pend_atual
after insert or update ON TASY.ESCALA_MINI_MENTAL for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'101');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MINI_MENTAL_delete
after delete ON TASY.ESCALA_MINI_MENTAL for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;


begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '101'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_MINI_MENTAL ADD (
  CONSTRAINT ESCMIME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MINI_MENTAL ADD (
  CONSTRAINT ESCMIME_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMIME_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMIME_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMIME_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_MINI_MENTAL TO NIVEL_1;

