ALTER TABLE TASY.PATIENT_IMPOT_TEMPLATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATIENT_IMPOT_TEMPLATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATIENT_IMPOT_TEMPLATE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TEMPLATE      NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAIMTEMP_EHRTEMP_FK_I ON TASY.PATIENT_IMPOT_TEMPLATE
(NR_SEQ_TEMPLATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAIMTEMP_PK ON TASY.PATIENT_IMPOT_TEMPLATE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PATIENT_IMPOT_TEMPLATE ADD (
  CONSTRAINT PAIMTEMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PATIENT_IMPOT_TEMPLATE ADD (
  CONSTRAINT PAIMTEMP_EHRTEMP_FK 
 FOREIGN KEY (NR_SEQ_TEMPLATE) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PATIENT_IMPOT_TEMPLATE TO NIVEL_1;

