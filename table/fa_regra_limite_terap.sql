ALTER TABLE TASY.FA_REGRA_LIMITE_TERAP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FA_REGRA_LIMITE_TERAP CASCADE CONSTRAINTS;

CREATE TABLE TASY.FA_REGRA_LIMITE_TERAP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_INTERVALO         VARCHAR2(7 BYTE),
  NR_SEQ_MEDIC         NUMBER(10)               NOT NULL,
  NR_DIAS_UTILIZACAO   NUMBER(5),
  QT_IDADE_MIN         NUMBER(15,2),
  QT_IDADE_MIN_MES     NUMBER(15,2),
  QT_IDADE_MIN_DIA     NUMBER(15,2),
  QT_IDADE_MAX         NUMBER(15,2),
  QT_IDADE_MAX_MES     NUMBER(15,2),
  QT_IDADE_MAX_DIA     NUMBER(15,2),
  QT_DOSE_MINIMA       NUMBER(15,3),
  QT_DOSE_MAXIMA       NUMBER(15,3),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  IE_DOSE_LIMITE       VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  IE_JUSTIFICATIVA     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FALIMTE_FAMEFAA_FK_I ON TASY.FA_REGRA_LIMITE_TERAP
(NR_SEQ_MEDIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FALIMTE_FAMEFAA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FALIMTE_INTPRES_FK_I ON TASY.FA_REGRA_LIMITE_TERAP
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FALIMTE_INTPRES_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FALIMTE_PK ON TASY.FA_REGRA_LIMITE_TERAP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FALIMTE_PK
  MONITORING USAGE;


CREATE INDEX TASY.FALIMTE_UNIMEDI_FK_I ON TASY.FA_REGRA_LIMITE_TERAP
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FALIMTE_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.FA_REGRA_LIMITE_TERAP ADD (
  CONSTRAINT FALIMTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FA_REGRA_LIMITE_TERAP ADD (
  CONSTRAINT FALIMTE_FAMEFAA_FK 
 FOREIGN KEY (NR_SEQ_MEDIC) 
 REFERENCES TASY.FA_MEDIC_FARMACIA_AMB (NR_SEQUENCIA),
  CONSTRAINT FALIMTE_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT FALIMTE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.FA_REGRA_LIMITE_TERAP TO NIVEL_1;

