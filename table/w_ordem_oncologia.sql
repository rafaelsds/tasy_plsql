ALTER TABLE TASY.W_ORDEM_ONCOLOGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ORDEM_ONCOLOGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ORDEM_ONCOLOGIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_ITEM         VARCHAR2(5 BYTE)         NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM_ADEP    NUMBER(10)               NOT NULL,
  DS_ITEM              VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WORDONC_PK ON TASY.W_ORDEM_ONCOLOGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WORDONC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.w_ordem_oncologia_befupd
BEFORE UPDATE ON TASY.W_ORDEM_ONCOLOGIA FOR EACH ROW
DECLARE

begin
if	(:new.nr_seq_ordem_adep is null) then
	:new.nr_seq_ordem_adep	:= 0;
end if;
END;
/


ALTER TABLE TASY.W_ORDEM_ONCOLOGIA ADD (
  CONSTRAINT WORDONC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ORDEM_ONCOLOGIA TO NIVEL_1;

