ALTER TABLE TASY.W_TEST_PLAN_PENDING_URS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_TEST_PLAN_PENDING_URS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TEST_PLAN_PENDING_URS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CUSTOMER      NUMBER(10),
  NR_SEQ_INTENCAO_USO  NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WTPPENDURS_PK ON TASY.W_TEST_PLAN_PENDING_URS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WTPPENDURS_REGCR_FK_I ON TASY.W_TEST_PLAN_PENDING_URS
(NR_SEQ_CUSTOMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WTPPENDURS_REGINU_FK_I ON TASY.W_TEST_PLAN_PENDING_URS
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_TEST_PLAN_PENDING_URS ADD (
  CONSTRAINT WTPPENDURS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.W_TEST_PLAN_PENDING_URS ADD (
  CONSTRAINT WTPPENDURS_REGCR_FK 
 FOREIGN KEY (NR_SEQ_CUSTOMER) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT WTPPENDURS_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_TEST_PLAN_PENDING_URS TO NIVEL_1;

