ALTER TABLE TASY.SIB_DEV_ERRO_CONTEUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIB_DEV_ERRO_CONTEUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIB_DEV_ERRO_CONTEUDO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_DEVOLUCAO_SIB  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CAMPO_ERRO         VARCHAR2(255 BYTE),
  DS_ERRO               VARCHAR2(255 BYTE),
  VL_CAMPO_ERRO         VARCHAR2(255 BYTE),
  CD_ERRO               VARCHAR2(255 BYTE),
  DS_MENSAGEM_ERRO      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SIBDECO_PK ON TASY.SIB_DEV_ERRO_CONTEUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIBDECO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SIBDECO_SIBDEER_FK_I ON TASY.SIB_DEV_ERRO_CONTEUDO
(NR_SEQ_DEVOLUCAO_SIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SIB_DEV_ERRO_CONTEUDO ADD (
  CONSTRAINT SIBDECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIB_DEV_ERRO_CONTEUDO ADD (
  CONSTRAINT SIBDECO_SIBDEER_FK 
 FOREIGN KEY (NR_SEQ_DEVOLUCAO_SIB) 
 REFERENCES TASY.SIB_DEVOLUCAO_ERRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIB_DEV_ERRO_CONTEUDO TO NIVEL_1;

