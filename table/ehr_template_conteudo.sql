ALTER TABLE TASY.EHR_TEMPLATE_CONTEUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_TEMPLATE_CONTEUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_TEMPLATE_CONTEUDO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_TEMPLATE           NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_ELEMENTO           NUMBER(10),
  NR_SEQ_ELEM_VISUAL        NUMBER(10),
  NR_SEQ_APRES              NUMBER(15)          NOT NULL,
  QT_TAMANHO                NUMBER(10),
  DS_ELEMENTO               VARCHAR2(4000 BYTE),
  DS_LABEL                  VARCHAR2(130 BYTE),
  QT_TAM_GRID               NUMBER(5),
  QT_ALTURA                 NUMBER(4),
  IE_OPCIONAL               VARCHAR2(1 BYTE),
  IE_VAZIO                  VARCHAR2(1 BYTE),
  DS_LABEL_GRID             VARCHAR2(50 BYTE),
  DS_MASCARA                VARCHAR2(20 BYTE),
  VL_MINIMO                 NUMBER(15,4),
  VL_MAXIMO                 NUMBER(15,4),
  NR_SEQ_GRID               NUMBER(3),
  QT_DESLOC_DIR             NUMBER(3),
  IE_OBRIGATORIO            VARCHAR2(1 BYTE)    NOT NULL,
  IE_READONLY               VARCHAR2(1 BYTE)    NOT NULL,
  IE_TABSTOP                VARCHAR2(1 BYTE)    NOT NULL,
  VL_PADRAO                 VARCHAR2(4000 BYTE),
  DS_SQL                    VARCHAR2(4000 BYTE),
  IE_COPIA                  VARCHAR2(1 BYTE),
  IE_ENTIDADE               VARCHAR2(15 BYTE),
  NR_SEQ_TEMPLATE_CLUSTER   NUMBER(10),
  NR_SEQ_ELEM_SUP           NUMBER(10),
  QT_COLUNA                 NUMBER(3),
  IE_ALINHAMENTO            VARCHAR2(1 BYTE),
  IE_WORDWRAP               VARCHAR2(1 BYTE),
  DS_FORMULA                VARCHAR2(2000 BYTE),
  IE_ITALICO                VARCHAR2(1 BYTE),
  IE_NEGRITO                VARCHAR2(1 BYTE),
  IE_SUBLINHADO             VARCHAR2(1 BYTE),
  QT_FONTE                  NUMBER(2),
  DS_COR                    VARCHAR2(15 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  IE_MACRO                  VARCHAR2(30 BYTE),
  NR_SEQ_TEXTO_PADRAO       NUMBER(10),
  QT_POS_ESQ                NUMBER(5),
  NR_SEQ_SINAL_VITAL        NUMBER(10),
  QT_CARACTER               NUMBER(4),
  DS_FUNCTION_GRID          VARCHAR2(255 BYTE),
  QT_POS_TOPO               NUMBER(5),
  IE_BLOQUEIA_AVISO         VARCHAR2(1 BYTE),
  DS_MSG_AVISO              VARCHAR2(255 BYTE),
  IE_MACRO_CONSISTENCIA     VARCHAR2(15 BYTE),
  DT_INATIVACAO             DATE,
  IE_EVOLUCAO               VARCHAR2(1 BYTE),
  DS_PROPRIEDADE            VARCHAR2(100 BYTE),
  IE_NOVA_LINHA             VARCHAR2(1 BYTE),
  IE_SENSIVEL               VARCHAR2(10 BYTE),
  IE_INFO_PACIENTE          VARCHAR2(10 BYTE),
  IE_COLLAPSED              VARCHAR2(1 BYTE),
  IE_CHECKBOX_CLUSTER       VARCHAR2(1 BYTE)    DEFAULT null,
  NR_SEQ_LINKED_DATA        NUMBER(10),
  NR_SEQ_CAD_HIST_SAUDE     NUMBER(10),
  NM_TIME_ZONE_ATTRIBUTE    VARCHAR2(50 BYTE),
  NR_SEQ_TEMP_CONTEUDO_SUP  NUMBER(10),
  NR_SEQ_LINKED_DATA_SUP    NUMBER(10),
  QT_MAX_IMAGEM             NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHTTEIN_EHRELEM_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_EHRELVI_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_ELEM_VISUAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_EHRTEMP_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_TEMPLATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_EHRTEMP_FK2_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_TEMPLATE_CLUSTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_EHRTEPA_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_TEXTO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_EHTTEIN_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_TEMP_CONTEUDO_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTTEIN_LKDT_FK_I ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQ_LINKED_DATA_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHTTEIN_PK ON TASY.EHR_TEMPLATE_CONTEUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ehr_template_conteudo_update
before update ON TASY.EHR_TEMPLATE_CONTEUDO for each row
declare

begin

if	(:new.nr_seq_elemento <> :old.nr_seq_elemento) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(228929);
elsif (:new.nr_seq_linked_data <> :old.nr_seq_linked_data and (:old.nr_seq_linked_data is not null)) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(1131452);
end	if;

if	(:new.ie_situacao = 'I') then
	:new.dt_inativacao := sysdate;
elsif	(:new.ie_situacao = 'A') then
	:new.dt_inativacao := null;
end	if;

end;
/


CREATE OR REPLACE TRIGGER TASY.EHR_TEMPLATE_CONTEUDO_tp  after update ON TASY.EHR_TEMPLATE_CONTEUDO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_ELEM_VISUAL,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ELEM_VISUAL,1,4000),substr(:new.NR_SEQ_ELEM_VISUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ELEM_VISUAL',ie_log_w,ds_w,'EHR_TEMPLATE_CONTEUDO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ELEMENTO,1,4000),substr(:new.NR_SEQ_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ELEMENTO',ie_log_w,ds_w,'EHR_TEMPLATE_CONTEUDO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'EHR_TEMPLATE_CONTEUDO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LABEL,1,4000),substr(:new.DS_LABEL,1,4000),:new.nm_usuario,nr_seq_w,'DS_LABEL',ie_log_w,ds_w,'EHR_TEMPLATE_CONTEUDO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'EHR_TEMPLATE_CONTEUDO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.EHR_TEMPLATE_CONTEUDO_ATUAL
before insert or update ON TASY.EHR_TEMPLATE_CONTEUDO for each row
declare
NR_SEQ_TIPO_DADO_w	number(10);
qtd_w number(10);

begin

if	(:new.DS_MASCARA is not null) and
	(:new.NR_SEQ_ELEMENTO is not null) and
	(instr(:new.DS_MASCARA,'#')	> 0)	then
	begin

	select	max(NR_SEQ_TIPO_DADO)
	into	NR_SEQ_TIPO_DADO_w
	from	ehr_elemento
	where	nr_sequencia	= :new.NR_SEQ_ELEMENTO;


	if	(NR_SEQ_TIPO_DADO_w in (4,5)) then
		begin
		wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(270209);
		end;
	end if;

	end;
end if;

  if (((inserting) and
	(:new.nr_seq_elemento = 1613 and :new.nr_seq_linked_data is not null)) or
      ((updating) and
	 (:new.nr_seq_elemento = 1613 and :new.nr_seq_linked_data is not null and (:new.nr_seq_linked_data <> :old.nr_seq_linked_data))))	then

    select  count(1)
    into    qtd_w
    from    ehr_template_conteudo
    where   nr_sequencia <> :new.nr_sequencia
	and		nr_seq_template = :new.nr_seq_template and nr_seq_linked_data = :new.nr_seq_linked_data;

    if (qtd_w > 0) then
      wheb_mensagem_pck.exibir_mensagem_abort(1131742);
    end if;

    IF inserting THEN
       ehr_alterar_tabela_linked(:new.nr_seq_template, :new.nr_seq_linked_data, 'CREATE');
    END IF;

  end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ehr_template_conteudo_delete
before delete ON TASY.EHR_TEMPLATE_CONTEUDO for each row
declare
NR_LINKED_DATA_ELEMENT_C CONSTANT EHR_ELEMENTO.NR_SEQUENCIA%TYPE DEFAULT 1613;

dt_liberacao_w	date;

begin

select	max(dt_liberacao)
into	dt_liberacao_w
from	ehr_template
where	nr_sequencia = :old.nr_seq_template;

if	(dt_liberacao_w is not null) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(253037);
end	if;

  IF :old.nr_seq_elemento = NR_LINKED_DATA_ELEMENT_C AND :old.nr_seq_linked_data IS NOT NULL THEN
    ehr_alterar_tabela_linked(:old.nr_seq_template, :old.nr_seq_linked_data, 'DROP');
  END IF;

end;
/


ALTER TABLE TASY.EHR_TEMPLATE_CONTEUDO ADD (
  CONSTRAINT EHTTEIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_TEMPLATE_CONTEUDO ADD (
  CONSTRAINT EHTTEIN_EHTTEIN_FK 
 FOREIGN KEY (NR_SEQ_TEMP_CONTEUDO_SUP) 
 REFERENCES TASY.EHR_TEMPLATE_CONTEUDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHTTEIN_LKDT_FK 
 FOREIGN KEY (NR_SEQ_LINKED_DATA_SUP) 
 REFERENCES TASY.LINKED_DATA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHTTEIN_EHRELEM_FK 
 FOREIGN KEY (NR_SEQ_ELEMENTO) 
 REFERENCES TASY.EHR_ELEMENTO (NR_SEQUENCIA),
  CONSTRAINT EHTTEIN_EHRELVI_FK 
 FOREIGN KEY (NR_SEQ_ELEM_VISUAL) 
 REFERENCES TASY.EHR_ELEMENTO_VISUAL (NR_SEQUENCIA),
  CONSTRAINT EHTTEIN_EHRTEPA_FK 
 FOREIGN KEY (NR_SEQ_TEXTO_PADRAO) 
 REFERENCES TASY.EHR_TEXTO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT EHTTEIN_EHRTEMP_FK 
 FOREIGN KEY (NR_SEQ_TEMPLATE) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHTTEIN_EHRTEMP_FK2 
 FOREIGN KEY (NR_SEQ_TEMPLATE_CLUSTER) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA));

GRANT SELECT ON TASY.EHR_TEMPLATE_CONTEUDO TO NIVEL_1;

