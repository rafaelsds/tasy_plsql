ALTER TABLE TASY.PLS_FATURA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FATURA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FATURA_PROC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_FATURA_CONTA      NUMBER(10)           NOT NULL,
  NR_SEQ_CONTA_PROC        NUMBER(10),
  VL_FATURADO              NUMBER(15,2)         NOT NULL,
  CD_CONTA_DEBITO          VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO         VARCHAR2(20 BYTE),
  NR_LOTE_CONTABIL         NUMBER(10)           NOT NULL,
  CD_HISTORICO             NUMBER(10),
  NR_SEQ_ESQUEMA           NUMBER(10),
  CD_CLASSIF_CRED          VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB           VARCHAR2(40 BYTE),
  CD_HISTORICO_BAIXA       NUMBER(10),
  CD_HISTORICO_ESTORNO     NUMBER(10),
  IE_TIPO_COBRANCA         VARCHAR2(3 BYTE),
  IE_LIBERADO              VARCHAR2(1 BYTE),
  NR_SEQ_CONTA_POS_ESTAB   NUMBER(10),
  NR_SEQ_FAT_PROC_CANCEL   NUMBER(10),
  VL_FATURADO_NDC          NUMBER(15,2),
  CD_CONTA_DEBITO_NDC      VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO_NDC     VARCHAR2(20 BYTE),
  CD_CLASSIF_CRED_NDC      VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB_NDC       VARCHAR2(40 BYTE),
  NR_SEQ_ESQUEMA_NDC       NUMBER(10),
  CD_HISTORICO_NDC         NUMBER(10),
  CD_HISTORICO_DIF         NUMBER(10),
  NR_SEQ_ESQUEMA_DIF       NUMBER(10),
  CD_CLASSIF_DEB_DIF       VARCHAR2(40 BYTE),
  CD_CLASSIF_CRED_DIF      VARCHAR2(40 BYTE),
  CD_CONTA_DEBITO_DIF      VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO_DIF     VARCHAR2(20 BYTE),
  NR_SEQ_CONTA_POS_CONTAB  NUMBER(10),
  VL_CUSTO_OPERACIONAL     NUMBER(15,2),
  VL_MATERIAIS             NUMBER(15,2),
  VL_MEDICO                NUMBER(15,2),
  VL_LIB_TAXA_CO           NUMBER(15,2),
  VL_LIB_TAXA_MATERIAL     NUMBER(15,2),
  VL_LIB_TAXA_SERVICO      NUMBER(15,2),
  NR_SEQ_POS_ESTAB_TAXA    NUMBER(10),
  NR_SEQ_POS_TAXA_CONTAB   NUMBER(10),
  NR_SEQ_POS_PROC          NUMBER(10),
  NR_SEQ_POS_PROC_FAT      NUMBER(10),
  NR_SEQ_POS_PROC_TX       NUMBER(10),
  NR_SEQ_POS_PROC_TX_FAT   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSFATP_CONCONT_FK_I ON TASY.PLS_FATURA_PROC
(CD_CONTA_DEBITO_DIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_CONCONT_FK2_I ON TASY.PLS_FATURA_PROC
(CD_CONTA_CREDITO_DIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_HISPADR_FK_I ON TASY.PLS_FATURA_PROC
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_HISPADR_FK2_I ON TASY.PLS_FATURA_PROC
(CD_HISTORICO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_HISPADR_FK3_I ON TASY.PLS_FATURA_PROC
(CD_HISTORICO_ESTORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_HISPADR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_HISPADR_FK4_I ON TASY.PLS_FATURA_PROC
(CD_HISTORICO_NDC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_HISPADR_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_HISPADR_FK5_I ON TASY.PLS_FATURA_PROC
(CD_HISTORICO_DIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_I1 ON TASY.PLS_FATURA_PROC
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_LOTCONT_FK_I ON TASY.PLS_FATURA_PROC
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSFATP_PK ON TASY.PLS_FATURA_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_PLSCOVB_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_CONTA_POS_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCPEC_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_CONTA_POS_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCPET_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_ESTAB_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCPTC_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_TAXA_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCPTF_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_PROC_TX_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCTAPF_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_PROC_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSCTAPTX_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_PROC_TX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSESCO_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_PLSESCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_PLSESCO_FK2_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_ESQUEMA_NDC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_PLSESCO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_PLSESCO_FK3_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_ESQUEMA_DIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATP_PLSFACO_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_FATURA_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_PLSFACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_PLSFATP_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_FAT_PROC_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATP_PLSFATP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATP_PLSPPROC_FK_I ON TASY.PLS_FATURA_PROC
(NR_SEQ_POS_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_fatura_proc_ins
before insert ON TASY.PLS_FATURA_PROC for each row
declare

qt_registro_pos_w	number(10);
qt_registro_w		number(10);

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	select	count(1)
	into	qt_registro_pos_w
	from	pls_conta_pos_estabelecido a
	where	nr_sequencia = :new.nr_seq_conta_pos_estab
	and	nr_seq_lote_fat is null
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido x
				where	x.nr_seq_conta = a.nr_seq_conta
				and	x.nr_sequencia <> a.nr_sequencia
				and	nr_seq_lote_fat is not null);

	select	count(1)
	into	qt_registro_w
	from	pls_fatura_conta c,
		pls_fatura_evento z,
		pls_fatura s
	where	s.nr_sequencia	= z.nr_seq_fatura
	and	z.nr_sequencia	= c.nr_seq_fatura_evento
	and	c.nr_sequencia	= :new.nr_seq_fatura_conta
	and	nvl(s.ie_cancelamento,'X') in ('C','E');

	if	(qt_registro_pos_w > 0) and
		(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(264165,'NR_SEQ_CONTA_PROC=' || :new.nr_seq_conta_proc);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_fatura_proc_insert
before insert or update ON TASY.PLS_FATURA_PROC for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

nr_seq_conta_w		pls_conta.nr_sequencia%type;
ie_cancelamento_w	pls_fatura.ie_cancelamento%type;
nr_seq_lote_disc_w	pls_conta_pos_estabelecido.nr_seq_lote_disc%type;
nr_seq_lote_fat_w	pls_lote_faturamento.nr_sequencia%type;

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	((nvl(:new.vl_faturado_ndc,0) < 0) and (nvl(:new.vl_faturado_ndc,0) <> nvl(:old.vl_faturado_ndc,0))) or
		((nvl(:new.vl_faturado,0) < 0) and (nvl(:new.vl_faturado,0) <> nvl(:old.vl_faturado,0))) then
		select	max(a.ie_cancelamento)
		into	ie_cancelamento_w
		from	pls_fatura_conta	c,
			pls_fatura_evento	b,
			pls_fatura		a
		where	a.nr_sequencia	= b.nr_seq_fatura
		and	b.nr_sequencia	= c.nr_seq_fatura_evento
		and	c.nr_sequencia	= :new.nr_seq_fatura_conta;

		if	(ie_cancelamento_w is null) and
			(nvl(:new.nr_lote_contabil,0) = 0) then
			select	max(nr_seq_conta)
			into	nr_seq_conta_w
			from	pls_conta_proc
			where	nr_sequencia	= :new.nr_seq_conta_proc;

			wheb_mensagem_pck.exibir_mensagem_abort( 281537,	'NR_SEQ_CONTA=' || nr_seq_conta_w || ';' ||
										'NR_SEQ_CONTA_POS=' || :new.nr_seq_conta_pos_estab);
		end if;
	end if;

	select	max(nr_seq_lote_disc)
	into	nr_seq_lote_disc_w
	from	pls_conta_pos_estabelecido
	where	nr_sequencia	= :new.nr_seq_conta_pos_estab;

	if	(nr_seq_lote_disc_w is null) then
		select	max(nr_seq_lote_disc)
		into	nr_seq_lote_disc_w
		from	pls_conta_pos_proc
		where	nr_sequencia	= :new.nr_seq_pos_proc;
	end if;

	if	(nr_seq_lote_disc_w is null) then
		select	max(x.nr_sequencia)
		into	nr_seq_lote_fat_w
		from	pls_fatura_conta	c,
			pls_fatura_evento	b,
			pls_lote_faturamento	x,
			pls_fatura		a
		where	a.nr_sequencia	= b.nr_seq_fatura
		and	b.nr_sequencia	= c.nr_seq_fatura_evento
		and	x.nr_sequencia	= a.nr_seq_lote
		and	c.nr_sequencia	= :new.nr_seq_fatura_conta
		and	a.ie_cancelamento is null
		and	x.nr_seq_lote_disc is not null;

		if	(nr_seq_lote_fat_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort( 290979,	'NR_SEQ_LOTE_FAT=' || nr_seq_lote_fat_w || ';' ||
										'NR_SEQ_LOTE_DISC=' || nr_seq_lote_disc_w || ';' ||
										'NR_SEQ_POS_ESTAB=' || :new.nr_seq_conta_pos_estab );
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_FATURA_PROC ADD (
  CONSTRAINT PLSFATP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FATURA_PROC ADD (
  CONSTRAINT PLSFATP_PLSPPROC_FK 
 FOREIGN KEY (NR_SEQ_POS_PROC) 
 REFERENCES TASY.PLS_CONTA_POS_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCPTF_FK 
 FOREIGN KEY (NR_SEQ_POS_PROC_TX_FAT) 
 REFERENCES TASY.PLS_CONTA_POS_PROC_TX_FAT (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCTAPTX_FK 
 FOREIGN KEY (NR_SEQ_POS_PROC_TX) 
 REFERENCES TASY.PLS_CONTA_POS_PROC_TX (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSFACO_FK 
 FOREIGN KEY (NR_SEQ_FATURA_CONTA) 
 REFERENCES TASY.PLS_FATURA_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSFATP_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_BAIXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSFATP_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_ESTORNO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSFATP_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCOVB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS_ESTAB) 
 REFERENCES TASY.PLS_CONTA_POS_ESTABELECIDO (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSFATP_FK 
 FOREIGN KEY (NR_SEQ_FAT_PROC_CANCEL) 
 REFERENCES TASY.PLS_FATURA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_HISPADR_FK4 
 FOREIGN KEY (CD_HISTORICO_NDC) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSFATP_PLSESCO_FK2 
 FOREIGN KEY (NR_SEQ_ESQUEMA_NDC) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_DEBITO_DIF) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSFATP_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_CREDITO_DIF) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSFATP_HISPADR_FK5 
 FOREIGN KEY (CD_HISTORICO_DIF) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSFATP_PLSESCO_FK3 
 FOREIGN KEY (NR_SEQ_ESQUEMA_DIF) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSFATP_PLSCPEC_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS_CONTAB) 
 REFERENCES TASY.PLS_CONTA_POS_ESTAB_CONTAB (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCTAPF_FK 
 FOREIGN KEY (NR_SEQ_POS_PROC_FAT) 
 REFERENCES TASY.PLS_CONTA_POS_PROC_FAT (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCPET_FK 
 FOREIGN KEY (NR_SEQ_POS_ESTAB_TAXA) 
 REFERENCES TASY.PLS_CONTA_POS_ESTAB_TAXA (NR_SEQUENCIA),
  CONSTRAINT PLSFATP_PLSCPTC_FK 
 FOREIGN KEY (NR_SEQ_POS_TAXA_CONTAB) 
 REFERENCES TASY.PLS_CONTA_POS_TAXA_CONTAB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FATURA_PROC TO NIVEL_1;

