ALTER TABLE TASY.LOTE_AUDIT_HIST_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_AUDIT_HIST_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_AUDIT_HIST_GUIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_INTERNO_CONTA       NUMBER(10)             NOT NULL,
  CD_AUTORIZACAO         VARCHAR2(20 BYTE)      NOT NULL,
  NR_SEQ_LOTE_HIST       NUMBER(10)             NOT NULL,
  NR_SEQ_RETORNO         NUMBER(10),
  VL_SALDO_GUIA          NUMBER(15,2),
  IE_GUIA_SEM_SALDO      VARCHAR2(1 BYTE),
  DT_BAIXA_GLOSA         DATE,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  VL_SALDO_ORIGINAL      NUMBER(15,2),
  IE_GUIA_SEM_RESPOSTA   VARCHAR2(2 BYTE),
  NR_TITULO_GUIA         NUMBER(10),
  NR_NOTA_FISCAL_GUIA    VARCHAR2(255 BYTE),
  DT_MESANO_REF_GUIA     DATE,
  NR_ATEND_GUIA          NUMBER(10),
  NM_PACIENTE_GUIA       VARCHAR2(60 BYTE),
  VL_GUIA                NUMBER(15),
  CD_PESSOA_GUIA         VARCHAR2(10 BYTE),
  NR_SEQ_PROTOCOLO_GUIA  NUMBER(10),
  DT_INTEGRACAO          DATE,
  DT_RECURSO             DATE,
  DT_PAGAMENTO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOTAUDGUI_ATEND_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_ATEND_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_CODPESSOA_I ON TASY.LOTE_AUDIT_HIST_GUIA
(CD_PESSOA_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_CONPACI_FK_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_CONPAGU_FK_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_INTERNO_CONTA, CD_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_CONRETO_FK_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTAUDGUI_CONRETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOTAUDGUI_LOTAUHI_FK_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_SEQ_LOTE_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_NMPACIENTE_I ON TASY.LOTE_AUDIT_HIST_GUIA
(UPPER("NM_PACIENTE_GUIA"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_NOTAF_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_NOTA_FISCAL_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOTAUDGUI_PK ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTAUDGUI_PK
  MONITORING USAGE;


CREATE INDEX TASY.LOTAUDGUI_PROTOC_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_SEQ_PROTOCOLO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_REFCONTA_I ON TASY.LOTE_AUDIT_HIST_GUIA
(DT_MESANO_REF_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTAUDGUI_TITULO_I ON TASY.LOTE_AUDIT_HIST_GUIA
(NR_TITULO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOTE_AUDIT_HIST_GUIA_DELETE
BEFORE DELETE ON TASY.LOTE_AUDIT_HIST_GUIA FOR EACH ROW
DECLARE

begin

update	lote_audit_hist_imp
set	nr_seq_guia	= null
where	nr_seq_guia	= :old.nr_sequencia;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_AUDIT_HIST_GUIA_INSERT
BEFORE INSERT ON TASY.LOTE_AUDIT_HIST_GUIA FOR EACH ROW
DECLARE

cd_estabelecimento_w		number(4);
cd_convenio_w			lote_auditoria.cd_convenio%type;
nr_seq_retorno_w		number(10);
ie_status_retorno_w		varchar2(1);
ie_sem_retorno_w		varchar2(1);
ie_vinc_adiant_prot_pj_w	varchar2(1);
nr_adiantamento_w		adiantamento.nr_adiantamento%type;

BEGIN

select	max(b.cd_estabelecimento),
	max(b.cd_convenio)
into	cd_estabelecimento_w,
	cd_convenio_w
from	lote_auditoria b,
	lote_audit_hist a
where	a.nr_seq_lote_audit	= b.nr_sequencia
and	a.nr_sequencia		= :new.nr_seq_lote_hist;

obter_param_usuario(69,23,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_sem_retorno_w);

select 	nvl(x.ie_vinc_adiant_prot_pj,'N')
into	ie_vinc_adiant_prot_pj_w
from  	convenio_estabelecimento x
where 	x.cd_convenio = cd_convenio_w
and	x.cd_estabelecimento = cd_estabelecimento_w;

select	max(nr_adiantamento)
into	nr_adiantamento_w
from	conta_paciente_adiant
where	nr_interno_conta = :new.nr_interno_conta;

if 	(ie_vinc_adiant_prot_pj_w = 'S') and (nr_adiantamento_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1026496,'nr_adiantamento_w='||nr_adiantamento_w||';'||
							'nr_interno_conta_w='||:new.nr_interno_conta);
end if;

if	(ie_sem_retorno_w = 'N') then

	select	nvl(max(b.nr_sequencia),0),
		max(b.ie_status_retorno)
	into	nr_seq_retorno_w,
		ie_status_retorno_w
	from	convenio_retorno b,
		convenio_retorno_item a
	where	a.nr_seq_retorno	= b.nr_sequencia
	and	nvl(a.cd_autorizacao,'N?o Informada')	= nvl(:new.cd_autorizacao,'N?o Informada')
	and	a.nr_interno_conta	= :new.nr_interno_conta;

	if	(nvl(nr_seq_retorno_w,0) = 0) then
		--r.aise_application_error(-20011,'A guia ' || :new.cd_autorizacao || ' da conta ' || :new.nr_interno_conta || ' ainda n?o foi tratada na func?o Retorno Convenio! Parametro [23]');
		wheb_mensagem_pck.exibir_mensagem_abort(263407,	'cd_autorizacao_w='||:new.cd_autorizacao||';'||
								'nr_interno_conta_w='||:new.nr_interno_conta);
	elsif	(ie_status_retorno_w <> 'F' and :new.nr_seq_retorno <> nr_seq_retorno_w) then
		--r.aise_application_error(-20011,'A guia ' || :new.cd_autorizacao || ' da conta ' || :new.nr_interno_conta || ' ainda esta sendo tratada no retorno ' || nr_seq_retorno_w || '! Parametro [23]');
		wheb_mensagem_pck.exibir_mensagem_abort(263408,	'cd_autorizacao_w='||:new.cd_autorizacao||';'||
								'nr_interno_conta_w='||:new.nr_interno_conta||';'||
								'nr_seq_retorno_w='||nr_seq_retorno_w);
	end if;

end if;

END;
/


ALTER TABLE TASY.LOTE_AUDIT_HIST_GUIA ADD (
  CONSTRAINT LOTAUDGUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_AUDIT_HIST_GUIA ADD (
  CONSTRAINT LOTAUDGUI_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT LOTAUDGUI_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA),
  CONSTRAINT LOTAUDGUI_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT LOTAUDGUI_CONPAGU_FK 
 FOREIGN KEY (NR_INTERNO_CONTA, CD_AUTORIZACAO) 
 REFERENCES TASY.CONTA_PACIENTE_GUIA (NR_INTERNO_CONTA,CD_AUTORIZACAO));

GRANT SELECT ON TASY.LOTE_AUDIT_HIST_GUIA TO NIVEL_1;

