ALTER TABLE TASY.DASHBOARD_PMO_PROJ_GRAF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DASHBOARD_PMO_PROJ_GRAF CASCADE CONSTRAINTS;

CREATE TABLE TASY.DASHBOARD_PMO_PROJ_GRAF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  PR_PREV              NUMBER(15,5),
  QT_HORA_PREV         NUMBER(15,2)             NOT NULL,
  QT_HORA_REAL         NUMBER(15,2),
  PR_REAL              NUMBER(15,5),
  NR_SEQ_PROJ          NUMBER(10)               NOT NULL,
  DS_PROJETO           VARCHAR2(255 BYTE),
  DT_REFERENCIA        DATE,
  IE_TIPO_CRONOGRAMA   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DPPOGRA_PK ON TASY.DASHBOARD_PMO_PROJ_GRAF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DPPOGRA_PROPROJ_FK_I ON TASY.DASHBOARD_PMO_PROJ_GRAF
(NR_SEQ_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DASHBOARD_PMO_PROJ_GRAF ADD (
  CONSTRAINT DPPOGRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DASHBOARD_PMO_PROJ_GRAF ADD (
  CONSTRAINT DPPOGRA_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DASHBOARD_PMO_PROJ_GRAF TO NIVEL_1;

