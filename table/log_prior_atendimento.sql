ALTER TABLE TASY.LOG_PRIOR_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_PRIOR_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_PRIOR_ATENDIMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TRIAGEM        NUMBER(10),
  NR_SEQ_TRIAGEM_PRIOR  NUMBER(10),
  NR_ATENDIMENTO        NUMBER(10),
  NR_ATENDIMENTO_PRIOR  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOPRATE_ATEPACI_FK_I ON TASY.LOG_PRIOR_ATENDIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOPRATE_ATEPACI_FK2_I ON TASY.LOG_PRIOR_ATENDIMENTO
(NR_ATENDIMENTO_PRIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOPRATE_PK ON TASY.LOG_PRIOR_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOPRATE_PK
  MONITORING USAGE;


CREATE INDEX TASY.LOPRATE_TRICLRI_FK_I ON TASY.LOG_PRIOR_ATENDIMENTO
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOPRATE_TRICLRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOPRATE_TRICLRI_FK2_I ON TASY.LOG_PRIOR_ATENDIMENTO
(NR_SEQ_TRIAGEM_PRIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOPRATE_TRICLRI_FK2_I
  MONITORING USAGE;


ALTER TABLE TASY.LOG_PRIOR_ATENDIMENTO ADD (
  CONSTRAINT LOPRATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_PRIOR_ATENDIMENTO ADD (
  CONSTRAINT LOPRATE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT LOPRATE_ATEPACI_FK2 
 FOREIGN KEY (NR_ATENDIMENTO_PRIOR) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT LOPRATE_TRICLRI_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_CLASSIF_RISCO (NR_SEQUENCIA),
  CONSTRAINT LOPRATE_TRICLRI_FK2 
 FOREIGN KEY (NR_SEQ_TRIAGEM_PRIOR) 
 REFERENCES TASY.TRIAGEM_CLASSIF_RISCO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_PRIOR_ATENDIMENTO TO NIVEL_1;

