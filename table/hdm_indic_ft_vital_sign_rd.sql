ALTER TABLE TASY.HDM_INDIC_FT_VITAL_SIGN_RD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HDM_INDIC_FT_VITAL_SIGN_RD CASCADE CONSTRAINTS;

CREATE TABLE TASY.HDM_INDIC_FT_VITAL_SIGN_RD
(
  NR_SEQ_FACT       NUMBER(10)                  NOT NULL,
  NR_SEQ_DIMENSION  NUMBER(10)                  NOT NULL,
  QT_MEASUREMENT    NUMBER(15,5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDMIVSR_HDMIDRD_FK_I ON TASY.HDM_INDIC_FT_VITAL_SIGN_RD
(NR_SEQ_DIMENSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDMIVSR_HDMIFTVS_FK_I ON TASY.HDM_INDIC_FT_VITAL_SIGN_RD
(NR_SEQ_FACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE UNIQUE INDEX TASY.HDMIVSR_PK ON TASY.HDM_INDIC_FT_VITAL_SIGN_RD
(NR_SEQ_FACT, NR_SEQ_DIMENSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HDM_INDIC_FT_VITAL_SIGN_RD ADD (
  CONSTRAINT HDMIVSR_PK
 PRIMARY KEY
 (NR_SEQ_FACT, NR_SEQ_DIMENSION)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HDM_INDIC_FT_VITAL_SIGN_RD ADD (
  CONSTRAINT HDMIVSR_HDMIDRD_FK 
 FOREIGN KEY (NR_SEQ_DIMENSION) 
 REFERENCES TASY.HDM_INDIC_DM_RISK_DISEASE (NR_SEQUENCIA),
  CONSTRAINT HDMIVSR_HDMIFTVS_FK 
 FOREIGN KEY (NR_SEQ_FACT) 
 REFERENCES TASY.HDM_INDIC_FT_VITAL_SIGN (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HDM_INDIC_FT_VITAL_SIGN_RD TO NIVEL_1;

