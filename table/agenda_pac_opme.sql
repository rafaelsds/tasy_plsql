ALTER TABLE TASY.AGENDA_PAC_OPME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_OPME CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_OPME
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_AGENDA           NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_MATERIAL             NUMBER(10)            NOT NULL,
  QT_MATERIAL             NUMBER(15,3)          NOT NULL,
  NR_SEQ_APRES            NUMBER(15)            NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_ORIGEM_INF           VARCHAR2(1 BYTE)      NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_AUTORIZADO           VARCHAR2(3 BYTE),
  IE_PADRAO               VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_MOTIVO_EXCLUSAO  NUMBER(10),
  NM_USUARIO_EXCLUSAO     VARCHAR2(15 BYTE),
  DT_EXCLUSAO             DATE,
  CD_CGC                  VARCHAR2(14 BYTE),
  NR_SEQ_PROC_INTERNO     NUMBER(10),
  IE_INTEGRACAO           VARCHAR2(15 BYTE),
  VL_UNITARIO_ITEM        NUMBER(13,4),
  VL_DESCONTO             NUMBER(13,2),
  CD_COND_PAGTO           NUMBER(10),
  IE_INTEGRACAO_UTIL      VARCHAR2(15 BYTE),
  IE_GERAR_AUTOR          VARCHAR2(1 BYTE),
  NR_SEQ_PRESCRICAO       NUMBER(6),
  NR_SEQ_AGENDA_INT       NUMBER(10),
  NR_SEQ_ITEM             NUMBER(10),
  NR_SEQ_AGENDA_CONS      NUMBER(10),
  NR_SEQ_MARCA            NUMBER(10),
  NR_SEQ_LOTE             NUMBER(10),
  NR_ORCAMENTO            VARCHAR2(20 BYTE),
  NR_SEQ_AGRUP            NUMBER(10),
  NR_SEQ_MAP              NUMBER(10),
  CD_MATERIAL_CONVENIO    VARCHAR2(70 BYTE),
  VL_UNITARIO_ATUALIZADO  NUMBER(13,4),
  DT_SOLIC_ENVIO          DATE,
  NM_USUARIO_SOLIC_ENVIO  VARCHAR2(15 BYTE),
  DT_LIBERACAO_ENVIO      DATE,
  NM_USUARIO_LIB_ENVIO    VARCHAR2(15 BYTE),
  NR_PRESCRICAO           NUMBER(10),
  IE_CONTRATO             VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_PAGTO      NUMBER(10),
  IE_EXCLUSIVO            VARCHAR2(1 BYTE),
  VL_CUSTO                NUMBER(13,4),
  IE_STATUS_COTACAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGPOPME_AGECONS_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_AGECONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPOPME_AGEINIT_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_AGEINIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPOPME_AGEINTE_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_AGENDA_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPOPME_AGEPACI_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPOPME_AGPAOPA_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPOPME_FORPAGA_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_FORMA_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPOPME_MATERIA_FK_I ON TASY.AGENDA_PAC_OPME
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPOPME_MATMARC_FK_I ON TASY.AGENDA_PAC_OPME
(CD_MATERIAL, NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_MATMARC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPOPME_MOEXOPA_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_MOTIVO_EXCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_MOEXOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPOPME_PESJURI_FK_I ON TASY.AGENDA_PAC_OPME
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGPOPME_PK ON TASY.AGENDA_PAC_OPME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPOPME_PROINTE_FK_I ON TASY.AGENDA_PAC_OPME
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPOPME_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_INSERT
AFTER INSERT ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE

nr_seq_evento_w			number(10);
qt_idade_w			number(10);
cd_convenio_w			number(15);
cd_medico_w			varchar2(15);
cd_pessoa_fisica_w		varchar2(15);
ie_sexo_w			varchar2(15);
nr_seq_proc_interno_w		number(10);
cd_estabelecimento_w		number(4);
cd_agenda_w			number(10);
hr_inicio_w			date;
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
dt_cancelamento_w		date;
cd_motivo_cancel_w		varchar2(15);
nr_atendimento_w		number(10,0);
ie_status_agenda_w		varchar2(3) := null;
nr_cirurgia_w			number(10);
ds_observacao_w			varchar2(255);

json_w			philips_json;
json_data_w		clob;

cursor c01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	ie_evento_disp			= 'IOPME'
	and	nvl(qt_idade_w,0) between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_sexo,nvl(ie_sexo_w,'XPTO'))  = nvl(ie_sexo_w,'XPTO')
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	(obter_se_proc_rec_alerta(nr_seq_proc_interno_w,nr_sequencia,cd_procedimento_w,ie_origem_proced_w) = 'S')
	and	(obter_se_mat_rec_alerta(:new.cd_material,nr_sequencia) = 'S')
	and	(obter_se_regra_envio(nr_sequencia,nr_atendimento_w) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and	nvl(ie_situacao,'A') = 'A';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') and
	(:new.nr_seq_agenda is not null) then
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	select	cd_pessoa_fisica,
		nvl(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),nvl(qt_idade_paciente,0)),
		Obter_Sexo_PF(cd_pessoa_fisica,'C'),
		cd_medico,
		cd_convenio,
		nr_seq_proc_interno,
		cd_agenda,
		hr_inicio,
		cd_procedimento,
		ie_origem_proced,
		dt_cancelamento,
		cd_motivo_cancelamento,
		nr_atendimento,
		ie_status_agenda,
		nr_cirurgia,
		substr(ds_observacao,1,255)
	into	cd_pessoa_fisica_w,
		qt_idade_w,
		ie_sexo_w,
		cd_medico_w,
		cd_convenio_w,
		nr_seq_proc_interno_w,
		cd_agenda_w,
		hr_inicio_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_cancelamento_w,
		cd_motivo_cancel_w,
		nr_atendimento_w,
		ie_status_agenda_w,
		nr_cirurgia_w,
		ds_observacao_w
	from	agenda_paciente
	where	nr_sequencia = :new.nr_seq_agenda;


	open c01;
	loop
	fetch c01 into
		nr_seq_evento_w;
	exit when c01%notfound;
		begin
		gerar_evento_agenda_trigger(nr_seq_evento_w,nr_atendimento_w,
									cd_pessoa_fisica_w,
									null,
									:new.nm_usuario,
									cd_agenda_w,
									hr_inicio_w,
									cd_medico_w,
									cd_procedimento_w,
									ie_origem_proced_w,
									dt_cancelamento_w,
									:new.cd_material,
									:new.qt_material,
									:new.ie_autorizado,
									cd_convenio_w,
									cd_motivo_cancel_w,
									'S',
									:new.nr_seq_agenda,
									null,
									'',
									'',
									null,
									substr(ds_observacao_w,1,255),
									'',
									null,
									nr_cirurgia_w,
									:new.cd_cgc);
		end;
	end loop;
	close c01;
end if;

json_w := philips_json();
json_w.put('id', :new.nr_sequencia);
json_w.put('surgicalSchedule', :new.nr_seq_agenda);
json_w.put('material', :new.cd_material);
json_w.put('paymentTerm', :new.cd_cond_pagto);
json_w.put('supplier', :new.cd_cgc);
json_w.put('brand', :new.nr_seq_marca);
json_w.put('materialAmount', :new.qt_material);
json_w.put('unitValue', :new.vl_unitario_item);
json_w.put('discountValue', :new.vl_desconto);
json_w.put('itemStatusSchedule', :new.ie_origem_inf);
json_w.put('standardItem', :new.ie_padrao);
json_w.put('authorizationStatus', :new.ie_autorizado);
json_w.put('integration', :new.ie_integracao);
json_w.put('generateInsuranceAuthorization', :new.ie_gerar_autor);
json_w.put('descriptionNotes', :new.ds_observacao);
json_w.put('lastUpdate', :new.dt_atualizacao);

dbms_lob.createtemporary(json_data_w, true);
json_w.to_clob(json_data_w);

json_data_w := bifrost.send_integration_content('surgicalSchedule.add.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_DELETE
BEFORE DELETE ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE


nr_seq_autorizacao_w		number(15,0);
cd_material_w			number(10);
cd_kit_material_w		number(6);
nr_seq_evento_w			number(10);
qt_idade_w			number(10);
cd_convenio_w			number(15);
cd_medico_w			varchar2(15);
cd_pessoa_fisica_w		varchar2(15);
ie_sexo_w			varchar2(15);
nr_seq_proc_interno_w		number(10);
cd_estabelecimento_w		number(4);
cd_agenda_w		number(10);
hr_inicio_w		date;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
dt_cancelamento_w	date;
cd_motivo_cancel_w	varchar2(15);
qt_autor_conv_w		number(10,0) := 0;
qt_autor_cir_w		number(10,0) := 0;
ie_zerar_qt_opme_exc_w	varchar2(15) := 'E';
nr_atendimento_w	number(10,0);
ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);
ds_observacao_w		varchar2(255);
nm_usuario_atual_w	varchar2(15) := Wheb_Usuario_Pck.Get_Nm_Usuario;
nr_seq_autor_conv_w	number(10,0);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(773992, null, wheb_usuario_pck.get_nr_seq_idioma);--Item excluido na Agenda pelo usu�rio
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(327202, null, wheb_usuario_pck.get_nr_seq_idioma);--Data

cursor c01 is
select	a.nr_sequencia,
	a.nr_seq_autor_conv
from	autorizacao_cirurgia a
where	a.nr_seq_agenda	= :old.nr_seq_agenda
and	a.dt_liberacao	is null
and	nvl(ie_zerar_qt_opme_exc_w,'E') = 'E';

cursor	c02 is
select cd_kit_material
from   material_estab
where  cd_material = :new.cd_material;

cursor	c03 is
select	a.cd_material
from	componente_kit a
where	a.cd_kit_material	= cd_kit_material_w;

cursor c04 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	ie_evento_disp			= 'EOPME'
	and	nvl(qt_idade_w,0) between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_sexo,nvl(ie_sexo_w,'XPTO'))  = nvl(ie_sexo_w,'XPTO')
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	(obter_se_proc_rec_alerta(nr_seq_proc_interno_w,nr_sequencia,cd_procedimento_w,ie_origem_proced_w) = 'S')
	and	(obter_se_mat_rec_alerta(:old.cd_material,nr_sequencia) = 'S')
	and	(obter_se_regra_envio(nr_sequencia,nr_atendimento_w) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	nvl(ie_situacao,'A') = 'A';



pragma autonomous_transaction;
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	obter_param_usuario(871,618,obter_perfil_ativo,:old.nm_usuario,cd_estabelecimento_w,ie_zerar_qt_opme_exc_w);

	open c01;
	loop
	fetch c01 into
		nr_seq_autorizacao_w,
		nr_seq_autor_conv_w;
	exit when c01%notfound;
		begin

		delete
		from	material_autorizado  a
		where	a.cd_material	= :old.cd_material
		and	((a.nr_seq_opme	= :old.nr_sequencia) or (a.nr_seq_opme is null))
		and	((a.nr_sequencia_autor	= nr_seq_autor_conv_w) or
			(exists (select	1
				from	autorizacao_convenio x
				where	x.nr_sequencia = a.nr_sequencia_autor
				and	x.nr_seq_autor_cirurgia =  nr_seq_autorizacao_w)));



 		delete
		from	material_autor_cirurgia
		where	cd_material	= :old.cd_material
		and	nr_seq_autorizacao	= nr_seq_autorizacao_w;

		--Para Kit's e n�o material avulso
		open c02;
		loop
		fetch c02 into
			cd_kit_material_w;
		exit when c02%notfound;
			begin
			open c03;
			loop
			fetch c03 into
				cd_material_w;
			exit when c03%notfound;
				begin
				delete
				from	material_autor_cirurgia
				where	cd_material		= cd_material_w
				and	nr_seq_autorizacao	= nr_seq_autorizacao_w;
				end;
			end loop;
			close c03;
			end;
		end loop;
		close c02;

		update	autorizacao_cirurgia a
		set	a.ie_estagio_autor = '6',
			a.nm_usuario	 = nvl(nm_usuario_atual_w, nm_usuario),
			a.dt_atualizacao	 = sysdate
		where	a.nr_sequencia	 = nr_seq_autorizacao_w
		and	not exists(	select	1
					from	material_autor_cirurgia x
					where	x.nr_seq_autorizacao = a.nr_sequencia);


		update	autorizacao_convenio a
		set	a.nr_seq_estagio = (	select 	max(x.nr_sequencia)
						from	estagio_autorizacao x
						where	x.ie_interno = '70'),
			a.nm_usuario	 = nvl(nm_usuario_atual_w, nm_usuario),
			a.dt_atualizacao = sysdate
		where	a.nr_seq_agenda  = :old.nr_seq_agenda
		and	((a.nr_sequencia = nr_seq_autor_conv_w) or  (a.nr_seq_autor_cirurgia = nr_seq_autorizacao_w))
		and	not exists(	select	1
					from	material_autorizado x
					where	x.nr_sequencia_autor = a.nr_sequencia);


		end;
	end loop;
	close c01;


	begin
	select	cd_pessoa_fisica,
		nvl(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),nvl(qt_idade_paciente,0)),
		Obter_Sexo_PF(cd_pessoa_fisica,'C'),
		cd_medico,
		cd_convenio,
		nr_seq_proc_interno,
		cd_agenda,
		hr_inicio,
		cd_procedimento,
		ie_origem_proced,
		dt_cancelamento,
		cd_motivo_cancelamento,
		nr_atendimento,
		substr(ds_observacao,1,255)
	into	cd_pessoa_fisica_w,
		qt_idade_w,
		ie_sexo_w,
		cd_medico_w,
		cd_convenio_w,
		nr_seq_proc_interno_w,
		cd_agenda_w,
		hr_inicio_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_cancelamento_w,
		cd_motivo_cancel_w,
		nr_atendimento_w,
		ds_observacao_w
	from	agenda_paciente
	where	nr_sequencia = :old.nr_seq_agenda;
	exception
	when others then
		cd_pessoa_fisica_w := null;
		qt_idade_w := null;
		ie_sexo_w := null;
		cd_medico_w := null;
		cd_convenio_w := null;
		nr_seq_proc_interno_w := null;
		cd_agenda_w := null;
		hr_inicio_w := null;
		cd_procedimento_w := null;
		ie_origem_proced_w := null;
		dt_cancelamento_w := null;
		cd_motivo_cancel_w := null;
		ds_observacao_w	:= null;
	end;


	open c04;
	loop
	fetch c04 into
		nr_seq_evento_w;
	exit when c04%notfound;
		begin
		gerar_evento_agenda_trigger(	nr_seq_evento_w,
						null,
						cd_pessoa_fisica_w,
						null,
						:old.nm_usuario,
						cd_agenda_w,
						hr_inicio_w,
						cd_medico_w,
						cd_procedimento_w,
						ie_origem_proced_w,
						dt_cancelamento_w,
						:old.cd_material,
						:old.qt_material,
						:old.ie_autorizado,
						cd_convenio_w,
						cd_motivo_cancel_w,
						'S',
						null,
						null,
						null,
						null,
						null,
						ds_observacao_w,
						NULL,
						NULL,
						NULL,
						:old.cd_cgc);

		end;
	end loop;
	close c04;

	-- Ronaldo OS

	if	(nvl(ie_zerar_qt_opme_exc_w,'E') = 'A') then
		update	material_autor_cirurgia
		set	qt_solicitada	= 0,
			nm_usuario	= nvl(nm_usuario_atual_w, nm_usuario),
			dt_atualizacao	= sysdate,
			ds_observacao 	= expressao1_w || ' ' || nm_usuario_atual_w || ' ' || expressao2_w || ' ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
		where	nr_seq_opme	= :old.nr_sequencia;

		update	material_autorizado
		set	qt_solicitada	= 0,
			nm_usuario	= nvl(nm_usuario_atual_w, nm_usuario),
			dt_atualizacao	= sysdate,
			ds_observacao	= expressao1_w || ' ' || nm_usuario_atual_w || ' ' || expressao2_w || ' ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
		where	nr_seq_opme	= :old.nr_sequencia;

		select 	count(*)
		into	qt_autor_conv_w
		from	material_autorizado a,
			autorizacao_convenio b
		where	a.nr_sequencia_autor = b.nr_sequencia
		and	b.nr_seq_agenda = :old.nr_seq_agenda
		--and	b.ie_tipo_autorizacao	= '4'
		and	a.qt_solicitada	 > 0
		and	exists(	select	1
					from	material_autorizado x
					where	x.nr_sequencia_autor = b.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia);



		select	count(*)
		into	qt_autor_cir_w
		from	material_autor_cirurgia a,
			autorizacao_cirurgia b
		where	a.nr_seq_autorizacao = b.nr_sequencia
		and	b.nr_seq_agenda = :old.nr_seq_agenda
		and	a.qt_solicitada > 0
		and	exists(	select	1
				from	material_autor_cirurgia x
				where	x.nr_seq_autorizacao = b.nr_sequencia
				and	x.nr_seq_opme	     = :new.nr_sequencia);


		if	(qt_autor_cir_w = 0) then

			update	autorizacao_cirurgia a
			set	a.ie_estagio_autor = '6',
				a.nm_usuario	= nvl(nm_usuario_atual_w, nm_usuario),
				a.dt_atualizacao	= sysdate
			where	a.nr_seq_agenda = :old.nr_seq_agenda
			and	exists(	select	1
					from	material_autor_cirurgia x
					where	x.nr_seq_autorizacao = a.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia);

		end if;

		if	(qt_autor_conv_w = 0) then

			update	autorizacao_convenio a
			set	a.nr_seq_estagio = (	select 	max(x.nr_sequencia)
							from	estagio_autorizacao x
							where	x.ie_interno = '70'),
				a.nm_usuario	 = nvl(nm_usuario_atual_w, nm_usuario),
				a.dt_atualizacao = sysdate
			where	a.nr_seq_agenda  = :old.nr_seq_agenda
			and	exists(	select	1
					from	material_autorizado x
					where	x.nr_sequencia_autor = a.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia);

		end if;

	end if;
	-- Fim OS

	begin

	obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);

	select  max(a.ie_status_agenda),
		max(b.cd_tipo_agenda)
	into	ie_status_agenda_w,
		cd_tipo_agenda_w
	from    agenda_paciente a,
		agenda b
	where   a.cd_agenda = b.cd_agenda
	and	nr_sequencia = :old.nr_seq_agenda;

	exception
		when others then
		null;
	end;

	if	((ie_permite_alt_executada_w = 'N') and
		(cd_tipo_agenda_w = 1) and
		(ie_status_agenda_w = 'E')) and
		(:old.ie_integracao <> 'S') then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

	end if;
/*

if	(nvl(:old.nr_seq_classificacao,0) > 0) then
	ds_altera��o_w	:=	substr('Exclus�o do CME '||substr(cme_obter_desc_classif_conj(:old.nr_seq_classificacao),1,125),1,4000);
end if;

if	(ds_altera��o_w is not null) then
	gravar_historico_montagem(:old.nr_seq_agenda,'EC',ds_altera��o_w,:old.nm_usuario);
end if;
*/
end if;

commit;

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_UPDATE
BEFORE UPDATE ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE

nr_seq_autorizacao_w		number(15,0);
atrib_oldvalue_w		varchar2(255);
atrib_newvalue_w		varchar2(255);
cd_material_w			number(10);
cd_kit_material_w		number(6);
nr_seq_evento_w			number(10);
qt_idade_w			number(10);
cd_convenio_w			number(15);
cd_medico_w			varchar2(15);
cd_pessoa_fisica_w		varchar2(15);
ie_sexo_w			varchar2(15);
nr_seq_proc_interno_w		number(10);
cd_estabelecimento_w		number(4);
ie_evento_w			varchar2(15);
cd_agenda_w			number(10);
hr_inicio_w			date;
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
dt_cancelamento_w		date;
cd_motivo_cancel_w		varchar2(15);
qt_material_w			number(10,0);
ds_observacao_w			varchar2(4000);
ds_observacao_ww		varchar2(255);
nr_atendimento_w		number(10,0);
nr_seq_mat_autor_w		number(10);
ie_regra_acresc_mat_autor_w	varchar2(1) := 'N';

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);
ie_zerar_qt_opme_exc_w		varchar2(2) := 'E';
qt_autor_conv_w			number(10,0) := 0;
qt_autor_cir_w			number(10,0) := 0;
nr_seq_autor_conv_w		number(10,0);
qt_mat_cirurgia			number(10);
nr_seq_mat_cirurgia_w		material_autor_cirurgia.nr_sequencia%type;

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(773980, null, wheb_usuario_pck.get_nr_seq_idioma);--Status da autorizacao de
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(727816, null, wheb_usuario_pck.get_nr_seq_idioma);--para
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(773985, null, wheb_usuario_pck.get_nr_seq_idioma);--Alterado o material
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(773986, null, wheb_usuario_pck.get_nr_seq_idioma);--Alterada a quantidade de
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(773987, null, wheb_usuario_pck.get_nr_seq_idioma);--Alterado a obervacao
expressao6_w	varchar2(255) := obter_desc_expressao_idioma(773992, null, wheb_usuario_pck.get_nr_seq_idioma);--Item excluido na Agenda pelo usuario
expressao7_w	varchar2(255) := obter_desc_expressao_idioma(327202, null, wheb_usuario_pck.get_nr_seq_idioma);--Data


cursor c01 is
select	a.nr_sequencia,
	a.nr_seq_autor_conv
from	autorizacao_cirurgia a
where	a.nr_seq_agenda	= :new.nr_seq_agenda
and	a.dt_liberacao	is null;

cursor	c02 is
select cd_kit_material
from   material_estab
where  cd_material = :new.cd_material;

cursor	c03 is
select	a.cd_material
from	componente_kit a
where	a.cd_kit_material	= cd_kit_material_w;

cursor c04 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	ie_evento_disp			= ie_evento_w
	and	nvl(qt_idade_w,0) between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_sexo,nvl(ie_sexo_w,'XPTO'))  = nvl(ie_sexo_w,'XPTO')
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	(obter_se_proc_rec_alerta(nr_seq_proc_interno_w,nr_sequencia,cd_procedimento_w,ie_origem_proced_w) = 'S')
	and	(obter_se_mat_rec_alerta(:new.cd_material,nr_sequencia) = 'S')
	and	(obter_se_regra_envio(nr_sequencia,nr_atendimento_w) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	nvl(ie_situacao,'A') = 'A';

cursor c05 is
select	a.nr_sequencia
from	autorizacao_convenio b,
	material_autorizado a
where	a.nr_sequencia_autor				= b.nr_sequencia
and	b.nr_seq_agenda					= :new.nr_seq_agenda
and	a.nr_seq_opme					= :new.nr_sequencia
and	OBTER_ESTAGIO_AUTOR(b.nr_seq_estagio,'C') 	not in ('10','70','90'); --Autorizado, Cancelado , Reprovado;


BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	obter_param_usuario(871,618,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_zerar_qt_opme_exc_w);

	if	(:old.ie_autorizado is not null) and
		(:new.ie_autorizado is not null) and
		(:old.ie_autorizado <> :new.ie_autorizado) then

		atrib_oldvalue_w := substr(obter_valor_dominio(1637,:old.ie_autorizado),1,255);
		atrib_newvalue_w := substr(obter_valor_dominio(1637,:new.ie_autorizado),1,255);

		gerar_opme_hist(:new.nr_sequencia,'1',wheb_usuario_pck.get_nm_usuario,expressao1_w || ' ' || atrib_oldvalue_w || ' ' || expressao2_w || ' ' || atrib_newvalue_w,sysdate);
	end if;

	if	(:old.cd_material is not null) and
		(:new.cd_material is not null) and
		(:old.cd_material <> :new.cd_material) then

		atrib_oldvalue_w := substr(obter_desc_material(:old.cd_material),1,255);
		atrib_newvalue_w := substr(obter_desc_material(:new.cd_material),1,255);

		gerar_opme_hist(:new.nr_sequencia,'2',wheb_usuario_pck.get_nm_usuario,expressao3_w || ' ' || atrib_oldvalue_w || ' ' || expressao2_w || ' ' || atrib_newvalue_w,sysdate);
	end if;

	if	(:old.qt_material is not null) and
		(:new.qt_material is not null) and
		(:old.qt_material <> :new.qt_material) then

		atrib_oldvalue_w := :old.qt_material;
		atrib_newvalue_w := :new.qt_material;

		gerar_opme_hist(:new.nr_sequencia,'3',wheb_usuario_pck.get_nm_usuario,expressao4_w || ' ' || atrib_oldvalue_w || ' ' || expressao2_w || ' ' || atrib_newvalue_w,sysdate);
	end if;

	if	(:old.DS_OBSERVACAO is not null) and
		(:new.DS_OBSERVACAO is not null) and
		(:old.DS_OBSERVACAO <> :new.DS_OBSERVACAO) then

		atrib_oldvalue_w := substr(:old.DS_OBSERVACAO,1,255);
		atrib_newvalue_w := substr(:new.DS_OBSERVACAO,1,255);

		gerar_opme_hist(:new.nr_sequencia,'4',wheb_usuario_pck.get_nm_usuario,expressao5_w || ' ' || atrib_oldvalue_w || ' ' || expressao2_w || ' ' || atrib_newvalue_w,sysdate);
	end if;


	select	nvl(max(IE_REGRA_ACRESC_MAT_AUTOR),'N')
	into	ie_regra_acresc_mat_autor_w
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_w;


	open c01;
	loop
	fetch c01 into
		nr_seq_autorizacao_w,
		nr_seq_autor_conv_w;
	exit when c01%notfound;
		begin

		begin
		select	max(qt_material),
			max(substr(ds_observacao,1,3800))
		into	qt_material_w,
			ds_observacao_w
		from	material_autor_cirurgia
		where	((nr_seq_opme	= :new.nr_sequencia) or
			((nr_seq_opme is null) and (cd_material = :new.cd_material)))
		and	nr_seq_autorizacao 	= nr_seq_autorizacao_w;
		exception
		when others then
			qt_material_w := 0;
			ds_observacao_w := null;
		end;

		if	(qt_material_w <> :new.qt_material) then
			begin
			update	material_autor_cirurgia
			set	qt_solicitada		= :new.qt_material,
				dt_atualizacao		= sysdate,
				nm_usuario		= :new.nm_usuario
			where	((nr_seq_opme	= :new.nr_sequencia) or
				((nr_seq_opme is null) and (cd_material = :new.cd_material)))
			and	nr_seq_autorizacao	= nr_seq_autorizacao_w;
			end;
		end if;

		if	(ds_observacao_w <> :new.ds_observacao) then
			begin
			update	material_autor_cirurgia
			set	ds_observacao		= substr(:new.ds_observacao,1,3800),
				dt_atualizacao		= sysdate,
				nm_usuario		= :new.nm_usuario
			where	((nr_seq_opme	= :new.nr_sequencia) or
				((nr_seq_opme is null) and (cd_material = :new.cd_material)))
			and	nr_seq_autorizacao	= nr_seq_autorizacao_w;
			end;
		end if;

		if	(nvl(:old.vl_unitario_item,0) <> nvl(:new.vl_unitario_item,0)) then

			select	max(a.nr_sequencia)
			into	nr_seq_mat_cirurgia_w
			from	material_autor_cirurgia a
			where	((nr_seq_opme		= :new.nr_sequencia) or
				((nr_seq_opme is null) and (cd_material = :new.cd_material)))
			and	nr_seq_autorizacao	= nr_seq_autorizacao_w;

			if(nvl(nr_seq_mat_cirurgia_w,0) > 0) and (nvl(:new.cd_cgc,'0') <> '0') then

				update	material_autor_cir_cot
				set	dt_atualizacao		= sysdate,
					nm_usuario		= :new.nm_usuario,
					vl_cotado		= (:new.qt_material * :new.vl_unitario_item),
					vl_unitario_cotado	= :new.vl_unitario_item
				where	nr_sequencia		= nr_seq_mat_cirurgia_w
				and 	cd_cgc			= :new.cd_cgc
				and 	nvl(vl_unitario_cotado,0) <> nvl(:new.vl_unitario_item,0);

			end if;
		end if;

		end;
	end loop;
	close c01;

	open c01;
	loop
	fetch c01 into
		nr_seq_autorizacao_w,
		nr_seq_autor_conv_w;
	exit when c01%notfound;
		begin
		if	(:new.ie_origem_inf = 'E') then
			if	(nvl(ie_zerar_qt_opme_exc_w,'E') = 'E') then

			select 	nvl(count(*),0)
			into 	qt_mat_cirurgia
			from 	material_autor_cirurgia a
			where 	a.nr_seq_autorizacao	= nr_seq_autorizacao_w
			and 	a.cd_material 		= :new.cd_material;


			if (qt_mat_cirurgia = 1 ) then --Lancado 1 vez o Mat na Autor Mat Esp
				delete
				from 	material_autorizado a
				where 	a.nr_sequencia_autor		= nr_seq_autor_conv_w
				and 	a.cd_material 			= :new.cd_material
				and 	nvl(a.cd_cgc_fabricante,'') 	= nvl(:new.cd_cgc,'')
				and 	rownum = 1;--nr_seq_opme pode ser repetida na mat_autor, por isso exclui apenas uma linha conforme o fornecedor.

				delete
				from 	material_autor_cirurgia a
				where	((a.nr_seq_opme	= :new.nr_sequencia) or ((a.nr_seq_opme is null) and (a.cd_material = :new.cd_material)))
				and	a.nr_seq_autorizacao	= nr_seq_autorizacao_w
				and 	not exists (	select 	1
						from 	autorizacao_cirurgia b,
							material_autorizado c
						where    c.nr_sequencia_autor       = b.nr_seq_autor_conv
						and      b.nr_sequencia             = a.nr_seq_autorizacao
						and      c.cd_material              = a.cd_material);
			else
				delete
				from	material_autor_cirurgia
				where	((nr_seq_opme	= :new.nr_sequencia) or
					((nr_seq_opme is null) and (cd_material = :new.cd_material)))
				and	nr_seq_autorizacao	= nr_seq_autorizacao_w;
			end if;

				--Para Kit's e nao material avulso
				open c02;
				loop
				fetch c02 into
					cd_kit_material_w;
				exit when c02%notfound;
					begin
					open c03;
					loop
					fetch c03 into
						cd_material_w;
					exit when c03%notfound;
						begin

						select 	nvl(count(*),0)
						into 	qt_mat_cirurgia
						from 	material_autor_cirurgia a
						where 	a.nr_seq_autorizacao	= nr_seq_autorizacao_w
						and 	a.cd_material 		= :new.cd_material;

						if (qt_mat_cirurgia = 1 ) then --Lancado 1 vez o Mat na Autor Mat Esp
							delete
							from 	material_autor_cirurgia a
							where	((a.nr_seq_opme	= :new.nr_sequencia) or ((a.nr_seq_opme is null) and (a.cd_material = :new.cd_material)))
							and	a.nr_seq_autorizacao	= nr_seq_autorizacao_w
							and 	not exists (	select 	1
									from 	autorizacao_cirurgia b,
										material_autorizado c
									where    c.nr_sequencia_autor       = b.nr_seq_autor_conv
									and      b.nr_sequencia             = a.nr_seq_autorizacao
									and      c.cd_material              = a.cd_material);
						else
							delete
							from	material_autor_cirurgia
							where	((nr_seq_opme	= :new.nr_sequencia) or
								((nr_seq_opme is null) and (cd_material = :new.cd_material)))
							and	nr_seq_autorizacao	= nr_seq_autorizacao_w;
						end if;

						end;
					end loop;
					close c03;
					end;
				end loop;
				close c02;


				update	autorizacao_cirurgia a
				set	a.ie_estagio_autor = '6',
					a.nm_usuario	 = :new.nm_usuario,
					a.dt_atualizacao	 = sysdate
				where	a.nr_sequencia	 = nr_seq_autorizacao_w
				and	not exists(	select	1
							from	material_autor_cirurgia x
							where	x.nr_seq_autorizacao = a.nr_sequencia);


				update	autorizacao_convenio a
				set	a.nr_seq_estagio = (	select 	max(x.nr_sequencia)
								from	estagio_autorizacao x
								where	x.ie_interno = '70'
								and 	x.ie_situacao = 'A'
								and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = x.cd_empresa),
					a.nm_usuario	 = :new.nm_usuario,
					a.dt_atualizacao = sysdate
				where	a.nr_seq_agenda  = :new.nr_seq_agenda
				and	((a.nr_sequencia = nr_seq_autor_conv_w) or  (a.nr_seq_autor_cirurgia = nr_seq_autorizacao_w))
				and	not exists(	select	1
							from	material_autorizado x
							where	x.nr_sequencia_autor = a.nr_sequencia)
				and	not exists(	select	1
							from	procedimento_autorizado p
							where	p.nr_sequencia_autor = a.nr_sequencia);


			end if;

		end if;
		end;
	end loop;
	close c01;



	if	(nvl(ie_zerar_qt_opme_exc_w,'E') = 'A') and
		(:new.ie_origem_inf = 'E') then


		update	material_autor_cirurgia
		set	qt_solicitada	= 0,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate,
			ds_observacao 	= expressao6_w || ' ' || :new.nm_usuario || ' ' || expressao7_w || ' '
							|| PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate,'timestamp',ESTABLISHMENT_TIMEZONE_UTILS.GETTIMEZONE)
		where	nr_seq_opme	= :new.nr_sequencia;

		update	material_autorizado
		set	qt_solicitada	= 0,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate,
			ds_observacao	= expressao6_w || ' ' || :new.nm_usuario || ' ' || expressao7_w || ' '
							|| PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate,'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.GETTIMEZONE)
		where	nr_seq_opme	= :new.nr_sequencia;

		select 	count(1)
		into	qt_autor_conv_w
		from	material_autorizado a,
			autorizacao_convenio b
		where	a.nr_sequencia_autor = b.nr_sequencia
		and	b.nr_seq_agenda = :new.nr_seq_agenda
		--and	b.ie_tipo_autorizacao	= '4'
		and	a.qt_solicitada	 > 0
		and	exists(	select	1
					from	material_autorizado x
					where	x.nr_sequencia_autor = b.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia);


		select	count(1)
		into	qt_autor_cir_w
		from	material_autor_cirurgia a,
			autorizacao_cirurgia b
		where	a.nr_seq_autorizacao = b.nr_sequencia
		and	b.nr_seq_agenda = :new.nr_seq_agenda
		and	a.qt_solicitada > 0
		and	exists(	select	1
				from	material_autor_cirurgia x
				where	x.nr_seq_autorizacao = b.nr_sequencia
				and	x.nr_seq_opme	     = :new.nr_sequencia);


		if	(qt_autor_cir_w = 0) then

			update	autorizacao_cirurgia a
			set	a.ie_estagio_autor = '6',
				a.nm_usuario	= :new.nm_usuario,
				a.dt_atualizacao	= sysdate
			where	a.nr_seq_agenda = :new.nr_seq_agenda
			and	exists(	select	1
					from	material_autor_cirurgia x
					where	x.nr_seq_autorizacao = a.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia);

		end if;

		if	(qt_autor_conv_w = 0) then
			-- A procedure atualizar_autorizacao_convenio nao pode ser chamada por que vai dar referencia circular
			update	autorizacao_convenio a
			set	a.nr_seq_estagio = (	select 	max(x.nr_sequencia)
							from	estagio_autorizacao x
							where	x.ie_interno = '70'
							and 	x.ie_situacao = 'A'
							and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = x.cd_empresa),
				a.nm_usuario	 = :new.nm_usuario,
				a.dt_atualizacao = sysdate
			where	a.nr_seq_agenda  = :new.nr_seq_agenda
			and	exists(	select	1
					from	material_autorizado x
					where	x.nr_sequencia_autor = a.nr_sequencia
					and	x.nr_seq_opme	     = :new.nr_sequencia)
			and	not exists(	select	1
							from	procedimento_autorizado p
							where	p.nr_sequencia_autor = a.nr_sequencia);

		end if;

	end if;


	open C05;
	loop
	fetch C05 into
		nr_seq_mat_autor_w;
	exit when C05%notfound;
		begin

		if	(nvl(:old.vl_unitario_item,0) <> nvl(:new.vl_unitario_item,0)) then

			if	(ie_regra_acresc_mat_autor_w = 'S') then

				update	material_autorizado
				set	vl_cotado	= :new.vl_unitario_item,
					vl_unitario	= decode(nvl(ie_origem_preco,6),6,decode(ie_reducao_acrescimo,'R',
									(nvl(:new.vl_unitario_item,0) - (nvl(:new.vl_unitario_item,0)  * nvl(pr_adicional/100,0))),
									(nvl(:new.vl_unitario_item,0)  + (nvl(:new.vl_unitario_item,0)  * nvl(pr_adicional/100,0)))),nvl(:new.vl_unitario_item,0)),
					nm_usuario	= :new.nm_usuario,
					dt_atualizacao	= sysdate
				where	nr_sequencia	= nr_seq_mat_autor_w;

			elsif	(ie_regra_acresc_mat_autor_w = 'N') then

				update	material_autorizado
				set	vl_cotado	= :new.vl_unitario_item,
					vl_unitario	= :new.vl_unitario_item,
					nm_usuario	= :new.nm_usuario,
					dt_atualizacao	= sysdate
				where	nr_sequencia	= nr_seq_mat_autor_w;

			end if;

		end if;

		if	(Nvl(:new.cd_cgc,'X') <> Nvl(:old.cd_cgc,'X')) then
			update	material_autorizado
			set	cd_cgc_fabricante = nvl(:new.cd_cgc, cd_cgc_fabricante),
				nm_usuario	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_mat_autor_w;

		end if;
		end;
	end loop;
	close C05;

	if	(nvl(:old.vl_unitario_item,0) <> nvl(:new.vl_unitario_item,0)) then
		:new.vl_unitario_atualizado := (:new.vl_unitario_item - Nvl(:new.vl_desconto,0));
	end 	if;

	if	(:old.ie_autorizado is not null) and
		(:old.ie_autorizado <> :new.ie_autorizado) then
		ie_evento_w :=	'ASAA';
	elsif	(:new.ie_origem_inf = 'E') then
		ie_evento_w :=	'EOPME';
	else
		ie_evento_w :=	'AOPME';
	end if;

	if	(:new.nr_seq_agenda is not null) then
		select	cd_pessoa_fisica,
			nvl(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),nvl(qt_idade_paciente,0)),
			Obter_Sexo_PF(cd_pessoa_fisica,'C'),
			cd_medico,
			cd_convenio,
			nr_seq_proc_interno,
			cd_agenda,
			hr_inicio,
			cd_procedimento,
			ie_origem_proced,
			dt_cancelamento,
			cd_motivo_cancelamento,
			nr_atendimento,
			substr(ds_observacao,1,255)
		into	cd_pessoa_fisica_w,
			qt_idade_w,
			ie_sexo_w,
			cd_medico_w,
			cd_convenio_w,
			nr_seq_proc_interno_w,
			cd_agenda_w,
			hr_inicio_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			dt_cancelamento_w,
			cd_motivo_cancel_w,
			nr_atendimento_w,
			ds_observacao_ww
		from	agenda_paciente
		where	nr_sequencia = :new.nr_seq_agenda;


		open c04;
		loop
		fetch c04 into
			nr_seq_evento_w;
		exit when c04%notfound;
			begin
			gerar_evento_agenda_trigger(	nr_seq_evento_w,
							null,
							cd_pessoa_fisica_w,
							null,
							:new.nm_usuario,
							cd_agenda_w,
							hr_inicio_w,
							cd_medico_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							dt_cancelamento_w,
							:new.cd_material,
							:new.qt_material,
							:new.ie_autorizado,
							cd_convenio_w,
							cd_motivo_cancel_w,
							'S',
							null,
							null,
							null,
							null,
							null,
							ds_observacao_ww,
							NULL,
							NULL,
							NULL,
							:new.cd_cgc);
			end;
		end loop;
		close c04;
	end if;

	begin

	obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);


	select  max(a.ie_status_agenda),
		max(b.cd_tipo_agenda)
	into	ie_status_agenda_w,
		cd_tipo_agenda_w
	from    agenda_paciente a,
		agenda b
	where   a.cd_agenda = b.cd_agenda
	and	nr_sequencia = :new.nr_seq_agenda;

	exception
		when others then
		null;
	end;

	if	(ie_permite_alt_executada_w = 'N') and
		(cd_tipo_agenda_w = 1) and
		(ie_status_agenda_w = 'E') then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

	end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_ATUAL
before insert ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE

ie_permite_alt_executada_w	varchar2(1);
ie_status_agenda_w		varchar2(3);
cd_tipo_agenda_w		number(10);

pragma autonomous_transaction;

BEGIN
begin

obter_param_usuario(871, 758, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_alt_executada_w);

select  max(a.ie_status_agenda),
	max(b.cd_tipo_agenda)
into	ie_status_agenda_w,
	cd_tipo_agenda_w
from    agenda_paciente a,
	agenda b
where   a.cd_agenda = b.cd_agenda
and	nr_sequencia = :new.nr_seq_agenda;

exception
	when others then
      	null;
end;

if	(ie_permite_alt_executada_w = 'N') and
	(cd_tipo_agenda_w = 1) and
	(ie_status_agenda_w = 'E') and
	(:new.ie_integracao <> 'S') then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(236679);

end if;

if	(:new.vl_unitario_item > 0) and
	(:new.IE_INTEGRACAO <> 'S') then
	:new.vl_unitario_atualizado := (:new.vl_unitario_item - Nvl(:new.vl_desconto,0));
end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_pac_opme_tie
after insert or update or delete ON TASY.AGENDA_PAC_OPME for each row
declare

json_w            philips_json;
json_data_w       clob;
opsm_w            varchar2(5);
nr_seq_agenda_w   agenda_pac_opme.nr_seq_agenda%type;

pragma autonomous_transaction;

cursor c01 is
select  a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        a.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        (to_char((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)),'hh24:mi:ss')) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7),
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        pessoa_fisica d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate
and     sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24
and     b.cd_tipo_agenda = 1
and     a.nr_sequencia = nr_seq_agenda_w
union
select  a.nr_sequencia schedule_id,
        obter_nome_agenda(a.cd_agenda) schedule,
        b.nr_seq_classif schedule_classification_id,
        substr(obter_desc_classif_agenda_cir(b.nr_seq_classif),1,255) schedule_classification,
        a.ie_status_agenda schedule_status_id,
        substr(obter_status_agenda_paciente(a.nr_sequencia),1,255) schedule_status,
        a.dt_agenda schedule_date,
        a.dt_agendamento scheduling_date,
        a.cd_agenda schedule_code,
        j.cd_procedimento procedure_id,
        substr(obter_descricao_procedimento(j.cd_procedimento, j.ie_origem_proced),1,100) procedure_description,
        wheb_mensagem_pck.get_texto(163013) procedure_status,
        a.nr_reserva reservation,
        c.dt_entrada admission_date,
        a.dt_chegada_prev estimated_admission_date,
        obter_estagio_autor_agepac(a.nr_sequencia,'C') authorization_status_id,
        obter_estagio_autor_agepac(a.nr_sequencia,'D') authorization_status,
        a.hr_inicio start_time,
        a.qt_idade_gestacional gestational_age,
        a.qt_idade_paciente age,
        substr(obter_nome_medico(a.cd_medico,'NC'),1,255) surgeon,
        c.nr_atendimento encounter,
        obter_unid_atend_setor_atual(c.nr_atendimento, obter_setor_atendimento(a.nr_atendimento), 'U') room,
        substr(obter_desc_convenio(a.cd_convenio),1,255) insurance,
        a.cd_usuario_convenio insurance_user,
        obter_valor_dominio(1545, a.ie_reserva_leito) admission,
        a.nr_telefone telephone,
        a.nr_minuto_duracao procedure_estimated_duration,
        (to_char((a.hr_inicio + to_number(a.nr_minuto_duracao)/(24*60)),'hh24:mi:ss')) procedure_duration,
        a.cd_pessoa_fisica patient_id,
        d.nm_pessoa_fisica patient,
        d.dt_nascimento birth,
        d.nr_prontuario medical_record,
        d.ie_tipo_sangue || ' ' || d.ie_fator_rh blood,
        d.nr_telefone_celular cellphone,
        obter_nome_pessoa_fisica(d.cd_pessoa_mae,'') mother,
        decode(d.ie_sexo, 'M', wheb_mensagem_pck.get_texto(354750), 'F', wheb_mensagem_pck.get_texto(354751)) gender,
        decode(a.ie_anestesia, 'N', wheb_mensagem_pck.get_texto(1118500), 'S', wheb_mensagem_pck.get_texto(307592), 'X', wheb_mensagem_pck.get_texto(763069)) anaesthesia,
        decode(a.ie_carater_cirurgia, 'A', wheb_mensagem_pck.get_texto(796908), 'E', wheb_mensagem_pck.get_texto(312715), 'M', wheb_mensagem_pck.get_texto(415759), 'U', wheb_mensagem_pck.get_texto(309481)) surgery_nature,
        nvl((select max(wheb_mensagem_pck.get_texto(94754)) from agenda_pac_sangue h where h.nr_seq_agenda = a.nr_sequencia), wheb_mensagem_pck.get_texto(94755)) preorder_blood,
        nvl((nvl((select max(wheb_mensagem_pck.get_texto(94754)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 7),
        (select max(wheb_mensagem_pck.get_texto(1118702)) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 8))), wheb_mensagem_pck.get_texto(94755)) recommendation,
        (select max(h.ds_classif_paciente) from tipo_classificao_paciente h, agenda_paciente i where h.nr_sequencia = i.nr_seq_tipo_classif_pac and i.nr_sequencia = a.nr_sequencia) patient_classification,
        (select max(h.nr_cirurgia) from cirurgia h where h.nr_seq_agenda = a.nr_sequencia) surgery,
        (select max(h.ds_cobertura) from convenio_cobertura h where h.nr_sequencia = a.nr_seq_cobertura) insurance_coverage,
        (select max(h.ds_observacao) from pessoa_classif h where e.cd_pessoa_fisica = h.cd_pessoa_fisica and h.nr_seq_classif = 10) notes,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico = 6818), wheb_mensagem_pck.get_texto(94755)) ICU,
        nvl((select     max(wheb_mensagem_pck.get_texto(94754))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6319), wheb_mensagem_pck.get_texto(94755)) NICU,
        nvl((select     max(substr(obter_valor_dominio(3195,h.ie_status),1,255))
             from       agenda_pac_servico h,
                        agenda_paciente i
             where      i.nr_sequencia = h.nr_seq_agenda
             and        i.nr_sequencia = a.nr_sequencia
             and        nr_seq_proc_servico =  6817), wheb_mensagem_pck.get_texto(94755)) surgical_services_status
from    agenda_paciente a,
        agenda b,
        atendimento_paciente c,
        pessoa_fisica d,
        pessoa_classif e,
        classif_agenda_cirurgica f,
        autorizacao_convenio g,
        agenda_paciente_proc j
where   a.cd_pessoa_fisica = e.cd_pessoa_fisica(+)
and     g.nr_seq_agenda(+) = a.nr_sequencia
and     a.nr_atendimento = c.nr_atendimento(+)
and     a.cd_agenda = b.cd_agenda
and     f.nr_sequencia = b.nr_seq_classif(+)
and     d.cd_pessoa_fisica = a.cd_pessoa_fisica
and     j.nr_sequencia = a.nr_sequencia
and     a.dt_agenda >= sysdate
and     a.ie_status_agenda not in ('L', 'B', 'C')
and     a.hr_inicio between sysdate
and     (sysdate + to_number(obter_valor_param_usuario(410, 64, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento))/24)
and     b.cd_tipo_agenda = 1
and     a.nr_sequencia = nr_seq_agenda_w
order by schedule_date, start_time, patient;

begin

if (inserting or updating) then

    nr_seq_agenda_w := :new.nr_seq_agenda;

    if (:new.dt_exclusao is null) then
        opsm_w := wheb_mensagem_pck.get_texto(94754);
    else
        select nvl(max(wheb_mensagem_pck.get_texto(94754)),wheb_mensagem_pck.get_texto(94755))
        into opsm_w
        from agenda_pac_opme h
        where h.nr_seq_agenda = nr_seq_agenda_w
        and h.nr_sequencia <> :new.nr_sequencia
        and h.dt_exclusao is null;
    end if;

elsif (deleting) then

    nr_seq_agenda_w := :old.nr_seq_agenda;

    select nvl(max(wheb_mensagem_pck.get_texto(94754)),wheb_mensagem_pck.get_texto(94755))
    into opsm_w
    from agenda_pac_opme h
    where h.nr_seq_agenda = nr_seq_agenda_w
    and h.nr_sequencia <> :old.nr_sequencia
    and h.dt_exclusao is null;

end if;

if (wheb_usuario_pck.get_nm_usuario is not null) then

	for c01_w in c01 loop

		json_w := philips_json();
		json_w.put('schedule_id', c01_w.schedule_id);
		json_w.put('schedule', c01_w.schedule);
		json_w.put('schedule_classification_id', c01_w.schedule_classification_id);
		json_w.put('schedule_classification', c01_w.schedule_classification);
		json_w.put('schedule_status_id', c01_w.schedule_status_id);
		json_w.put('schedule_status', c01_w.schedule_status);
		json_w.put('schedule_date', c01_w.schedule_date);
		json_w.put('scheduling_date', c01_w.scheduling_date);
		json_w.put('schedule_code', c01_w.schedule_code);
		json_w.put('procedure_id', c01_w.procedure_id);
		json_w.put('procedure_description', c01_w.procedure_description);
		json_w.put('procedure_status', c01_w.procedure_status);
		json_w.put('reservation', c01_w.reservation);
		json_w.put('admission_date', c01_w.admission_date);
		json_w.put('estimated_admission_date', c01_w.estimated_admission_date);
		json_w.put('authorization_status_id', c01_w.authorization_status_id);
		json_w.put('authorization_status', c01_w.authorization_status);
		json_w.put('start_time', c01_w.start_time);
		json_w.put('gestational_age', c01_w.gestational_age);
		json_w.put('age', c01_w.age);
		json_w.put('surgeon', c01_w.surgeon);
		json_w.put('encounter', c01_w.encounter);
		json_w.put('room', c01_w.room);
		json_w.put('insurance', c01_w.insurance);
		json_w.put('insurance_user', c01_w.insurance_user);
		json_w.put('admission', c01_w.admission);
		json_w.put('telephone', c01_w.telephone);
		json_w.put('procedure_estimated_duration', c01_w.procedure_estimated_duration);
		json_w.put('procedure_duration', c01_w.procedure_duration);
		json_w.put('patient_id', c01_w.patient_id);
		json_w.put('patient', c01_w.patient);
		json_w.put('birth', c01_w.birth);
		json_w.put('medical_record', c01_w.medical_record);
		json_w.put('blood', c01_w.blood);
		json_w.put('cellphone', c01_w.cellphone);
		json_w.put('mother', c01_w.mother);
		json_w.put('gender', c01_w.gender);
		json_w.put('anaesthesia', c01_w.anaesthesia);
		json_w.put('surgery_nature', c01_w.surgery_nature);
		json_w.put('preorder_blood', c01_w.preorder_blood);
		json_w.put('OPSM', opsm_w);
		json_w.put('recommendation', c01_w.recommendation);
		json_w.put('patient_classification', c01_w.patient_classification);
		json_w.put('surgery', c01_w.surgery);
		json_w.put('insurance_coverage', c01_w.insurance_coverage);
		json_w.put('notes', c01_w.notes);
		json_w.put('ICU', c01_w.ICU);
		json_w.put('NICU', c01_w.NICU);
		json_w.put('surgical_services_status', c01_w.surgical_services_status);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end loop;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_UPDATE_AUTOR
BEFORE UPDATE ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE

nr_seq_autorizacao_w		number(15,0);

cursor c01 is
select	a.nr_sequencia
from	autorizacao_convenio a
where	a.nr_seq_agenda	= :new.nr_seq_agenda;

BEGIN

if	((wheb_usuario_pck.get_ie_executar_trigger = 'S') and (:old.ie_gerar_autor <> :new.ie_gerar_autor))  then

	open c01;
	loop
	fetch c01 into
		nr_seq_autorizacao_w;
	exit when c01%notfound;
		begin

 		delete
		from	material_autorizado
		where	cd_material	= :new.cd_material
		and	:new.ie_gerar_autor	= 'N'
		and	nr_sequencia_autor	= nr_seq_autorizacao_w;
		end;

	end loop;
	close c01;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_OPME_tp  after update ON TASY.AGENDA_PAC_OPME FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_PADRAO,1,500);gravar_log_alteracao(substr(:old.IE_PADRAO,1,4000),substr(:new.IE_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_AGENDA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA,1,4000),substr(:new.NR_SEQ_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_MATERIAL,1,500);gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_AGENDA_INT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA_INT,1,4000),substr(:new.NR_SEQ_AGENDA_INT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA_INT',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_EXCLUSAO,1,500);gravar_log_alteracao(substr(:old.DT_EXCLUSAO,1,4000),substr(:new.DT_EXCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EXCLUSAO',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC,1,500);gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_INTEGRACAO,1,500);gravar_log_alteracao(substr(:old.IE_INTEGRACAO,1,4000),substr(:new.IE_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTEGRACAO',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GERAR_AUTOR,1,500);gravar_log_alteracao(substr(:old.IE_GERAR_AUTOR,1,4000),substr(:new.IE_GERAR_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_AUTOR',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_UNITARIO_ITEM,1,500);gravar_log_alteracao(substr(:old.VL_UNITARIO_ITEM,1,4000),substr(:new.VL_UNITARIO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNITARIO_ITEM',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_AUTORIZADO,1,500);gravar_log_alteracao(substr(:old.IE_AUTORIZADO,1,4000),substr(:new.IE_AUTORIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTORIZADO',ie_log_w,ds_w,'AGENDA_PAC_OPME',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AGENDA_PAC_OPME ADD (
  CONSTRAINT AGPOPME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_OPME ADD (
  CONSTRAINT AGPOPME_FORPAGA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_PAGTO) 
 REFERENCES TASY.FORMA_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGPOPME_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPOPME_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT AGPOPME_MOEXOPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXCLUSAO) 
 REFERENCES TASY.MOTIVO_EXC_OPME_AGENDA_PAC (NR_SEQUENCIA),
  CONSTRAINT AGPOPME_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT AGPOPME_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGPOPME_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_INT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPOPME_AGEINIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.AGENDA_INTEGRADA_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPOPME_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPOPME_MATMARC_FK 
 FOREIGN KEY (CD_MATERIAL, NR_SEQ_MARCA) 
 REFERENCES TASY.MATERIAL_MARCA (CD_MATERIAL,NR_SEQUENCIA),
  CONSTRAINT AGPOPME_AGPAOPA_FK 
 FOREIGN KEY (NR_SEQ_AGRUP) 
 REFERENCES TASY.AGENDA_PAC_OPME_AGRUP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_PAC_OPME TO NIVEL_1;

