ALTER TABLE TASY.REGRA_LIB_TIT_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LIB_TIT_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LIB_TIT_USUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)        NOT NULL,
  IE_NIVEL             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RELTUSU_PK ON TASY.REGRA_LIB_TIT_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELTUSU_PK
  MONITORING USAGE;


CREATE INDEX TASY.RELTUSU_RELIITP_FK_I ON TASY.REGRA_LIB_TIT_USUARIO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_LIB_TIT_USUARIO ADD (
  CONSTRAINT RELTUSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_LIB_TIT_USUARIO ADD (
  CONSTRAINT RELTUSU_RELIITP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_LIB_TIT_PAGAR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_LIB_TIT_USUARIO TO NIVEL_1;

