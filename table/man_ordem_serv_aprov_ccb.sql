ALTER TABLE TASY.MAN_ORDEM_SERV_APROV_CCB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_APROV_CCB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_APROV_CCB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM_SERV    NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPE        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_APROVACAO         DATE,
  DT_REPROVACAO        DATE,
  NR_SEQ_IMPACTO       NUMBER(10)               NOT NULL,
  NM_USUARIO_APROV     VARCHAR2(15 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  NM_USUARIO_REPROV    VARCHAR2(15 BYTE),
  DS_MOTIVO_REPROV     VARCHAR2(255 BYTE),
  NR_SEQ_GRUPO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSAC_CCBEQUI_FK_I ON TASY.MAN_ORDEM_SERV_APROV_CCB
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSAC_GRUDESE_FK_I ON TASY.MAN_ORDEM_SERV_APROV_CCB
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSAC_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_APROV_CCB
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANOSAC_MANOSIM_FK_I ON TASY.MAN_ORDEM_SERV_APROV_CCB
(NR_SEQ_IMPACTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANOSAC_PK ON TASY.MAN_ORDEM_SERV_APROV_CCB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ordem_serv_aprov_ccb_before
before insert or update ON TASY.MAN_ORDEM_SERV_APROV_CCB for each row
declare

ie_complaint_w			man_ordem_servico.ie_complaint%type;
ie_waiting_complaint_w	man_ordem_servico.ie_complaint%type;

begin

	select	nvl(max('S'), 'N')
	into	ie_complaint_w
	from	man_ordem_servico a
	where	man_complaint_pck.obter_se_os_complaint(a.nr_sequencia)	= 'S'
	and		man_obter_se_loc_terceiro(a.nr_seq_localizacao)	= 'S'
	and		a.ie_classificacao	= 'E'
	and		a.nr_sequencia		= :new.nr_seq_ordem_serv;

	if ((ie_complaint_w = 'S') and ((:old.dt_aprovacao is null and :new.dt_aprovacao is not null) or (:old.dt_reprovacao is null and :new.dt_reprovacao is not null))) then

		select	nvl(max('S'), 'N')
		into	ie_waiting_complaint_w
		from	man_ordem_servico a
		where	a.nr_sequencia		= :new.nr_seq_ordem_serv
		and		a.ie_classificacao	= 'E'
		and		man_obter_se_loc_terceiro(a.nr_seq_localizacao) = 'S'
		and		man_complaint_pck.obter_status_complaint(a.nr_sequencia) in ('WFI', 'WT', 'WD');

		if (ie_waiting_complaint_w = 'S') then
			if updating then
				wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(1094590);
			end if;
		end if;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERV_APROV_CCB_tp  after update ON TASY.MAN_ORDEM_SERV_APROV_CCB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_APROVACAO,1,4000),substr(:new.DT_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_APROVACAO',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REPROVACAO,1,4000),substr(:new.DT_REPROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_REPROVACAO',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO_REPROV,1,4000),substr(:new.DS_MOTIVO_REPROV,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_REPROV',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_REPROV,1,4000),substr(:new.NM_USUARIO_REPROV,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_REPROV',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_APROV,1,4000),substr(:new.NM_USUARIO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_APROV',ie_log_w,ds_w,'MAN_ORDEM_SERV_APROV_CCB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MAN_ORDEM_SERV_APROV_CCB ADD (
  CONSTRAINT MANOSAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_APROV_CCB ADD (
  CONSTRAINT MANOSAC_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA),
  CONSTRAINT MANOSAC_CCBEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.CCB_EQUIPE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANOSAC_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANOSAC_MANOSIM_FK 
 FOREIGN KEY (NR_SEQ_IMPACTO) 
 REFERENCES TASY.MAN_ORDEM_SERV_IMPACTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MAN_ORDEM_SERV_APROV_CCB TO NIVEL_1;

