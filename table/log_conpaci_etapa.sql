ALTER TABLE TASY.LOG_CONPACI_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_CONPACI_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_CONPACI_ETAPA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_INTERNO_CONTA      NUMBER(15),
  DT_ATUALIZACAO        DATE,
  NM_USUARIO            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ETAPA              DATE,
  NR_SEQ_ETAPA          NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  DT_FIM_ETAPA          DATE,
  NR_SEQ_MOTIVO_DEV     NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  NR_LOTE_BARRAS        NUMBER(10),
  CD_PESSOA_EXEC        VARCHAR2(10 BYTE),
  DT_RECEBIMENTO        DATE,
  VL_CONTA              NUMBER(15,2),
  NR_SEQ_PROTOCOLO      NUMBER(10),
  IE_ETAPA_PROTOCOLO    VARCHAR2(1 BYTE),
  IE_ETAPA_CRITICA      VARCHAR2(1 BYTE),
  DT_PERIODO_INICIAL    DATE,
  DT_PERIODO_FINAL      DATE,
  NR_SEQ_CONTA_ORIGEM   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGCONE_CONPACI_FK_I ON TASY.LOG_CONPACI_ETAPA
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGCONE_FATETAP_FK_I ON TASY.LOG_CONPACI_ETAPA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGCONE_FATETAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOGCONE_FATMODE_FK_I ON TASY.LOG_CONPACI_ETAPA
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGCONE_FATMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOGCONE_PESFISI_FK_I ON TASY.LOG_CONPACI_ETAPA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGCONE_PESFISI_FK2_I ON TASY.LOG_CONPACI_ETAPA
(CD_PESSOA_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOGCONE_PK ON TASY.LOG_CONPACI_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGCONE_PK
  MONITORING USAGE;


CREATE INDEX TASY.LOGCONE_SETATEN_FK_I ON TASY.LOG_CONPACI_ETAPA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGCONE_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.LOG_CONPACI_ETAPA_tp  after update ON TASY.LOG_CONPACI_ETAPA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_EXEC,1,4000),substr(:new.CD_PESSOA_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_EXEC',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ETAPA,1,4000),substr(:new.DT_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'DT_ETAPA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_ETAPA,1,4000),substr(:new.DT_FIM_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_ETAPA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_FINAL,1,4000),substr(:new.DT_PERIODO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_FINAL',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_INICIAL,1,4000),substr(:new.DT_PERIODO_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_INICIAL',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_RECEBIMENTO,1,4000),substr(:new.DT_RECEBIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_RECEBIMENTO',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ETAPA_CRITICA,1,4000),substr(:new.IE_ETAPA_CRITICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ETAPA_CRITICA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ETAPA_PROTOCOLO,1,4000),substr(:new.IE_ETAPA_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ETAPA_PROTOCOLO',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LOTE_BARRAS,1,4000),substr(:new.NR_LOTE_BARRAS,1,4000),:new.nm_usuario,nr_seq_w,'NR_LOTE_BARRAS',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_ORIGEM,1,4000),substr(:new.NR_SEQ_CONTA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_ORIGEM',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ETAPA,1,4000),substr(:new.NR_SEQ_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ETAPA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_DEV,1,4000),substr(:new.NR_SEQ_MOTIVO_DEV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_DEV',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOLO,1,4000),substr(:new.NR_SEQ_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOLO',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CONTA,1,4000),substr(:new.VL_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_CONTA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'LOG_CONPACI_ETAPA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOG_CONPACI_ETAPA ADD (
  CONSTRAINT LOGCONE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_CONPACI_ETAPA ADD (
  CONSTRAINT LOGCONE_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT LOGCONE_FATETAP_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.FATUR_ETAPA (NR_SEQUENCIA),
  CONSTRAINT LOGCONE_FATMODE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.FATUR_MOTIVO_DEVOL (NR_SEQUENCIA),
  CONSTRAINT LOGCONE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LOGCONE_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_EXEC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LOGCONE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.LOG_CONPACI_ETAPA TO NIVEL_1;

