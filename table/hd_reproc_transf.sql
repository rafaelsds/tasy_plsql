ALTER TABLE TASY.HD_REPROC_TRANSF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_REPROC_TRANSF CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_REPROC_TRANSF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_TRANSFERENCIA     DATE                     NOT NULL,
  NR_SEQ_REPROC        NUMBER(10)               NOT NULL,
  NR_SEQ_UNID_ORIGEM   NUMBER(10)               NOT NULL,
  NR_SEQ_UNID_DESTINO  NUMBER(10)               NOT NULL,
  CD_PF_TRANSF         VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDREPTR_HDREPDI_FK_I ON TASY.HD_REPROC_TRANSF
(NR_SEQ_REPROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDREPTR_HDUNIDD_FK_I ON TASY.HD_REPROC_TRANSF
(NR_SEQ_UNID_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDREPTR_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDREPTR_HDUNIDD_FK2_I ON TASY.HD_REPROC_TRANSF
(NR_SEQ_UNID_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDREPTR_HDUNIDD_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.HDREPTR_PESFISI_FK_I ON TASY.HD_REPROC_TRANSF
(CD_PF_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDREPTR_PK ON TASY.HD_REPROC_TRANSF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDREPTR_PK
  MONITORING USAGE;


ALTER TABLE TASY.HD_REPROC_TRANSF ADD (
  CONSTRAINT HDREPTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_REPROC_TRANSF ADD (
  CONSTRAINT HDREPTR_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DESTINO) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDREPTR_HDUNIDD_FK2 
 FOREIGN KEY (NR_SEQ_UNID_ORIGEM) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDREPTR_PESFISI_FK 
 FOREIGN KEY (CD_PF_TRANSF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDREPTR_HDREPDI_FK 
 FOREIGN KEY (NR_SEQ_REPROC) 
 REFERENCES TASY.HD_REPROC_DIALIZADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_REPROC_TRANSF TO NIVEL_1;

