ALTER TABLE TASY.SIM_REGRA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIM_REGRA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIM_REGRA_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SIM            NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_TIPO_ATENDIMENTO   NUMBER(3),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  TX_QUANTIDADE         NUMBER(15,4),
  TX_CUSTO_OPER         NUMBER(15,4),
  TX_MEDICO             NUMBER(15,4),
  TX_MATERIAL           NUMBER(15,4),
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE),
  IE_REGRA              VARCHAR2(3 BYTE)        NOT NULL,
  DT_REFERENCIA         DATE,
  CD_CONVENIO           NUMBER(5),
  CD_CATEGORIA          VARCHAR2(10 BYTE),
  IE_PACOTE             VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_BASE               DATE,
  CD_PLANO              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIMREPR_AREPROC_FK_I ON TASY.SIM_REGRA_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_CATCONV_FK_I ON TASY.SIM_REGRA_PROC
(CD_CONVENIO, CD_CATEGORIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_CONCONT_FK_I ON TASY.SIM_REGRA_PROC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_CONPLAN_FK_I ON TASY.SIM_REGRA_PROC
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIMREPR_CONVENI_FK_I ON TASY.SIM_REGRA_PROC
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_ESPPROC_FK_I ON TASY.SIM_REGRA_PROC
(CD_ESPECIALIDADE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_GRUPROC_FK_I ON TASY.SIM_REGRA_PROC
(CD_GRUPO_PROC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SIMREPR_PK ON TASY.SIM_REGRA_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_PROCEDI_FK_I ON TASY.SIM_REGRA_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_SETATEN_FK_I ON TASY.SIM_REGRA_PROC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMREPR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMREPR_SIMULAC_FK_I ON TASY.SIM_REGRA_PROC
(NR_SEQ_SIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SIM_REGRA_PROC ADD (
  CONSTRAINT SIMREPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIM_REGRA_PROC ADD (
  CONSTRAINT SIMREPR_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT SIMREPR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SIMREPR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT SIMREPR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT SIMREPR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT SIMREPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SIMREPR_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT SIMREPR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT SIMREPR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT SIMREPR_SIMULAC_FK 
 FOREIGN KEY (NR_SEQ_SIM) 
 REFERENCES TASY.SIMULACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SIM_REGRA_PROC TO NIVEL_1;

