ALTER TABLE TASY.PE_INTERV_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_INTERV_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_INTERV_MODELO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC          NUMBER(10)               NOT NULL,
  NR_SEQ_MODELO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEINMOD_PEPROCE_FK_I ON TASY.PE_INTERV_MODELO
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINMOD_PEPROCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEINMOD_PESAEMO_FK_I ON TASY.PE_INTERV_MODELO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINMOD_PESAEMO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEINMOD_PK ON TASY.PE_INTERV_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINMOD_PK
  MONITORING USAGE;


ALTER TABLE TASY.PE_INTERV_MODELO ADD (
  CONSTRAINT PEINMOD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_INTERV_MODELO ADD (
  CONSTRAINT PEINMOD_PESAEMO_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PE_SAE_MODELO (NR_SEQUENCIA),
  CONSTRAINT PEINMOD_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_INTERV_MODELO TO NIVEL_1;

