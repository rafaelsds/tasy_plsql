ALTER TABLE TASY.PROJ_REC_LIB_MOD_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_REC_LIB_MOD_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_REC_LIB_MOD_COMPRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOD_COMPRA    NUMBER(10)               NOT NULL,
  NR_SEQ_PROJ_REC      NUMBER(10),
  IE_LIBERADO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRLMOCO_PK ON TASY.PROJ_REC_LIB_MOD_COMPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRLMOCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRLMOCO_PRORECU_FK_I ON TASY.PROJ_REC_LIB_MOD_COMPRA
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRLMOCO_PRORECU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRLMOCO_REGLIMC_FK_I ON TASY.PROJ_REC_LIB_MOD_COMPRA
(NR_SEQ_MOD_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRLMOCO_REGLIMC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_rec_lib_mod_com_bfinsupd
before insert or update ON TASY.PROJ_REC_LIB_MOD_COMPRA for each row
begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


ALTER TABLE TASY.PROJ_REC_LIB_MOD_COMPRA ADD (
  CONSTRAINT PRLMOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_REC_LIB_MOD_COMPRA ADD (
  CONSTRAINT PRLMOCO_REGLIMC_FK 
 FOREIGN KEY (NR_SEQ_MOD_COMPRA) 
 REFERENCES TASY.REG_LIC_MOD_COMPRA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRLMOCO_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_REC_LIB_MOD_COMPRA TO NIVEL_1;

