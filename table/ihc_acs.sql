ALTER TABLE TASY.IHC_ACS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IHC_ACS CASCADE CONSTRAINTS;

CREATE TABLE TASY.IHC_ACS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLAIM         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_ACCOMMODATION     NUMBER(10),
  IE_CHARGE_RAISED     VARCHAR2(1 BYTE),
  DT_FROM              DATE,
  QT_LEAVE             NUMBER(10),
  QT_DAYS              NUMBER(10),
  DT_END               DATE,
  VL_CHARGE            NUMBER(12,4),
  IE_SITUATION         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IHCACSE_IHCCLAI_FK_I ON TASY.IHC_ACS
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IHCACSE_PK ON TASY.IHC_ACS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IHC_ACS ADD (
  CONSTRAINT IHCACSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IHC_ACS ADD (
  CONSTRAINT IHCACSE_IHCCLAI_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.IHC_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.IHC_ACS TO NIVEL_1;

