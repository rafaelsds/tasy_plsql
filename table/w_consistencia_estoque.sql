DROP TABLE TASY.W_CONSISTENCIA_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CONSISTENCIA_ESTOQUE
(
  CD_MATERIAL           NUMBER(10)              NOT NULL,
  DS_CONSISTENCIA       VARCHAR2(255 BYTE),
  DT_MESANO_REFERENCIA  DATE,
  IE_TIPO               VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCONSES_MATERIA_FK_I ON TASY.W_CONSISTENCIA_ESTOQUE
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_CONSISTENCIA_ESTOQUE ADD (
  CONSTRAINT WCONSES_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_CONSISTENCIA_ESTOQUE TO NIVEL_1;

