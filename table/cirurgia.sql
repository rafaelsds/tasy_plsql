ALTER TABLE TASY.CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA
(
  NR_CIRURGIA                 NUMBER(10)        NOT NULL,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  CD_MEDICO_CIRURGIAO         VARCHAR2(10 BYTE) NOT NULL,
  CD_PROCEDIMENTO_PRINC       NUMBER(15)        NOT NULL,
  CD_TIPO_ANESTESIA           VARCHAR2(10 BYTE),
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  DT_INICIO_PREVISTA          DATE              NOT NULL,
  DT_INICIO_REAL              DATE,
  NR_MIN_DURACAO_PREV         NUMBER(10)        NOT NULL,
  NR_MIN_DURACAO_REAL         NUMBER(10),
  DT_TERMINO                  DATE,
  NR_PRESCRICAO               NUMBER(10),
  NR_ATENDIMENTO              NUMBER(10),
  DT_ENTRADA_UNIDADE          DATE,
  CD_MEDICO_ANESTESISTA       VARCHAR2(10 BYTE),
  CD_CONVENIO                 NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  IE_ORIGEM_PROCED            NUMBER(10),
  IE_STATUS_CIRURGIA          VARCHAR2(3 BYTE),
  IE_MOTIVO_CANCELAMENTO      VARCHAR2(15 BYTE),
  IE_CARATER_CIRURGIA         VARCHAR2(1 BYTE),
  CD_TIPO_CIRURGIA            NUMBER(10),
  IE_ANAT_PATOL               VARCHAR2(1 BYTE),
  IE_TRAUMA                   VARCHAR2(1 BYTE),
  IE_ORTESE_PROTESE           VARCHAR2(1 BYTE),
  IE_ANTIBIOTICO              VARCHAR2(1 BYTE),
  IE_SANGUE                   VARCHAR2(1 BYTE),
  DS_ANTIBIOTICO              VARCHAR2(255 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  DT_TERMINO_PREVISTA         DATE,
  NR_SEQ_STATUS               NUMBER(10),
  DT_ENTRADA_RECUP            DATE,
  DT_SAIDA_RECUP_PREV         DATE,
  DT_SAIDA_RECUP              DATE,
  DS_ANDAMENTO                VARCHAR2(2000 BYTE),
  DT_CHAMADA                  DATE,
  DT_PREPARACAO               DATE,
  DT_CHEGADA_SALA             DATE,
  DT_CHEGADA_ANESTESISTA      DATE,
  DT_INICIO_ANESTESIA         DATE,
  DT_CHEGADA_CIRURGIAO        DATE,
  DT_INICIO_CIRURGIA          DATE,
  DT_FIM_CIRURGIA             DATE,
  DT_FIM_EXTUBACAO            DATE,
  DT_LIBERACAO_SALA           DATE,
  IE_ASA_ESTADO_PACIENTE      VARCHAR2(3 BYTE),
  CD_MEDICO_REQ               VARCHAR2(10 BYTE),
  IE_LEITO_UTI                VARCHAR2(1 BYTE),
  IE_PECA_ANAT_PATOL          VARCHAR2(1 BYTE),
  DT_CHEGADA_SRPA             DATE,
  DT_ENTR_MATMED              DATE,
  NM_USUARIO_MATMED           VARCHAR2(15 BYTE),
  DT_SAIDA_SALA               DATE,
  CD_ESTABELECIMENTO          NUMBER(4),
  DT_FIM_ANESTESIA            DATE,
  IE_PORTE                    VARCHAR2(15 BYTE),
  IE_LADO                     VARCHAR2(1 BYTE),
  NR_SEQ_PROC_INTERNO         NUMBER(10),
  QT_RIC                      NUMBER(1),
  DT_LIB_NISS                 DATE,
  IE_INFECCAO                 VARCHAR2(1 BYTE),
  DT_INFECCAO                 DATE,
  NR_SEQ_PROCED_NISS          NUMBER(10),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  QT_PESO                     NUMBER(10,3),
  NR_SEQ_INTERRUPCAO          NUMBER(10),
  IE_SITIO_INFECCAO           VARCHAR2(15 BYTE),
  IE_CONTROLA_PESO            VARCHAR2(1 BYTE),
  IE_TIPO_CIRURGIA            VARCHAR2(15 BYTE),
  IE_REOPERACAO               VARCHAR2(1 BYTE),
  DT_CANCELAMENTO             DATE,
  DT_LANC_AUTOMATICO          DATE,
  NR_SEQ_SITIO_PRINC          NUMBER(10),
  NR_SEQ_SITIO_ESPEC          NUMBER(10),
  NR_CIRURGIA_SUPERIOR        NUMBER(10),
  NR_SEQ_PEPO                 NUMBER(10),
  IE_VIA_ACESSO               VARCHAR2(15 BYTE),
  NR_SEQ_VIA                  NUMBER(10),
  NR_SEQ_AGENDA               NUMBER(10),
  IE_AVALIACAO_PRE            VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_REOP          NUMBER(10),
  CD_CATEGORIA                VARCHAR2(10 BYTE),
  IE_INTEGRACAO_OPME          VARCHAR2(1 BYTE),
  DT_LIBERACAO_ANESTESISTA    DATE,
  NM_USUARIO_LIB_ANEST        VARCHAR2(15 BYTE),
  DT_DESFEITA_LIB             DATE,
  DS_JUSTIFICATIVA_ASA        VARCHAR2(1000 BYTE),
  DT_FIM_CONFERENCIA          DATE,
  NM_USUARIO_FIM_CONFERENCIA  VARCHAR2(15 BYTE),
  DT_BLOQUEIO_FATURAMENTO     DATE,
  NM_USUARIO_BLOQUEIO_FAT     VARCHAR2(15 BYTE),
  NM_USUARIO_ANTIBIOTICO      VARCHAR2(15 BYTE),
  DT_INTERRUPCAO              DATE,
  NR_SEQ_MOTIVO               NUMBER(10),
  IE_VIDEO                    VARCHAR2(1 BYTE),
  NM_USUARIO_VIDEO            VARCHAR2(15 BYTE),
  IE_FINALIDADE               VARCHAR2(15 BYTE),
  NR_SEQ_TRANSPLANTE          NUMBER(10),
  DS_JUST_TROCA_PROCED        VARCHAR2(1000 BYTE),
  CD_PROCEDIMENTO_TUSS        NUMBER(15),
  NR_PRESCRICAO_ESPEC         NUMBER(10),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE),
  IE_EQUIP_VERIFICADO         VARCHAR2(1 BYTE),
  QT_PATOLOGICO               NUMBER(3),
  IE_PATOLOGICO               VARCHAR2(1 BYTE),
  IE_MICROBIOLOGICO           VARCHAR2(1 BYTE),
  QT_AMOSTRA                  NUMBER(3),
  QT_MICROBIOLOGICO           NUMBER(3),
  IE_AMOSTRA                  VARCHAR2(1 BYTE),
  DT_AMOSTRA                  DATE,
  DT_REQUISICAO_LAB           DATE,
  DT_AMOSTRA_PATO             DATE,
  DT_AMOSTRA_MICRO            DATE,
  DT_REQ_PATO_LAB             DATE,
  DT_REQ_MICRO_LAB            DATE,
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  NR_ORIGEM                   NUMBER(10),
  CD_EVOLUCAO                 NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRURGI_AGENDA_I ON TASY.CIRURGIA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_ANESTES_FK_I ON TASY.CIRURGIA
(CD_MEDICO_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_ATCONSPEPA_FK_I ON TASY.CIRURGIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_ATEPACU_FK_I ON TASY.CIRURGIA
(NR_ATENDIMENTO, DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_CIHTICI_FK_I ON TASY.CIRURGIA
(CD_TIPO_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_CIHTICI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_CIRSTAT_FK_I ON TASY.CIRURGIA
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_CIRSTAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_CIRURGI_FK_I ON TASY.CIRURGIA
(NR_CIRURGIA_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_CONVENI_FK_I ON TASY.CIRURGIA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_ESTABEL_FK_I ON TASY.CIRURGIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_EVOPACI_FK_I ON TASY.CIRURGIA
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I1 ON TASY.CIRURGIA
(DT_INICIO_REAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I2 ON TASY.CIRURGIA
(DT_INICIO_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I3 ON TASY.CIRURGIA
(NR_CIRURGIA, IE_PORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          96K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I4 ON TASY.CIRURGIA
(IE_STATUS_CIRURGIA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I5 ON TASY.CIRURGIA
(CD_MEDICO_CIRURGIAO, NR_ATENDIMENTO, DT_INICIO_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I6 ON TASY.CIRURGIA
(CD_MEDICO_CIRURGIAO, DT_INICIO_PREVISTA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_I7 ON TASY.CIRURGIA
(NR_ATENDIMENTO, IE_STATUS_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_MEDICO_FK_I ON TASY.CIRURGIA
(CD_MEDICO_CIRURGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_MOINTCI_FK_I ON TASY.CIRURGIA
(NR_SEQ_INTERRUPCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_MOINTCI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_MOTCIRU_FK_I ON TASY.CIRURGIA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_MOTCIRU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_MOTREOP_FK_I ON TASY.CIRURGIA
(NR_SEQ_MOTIVO_REOP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_MOTREOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_NIPROCE_FK_I ON TASY.CIRURGIA
(NR_SEQ_PROCED_NISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_NIPROCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_NISSIES_FK_I ON TASY.CIRURGIA
(NR_SEQ_SITIO_ESPEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_NISSIES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_NISSIPR_FK_I ON TASY.CIRURGIA
(NR_SEQ_SITIO_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_NISSIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_PEPOCIR_FK_I ON TASY.CIRURGIA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_PESFISI_FK_I ON TASY.CIRURGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_PESFISI_FK2_I ON TASY.CIRURGIA
(CD_MEDICO_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRURGI_PK ON TASY.CIRURGIA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_PRESMED_FK2_I ON TASY.CIRURGIA
(NR_PRESCRICAO_ESPEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_PROCEDI_FK_I ON TASY.CIRURGIA
(CD_PROCEDIMENTO_PRINC, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_PROINTE_FK_I ON TASY.CIRURGIA
(NR_SEQ_PROC_INTERNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_SETATEND_FK_I ON TASY.CIRURGIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_TXRECE_FK_I ON TASY.CIRURGIA
(NR_SEQ_TRANSPLANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_TXRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRURGI_UK ON TASY.CIRURGIA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRURGI_VIAACEC_FK_I ON TASY.CIRURGIA
(NR_SEQ_VIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRURGI_VIAACEC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_tp  after update ON TASY.CIRURGIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_CIRURGIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MIN_DURACAO_REAL,1,4000),substr(:new.NR_MIN_DURACAO_REAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_MIN_DURACAO_REAL',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTE,1,4000),substr(:new.IE_PORTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTE',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MIN_DURACAO_PREV,1,4000),substr(:new.NR_MIN_DURACAO_PREV,1,4000),:new.nm_usuario,nr_seq_w,'NR_MIN_DURACAO_PREV',ie_log_w,ds_w,'CIRURGIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Cirurgia_insert
BEFORE INSERT ON TASY.CIRURGIA FOR EACH ROW
declare

ie_gera_proc_w			varchar2(1);
ie_gera_cir_pepo_w		varchar2(1);
ie_consiste_medico_w		varchar2(1);
ie_permissao_atend_w		varchar2(1);
nr_sequencia_w			number(10,0);
ie_status_cirurgia_w		varchar2(3);
ie_vincula_cir_processo_w	varchar2(1);
ds_origem_w			varchar2(2000);
dt_cancelamento_w   		date;

BEGIN

obter_param_usuario(901, 112, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_proc_w);
obter_param_usuario(872, 158, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_cir_pepo_w);


select	nvl(max(ie_agrupar_cir_dia),'N')
into	ie_vincula_cir_processo_w
from	parametros_pepo
where	cd_estabelecimento	= :new.cd_estabelecimento;



Consistir_impedimento_pf(:new.cd_medico_cirurgiao,'CIR',:new.nm_usuario);
if	(:new.cd_medico_anestesista is not null) then
	Consistir_impedimento_pf(:new.cd_medico_anestesista,'ANES',:new.nm_usuario);
end if;

if (ie_consiste_medico_w = 'S') then
	select	obter_se_assumir_paciente(:new.cd_medico_cirurgiao, :old.cd_estabelecimento)
	into	ie_permissao_atend_w
	from	dual;

	if	(ie_permissao_atend_w = 'N') then
		--Medico nao possui autorizacao para assumir o paciente!
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(233793);
	end if;
end if;


if	(:new.ie_infeccao is null) then
	:new.ie_infeccao	:= 'N';
end if;


if	((ie_gera_proc_w = 'S') and (:new.nr_prescricao is not null) and (:new.dt_entrada_unidade is not null) and (:new.nr_atendimento is not null))  then
	gravar_prescr_proc_cir(:new.nr_prescricao,:new.cd_procedimento_princ,:new.ie_origem_proced,:new.dt_entrada_unidade,
	:new.nr_atendimento, :new.nr_seq_proc_interno, :new.nm_usuario);
end if;

if	(:new.qt_peso is null) and (:new.nr_atendimento is not null)  then
	:new.qt_peso := obter_sinal_vital(:new.nr_atendimento,'Peso');
end if;

if	(ie_gera_cir_pepo_w = 'S') and
	(:new.nr_seq_pepo is null) then

	Select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	pepo_cirurgia
	where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_cirurgia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_inicio_prevista)
	and	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	select	nvl(max(ie_status_cirurgia),0)
	into	ie_status_cirurgia_w
	from	cirurgia
	where	nr_seq_pepo = nr_sequencia_w;


	if 	(ie_vincula_cir_processo_w = 'S') and  (nr_sequencia_w > 0) then
		nr_sequencia_w := nr_sequencia_w;

	elsif	(nr_sequencia_w = 0 ) or
		((nr_sequencia_w > 0) and (ie_status_cirurgia_w = 2)) then
			select	pepo_cirurgia_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into pepo_cirurgia(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				nr_atendimento,
				dt_cirurgia,
				cd_medico_anestesista,
				ie_carater_cirurgia,
				ie_asa_estado_paciente,
				ie_tipo_cirurgia,
				ie_tipo_procedimento,
            cd_estabelecimento)
			values(
				nr_sequencia_w,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.cd_pessoa_fisica,
				:new.nr_atendimento,
				:new.dt_inicio_prevista,
				:new.cd_medico_anestesista,
				:new.ie_carater_cirurgia,
				:new.ie_asa_estado_paciente,
				:new.ie_tipo_cirurgia,
				'C',
            :new.cd_estabelecimento);

	end if;

	:new.nr_seq_pepo := nr_sequencia_w;
end if;

--OS 975521 - Nao permitir vincular atendimentos cancelados na cirurgia e no agendamento.
if (((:old.nr_atendimento is null) and (:new.nr_atendimento is not null)) or
	((:old.nr_atendimento is not null) and (:new.nr_atendimento is not null) and
	(:old.nr_atendimento <> :new.nr_atendimento))) then

	select dt_cancelamento
	into   dt_cancelamento_w
	from   atendimento_paciente
	where  nr_atendimento = :new.nr_atendimento;

	if (dt_cancelamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(384733, 'NR_ATENDIMENTO_P='||to_char(:new.nr_atendimento)); --Nao sera possivel vincular o atendimento #@nr_atendimento_p#@! Este atendimento esta cancelado.
	end if;
end if;

ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

END;
/


CREATE OR REPLACE TRIGGER TASY.cirurgia_delete
before delete ON TASY.CIRURGIA FOR EACH ROW
DECLARE

ds_log_w	varchar2(4000);

begin

--Tratar na OS 569728
ds_log_w := '';

end;
/


CREATE OR REPLACE TRIGGER TASY.Cirurgia_Update
BEFORE UPDATE ON TASY.CIRURGIA FOR EACH ROW
DECLARE

dt_entrada_w			date;
dt_inicio_vigencia_w		date := NULL;
cd_agenda_w			number(10,0);
dt_agenda_w			date;
hr_inicio_w			date;
hr_atual_w			date;
qt_agenda_w			number(10,0);
cd_pessoa_prescr_w		varchar2(10);
nr_seq_proced_niss_w		number(10,0);
ie_gera_proc_w			varchar2(1);
ie_gera_proc_gest_w		varchar2(1);
ie_altera_status_agenda_w		varchar2(1);

ie_inicio_proc_w			varchar2(15);
ie_fim_proc_w			varchar2(15);
dt_inicio_w			date;
dt_fim_w				date;
qt_min_cirurgia_w			number(10,3);
qt_reg_w				number(1);
cd_pessoa_fisica_w		varchar2(10);

ie_consiste_medico_w		varchar2(1);
ie_permissao_atend_w		varchar2(1);
ie_vigencia_w			varchar2(1);
cd_pessoa_agenda_w		varchar2(10);
cd_classif_setor_w		number(10);
ie_consiste_setor_centro_cir_w  varchar2(1);
ds_origem_w			varchar(1800);
ie_grava_log_w			varchar2(1);
ds_log_w			varchar2(15);
ie_status_agend_futuro_w	number(10,0);
ie_perm_alter_proc_princ_w	varchar2(1) := 'S';
ie_durac_real_interromper_w	varchar2(1);
cd_edicao_amb_w                 number(6,0);
vl_m2_filme_w                   number(15,4) := 0;
dt_cancelamento_w   	date;
cd_setor_cirurgia_w		cirurgia.cd_setor_atendimento%type;
qt_existe_regra_setor_w	number(10);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(284979, null, wheb_usuario_pck.get_nr_seq_idioma);--Cirurgia
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(296208, null, wheb_usuario_pck.get_nr_seq_idioma);--Prescricao
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(289496, null, wheb_usuario_pck.get_nr_seq_idioma);--Estabelecimento
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(295451, null, wheb_usuario_pck.get_nr_seq_idioma);--Perfil
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(290509, null, wheb_usuario_pck.get_nr_seq_idioma);--Funcao
expressao6_w	varchar2(255) := obter_desc_expressao_idioma(328225, null, wheb_usuario_pck.get_nr_seq_idioma);--Usuario
expressao7_w	varchar2(255) := obter_desc_expressao_idioma(320589, null, wheb_usuario_pck.get_nr_seq_idioma);--Interromper cirurgia
expressao8_w	varchar2(255) := obter_desc_expressao_idioma(321297, null, wheb_usuario_pck.get_nr_seq_idioma);--Desfazer interrupcao cirurgia
expressao9_w	varchar2(255) := obter_desc_expressao_idioma(496838, null, wheb_usuario_pck.get_nr_seq_idioma);--Desfazer cancelamento cirurgia
expressao10_w	varchar2(255) := obter_desc_expressao_idioma(497792, null, wheb_usuario_pck.get_nr_seq_idioma);--Geracao da prescricao na cirurgia
expressao11_w	varchar2(255) := obter_desc_expressao_idioma(608541, null, wheb_usuario_pck.get_nr_seq_idioma);--Alteracao da duracao real
expressao12_w	varchar2(255) := obter_desc_expressao_idioma(773956, null, wheb_usuario_pck.get_nr_seq_idioma);--Alteracao/Vinculo do atendimento


BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

ds_origem_w	:=	substr(dbms_utility.format_call_stack,1,1800);
ds_origem_w	:=	substr(ds_origem_w ||chr(13)||chr(10)||
			expressao1_w || ' ' ||:new.nr_cirurgia||chr(13)||chr(10)||
			expressao2_w || ' ' ||:new.nr_prescricao||chr(13)||chr(10)||
			expressao3_w || ' ' ||:new.cd_estabelecimento||chr(13)||chr(10)||
			expressao4_w || ' ' ||obter_perfil_ativo||chr(13)||chr(10)||
			expressao5_w || ' ' ||to_char(wheb_usuario_pck.get_cd_funcao)||chr(13)||chr(10)||
			expressao6_w || ' ' ||:new.nm_usuario,1,1800);


obter_param_usuario(871, 103, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_altera_status_agenda_w);
obter_param_usuario(901, 112, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_proc_w);
obter_param_usuario(900, 11, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_gera_proc_gest_w);

obter_param_usuario(900, 194, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_vigencia_w);

obter_param_usuario(7022, 4, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_inicio_proc_w);
obter_param_usuario(7022, 5, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_fim_proc_w);
obter_param_usuario(900, 193, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_consiste_medico_w);
obter_param_usuario(900, 350, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_consiste_setor_centro_cir_w);
obter_param_usuario(900, 405, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ds_log_w);
obter_param_usuario(10000,31, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_status_agend_futuro_w);

obter_param_usuario(900,507, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_perm_alter_proc_princ_w);
obter_param_usuario(900,514, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_durac_real_interromper_w);
obter_param_usuario(900,202, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_grava_log_w);

if (ie_consiste_medico_w = 'S') then
	select	obter_se_assumir_paciente(:new.cd_medico_cirurgiao, :old.cd_estabelecimento)
	into	ie_permissao_atend_w
	from	dual;

	if	(ie_permissao_atend_w = 'N') then
		Wheb_mensagem_pck.exibir_mensagem_abort(192176); --Medico nao possui autorizacao para assumir o paciente! RICHART
	end if;
end if;

if	((ie_inicio_proc_w is not null) and (ie_fim_proc_w is not null)) then

	begin
	select	max(a.dt_registro)
	into	dt_inicio_w
	from	evento_cirurgia b,
		evento_cirurgia_paciente a
	where	a.nr_seq_evento = b.nr_sequencia
	and	b.ie_etapa_cirurgia = campo_numerico(ie_inicio_proc_w)
	and	a.nr_cirurgia = :new.nr_cirurgia
	and	nvl(a.ie_situacao,'A') = 'A';
	exception
	when others then
		dt_inicio_w := null;
	end;

	begin
	select	max(a.dt_registro)
	into	dt_fim_w
	from	evento_cirurgia b,
			evento_cirurgia_paciente a
	where	a.nr_seq_evento = b.nr_sequencia
	and		b.ie_etapa_cirurgia = campo_numerico(ie_fim_proc_w)
	and		a.nr_cirurgia = :new.nr_cirurgia;
	exception
	when others then
		dt_fim_w := null;
	end;

	if	((dt_inicio_w is not null) and (dt_fim_w is not null)) then
		qt_min_cirurgia_w	:= Obter_Minutos_Espera(dt_inicio_w, dt_fim_w);
	else
		qt_min_cirurgia_w	:= :new.nr_min_duracao_real;
	end if;
else
	qt_min_cirurgia_w	:= :new.nr_min_duracao_real;
end if;

if	(:new.cd_medico_cirurgiao <> :old.cd_medico_cirurgiao) then
	Consistir_impedimento_pf(:new.cd_medico_cirurgiao,'CIR',:new.nm_usuario);
end if;

if	((:new.cd_medico_anestesista is not null) and (:old.cd_medico_anestesista is null)) or
	(:new.cd_medico_anestesista <> :old.cd_medico_anestesista) then
	Consistir_impedimento_pf(:new.cd_medico_anestesista,'CIR',:new.nm_usuario);
end if;

/* Ricardo - 13/04/2005 - Incluida a rotina abaixo para nao passar a cirurgia para realizada se nao tiver atendimento */
if	(:new.nr_atendimento is null) and
	(:new.ie_status_cirurgia = '2') then
	:new.ie_status_cirurgia	:= '1';
end if;

if	(nvl(:old.ie_status_cirurgia,'X') <> 'X') and (:new.ie_status_cirurgia = 4) then
	if	(:new.dt_interrupcao is null) then
		:new.dt_interrupcao := sysdate;
	end if;
	if	(ie_durac_real_interromper_w = 'S') then
		:new.nr_min_duracao_real := Obter_Min_Entre_Datas(:new.dt_inicio_real,:new.dt_interrupcao,null);
	end if;
elsif	(:old.ie_status_cirurgia = 4) and
	(:old.ie_status_cirurgia <> :new.ie_status_cirurgia) then
	:new.dt_interrupcao 		:= null;
	if	(ie_durac_real_interromper_w = 'S') then
		:new.nr_min_duracao_real 	:= null;
	end if;
end if;

if	(ie_grava_log_w = 'S') then
	if	(:old.dt_interrupcao is null) and (:new.dt_interrupcao is not null) then
		gerar_cirurgia_hist(:new.nr_cirurgia,'ITC',wheb_usuario_pck.get_nm_usuario,expressao7_w,'N');
	elsif	(:old.dt_interrupcao is not null) and (:new.dt_interrupcao is null) then
		gerar_cirurgia_hist(:new.nr_cirurgia,'DTC',wheb_usuario_pck.get_nm_usuario,expressao8_w,'N');
	end if;
	if	(:old.ie_status_cirurgia = 3) and (:new.ie_status_cirurgia <> 3) then
		gerar_cirurgia_hist(:new.nr_cirurgia,'DCC',wheb_usuario_pck.get_nm_usuario,expressao9_w,'N');
	end if;
end if;

if	(:new.dt_termino is not null) and (:new.dt_inicio_real is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(192182); -- nao e possivel finalizar a cirurgia sem antes iniciar a mesma! RICHART
end if;

if	(:new.nr_atendimento is null) and
	(:old.dt_inicio_real is null) and (:new.dt_inicio_real is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(285968); -- nao e possivel iniciar a cirurgia sem informar o atendimento.
end if;

if	(:new.nr_atendimento is not null) then
	begin

	if	(:new.nr_prescricao is not null) then
		update prescr_medica
		set 	nr_atendimento = :new.nr_atendimento
		where 	nr_prescricao = :new.nr_prescricao;
	end if;

	select	dt_entrada
	into	dt_entrada_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade) and
		(:new.dt_entrada_unidade is not null) and
		(:new.dt_entrada_unidade < dt_entrada_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192177); -- A data de entrada da cirurgia nao pode ser menor que a data de entrada do paciente! RICHART
	elsif	(:new.dt_inicio_real <> :old.dt_inicio_real) and
		(:new.dt_inicio_real is not null) and
		(:new.dt_inicio_real < :new.dt_entrada_unidade) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192178); -- A data de inicio real nao pode ser menor que a data de entrada! RICHART
	elsif	(:new.dt_inicio_real <> :old.dt_inicio_real) and
		(:new.dt_inicio_real is not null) and
		(:new.dt_inicio_real < dt_entrada_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192179); --  A data de inicio da cirurgia nao pode ser menor que a data de entrada do paciente! RICHART
	elsif	(:new.dt_termino <> :old.dt_termino) and
		(:new.dt_termino is not null) and
		(:new.dt_termino < dt_entrada_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192180); -- A data de termino da cirurgia nao pode ser menor que a data de entrada do paciente! RICHART
	elsif	(:new.dt_termino < :new.dt_inicio_real) and
		(:new.dt_termino is not null) and
		(:new.dt_inicio_real is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192181);  -- A data de termino dever ser maior que a inicial! RICHART
	elsif	((:old.dt_interrupcao is null) and (:new.dt_interrupcao is not null)) and
		 (:new.dt_interrupcao < :new.dt_entrada_unidade) then
		Wheb_mensagem_pck.exibir_mensagem_abort(236042);  --A data de interrupcao da cirurgia nao pode ser menor que a data de entrada do paciente!
	elsif	((:old.dt_interrupcao is null) and (:new.dt_interrupcao is not null)) and
		 (:new.dt_interrupcao < :new.dt_inicio_real) then
		Wheb_mensagem_pck.exibir_mensagem_abort(236043);  --A data de interrupcao da cirurgia nao pode ser menor que a data de inicio da cirurgia!
	end if;

	/* os13448 em 15/12/2004 Marcus Inclui o MIN() */
	select	MIN(a.dt_inicio_vigencia)
	into	dt_inicio_vigencia_w
	from	atend_categoria_convenio a
	where	a.nr_atendimento	= :new.nr_atendimento
	and	a.nr_seq_interno	=
			(select	max(b.nr_seq_interno)
			from		atend_categoria_convenio b
			where		b.nr_atendimento	= :new.nr_atendimento);

	if	(dt_inicio_vigencia_w is not null) and (ie_vigencia_w = 'S') then
		begin
		if	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade) and
			(:new.dt_entrada_unidade is not null) and
			(:new.dt_entrada_unidade < dt_inicio_vigencia_w) then
			Wheb_mensagem_pck.exibir_mensagem_abort(192184); -- A data de entrada da cirurgia nao pode ser menor que a data de inicio da vigencia! RICHART
		elsif	(:new.dt_inicio_real <> :old.dt_inicio_real) and
			(:new.dt_inicio_real is not null) and
			(:new.dt_inicio_real < dt_inicio_vigencia_w) then
			Wheb_mensagem_pck.exibir_mensagem_abort(192185); -- A data de inicio da cirurgia nao pode ser menor que a data de inicio da vigencia! RICHART
		elsif	(:new.dt_termino <> :old.dt_termino) and
			(:new.dt_termino is not null) and
			(:new.dt_termino < dt_inicio_vigencia_w) then
			Wheb_mensagem_pck.exibir_mensagem_abort(192186); -- A data de termino da cirurgia nao pode ser menor que a data de inicio da vigencia! RICHART
		end if;
		end;
	end if;

	/* Ivan em 25/07/2007 OS63637 */
	if	((:old.dt_termino is null) or (:old.dt_termino <> :new.dt_termino)) and
		(:new.dt_termino is not null) and
		(:new.dt_termino > :new.dt_inicio_real) then
		begin

		update	atend_paciente_unidade
		set	dt_saida_unidade	= :new.dt_termino,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where 	nr_atendimento		= :new.nr_atendimento
		and	dt_entrada_unidade	= :new.dt_entrada_unidade;

		end;
	end if;

	end;
end if;

/* Tirei esta linha: 	(:new.dt_termino_prevista is null) */
/* e adicionei (:new.nr_min_duracao_prev <> :old.nr_min_duracao_prev) no if abaixo. Dalcastagne em 31/08/2009 OS 163672 */
if	(:new.dt_inicio_real is not null) and
	(:new.nr_min_duracao_prev is not null) then
	:new.dt_termino_prevista	:= :new.dt_inicio_real + (:new.nr_min_duracao_prev / 1440);
end if;


if	(nvl(:new.ie_status_cirurgia,'X') <> nvl(:old.ie_status_cirurgia,'X')) and
	((ie_altera_status_agenda_w = 'S') and (:new.ie_status_cirurgia = '2')) then
	begin

	select	nvl(max(cd_agenda),0),
		max(dt_agenda),
		max(hr_inicio),
		max(cd_pessoa_fisica)
	into	cd_agenda_w,
		dt_agenda_w,
		hr_inicio_w,
		cd_pessoa_agenda_w
	from	agenda_paciente
	where	nr_cirurgia		= :new.nr_cirurgia;

	select	count(*)
	into	qt_agenda_w
	from	agenda_paciente
	where	cd_agenda		= cd_agenda_w
	and	dt_agenda		= dt_agenda_w
	and	hr_inicio		= hr_inicio_w;

	if	(qt_agenda_w = 1) then
		update 	agenda_paciente
		set	ie_status_agenda	= 'E'
		where	nr_cirurgia		= :new.nr_cirurgia;
	end if;

	end;
end if;

if	(:new.nr_prescricao is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_prescr_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;

	if	(:new.cd_pessoa_fisica <> cd_pessoa_prescr_w) and
		(:new.dt_entrada_unidade is not null) and
		(:new.cd_pessoa_fisica = :old.cd_pessoa_fisica) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192187); --A pessoa fisica da cirurgia nao e a mesma da prescricao! Entre em contato com o setor de informatica! RICHART
	end if;
end if;

if	(:new.nr_seq_proc_interno is not null) then

	select	nvl(max(nr_seq_proced_niss),0)
	into	nr_seq_proced_niss_w
	from	proc_interno
	where	nr_sequencia	=	:new.nr_seq_proc_interno;

	if	(nr_seq_proced_niss_w > 0) then
		:new.nr_seq_proced_niss	:= nr_seq_proced_niss_w;
	end if;

end if;

if	((:new.nr_seq_proc_interno is not null) or (:new.nr_seq_proced_niss is not null)) and
	(:new.nr_min_duracao_real is not null) and
	(:new.cd_tipo_cirurgia is not null) and
	(:new.ie_asa_estado_paciente is not null) then
	:new.qt_ric	:= obter_iric(	:new.ie_asa_estado_paciente,:new.cd_tipo_cirurgia,:new.nr_seq_proc_interno,qt_min_cirurgia_w,
					:new.nr_seq_proced_niss);
end if;


if	(:new.ie_infeccao is not null) then
	update	cih_cirurgia
	set	ie_infeccao		=	:new.ie_infeccao
	where	nr_seq_cirurgia_pac	=	:new.nr_cirurgia;

end if;


if	(((ie_gera_proc_w = 'S' ) or (ie_gera_proc_gest_w = 'S')) and (:new.nr_prescricao is not null) and (:new.dt_entrada_unidade is not null) and (:new.nr_atendimento is not null))  then
	gravar_prescr_proc_cir(:new.nr_prescricao,:new.cd_procedimento_princ,:new.ie_origem_proced,:new.dt_entrada_unidade,
				:new.nr_atendimento, :new.nr_seq_proc_interno, :new.nm_usuario);
end if;

if	(:new.qt_peso is null) and (:new.nr_atendimento is not null) and
	(:new.dt_termino is null) then
	:new.qt_peso := obter_sinal_vital(:new.nr_atendimento,'Peso');
end if;

if	(:old.nr_prescricao <> :new.nr_prescricao) then
	gerar_cirurgia_hist(:new.nr_cirurgia,'GP',wheb_usuario_pck.get_nm_usuario,expressao10_w,'N');
end if;

if	(:old.cd_procedimento_princ <> :new.cd_procedimento_princ) or
	(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno) or
	(:old.ie_origem_proced <> :new.ie_origem_proced) then
	insert into log_alter_proc_cirurgia(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_atendimento,
		nr_cirurgia,
		nr_prescricao,
		nr_seq_proc,
		nr_seq_proc_interno,
		cd_procedimento,
		ie_origem_proced)
	values (
		log_alter_proc_cirurgia_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_atendimento,
		:new.nr_cirurgia,
		:new.nr_prescricao,
		null,
		:old.nr_seq_proc_interno,
		:old.cd_procedimento_princ,
		:old.ie_origem_proced);
end if;

if	(:new.cd_pessoa_fisica is not null) and
	(cd_pessoa_agenda_w is not null) and
	(:new.cd_pessoa_fisica <> cd_pessoa_agenda_w) then
	Wheb_mensagem_pck.exibir_mensagem_abort(192188); -- As pessoas da cirurgia e do agendamento sao diferentes. Favor verificar. RICHART
end if;

if	(:new.nr_prescricao is not null) then
	Select	nvl(max(cd_pessoa_fisica),:new.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;

	if	(:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(192189, 'NR_CIRURGIA_P='||to_char(:new.nr_cirurgia)); --O paciente da cirurgia ' || to_char(:new.nr_cirurgia) || ' nao e o mesmo da prescricao RICHART
	end if;
end if;

-- OS 299680 - Criado parametro para impedir realizar a alteracao na data de entrada, pois a mesma ira pegar outra movimentacao que nao pertence ao centro cirurgico.
if	(ie_consiste_setor_centro_cir_w = 'S') and
	(:new.nr_atendimento is not null) and
	(:new.dt_entrada_unidade is not null) and
	(:old.dt_entrada_unidade <> :new.dt_entrada_unidade) then
	select	max(obter_classif_setor(cd_setor_atendimento))
	into	cd_classif_setor_w
	from	atend_paciente_unidade
	where	nr_atendimento	 	 = :new.nr_atendimento
	and	dt_entrada_unidade	 = :new.dt_entrada_unidade;
	if	(cd_classif_setor_w <> 2) then
		 Wheb_mensagem_pck.exibir_mensagem_abort(192190); --nao e possivel alterar o setor da cirurgia! O setor nao pertence ao centro cirirgico.'||chr(13)|| 'Parametro [350] da funcao Gesteo de cirurgias RICHART
	end if;
end if;

if 	((ie_perm_alter_proc_princ_w = 'N') and
	(:new.dt_termino is not null) and
	((:old.cd_procedimento_princ <> :new.cd_procedimento_princ) or
	(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno) or
	(:old.ie_origem_proced <> :new.ie_origem_proced))) then
	Wheb_mensagem_pck.exibir_mensagem_abort(231316); -- nao e possivel alterar o procedimento principal de uma cirurgia finalizada. Parametro [507].
end if;

-- RICHART - Coloquei a rotina abaixo pois na O.S. 419404, o cliente solicitou que fosse alterado o status do atendimento futuro assim que fosse informado a data de fim da cirurgia.

if 	(nvl(ie_status_agend_futuro_w,0) > 0) and
	(((:old.dt_fim_cirurgia is null) and
	(:new.dt_fim_cirurgia is not null)) or
	(:old.dt_termino is null) and
	(:new.dt_termino is not null)) then
	atualiza_status_agend_futuro(:new.nr_cirurgia, :new.nm_usuario);
end if;

if	(nvl(:old.nr_min_duracao_real,0) <> nvl(:new.nr_min_duracao_real,0)) then
	gerar_cirurgia_hist(:new.nr_cirurgia,'ADR',wheb_usuario_pck.get_nm_usuario,expressao11_w,'N');
end if;

if	(nvl(:old.nr_atendimento,0) <> nvl(:new.nr_atendimento,0)) then
	gerar_cirurgia_hist(:new.nr_cirurgia,'VA',wheb_usuario_pck.get_nm_usuario,expressao12_w,'N');
end if;

if      (:new.nr_seq_proc_interno is not null) then

	obter_edicao_proc(	:new.cd_estabelecimento,
				:new.cd_convenio,
				:new.cd_categoria,
				sysdate,
				:new.cd_procedimento_princ,
				cd_edicao_amb_w,
				vl_m2_filme_w);

	:new.cd_procedimento_tuss       :=      define_procedimento_tuss(:new.cd_estabelecimento,
								   :new.nr_seq_proc_interno,
								   :new.cd_convenio,
								   :new.cd_categoria,
								   obter_tipo_atendimento(:new.nr_atendimento),
								   sysdate,
								   :new.cd_procedimento_princ,
								   :new.ie_origem_proced,
								   cd_edicao_amb_w,
								   null,
								   null);
end if;


--OS 975521 - Nao permitir vincular atendimentos cancelados na cirurgia e no agendamento.
if (((:old.nr_atendimento is null) and (:new.nr_atendimento is not null)) or
	((:old.nr_atendimento is not null) and (:new.nr_atendimento is not null) and
	(:old.nr_atendimento <> :new.nr_atendimento))) then

	select dt_cancelamento
	into   dt_cancelamento_w
	from   atendimento_paciente
	where  nr_atendimento = :new.nr_atendimento;

	if (dt_cancelamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(384733, 'NR_ATENDIMENTO_P='||to_char(:new.nr_atendimento)); --nao sera possivel vincular o atendimento #@nr_atendimento_p#@! Este atendimento esta cancelado.
	end if;
end if;

gravar_log_tasy(5151, ':old.nr_atendimento='|| :old.nr_atendimento || ';new.nr_atendimento='|| :new.nr_atendimento, :new.nr_cirurgia);

if	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then

	begin
	select	1
	into	qt_existe_regra_setor_w
	from	dis_regra_setor
	where	cd_setor_atendimento = :new.cd_setor_atendimento
	and	rownum = 1;
	exception
	when others then
		qt_existe_regra_setor_w := 0;
	end;

	gravar_log_tasy(5151, ':old.nr_atendimento='|| :old.nr_atendimento || ';new.nr_atendimento='|| :new.nr_atendimento || ';cd_setor_cirurgia_w='|| cd_setor_cirurgia_w ||
		';qt_existe_regra_setor_w='||qt_existe_regra_setor_w , :new.nr_cirurgia);

	if	(qt_existe_regra_setor_w > 0) then
		select	max(dt_entrada)
		into	dt_entrada_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento;

		update	int_disp_movt_pac
		set	nr_atendimento = :new.nr_atendimento,
			dt_entrada = dt_entrada_w,
			dt_leitura = null
		where	nr_cirurgia = :new.nr_cirurgia;
	end if;
end if;


<<Final>>

qt_reg_w	:= 0;


END;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_ATUAL
before insert or update ON TASY.CIRURGIA 
for each row
declare

ie_clinica_w			atendimento_paciente.ie_clinica%type;

begin
	if	(nvl(:old.DT_INICIO_PREVISTA,sysdate+10) <> :new.DT_INICIO_PREVISTA) and
		(:new.DT_INICIO_PREVISTA is not null) then
		:new.ds_utc		:= obter_data_utc(:new.DT_INICIO_PREVISTA, 'HV');	
	end if;

	if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
		(:new.DT_LIBERACAO is not null) then
		:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
	end if;
	
	if (updating) then
		if (nvl((pkg_i18n.get_user_locale), 'pt_BR') = 'es_MX') and 
			(:old.dt_fim_cirurgia is null) and 
			(:new.dt_fim_cirurgia is not null) then
		
			select 	max(ie_clinica)
			into 	ie_clinica_w
			from 	atendimento_paciente
			where nr_atendimento = :new.nr_atendimento;
			
			gerar_codificacao_atendimento(
				:new.nr_atendimento, 
				ie_clinica_w, 
				:new.nm_usuario, 
				null, 
				null, 
				:new.cd_procedimento_princ, 
				:new.ie_origem_proced, 
				'N',
				:new.nr_cirurgia);
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_cirurgia_medico_aftupd
AFTER UPDATE of cd_medico_cirurgiao ON TASY.CIRURGIA FOR EACH ROW
--DISABLE
DECLARE


cd_pessoa_fisica_w          pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w            atendimento_paciente.nr_atendimento%type;

qt_reg_w number(10);
ds_sql_w varchar2(4000);

BEGIN


    if :old.cd_medico_cirurgiao != :new.cd_medico_cirurgiao then
        select  count(*)
        into qt_reg_w
        from CIRURGIA_DESCRICAO 
        where nr_cirurgia = :new.nr_cirurgia
        and cd_responsavel != :new.cd_medico_cirurgiao
        and dt_liberacao is null;
        
             
        if qt_reg_w > 0 then  
                     
            ds_sql_w:='update CIRURGIA_DESCRICAO set cd_responsavel = '||:new.cd_medico_cirurgiao||', nm_usuario = '''||obter_usuario_ativo||''', dt_atualizacao = sysdate
            where nr_cirurgia = '||:new.nr_cirurgia||'
            and cd_responsavel != '||:new.cd_medico_cirurgiao||'
            and dt_liberacao is null;commit;';

            hsj_executar_processo_paralelo(ds_sql_w);
                
        end if;        
    end if;
    
     
    

END hsj_cirurgia_medico_aftupd;
/


ALTER TABLE TASY.CIRURGIA ADD (
  CONSTRAINT CIRURGI_PK
 PRIMARY KEY
 (NR_CIRURGIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CIRURGI_UK
 UNIQUE (NR_PRESCRICAO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA ADD (
  CONSTRAINT CIRURGI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT CIRURGI_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_TRANSPLANTE) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_ATEPACU_FK 
 FOREIGN KEY (NR_ATENDIMENTO, DT_ENTRADA_UNIDADE) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_ATENDIMENTO,DT_ENTRADA_UNIDADE),
  CONSTRAINT CIRURGI_CIHTICI_FK 
 FOREIGN KEY (CD_TIPO_CIRURGIA) 
 REFERENCES TASY.CIH_TIPO_CIRURGIA (CD_TIPO_CIRURGIA),
  CONSTRAINT CIRURGI_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT CIRURGI_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_CIRURGIAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRURGI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRURGI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT CIRURGI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_PRINC, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT CIRURGI_SETATEND_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CIRURGI_ANESTES_FK 
 FOREIGN KEY (CD_MEDICO_ANESTESISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRURGI_CIRSTAT_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.CIRURGIA_STATUS (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_REQ) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRURGI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CIRURGI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_NIPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROCED_NISS) 
 REFERENCES TASY.NISS_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_MOINTCI_FK 
 FOREIGN KEY (NR_SEQ_INTERRUPCAO) 
 REFERENCES TASY.MOTIVO_INTERRUP_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_NISSIPR_FK 
 FOREIGN KEY (NR_SEQ_SITIO_PRINC) 
 REFERENCES TASY.NISS_SITIO_PRINCIPAL (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_NISSIES_FK 
 FOREIGN KEY (NR_SEQ_SITIO_ESPEC) 
 REFERENCES TASY.NISS_SITIO_ESPECIFICO (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA_SUPERIOR) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CIRURGI_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRURGI_VIAACEC_FK 
 FOREIGN KEY (NR_SEQ_VIA) 
 REFERENCES TASY.VIA_ACESSO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_MOTREOP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REOP) 
 REFERENCES TASY.MOTIVO_REOPERACAO (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_MOTCIRU_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT CIRURGI_PRESMED_FK2 
 FOREIGN KEY (NR_PRESCRICAO_ESPEC) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO));

GRANT SELECT ON TASY.CIRURGIA TO NIVEL_1;

