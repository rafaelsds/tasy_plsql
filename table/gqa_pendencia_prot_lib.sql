ALTER TABLE TASY.GQA_PENDENCIA_PROT_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GQA_PENDENCIA_PROT_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.GQA_PENDENCIA_PROT_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_ESPECIALIDADE      NUMBER(5),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_PERFIL             NUMBER(5),
  NR_SEQ_PROTOCOLO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GQPEPRL_ESPMEDI_FK_I ON TASY.GQA_PENDENCIA_PROT_LIB
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQPEPRL_ESTABEL_FK_I ON TASY.GQA_PENDENCIA_PROT_LIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQPEPRL_PERFIL_FK_I ON TASY.GQA_PENDENCIA_PROT_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GQPEPRL_PK ON TASY.GQA_PENDENCIA_PROT_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQPEPRL_QGAPEND_FK_I ON TASY.GQA_PENDENCIA_PROT_LIB
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQPEPRL_SETATEN_FK_I ON TASY.GQA_PENDENCIA_PROT_LIB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GQA_PENDENCIA_PROT_LIB ADD (
  CONSTRAINT GQPEPRL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GQA_PENDENCIA_PROT_LIB ADD (
  CONSTRAINT GQPEPRL_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT GQPEPRL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT GQPEPRL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT GQPEPRL_QGAPEND_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.GQA_PENDENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT GQPEPRL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.GQA_PENDENCIA_PROT_LIB TO NIVEL_1;

