ALTER TABLE TASY.ENFERM_PERFIL_PRINC_VIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ENFERM_PERFIL_PRINC_VIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENFERM_PERFIL_PRINC_VIDA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_NECES_RELIG             VARCHAR2(1 BYTE),
  IE_INFLUENCIA              VARCHAR2(1 BYTE),
  IE_SUPORTE                 VARCHAR2(1 BYTE),
  IE_OUTRO_SIGNIF            VARCHAR2(1 BYTE),
  IE_CUSTO                   VARCHAR2(1 BYTE),
  IE_OUTROS                  VARCHAR2(1 BYTE),
  DS_OUTRAS_INFLUENCIAS      VARCHAR2(255 BYTE),
  OBSERVACAO_INFLUENCIA      VARCHAR2(255 BYTE),
  OBSERVACAO_IMPORTANCIA     VARCHAR2(255 BYTE),
  IE_AUTO_TERAPIA            VARCHAR2(1 BYTE),
  DS_AUTO_TERAPIA            VARCHAR2(255 BYTE),
  OBSERVACAO_AUTO_TERAPIA    VARCHAR2(255 BYTE),
  IE_HAB_CRENCA              VARCHAR2(1 BYTE),
  OBSERVACAO_HAB_CRENCA      VARCHAR2(255 BYTE),
  IE_COOP_FAMILIA            VARCHAR2(1 BYTE),
  DS_TRAT_FAMILIAR           VARCHAR2(255 BYTE),
  IE_CONT_CRENCA             VARCHAR2(1 BYTE),
  DS_CONT_CRENCA             VARCHAR2(255 BYTE),
  IE_ESP_VISIT_CONT          VARCHAR2(1 BYTE),
  IE_EQP_CRENCA              VARCHAR2(1 BYTE),
  OBSERVACAO_EQUIPE          VARCHAR2(255 BYTE),
  IE_RESTR_TERAP             VARCHAR2(1 BYTE),
  DS_RESTRICAO_TERAP         VARCHAR2(255 BYTE),
  IE_CONHECE_ESTILO          VARCHAR2(1 BYTE),
  DS_CONHECE_ESTILO          VARCHAR2(255 BYTE),
  DS_OUTROS                  VARCHAR2(255 BYTE),
  IE_PROBLEMA                VARCHAR2(1 BYTE),
  DT_RESUMO                  DATE,
  DS_RESUMO                  VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_AUTO_MEDICACAO          VARCHAR2(1 BYTE),
  DS_AUTO_MEDICACAO          VARCHAR2(255 BYTE),
  OBSERVACAO_AUTO_MEDICACAO  VARCHAR2(255 BYTE),
  NR_SEQ_CABECALHO           NUMBER(10),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENFPVID_ATEPACI_FK_I ON TASY.ENFERM_PERFIL_PRINC_VIDA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFPVID_CABECAL_FK_I ON TASY.ENFERM_PERFIL_PRINC_VIDA
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFPVID_ESTABEL_FK_I ON TASY.ENFERM_PERFIL_PRINC_VIDA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFPVID_PESFISI_FK_I ON TASY.ENFERM_PERFIL_PRINC_VIDA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ENFPVID_PK ON TASY.ENFERM_PERFIL_PRINC_VIDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ENFERM_PERFIL_PRINC_VIDA_tp  after update ON TASY.ENFERM_PERFIL_PRINC_VIDA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CABECALHO,1,4000),substr(:new.NR_SEQ_CABECALHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CABECALHO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_RESUMO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_RESUMO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_RESUMO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_AUTO_MEDICACAO,1,4000),substr(:new.DS_AUTO_MEDICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_AUTO_MEDICACAO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.OBSERVACAO_AUTO_MEDICACAO,1,4000),substr(:new.OBSERVACAO_AUTO_MEDICACAO,1,4000),:new.nm_usuario,nr_seq_w,'OBSERVACAO_AUTO_MEDICACAO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NECES_RELIG,1,4000),substr(:new.IE_NECES_RELIG,1,4000),:new.nm_usuario,nr_seq_w,'IE_NECES_RELIG',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INFLUENCIA,1,4000),substr(:new.IE_INFLUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INFLUENCIA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SUPORTE,1,4000),substr(:new.IE_SUPORTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SUPORTE',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OUTRO_SIGNIF,1,4000),substr(:new.IE_OUTRO_SIGNIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_OUTRO_SIGNIF',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO,1,4000),substr(:new.IE_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OUTROS,1,4000),substr(:new.IE_OUTROS,1,4000),:new.nm_usuario,nr_seq_w,'IE_OUTROS',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OUTRAS_INFLUENCIAS,1,4000),substr(:new.DS_OUTRAS_INFLUENCIAS,1,4000),:new.nm_usuario,nr_seq_w,'DS_OUTRAS_INFLUENCIAS',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.OBSERVACAO_INFLUENCIA,1,4000),substr(:new.OBSERVACAO_INFLUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'OBSERVACAO_INFLUENCIA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.OBSERVACAO_IMPORTANCIA,1,4000),substr(:new.OBSERVACAO_IMPORTANCIA,1,4000),:new.nm_usuario,nr_seq_w,'OBSERVACAO_IMPORTANCIA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_HAB_CRENCA,1,4000),substr(:new.IE_HAB_CRENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_HAB_CRENCA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.OBSERVACAO_HAB_CRENCA,1,4000),substr(:new.OBSERVACAO_HAB_CRENCA,1,4000),:new.nm_usuario,nr_seq_w,'OBSERVACAO_HAB_CRENCA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COOP_FAMILIA,1,4000),substr(:new.IE_COOP_FAMILIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_COOP_FAMILIA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TRAT_FAMILIAR,1,4000),substr(:new.DS_TRAT_FAMILIAR,1,4000),:new.nm_usuario,nr_seq_w,'DS_TRAT_FAMILIAR',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONT_CRENCA,1,4000),substr(:new.IE_CONT_CRENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONT_CRENCA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONT_CRENCA,1,4000),substr(:new.DS_CONT_CRENCA,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONT_CRENCA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESP_VISIT_CONT,1,4000),substr(:new.IE_ESP_VISIT_CONT,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESP_VISIT_CONT',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EQP_CRENCA,1,4000),substr(:new.IE_EQP_CRENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EQP_CRENCA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.OBSERVACAO_EQUIPE,1,4000),substr(:new.OBSERVACAO_EQUIPE,1,4000),:new.nm_usuario,nr_seq_w,'OBSERVACAO_EQUIPE',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTR_TERAP,1,4000),substr(:new.IE_RESTR_TERAP,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTR_TERAP',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RESTRICAO_TERAP,1,4000),substr(:new.DS_RESTRICAO_TERAP,1,4000),:new.nm_usuario,nr_seq_w,'DS_RESTRICAO_TERAP',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONHECE_ESTILO,1,4000),substr(:new.IE_CONHECE_ESTILO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONHECE_ESTILO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONHECE_ESTILO,1,4000),substr(:new.DS_CONHECE_ESTILO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONHECE_ESTILO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OUTROS,1,4000),substr(:new.DS_OUTROS,1,4000),:new.nm_usuario,nr_seq_w,'DS_OUTROS',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROBLEMA,1,4000),substr(:new.IE_PROBLEMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROBLEMA',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RESUMO,1,4000),substr(:new.DS_RESUMO,1,4000),:new.nm_usuario,nr_seq_w,'DS_RESUMO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTO_MEDICACAO,1,4000),substr(:new.IE_AUTO_MEDICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTO_MEDICACAO',ie_log_w,ds_w,'ENFERM_PERFIL_PRINC_VIDA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ENFERM_PERFIL_PRINC_VIDA ADD (
  CONSTRAINT ENFPVID_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ENFERM_PERFIL_PRINC_VIDA ADD (
  CONSTRAINT ENFPVID_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ENFPVID_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ENFPVID_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENFPVID_CABECAL_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.ENFERM_PERFIL_CABECALHO (NR_SEQUENCIA));

