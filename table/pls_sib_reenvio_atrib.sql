ALTER TABLE TASY.PLS_SIB_REENVIO_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SIB_REENVIO_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SIB_REENVIO_ATRIB
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ATRIBUTO                 NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_REENVIO              NUMBER(10),
  DT_CANCELAMENTO             DATE,
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  DT_REATIVACAO               DATE,
  CD_BENEFICIARIO             VARCHAR2(30 BYTE),
  NR_PLANO_ANS                VARCHAR2(20 BYTE),
  NR_PLANO_OPERADORA          VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSSBRA_PK ON TASY.PLS_SIB_REENVIO_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSBRA_PLSMOCA_FK_I ON TASY.PLS_SIB_REENVIO_ATRIB
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSBRA_PLSSBRV_FK_I ON TASY.PLS_SIB_REENVIO_ATRIB
(NR_SEQ_REENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_sib_reenvio_atrib_atual
before insert or update ON TASY.PLS_SIB_REENVIO_ATRIB for each row
declare

begin

if	(:new.dt_cancelamento is not null or :new.nr_seq_motivo_cancelamento is not null) and
	(:new.cd_atributo not in (31,32)) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146441); --Data e motivo de cancelamento nao podem ser informadas para o atributo selecionado
elsif	(:new.dt_reativacao is not null) and
	(:new.cd_atributo <> 20) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146442); --Data de reativacao nao pode ser informada para o atributo selecionado
elsif	(:new.cd_beneficiario is not null) and
	(:new.cd_atributo <> 8) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146530); --Codigo do beneficiario nao pode ser informado para o atributo selecionado
elsif	(:new.nr_plano_ans is not null) and
	(:new.cd_atributo <> 9) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146533); --Protocolo ANS nao pode ser informado para o atributo selecionado
elsif	(:new.nr_plano_operadora is not null) and
	(:new.cd_atributo <> 10) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146534); --Codigo SCPA nao pode ser informado para o atributo selecionado
elsif	(:new.dt_cancelamento is not null and :new.nr_seq_motivo_cancelamento is null) or
	(:new.dt_cancelamento is null and :new.nr_seq_motivo_cancelamento is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1146443); --E necessario informar data e motivo de cancelamento, ao preencher uma das informacoes
end if;

end;
/


ALTER TABLE TASY.PLS_SIB_REENVIO_ATRIB ADD (
  CONSTRAINT PLSSBRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SIB_REENVIO_ATRIB ADD (
  CONSTRAINT PLSSBRA_PLSSBRV_FK 
 FOREIGN KEY (NR_SEQ_REENVIO) 
 REFERENCES TASY.PLS_SIB_REENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSSBRA_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SIB_REENVIO_ATRIB TO NIVEL_1;

