ALTER TABLE TASY.W_ESTATISTICA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ESTATISTICA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ESTATISTICA_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_CONVENIO           NUMBER(5),
  QT_PROCEDIMENTO       NUMBER(5),
  VL_PROCEDIMENTO       NUMBER(15,2),
  VL_MATERIAL           NUMBER(15,2),
  VL_TOTAL              NUMBER(15,2),
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WESTPRO_PK ON TASY.W_ESTATISTICA_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WESTPRO_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_ESTATISTICA_PROC ADD (
  CONSTRAINT WESTPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ESTATISTICA_PROC TO NIVEL_1;

