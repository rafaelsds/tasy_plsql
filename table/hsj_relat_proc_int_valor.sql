DROP TABLE TASY.HSJ_RELAT_PROC_INT_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_RELAT_PROC_INT_VALOR
(
  NM_USUARIO           VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  DS_PROCEDIMENTO      VARCHAR2(240 BYTE),
  VL_PROCEDIMENTO      NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_RELAT_PROC_INT_VALOR TO NIVEL_1;

