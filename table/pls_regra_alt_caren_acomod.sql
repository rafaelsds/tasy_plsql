ALTER TABLE TASY.PLS_REGRA_ALT_CAREN_ACOMOD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ALT_CAREN_ACOMOD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ALT_CAREN_ACOMOD
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_CARENCIA_ESPECIFICA_ACOMD  VARCHAR2(1 BYTE),
  IE_TIPO_ALTERACAO             VARCHAR2(1 BYTE),
  IE_ACAO_REGRA                 VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRACA_PK ON TASY.PLS_REGRA_ALT_CAREN_ACOMOD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRACA_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_ALT_CAREN_ACOMOD ADD (
  CONSTRAINT PLSRACA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_ALT_CAREN_ACOMOD TO NIVEL_1;

