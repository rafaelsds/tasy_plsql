ALTER TABLE TASY.ITEM_REQUISICAO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ITEM_REQUISICAO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ITEM_REQUISICAO_MATERIAL
(
  NR_REQUISICAO              NUMBER(10)         NOT NULL,
  NR_SEQUENCIA               NUMBER(5)          NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  QT_MATERIAL_REQUISITADA    NUMBER(13,4)       NOT NULL,
  QT_MATERIAL_ATENDIDA       NUMBER(13,4),
  VL_MATERIAL                NUMBER(15,2)       NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_UNIDADE_MEDIDA          VARCHAR2(30 BYTE),
  DT_ATENDIMENTO             DATE,
  CD_PESSOA_RECEBE           VARCHAR2(10 BYTE),
  CD_PESSOA_ATENDE           VARCHAR2(10 BYTE),
  IE_ACAO                    VARCHAR2(1 BYTE),
  CD_MOTIVO_BAIXA            NUMBER(10),
  QT_ESTOQUE                 NUMBER(13,4),
  CD_UNIDADE_MEDIDA_ESTOQUE  VARCHAR2(30 BYTE),
  CD_CONTA_CONTABIL          VARCHAR2(20 BYTE),
  CD_MATERIAL_REQ            NUMBER(6),
  NR_SEQ_LOTE_FORNEC         NUMBER(10),
  CD_CGC_FORNECEDOR          VARCHAR2(14 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_GERACAO                 VARCHAR2(1 BYTE),
  NR_SEQ_COR_EXEC            NUMBER(10),
  VL_PRECO_VENDA             NUMBER(15,2),
  NM_USUARIO_RETIRADA        VARCHAR2(15 BYTE),
  CD_MATERIAL_LIDO           NUMBER(6),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ETAPA_GPI           NUMBER(10),
  NR_DOCUMENTO_EXTERNO       NUMBER(10),
  NR_ITEM_DOCTO_EXTERNO      NUMBER(10),
  NM_USUARIO_APROV           VARCHAR2(15 BYTE),
  DT_APROVACAO               DATE,
  VL_UNIT_PREVISTO           NUMBER(17,4),
  DT_REPROVACAO              DATE,
  NR_SEQ_APROVACAO           NUMBER(10),
  IE_MSG_ESTOQUE_MAX         VARCHAR2(15 BYTE),
  QT_ESTOQUE_SUPEROU         NUMBER(13,4),
  CD_KIT_MATERIAL            NUMBER(5),
  DS_JUSTIFICATIVA_ATEND     VARCHAR2(255 BYTE),
  CD_PROCESSO_APROV          NUMBER(10),
  QT_SALDO_ESTOQUE           NUMBER(15,4),
  QT_SALDO_ESTOQUE_DEST      NUMBER(15,4),
  DT_DESDOBR_APROV           DATE,
  DT_DISPENSACAO             DATE,
  DT_RECEBIMENTO             DATE,
  NM_USUARIO_DISP            VARCHAR2(15 BYTE),
  NM_USUARIO_RECEB           VARCHAR2(15 BYTE),
  IE_MOTIVO_REPROVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_REPROV    VARCHAR2(255 BYTE),
  NR_SEQ_KIT_ESTOQUE         NUMBER(10),
  QT_MATERIAL_REQ_AUTO       NUMBER(13,4),
  NR_SEQ_PROC_APROV          NUMBER(10),
  NR_SEQ_NOTA_FISCAL         NUMBER(10),
  CD_BARRAS                  VARCHAR2(4000 BYTE),
  NR_SEQ_JUSTIFICATIVA       NUMBER(10),
  NR_ORDEM_COMPRA            NUMBER(10),
  NR_ITEM_OCI                NUMBER(5),
  NR_SEQ_OP_COMP_OPM         NUMBER(10),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ORC_ITEM_GPI        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          224M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ITEREMA_GPICRETA_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_GPIORIT_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_I2 ON TASY.ITEM_REQUISICAO_MATERIAL
(DT_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_I3 ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_MATERIAL, QT_MATERIAL_ATENDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_KITESTO_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_KIT_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_KITESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREMA_KITMATE_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1584K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_KITMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREMA_MATERIA_FK2_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_MATERIAL_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_MATERIA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREMA_MATLOFO_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_MOJUREQ_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_NOTFISC_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_NOTFISC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREMA_ORCOITE_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_PESJURI_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREMA_PROCOMP_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_SEQ_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_SUMOBAR_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_MOTIVO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREMA_UNIMEDIEST_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_UNIDADE_MEDIDA_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          44M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREMA_UNIMEDIEST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREQM_CONCONT_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREQM_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEREQM_I1 ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_MOTIVO_BAIXA, NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREQM_MATERIA_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          54M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREQM_PESFISI_ATENDIDA_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_PESSOA_ATENDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREQM_PESFISI_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_PESSOA_RECEBE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ITEREQM_PK ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_REQUISICAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREQM_REQMATE_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          62M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEREQM_UNIMEDI_FK_I ON TASY.ITEM_REQUISICAO_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          35M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEREQM_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.item_requisicao_material_atual
after update or insert ON TASY.ITEM_REQUISICAO_MATERIAL for each row
declare
qt_existe_w			number(10);
ie_integra_w			varchar2(1) := 'N';
ds_enter_w			varchar2(3) := chr(13) || chr(10);
ds_origem_w			varchar2(2000);
ds_param_integ_hl7_w		varchar2(4000) := '';
ie_verificar_w			varchar2(1) := 'N';
cd_centro_custo_w		centro_custo.cd_centro_custo%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_local_estoque_destino_w	local_estoque.cd_local_estoque%type;
cd_operacao_estoque_w		operacao_estoque.cd_operacao_estoque%type;
dt_liberacao_w			requisicao_material.dt_liberacao%type;
cd_local_estoque_w		local_estoque.cd_local_estoque%type;
cd_pessoa_requisitante_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_origem_requisicao_w		requisicao_material.ie_origem_requisicao%type;

reg_integracao_p		gerar_int_padrao.reg_integracao;

ie_setor_athena_w				VARCHAR2(1) := 'N';
ie_setor_supply_w				VARCHAR2(1) := 'N';

begin


select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_req b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'RM'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A';


select	cd_estabelecimento,
	cd_local_estoque,
	cd_local_estoque_destino,
	cd_operacao_estoque,
	cd_centro_custo,
	ie_origem_requisicao,
	dt_liberacao,
	cd_pessoa_requisitante
into	cd_estabelecimento_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w,
	cd_operacao_estoque_w,
	cd_centro_custo_w,
	ie_origem_requisicao_w,
	dt_liberacao_w,
	cd_pessoa_requisitante_w
from	requisicao_material
where	nr_requisicao = :new.nr_requisicao;


begin
ds_origem_w	:= 	null;

if	(	(inserting) or
	(	(updating) and
		((nvl(:new.cd_motivo_baixa,0) <> nvl(:old.cd_motivo_baixa,0)) or
		(nvl(:new.qt_material_atendida,0) <> nvl(:old.qt_material_atendida,0)) or
		(:new.dt_atendimento is null and :old.dt_atendimento is not null) or
		(:new.dt_atendimento is not null and :old.dt_atendimento is null)))) then
	ie_verificar_w	:= 'S';
end if;

if	(ie_verificar_w = 'S') then
	begin
	if	(nvl(:new.cd_motivo_baixa,0) = 0) and (nvl(:new.qt_material_atendida,0) > 0) then
		ds_origem_w := substr(WHEB_MENSAGEM_PCK.get_texto(303094,
					'NR_SEQ_W='|| '1' ||
					';NR_REQUISICAO_W='|| :new.nr_requisicao ||
					';NR_SEQUENCIA_W='|| :new.nr_sequencia ||
					';NM_ESTACAO_W='|| substr(wheb_usuario_pck.get_nm_estacao,1,80) ||
					';DS_FUNCAO_W='|| nvl(obter_funcao_ativa,0) ||
					';DS_PERFIL_W='|| nvl(obter_perfil_ativo,0) ||
					';NM_USUARIO_W='|| obter_usuario_ativo ||
					';DS_RETORNO_W='|| substr(dbms_utility.format_call_stack,1,1800)),1,2000);

					/*NR_SEQ_W - NR_REQUISICAO_W - NR_SEQUENCIA_W
					Estacoo: NM_ESTACAO_W
					Funcoo: DS_FUNCAO_W
					Perfil: DS_PERFIL_W
					Usuario: NM_USUARIO_W

					DS_RETORNO_W*/

	elsif	(:new.dt_atendimento is not null) and (nvl(:new.cd_motivo_baixa,0) = 0) then
		ds_origem_w := substr(WHEB_MENSAGEM_PCK.get_texto(303094,
					'NR_SEQ_W='|| '2' ||
					';NR_REQUISICAO_W='|| :new.nr_requisicao ||
					';NR_SEQUENCIA_W='|| :new.nr_sequencia ||
					';NM_ESTACAO_W='|| substr(wheb_usuario_pck.get_nm_estacao,1,80) ||
					';DS_FUNCAO_W='|| nvl(obter_funcao_ativa,0) ||
					';DS_PERFIL_W='|| nvl(obter_perfil_ativo,0) ||
					';NM_USUARIO_W='|| obter_usuario_ativo ||
					';DS_RETORNO_W='|| substr(dbms_utility.format_call_stack,1,1800)),1,2000);
	elsif	(:new.cd_motivo_baixa is null) then
		ds_origem_w := substr(WHEB_MENSAGEM_PCK.get_texto(303094,
					'NR_SEQ_W='|| '3' ||
					';NR_REQUISICAO_W='|| :new.nr_requisicao ||
					';NR_SEQUENCIA_W='|| :new.nr_sequencia ||
					';NM_ESTACAO_W='|| substr(wheb_usuario_pck.get_nm_estacao,1,80) ||
					';DS_FUNCAO_W='|| nvl(obter_funcao_ativa,0) ||
					';DS_PERFIL_W='|| nvl(obter_perfil_ativo,0) ||
					';NM_USUARIO_W='|| obter_usuario_ativo ||
					';DS_RETORNO_W='|| substr(dbms_utility.format_call_stack,1,1800)),1,2000);
	end if;
	end;
end if;

if	(nvl(ds_origem_w,'X') <> 'X') then
	begin
	if	(inserting) then
		ds_origem_w := substr('Insert - ' || ds_origem_w,1,2000);
	else
		ds_origem_w := substr('Update - ' || ds_origem_w,1,2000);
	end if;

	insert into requisicao_mat_historico(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_requisicao,
		dt_historico,
		ds_titulo,
		ds_historico,
		ie_tipo,
		dt_liberacao,
		nm_usuario_lib,
		cd_evento)
	values (
		requisicao_mat_historico_seq.nextval,
		sysdate,
		:new.nm_usuario,
		:new.nr_requisicao,
		sysdate,
		Wheb_mensagem_pck.get_Texto(303098), /*'Historico da requisicao com motivo de baixa nula',*/
		ds_origem_w,
		'S',
		sysdate,
		:new.nm_usuario,
		'LB');
		--gravar_log_tasy(88929,ds_origem_w,:new.nm_usuario);
	end;
end if;

if	(nvl(:old.qt_estoque,0) > 0) and (:new.qt_estoque = 0) then
	begin

	if	(dt_liberacao_w is not null) then
		begin
		ds_origem_w := substr(WHEB_MENSAGEM_PCK.get_texto(303099,
					'SEQ_W='|| '3' ||
					';NR_REQUISICAO_W='|| :new.nr_requisicao ||
					';NR_SEQUENCIA_W='|| :new.nr_sequencia ||
					';CD_MOTIVO_BAIXA_W='|| :new.cd_motivo_baixa ||
					';DS_RETORNO_W='|| substr(dbms_utility.format_call_stack,1,1800)),1,2000);

					/*#EQ_W - NR_REQUISICAO_W - NR_SEQUENCIA_W - Motivo: CD_MOTIVO_BAIXA_W DS_RETORNO_W*/

		--gravar_log_tasy(88929,ds_origem_w,:new.nm_usuario);
		end;
	end if;
	end;
end if;

exception
when others then
	ds_origem_w := null;
end;

if	(qt_existe_w > 0) then
	begin


	if	(nvl(:old.qt_material_atendida,0) = 0) and
		(nvl(:new.qt_material_atendida,0) > 0) and
		(obter_tipo_motivo_baixa_req(nvl(:old.cd_motivo_baixa,0)) = 0) and
		(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)) > 0) then
		begin
		select	count(*)
		into	qt_existe_w
		from	sup_parametro_integracao a,
			sup_int_regra_req b
		where	a.nr_sequencia = b.nr_seq_integracao
		and	a.ie_evento = 'RM'
		and	a.ie_forma = 'E'
		and	a.ie_situacao = 'A'
		and	b.ie_atende = 'A';

		if	(qt_existe_w > 0) then
			ie_integra_w := substr(obter_se_integra_item_req(:new.nr_requisicao, cd_local_estoque_w, cd_local_estoque_destino_w, cd_operacao_estoque_w, :new.cd_material, 'A'),1,1);
		end if;
		end;
	elsif	(nvl(:new.qt_material_atendida,0) = 0) and
		(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)) = 0) and
		(:old.dt_aprovacao is null) and
		(:new.dt_aprovacao is not null) then
		begin
		select	count(*)
		into	qt_existe_w
		from	sup_parametro_integracao a,
			sup_int_regra_req b
		where	a.nr_sequencia = b.nr_seq_integracao
		and	a.ie_evento = 'RM'
		and	a.ie_forma = 'E'
		and	a.ie_situacao = 'A'
		and	b.ie_atende = 'L';

		if	(qt_existe_w > 0) then
			ie_integra_w := substr(obter_se_integra_item_req(:new.nr_requisicao, cd_local_estoque_w, cd_local_estoque_destino_w, cd_operacao_estoque_w, :new.cd_material, 'L'),1,1);
		end if;
		end;
	end if;
	if	(ie_integra_w = 'S') then
		sup_carrega_itens_atend_req(
			:new.nr_requisicao,
			:new.nr_Sequencia,
			:new.cd_unidade_medida,
			:new.cd_estabelecimento,
			:new.dt_atendimento,
			:new.cd_pessoa_recebe,
			:new.cd_pessoa_atende,
			:new.ie_acao,
			:new.cd_motivo_baixa,
			:new.qt_estoque,
			:new.cd_unidade_medida_estoque,
			:new.cd_conta_contabil,
			:new.cd_material,
			:new.cd_material_req,
			:new.nr_seq_lote_fornec,
			:new.cd_cgc_fornecedor,
			:new.qt_material_requisitada,
			:new.ds_observacao,
			:new.nm_usuario_retirada,
			:new.ds_justificativa,
			:new.ds_justificativa_atend,
			:new.qt_material_atendida,
			:new.cd_material_lido,
			:new.ie_geracao,
			:new.vl_material,
			:new.nr_documento_externo,
			:new.nr_item_docto_externo,
			:new.nm_usuario_aprov,
			:new.dt_aprovacao,
			:new.vl_unit_previsto,
			:new.dt_reprovacao,
			:new.nr_seq_cor_exec,
			:new.vl_preco_venda,
			:new.nr_seq_etapa_gpi,
			:new.ie_msg_estoque_max,
			:new.qt_estoque_superou,
			:new.nr_seq_aprovacao,
			:new.cd_kit_material,
			:new.nm_usuario);
	end if;
	end;
end if;

-- Swisslog / SupplyPoint / Athena
if	(updating) and
	(nvl(:old.cd_motivo_baixa,0) = 0) and
	(nvl(:new.cd_motivo_baixa,0) = 1) and
	(nvl(:new.qt_material_atendida,0) > 0) then
	begin
	select	count(1)
	into	qt_existe_w
	from	far_setores_integracao
	where	nr_seq_empresa_int = 76
	and	rownum = 1;

	if	(qt_existe_w > 0) then
		select	decode(nvl(max(1),0),1,'S','N')
		into	ie_integra_w
		from	parametros_farmacia
		where	cd_pessoa_requisicao = cd_pessoa_requisitante_w
		and	cd_estabelecimento = :new.cd_estabelecimento;

		if	(ie_integra_w = 'S') then
			ds_param_integ_hl7_w := 'nr_requisicao=' || :new.nr_requisicao || obter_separador_bv || 'nr_sequencia=' || :new.nr_sequencia || obter_separador_bv;
			swisslog_gerar_integracao(441, ds_param_integ_hl7_w);
		end if;
	end if;

	SELECT DECODE(COUNT(*), 0, 'N', 'S')
	INTO ie_setor_supply_w
	FROM far_setores_integracao
	WHERE nr_seq_empresa_int = 82
	AND ROWNUM = 1;

	SELECT DECODE(COUNT(*), 0, 'N', 'S')
	INTO ie_setor_athena_w
	FROM far_setores_integracao
	WHERE nr_seq_empresa_int = 221
	AND ROWNUM = 1;

	select	decode(nvl(max(1),0),1,'S','N')
	into	ie_integra_w
	from	parametros_farmacia
	where	cd_pessoa_requisicao = cd_pessoa_requisitante_w
	and	cd_estabelecimento = :new.cd_estabelecimento;

	if (ie_setor_supply_w = 'S' and ie_setor_athena_w = 'S')  then

		SELECT DECODE(COUNT(*), 0, 'N', 'S')
		INTO ie_setor_supply_w
		FROM empresa_integracao a,
			far_setores_integracao b,
			far_local_cc_int c
		WHERE b.nr_seq_empresa_int = 82
		AND  c.cd_local_estoque =  cd_local_estoque_destino_w
		AND  a.nr_sequencia = b.nr_seq_empresa_int
		AND  c.nr_seq_far_setores = b.nr_sequencia
		AND ROWNUM = 1;

		SELECT DECODE(COUNT(*), 0, 'N', 'S')
		INTO ie_setor_athena_w
		FROM empresa_integracao a,
			far_setores_integracao b,
			far_local_cc_int c
		WHERE b.nr_seq_empresa_int = 221
		AND  (c.cd_local_estoque =  cd_local_estoque_destino_w) or (c.cd_local_estoque =  cd_local_estoque_w)
		AND  a.nr_sequencia = b.nr_seq_empresa_int
		AND  c.nr_seq_far_setores = b.nr_sequencia
		AND ROWNUM = 1;

		if	(ie_integra_w = 'S') then
			ds_param_integ_hl7_w := 'nr_requisicao=' || :new.nr_requisicao || obter_separador_bv || 'nr_sequencia=' || :new.nr_sequencia || obter_separador_bv;

			if (ie_setor_supply_w = 'S') then
				gravar_agend_integracao(441, ds_param_integ_hl7_w);
			end if;

			if (ie_setor_athena_w = 'S') then
				gravar_agend_integracao(918, ds_param_integ_hl7_w);
			end if;
		end if;

	elsif (ie_setor_supply_w = 'S' or ie_setor_athena_w = 'S')  then

		if	(ie_integra_w = 'S') then
			ds_param_integ_hl7_w := 'nr_requisicao=' || :new.nr_requisicao || obter_separador_bv || 'nr_sequencia=' || :new.nr_sequencia || obter_separador_bv;

			if (ie_setor_supply_w = 'S') then
				gravar_agend_integracao(441, ds_param_integ_hl7_w);
			elsif (ie_setor_supply_w = 'S') then
				gravar_agend_integracao(918, ds_param_integ_hl7_w);
			end if;
		end if;

	end if;

	gerar_int_dankia_pck.dankia_disp_barras_req(:new.cd_material, :new.nr_seq_lote_fornec, :new.qt_material_atendida, :new.cd_barras, :new.nm_usuario);
	gerar_int_dankia_pck.dankia_disp_item_transf(:new.cd_material, :new.cd_barras, :new.nr_requisicao, :new.nr_sequencia, :new.qt_material_atendida, cd_local_estoque_destino_w, cd_local_estoque_w, :new.nm_usuario,cd_estabelecimento_w, 'I',:new.nr_seq_lote_fornec);

	exception
	when others then
		null;
	end;
end if;
-- Swisslog / SupplyPoint / Athena

if	(updating) and
	(nvl(:old.cd_motivo_baixa,0) = 1) and
	(nvl(:new.cd_motivo_baixa,0) = 0) and
	(nvl(:new.qt_material_atendida,0) = 0) then
	begin
	gerar_int_dankia_pck.dankia_disp_item_transf(:old.cd_material, :old.cd_barras, :old.nr_requisicao, :old.nr_sequencia, :old.qt_material_atendida, cd_local_estoque_destino_w, cd_local_estoque_w, :new.nm_usuario,cd_estabelecimento_w, 'E', :old.nr_seq_lote_fornec);
	exception
	when others then
		null;
	end;
end if;

/*INTPD - Envia Baixa do item da requisicao*/
ie_integra_w := 'N';
if	(:old.dt_atendimento is null) and
	(:new.dt_atendimento is not null) then
	reg_integracao_p.ie_operacao	:=	'I';
	ie_integra_w 			:= 'S';
elsif	(:old.dt_atendimento is not null) and
	(:new.dt_atendimento is null) then
	reg_integracao_p.ie_operacao	:=	'A';
	ie_integra_w 			:= 'S';
end if;

if	(ie_integra_w = 'S') then
	reg_integracao_p.cd_estab_documento		:=	cd_estabelecimento_w;
	reg_integracao_p.cd_local_estoque		:=	cd_local_estoque_w;
	reg_integracao_p.cd_local_estoque_destino	:=	cd_local_estoque_destino_w;
	reg_integracao_p.cd_centro_custo		:=	cd_centro_custo_w;
	reg_integracao_p.cd_operacao_estoque		:=	cd_operacao_estoque_w;
	reg_integracao_p.ds_id_origin			:=	ie_origem_requisicao_w;
	reg_integracao_p.nr_seq_item_documento_p	:=	:new.nr_sequencia;
	gerar_int_padrao.gravar_integracao('56', :new.nr_requisicao, :new.nm_usuario, reg_integracao_p);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Item_Req_Material_Insert
BEFORE INSERT ON TASY.ITEM_REQUISICAO_MATERIAL FOR EACH ROW
declare
dt_aprovacao_w		date;
nm_usuario_aprov_w	varchar2(15);

BEGIN

select	dt_aprovacao,
	nm_usuario_aprov
into	dt_aprovacao_w,
	nm_usuario_aprov_w
from	requisicao_material
where	nr_requisicao = :new.nr_requisicao;

if	(dt_aprovacao_w is not null) and
	(:new.dt_aprovacao is null) then
	begin

	:new.dt_aprovacao	:= dt_aprovacao_w;
	:new.nm_usuario_aprov	:= nm_usuario_aprov_w;

	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.item_req_material_beforeinsert
before insert ON TASY.ITEM_REQUISICAO_MATERIAL for each row
declare
dt_liberacao_w			date;
ie_consignado_w			varchar2(1);
cd_local_estoque_w		number(5);
cd_local_estoque_destino_w		number(5);

begin

select	max(dt_liberacao),
	max(cd_local_estoque),
	nvl(max(cd_local_estoque_destino),0)
into	dt_liberacao_w,
	cd_local_estoque_w,
	cd_local_estoque_destino_w
from	requisicao_material
where	nr_requisicao = :new.nr_requisicao;

select 	nvl(max(a.ie_consignado),'0')
into	ie_consignado_w
from 	operacao_estoque a,
	requisicao_material b
where 	b.nr_requisicao = :new.nr_requisicao
and	a.cd_operacao_estoque = b.cd_operacao_estoque;


/* Retirado por Fabio - OS 386419 - Foi colocado na procedure CONSISTIR_REQUISICAO
if	(:new.qt_saldo_estoque is null) then
	begin

	if	(ie_consignado_w <> '0') then
		:new.qt_saldo_estoque	:= obter_saldo_estoque_consig(:new.cd_estabelecimento, :new.cd_cgc_fornecedor, :new.cd_material, cd_local_estoque_w);
	else
		:new.qt_saldo_estoque	:= obter_saldo_disp_estoque(:new.cd_estabelecimento, :new.cd_material, cd_local_estoque_w, trunc(sysdate,'mm'));
	end if;

	end;
end if;

if	(:new.qt_saldo_estoque_dest is null) and
	(cd_local_estoque_destino_w > 0) then
	begin

	if	(ie_consignado_w <> '0') then
		:new.qt_saldo_estoque_dest	:= obter_saldo_estoque_consig(:new.cd_estabelecimento, :new.cd_cgc_fornecedor, :new.cd_material, cd_local_estoque_destino_w);
	else
		:new.qt_saldo_estoque_dest	:= obter_saldo_disp_estoque(:new.cd_estabelecimento, :new.cd_material, cd_local_estoque_destino_w, trunc(sysdate,'mm'));
	end if;

	end;
end if;*/

:new.QT_MATERIAL_ATENDIDA := nvl(:new.QT_MATERIAL_ATENDIDA, 0);
end;
/


CREATE OR REPLACE TRIGGER TASY.intdisp_requisicao_material
before update ON TASY.ITEM_REQUISICAO_MATERIAL for each row
declare

qt_existe_regra_w		number(10);
dt_validade_w			date;
cd_setor_atendimento_w		number(10);
cd_local_estoque_destino_w	number(6);
nr_sequencia_w			number(10);
nr_digito_verif_w		number(5);
nr_seq_lote_fornec_w		number(10);

begin
if	(:old.dt_atendimento is null) and
	(:old.cd_motivo_baixa = '0') and
	(:new.dt_atendimento is not null) and
	(:new.cd_motivo_baixa > '0') then
	begin
	select 	cd_local_estoque_destino
	into	cd_local_estoque_destino_w
	from	requisicao_material
	where	nr_requisicao = :new.nr_requisicao;

	begin
	select	1
	into	qt_existe_regra_w
	from	dis_regra_local_setor
	where	cd_local_estoque = cd_local_estoque_destino_w
	and	rownum = 1;
	exception
	when others then
		qt_existe_regra_w := 0;
	end;

	if	(:new.nr_seq_lote_fornec is not null) then
		begin
		select	max(nr_digito_verif),
			max(dt_validade)
		into	nr_digito_verif_w,
			dt_validade_w
		from	material_lote_fornec
		where	nr_sequencia = :new.nr_seq_lote_fornec;

		nr_seq_lote_fornec_w :=	substr(adiciona_zeros_esquerda(:new.nr_seq_lote_fornec||nr_digito_verif_w,11),1,15);
		end;
	end if;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	int_disp_req_material;

	if	(qt_existe_regra_w > 0) and (nvl(:new.qt_material_atendida,0) > 0) then

		insert into int_disp_req_material(
			nr_sequencia,
			nr_requisicao,
			dt_leitura,
			cd_acao,
			cd_material,
			qt_material_atendida,
			nr_seq_lote_fornec,
			dt_validade,
			cd_estabelecimento,
			cd_barras,
			dt_atualizacao)
		values(	nr_sequencia_w,
			:new.nr_requisicao,
			'',
			'ESA',
			:new.cd_material,
			:new.qt_material_atendida,
			nr_seq_lote_fornec_w,
			dt_validade_w,
			:new.cd_estabelecimento,
			:new.cd_barras,
			sysdate);
	end if;
	end;
end if;

end intdisp_requisicao_material;
/


CREATE OR REPLACE TRIGGER TASY.Item_Req_Material_Update
BEFORE UPDATE ON TASY.ITEM_REQUISICAO_MATERIAL FOR EACH ROW
declare
nr_seq_terceiro_w				Number(10,0);
nr_sequencia_w				Number(10,0);
nr_seq_operacao_w			Number(10,0);
nr_seq_oper_terc_w			Number(10,0) 	:= 0;
ie_entrada_saida_w				Varchar2(01)	:= 'S';
ie_existe_w				Number(5);
qt_reg_w					number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(nvl(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)),0) > 0) and
	((:new.dt_atendimento is null ) and (:new.dt_reprovacao is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(266078);
	--'N�o pode ter codigo de baixa com data de atendimento nula!!! ');
end if;

if	(nvl(obter_tipo_motivo_baixa_req(nvl(:new.cd_motivo_baixa,0)),0) = 0) and
	(:new.dt_atendimento is not null ) then
	wheb_mensagem_pck.exibir_mensagem_abort(395271);
	--'N�o pode ter data de atendimento sem motivo de baixa.');
end if;

begin
select	nvl(max(b.nr_seq_terceiro),0),
	max(c.ie_entrada_saida)
into	nr_seq_terceiro_w,
	ie_entrada_saida_w
from 	Operacao_estoque c,
	centro_custo b,
	requisicao_material a
where	a.cd_centro_custo 	= b.cd_centro_custo
and	a.nr_requisicao		= :new.nr_requisicao
and	a.cd_operacao_estoque	= c.cd_operacao_estoque;
exception
	when others then
		nr_seq_terceiro_w	:= 0;
end;

if	(nr_seq_terceiro_w > 0) then
	begin
	begin
	select nr_sequencia
	into nr_seq_oper_terc_w
	from terceiro_operacao
	where nr_seq_terceiro 	= nr_seq_terceiro_w
	  and nr_doc			= :new.nr_requisicao
	  and nr_seq_doc		= :new.nr_sequencia;
	exception
		when others then
			nr_seq_oper_terc_w	:= 0;
	end;

	if  	(nvl(obter_tipo_motivo_baixa_req(:new.cd_motivo_baixa),0) in (1,5)) and /* Matheus OS61284 11/07/07 inclui motivo 5*/
	  	(nr_seq_oper_terc_w = 0) then

		obter_param_usuario(907,60,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,nr_seq_operacao_w);
		select	count(*)
		into	ie_existe_w
		from	operacao_terceiro
		where	nr_sequencia = nvl(nr_seq_operacao_w,1);
		if	(ie_existe_w	= 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(266079);
			--'N�o existe opera��o de terceiro cadastrada.' || chr(10) || chr(13));
		end if;

		begin

		select terceiro_operacao_seq.nextval
		into	nr_sequencia_w
		from	dual;
		insert into terceiro_operacao(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_terceiro,
			nr_seq_operacao,
			tx_operacao,
			dt_atualizacao,
			nm_usuario,
			nr_doc,
			nr_seq_doc,
			dt_operacao,
			nr_seq_conta,
			cd_material,
			ie_origem_proced,
			cd_procedimento,
			qt_operacao,
			vl_operacao)
		values(nr_sequencia_w,
			:new.cd_estabelecimento,
			nr_seq_terceiro_w,
			nvl(nr_seq_operacao_w,1),
			100,
			sysdate,
			:new.nm_usuario,
			:new.nr_requisicao,
			:new.nr_sequencia,
			:new.dt_atendimento,
			null,
 			:new.cd_material,
			null,
			null,
			decode(ie_entrada_saida_w,'S', :new.qt_material_atendida, :new.qt_material_atendida * -1),
			0);
		atualizar_operacao_terceiro(nr_sequencia_w, :new.nm_usuario);
		end;
	elsif  	(nvl(obter_tipo_motivo_baixa_req(:new.cd_motivo_baixa),0) = 0) then
		begin
		delete from terceiro_operacao
		where	nr_sequencia = nr_seq_oper_terc_w;
		end;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.ITEM_REQUISICAO_MATERIAL_tp  after update ON TASY.ITEM_REQUISICAO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_REQUISICAO='||to_char(:old.NR_REQUISICAO)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'ITEM_REQUISICAO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ITEM_REQUISICAO_MATERIAL ADD (
  CONSTRAINT ITEREQM_PK
 PRIMARY KEY
 (NR_REQUISICAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          72M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ITEM_REQUISICAO_MATERIAL ADD (
  CONSTRAINT ITEREMA_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_MOJUREQ_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.MOTIVO_JUSTIF_REQ (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI),
  CONSTRAINT ITEREMA_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_FISCAL) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_REQ) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ITEREMA_KITESTO_FK 
 FOREIGN KEY (NR_SEQ_KIT_ESTOQUE) 
 REFERENCES TASY.KIT_ESTOQUE (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT ITEREQM_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT ITEREQM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ITEREQM_PESFISI_ATENDIDA_FK 
 FOREIGN KEY (CD_PESSOA_ATENDE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ITEREQM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RECEBE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ITEREQM_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT ITEREMA_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ITEREMA_UNIMEDIEST_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_ESTOQUE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT ITEREQM_REQMATE_FK 
 FOREIGN KEY (NR_REQUISICAO) 
 REFERENCES TASY.REQUISICAO_MATERIAL (NR_REQUISICAO)
    ON DELETE CASCADE,
  CONSTRAINT ITEREMA_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_PROCOMP_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO) 
 REFERENCES TASY.PROCESSO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ITEREMA_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT ITEREMA_SUMOBAR_FK 
 FOREIGN KEY (CD_MOTIVO_BAIXA) 
 REFERENCES TASY.SUP_MOTIVO_BAIXA_REQ (NR_SEQUENCIA));

GRANT SELECT ON TASY.ITEM_REQUISICAO_MATERIAL TO NIVEL_1;

