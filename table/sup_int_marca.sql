ALTER TABLE TASY.SUP_INT_MARCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_MARCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_MARCA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_FORMA_INTEGRACAO     VARCHAR2(15 BYTE)     NOT NULL,
  DT_LEITURA              DATE,
  DT_CONFIRMA_INTEGRACAO  DATE,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_MARCA                VARCHAR2(80 BYTE)     NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_TIPO             NUMBER(10),
  QT_DIA_RESSUP           NUMBER(3),
  DT_REPROVACAO           DATE,
  NM_USUARIO_REPROVACAO   VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  CD_SISTEMA_ANT          VARCHAR2(80 BYTE),
  CD_CNPJ_FABRICANTE      VARCHAR2(14 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINMAR_PK ON TASY.SUP_INT_MARCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.sup_int_marca_atual
before update or insert ON TASY.SUP_INT_MARCA for each row
declare

nr_sequencia_w		marca.nr_sequencia%type;
nr_seq_marca_w		marca.nr_sequencia%type := 0;
cd_cnpj_fabricante_w	pessoa_juridica.cd_cgc%type;
nr_seq_tipo_w		tipo_marca.nr_sequencia%type;
qt_existe_erros_w	number(10);

begin

if	(:new.ie_forma_integracao = 'R') then

	:new.dt_leitura	:= sysdate;

	consiste_sup_int_marca(	:new.nr_sequencia,
				:new.cd_cnpj_fabricante,
				:new.nr_seq_tipo,
				:new.nm_usuario );

	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_marca_consist
	where	nr_sequencia = :new.nr_sequencia;

	if	(qt_existe_erros_w = 0) then

		if (updating) then

			select	nvl(max(nr_sequencia),0)
			into	nr_seq_marca_w
			from	marca
			where	cd_sistema_ant = to_char(:new.nr_sequencia);

		end if;

		if (nr_seq_marca_w = 0) then

			select 	marca_seq.nextval
			into	nr_sequencia_w
			from	dual;

			select 	max(cd_cgc)
			into	cd_cnpj_fabricante_w
			from 	pessoa_juridica
			where 	cd_sistema_ant = :new.cd_cnpj_fabricante;

			select 	max(nr_sequencia)
			into	nr_seq_tipo_w
			from 	tipo_marca
			where	cd_sistema_ant = :new.nr_seq_tipo;

			insert into marca(
				nr_sequencia,
				ds_marca,
				ie_situacao,
				nr_seq_tipo,
				qt_dia_ressup,
				dt_reprovacao,
				nm_usuario_reprovacao,
				ds_observacao,
				cd_sistema_ant,
				cd_cnpj_fabricante,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_sequencia_w,
				:new.ds_marca,
				:new.ie_situacao,
				nr_seq_tipo_w,
				:new.qt_dia_ressup,
				:new.dt_reprovacao,
				:new.nm_usuario_reprovacao,
				:new.ds_observacao,
				:new.nr_sequencia,
				cd_cnpj_fabricante_w,
				:new.dt_atualizacao,
				:new.nm_usuario,
				:new.dt_atualizacao_nrec,
				:new.nm_usuario);

			:new.dt_confirma_integracao := sysdate;
			:new.cd_sistema_ant := nr_sequencia_w;

		else

			select 	max(cd_cgc)
			into	cd_cnpj_fabricante_w
			from 	pessoa_juridica
			where 	cd_sistema_ant = :new.cd_cnpj_fabricante;

			select 	max(nr_sequencia)
			into	nr_seq_tipo_w
			from 	tipo_marca
			where	cd_sistema_ant = :new.nr_seq_tipo;

			update	marca
			set	ds_marca = :new.ds_marca,
				ie_situacao = :new.ie_situacao,
				nr_seq_tipo = nr_seq_tipo_w,
				qt_dia_ressup = :new.qt_dia_ressup,
				dt_reprovacao = :new.dt_reprovacao,
				nm_usuario_reprovacao = :new.nm_usuario_reprovacao,
				ds_observacao = :new.ds_observacao,
				cd_cnpj_fabricante = cd_cnpj_fabricante_w,
				dt_atualizacao = :new.dt_atualizacao,
				nm_usuario = :new.nm_usuario,
				dt_atualizacao_nrec = :new.dt_atualizacao_nrec,
				nm_usuario_nrec = :new.nm_usuario
			where	cd_sistema_ant = :new.nr_sequencia;

		end if;

	end if;

end if;

end;
/


ALTER TABLE TASY.SUP_INT_MARCA ADD (
  CONSTRAINT SUINMAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_MARCA TO NIVEL_1;

