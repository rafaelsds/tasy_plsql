ALTER TABLE TASY.PROCESSO_APROV_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCESSO_APROV_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCESSO_APROV_RESP
(
  CD_PROCESSO_APROV           NUMBER(10)        NOT NULL,
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_RESPONSAVEL              VARCHAR2(1 BYTE)  NOT NULL,
  IE_SOLICITACAO_COMPRA       VARCHAR2(1 BYTE)  NOT NULL,
  IE_ORDEM_COMPRA             VARCHAR2(1 BYTE)  NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_CARGO                    NUMBER(10),
  VL_MINIMO                   NUMBER(15,2),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_ORDEM_COMPRA_TRANSF      VARCHAR2(1 BYTE)  NOT NULL,
  VL_MAXIMO                   NUMBER(15,2),
  NM_USUARIO_REGRA            VARCHAR2(15 BYTE),
  IE_REQUISICAO               VARCHAR2(1 BYTE)  NOT NULL,
  IE_COTACAO                  VARCHAR2(1 BYTE)  NOT NULL,
  IE_DESCONSIDERA_REQ_TRANSF  VARCHAR2(1 BYTE)  NOT NULL,
  IE_SOLICITACAO_PAGTO        VARCHAR2(1 BYTE)  NOT NULL,
  NR_NIVEL_APROVACAO          NUMBER(10),
  QT_MINIMO_APROVADOR         NUMBER(10),
  IE_NOTA_FISCAL              VARCHAR2(1 BYTE)  NOT NULL,
  IE_REG_LICITACAO            VARCHAR2(1 BYTE)  NOT NULL,
  IE_TRANSF_PCS               VARCHAR2(1 BYTE),
  IE_NOTA_FISCAL_CONTRATO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROAPRE_CARGO_FK_I ON TASY.PROCESSO_APROV_RESP
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROAPRE_PK ON TASY.PROCESSO_APROV_RESP
(CD_PROCESSO_APROV, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAPRE_PROAPRO_FK_I ON TASY.PROCESSO_APROV_RESP
(CD_PROCESSO_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAPRE_USUARIO_FK_I ON TASY.PROCESSO_APROV_RESP
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROCESSO_APROV_RESP_tp  after update ON TASY.PROCESSO_APROV_RESP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_PROCESSO_APROV='||to_char(:old.CD_PROCESSO_APROV)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.IE_REG_LICITACAO,1,4000),substr(:new.IE_REG_LICITACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REG_LICITACAO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOTA_FISCAL,1,4000),substr(:new.IE_NOTA_FISCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOTA_FISCAL',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESPONSAVEL,1,4000),substr(:new.IE_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESPONSAVEL',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLICITACAO_COMPRA,1,4000),substr(:new.IE_SOLICITACAO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITACAO_COMPRA',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDEM_COMPRA,1,4000),substr(:new.IE_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDEM_COMPRA',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO,1,4000),substr(:new.VL_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDEM_COMPRA_TRANSF,1,4000),substr(:new.IE_ORDEM_COMPRA_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDEM_COMPRA_TRANSF',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRANSF_PCS,1,4000),substr(:new.IE_TRANSF_PCS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSF_PCS',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAXIMO,1,4000),substr(:new.VL_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAXIMO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_REGRA,1,4000),substr(:new.NM_USUARIO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_REGRA',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQUISICAO,1,4000),substr(:new.IE_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQUISICAO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COTACAO,1,4000),substr(:new.IE_COTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COTACAO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESCONSIDERA_REQ_TRANSF,1,4000),substr(:new.IE_DESCONSIDERA_REQ_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESCONSIDERA_REQ_TRANSF',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLICITACAO_PAGTO,1,4000),substr(:new.IE_SOLICITACAO_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITACAO_PAGTO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_NIVEL_APROVACAO,1,4000),substr(:new.NR_NIVEL_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_NIVEL_APROVACAO',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_APROVADOR,1,4000),substr(:new.QT_MINIMO_APROVADOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_APROVADOR',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCESSO_APROV,1,4000),substr(:new.CD_PROCESSO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCESSO_APROV',ie_log_w,ds_w,'PROCESSO_APROV_RESP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.processo_aprov_resp_atual
before update or insert ON TASY.PROCESSO_APROV_RESP for each row
declare

begin

if	(nvl(:new.ie_responsavel,'X') <> nvl(:old.ie_responsavel,'X')) or
	(nvl(:new.cd_cargo,0) <> nvl(:old.cd_cargo,0)) or
	(nvl(:new.nm_usuario_regra,'X') <> nvl(:old.nm_usuario_regra,'X')) then
	begin
	if	(:new.ie_responsavel = 'C') then
		if	(:new.cd_cargo is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(314029);
		end if;

		if	(:new.nm_usuario_regra is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(314030);
		end if;
	end if;

	if	(:new.ie_responsavel = 'F') then
		if	(:new.nm_usuario_regra is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(314031);
		end if;

		if	(:new.cd_cargo is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(314032);
		end if;
	end if;
	end;
end if;

end;
/


ALTER TABLE TASY.PROCESSO_APROV_RESP ADD (
  CONSTRAINT PROAPRE_PK
 PRIMARY KEY
 (CD_PROCESSO_APROV, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCESSO_APROV_RESP ADD (
  CONSTRAINT PROAPRE_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT PROAPRE_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO)
    ON DELETE CASCADE,
  CONSTRAINT PROAPRE_PROAPRO_FK 
 FOREIGN KEY (CD_PROCESSO_APROV) 
 REFERENCES TASY.PROCESSO_APROVACAO (CD_PROCESSO_APROV)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROCESSO_APROV_RESP TO NIVEL_1;

