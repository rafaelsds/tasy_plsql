ALTER TABLE TASY.MATERIAL_REPASSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_REPASSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_REPASSE
(
  NR_SEQ_MATERIAL             NUMBER(10)        NOT NULL,
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  VL_REPASSE                  NUMBER(15,2)      NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_SEQ_TERCEIRO             NUMBER(10)        NOT NULL,
  NR_LOTE_CONTABIL            NUMBER(10),
  NR_REPASSE_TERCEIRO         NUMBER(10),
  CD_CONTA_CONTABIL           VARCHAR2(20 BYTE),
  NR_SEQ_TRANS_FIN            NUMBER(10),
  VL_LIBERADO                 NUMBER(15,2),
  NR_SEQ_ITEM_RETORNO         NUMBER(10),
  IE_STATUS                   VARCHAR2(1 BYTE)  NOT NULL,
  NR_SEQ_ORIGEM               NUMBER(10),
  CD_REGRA                    NUMBER(5)         NOT NULL,
  DT_LIBERACAO                DATE,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  DT_CONTABIL_TITULO          DATE,
  NR_SEQ_RET_GLOSA            NUMBER(10),
  DT_CONTABIL                 DATE,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  NR_INTERNO_CONTA_EST        NUMBER(10),
  NR_SEQ_TRANS_FIN_REP_MAIOR  NUMBER(10),
  NR_SEQ_CRITERIO             NUMBER(10),
  IE_ESTORNO                  VARCHAR2(1 BYTE),
  IE_REPASSE_CALC             VARCHAR2(1 BYTE),
  VL_ORIGINAL_REPASSE         NUMBER(15,2),
  NR_SEQ_REGRA_ITEM           NUMBER(10),
  IE_ANALISADO                VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_DES           NUMBER(10),
  IE_DESC_CAIXA               VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_AUDIT_HIST      NUMBER(10),
  VL_DESP_CARTAO              NUMBER(15,2),
  NR_SEQ_PARCELA              NUMBER(10),
  VL_CUSTO_ITEM               NUMBER(15,2),
  VL_DESCONTO                 NUMBER(15,2),
  DS_REGRA                    VARCHAR2(255 BYTE),
  CD_MATERIAL                 NUMBER(6),
  DS_MATERIAL                 VARCHAR2(255 BYTE),
  NR_ATENDIMENTO              NUMBER(10),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  NM_PESSOA_FISICA            VARCHAR2(60 BYTE),
  DS_STATUS                   VARCHAR2(255 BYTE),
  DS_TRANSACAO                VARCHAR2(255 BYTE),
  DS_CONVENIO                 VARCHAR2(255 BYTE),
  NM_MEDICO                   VARCHAR2(255 BYTE),
  NR_INTERNO_CONTA            NUMBER(10),
  DT_CONTA                    DATE,
  DS_TIPO_ATENDIMENTO         VARCHAR2(255 BYTE),
  DS_CENTRO_CUSTO             VARCHAR2(255 BYTE),
  NM_USUARIO_ORIGINAL         VARCHAR2(255 BYTE),
  DS_OBSERVACAO_CRITERIO      VARCHAR2(255 BYTE),
  DS_CATEGORIA                VARCHAR2(255 BYTE),
  NR_PRESCRICAO               NUMBER(14),
  DS_CLASSIF_ATUA_MEDICO      VARCHAR2(255 BYTE),
  DT_ENTRADA                  DATE,
  CD_CONVENIO                 NUMBER(5),
  IE_TIPO_CONVENIO            NUMBER(2),
  IE_TIPO_ATENDIMENTO         NUMBER(3),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  QT_MATERIAL                 NUMBER(9,3),
  CD_MOTIVO_EXC_CONTA         NUMBER(3),
  CD_PROTOCOLO                NUMBER(10),
  NR_CICLO                    NUMBER(3),
  VL_CUSTO                    NUMBER(15,2),
  VL_IMPOSTO                  NUMBER(15,2),
  NR_SEQ_MEDICACAO            NUMBER(6),
  DT_ENTREGA_CONVENIO         DATE,
  DS_TIPO_CONV                VARCHAR2(255 BYTE),
  DS_CLINICA                  VARCHAR2(255 BYTE),
  CD_UNIDADE_BASICA           VARCHAR2(10 BYTE),
  NM_MEDICO_REFERIDO          VARCHAR2(255 BYTE),
  NR_ATENDIMENTO_MAE          NUMBER(10),
  DS_LOG                      VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATREPA_CONCONT_FK_I ON TASY.MATERIAL_REPASSE
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_CONREIT_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_ITEM_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_CONREIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_CORETGL_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_RET_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_I1 ON TASY.MATERIAL_REPASSE
(NR_SEQ_TERCEIRO, IE_STATUS, NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_I2 ON TASY.MATERIAL_REPASSE
(DT_CONTABIL, DT_CONTABIL_TITULO, NR_SEQ_TERCEIRO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_I2
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_LOTAUHI_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_LOTCONT_FK_I ON TASY.MATERIAL_REPASSE
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_MATPACI_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_MATREPA_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_MOALREP_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_MOTIVO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_MOALREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_MOCRPAR_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_PARCELA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_PESFISI_FK_I ON TASY.MATERIAL_REPASSE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATREPA_PK ON TASY.MATERIAL_REPASSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_REGRETE_FK_I ON TASY.MATERIAL_REPASSE
(CD_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_REGRETE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_REPTERC_FK_I ON TASY.MATERIAL_REPASSE
(NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATREPA_TRAFINA_FK_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATREPA_TRAFINA_FK2_I ON TASY.MATERIAL_REPASSE
(NR_SEQ_TRANS_FIN_REP_MAIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATREPA_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Material_Repasse_Delete
BEFORE DELETE ON TASY.MATERIAL_REPASSE FOR EACH ROW
declare

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	null;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.material_repasse_insert
before insert ON TASY.MATERIAL_REPASSE for each row
declare

qt_reg_w			number(1);

begin

:new.ds_log := substr('Processo de geracao do Repasse: ' || dbms_utility.format_call_stack,1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_atualizacao_nrec is null) then
	begin
	:new.dt_atualizacao_nrec := sysdate;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_repasse_Atual
BEFORE UPDATE ON TASY.MATERIAL_REPASSE FOR EACH ROW
declare

cont_w				number(1, 0);
nr_seq_repasse_w		Number(10,0);
qt_reg_w			number(1);
ie_vincular_rep_proc_pos_w	varchar2(1);
ie_vinc_repasse_ret_w		varchar2(1);

begin

:new.ds_log := substr(:old.ds_log || chr(13) || chr(10) || ' Atualizacao no repasse: ' || substr(dbms_utility.format_call_stack,1,4000),1,4000);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

CONSISTIR_VENC_REPASSE(
	:old.nr_repasse_terceiro,
	:new.nr_repasse_terceiro,
	:old.vl_repasse,
	:new.vl_repasse,
	:old.vl_liberado,
	:new.vl_liberado);


-- se esta desvinculando
if	(:new.nr_repasse_terceiro is null) and
	(:old.nr_repasse_terceiro is not null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :old.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		/* O repasse deste material ja esta fechado!
		Repasse: :old.nr_repasse_terceiro */
		wheb_mensagem_pck.exibir_mensagem_abort(262460,'NR_REPASSE_TERCEIRO_W='||:old.nr_repasse_terceiro);
	end if;
end if;

-- se esta vinculando
if	(:new.nr_repasse_terceiro is not null) and
	(:old.nr_repasse_terceiro is null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :new.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		/* O repasse deste material ja esta fechado!
		Repasse: :old.nr_repasse_terceiro */
		wheb_mensagem_pck.exibir_mensagem_abort(262460,'NR_REPASSE_TERCEIRO_W='||:old.nr_repasse_terceiro);
	end if;
end if;

if	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'S') or
	 (:new.ie_status = 'L')) then
	begin
	if	(:new.dt_liberacao is null) then
		:new.dt_liberacao	:= sysdate;
	end if;

	if	(:new.nr_repasse_terceiro is null) and
		(:old.nr_repasse_terceiro is null) then
		obter_repasse_terceiro(
			:new.dt_liberacao,
			:new.nr_seq_terceiro,
			:new.nm_usuario,
			:new.nr_seq_material,
			'M',
			nr_seq_repasse_w,
			null,
			null);

		select	nvl(max(b.ie_vincular_rep_proc_pos),'S'),
			nvl(max(b.ie_vinc_repasse_ret),'S')
		into	ie_vincular_rep_proc_pos_w,
			ie_vinc_repasse_ret_w
		from	parametro_faturamento b,
			terceiro a
		where	a.cd_estabelecimento	= b.cd_estabelecimento
		and	a.nr_sequencia		= :new.nr_seq_terceiro;

		if	(nvl(nr_seq_repasse_w,0) > 0) then
			if	((ie_vincular_rep_proc_pos_w = 'N') and
				 (trunc(:new.dt_liberacao, 'dd') > trunc(sysdate, 'dd'))) or
				(ie_vinc_repasse_ret_w = 'N') then
				:new.nr_repasse_terceiro	:= null;
			else
				:new.nr_repasse_terceiro	:= nr_seq_repasse_w;
			end if;
		end if;

	end if;
	end;
end if;

if	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'R') or
	 (:new.ie_status = 'S') or
	 (:new.ie_status = 'L')) then

	CONSISTIR_ITEM_REPASSE_TIT(null,:new.nr_seq_material);

end if;

if  (:new.ie_status <> :old.ie_status) then
	:new.ds_status := substr(obter_valor_dominio(1129, :new.ie_status),1,50);
end if;

if  (:new.nr_seq_trans_fin <> :old.nr_seq_trans_fin) then
    :new.ds_transacao := substr(obter_desc_trans_financ(:new.nr_seq_trans_fin),1,100);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.MATERIAL_REPASSE ADD (
  CONSTRAINT MATREPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_REPASSE ADD (
  CONSTRAINT MATREPA_MOCRPAR_FK 
 FOREIGN KEY (NR_SEQ_PARCELA) 
 REFERENCES TASY.MOVTO_CARTAO_CR_PARCELA (NR_SEQUENCIA),
  CONSTRAINT MATREPA_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT MATREPA_MOALREP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DES) 
 REFERENCES TASY.MOTIVO_ALT_REPASSE (NR_SEQUENCIA),
  CONSTRAINT MATREPA_MATPACI_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATREPA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT MATREPA_REPTERC_FK 
 FOREIGN KEY (NR_REPASSE_TERCEIRO) 
 REFERENCES TASY.REPASSE_TERCEIRO (NR_REPASSE_TERCEIRO)
    ON DELETE CASCADE,
  CONSTRAINT MATREPA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT MATREPA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MATREPA_MATREPA_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.MATERIAL_REPASSE (NR_SEQUENCIA),
  CONSTRAINT MATREPA_REGRETE_FK 
 FOREIGN KEY (CD_REGRA) 
 REFERENCES TASY.REGRA_REPASSE_TERCEIRO (CD_REGRA),
  CONSTRAINT MATREPA_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT MATREPA_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MATREPA_CORETGL_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT MATREPA_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_REP_MAIOR) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_REPASSE TO NIVEL_1;

