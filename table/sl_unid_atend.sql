ALTER TABLE TASY.SL_UNID_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SL_UNID_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.SL_UNID_ATEND
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  NR_SEQ_UNIDADE              NUMBER(10),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE              NOT NULL,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE) NOT NULL,
  IE_STATUS_SERV              VARCHAR2(15 BYTE) NOT NULL,
  NR_SEQ_SERVICO              NUMBER(10)        NOT NULL,
  DT_PREVISTA                 DATE              NOT NULL,
  IE_EXECUTOR                 VARCHAR2(15 BYTE) NOT NULL,
  DT_INICIO                   DATE,
  DT_FIM                      DATE,
  CD_EXECUTOR                 VARCHAR2(10 BYTE),
  NR_SEQ_SERVICO_EXEC         NUMBER(10),
  DT_SAIDA_REAL               DATE,
  DT_PAUSA_SERVICO            DATE,
  DT_FIM_PAUSA_SERVICO        DATE,
  DS_OBS_PAUSA_LEITO          VARCHAR2(255 BYTE),
  DT_APROVACAO                DATE,
  CD_EXECUTOR_INIC_SERV       VARCHAR2(10 BYTE),
  CD_EXECUTOR_FIM_SERV        VARCHAR2(10 BYTE),
  CD_EXECUTOR_APROV_SERV      VARCHAR2(10 BYTE),
  CD_EXECUTOR_PAUSA_SERV      VARCHAR2(10 BYTE),
  DS_OBS_FIM_SERV             VARCHAR2(255 BYTE),
  IE_PRIORIDADE               VARCHAR2(1 BYTE),
  DT_INTERDICAO               DATE,
  DT_FIM_INTERDICAO           DATE,
  NM_USUARIO_INTERD           VARCHAR2(15 BYTE),
  IE_STATUS_SERV_INTERD       VARCHAR2(15 BYTE),
  CD_PF_USUARIO_INIC_SERV     VARCHAR2(10 BYTE),
  CD_PF_USUARIO_FIM_SERV      VARCHAR2(10 BYTE),
  IE_EVENTO                   VARCHAR2(15 BYTE),
  CD_PROF_PREVISTO            VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  DT_CANCELAMENTO_SERVICO     DATE,
  CD_EXECUTOR_CANC_SERV       VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_HIGIENIZACAO  NUMBER(10),
  CD_SETOR_HIGIENIZACAO       NUMBER(5),
  DS_JUST_TEMPO_SERV          VARCHAR2(4000 BYTE),
  NR_SEQ_MOTIVO_PAUSA         NUMBER(10),
  IE_STATUS_UNID_ANT          VARCHAR2(3 BYTE),
  NM_USUARIO_MANUTENCAO       VARCHAR2(15 BYTE),
  DT_FIM_MANUTENCAO           DATE,
  DT_MANUTENCAO               DATE,
  NM_USUARIO_FIM_MANUTENCAO   VARCHAR2(15 BYTE),
  CD_EVENTO_ERITEL            NUMBER(10),
  NR_SEQ_MOTIVO_PRIOR         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SLUNIAT_ESTABEL_FK_I ON TASY.SL_UNID_ATEND
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLUNIAT_I1 ON TASY.SL_UNID_ATEND
(NR_SEQ_UNIDADE, TRUNC("DT_INICIO"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLUNIAT_I2 ON TASY.SL_UNID_ATEND
(NR_SEQ_UNIDADE, NR_SEQ_SERVICO, DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLUNIAT_PESFISI_FK_I ON TASY.SL_UNID_ATEND
(CD_EXECUTOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SLUNIAT_PK ON TASY.SL_UNID_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLUNIAT_SETATEN_FK_I ON TASY.SL_UNID_ATEND
(CD_SETOR_HIGIENIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLUNIAT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SLUNIAT_SLMOHIG_FK_I ON TASY.SL_UNID_ATEND
(NR_SEQ_MOTIVO_HIGIENIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLUNIAT_SLMOHIG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SLUNIAT_SLMOTCA_FK_I ON TASY.SL_UNID_ATEND
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLUNIAT_SLMOTCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SLUNIAT_SLMTVPS_FK_I ON TASY.SL_UNID_ATEND
(NR_SEQ_MOTIVO_PAUSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLUNIAT_SLMTVPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SLUNIAT_SLSERVI_FK_I ON TASY.SL_UNID_ATEND
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLUNIAT_SLSERVI_FK2_I ON TASY.SL_UNID_ATEND
(NR_SEQ_SERVICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLUNIAT_SLSERVI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SLUNIAT_UNIATEN_FK_I ON TASY.SL_UNID_ATEND
(NR_SEQ_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SL_UNID_ATEND_tp  after update ON TASY.SL_UNID_ATEND FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PRIORIDADE,1,2000),substr(:new.IE_PRIORIDADE,1,2000),:new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'SL_UNID_ATEND',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.sl_unid_atend_insert
before insert ON TASY.SL_UNID_ATEND for each row
declare

cd_pessoa_destino_w 	varchar2(30);
ie_dbms_alert_w		varchar2(1);

begin

Obter_param_Usuario(75, 63, obter_perfil_ativo, nvl(:new.nm_usuario,'Tasy'), wheb_usuario_pck.get_cd_estabelecimento, ie_dbms_alert_w);

if	(:new.cd_setor_higienizacao is null) then

	select	distinct min(f.cd_pessoa_fisica)
	into	cd_pessoa_destino_w
	from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e,
		escala_diaria_adic f,
		escala_setor h
	where	c.nr_sequencia = g.nr_seq_classif
	and	g.nr_sequencia = e.nr_seq_grupo
	and	e.nr_sequencia = d.nr_seq_escala
	and	f.nr_seq_escala_diaria = d.nr_sequencia
	and	h.nr_seq_escala(+) = e.nr_sequencia
	and	obter_tipo_classif_escala(e.nr_sequencia) = 'S'
	and	sysdate between dt_inicio and dt_fim
	and	substr(obter_se_prof_ocup_livre(f.cd_pessoa_fisica,trunc(sysdate)),1,1) = 'L'
	and 	((exists ( select	1
			   from		escala_setor h
			   where	h.nr_seq_escala = e.nr_sequencia
			   and		h.cd_setor_atendimento in (select cd_setor_atendimento from unidade_atendimento where nr_seq_interno = :new.nr_seq_unidade)))
			   or 		(h.cd_setor_atendimento is null))
	order by substr(sl_obter_ult_serv_prof(f.cd_pessoa_fisica,sysdate, null, 'DI'),1,90);

else

	select	distinct min(f.cd_pessoa_fisica)
	into	cd_pessoa_destino_w
	from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e,
		escala_diaria_adic f,
		escala_setor h
	where	c.nr_sequencia = g.nr_seq_classif
	and	g.nr_sequencia = e.nr_seq_grupo
	and	e.nr_sequencia = d.nr_seq_escala
	and	f.nr_seq_escala_diaria = d.nr_sequencia
	and	h.nr_seq_escala(+) = e.nr_sequencia
	and	obter_tipo_classif_escala(e.nr_sequencia) = 'S'
	and	sysdate between dt_inicio and dt_fim
	and	substr(obter_se_prof_ocup_livre(f.cd_pessoa_fisica,trunc(sysdate)),1,1) = 'L'
	and 	((exists ( select	1
			   from		escala_setor h
			   where	h.nr_seq_escala = e.nr_sequencia
			   and		h.cd_setor_atendimento = :new.cd_setor_higienizacao))
			   or 		(h.cd_setor_atendimento is null))
	order by substr(sl_obter_ult_serv_prof(f.cd_pessoa_fisica,sysdate, null, 'DI'),1,90);

end if;

if 	(cd_pessoa_destino_w is not null) then
	:new.cd_prof_previsto := cd_pessoa_destino_w;
end if;

If	(ie_dbms_alert_w = 'S') then
	Gestao_Serv_leito_alert_signal(nvl(:new.nm_usuario,'Tasy'));
End if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sl_unid_atend_update
before update ON TASY.SL_UNID_ATEND for each row
declare
ie_status_unidade_w		varchar2(3);
ie_status_ant_unidade_w		varchar2(3);
ie_higienizar_unidade_w		varchar2(1) := 'E';
ie_atualizar_status_ant_w	varchar2(1);
nr_seq_evento_lider_w		number(10);
nr_seq_evento_gerencia_w	number(10);
cd_setor_atendimento_w		number(5,0);
qt_reg_w			number(1);
ie_ignorar_checklist_w		varchar2(1);
ie_exige_check_list_aprov_w	varchar2(1);
qt_itens_w			number(1);
ie_status_ocup_w		varchar2(5);
ie_feriado_w			varchar2(1);
qt_regra_feriado_w		number(10);
nr_seq_w			number(10);
ie_log_w			varchar2(1);
ds_w				varchar2(255);
ds_s_w				varchar2(255);
ds_c_w				varchar2(255);
ie_dbms_alert_w			varchar2(1);
ie_aguardando_higi_manut_w	varchar2(1);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
ie_higienizacao_ocup_w		varchar2(1);
ie_livre_ocup_w			varchar2(1);
ie_status_fim_w			varchar2(1);
ie_atualizar_unidade_livre_w	varchar2(1);
ie_permite_inicio_antes_prev_w	varchar2(1);
ie_permite_aprov_antes_fim_w	varchar2(1);
ie_alterar_livre_job_w	varchar2(1);

Cursor C01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= :new.cd_estabelecimento
	and	a.ie_evento_disp	= 'FSL'
	and	nvl(a.ie_situacao,'A') = 'A';

Cursor C02 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= :new.cd_estabelecimento
	and	a.ie_evento_disp	= 'ASL'
	and	nvl(a.ie_situacao,'A') = 'A';

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(nvl(ie_status_unidade,'L')),
	max(nvl(ie_status_ant_unidade,'L'))
into	ie_status_unidade_w,
	ie_status_ant_unidade_w
from	unidade_atendimento
where	nr_seq_interno = :new.nr_seq_unidade;

Obter_Param_Usuario(75, 16, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_higienizar_unidade_w);
Obter_Param_Usuario(75, 34, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atualizar_status_ant_w);
Obter_Param_Usuario(75, 37, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_status_ocup_w);
Obter_param_Usuario(75, 63, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_dbms_alert_w);
Obter_param_Usuario(75, 89, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_higienizacao_ocup_w);
Obter_param_Usuario(75, 90, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_livre_ocup_w);
Obter_param_Usuario(75, 97, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atualizar_unidade_livre_w);
Obter_param_Usuario(75, 110, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_permite_inicio_antes_prev_w);
Obter_param_Usuario(75, 111, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_permite_aprov_antes_fim_w);
Obter_param_Usuario(75, 112, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_alterar_livre_job_w);

if	((ie_permite_inicio_antes_prev_w = 'N') and
	(:new.dt_inicio is not null) and
	(:new.dt_inicio < :new.dt_prevista)) then
	wheb_mensagem_pck.exibir_mensagem_abort(728462, 'DS_DATA_MENOR=' || obter_desc_expressao(286737) ||
							';DS_DATA_MAIOR=' || obter_desc_expressao(287126) ||
							';NR_PARAMETRO=110'); -- Data de in�cio n�o pode ser inferior a Data prevista! Par�metro [110].
end if;

if 	(:new.dt_fim is not null) and
	(:new.dt_fim < :new.dt_inicio) then
	wheb_mensagem_pck.exibir_mensagem_abort(728452); -- A data fim n�o pode ser menor que a data in�cio!
end if;

if 	((ie_permite_aprov_antes_fim_w = 'N') and
	(:new.dt_aprovacao is not null) and
	(:new.dt_aprovacao < :new.dt_fim)) then
	wheb_mensagem_pck.exibir_mensagem_abort(728462, 'DS_DATA_MENOR=' || obter_desc_expressao(286713) ||
							';DS_DATA_MAIOR=' || obter_desc_expressao(286879) ||
							';NR_PARAMETRO=111'); -- Data de aprova��o n�o pode ser inferior a Data fim! Par�metro [111].
end if;

if	((:old.dt_inicio is not null) and (nvl(:new.dt_inicio,sysdate+1) <> nvl(:old.dt_inicio,sysdate+1)) and (:new.dt_fim is not null))  OR
	((:new.IE_STATUS_SERV = 'EE') and (:new.dt_fim is not null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(213231);
end if;


if	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'C' and
	((ie_alterar_livre_job_w = 'S') or
	 (ie_alterar_livre_job_w = 'N' and :new.ie_evento <> 'J'))) then

	update	unidade_atendimento
	set		ie_status_unidade	= decode(	nr_atendimento,
								null,
								decode(ie_status_ocup_w, null, decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R') ,ie_status_ocup_w),
								ie_status_unidade),
			nm_usuario = :new.nm_usuario
	where	nr_seq_interno		= :new.nr_seq_unidade;

end if;
if	(:old.dt_fim is null) and (:new.dt_fim is not null) and (:new.ie_status_serv <> 'E') then
	wheb_mensagem_pck.exibir_mensagem_abort(213232);
end if;

if	(ie_status_unidade_w in ('A','H','L','R','S','G','C','E')) then

	if	(:new.dt_inicio is not null) and
		(:old.dt_inicio is null) then

		update	unidade_atendimento
		set	dt_inicio_higienizacao	= :new.dt_inicio,
			dt_higienizacao		= null,
			nm_usuario_fim_higienizacao = null,
			ie_status_unidade	= decode(ie_status_unidade_w, 'C', 'E', decode(ie_higienizacao_ocup_w, 'S', 'H', ie_status_unidade)),
			nm_usuario_higienizacao	= :new.nm_usuario,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_interno		= :new.nr_seq_unidade;

	end if;

	if	(:new.dt_fim is not null) and
		(:old.dt_fim is null) and
		(ie_higienizar_unidade_w = 'F') then

		if	(ie_status_unidade_w = 'E') then

			select	max(cd_unidade_basica),
				max(cd_unidade_compl),
				max(cd_setor_atendimento)
			into	cd_unidade_basica_w,
				cd_unidade_compl_w,
				cd_setor_atendimento_w
			from	unidade_atendimento
			where	nr_seq_interno = :new.nr_seq_unidade;


			atualizar_leito_manutencao(:new.nm_usuario, cd_unidade_basica_w, cd_unidade_compl_w, cd_setor_atendimento_w, 'G', 'S');

		else
			select	nvl(max(ie_status_fim),'N')
			into	ie_status_fim_w
			from	sl_servico
			where 	nr_sequencia = nvl(:new.nr_seq_servico_exec, :new.nr_seq_servico);

			if (nvl(ie_status_fim_w,'N') = 'N') then
				update	unidade_atendimento
				set	dt_higienizacao		= :new.dt_fim,
					ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'),
					nm_usuario_higienizacao	= nvl(nm_usuario_higienizacao,:new.nm_usuario),
					nm_usuario_fim_higienizacao = :new.nm_usuario,
					nm_usuario		= :new.nm_usuario,
					dt_atualizacao		= sysdate
				where	nr_seq_interno		= :new.nr_seq_unidade;
			end if;
		end if;

	end if;

	if	(:new.dt_aprovacao is not null) and
		(:old.dt_aprovacao is null) and
		(ie_higienizar_unidade_w = 'A') then

		if	(ie_atualizar_unidade_livre_w = 'S')	then

			update	unidade_atendimento
			set	dt_higienizacao		= :new.dt_fim,
				ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'),
				nm_usuario_higienizacao	= :new.nm_usuario,
				nm_usuario_fim_higienizacao = :new.nm_usuario,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
			where	nr_seq_interno		= :new.nr_seq_unidade;

		elsif	(ie_status_unidade_w = 'E') then

			select	max(cd_unidade_basica),
				max(cd_unidade_compl),
				max(cd_setor_atendimento)
			into	cd_unidade_basica_w,
				cd_unidade_compl_w,
				cd_setor_atendimento_w
			from	unidade_atendimento
			where	nr_seq_interno = :new.nr_seq_unidade;


			atualizar_leito_manutencao(:new.nm_usuario, cd_unidade_basica_w, cd_unidade_compl_w, cd_setor_atendimento_w, 'G', 'S');

		else

			update	unidade_atendimento
			set	dt_higienizacao		= :new.dt_fim,
				ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R'),
				nm_usuario_higienizacao	= :new.nm_usuario,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate
			where	nr_seq_interno		= :new.nr_seq_unidade;

		end if;
	end if;

	if	(:new.dt_inicio is null) and
		(:old.dt_inicio is not null) and
		(ie_atualizar_status_ant_w = 'S') then

		update	unidade_atendimento
		set	dt_inicio_higienizacao	= :new.dt_inicio,
			dt_higienizacao		= null,
			nm_usuario_fim_higienizacao = null,
			ie_status_unidade	= decode(ie_status_ant_unidade_w,'P',decode(nr_atendimento,null,'H','P'),ie_status_ant_unidade_w),
			nm_usuario_higienizacao	= null,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= sysdate
		where	nr_seq_interno		= :new.nr_seq_unidade;

	end if;

	if	(:new.dt_fim is not null) and
		(:old.dt_fim is null) then
		begin
		select	cd_setor_atendimento
		into	cd_setor_atendimento_w
		from	unidade_atendimento
		where	nr_seq_interno = :new.nr_seq_unidade;

		open C01;
		loop
		fetch C01 into
			nr_seq_evento_lider_w;
		exit when C01%notfound;
			begin
			gerar_evento_lider_escala(nr_seq_evento_lider_w,null,null,null,:new.nm_usuario,:new.dt_fim,cd_setor_atendimento_w,:new.nr_seq_unidade);
			end;
		end loop;
		close C01;
		end;
	end if;

	if	(:new.dt_aprovacao is not null) and
		(:old.dt_aprovacao is null) then

		select	nvl(max(ie_ignorar_checklist),'N')
		into	ie_ignorar_checklist_w
		from	unidade_atendimento
		where	nr_seq_interno = :new.nr_seq_unidade;

		if	(ie_ignorar_checklist_w = 'N') then

			select 	max(nvl(ie_exige_check_list_aprov,'N'))
			into	ie_exige_check_list_aprov_w
			from	sl_servico
			where	nr_sequencia = :new.nr_seq_servico;

			if	(ie_exige_check_list_aprov_w = 'S') then

				select 	count(*)
				into	qt_itens_w
				from	sl_check_list_unid
				where	nr_seq_sl_unid = :new.nr_sequencia;

				if	(qt_itens_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(213233);
				end if;
			elsif	(ie_exige_check_list_aprov_w = 'N') then

				select	count(*)
				into	qt_regra_feriado_w
				from	sl_servico_regra_check
				where	nr_seq_servico = :new.nr_seq_servico;

				select 	count(*)
				into	qt_itens_w
				from	sl_check_list_unid
				where	nr_seq_sl_unid = :new.nr_sequencia;

				if	(qt_regra_feriado_w > 0)  then
					if	(obter_se_feriado(:new.cd_estabelecimento,:new.dt_inicio) = 0) and (qt_itens_w = 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(213234);
					elsif	(obter_se_feriado(:new.cd_estabelecimento,:new.dt_inicio) > 0) then

						select	max(nvl(ie_feriado,'N'))
						into	ie_feriado_w
						from	sl_servico_regra_check
						where	nr_seq_servico = :new.nr_seq_servico;

						if	(ie_feriado_w = 'S') and (qt_itens_w = 0)  then
							wheb_mensagem_pck.exibir_mensagem_abort(213241);
						end if;
					end if;
				end if;
			end if;
		end if;

		begin
		open c02;
		loop
		fetch C02 into
			nr_seq_evento_gerencia_w;
		exit when C02%notfound;
			begin
			gerar_evento_gerencia_escala(nr_seq_evento_gerencia_w,null,null,null,:new.nm_usuario,:new.nr_seq_unidade);
			end;
		end loop;
		close c02;
		end;
	end if;

if	(ie_higienizacao_ocup_w = 'S') and
	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'EE') then

	update	unidade_atendimento
	set		ie_status_unidade	= 'H'
	where	nr_seq_interno		= :new.nr_seq_unidade;
end if;

if	(ie_livre_ocup_w = 'S') and
	(:new.ie_status_serv <> :old.ie_status_serv) and
	(:new.ie_status_serv = 'A') then

	update	unidade_atendimento
	set		ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva),null,'L','R')
	where	nr_seq_interno		= :new.nr_seq_unidade;
end if;

end if;

<<Final>>
If	(:old.ie_prioridade <> :new.ie_prioridade) then
	gravar_log_alteracao(substr(:old.ie_prioridade,1,1), substr(:new.ie_prioridade,1,1), :new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'sl_unid_atend',ds_s_w,ds_c_w);
End if;
If	(:old.nr_seq_unidade <> :new.nr_seq_unidade) then
	gravar_log_alteracao(substr(:old.nr_seq_unidade,1,10), substr(:new.nr_seq_unidade,1,10), :new.nm_usuario,nr_seq_w,'NR_SEQ_UNIDADE',ie_log_w,ds_w,'sl_unid_atend',ds_s_w,ds_c_w);
End if;
qt_reg_w	:= 0;

If	(ie_dbms_alert_w = 'S') then
	Gestao_Serv_leito_alert_signal(nvl(:new.nm_usuario,'Tasy'));
End if;

commit;

end;
/


ALTER TABLE TASY.SL_UNID_ATEND ADD (
  CONSTRAINT SLUNIAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          896K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SL_UNID_ATEND ADD (
  CONSTRAINT SLUNIAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SLUNIAT_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_UNIDADE) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT SLUNIAT_PESFISI_FK 
 FOREIGN KEY (CD_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SLUNIAT_SLSERVI_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.SL_SERVICO (NR_SEQUENCIA),
  CONSTRAINT SLUNIAT_SLSERVI_FK2 
 FOREIGN KEY (NR_SEQ_SERVICO_EXEC) 
 REFERENCES TASY.SL_SERVICO (NR_SEQUENCIA),
  CONSTRAINT SLUNIAT_SLMOTCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.SL_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT SLUNIAT_SLMOHIG_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_HIGIENIZACAO) 
 REFERENCES TASY.SL_MOTIVO_HIGIENIZACAO (NR_SEQUENCIA),
  CONSTRAINT SLUNIAT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_HIGIENIZACAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SLUNIAT_SLMTVPS_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PAUSA) 
 REFERENCES TASY.SL_MOTIVO_PAUSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SL_UNID_ATEND TO NIVEL_1;

