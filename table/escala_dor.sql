ALTER TABLE TASY.ESCALA_DOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DOR
(
  CD_ESCALA_DOR        VARCHAR2(5 BYTE)         NOT NULL,
  DS_ESCALA_DOR        VARCHAR2(80 BYTE)        NOT NULL,
  IE_SEXO              VARCHAR2(1 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_IDADE_MIN         NUMBER(15,2),
  QT_IDADE_MIN_MES     NUMBER(15,2),
  QT_IDADE_MIN_DIA     NUMBER(15,2),
  QT_IDADE_MAX         NUMBER(15,2),
  QT_IDADE_MAX_MES     NUMBER(15,2),
  QT_IDADE_MAX_DIA     NUMBER(15,2),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_AUTOSIZE          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESCDOR_PK ON TASY.ESCALA_DOR
(CD_ESCALA_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_DOR_tp  after update ON TASY.ESCALA_DOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_ESCALA_DOR);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'ESCALA_DOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ESCALA_DOR ADD (
  CONSTRAINT ESCDOR_PK
 PRIMARY KEY
 (CD_ESCALA_DOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ESCALA_DOR TO NIVEL_1;

