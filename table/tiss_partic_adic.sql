ALTER TABLE TASY.TISS_PARTIC_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_PARTIC_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_PARTIC_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_PARTICIPANTE      VARCHAR2(255 BYTE),
  CD_INTERNO           VARCHAR2(100 BYTE),
  IE_FUNCAO            VARCHAR2(20 BYTE),
  SG_CONSELHO          VARCHAR2(40 BYTE),
  NR_CRM               VARCHAR2(40 BYTE),
  UF_CRM               VARCHAR2(20 BYTE),
  NR_CPF               VARCHAR2(20 BYTE),
  CD_CBOS              VARCHAR2(40 BYTE),
  IE_MEDICO_ATEND      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TSSPAAD_PK ON TASY.TISS_PARTIC_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TSSPAAD_PK
  MONITORING USAGE;


CREATE INDEX TASY.TSSPAAD_TISSRAA_FK_I ON TASY.TISS_PARTIC_ADIC
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TSSPAAD_TISSRAA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_PARTIC_ADIC_tp  after update ON TASY.TISS_PARTIC_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_FUNCAO,1,500);gravar_log_alteracao(substr(:old.IE_FUNCAO,1,4000),substr(:new.IE_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCAO',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_PARTICIPANTE,1,500);gravar_log_alteracao(substr(:old.NM_PARTICIPANTE,1,4000),substr(:new.NM_PARTICIPANTE,1,4000),:new.nm_usuario,nr_seq_w,'NM_PARTICIPANTE',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_INTERNO,1,500);gravar_log_alteracao(substr(:old.CD_INTERNO,1,4000),substr(:new.CD_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNO',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CBOS,1,500);gravar_log_alteracao(substr(:old.CD_CBOS,1,4000),substr(:new.CD_CBOS,1,4000),:new.nm_usuario,nr_seq_w,'CD_CBOS',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_CRM,1,500);gravar_log_alteracao(substr(:old.NR_CRM,1,4000),substr(:new.NR_CRM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CRM',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.UF_CRM,1,500);gravar_log_alteracao(substr(:old.UF_CRM,1,4000),substr(:new.UF_CRM,1,4000),:new.nm_usuario,nr_seq_w,'UF_CRM',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_CPF,1,500);gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.SG_CONSELHO,1,500);gravar_log_alteracao(substr(:old.SG_CONSELHO,1,4000),substr(:new.SG_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'SG_CONSELHO',ie_log_w,ds_w,'TISS_PARTIC_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_PARTIC_ADIC ADD (
  CONSTRAINT TSSPAAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_PARTIC_ADIC ADD (
  CONSTRAINT TSSPAAD_TISSRAA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.TISS_REGRA_PARTIC_ADIC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TISS_PARTIC_ADIC TO NIVEL_1;

