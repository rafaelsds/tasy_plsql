ALTER TABLE TASY.GER_CARGA_INICIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GER_CARGA_INICIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.GER_CARGA_INICIAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_INICIO_LEITURA     DATE,
  DT_FIM_LEITURA        DATE,
  NR_SEQ_TIPO_CARGA     NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA_CONV     NUMBER(10),
  IE_ATUALIZAR          VARCHAR2(1 BYTE)        NOT NULL,
  DT_EXCLUSAO           DATE,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_USUARIO_EXC        VARCHAR2(15 BYTE),
  IE_TIPO               NUMBER(3),
  DS_CARGA              VARCHAR2(255 BYTE),
  DT_INICIO_IMPORTACAO  DATE,
  IE_ACAO               VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GERCARI_GERTIPCRG_FK_I ON TASY.GER_CARGA_INICIAL
(NR_SEQ_TIPO_CARGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GERCARI_PK ON TASY.GER_CARGA_INICIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GERCARI_RECOMEX_FK_I ON TASY.GER_CARGA_INICIAL
(NR_SEQ_REGRA_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GER_CARGA_INICIAL_AFTER
after insert or update ON TASY.GER_CARGA_INICIAL for each row
declare

cursor c01 is
select	a.*
from	ger_tipo_carga_arq a
where	a.nr_seq_tipo_carga = :new.nr_seq_tipo_carga
order by ie_ordem;

c01_w	c01%rowtype;

begin

if	(:new.nr_seq_tipo_carga <> :old.nr_seq_tipo_carga) or
	(:new.nr_seq_tipo_carga is not null and :old.nr_seq_tipo_carga is null) then

	if	(updating) then
		delete from ger_carga_arq
		where	nr_seq_carga	= :new.nr_sequencia;
	end if;

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;

		insert into ger_carga_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_tipo_arq,
			nr_seq_carga,
			ie_obrigatorio)
		values	(ger_carga_arq_seq.nextval,
			sysdate,
			:new.nm_usuario,
			c01_w.nr_sequencia,
			:new.nr_sequencia,
			c01_w.ie_obrigatorio);
	end loop;
	close C01;
end if;

end GER_CARGA_INICIAL_AFTER;
/


ALTER TABLE TASY.GER_CARGA_INICIAL ADD (
  CONSTRAINT GERCARI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GER_CARGA_INICIAL ADD (
  CONSTRAINT GERCARI_RECOMEX_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONV) 
 REFERENCES TASY.REGRA_CONV_MEIO_EXT (NR_SEQUENCIA));

GRANT SELECT ON TASY.GER_CARGA_INICIAL TO NIVEL_1;

