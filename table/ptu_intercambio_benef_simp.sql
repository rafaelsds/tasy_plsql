ALTER TABLE TASY.PTU_INTERCAMBIO_BENEF_SIMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_INTERCAMBIO_BENEF_SIMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_INTERCAMBIO_BENEF_SIMP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_INTERCAMBIO       NUMBER(10)           DEFAULT null,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_SEGURADO          NUMBER(10),
  CD_UNIMED                VARCHAR2(4 BYTE),
  CD_BENEFICIARIO          VARCHAR2(20 BYTE),
  CD_CPF                   VARCHAR2(11 BYTE),
  NM_COMPLETO              VARCHAR2(255 BYTE),
  DT_NASCIMENTO            DATE,
  IE_TIPO_CONTRATO_LOCAL   VARCHAR2(1 BYTE),
  DT_COMPARTILHAMENTO      DATE,
  DT_FIM_COMPARTILHAMENTO  DATE,
  CD_USUARIO_PLANO         VARCHAR2(30 BYTE),
  IE_STATUS                VARCHAR2(1 BYTE)     DEFAULT null,
  DT_CANCELAMENTO          DATE                 DEFAULT null,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(30 BYTE)    DEFAULT null,
  NR_SEQ_MOTIVO_REJEICAO   NUMBER(10)           DEFAULT null,
  IE_PROPRIO_RECEBIDO      VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_RECEB        NUMBER(10),
  CD_UNIMED_DESTINO        VARCHAR2(4 BYTE),
  CD_UNIMED_ORIGEM         VARCHAR2(4 BYTE),
  DS_INCONSISTENCIA        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUIBSP_PK ON TASY.PTU_INTERCAMBIO_BENEF_SIMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUIBSP_PLSMRCB_FK_I ON TASY.PTU_INTERCAMBIO_BENEF_SIMP
(NR_SEQ_MOTIVO_REJEICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUIBSP_PLSSEGU_FK_I ON TASY.PTU_INTERCAMBIO_BENEF_SIMP
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUIBSP_PTUILTR_FK_I ON TASY.PTU_INTERCAMBIO_BENEF_SIMP
(NR_SEQ_LOTE_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUIBSP_PTUINTE_FK_I ON TASY.PTU_INTERCAMBIO_BENEF_SIMP
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_INTERCAMBIO_BENEF_SIMP ADD (
  CONSTRAINT PTUIBSP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PTU_INTERCAMBIO_BENEF_SIMP ADD (
  CONSTRAINT PTUIBSP_PLSMRCB_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REJEICAO) 
 REFERENCES TASY.PLS_MOTIVO_REJ_REC_BENEF (NR_SEQUENCIA),
  CONSTRAINT PTUIBSP_PTUILTR_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RECEB) 
 REFERENCES TASY.PTU_INTERCAMBIO_LOTE_RECEB (NR_SEQUENCIA),
  CONSTRAINT PTUIBSP_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PTUIBSP_PTUINTE_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PTU_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_INTERCAMBIO_BENEF_SIMP TO NIVEL_1;

