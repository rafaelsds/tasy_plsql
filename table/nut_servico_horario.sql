ALTER TABLE TASY.NUT_SERVICO_HORARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_SERVICO_HORARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_SERVICO_HORARIO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_SERVICO              NUMBER(10)        NOT NULL,
  DS_HORARIOS                 VARCHAR2(5 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  DS_HORARIO_CORTE_INIC       VARCHAR2(5 BYTE),
  DS_HORARIO_CORTE_FIM        VARCHAR2(5 BYTE),
  DS_HORARIOS_FIM             VARCHAR2(5 BYTE),
  DS_HORARIOS_HTML            DATE,
  DS_HORARIOS_FIM_HTML        DATE,
  DS_HORARIO_CORTE_INIC_HTML  DATE,
  DS_HORARIO_CORTE_FIM_HTML   DATE,
  DS_HORARIO_LIMITE           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SETSEHO_NUTSERV_FK_I ON TASY.NUT_SERVICO_HORARIO
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETSEHO_NUTSERV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SETSEHO_PK ON TASY.NUT_SERVICO_HORARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SETSEHO_SETATEN_FK_I ON TASY.NUT_SERVICO_HORARIO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SETSEHO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.NUT_HORARIO_UPDATE_HTML
BEFORE INSERT OR UPDATE ON TASY.NUT_SERVICO_HORARIO FOR EACH ROW
DECLARE

cd_estabelecimento_w 			estabelecimento.cd_estabelecimento%type;
nm_usuario_w 					usuario.nm_usuario%type;
ie_html_w						varchar2(1);

BEGIN
ie_html_w := 'N';

/*rotina específica para a plataforma HTML5 */
/* campos ds_horarios date (para utilização do dateTimePickerr) em HTML x ds_horarios String em Java*/
if ((:new.DS_HORARIOS_HTML is not null and (:new.DS_HORARIOS_HTML <> :old.DS_HORARIOS_HTML or :old.DS_HORARIOS_HTML is null)) or
	(:new.DS_HORARIOS_FIM_HTML is not null	and (:new.DS_HORARIOS_FIM_HTML <> :old.DS_HORARIOS_FIM_HTML or :old.DS_HORARIOS_FIM_HTML is null)) or
	(:new.DS_HORARIO_CORTE_INIC_HTML is not null and (:new.DS_HORARIO_CORTE_INIC_HTML <> :old.DS_HORARIO_CORTE_INIC_HTML or :old.DS_HORARIO_CORTE_INIC_HTML is null)) or
	(:new.DS_HORARIO_CORTE_FIM_HTML is not null and (:new.DS_HORARIO_CORTE_FIM_HTML <> :old.DS_HORARIO_CORTE_FIM_HTML or :old.DS_HORARIO_CORTE_FIM_HTML is null))) then
	begin
	ie_html_w := 'S';
	cd_estabelecimento_w 	:= wheb_usuario_pck.get_cd_estabelecimento;
	nm_usuario_w 			:= wheb_usuario_pck.get_nm_usuario;

	:new.DS_HORARIOS 			:= to_char(:new.ds_horarios_html, 'HH24:MI');
	:new.DS_HORARIOS_FIM 		:= to_char(:new.ds_horarios_fim_html, 'HH24:MI');
	:new.DS_HORARIO_CORTE_INIC	:= to_char(:new.ds_horario_corte_inic_html, 'HH24:MI');
	:new.DS_HORARIO_CORTE_FIM 	:= to_char(:new.ds_horario_corte_fim_html, 'HH24:MI');
	end;
end if;

if (ie_html_w = 'N' and
	(:new.DS_HORARIOS is not null and (:new.DS_HORARIOS <> :old.DS_HORARIOS or :old.DS_HORARIOS is null)) or
	(:new.DS_HORARIOS_FIM is not null	and (:new.DS_HORARIOS_FIM <> :old.DS_HORARIOS_FIM or :old.DS_HORARIOS_FIM is null)) or
	(:new.DS_HORARIO_CORTE_INIC is not null and (:new.DS_HORARIO_CORTE_INIC <> :old.DS_HORARIO_CORTE_INIC or :old.DS_HORARIO_CORTE_INIC is null)) or
	(:new.DS_HORARIO_CORTE_FIM is not null and (:new.DS_HORARIO_CORTE_FIM <> :old.DS_HORARIO_CORTE_FIM or :old.DS_HORARIO_CORTE_FIM is null))) then
	begin
	cd_estabelecimento_w 	:= wheb_usuario_pck.get_cd_estabelecimento;
	nm_usuario_w 			:= wheb_usuario_pck.get_nm_usuario;

	:new.DS_HORARIOS_HTML 			:= to_date(:new.ds_horarios, 'HH24:MI');
	:new.DS_HORARIOS_FIM_HTML 		:= to_date(:new.ds_horarios_fim, 'HH24:MI');
	:new.DS_HORARIO_CORTE_INIC_HTML	:= to_date(:new.ds_horario_corte_inic, 'HH24:MI');
	:new.DS_HORARIO_CORTE_FIM_HTML 	:= to_date(:new.ds_horario_corte_fim, 'HH24:MI');
	end;
end if;

END;
/


ALTER TABLE TASY.NUT_SERVICO_HORARIO ADD (
  CONSTRAINT SETSEHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_SERVICO_HORARIO ADD (
  CONSTRAINT SETSEHO_NUTSERV_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.NUT_SERVICO (NR_SEQUENCIA),
  CONSTRAINT SETSEHO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.NUT_SERVICO_HORARIO TO NIVEL_1;

