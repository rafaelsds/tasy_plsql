ALTER TABLE TASY.AUSENCIA_MENSAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUSENCIA_MENSAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUSENCIA_MENSAGEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AUSENCIA       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_PERFIL_DEST        NUMBER(5),
  NM_USUARIO_DEST       VARCHAR2(15 BYTE),
  DS_MENSAGEM           VARCHAR2(255 BYTE),
  NR_SEQ_GRUPO_USUARIO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUSMENS_AUSTASY_FK_I ON TASY.AUSENCIA_MENSAGEM
(NR_SEQ_AUSENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUSMENS_AUSTASY_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUSMENS_GRUUSUA_FK_I ON TASY.AUSENCIA_MENSAGEM
(NR_SEQ_GRUPO_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUSMENS_GRUUSUA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUSMENS_PERFIL_FK_I ON TASY.AUSENCIA_MENSAGEM
(CD_PERFIL_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUSMENS_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUSMENS_PK ON TASY.AUSENCIA_MENSAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AUSENCIA_MENSAGEM_INSERT
AFTER INSERT ON TASY.AUSENCIA_MENSAGEM FOR EACH ROW
DECLARE

ds_comunicado_w			varchar2(2000);
nr_sequencia_w			number(10,0);
dt_inicio_w			date;
dt_fim_w			date;
ie_geraL_w			varchar2(1)	:= 'N';
nr_seq_classif_w		Number(10,0);

expressao1_w	varchar2(255) := obter_texto_tasy(880715, null);
expressao2_w	varchar2(255) := obter_texto_tasy(880716, null);
expressao3_w	varchar2(255) := obter_texto_tasy(880717, null);
expressao4_w	varchar2(255) := obter_texto_tasy(880718, null);

begin


if	(:new.cd_perfil_dest is null) and
	(:new.nm_usuario_dest is null) and
	(:new.nr_seq_grupo_usuario is null) then
	ie_geral_w	:= 'S';
end if;


select	dt_inicio,
	dt_fim
into	dt_inicio_w,
	dt_fim_w
from	ausencia_tasy
where	nr_sequencia	= :new.nr_seq_ausencia;

select	comunic_interna_seq.nextval
into	nr_sequencia_w
from	dual;

select obter_classif_comunic('F')
into	nr_seq_classif_w
from dual;

ds_comunicado_w			:= 	expressao1_w || ' ' ||
				PKG_DATE_FORMATERS.to_varchar(dt_inicio_w,'timestamp',wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario) ||
				' ' || expressao2_w || ' ' || PKG_DATE_FORMATERS.to_varchar(dt_fim_w,'timestamp',wheb_usuario_pck.get_cd_estabelecimento,wheb_usuario_pck.get_nm_usuario) ||
				' ' || expressao3_w || ' ' ||
				:new.ds_mensagem;


insert	into comunic_interna
	(dt_comunicado,
	ds_titulo,
	ds_comunicado,
	nm_usuario,
	dt_atualizacao,
	ie_geral,
	nm_usuario_destino,
	cd_perfil,
	nr_sequencia,
	nr_seq_classif,
	ie_gerencial,
	ds_grupo,
	dt_liberacao)
values	(sysdate,
	expressao4_w,
	ds_comunicado_w,
	:new.nm_usuario,
	sysdate,
	ie_geral_w,
	:new.nm_usuario_dest || ',',
	:new.cd_perfil_dest,
	nr_sequencia_w,
	nr_seq_classif_w,
	'N',
	:new.nr_seq_grupo_usuario,
	sysdate);


end;
/


ALTER TABLE TASY.AUSENCIA_MENSAGEM ADD (
  CONSTRAINT AUSMENS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUSENCIA_MENSAGEM ADD (
  CONSTRAINT AUSMENS_AUSTASY_FK 
 FOREIGN KEY (NR_SEQ_AUSENCIA) 
 REFERENCES TASY.AUSENCIA_TASY (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUSMENS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_DEST) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT AUSMENS_GRUUSUA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_USUARIO) 
 REFERENCES TASY.GRUPO_USUARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AUSENCIA_MENSAGEM TO NIVEL_1;

