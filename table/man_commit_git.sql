ALTER TABLE TASY.MAN_COMMIT_GIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_COMMIT_GIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_COMMIT_GIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM_SERV    NUMBER(10),
  DS_BRANCH            VARCHAR2(255 BYTE),
  DS_PROJETO           VARCHAR2(255 BYTE),
  IE_REG_PEND_CHECKED  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANCGIT_I1 ON TASY.MAN_COMMIT_GIT
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANCGIT_PK ON TASY.MAN_COMMIT_GIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_commit_git_after
after insert ON TASY.MAN_COMMIT_GIT for each row
declare

ie_plataforma_w		man_ordem_servico.ie_plataforma%type;

begin

select	max(ie_plataforma)
into	ie_plataforma_w
from	man_ordem_servico
where	nr_sequencia = :new.nr_seq_ordem_serv;

if (ie_plataforma_w = 'H') then
	reg_test_plan_pck.add_pendency('F', :new.nr_seq_ordem_serv, null, :new.nm_usuario_nrec);
end if;

end;
/


ALTER TABLE TASY.MAN_COMMIT_GIT ADD (
  CONSTRAINT MANCGIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MAN_COMMIT_GIT TO NIVEL_1;

