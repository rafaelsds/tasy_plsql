ALTER TABLE TASY.TASY_JOBS_ATUALIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_JOBS_ATUALIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_JOBS_ATUALIZACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_JOB               NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_COMANDO           VARCHAR2(128 BYTE)       NOT NULL,
  NR_SEQ_ATUALIZACAO   NUMBER(10),
  IE_EXECUTADO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAJOBATUA_ATUVERS_FK_I ON TASY.TASY_JOBS_ATUALIZACAO
(NR_SEQ_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TAJOBATUA_PK ON TASY.TASY_JOBS_ATUALIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_JOBS_ATUALIZACAO ADD (
  CONSTRAINT TAJOBATUA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_JOBS_ATUALIZACAO ADD (
  CONSTRAINT TAJOBATUA_ATUVERS_FK 
 FOREIGN KEY (NR_SEQ_ATUALIZACAO) 
 REFERENCES TASY.ATUALIZACAO_VERSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_JOBS_ATUALIZACAO TO NIVEL_1;

