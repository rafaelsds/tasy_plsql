DROP TABLE TASY.SSJ_LOG_GERACAO_INTERFACE_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SSJ_LOG_GERACAO_INTERFACE_MENS
(
  NR_SEQUENCIA    INTEGER,
  DT_ATUALIZACAO  DATE,
  CD_CGC          VARCHAR2(14 BYTE),
  DT_REFERENCIA   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.SSJ_LOG_GERACAO_INTERFACE_MENS TO NIVEL_1;

