ALTER TABLE TASY.PLS_FATURA_MOTIVO_IMP_COB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FATURA_MOTIVO_IMP_COB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FATURA_MOTIVO_IMP_COB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_MOTIVO            VARCHAR2(3 BYTE)         NOT NULL,
  NR_SEQ_FATURA        NUMBER(10),
  NR_SEQ_CONTA         NUMBER(10),
  NR_SEQ_LOTE_FAT      NUMBER(10),
  NR_SEQ_CONTA_SUS     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSFMIC_I1 ON TASY.PLS_FATURA_MOTIVO_IMP_COB
(NR_SEQ_FATURA, NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSFMIC_PK ON TASY.PLS_FATURA_MOTIVO_IMP_COB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFMIC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSFMIC_PLSCOME_FK_I ON TASY.PLS_FATURA_MOTIVO_IMP_COB
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFMIC_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFMIC_PLSFATU_FK_I ON TASY.PLS_FATURA_MOTIVO_IMP_COB
(NR_SEQ_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFMIC_PLSFATU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_FATURA_MOTIVO_IMP_COB ADD (
  CONSTRAINT PLSFMIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FATURA_MOTIVO_IMP_COB ADD (
  CONSTRAINT PLSFMIC_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSFMIC_PLSFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FATURA_MOTIVO_IMP_COB TO NIVEL_1;

