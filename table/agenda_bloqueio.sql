ALTER TABLE TASY.AGENDA_BLOQUEIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_BLOQUEIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_BLOQUEIO
(
  CD_AGENDA              NUMBER(10)             NOT NULL,
  DT_INICIAL             DATE                   NOT NULL,
  DT_FINAL               DATE                   NOT NULL,
  IE_MOTIVO_BLOQUEIO     VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  IE_DIA_SEMANA          NUMBER(5),
  HR_INICIO_BLOQUEIO     DATE,
  HR_FINAL_BLOQUEIO      DATE,
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  IE_CLASSIF_BLOQUEIO    VARCHAR2(5 BYTE),
  QT_AGENDA_BLOQ         NUMBER(3),
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_BLOQ_AG  NUMBER(10),
  DS_OBS_INTERNA         VARCHAR2(255 BYTE),
  QT_DIAS_ANTECEDENCIA   NUMBER(3),
  DT_SOLICITACAO         DATE,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEBLOQ_AGENDA_FK_I ON TASY.AGENDA_BLOQUEIO
(CD_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEBLOQ_PESFISI_FK_I ON TASY.AGENDA_BLOQUEIO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEBLOQ_PK ON TASY.AGENDA_BLOQUEIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Agenda_Bloqueio_Atual
before insert or update ON TASY.AGENDA_BLOQUEIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN

if	(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_final) < ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_inicial)) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(212526);
end if;

if	(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_final) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate + 730)) or
	(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:old.dt_final) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate + 730))then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(355087);
end if;

begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	dt_agenda between :new.dt_inicial and :new.dt_final
and	cd_agenda	 = :new.cd_agenda;

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	dt_agenda between :old.dt_inicial and :old.dt_final
and	cd_agenda	 = :new.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Bloqueio_Insert
after insert or update ON TASY.AGENDA_BLOQUEIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
cd_tipo_agenda_w	agenda.cd_tipo_agenda%type;
BEGIN
begin

select	max(cd_tipo_agenda)
into	cd_tipo_agenda_w
from	agenda
where	cd_agenda = :new.cd_agenda;

if	(cd_tipo_agenda_w = '3' or cd_tipo_agenda_w = '4' or cd_tipo_agenda_w = '5') then

	delete	from agenda_consulta
	where	trunc(dt_agenda) between :new.dt_inicial and :new.dt_final
	and	cd_agenda	= :new.cd_agenda
	and	ie_status_agenda = 'L';

elsif	(cd_tipo_agenda_w = '2') then

	delete from agenda_paciente
	where	trunc(dt_agenda) between :new.dt_inicial and :new.dt_final
	and	cd_agenda	= :new.cd_agenda
	and	ie_status_agenda = 'L';

end if;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Bloqueio_Delete
before delete ON TASY.AGENDA_BLOQUEIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and		dt_agenda between :old.dt_inicial and :old.dt_final
and		cd_agenda	 = :old.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_BLOQUEIO_tp  after update ON TASY.AGENDA_BLOQUEIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_FINAL,1,4000),substr(:new.DT_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL',ie_log_w,ds_w,'AGENDA_BLOQUEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIAL,1,4000),substr(:new.DT_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIAL',ie_log_w,ds_w,'AGENDA_BLOQUEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIA_SEMANA,1,4000),substr(:new.IE_DIA_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIA_SEMANA',ie_log_w,ds_w,'AGENDA_BLOQUEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.HR_FINAL_BLOQUEIO,1,4000),substr(:new.HR_FINAL_BLOQUEIO,1,4000),:new.nm_usuario,nr_seq_w,'HR_FINAL_BLOQUEIO',ie_log_w,ds_w,'AGENDA_BLOQUEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.HR_INICIO_BLOQUEIO,1,4000),substr(:new.HR_INICIO_BLOQUEIO,1,4000),:new.nm_usuario,nr_seq_w,'HR_INICIO_BLOQUEIO',ie_log_w,ds_w,'AGENDA_BLOQUEIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AGENDA_BLOQUEIO ADD (
  CONSTRAINT AGEBLOQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_BLOQUEIO ADD (
  CONSTRAINT AGEBLOQ_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGEBLOQ_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGENDA_BLOQUEIO TO NIVEL_1;

GRANT SELECT ON TASY.AGENDA_BLOQUEIO TO ROBOCMTECNOLOGIA;

