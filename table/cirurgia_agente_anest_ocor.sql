ALTER TABLE TASY.CIRURGIA_AGENTE_ANEST_OCOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_AGENTE_ANEST_OCOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_AGENTE_ANEST_OCOR
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CIRUR_AGENTE        NUMBER(10)         NOT NULL,
  DT_INICIO_ADM              DATE,
  DT_FINAL_ADM               DATE,
  QT_DOSE                    NUMBER(15,3),
  IE_APLIC_BOLUS             VARCHAR2(1 BYTE)   NOT NULL,
  QT_VELOCIDADE_INF          NUMBER(15,4),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  QT_DOSE_ATAQUE             NUMBER(15,3),
  QT_HALOG_INS               NUMBER(15,4),
  QT_DOSE_TOTAL              NUMBER(15,4),
  IE_MODO_REGISTRO           VARCHAR2(1 BYTE),
  QT_UTILIZADA               NUMBER(15,3),
  QT_MINUTOS                 NUMBER(15,3),
  CD_UNID_MED                VARCHAR2(30 BYTE),
  DS_IDENTIFICACAO_BOLSA     VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NM_USUARIO_SECOND          VARCHAR2(20 BYTE),
  QT_TOTAL_DOSE              NUMBER(15,4),
  IE_INTEGRACAO              VARCHAR2(1 BYTE),
  NR_PROTOCOLO_PEPO          NUMBER(10),
  NR_SEQ_EVENTO              NUMBER(10),
  DT_AVISO_REPIQUE           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRAGAO_CIAGANE_FK_I ON TASY.CIRURGIA_AGENTE_ANEST_OCOR
(NR_SEQ_CIRUR_AGENTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRAGAO_PK ON TASY.CIRURGIA_AGENTE_ANEST_OCOR
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRAGAO_TASASDI_FK_I ON TASY.CIRURGIA_AGENTE_ANEST_OCOR
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRAGAO_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRAGAO_TASASDI_FK2_I ON TASY.CIRURGIA_AGENTE_ANEST_OCOR
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRAGAO_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRAGAO_UNIMEDI_FK_I ON TASY.CIRURGIA_AGENTE_ANEST_OCOR
(CD_UNID_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRAGAO_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cirurgia_agente_anest_atual
BEFORE update or insert ON TASY.CIRURGIA_AGENTE_ANEST_OCOR FOR EACH ROW
DECLARE
ie_consiste_w		varchar2(2) := 'N';
dt_inicio_real_w	date;
cd_estabelecimento_w	number(4,0);
nr_seq_pepo_w		number(10,0);
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_tipo_w			varchar2(10);
nr_cirurgia_w 		cirurgia_agente_anestesico.nr_cirurgia%type;
ie_tipo_cir_ag_w	cirurgia_agente_anestesico.ie_tipo%type;
ie_libera_medic_pepo_w		varchar2(1);
ie_libera_material_pepo_w	varchar2(1);
ie_libera_agente_pepo_w		varchar2(1);
ie_libera_hemoder_pepo_w	varchar2(1);
ie_libera_terapia_pepo_w	varchar2(1);
cd_setor_atendimento_w		cirurgia.cd_setor_atendimento%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

select	nvl(max(b.nr_seq_pepo),0)
into	nr_seq_pepo_w
from 	cirurgia_agente_anestesico b
where	b.nr_sequencia = :new.nr_seq_cirur_agente
and	nvl(b.ie_situacao,'A') = 'A';

if	(nr_seq_pepo_w > 0) then
	select	min(a.dt_inicio_real),
		min(a.cd_estabelecimento)
	into	dt_inicio_real_w,
		cd_estabelecimento_w
	from 	cirurgia a,
		cirurgia_agente_anestesico b
	where	a.nr_seq_pepo = b.nr_seq_pepo
	and	nvl(b.ie_situacao,'A') = 'A'
	and	b.nr_sequencia = :new.nr_seq_cirur_agente;
else
	select	max(a.dt_inicio_real),
		max(a.cd_estabelecimento)
	into	dt_inicio_real_w,
		cd_estabelecimento_w
	from 	cirurgia a,
		cirurgia_agente_anestesico b
	where	a.nr_cirurgia = b.nr_cirurgia
	and	nvl(b.ie_situacao,'A') = 'A'
	and	b.nr_sequencia = :new.nr_seq_cirur_agente;
end if;


if 	((:new.dt_inicio_adm is not null) and (:new.dt_final_adm is not null)) and
	(:new.dt_final_adm < :new.dt_inicio_adm) and
    (:NEW.dt_inativacao is null)	then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(202905, 'DT_INICIO_ADM_P='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio_adm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_FINAL_ADM_P='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_final_adm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
end if;

Obter_Param_Usuario(872,93,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_consiste_w);

if	(ie_consiste_w = 'S') and
	((dt_inicio_real_w is null) or
	(:new.dt_inicio_adm < dt_inicio_real_w)) and
    (:NEW.dt_inativacao is null)	then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(202909);
end if;

select	max(ie_libera_medic_pepo),
	max(ie_libera_material_pepo),
	max(ie_libera_agente_pepo),
	max(ie_libera_hemoder_pepo),
	max(ie_libera_terapia_pepo)
into	ie_libera_medic_pepo_w,
	ie_libera_material_pepo_w,
	ie_libera_agente_pepo_w,
	ie_libera_hemoder_pepo_w,
	ie_libera_terapia_pepo_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

select	ie_tipo
into	ie_tipo_cir_ag_w
from cirurgia_agente_anestesico
where nr_sequencia = :new.NR_SEQ_CIRUR_AGENTE;

if	(ie_tipo_cir_ag_w = 1) and (nvl(ie_libera_agente_pepo_w,'N') = 'S') then --Agentes anestesicos
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'ADAA';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XADAA';
	end if;
elsif	(ie_tipo_cir_ag_w = 2) and (nvl(ie_libera_terapia_pepo_w,'N') = 'S') then --Terapia hidroeletrolitica
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'ADTH';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XADTH';
	end if;
elsif	(ie_tipo_cir_ag_w = 3) and (nvl(ie_libera_medic_pepo_w,'N') = 'S') then --Medicamentos
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'ADMD';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XADMD';
	end if;
elsif	(ie_tipo_cir_ag_w = 5) and (nvl(ie_libera_hemoder_pepo_w,'N') = 'S') then --Hemocomponente
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'ADHE';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XADHE';
	end if;

end if;

if	(ie_tipo_w	is not null) then
select	max(nr_cirurgia),
		max(nr_seq_pepo)
into	nr_cirurgia_w,
		nr_seq_pepo_w
from cirurgia_agente_anestesico
where nr_sequencia = :new.NR_SEQ_CIRUR_AGENTE;

select	max(c.nr_atendimento),
		max(c.cd_pessoa_fisica),
		max(c.cd_setor_atendimento)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w,
		cd_setor_atendimento_w
	from	cirurgia c
	where	c.nr_cirurgia = nr_cirurgia_w;

	if 	(cd_pessoa_fisica_w is null) then
		select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	pepo_cirurgia c
		where	c.nr_sequencia = nr_seq_pepo_w;
	end if;

	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
end if;

<<final>>
null;

end;
/


ALTER TABLE TASY.CIRURGIA_AGENTE_ANEST_OCOR ADD (
  CONSTRAINT CIRAGAO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA_AGENTE_ANEST_OCOR ADD (
  CONSTRAINT CIRAGAO_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRAGAO_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRAGAO_CIAGANE_FK 
 FOREIGN KEY (NR_SEQ_CIRUR_AGENTE) 
 REFERENCES TASY.CIRURGIA_AGENTE_ANESTESICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRAGAO_UNIMEDI_FK 
 FOREIGN KEY (CD_UNID_MED) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.CIRURGIA_AGENTE_ANEST_OCOR TO NIVEL_1;

