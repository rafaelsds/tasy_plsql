ALTER TABLE TASY.PTU_QUESTIONAMENTO_RRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_QUESTIONAMENTO_RRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_QUESTIONAMENTO_RRS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONTESTACAO    NUMBER(10)              NOT NULL,
  CD_UNIMED             VARCHAR2(10 BYTE),
  NR_LOTE               NUMBER(8),
  ID_BENEF              VARCHAR2(13 BYTE),
  NR_NOTA               VARCHAR2(20 BYTE),
  NM_BENEFICIARIO       VARCHAR2(25 BYTE),
  NR_NOTA_NUMERICO      VARCHAR2(20 BYTE),
  DT_REEMBOLSO          DATE,
  NR_CNPJ_CPF           VARCHAR2(14 BYTE),
  NM_PRESTADOR          VARCHAR2(70 BYTE),
  NR_SEQ_CONTA          NUMBER(10),
  NR_SEQ_NOTA_COBR_RRS  NUMBER(10),
  IE_TIPO_TABELA_TISS   VARCHAR2(5 BYTE),
  ID_REEMBOLSO          VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUQRRS_PK ON TASY.PTU_QUESTIONAMENTO_RRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQRRS_PLSCOME_FK_I ON TASY.PTU_QUESTIONAMENTO_RRS
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQRRS_PTUCAMC_FK_I ON TASY.PTU_QUESTIONAMENTO_RRS
(NR_SEQ_CONTESTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQRRS_PTUCRRS_FK_I ON TASY.PTU_QUESTIONAMENTO_RRS
(NR_SEQ_NOTA_COBR_RRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_questionamento_rrs_atual
before insert or update ON TASY.PTU_QUESTIONAMENTO_RRS for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

begin
:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/


ALTER TABLE TASY.PTU_QUESTIONAMENTO_RRS ADD (
  CONSTRAINT PTUQRRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_QUESTIONAMENTO_RRS ADD (
  CONSTRAINT PTUQRRS_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PTUQRRS_PTUCAMC_FK 
 FOREIGN KEY (NR_SEQ_CONTESTACAO) 
 REFERENCES TASY.PTU_CAMARA_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PTUQRRS_PTUCRRS_FK 
 FOREIGN KEY (NR_SEQ_NOTA_COBR_RRS) 
 REFERENCES TASY.PTU_NOTA_COBRANCA_RRS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_QUESTIONAMENTO_RRS TO NIVEL_1;

