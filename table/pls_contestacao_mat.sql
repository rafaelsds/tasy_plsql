ALTER TABLE TASY.PLS_CONTESTACAO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTESTACAO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTESTACAO_MAT
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_MAT            NUMBER(10),
  VL_MATERIAL                 NUMBER(15,2)      NOT NULL,
  NR_SEQ_CONTESTACAO          NUMBER(10)        NOT NULL,
  QT_MATERIAL                 NUMBER(12,4)      NOT NULL,
  QT_CONTESTADA               NUMBER(12,4)      NOT NULL,
  VL_CONTESTADO               NUMBER(15,2)      NOT NULL,
  QT_ACEITA                   NUMBER(12,4),
  VL_ACEITO                   NUMBER(15,2),
  NR_SEQ_MOTIVO_GLOSA_ACEITA  NUMBER(10),
  NR_SEQ_MOTIVO_GLOSA_NEG     NUMBER(10),
  QT_ATUAL                    NUMBER(12,4),
  VL_ATUAL                    NUMBER(15,2),
  NR_SEQ_FATURA_MAT           NUMBER(10),
  NR_SEQ_A500                 NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCMAT_PK ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCMAT_PLSCOMAT_FK_I ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCMAT_PLSCOMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCMAT_PLSCTST_FK_I ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQ_CONTESTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCMAT_PLSCTST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCMAT_PLSFATM_FK_I ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQ_FATURA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCMAT_PLSFATM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCMAT_PLSMGNE_FK_I ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQ_MOTIVO_GLOSA_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCMAT_PLSMGNE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCMAT_PLSMOGA_FK_I ON TASY.PLS_CONTESTACAO_MAT
(NR_SEQ_MOTIVO_GLOSA_ACEITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCMAT_PLSMOGA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_contestacao_mat_update
before insert or update ON TASY.PLS_CONTESTACAO_MAT for each row
declare
nr_seq_conta_mat_w	pls_conta_mat.nr_sequencia%type;
nr_seq_analise_w	pls_conta.nr_seq_analise%type;

begin
select	max(b.nr_sequencia),
	max(a.nr_seq_analise)
into	nr_seq_conta_mat_w,
	nr_seq_analise_w
from	pls_conta		a,
	pls_conta_mat		b
where	b.nr_seq_conta		= a.nr_sequencia
and	b.nr_sequencia		= :old.nr_seq_conta_mat;

if	((:new.vl_contestado > :old.vl_material) and ((:old.vl_contestado <> :new.vl_contestado) or (:old.vl_material <> :new.vl_material))) then
	wheb_mensagem_pck.exibir_mensagem_abort(170908, 'NR_SEQ_ANALISE_W=' || nr_seq_analise_w ||';'||
							'NR_SEQ_PROC_W=' || nr_seq_conta_mat_w);
end if;

if	((:new.qt_contestada > :old.qt_material) and ((:old.qt_contestada <> :new.qt_contestada) or (:old.qt_material <> :new.qt_material))) then
	wheb_mensagem_pck.exibir_mensagem_abort(170909, 'NR_SEQ_ANALISE_W=' || nr_seq_analise_w ||';'||
							'NR_SEQ_PROC_W=' || nr_seq_conta_mat_w);
end if;

end;
/


ALTER TABLE TASY.PLS_CONTESTACAO_MAT ADD (
  CONSTRAINT PLSCMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTESTACAO_MAT ADD (
  CONSTRAINT PLSCMAT_PLSFATM_FK 
 FOREIGN KEY (NR_SEQ_FATURA_MAT) 
 REFERENCES TASY.PLS_FATURA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCMAT_PLSCTST_FK 
 FOREIGN KEY (NR_SEQ_CONTESTACAO) 
 REFERENCES TASY.PLS_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCMAT_PLSMOGA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA_ACEITA) 
 REFERENCES TASY.PLS_MOTIVO_GLOSA_ACEITA (NR_SEQUENCIA),
  CONSTRAINT PLSCMAT_PLSMGNE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA_NEG) 
 REFERENCES TASY.PLS_MOTIVO_GLOSA_NEGADA (NR_SEQUENCIA),
  CONSTRAINT PLSCMAT_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTESTACAO_MAT TO NIVEL_1;

