ALTER TABLE TASY.QUA_DOC_REVISAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_DOC_REVISAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_DOC_REVISAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_DOC             NUMBER(5)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_REVISAO             DATE                   NOT NULL,
  CD_REVISAO             VARCHAR2(20 BYTE)      NOT NULL,
  CD_PESSOA_REVISAO      VARCHAR2(10 BYTE)      NOT NULL,
  CD_PESSOA_APROVACAO    VARCHAR2(10 BYTE),
  DT_APROVACAO           DATE,
  NM_USUARIO_APROVACAO   VARCHAR2(15 BYTE),
  DS_ARQUIVO             VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_VALIDACAO    VARCHAR2(10 BYTE),
  DT_VALIDACAO           DATE,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  NR_SEQ_CLASSIF         NUMBER(10),
  NR_SEQ_ORDEM_SERV      NUMBER(10),
  IE_REATIVACAO          VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM_TREIN     NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUADORE_MANORSE_FK_I ON TASY.QUA_DOC_REVISAO
(NR_SEQ_ORDEM_TREIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADORE_PESFISI_FK_I ON TASY.QUA_DOC_REVISAO
(CD_PESSOA_REVISAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADORE_PESFISI_FK2_I ON TASY.QUA_DOC_REVISAO
(CD_PESSOA_APROVACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADORE_PESFISI_FK3_I ON TASY.QUA_DOC_REVISAO
(CD_PESSOA_VALIDACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUADORE_PK ON TASY.QUA_DOC_REVISAO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADORE_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUADORE_QUADECL_FK_I ON TASY.QUA_DOC_REVISAO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADORE_QUADECL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUADORE_QUADOCU_FK_I ON TASY.QUA_DOC_REVISAO
(NR_SEQ_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.qua_doc_revisao_atual
BEFORE UPDATE ON TASY.QUA_DOC_REVISAO FOR EACH ROW
DECLARE

nm_user_w			varchar2(50);
nr_seq_ordem_w			man_ordem_servico.nr_sequencia%type;
nr_seq_doc_pai_w		qua_documento.nr_sequencia%type;
cd_documento_w			qua_documento.cd_documento%type;
nm_documento_w			qua_documento.nm_documento%type;
nr_seq_classif_w		qua_documento.nr_seq_classif%type;
nr_seq_tipo_w			qua_documento.nr_seq_tipo%type;

nr_seq_documento_w		qms_treinamento.nr_seq_documento%type;
cd_pessoa_fisica_w		qms_treinamento.cd_pessoa_fisica%type;
cd_pessoa_resp_w		qms_treinamento.cd_responsavel%type;
cd_gerente_w			qms_treinamento.cd_gerente_pessoa%type;
cd_revisao_w			qua_doc_revisao.cd_revisao%type;
nr_seq_cargo_w			qms_treinamento_cargo.nr_seq_cargo%type;
nr_seq_estagio_w		man_ordem_servico.nr_seq_estagio%type;
qt_existe_pendencias_w	number(10);
qt_existe_nova_rev_w	number(10);


Cursor	c01 is
select	mos.nr_sequencia
from	man_ordem_servico mos
where	mos.nr_seq_documento = :new.nr_seq_doc
and	mos.ie_tipo_ordem = 12
and	mos.ie_status_ordem <> '3'
and	not exists	(
				select	1
				from	qua_doc_arquivo qda
				where	qda.nr_seq_doc =	(
									select	min(doc.nr_seq_documento)
									from	(
											select	qd.nr_sequencia nr_seq_documento
											from	qua_documento qd
											where	qd.nr_sequencia = :new.nr_seq_doc
											and	qd.nr_seq_superior is null
											union
											select	qd.nr_seq_superior nr_seq_documento
											from	qua_documento qd
											where	nr_seq_superior is not null
											and	nr_sequencia = :new.nr_seq_doc
										) doc
								)
			);

begin

select	username
into	nm_user_w
from	user_users;

if	(nm_user_w = 'CORP') and
	(:new.dt_aprovacao is not null) and
	(:old.dt_aprovacao is null) then
	begin

	select	cd_documento,
		substr(obter_desc_expressao(cd_exp_documento, nm_documento), 1, 255) nm_documento,
		nr_seq_classif,
		nr_seq_tipo
	into	cd_documento_w,
		nm_documento_w,
		nr_seq_classif_w,
		nr_seq_tipo_w
	from	qua_documento
	where	nr_sequencia	= :new.nr_seq_doc;

	open C01;
	loop
	fetch C01 into
		nr_seq_ordem_w;
	exit when C01%notfound;
		begin

			if	(	(nr_seq_classif_w = 1)
				and	nr_seq_tipo_w in (2, 3, 5, 15)
				and	(obter_se_base_corp = 'S'
					or	obter_se_base_wheb = 'S')) then
				begin
					select	max(mes.nr_sequencia)
					into	nr_seq_estagio_w
					from	man_estagio_processo mes
					where	mes.nr_sequencia = 1871;

					if	(nr_seq_estagio_w is not null) then
						begin
							update	man_ordem_servico
							set	nr_seq_estagio = nr_seq_estagio_w
							where	nr_sequencia = nr_seq_ordem_w;
						end;
					end if;
				end;
			else
				begin
					man_encerrar_OS(nr_seq_ordem_w,9,:new.nm_usuario,substr(Wheb_mensagem_pck.get_texto(799312) || ' ' ||cd_documento_w||' - '||nm_documento_w,1,255),'N');
				end;
			end if;

		end;
	end loop;
	close C01;

	end;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.QUA_DOC_REVISAO_tp  after update ON TASY.QUA_DOC_REVISAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_REVISAO,1,500);gravar_log_alteracao(substr(:old.DT_REVISAO,1,4000),substr(:new.DT_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_REVISAO',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_REVISAO,1,500);gravar_log_alteracao(substr(:old.CD_REVISAO,1,4000),substr(:new.CD_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_REVISAO',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_DOC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_DOC,1,4000),substr(:new.NR_SEQ_DOC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DOC',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ORDEM_SERV,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_SERV,1,4000),substr(:new.NR_SEQ_ORDEM_SERV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_SERV',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_APROVACAO,1,500);gravar_log_alteracao(substr(:old.DT_APROVACAO,1,4000),substr(:new.DT_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_APROVACAO',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_VALIDACAO,1,500);gravar_log_alteracao(substr(:old.DT_VALIDACAO,1,4000),substr(:new.DT_VALIDACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_VALIDACAO',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ORDEM_TREIN,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_TREIN,1,4000),substr(:new.NR_SEQ_ORDEM_TREIN,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_TREIN',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ARQUIVO,1,500);gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'QUA_DOC_REVISAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.QUA_DOC_REVISAO ADD (
  CONSTRAINT QUADORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_DOC_REVISAO ADD (
  CONSTRAINT QUADORE_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_TREIN) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT QUADORE_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_VALIDACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADORE_QUADOCU_FK 
 FOREIGN KEY (NR_SEQ_DOC) 
 REFERENCES TASY.QUA_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT QUADORE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_REVISAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADORE_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_APROVACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADORE_QUADECL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.QUA_DOC_REVISAO_CLASSIF (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_DOC_REVISAO TO NIVEL_1;

