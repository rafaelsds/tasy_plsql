ALTER TABLE TASY.CONTA_BANCO_TIPO_APLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_BANCO_TIPO_APLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_BANCO_TIPO_APLIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DS_TIPO_APLICACAO      VARCHAR2(255 BYTE)     NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_CARENCIA_OBRIG      VARCHAR2(1 BYTE)       NOT NULL,
  NR_DIAS_CARENCIA       NUMBER(15),
  VL_TAXA_REND           NUMBER(15,8),
  IE_FORMA_REND          VARCHAR2(1 BYTE)       NOT NULL,
  NR_DIAS_REND           NUMBER(15),
  NR_SEQ_TRANS_FIN_REND  NUMBER(15)             NOT NULL,
  NR_SEQ_TRANS_FIN_RESG  NUMBER(15)             NOT NULL,
  IE_FORMA_LANCAMENTO    VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIPO_FUNDO          VARCHAR2(1 BYTE),
  IE_TIPO_APLIC_FIXA     VARCHAR2(1 BYTE),
  NR_DIAS_MIN_RESG       NUMBER(15),
  VL_MIN_RESG            NUMBER(15,2),
  VL_MIN_APLIC           NUMBER(15,2),
  IE_TIPO_PRAZO          VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTABATIPAP_ESTABEL_FK_I ON TASY.CONTA_BANCO_TIPO_APLIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTABATIPAP_PK ON TASY.CONTA_BANCO_TIPO_APLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTABATIPAP_TRAFINA_FK_I ON TASY.CONTA_BANCO_TIPO_APLIC
(NR_SEQ_TRANS_FIN_REND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTABATIPAP_TRAFIN2_FK_I ON TASY.CONTA_BANCO_TIPO_APLIC
(NR_SEQ_TRANS_FIN_RESG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.conta_banco_tipo_aplic_atual
after update ON TASY.CONTA_BANCO_TIPO_APLIC for each row
declare

ds_historico_w		varchar2(255);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.ds_tipo_aplicacao <> :old.ds_tipo_aplicacao) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||:old.ds_tipo_aplicacao||';VALOR_NOVO='||:new.ds_tipo_aplicacao),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_forma_lancamento <> :old.ie_forma_lancamento) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(7602,:old.ie_forma_lancamento)
								||';VALOR_NOVO='||obter_descricao_dominio(7602,:new.ie_forma_lancamento)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_forma_rend <> :old.ie_forma_rend) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(7601,:old.ie_forma_rend)
								||';VALOR_NOVO='||obter_descricao_dominio(7601,:new.ie_forma_rend)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.nr_seq_trans_fin_rend <> :old.nr_seq_trans_fin_rend) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_ds_transacao(:old.nr_seq_trans_fin_rend)
								||';VALOR_NOVO='||obter_ds_transacao(:new.nr_seq_trans_fin_rend)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.nr_seq_trans_fin_resg <> :old.nr_seq_trans_fin_resg) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_ds_transacao(:old.nr_seq_trans_fin_resg)
								||';VALOR_NOVO='||obter_ds_transacao(:new.nr_seq_trans_fin_resg)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_tipo_aplic_fixa <> :old.ie_tipo_aplic_fixa) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(7604,:old.ie_tipo_aplic_fixa)
								||';VALOR_NOVO='||obter_descricao_dominio(7604,:new.ie_tipo_aplic_fixa)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_tipo_fundo <> :old.ie_tipo_fundo) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(7603,:old.ie_tipo_fundo)
								||';VALOR_NOVO='||obter_descricao_dominio(7603,:new.ie_tipo_fundo)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_carencia_obrig <> :old.ie_carencia_obrig) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(6,:old.ie_carencia_obrig)
								||';VALOR_NOVO='||obter_descricao_dominio(6,:new.ie_carencia_obrig)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.nr_dias_carencia <> :old.nr_dias_carencia) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||to_char(:old.nr_dias_carencia)
								||';VALOR_NOVO='||to_char(:new.nr_dias_carencia)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_tipo_prazo <> :old.ie_tipo_prazo) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354771,'VALOR_ANTIGO='||obter_descricao_dominio(7605,:old.ie_tipo_prazo)
								||';VALOR_NOVO='||obter_descricao_dominio(7605,:new.ie_tipo_prazo)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_taxa_rend <> :old.vl_taxa_rend) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||to_char(:old.vl_taxa_rend,'999,999,999.99')
								||';VALOR_NOVO='||to_char(:new.vl_taxa_rend,'999,999,999.99')),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.nr_dias_rend <> :old.nr_dias_rend) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||to_char(:old.nr_dias_rend)
								||';VALOR_NOVO='||to_char(:new.nr_dias_rend)),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_min_aplic <> :old.vl_min_aplic) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||to_char(:old.vl_min_aplic,'999,999,999.99')
								||';VALOR_NOVO='||to_char(:new.vl_min_aplic,'999,999,999.99')),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_min_resg <> :old.vl_min_resg) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||to_char(:old.vl_min_resg,'999,999,999.99')
								||';VALOR_NOVO='||to_char(:new.vl_min_resg,'999,999,999.99')),0,254);

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.ie_situacao <> :old.ie_situacao) then
	select substr(wheb_mensagem_pck.get_texto(354769,'VALOR_ANTIGO='||decode(:old.ie_situacao,'A',wheb_mensagem_pck.get_texto(354788),wheb_mensagem_pck.get_texto(354792))
							||';VALOR_NOVO='||decode(:new.ie_situacao,'A',wheb_mensagem_pck.get_texto(354788),wheb_mensagem_pck.get_texto(354792))),0,254)
	into	ds_historico_w
	from	dual;

	insert into tipo_aplicacao_hist(
		nr_sequencia,
		cd_estabelecimento,
		ds_historico,
		nr_seq_tipo_aplic,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		:new.cd_estabelecimento,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

end if;

end;
/


ALTER TABLE TASY.CONTA_BANCO_TIPO_APLIC ADD (
  CONSTRAINT CTABATIPAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_BANCO_TIPO_APLIC ADD (
  CONSTRAINT CTABATIPAP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTABATIPAP_TRAFIN2_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_RESG) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CTABATIPAP_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_REND) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONTA_BANCO_TIPO_APLIC TO NIVEL_1;

