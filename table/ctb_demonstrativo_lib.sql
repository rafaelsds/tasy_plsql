ALTER TABLE TASY.CTB_DEMONSTRATIVO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_DEMONSTRATIVO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_DEMONSTRATIVO_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_MODELO         NUMBER(10)              NOT NULL,
  NR_SEQ_DEMONSTRATIVO  NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PERFIL             NUMBER(5),
  CD_CARGO              NUMBER(10),
  NM_USUARIO_LIB        VARCHAR2(15 BYTE),
  IE_PERMISSAO          VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBDELIB_CARGO_FK_I ON TASY.CTB_DEMONSTRATIVO_LIB
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDELIB_CTBDEMO_FK_I ON TASY.CTB_DEMONSTRATIVO_LIB
(NR_SEQ_DEMONSTRATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDELIB_CTBMORE_FK_I ON TASY.CTB_DEMONSTRATIVO_LIB
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDELIB_PERFIL_FK_I ON TASY.CTB_DEMONSTRATIVO_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBDELIB_PK ON TASY.CTB_DEMONSTRATIVO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDELIB_USUARIO_FK_I ON TASY.CTB_DEMONSTRATIVO_LIB
(NM_USUARIO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CTB_DEMONSTRATIVO_LIB ADD (
  CONSTRAINT CTBDELIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_DEMONSTRATIVO_LIB ADD (
  CONSTRAINT CTBDELIB_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT CTBDELIB_CTBDEMO_FK 
 FOREIGN KEY (NR_SEQ_DEMONSTRATIVO) 
 REFERENCES TASY.CTB_DEMONSTRATIVO (NR_SEQUENCIA),
  CONSTRAINT CTBDELIB_CTBMORE_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.CTB_MODELO_RELAT (NR_SEQUENCIA),
  CONSTRAINT CTBDELIB_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CTBDELIB_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_LIB) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.CTB_DEMONSTRATIVO_LIB TO NIVEL_1;

