ALTER TABLE TASY.EHR_TEMPLATE_CONT_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_TEMPLATE_CONT_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_TEMPLATE_CONT_ATRIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TEMP_CONTEUDO  NUMBER(10)              NOT NULL,
  NM_TABELA             VARCHAR2(50 BYTE)       NOT NULL,
  NM_ATRIBUTO           VARCHAR2(50 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRTCAT_EHTTEIN_FK_I ON TASY.EHR_TEMPLATE_CONT_ATRIB
(NR_SEQ_TEMP_CONTEUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRTCAT_PK ON TASY.EHR_TEMPLATE_CONT_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRTCAT_TABSIST_FK_I ON TASY.EHR_TEMPLATE_CONT_ATRIB
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EHR_TEMPLATE_CONT_ATRIB ADD (
  CONSTRAINT EHRTCAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_TEMPLATE_CONT_ATRIB ADD (
  CONSTRAINT EHRTCAT_EHTTEIN_FK 
 FOREIGN KEY (NR_SEQ_TEMP_CONTEUDO) 
 REFERENCES TASY.EHR_TEMPLATE_CONTEUDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHRTCAT_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA));

GRANT SELECT ON TASY.EHR_TEMPLATE_CONT_ATRIB TO NIVEL_1;

