ALTER TABLE TASY.VIDEO_SERVER_TENANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VIDEO_SERVER_TENANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.VIDEO_SERVER_TENANT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_SERVER_TENANT     VARCHAR2(100 BYTE)       NOT NULL,
  DS_URL               VARCHAR2(255 BYTE)       NOT NULL,
  NM_USER              VARCHAR2(100 BYTE),
  CD_PASSWORD          VARCHAR2(100 BYTE),
  IE_DEFAULT           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.VIDSERTEN_PK ON TASY.VIDEO_SERVER_TENANT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.VIDEO_SERVER_TENANT ADD (
  CONSTRAINT VIDSERTEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.VIDEO_SERVER_TENANT TO NIVEL_1;

