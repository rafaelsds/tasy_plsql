ALTER TABLE TASY.PCS_GRUPAMENTOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_GRUPAMENTOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_GRUPAMENTOS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DS_GRUPAMENTO        VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_TIPO_LISTA    NUMBER(10),
  CD_RESPONSAVEL       VARCHAR2(10 BYTE)        NOT NULL,
  QT_POLITICA          NUMBER(10),
  QT_FREQUENCIA        NUMBER(10),
  QT_DIAS_CORTE_MLC    NUMBER(10),
  QT_DIAS_CORTE_LC     NUMBER(10),
  QT_MARGEM_DIAS       NUMBER(10),
  IE_CURVA_ABC         VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_LISTA         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCSGRUP_EMPRESA_FK_I ON TASY.PCS_GRUPAMENTOS
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSGRUP_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PCSGRUP_PCSLIST_FK_I ON TASY.PCS_GRUPAMENTOS
(NR_SEQ_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCSGRUP_PCSTPLI_FK_I ON TASY.PCS_GRUPAMENTOS
(NR_SEQ_TIPO_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSGRUP_PCSTPLI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PCSGRUP_PESFISI_FK_I ON TASY.PCS_GRUPAMENTOS
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCSGRUP_PK ON TASY.PCS_GRUPAMENTOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PCS_GRUPAMENTOS_tp  after update ON TASY.PCS_GRUPAMENTOS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CURVA_ABC,1,4000),substr(:new.IE_CURVA_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CURVA_ABC',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_GRUPAMENTO,1,4000),substr(:new.DS_GRUPAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRUPAMENTO',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RESPONSAVEL,1,4000),substr(:new.CD_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'CD_RESPONSAVEL',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MARGEM_DIAS,1,4000),substr(:new.QT_MARGEM_DIAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_MARGEM_DIAS',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_POLITICA,1,4000),substr(:new.QT_POLITICA,1,4000),:new.nm_usuario,nr_seq_w,'QT_POLITICA',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FREQUENCIA,1,4000),substr(:new.QT_FREQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_FREQUENCIA',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CORTE_MLC,1,4000),substr(:new.QT_DIAS_CORTE_MLC,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CORTE_MLC',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CORTE_LC,1,4000),substr(:new.QT_DIAS_CORTE_LC,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CORTE_LC',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_LISTA,1,4000),substr(:new.NR_SEQ_TIPO_LISTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_LISTA',ie_log_w,ds_w,'PCS_GRUPAMENTOS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PCS_GRUPAMENTOS ADD (
  CONSTRAINT PCSGRUP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PCS_GRUPAMENTOS ADD (
  CONSTRAINT PCSGRUP_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PCSGRUP_PCSTPLI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LISTA) 
 REFERENCES TASY.PCS_TIPO_LISTA (NR_SEQUENCIA),
  CONSTRAINT PCSGRUP_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PCSGRUP_PCSLIST_FK 
 FOREIGN KEY (NR_SEQ_LISTA) 
 REFERENCES TASY.PCS_LISTAS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PCS_GRUPAMENTOS TO NIVEL_1;

