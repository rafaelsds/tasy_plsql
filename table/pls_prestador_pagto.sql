ALTER TABLE TASY.PLS_PRESTADOR_PAGTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_PAGTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_PAGTO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR            NUMBER(10)        NOT NULL,
  CD_CONDICAO_PAGAMENTO       NUMBER(10)        NOT NULL,
  IE_FORMA_PAGTO              VARCHAR2(5 BYTE),
  QT_DIA_VENCIMENTO           NUMBER(2),
  IE_GERACAO_NOTA_TITULO      VARCHAR2(2 BYTE),
  IE_SALDO_NEGATIVO           VARCHAR2(3 BYTE)  NOT NULL,
  CD_BANCO                    NUMBER(5),
  CD_AGENCIA_BANCARIA         VARCHAR2(8 BYTE),
  NR_CONTA                    VARCHAR2(20 BYTE),
  CD_CGC                      VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  NR_SEQ_PESSOA_JUR_CONTA     NUMBER(10),
  NR_SEQ_PESSOA_FISICA_CONTA  NUMBER(10),
  IE_PERIODO_APROP_NEG        VARCHAR2(1 BYTE),
  QT_PAG_NEGATIVO_MAX         NUMBER(5),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  VL_MINIMO_TIT_LIQ           NUMBER(15,2),
  DT_INICIO_VIGENCIA          DATE,
  DT_FIM_VIGENCIA             DATE,
  DT_INICIO_VIGENCIA_REF      DATE,
  DT_FIM_VIGENCIA_REF         DATE,
  IE_EXCECAO_PAG_NEGATIVO     VARCHAR2(2 BYTE),
  NR_PRAZO                    NUMBER(2),
  IE_TIPO_PAGAMENTO           VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRPA_BANCO_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPA_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPA_CONPAGA_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPA_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPA_I1 ON TASY.PLS_PRESTADOR_PAGTO
(NR_SEQ_PRESTADOR, DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPA_I2 ON TASY.PLS_PRESTADOR_PAGTO
(NR_SEQ_PRESTADOR, CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPA_PESFICO_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_PESSOA_FISICA, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPA_PESFISI_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPA_PESFISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPA_PESJUCO_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_CGC, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPA_PESJUCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPA_PESJURI_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPRPA_PK ON TASY.PLS_PRESTADOR_PAGTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPA_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_PAGTO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_prestador_pagto_delete
after delete ON TASY.PLS_PRESTADOR_PAGTO for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_PAGTO',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_PAGTO_tp  after update ON TASY.PLS_PRESTADOR_PAGTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONDICAO_PAGAMENTO,1,500);gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FORMA_PAGTO,1,500);gravar_log_alteracao(substr(:old.IE_FORMA_PAGTO,1,4000),substr(:new.IE_FORMA_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_PAGTO',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GERACAO_NOTA_TITULO,1,500);gravar_log_alteracao(substr(:old.IE_GERACAO_NOTA_TITULO,1,4000),substr(:new.IE_GERACAO_NOTA_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERACAO_NOTA_TITULO',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DIA_VENCIMENTO,1,500);gravar_log_alteracao(substr(:old.QT_DIA_VENCIMENTO,1,4000),substr(:new.QT_DIA_VENCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_VENCIMENTO',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_PRESTADOR_PAGTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_pagto_alt
before update or insert ON TASY.PLS_PRESTADOR_PAGTO for each row
declare

begin

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia,'I');

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia,'F');
end;
/


ALTER TABLE TASY.PLS_PRESTADOR_PAGTO ADD (
  CONSTRAINT PLSPRPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_PAGTO ADD (
  CONSTRAINT PLSPRPA_PESFICO_FK 
 FOREIGN KEY (CD_PESSOA_FISICA, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA) 
 REFERENCES TASY.PESSOA_FISICA_CONTA (CD_PESSOA_FISICA,CD_BANCO,CD_AGENCIA_BANCARIA,NR_CONTA),
  CONSTRAINT PLSPRPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRPA_PESJUCO_FK 
 FOREIGN KEY (CD_CGC, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA) 
 REFERENCES TASY.PESSOA_JURIDICA_CONTA (CD_CGC,CD_BANCO,CD_AGENCIA_BANCARIA,NR_CONTA),
  CONSTRAINT PLSPRPA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRPA_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRPA_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPA_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO));

GRANT SELECT ON TASY.PLS_PRESTADOR_PAGTO TO NIVEL_1;

