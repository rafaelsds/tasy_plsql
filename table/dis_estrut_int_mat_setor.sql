ALTER TABLE TASY.DIS_ESTRUT_INT_MAT_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIS_ESTRUT_INT_MAT_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIS_ESTRUT_INT_MAT_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUT         NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESMASE_DISESIM_FK_I ON TASY.DIS_ESTRUT_INT_MAT_SETOR
(NR_SEQ_ESTRUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESMASE_DISESIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DESMASE_ESTABEL_FK_I ON TASY.DIS_ESTRUT_INT_MAT_SETOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESMASE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DESMASE_PK ON TASY.DIS_ESTRUT_INT_MAT_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESMASE_PK
  MONITORING USAGE;


CREATE INDEX TASY.DESMASE_SETATEN_FK_I ON TASY.DIS_ESTRUT_INT_MAT_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESMASE_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.DIS_ESTRUT_INT_MAT_SETOR ADD (
  CONSTRAINT DESMASE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIS_ESTRUT_INT_MAT_SETOR ADD (
  CONSTRAINT DESMASE_DISESIM_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT) 
 REFERENCES TASY.DIS_ESTRUT_INT_MAT (NR_SEQUENCIA),
  CONSTRAINT DESMASE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DESMASE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.DIS_ESTRUT_INT_MAT_SETOR TO NIVEL_1;

