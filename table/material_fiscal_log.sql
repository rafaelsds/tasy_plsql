ALTER TABLE TASY.MATERIAL_FISCAL_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_FISCAL_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_FISCAL_LOG
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_MATERIAL_ORIG       NUMBER(6),
  CD_MATERIAL_DEST       NUMBER(6),
  CD_CLASSE_MATERIAL     NUMBER(5),
  CD_FAMILIA_MATERIAL    NUMBER(5),
  IE_ALTERAR_TIPO_MEDIC  VARCHAR2(1 BYTE),
  QT_ALTERADO            NUMBER(10),
  CD_GRUPO_MATERIAL      NUMBER(6),
  CD_SUBGRUPO_MATERIAL   NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MATFL_PK ON TASY.MATERIAL_FISCAL_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MATERIAL_FISCAL_LOG ADD (
  CONSTRAINT MATFL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MATERIAL_FISCAL_LOG TO NIVEL_1;

