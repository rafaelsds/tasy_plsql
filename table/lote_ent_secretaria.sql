ALTER TABLE TASY.LOTE_ENT_SECRETARIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENT_SECRETARIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENT_SECRETARIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_INSTITUICAO    NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TIPO_FICHA         VARCHAR2(5 BYTE)        NOT NULL,
  QT_FICHAS_LOTE        NUMBER(10),
  DT_RECEBIMENTO        DATE                    NOT NULL,
  NR_SEQ_LOTE_ANTERIOR  NUMBER(10),
  DT_LIBERACAO          DATE,
  DT_IMP_ETIQUETA       DATE,
  NR_SEQ_PACOTE         NUMBER(10),
  NR_LOTE               NUMBER(10)              NOT NULL,
  NR_SEQ_POSTO          NUMBER(10),
  IE_LOTE_INTERNO       VARCHAR2(1 BYTE),
  CD_PACOTE_EXT         VARCHAR2(20 BYTE),
  NR_SEQ_LOTE_ANT_VINC  NUMBER(10),
  IE_INTEGRACAO         VARCHAR2(1 BYTE),
  DT_INTEGRACAO         DATE,
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LESEC_LEINPO_FK_I ON TASY.LOTE_ENT_SECRETARIA
(NR_SEQ_POSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEC_LEINPO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESEC_LOTEENTINS_FK_I ON TASY.LOTE_ENT_SECRETARIA
(NR_SEQ_INSTITUICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEC_LOTEENTINS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LESEC_PK ON TASY.LOTE_ENT_SECRETARIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LESEC_SETATEN_FK_I ON TASY.LOTE_ENT_SECRETARIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lote_ent_secretaria_afterup
after update ON TASY.LOTE_ENT_SECRETARIA for each row
declare
nr_ultimo_lote_w	number(10);

begin

if 	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.ie_lote_interno <> 'S') then

	select	nvl(max(nr_ultimo_lote),0)
	into	nr_ultimo_lote_w
	from	lote_ent_inst_posto
	where	nr_seq_instituicao = :new.nr_seq_instituicao
	and		nr_sequencia = :new.nr_seq_posto;

	if (:new.nr_lote > 0) then

		update 	lote_ent_inst_posto
		set	nr_ultimo_lote = :new.nr_lote
		where	nr_seq_instituicao = :new.nr_seq_instituicao
		and	nr_sequencia = :new.nr_seq_posto;

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_ENT_SECRETARIA_tp  after update ON TASY.LOTE_ENT_SECRETARIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_INTEGRACAO,1,500);gravar_log_alteracao(substr(:old.DT_INTEGRACAO,1,4000),substr(:new.DT_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INTEGRACAO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_POSTO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_POSTO,1,4000),substr(:new.NR_SEQ_POSTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_POSTO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_IMP_ETIQUETA,1,500);gravar_log_alteracao(substr(:old.DT_IMP_ETIQUETA,1,4000),substr(:new.DT_IMP_ETIQUETA,1,4000),:new.nm_usuario,nr_seq_w,'DT_IMP_ETIQUETA',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PACOTE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PACOTE,1,4000),substr(:new.NR_SEQ_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PACOTE',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_LOTE,1,500);gravar_log_alteracao(substr(:old.NR_LOTE,1,4000),substr(:new.NR_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_LOTE',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_INSTITUICAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_INSTITUICAO,1,4000),substr(:new.NR_SEQ_INSTITUICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INSTITUICAO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_FICHA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_FICHA,1,4000),substr(:new.IE_TIPO_FICHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FICHA',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_FICHAS_LOTE,1,500);gravar_log_alteracao(substr(:old.QT_FICHAS_LOTE,1,4000),substr(:new.QT_FICHAS_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_FICHAS_LOTE',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_RECEBIMENTO,1,500);gravar_log_alteracao(substr(:old.DT_RECEBIMENTO,1,4000),substr(:new.DT_RECEBIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_RECEBIMENTO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOTE_ANTERIOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_ANTERIOR,1,4000),substr(:new.NR_SEQ_LOTE_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_ANTERIOR',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LOTE_INTERNO,1,500);gravar_log_alteracao(substr(:old.IE_LOTE_INTERNO,1,4000),substr(:new.IE_LOTE_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOTE_INTERNO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PACOTE_EXT,1,500);gravar_log_alteracao(substr(:old.CD_PACOTE_EXT,1,4000),substr(:new.CD_PACOTE_EXT,1,4000),:new.nm_usuario,nr_seq_w,'CD_PACOTE_EXT',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOTE_ANT_VINC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_ANT_VINC,1,4000),substr(:new.NR_SEQ_LOTE_ANT_VINC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_ANT_VINC',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_INTEGRACAO,1,500);gravar_log_alteracao(substr(:old.IE_INTEGRACAO,1,4000),substr(:new.IE_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTEGRACAO',ie_log_w,ds_w,'LOTE_ENT_SECRETARIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOTE_ENT_SECRETARIA ADD (
  CONSTRAINT LESEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENT_SECRETARIA ADD (
  CONSTRAINT LESEC_LOTEENTINS_FK 
 FOREIGN KEY (NR_SEQ_INSTITUICAO) 
 REFERENCES TASY.LOTE_ENT_INSTITUICAO (NR_SEQUENCIA),
  CONSTRAINT LESEC_LEINPO_FK 
 FOREIGN KEY (NR_SEQ_POSTO) 
 REFERENCES TASY.LOTE_ENT_INST_POSTO (NR_SEQUENCIA),
  CONSTRAINT LESEC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.LOTE_ENT_SECRETARIA TO NIVEL_1;

