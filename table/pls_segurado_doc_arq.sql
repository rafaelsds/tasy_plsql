ALTER TABLE TASY.PLS_SEGURADO_DOC_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SEGURADO_DOC_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SEGURADO_DOC_ARQ
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_SEGURADO        NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DS_ARQUIVO             VARCHAR2(255 BYTE)     NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ORIGEM_ANEXO        VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  IE_APRESENTA_PORTAL    VARCHAR2(1 BYTE),
  DT_INI_APRESENTA       DATE,
  DT_FIN_APRESENTA       DATE,
  DT_INI_APRESENTA_REF   DATE,
  DT_FIN_APRESENTA_REF   DATE,
  NM_ARQUIVO             VARCHAR2(255 BYTE),
  DT_DOCUMENTO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSEDA_ESTABEL_FK_I ON TASY.PLS_SEGURADO_DOC_ARQ
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEDA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSSEDA_PK ON TASY.PLS_SEGURADO_DOC_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEDA_PLSSEGU_FK_I ON TASY.PLS_SEGURADO_DOC_ARQ
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEDA_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEDA_TIPODOC_FK_I ON TASY.PLS_SEGURADO_DOC_ARQ
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEDA_TIPODOC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_segurado_doc_arq_atual
before insert or update ON TASY.PLS_SEGURADO_DOC_ARQ for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo
:new.dt_ini_apresenta_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_ini_apresenta, 'i');
:new.dt_fin_apresenta_ref    := pls_util_pck.obter_dt_vigencia_null( :new.dt_fin_apresenta, 'f');

end pls_segurado_doc_arq_atual;
/


ALTER TABLE TASY.PLS_SEGURADO_DOC_ARQ ADD (
  CONSTRAINT PLSSEDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SEGURADO_DOC_ARQ ADD (
  CONSTRAINT PLSSEDA_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSSEDA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSEDA_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SEGURADO_DOC_ARQ TO NIVEL_1;

