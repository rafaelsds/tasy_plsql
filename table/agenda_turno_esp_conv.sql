ALTER TABLE TASY.AGENDA_TURNO_ESP_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_TURNO_ESP_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_TURNO_ESP_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_AGENDA            NUMBER(10)               NOT NULL,
  HR_INICIAL           DATE                     NOT NULL,
  CD_CONVENIO_OLD      NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NR_SEQ_TURNO_ESP     NUMBER(10),
  IE_ATENDE_CONVENIO   VARCHAR2(1 BYTE)         NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  QT_PERMISSAO         NUMBER(10)               NOT NULL,
  PR_PERMISSAO         NUMBER(15),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_VALIDA_ZERADA     VARCHAR2(1 BYTE),
  QT_PERM_ENCAIXE      NUMBER(10),
  IE_TIPO_CONVENIO     NUMBER(2),
  DT_FIM_VIGENCIA      DATE,
  DT_INICIO_VIGENCIA   DATE,
  CD_CATEGORIA         VARCHAR2(10 BYTE),
  CD_CONVENIO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGTUECO_AGETUES_FK_I ON TASY.AGENDA_TURNO_ESP_CONV
(NR_SEQ_TURNO_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGTUECO_AGETUES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGTUECO_CATCONV_FK_I ON TASY.AGENDA_TURNO_ESP_CONV
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGTUECO_CONVENI_FK_I ON TASY.AGENDA_TURNO_ESP_CONV
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGTUECO_I1 ON TASY.AGENDA_TURNO_ESP_CONV
(CD_AGENDA, HR_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGTUECO_I1
  MONITORING USAGE;


CREATE INDEX TASY.AGTUECO_PESFISI_FK_I ON TASY.AGENDA_TURNO_ESP_CONV
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGTUECO_PK ON TASY.AGENDA_TURNO_ESP_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGTUECO_PROCEDI_FK_I ON TASY.AGENDA_TURNO_ESP_CONV
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGTUECO_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.AGENDA_TURNO_ESP_CONV ADD (
  CONSTRAINT AGTUECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_TURNO_ESP_CONV ADD (
  CONSTRAINT AGTUECO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGTUECO_AGETUES_FK 
 FOREIGN KEY (NR_SEQ_TURNO_ESP) 
 REFERENCES TASY.AGENDA_TURNO_ESP (NR_SEQUENCIA),
  CONSTRAINT AGTUECO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGTUECO_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AGTUECO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.AGENDA_TURNO_ESP_CONV TO NIVEL_1;

