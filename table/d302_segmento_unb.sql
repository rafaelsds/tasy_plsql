ALTER TABLE TASY.D302_SEGMENTO_UNB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.D302_SEGMENTO_UNB CASCADE CONSTRAINTS;

CREATE TABLE TASY.D302_SEGMENTO_UNB
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ARQUIVO           NUMBER(10),
  IE_SINTAXE               VARCHAR2(4 BYTE),
  NR_VERSAO_SINTAXE        NUMBER(1),
  NR_IK_REMETENDE          VARCHAR2(35 BYTE),
  NR_IK_DESTINATARIO       VARCHAR2(35 BYTE),
  DS_DT_CRIACAO            VARCHAR2(8 BYTE),
  DS_HR_CRIACAO            VARCHAR2(4 BYTE),
  NR_REFERENCIA_ARQ        VARCHAR2(14 BYTE),
  NR_REFERENCIA_APLICACAO  VARCHAR2(14 BYTE),
  NR_SEQ_302_AREA_SERV     NUMBER(10),
  IE_TIPO_ARQUIVO          NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.D302UNB_D302ARQE_FK_I ON TASY.D302_SEGMENTO_UNB
(NR_SEQ_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.D302UNB_PK ON TASY.D302_SEGMENTO_UNB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.D302_SEGMENTO_UNB ADD (
  CONSTRAINT D302UNB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.D302_SEGMENTO_UNB ADD (
  CONSTRAINT D302UNB_D302ARQE_FK 
 FOREIGN KEY (NR_SEQ_ARQUIVO) 
 REFERENCES TASY.D302_ARQUIVO_ENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.D302_SEGMENTO_UNB TO NIVEL_1;

