ALTER TABLE TASY.REGRA_GERACAO_ORDEM_TOT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_GERACAO_ORDEM_TOT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_GERACAO_ORDEM_TOT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGEORT_MATERIA_FK_I ON TASY.REGRA_GERACAO_ORDEM_TOT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGEORT_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGEORT_PK ON TASY.REGRA_GERACAO_ORDEM_TOT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGEORT_PK
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_GERACAO_ORDEM_TOT ADD (
  CONSTRAINT REGEORT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_GERACAO_ORDEM_TOT ADD (
  CONSTRAINT REGEORT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.REGRA_GERACAO_ORDEM_TOT TO NIVEL_1;

