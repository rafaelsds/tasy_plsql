ALTER TABLE TASY.HD_PRESCRICAO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_PRESCRICAO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_PRESCRICAO_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_PRESCRICAO        NUMBER(10)               NOT NULL,
  NR_SEQ_SOLUCAO       NUMBER(6)                NOT NULL,
  IE_EVENTO            VARCHAR2(15 BYTE)        NOT NULL,
  DT_EVENTO            DATE                     NOT NULL,
  CD_PESSOA_EVENTO     VARCHAR2(10 BYTE)        NOT NULL,
  QT_VOLUME            NUMBER(15,4),
  QT_VOL_DRENAGEM      NUMBER(15,2),
  NR_ETAPA_EVENTO      NUMBER(3),
  QT_DOSAGEM           NUMBER(15,4),
  IE_UNID_VEL_INF      VARCHAR2(3 BYTE),
  DT_CICLO             DATE,
  NR_SEQ_ASSINATURA    NUMBER(15),
  NR_SEQ_LOTE          NUMBER(10)               DEFAULT null,
  DT_HORARIO           DATE,
  IE_EVENTO_VALIDO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDPREVE_PESFISI_FK_I ON TASY.HD_PRESCRICAO_EVENTO
(CD_PESSOA_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDPREVE_PK ON TASY.HD_PRESCRICAO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPREVE_PK
  MONITORING USAGE;


CREATE INDEX TASY.HDPREVE_PRESOLU_FK_I ON TASY.HD_PRESCRICAO_EVENTO
(NR_PRESCRICAO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPREVE_PRESOLU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hd_prescricao_evento_insert
after INSERT ON TASY.HD_PRESCRICAO_EVENTO FOR EACH ROW
DECLARE

cd_setor_atendimento_w		number(15,0);
nr_seq_atend_w			number(15,0);
nr_seq_tipo_w			number(15,0);
cd_perfil_w			number(5);
ie_todas_gp_w			varchar2(2);
dt_medicao_w			Date;
nr_atendimento_w			number(15,0);

cursor c01 is
select	nr_seq_tipo
from	prescr_sol_perda_ganho
where	ie_evento		= decode(:new.ie_evento, 'IT', 45, decode(:new.ie_evento, 'DT', 46, 0))
and	nvl(ie_tipo_solucao,1)	= obter_tipo_dialise(:new.nr_prescricao, :new.nr_seq_solucao)
and	nvl(ie_gerar_evento_solucao,'S') = 'S'
and	((cd_material is null) or (cd_material in(	select	a.cd_material
					from	prescr_material a
					where	a.nr_sequencia_solucao	= :new.nr_seq_solucao
					and	a.nr_prescricao		= :new.nr_prescricao)))
and	((cd_setor_Atendimento is null) or
	 (cd_setor_Atendimento	= cd_setor_atendimento_w))
and	((cd_perfil is null) or
	 (cd_perfil = cd_perfil_w));

BEGIN
if	(:new.qt_volume is not null or :new.qt_vol_drenagem is not null) then

	Obter_Param_Usuario(1113,403,obter_perfil_ativo,:new.nm_usuario,0,ie_todas_gp_w);

	select	max(cd_setor_atendimento),
		max(cd_perfil_ativo),
		max(nr_atendimento)
	into	cd_setor_atendimento_w,
		cd_perfil_w,
		nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;

	dt_medicao_w := :new.dt_evento;

	if	(ie_todas_gp_w = 'N') then

		select	max(nr_seq_tipo)
		into	nr_seq_tipo_w
		from	prescr_sol_perda_ganho
		where	ie_evento		= decode(:new.ie_evento, 'IT', 45, decode(:new.ie_evento, 'DT', 46, 0))
		and	nvl(ie_tipo_solucao,1)	= obter_tipo_dialise(:new.nr_prescricao, :new.nr_seq_solucao)
		and	nvl(ie_gerar_evento_solucao,'S') = 'S'
		and	((cd_material is null) or (cd_material in(	select	a.cd_material
							from	prescr_material a
							where	a.nr_sequencia_solucao	= :new.nr_seq_solucao
							and	a.nr_prescricao		= :new.nr_prescricao)))
		and	((cd_setor_Atendimento is null) or
			 (cd_setor_Atendimento	= cd_setor_atendimento_w))
		and	((cd_perfil is null) or
			 (cd_perfil = cd_perfil_w));

		if	(nr_seq_tipo_w is not null) then

			select	atendimento_perda_ganho_seq.nextval
			into	nr_seq_atend_w
			from	dual;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_evento_hd)
			values	(nr_seq_atend_w,
				nr_atendimento_w,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				decode(:new.ie_evento, 'DT', :new.qt_vol_drenagem, :new.qt_volume),
				nvl(dt_medicao_w,sysdate),
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_evento,
				'A',
				sysdate,
				nvl(dt_medicao_w,sysdate),
				1,
				:new.nr_sequencia);
		end if;
	else

		open C01;
		loop
		fetch C01 into
			nr_seq_tipo_w;
		exit when C01%notfound;

			select	atendimento_perda_ganho_seq.nextval
			into	nr_seq_atend_w
			from	dual;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_evento_hd)
			values	(nr_seq_atend_w,
				nr_atendimento_w,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				decode(:new.ie_evento, 'DT', :new.qt_vol_drenagem, :new.qt_volume),
				nvl(dt_medicao_w,sysdate),
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_evento,
				'A',
				sysdate,
				nvl(dt_medicao_w,sysdate),
				1,
				:new.nr_sequencia);
		end loop;
		close C01;
	end if;

end if;

END;
/


ALTER TABLE TASY.HD_PRESCRICAO_EVENTO ADD (
  CONSTRAINT HDPREVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_PRESCRICAO_EVENTO ADD (
  CONSTRAINT HDPREVE_PRESOLU_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PRESCR_SOLUCAO (NR_PRESCRICAO,NR_SEQ_SOLUCAO),
  CONSTRAINT HDPREVE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_EVENTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.HD_PRESCRICAO_EVENTO TO NIVEL_1;

