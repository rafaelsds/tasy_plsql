ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_TAXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_POS_ESTAB_TAXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_POS_ESTAB_TAXA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_CONTA_POS_ESTAB  NUMBER(10)            NOT NULL,
  IE_TIPO_TAXA            VARCHAR2(2 BYTE),
  VL_TAXA_MANUTENCAO      NUMBER(15,2)          NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_POS_ESTAB  NUMBER(10),
  CD_CLASSIF_CRED         VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB          VARCHAR2(40 BYTE),
  CD_CONTA_CRED           VARCHAR2(20 BYTE),
  CD_CONTA_DEB            VARCHAR2(20 BYTE),
  CD_HISTORICO            NUMBER(10),
  CD_HISTORICO_BAIXA      NUMBER(10),
  CD_HISTORICO_ESTORNO    NUMBER(10),
  NR_SEQ_ESQUEMA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCPET_CONCONT_FK_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_CONCONT_FK2_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_HISPADR_FK_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_HISPADR_FK2_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(CD_HISTORICO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_HISPADR_FK3_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(CD_HISTORICO_ESTORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCPET_PK ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_PLSCOVB_FK_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(NR_SEQ_CONTA_POS_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_PLSESCO_FK_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPET_PLSREPES_FK_I ON TASY.PLS_CONTA_POS_ESTAB_TAXA
(NR_SEQ_REGRA_POS_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_TAXA ADD (
  CONSTRAINT PLSCPET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_TAXA ADD (
  CONSTRAINT PLSCPET_PLSCOVB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS_ESTAB) 
 REFERENCES TASY.PLS_CONTA_POS_ESTABELECIDO (NR_SEQUENCIA),
  CONSTRAINT PLSCPET_PLSREPES_FK 
 FOREIGN KEY (NR_SEQ_REGRA_POS_ESTAB) 
 REFERENCES TASY.PLS_REGRA_POS_ESTABELECIDO (NR_SEQUENCIA),
  CONSTRAINT PLSCPET_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCPET_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCPET_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCPET_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_BAIXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCPET_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_ESTORNO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCPET_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_POS_ESTAB_TAXA TO NIVEL_1;

