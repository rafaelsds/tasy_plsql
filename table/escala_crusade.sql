ALTER TABLE TASY.ESCALA_CRUSADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CRUSADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CRUSADE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_PA_SISTOLICA        NUMBER(3)              NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  QT_FREQ_CARDIACA       NUMBER(3)              NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_HEMATOCRITO         NUMBER(10,3)           NOT NULL,
  QT_CLEARANCE           NUMBER(10,3)           NOT NULL,
  IE_DOENCA_VASC_PREVIA  VARCHAR2(1 BYTE)       NOT NULL,
  IE_DIABETE             VARCHAR2(1 BYTE)       NOT NULL,
  IE_SINAIS_IC           VARCHAR2(1 BYTE)       NOT NULL,
  QT_SCORE               NUMBER(10,2),
  PR_RISCO               NUMBER(10,3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCRUS_ATEPACI_FK_I ON TASY.ESCALA_CRUSADE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCRUS_PESFISI_FK_I ON TASY.ESCALA_CRUSADE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCRUS_PK ON TASY.ESCALA_CRUSADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCRUS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_crusade_atual
before insert or update on  escala_crusade
for each row
declare


TYPE range 	IS VARRAY(200) OF Number;
Range_w		Range;

qt_reg_w		number(1);
qt_score_w		number(10,5)	:= 0;
cd_pessoa_fisica_w	varchar2(15);
ie_sexo_w		varchar2(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.qt_score	:=	null;
:new.pr_risco	:=	null;

cd_pessoa_fisica_w 	:= obter_pessoa_atendimento(:new.Nr_atendimento,'C');
ie_sexo_w 		:= obter_sexo_pf(cd_pessoa_fisica_w,'C');

if	(ie_sexo_w = 'F') then
	qt_score_w := qt_score_w + 8;
end if;


if	(:new.QT_HEMATOCRITO > 39.9) then
	qt_score_w := qt_score_w;
elsif	(:new.QT_HEMATOCRITO > 37) then
	qt_score_w := qt_score_w + 2;
elsif	(:new.QT_HEMATOCRITO > 34) then
	qt_score_w := qt_score_w + 3;
elsif	(:new.QT_HEMATOCRITO > 31) then
	qt_score_w := qt_score_w + 7;
else
	qt_score_w := qt_score_w + 9;
end if;


if	(:new.QT_CLEARANCE > 120) then
	qt_score_w := qt_score_w;
elsif	(:new.QT_CLEARANCE > 91) then
	qt_score_w := qt_score_w + 7;
elsif	(:new.QT_CLEARANCE > 61) then
	qt_score_w := qt_score_w + 17;
elsif	(:new.QT_CLEARANCE > 31) then
	qt_score_w := qt_score_w + 28;
elsif	(:new.QT_CLEARANCE > 16) then
	qt_score_w := qt_score_w + 35;
else
	qt_score_w := qt_score_w + 39;
end if;


if	(:new.QT_FREQ_CARDIACA > 120) then
	qt_score_w := qt_score_w + 11;
elsif	(:new.QT_FREQ_CARDIACA > 111) then
	qt_score_w := qt_score_w + 10;
elsif	(:new.QT_FREQ_CARDIACA > 101) then
	qt_score_w := qt_score_w + 8;
elsif	(:new.QT_FREQ_CARDIACA > 91) then
	qt_score_w := qt_score_w + 6;
elsif	(:new.QT_FREQ_CARDIACA > 81) then
	qt_score_w := qt_score_w + 3;
elsif	(:new.QT_FREQ_CARDIACA > 71) then
	qt_score_w := qt_score_w + 1;
end if;


if	(:new.QT_PA_SISTOLICA > 200) then
	qt_score_w := qt_score_w + 5;
elsif	(:new.QT_PA_SISTOLICA > 181) then
	qt_score_w := qt_score_w + 3;
elsif	(:new.QT_PA_SISTOLICA > 121) then
	qt_score_w := qt_score_w + 1;
elsif	(:new.QT_PA_SISTOLICA > 101) then
	qt_score_w := qt_score_w + 5;
elsif	(:new.QT_PA_SISTOLICA > 91) then
	qt_score_w := qt_score_w + 8;
else
	qt_score_w := qt_score_w + 10;
end if;

if	(:new.IE_DIABETE = 'S') then
	qt_score_w := qt_score_w + 6;
end if;

if	(:new.IE_DOENCA_VASC_PREVIA = 'S') then
	qt_score_w := qt_score_w + 6;
end if;

if	(:new.IE_SINAIS_IC = 'S') then
	qt_score_w := qt_score_w + 7;
end if;

:new.qt_score := qt_score_w;

Range_w := range(2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.8,3.9,4,4.1,4.3,4.4,4.6,4.7,
		 4.9,5,5.2,5.4,5.6,5.7,5.9,6.1,6.3,6.5,6.7,6.9,7.2,7.4,7.6,7.9,8.1,8.4,8.6,8.9,
		 9.2,9.5,9.8,10.1,10.4,10.7,11.1,11.4,11.7,12.1,12.5,12.8,13.2,13.6,14,14.4,14.9,
		 15.3,15.7,16.2,16.7,17.1,17.6,18.1,18.6,19.2,19.7,20.2,20.8,21.4,21.9,22.5,23.1,
		 23.7,24.4,25,25.6,26.3,27,27.6,28.3,29,29.7,30.4,31.2,31.9,32.6,33.4,34.2,34.9,
		 35.7,36.5,37.3,38.1,38.9,39.7,40.5,41.3,42.2,43,43.8);


:new.pr_risco 	:=	range_w(qt_score_w + 1);

<<Final>>
qt_reg_w	:= 0;

end escala_crusade_atual;
/


ALTER TABLE TASY.ESCALA_CRUSADE ADD (
  CONSTRAINT ESCCRUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CRUSADE ADD (
  CONSTRAINT ESCCRUS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCRUS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CRUSADE TO NIVEL_1;

