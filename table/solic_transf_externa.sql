ALTER TABLE TASY.SOLIC_TRANSF_EXTERNA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SOLIC_TRANSF_EXTERNA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SOLIC_TRANSF_EXTERNA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  DT_SOLICITACAO               DATE,
  NR_SEQ_MOTIVO_SOLIC_EXTERNA  NUMBER(10),
  NM_USUARIO_LIB               VARCHAR2(15 BYTE),
  DT_GERACAO_VAGA              DATE,
  NR_SEQ_VAGA                  NUMBER(10),
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  DT_LIBERACAO                 DATE,
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE),
  NR_SEQ_SOAP                  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SOLTREX_ATEPACI_FK_I ON TASY.SOLIC_TRANSF_EXTERNA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLTREX_ATESOAP_FK_I ON TASY.SOLIC_TRANSF_EXTERNA
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SOLTREX_PK ON TASY.SOLIC_TRANSF_EXTERNA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SOLIC_TRANSF_EXTERNA_ATUAL
before insert or update ON TASY.SOLIC_TRANSF_EXTERNA for each row
declare


begin
if	(nvl(:old.DT_SOLICITACAO,sysdate+10) <> :new.DT_SOLICITACAO) and
	(:new.DT_SOLICITACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_SOLICITACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


ALTER TABLE TASY.SOLIC_TRANSF_EXTERNA ADD (
  CONSTRAINT SOLTREX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SOLIC_TRANSF_EXTERNA ADD (
  CONSTRAINT SOLTREX_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SOLTREX_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA));

GRANT SELECT ON TASY.SOLIC_TRANSF_EXTERNA TO NIVEL_1;

