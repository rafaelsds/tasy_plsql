ALTER TABLE TASY.TISS_REGRA_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_GUIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  IE_TISS_TIPO_GUIA        VARCHAR2(2 BYTE),
  IE_FORMA                 VARCHAR2(10 BYTE)    NOT NULL,
  DS_ADICIONAL             VARCHAR2(20 BYTE),
  IE_TIPO_ATENDIMENTO      NUMBER(3),
  NR_INICIAL               NUMBER(15)           NOT NULL,
  NR_FINAL                 NUMBER(15)           NOT NULL,
  NR_ATUAL                 NUMBER(15)           NOT NULL,
  DS_MASCARA               VARCHAR2(40 BYTE),
  CD_PERFIL_COMUNIC        NUMBER(5),
  NR_DOC_CONV_COMUNIC      VARCHAR2(20 BYTE),
  IE_GUIA_HONORARIO        VARCHAR2(3 BYTE)     NOT NULL,
  QT_MAX_PROC              NUMBER(10),
  IE_GUIA_PRESTADOR        VARCHAR2(3 BYTE)     NOT NULL,
  IE_RESPONSAVEL_CREDITO   VARCHAR2(5 BYTE),
  IE_FUNCAO_MEDICO         VARCHAR2(1 BYTE)     NOT NULL,
  IE_CBOS                  VARCHAR2(1 BYTE)     NOT NULL,
  IE_GUIA_MEDICO           VARCHAR2(1 BYTE)     NOT NULL,
  QT_ITENS_GUIA            NUMBER(10),
  IE_GUIA                  VARCHAR2(3 BYTE),
  IE_GUIA_PARTIC_DIF       VARCHAR2(1 BYTE)     NOT NULL,
  IE_SENHA                 VARCHAR2(1 BYTE)     NOT NULL,
  IE_SETOR_ATENDIMENTO     VARCHAR2(1 BYTE),
  IE_MEDICO_REQ            VARCHAR2(1 BYTE),
  IE_PRESCRICAO            VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  IE_MEDICO_SOLIC_PRESCR   VARCHAR2(1 BYTE),
  IE_TIPO_ATEND_CONTA      NUMBER(3),
  IE_FUNCIONARIO           VARCHAR2(1 BYTE),
  CD_FUNCAO                NUMBER(3),
  IE_CONSIDERAR_SETOR      VARCHAR2(1 BYTE),
  IE_TIPO_GUIA_ATEND       VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  CD_AREA_PROCEDIMENTO     NUMBER(15),
  CD_ESPECIALIDADE         NUMBER(15),
  CD_GRUPO_PROC            NUMBER(15),
  IE_CONSIDERAR_TIPO_GUIA  VARCHAR2(1 BYTE),
  IE_TIPO_PROCEDIMENTO     VARCHAR2(1 BYTE),
  IE_DATA_ITEM             VARCHAR2(1 BYTE),
  IE_TIPO_ATEND_TISS_PROC  VARCHAR2(1 BYTE),
  IE_RESTRINGIR_CONTA      VARCHAR2(1 BYTE),
  IE_SETOR_PROC_PRESCR     VARCHAR2(1 BYTE),
  IE_RESTRINGIR_HONOR      VARCHAR2(1 BYTE),
  CD_CATEGORIA             VARCHAR2(10 BYTE),
  IE_PROC_PACOTE           VARCHAR2(1 BYTE),
  IE_SOMENTE_HONORARIO     VARCHAR2(5 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSREG_AREPROC_FK_I ON TASY.TISS_REGRA_GUIA
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREG_CATCONV_FK_I ON TASY.TISS_REGRA_GUIA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREG_CONVENI_FK_I ON TASY.TISS_REGRA_GUIA
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREG_ESPPROC_FK_I ON TASY.TISS_REGRA_GUIA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREG_ESTABEL_FK_I ON TASY.TISS_REGRA_GUIA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREG_FUNMEDI_FK_I ON TASY.TISS_REGRA_GUIA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_FUNMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREG_GRUPROC_FK_I ON TASY.TISS_REGRA_GUIA
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREG_I1 ON TASY.TISS_REGRA_GUIA
(CD_ESTABELECIMENTO, CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSREG_PK ON TASY.TISS_REGRA_GUIA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREG_PROCEDI_FK_I ON TASY.TISS_REGRA_GUIA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREG_SETATEN_FK_I ON TASY.TISS_REGRA_GUIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREG_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_GUIA_tp  after update ON TASY.TISS_REGRA_GUIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_ADICIONAL,1,500);gravar_log_alteracao(substr(:old.DS_ADICIONAL,1,4000),substr(:new.DS_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_ADICIONAL',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROC_PACOTE,1,500);gravar_log_alteracao(substr(:old.IE_PROC_PACOTE,1,4000),substr(:new.IE_PROC_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_PACOTE',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_FINAL,1,500);gravar_log_alteracao(substr(:old.NR_FINAL,1,4000),substr(:new.NR_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_FINAL',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_ATUAL,1,500);gravar_log_alteracao(substr(:old.NR_ATUAL,1,4000),substr(:new.NR_ATUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATUAL',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FORMA,1,500);gravar_log_alteracao(substr(:old.IE_FORMA,1,4000),substr(:new.IE_FORMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PERFIL_COMUNIC,1,500);gravar_log_alteracao(substr(:old.CD_PERFIL_COMUNIC,1,4000),substr(:new.CD_PERFIL_COMUNIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL_COMUNIC',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_DOC_CONV_COMUNIC,1,500);gravar_log_alteracao(substr(:old.NR_DOC_CONV_COMUNIC,1,4000),substr(:new.NR_DOC_CONV_COMUNIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOC_CONV_COMUNIC',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA,1,4000),substr(:new.DS_MASCARA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TISS_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_TISS_TIPO_GUIA,1,4000),substr(:new.IE_TISS_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TISS_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_HONORARIO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_HONORARIO,1,4000),substr(:new.IE_GUIA_HONORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_HONORARIO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FUNCAO_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_FUNCAO_MEDICO,1,4000),substr(:new.IE_FUNCAO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCAO_MEDICO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CBOS,1,500);gravar_log_alteracao(substr(:old.IE_CBOS,1,4000),substr(:new.IE_CBOS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CBOS',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_MEDICO,1,4000),substr(:new.IE_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_MEDICO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_MAX_PROC,1,500);gravar_log_alteracao(substr(:old.QT_MAX_PROC,1,4000),substr(:new.QT_MAX_PROC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_PROC',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PRESTADOR,1,4000),substr(:new.IE_GUIA_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PRESTADOR',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RESPONSAVEL_CREDITO,1,500);gravar_log_alteracao(substr(:old.IE_RESPONSAVEL_CREDITO,1,4000),substr(:new.IE_RESPONSAVEL_CREDITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESPONSAVEL_CREDITO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_ITENS_GUIA,1,500);gravar_log_alteracao(substr(:old.QT_ITENS_GUIA,1,4000),substr(:new.QT_ITENS_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ITENS_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SETOR_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_SETOR_ATENDIMENTO,1,4000),substr(:new.IE_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SETOR_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_GUIA,1,4000),substr(:new.IE_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PARTIC_DIF,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PARTIC_DIF,1,4000),substr(:new.IE_GUIA_PARTIC_DIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PARTIC_DIF',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENHA,1,500);gravar_log_alteracao(substr(:old.IE_SENHA,1,4000),substr(:new.IE_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEDICO_REQ,1,500);gravar_log_alteracao(substr(:old.IE_MEDICO_REQ,1,4000),substr(:new.IE_MEDICO_REQ,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_REQ',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PRESCRICAO,1,500);gravar_log_alteracao(substr(:old.IE_PRESCRICAO,1,4000),substr(:new.IE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESCRICAO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEDICO_SOLIC_PRESCR,1,500);gravar_log_alteracao(substr(:old.IE_MEDICO_SOLIC_PRESCR,1,4000),substr(:new.IE_MEDICO_SOLIC_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_SOLIC_PRESCR',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_SETOR_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATEND_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATEND_CONTA,1,4000),substr(:new.IE_TIPO_ATEND_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATEND_CONTA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FUNCIONARIO,1,500);gravar_log_alteracao(substr(:old.IE_FUNCIONARIO,1,4000),substr(:new.IE_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCIONARIO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_FUNCAO,1,500);gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSIDERAR_SETOR,1,500);gravar_log_alteracao(substr(:old.IE_CONSIDERAR_SETOR,1,4000),substr(:new.IE_CONSIDERAR_SETOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_SETOR',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONSIDERAR_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_CONSIDERAR_TIPO_GUIA,1,4000),substr(:new.IE_CONSIDERAR_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PROCEDIMENTO,1,4000),substr(:new.IE_TIPO_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PROCEDIMENTO',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DATA_ITEM,1,500);gravar_log_alteracao(substr(:old.IE_DATA_ITEM,1,4000),substr(:new.IE_DATA_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_ITEM',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_INICIAL,1,500);gravar_log_alteracao(substr(:old.NR_INICIAL,1,4000),substr(:new.NR_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INICIAL',ie_log_w,ds_w,'TISS_REGRA_GUIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_GUIA ADD (
  CONSTRAINT TISSREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_GUIA ADD (
  CONSTRAINT TISSREG_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSREG_FUNMEDI_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO_MEDICO (CD_FUNCAO),
  CONSTRAINT TISSREG_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT TISSREG_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT TISSREG_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT TISSREG_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT TISSREG_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT TISSREG_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT TISSREG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TISS_REGRA_GUIA TO NIVEL_1;

