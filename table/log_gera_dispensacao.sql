ALTER TABLE TASY.LOG_GERA_DISPENSACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_GERA_DISPENSACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_GERA_DISPENSACAO
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NM_USUARIO    VARCHAR2(15 BYTE)               NOT NULL,
  DT_INICIO     DATE,
  DT_FINAL      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOGEDIS_PK ON TASY.LOG_GERA_DISPENSACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGEDIS_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOG_GERA_DISPENSACAO ADD (
  CONSTRAINT LOGEDIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LOG_GERA_DISPENSACAO TO NIVEL_1;

