ALTER TABLE TASY.TITULO_PAGAR_BAIXA_CC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_BAIXA_CC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_BAIXA_CC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_TITULO             NUMBER(10)              NOT NULL,
  NR_SEQ_BAIXA          NUMBER(10)              NOT NULL,
  CD_CENTRO_CUSTO       NUMBER(8),
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE),
  VL_BAIXA              NUMBER(15,2)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CONTA_FINANC       NUMBER(10),
  VL_JUROS              NUMBER(15,2),
  VL_MULTA              NUMBER(15,2),
  VL_DESCONTO           NUMBER(15,2),
  VL_OUTROS_ACRESCIMOS  NUMBER(15,2),
  VL_PAGO               NUMBER(15,2),
  NR_SEQ_CLASSIF        NUMBER(10),
  VL_BAIXA_ESTRANG      NUMBER(15,2),
  CD_MOEDA              NUMBER(5),
  VL_COTACAO            NUMBER(21,10),
  VL_COMPLEMENTO        NUMBER(15,2),
  VL_OUTRAS_DESPESAS    NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPABCC_CENCUST_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABCC_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABCC_CONCONT_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABCC_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABCC_CONFINA_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABCC_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABCC_MOEDA_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPABCC_PK ON TASY.TITULO_PAGAR_BAIXA_CC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABCC_TITPABA_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABCC_TITPACL_FK_I ON TASY.TITULO_PAGAR_BAIXA_CC
(NR_TITULO, NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABCC_TITPACL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_baixa_cc_aftins
after insert ON TASY.TITULO_PAGAR_BAIXA_CC for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_titulo,:new.nr_seq_baixa,'TPB',:new.dt_atualizacao,'I',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_baixa_cc_after
after insert ON TASY.TITULO_PAGAR_BAIXA_CC for each row
declare

vl_movimento_w			ctb_documento.vl_movimento%type;
dt_competencia_w		ctb_documento.dt_competencia%type;
cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;
nr_seq_trans_fin_w		ctb_documento.nr_seq_trans_financ%type;
ie_contab_classif_baixa_w	parametros_contas_pagar.ie_contab_classif_baixa%type;
ie_tipo_titulo_w		titulo_pagar.ie_tipo_titulo%type;
nm_tabela_w			ctb_documento.nm_tabela%type;
ie_contab_cp_w			parametro_tesouraria.ie_contab_cp%type;

cursor c01 is
	select	a.nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 7
	and 	a.nm_atributo in ( 	'VL_BAIXA', 'VL_DESCONTOS', 'VL_JUROS',
					'VL_MULTA', 'VL_OUTROS_ACRESCIMOS', 'VL_PAGO',
					'VL_BAIXA_ESTRANG', 'VL_MOEDA_COMPLEMENTAR');

c01_w		c01%rowtype;

cursor c02 is
	select	a.nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 10
	and 	a.nm_atributo in (	'VL_BAIXA', 'VL_DESCONTOS', 'VL_JUROS',
					'VL_MULTA', 'VL_OUTROS_ACRESCIMOS');

c02_w		c02%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select 	a.dt_baixa,
	b.cd_estabelecimento,
	a.nr_seq_trans_fin,
	ie_tipo_titulo
into	dt_competencia_w,
	cd_estabelecimento_w,
	nr_seq_trans_fin_w,
	ie_tipo_titulo_w
from	titulo_pagar_baixa a,
	titulo_pagar b
where	a.nr_titulo	= b.nr_titulo
and	a.nr_sequencia 	= :new.nr_seq_baixa
and 	a.nr_titulo	= :new.nr_titulo;

begin
select  nvl(ie_contab_classif_baixa, 'N')
into    ie_contab_classif_baixa_w
from    parametros_contas_pagar
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
        ie_contab_classif_baixa_w := 'N';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_classif_baixa, 'N'), ie_contab_classif_baixa_w)
into    ie_contab_classif_baixa_w
from    ctb_param_lote_contas_pag a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	vl_movimento_w:= null;

	if	((ie_tipo_titulo_w in ('5','14','26')) and
		(c01_w.nm_atributo <> 'VL_JUROS') and
		(ie_contab_classif_baixa_w 	= 'S') and
		(c01_w.nm_atributo <> 'VL_BAIXA_ESTRANG') and
		(c01_w.nm_atributo <> 'VL_MOEDA_COMPLEMENTAR')) then
		begin

		vl_movimento_w:= 	case c01_w.nm_atributo
					when 'VL_BAIXA' then :new.vl_baixa
					when 'VL_DESCONTOS' then :new.vl_desconto
					when 'VL_MULTA'  then :new.vl_multa
					when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
					when 'VL_PAGO'  then :new.vl_pago
					else
						null
					end;

		if	(nvl(vl_movimento_w, 0) <> 0) then
			begin
			delete 	from	ctb_documento
			where	nm_atributo = c01_w.nm_atributo
			and	nr_documento = :new.nr_titulo
			and 	dt_competencia = dt_competencia_w
			and 	nr_seq_doc_compl = :new.nr_seq_baixa;

			nm_tabela_w:= 'TITULO_PAGAR_BAIXA_CC';

			end;
		end if;



		end;
	elsif ((ie_tipo_titulo_w = '21') and
		(c01_w.nm_atributo <> 'VL_BAIXA') and
		(ie_contab_classif_baixa_w 	= 'S') and
		(c01_w.nm_atributo <> 'VL_BAIXA_ESTRANG') and
		(c01_w.nm_atributo <> 'VL_MOEDA_COMPLEMENTAR')) then
		begin

		vl_movimento_w:= 	case c01_w.nm_atributo
					when 'VL_JUROS' then :new.vl_juros
					when 'VL_DESCONTOS' then :new.vl_desconto
					when 'VL_MULTA'  then :new.vl_multa
					when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
					when 'VL_PAGO'  then :new.vl_pago
					else
						null
					end;

		if	(nvl(vl_movimento_w, 0) <> 0) then
			begin

			delete 	from	ctb_documento
			where	nm_atributo = c01_w.nm_atributo
			and	nr_documento = :new.nr_titulo
			and 	dt_competencia = dt_competencia_w
			and 	nr_seq_doc_compl = :new.nr_seq_baixa;

			nm_tabela_w:= 'TITULO_PAGAR_BAIXA_CC';

			end;
		end if;

		end;
	elsif	((ie_contab_classif_baixa_w = 'T') and
		(c01_w.nm_atributo <> 'VL_BAIXA_ESTRANG') and
		(c01_w.nm_atributo <> 'VL_MOEDA_COMPLEMENTAR')) then
		begin

		vl_movimento_w:= 	case c01_w.nm_atributo
					when 'VL_BAIXA' then :new.vl_baixa
					when 'VL_JUROS' then :new.vl_juros
					when 'VL_DESCONTOS' then :new.vl_desconto
					when 'VL_MULTA'  then :new.vl_multa
					when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
					when 'VL_PAGO'  then :new.vl_pago
					else
						null
					end;



		if	(nvl(vl_movimento_w, 0) <> 0) then
			begin

			delete 	from	ctb_documento
			where	nm_atributo = c01_w.nm_atributo
			and	nr_documento = :new.nr_titulo
			and 	dt_competencia = dt_competencia_w
			and 	nr_seq_doc_compl = :new.nr_seq_baixa;

			nm_tabela_w:= 'TITULO_PAGAR_BAIXA_CC_CONTAB_V';

			end;
		end if;

		end;
	else
		begin
		vl_movimento_w	:=	case c01_w.nm_atributo
					when 'VL_BAIXA_ESTRANG' then :new.vl_baixa_estrang
					when 'VL_MOEDA_COMPLEMENTAR' then :new.vl_complemento
					else
						null
					end;

		nm_tabela_w:=	'TITULO_PAGAR_BAIXA_CC';
		end;
	end if;



	if	(nvl(vl_movimento_w, 0) <> 0) then
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									dt_competencia_w,
									7,
									nr_seq_trans_fin_w,
									13,
									:new.nr_titulo,
									:new.nr_seq_baixa,
									:new.nr_sequencia,
									vl_movimento_w,
									nm_tabela_w,
									c01_w.nm_atributo,
									:new.nm_usuario);

		end;
	end if;

	end;
end loop;
close c01;


begin
select  nvl(ie_contab_cp, 'N')
into    ie_contab_cp_w
from    parametro_tesouraria
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
        ie_contab_cp_w := 'N';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_cp, 'N'), ie_contab_cp_w)
into    ie_contab_cp_w
from    ctb_param_lote_tesouraria a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

if	(ie_contab_cp_w = 'S') then
	begin

	open c02;
	loop
	fetch c02 into
		c02_w;
	exit when c02%notfound;
		begin

		vl_movimento_w:= 	case c01_w.nm_atributo
					when 'VL_BAIXA' then :new.vl_baixa
					when 'VL_DESCONTOS' then :new.vl_desconto
					when 'VL_JUROS'  then :new.vl_juros
					when 'VL_MULTA'  then :new.vl_multa
					when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
					end;

		delete 	from	ctb_documento
		where	nm_atributo = c01_w.nm_atributo
		and 	nm_tabela = 'TITULO_PAGAR_BAIXA_CC_CONTAB_V'
		and	nr_documento = :new.nr_titulo
		and 	dt_competencia = dt_competencia_w
		and 	nr_seq_doc_compl = :new.nr_seq_baixa;

		if	(nvl(vl_movimento_w, 0) <> 0) then
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									dt_competencia_w,
									10,
									nr_seq_trans_fin_w,
									13,
									:new.nr_titulo,
									:new.nr_sequencia,
									null,
									vl_movimento_w,
									'TITULO_PAGAR_BAIXA_CC_CONTAB_V',
									c02_w.nm_atributo,
									:new.nm_usuario);


			end;
		end if;

		end;
	end loop;
	close c02;

	end;
end if;
end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_BAIXA_CC ADD (
  CONSTRAINT TITPABCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_BAIXA_CC ADD (
  CONSTRAINT TITPABCC_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITPABCC_TITPABA_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_PAGAR_BAIXA (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT TITPABCC_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITPABCC_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITPABCC_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TITPABCC_TITPACL_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_CLASSIF) 
 REFERENCES TASY.TITULO_PAGAR_CLASSIF (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TITULO_PAGAR_BAIXA_CC TO NIVEL_1;

