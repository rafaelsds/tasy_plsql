ALTER TABLE TASY.FIS_REINF_R4010_OP_SAUDE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_REINF_R4010_OP_SAUDE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_REINF_R4010_OP_SAUDE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_NR_INSC           VARCHAR2(14 BYTE),
  NR_REG_ANS           NUMBER(6),
  VL_SAUDE             NUMBER(14,2),
  NR_SEQ_EVENTO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.R4010OPSAU_EVENTO4010_FK_I ON TASY.FIS_REINF_R4010_OP_SAUDE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.R4010OPSAU_PK ON TASY.FIS_REINF_R4010_OP_SAUDE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_REINF_R4010_OP_SAUDE ADD (
  CONSTRAINT R4010OPSAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_REINF_R4010_OP_SAUDE ADD (
  CONSTRAINT R4010OPSAU_EVENTO4010_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.FIS_REINF_R4010_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_REINF_R4010_OP_SAUDE TO NIVEL_1;

