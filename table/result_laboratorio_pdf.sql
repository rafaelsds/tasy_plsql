ALTER TABLE TASY.RESULT_LABORATORIO_PDF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_LABORATORIO_PDF CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_LABORATORIO_PDF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(14),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRESCRICAO    NUMBER(10),
  NR_SEQ_RESULTADO     NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10),
  DS_PDF_SERIAL        LONG                     NOT NULL,
  DS_PDF_SERIAL_CLOB   CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_PDF_SERIAL_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RESLABPDF_PK ON TASY.RESULT_LABORATORIO_PDF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLABPDF_PK
  MONITORING USAGE;


CREATE INDEX TASY.RESLABPDF_RESLABO_FK_I ON TASY.RESULT_LABORATORIO_PDF
(NR_SEQ_RESULTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLABPDF_RESLABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESLABPDF_RESLABP_FK_I ON TASY.RESULT_LABORATORIO_PDF
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.result_laboratorio_pdf_insert
after insert ON TASY.RESULT_LABORATORIO_PDF 
for each row
declare
ie_status_atend_w		prescr_procedimento.ie_status_atend%type;
ie_suspenso_w			prescr_procedimento.ie_suspenso%type;
total_err_w			number(3);
qt_reg_w			number(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	Goto Final;
end if;

if (wheb_usuario_pck.is_evento_ativo(766) = 'S') then

	select a.ie_status_atend, ie_suspenso
		into ie_status_atend_w, ie_suspenso_w
	from prescr_procedimento a
		where a.nr_sequencia = :new.nr_seq_prescricao
		and a.nr_prescricao = :new.nr_prescricao;

	if (ie_status_atend_w >= 35) and (ie_suspenso_w <> 'S') then
		integrar_unimed_rs_ws(766, :new.nr_prescricao, :new.nr_seq_prescricao , :new.nm_usuario, 'Registro');
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.RESULT_LABORATORIO_PDF ADD (
  CONSTRAINT RESLABPDF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULT_LABORATORIO_PDF ADD (
  CONSTRAINT RESLABPDF_RESLABO_FK 
 FOREIGN KEY (NR_SEQ_RESULTADO) 
 REFERENCES TASY.RESULT_LABORATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RESULT_LABORATORIO_PDF TO NIVEL_1;

