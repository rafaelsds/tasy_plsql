ALTER TABLE TASY.FA_ENTREGA_MEDICACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FA_ENTREGA_MEDICACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FA_ENTREGA_MEDICACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_PERIODO_INICIAL       DATE,
  NR_SEQ_MOTIVO_CANC       NUMBER(10),
  NR_SEQ_RECEITA_AMB       NUMBER(10),
  DT_PERIODO_FINAL         DATE,
  DT_PREVISTA_RETORNO      DATE,
  NM_USUARIO_CANC          VARCHAR2(15 BYTE),
  NR_DIAS_UTIL             NUMBER(4),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  DT_ENTREGA_MEDICACAO     DATE,
  NR_SEQ_PACIENTE_ENTREGA  NUMBER(10),
  IE_STATUS_MEDICACAO      VARCHAR2(15 BYTE)    NOT NULL,
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  DT_CANCELAMENTO          DATE,
  DT_IMPRESSAO             DATE,
  NM_USUARIO_IMP           VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FAENTME_ESTABEL_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAENTME_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAENTME_FAMOTCE_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAENTME_FAMOTCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAENTME_FAPACEN_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(NR_SEQ_PACIENTE_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAENTME_FAREFAR_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(NR_SEQ_RECEITA_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAENTME_FAREFAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAENTME_PESFISI_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FAENTME_PK ON TASY.FA_ENTREGA_MEDICACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAENTME_SETATEN_FK_I ON TASY.FA_ENTREGA_MEDICACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAENTME_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.FA_ENTREGA_MEDICACAO_tp  after update ON TASY.FA_ENTREGA_MEDICACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_PREVISTA_RETORNO,1,4000),substr(:new.DT_PREVISTA_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'DT_PREVISTA_RETORNO',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RECEITA_AMB,1,4000),substr(:new.NR_SEQ_RECEITA_AMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RECEITA_AMB',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_MEDICACAO,1,4000),substr(:new.IE_STATUS_MEDICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_MEDICACAO',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_FINAL,1,4000),substr(:new.DT_PERIODO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_FINAL',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PACIENTE_ENTREGA,1,4000),substr(:new.NR_SEQ_PACIENTE_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PACIENTE_ENTREGA',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_INICIAL,1,4000),substr(:new.DT_PERIODO_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_INICIAL',ie_log_w,ds_w,'FA_ENTREGA_MEDICACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.fa_entrega_medicacao_update
before update ON TASY.FA_ENTREGA_MEDICACAO for each row
declare
cd_pessoa_fisica_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if (:old.ie_status_medicacao = :new.ie_status_medicacao) and (:new.dt_cancelamento is null) and ((:old.dt_impressao = :new.dt_impressao) or (:new.dt_impressao is null)) then
		if (:new.ie_status_medicacao = 'AC') then
			-- Status da entrega j� est� como Aguardando confer�ncia. Favor atualizar a tela!
			Wheb_mensagem_pck.exibir_mensagem_abort(215070);
		elsif (:new.ie_status_medicacao = 'CM') then
			-- Status da entrega j� est� como Confer�ncia da medica��o. Favor atualizar a tela!
			Wheb_mensagem_pck.exibir_mensagem_abort(215071);
		elsif (:new.ie_status_medicacao = 'EM') then
			-- Status da entrega j� est� como Entrega da medica��o. Favor atualizar a tela!
			Wheb_mensagem_pck.exibir_mensagem_abort(215072);
		end if;
	end if;

	if	(:new.dt_cancelamento is not null) and
		(:old.dt_cancelamento is null) and
		(:new.nr_seq_paciente_entrega is not null) then

		update	fa_paciente_entrega
		set	ie_status_paciente = 'EC'
		where 	nr_sequencia = :new.nr_seq_paciente_entrega;
	end if;

	if ((:new.cd_pessoa_fisica is not null) and (:new.nr_seq_receita_amb is not null)) then

		SELECT max(cd_pessoa_fisica)
		INTO   cd_pessoa_fisica_w
		FROM   fa_receita_farmacia
		WHERE  nr_sequencia = :new.nr_seq_receita_amb;

		if (:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
			-- O paciente da entrega � diferente da receita. Favor verificar!
			Wheb_mensagem_pck.exibir_mensagem_abort(215066);
		end if;

		select 	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	fa_paciente_entrega
		where	nr_sequencia 	= :new.nr_seq_paciente_entrega;

		if (:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
			-- O paciente da entrega � diferente do atendimento. Favor verificar!
			Wheb_mensagem_pck.exibir_mensagem_abort(215067);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.fa_entrega_medicacao_insert
before INSERT ON TASY.FA_ENTREGA_MEDICACAO FOR EACH ROW
declare
cd_pessoa_fisica_w	varchar2(10);
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

	if ((:new.cd_pessoa_fisica is not null) and (:new.nr_seq_receita_amb is not null)) then

		SELECT max(cd_pessoa_fisica)
		INTO   cd_pessoa_fisica_w
		FROM   fa_receita_farmacia
		WHERE  nr_sequencia = :new.nr_seq_receita_amb;

		if (:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
			-- O paciente da entrega � diferente da receita. Favor verificar!
			Wheb_mensagem_pck.exibir_mensagem_abort(215066);
		end if;

		select 	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	fa_paciente_entrega
		where	nr_sequencia 	= :new.nr_seq_paciente_entrega;

		if (:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
			-- O paciente da entrega � diferente do atendimento. Favor verificar!
			Wheb_mensagem_pck.exibir_mensagem_abort(215067);
		end if;
	end if;

end if;
END;
/


ALTER TABLE TASY.FA_ENTREGA_MEDICACAO ADD (
  CONSTRAINT FAENTME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FA_ENTREGA_MEDICACAO ADD (
  CONSTRAINT FAENTME_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FAENTME_FAREFAR_FK 
 FOREIGN KEY (NR_SEQ_RECEITA_AMB) 
 REFERENCES TASY.FA_RECEITA_FARMACIA (NR_SEQUENCIA),
  CONSTRAINT FAENTME_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FAENTME_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT FAENTME_FAMOTCE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.FA_MOTIVO_CANC_ENTREGA (NR_SEQUENCIA),
  CONSTRAINT FAENTME_FAPACEN_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE_ENTREGA) 
 REFERENCES TASY.FA_PACIENTE_ENTREGA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FA_ENTREGA_MEDICACAO TO NIVEL_1;

