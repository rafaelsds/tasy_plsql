ALTER TABLE TASY.CAPTURA_IMAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAPTURA_IMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAPTURA_IMAGEM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DS_CAMINHO             VARCHAR2(4000 BYTE)    NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_IMPRESSAO           VARCHAR2(1 BYTE),
  NR_ORDEM_APRESENTACAO  NUMBER(10),
  DS_CAMINHO_BACKUP      VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CAPIMG_PK ON TASY.CAPTURA_IMAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.captura_imagem_delete
BEFORE DELETE ON TASY.CAPTURA_IMAGEM FOR EACH ROW
DECLARE

BEGIN
  gravar_log_exclusao('CAPTURA_IMAGEM', obter_usuario_ativo, 'NR_SEQUENCIA=' || :OLD.nr_sequencia,'N');
END;
/


CREATE OR REPLACE TRIGGER TASY.CAPTURA_IMAGEM_tp  after update ON TASY.CAPTURA_IMAGEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'CAPTURA_IMAGEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CAPTURA_IMAGEM ADD (
  CONSTRAINT CAPIMG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CAPTURA_IMAGEM TO NIVEL_1;

