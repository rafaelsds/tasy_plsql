ALTER TABLE TASY.TIPO_EVOLUCAO_PAC_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_EVOLUCAO_PAC_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_EVOLUCAO_PAC_GRUPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  CD_TIPO_EVOLUCAO     VARCHAR2(3 BYTE)         NOT NULL,
  IE_DESVINCULAR_ALTA  VARCHAR2(1 BYTE),
  IE_TIPO_VINCULO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPEVPG_PACGRUP_FK_I ON TASY.TIPO_EVOLUCAO_PAC_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPEVPG_PACGRUP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIPEVPG_PK ON TASY.TIPO_EVOLUCAO_PAC_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPEVPG_TIPEVOL_FK_I ON TASY.TIPO_EVOLUCAO_PAC_GRUPO
(CD_TIPO_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_EVOLUCAO_PAC_GRUPO ADD (
  CONSTRAINT TIPEVPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_EVOLUCAO_PAC_GRUPO ADD (
  CONSTRAINT TIPEVPG_PACGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PAC_GRUPO (NR_SEQUENCIA),
  CONSTRAINT TIPEVPG_TIPEVOL_FK 
 FOREIGN KEY (CD_TIPO_EVOLUCAO) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TIPO_EVOLUCAO_PAC_GRUPO TO NIVEL_1;

