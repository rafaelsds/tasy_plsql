ALTER TABLE TASY.ESCALA_CAPURRO_NEURO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CAPURRO_NEURO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CAPURRO_NEURO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_TEXTURA_PELE        NUMBER(10)             NOT NULL,
  QT_PREGAS_PLANTARES    NUMBER(10)             NOT NULL,
  QT_GLANDULA_MAMARIA    NUMBER(10)             NOT NULL,
  QT_FORMATO_ORELHA      NUMBER(10)             NOT NULL,
  QT_ANGULO_CABECA       NUMBER(10)             NOT NULL,
  QT_DIAS                NUMBER(10),
  QT_SEMANAS             NUMBER(10),
  QT_SINAL_XALE          NUMBER(10)             NOT NULL,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  DS_UTC                 VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO       VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO     VARCHAR2(50 BYTE),
  NR_SEQ_NASCIMENTO      NUMBER(3)              DEFAULT null,
  NR_ATEND_RN            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPACN_ATEPACI_FK_I ON TASY.ESCALA_CAPURRO_NEURO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPACN_ATEPACI_FK2_I ON TASY.ESCALA_CAPURRO_NEURO
(NR_ATEND_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPACN_PESFISI_FK_I ON TASY.ESCALA_CAPURRO_NEURO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPACN_PK ON TASY.ESCALA_CAPURRO_NEURO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_CAPURRO_NEURO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_CAPURRO_NEURO FOR EACH ROW
declare

qt_pontuacao_w 	number(10);
qt_reg_w		number(10);
cd_estabelecimento_w	number(10);
ie_ig_capurro_w		varchar2(5);


BEGIN

qt_pontuacao_w := 0;

qt_pontuacao_w := :new.QT_TEXTURA_PELE + :new.QT_PREGAS_PLANTARES + :new.QT_GLANDULA_MAMARIA +
		  :new.QT_FORMATO_ORELHA + :new.QT_ANGULO_CABECA  + :new.QT_SINAL_XALE;

:new.qt_dias := qt_pontuacao_w + 200;

:new.QT_SEMANAS	:= dividir(:new.qt_dias,7);


select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

select	count(*)
into	qt_reg_w
from	nascimento
where	nr_atendimento = :new.nr_atendimento;

select	nvl(max(ie_ig_capurro),'N')
into	ie_ig_capurro_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(qt_reg_w > 0) and
	(ie_ig_capurro_w = 'S') then
	update	nascimento
	set	qt_sem_ig = :new.QT_SEMANAS,
		qt_dia_ig = :new.QT_DIAS
	where	nr_atendimento = :new.nr_atendimento;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.escala_capurro_neuro_atual
before update ON TASY.ESCALA_CAPURRO_NEURO for each row
declare

qt_reg_w		number(10);
cd_estabelecimento_w	number(10);
ie_ig_capurro_w		varchar2(5);

begin

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

select	count(*)
into	qt_reg_w
from	nascimento
where	nr_atendimento = :new.nr_atendimento;

select	nvl(max(ie_ig_capurro),'N')
into	ie_ig_capurro_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(qt_reg_w > 0) and
	(ie_ig_capurro_w = 'S') then
	update	nascimento
	set	qt_sem_ig = :new.QT_SEMANAS,
		qt_dia_ig = :new.QT_DIAS
	where	nr_atendimento = :new.nr_atendimento;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_CAPURRO_NEURO_BEFINSUP
before insert or update ON TASY.ESCALA_CAPURRO_NEURO for each row
declare


begin
if	(nvl(:old.DT_AVALIACAO,sysdate+10) <> :new.DT_AVALIACAO) and
	(:new.DT_AVALIACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_AVALIACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


ALTER TABLE TASY.ESCALA_CAPURRO_NEURO ADD (
  CONSTRAINT ESCPACN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CAPURRO_NEURO ADD (
  CONSTRAINT ESCPACN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPACN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCPACN_ATEPACI_FK2 
 FOREIGN KEY (NR_ATEND_RN) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_CAPURRO_NEURO TO NIVEL_1;

