ALTER TABLE TASY.REGRA_OBRIGA_NF_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_OBRIGA_NF_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_OBRIGA_NF_ATRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REONFAT_PK ON TASY.REGRA_OBRIGA_NF_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REONFAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.REONFAT_REONFES_FK_I ON TASY.REGRA_OBRIGA_NF_ATRIB
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_OBRIGA_NF_ATRIB_tp  after update ON TASY.REGRA_OBRIGA_NF_ATRIB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'REGRA_OBRIGA_NF_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ATRIBUTO,1,4000),substr(:new.NM_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO',ie_log_w,ds_w,'REGRA_OBRIGA_NF_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_TABELA,1,4000),substr(:new.NM_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NM_TABELA',ie_log_w,ds_w,'REGRA_OBRIGA_NF_ATRIB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_OBRIGA_NF_ATRIB ADD (
  CONSTRAINT REONFAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_OBRIGA_NF_ATRIB ADD (
  CONSTRAINT REONFAT_REONFES_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_OBRIGA_NF_ESTRUT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_OBRIGA_NF_ATRIB TO NIVEL_1;

