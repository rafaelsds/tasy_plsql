ALTER TABLE TASY.TST_TESTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TST_TESTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TST_TESTE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PROT_TESTE       NUMBER(10),
  NR_SEQ_AGENDA           NUMBER(10)            NOT NULL,
  NR_SEQ_PROT_TESTE_PROC  NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TSTTEST_PK ON TASY.TST_TESTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TSTTEST_PK
  MONITORING USAGE;


CREATE INDEX TASY.TSTTEST_TESFUNC_FK_I ON TASY.TST_TESTE
(NR_SEQ_PROT_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TSTTEST_TESFUNC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TSTTEST_TSTAGEN_FK_I ON TASY.TST_TESTE
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TSTTEST_TSTAGEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TST_TESTE ADD (
  CONSTRAINT TSTTEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TST_TESTE ADD (
  CONSTRAINT TSTTEST_TSTAGEN_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.TST_AGENDA (NR_SEQUENCIA),
  CONSTRAINT TSTTEST_TESFUNC_FK 
 FOREIGN KEY (NR_SEQ_PROT_TESTE) 
 REFERENCES TASY.TESTE_FUNCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TST_TESTE TO NIVEL_1;

