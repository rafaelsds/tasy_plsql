ALTER TABLE TASY.INT_DISP_LANC_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INT_DISP_LANC_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.INT_DISP_LANC_MATERIAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE,
  NM_USUARIO            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_ACAO               VARCHAR2(15 BYTE),
  NR_ATENDIMENTO        NUMBER(10),
  CD_LOCAL_ESTOQUE      NUMBER(4),
  CD_OPERACAO_ESTOQUE   NUMBER(3),
  CD_MATERIAL           NUMBER(6),
  QT_ATUAL              NUMBER(15,4),
  DT_MOVIMENTO          DATE,
  CD_CGC                VARCHAR2(14 BYTE),
  IE_ATUALIZOU          VARCHAR2(1 BYTE),
  DS_ERRO               VARCHAR2(2000 BYTE),
  NR_SEQ_MAT_HOR        NUMBER(10),
  CD_BARRAS             VARCHAR2(4000 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_PRESCRICAO         NUMBER(14),
  NR_CIRURGIA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTDLMA_CIRURGI_FK_I ON TASY.INT_DISP_LANC_MATERIAL
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTDLMA_PK ON TASY.INT_DISP_LANC_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTDLMA_PK
  MONITORING USAGE;


CREATE INDEX TASY.INTDLMA_PRESMED_FK_I ON TASY.INT_DISP_LANC_MATERIAL
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTDLMA_SETATEN_FK_I ON TASY.INT_DISP_LANC_MATERIAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.int_disp_lanc_material_insert
before insert ON TASY.INT_DISP_LANC_MATERIAL for each row
declare

ds_erro_w	varchar2(2000);

begin
if	(:new.cd_acao is null) or (:new.cd_acao not in ('EVM','ERM'))then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(310130); --'Campo CD_ACAO esta nulo ou valor invalido.';
end if;
if	(:new.nr_atendimento is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(310131); --Campo NR_ATENDIMENTO esta nulo.
end if;
if	(:new.cd_local_estoque is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(310132); --Campo CD_LOCAL_ESTOQUE esta nulo.
end if;
if	(:new.cd_material is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(310133); --Campo CD_MATERIAL esta nulo.
end if;
if	(:new.qt_atual is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(310134); --Campo QT_ATUAL esta nulo.
end if;

:new.nm_usuario := :new.nm_usuario_nrec;

 intdisp_lancamento_material(
		:new.cd_acao,
		:new.nr_atendimento,
		:new.cd_local_estoque,
		:new.cd_operacao_estoque,
		:new.cd_material,
		:new.qt_atual,
		:new.dt_movimento,
		:new.cd_cgc,
		:new.ie_atualizou,
		:new.ds_erro,
		:new.nr_seq_mat_hor,
		:new.cd_barras,
		:new.nm_usuario_nrec,
		'N',
		:new.cd_setor_atendimento,
		:new.nr_cirurgia);

if	(:new.ds_erro is not null) or (ds_erro_w is not null) then
	:new.ds_erro := substr(:new.ds_erro||' | '||ds_erro_w,1,2000);
end if;

end;
/


ALTER TABLE TASY.INT_DISP_LANC_MATERIAL ADD (
  CONSTRAINT INTDLMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INT_DISP_LANC_MATERIAL ADD (
  CONSTRAINT INTDLMA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT INTDLMA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT INTDLMA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INT_DISP_LANC_MATERIAL TO NIVEL_1;

