ALTER TABLE TASY.PLS_COMISSAO_BENEF_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMISSAO_BENEF_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMISSAO_BENEF_ITEM
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_COMISSAO_BENEF      NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  TX_COMISSAO                NUMBER(7,4),
  IE_TIPO_ITEM               VARCHAR2(3 BYTE),
  VL_COMISSAO                NUMBER(15,2),
  VL_INCREMENTO              NUMBER(15,2),
  VL_ITEM_MENSALIDADE        NUMBER(15,2),
  VL_PREMIO                  NUMBER(15,2),
  NR_SEQ_PLANO               NUMBER(10),
  NR_SEQ_PLANO_SCA           NUMBER(10),
  NR_SEQ_BONIFICACAO         NUMBER(10),
  IE_SCA_EMBUTIDO            VARCHAR2(1 BYTE),
  VL_ORIGEM                  NUMBER(15,2),
  NR_SEQ_REGRA               NUMBER(10),
  NR_SEQ_REGRA_EQUIPE        NUMBER(10),
  NR_SEQ_REGRA_EQUIPE_SUP    NUMBER(10),
  CD_CONTA_CREDITO           VARCHAR2(20 BYTE),
  CD_CONTA_DEBITO            VARCHAR2(20 BYTE),
  CD_CLASSIF_CREDITO         VARCHAR2(40 BYTE),
  CD_CLASSIF_DEBITO          VARCHAR2(40 BYTE),
  NR_LOTE_CONTABIL           NUMBER(10),
  NR_SEQ_REGRA_CONTAB        NUMBER(10),
  CD_HISTORICO               NUMBER(10),
  NR_SEQ_ITEM_ORIGEM_CANCEL  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCBEI_CONCONT_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(CD_CONTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_CONCONT_FK2_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(CD_CONTA_DEBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_HISPADR_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_LOTCONT_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCBEI_PK ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCBEI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCBEI_PLSBONI_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_BONIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCBEI_PLSBONI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCBEI_PLSCBEI_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_ITEM_ORIGEM_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_PLSCOBE_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_COMISSAO_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCBEI_PLSCOBE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCBEI_PLSEMCO_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_REGRA_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_PLSPLAN_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCBEI_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCBEI_PLSPLAN_FK2_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_PLANO_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCBEI_PLSPLAN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCBEI_PLSRCMV_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_REGRA_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_PLSRRSI_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_REGRA_EQUIPE_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBEI_PLSVERE_FK_I ON TASY.PLS_COMISSAO_BENEF_ITEM
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_COMISSAO_BENEF_ITEM ADD (
  CONSTRAINT PLSCBEI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMISSAO_BENEF_ITEM ADD (
  CONSTRAINT PLSCBEI_PLSCOBE_FK 
 FOREIGN KEY (NR_SEQ_COMISSAO_BENEF) 
 REFERENCES TASY.PLS_COMISSAO_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSBONI_FK 
 FOREIGN KEY (NR_SEQ_BONIFICACAO) 
 REFERENCES TASY.PLS_BONIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_PLANO_SCA) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSEMCO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_EQUIPE) 
 REFERENCES TASY.PLS_EQUIPE_META_COMISSAO (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSRRSI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_EQUIPE_SUP) 
 REFERENCES TASY.PLS_REGRA_REPASSE_SUP_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSVERE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_VENDEDOR_REGRA (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CREDITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCBEI_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEBITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCBEI_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCBEI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCBEI_PLSRCMV_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTAB) 
 REFERENCES TASY.PLS_REGRA_CTB_MENSAL_VENDA (NR_SEQUENCIA),
  CONSTRAINT PLSCBEI_PLSCBEI_FK 
 FOREIGN KEY (NR_SEQ_ITEM_ORIGEM_CANCEL) 
 REFERENCES TASY.PLS_COMISSAO_BENEF_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMISSAO_BENEF_ITEM TO NIVEL_1;

