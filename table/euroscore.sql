ALTER TABLE TASY.EUROSCORE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EUROSCORE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EUROSCORE
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  NR_ATENDIMENTO                 NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  DT_AVALIACAO                   DATE           NOT NULL,
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE) NOT NULL,
  QT_ANO                         NUMBER(3)      NOT NULL,
  IE_SEXO                        VARCHAR2(1 BYTE) NOT NULL,
  IE_DOENCA_PULM_CRONICA         VARCHAR2(1 BYTE) NOT NULL,
  IE_ARTERIOPATIA_EXTRACARDIACA  VARCHAR2(1 BYTE) NOT NULL,
  IE_DISFUNCAO_NEUROLOGICA       VARCHAR2(1 BYTE) NOT NULL,
  IE_CIRURGIA_CARDIACA_PREVIA    VARCHAR2(1 BYTE) NOT NULL,
  IE_CREATININA_200              VARCHAR2(1 BYTE) NOT NULL,
  IE_ENDOCARTITE_ATIVA           VARCHAR2(1 BYTE) NOT NULL,
  IE_PREOPERATIVO_CRITICO        VARCHAR2(1 BYTE) NOT NULL,
  IE_ANGINA_INSTAVEL             VARCHAR2(1 BYTE) NOT NULL,
  IE_FUNCAO_VE                   VARCHAR2(1 BYTE) NOT NULL,
  IE_INFARTO_MIOCARDIO           VARCHAR2(1 BYTE) NOT NULL,
  IE_HIPERTENSAO_PULMONAR        VARCHAR2(1 BYTE) NOT NULL,
  IE_EMERGENCIA                  VARCHAR2(1 BYTE) NOT NULL,
  IE_OUTRO_PROC_REVASC           VARCHAR2(1 BYTE) NOT NULL,
  IE_CIRURGIA_AORTA              VARCHAR2(1 BYTE) NOT NULL,
  IE_RUPTURA_SEPTAL              VARCHAR2(1 BYTE) NOT NULL,
  QT_SCORE_ANO                   NUMBER(15,8)   NOT NULL,
  QT_SEXO                        NUMBER(15,8)   NOT NULL,
  QT_DOENCA_PULM_CRONICA         NUMBER(15,8)   NOT NULL,
  QT_ARTERIOPATIA_EXTRACARDIACA  NUMBER(15,8)   NOT NULL,
  QT_DISFUNCAO_NEUROLOGICA       NUMBER(15,8)   NOT NULL,
  QT_CIRURGIA_CARDIACA_PREVIA    NUMBER(15,8)   NOT NULL,
  QT_CREATININA_200              NUMBER(15,8)   NOT NULL,
  QT_ENDOCARTITE_ATIVA           NUMBER(15,8)   NOT NULL,
  QT_PREOPERATIVO_CRITICO        NUMBER(15,8)   NOT NULL,
  QT_ANGINA_INSTAVEL             NUMBER(15,8)   NOT NULL,
  QT_FUNCAO_VE                   NUMBER(15,8)   NOT NULL,
  QT_INFARTO_MIOCARDIO           NUMBER(15,8)   NOT NULL,
  QT_HIPERTENSAO_PULMONAR        NUMBER(15,8)   NOT NULL,
  QT_EMERGENCIA                  NUMBER(15,8)   NOT NULL,
  QT_OUTRO_PROC_REVASC           NUMBER(15,8)   NOT NULL,
  QT_CIRURGIA_AORTA              NUMBER(15,8)   NOT NULL,
  QT_RUPTURA_SEPTAL              NUMBER(15,8)   NOT NULL,
  IE_CALCULO                     VARCHAR2(1 BYTE) NOT NULL,
  QT_SCORE                       NUMBER(15,8),
  DT_LIBERACAO                   DATE,
  CD_PERFIL_ATIVO                NUMBER(5),
  IE_SITUACAO                    VARCHAR2(1 BYTE),
  DT_INATIVACAO                  DATE,
  NM_USUARIO_INATIVACAO          VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA               VARCHAR2(255 BYTE),
  NR_HORA                        NUMBER(2),
  QT_SCORE2                      NUMBER(15,8),
  IE_NIVEL_ATENCAO               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EUROSCO_ATEPACI_FK_I ON TASY.EUROSCORE
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EUROSCO_I1 ON TASY.EUROSCORE
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EUROSCO_PERFIL_FK_I ON TASY.EUROSCORE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EUROSCO_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EUROSCO_PESFISI_FK_I ON TASY.EUROSCORE
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EUROSCO_PK ON TASY.EUROSCORE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EUROSCO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.EUROSCORE_ATUAL
BEFORE INSERT OR UPDATE ON TASY.EUROSCORE FOR EACH ROW
DECLARE
qt_score_w			Number(22,14);
e				Number(22,14);
qt_reg_w	number(1);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_score_ano				:= 0;
:new.qt_sexo					:= 0;
:new.qt_doenca_pulm_cronica			:= 0;
:new.qt_arteriopatia_extracardiaca		:= 0;
:new.qt_disfuncao_neurologica		:= 0;
:new.qt_cirurgia_cardiaca_previa		:= 0;
:new.qt_creatinina_200			:= 0;
:new.qt_endocartite_ativa			:= 0;
:new.qt_preoperativo_critico		:= 0;
:new.qt_angina_instavel			:= 0;
:new.qt_funcao_ve				:= 0;
:new.qt_infarto_miocardio			:= 0;
:new.qt_hipertensao_pulmonar		:= 0;
:new.qt_emergencia				:= 0;
:new.qt_outro_proc_revasc			:= 0;
:new.qt_cirurgia_aorta			:= 0;
:new.qt_ruptura_septal			:= 0;
:new.qt_score					:= 0;

:new.qt_ano					:= nvl(:new.qt_ano,0);

if	(:new.qt_ano >= 100) then
	:new.qt_score_ano		:= 9;
elsif	(:new.qt_ano >= 95) then
	:new.qt_score_ano		:= 8;
elsif	(:new.qt_ano >= 90) then
	:new.qt_score_ano		:= 7;
elsif	(:new.qt_ano >= 85) then
	:new.qt_score_ano		:= 6;
elsif	(:new.qt_ano >= 80) then
	:new.qt_score_ano		:= 5;
elsif	(:new.qt_ano >= 75) then
	:new.qt_score_ano		:= 4;
elsif	(:new.qt_ano >= 70) then
	:new.qt_score_ano		:= 3;
elsif	(:new.qt_ano >= 65) then
	:new.qt_score_ano		:= 2;
elsif	(:new.qt_ano >= 60) then
	:new.qt_score_ano		:= 1;
else
	:new.qt_score_ano		:= 0;
end if;

if	(:new.ie_calculo in ('P','A')) then
	if	(:new.ie_sexo = 'F') then
		:new.qt_sexo				:= 1;
	end if;
	if	(:new.ie_doenca_pulm_cronica = 'S') then
		:new.qt_doenca_pulm_cronica		:= 1;
	end if;
	if	(:new.ie_arteriopatia_extracardiaca = 'S') then
		:new.qt_arteriopatia_extracardiaca	:= 2;
	end if;
	if	(:new.ie_disfuncao_neurologica = 'S') then
		:new.qt_disfuncao_neurologica	:= 2;
	end if;
	if	(:new.ie_cirurgia_cardiaca_previa = 'S') then
		:new.qt_cirurgia_cardiaca_previa	:= 3;
	end if;
	if	(:new.ie_creatinina_200 = 'S') then
		:new.qt_creatinina_200		:= 2;
	end if;
	if	(:new.ie_endocartite_ativa = 'S') then
		:new.qt_endocartite_ativa		:= 3;
	end if;
	if	(:new.ie_preoperativo_critico = 'S') then
		:new.qt_preoperativo_critico	:= 3;
	end if;
	if	(:new.ie_angina_instavel = 'S') then
		:new.qt_angina_instavel		:= 2;
	end if;
	if	(:new.ie_funcao_ve = 'I') then
		:new.qt_funcao_ve			:= 3;
	elsif	(:new.ie_funcao_ve = 'M') then
		:new.qt_funcao_ve			:= 1;
	end if;
	if	(:new.ie_infarto_miocardio = 'S') then
		:new.qt_infarto_miocardio		:= 2;
	end if;
	if	(:new.ie_hipertensao_pulmonar = 'S') then
		:new.qt_hipertensao_pulmonar	:= 2;
	end if;
	if	(:new.ie_emergencia	= 'S') then
		:new.qt_emergencia			:= 2;
	end if;
	if	(:new.ie_outro_proc_revasc = 'S') then
		:new.qt_outro_proc_revasc		:= 2;
	end if;
	if	(:new.ie_cirurgia_aorta = 'S') then
		:new.qt_cirurgia_aorta		:= 3;
	end if;
	if	(:new.ie_ruptura_septal = 'S') then
		:new.qt_ruptura_septal		:= 4;
	end if;
	:new.qt_score	:=
	:new.qt_score_ano +
	:new.qt_sexo +
	:new.qt_doenca_pulm_cronica	 +
	:new.qt_arteriopatia_extracardiaca +
	:new.qt_disfuncao_neurologica +
	:new.qt_cirurgia_cardiaca_previa +
	:new.qt_creatinina_200 +
	:new.qt_endocartite_ativa +
	:new.qt_preoperativo_critico +
	:new.qt_angina_instavel +
	:new.qt_funcao_ve +
	:new.qt_infarto_miocardio +
	:new.qt_hipertensao_pulmonar +
	:new.qt_emergencia +
	:new.qt_outro_proc_revasc +
	:new.qt_cirurgia_aorta +
	:new.qt_ruptura_septal;
end if;

if	(:new.ie_calculo in ('L','A')) then

	if	(:new.qt_ano > 59) then
		:new.qt_score_ano	:= ((:new.qt_ano - 58) * .066635);
	else
		:new.qt_score_ano	:= 1 * .066635;
	end if;

	if	(:new.ie_sexo = 'F') then
		:new.qt_sexo				:= .3304052;
	end if;
	if	(:new.ie_doenca_pulm_cronica = 'S') then
		:new.qt_doenca_pulm_cronica		:= .4931341;
	end if;
	if	(:new.ie_arteriopatia_extracardiaca = 'S') then
		:new.qt_arteriopatia_extracardiaca	:= .6558917;
	end if;
	if	(:new.ie_disfuncao_neurologica = 'S') then
		:new.qt_disfuncao_neurologica	:= .841626;
	end if;
	if	(:new.ie_cirurgia_cardiaca_previa = 'S') then
		:new.qt_cirurgia_cardiaca_previa	:= 1.002625;
	end if;
	if	(:new.ie_creatinina_200 = 'S') then
		:new.qt_creatinina_200		:= .6521653;
	end if;
	if	(:new.ie_endocartite_ativa = 'S') then
		:new.qt_endocartite_ativa		:= 1.101265;
	end if;
	if	(:new.ie_preoperativo_critico = 'S') then
		:new.qt_preoperativo_critico	:= .9058132;
	end if;
	if	(:new.ie_angina_instavel = 'S') then
		:new.qt_angina_instavel		:= .5677075;
	end if;
	if	(:new.ie_funcao_ve = 'I') then
		:new.qt_funcao_ve			:= 1.094443;
	elsif	(:new.ie_funcao_ve = 'M') then
		:new.qt_funcao_ve			:= .4191643;
	end if;
	if	(:new.ie_infarto_miocardio = 'S') then
		:new.qt_infarto_miocardio		:= .5460218;
	end if;
	if	(:new.ie_hipertensao_pulmonar = 'S') then
		:new.qt_hipertensao_pulmonar	:= .7676924;
	end if;
	if	(:new.ie_emergencia	= 'S') then
		:new.qt_emergencia			:= .7127953;
	end if;
	if	(:new.ie_outro_proc_revasc = 'S') then
		:new.qt_outro_proc_revasc		:= .5420364;
	end if;
	if	(:new.ie_cirurgia_aorta = 'S') then
		:new.qt_cirurgia_aorta		:= 1.159787;
	end if;
	if	(:new.ie_ruptura_septal = 'S') then
		:new.qt_ruptura_septal		:= 1.462009;
	end if;
	qt_score_w := -4.789594 +
		:new.qt_score_ano +
		:new.qt_sexo +
		:new.qt_doenca_pulm_cronica	 +
		:new.qt_arteriopatia_extracardiaca +
		:new.qt_disfuncao_neurologica +
		:new.qt_cirurgia_cardiaca_previa +
		:new.qt_creatinina_200 +
		:new.qt_endocartite_ativa +
		:new.qt_preoperativo_critico +
		:new.qt_angina_instavel +
		:new.qt_funcao_ve +
		:new.qt_infarto_miocardio +
		:new.qt_hipertensao_pulmonar +
		:new.qt_emergencia +
		:new.qt_outro_proc_revasc +
		:new.qt_cirurgia_aorta +
		:new.qt_ruptura_septal;
	e	:= 2.718281828;
	if	(:new.ie_calculo  = 'A') then
		:new.qt_score2		:= (Power(e, qt_score_w) / (1 + power(e,qt_score_w)) * 100);
	else
		:new.qt_score		:= (Power(e, qt_score_w) / (1 + power(e,qt_score_w)) * 100);
	end if;
end if;
<<Final>>
qt_reg_w	:= 0;


END;
/


ALTER TABLE TASY.EUROSCORE ADD (
  CONSTRAINT EUROSCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EUROSCORE ADD (
  CONSTRAINT EUROSCO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EUROSCO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT EUROSCO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.EUROSCORE TO NIVEL_1;

