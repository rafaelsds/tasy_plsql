ALTER TABLE TASY.DPC_EDITION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DPC_EDITION CASCADE CONSTRAINTS;

CREATE TABLE TASY.DPC_EDITION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_START             DATE                     NOT NULL,
  DT_END               DATE,
  DT_IMPORT            DATE,
  DT_PROCEDURE_UPDATE  DATE,
  NR_VERSION           VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DPCEDTN_EMPRESA_FK_I ON TASY.DPC_EDITION
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DPCEDTN_ESTABEL_FK_I ON TASY.DPC_EDITION
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DPCEDTN_PK ON TASY.DPC_EDITION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.dpc_edition_ins_upd_after
after insert or update ON TASY.DPC_EDITION for each row
declare

nr_sequencia_w  patient_dpc.nr_sequencia%type;
nr_atendimento_w patient_dpc.nr_atendimento%type;
qt_w number(10);
nm_usuario_w varchar2(30);

cursor  c01 is
select  nr_sequencia,
	    nr_atendimento_w
from    patient_dpc
where   nr_seq_edition = :new.nr_sequencia
and     nvl(dt_end_dpc,sysdate) > :new.dt_end;

cursor c02 is
select  distinct obter_usuario_pf(a.cd_prescritor)
from 	pessoa_fisica c,
		medico b,
		prescr_medica a
where 	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and 	a.cd_prescritor  = b.cd_pessoa_fisica
and 	a.nr_atendimento = nr_atendimento_w
and 	b.ie_situacao = 'A'
union
select 	distinct obter_usuario_pf(a.cd_medico)
from 	pessoa_fisica c,
		medico b,
		evolucao_paciente a
where 	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and 	a.cd_medico = b.cd_pessoa_fisica
and 	a.nr_atendimento  = nr_atendimento_w
and 	b.ie_situacao = 'A'
union
select 	distinct obter_usuario_pf(a.cd_medico_referido)
from 	pessoa_fisica c,
		medico b,
		atendimento_paciente a
where 	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and 	a.cd_medico_referido = b.cd_pessoa_fisica
and 	a.nr_atendimento  = nr_atendimento_w
and 	b.ie_situacao = 'A'
union
select 	distinct obter_usuario_pf(b.cd_medico)
from 	pessoa_fisica c,
		pf_medico_externo b,
		pessoa_fisica m
where 	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and 	c.cd_pessoa_fisica = b.cd_pessoa_fisica
and 	m.cd_pessoa_fisica = b.cd_medico
and 	b.cd_pessoa_fisica = obter_pessoa_atendimento(nr_atendimento_w,'C');

begin

if ((get_uses_dpc() = 'S') and (:old.dt_end <> :new.dt_end)) then

	open c01;
	loop
	fetch c01 into
	nr_sequencia_w,
	nr_atendimento_w;
	exit when c01%notfound;

		select  count(*)
		into    qt_w
		from    pep_item_pendente
		where   ie_tipo_registro = 'DPC2'
		and     nr_seq_registro = nr_sequencia_w
		and     nr_atendimento = nr_atendimento_w;


		if (qt_w = 0) then

		select  obter_usuario_pf(cd_medico_resp)
		into 	nm_usuario_w
		from 	atendimento_paciente
		where 	nr_atendimento = nr_atendimento_w;


		insert into pep_item_pendente(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_registro,
		cd_pessoa_fisica,
		nr_atendimento,
		nr_seq_registro,
		ie_tipo_pendencia,
		ie_tipo_registro,
		ds_item)
		select
		pep_item_pendente_seq.nextval,
		sysdate,
		nm_usuario_w,
		sysdate,
		nm_usuario_w,
		:new.dt_end,
		obter_pessoa_atendimento(nr_atendimento_w,'C'),
		nr_atendimento_w,
		nr_sequencia_w,
		'L',
		'DPC2',
		obter_desc_expressao(974849)||' '|| to_char(:new.dt_end,'YYYY-MM-DD') as ds_item from dual;

			open c02;
			loop
			fetch c02 into
			nm_usuario_w;
			exit when c01%notfound;

			insert into pep_item_pendente(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_registro,
				cd_pessoa_fisica,
				nr_atendimento,
				nr_seq_registro,
				ie_tipo_pendencia,
				ie_tipo_registro,
				ds_item)
			select
				pep_item_pendente_seq.nextval,
				sysdate,
				nm_usuario_w,
				sysdate,
				nm_usuario_w,
				:new.dt_end,
				obter_pessoa_atendimento(nr_atendimento_w,'C'),
				nr_atendimento_w,
				nr_sequencia_w,
				'L',
				'DPC2',
				obter_desc_expressao(974849)||' '|| to_char(:new.dt_end,'YYYY-MM-DD') as ds_item from dual;

			end loop;
			close c02;


		end if;

	end loop;
	close c01;

end if;
end;
/


ALTER TABLE TASY.DPC_EDITION ADD (
  CONSTRAINT DPCEDTN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DPC_EDITION ADD (
  CONSTRAINT DPCEDTN_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT DPCEDTN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.DPC_EDITION TO NIVEL_1;

