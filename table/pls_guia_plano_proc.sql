ALTER TABLE TASY.PLS_GUIA_PLANO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GUIA_PLANO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GUIA_PLANO_PROC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_GUIA                 NUMBER(10),
  CD_PROCEDIMENTO             NUMBER(15),
  CD_PROCEDIMENTO_IMP         NUMBER(15),
  IE_ORIGEM_PROCED            NUMBER(10),
  DS_PROCEDIMENTO_IMP         VARCHAR2(255 BYTE),
  QT_SOLICITADA               NUMBER(7,3),
  QT_SOLICITADA_IMP           NUMBER(7,3),
  QT_AUTORIZADA               NUMBER(7,3),
  IE_STATUS                   VARCHAR2(2 BYTE),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIBERACAO        VARCHAR2(15 BYTE),
  NR_SEQ_PRORROG              NUMBER(10),
  CD_TIPO_TABELA_IMP          VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_LIMITACAO       NUMBER(10),
  NR_SEQ_REGRA_LIBERACAO      NUMBER(10),
  NR_SEQ_MOTIVO_EXC           NUMBER(10),
  IE_UTILIZADO                VARCHAR2(2 BYTE),
  QT_UTILIZADO                NUMBER(12,4),
  IE_COBRANCA_PREVISTA        VARCHAR2(1 BYTE),
  NR_SEQ_PROC_PRINC           NUMBER(10),
  NR_SEQ_COBERTURA            NUMBER(10),
  IE_TIPO_COBERTURA           VARCHAR2(1 BYTE),
  NR_SEQ_SCA_COBERTURA        NUMBER(10),
  NR_SEQ_PACOTE               NUMBER(10),
  VL_PROCEDIMENTO             NUMBER(15,2),
  VL_CUSTO_OPERACIONAL        NUMBER(15,2),
  VL_ANESTESISTA              NUMBER(15,2),
  VL_MATERIAIS                NUMBER(15,2),
  VL_MEDICO                   NUMBER(15,2),
  VL_AUXILIARES               NUMBER(15,2),
  NR_SEQ_REGRA                NUMBER(10),
  NR_SEQ_COMPL_PTU            NUMBER(10),
  NR_SEQ_CONVERSAO_PACOTE     NUMBER(10),
  CD_PORTE_ANESTESICO         VARCHAR2(10 BYTE),
  IE_LIBERA_INTERCAMBIO       VARCHAR2(1 BYTE),
  NR_SEQ_PROC_AUTOR           NUMBER(10),
  NR_SEQ_PRECO_PACOTE         NUMBER(10),
  CD_MEDICO_EXECUTOR          VARCHAR2(10 BYTE),
  IE_COBRANCA_PREVIA_SERVICO  VARCHAR2(1 BYTE),
  IE_ORIGEM_INCLUSAO          VARCHAR2(2 BYTE),
  VL_PROCEDIMENTO_CONTA       NUMBER(15,2),
  IE_EXIGE_VALOR_CONTA        VARCHAR2(1 BYTE),
  NR_SEQ_MAT_PRINC            NUMBER(10),
  VL_TOTAL_PACOTE             NUMBER(15,2),
  IE_TIPO_ANEXO               VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO_PTU         NUMBER(15),
  DS_PROCEDIMENTO_PTU         VARCHAR2(80 BYTE),
  IE_PACOTE_PTU               VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_CARENCIA        NUMBER(10),
  IE_TIPO_TABELA              VARCHAR2(3 BYTE),
  NR_SEQ_ITEM_TISS            NUMBER(4),
  IE_SERVICO_PROPRIO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLGUPRO_I ON TASY.PLS_GUIA_PLANO_PROC
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_I1 ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_PROC_PRINC, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_I2 ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_MAT_PRINC, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_I3 ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_GUIA, IE_ORIGEM_PROCED, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_PESFISI_FK_I ON TASY.PLS_GUIA_PLANO_PROC
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLGUPRO_PK ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_PLSGUIA_FK_I ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_PLSMOEX_FK_I ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_MOTIVO_EXC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUPRO_PLSMOEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLGUPRO_PLSPACO_FK_I ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_PROC_AUTOR_I ON TASY.PLS_GUIA_PLANO_PROC
(NR_SEQ_PROC_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUPRO_PROCEDI_FK_I ON TASY.PLS_GUIA_PLANO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_guia_plano_proc_Insert 
before insert or update ON TASY.PLS_GUIA_PLANO_PROC 
for each row
declare 
 
nr_seq_pacote_w			Number(10); 
 
ie_regra_preco_w		varchar2(3); 
nr_seq_regra_pacote_out_w	number(10); 
cd_proc_pacote_out_w		number(15)	:= null; 
ie_origem_pacote_out_w		number(15)	:= null; 
vl_pacote_out_w			number(15,2)	:= 0; 
vl_medico_out_w			number(15,2)	:= 0; 
vl_anestesista_out_w		number(15,2)	:= 0; 
vl_auxiliares_out_w		number(15,2)	:= 0; 
vl_custo_operacional_out_w	number(15,2)	:= 0; 
vl_materiais_out_w		number(15,2)	:= 0; 
nr_seq_intercambio_w		number(10); 
nr_seq_classificacao_prest_w	number(10); 
nr_seq_protocolo_w		number(10); 
dt_conta_guia_w			date; 
nr_seq_segurado_w		number(10); 
cd_guia_w			varchar2(30); 
sg_estado_w			pessoa_juridica.sg_estado%type; 
sg_estado_int_w			pessoa_juridica.sg_estado%type; 
nr_seq_prestador_prot_w		number(10); 
cd_estabelecimento_w		number(4); 
nr_seq_tipo_acomod_w		number(10); 
dt_pacote_w			date; 
ie_internado_w			varchar2(3); 
nr_seq_plano_w			number(10); 
nr_contrato_w			number(10); 
nr_seq_congenere_w		number(10); 
ie_origem_conta_w		varchar2(5); 
ie_tipo_intercambio_w		varchar2(5); 
nr_seq_conta_w			number(10); 
nr_seq_prestador_conta_w	number(10); 
cd_procedimento_w		number(15); 
ie_origem_proced_w		number(15); 
nr_seq_prestador_w		number(10); 
nr_seq_guia_w			number(10); 
ie_tipo_processo_w		varchar2(2); 
ie_tipo_intercambio_ww		pls_guia_plano.ie_tipo_intercambio%type; 
ie_ident_pacote_ptu_w		pls_param_intercambio_scs.ie_ident_pacote_ptu%type; 
 
begin 
 
select	max(nr_sequencia) 
into	nr_seq_pacote_w 
from	pls_pacote 
where	cd_procedimento		= :new.cd_procedimento 
and	ie_origem_proced	= :new.ie_origem_proced 
and	ie_situacao		= 'A'; 
	 
if	(nr_seq_pacote_w	is not null) then 
 
	select	nvl(ie_regra_preco,'N') 
	into	ie_regra_preco_w 
	from	pls_pacote a 
	where	a.nr_sequencia = nr_seq_pacote_w; 
	 
	nr_seq_guia_w	:= :new.nr_seq_guia; 
	 
	/*Obter dados da guia*/ 
	select	nr_seq_prestador, 
		cd_estabelecimento, 
		dt_solicitacao, 
		nr_seq_tipo_acomodacao, 
		nr_seq_plano, 
		pls_obter_internado_guia(nr_sequencia,'A'), 
		nr_seq_segurado, 
		ie_tipo_processo, 
		nvl(ie_tipo_intercambio, 'N') 
	into	nr_seq_prestador_w, 
		cd_estabelecimento_w, 
		dt_pacote_w, 
		nr_seq_tipo_acomod_w, 
		nr_seq_plano_w, 
		ie_internado_w, 
		nr_seq_segurado_w, 
		ie_tipo_processo_w, 
		ie_tipo_intercambio_ww 
	from	pls_guia_plano 
	where	nr_sequencia	= nr_seq_guia_w; 
	 
	if	(ie_regra_preco_w = 'S') then 
		if	(nvl(nr_seq_plano_w,0) = 0) then		 
			begin 
			select	a.nr_seq_plano 
			into	nr_seq_plano_w 
			from	pls_segurado			a	 
			where	a.nr_sequencia = nr_seq_segurado_w;	 
			exception 
			when others then 
				nr_seq_plano_w := null; 
			end; 
		end if; 
		 
		begin 
		select	nvl(a.nr_contrato,0), 
			b.nr_seq_congenere 
		into	nr_contrato_w, 
			nr_seq_congenere_w 
		from	pls_contrato	a, 
			pls_segurado	b 
		where	a.nr_sequencia	= b.nr_seq_contrato 
		and	b.nr_sequencia	= nr_seq_segurado_w; 
		exception 
		when others then 
			nr_contrato_w	:= 0; 
		end; 
		 
		if	(nr_seq_congenere_w is null) then 
			select	b.nr_seq_congenere 
			into	nr_seq_congenere_w 
			from	pls_segurado	b 
			where	b.nr_sequencia	= nr_seq_segurado_w; 
		end if; 
		 
		select	max(nr_seq_intercambio) 
		into	nr_seq_intercambio_w 
		from	pls_segurado 
		where	nr_sequencia	= nr_seq_segurado_w; 
		 
		select	max(nr_seq_classificacao) 
		into	nr_seq_classificacao_prest_w 
		from	pls_prestador 
		where	nr_sequencia = nr_seq_prestador_w; 
		 
		select	nvl(max(sg_estado),'X') 
		into	sg_estado_w 
		from	pessoa_juridica 
		where	cd_cgc	= (	select	max(cd_cgc_outorgante) 
					from	pls_outorgante 
					where	cd_estabelecimento	= cd_estabelecimento_w); 
		select	nvl(max(a.sg_estado),'X') 
		into	sg_estado_int_w 
		from	pessoa_juridica	a, 
			pls_congenere	b 
		where	a.cd_cgc	= b.cd_cgc 
		and	b.nr_sequencia	= nr_seq_congenere_w; 
 
		if	(sg_estado_w <> 'X') and 
			(sg_estado_int_w <> 'X') then 
			if	(sg_estado_w	= sg_estado_int_w) then 
				ie_tipo_intercambio_w	:= 'E'; 
			else	 
				ie_tipo_intercambio_w	:= 'N'; 
			end if; 
		else 
			ie_tipo_intercambio_w	:= 'A'; 
		end if; 
 
		pls_define_preco_pacote(cd_estabelecimento_w, 
					nr_seq_prestador_w, 
					nr_seq_tipo_acomod_w, 
					dt_pacote_w, 
					:new.cd_procedimento, 
					:new.ie_origem_proced, 
					ie_internado_w, 
					nr_seq_plano_w, 
					nr_contrato_w, 
					nr_seq_congenere_w, 
					:new.nm_usuario, 
					ie_origem_conta_w, 
					ie_tipo_intercambio_w, 
					nr_seq_pacote_w, 
					nr_seq_regra_pacote_out_w, 
					cd_proc_pacote_out_w, 
					ie_origem_pacote_out_w, 
					vl_pacote_out_w, 
					vl_medico_out_w, 
					vl_anestesista_out_w, 
					vl_auxiliares_out_w, 
					vl_custo_operacional_out_w, 
					vl_materiais_out_w, 
					nr_seq_intercambio_w, 
					nr_seq_classificacao_prest_w, 
					null, 
					nr_seq_segurado_w, 
					null, 
					'N', 
					null, 
					1); 
	end if; 
 
	:new.nr_seq_pacote	:= nr_seq_pacote_w; 
	 
	select 	max(nvl(ie_ident_pacote_ptu, 'N')) 
	into	ie_ident_pacote_ptu_w 
	from	pls_param_intercambio_scs; 
	 
	if	((nr_seq_pacote_w	is not null) and 
		(ie_tipo_processo_w	= 'I' and ie_tipo_intercambio_ww = 'I') and 
		(ie_ident_pacote_ptu_w	= 'S'))then 
		:new.ie_pacote_ptu	:= 'S'; 
	end if; 
end if; 
 
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_bloqueia_insert_proc_estag
before insert ON TASY.PLS_GUIA_PLANO_PROC for each row
declare

ie_estagio_w	pls_guia_plano.ie_estagio%type;
ds_estagio_w	Varchar2(255);
qt_rotina_w	Number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
		select	ie_estagio
		into	ie_estagio_w
		from	pls_guia_plano
		where	nr_sequencia	= :new.nr_seq_guia;
	exception
	when others then
		ie_estagio_w	:= 0;
	end;

	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'INCPROC%';

	if	(ie_estagio_w	<> 7) and (qt_rotina_w	= 0) then
		ds_estagio_w	:= obter_valor_dominio(2055,ie_estagio_w);
		wheb_mensagem_pck.exibir_mensagem_abort(255327,'DS_ESTAGIO=' || ds_estagio_w);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_alt_autor_proc_log_bd
before update or delete ON TASY.PLS_GUIA_PLANO_PROC for each row
declare

nm_machine_w		varchar2(255);
ie_tipo_alteracao_w	varchar2(1)	:= null;
ds_log_w		varchar2(2000)	:= null;
qt_rotina_w		number(10);

begin

if	(pls_se_aplicacao_tasy = 'N') then
	begin
	SELECT	machine
	INTO	nm_machine_w
	FROM	v$session
	WHERE	audsid = USERENV('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;

	/* Verificação de SCS */
	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'TASY_SCS%';

	if	(qt_rotina_w = 0) then
		if	(updating) then
			if	(nvl(:new.qt_solicitada,0) <>  nvl(:old.qt_solicitada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_solicitada_old= ' || :old.qt_solicitada || ' qt_solicitada_new= ' || :new.qt_solicitada;
			end if;

			if	(nvl(:new.qt_autorizada,0) <>  nvl(:old.qt_autorizada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_autorizada_old= ' || :old.qt_autorizada || ' qt_autorizada_new= ' || :new.qt_autorizada;
			end if;

			if	(nvl(:new.ie_status,0) <>  nvl(:old.ie_status,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'ie_status_old= ' || :old.ie_status || ' ie_estagio_new= ' || :new.ie_status;
			end if;

			if	(ds_log_w is not null) then
				ie_tipo_alteracao_w	:= 'A';
			end if;
		elsif	(deleting) then
			ie_tipo_alteracao_w	:= 'E';

			ds_log_w	:= 'Procedimento: ' || :new.cd_procedimento;
		end if;

		if	(ie_tipo_alteracao_w is not null) then
			insert into pls_log_alt_autor_bd
				(nr_sequencia,
				dt_alteracao,
				nr_seq_guia_plano,
				nr_seq_guia_plano_proc,
				nr_seq_guia_plano_mat,
				ie_tipo_alteracao,
				ds_log,
				nm_maquina)
			values	(pls_log_alt_autor_bd_seq.nextval,
				sysdate,
				:new.nr_seq_guia,
				:new.nr_sequencia,
				null,
				ie_tipo_alteracao_w,
				ds_log_w,
				nm_machine_w);
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_GUIA_PLANO_PROC ADD (
  CONSTRAINT PLGUPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GUIA_PLANO_PROC ADD (
  CONSTRAINT PLGUPRO_PLSMOEX_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXC) 
 REFERENCES TASY.PLS_MOTIVO_EXCLUSAO (NR_SEQUENCIA),
  CONSTRAINT PLGUPRO_PLSPACO_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PLS_PACOTE (NR_SEQUENCIA),
  CONSTRAINT PLGUPRO_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLGUPRO_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLGUPRO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_GUIA_PLANO_PROC TO NIVEL_1;

