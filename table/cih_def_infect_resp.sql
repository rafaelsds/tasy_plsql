ALTER TABLE TASY.CIH_DEF_INFECT_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_DEF_INFECT_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_DEF_INFECT_RESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DEF_INFECT    NUMBER(10)               NOT NULL,
  IE_MEDICO_CIENTE     VARCHAR2(1 BYTE),
  DS_RESPOSTA          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHDEFINFR_CIHDENF_FK_I ON TASY.CIH_DEF_INFECT_RESP
(NR_SEQ_DEF_INFECT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIHDEFINFR_PK ON TASY.CIH_DEF_INFECT_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHDEFINFR_PK
  MONITORING USAGE;


ALTER TABLE TASY.CIH_DEF_INFECT_RESP ADD (
  CONSTRAINT CIHDEFINFR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_DEF_INFECT_RESP ADD (
  CONSTRAINT CIHDEFINFR_CIHDENF_FK 
 FOREIGN KEY (NR_SEQ_DEF_INFECT) 
 REFERENCES TASY.CIH_DEF_INFECT (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIH_DEF_INFECT_RESP TO NIVEL_1;

