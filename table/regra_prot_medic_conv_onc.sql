ALTER TABLE TASY.REGRA_PROT_MEDIC_CONV_ONC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PROT_MEDIC_CONV_ONC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PROT_MEDIC_CONV_ONC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_PROTOCOLO                NUMBER(10),
  NR_SEQ_MEDICACAO            NUMBER(6),
  CD_PERFIL                   NUMBER(5),
  CD_CONVENIO                 NUMBER(5),
  IE_SOMENTE_ESTABELECIMENTO  VARCHAR2(1 BYTE),
  IE_RESTRINGE_EXIBICAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPRCVO_CONVENI_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPRCVO_ESTABEL_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPRCVO_PERFIL_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPRCVO_PK ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_PK
  MONITORING USAGE;


CREATE INDEX TASY.REPRCVO_PROMEDI_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPRCVO_PROTOCO_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPRCVO_SETATEN_FK_I ON TASY.REGRA_PROT_MEDIC_CONV_ONC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPRCVO_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_PROT_MEDIC_CONV_ONC ADD (
  CONSTRAINT REPRCVO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PROT_MEDIC_CONV_ONC ADD (
  CONSTRAINT REPRCVO_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT REPRCVO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REPRCVO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REPRCVO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REPRCVO_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO)
    ON DELETE CASCADE,
  CONSTRAINT REPRCVO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_PROT_MEDIC_CONV_ONC TO NIVEL_1;

