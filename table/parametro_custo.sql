ALTER TABLE TASY.PARAMETRO_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_CUSTO
(
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  CD_COMP_TAXA_FINANC         NUMBER(5),
  CD_COMP_IMP_RENDA           NUMBER(5),
  CD_COMP_CUSTO_IND_FIXO      NUMBER(5),
  CD_COMP_DESP_FIXAS          NUMBER(5),
  CD_COMP_PERC_LUCRO          NUMBER(5),
  CD_COMP_VALOR_LUCRO         NUMBER(5),
  CD_COMP_PERC_MARGEM         NUMBER(10),
  CD_COMP_VALOR_MARGEM        NUMBER(5),
  CD_COMP_REDUC_BRAS          NUMBER(5),
  CD_GNG_MATERIAIS            NUMBER(8),
  CD_GNG_SERVICOS_TERC        NUMBER(8),
  CD_TABELA_VENDA             NUMBER(5),
  DS_CRITERIO_ALOC_MAT        VARCHAR2(3 BYTE),
  QT_DIA_CUSTO_COMPRA         NUMBER(3),
  DT_REFERENCIA               DATE,
  IE_MES_CALCULO              VARCHAR2(1 BYTE)  NOT NULL,
  IE_CUSTO_OC_CONSIG          VARCHAR2(1 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  QT_REP_DISTR                NUMBER(15)        NOT NULL,
  IE_PROC_AIH_CO              VARCHAR2(10 BYTE) NOT NULL,
  IE_CUSTO_CONTA_MC           VARCHAR2(1 BYTE)  NOT NULL,
  NR_SEQ_GNG_MAT              NUMBER(10),
  NR_SEQ_GNG_SERV_TERC        NUMBER(10),
  IE_SOMENTE_CUSTO_MES        VARCHAR2(1 BYTE)  NOT NULL,
  IE_CAPAC_CALC_OCIOSIDADE    VARCHAR2(1 BYTE)  NOT NULL,
  IE_QTD_CALCULO_CUSTO_MAT    VARCHAR2(1 BYTE)  NOT NULL,
  IE_CUSTO_CONTA_ESTORNADA    VARCHAR2(1 BYTE)  NOT NULL,
  IE_CUSTO_MULTI_ESTAB        VARCHAR2(1 BYTE),
  IE_CUSTO_MAT_UNID_CONVENIO  VARCHAR2(1 BYTE),
  IE_SOMENTE_TAB_REAL         VARCHAR2(1 BYTE),
  IE_CONSIDERAR_ENCER_SALDO   VARCHAR2(1 BYTE),
  IE_TX_CUSTO_PADRAO          VARCHAR2(15 BYTE),
  IE_CUSTO_NF_ATEND           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARCUST_GRUNAGA_FK_I ON TASY.PARAMETRO_CUSTO
(NR_SEQ_GNG_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCUST_GRUNAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARCUST_GRUNAGA_FK2_I ON TASY.PARAMETRO_CUSTO
(NR_SEQ_GNG_SERV_TERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARCUST_GRUNAGA_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PARCUST_PK ON TASY.PARAMETRO_CUSTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Parametro_Custo_Atual
BEFORE INSERT OR UPDATE ON Parametro_custo
FOR EACH ROW
DECLARE

BEGIN

:new.cd_comp_custo_ind_fixo		:= nvl(:new.cd_comp_custo_ind_fixo,30);
:new.cd_comp_desp_fixas		:= nvl(:new.cd_comp_desp_fixas, 40);
:new.cd_comp_perc_margem		:= nvl(:new.cd_comp_perc_margem,70);

END;
/


CREATE OR REPLACE TRIGGER TASY.PARAMETRO_CUSTO_tp  after update ON TASY.PARAMETRO_CUSTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_ESTABELECIMENTO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CUSTO_MAT_UNID_CONVENIO,1,4000),substr(:new.IE_CUSTO_MAT_UNID_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_MAT_UNID_CONVENIO',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_MULTI_ESTAB,1,4000),substr(:new.IE_CUSTO_MULTI_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_MULTI_ESTAB',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TABELA_VENDA,1,4000),substr(:new.CD_TABELA_VENDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TABELA_VENDA',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CRITERIO_ALOC_MAT,1,4000),substr(:new.DS_CRITERIO_ALOC_MAT,1,4000),:new.nm_usuario,nr_seq_w,'DS_CRITERIO_ALOC_MAT',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REFERENCIA',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MES_CALCULO,1,4000),substr(:new.IE_MES_CALCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MES_CALCULO',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_OC_CONSIG,1,4000),substr(:new.IE_CUSTO_OC_CONSIG,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_OC_CONSIG',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_REP_DISTR,1,4000),substr(:new.QT_REP_DISTR,1,4000),:new.nm_usuario,nr_seq_w,'QT_REP_DISTR',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROC_AIH_CO,1,4000),substr(:new.IE_PROC_AIH_CO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_AIH_CO',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TX_CUSTO_PADRAO,1,4000),substr(:new.IE_TX_CUSTO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TX_CUSTO_PADRAO',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERAR_ENCER_SALDO,1,4000),substr(:new.IE_CONSIDERAR_ENCER_SALDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_ENCER_SALDO',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_NF_ATEND,1,4000),substr(:new.IE_CUSTO_NF_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_NF_ATEND',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_CONTA_MC,1,4000),substr(:new.IE_CUSTO_CONTA_MC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_CONTA_MC',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GNG_SERV_TERC,1,4000),substr(:new.NR_SEQ_GNG_SERV_TERC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GNG_SERV_TERC',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GNG_MAT,1,4000),substr(:new.NR_SEQ_GNG_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GNG_MAT',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_CUSTO_MES,1,4000),substr(:new.IE_SOMENTE_CUSTO_MES,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_CUSTO_MES',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAPAC_CALC_OCIOSIDADE,1,4000),substr(:new.IE_CAPAC_CALC_OCIOSIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAPAC_CALC_OCIOSIDADE',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QTD_CALCULO_CUSTO_MAT,1,4000),substr(:new.IE_QTD_CALCULO_CUSTO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_QTD_CALCULO_CUSTO_MAT',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_CONTA_ESTORNADA,1,4000),substr(:new.IE_CUSTO_CONTA_ESTORNADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_CONTA_ESTORNADA',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_TAB_REAL,1,4000),substr(:new.IE_SOMENTE_TAB_REAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_TAB_REAL',ie_log_w,ds_w,'PARAMETRO_CUSTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PARAMETRO_CUSTO ADD (
  CONSTRAINT PARCUST_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETRO_CUSTO ADD (
  CONSTRAINT PARCUST_GRUNAGA_FK 
 FOREIGN KEY (NR_SEQ_GNG_MAT) 
 REFERENCES TASY.GRUPO_NATUREZA_GASTO (NR_SEQUENCIA),
  CONSTRAINT PARCUST_GRUNAGA_FK2 
 FOREIGN KEY (NR_SEQ_GNG_SERV_TERC) 
 REFERENCES TASY.GRUPO_NATUREZA_GASTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PARAMETRO_CUSTO TO NIVEL_1;

