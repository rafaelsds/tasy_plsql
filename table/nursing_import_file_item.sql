ALTER TABLE TASY.NURSING_IMPORT_FILE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NURSING_IMPORT_FILE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.NURSING_IMPORT_FILE_ITEM
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NR_SEQ_FILE   NUMBER(10),
  NR_SEQ_ITEM   NUMBER(10),
  NR_ROW        NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUIMFIIT_NUCAIMFI_FK_I ON TASY.NURSING_IMPORT_FILE_ITEM
(NR_SEQ_FILE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUIMFIIT_NUCAIMIT_FK_I ON TASY.NURSING_IMPORT_FILE_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NUIMFIIT_PK ON TASY.NURSING_IMPORT_FILE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NURSING_IMPORT_FILE_ITEM ADD (
  CONSTRAINT NUIMFIIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NURSING_IMPORT_FILE_ITEM ADD (
  CONSTRAINT NUIMFIIT_NUCAIMIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.NURSING_CARE_IMPORT_ITEMS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NUIMFIIT_NUCAIMFI_FK 
 FOREIGN KEY (NR_SEQ_FILE) 
 REFERENCES TASY.NURSING_CARE_IMPORT_FILES (NR_SEQUENCIA)
    ON DELETE CASCADE);

