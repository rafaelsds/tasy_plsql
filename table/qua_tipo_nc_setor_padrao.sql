ALTER TABLE TASY.QUA_TIPO_NC_SETOR_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_TIPO_NC_SETOR_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_TIPO_NC_SETOR_PADRAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_TIPO             NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_ESTABELECIMENTO_LIB  NUMBER(4),
  NR_SEQ_GRUPO_USUARIO    NUMBER(10),
  NM_USUARIO_LIB          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUATNCS_ESTABEL_FK_I ON TASY.QUA_TIPO_NC_SETOR_PADRAO
(CD_ESTABELECIMENTO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATNCS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUATNCS_GRUUSUA_FK_I ON TASY.QUA_TIPO_NC_SETOR_PADRAO
(NR_SEQ_GRUPO_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATNCS_GRUUSUA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUATNCS_PK ON TASY.QUA_TIPO_NC_SETOR_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATNCS_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUATNCS_QUATINC_FK_I ON TASY.QUA_TIPO_NC_SETOR_PADRAO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATNCS_QUATINC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUATNCS_SETATEN_FK_I ON TASY.QUA_TIPO_NC_SETOR_PADRAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUATNCS_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUA_TIPO_NC_SETOR_PADRAO ADD (
  CONSTRAINT QUATNCS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_TIPO_NC_SETOR_PADRAO ADD (
  CONSTRAINT QUATNCS_GRUUSUA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_USUARIO) 
 REFERENCES TASY.GRUPO_USUARIO (NR_SEQUENCIA),
  CONSTRAINT QUATNCS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO_LIB) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUATNCS_QUATINC_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.QUA_TIPO_NAO_CONFORM (NR_SEQUENCIA),
  CONSTRAINT QUATNCS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.QUA_TIPO_NC_SETOR_PADRAO TO NIVEL_1;

