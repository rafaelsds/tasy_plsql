ALTER TABLE TASY.REG_INTENCAO_USO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_INTENCAO_USO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_INTENCAO_USO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_DESCRICAO             VARCHAR2(4000 BYTE),
  DT_APROVACAO             DATE,
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DS_INTENCAO_USO          VARCHAR2(255 BYTE),
  NR_SEQ_ESTAGIO           NUMBER(10),
  DS_BRIEFING              VARCHAR2(4000 BYTE),
  DS_OBS_BRIEFING          VARCHAR2(255 BYTE),
  CD_VERSAO                VARCHAR2(255 BYTE),
  DS_PREFIXO               VARCHAR2(15 BYTE),
  NR_SEQ_INTENCAO_USO_PAI  NUMBER(10),
  DT_LIBERACAO             DATE,
  IE_PLATAFORMA_TASY       VARCHAR2(1 BYTE),
  IE_UTILIZA_CCB           VARCHAR2(1 BYTE),
  NM_PRODUTO               VARCHAR2(80 BYTE),
  DS_PRODUTO               VARCHAR2(4000 BYTE),
  DS_PRINC_OPERACIONAL     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGINU_PK ON TASY.REG_INTENCAO_USO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGINU_REGEST_FK_I ON TASY.REG_INTENCAO_USO
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGINU_REGINU_FK_I ON TASY.REG_INTENCAO_USO
(NR_SEQ_INTENCAO_USO_PAI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_intencao_uso_after
after insert or update ON TASY.REG_INTENCAO_USO for each row
declare

ie_tipo_alteracao_w		reg_intencao_uso_hist.ie_tipo_alteracao%type;
nr_seq_gerencia_w     reg_intencao_uso_lib.nr_seq_gerencia%type;
nr_seq_diretoria_w    reg_intencao_uso_lib.nr_seq_diretoria%type;
qt_reg_w				number(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto fim;
end if;

if (inserting) then
	ie_tipo_alteracao_w := 'I';

	begin
		select	ger.nr_sequencia,
			ger.nr_seq_diretoria
		into	nr_seq_gerencia_w,
			nr_seq_diretoria_w
		from	gerencia_wheb ger
		where	ger.nr_sequencia in (	select	max(gru.nr_seq_gerencia)
						from	gerencia_wheb_grupo_usu usu,
							gerencia_wheb_grupo gru
						where	usu.nr_seq_grupo = gru.nr_sequencia
						and	nvl(gru.ie_situacao, 'A') = 'A'
						and	usu.nm_usuario_grupo = :new.nm_usuario);
	exception
	when others then
		nr_seq_gerencia_w := null;
		nr_seq_diretoria_w := null;
	end;

  insert into reg_intencao_uso_lib (
    nr_sequencia,
    nr_seq_intencao_uso,
    nr_seq_gerencia,
    nr_seq_diretoria,
    nm_usuario,
    nm_usuario_nrec,
    dt_atualizacao,
    dt_atualizacao_nrec,
    cd_perfil)
  values (
    reg_intencao_uso_lib_seq.nextval,
    :new.nr_sequencia,
    nr_seq_gerencia_w,
    nr_seq_diretoria_w,
    :new.nm_usuario,
    :new.nm_usuario,
    sysdate,
    sysdate,
    null);

elsif	(updating) and
	(:new.ie_situacao = 'I') then
	ie_tipo_alteracao_w := 'E';
else
	ie_tipo_alteracao_w := 'A';
end if;

insert into reg_intencao_uso_hist (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_briefing,
	ds_obs_briefing,
	cd_versao,
	nr_seq_reg_version_rev,
	ds_descricao,
	dt_aprovacao,
	ie_situacao,
	ds_intencao_uso,
	nr_seq_intencao_uso,
	nr_seq_estagio,
	ie_tipo_alteracao)
values (
	reg_intencao_uso_hist_seq.nextval,
	:new.dt_atualizacao,
	:new.nm_usuario,
	:new.dt_atualizacao_nrec,
	:new.nm_usuario_nrec,
	:new.ds_briefing,
	:new.ds_obs_briefing,
	:new.cd_versao,
	null,
	:new.ds_descricao,
	:new.dt_aprovacao,
	:new.ie_situacao,
	:new.ds_intencao_uso,
	:new.nr_sequencia,
	:new.nr_seq_estagio,
	ie_tipo_alteracao_w);

<<fim>>
qt_reg_w := 0;
end;
/


ALTER TABLE TASY.REG_INTENCAO_USO ADD (
  CONSTRAINT REGINU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_INTENCAO_USO ADD (
  CONSTRAINT REGINU_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO_PAI) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA),
  CONSTRAINT REGINU_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_INTENCAO_USO TO NIVEL_1;

