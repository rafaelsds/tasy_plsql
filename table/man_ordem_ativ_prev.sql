ALTER TABLE TASY.MAN_ORDEM_ATIV_PREV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_ATIV_PREV CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_ATIV_PREV
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_ORDEM_SERV       NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_ATIVIDADE            VARCHAR2(2000 BYTE),
  DT_PREVISTA             DATE,
  DT_REAL                 DATE,
  NR_SEQ_ESTAGIO          NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  QT_MIN_PREV             NUMBER(15),
  NM_USUARIO_REAL         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_FUNCAO           NUMBER(10),
  NM_USUARIO_PREV         VARCHAR2(15 BYTE),
  QT_HORA_PREV            NUMBER(5),
  VL_COBRANCA_VENDA       NUMBER(15,2),
  VL_COBRANCA             NUMBER(15,2),
  NR_SEQ_GRUPO_TRABALHO   NUMBER(10),
  NR_SEQ_APRES            NUMBER(5),
  NR_SEQ_ATIV_EXEC        NUMBER(10),
  QT_ATIVIDADE            NUMBER(5),
  QT_MIN_PADRAO_ATIV      NUMBER(15),
  PR_ATIVIDADE            NUMBER(10),
  IE_PRIORIDADE_DESEN     NUMBER(3),
  IE_PRIORIDADE_SUP       NUMBER(3),
  IE_FORA_PLANEJ_DIARIO   VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_REPROG    NUMBER(10),
  NR_SEQ_MOTIVO_ASSERT    NUMBER(10),
  NR_SEQ_MOTIVO_ALT_PREV  NUMBER(10),
  NR_SEQ_LP               NUMBER(10),
  NR_SEQ_TIPO_ATIV        NUMBER(10),
  DT_PREV_ENTREGA         DATE,
  NR_SEQ_ESPECIF_TO_DO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANORAP_DESATEX_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_ATIV_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORAP_DESMRAT_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_MOTIVO_REPROG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_DESMRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_I1 ON TASY.MAN_ORDEM_ATIV_PREV
(NM_USUARIO_PREV, DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORAP_I2 ON TASY.MAN_ORDEM_ATIV_PREV
(DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_I2
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_LINPROG_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_LP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_LINPROG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MAESTPR_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_ESTAGIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MAESTPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MANGRTR_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MANMOAPR_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_MOTIVO_ALT_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MANMOAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MANMOTA_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_MOTIVO_ASSERT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MANMOTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MANORSE_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORAP_MANOSEDTD_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_ESPECIF_TO_DO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORAP_MANTIFU_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MANTIFU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORAP_MANTPATV_FK_I ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQ_TIPO_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORAP_MANTPATV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANORAP_PK ON TASY.MAN_ORDEM_ATIV_PREV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_ordem_ativ_prev_especif
before insert ON TASY.MAN_ORDEM_ATIV_PREV for each row
declare
pragma autonomous_transaction;

ie_classificacao_w		varchar2(1);
ie_origem_os_w			varchar2(15);
ie_possui_ativ_prog_w		varchar2(1);
ie_possui_especif_lib_w		varchar2(1);
ie_consiste_w			varchar2(1);
cd_cargo_w			number(10);
nr_seq_proj_cron_etapa_w	number(10);

begin

	select	max(ie_classificacao),
		max(ie_origem_os),
		max(nr_seq_proj_cron_etapa)
	into	ie_classificacao_w,
		ie_origem_os_w,
		nr_seq_proj_cron_etapa_w
	from	man_ordem_servico
	where	nr_sequencia = :new.nr_seq_ordem_serv;

	if	(ie_origem_os_w = '1') and
		(ie_classificacao_w = 'S') and
		(nr_seq_proj_cron_etapa_w is null) then

		select	max(a.cd_cargo)
		into	cd_cargo_w
		from	pessoa_fisica a,
			usuario b
		where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and	b.nm_usuario = :new.nm_usuario_prev;

		if	(cd_cargo_w in (251, 425, 426, 427, 427, 73, 66, 69, 80)) then

			obter_param_padrao_usuario(297, 94, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_consiste_w);

			select	decode(count(*), 0, 'N', 'S')
			into	ie_possui_ativ_prog_w
			from	man_ordem_ativ_prev a,
				usuario b,
				pessoa_fisica c
			where	a.nr_seq_ordem_serv = :new.nr_seq_ordem_serv
			and	a.nm_usuario_prev = b.nm_usuario
			and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
			and	c.cd_cargo in (251, 425, 426, 427, 427, 73, 66, 69, 80);

			if	(ie_possui_ativ_prog_w = 'N') and
				(substr(man_obter_se_os_vol_clien_test(:new.nr_seq_ordem_serv), 1, 1) = 'N') and
				(ie_consiste_w = 'S') then

				select	decode(count(*), 0, 'N', 'S')
				into	ie_possui_especif_lib_w
				from	man_os_especificacao
				where	nr_seq_ordem_servico = :new.nr_seq_ordem_serv
				and	dt_liberacao is not null;

				if	(ie_possui_especif_lib_w = 'N') then
					wheb_mensagem_pck.exibir_mensagem_abort(358670);
				end if;
			end if;
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.man_ordem_ativ_prev_after
after insert ON TASY.MAN_ORDEM_ATIV_PREV for each row
declare

nr_seq_tipo_exec_w	number;
ie_existe_registro_w 	varchar(1);
qt_fila_w			number(10);


begin

	select 	decode(count(*),0,'N','S')
	into 	ie_existe_registro_w
	from 	man_ordem_servico_exec a
	where 	:new.nr_seq_ordem_serv = a.nr_seq_ordem
	and 	:new.nm_usuario_prev = a.nm_usuario_exec
	and 	a.dt_fim_execucao is null;

	select 	max(a.nr_sequencia)
	into	nr_seq_tipo_exec_w
	from	man_tipo_executor a,
		man_ordem_servico_exec b
	where 	a.nr_sequencia = b.nr_seq_tipo_exec
	and	b.nr_sequencia = :new.nr_sequencia;

	select	count(*)
	into	qt_fila_w
	from	cadastro_fila
	where	nm_usuario_fila = :new.nm_usuario_prev;

	if (:new.nm_usuario_prev is not null) and
		(qt_fila_w > 0) then

		if	(ie_existe_registro_w = 'N') then
			begin
				insert into man_ordem_servico_exec(
							nr_sequencia,
							nr_seq_ordem,
							dt_atualizacao,
							nm_usuario,
							nm_usuario_exec,
							qt_min_prev,
							dt_ult_visao,
							nr_seq_funcao,
							nr_seq_tipo_exec,
							dt_atualizacao_nrec,
							nm_usuario_nrec)
				values(		man_ordem_servico_exec_seq.nextval,
							:new.nr_seq_ordem_serv,
							sysdate,
							:new.nm_usuario,
							:new.nm_usuario_prev,
							:new.qt_min_prev,
							null,
							:new.nr_seq_funcao,
							nr_seq_tipo_exec_w,
							:new.dt_atualizacao_nrec,
							:new.nm_usuario_nrec);
			end;
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_ATIV_PREV_ATUAL
after update or insert ON TASY.MAN_ORDEM_ATIV_PREV for each row
declare
nm_user_w			varchar2(50);

begin

select	username
into	nm_user_w
from	user_users;

end;
/


ALTER TABLE TASY.MAN_ORDEM_ATIV_PREV ADD (
  CONSTRAINT MANORAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_ATIV_PREV ADD (
  CONSTRAINT MANORAP_MANOSEDTD_FK 
 FOREIGN KEY (NR_SEQ_ESPECIF_TO_DO) 
 REFERENCES TASY.MAN_OS_ESPECIF_DET_TO_DO (NR_SEQUENCIA),
  CONSTRAINT MANORAP_LINPROG_FK 
 FOREIGN KEY (NR_SEQ_LP) 
 REFERENCES TASY.LINGUAGEM_PROGRAMACAO (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANTPATV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATIV) 
 REFERENCES TASY.MAN_TIPO_ATIV (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANORAP_DESATEX_FK 
 FOREIGN KEY (NR_SEQ_ATIV_EXEC) 
 REFERENCES TASY.DES_ATIV_EXEC (NR_SEQUENCIA),
  CONSTRAINT MANORAP_DESMRAT_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REPROG) 
 REFERENCES TASY.DESENV_MOTIVO_REPROG_ATIV (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANMOTA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ASSERT) 
 REFERENCES TASY.MAN_MOTIVO_ASSERTIVIDADE (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANMOAPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ALT_PREV) 
 REFERENCES TASY.MAN_MOTIVO_ALT_PREVISAO (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANORAP_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT MANORAP_MANTIFU_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.MAN_TIPO_FUNCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_ATIV_PREV TO NIVEL_1;

