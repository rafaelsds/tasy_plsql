ALTER TABLE TASY.INTDISP_PADRAO_LOCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTDISP_PADRAO_LOCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTDISP_PADRAO_LOCAL
(
  CD_ACAO               VARCHAR2(20 BYTE),
  DT_LEITURA            DATE,
  NM_PACIENTE           VARCHAR2(60 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE),
  NR_PRESCRICAO         NUMBER(14),
  CD_MEDICO             VARCHAR2(10 BYTE),
  CD_MATERIAL           NUMBER(6),
  DS_MATERIAL           VARCHAR2(255 BYTE),
  CD_CLASSE_MATERIAL    NUMBER(5),
  NM_USUARIO            VARCHAR2(15 BYTE),
  DS_USUARIO            VARCHAR2(60 BYTE),
  CD_MATERIAL_GENERICO  NUMBER(6),
  CD_ESTABELECIMENTO    NUMBER(4),
  QT_ESTOQUE            NUMBER(15,4),
  QT_MAXIMA             NUMBER(15,4),
  QT_MINIMA             NUMBER(15,4),
  DT_MOVIMENTO          DATE,
  NR_SEQ_LOTE           NUMBER(10),
  DS_FUNCAO             VARCHAR2(80 BYTE),
  CD_OPERACAO_ESTOQUE   NUMBER(3),
  CD_LOCAL_ESTOQUE      NUMBER(4),
  DT_VALIDADE           DATE,
  DS_LOCAL_ESTOQUE      VARCHAR2(40 BYTE),
  DS_ERRO               VARCHAR2(255 BYTE),
  DS_XML                CLOB,
  CD_BARRAS             VARCHAR2(40 BYTE),
  QT_MOVIMENTO          NUMBER(15,4),
  DT_ATUALIZACAO        DATE,
  NR_SEQUENCIA          NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_XML) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INTDISPE_PK ON TASY.INTDISP_PADRAO_LOCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.intdisp_padrao_local_insert
before insert ON TASY.INTDISP_PADRAO_LOCAL for each row
declare

nr_sequencia_w		intdisp_padrao_local.nr_sequencia%type;

begin

if	(:new.nr_sequencia is null) then

	select	intdisp_padrao_local_seq.nextval
	into	nr_sequencia_w
	from	dual;

	:new.nr_sequencia := nr_sequencia_w;
end if;

end;
/


ALTER TABLE TASY.INTDISP_PADRAO_LOCAL ADD (
  CONSTRAINT INTDISPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.INTDISP_PADRAO_LOCAL TO NIVEL_1;

