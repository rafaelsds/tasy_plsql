ALTER TABLE TASY.RELATORIO_DOCUMENTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_DOCUMENTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_DOCUMENTACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_RELATORIO       NUMBER(10)             NOT NULL,
  CD_PF_SOLIC            VARCHAR2(10 BYTE),
  CD_PF_CRIADOR          VARCHAR2(10 BYTE),
  DS_OBJETIVO            VARCHAR2(1000 BYTE),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_CLASSIFICACAO       DATE,
  IE_TIPO_CLASSIFICACAO  VARCHAR2(2 BYTE),
  NR_SEQ_ORDEM_SERV      NUMBER(10),
  CD_DOCUMENTACAO        VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELADOC_MANORSE_FK_I ON TASY.RELATORIO_DOCUMENTACAO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELADOC_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELADOC_PESFISI_FK_I ON TASY.RELATORIO_DOCUMENTACAO
(CD_PF_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELADOC_PESFISI_FK2_I ON TASY.RELATORIO_DOCUMENTACAO
(CD_PF_CRIADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELADOC_PK ON TASY.RELATORIO_DOCUMENTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELADOC_RELATOR_FK_I ON TASY.RELATORIO_DOCUMENTACAO
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REL_RELATORIO_DOCUMENT_ATUAL
BEFORE INSERT OR DELETE OR UPDATE ON TASY.RELATORIO_DOCUMENTACAO FOR EACH ROW
BEGIN

exec_sql_dinamico('',
	' UPDATE RELATORIO '||
	' SET 	IE_GERAR_RELATORIO = ''S'' ,'||
	'		DT_LAST_MODIFICATION     = SYSDATE,'||
	'		DT_ATUALIZACAO     = SYSDATE,'||
	'		NM_USUARIO         = '|| CHR(39) || NVL(:NEW.NM_USUARIO,:OLD.NM_USUARIO) || CHR(39) ||
	' WHERE 	NR_SEQUENCIA 	     = '|| NVL(:NEW.NR_SEQ_RELATORIO,:OLD.NR_SEQ_RELATORIO));
END;
/


ALTER TABLE TASY.RELATORIO_DOCUMENTACAO ADD (
  CONSTRAINT RELADOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO_DOCUMENTACAO ADD (
  CONSTRAINT RELADOC_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT RELADOC_PESFISI_FK2 
 FOREIGN KEY (CD_PF_CRIADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RELADOC_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RELADOC_PESFISI_FK 
 FOREIGN KEY (CD_PF_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.RELATORIO_DOCUMENTACAO TO NIVEL_1;

