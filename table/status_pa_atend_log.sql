ALTER TABLE TASY.STATUS_PA_ATEND_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.STATUS_PA_ATEND_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.STATUS_PA_ATEND_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_REGISTRO          DATE                     NOT NULL,
  NR_SEQ_STATUS        NUMBER(10)               NOT NULL,
  NR_ATENDIMENTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.STPAATL_ATEPACI_FK_I ON TASY.STATUS_PA_ATEND_LOG
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.STPAATL_PASTAPA_FK_I ON TASY.STATUS_PA_ATEND_LOG
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.STPAATL_PK ON TASY.STATUS_PA_ATEND_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.STATUS_PA_ATEND_LOG ADD (
  CONSTRAINT STPAATL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.STATUS_PA_ATEND_LOG ADD (
  CONSTRAINT STPAATL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT STPAATL_PASTAPA_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.PA_STATUS_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.STATUS_PA_ATEND_LOG TO NIVEL_1;

