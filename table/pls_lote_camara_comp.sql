ALTER TABLE TASY.PLS_LOTE_CAMARA_COMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_CAMARA_COMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_CAMARA_COMP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_LOTE                DATE                   NOT NULL,
  NR_SEQ_CAMARA          NUMBER(10)             NOT NULL,
  NR_SEQ_PERIODO         NUMBER(10)             NOT NULL,
  DT_BAIXA               DATE,
  DT_PERIODO_INICIAL     DATE,
  DT_PERIODO_FINAL       DATE,
  DT_GERACAO             DATE,
  TX_ADMINISTRATIVA      NUMBER(7,4)            NOT NULL,
  NR_TIT_PAGAR_TAXA      NUMBER(10),
  IE_TIPO_DATA_CP        VARCHAR2(3 BYTE)       NOT NULL,
  IE_TIPO_DATA_CR        VARCHAR2(3 BYTE)       NOT NULL,
  DT_PERIODO_INICIAL_CP  DATE,
  DT_PERIODO_FINAL_CP    DATE,
  DT_GERACAO_CP          DATE,
  VL_DESC_BENEFIC_CUSTO  NUMBER(15,2),
  DT_SALDO_CREDOR        DATE,
  DT_SALDO_DEVEDOR       DATE,
  VL_TAXA_FIXA           NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOCC_ESTABEL_FK_I ON TASY.PLS_LOTE_CAMARA_COMP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOCC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOCC_PK ON TASY.PLS_LOTE_CAMARA_COMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOCC_PLSCACO_FK_I ON TASY.PLS_LOTE_CAMARA_COMP
(NR_SEQ_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOCC_PLSCACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOCC_PLSCCPE_FK_I ON TASY.PLS_LOTE_CAMARA_COMP
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOCC_PLSCCPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOCC_TITPAGA_FK_I ON TASY.PLS_LOTE_CAMARA_COMP
(NR_TIT_PAGAR_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOTE_CAMARA_COMP ADD (
  CONSTRAINT PLSLOCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_CAMARA_COMP ADD (
  CONSTRAINT PLSLOCC_PLSCACO_FK 
 FOREIGN KEY (NR_SEQ_CAMARA) 
 REFERENCES TASY.PLS_CAMARA_COMPENSACAO (NR_SEQUENCIA),
  CONSTRAINT PLSLOCC_PLSCCPE_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.PLS_CAMARA_CALEND_PERIODO (NR_SEQUENCIA),
  CONSTRAINT PLSLOCC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOCC_TITPAGA_FK 
 FOREIGN KEY (NR_TIT_PAGAR_TAXA) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.PLS_LOTE_CAMARA_COMP TO NIVEL_1;

