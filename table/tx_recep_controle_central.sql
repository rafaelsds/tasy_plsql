ALTER TABLE TASY.TX_RECEP_CONTROLE_CENTRAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_RECEP_CONTROLE_CENTRAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_RECEP_CONTROLE_CENTRAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_COLETA            DATE,
  NR_SEQ_RECEPTOR      NUMBER(10),
  DT_VENCIMENTO        DATE,
  DT_ENTREGA_CENTRAL   DATE,
  NM_PESSOA_RECEB      VARCHAR2(80 BYTE),
  NM_USUARIO_COLETA    VARCHAR2(15 BYTE),
  DT_RECEBIMENTO       DATE,
  NM_USUARIO_RECEB     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXRECOC_PK ON TASY.TX_RECEP_CONTROLE_CENTRAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXRECOC_TXRECE_FK_I ON TASY.TX_RECEP_CONTROLE_CENTRAL
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TX_RECEP_CONTROLE_CENTRAL ADD (
  CONSTRAINT TXRECOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_RECEP_CONTROLE_CENTRAL ADD (
  CONSTRAINT TXRECOC_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_RECEP_CONTROLE_CENTRAL TO NIVEL_1;

