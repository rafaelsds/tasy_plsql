ALTER TABLE TASY.REG_LIC_VENCEDOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_LIC_VENCEDOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_LIC_VENCEDOR
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  NR_SEQ_LICITACAO  NUMBER(10)                  NOT NULL,
  NR_SEQ_LIC_ITEM   NUMBER(10)                  NOT NULL,
  CD_MATERIAL       NUMBER(6),
  NR_SEQ_FORNEC     NUMBER(10)                  NOT NULL,
  VL_ITEM           NUMBER(15,4)                NOT NULL,
  NR_SEQ_LOTE       NUMBER(5),
  NR_LOTE_COMPRA    VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGLIVE_LICFORN_FK_I ON TASY.REG_LIC_VENCEDOR
(NR_SEQ_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIVE_LICFORN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLIVE_MATERIA_FK_I ON TASY.REG_LIC_VENCEDOR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIVE_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGLIVE_PK ON TASY.REG_LIC_VENCEDOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIVE_PK
  MONITORING USAGE;


CREATE INDEX TASY.REGLIVE_REGLIIT_FK_I ON TASY.REG_LIC_VENCEDOR
(NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIVE_REGLIIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLIVE_RELICIT_FK_I ON TASY.REG_LIC_VENCEDOR
(NR_SEQ_LICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLIVE_RELICIT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REG_LIC_VENCEDOR ADD (
  CONSTRAINT REGLIVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_LIC_VENCEDOR ADD (
  CONSTRAINT REGLIVE_REGLIIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM) 
 REFERENCES TASY.REG_LIC_ITEM (NR_SEQ_LICITACAO,NR_SEQ_LIC_ITEM),
  CONSTRAINT REGLIVE_LICFORN_FK 
 FOREIGN KEY (NR_SEQ_FORNEC) 
 REFERENCES TASY.REG_LIC_FORNEC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGLIVE_RELICIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO) 
 REFERENCES TASY.REG_LICITACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGLIVE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.REG_LIC_VENCEDOR TO NIVEL_1;

