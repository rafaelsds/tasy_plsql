ALTER TABLE TASY.BORDERO_TIT_PAGAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BORDERO_TIT_PAGAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.BORDERO_TIT_PAGAR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_BORDERO           NUMBER(10)               NOT NULL,
  NR_TITULO            NUMBER(10)               NOT NULL,
  VL_BORDERO           NUMBER(15,2)             NOT NULL,
  VL_JUROS_BORDERO     NUMBER(15,2)             NOT NULL,
  VL_MULTA_BORDERO     NUMBER(15,2)             NOT NULL,
  VL_DESCONTO_BORDERO  NUMBER(15,2)             NOT NULL,
  VL_OUT_DESP_BORDERO  NUMBER(15,2)             NOT NULL,
  CD_CENTRO_CUSTO      NUMBER(8),
  VL_OUTRAS_DEDUCOES   NUMBER(15,2),
  IE_SELECIONADO       VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  VL_BORDERO_ESTRANG   NUMBER(15,2),
  CD_MOEDA             NUMBER(5),
  VL_COTACAO           NUMBER(21,10),
  VL_COMPLEMENTO       NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BOTITRE_BORPAGA_FK_I ON TASY.BORDERO_TIT_PAGAR
(NR_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOTITRE_CENCUST_FK_I ON TASY.BORDERO_TIT_PAGAR
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BOTITRE_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BOTITRE_MOEDA_FK_I ON TASY.BORDERO_TIT_PAGAR
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOTITRE_PK ON TASY.BORDERO_TIT_PAGAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOTITRE_TITPAGA_FK_I ON TASY.BORDERO_TIT_PAGAR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOTITRE_UK ON TASY.BORDERO_TIT_PAGAR
(NR_BORDERO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.bordero_tit_pagar_after
after insert or update ON TASY.BORDERO_TIT_PAGAR for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.bordero_tit_pagar_delete
before delete ON TASY.BORDERO_TIT_PAGAR for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TP',:old.dt_atualizacao,'E',:old.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.BORDERO_TIT_PAGAR ADD (
  CONSTRAINT BOTITRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT BOTITRE_UK
 UNIQUE (NR_BORDERO, NR_TITULO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BORDERO_TIT_PAGAR ADD (
  CONSTRAINT BOTITRE_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT BOTITRE_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT BOTITRE_BORPAGA_FK 
 FOREIGN KEY (NR_BORDERO) 
 REFERENCES TASY.BORDERO_PAGAMENTO (NR_BORDERO)
    ON DELETE CASCADE,
  CONSTRAINT BOTITRE_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.BORDERO_TIT_PAGAR TO NIVEL_1;

