ALTER TABLE TASY.ESCALA_BARTHEL_MODIFICADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_BARTHEL_MODIFICADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_BARTHEL_MODIFICADA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_COTIDIANO           NUMBER(3),
  IE_ORIENTACAO          NUMBER(3),
  IE_VER                 NUMBER(3),
  IE_FAZER_ENTENDER      NUMBER(3),
  IE_ENTENDER            NUMBER(3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  IE_SOCIAL              NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESBAMOD_ATEPACI_FK_I ON TASY.ESCALA_BARTHEL_MODIFICADA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESBAMOD_PESFISI_FK2_I ON TASY.ESCALA_BARTHEL_MODIFICADA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESBAMOD_PK ON TASY.ESCALA_BARTHEL_MODIFICADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_BARTHEL_MOD_ATUAL
before insert ON TASY.ESCALA_BARTHEL_MODIFICADA FOR EACH ROW
declare

begin
:new.QT_PONTUACAO	:=	nvl(:new.IE_COTIDIANO, 0)		+
						nvl(:new.IE_ENTENDER, 0)		+
						nvl(:new.IE_FAZER_ENTENDER, 0)	+
						nvl(:new.IE_ORIENTACAO, 0)		+
						nvl(:new.IE_SOCIAL, 0)			+
						nvl(:new.IE_VER, 0);
end ESCALA_BARTHEL_MOD_ATUAL;
/


ALTER TABLE TASY.ESCALA_BARTHEL_MODIFICADA ADD (
  CONSTRAINT ESBAMOD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_BARTHEL_MODIFICADA ADD (
  CONSTRAINT ESBAMOD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESBAMOD_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_BARTHEL_MODIFICADA TO NIVEL_1;

