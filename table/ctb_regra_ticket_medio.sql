ALTER TABLE TASY.CTB_REGRA_TICKET_MEDIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_TICKET_MEDIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_TICKET_MEDIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_CENARIO       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_REGRA         NUMBER(15)               NOT NULL,
  NR_SEQ_METRICA       NUMBER(10),
  NR_SEQ_MES_REF       NUMBER(10),
  CD_CENTRO_CUSTO      NUMBER(8),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  DT_MES_INIC          DATE,
  DT_MES_FIM           DATE,
  IE_REGRA             VARCHAR2(15 BYTE)        NOT NULL,
  PR_APLICAR           NUMBER(15,4),
  IE_SOBREPOR          VARCHAR2(1 BYTE)         NOT NULL,
  VL_FIXO              NUMBER(15,2),
  IE_PADRAO_AJUSTE     VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_CONTA   NUMBER(10),
  NR_SEQ_GRUPO_CENTRO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBRETM_CENCUST_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(CD_CENTRO_CUSTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CONCONT_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(CD_CONTA_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CTBCGCE_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQ_GRUPO_CENTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CTBCGCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CTBCGCO_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQ_GRUPO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CTBCGCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CTBMERE_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CTBMETR_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQ_METRICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CTBMETR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_CTBORCE_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQ_CENARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_CTBORCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRETM_ESTABEL_FK_I ON TASY.CTB_REGRA_TICKET_MEDIO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBRETM_PK ON TASY.CTB_REGRA_TICKET_MEDIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRETM_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ctb_regra_ticket_medio_insert
before insert or update ON TASY.CTB_REGRA_TICKET_MEDIO for each row
declare

ds_regra_w		varchar2(40);
ie_conta_vigente_w	Varchar2(1);
ie_tipo_w			varchar2(1);
ie_situacao_w		varchar2(1);
ie_resultado_w		varchar2(1);

BEGIN
ds_regra_w	:= substr(obter_valor_dominio(1719,:new.ie_regra),1,40);

if	(:new.nr_seq_grupo_conta is not null) then
	:new.cd_conta_contabil	:= null;
end if;

if	(:new.ie_regra = 'VF') then
	:new.pr_aplicar		:= null;
	:new.nr_seq_mes_ref	:= null;
elsif	(:new.ie_regra = 'PO') then
	:new.vl_fixo		:= null;
elsif	(:new.ie_regra = 'PV') then
	:new.vl_fixo		:= null;
	:new.nr_seq_mes_ref	:= null;
	if	(:new.dt_mes_inic is null) or
		(:new.dt_mes_fim is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266575);
		/*Para este tipo de regra � obrigat�ria a informa��o dos campos M�s inicial e final!#@#@');*/
	end if;
end if;

if	(:new.cd_conta_contabil is null) and
	(:new.nr_seq_grupo_conta is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(266608,'NR_SEQ_REGRA=' || :new.nr_seq_regra);
end if;

ie_conta_vigente_w	:= substr(obter_se_conta_vigente(:new.cd_conta_contabil, sysdate),1,1);

if	(ie_conta_vigente_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(237680,'CD_CONTA_CONTABIL_W=' || :new.cd_conta_contabil);
	/*Esta conta cont�bil esta fora da data de vig�ncia.');*/
end if;

if	(:new.ie_regra = 'VF') and
	(:new.cd_centro_custo is null) and
	(:new.nr_seq_grupo_centro is null) then
	/*Deve ser informado um grupo ou um centro de custo!');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266610);
end if;


/* Consist�ncia centro de custo */
if	(:new.cd_centro_custo is not null) then
	select	nvl(max(ie_tipo),'X'),
		nvl(max(ie_situacao),'I')
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo = :new.cd_centro_custo;

	if	(ie_tipo_w <> 'A') then
		/*S� pode ser informado centro de custo do tipo anal�tico!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266580);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266581);
		/*O centro de custo informado est� inativo! ' || 'C�digo: ' || :new.cd_centro_custo);*/
	end if;

end if;

/* Conta cont�bil */
if	(:new.cd_conta_contabil is not null) then
	select	nvl(max(a.ie_tipo),'X'),
		nvl(max(a.ie_situacao),'I'),
		max(b.ie_tipo)
	into	ie_tipo_w,
		ie_situacao_w,
		ie_resultado_w
	from	ctb_grupo_conta b,
		conta_contabil a
	where	a.cd_grupo		= b.cd_grupo
	and	a.cd_conta_contabil	= :new.cd_conta_contabil;

	if	(ie_tipo_w <> 'A') then
		wheb_mensagem_pck.exibir_mensagem_abort(266584);
		/*S� pode ser informado Conta cont�bil anal�tica!');*/
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266611,'CD_CONTA_CONTABIL=' || :new.cd_conta_contabil);
		/*'A conta cont�bil informada est� inativa!');*/
	end if;
	if	(ie_resultado_w not in ('R','C','D')) then
		/*A conta cont�bil informada n�o � do tipo resultado!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266614);
	end if;

end if;

END ctb_regra_ticket_medio_insert;
/


ALTER TABLE TASY.CTB_REGRA_TICKET_MEDIO ADD (
  CONSTRAINT CTBRETM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGRA_TICKET_MEDIO ADD (
  CONSTRAINT CTBRETM_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBRETM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBRETM_CTBORCE_FK 
 FOREIGN KEY (NR_SEQ_CENARIO) 
 REFERENCES TASY.CTB_ORC_CENARIO (NR_SEQUENCIA),
  CONSTRAINT CTBRETM_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBRETM_CTBMETR_FK 
 FOREIGN KEY (NR_SEQ_METRICA) 
 REFERENCES TASY.CTB_METRICA (NR_SEQUENCIA),
  CONSTRAINT CTBRETM_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBRETM_CTBCGCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTA) 
 REFERENCES TASY.CTB_CEN_GRUPO_CONTA (NR_SEQUENCIA),
  CONSTRAINT CTBRETM_CTBCGCE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CENTRO) 
 REFERENCES TASY.CTB_CEN_GRUPO_CENTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_REGRA_TICKET_MEDIO TO NIVEL_1;

