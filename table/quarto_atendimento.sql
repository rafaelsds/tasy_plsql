ALTER TABLE TASY.QUARTO_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUARTO_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUARTO_ATENDIMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ROOM_NUMBER   NUMBER(5,5),
  ROOM_DESCRIPTION     VARCHAR2(10 BYTE),
  NR_TOTAL_BEDS        NUMBER(5,5),
  NR_COLSPAN_BEDS      NUMBER(5,5),
  NR_ROWSPAN_BEDS      NUMBER(5,5),
  NR_SEQ_SETOR_ATEND   NUMBER(5,5),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QUARATD_PK ON TASY.QUARTO_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUARATD_SETATEN_FK_I ON TASY.QUARTO_ATENDIMENTO
(NR_SEQ_SETOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUARATD_UNIATEN_FK_I ON TASY.QUARTO_ATENDIMENTO
(NR_SEQ_ROOM_NUMBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUARTO_ATENDIMENTO ADD (
  CONSTRAINT QUARATD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.QUARTO_ATENDIMENTO ADD (
  CONSTRAINT QUARATD_SETATEN_FK 
 FOREIGN KEY (NR_SEQ_SETOR_ATEND) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

