ALTER TABLE TASY.BOMBA_INFUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BOMBA_INFUSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.BOMBA_INFUSAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_EQUIPAMENTO      NUMBER(10)            NOT NULL,
  IE_STATUS               VARCHAR2(2 BYTE),
  IE_ATIVO                VARCHAR2(1 BYTE),
  DT_BOMBA_INFUSAO        DATE,
  NM_USUARIO              VARCHAR2(15 BYTE),
  NR_PRESCRICAO           NUMBER(14),
  NR_ATENDIMENTO          NUMBER(14),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CANAL_BOMBA      NUMBER(10),
  NR_SEQ_MAT_CPOE         NUMBER(10),
  DT_FIM_INFUSAO          DATE,
  NR_SEQ_SOLUCAO          NUMBER(10),
  DT_REGISTRO_VOLUME      DATE,
  NR_SEQ_BOMBA_INTERFACE  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BOINF_BOMINTER_FK_I ON TASY.BOMBA_INFUSAO
(NR_SEQ_BOMBA_INTERFACE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOINF_CPOEMAT_FK_I ON TASY.BOMBA_INFUSAO
(NR_SEQ_MAT_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOINF_MANEQBO_FK_I ON TASY.BOMBA_INFUSAO
(NR_SEQ_CANAL_BOMBA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOINF_MANEQUI_FK_I ON TASY.BOMBA_INFUSAO
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOINF_PK ON TASY.BOMBA_INFUSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BOMBA_INFUSAO ADD (
  CONSTRAINT BOINF_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.BOMBA_INFUSAO ADD (
  CONSTRAINT BOINF_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT BOINF_MANEQBO_FK 
 FOREIGN KEY (NR_SEQ_CANAL_BOMBA) 
 REFERENCES TASY.MAN_EQUIPAMENTO_BOMBA (NR_SEQUENCIA),
  CONSTRAINT BOINF_CPOEMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_CPOE) 
 REFERENCES TASY.CPOE_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT BOINF_BOMINTER_FK 
 FOREIGN KEY (NR_SEQ_BOMBA_INTERFACE) 
 REFERENCES TASY.BOMBA_INFUSAO_INTERFACE (NR_SEQUENCIA));

GRANT SELECT ON TASY.BOMBA_INFUSAO TO NIVEL_1;

