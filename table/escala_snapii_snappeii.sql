ALTER TABLE TASY.ESCALA_SNAPII_SNAPPEII
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SNAPII_SNAPPEII CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SNAPII_SNAPPEII
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  QT_PA_SISTOLICA        NUMBER(3),
  QT_PA_DIASTOLICA       NUMBER(3),
  QT_PAM                 NUMBER(3),
  QT_PTO_PAM             NUMBER(2)              NOT NULL,
  QT_TEMP                NUMBER(4,2),
  QT_PTO_TEMP            NUMBER(2)              NOT NULL,
  QT_REL_PO2_FIO2        NUMBER(5,2),
  QT_PTO_REL_PO2_FIO2    NUMBER(2)              NOT NULL,
  QT_PH_SERICO           NUMBER(5,2),
  QT_PTO_PH_SERICO       NUMBER(2)              NOT NULL,
  QT_DIURESE             NUMBER(15,2),
  QT_PTO_DIURESE         NUMBER(2)              NOT NULL,
  IE_CONVULSAO_MULTIPLA  VARCHAR2(1 BYTE)       NOT NULL,
  QT_PTO_CONVUL          NUMBER(2)              NOT NULL,
  QT_SNAP_II             NUMBER(3)              NOT NULL,
  QT_PESO                NUMBER(15,2),
  QT_PTO_PESO            NUMBER(2)              NOT NULL,
  IE_PEQ_IDADE_GEST      VARCHAR2(1 BYTE)       NOT NULL,
  QT_PTO_IDADE_GEST      NUMBER(2)              NOT NULL,
  QT_APGAR_QUINTO_MIN    NUMBER(3,1),
  QT_PTO_APGAR           NUMBER(2)              NOT NULL,
  QT_SNAPPE_II           NUMBER(3)              NOT NULL,
  QT_PESO_ATUAL          NUMBER(15,2),
  QT_IDADE_GESTACIONAL   NUMBER(3),
  PR_MORTALIDADE         NUMBER(5,2)            NOT NULL,
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSNSN_ATEPACI_FK_I ON TASY.ESCALA_SNAPII_SNAPPEII
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSNSN_I1 ON TASY.ESCALA_SNAPII_SNAPPEII
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSNSN_PERFIL_FK_I ON TASY.ESCALA_SNAPII_SNAPPEII
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSNSN_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSNSN_PESFISI_FK_I ON TASY.ESCALA_SNAPII_SNAPPEII
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSNSN_PK ON TASY.ESCALA_SNAPII_SNAPPEII
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSNSN_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_snapii_snappeii_befins
before insert or update ON TASY.ESCALA_SNAPII_SNAPPEII for each row
declare

qt_peso_percentil_w	number(15,0);
qt_peso_atual_kg_w	number(15,2);
qt_diurese_w		number(15,2);
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
/* Pontua��o rela��o Pa02/FiO2 */
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.qt_rel_po2_fio2 >= 1) and
	(:new.qt_rel_po2_fio2 <= 2.49) then
	:new.qt_pto_rel_po2_fio2:= 5;
elsif	(:new.qt_rel_po2_fio2 >= 0.3) and
	(:new.qt_rel_po2_fio2 < 1) then
	:new.qt_pto_rel_po2_fio2:= 16;
elsif	(:new.qt_rel_po2_fio2 < 0.3) then
	:new.qt_pto_rel_po2_fio2:= 28;
elsif	(:new.qt_rel_po2_fio2 > 2.49) then
	:new.qt_pto_rel_po2_fio2:= 0;
end if;

/* Pontua��o para PAM */
if	(:new.qt_pa_diastolica is not null) and
	(:new.qt_pa_sistolica is not null) then
	:new.qt_pam:= nvl(dividir((:new.qt_pa_sistolica + (:new.qt_pa_diastolica * 2)), 3),0);
end if;

if	(:new.qt_pam is null) then
	:new.qt_pto_pam:= 0;
elsif	(:new.qt_pam < 20) then
	:new.qt_pto_pam:= 19;
elsif	(:new.qt_pam >= 20) and
	(:new.qt_pam <= 29) then
	:new.qt_pto_pam:= 9;
elsif	(:new.qt_pam >= 30) then
	:new.qt_pto_pam:= 0;
end if;

/* Pontua��o da Temperatura C� */
if	(:new.qt_temp is null) then
	:new.qt_pto_temp:= 0;
elsif	(:new.qt_temp < 35) then
	:new.qt_pto_temp:= 15;
elsif	(:new.qt_temp >= 35) and
	(:new.qt_temp <= 35.6) then
	:new.qt_pto_temp:= 8;
elsif	(:new.qt_temp > 35.6) then
	:new.qt_pto_temp:= 0;
end if;

/* Pontua��o pH S�rico m�nimo */
if	(:new.qt_ph_serico is null) then
	:new.qt_pto_ph_serico:= 0;
elsif	(:new.qt_ph_serico < 7.1) then
	:new.qt_pto_ph_serico:= 16;
elsif	(:new.qt_ph_serico >= 7.1) and
	(:new.qt_ph_serico <= 7.19) then
	:new.qt_pto_ph_serico:= 7;
elsif	(:new.qt_ph_serico >= 7.2) then
	:new.qt_pto_ph_serico:= 0;
end if;

/* Pontua��o Creatinina S�rica ou Diurese */
qt_peso_atual_kg_w := dividir(nvl(:new.QT_PESO,:new.qt_peso_atual), 1000);
qt_diurese_w	   := dividir(:new.qt_diurese, qt_peso_atual_kg_w);
qt_diurese_w		:= dividir(qt_diurese_w,12);

if	(:new.qt_diurese is not null) and
	(qt_diurese_w < 0.1) then
	:new.qt_pto_diurese:= 18;
elsif	(qt_diurese_w >= 0.1) and
	(qt_diurese_w <= 0.9) then
	:new.qt_pto_diurese:= 5;
elsif	(qt_diurese_w >= 1) then
	:new.qt_pto_diurese:= 0;
elsif	(:new.qt_diurese is null) then
	:new.qt_pto_diurese:= 0;
end if;

/* Pontua��o Convul��es m�ltiplas */
select	decode(:new.ie_convulsao_multipla,'S',19,0)
into	:new.qt_pto_convul
from	dual;

/* Pontua��o de peso de nascimento */
if	(:new.qt_peso is null) then
	:new.qt_pto_peso:= 0;
elsif	(:new.qt_peso < 750) then
	:new.qt_pto_peso:= 17;
elsif	(:new.qt_peso >= 750) and
	(:new.qt_peso <= 999) then
	:new.qt_pto_peso:= 10;
elsif	(:new.qt_peso >= 1000) then
	:new.qt_pto_peso:= 0;

end if;

/* Pontua��o de PIG - Pequeno para idade gestacional */
if	(:old.qt_idade_gestacional is null) then
	qt_peso_percentil_w := 0;
	:new.qt_pto_idade_gest := 0;
elsif	(:new.qt_idade_gestacional = 22) then
	qt_peso_percentil_w := 320;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 23) then
	qt_peso_percentil_w := 380;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 24) then
	qt_peso_percentil_w := 430;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 25) then
	qt_peso_percentil_w := 500;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 26) then
	qt_peso_percentil_w := 580;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 27) then
	qt_peso_percentil_w := 670;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 28) then
	qt_peso_percentil_w := 740;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 29) then
	qt_peso_percentil_w := 820;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 30) then
	qt_peso_percentil_w := 920;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 31) then
	qt_peso_percentil_w := 1030;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 32) then
	qt_peso_percentil_w := 1140;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 33) then
	qt_peso_percentil_w := 1280;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 34) then
	qt_peso_percentil_w := 1420;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 35) then
	qt_peso_percentil_w := 1580;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 36) then
	qt_peso_percentil_w := 1750;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 37) then
	qt_peso_percentil_w := 1920;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 38) then
	qt_peso_percentil_w := 2120;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 39) then
	qt_peso_percentil_w := 2350;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 40) then
	qt_peso_percentil_w := 2520;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional = 41) then
	qt_peso_percentil_w := 2660;
	if	(:new.qt_peso < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
elsif	(:new.qt_idade_gestacional > 41) then
	qt_peso_percentil_w := 2750;
	if	(:new.qt_peso_atual < qt_peso_percentil_w) then
		:new.qt_pto_idade_gest := 12;
	else
		:new.qt_pto_idade_gest := 0;
	end if;
end if;

/* Pontua��o de APGAR */
if	(:new.qt_peso is null) then
	:new.qt_pto_peso:= 0;
elsif	(:new.qt_apgar_quinto_min < 7) then
	:new.qt_pto_apgar:= 18;
elsif	(:new.qt_apgar_quinto_min >= 7) then
	:new.qt_pto_apgar:= 0;
end if;


/* Obter a pontua��o total SNAP II */
:new.qt_snap_ii:= nvl(:new.qt_pto_rel_po2_fio2,0)	+ nvl(:new.qt_pto_pam,0)	+ nvl(:new.qt_pto_temp,0)	+
			nvl(:new.qt_pto_ph_serico,0)	+ nvl(:new.qt_pto_diurese,0	)	+ nvl(:new.qt_pto_convul,0);

/* Obter a pontua��o total SNAP-PE II*/
:new.qt_snappe_ii:= nvl(:new.qt_snap_ii,0) + nvl(:new.qt_pto_peso,0) + nvl(:new.qt_pto_idade_gest,0) + nvl(:new.qt_pto_apgar,0);

/* Obter mortalidade */
if	(:new.qt_peso < 1500) then
	if	(:new.qt_snappe_ii >= 0) and
		(:new.qt_snappe_ii <= 9) then
		:new.pr_mortalidade := 0.5;
	elsif	(:new.qt_snappe_ii >= 10) and
		(:new.qt_snappe_ii <= 19) then
		:new.pr_mortalidade := 2.2;
	elsif	(:new.qt_snappe_ii >= 20) and
		(:new.qt_snappe_ii <= 29) then
		:new.pr_mortalidade := 4.9;
	elsif	(:new.qt_snappe_ii >= 30) and
		(:new.qt_snappe_ii <= 39) then
		:new.pr_mortalidade := 9.3;
	elsif	(:new.qt_snappe_ii >= 40) and
		(:new.qt_snappe_ii <= 49) then
		:new.pr_mortalidade := 15.7;
	elsif	(:new.qt_snappe_ii >= 50) and
		(:new.qt_snappe_ii <= 59) then
		:new.pr_mortalidade := 22.6;
	elsif	(:new.qt_snappe_ii >= 60) and
		(:new.qt_snappe_ii <= 69) then
		:new.pr_mortalidade := 32.3;
	elsif	(:new.qt_snappe_ii >= 70) and
		(:new.qt_snappe_ii <= 79) then
		:new.pr_mortalidade := 43.8;
	elsif	(:new.qt_snappe_ii >= 80) then
		:new.pr_mortalidade := 64.4;
	end if;
elsif	(:new.qt_peso >= 1500) then
	if	(:new.qt_snappe_ii >= 0) and
		(:new.qt_snappe_ii <= 9) then
		:new.pr_mortalidade := 0.3;
	elsif	(:new.qt_snappe_ii >= 10) and
		(:new.qt_snappe_ii <= 19) then
		:new.pr_mortalidade := 1.3;
	elsif	(:new.qt_snappe_ii >= 20) and
		(:new.qt_snappe_ii <= 29) then
		:new.pr_mortalidade := 2.8;
	elsif	(:new.qt_snappe_ii >= 30) and
		(:new.qt_snappe_ii <= 39) then
		:new.pr_mortalidade := 6.5;
	elsif	(:new.qt_snappe_ii >= 40) and
		(:new.qt_snappe_ii <= 49) then
		:new.pr_mortalidade := 13.0;
	elsif	(:new.qt_snappe_ii >= 50) and
		(:new.qt_snappe_ii <= 59) then
		:new.pr_mortalidade := 19.0;
	elsif	(:new.qt_snappe_ii >= 60) and
		(:new.qt_snappe_ii <= 69) then
		:new.pr_mortalidade := 32.0;
	elsif	(:new.qt_snappe_ii >= 70) and
		(:new.qt_snappe_ii <= 79) then
		:new.pr_mortalidade := 47.6;
	elsif	(:new.qt_snappe_ii >= 80) then
		:new.pr_mortalidade := 55.6;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.ESCALA_SNAPII_SNAPPEII ADD (
  CONSTRAINT ESCSNSN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SNAPII_SNAPPEII ADD (
  CONSTRAINT ESCSNSN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSNSN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ESCSNSN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_SNAPII_SNAPPEII TO NIVEL_1;

