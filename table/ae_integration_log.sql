ALTER TABLE TASY.AE_INTEGRATION_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_INTEGRATION_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_INTEGRATION_LOG
(
  EVENT_ID                 VARCHAR2(36 BYTE),
  TIMESTAMP                TIMESTAMP(6),
  USER_ID                  VARCHAR2(100 BYTE),
  USER_FULLNAME            VARCHAR2(256 BYTE),
  HOST_ADDRESS             VARCHAR2(256 BYTE),
  INTEGRATION_ID           VARCHAR2(50 BYTE),
  CONTENT_SENDED           CLOB,
  CONTENT_ERROR            CLOB,
  CONTENT_RESULT           CLOB,
  RECEIVE_UPDATE           TIMESTAMP(6),
  RECEIVE_USER_ID          VARCHAR2(100 BYTE),
  STATUS                   VARCHAR2(15 BYTE),
  GENERATION_DATE          TIMESTAMP(6),
  RELEASE_DATE             TIMESTAMP(6),
  RETURN_DATE              TIMESTAMP(6),
  SEQUENCE_INFORMATION     NUMBER(10),
  SEQUENCE_SYSTEM_DESTINY  NUMBER(10),
  SEQUENCE_SYSTEM_ORIGIN   NUMBER(10),
  RETRANSMISSION           NUMBER(5),
  LOG_SEQUENCE             NUMBER(10),
  PARENTE_EVENT_ID         VARCHAR2(36 BYTE),
  ROOT_EVENT_ID            VARCHAR2(36 BYTE),
  TIMESTAMP_ZONE_ID        VARCHAR2(256 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (CONTENT_SENDED) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (CONTENT_ERROR) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (CONTENT_RESULT) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AEITLOG_PK ON TASY.AE_INTEGRATION_LOG
(EVENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_INTEGRATION_LOG ADD (
  CONSTRAINT AEITLOG_PK
 PRIMARY KEY
 (EVENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_INTEGRATION_LOG TO NIVEL_1;

