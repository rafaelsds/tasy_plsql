ALTER TABLE TASY.XML_ATRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.XML_ATRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.XML_ATRIBUTO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_ELEMENTO           NUMBER(10)          NOT NULL,
  NR_SEQ_APRESENTACAO       NUMBER(10)          NOT NULL,
  NM_ATRIBUTO_XML           VARCHAR2(60 BYTE)   NOT NULL,
  NM_ATRIBUTO               VARCHAR2(60 BYTE),
  IE_CRIAR_NULO             VARCHAR2(1 BYTE)    NOT NULL,
  IE_OBRIGATORIO            VARCHAR2(1 BYTE),
  IE_TIPO_ATRIBUTO          VARCHAR2(10 BYTE),
  DS_MASCARA                VARCHAR2(20 BYTE),
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NR_SEQ_ATRIB_ELEM         NUMBER(10),
  IE_CRIAR_ATRIBUTO         VARCHAR2(1 BYTE)    NOT NULL,
  DS_CABECALHO              VARCHAR2(255 BYTE),
  IE_CONTROLE_PB            VARCHAR2(1 BYTE)    NOT NULL,
  IE_REMOVER_ESPACO_BRANCO  VARCHAR2(1 BYTE),
  DS_NAMESPACE              VARCHAR2(20 BYTE),
  NM_TABELA_DEF_BANCO       VARCHAR2(30 BYTE),
  NM_ATRIBUTO_XML_PESQUISA  VARCHAR2(60 BYTE),
  CD_EXP_OBSERVACAO         NUMBER(10),
  NM_TABELA_BASE            VARCHAR2(50 BYTE),
  NM_ATRIBUTO_BASE          VARCHAR2(50 BYTE),
  QT_TAMANHO                NUMBER(10),
  QT_DECIMAIS               NUMBER(10),
  CD_DOMINIO                NUMBER(5),
  NR_SEQ_ITEM_XSD           NUMBER(10),
  IE_ATRIB_CDATA            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.XMLATRB_PK ON TASY.XML_ATRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_DICEXPR_FK_I ON TASY.XML_ATRIBUTO
(CD_EXP_OBSERVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_DOMINIO_FK_I ON TASY.XML_ATRIBUTO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_I ON TASY.XML_ATRIBUTO
(NM_ATRIBUTO_XML_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_TABATRI_FK_I ON TASY.XML_ATRIBUTO
(NM_TABELA_BASE, NM_ATRIBUTO_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_TABSIST_FK_I ON TASY.XML_ATRIBUTO
(NM_TABELA_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_XMLELEM_FK_I ON TASY.XML_ATRIBUTO
(NR_SEQ_ELEMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLATRI_XMLELEM_FK2_I ON TASY.XML_ATRIBUTO
(NR_SEQ_ATRIB_ELEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.xml_atributo_atual
before insert or update ON TASY.XML_ATRIBUTO for each row
declare

begin

begin
if	(:new.nm_atributo_xml <> :old.nm_atributo_xml) or
	((:old.nm_atributo_xml_pesquisa is null) and (:new.nm_atributo_xml  is not null)) then
	:new.nm_atributo_xml_pesquisa	:= upper(:new.nm_atributo_xml);
end if;
exception
when others then
	:new.nm_atributo_xml_pesquisa	:= null;
end;

end xml_atributo_atual;
/


CREATE OR REPLACE TRIGGER TASY.XML_ATRIBUTO_tp  after update ON TASY.XML_ATRIBUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REMOVER_ESPACO_BRANCO,1,500);gravar_log_alteracao(substr(:old.IE_REMOVER_ESPACO_BRANCO,1,4000),substr(:new.IE_REMOVER_ESPACO_BRANCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REMOVER_ESPACO_BRANCO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_ATRIBUTO_XML,1,500);gravar_log_alteracao(substr(:old.NM_ATRIBUTO_XML,1,4000),substr(:new.NM_ATRIBUTO_XML,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO_XML',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ELEMENTO,1,4000),substr(:new.NR_SEQ_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ELEMENTO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_APRESENTACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CRIAR_NULO,1,500);gravar_log_alteracao(substr(:old.IE_CRIAR_NULO,1,4000),substr(:new.IE_CRIAR_NULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_NULO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_OBRIGATORIO,1,500);gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATRIBUTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATRIBUTO,1,4000),substr(:new.IE_TIPO_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATRIBUTO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA,1,4000),substr(:new.DS_MASCARA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_ATRIBUTO,1,500);gravar_log_alteracao(substr(:old.NM_ATRIBUTO,1,4000),substr(:new.NM_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ATRIB_ELEM,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ATRIB_ELEM,1,4000),substr(:new.NR_SEQ_ATRIB_ELEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ATRIB_ELEM',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CRIAR_ATRIBUTO,1,500);gravar_log_alteracao(substr(:old.IE_CRIAR_ATRIBUTO,1,4000),substr(:new.IE_CRIAR_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_ATRIBUTO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CABECALHO,1,500);gravar_log_alteracao(substr(:old.DS_CABECALHO,1,4000),substr(:new.DS_CABECALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CABECALHO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CONTROLE_PB,1,500);gravar_log_alteracao(substr(:old.IE_CONTROLE_PB,1,4000),substr(:new.IE_CONTROLE_PB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE_PB',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'XML_ATRIBUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.XML_ATRIBUTO ADD (
  CONSTRAINT XMLATRB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.XML_ATRIBUTO ADD (
  CONSTRAINT XMLATRI_XMLELEM_FK2 
 FOREIGN KEY (NR_SEQ_ATRIB_ELEM) 
 REFERENCES TASY.XML_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT XMLATRI_XMLELEM_FK 
 FOREIGN KEY (NR_SEQ_ELEMENTO) 
 REFERENCES TASY.XML_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT XMLATRI_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_OBSERVACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT XMLATRI_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO),
  CONSTRAINT XMLATRI_TABATRI_FK 
 FOREIGN KEY (NM_TABELA_BASE, NM_ATRIBUTO_BASE) 
 REFERENCES TASY.TABELA_ATRIBUTO (NM_TABELA,NM_ATRIBUTO),
  CONSTRAINT XMLATRI_TABSIST_FK 
 FOREIGN KEY (NM_TABELA_BASE) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA));

GRANT SELECT ON TASY.XML_ATRIBUTO TO NIVEL_1;

