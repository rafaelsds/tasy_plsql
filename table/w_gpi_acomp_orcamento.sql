DROP TABLE TASY.W_GPI_ACOMP_ORCAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_GPI_ACOMP_ORCAMENTO
(
  NR_SEQ_ORCAMENTO   NUMBER(10)                 NOT NULL,
  NR_SEQ_CONTA       NUMBER(10),
  DS_GERENCIAL       VARCHAR2(120 BYTE)         NOT NULL,
  CD_CLASSIFICACAO   VARCHAR2(40 BYTE),
  VL_ORCADO          NUMBER(15,2)               NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  NR_SEQ_ETAPA       NUMBER(10),
  VL_REALIZADO       NUMBER(15,2),
  CD_MATERIAL        NUMBER(6),
  CD_CENTRO_CUSTO    NUMBER(8),
  DT_MES_REFERENCIA  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_GPI_ACOMP_ORCAMENTO TO NIVEL_1;

