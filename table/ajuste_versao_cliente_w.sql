ALTER TABLE TASY.AJUSTE_VERSAO_CLIENTE_W
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AJUSTE_VERSAO_CLIENTE_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.AJUSTE_VERSAO_CLIENTE_W
(
  CD_VERSAO            VARCHAR2(15 BYTE),
  NR_RELEASE           NUMBER(10),
  NR_ORDEM             NUMBER(10),
  NM_RESPONSAVEL       VARCHAR2(60 BYTE),
  DS_MOTIVO            VARCHAR2(2000 BYTE),
  DS_COMANDO           CLOB,
  NR_SEQUENCIA         NUMBER(10),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_COMANDO_LONG      LONG
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_COMANDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AJVERCLIW_PK ON TASY.AJUSTE_VERSAO_CLIENTE_W
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AJUSTE_VERSAO_CLIENTE_W ADD (
  CONSTRAINT AJVERCLIW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AJUSTE_VERSAO_CLIENTE_W TO NIVEL_1;

