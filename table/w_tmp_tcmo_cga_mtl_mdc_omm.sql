DROP TABLE TASY.W_TMP_TCMO_CGA_MTL_MDC_OMM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TMP_TCMO_CGA_MTL_MDC_OMM
(
  DT_LIB_INTERCOMPANY   DATE,
  IE_LIB_INTERCOMPANY   VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOLO      NUMBER(10)              NOT NULL,
  TCMO_MES_RFR          DATE,
  TCMO_PRTD_COD         NUMBER(5)               NOT NULL,
  TCMO_COD_EMP          NUMBER(2)               NOT NULL,
  TCMO_TIP_GUI          NUMBER(2)               NOT NULL,
  TCMO_NRO_GUI          NUMBER(12)              NOT NULL,
  TCMO_TIP_DSP          NUMBER(1)               NOT NULL,
  TCMO_SEQ_ITE          NUMBER(5)               NOT NULL,
  TCMO_DAT_EXU_RAL      DATE                    NOT NULL,
  TCMO_HOR_INI_EXU_RAL  DATE                    NOT NULL,
  TCMO_HOR_FIM_EXU_RAL  DATE                    NOT NULL,
  TCMO_TBL_RFR          NUMBER(2)               NOT NULL,
  TCMO_COD_ITE          NUMBER(10)              NOT NULL,
  TCMO_QTD_ITE          NUMBER(7,2)             NOT NULL,
  TCMO_UND_MDD_ITE      VARCHAR2(3 BYTE)        NOT NULL,
  TCMO_FAT_RDT_ACR_ITE  NUMBER(4,2),
  TCMO_VLR_UNT_ITE      NUMBER(15,2)            NOT NULL,
  TCMO_VLR_TOT_ITE      NUMBER(15,2)            NOT NULL,
  TCMO_NRO_REG_MTL_AVV  VARCHAR2(15 BYTE),
  TCMO_COD_RFR_MTL_FAB  VARCHAR2(30 BYTE),
  TCMO_NRO_AUT_EMP_VDO  VARCHAR2(30 BYTE),
  TCMO_DES_ITE          VARCHAR2(150 BYTE)      NOT NULL,
  TCMO_VAL_CST_MDI_UNT  NUMBER(8,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QTTCMMO_IK ON TASY.W_TMP_TCMO_CGA_MTL_MDC_OMM
(TCMO_MES_RFR, TCMO_PRTD_COD, TCMO_COD_EMP, TCMO_TIP_GUI, TCMO_NRO_GUI, 
TCMO_TIP_DSP, TCMO_SEQ_ITE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


GRANT SELECT ON TASY.W_TMP_TCMO_CGA_MTL_MDC_OMM TO NIVEL_1;

