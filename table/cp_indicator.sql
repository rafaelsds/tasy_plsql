ALTER TABLE TASY.CP_INDICATOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CP_INDICATOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CP_INDICATOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_UNIQUE_VALUE      VARCHAR2(255 BYTE),
  DS_DISPLAY_NAME      VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_ORDER         NUMBER(5),
  IE_LANGUAGE          VARCHAR2(10 BYTE),
  DS_VERSION           VARCHAR2(255 BYTE),
  IE_ORIGIN            VARCHAR2(10 BYTE)        NOT NULL,
  IE_ACTIVE            VARCHAR2(10 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_DATA_TYPE         VARCHAR2(10 BYTE),
  NR_SEQ_MEASURE_TYPE  NUMBER(10),
  IE_PROGRESSIVE       VARCHAR2(1 BYTE),
  DS_ALTERNATIVE       VARCHAR2(255 BYTE),
  SI_IMPORT_STATUS     VARCHAR2(2 BYTE),
  SI_RECORD_CHANGED    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPINDIC_CPMEATY_FK_I ON TASY.CP_INDICATOR
(NR_SEQ_MEASURE_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPINDIC_PK ON TASY.CP_INDICATOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CP_INDICATOR ADD (
  CONSTRAINT CPINDIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CP_INDICATOR ADD (
  CONSTRAINT CPINDIC_CPMEATY_FK 
 FOREIGN KEY (NR_SEQ_MEASURE_TYPE) 
 REFERENCES TASY.CP_MEASURE_TYPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CP_INDICATOR TO NIVEL_1;

