ALTER TABLE TASY.CYCLE_MENU
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CYCLE_MENU CASCADE CONSTRAINTS;

CREATE TABLE TASY.CYCLE_MENU
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_CYCLE             VARCHAR2(1024 BYTE),
  DT_VALIDITY_START    DATE,
  DT_VALIDITY_END      DATE                     NOT NULL,
  NR_CYCLE_DAYS        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CM_PK ON TASY.CYCLE_MENU
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cycle_menu_after_update
after update ON TASY.CYCLE_MENU for each row

/*
This Trigger is used to update the initial and final date into NUT_CARDAPIO_DIA table once update the same dates from CYCLE_MENU
*/
begin

if	(:old.dt_validity_start <> :new.dt_validity_start or :old.dt_validity_end <> :new.dt_validity_end) then

    update nut_cardapio_dia
    set dt_vigencia_inicial = :new.dt_validity_start,
    dt_vigencia_final = :new.dt_validity_end,
    dt_atualizacao = :new.dt_atualizacao,
    nm_usuario = :new.nm_usuario
    where nr_seq_cycle = :new.nr_sequencia;

end if;

end;
/


ALTER TABLE TASY.CYCLE_MENU ADD (
  CONSTRAINT CM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.CYCLE_MENU TO NIVEL_1;

