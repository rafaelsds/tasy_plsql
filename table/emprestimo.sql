ALTER TABLE TASY.EMPRESTIMO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMPRESTIMO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMPRESTIMO
(
  NR_EMPRESTIMO       NUMBER(10)                NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  CD_LOCAL_ESTOQUE    NUMBER(5)                 NOT NULL,
  IE_TIPO             VARCHAR2(1 BYTE)          NOT NULL,
  DT_EMPRESTIMO       DATE                      NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_PESSOA_JURIDICA  VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE),
  NR_DOCUMENTO        VARCHAR2(20 BYTE),
  NM_USUARIO_RESP     VARCHAR2(15 BYTE),
  DT_PREV_RETORNO     DATE,
  IE_SITUACAO         VARCHAR2(1 BYTE),
  DS_OBSERVACAO       VARCHAR2(255 BYTE),
  DT_LIBERACAO        DATE,
  NR_SEQ_MOTIVO       NUMBER(10),
  NR_EMPRESTIMO_REF   NUMBER(10),
  DT_APROVACAO        DATE,
  NM_USUARIO_APROV    VARCHAR2(15 BYTE),
  DT_REPROVACAO       DATE,
  NM_USUARIO_REPROV   VARCHAR2(15 BYTE),
  IE_ORIGEM           VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMPREST_ESTABEL_FK_I ON TASY.EMPRESTIMO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPREST_LOCESTO_FK_I ON TASY.EMPRESTIMO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPREST_MOTEMPR_FK_I ON TASY.EMPRESTIMO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPREST_MOTEMPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPREST_PESFISI_FK_I ON TASY.EMPRESTIMO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPREST_PESJURI_FK_I ON TASY.EMPRESTIMO
(CD_PESSOA_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EMPREST_PK ON TASY.EMPRESTIMO
(NR_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.emprestimo_atual
before update ON TASY.EMPRESTIMO for each row
declare

ie_acao_w			varchar2(1);
ie_disp_emprestimo_lib_w	parametro_estoque.ie_disp_emprestimo_lib%type;
cd_material_w			emprestimo_material.cd_material%type;
nr_seq_lote_w			emprestimo_material.nr_seq_lote%type;
qt_material_w			emprestimo_material.qt_material%type;

cd_material_ww			emprestimo_material.cd_material%type;
nr_seq_lote_ww			emprestimo_material.nr_seq_lote%type;
qt_material_ww			emprestimo_material.qt_material%type;

ie_erro_w			varchar2(255);
ie_atualizou_lote_w		varchar2(1);
ie_gerar_w			varchar2(1);
ie_quantidade_w			varchar2(15);
ie_desdobrado_w			varchar2(1);

Cursor C01 is
select	b.cd_material_estoque,
	nr_seq_lote,
	qt_material
from	emprestimo_material a,
	material b
where	a.cd_material = b.cd_material
and	a.nr_emprestimo = :new.nr_emprestimo;

Cursor C02 is
select	b.cd_material_estoque,
	a.nr_sequencia,
	a.qt_material
from	material_lote_fornec a,
	material b
where	a.cd_material = b.cd_material
and	a.cd_material = cd_material_w
and	a.nr_emprestimo = :new.nr_emprestimo;

begin
if	(:old.dt_liberacao is not null) and
	(:new.dt_liberacao is null) then
	begin
	select	max(ie_disp_emprestimo_lib)
	into	ie_disp_emprestimo_lib_w
	from	parametro_estoque
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if	(:new.ie_tipo = 'E') then
		ie_acao_w	:=	'1';
	else
		ie_acao_w	:=	'2';
	end if;

	if	(ie_disp_emprestimo_lib_w = 'S') then
		begin
		open C01;
		loop
		fetch C01 into
			cd_material_w,
			nr_seq_lote_w,
			qt_material_w;
		exit when C01%notfound;
			begin
			ie_atualizou_lote_w	:=	'N';

			obter_regra_empr_lote_fornec(
				cd_material_w,
				:new.cd_estabelecimento,
				ie_gerar_w,
				ie_quantidade_w,
				ie_desdobrado_w);

			if	(ie_desdobrado_w = 'S') then
				open C02;
				loop
				fetch C02 into
					cd_material_ww,
					nr_seq_lote_ww,
					qt_material_ww;
				exit when C02%notfound;
					begin
					atualizar_saldo_lote(
						:new.cd_estabelecimento,
						:new.cd_local_estoque,
						cd_material_ww,
						trunc(sysdate,'mm'),
						nr_seq_lote_ww,
						0,
						qt_material_ww,
						ie_acao_w,
						:new.nm_usuario,
						ie_erro_w);

					ie_atualizou_lote_w	:=	'S';
					end;
				end loop;
				close C02;
			end if;

			if	(ie_atualizou_lote_w = 'N') then
				atualizar_saldo_lote(
					:new.cd_estabelecimento,
					:new.cd_local_estoque,
					cd_material_w,
					trunc(sysdate,'mm'),
					nr_seq_lote_w,
					0,
					qt_material_w,
					ie_acao_w,
					:new.nm_usuario,
					ie_erro_w);
			end if;


			end;
		end loop;
		close C01;
		end;
	end if;
	end;
end if;

end;
/


ALTER TABLE TASY.EMPRESTIMO ADD (
  CONSTRAINT EMPREST_PK
 PRIMARY KEY
 (NR_EMPRESTIMO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMPRESTIMO ADD (
  CONSTRAINT EMPREST_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT EMPREST_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EMPREST_PESJURI_FK 
 FOREIGN KEY (CD_PESSOA_JURIDICA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT EMPREST_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EMPREST_MOTEMPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_EMPRESTIMO (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMPRESTIMO TO NIVEL_1;

