ALTER TABLE TASY.MAN_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_EQUIPAMENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DS_EQUIPAMENTO             VARCHAR2(80 BYTE)  NOT NULL,
  NR_SEQ_LOCAL               NUMBER(10)         NOT NULL,
  NR_SEQ_TIPO_EQUIP          NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_PLANEJ              NUMBER(10)         NOT NULL,
  NR_SEQ_TRAB                NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  CD_IMOBILIZADO             VARCHAR2(20 BYTE),
  CD_IMPACTO                 VARCHAR2(1 BYTE),
  CD_ESTAB_CONTABIL          NUMBER(4),
  CD_CENTRO_CUSTO            NUMBER(8),
  IE_VOLTAGEM                VARCHAR2(15 BYTE),
  DT_ANO_FABRICACAO          NUMBER(4),
  DS_MARCA                   VARCHAR2(50 BYTE),
  DS_MODELO                  VARCHAR2(50 BYTE),
  QT_PESO                    NUMBER(9,3),
  NR_SEQ_FABRICANTE          NUMBER(10),
  CD_NACIONALIDADE           VARCHAR2(8 BYTE),
  CD_FORNECEDOR              VARCHAR2(14 BYTE),
  DT_AQUISICAO               DATE,
  VL_AQUISICAO               NUMBER(15,2),
  CD_MOEDA                   NUMBER(3),
  DT_INICIO_GARANTIA         DATE,
  DT_FIM_GARANTIA            DATE,
  NR_DOC_GARANTIA            VARCHAR2(20 BYTE),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SERIE                   VARCHAR2(30 BYTE),
  IE_DISPONIBILIDADE         VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_CONTADOR            NUMBER(10),
  IE_ROTINA_SEGURANCA        VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_PROPRIEDADE             VARCHAR2(1 BYTE)   NOT NULL,
  CD_CGC_TERC                VARCHAR2(14 BYTE),
  VL_CUSTO_SEM_UTILIZACAO    NUMBER(15,2),
  IE_PARADO                  VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_SUPERIOR            NUMBER(10),
  DS_PADRAO_OS               VARCHAR2(2000 BYTE),
  CD_PESSOA_TERCEIRO         VARCHAR2(10 BYTE),
  CD_IMOBILIZADO_EXT         VARCHAR2(20 BYTE),
  IE_CONTROLE_SETOR          VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_CATEGORIA           NUMBER(10),
  NR_SEQ_STATUS              NUMBER(10),
  IE_CALIBRACAO              VARCHAR2(1 BYTE),
  DS_ENDERECO                VARCHAR2(255 BYTE),
  IE_TENSAO_DC               VARCHAR2(20 BYTE),
  IE_TENSAO_AC               VARCHAR2(20 BYTE),
  NR_SEQ_MARCA               NUMBER(10),
  NR_SEQ_MODELO              NUMBER(10),
  QT_ALTURA                  NUMBER(7,2),
  QT_LARGURA                 NUMBER(7,2),
  QT_COMPRIMENTO             NUMBER(7,2),
  QT_CORRENTE_AC             VARCHAR2(20 BYTE),
  QT_CORRENTE_DC             VARCHAR2(20 BYTE),
  QT_POTENCIA_AC             VARCHAR2(20 BYTE),
  QT_POTENCIA_DC             VARCHAR2(20 BYTE),
  IE_PRIORIDADE              VARCHAR2(15 BYTE),
  IE_EQUIP_HOSPITALAR        VARCHAR2(15 BYTE),
  NR_REGISTRO_ANVISA         VARCHAR2(60 BYTE),
  NR_SEQ_FAMILIA             NUMBER(10),
  QT_CARGA_HORARIA           NUMBER(5,2),
  NR_SEQ_ESTAGIO_INICIO_OS   NUMBER(10),
  NR_SEQ_CLASSIF_RISCO       NUMBER(10),
  NR_SEQ_CLASSIF_ELET_EQUIP  NUMBER(10),
  DS_DANO_BREVE              VARCHAR2(80 BYTE),
  DS_COR_EQUIP               VARCHAR2(255 BYTE),
  NR_SEQ_BEM                 NUMBER(10),
  QT_TEMPO_VIDA_UTIL         NUMBER(10),
  IE_EXIGE_FUNCAO_OS         VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_ORDEM          NUMBER(10),
  IE_TIPO_ORDEM              VARCHAR2(15 BYTE),
  DS_OBSERVACAO_TENSAO       VARCHAR2(2000 BYTE),
  IE_TENSAO_DC_SAIDA         VARCHAR2(20 BYTE),
  IE_TENSAO_AC_SAIDA         VARCHAR2(20 BYTE),
  QT_CORRENTE_AC_SAIDA       VARCHAR2(20 BYTE),
  QT_CORRENTE_DC_SAIDA       VARCHAR2(20 BYTE),
  QT_POTENCIA_AC_SAIDA       VARCHAR2(20 BYTE),
  QT_POTENCIA_DC_SAIDA       VARCHAR2(20 BYTE),
  IE_CONSISTE_OS_DUPLIC      VARCHAR2(15 BYTE),
  NR_SEQ_ORIGEM_DANO         NUMBER(10),
  CD_CONTROLE                VARCHAR2(30 BYTE),
  IE_CLASSIFICACAO_OS        VARCHAR2(1 BYTE),
  DT_PROG_PREVENTIVA         DATE,
  NR_SEQ_NF_INTEG            NUMBER(10),
  NR_SEQ_ITEM_NF_INTEG       NUMBER(5),
  CD_MATERIAL                NUMBER(6),
  CD_LOTE_FABRICACAO         VARCHAR2(20 BYTE),
  NR_SEQ_LOTE_FORNEC         NUMBER(10),
  DT_INSTALACAO              DATE,
  DS_STATUS_VIDA_UTIL        VARCHAR2(250 BYTE),
  DS_STATUS_GARANTIA         VARCHAR2(250 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANEQUI_CENCUST_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_ESTABEL_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_ESTAB_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_GRUPLAN_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_GRUTRAB_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_TRAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_LOCALIZ_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_MAESTPR_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_ESTAGIO_INICIO_OS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MAESTPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANCAEQ_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANCAEQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANCLRI_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_CLASSIF_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANCLRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANCSEL_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_CLASSIF_ELET_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANCSEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANEFAM_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANEFAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANEQTA_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANEQTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANEQUI_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANFABR_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_FABRICANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANFABR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANMARC_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_MANMODE_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANORDA_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_ORIGEM_DANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANORDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MANTICO_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_CONTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_MANTIOS_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_TIPO_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MANTIOS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_MATERIA_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_MOEDA_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_NACIONA_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_NACIONALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_NACIONA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_NOTFIIT_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_NF_INTEG, NR_SEQ_ITEM_NF_INTEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_NOTFISC_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_NF_INTEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_PATBEM_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_BEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_PESFISI_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_PESSOA_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_PESJURI_FK_I ON TASY.MAN_EQUIPAMENTO
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANEQUI_PESJURI_FK2_I ON TASY.MAN_EQUIPAMENTO
(CD_CGC_TERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANEQUI_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANEQUI_PK ON TASY.MAN_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_SEARCH ON TASY.MAN_EQUIPAMENTO
("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_EQUIPAMENTO"))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANEQUI_TPEQUIP_FK_I ON TASY.MAN_EQUIPAMENTO
(NR_SEQ_TIPO_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAN_EQUIPAMENTO_ATUAL
AFTER INSERT OR UPDATE ON TASY.MAN_EQUIPAMENTO FOR EACH ROW
DECLARE

begin
if	(:old.nm_usuario is not null) then
	insert into man_equipamento_alteracao(
			nr_sequencia,
			ds_equipamento,
			nr_seq_local,
			nr_seq_tipo_equip,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_planej,
			nr_seq_trab,
			ie_situacao,
			cd_imobilizado,
			cd_impacto,
			cd_estab_contabil,
			cd_imobilizado_ext,
			cd_centro_custo,
			ie_voltagem,
			dt_ano_fabricacao,
			ds_marca,
			ds_modelo,
			nr_serie,
			qt_peso,
			nr_seq_fabricante,
			nr_seq_superior,
			cd_nacionalidade,
			nr_seq_categoria,
			nr_seq_status,
			cd_fornecedor,
			dt_aquisicao,
			vl_aquisicao,
			cd_moeda,
			vl_custo_sem_utilizacao,
			ie_controle_setor,
			dt_inicio_garantia,
			dt_fim_garantia,
			nr_doc_garantia,
			nr_seq_contador,
			ie_rotina_seguranca,
			ds_padrao_os,
			ie_disponibilidade,
			ie_propriedade,
			ds_observacao,
			cd_cgc_terc,
			ie_parado,
			cd_pessoa_terceiro,
			ie_calibracao,
			ds_endereco,
			ie_tensao_dc,
			ie_tensao_ac,
			nr_seq_equipamento,
			ie_prioridade,
			qt_corrente_ac,
			qt_potencia_ac,
			qt_corrente_dc,
			qt_potencia_dc,
			nr_seq_marca,
			nr_seq_modelo,
			qt_largura,
			qt_comprimento,
			qt_altura,
			ie_tensao_dc_saida,
			ie_tensao_ac_saida,
			qt_corrente_ac_saida,
			qt_corrente_dc_saida,
			qt_potencia_ac_saida,
			qt_potencia_dc_saida,
			ds_observacao_tensao)
		values(	man_equipamento_alteracao_seq.nextval,
			:old.ds_equipamento,
			:old.nr_seq_local,
			:old.nr_seq_tipo_equip,
			sysdate,
			:old.nm_usuario,
			:old.dt_atualizacao_nrec,
			:old.nm_usuario_nrec,
			:old.nr_seq_planej,
			:old.nr_seq_trab,
			:old.ie_situacao,
			:old.cd_imobilizado,
			:old.cd_impacto,
			:old.cd_estab_contabil,
			:old.cd_imobilizado_ext,
			:old.cd_centro_custo,
			:old.ie_voltagem,
			:old.dt_ano_fabricacao,
			:old.ds_marca,
			:old.ds_modelo,
			:old.nr_serie,
			:old.qt_peso,
			:old.nr_seq_fabricante,
			:old.nr_seq_superior,
			:old.cd_nacionalidade,
			:old.nr_seq_categoria,
			:old.nr_seq_status,
			:old.cd_fornecedor,
			:old.dt_aquisicao,
			:old.vl_aquisicao,
			:old.cd_moeda,
			:old.vl_custo_sem_utilizacao,
			:old.ie_controle_setor,
			:old.dt_inicio_garantia,
			:old.dt_fim_garantia,
			:old.nr_doc_garantia,
			:old.nr_seq_contador,
			:old.ie_rotina_seguranca,
			:old.ds_padrao_os,
			:old.ie_disponibilidade,
			:old.ie_propriedade,
			:old.ds_observacao,
			:old.cd_cgc_terc,
			:old.ie_parado,
			:old.cd_pessoa_terceiro,
			:old.ie_calibracao,
			:old.ds_endereco,
			:old.ie_tensao_dc,
			:old.ie_tensao_ac,
			:old.nr_sequencia,
			:old.ie_prioridade,
			:old.qt_corrente_ac,
			:old.qt_potencia_ac,
			:old.qt_corrente_dc,
			:old.qt_potencia_dc,
			:old.nr_seq_marca,
			:old.nr_seq_modelo,
			:old.qt_largura,
			:old.qt_comprimento,
			:old.qt_altura,
			:old.ie_tensao_dc_saida,
			:old.ie_tensao_ac_saida,
			:old.qt_corrente_ac_saida,
			:old.qt_corrente_dc_saida,
			:old.qt_potencia_ac_saida,
			:old.qt_potencia_dc_saida,
			:old.ds_observacao_tensao);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.man_equipamento_Delete
BEFORE DELETE on man_equipamento
for each row
BEGIN
update	nf_integracao_pat_equip
set	nr_seq_equipamento	= null
where	nr_seq_equipamento	= :old.nr_sequencia;

END;
/


CREATE OR REPLACE TRIGGER TASY.man_equipamento_update
BEFORE UPDATE ON TASY.MAN_EQUIPAMENTO for each row
declare

ds_mensagem_w			varchar2(4000);
qt_regra_fixa_w			number(10,0) := 0;

BEGIN
if	(:new.ie_situacao = 'I') then
	/*'O usu�rio ' || :new.nm_usuario || ' inativou o equipamento ' || :old.nr_sequencia || ' - ' || :old.ds_equipamento || '.' || chr(13) ||
				'Imobilizado: ' || :old.cd_imobilizado || '.';*/
	ds_mensagem_w := 	wheb_mensagem_pck.get_texto(305911,'NM_USUARIO_W='||:new.nm_usuario||';NR_SEQUENCIA_W='||:old.nr_sequencia||';DS_EQUIPAMENTO_W='||:old.ds_equipamento||';CD_IMOBILIZADO_W='||:old.cd_imobilizado);

	select	count(*)
	into	qt_regra_fixa_w
	from	man_regra_data_frequencia
	where	nr_seq_equipamento = :old.nr_sequencia;

	if	(qt_regra_fixa_w > 0) then
		begin
		delete  from man_regra_data_frequencia
		where	nr_seq_equipamento = :old.nr_sequencia;

		end;
	end if;

end if;

if	(:new.nr_seq_superior <> :old.nr_seq_superior) then

	insert	into	man_equip_log_equip_sup (
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_equipamento,
		nr_seq_superior
	)values(
		man_equip_log_equip_sup_seq.nextval,
		sysdate,
		sysdate,
		:new.nm_usuario,
		:new.nm_usuario,
		:old.nr_sequencia,
		:new.nr_seq_superior);
end if;

END;
/


ALTER TABLE TASY.MAN_EQUIPAMENTO ADD (
  CONSTRAINT MANEQUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_EQUIPAMENTO ADD (
  CONSTRAINT MANEQUI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MANEQUI_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQ_NF_INTEG, NR_SEQ_ITEM_NF_INTEG) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF),
  CONSTRAINT MANEQUI_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NF_INTEG) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANMODE_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MAN_MODELO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANEFAM_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MAN_EQUIP_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO_INICIO_OS) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANCLRI_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_RISCO) 
 REFERENCES TASY.MAN_CLASSIF_RISCO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANCSEL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ELET_EQUIP) 
 REFERENCES TASY.MAN_CLASSIF_SEG_ELETRICA (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_PATBEM_FK 
 FOREIGN KEY (NR_SEQ_BEM) 
 REFERENCES TASY.PAT_BEM (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANTIOS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ORDEM) 
 REFERENCES TASY.MAN_TIPO_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANORDA_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM_DANO) 
 REFERENCES TASY.MAN_ORIGEM_DANO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_GRUPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_GRUTRAB_FK 
 FOREIGN KEY (NR_SEQ_TRAB) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_CONTABIL) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MANEQUI_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT MANEQUI_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MANEQUI_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT MANEQUI_NACIONA_FK 
 FOREIGN KEY (CD_NACIONALIDADE) 
 REFERENCES TASY.NACIONALIDADE (CD_NACIONALIDADE),
  CONSTRAINT MANEQUI_TPEQUIP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EQUIP) 
 REFERENCES TASY.MAN_TIPO_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRICANTE) 
 REFERENCES TASY.MAN_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_LOCALIZ_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANTICO_FK 
 FOREIGN KEY (NR_SEQ_CONTADOR) 
 REFERENCES TASY.MAN_TIPO_CONTADOR (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_TERC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MANEQUI_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_TERCEIRO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT MANEQUI_MANCAEQ_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.MAN_CATEGORIA_EQUIP (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANEQTA_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.MAN_EQUIP_STATUS (NR_SEQUENCIA),
  CONSTRAINT MANEQUI_MANMARC_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MAN_MARCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_EQUIPAMENTO TO NIVEL_1;

