ALTER TABLE TASY.PLS_TITULO_REC_LIQ_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TITULO_REC_LIQ_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TITULO_REC_LIQ_MENS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_TITULO              NUMBER(10)             NOT NULL,
  NR_SEQ_BAIXA           NUMBER(10)             NOT NULL,
  NR_SEQ_ITEM_MENS       NUMBER(10),
  NR_SEQ_SEGURADO        NUMBER(10),
  CD_CONTA_DEBITO        VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO       VARCHAR2(20 BYTE),
  IE_TIPO_LANCAMENTO     VARCHAR2(5 BYTE)       NOT NULL,
  NR_LOTE_CONTABIL       NUMBER(10)             NOT NULL,
  CD_HISTORICO           NUMBER(10),
  VL_LANCAMENTO          NUMBER(15,2)           NOT NULL,
  DT_ADESAO              DATE,
  DT_REF_MENS            DATE,
  DT_INICIO_COBERTURA    DATE                   NOT NULL,
  DT_REF_CONTAB_INICIAL  DATE                   NOT NULL,
  DT_FIM_COBERTURA       DATE                   NOT NULL,
  DT_REF_CONTAB_FINAL    DATE                   NOT NULL,
  DT_CONTABIL            DATE                   NOT NULL,
  CD_CENTRO_CUSTO        NUMBER(8),
  NR_CODIGO_CONTROLE     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTRLM_CENCUST_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_CONCONT_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(CD_CONTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_CONCONT_FK2_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(CD_CONTA_DEBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_HISPADR_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_LOTCONT_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSTRLM_PK ON TASY.PLS_TITULO_REC_LIQ_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLM_PLSMSI_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(NR_SEQ_ITEM_MENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_PLSMSI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_PLSSEGU_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRLM_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRLM_TIRELIQ_FK_I ON TASY.PLS_TITULO_REC_LIQ_MENS
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_titulo_rec_liq_mens_after
after insert or update ON TASY.PLS_TITULO_REC_LIQ_MENS for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w			number(10);
dt_movimento_w          	ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w   	ctb_documento.nr_seq_trans_financ%type;
nr_documento_w          	ctb_documento.nr_documento%type;
nr_seq_doc_compl_w      	ctb_documento.nr_seq_doc_compl%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_contab_tit_cancelado_w	parametro_contas_receber.ie_contab_tit_cancelado%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;

Cursor c01 is
	select 	c.cd_tipo_lote_contab,
		14 nr_seq_info_ctb,
		'PLS_TITULO_REC_LIQ_MENS' nm_tabela,
		c.nm_atributo nm_atributo
	from	atributo_contab c
	where 	c.cd_tipo_lote_contab = 39
	and	c.nm_atributo = 'VL_REC_PLS_MENS'
	and	ie_concil_contab_w = 'S'
	and exists (	select 	1
			from 	titulo_receber_liq y
			where	y.nr_seq_lote_enc_contas is null
			and     y.nr_seq_pls_lote_camara is null
			and	nvl(y.ie_lib_caixa, 'S') = 'S'
			and	exists	(select	1
					from	titulo_receber x
					where	x.nr_titulo	= y.nr_titulo
					and	x.ie_pls	= 'S'
					and	x.ie_origem_titulo = '3'
					and	((x.ie_situacao	<> '3' or ie_contab_tit_cancelado_w = 'S')
					or 	(x.ie_situacao = '3' and y.vl_rec_maior <> 0)))
			and	y.nr_sequencia 	= :new.nr_seq_baixa
			and	y.nr_titulo 	= :new.nr_titulo);

	vet_c01 c01%rowtype;

begin
begin
if (:new.nr_seq_baixa is not null) and (:new.nr_titulo is not null) then

	/*Esse select � para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 	= to_char(:new.nr_titulo)
	and	nr_seq_item_documento	= to_char(:new.nr_seq_baixa)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p :=	:new.nr_seq_baixa;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;

select 	a.cd_estabelecimento,
	c.dt_contabil,
	b.nr_seq_trans_fin,
	a.nr_titulo,
	b.nr_sequencia
into	cd_estabelecimento_w,
	dt_movimento_w,
	nr_seq_trans_financ_w,
	nr_documento_w,
	nr_seq_doc_compl_w
from	titulo_receber a,
	titulo_receber_liq b,
	pls_titulo_rec_liq_mens c
where	a.nr_titulo = b.nr_titulo
and	b.nr_titulo = c.nr_titulo
and	b.nr_sequencia = c.nr_seq_baixa
and	c.nr_sequencia = :new.nr_sequencia;

exception when others then
	nr_seq_trans_financ_w	:= null;
end;

select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_w;

select  nvl(max(ie_contab_tit_cancelado), 'S')
into   	ie_contab_tit_cancelado_w
from    parametro_contas_receber
where   cd_estabelecimento = cd_estabelecimento_w;

if      (nvl(:new.vl_lancamento, 0) <> 0 and nvl(nr_seq_trans_financ_w,0) <> 0) then
	open c01;
	loop
	fetch c01 into
		vet_c01;
	exit when c01%notfound;
	begin

	ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
								dt_movimento_w,
								vet_c01.cd_tipo_lote_contab,
								nr_seq_trans_financ_w,
								vet_c01.nr_seq_info_ctb,
								nr_documento_w,
								nr_seq_doc_compl_w,
								:new.nr_sequencia,
								:new.vl_lancamento,
								vet_c01.nm_tabela,
								vet_c01.nm_atributo,
								:new.nm_usuario);

	end;
	end loop;
	close c01;

end if;
end;
/


ALTER TABLE TASY.PLS_TITULO_REC_LIQ_MENS ADD (
  CONSTRAINT PLSTRLM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TITULO_REC_LIQ_MENS ADD (
  CONSTRAINT PLSTRLM_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PLSTRLM_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CREDITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSTRLM_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEBITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSTRLM_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSTRLM_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSTRLM_PLSMSI_FK 
 FOREIGN KEY (NR_SEQ_ITEM_MENS) 
 REFERENCES TASY.PLS_MENSALIDADE_SEG_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSTRLM_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSTRLM_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT DELETE ON TASY.PLS_TITULO_REC_LIQ_MENS TO DWONATAM;

GRANT SELECT ON TASY.PLS_TITULO_REC_LIQ_MENS TO NIVEL_1;

