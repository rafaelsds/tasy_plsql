DROP TABLE TASY.EIS_HD_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_HD_EXAME
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_REFERENCIA        DATE,
  NR_SEQ_UNID_DIALISE  NUMBER(10),
  QT_KTV1              NUMBER(10),
  QT_KTV2              NUMBER(10),
  QT_KTV3              NUMBER(10),
  QT_KTV4              NUMBER(10),
  QT_KTV5              NUMBER(10),
  QT_HB1               NUMBER(10),
  QT_HB2               NUMBER(10),
  QT_HB3               NUMBER(10),
  QT_HT1               NUMBER(10),
  QT_HT2               NUMBER(10),
  QT_HT3               NUMBER(10),
  QT_HT4               NUMBER(10),
  QT_HT5               NUMBER(10),
  QT_PO41              NUMBER(10),
  QT_PO42              NUMBER(10),
  QT_CAXPO41           NUMBER(10),
  QT_CAXPO42           NUMBER(10),
  QT_PTH1              NUMBER(10),
  QT_PTH2              NUMBER(10),
  QT_PTH3              NUMBER(10),
  QT_PTH4              NUMBER(10),
  QT_PTH5              NUMBER(10),
  QT_ALB1              NUMBER(10),
  QT_ALB2              NUMBER(10),
  QT_HBS_AG_POS        NUMBER(10),
  QT_ANTI_HBS_POS      NUMBER(10),
  QT_ANTI_HCV_POS      NUMBER(10),
  QT_ANTI_HIV_POS      NUMBER(10),
  QT_CONV_HBS_AG       NUMBER(10),
  QT_CONV_HCV          NUMBER(10),
  QT_CONV_HIV          NUMBER(10),
  QT_CRONICOS_FIM_MES  NUMBER(10),
  QT_MES_ATUAL         NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  QT_KTV6              NUMBER(10),
  QT_KTV7              NUMBER(10),
  QT_HBS_AG_NEG        NUMBER(10),
  QT_HB4               NUMBER(10),
  QT_HB5               NUMBER(10),
  IE_TRATAMENTO        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISHDEX_EMPRESA_FK_I ON TASY.EIS_HD_EXAME
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISHDEX_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EISHDEX_ESTABEL_FK_I ON TASY.EIS_HD_EXAME
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISHDEX_ESTABEL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EIS_HD_EXAME ADD (
  CONSTRAINT EISHDEX_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EISHDEX_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.EIS_HD_EXAME TO NIVEL_1;

