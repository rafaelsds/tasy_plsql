ALTER TABLE TASY.ESCALA_NIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NIC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PROFISSIONAL             VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                DATE              NOT NULL,
  DT_LIBERACAO                DATE,
  NM_USUAIRO_LIB              VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  QT_SUPERF_CORPORIA          NUMBER(15,2),
  QT_PESO                     NUMBER(10),
  QT_ALTURA_CM                NUMBER(5),
  QT_CREATININA_SERICA        NUMBER(5,2),
  IE_IDADE                    VARCHAR2(1 BYTE),
  IE_PA_MAX                   VARCHAR2(1 BYTE),
  IE_BALAO_AORTICO            VARCHAR2(1 BYTE),
  IE_INSUF_CARD               VARCHAR2(1 BYTE),
  IE_ANEMIA                   VARCHAR2(1 BYTE),
  IE_DIABETE                  VARCHAR2(1 BYTE),
  QT_CONTRASTE                NUMBER(5)         NOT NULL,
  IE_CREATININA_SERICA_MAIOR  VARCHAR2(1 BYTE),
  IE_CLEARENCE_CREATININA     VARCHAR2(1 BYTE),
  QT_RISCO_NIC                NUMBER(5,2),
  QT_RISCO_DIALISE            NUMBER(5,2),
  QT_ESCORE                   NUMBER(5,2),
  QT_CLEARENCE_CREATININA     NUMBER(5,2),
  IE_CREATINIA                VARCHAR2(3 BYTE),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCALNI_ESTABEL_FK_I ON TASY.ESCALA_NIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALNI_PESFISI_FK_I ON TASY.ESCALA_NIC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCALNI_PK ON TASY.ESCALA_NIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_nic_atual
before insert or update ON TASY.ESCALA_NIC for each row
declare

begin
	:new.qt_escore	:= 0;

	if	(:new.ie_pa_max = 'S') then
		:new.qt_escore := :new.qt_escore + 5;
	end if;

	if	(:new.ie_balao_aortico = 'S') then
		:new.qt_escore := :new.qt_escore + 5;
	end if;

	if	(:new.ie_insuf_card = 'S') then
		:new.qt_escore := :new.qt_escore + 5;
	end if;

	if	(:new.ie_idade = 'S') then
		:new.qt_escore := :new.qt_escore + 4;
	end if;

	if	(:new.ie_anemia = 'S') then
		:new.qt_escore := :new.qt_escore + 3;
	end if;

	if	(:new.ie_diabete = 'S') then
		:new.qt_escore := :new.qt_escore + 3;
	end if;

	if	(:new.ie_creatinina_serica_maior = 'S') then
		:new.qt_escore := :new.qt_escore + 4;
	end if;

	if	(:new.ie_clearence_creatinina = 'S') then
		if	(:new.qt_creatinina_serica <= 20) then
			:new.qt_escore := :new.qt_escore + 6;
		elsif	(:new.qt_creatinina_serica <= 40) then
			:new.qt_escore := :new.qt_escore + 4;
		elsif	(:new.qt_creatinina_serica <= 60) then
			:new.qt_escore := :new.qt_escore + 2;
		end if;

	end if;

	:new.qt_escore := :new.qt_escore + round(:new.qt_contraste / 100);

	if	(:new.qt_escore <= 5) then
		:new.qt_risco_nic	:= 7.5;
		:new.qt_risco_dialise	:= 0.04;
	elsif	(:new.qt_escore <= 10) then
		:new.qt_risco_nic	:= 14;
		:new.qt_risco_dialise := 0.12;
	elsif	(:new.qt_escore < 16) then
		:new.qt_risco_nic 	:= 26.1;
		:new.qt_risco_dialise	:= 1.09;
	elsif	(:new.qt_escore >= 16) then
		:new.qt_risco_nic	:= 57.3;
		:new.qt_risco_dialise	:= 12.6;
	end if;


end;
/


ALTER TABLE TASY.ESCALA_NIC ADD (
  CONSTRAINT ESCALNI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NIC ADD (
  CONSTRAINT ESCALNI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ESCALNI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_NIC TO NIVEL_1;

