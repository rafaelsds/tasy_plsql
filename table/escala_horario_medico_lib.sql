ALTER TABLE TASY.ESCALA_HORARIO_MEDICO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_HORARIO_MEDICO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_HORARIO_MEDICO_LIB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_ESCALA          NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_ESCALA_HORARIO  NUMBER(10)             NOT NULL,
  IE_DIA_SEMANA          VARCHAR2(15 BYTE),
  HR_INICIAL_VINC        VARCHAR2(20 BYTE),
  HR_FINAL_VINC          VARCHAR2(20 BYTE),
  IE_FERIADO             VARCHAR2(15 BYTE),
  DT_FINAL_VINC          DATE,
  DT_INICIAL_VINC        DATE,
  HR_INICIAL_VINCULO     DATE,
  HR_FINAL_VINCULO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCHMLI_ESCALA_FK_I ON TASY.ESCALA_HORARIO_MEDICO_LIB
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCHMLI_ESCALA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCHMLI_ESCHORME_FK_I ON TASY.ESCALA_HORARIO_MEDICO_LIB
(NR_SEQ_ESCALA_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCHMLI_ESCHORME_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ESCHMLI_PK ON TASY.ESCALA_HORARIO_MEDICO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_horario_medico_lib_atua
before insert or update ON TASY.ESCALA_HORARIO_MEDICO_LIB for each row
declare

begin
begin
    if (:new.hr_final_vinc is not null) and ((:new.hr_final_vinc <> :old.hr_final_vinc) or (:old.dt_final_vinc is null)) then
		:new.dt_final_vinc := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final_vinc,'dd/mm/yyyy hh24:mi');
	end if;
    if (:new.hr_inicial_vinc is not null) and ((:new.hr_inicial_vinc <> :old.hr_inicial_vinc) or (:old.dt_inicial_vinc is null)) then
		:new.dt_inicial_vinc := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicial_vinc,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_HORARIO_MEDICO_LIB_SYNC
before insert or update ON TASY.ESCALA_HORARIO_MEDICO_LIB for each row
declare

begin

begin
if (nvl(:old.HR_INICIAL_VINC,'X') <> nvl(:new.HR_INICIAL_VINC,'X')) then
 if (:new.HR_INICIAL_VINC is not null) then
  :new.HR_INICIAL_VINCULO := pkg_date_utils.get_Time(sysdate,:new.HR_INICIAL_VINC);
 else
  :new.HR_INICIAL_VINCULO := null;
 end if;
elsif (nvl(:old.HR_INICIAL_VINCULO,pkg_date_utils.get_Time('')) <> nvl(:new.HR_INICIAL_VINCULO,pkg_date_utils.get_Time(''))) then
 :new.HR_INICIAL_VINC := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_INICIAL_VINCULO),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_INICIAL_VINCULO),2,0);
end if;

if (nvl(:old.HR_FINAL_VINC,'X') <> nvl(:new.HR_FINAL_VINC,'X')) then
 if (:new.HR_FINAL_VINC is not null) then
  :new.HR_FINAL_VINCULO := pkg_date_utils.get_Time(sysdate,:new.HR_FINAL_VINC);
 else
  :new.HR_FINAL_VINCULO := null;
 end if;
elsif (nvl(:old.HR_FINAL_VINCULO,pkg_date_utils.get_Time('')) <> nvl(:new.HR_FINAL_VINCULO,pkg_date_utils.get_Time(''))) then
 :new.HR_FINAL_VINC := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_FINAL_VINCULO),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_FINAL_VINCULO),2,0);
end if;
exception
when others then
 null;
end;

end;
/


ALTER TABLE TASY.ESCALA_HORARIO_MEDICO_LIB ADD (
  CONSTRAINT ESCHMLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_HORARIO_MEDICO_LIB ADD (
  CONSTRAINT ESCHMLI_ESCALA_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.ESCALA (NR_SEQUENCIA),
  CONSTRAINT ESCHMLI_ESCHORME_FK 
 FOREIGN KEY (NR_SEQ_ESCALA_HORARIO) 
 REFERENCES TASY.ESCALA_HORARIO_MEDICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_HORARIO_MEDICO_LIB TO NIVEL_1;

