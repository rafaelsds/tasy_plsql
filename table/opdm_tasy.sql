ALTER TABLE TASY.OPDM_TASY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_TASY
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_TITULO              VARCHAR2(255 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.OPDMTASY_PK ON TASY.OPDM_TASY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_TASY_tp  after update ON TASY.OPDM_TASY FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_USUARIO_INATIVACAO,1,4000),substr(:new.NM_USUARIO_INATIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_INATIVACAO',ie_log_w,ds_w,'OPDM_TASY',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_TASY',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INATIVACAO',ie_log_w,ds_w,'OPDM_TASY',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'OPDM_TASY',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'OPDM_TASY',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_TASY ADD (
  CONSTRAINT OPDMTASY_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_TASY TO NIVEL_1;

