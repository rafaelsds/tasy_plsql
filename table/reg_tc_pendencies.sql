ALTER TABLE TASY.REG_TC_PENDENCIES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_TC_PENDENCIES CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_TC_PENDENCIES
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TC              NUMBER(10)             NOT NULL,
  NR_SEQ_PR              NUMBER(10)             NOT NULL,
  DS_VERSION             VARCHAR2(255 BYTE),
  CD_FUNCAO              VARCHAR2(255 BYTE)     NOT NULL,
  IE_STATUS              VARCHAR2(255 BYTE)     NOT NULL,
  IE_TIPO_MUDANCA        VARCHAR2(2 BYTE),
  NR_SEQ_CONTROLE_PLANO  NUMBER(10),
  DS_MOTIVO_EXCLUSAO     VARCHAR2(255 BYTE),
  NR_SEQ_INTENCAO_USO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGTCPE_PK ON TASY.REG_TC_PENDENCIES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTCPE_RECATE_FK_I ON TASY.REG_TC_PENDENCIES
(NR_SEQ_TC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTCPE_REGINU_FK_I ON TASY.REG_TC_PENDENCIES
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTCPE_REGPR_FK_I ON TASY.REG_TC_PENDENCIES
(NR_SEQ_PR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTCPE_REGPTC_FK_I ON TASY.REG_TC_PENDENCIES
(NR_SEQ_CONTROLE_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_tc_pend_before_insert
  before insert ON TASY.REG_TC_PENDENCIES   for each row
declare

  nr_seq_intencao_uso_w reg_tc_pendencies.nr_seq_intencao_uso%type;

begin

	if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		goto Final;
	end if;

    if (:new.nr_seq_intencao_uso is null and :new.nr_seq_tc is not null) then

        select nr_seq_intencao_uso
          into nr_seq_intencao_uso_w
          from reg_caso_teste
         where nr_sequencia = :new.nr_seq_tc;

        :new.nr_seq_intencao_uso := nr_seq_intencao_uso_w;

    end if;

	<<Final>>

	nr_seq_intencao_uso_w := null;

end;
/


ALTER TABLE TASY.REG_TC_PENDENCIES ADD (
  CONSTRAINT REGTCPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_TC_PENDENCIES ADD (
  CONSTRAINT REGTCPE_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA),
  CONSTRAINT REGTCPE_RECATE_FK 
 FOREIGN KEY (NR_SEQ_TC) 
 REFERENCES TASY.REG_CASO_TESTE (NR_SEQUENCIA),
  CONSTRAINT REGTCPE_REGPR_FK 
 FOREIGN KEY (NR_SEQ_PR) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT REGTCPE_REGPTC_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_PLANO) 
 REFERENCES TASY.REG_PLANO_TESTE_CONTROLE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_TC_PENDENCIES TO NIVEL_1;

