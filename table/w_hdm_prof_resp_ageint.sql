ALTER TABLE TASY.W_HDM_PROF_RESP_AGEINT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_HDM_PROF_RESP_AGEINT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_HDM_PROF_RESP_AGEINT
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_CAPTACAO           NUMBER(10),
  CD_PESSOA_FISICA_RESP     VARCHAR2(10 BYTE),
  NR_SEQ_PARTIC_CICLO_ITEM  NUMBER(10),
  CD_ESPECIALIDADE          NUMBER(5),
  CD_CLASSIFICACAO          VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WHPROFA_ESTABEL_FK_I ON TASY.W_HDM_PROF_RESP_AGEINT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WHPROFA_MPRCAPT_FK_I ON TASY.W_HDM_PROF_RESP_AGEINT
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WHPROFA_MPRPACI_FK_I ON TASY.W_HDM_PROF_RESP_AGEINT
(NR_SEQ_PARTIC_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WHPROFA_PESFISI_FK_I ON TASY.W_HDM_PROF_RESP_AGEINT
(CD_PESSOA_FISICA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WHPROFA_PK ON TASY.W_HDM_PROF_RESP_AGEINT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_HDM_PROF_RESP_AGEINT ADD (
  CONSTRAINT WHPROFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_HDM_PROF_RESP_AGEINT ADD (
  CONSTRAINT WHPROFA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WHPROFA_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT WHPROFA_MPRPACI_FK 
 FOREIGN KEY (NR_SEQ_PARTIC_CICLO_ITEM) 
 REFERENCES TASY.MPREV_PARTIC_CICLO_ITEM (NR_SEQUENCIA),
  CONSTRAINT WHPROFA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.W_HDM_PROF_RESP_AGEINT TO NIVEL_1;

