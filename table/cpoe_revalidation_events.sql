ALTER TABLE TASY.CPOE_REVALIDATION_EVENTS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_REVALIDATION_EVENTS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_REVALIDATION_EVENTS
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10),
  CD_MEDICO                 VARCHAR2(10 BYTE),
  CD_MEDICO_ANTERIOR        VARCHAR2(10 BYTE),
  NR_SEQ_DIALYSIS           NUMBER(10),
  NR_SEQ_DIET               NUMBER(10),
  NR_SEQ_HEMOTHERAPY        NUMBER(10),
  NR_SEQ_MATERIAL           NUMBER(10),
  NR_SEQ_EXAM               NUMBER(10),
  NR_SEQ_RECOMENDATION      NUMBER(10),
  NR_SEQ_GASOTHERAPY        NUMBER(10),
  NR_SEQ_REVALIDATION_RULE  NUMBER(10),
  DT_VALIDACAO              DATE                DEFAULT null,
  DT_ENVIO_COMUNICADO       DATE                DEFAULT null,
  DS_STACK                  VARCHAR2(4000 BYTE) DEFAULT null,
  NR_SEQ_ASSINATURA_REV     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPREEVEN_ATEPACI_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEDIA_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_DIALYSIS)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEDIE_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_DIET)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEGAS_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_GASOTHERAPY)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEMAT_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEPRO_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_EXAM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOEREC_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_RECOMENDATION)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPOHEMO_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_HEMOTHERAPY)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_CPRERULE_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_REVALIDATION_RULE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_I2 ON TASY.CPOE_REVALIDATION_EVENTS
(DT_VALIDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_PESFISI_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(CD_MEDICO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_PESFISI_FK2_I ON TASY.CPOE_REVALIDATION_EVENTS
(CD_MEDICO_ANTERIOR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPREEVEN_PK ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPREEVEN_TASASDI_FK_I ON TASY.CPOE_REVALIDATION_EVENTS
(NR_SEQ_ASSINATURA_REV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cpoe_reval_events_bef_insert
before insert ON TASY.CPOE_REVALIDATION_EVENTS for each row
declare

begin

	:new.ds_stack := substr(dbms_utility.format_call_stack, 1, 3000);

end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_reval_events_aft_insert
after insert or update ON TASY.CPOE_REVALIDATION_EVENTS for each row
declare

	ds_stack_w		varchar2(2000);
	ds_log_cpoe_w	varchar(2000);
	dt_min_date_w	date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

begin

	if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_atendimento(' || nvl(:old.nr_atendimento,0) || '/' || nvl(:new.nr_atendimento,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_medico,0) <> nvl(:old.cd_medico,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_medico(' || nvl(:old.cd_medico,0) || '/' || nvl(:new.cd_medico,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_medico_anterior,0) <> nvl(:old.cd_medico_anterior,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_medico_anterior(' || nvl(:old.cd_medico_anterior,0) || '/' || nvl(:new.cd_medico_anterior,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_validacao, dt_min_date_w) <> nvl(:old.dt_validacao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_validacao(' || nvl(to_char(:old.dt_validacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_validacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_envio_comunicado, dt_min_date_w) <> nvl(:old.dt_envio_comunicado, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_envio_comunicado(' || nvl(to_char(:old.dt_envio_comunicado,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_envio_comunicado,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_dialysis,0) <> nvl(:old.nr_seq_dialysis,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_dialysis(' || nvl(:old.nr_seq_dialysis,0) || '/' || nvl(:new.nr_seq_dialysis,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_diet,0) <> nvl(:old.nr_seq_diet,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_diet(' || nvl(:old.nr_seq_diet,0) || '/' || nvl(:new.nr_seq_diet,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_hemotherapy,0) <> nvl(:old.nr_seq_hemotherapy,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_hemotherapy(' || nvl(:old.nr_seq_hemotherapy,0) || '/' || nvl(:new.nr_seq_hemotherapy,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_material,0) <> nvl(:old.nr_seq_material,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_material(' || nvl(:old.nr_seq_material,0) || '/' || nvl(:new.nr_seq_material,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_exam,0) <> nvl(:old.nr_seq_exam,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_exam(' || nvl(:old.nr_seq_exam,0) || '/' || nvl(:new.nr_seq_exam,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_recomendation,0) <> nvl(:old.nr_seq_recomendation,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_recomendation(' || nvl(:old.nr_seq_recomendation,0) || '/' || nvl(:new.nr_seq_recomendation,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_gasotherapy,0) <> nvl(:old.nr_seq_gasotherapy,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_gasotherapy(' || nvl(:old.nr_seq_gasotherapy,0) || '/' || nvl(:new.nr_seq_gasotherapy,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_revalidation_rule,0) <> nvl(:old.nr_seq_revalidation_rule,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_revalidation_rule(' || nvl(:old.nr_seq_revalidation_rule,0) || '/' || nvl(:new.nr_seq_revalidation_rule,0)||'); ',1,2000);
	end if;

	if	(ds_log_cpoe_w is not null) then

		if (nvl(:old.nr_sequencia, 0) > 0) then
			ds_log_cpoe_w := substr('Altera��es(old/new)= ' || ds_log_cpoe_w,1,2000);
		else
			ds_log_cpoe_w := substr('Cria��o(old/new)= ' || ds_log_cpoe_w,1,2000);
		end if;

		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr('CPOE_REVAL_EVENTS_AFT_INSERT: ' || ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

		insert into log_cpoe(	nr_sequencia,
								nr_atendimento,
								dt_atualizacao,
								nm_usuario,
								nr_seq_material,
								nr_seq_dieta,
								nr_seq_gasoterapia,
								nr_seq_recomendacao,
								nr_seq_procedimento,
								nr_seq_dialise,
								nr_seq_hemoterapia,
								ds_log,
								ds_stack)
					values (	log_cpoe_seq.nextval,
								:new.nr_atendimento,
								sysdate,
								:new.nm_usuario,
								:new.nr_seq_material,
								:new.nr_seq_diet,
								:new.nr_seq_gasotherapy,
								:new.nr_seq_recomendation,
								:new.nr_seq_exam,
								:new.nr_seq_dialysis,
								:new.nr_seq_hemotherapy,
								ds_log_cpoe_w,
								ds_stack_w);
	end if;

exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr('EXCEPTION CPOE_REVAL_EVENTS_AFT_INSERT ' || to_char(sqlerrm),1, 2000);

		insert into log_cpoe(nr_sequencia,
							nr_atendimento,
							dt_atualizacao,
							nm_usuario,
							ds_log,
							ds_stack)
		values (			log_cpoe_seq.nextval,
							:new.nr_atendimento,
							sysdate,
							:new.nm_usuario,
							ds_log_cpoe_w,
							ds_stack_w);
end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_reval_events_insert_wms
after insert ON TASY.CPOE_REVALIDATION_EVENTS for each row
declare

reg_integracao_p			gerar_int_padrao.reg_integracao;
cd_local_estoque_w			ap_lote.cd_local_estoque%type;
cd_setor_atendimento_w		ap_lote.cd_setor_atendimento%type;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;
nr_seq_lote_w				ap_lote.nr_sequencia%type;
ie_status_lote_w			ap_lote.ie_status_lote%type;
ie_param_119_w				varchar2(1):= 'N';

cursor c07 is
	select	e.cd_local_estoque,
			e.cd_setor_atendimento,
			b.cd_estabelecimento,
			e.nr_sequencia,
			e.ie_status_lote
	from 	prescr_medica b,
			prescr_material c,
			prescr_mat_hor d,
			ap_lote e,
			cpoe_material f
	where 	b.nr_prescricao = c.nr_prescricao
	and 	c.nr_prescricao = d.nr_prescricao
	and 	c.nr_sequencia = d.nr_seq_material
	and 	d.nr_seq_lote = e.nr_sequencia
	and 	c.nr_seq_mat_cpoe = f.nr_Sequencia
	and 	b.nr_atendimento = :new.nr_atendimento
	and 	f.nr_sequencia = :new.nr_seq_material;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	obter_param_usuario(7029, 119, wheb_usuario_pck.get_cd_perfil, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param_119_w);

	if	(nvl(ie_param_119_w,'N') = 'S') then
		open C07;
		loop
		fetch C07 into
			cd_local_estoque_w,
			cd_setor_atendimento_w,
			cd_estabelecimento_w,
			nr_seq_lote_w,
			ie_status_lote_w;
		exit when C07%notfound;
			begin
			reg_integracao_p.cd_estab_documento		:= cd_estabelecimento_w;
			reg_integracao_p.cd_local_estoque		:= cd_local_estoque_w;
			reg_integracao_p.cd_setor_atendimento	:= cd_setor_atendimento_w;
			reg_integracao_p.nr_seq_agrupador		:= nr_seq_lote_w;
			reg_integracao_p.ie_operacao			:= 'A';

			gerar_int_padrao.gravar_integracao('260', nr_seq_lote_w, :new.nm_usuario, reg_integracao_p);

			intdisp_movto_mat_hor(nr_seq_lote_w,ie_status_lote_w,cd_local_estoque_w,2); -- EOD
			intdisp_movto_mat_hor(nr_seq_lote_w,ie_status_lote_w,cd_local_estoque_w,1); -- EOA
			end;
		end loop;
		close C07;
	end if;
end if;
exception
when others then
	grava_log_tasy(1812,substr('Error cpoe_revalidation_events nr_seq_lote_w' || nr_seq_lote_w ||
	'Stack=' || SQLERRM(sqlcode) || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,1,2000),'INTEGRATION');
end;
/


ALTER TABLE TASY.CPOE_REVALIDATION_EVENTS ADD (
  CONSTRAINT CPREEVEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_REVALIDATION_EVENTS ADD (
  CONSTRAINT CPREEVEN_CPOEDIE_FK 
 FOREIGN KEY (NR_SEQ_DIET) 
 REFERENCES TASY.CPOE_DIETA (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPREEVEN_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_ANTERIOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPREEVEN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA_REV) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPOEREC_FK 
 FOREIGN KEY (NR_SEQ_RECOMENDATION) 
 REFERENCES TASY.CPOE_RECOMENDACAO (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPOEDIA_FK 
 FOREIGN KEY (NR_SEQ_DIALYSIS) 
 REFERENCES TASY.CPOE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPRERULE_FK 
 FOREIGN KEY (NR_SEQ_REVALIDATION_RULE) 
 REFERENCES TASY.CPOE_REVALIDATION_RULE (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPOEGAS_FK 
 FOREIGN KEY (NR_SEQ_GASOTHERAPY) 
 REFERENCES TASY.CPOE_GASOTERAPIA (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPOEMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.CPOE_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CPREEVEN_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_EXAM) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CPREEVEN_CPOHEMO_FK 
 FOREIGN KEY (NR_SEQ_HEMOTHERAPY) 
 REFERENCES TASY.CPOE_HEMOTERAPIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CPOE_REVALIDATION_EVENTS TO NIVEL_1;

