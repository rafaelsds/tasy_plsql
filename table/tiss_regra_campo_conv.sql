ALTER TABLE TASY.TISS_REGRA_CAMPO_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_CAMPO_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_CAMPO_CONV
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  NR_SEQ_CAMPO             NUMBER(10)           NOT NULL,
  IE_ENVIO                 VARCHAR2(3 BYTE)     NOT NULL,
  DS_PADRAO                VARCHAR2(4000 BYTE),
  IE_TIPO_ATENDIMENTO      NUMBER(3),
  CD_CATEGORIA             VARCHAR2(10 BYTE),
  IE_TAXAS                 VARCHAR2(3 BYTE)     NOT NULL,
  VL_MAX_CAMPO             NUMBER(15,4),
  DS_VALOR_GERADO          VARCHAR2(255 BYTE),
  IE_GASES                 VARCHAR2(1 BYTE)     NOT NULL,
  IE_MEDICAMENTO           VARCHAR2(1 BYTE)     NOT NULL,
  IE_MATERIAL              VARCHAR2(1 BYTE)     NOT NULL,
  IE_TAXAS_DIVERSAS        VARCHAR2(1 BYTE)     NOT NULL,
  IE_DIARIAS               VARCHAR2(1 BYTE)     NOT NULL,
  IE_ALUGUEIS              VARCHAR2(1 BYTE)     NOT NULL,
  IE_CLINICA               NUMBER(5),
  NR_SEQ_CLASSIFICACAO     NUMBER(10),
  CD_SETOR_ENTRADA         NUMBER(5),
  IE_TIPO_ATEND_TISS       VARCHAR2(5 BYTE),
  IE_TIPO_ATEND_PROC_TISS  VARCHAR2(5 BYTE),
  DS_MASCARA               VARCHAR2(255 BYTE),
  CD_PERFIL                NUMBER(5),
  CD_SETOR_EXEC            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRCC_CATCONV_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_CONVENIO, CD_CATEGORIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCC_CLAATEN_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCC_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCC_CONVENI_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCC_ESTABEL_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCC_PERFIL_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCC_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRCC_PK ON TASY.TISS_REGRA_CAMPO_CONV
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCC_SETATEN_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_SETOR_ENTRADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCC_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCC_SETEXEC_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(CD_SETOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCC_TISSLIC_FK_I ON TASY.TISS_REGRA_CAMPO_CONV
(NR_SEQ_CAMPO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_CAMPO_CONV_tp  after update ON TASY.TISS_REGRA_CAMPO_CONV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_CAMPO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CAMPO,1,4000),substr(:new.NR_SEQ_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CAMPO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_PADRAO,1,500);gravar_log_alteracao(substr(:old.DS_PADRAO,1,4000),substr(:new.DS_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PADRAO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CATEGORIA,1,500);gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_MAX_CAMPO,1,500);gravar_log_alteracao(substr(:old.VL_MAX_CAMPO,1,4000),substr(:new.VL_MAX_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAX_CAMPO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TAXAS,1,500);gravar_log_alteracao(substr(:old.IE_TAXAS,1,4000),substr(:new.IE_TAXAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAXAS',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_VALOR_GERADO,1,500);gravar_log_alteracao(substr(:old.DS_VALOR_GERADO,1,4000),substr(:new.DS_VALOR_GERADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALOR_GERADO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GASES,1,500);gravar_log_alteracao(substr(:old.IE_GASES,1,4000),substr(:new.IE_GASES,1,4000),:new.nm_usuario,nr_seq_w,'IE_GASES',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MEDICAMENTO,1,500);gravar_log_alteracao(substr(:old.IE_MEDICAMENTO,1,4000),substr(:new.IE_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICAMENTO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MATERIAL,1,500);gravar_log_alteracao(substr(:old.IE_MATERIAL,1,4000),substr(:new.IE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TAXAS_DIVERSAS,1,500);gravar_log_alteracao(substr(:old.IE_TAXAS_DIVERSAS,1,4000),substr(:new.IE_TAXAS_DIVERSAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAXAS_DIVERSAS',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DIARIAS,1,500);gravar_log_alteracao(substr(:old.IE_DIARIAS,1,4000),substr(:new.IE_DIARIAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIARIAS',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ALUGUEIS,1,500);gravar_log_alteracao(substr(:old.IE_ALUGUEIS,1,4000),substr(:new.IE_ALUGUEIS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALUGUEIS',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ENVIO,1,500);gravar_log_alteracao(substr(:old.IE_ENVIO,1,4000),substr(:new.IE_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENVIO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_CONV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_CAMPO_CONV ADD (
  CONSTRAINT TISSRCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_CAMPO_CONV ADD (
  CONSTRAINT TISSRCC_SETEXEC_FK 
 FOREIGN KEY (CD_SETOR_EXEC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSRCC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ENTRADA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSRCC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TISSRCC_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT TISSRCC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRCC_TISSLIC_FK 
 FOREIGN KEY (NR_SEQ_CAMPO) 
 REFERENCES TASY.TISS_LISTA_CAMPO (NR_SEQUENCIA),
  CONSTRAINT TISSRCC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSRCC_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA));

GRANT SELECT ON TASY.TISS_REGRA_CAMPO_CONV TO NIVEL_1;

