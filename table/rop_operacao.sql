ALTER TABLE TASY.ROP_OPERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_OPERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_OPERACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_OPERACAO             VARCHAR2(80 BYTE)     NOT NULL,
  IE_OPERACAO             VARCHAR2(15 BYTE)     NOT NULL,
  CD_EMPRESA              NUMBER(4)             NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_MOVTO_AUTOMATICO     VARCHAR2(1 BYTE)      NOT NULL,
  IE_AVISA_FIM_VIDA_UTIL  VARCHAR2(1 BYTE)      NOT NULL,
  IE_TIPO_OPERACAO        VARCHAR2(1 BYTE),
  IE_ENTRADA_SAIDA        VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_LOCAL            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPOPER_EMPRESA_FK_I ON TASY.ROP_OPERACAO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ROPOPER_PK ON TASY.ROP_OPERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPOPER_ROPLOCA_FK_I ON TASY.ROP_OPERACAO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPOPER_ROPLOCA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_OPERACAO_tp  after update ON TASY.ROP_OPERACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OPERACAO,1,4000),substr(:new.DS_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OPERACAO',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPERACAO,1,4000),substr(:new.IE_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPERACAO',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCAL,1,4000),substr(:new.NR_SEQ_LOCAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCAL',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AVISA_FIM_VIDA_UTIL,1,4000),substr(:new.IE_AVISA_FIM_VIDA_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_AVISA_FIM_VIDA_UTIL',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_OPERACAO,1,4000),substr(:new.IE_TIPO_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_OPERACAO',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENTRADA_SAIDA,1,4000),substr(:new.IE_ENTRADA_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENTRADA_SAIDA',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOVTO_AUTOMATICO,1,4000),substr(:new.IE_MOVTO_AUTOMATICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOVTO_AUTOMATICO',ie_log_w,ds_w,'ROP_OPERACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_OPERACAO ADD (
  CONSTRAINT ROPOPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_OPERACAO ADD (
  CONSTRAINT ROPOPER_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT ROPOPER_ROPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_OPERACAO TO NIVEL_1;

