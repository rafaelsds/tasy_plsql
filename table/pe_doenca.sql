ALTER TABLE TASY.PE_DOENCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_DOENCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_DOENCA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_DOENCA            VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEDOENC_PEGRUDO_FK_I ON TASY.PE_DOENCA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDOENC_PEGRUDO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEDOENC_PK ON TASY.PE_DOENCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDOENC_PK
  MONITORING USAGE;


ALTER TABLE TASY.PE_DOENCA ADD (
  CONSTRAINT PEDOENC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_DOENCA ADD (
  CONSTRAINT PEDOENC_PEGRUDO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PE_GRUPO_DOENCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_DOENCA TO NIVEL_1;

