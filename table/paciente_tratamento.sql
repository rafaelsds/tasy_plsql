ALTER TABLE TASY.PACIENTE_TRATAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_TRATAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_TRATAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  IE_TRATAMENTO         VARCHAR2(15 BYTE)       NOT NULL,
  DT_INICIO_TRATAMENTO  DATE                    NOT NULL,
  DT_FINAL_TRATAMENTO   DATE,
  DS_MOTIVO_FIM         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_FIM     NUMBER(10),
  NR_SEQ_RECEPTOR       NUMBER(10),
  IE_PACIENTE_AGUDO     VARCHAR2(1 BYTE),
  NR_SEQ_UNID_DIALISE   NUMBER(10),
  NR_SEQ_CAUSA_MORTE    NUMBER(10),
  CD_PERFIL_ATIVO       NUMBER(5),
  NR_ATEND_ORIGEM       NUMBER(10),
  IE_DOMICILIO          VARCHAR2(1 BYTE),
  IE_DIARIO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACTRAT_ATEPACI_FK_I ON TASY.PACIENTE_TRATAMENTO
(NR_ATEND_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACTRAT_CAUSAMO_FK_I ON TASY.PACIENTE_TRATAMENTO
(NR_SEQ_CAUSA_MORTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACTRAT_CAUSAMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACTRAT_MOTIFIM_FK_I ON TASY.PACIENTE_TRATAMENTO
(NR_SEQ_MOTIVO_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACTRAT_MOTIFIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACTRAT_PERFIL_FK_I ON TASY.PACIENTE_TRATAMENTO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACTRAT_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACTRAT_PESFISI_FK_I ON TASY.PACIENTE_TRATAMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACTRAT_PK ON TASY.PACIENTE_TRATAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACTRAT_TXRECE_FK_I ON TASY.PACIENTE_TRATAMENTO
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACTRAT_TXRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.paciente_tratamento_befinsupd
before insert or update ON TASY.PACIENTE_TRATAMENTO for each row
declare

cd_perfil_w		number(5,0);
cd_estabelecimento_w	number(5,0);
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_final_tratamento is not null) and (:new.nr_seq_motivo_fim is null) and (:new.ds_motivo_fim is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(182367);
end if;

if	(:new.dt_final_tratamento is null) and
	(:new.ie_tratamento in ('CAPD','CO','CPI','DPA','HD','PL','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','DPAV','DDC','IHF','IHDF')) then
	begin
	update	hd_pac_renal_cronico
	set	ie_situacao = 'S'
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
	end;
end if;

if	(:new.dt_final_tratamento is not null) then
	begin
	update	hd_pac_renal_cronico
	set	ie_situacao = 'N'
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
	end;
end if;

if	(:new.dt_final_tratamento is not null) and (:old.dt_final_tratamento is null) and
	(:new.ie_tratamento in ('CAPD','CO','CPI','DPA','HD','PL','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','DPAV','DDC','IHF','IHDF')) then
	begin
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	select	max(cd_perfil_paciente_fim)
	into	cd_perfil_w
	from	hd_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception
		when others then
		cd_perfil_w	:= 0;
	end;


	if 	(cd_perfil_w > 0) then

		if 	(:new.cd_pessoa_fisica is not null) then
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(793683),
				wheb_mensagem_pck.get_texto(793676)||' ' || :new.cd_pessoa_fisica ||' - '|| obter_nome_pf(:new.cd_pessoa_fisica) ||' '|| wheb_mensagem_pck.get_texto(793705) ||' '|| substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80) || '.'|| chr(10) || wheb_mensagem_pck.get_texto(793682)|| ' ' || substr(obter_dados_motivo_fim(:new.nr_seq_motivo_fim,'D'),1,255),
				:new.nm_usuario,
				sysdate,
				'N',
				'',
				cd_perfil_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_w);
		end if;
	end if;
end if;

if	(:new.dt_final_tratamento is not null) and (:old.dt_final_tratamento is null) and
	(:new.nr_seq_receptor is not null) and
	(:new.ie_tratamento in ('PT','TR','TX')) then

	cd_estabelecimento_w	:= tx_obter_estab_receptor(:new.nr_seq_receptor);

	begin

	select	max(cd_perfil_paciente_fim)
	into	cd_perfil_w
	from	tx_parametros
	where	cd_estabelecimento = nvl(cd_estabelecimento_w,cd_estabelecimento);
	exception
		when others then
		cd_perfil_w	:= 0;
	end;

	if 	(cd_perfil_w > 0) then

		if 	(:new.cd_pessoa_fisica is not null) then
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(793683),
				wheb_mensagem_pck.get_texto(793676)||' ' || :new.cd_pessoa_fisica ||' - '|| obter_nome_pf(:new.cd_pessoa_fisica) ||' '|| wheb_mensagem_pck.get_texto(793705) ||' '|| substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80) || '.'|| chr(10) || wheb_mensagem_pck.get_texto(793682)|| ' ' || substr(obter_dados_motivo_fim(:new.nr_seq_motivo_fim,'D'),1,255),
				:new.nm_usuario,
				sysdate,
				'N',
				'',
				cd_perfil_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_w);
		end if;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_tratamento_after_upd
after update ON TASY.PACIENTE_TRATAMENTO for each row
declare

begin

if (:new.dt_final_tratamento is not null) and (:old.dt_final_tratamento is null) then

	update	hd_protocolo_exame
	set	dt_fim = sysdate
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica
	and	dt_fim is null
	and	ie_tratamento = :new.ie_tratamento;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_tratamento_insert
before insert ON TASY.PACIENTE_TRATAMENTO for each row
declare
cd_perfil_w		number(5,0);
cd_funcao_w		number(5,0);
cd_estabelecimento_w	number(5,0);
cd_controle_w		number(1);
pragma autonomous_transaction;
begin

cd_funcao_w	:= obter_funcao_ativa;

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;

if	(:new.ie_tratamento in ('CAPD','CO','CPI','DPA','HD','PL','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','DPAV','DDC','IHF','IHDF')) or
	((cd_funcao_w = 7009) and (:new.ie_tratamento in ('PT','TR','TX'))) then


	if	(:new.nr_seq_unid_dialise is null)	 then
		:new.nr_seq_unid_dialise	:= HD_OBTER_UNIDADE_PRC(:new.cd_pessoa_fisica,'C');
		commit;
	end if;
	/*
	update 	hd_pac_renal_cronico
	set	NR_SEQ_UNIDADE_ATUAL 	= :new.nr_seq_unid_dialise
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	*/ -- Retirado e colocdo para ser tratado no afterPost do DBPanel.
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	begin
	select	cd_perfil_inicio_trat
	into	cd_perfil_w
	from	hd_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception
		when others then
		cd_perfil_w	:= 0;
	end;

	if 	(cd_perfil_w > 0) then

		if 	(:new.cd_pessoa_fisica is not null) then
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(793678)||' ' || substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80),
				wheb_mensagem_pck.get_texto(793676)||' ' || :new.cd_pessoa_fisica ||' - '|| obter_nome_pf(:new.cd_pessoa_fisica) ||' '|| wheb_mensagem_pck.get_texto(793679) ||' '|| substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80) ||'.'||chr(10)||
				wheb_mensagem_pck.get_texto(793677)||' ' || SUBSTR(HD_OBTER_DESC_UNID_DIALISE(:new.NR_SEQ_UNID_DIALISE),1,255)||'.',
				:new.nm_usuario,
				sysdate,
				'N',
				'',
				cd_perfil_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_w);
			commit;
		end if;
	end if;
end if;


if	(:new.nr_seq_receptor is not null) and
	(:new.ie_tratamento in ('PT','TR','TX')) then

	cd_estabelecimento_w	:= tx_obter_estab_receptor(:new.nr_seq_receptor);

	begin
	select	cd_perfil_inicio_trat
	into	cd_perfil_w
	from	hd_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception
		when others then
		cd_perfil_w	:= 0;
	end;
	if 	(cd_perfil_w > 0) then
		if 	(:new.cd_pessoa_fisica is not null) then
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(793678)||' ' || substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80),
				wheb_mensagem_pck.get_texto(793676)||' ' || :new.cd_pessoa_fisica ||' - '|| obter_nome_pf(:new.cd_pessoa_fisica) ||' '|| wheb_mensagem_pck.get_texto(793679) ||' '|| substr(obter_valor_dominio(2197,:new.IE_TRATAMENTO),1,80) || '.',
				:new.nm_usuario,
				sysdate,
				'N',
				'',
				cd_perfil_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_w);
			commit;
		end if;
	end if;
end if;

<<Final>>
cd_controle_w := 0;
commit;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_TRATAMENTO_tp  after update ON TASY.PACIENTE_TRATAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TRATAMENTO,1,4000),substr(:new.IE_TRATAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRATAMENTO',ie_log_w,ds_w,'PACIENTE_TRATAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACIENTE_TRATAMENTO ADD (
  CONSTRAINT PACTRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_TRATAMENTO ADD (
  CONSTRAINT PACTRAT_ATEPACI_FK 
 FOREIGN KEY (NR_ATEND_ORIGEM) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACTRAT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PACTRAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACTRAT_MOTIFIM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM) 
 REFERENCES TASY.MOTIVO_FIM (NR_SEQUENCIA),
  CONSTRAINT PACTRAT_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA),
  CONSTRAINT PACTRAT_CAUSAMO_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_MORTE) 
 REFERENCES TASY.CAUSA_MORTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_TRATAMENTO TO NIVEL_1;

