ALTER TABLE TASY.EXAME_LAB_EQUIP_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LAB_EQUIP_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LAB_EQUIP_REGRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_EXAME_EQUIP    NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO    NUMBER(5),
  IE_DIA_SEMANA         NUMBER(1),
  DS_HORA_INICIO        VARCHAR2(5 BYTE),
  DS_HORA_FIM           VARCHAR2(5 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  DT_HORA_FIM_HTML5     DATE,
  DT_HORA_INICIO_HTML5  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXALAER_ESTABEL_FK_I ON TASY.EXAME_LAB_EQUIP_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALAER_LABEXEQ_FK_I ON TASY.EXAME_LAB_EQUIP_REGRA
(NR_SEQ_EXAME_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALAER_LABEXEQ_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EXALAER_PK ON TASY.EXAME_LAB_EQUIP_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALAER_PK
  MONITORING USAGE;


CREATE INDEX TASY.EXALAER_SETATEN_FK_I ON TASY.EXAME_LAB_EQUIP_REGRA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALAER_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.LAB_EQUIP_REGRA_UPDATE_HTML

BEFORE INSERT OR UPDATE ON TASY.EXAME_LAB_EQUIP_REGRA 
FOR EACH ROW
DECLARE

cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;

nm_usuario_w      usuario.nm_usuario%type;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

if (:new.DT_HORA_INICIO_HTML5 is not null and (:new.DT_HORA_INICIO_HTML5 <> :old.DT_HORA_INICIO_HTML5 or :old.DT_HORA_INICIO_HTML5 is null)) then

 begin

  cd_estabelecimento_w  := wheb_usuario_pck.get_cd_estabelecimento;

  nm_usuario_w    := wheb_usuario_pck.get_nm_usuario;

  :new.DS_HORA_INICIO    := substr(to_char(:new.DT_HORA_INICIO_HTML5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5);

 end;

end if;

if (:new.DT_HORA_FIM_HTML5 is not null and (:new.DT_HORA_FIM_HTML5 <> :old.DT_HORA_FIM_HTML5 or :old.DT_HORA_FIM_HTML5 is null)) then

 begin

  cd_estabelecimento_w  := wheb_usuario_pck.get_cd_estabelecimento;

  nm_usuario_w    := wheb_usuario_pck.get_nm_usuario;

  :new.DS_HORA_FIM       := substr(to_char(:new.DT_HORA_FIM_HTML5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5);

 end;

end if;

end if;

END;
/


ALTER TABLE TASY.EXAME_LAB_EQUIP_REGRA ADD (
  CONSTRAINT EXALAER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXAME_LAB_EQUIP_REGRA ADD (
  CONSTRAINT EXALAER_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EXALAER_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EXALAER_LABEXEQ_FK 
 FOREIGN KEY (NR_SEQ_EXAME_EQUIP) 
 REFERENCES TASY.LAB_EXAME_EQUIP (NR_SEQUENCIA));

GRANT SELECT ON TASY.EXAME_LAB_EQUIP_REGRA TO NIVEL_1;

