ALTER TABLE TASY.OEC_VOUCHER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OEC_VOUCHER CASCADE CONSTRAINTS;

CREATE TABLE TASY.OEC_VOUCHER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ADMISSION         DATE,
  DT_BIRTH             DATE,
  NM_FAMILY            VARCHAR2(40 BYTE),
  NM_FIRST             VARCHAR2(40 BYTE),
  IE_GENDER            VARCHAR2(1 BYTE),
  IE_COMPENSATION      VARCHAR2(1 BYTE),
  DT_DISCHARGE         DATE,
  CD_FUND_PAYMENT      VARCHAR2(12 BYTE),
  NM_ALIAS_FAMILY      VARCHAR2(40 BYTE),
  NM_ALIAS_FIRST       VARCHAR2(40 BYTE),
  CD_FUND_CARD         VARCHAR2(19 BYTE),
  CD_UPI               NUMBER(2),
  CD_MEDICARE_CARD     NUMBER(10),
  NR_PATIENT_REF       NUMBER(2),
  NM_SECOND            VARCHAR2(1 BYTE),
  CD_PROVIDER          VARCHAR2(8 BYTE),
  NR_SEQ_CLAIM         NUMBER(10)               NOT NULL,
  NR_VOUCHER           VARCHAR2(4 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OECV_OEC_FK_I ON TASY.OEC_VOUCHER
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OECV_PK ON TASY.OEC_VOUCHER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OEC_VOUCHER ADD (
  CONSTRAINT OECV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OEC_VOUCHER ADD (
  CONSTRAINT OECV_OEC_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.OEC_CLAIM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OEC_VOUCHER TO NIVEL_1;

