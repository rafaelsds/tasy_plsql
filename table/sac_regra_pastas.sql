ALTER TABLE TASY.SAC_REGRA_PASTAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAC_REGRA_PASTAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAC_REGRA_PASTAS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10),
  IE_PASTA             VARCHAR2(2 BYTE)         NOT NULL,
  IE_SITUACAO_PASTA    VARCHAR2(2 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SACREGPA_PK ON TASY.SAC_REGRA_PASTAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SACREGPA_PK
  MONITORING USAGE;


CREATE INDEX TASY.SACREGPA_SACREGST_FK_I ON TASY.SAC_REGRA_PASTAS
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SACREGPA_SACREGST_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAC_REGRA_PASTAS ADD (
  CONSTRAINT SACREGPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAC_REGRA_PASTAS ADD (
  CONSTRAINT SACREGPA_SACREGST_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.SAC_REGRA_STATUS (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAC_REGRA_PASTAS TO NIVEL_1;

