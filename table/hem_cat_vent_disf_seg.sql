ALTER TABLE TASY.HEM_CAT_VENT_DISF_SEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_CAT_VENT_DISF_SEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_CAT_VENT_DISF_SEG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_VENTR         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_SEGMENTO      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_CONTRAT_SEG       VARCHAR2(1 BYTE),
  IE_CINESIA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMCVDS_HEMCAVE_FK_I ON TASY.HEM_CAT_VENT_DISF_SEG
(NR_SEQ_VENTR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMCVDS_HEMCAVE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HEMCVDS_HEMSEVE_FK_I ON TASY.HEM_CAT_VENT_DISF_SEG
(NR_SEQ_SEGMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMCVDS_HEMSEVE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HEMCVDS_PK ON TASY.HEM_CAT_VENT_DISF_SEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMCVDS_PK
  MONITORING USAGE;


ALTER TABLE TASY.HEM_CAT_VENT_DISF_SEG ADD (
  CONSTRAINT HEMCVDS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_CAT_VENT_DISF_SEG ADD (
  CONSTRAINT HEMCVDS_HEMSEVE_FK 
 FOREIGN KEY (NR_SEQ_SEGMENTO) 
 REFERENCES TASY.HEM_SEG_VENTR (NR_SEQUENCIA),
  CONSTRAINT HEMCVDS_HEMCAVE_FK 
 FOREIGN KEY (NR_SEQ_VENTR) 
 REFERENCES TASY.HEM_CAT_VENTRIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_CAT_VENT_DISF_SEG TO NIVEL_1;

