ALTER TABLE TASY.PLS_OCORRENCIA_CONTA_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCORRENCIA_CONTA_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCORRENCIA_CONTA_ATRIB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_OCORRENCIA       NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_TIPO_OCORRENCIA      VARCHAR2(3 BYTE)      NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA         DATE,
  IE_ATRIBUTO             NUMBER(10),
  IE_OBRIGATORIO          VARCHAR2(1 BYTE),
  IE_QTD_PARTO            VARCHAR2(1 BYTE),
  DS_VALOR                VARCHAR2(120 BYTE),
  IE_OBITO_MULHER         VARCHAR2(1 BYTE),
  QT_MINIMO_CARACTER      NUMBER(10),
  IE_TIPO_GUIA            VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  DS_REGRA                VARCHAR2(255 BYTE),
  IE_SOMENTE_NUMERO       VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  CD_ESTABELECIMENTO      NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCCA_ESTABEL_FK_I ON TASY.PLS_OCORRENCIA_CONTA_ATRIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCCA_PK ON TASY.PLS_OCORRENCIA_CONTA_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCCA_PLSOCOR_FK_I ON TASY.PLS_OCORRENCIA_CONTA_ATRIB
(NR_SEQ_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCA_PLSOCOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCCA_PROCEDI_FK_I ON TASY.PLS_OCORRENCIA_CONTA_ATRIB
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_CONTA_ATRIB_tp  after update ON TASY.PLS_OCORRENCIA_CONTA_ATRIB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_REGRA,1,500);gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_NUMERO,1,4000),substr(:new.IE_SOMENTE_NUMERO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_NUMERO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OCORRENCIA,1,4000),substr(:new.NR_SEQ_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OCORRENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATRIBUTO,1,4000),substr(:new.IE_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATRIBUTO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VALOR,1,4000),substr(:new.DS_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALOR',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBITO_MULHER,1,4000),substr(:new.IE_OBITO_MULHER,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBITO_MULHER',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_OCORRENCIA,1,4000),substr(:new.IE_TIPO_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_OCORRENCIA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QTD_PARTO,1,4000),substr(:new.IE_QTD_PARTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_QTD_PARTO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_CARACTER,1,4000),substr(:new.QT_MINIMO_CARACTER,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_CARACTER',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA_REF,1,4000),substr(:new.DT_INICIO_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA_REF,1,4000),substr(:new.DT_FIM_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_OCORRENCIA_CONTA_ATRIB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_ocor_conta_atrib_atual
before insert or update ON TASY.PLS_OCORRENCIA_CONTA_ATRIB for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo
:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref    := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_ocor_conta_atrib_atual;
/


ALTER TABLE TASY.PLS_OCORRENCIA_CONTA_ATRIB ADD (
  CONSTRAINT PLSOCCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCORRENCIA_CONTA_ATRIB ADD (
  CONSTRAINT PLSOCCA_PLSOCOR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA) 
 REFERENCES TASY.PLS_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSOCCA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSOCCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_OCORRENCIA_CONTA_ATRIB TO NIVEL_1;

