ALTER TABLE TASY.PLS_SEGURADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SEGURADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SEGURADO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE) NOT NULL,
  NR_SEQ_CONTRATO               NUMBER(10),
  NR_SEQ_PARENTESCO             NUMBER(10),
  NR_SEQ_TITULAR                NUMBER(10),
  NR_SEQ_PAGADOR                NUMBER(10),
  NR_SEQ_PLANO                  NUMBER(10),
  NR_SEQ_TABELA                 NUMBER(10),
  DT_CONTRATACAO                DATE,
  NR_PROPOSTA_ADESAO            VARCHAR2(20 BYTE),
  DT_LIBERACAO                  DATE,
  DT_RESCISAO                   DATE,
  NR_SEQ_SEGURADO_ANT           NUMBER(10),
  DT_MIGRACAO                   DATE,
  DS_OBSERVACAO                 VARCHAR2(2000 BYTE),
  CD_SUBESTIPULANTE_PF          VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_CANCELAMENTO    NUMBER(10),
  DT_REATIVACAO                 DATE,
  VL_ANALISE                    NUMBER(15,2),
  CD_SEGURADO_CONTRATO          VARCHAR2(20 BYTE),
  CD_SEGURADO_FAMILIA           VARCHAR2(20 BYTE),
  DT_INCLUSAO_OPERADORA         DATE,
  DT_LIMITE_UTILIZACAO          DATE,
  NR_SEQ_VINCULO_EST            NUMBER(10),
  CD_MATRICULA_ESTIPULANTE      VARCHAR2(30 BYTE),
  NR_SEQ_TABELA_ORIGEM          NUMBER(10),
  NR_SEQ_MOTIVO_INCLUSAO        NUMBER(10),
  NR_SEQ_SEGURADO_MIG           NUMBER(10),
  NR_SEQ_VENDEDOR_PF            NUMBER(10),
  NR_SEQ_VENDEDOR_CANAL         NUMBER(10),
  IE_ACAO_CONTRATO              VARCHAR2(2 BYTE),
  IE_TIPO_PLANO                 VARCHAR2(3 BYTE),
  DT_ULTIMA_CARENCIA            DATE,
  IE_TAXA_INSCRICAO             VARCHAR2(1 BYTE),
  DT_CANCELAMENTO               DATE,
  IE_TIPO_SEGURADO              VARCHAR2(3 BYTE),
  NR_SEQ_PESSOA_PROPOSTA        NUMBER(10),
  NR_SEQ_CONTRATANTE_PRINC      NUMBER(10),
  NR_SEQ_VINCULO                NUMBER(10),
  CD_COD_ANTERIOR               VARCHAR2(20 BYTE),
  NR_SEQ_SEG_CONTRATO           NUMBER(10),
  IE_TIPO_VALOR                 VARCHAR2(3 BYTE),
  NR_SEQ_NOTIFIC_PAGADOR        NUMBER(10),
  CD_SUBESTIPULANTE_PJ          VARCHAR2(14 BYTE),
  NR_SEQ_PORTABILIDADE          NUMBER(10),
  NR_SEQ_SUBESTIPULANTE         NUMBER(10),
  NR_CCO                        NUMBER(10),
  IE_DIGITO_CCO                 NUMBER(2),
  IE_REENVIAR_SIB               VARCHAR2(2 BYTE),
  IE_TIPO_REG_REENVIO           NUMBER(10),
  NR_SEQ_MOTIVO_REENVIO         NUMBER(10),
  IE_OPCAO_SIB                  NUMBER(10),
  IE_CARTEIRA_ANT_SIB           VARCHAR2(2 BYTE),
  IE_NASCIDO_PLANO              VARCHAR2(1 BYTE),
  NR_SEQ_CONGENERE              NUMBER(10),
  IE_SITUACAO_ATEND             VARCHAR2(1 BYTE) NOT NULL,
  IE_BONUS_INDICACAO            VARCHAR2(1 BYTE),
  CD_MATRICULA_FAMILIA          NUMBER(10),
  NR_SEQ_MOTIVO_SUSP            NUMBER(10),
  NR_SEQ_NOTIFIC_SUSP           NUMBER(10),
  NR_SEQ_INTERCAMBIO            NUMBER(10),
  CD_UNIMED                     NUMBER(4),
  IE_TIPO_REPASSE               VARCHAR2(1 BYTE),
  IE_TIPO_RESCISAO              VARCHAR2(2 BYTE),
  NR_SEQ_CONTRATO_ANT           NUMBER(10),
  NR_SEQ_INTERCAMBIO_BENEF      NUMBER(10),
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  IE_IMPORTACAO_BASE            VARCHAR2(1 BYTE),
  IE_TIPO_VINCULO_OPERADORA     VARCHAR2(2 BYTE),
  IE_MENSALIDADE_PROPORCIONAL   VARCHAR2(1 BYTE),
  IE_PCMSO                      VARCHAR2(1 BYTE),
  DT_REAJUSTE                   DATE,
  DT_SOLICITACAO_INCLUSAO       DATE,
  CD_CARTAO_INTERCAMBIO         VARCHAR2(30 BYTE),
  CD_APOLICE_SEGURO_OBITO       VARCHAR2(60 BYTE),
  IE_CUSTO_OPER_INTERCAMBIO     VARCHAR2(1 BYTE),
  IE_CONTROLA_DT_DEPENDENCIA    VARCHAR2(1 BYTE),
  CD_FAMILIA_EMPRESA            NUMBER(10),
  CD_OPERADORA_EMPRESA          NUMBER(10),
  IE_SITUACAO_TRABALHISTA       VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_ACOMODACAO        NUMBER(10),
  IE_ABRANGENCIA                VARCHAR2(3 BYTE),
  NR_SEQ_SEGURADO_OBITO         NUMBER(10),
  NR_CONTRATO_ANT               NUMBER(10),
  NR_CONTRATO_MIGRADO           NUMBER(10),
  NR_SEQ_ANALISE_ADESAO         NUMBER(10),
  IE_ABRANGENCIA_INTERCAMBIO    VARCHAR2(2 BYTE),
  IE_TITULARIDADE               VARCHAR2(5 BYTE),
  IE_RENOVACAO_CARTEIRA         VARCHAR2(1 BYTE),
  IE_BONIFIC_COOPERADO          VARCHAR2(1 BYTE),
  NR_SEQ_TABELA_REPASSE         NUMBER(10),
  IE_LOCAL_CADASTRO             NUMBER(2),
  NR_SEQ_GRUPO_INTERCAMBIO      NUMBER(10),
  NR_SEQ_LOCALIZACAO_BENEF      NUMBER(10),
  CD_LOTACAO_INTERC             VARCHAR2(10 BYTE),
  NR_SEQ_BENEF_PROP_ADESAO      NUMBER(10),
  NR_SEQ_BENEF_INCLUSAO         NUMBER(10),
  NR_SEQ_TIPO_COMERCIAL         NUMBER(10),
  IE_TIPO_PARENTESCO            VARCHAR2(3 BYTE),
  IE_DEPENDENTE_A100            VARCHAR2(1 BYTE),
  NR_SEQUENCIAL_EMPRESA         NUMBER(10),
  NR_SEQ_CAUSA_RESCISAO         NUMBER(10),
  NR_SEQ_SUSPENSAO              NUMBER(10),
  NR_SEQ_OPS_CONGENERE          NUMBER(10),
  CD_CARTAO_IDENT_ANS_SIST_ANT  VARCHAR2(30 BYTE),
  IE_DIVISAO_REPASSE            VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_COOP             NUMBER(10),
  CD_CARTEIRA_ANT               VARCHAR2(30 BYTE),
  NR_SEQ_AGENTE_MOTIVADOR       NUMBER(10),
  NR_SEQ_ORIGEM_AGENTE          NUMBER(10),
  NR_SEQ_CLASSIF_DEPENDENCIA    NUMBER(10),
  NR_SEQ_FAIXA_SALARIAL         NUMBER(10),
  NR_SEQ_CLASSIFICACAO          NUMBER(10),
  QT_ANOS_LIMITE_REAJ           NUMBER(3),
  QT_IDADE_LIMITE_REAJ          NUMBER(3),
  NR_SEQ_CONGENERE_OK           NUMBER(10),
  NR_SEQ_SIB_LOG_EXCLUSAO       NUMBER(10),
  DT_INCLUSAO_PCE               DATE,
  QT_DIAS_REATIVACAO            NUMBER(5),
  IE_TIPO_FLEX                  VARCHAR2(1 BYTE),
  IE_RESCISAO_MIGRACAO          VARCHAR2(1 BYTE),
  CD_CCO                        VARCHAR2(12 BYTE),
  NR_MES_REAJUSTE               NUMBER(2),
  DT_RESCISAO_ANT               DATE,
  DT_REATIVACAO_ANT             DATE,
  DT_COMP_RISCO                 DATE            DEFAULT null,
  DT_RECEBIMENTO                DATE,
  DT_ALTERACAO_TIPO_SEGURADO    DATE            DEFAULT null,
  NR_SEQ_PROP_BENEF_ONLINE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSEGU_ESTABEL_FK_I ON TASY.PLS_SEGURADO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_GRAUPA_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_GRAUPA_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_VINCULO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_GRAUPA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_I1 ON TASY.PLS_SEGURADO
(DT_INCLUSAO_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_I10 ON TASY.PLS_SEGURADO
(CD_COD_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I11 ON TASY.PLS_SEGURADO
(CD_CCO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I12 ON TASY.PLS_SEGURADO
(NR_PROPOSTA_ADESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I15 ON TASY.PLS_SEGURADO
(IE_TIPO_SEGURADO, NVL("DT_CONTRATACAO",TO_DATE(' 1500-01-01 00:00:00', 'syyyy-mm-dd hh24:mi:ss')), NVL("DT_RESCISAO",TO_DATE(' 1500-01-01 00:00:00', 'syyyy-mm-dd hh24:mi:ss')))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I2 ON TASY.PLS_SEGURADO
(NR_CCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I20 ON TASY.PLS_SEGURADO
(NR_SEQ_PESSOA_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I3 ON TASY.PLS_SEGURADO
(CD_OPERADORA_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_I3
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_I4 ON TASY.PLS_SEGURADO
(CD_MATRICULA_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_I4
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_I5 ON TASY.PLS_SEGURADO
(CD_CARTAO_IDENT_ANS_SIST_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_I5
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_I6 ON TASY.PLS_SEGURADO
(NR_SEQ_SEGURADO_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I7 ON TASY.PLS_SEGURADO
(CD_CARTAO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I8 ON TASY.PLS_SEGURADO
(IE_TIPO_SEGURADO, DT_CONTRATACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_I9 ON TASY.PLS_SEGURADO
(NR_SEQ_INTERCAMBIO, IE_TIPO_REPASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PESFISI_FK_I ON TASY.PLS_SEGURADO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PESFISI_FK2_I ON TASY.PLS_SEGURADO
(CD_SUBESTIPULANTE_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PESJURI_FK_I ON TASY.PLS_SEGURADO
(CD_SUBESTIPULANTE_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSSEGU_PK ON TASY.PLS_SEGURADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLCONPAG_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSAADE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_ANALISE_ADESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSAADE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSAGMO_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_AGENTE_MOTIVADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSAGMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSCARS_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_CAUSA_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSCARS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSCFDP_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_CLASSIF_DEPENDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSCLBE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSCONG_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSCONG_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_OPS_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSCONG_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSCONT_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSFXSL_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_FAIXA_SALARIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSGCOP_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_GRUPO_COOP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSGCOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSINBE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_BENEF_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSINBE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSINCA_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSLOBE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_LOCALIZACAO_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSLOBE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSMOCA_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSMOISE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_MOTIVO_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSMOISE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSMOTSUSE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSMOTSUSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSNOPA_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_NOTIFIC_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSNOPA_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_NOTIFIC_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSPBON_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_PROP_BENEF_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSPLAN_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSPOPE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_PORTABILIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSPOPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSRGIN_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_GRUPO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSRGIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSSEGSU_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_SUSPENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSSEGSU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSSEGU_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSSEGU_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_SEGURADO_MIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSSEGU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSSEGU_FK3_I ON TASY.PLS_SEGURADO
(NR_SEQ_CONTRATANTE_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSSEGU_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSSEGU_FK4_I ON TASY.PLS_SEGURADO
(IE_TIPO_SEGURADO, DT_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSSEGU_FK5_I ON TASY.PLS_SEGURADO
(IE_TIPO_SEGURADO, DT_LIBERACAO, DT_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PLSSEGU_PLSSUES_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_SUBESTIPULANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSTAPR_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSTAPR_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_TABELA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSTAPR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSTAPR_FK3_I ON TASY.PLS_SEGURADO
(NR_SEQ_TABELA_REPASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSTAPR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSTIAC_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSTIBC_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_TIPO_COMERCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PLSTIBC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEGU_PLSVEND_FK2_I ON TASY.PLS_SEGURADO
(NR_SEQ_VENDEDOR_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSVEVI_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_VENDEDOR_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PLSVIES_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_VINCULO_EST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEGU_PTUINBE_FK_I ON TASY.PLS_SEGURADO
(NR_SEQ_INTERCAMBIO_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEGU_PTUINBE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_segurado_afterpost
after insert or update ON TASY.PLS_SEGURADO for each row
declare

nr_seq_participante_w	mprev_participante.nr_sequencia%type;
ie_situacao_partic_w	mprev_participante.ie_situacao%type	:= null;
ds_comunicado_w		varchar2(4000);
ds_lista_usuario_w	varchar2(4000);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_captacao_w	mprev_captacao.nr_sequencia%type;
nr_seq_seg_capt_w	pls_segurado.nr_sequencia%type;
nr_seq_busca_emp_w	mprev_busca_empresarial.nr_sequencia%type;
nr_seq_demanda_espont_w mprev_demanda_espont.nr_sequencia%type;
nr_seq_indicacao_w	mprev_indicacao_paciente.nr_sequencia%type;
nm_usuario_w		usuario.nm_usuario%type;
dt_ocorrencia_w		date;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	nm_usuario_w	:= nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario);

	if	(:old.ie_tipo_segurado = 'I' or :old.ie_tipo_segurado is null) and
		(:new.dt_comp_risco is not null) then --Se era eventual e agora foi assumido como repasse
		dt_ocorrencia_w	:= trunc(:new.dt_comp_risco,'dd');
	else
		dt_ocorrencia_w	:= trunc(sysdate,'dd');
	end if;

	--Alteração do tipo de beneficiário
	if	(updating) and
		(:new.ie_tipo_segurado <> :old.ie_tipo_segurado) then
		insert into pls_segurado_historico
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_segurado,
			dt_historico, ds_historico, ds_observacao,
			ie_tipo_historico, dt_ocorrencia_sib, ie_historico_situacao,
			dt_liberacao_hist, ie_envio_sib, ie_tipo_segurado,
			ie_tipo_segurado_ant, ie_situacao_compartilhamento)
		values(	pls_segurado_historico_seq.nextval, sysdate, nm_usuario_w,
			sysdate, nm_usuario_w, :new.nr_sequencia,
			sysdate, 'Tipo de beneficiário alterado de "'||obter_valor_dominio(2406,:old.ie_tipo_segurado)||'" para "'||obter_valor_dominio(2406,:new.ie_tipo_segurado)||'"', '',
			'102', dt_ocorrencia_w, 'S',
			sysdate, 'N', :new.ie_tipo_segurado,
			:old.ie_tipo_segurado, 'A');

		pls_inativar_historico_compart(:new.nr_sequencia, dt_ocorrencia_w, nm_usuario_w, 'N');
	elsif	((inserting) and
		(:new.ie_tipo_segurado = 'H')) then
		insert into pls_segurado_historico
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_segurado,
			dt_historico, ds_historico, ds_observacao,
			ie_tipo_historico, dt_ocorrencia_sib, ie_historico_situacao,
			dt_liberacao_hist, ie_envio_sib, ie_tipo_segurado,
			ie_tipo_segurado_ant, ie_situacao_compartilhamento)
		values(	pls_segurado_historico_seq.nextval, sysdate, nm_usuario_w,
			sysdate, nm_usuario_w, :new.nr_sequencia,
			sysdate, 'Tipo de beneficiário alterado de "'||obter_valor_dominio(2406,'I')||'" para "'||obter_valor_dominio(2406,:new.ie_tipo_segurado)||'"', '',
			'102', dt_ocorrencia_w, 'S',
			sysdate, 'N', :new.ie_tipo_segurado,
			'I', 'A');

		pls_inativar_historico_compart(:new.nr_sequencia, dt_ocorrencia_w, nm_usuario_w, 'N');
	end if;

	--Quando a situação de atendimento do beneficiário ficou Inativo
	if	(:new.ie_situacao_atend = 'I') and
		(nvl(:old.ie_situacao_atend,'X') <> 'I') then
		ie_situacao_partic_w	:= 'I';
	--Quando a situação de atendimento do beneficiário ficou Suspenso
	elsif	(:new.ie_situacao_atend = 'S') and
		(nvl(:old.ie_situacao_atend,'X') <> 'S') then
		ie_situacao_partic_w	:= 'S';
	end if;

	if	(updating) and
		(:old.nr_seq_segurado_ant is not null) and
		(:old.dt_migracao is not null) and
		(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then

		select	max(a.nr_sequencia)
		into	nr_seq_captacao_w
		from	mprev_captacao a
		where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
		and	a.ie_status = 'A' -- Aceito
		and	exists(	select	1
				from	mprev_participante x
				where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);

		if	(nr_seq_captacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_captacao_w
			from	mprev_captacao a
			where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	a.ie_status = 'T' -- Triagem
			and	exists(	select	1
					from	mprev_participante x
					where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);
		end if;

		if	(nr_seq_captacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_captacao_w
			from	mprev_captacao a
			where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	a.ie_status = 'P' -- Pendente
			and	exists(	select	1
					from	mprev_participante x
					where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);
		end if;

		if	(nr_seq_captacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_captacao_w
			from	mprev_captacao a
			where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	a.ie_status = 'N' -- Negada
			and	exists(	select	1
					from	mprev_participante x
					where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);
		end if;

		if	(nr_seq_captacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_captacao_w
			from	mprev_captacao a
			where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	a.ie_status = 'R' -- Regeitada
			and	exists(	select	1
					from	mprev_participante x
					where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);
		end if;

		if	(nr_seq_captacao_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_captacao_w
			from	mprev_captacao a
			where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
			and	a.ie_status = 'C' -- Cancelada
			and	exists(	select	1
					from	mprev_participante x
					where	x.cd_pessoa_fisica = a.cd_pessoa_fisica);
		end if;

		if	(nr_seq_captacao_w is not null) then
			select	max(nr_seq_busca_emp),
				max(nr_seq_demanda_espont),
				max(nr_seq_indicacao)
			into	nr_seq_busca_emp_w,
				nr_seq_demanda_espont_w,
				nr_seq_indicacao_w
			from	mprev_captacao
			where	nr_sequencia = nr_seq_captacao_w;

			if	(nr_seq_busca_emp_w is not null) then
				update	mprev_busca_empresarial
				set	nr_seq_segurado = :new.nr_sequencia,
					nr_seq_segurado_ant = :new.nr_seq_segurado_ant,
					dt_atualizacao = sysdate,
					nm_usuario = :new.nm_usuario
				where	nr_sequencia = nr_seq_busca_emp_w
				and	nr_seq_segurado = :new.nr_seq_segurado_ant;
			elsif	(nr_seq_demanda_espont_w is not null) then
				update	mprev_demanda_espont
				set	nr_seq_segurado = :new.nr_sequencia,
					nr_seq_segurado_ant = :new.nr_seq_segurado_ant,
					dt_atualizacao = sysdate,
					nm_usuario = :new.nm_usuario
				where	nr_sequencia = nr_seq_demanda_espont_w
				and	nr_seq_segurado = :new.nr_seq_segurado_ant;
			elsif	(nr_seq_indicacao_w is not null) then
				update	mprev_indicacao_paciente
				set	nr_seq_segurado = :new.nr_sequencia,
					nr_seq_segurado_ant = :new.nr_seq_segurado_ant,
					dt_atualizacao = sysdate,
					nm_usuario = :new.nm_usuario
				where	nr_sequencia = nr_seq_indicacao_w
				and	nr_seq_segurado = :new.nr_seq_segurado_ant;
			end if;
		end if;
	else
		/* Se mudou a situação, procurar qual o participante e alterar no mesmo */
		if	(ie_situacao_partic_w is not null) and (:new.nr_seq_segurado_mig is null) then
			if	(pkg_date_utils.start_of(:new.dt_rescisao,'DAY') <= sysdate) then
				cd_estabelecimento_w	:= obter_estabelecimento_ativo;

				mprev_tratar_exclusao_part(	:new.nr_sequencia,
								:new.cd_pessoa_fisica,
								null,
								'R',
								:new.dt_rescisao,
								null,
								ie_situacao_partic_w,
								cd_estabelecimento_w,
								:new.nm_usuario);
			end if;
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_seguradobeforepost
before insert or update ON TASY.PLS_SEGURADO for each row
declare

ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;
ie_participacao_w		pls_plano.ie_participacao%type;
ie_tipo_plano_w			pls_segurado.ie_tipo_plano%type;
nr_seq_seg_contrato_w		pls_segurado.nr_seq_seg_contrato%type;
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
qt_reg_w			pls_integer;
ds_erro_w			varchar2(255);
nr_seq_segurado_status_w	pls_segurado_status.nr_sequencia%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.nr_seq_congenere_ok := nvl(:new.nr_seq_ops_congenere,:new.nr_seq_congenere);

if	(trunc(:new.dt_inclusao_operadora,'dd') > trunc(:new.dt_contratacao,'dd')) then
	if	(:new.cd_pessoa_fisica is not null) then
		select	nm_pessoa_fisica
		into	nm_pessoa_fisica_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;
	end if;

	wheb_mensagem_pck.exibir_mensagem_abort(309308,'BENEFICIARIO='||:new.nr_sequencia||' - '||nm_pessoa_fisica_w);
end if;

if	(:new.nr_seq_plano <> :old.nr_seq_plano) then
	select	ie_tipo_contratacao,
		ie_participacao
	into	ie_tipo_contratacao_w,
		ie_participacao_w
	from	pls_plano
	where	nr_sequencia	= :new.nr_seq_plano;

	if	(ie_tipo_contratacao_w	= 'I') then
		ie_tipo_plano_w	:= 'IF';
	elsif	(ie_participacao_w	= 'C') then
		ie_tipo_plano_w	:= 'CCP';
	elsif	(ie_participacao_w	= 'S') then
		ie_tipo_plano_w	:= 'CSP';
	end if;
	:new.ie_tipo_plano	:= ie_tipo_plano_w;
end if;

/*Tratamento para nao verificar essa coluna quando realizar carga dos beneficiarios*/
if	(pls_usuario_pck.get_ie_importacao_benef = 'N') then
	/* Tratar para nao duplicar o codigo interno do beneficiario */
	begin
	select	count(1)
	into	qt_reg_w
	from	pls_segurado a
	where	a.nr_seq_seg_contrato	= :new.nr_seq_seg_contrato
	and	a.nr_sequencia		<> :new.nr_sequencia
	and	a.nr_seq_contrato	= :new.nr_seq_contrato;

	if	(qt_reg_w > 0) then
		select	max(nr_seq_seg_contrato)
		into	nr_seq_seg_contrato_w
		from	pls_segurado
		where	nr_seq_contrato	= :new.nr_seq_contrato;

		:new.nr_seq_seg_contrato	:= nvl(nr_seq_seg_contrato_w,0) + 1;
	end if;
	exception
	when others then
		ds_erro_w	:= '';
	end;
end if;

--Atualizar o status do beneficiario
if	(:new.dt_rescisao is not null) and (:old.dt_rescisao is null) then --Rescisao

	pls_atualiza_segurado_status(:new.nr_sequencia, 2, :new.dt_rescisao, :new.nr_seq_motivo_cancelamento, :new.nr_seq_plano, null, :new.nm_usuario, :new.cd_estabelecimento,'N');

	:new.dt_reativacao_ant := :old.dt_reativacao;

	if	(:old.dt_rescisao_ant is not null) then
		:new.dt_rescisao_ant := null;
	end if;

elsif	(:new.dt_reativacao is not null) and (:old.dt_reativacao is null) then --Reativacao

	pls_atualiza_segurado_status(:new.nr_sequencia, 3, :new.dt_reativacao, null, :new.nr_seq_plano, null, :new.nm_usuario, :new.cd_estabelecimento,'N');

	:new.dt_rescisao_ant := :old.dt_rescisao;

	if	(:old.dt_reativacao_ant is not null) then
		:new.dt_reativacao_ant := null;
	end if;

elsif	(:new.dt_rescisao is null) and (:old.dt_rescisao is not null) then --Desfazer rescisao

	pls_atualiza_segurado_status(:new.nr_sequencia, 3, :old.dt_rescisao, null, :new.nr_seq_plano, null, :new.nm_usuario, :new.cd_estabelecimento,'N');

elsif	(:new.dt_contratacao is not null) and (:old.dt_contratacao is not null) and (:new.dt_contratacao <> :old.dt_contratacao) then --Alteracao da data de contratacao

	pls_alt_segurado_status( :new.nr_sequencia, 'I', :old.dt_contratacao, :new.dt_contratacao, :new.nm_usuario, 'N');

elsif	(:new.dt_reativacao is not null) and (:old.dt_reativacao is not null) and (:new.dt_reativacao <> :old.dt_reativacao) then --Alteracao da data de reativacao

	pls_alt_segurado_status( :new.nr_sequencia, 'I', :old.dt_reativacao, :new.dt_reativacao, :new.nm_usuario, 'N');

elsif	(:new.dt_rescisao is not null) and (:old.dt_rescisao is not null) and (:new.dt_rescisao <> :old.dt_rescisao) then --Alteracao da data de rescisao

	pls_alt_segurado_status( :new.nr_sequencia, 'F', :old.dt_rescisao, :new.dt_rescisao, :new.nm_usuario, 'N');

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PLS_SEGURADO ADD (
  CONSTRAINT PLSSEGU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SEGURADO ADD (
  CONSTRAINT PLSSEGU_PLSPBON_FK 
 FOREIGN KEY (NR_SEQ_PROP_BENEF_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_BENEF_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSAGMO_FK 
 FOREIGN KEY (NR_SEQ_AGENTE_MOTIVADOR) 
 REFERENCES TASY.PLS_AGENTE_MOTIVADOR (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSMOISE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INCLUSAO) 
 REFERENCES TASY.PLS_MOTIVO_INCLUSAO_SEG (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSCFDP_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_DEPENDENCIA) 
 REFERENCES TASY.PLS_CLASSIF_DEPENDENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSFXSL_FK 
 FOREIGN KEY (NR_SEQ_FAIXA_SALARIAL) 
 REFERENCES TASY.PLS_FAIXA_SALARIAL (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSCLBE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIFICACAO_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSRGIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_GRUPO_INTER (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSCARS_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_RESCISAO) 
 REFERENCES TASY.PLS_CAUSA_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSLOBE_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO_BENEF) 
 REFERENCES TASY.PLS_LOCALIZACAO_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSINBE_FK 
 FOREIGN KEY (NR_SEQ_BENEF_INCLUSAO) 
 REFERENCES TASY.PLS_INCLUSAO_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSTIBC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMERCIAL) 
 REFERENCES TASY.PLS_TIPO_BENEF_COMERCIAL (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSSEGSU_FK 
 FOREIGN KEY (NR_SEQ_SUSPENSAO) 
 REFERENCES TASY.PLS_SEGURADO_SUSPENSAO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_OPS_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSGCOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_COOP) 
 REFERENCES TASY.PLS_GRUPO_COOPERATIVA (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PESFISI_FK2 
 FOREIGN KEY (CD_SUBESTIPULANTE_PF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSSEGU_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_TITULAR) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSSEGU_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSSEGU_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSVIES_FK 
 FOREIGN KEY (NR_SEQ_VINCULO_EST) 
 REFERENCES TASY.PLS_VINCULO_ESTIPULANTE (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSTAPR_FK2 
 FOREIGN KEY (NR_SEQ_TABELA_ORIGEM) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSSEGU_FK2 
 FOREIGN KEY (NR_SEQ_SEGURADO_MIG) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSVEND_FK2 
 FOREIGN KEY (NR_SEQ_VENDEDOR_CANAL) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSVEVI_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_PF) 
 REFERENCES TASY.PLS_VENDEDOR_VINCULADO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSSEGU_FK3 
 FOREIGN KEY (NR_SEQ_CONTRATANTE_PRINC) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_GRAUPA_FK2 
 FOREIGN KEY (NR_SEQ_VINCULO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSNOPA_FK 
 FOREIGN KEY (NR_SEQ_NOTIFIC_PAGADOR) 
 REFERENCES TASY.PLS_NOTIFICACAO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PESJURI_FK 
 FOREIGN KEY (CD_SUBESTIPULANTE_PJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSSEGU_PLSPOPE_FK 
 FOREIGN KEY (NR_SEQ_PORTABILIDADE) 
 REFERENCES TASY.PLS_PORTAB_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSSUES_FK 
 FOREIGN KEY (NR_SEQ_SUBESTIPULANTE) 
 REFERENCES TASY.PLS_SUB_ESTIPULANTE (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSMOTSUSE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.PLS_MOTIVO_SUSP_SEG (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSNOPA_FK2 
 FOREIGN KEY (NR_SEQ_NOTIFIC_SUSP) 
 REFERENCES TASY.PLS_NOTIFICACAO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PTUINBE_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO_BENEF) 
 REFERENCES TASY.PTU_INTERCAMBIO_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSEGU_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSAADE_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_ADESAO) 
 REFERENCES TASY.PLS_ANALISE_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSSEGU_PLSTAPR_FK3 
 FOREIGN KEY (NR_SEQ_TABELA_REPASSE) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SEGURADO TO APPSSJ;

GRANT SELECT ON TASY.PLS_SEGURADO TO NIVEL_1;

GRANT SELECT ON TASY.PLS_SEGURADO TO ROBOCMTECNOLOGIA;

