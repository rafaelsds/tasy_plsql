ALTER TABLE TASY.TIPO_INTERC_QUIMIO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_INTERC_QUIMIO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_INTERC_QUIMIO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO          NUMBER(10),
  DS_SUB_ITEM          VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TIINQUI_PK ON TASY.TIPO_INTERC_QUIMIO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIINQUI_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIINQUI_TINPAAT_FK_I ON TASY.TIPO_INTERC_QUIMIO_ITEM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIINQUI_TINPAAT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TIPO_INTERC_QUIMIO_ITEM ADD (
  CONSTRAINT TIINQUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_INTERC_QUIMIO_ITEM ADD (
  CONSTRAINT TIINQUI_TINPAAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_INTERC_PAC_ATEND (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TIPO_INTERC_QUIMIO_ITEM TO NIVEL_1;

