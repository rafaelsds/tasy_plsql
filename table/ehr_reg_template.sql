ALTER TABLE TASY.EHR_REG_TEMPLATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_REG_TEMPLATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_REG_TEMPLATE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REG             NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE                   NOT NULL,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)      NOT NULL,
  DT_REGISTRO            DATE                   NOT NULL,
  NR_SEQ_TEMPLATE        NUMBER(10),
  DT_LIBERACAO           DATE,
  NR_SEQ_ITEM_PEP        NUMBER(10),
  NR_SEQ_APRES           NUMBER(5),
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DT_INATIVACAO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRRETE_EHRREGI_FK_I ON TASY.EHR_REG_TEMPLATE
(NR_SEQ_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRRETE_EHRTEMP_FK_I ON TASY.EHR_REG_TEMPLATE
(NR_SEQ_TEMPLATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRRETE_PK ON TASY.EHR_REG_TEMPLATE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHRRETE_PROITEM_FK_I ON TASY.EHR_REG_TEMPLATE
(NR_SEQ_ITEM_PEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EHR_REG_TEMPLATE_tp  after update ON TASY.EHR_REG_TEMPLATE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_TEMPLATE,1,4000),substr(:new.NR_SEQ_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TEMPLATE',ie_log_w,ds_w,'EHR_REG_TEMPLATE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ehr_reg_template_pend_atual
after insert or update ON TASY.EHR_REG_TEMPLATE for each row
declare

qt_reg_w					number(1);
ie_tipo_w					varchar2(10);
ie_liberar_ganho_perda_w	varchar2(10);
cd_paciente_w				varchar2(10);
nr_atendimento_w			number(10);
ie_gerar_assinatura_w		varchar2(1) := 'S';

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if (:new.dt_liberacao is null) then
	ie_tipo_w := 'EHR';
elsif (:old.dt_liberacao is null) and
	  (:new.dt_liberacao is not null) then
	ie_tipo_w := 'XEHR';
end if;

if (ie_tipo_w is not null) then
	if (obter_funcao_ativa = 10026) then
		ie_gerar_assinatura_w := 'S';
	elsif (obter_funcao_ativa = 874) then
		ie_gerar_assinatura_w := 'N';
	end if;

	if (ie_gerar_assinatura_w = 'S') then
		select	cd_paciente,
				nr_atendimento
		into	cd_paciente_w,
				nr_atendimento_w
		from	ehr_registro
		where	nr_sequencia = :new.NR_SEQ_REG;

		Gerar_registro_pendente_PEP(ie_tipo_w, :new.NR_SEQ_REG, cd_paciente_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w := 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.EHR_REG_TEMPLATE_UPDATE
after update ON TASY.EHR_REG_TEMPLATE for each row
declare

begin

if	(:new.NR_SEQ_TEMPLATE	<> :old.NR_SEQ_TEMPLATE) then

	begin

	delete	ehr_reg_elemento
	where	nr_seq_reg_template	= :new.nr_sequencia;

	exception
		when others then
		null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ehr_reg_template_delete
before delete ON TASY.EHR_REG_TEMPLATE for each row
declare
qt_reg_w					number(1);

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

<<Final>>
qt_reg_w := 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ehr_reg_template_atual
after insert or update ON TASY.EHR_REG_TEMPLATE for each row
declare

begin

if ((:old.nr_seq_template <> :new.nr_seq_template) or (inserting)) then
	update	ehr_registro
	set	nr_seq_templ = :new.nr_seq_template
	where	nr_sequencia = :new.nr_seq_reg;
end if;

end;
/


ALTER TABLE TASY.EHR_REG_TEMPLATE ADD (
  CONSTRAINT EHRRETE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_REG_TEMPLATE ADD (
  CONSTRAINT EHRRETE_PROITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PEP) 
 REFERENCES TASY.PRONTUARIO_ITEM (NR_SEQUENCIA),
  CONSTRAINT EHRRETE_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_REG) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHRRETE_EHRTEMP_FK 
 FOREIGN KEY (NR_SEQ_TEMPLATE) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA));

GRANT SELECT ON TASY.EHR_REG_TEMPLATE TO NIVEL_1;

