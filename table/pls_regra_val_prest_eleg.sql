ALTER TABLE TASY.PLS_REGRA_VAL_PREST_ELEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_VAL_PREST_ELEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_VAL_PREST_ELEG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_ELEG    NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_USUARIO_WEB   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRVPE_PK ON TASY.PLS_REGRA_VAL_PREST_ELEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRVPE_PLSRVAE_FK_I ON TASY.PLS_REGRA_VAL_PREST_ELEG
(NR_SEQ_REGRA_ELEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRVPE_PLSUSWE_FK_I ON TASY.PLS_REGRA_VAL_PREST_ELEG
(NR_SEQ_USUARIO_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_VAL_PREST_ELEG ADD (
  CONSTRAINT PLSRVPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_VAL_PREST_ELEG ADD (
  CONSTRAINT PLSRVPE_PLSRVAE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ELEG) 
 REFERENCES TASY.PLS_REGRA_VAL_ELEG (NR_SEQUENCIA),
  CONSTRAINT PLSRVPE_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_USUARIO_WEB) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_VAL_PREST_ELEG TO NIVEL_1;

