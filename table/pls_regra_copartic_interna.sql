ALTER TABLE TASY.PLS_REGRA_COPARTIC_INTERNA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_INTERNA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_INTERNA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CLINICA       NUMBER(10),
  QT_DIARIA_INICIAL    NUMBER(10),
  QT_DIARIA_FINAL      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRCPI_PK ON TASY.PLS_REGRA_COPARTIC_INTERNA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCPI_PLSCLIN_FK_I ON TASY.PLS_REGRA_COPARTIC_INTERNA
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCPI_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_INTERNA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copartic_interna_upd
before update ON TASY.PLS_REGRA_COPARTIC_INTERNA for each row
declare

begin
if	(:new.nr_seq_clinica <> :old.nr_seq_clinica) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_clinica, :new.nr_seq_clinica, 'PLS_REGRA_COPARTIC_INTERNA', 'NR_SEQ_CLINICA', :new.nm_usuario);
end if;
if	(:new.qt_diaria_inicial <> :old.qt_diaria_inicial) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_diaria_inicial, :new.qt_diaria_inicial, 'PLS_REGRA_COPARTIC_INTERNA', 'QT_DIARIA_INICIAL', :new.nm_usuario);
end if;
if	(:new.qt_diaria_final <> :old.qt_diaria_final) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_diaria_final, :new.qt_diaria_final, 'PLS_REGRA_COPARTIC_INTERNA', 'QT_DIARIA_FINAL', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_INTERNA ADD (
  CONSTRAINT PLSRCPI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_INTERNA ADD (
  CONSTRAINT PLSRCPI_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSRCPI_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_INTERNA TO NIVEL_1;

