ALTER TABLE TASY.D301_DATASET_RETORNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.D301_DATASET_RETORNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.D301_DATASET_RETORNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_DATASET           VARCHAR2(4 BYTE)         NOT NULL,
  NR_ORDEM             NUMBER(5)                NOT NULL,
  NR_SEQ_ARQUIVO       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.D301DRE_D301ARE_FK_I ON TASY.D301_DATASET_RETORNO
(NR_SEQ_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.D301DRE_PK ON TASY.D301_DATASET_RETORNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.D301_DATASET_RETORNO ADD (
  CONSTRAINT D301DRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.D301_DATASET_RETORNO ADD (
  CONSTRAINT D301DRE_D301ARE_FK 
 FOREIGN KEY (NR_SEQ_ARQUIVO) 
 REFERENCES TASY.D301_ARQUIVO_RETORNO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.D301_DATASET_RETORNO TO NIVEL_1;

