ALTER TABLE TASY.LOG_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_PACOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_INTERNO_CONTA     NUMBER(10)               NOT NULL,
  NR_SEQ_PROC_PACOTE   NUMBER(10)               NOT NULL,
  DT_LOG               DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_LOG               VARCHAR2(2000 BYTE)      NOT NULL,
  IE_ACAO              VARCHAR2(3 BYTE),
  NR_SEQ_ITEM          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGPCTE_CONPACI_FK_I ON TASY.LOG_PACOTE
(NR_INTERNO_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOGPCTE_PK ON TASY.LOG_PACOTE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGPCTE_PK
  MONITORING USAGE;


CREATE INDEX TASY.LOGPCTE_PROPACI_FK_I ON TASY.LOG_PACOTE
(NR_SEQ_PROC_PACOTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOG_PACOTE ADD (
  CONSTRAINT LOGPCTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_PACOTE ADD (
  CONSTRAINT LOGPCTE_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT LOGPCTE_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROC_PACOTE) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOG_PACOTE TO NIVEL_1;

