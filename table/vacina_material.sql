ALTER TABLE TASY.VACINA_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VACINA_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.VACINA_MATERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_VACINA        NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.VACMAT_MATERIA_FK_I ON TASY.VACINA_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.VACMAT_PK ON TASY.VACINA_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VACMAT_VACINA_FK_I ON TASY.VACINA_MATERIAL
(NR_SEQ_VACINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.VACINA_MATERIAL ADD (
  CONSTRAINT VACMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.VACINA_MATERIAL ADD (
  CONSTRAINT VACMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT VACMAT_VACINA_FK 
 FOREIGN KEY (NR_SEQ_VACINA) 
 REFERENCES TASY.VACINA (NR_SEQUENCIA));

GRANT SELECT ON TASY.VACINA_MATERIAL TO NIVEL_1;

