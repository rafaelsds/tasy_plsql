ALTER TABLE TASY.OCV_RESPONSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OCV_RESPONSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.OCV_RESPONSE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_STATUS             VARCHAR2(4 BYTE),
  CD_MEDICARE_STATUS    NUMBER(4),
  CD_CONCESSION_STATUS  NUMBER(4),
  CD_MEDICARE_CARD      NUMBER(10),
  NR_PATIENT_REF        NUMBER(2),
  NM_FIRST              VARCHAR2(40 BYTE),
  NR_SEQ_TRANSACTION    VARCHAR2(24 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.OCV_RESP_PK ON TASY.OCV_RESPONSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OCV_RESPONSE ADD (
  CONSTRAINT OCV_RESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.OCV_RESPONSE TO NIVEL_1;

