ALTER TABLE TASY.CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_TIPO_CONTRATO     NUMBER(10)           NOT NULL,
  DS_OBJETO_CONTRATO       VARCHAR2(1000 BYTE)  NOT NULL,
  IE_RENOVACAO             VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_INICIO                DATE                 NOT NULL,
  DT_FIM                   DATE,
  CD_CGC_CONTRATADO        VARCHAR2(14 BYTE),
  CD_PESSOA_CONTRATADA     VARCHAR2(10 BYTE),
  QT_DIAS_RESCISAO         NUMBER(5),
  CD_CONTRATO              VARCHAR2(20 BYTE),
  IE_PRAZO_CONTRATO        VARCHAR2(1 BYTE),
  NM_CONTATO               VARCHAR2(40 BYTE),
  CD_PESSOA_RESP           VARCHAR2(10 BYTE),
  VL_TOTAL_CONTRATO        NUMBER(15,2),
  DS_ATRIBUICAO            VARCHAR2(4000 BYTE),
  IE_PAGAR_RECEBER         VARCHAR2(1 BYTE),
  CD_PESSOA_NEGOC          VARCHAR2(10 BYTE),
  DT_REVISAO               DATE,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  IE_AVISA_VENCIMENTO      VARCHAR2(1 BYTE)     NOT NULL,
  QT_DIAS_AVISO_VENC       NUMBER(5),
  CD_CARGO                 NUMBER(10),
  DS_MOTIVO_RESCISAO       VARCHAR2(255 BYTE),
  NR_SEQ_FORMA_RESCISAO    NUMBER(10),
  CD_SETOR                 NUMBER(5),
  VL_MULTA_CONTRATUAL      NUMBER(15,2),
  CD_CONDICAO_PAGAMENTO    NUMBER(10),
  IE_AVISA_VENC_SETOR      VARCHAR2(1 BYTE)     NOT NULL,
  PR_MULTA_CONTRATUAL      NUMBER(15,2),
  NR_SEQ_SUBTIPO_CONTRATO  NUMBER(10),
  IE_CLASSIFICACAO         VARCHAR2(2 BYTE)     NOT NULL,
  NR_SEQ_CONTRATO_ATUAL    NUMBER(10),
  DT_ATUALIZACAO_ADITIVO   DATE,
  NR_SEQ_ADITIVO           NUMBER(10),
  QT_DIAS_INTERV_AVISO     NUMBER(10),
  NR_SEQ_INDICE_REAJUSTE   NUMBER(10),
  NR_SEQ_AREA              NUMBER(10),
  DT_LIBERACAO             DATE,
  NM_USUARIO_LIB           VARCHAR2(15 BYTE),
  IE_AVISA_REAJUSTE        VARCHAR2(1 BYTE),
  DT_PUBLICACAO            DATE,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SOLIC_COMPRA          NUMBER(10),
  CD_OPERACAO_NF           NUMBER(4),
  CD_NATUREZA_OPERACAO     NUMBER(4),
  IE_EMAIL_ORIGEM          VARCHAR2(1 BYTE),
  DT_ASSINATURA            DATE,
  NM_USUARIO_ASSINAT       VARCHAR2(15 BYTE),
  QT_DIAS_REAJUSTE         NUMBER(10),
  EMAIL_CONTATO            VARCHAR2(150 BYTE),
  TELEFONE_CONTATO         VARCHAR2(150 BYTE),
  CD_PESSOA_CONTRATANTE    VARCHAR2(10 BYTE),
  CD_CNPJ_CONTRATANTE      VARCHAR2(15 BYTE),
  VL_CUSTAS_CONTRATO       NUMBER(15,2),
  PR_CUSTAS_CONTRATO       NUMBER(15,2),
  DS_TITULO_CONTRATO       VARCHAR2(255 BYTE),
  NR_DOCUMENTO_EXTERNO     VARCHAR2(100 BYTE),
  IE_PERIODO_NF            VARCHAR2(15 BYTE)    NOT NULL,
  QT_MAXIMO_NF_PERIODO     NUMBER(5),
  QT_DIAS_REVISAO          NUMBER(10),
  NR_SEQ_CONTRATO_GESTAO   NUMBER(10),
  VL_NEGOCIADO             NUMBER(15,2),
  NR_CPF_CONTATO           VARCHAR2(20 BYTE),
  QT_DIAS_PERIODO          NUMBER(10),
  IE_ESTAGIO               VARCHAR2(15 BYTE),
  DT_ENCERRAMENTO          DATE,
  NM_USUARIO_ENCER         VARCHAR2(15 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  NR_SEQ_ERRATA            NUMBER(10),
  NR_SEQ_PENDENCIA         NUMBER(10),
  IE_FINALIZADO            VARCHAR2(1 BYTE),
  DT_FINALIZADO            DATE,
  NM_USUARIO_FINALIZ       VARCHAR2(15 BYTE),
  IE_SLA                   VARCHAR2(1 BYTE),
  IE_ENVIAR_EMAIL_OC       VARCHAR2(1 BYTE),
  CD_MEDICO_RESP           VARCHAR2(10 BYTE),
  VL_FATUR_MINIMO          NUMBER(13,4),
  NR_SEQ_CLIENTE           NUMBER(10),
  NM_MEDICO_EXTERNO        VARCHAR2(255 BYTE),
  IE_INDICE_REAJUSTE       VARCHAR2(10 BYTE),
  CD_PACIENTE              VARCHAR2(10 BYTE),
  CD_MOEDA_ESTRANGEIRA     NUMBER(3),
  CD_COMPRADOR             VARCHAR2(10 BYTE),
  DT_ASSINATURA_ORIG       DATE,
  NM_USUARIO_ASSINAT_ORIG  VARCHAR2(15 BYTE),
  IE_SOMENTE_OC_PAGTO      VARCHAR2(1 BYTE),
  IE_SISTEMA_AMORT         VARCHAR2(1 BYTE),
  VL_TAXA_BANCO_CONTRAT    NUMBER(15,4),
  DS_INDEXADOR             VARCHAR2(250 BYTE),
  NR_DDI_TELEFONE          VARCHAR2(3 BYTE),
  DT_ULT_GERACAO_OC        DATE,
  QT_DIAS_OC_AUTO          NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONTRAT_ARECONT_FK_I ON TASY.CONTRATO
(NR_SEQ_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_ARECONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_CARGO_FK_I ON TASY.CONTRATO
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_COFOREC_FK_I ON TASY.CONTRATO
(NR_SEQ_FORMA_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_COFOREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_COMCLIE_FK_I ON TASY.CONTRATO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_COMPRAD_FK_I ON TASY.CONTRATO
(CD_COMPRADOR, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_CONPAGA_FK_I ON TASY.CONTRATO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_CONSUTI_FK_I ON TASY.CONTRATO
(NR_SEQ_SUBTIPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_CONSUTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_CONTGEST_FK_I ON TASY.CONTRATO
(NR_SEQ_CONTRATO_GESTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_CONTGEST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_CONTRAT_FK_I ON TASY.CONTRATO
(NR_SEQ_CONTRATO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_ESTABEL_FK_I ON TASY.CONTRATO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONTRAT_FK ON TASY.CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_MOEDA_FK_I ON TASY.CONTRATO
(CD_MOEDA_ESTRANGEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_NATOPER_FK_I ON TASY.CONTRATO
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_OPENOTA_FK_I ON TASY.CONTRATO
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_OPENOTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_PENDCON_FK_I ON TASY.CONTRATO
(NR_SEQ_PENDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK ON TASY.CONTRATO
(CD_PESSOA_NEGOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK1_I ON TASY.CONTRATO
(CD_PESSOA_CONTRATANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK2_I ON TASY.CONTRATO
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK3_I ON TASY.CONTRATO
(CD_PESSOA_CONTRATADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK4_I ON TASY.CONTRATO
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESFISI_FK5_I ON TASY.CONTRATO
(CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESJURI_FK_I ON TASY.CONTRATO
(CD_CGC_CONTRATADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTRAT_PESJURI_FK1_I ON TASY.CONTRATO
(CD_CNPJ_CONTRATANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_PESJURI_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_SETATEN_FK_I ON TASY.CONTRATO
(CD_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_SOLCOMP_FK_I ON TASY.CONTRATO
(NR_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_SOLCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_TIINREA_FK_I ON TASY.CONTRATO
(NR_SEQ_INDICE_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTRAT_TIINREA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTRAT_TIPCONT_FK_I ON TASY.CONTRATO
(NR_SEQ_TIPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.contrato_afinsert
after insert or update ON TASY.CONTRATO for each row
declare
ds_retorno_integracao_w	clob;

begin

if(:old.dt_liberacao is null and :new.dt_liberacao is not null) then
begin
    ds_retorno_integracao_w := null;
    select bifrost.send_integration(
        'contract.control.send.request',
        'com.philips.tasy.integration.contractcontrol.outbound.ContractControlCallback',
        ''||:new.nr_sequencia||'',
        :new.nm_usuario)
    into ds_retorno_integracao_w
    from dual;
end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.contrato_Insert
BEFORE INSERT OR UPDATE ON TASY.CONTRATO FOR EACH ROW
DECLARE
qt_reg_w	number(1);
ie_status_w  number;
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select count(*)
into ie_status_w
from contrato_regra_nf
where nr_seq_contrato = :new.nr_sequencia and obter_bloq_canc_proj_rec(nr_seq_proj_rec) > 0;

if (ie_status_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

if	(:new.cd_cgc_contratado is not null) and
	(:new.cd_pessoa_contratada is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278457);
end if;

if	(:new.cd_cgc_contratado is null) and
	(:new.cd_pessoa_contratada is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278459);
end if;
<<Final>>
qt_reg_w	:= 0;


END;
/


ALTER TABLE TASY.CONTRATO ADD (
  CONSTRAINT CONTRAT_FK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO ADD (
  CONSTRAINT CONTRAT_PESFISI_FK5 
 FOREIGN KEY (CD_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_ESTRANGEIRA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT CONTRAT_COMPRAD_FK 
 FOREIGN KEY (CD_COMPRADOR, CD_ESTABELECIMENTO) 
 REFERENCES TASY.COMPRADOR (CD_PESSOA_FISICA,CD_ESTABELECIMENTO),
  CONSTRAINT CONTRAT_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_ATUAL) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONTRAT_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_PESFISI_FK4 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_PENDCON_FK 
 FOREIGN KEY (NR_SEQ_PENDENCIA) 
 REFERENCES TASY.PENDENCIA_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT CONTRAT_COFOREC_FK 
 FOREIGN KEY (NR_SEQ_FORMA_RESCISAO) 
 REFERENCES TASY.CONTRATO_FORMA_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CONTRAT_TIPCONT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONTRATO) 
 REFERENCES TASY.TIPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_PESJURI_FK_I 
 FOREIGN KEY (CD_CGC_CONTRATADO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CONTRAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_NEGOC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_CONTRATADA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONTRAT_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT CONTRAT_CONSUTI_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_CONTRATO) 
 REFERENCES TASY.CONTRATO_SUBTIPO (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_TIINREA_FK 
 FOREIGN KEY (NR_SEQ_INDICE_REAJUSTE) 
 REFERENCES TASY.TIPO_INDICE_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_ARECONT_FK 
 FOREIGN KEY (NR_SEQ_AREA) 
 REFERENCES TASY.AREA_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT CONTRAT_SOLCOMP_FK 
 FOREIGN KEY (NR_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA (NR_SOLIC_COMPRA),
  CONSTRAINT CONTRAT_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT CONTRAT_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT CONTRAT_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_CONTRATANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTRAT_PESJURI_FK1 
 FOREIGN KEY (CD_CNPJ_CONTRATANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CONTRAT_CONTGEST_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_GESTAO) 
 REFERENCES TASY.CONTRATO_GESTAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONTRATO TO NIVEL_1;

