ALTER TABLE TASY.AUTORIZACAO_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONVENIO
(
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_AUTORIZACAO         NUMBER(10),
  CD_CONVENIO                NUMBER(5)          NOT NULL,
  CD_AUTORIZACAO             VARCHAR2(20 BYTE),
  DT_AUTORIZACAO             DATE               NOT NULL,
  DT_INICIO_VIGENCIA         DATE,
  DT_ATUALIZACAO             DATE,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_FIM_VIGENCIA            DATE,
  NM_RESPONSAVEL             VARCHAR2(40 BYTE),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  CD_SENHA                   VARCHAR2(20 BYTE),
  CD_PROCEDIMENTO_PRINCIPAL  NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  DT_PEDIDO_MEDICO           DATE,
  CD_MEDICO_SOLICITANTE      VARCHAR2(10 BYTE),
  IE_TIPO_GUIA               VARCHAR2(2 BYTE),
  QT_DIA_AUTORIZADO          NUMBER(3),
  NR_PRESCRICAO              NUMBER(14),
  DT_ENVIO                   DATE,
  DT_RETORNO                 DATE,
  NR_SEQ_ESTAGIO             NUMBER(10),
  IE_TIPO_AUTORIZACAO        VARCHAR2(2 BYTE),
  IE_TIPO_DIA                VARCHAR2(1 BYTE)   NOT NULL,
  CD_TIPO_ACOMODACAO         NUMBER(15),
  NR_SEQ_AUTOR_CIRURGIA      NUMBER(10),
  DS_INDICACAO               VARCHAR2(4000 BYTE),
  DT_GERACAO_CIH             DATE,
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_AGENDA              NUMBER(10),
  QT_DIA_SOLICITADO          NUMBER(3),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  CD_PROCEDIMENTO_CONVENIO   VARCHAR2(20 BYTE),
  DT_ENTRADA_PREVISTA        DATE,
  NR_SEQ_GESTAO              NUMBER(10),
  IE_CARATER_INT_TISS        VARCHAR2(2 BYTE),
  NR_SEQ_AUTOR_ORIGEM        NUMBER(10),
  IE_RESP_AUTOR              VARCHAR2(3 BYTE),
  NR_SEQ_AGENDA_CONSULTA     NUMBER(10),
  NR_SEQ_AUTOR_DESDOB        NUMBER(10),
  CD_CGC_PRESTADOR           VARCHAR2(14 BYTE),
  NR_SEQ_PACIENTE_SETOR      NUMBER(10),
  NR_CICLO                   NUMBER(3),
  NR_SEQ_AGE_INTEG           NUMBER(10),
  NR_SEQ_PACIENTE            NUMBER(10),
  CD_MEDICO_SOLIC_TISS       VARCHAR2(10 BYTE),
  NR_SEQ_CLASSIF             NUMBER(10),
  NR_INTERNO_CONTA_REF       NUMBER(10),
  NR_SEQ_AGENDA_PROC         NUMBER(10),
  QT_DIAS_PRAZO              NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  DS_DIA_CICLO               VARCHAR2(5 BYTE),
  CD_SETOR_ORIGEM            NUMBER(5),
  CD_SENHA_PROVISORIA        VARCHAR2(20 BYTE),
  DT_REFERENCIA              DATE,
  NR_SEQ_AUDITORIA           NUMBER(10),
  DS_TARJA_CARTAO            VARCHAR2(4000 BYTE),
  NR_SEQ_APRES               NUMBER(5),
  NR_SEQ_REGRA_AUTOR         NUMBER(10),
  NR_SEQ_GUIA_PLANO          NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4),
  CD_EMPRESA_PAC             NUMBER(10),
  DT_AGENDA                  DATE,
  DT_AGENDA_CONS             DATE,
  DT_AGENDA_INTEG            DATE,
  NR_ORDEM_COMPRA            NUMBER(10),
  DS_PRESTADOR_TISS          VARCHAR2(255 BYTE),
  CD_PRESTADOR_TISS          VARCHAR2(60 BYTE),
  NM_USUARIO_RESP            VARCHAR2(15 BYTE),
  IE_TISS_TIPO_ANEXO_AUTOR   VARCHAR2(15 BYTE),
  CD_SETOR_RESP              NUMBER(5),
  NR_SEQ_RXT_TRATAMENTO      NUMBER(10),
  DT_DESDOBRAMENTO_CONTA     DATE,
  IE_TIPO_INTERNACAO_TISS    VARCHAR2(2 BYTE),
  IE_REGIME_INTERNACAO       VARCHAR2(2 BYTE),
  IE_TISS_TIPO_ACIDENTE      VARCHAR2(15 BYTE),
  IE_PREVISAO_USO_QUIMIO     VARCHAR2(1 BYTE),
  IE_PREVISAO_USO_OPME       VARCHAR2(1 BYTE),
  CD_TIPO_ACOMOD_DESEJ       NUMBER(15),
  DS_CBO_TISS                VARCHAR2(255 BYTE),
  DT_GERACAO_PACOTE          DATE,
  CD_AUTORIZACAO_PREST       VARCHAR2(20 BYTE),
  DT_VALIDADE_GUIA           DATE,
  CD_INTERNO_ORIGEM_TISS     VARCHAR2(30 BYTE),
  CD_INTERNO_CONTRAT_EXEC    VARCHAR2(30 BYTE),
  DS_CONTRATADO_EXEC_TISS    VARCHAR2(100 BYTE),
  CD_INTERNO_CONTRAT_SOLIC   VARCHAR2(30 BYTE),
  CD_TIPO_PROCEDIMENTO       NUMBER(3),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_TISS_TIPO_ETAPA_AUTOR   VARCHAR2(2 BYTE),
  CD_VALIDACAO_TISS          VARCHAR2(10 BYTE),
  CD_AUSENCIA_COD_VALID      VARCHAR2(2 BYTE),
  CD_MEDICO_EXEC_AGENDA      VARCHAR2(10 BYTE),
  CD_PROC_PRINCIPAL_LOC      VARCHAR2(15 BYTE),
  IE_ECLIPSE_STATUS          VARCHAR2(40 BYTE),
  IE_TIPO_TRAMITE            VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTCONV_AGECONS_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AGENDA_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_AGEINTE_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AGE_INTEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_AGEPACI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_AGEPAPR_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AGENDA, NR_SEQ_AGENDA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_ATEPACI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_AUDCOPA_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_AUDCOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_AUTCIRU_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AUTOR_CIRURGIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_AUTCONV_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AUTOR_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_AUTCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_AUTCONV_FK2_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_AUTOR_DESDOB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_CLAUTOR_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_CONPACI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_INTERNO_CONTA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_CONPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_CONVENI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_EMPREFE_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_EMPRESA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_ESTABEL_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_ESTAUTO_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_GESVAGA_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_GESTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I1 ON TASY.AUTORIZACAO_CONVENIO
(CD_CONVENIO, CD_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I2 ON TASY.AUTORIZACAO_CONVENIO
(DT_AUTORIZACAO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I4 ON TASY.AUTORIZACAO_CONVENIO
(NVL(NVL("DT_AGENDA","DT_AGENDA_CONS"),"DT_AGENDA_INTEG"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I5 ON TASY.AUTORIZACAO_CONVENIO
(DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I6 ON TASY.AUTORIZACAO_CONVENIO
(DT_FIM_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I7 ON TASY.AUTORIZACAO_CONVENIO
(DT_PEDIDO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I8 ON TASY.AUTORIZACAO_CONVENIO
(DT_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_I9 ON TASY.AUTORIZACAO_CONVENIO
(DT_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_MED_EXEC_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_MEDICO_EXEC_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_MEDICO_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_MEDICO_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PACATEN_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PACSETO_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_PACIENTE_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PESFISI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_MEDICO_SOLIC_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PESFISI_FK2_I ON TASY.AUTORIZACAO_CONVENIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PESJURI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUTCONV_PK ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PLSGUIA_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_GUIA_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PRESMED_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_PROCEDI_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_PROCEDIMENTO_PRINCIPAL, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_PROINTE_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_RXTTRAT_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NR_SEQ_RXT_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCONV_SETATEN_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_SETOR_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_SETATEN_FK2_I ON TASY.AUTORIZACAO_CONVENIO
(CD_SETOR_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_TIPACOM_FK_I ON TASY.AUTORIZACAO_CONVENIO
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_TIPACOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCONV_USUARIO_FK_I ON TASY.AUTORIZACAO_CONVENIO
(NM_USUARIO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCONV_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CONVENIO_AFTINSERT
AFTER Insert ON TASY.AUTORIZACAO_CONVENIO FOR EACH ROW
DECLARE
qt_existe_w	Number(10);
nr_sequencia_w	Number(10);
ie_interno_w	varchar2(255);
ds_estagio_w	varchar2(255);
ie_medico_w   	varchar2(1);
ie_agua_just_med_w	varchar2(1) := '';
ds_texto_padrao_w	long;
ie_save_insurance_holder_w 	varchar2(1);
qt_anexo_agenda_w	number(3);
nr_seq_anexo_agenda_w	anexo_agenda.nr_sequencia%type;


Cursor C01 is
	select	ds_texto_padrao
	from	texto_padrao_justif_solic
	where	ie_tipo_justificativa = :new.ie_tipo_autorizacao
	and 	ie_situacao = 'A';

--TEXTO_PADRAO_JUSTIF_SOLIC

Cursor c02 is
	select	ds_arquivo,
				ds_observacao
	from	ged_atendimento
	where	nr_seq_agenda = :new.nr_seq_agenda;

c02_w		c02%rowtype;

begin

--Added below code for request SO#1996135
ie_save_insurance_holder_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'SI');

if 	(ie_save_insurance_holder_w = 'S') and
	(:new.cd_pessoa_fisica is not null) and
	(:new.cd_convenio is not null) and
	((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
	(nvl(:new.cd_pessoa_fisica, '0') <> nvl(:old.cd_pessoa_fisica, '0'))) then
	insere_atualiza_titular_conv(
				:new.nm_usuario,
				:new.cd_convenio,
				null,
				:new.cd_pessoa_fisica,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				'N',
				'2');
end if;


Obter_Param_Usuario(3004, 236, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,ie_agua_just_med_w);

if (ie_agua_just_med_w = 'S') then
   select	count(*)
   into	   qt_existe_w
   from	   paciente_justificativa
   where	   nr_atendimento		= :new.nr_atendimento
   and	   nr_seq_autorizacao	= :new.nr_seq_autorizacao;
elsif (ie_agua_just_med_w = 'P') then
   if (:new.nr_seq_autorizacao is not null) and
      (:new.nr_atendimento is not null) then
      select	count(*)
      into	   qt_existe_w
      from	   paciente_justificativa
      where	   nr_seq_autorizacao	= :new.nr_seq_autorizacao
      and      nr_atendimento		   = :new.nr_atendimento;

   else
      select	count(*)
      into	   qt_existe_w
      from	   paciente_justificativa
      where	   nr_sequencia_autor	= :new.nr_sequencia;
   end if;
end if;

begin
select	ie_interno
into	ie_interno_w
from	estagio_autorizacao
where	nr_sequencia = :new.nr_seq_estagio
and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
exception
when no_data_found then
	ie_interno_w	:= null;
end;

if	(qt_existe_w = 0) and
	(ie_interno_w = '2') and
   ((ie_agua_just_med_w = 'P') or (ie_agua_just_med_w = 'S' and :new.nr_atendimento is not null)) and
	(:new.ie_tipo_autorizacao is not null) then

	select	paciente_justificativa_seq.nextval
	into	nr_sequencia_w
	from	dual;

	SELECT obter_se_usuario_medico(:NEW.nm_usuario)
	into	ie_medico_w
	FROM dual;

	ds_texto_padrao_w:= '';
	open C01;
	loop
	fetch C01 into
		ds_texto_padrao_w;
	exit when C01%notfound;
		begin
		ds_texto_padrao_w:= ds_texto_padrao_w;
		end;
	end loop;
	close C01;

	insert into paciente_justificativa(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_autorizacao,
		nr_atendimento,
		dt_justificativa,
		ie_tipo_justificativa,
		qt_dia_prorrogacao,
		nr_sequencia_autor,
		ie_situacao,
		ds_justificativa,
		cd_profissional,
		ie_gerado_job,
		cd_pessoa_fisica,-- Francisco - 23/10/06 - Inclui sequencia
		dt_liberacao_parcial)
	values(	nr_sequencia_w,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_autorizacao,
		:new.nr_atendimento,
		sysdate,
		:new.ie_tipo_autorizacao,
		:new.qt_dia_autorizado,
		:new.nr_sequencia,
		'A',
		ds_texto_padrao_w,
		nvl(decode(ie_medico_w,'S',Obter_Medico_Prescricao(:new.nr_prescricao,'C'),:new.cd_medico_solicitante) ,null),
		'S',
		:new.cd_pessoa_fisica,
		sysdate);
end if;

select	max(ds_estagio)
into	ds_estagio_w
from	estagio_autorizacao
where	nr_sequencia	= :new.nr_seq_estagio
and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

if	(ds_estagio_w is not null) then
	insert into autorizacao_convenio_hist
		(nr_sequencia,
		 dt_atualizacao,
		 nm_usuario,
		 nr_atendimento,
		 nr_seq_autorizacao,
		 ds_historico,
		 nr_sequencia_autor,
		 nr_seq_estagio)
	values (autorizacao_convenio_hist_seq.nextval,
		 sysdate,
		 :new.nm_usuario,
		 :new.nr_atendimento,
		 :new.nr_seq_autorizacao,
		 ds_estagio_w,
		 :new.nr_sequencia,
		 :new.nr_seq_estagio);
end if;

/* OS 2007482 - Insercao de anexo agenda da Gestao da Agenda Cirurgica - Jeferson Job (jjrsantos) */
select	count(*)
into	qt_anexo_agenda_w
from	ged_atendimento
where	nr_seq_agenda	= :new.nr_seq_agenda;

if	(qt_anexo_agenda_w > 0) then
	open c02;
	loop
	fetch c02 into
		c02_w;
	exit when c02%notfound;
		select	count(*)
		into		qt_anexo_agenda_w
		from	anexo_agenda
		where	ds_arquivo = c02_w.ds_arquivo;

		if	( qt_anexo_agenda_w = 0) then

			select	anexo_agenda_seq.nextval
			into	nr_seq_anexo_agenda_w
			from	dual;

			insert into anexo_agenda (
					nr_sequencia,
					ds_arquivo,
					nr_seq_agenda,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_observacao,
					ie_situacao
				)
			values (
				nr_seq_anexo_agenda_w,
				c02_w.ds_arquivo,
				:new.nr_seq_agenda,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario_nrec,
				c02_w.ds_observacao,
				'A'
			);
		end if;
	end loop;
	close c02;
end if;
/* FIM - Insercao de anexo agenda da Gestao da Agenda Cirurgica */
END;
/


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CONVENIO_BEFINSERT
BEFORE Insert or Update ON TASY.AUTORIZACAO_CONVENIO FOR EACH ROW
DECLARE

ie_tipo_atendimento_w	number(3);
cd_proc_convenio_w	varchar2(20)	:= '';
cd_grupo_w		varchar2(10)	:= '';
nr_seq_conversao_w	number(10);
cd_plano_w		varchar2(10)	:= '';
ie_clinica_w		number(5);
cd_tipo_acomod_conv_w	number(4);
qt_idade_w		number(15,0);
cd_pessoa_fisica_w	varchar2(10);
ie_sexo_w		varchar2(1);
cd_empresa_ref_w	number(10,0);
ie_carater_inter_sus_w	varchar2(2);

pragma autonomous_transaction;

begin
/*2036854 - Generating insurance authorization from Service Schedule */

if(:new.cd_proc_principal_loc is null and :new.CD_PROCEDIMENTO_PRINCIPAL is not null and :new.ie_origem_proced is not null)then
  select cd_procedimento_loc
  into :new.cd_proc_principal_loc
  from procedimento
  where cd_procedimento=:new.CD_PROCEDIMENTO_PRINCIPAL
  and ie_origem_proced=:new.ie_origem_proced;
end if;

/* OS 51884 - Gravar proc conv�nio */
if	(:new.nr_atendimento is not null) then
	select	max(ie_tipo_atendimento)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	select 	max(cd_plano_convenio),
		nvl(max(cd_empresa),0)
	into	cd_plano_w,
		cd_empresa_ref_w
	from 	atend_categoria_convenio
	where	nr_atendimento = :new.nr_atendimento
	and 	cd_convenio    = :new.cd_convenio;

	select 	max(cd_tipo_acomodacao)
	into	cd_tipo_acomod_conv_w
	from 	atend_categoria_convenio
	where	nr_atendimento = :new.nr_atendimento
	and 	cd_convenio    = :new.cd_convenio;

	if	(:new.cd_pessoa_fisica is null) then
		select	max(cd_pessoa_fisica)
		into	:new.cd_pessoa_fisica
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;
	end if;
end if;

if	(:new.cd_procedimento_principal is not null) then

	select	max(cd_pessoa_fisica),
		max(ie_carater_inter_sus)
	into	cd_pessoa_fisica_w,
		ie_carater_inter_sus_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	begin
	select	max(obter_idade(dt_nascimento, nvl(dt_obito,sysdate),'DIA')),
		max(ie_sexo)
	into	qt_idade_w,
		ie_sexo_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	exception
	when others then
		qt_idade_w	:= 0;
		ie_sexo_w	:= '';
	end;

	converte_proc_convenio(null,  :new.cd_convenio,null,
				:new.cd_procedimento_principal,
				:new.ie_origem_proced,
				null,
				null,
				ie_tipo_atendimento_w,null,
				cd_proc_convenio_w,
				cd_grupo_w,
				nr_seq_conversao_w,
				null,
				null,
				null,
				'A',
				cd_plano_w,
				ie_clinica_w,
				0,
				null,
				cd_tipo_acomod_conv_w,
				qt_idade_w,
				ie_sexo_w,
				cd_empresa_ref_w,
				ie_carater_inter_sus_w,
				0,
				null,
				null,
				null);
end if;

if	(cd_proc_convenio_w is not null) then
	:new.cd_procedimento_convenio	:= cd_proc_convenio_w;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CONVENIO_tp  after update ON TASY.AUTORIZACAO_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_USUARIO_RESP,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_RESP,1,4000),substr(:new.NM_USUARIO_RESP,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_RESP',ie_log_w,ds_w,'AUTORIZACAO_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.AUTOR_CONVENIO_AFTER_DELETE
after delete ON TASY.AUTORIZACAO_CONVENIO for each row
declare


begin
if	wheb_usuario_pck.get_ie_executar_trigger = 'S' then
	begin
	update  autorizacao_cirurgia
	set     nr_seq_autor_conv = null
	where   nr_seq_autor_conv = :old.nr_sequencia;
	exception
		when others then	
		null;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CONVENIO_AFTUPDATE
after update ON TASY.AUTORIZACAO_CONVENIO for each row
declare
qt_existe_w			number(10);
nr_sequencia_w			number(10);
ie_interno_w			varchar2(255) := '';
ie_agua_just_med_w		varchar2(1) := '';
ds_classificacao_new_w		varchar2(255) := '';
ds_classificacao_old_w		varchar2(255) := '';
ds_estagio_w			varchar2(255) := '';
ds_titulo_w			varchar2(255) := 'Log de alteracao na autorizacao convenio';
ds_historico_w			varchar2(4000):= '';
dt_parametro_w			date	:= ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(sysdate) ;
nr_seq_etapa_autor_w		number(10,0);
ie_medico_w       		varchar2(1);
ds_texto_padrao_w		long;
nr_seq_anexo_tiss_w		number(10);
dt_bed_approval_w		date;
ie_status_old_w			varchar2(2) := '';
ie_current_bed_status 		varchar2(2) := '';
cd_estab_pck_autor_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
ie_new_bed_status_w		varchar2(2) := '';
cd_mapped_profile_w		varchar2(10) := '';
ds_modulo_w			varchar2(48);
ds_action_old_w			varchar2(32) := '';
ie_parcial_w			estagio_autorizacao.ie_parcial%type;
Cursor C01 is
	select	ds_texto_padrao
	from	texto_padrao_justif_solic
	where	ie_tipo_justificativa = :new.ie_tipo_autorizacao
	and 	ie_situacao = 'A';

cursor c02 is
select	nr_sequencia
from	tiss_anexo_guia
where	nr_sequencia_autor = :new.nr_sequencia;

begin

if nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S' then

	Obter_Param_Usuario(3004,236, obter_perfil_ativo,:new.nm_usuario,cd_estab_pck_autor_w,ie_agua_just_med_w);

	if	(cd_estab_pck_autor_w is null) then
		cd_estab_pck_autor_w := :new.cd_estabelecimento;
	end if;

	if (ie_agua_just_med_w = 'S') then
	   select	count(*)
	   into	   qt_existe_w
	   from	   paciente_justificativa
	   where	   nr_atendimento		   = :new.nr_atendimento
	   and	   nr_seq_autorizacao	= :new.nr_seq_autorizacao;
	elsif (ie_agua_just_med_w = 'P') then
	   if (:new.nr_seq_autorizacao is not null) and
	      (:new.nr_atendimento is not null) then
	      select	count(*)
	      into	   qt_existe_w
	      from	   paciente_justificativa
	      where	   nr_atendimento		   = :new.nr_atendimento
	      and	   nr_seq_autorizacao	= :new.nr_seq_autorizacao;
	   else
	      select	count(*)
	      into	   qt_existe_w
	      from	   paciente_justificativa
	      where	   nr_sequencia_autor	= :new.nr_sequencia;
	   end if;
	end if;

	if	(cd_estab_pck_autor_w is not null) then

		select	max(ds_estagio),
			max(nr_seq_etapa_autor),
			max(ie_interno),
			Nvl(max(ie_parcial),'N')
		into	ds_estagio_w,
			nr_seq_etapa_autor_w,
			ie_interno_w,
			ie_parcial_w
		from	estagio_autorizacao
		where	nr_sequencia	= :new.nr_seq_estagio
		and	OBTER_EMPRESA_ESTAB(cd_estab_pck_autor_w) = cd_empresa;

	else
		select	max(ds_estagio),
			max(nr_seq_etapa_autor),
			max(ie_interno),
			Nvl(max(ie_parcial),'N')
		into	ds_estagio_w,
			nr_seq_etapa_autor_w,
			ie_interno_w,
			ie_parcial_w
		from	estagio_autorizacao
		where	nr_sequencia	= :new.nr_seq_estagio;

	end if;



	if 	(Nvl(:new.nr_seq_estagio,0) <> Nvl(:old.nr_seq_estagio,0)) then

		DBMS_APPLICATION_INFO.READ_MODULE(ds_modulo_w, ds_action_old_w);
		DBMS_APPLICATION_INFO.SET_ACTION('AUTORIZACAO_CONVENIO_AFTUPDATE');

		if	(ds_action_old_w <> 'AUTORIZACAO_INTEGRACAO_UPDATE') AND
			(ds_action_old_w <> 'ATUALIZAR_AUTORIZACAO_CONVENIO_I')then

			if	(ie_interno_w = '1')  then --Necessidade
				update	autorizacao_integracao set
					cd_status_autorizacao = 'P'
				where	nr_sequencia_autor = :new.nr_sequencia
				and	nvl(cd_status_autorizacao,'P') <> 'P';
			/*elsif	(ie_interno_w = '2')  then --Aguardando justificativa medico
			elsif	(ie_interno_w = '3')  then --Justificado pelo medico
			elsif	(ie_interno_w = '4')  then --Pre Admissao */
			elsif	(ie_interno_w = '5')  then -- Encaminhado
				update	autorizacao_integracao set
					cd_status_autorizacao = 'E'
				where	nr_sequencia_autor = :new.nr_sequencia
				and	nvl(cd_status_autorizacao,'P') <> 'E';
			elsif	(ie_interno_w = '10')  then --Autorizado
				update	autorizacao_integracao set
					cd_status_autorizacao = Decode(ie_parcial_w,'S','PA','A')
				where	nr_sequencia_autor = :new.nr_sequencia
				and	nvl(cd_status_autorizacao,'P') <> 'A';
			--elsif	(ie_interno_w = '60')  then --Pendente de cancelamento
			elsif	(ie_interno_w = '70')  then --Cancelado
				update	autorizacao_integracao set
					cd_status_autorizacao = 'C' --cancelado
				where	nr_sequencia_autor = :new.nr_sequencia
				and	nvl(cd_status_autorizacao,'P') <> 'C';
			elsif	(ie_interno_w = '90')  then --Reprovado
				update	autorizacao_integracao set
					cd_status_autorizacao = 'N' --cancelado
				where	nr_sequencia_autor = :new.nr_sequencia
				and	nvl(cd_status_autorizacao,'P') <> 'N';
			end if;
		end if;

		DBMS_APPLICATION_INFO.SET_ACTION(ds_action_old_w);



	end if;


	begin
	if (nvl(:new.nr_atendimento,0) > 0) and (nvl(:old.nr_atendimento,0) > 0) and (nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
	   if (:new.nr_seq_autorizacao is not null) then
	      update   paciente_justificativa
	      set      nr_atendimento       = :new.nr_atendimento
	      where    nr_seq_autorizacao	= :new.nr_seq_autorizacao
		  and 	   nr_atendimento	    = :old.nr_atendimento;
	   else
	      update   paciente_justificativa
	      set      nr_atendimento       = :new.nr_atendimento
	      where    nr_sequencia_autor	= :new.nr_sequencia;
	   end if;
	end if;
	exception
	when others then
		null;
	end;

	if	(qt_existe_w = 0) and
		(ie_interno_w = '2') and
	   ((ie_agua_just_med_w = 'P') or (ie_agua_just_med_w = 'S' and :new.nr_atendimento is not null)) then

		if	(nvl(:new.ie_tipo_autorizacao,'X') = 'X') then
			Wheb_mensagem_pck.exibir_mensagem_abort(236801);
		end if;

		select	paciente_justificativa_seq.nextval
		into	nr_sequencia_w
		from	dual;

		SELECT obter_se_usuario_medico(:NEW.nm_usuario)
		into	ie_medico_w
		FROM dual;

		ds_texto_padrao_w:= '';
		open C01;
		loop
		fetch C01 into
			ds_texto_padrao_w;
		exit when C01%notfound;
			begin
			ds_texto_padrao_w:= ds_texto_padrao_w;
			end;
		end loop;
		close C01;

		insert into paciente_justificativa(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_autorizacao,
			nr_atendimento,
			dt_justificativa,
			ie_tipo_justificativa,
			qt_dia_prorrogacao,
			nr_sequencia_autor,
			ie_situacao,
			ds_justificativa,
			CD_PROFISSIONAL,
			ie_gerado_job,
			cd_pessoa_fisica,-- Francisco - 23/10/06 - Inclui sequencia
			dt_liberacao_parcial)
		values(	nr_sequencia_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_seq_autorizacao,
			:new.nr_atendimento,
			sysdate,
			:new.ie_tipo_autorizacao,
			:new.qt_dia_autorizado,
			:new.nr_sequencia,
			'A',
			ds_texto_padrao_w,
			nvl(decode(ie_medico_w,'S',Obter_Medico_Prescricao(:new.nr_prescricao,'C'),:new.cd_medico_solicitante) ,null),
			'S',
			:new.cd_pessoa_fisica,
			sysdate);
	end if;

	--rranganath SO-1844925 - Undo the approval of the bed request when the authorization is disapproved
	begin
		select 	ie_bmgmnt_status,
			cd_profile
		into 	ie_new_bed_status_w,
			cd_mapped_profile_w
		from 	map_iauth_status
		where 	ie_iauth_status = :new.nr_seq_estagio;
	exception
		when 	no_data_found then
			begin
			ie_new_bed_status_w := null;
			cd_mapped_profile_w := null;
			end;
	end;

	if	(:new.nr_seq_gestao is not null) and
		(ie_new_bed_status_w is not null) and
		(cd_mapped_profile_w is null or obter_perfil_ativo = cd_mapped_profile_w) then

		begin
		select 	dt_aprovacao,
			ie_status
		into 	dt_bed_approval_w,
			ie_current_bed_status
		from 	gestao_vaga
		where 	nr_sequencia = :new.nr_seq_gestao;

		exception
		when 	no_data_found then
			begin
			dt_bed_approval_w	:= null;
			ie_current_bed_status	:= null;
			end;
		end;

		update	gestao_vaga
		set	dt_aprovacao = null,
			nm_usuario_aprov = null,
			nr_seq_motivo_aprov_vaga = null,
			ds_justificativa_aprov = null,
			ie_status = ie_new_bed_status_w
		where 	nr_sequencia = :new.nr_seq_gestao;

	end if;
	--rranganath SO-1844925 - Undo the approval of the bed request when the authorization is disapproved

	if	(:old.nr_seq_classif <> :new.nr_seq_classif) then

		select	max(ds_classificacao)
		into	ds_classificacao_old_w
		from	classif_autorizacao
		where	nr_sequencia	= :old.nr_seq_classif;

		select	max(ds_classificacao)
		into	ds_classificacao_new_w
		from	classif_autorizacao
		where	nr_sequencia	= :new.nr_seq_classif;

		insert into autorizacao_convenio_hist
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_atendimento,
			nr_seq_autorizacao,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_sequencia_autor)
		select	autorizacao_convenio_hist_seq.nextval,
			sysdate,
			:new.nm_usuario,
			:new.nr_atendimento,
			:new.nr_seq_autorizacao,
			wheb_mensagem_pck.get_texto(1097955) ||' ' || ds_classificacao_old_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(1097956) ||' ' || ds_classificacao_new_w,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia
		from	dual;

	end if;

	if	(ds_estagio_w is not null) and
		(:old.nr_seq_estagio <> :new.nr_seq_estagio) then
		insert into autorizacao_convenio_hist
			(nr_sequencia,
			 dt_atualizacao,
			 nm_usuario,
			 nr_atendimento,
			 nr_seq_autorizacao,
			 ds_historico,
			 nr_sequencia_autor,
			 nr_seq_estagio)
		values (autorizacao_convenio_hist_seq.nextval,
			 sysdate,
			 :new.nm_usuario,
			 :new.nr_atendimento,
			 :new.nr_seq_autorizacao,
			 ds_estagio_w,
			 :new.nr_sequencia,
			 :new.nr_seq_estagio);
	end if;

	if	(nvl(nr_seq_etapa_autor_w,0) <> 0) and
		(:new.nr_seq_estagio <> :old.nr_seq_estagio) then

		insert	into	autorizacao_conv_etapa
			(nr_sequencia,
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_etapa,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_autor_conv,
			nr_seq_etapa,
			cd_pessoa_exec,
			ds_observacao)
		values	(autorizacao_conv_etapa_seq.nextval,
			sysdate,
			sysdate,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			:new.nr_sequencia,
			nr_seq_etapa_autor_w,
			obter_pf_usuario(:new.nm_usuario,'C'),
			'Gerado pela etapa do estagio'
			);
	end if;

	--Lhalves OS 789476 em 13/10/2014 - A senha e a guia devem ser atualizados tambem no anexo, mesmo se ja liberado e autorizado.
	if	(:new.cd_autorizacao <> :old.cd_autorizacao) or
		(:new.cd_autorizacao is not null and :old.cd_autorizacao is null) then

		open C02;
		loop
		fetch C02 into
			nr_seq_anexo_tiss_w;
		exit when C02%notfound;
			begin

			update	tiss_anexo_guia
			set	nr_guia_principal	= nvl(:new.cd_autorizacao,nr_guia_principal)
			where	nr_sequencia		= nr_seq_anexo_tiss_w;

			end;
		end loop;
		close C02;
	end if;

	if	(:new.cd_senha <> :old.cd_senha) or
		(:new.cd_senha is not null and :old.cd_senha is null)then

		open C02;
		loop
		fetch C02 into
			nr_seq_anexo_tiss_w;
		exit when C02%notfound;
			begin

			update	tiss_anexo_guia
			set	cd_senha	= nvl(:new.cd_senha,cd_senha)
			where	nr_sequencia	= nr_seq_anexo_tiss_w;

			end;
		end loop;
		close C02;
	end if;

	begin
	if (nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_ATENDIMENTO - Antes: '||:old.nr_atendimento||' Depois: '||:new.nr_atendimento||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_autorizacao,0) <> nvl(:old.nr_seq_autorizacao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AUTORIZACAO - Antes: '||:old.nr_seq_autorizacao||' Depois: '||:new.nr_seq_autorizacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_convenio,0) <> nvl(:old.cd_convenio,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_CONVENIO - Antes: '||:old.cd_convenio||' Depois: '||:new.cd_convenio||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_autorizacao,0) <> nvl(:old.cd_autorizacao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_AUTORIZACAO - Antes: '||:old.cd_autorizacao||' Depois: '||:new.cd_autorizacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_autorizacao,dt_parametro_w) <> nvl(:old.dt_autorizacao,dt_parametro_w)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DT_AUTORIZACAO - Antes: '||:old.dt_autorizacao||' Depois: '||:new.dt_autorizacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_inicio_vigencia,dt_parametro_w) <> nvl(:old.dt_inicio_vigencia,dt_parametro_w)) then
		ds_historico_w := substr(ds_historico_w||'Campo: DT_INICIO_VIGENCIA - Antes: '||:old.dt_inicio_vigencia||' Depois: '||:new.dt_inicio_vigencia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_fim_vigencia,dt_parametro_w) <> nvl(:old.dt_fim_vigencia,dt_parametro_w)) then
		ds_historico_w := substr(ds_historico_w||'Campo: DT_FIM_VIGENCIA - Antes: '||:old.dt_fim_vigencia||' Depois: '||:new.dt_fim_vigencia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nm_responsavel,0) <> nvl(:old.nm_responsavel,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NM_RESPONSAVEL - Antes: '||:old.nm_responsavel||' Depois: '||:new.nm_responsavel||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ds_observacao,0) <> nvl(:old.ds_observacao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: DS_OBSERVACAO - Antes: '||:old.ds_observacao||' Depois: '||:new.ds_observacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_senha,0) <> nvl(:old.cd_senha,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_SENHA - Antes: '||:old.cd_senha||' Depois: '||:new.cd_senha||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_procedimento_principal,0) <> nvl(:old.cd_procedimento_principal,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_PROCEDIMENTO_PRINCIPAL - Antes: '||:old.cd_procedimento_principal||' Depois: '||:new.cd_procedimento_principal||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_origem_proced,0) <> nvl(:old.ie_origem_proced,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: IE_ORIGEM_PROCED - Antes: '||:old.ie_origem_proced||' Depois: '||:new.ie_origem_proced||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_pedido_medico,dt_parametro_w) <> nvl(:old.dt_pedido_medico,dt_parametro_w)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DT_PEDIDO_MEDICO - Antes: '||:old.dt_pedido_medico||' Depois: '||:new.dt_pedido_medico||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_medico_solicitante,0) <> nvl(:old.cd_medico_solicitante,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: CD_MEDICO_SOLICITANTE - Antes: '||:old.cd_medico_solicitante||' Depois: '||:new.cd_medico_solicitante||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_tipo_guia,0) <> nvl(:old.ie_tipo_guia,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: IE_TIPO_GUIA - Antes: '||:old.ie_tipo_guia||' Depois: '||:new.ie_tipo_guia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.qt_dia_autorizado,0) <> nvl(:old.qt_dia_autorizado,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: QT_DIA_AUTORIZADO - Antes: '||:old.qt_dia_autorizado||' Depois: '||:new.qt_dia_autorizado||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_prescricao,0) <> nvl(:old.nr_prescricao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_PRESCRICAO - Antes: '||:old.nr_prescricao||' Depois: '||:new.nr_prescricao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_envio,dt_parametro_w) <> nvl(:old.dt_envio,dt_parametro_w)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DT_ENVIO - Antes: '||:old.dt_envio||' Depois: '||:new.dt_envio||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_retorno,dt_parametro_w) <> nvl(:old.dt_retorno,dt_parametro_w)) then
		ds_historico_w := substr(ds_historico_w||'Campo: DT_RETORNO - Antes: '||:old.dt_retorno||' Depois: '||:new.dt_retorno||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_estagio,0) <> nvl(:old.nr_seq_estagio,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_ESTAGIO - Antes: '||:old.nr_seq_estagio||' Depois: '||:new.nr_seq_estagio||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_tipo_autorizacao,0) <> nvl(:old.ie_tipo_autorizacao,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: IE_TIPO_AUTORIZACAO - Antes: '||:old.ie_tipo_autorizacao||' Depois: '||:new.ie_tipo_autorizacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_tipo_dia,0) <> nvl(:old.ie_tipo_dia,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: IE_TIPO_DIA - Antes: '||:old.ie_tipo_dia||' Depois: '||:new.ie_tipo_dia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_tipo_acomodacao,0) <> nvl(:old.cd_tipo_acomodacao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_TIPO_ACOMODACAO - Antes: '||:old.cd_tipo_acomodacao||' Depois: '||:new.cd_tipo_acomodacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_autor_cirurgia,0) <> nvl(:old.nr_seq_autor_cirurgia,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_AUTOR_CIRURGIA - Antes: '||:old.nr_seq_autor_cirurgia||' Depois: '||:new.nr_seq_autor_cirurgia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ds_indicacao,0) <> nvl(:old.ds_indicacao,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DS_INDICACAO - Antes: '||:old.ds_indicacao||' Depois: '||:new.ds_indicacao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_geracao_cih,dt_parametro_w) <> nvl(:old.dt_geracao_cih,dt_parametro_w)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DT_GERACAO_CIH - Antes: '||:old.dt_geracao_cih||' Depois: '||:new.dt_geracao_cih||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.qt_dia_solicitado,0) <> nvl(:old.qt_dia_solicitado,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: QT_DIA_SOLICITADO - Antes: '||:old.qt_dia_solicitado||' Depois: '||:new.qt_dia_solicitado||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_proc_interno,0) <> nvl(:old.nr_seq_proc_interno,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_PROC_INTERNO - Antes: '||:old.nr_seq_proc_interno||' Depois: '||:new.nr_seq_proc_interno||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_procedimento_convenio,0) <> nvl(:old.cd_procedimento_convenio,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: CD_PROCEDIMENTO_CONVENIO - Antes: '||:old.cd_procedimento_convenio||' Depois: '||:new.cd_procedimento_convenio||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_entrada_prevista,dt_parametro_w) <> nvl(:old.dt_entrada_prevista,dt_parametro_w)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DT_ENTRADA_PREVISTA - Antes: '||:old.dt_entrada_prevista||' Depois: '||:new.dt_entrada_prevista||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_gestao,0) <> nvl(:old.nr_seq_gestao,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_GESTAO - Antes: '||:old.nr_seq_gestao||' Depois: '||:new.nr_seq_gestao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_carater_int_tiss,0) <> nvl(:old.ie_carater_int_tiss,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: IE_CARATER_INT_TISS - Antes: '||:old.ie_carater_int_tiss||' Depois: '||:new.ie_carater_int_tiss||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_autor_origem,0) <> nvl(:old.nr_seq_autor_origem,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_AUTOR_ORIGEM - Antes: '||:old.nr_seq_autor_origem||' Depois: '||:new.nr_seq_autor_origem||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ie_resp_autor,0) <> nvl(:old.ie_resp_autor,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: IE_RESP_AUTOR - Antes: '||:old.ie_resp_autor||' Depois: '||:new.ie_resp_autor||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_autor_desdob,0) <> nvl(:old.nr_seq_autor_desdob,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_AUTOR_DESDOB - Antes: '||:old.nr_seq_autor_desdob||' Depois: '||:new.nr_seq_autor_desdob||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_cgc_prestador,0) <> nvl(:old.cd_cgc_prestador,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_CGC_PRESTADOR - Antes: '||:old.cd_cgc_prestador||' Depois: '||:new.cd_cgc_prestador||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_paciente_setor,0) <> nvl(:old.nr_seq_paciente_setor,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_PACIENTE_SETOR - Antes: '||:old.nr_seq_paciente_setor||' Depois: '||:new.nr_seq_paciente_setor||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_ciclo,0) <> nvl(:old.nr_ciclo,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_CICLO - Antes: '||:old.nr_ciclo||' Depois: '||:new.nr_ciclo||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_age_integ,0) <> nvl(:old.nr_seq_age_integ,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AGE_INTEG - Antes: '||:old.nr_seq_age_integ||' Depois: '||:new.nr_seq_age_integ||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_paciente,0) <> nvl(:old.nr_seq_paciente,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_PACIENTE - Antes: '||:old.nr_seq_paciente||' Depois: '||:new.nr_seq_paciente||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_medico_solic_tiss,0) <> nvl(:old.cd_medico_solic_tiss,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_MEDICO_SOLIC_TISS - Antes: '||:old.cd_medico_solic_tiss||' Depois: '||:new.cd_medico_solic_tiss||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_classif,0) <> nvl(:old.nr_seq_classif,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_CLASSIF - Antes: '||:old.nr_seq_classif||' Depois: '||:new.nr_seq_classif||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_interno_conta_ref,0) <> nvl(:old.nr_interno_conta_ref,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_INTERNO_CONTA_REF - Antes: '||:old.nr_interno_conta_ref||' Depois: '||:new.nr_interno_conta_ref||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_agenda_proc,0) <> nvl(:old.nr_seq_agenda_proc,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AGENDA_PROC - Antes: '||:old.nr_seq_agenda_proc||' Depois: '||:new.nr_seq_agenda_proc||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_agenda,0) <> nvl(:old.nr_seq_agenda,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AGENDA - Antes: '||:old.nr_seq_agenda||' Depois: '||:new.nr_seq_agenda||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.qt_dias_prazo,0) <> nvl(:old.qt_dias_prazo,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: QT_DIAS_PRAZO - Antes: '||:old.qt_dias_prazo||' Depois: '||:new.qt_dias_prazo||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_pessoa_fisica,0) <> nvl(:old.cd_pessoa_fisica,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: CD_PESSOA_FISICA - Antes: '||:old.cd_pessoa_fisica||' Depois: '||:new.cd_pessoa_fisica||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ds_dia_ciclo,0) <> nvl(:old.ds_dia_ciclo,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DS_DIA_CICLO - Antes: '||:old.ds_dia_ciclo||' Depois: '||:new.ds_dia_ciclo||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_setor_origem,0) <> nvl(:old.cd_setor_origem,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: CD_SETOR_ORIGEM - Antes: '||:old.cd_setor_origem||' Depois: '||:new.cd_setor_origem||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.cd_senha_provisoria,0) <> nvl(:old.cd_senha_provisoria,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: CD_SENHA_PROVISORIA - Antes: '||:old.cd_senha_provisoria||' Depois: '||:new.cd_senha_provisoria||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.dt_referencia,dt_parametro_w) <> nvl(:old.dt_referencia,dt_parametro_w)) then
		ds_historico_w := substr(ds_historico_w||'Campo: DT_REFERENCIA - Antes: '||:old.dt_referencia||' Depois: '||:new.dt_referencia||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_auditoria,0) <> nvl(:old.nr_seq_auditoria,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AUDITORIA - Antes: '||:old.nr_seq_auditoria||' Depois: '||:new.nr_seq_auditoria||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.ds_tarja_cartao,0) <> nvl(:old.ds_tarja_cartao,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: DS_TARJA_CARTAO - Antes: '||:old.ds_tarja_cartao||' Depois: '||:new.ds_tarja_cartao||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_apres,0) <> nvl(:old.nr_seq_apres,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_APRES - Antes: '||:old.nr_seq_apres||' Depois: '||:new.nr_seq_apres||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_regra_autor,0) <> nvl(:old.nr_seq_regra_autor,0)) then
		ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_REGRA_AUTOR - Antes: '||:old.nr_seq_regra_autor||' Depois: '||:new.nr_seq_regra_autor||chr(13)||chr(10),1,4000);
	end if;
	if (nvl(:new.nr_seq_guia_plano,0) <> nvl(:old.nr_seq_guia_plano,0)) then
		ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_GUIA_PLANO - Antes: '||:old.nr_seq_guia_plano||' Depois: '||:new.nr_seq_guia_plano||chr(13)||chr(10),1,4000);
	end if;
	exception
	when others then
		ds_historico_w := 'X';
	end;

	if (nvl(ds_historico_w,'X') <> 'X') then
		gravar_autor_conv_log_alter(:new.nr_sequencia,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.AUTORIZACAO_CONVENIO ADD (
  CONSTRAINT AUTCONV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONVENIO ADD (
  CONSTRAINT AUTCONV_MED_EXEC_FK 
 FOREIGN KEY (CD_MEDICO_EXEC_AGENDA) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT AUTCONV_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_RESP) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT AUTCONV_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_RESP) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUTCONV_RXTTRAT_FK 
 FOREIGN KEY (NR_SEQ_RXT_TRATAMENTO) 
 REFERENCES TASY.RXT_TRATAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGE_INTEG) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_SOLIC_TISS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCONV_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_CLAUTOR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_AUTORIZACAO (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA_REF) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT AUTCONV_AGEPAPR_FK 
 FOREIGN KEY (NR_SEQ_AGENDA, NR_SEQ_AGENDA_PROC) 
 REFERENCES TASY.AGENDA_PACIENTE_PROC (NR_SEQUENCIA,NR_SEQ_AGENDA),
  CONSTRAINT AUTCONV_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCONV_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PLANO) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AUTCONV_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_PAC) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT AUTCONV_AUDCOPA_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.AUDITORIA_CONTA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ORIGEM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUTCONV_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AUTCONV_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLICITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT AUTCONV_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_PRINCIPAL, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AUTCONV_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT AUTCONV_ESTAUTO_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.ESTAGIO_AUTORIZACAO (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT AUTCONV_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_AUTCONV_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_ORIGEM) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONSULTA) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_AUTCIRU_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_CIRURGIA) 
 REFERENCES TASY.AUTORIZACAO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_GESVAGA_FK 
 FOREIGN KEY (NR_SEQ_GESTAO) 
 REFERENCES TASY.GESTAO_VAGA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCONV_AUTCONV_FK2 
 FOREIGN KEY (NR_SEQ_AUTOR_DESDOB) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA),
  CONSTRAINT AUTCONV_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT AUTCONV_PACSETO_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE_SETOR) 
 REFERENCES TASY.PACIENTE_SETOR (NR_SEQ_PACIENTE));

GRANT SELECT ON TASY.AUTORIZACAO_CONVENIO TO NIVEL_1;

