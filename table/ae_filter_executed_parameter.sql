ALTER TABLE TASY.AE_FILTER_EXECUTED_PARAMETER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_FILTER_EXECUTED_PARAMETER CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_FILTER_EXECUTED_PARAMETER
(
  EVENT_ID              VARCHAR2(36 BYTE)       NOT NULL,
  FILTER_PARAMETER_SEQ  NUMBER(10)              NOT NULL,
  DESCRIPTION           VARCHAR2(1024 BYTE),
  PARAMETER_NAME        VARCHAR2(1024 BYTE),
  PARAMETER_NUMBER      NUMBER(38,16),
  PARAMETER_VARCHAR     VARCHAR2(4000 BYTE),
  PARAMETER_TIMESTAMP   TIMESTAMP(6),
  PARAMETER_CLOB        CLOB,
  PARAMETER_BLOB        BLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (PARAMETER_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (PARAMETER_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AE_FILTER_EXECUTED_PARAM ON TASY.AE_FILTER_EXECUTED_PARAMETER
(EVENT_ID, FILTER_PARAMETER_SEQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_FILTER_EXECUTED_PARAMETER ADD (
  CONSTRAINT PK_AE_FILTER_EXECUTED_PARAM
 PRIMARY KEY
 (EVENT_ID, FILTER_PARAMETER_SEQ)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_FILTER_EXECUTED_PARAMETER TO NIVEL_1;

