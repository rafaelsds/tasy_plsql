ALTER TABLE TASY.PESSOA_FISICA_EGK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_EGK CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_EGK
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  VERSICHERTEN_ID             VARCHAR2(12 BYTE),
  GEBURTSDATUM                DATE,
  VORNAME                     VARCHAR2(45 BYTE),
  NACHNAME                    VARCHAR2(45 BYTE),
  GESCHLECHT                  VARCHAR2(1 BYTE),
  VORSATZWORT                 VARCHAR2(20 BYTE),
  NAMENSZUSATZ                VARCHAR2(20 BYTE),
  TITEL                       VARCHAR2(20 BYTE),
  POSTLEITZAHL_SA             VARCHAR2(10 BYTE),
  ORT_SA                      VARCHAR2(40 BYTE),
  STRASSE_SA                  VARCHAR2(46 BYTE),
  HAUSNUMMER_SA               VARCHAR2(9 BYTE),
  ANSCHRIFTENZUSATZ_SA        VARCHAR2(40 BYTE),
  WOHNSITZLAENDERCODE_SA      VARCHAR2(3 BYTE),
  POSTLEITZAHL_PA             VARCHAR2(10 BYTE),
  ORT_PA                      VARCHAR2(40 BYTE),
  POSTFACH_PA                 VARCHAR2(8 BYTE),
  WOHNSITZLAENDERCODE_PA      VARCHAR2(8 BYTE),
  VERSICHERUNGSSCHUTZ_BEGINN  DATE,
  VERSICHERUNGSSCHUTZ_ENDE    DATE,
  KOSTENTRAEGER_KENNUNG       NUMBER(10),
  KOSTENTRAEGER_LAENDERCODE   VARCHAR2(3 BYTE),
  KOSTENTRAEGER_NAME          VARCHAR2(45 BYTE),
  ABRECHNENDER_KENNUNG        NUMBER(10),
  ABRECHNENDER_LAENDERCODE    VARCHAR2(3 BYTE),
  ABRECHNENDER_NAME           VARCHAR2(45 BYTE),
  BESONDERE_PERSONENGRUPPE    VARCHAR2(2 BYTE),
  DMP_KENNZEICHNUNG           VARCHAR2(2 BYTE),
  VERSICHERTENART             VARCHAR2(1 BYTE),
  ZUZAHLUNGSSTATUS            VARCHAR2(1 BYTE),
  GUELTIG_BIS                 DATE,
  ZUSATZINFOS_WOP             VARCHAR2(5 BYTE),
  RUHEN_BEGINN                DATE,
  RUHEN_ENDE                  DATE,
  RUHEN_ARTDESRUHENS          VARCHAR2(1 BYTE),
  SELEKTIV_AERZTLICH          VARCHAR2(1 BYTE),
  SELEKTIV_ZAHNAERZTLICH      VARCHAR2(1 BYTE),
  SELEKTIV_ART                VARCHAR2(1 BYTE),
  ZAHNAERZTLICHEVERSORGUNG    VARCHAR2(1 BYTE),
  STATIONAERERBEREICH         VARCHAR2(1 BYTE),
  VERANLASSTELEISTUNGEN       VARCHAR2(1 BYTE),
  AERZTLICHEVERSORGUNG        VARCHAR2(1 BYTE),
  DS_XML_PESSOA               CLOB,
  DS_XML_CONVENIO             CLOB,
  DS_CONTEUDO_KVK             VARCHAR2(4000 BYTE),
  VERSICHERTENSTATUS_KVK      VARCHAR2(4 BYTE),
  STATUSERGANZUNG_KVK         VARCHAR2(3 BYTE),
  IE_TIPO_CARTAO              VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFEGK_PESFISI_FK_I ON TASY.PESSOA_FISICA_EGK
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFEGK_PK ON TASY.PESSOA_FISICA_EGK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_EGK_AFTER
after insert or update ON TASY.PESSOA_FISICA_EGK for each row
declare
reg_integracao_p	gerar_int_padrao.reg_integracao;
nr_sequencia_w		Varchar2(35);
ie_gravar_w		varchar2(1);
qt_eventos_w		Number(10)	:= 0;
qt_registros_w		number(10)	:= 0;
ie_inserindo_conv_w	number(10)	:= 0;

begin
null;
/* OS 2016648,  evento n�o ser� mais agendado nesta trigger.
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') and
	(:new.cd_pessoa_fisica is not null) then
	begin
	select	1
	into	qt_eventos_w
	from	intpd_eventos
	where	ie_evento	= '360'
	and	ie_situacao	= 'A'
	and	rownum		= 1;
	exception
	when others then
		qt_eventos_w	:= 0;
	end;

	begin
	select	1
	into	qt_registros_w
	from	atendimento_paciente a,
		atend_paciente_unidade b,
		episodio_paciente c
	where	a.cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	a.dt_cancelamento	is null
	and	a.nr_seq_episodio	= c.nr_sequencia
	and	c.dt_cancelamento	is null
	and	c.dt_fim_episodio	is null
	and	a.nr_atendimento	= b.nr_atendimento;
	exception
	when others then
		qt_registros_w	:= 0;
	end;

	if	((qt_eventos_w > 0) and
		(qt_registros_w > 0)) then
		if	(inserting) then
			reg_integracao_p.nr_seq_agrupador	:= :new.cd_pessoa_fisica;
			reg_integracao_p.cd_pessoa_fisica	:= :new.cd_pessoa_fisica;
			reg_integracao_p.ie_status		:= 'P';
			reg_integracao_p.ie_operacao		:= 'I';
			nr_sequencia_w				:= :new.nr_sequencia;

			gerar_int_padrao.gravar_integracao('360', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);

		elsif	(updating) then

			reg_integracao_p.nr_seq_agrupador	:= :new.cd_pessoa_fisica;
			reg_integracao_p.cd_pessoa_fisica	:= :new.cd_pessoa_fisica;
			reg_integracao_p.ie_status		:= 'P';
			reg_integracao_p.ie_operacao		:= 'A';
			nr_sequencia_w				:= :new.nr_sequencia;

			gerar_int_padrao.gravar_integracao('360', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);

		end if;
	end if;
end if;*/

end PESSOA_FISICA_EGK_AFTER;
/


ALTER TABLE TASY.PESSOA_FISICA_EGK ADD (
  CONSTRAINT PFEGK_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PESSOA_FISICA_EGK ADD (
  CONSTRAINT PFEGK_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_FISICA_EGK TO NIVEL_1;

