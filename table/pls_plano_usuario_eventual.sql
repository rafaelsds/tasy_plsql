ALTER TABLE TASY.PLS_PLANO_USUARIO_EVENTUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PLANO_USUARIO_EVENTUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PLANO_USUARIO_EVENTUAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_PLANO          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA    DATE                    NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONGENERE      NUMBER(10),
  DT_FIM_VIGENCIA       DATE,
  CD_OPERADORA_EMPRESA  NUMBER(10),
  QT_POSICAO_INICIAL    NUMBER(5),
  QT_POSICAO_FINAL      NUMBER(5),
  IE_BLOQ_ALTER_PORTAL  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPLUE_ESTABEL_FK_I ON TASY.PLS_PLANO_USUARIO_EVENTUAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLUE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPLUE_PK ON TASY.PLS_PLANO_USUARIO_EVENTUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLUE_PLSCONG_FK_I ON TASY.PLS_PLANO_USUARIO_EVENTUAL
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLUE_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPLUE_PLSPLAN_FK_I ON TASY.PLS_PLANO_USUARIO_EVENTUAL
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLUE_PLSPLAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PLANO_USUARIO_EVENTUAL ADD (
  CONSTRAINT PLSPLUE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PLANO_USUARIO_EVENTUAL ADD (
  CONSTRAINT PLSPLUE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPLUE_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSPLUE_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PLANO_USUARIO_EVENTUAL TO NIVEL_1;

