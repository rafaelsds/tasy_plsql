ALTER TABLE TASY.ESCALA_GMFM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_GMFM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_GMFM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_ITEM_1              NUMBER(1),
  IE_ITEM_2              NUMBER(1),
  IE_ITEM_3              NUMBER(1),
  IE_ITEM_4              NUMBER(1),
  IE_ITEM_5              NUMBER(1),
  IE_ITEM_6              NUMBER(1),
  IE_ITEM_7              NUMBER(1),
  IE_ITEM_8              NUMBER(1),
  IE_ITEM_9              NUMBER(1),
  IE_ITEM_10             NUMBER(1),
  IE_ITEM_11             NUMBER(1),
  IE_ITEM_12             NUMBER(1),
  IE_ITEM_13             NUMBER(1),
  IE_ITEM_14             NUMBER(1),
  IE_ITEM_15             NUMBER(1),
  IE_ITEM_16             NUMBER(1),
  IE_ITEM_17             NUMBER(1),
  QT_DIMENSAO_A          NUMBER(5),
  IE_ITEM_18             NUMBER(1),
  IE_ITEM_19             NUMBER(1),
  IE_ITEM_20             NUMBER(1),
  IE_ITEM_21             NUMBER(1),
  IE_ITEM_22             NUMBER(1),
  IE_ITEM_23             NUMBER(1),
  IE_ITEM_24             NUMBER(1),
  IE_ITEM_25             NUMBER(1),
  IE_ITEM_26             NUMBER(1),
  IE_ITEM_27             NUMBER(1),
  IE_ITEM_28             NUMBER(1),
  IE_ITEM_29             NUMBER(1),
  IE_ITEM_30             NUMBER(1),
  IE_ITEM_31             NUMBER(1),
  IE_ITEM_32             NUMBER(1),
  IE_ITEM_33             NUMBER(1),
  IE_ITEM_34             NUMBER(1),
  IE_ITEM_35             NUMBER(1),
  IE_ITEM_36             NUMBER(1),
  IE_ITEM_37             NUMBER(1),
  QT_DIMENSAO_B          NUMBER(5),
  IE_ITEM_38             NUMBER(1),
  IE_ITEM_39             NUMBER(1),
  IE_ITEM_40             NUMBER(1),
  IE_ITEM_41             NUMBER(1),
  IE_ITEM_42             NUMBER(1),
  IE_ITEM_43             NUMBER(1),
  IE_ITEM_44             NUMBER(1),
  IE_ITEM_45             NUMBER(1),
  IE_ITEM_46             NUMBER(1),
  IE_ITEM_47             NUMBER(1),
  IE_ITEM_48             NUMBER(1),
  IE_ITEM_49             NUMBER(1),
  IE_ITEM_50             NUMBER(1),
  IE_ITEM_51             NUMBER(1),
  QT_DIMENSAO_C          NUMBER(5),
  IE_ITEM_52             NUMBER(1),
  IE_ITEM_53             NUMBER(1),
  IE_ITEM_54             NUMBER(1),
  IE_ITEM_55             NUMBER(1),
  IE_ITEM_56             NUMBER(1),
  IE_ITEM_57             NUMBER(1),
  IE_ITEM_58             NUMBER(1),
  IE_ITEM_59             NUMBER(1),
  IE_ITEM_60             NUMBER(1),
  IE_ITEM_61             NUMBER(1),
  IE_ITEM_62             NUMBER(1),
  IE_ITEM_63             NUMBER(1),
  IE_ITEM_64             NUMBER(1),
  QT_DIMENSAO_D          NUMBER(5),
  IE_ITEM_65             NUMBER(1),
  IE_ITEM_66             NUMBER(1),
  IE_ITEM_67             NUMBER(1),
  IE_ITEM_68             NUMBER(1),
  IE_ITEM_69             NUMBER(1),
  IE_ITEM_70             NUMBER(1),
  IE_ITEM_71             NUMBER(1),
  IE_ITEM_72             NUMBER(1),
  IE_ITEM_73             NUMBER(1),
  IE_ITEM_74             NUMBER(1),
  IE_ITEM_75             NUMBER(1),
  IE_ITEM_76             NUMBER(1),
  IE_ITEM_77             NUMBER(1),
  IE_ITEM_78             NUMBER(1),
  IE_ITEM_79             NUMBER(1),
  IE_ITEM_80             NUMBER(1),
  IE_ITEM_81             NUMBER(1),
  IE_ITEM_82             NUMBER(1),
  IE_ITEM_83             NUMBER(1),
  IE_ITEM_84             NUMBER(1),
  IE_ITEM_85             NUMBER(1),
  IE_ITEM_86             NUMBER(1),
  IE_ITEM_87             NUMBER(1),
  IE_ITEM_88             NUMBER(1),
  QT_DIMENSAO_E          NUMBER(5,2),
  PR_DIMENSAO_A          NUMBER(5,2),
  PR_DIMENSAO_B          NUMBER(5,2),
  PR_DIMENSAO_C          NUMBER(5,2),
  PR_DIMENSAO_D          NUMBER(5,2),
  PR_DIMENSAO_E          NUMBER(5,2),
  PR_TOTAL               NUMBER(5,2),
  IE_META_A              VARCHAR2(1 BYTE),
  IE_META_B              VARCHAR2(1 BYTE),
  IE_META_C              VARCHAR2(1 BYTE),
  IE_META_D              VARCHAR2(1 BYTE),
  IE_META_E              VARCHAR2(1 BYTE),
  PR_TOTAL_META          NUMBER(5,2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCGMFM_ATEPACI_FK_I ON TASY.ESCALA_GMFM
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCGMFM_PESFISI_FK_I ON TASY.ESCALA_GMFM
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCGMFM_PK ON TASY.ESCALA_GMFM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_GMFM_ATUAL
before insert or update ON TASY.ESCALA_GMFM for each row
declare
qt_dimensao_a_w		number(10,0) := 0;
qt_dimensao_b_w		number(10,0) := 0;
qt_dimensao_c_w		number(10,0) := 0;
qt_dimensao_d_w		number(10,0) := 0;
qt_dimensao_e_w		number(10,0) := 0;
pr_dimensao_a_w		number(10,2) := 0;
pr_dimensao_b_w		number(10,2) := 0;
pr_dimensao_c_w		number(10,2) := 0;
pr_dimensao_d_w		number(10,2) := 0;
pr_dimensao_e_w		number(10,2) := 0;
pr_total_w		number(10,2) := 0;
pr_total_meta_w		number(10,2) := 0;
qt_calculo_w		number(1)    := 0;

begin
qt_dimensao_a_w		:= 	:new.IE_ITEM_1 + :new.IE_ITEM_2 + :new.IE_ITEM_3 + :new.IE_ITEM_4 +
				:new.IE_ITEM_5 + :new.IE_ITEM_6 + :new.IE_ITEM_7 + :new.IE_ITEM_8 +
				:new.IE_ITEM_9 + :new.IE_ITEM_10 + :new.IE_ITEM_11 + :new.IE_ITEM_12 +
				:new.IE_ITEM_13 + :new.IE_ITEM_14 + :new.IE_ITEM_15 + :new.IE_ITEM_16 + :new.IE_ITEM_17;

:new.QT_DIMENSAO_A	:=	qt_dimensao_a_w;

qt_dimensao_b_w		:= 	:new.IE_ITEM_18 + :new.IE_ITEM_19 + :new.IE_ITEM_20 + :new.IE_ITEM_21 +
				:new.IE_ITEM_22 + :new.IE_ITEM_23 + :new.IE_ITEM_24 + :new.IE_ITEM_25 +
				:new.IE_ITEM_26 + :new.IE_ITEM_27 + :new.IE_ITEM_28 + :new.IE_ITEM_29 +
				:new.IE_ITEM_30 + :new.IE_ITEM_31 + :new.IE_ITEM_32 + :new.IE_ITEM_33 +
				:new.IE_ITEM_34 + :new.IE_ITEM_35 + :new.IE_ITEM_36 + :new.IE_ITEM_37;

:new.QT_DIMENSAO_B	:= 	qt_dimensao_b_w;

qt_dimensao_c_w		:= 	:new.IE_ITEM_38 + :new.IE_ITEM_39 + :new.IE_ITEM_40 + :new.IE_ITEM_41 +
				:new.IE_ITEM_42 + :new.IE_ITEM_43 + :new.IE_ITEM_44 + :new.IE_ITEM_45 +
				:new.IE_ITEM_46 + :new.IE_ITEM_47 + :new.IE_ITEM_48 + :new.IE_ITEM_49 +
				:new.IE_ITEM_50 + :new.IE_ITEM_51;

:new.QT_DIMENSAO_C	:= 	qt_dimensao_c_w;

qt_dimensao_d_w		:= 	:new.IE_ITEM_52 + :new.IE_ITEM_53 + :new.IE_ITEM_54 + :new.IE_ITEM_55 +
				:new.IE_ITEM_56 + :new.IE_ITEM_57 + :new.IE_ITEM_58 + :new.IE_ITEM_59 +
				:new.IE_ITEM_60 + :new.IE_ITEM_61 + :new.IE_ITEM_62 + :new.IE_ITEM_63 +
				:new.IE_ITEM_64;

:new.QT_DIMENSAO_D	:= 	qt_dimensao_d_w;

qt_dimensao_e_w		:= 	:new.IE_ITEM_65 + :new.IE_ITEM_66 + :new.IE_ITEM_67 + :new.IE_ITEM_68 +
				:new.IE_ITEM_69 + :new.IE_ITEM_70 + :new.IE_ITEM_71 + :new.IE_ITEM_72 +
				:new.IE_ITEM_73 + :new.IE_ITEM_74 + :new.IE_ITEM_75 + :new.IE_ITEM_76 +
				:new.IE_ITEM_77 + :new.IE_ITEM_78 + :new.IE_ITEM_79 + :new.IE_ITEM_80 +
				:new.IE_ITEM_81 + :new.IE_ITEM_82 + :new.IE_ITEM_83 + :new.IE_ITEM_84 +
				:new.IE_ITEM_85 + :new.IE_ITEM_86 + :new.IE_ITEM_87 + :new.IE_ITEM_88;

:new.QT_DIMENSAO_E	:= 	qt_dimensao_e_w;


pr_dimensao_a_w		:= 	((qt_dimensao_a_w/51)*100);
:new.PR_DIMENSAO_A	:= 	pr_dimensao_a_w;
pr_dimensao_b_w		:= 	((qt_dimensao_b_w/60)*100);
:new.PR_DIMENSAO_B	:= 	pr_dimensao_b_w;
pr_dimensao_c_w		:= 	((qt_dimensao_c_w/42)*100);
:new.PR_DIMENSAO_C	:= 	pr_dimensao_c_w;
pr_dimensao_d_w		:= 	((qt_dimensao_d_w/39)*100);
:new.PR_DIMENSAO_D	:= 	pr_dimensao_d_w;
pr_dimensao_e_w		:= 	((qt_dimensao_e_w/72)*100);
:new.PR_DIMENSAO_E	:= 	pr_dimensao_e_w;

if	(:new.IE_META_A = 'S') then
	pr_total_meta_w := pr_total_meta_w + pr_dimensao_a_w;
	qt_calculo_w := qt_calculo_w +1;
end if;
if 	(:new.IE_META_B = 'S') then
	pr_total_meta_w := pr_total_meta_w + pr_dimensao_b_w;
	qt_calculo_w := qt_calculo_w +1;
end if;
if 	(:new.IE_META_C = 'S') then
	pr_total_meta_w := pr_total_meta_w + pr_dimensao_c_w;
	qt_calculo_w := qt_calculo_w +1;
end if;
if 	(:new.IE_META_D = 'S') then
	pr_total_meta_w := pr_total_meta_w + pr_dimensao_d_w;
	qt_calculo_w := qt_calculo_w +1;
end if;
if 	(:new.IE_META_E = 'S') then
	pr_total_meta_w := pr_total_meta_w + pr_dimensao_e_w;
	qt_calculo_w := qt_calculo_w +1;
end if;

pr_total_meta_w		:=	Dividir( pr_total_meta_w,qt_calculo_w);
pr_total_w		:= 	((pr_dimensao_a_w + pr_dimensao_b_w + pr_dimensao_c_w + pr_dimensao_d_w + pr_dimensao_e_w)/5);

:new.PR_TOTAL		:= 	pr_total_w;
:new.PR_TOTAL_META	:=	pr_total_meta_w;

end;
/


ALTER TABLE TASY.ESCALA_GMFM ADD (
  CONSTRAINT ESCGMFM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_GMFM ADD (
  CONSTRAINT ESCGMFM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCGMFM_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_GMFM TO NIVEL_1;

