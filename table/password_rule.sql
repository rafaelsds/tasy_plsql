ALTER TABLE TASY.PASSWORD_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PASSWORD_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PASSWORD_RULE
(
  ID               VARCHAR2(36 BYTE)            NOT NULL,
  DT_CREATION      DATE                         NOT NULL,
  DT_MODIFICATION  DATE                         NOT NULL,
  VL_TYPE          NUMBER(2)                    NOT NULL,
  VL_OCCURRENCES   NUMBER(2)                    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PASRU_PK ON TASY.PASSWORD_RULE
(ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PASRU_UK ON TASY.PASSWORD_RULE
(VL_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PASSWORD_RULE ADD (
  CONSTRAINT PASRU_PK
 PRIMARY KEY
 (ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PASRU_UK
 UNIQUE (VL_TYPE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PASSWORD_RULE TO NIVEL_1;

