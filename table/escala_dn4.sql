ALTER TABLE TASY.ESCALA_DN4
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DN4 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DN4
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_SEQ_REG_ELEMENTO    NUMBER(10),
  IE_QUEIMACAO           VARCHAR2(1 BYTE),
  IE_RESULTADO           VARCHAR2(1 BYTE),
  IE_FRIO_DOLOROSA       VARCHAR2(1 BYTE),
  IE_CHOQUE_ELETRICO     VARCHAR2(1 BYTE),
  IE_FORMIGAMENTO        VARCHAR2(1 BYTE),
  IE_ALFINETADA          VARCHAR2(1 BYTE),
  IE_ADORMECIMENTO       VARCHAR2(1 BYTE),
  IE_COCEIRA             VARCHAR2(1 BYTE),
  IE_HIPOESTESIA_TOQUE   VARCHAR2(1 BYTE),
  IE_HIPOESTESIA_AGULHA  VARCHAR2(1 BYTE),
  IE_ESCOVACAO           VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCADN4_ATEPACI_FK_I ON TASY.ESCALA_DN4
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCADN4_PESFISI_FK_I ON TASY.ESCALA_DN4
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCADN4_PK ON TASY.ESCALA_DN4
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_dn4_atual
before insert or update ON TASY.ESCALA_DN4 for each row
declare

qt_resultado_w	number(5) := 0;

begin

if	(:new.IE_QUEIMACAO = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_FRIO_DOLOROSA = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_CHOQUE_ELETRICO = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_FORMIGAMENTO = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_ALFINETADA = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_ADORMECIMENTO = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_COCEIRA = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_HIPOESTESIA_TOQUE = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_HIPOESTESIA_AGULHA = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;

if	(:new.IE_ESCOVACAO = 'S') then
	qt_resultado_w := qt_resultado_w + 1;
end if;


if	(qt_resultado_w >= 4) then
	:new.IE_RESULTADO	:= 'P';
else
	:new.IE_RESULTADO 	:= 'N';
end if;


end;
/


ALTER TABLE TASY.ESCALA_DN4 ADD (
  CONSTRAINT ESCADN4_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_DN4 ADD (
  CONSTRAINT ESCADN4_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCADN4_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_DN4 TO NIVEL_1;

