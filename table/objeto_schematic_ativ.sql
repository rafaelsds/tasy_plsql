ALTER TABLE TASY.OBJETO_SCHEMATIC_ATIV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OBJETO_SCHEMATIC_ATIV CASCADE CONSTRAINTS;

CREATE TABLE TASY.OBJETO_SCHEMATIC_ATIV
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ATIVACAO          NUMBER(10)           NOT NULL,
  NR_SEQ_OBJETO_SCHEMATIC  NUMBER(10)           NOT NULL,
  DS_SQL                   VARCHAR2(2000 BYTE)  NOT NULL,
  NR_SEQ_APRES             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OBJSCHATI_OBJSCHE_FK_I ON TASY.OBJETO_SCHEMATIC_ATIV
(NR_SEQ_OBJETO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OBJSCHATI_PK ON TASY.OBJETO_SCHEMATIC_ATIV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHATI_TASIATI_FK_I ON TASY.OBJETO_SCHEMATIC_ATIV
(NR_SEQ_ATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OBJETO_SCHEMATIC_ATIV_tp  after update ON TASY.OBJETO_SCHEMATIC_ATIV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'OBJETO_SCHEMATIC_ATIV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'OBJETO_SCHEMATIC_ATIV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'OBJETO_SCHEMATIC_ATIV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OBJETO_SCHEMATIC_ATIV ADD (
  CONSTRAINT OBJSCHATI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OBJETO_SCHEMATIC_ATIV ADD (
  CONSTRAINT OBJSCHATI_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_OBJETO_SCHEMATIC) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA),
  CONSTRAINT OBJSCHATI_TASIATI_FK 
 FOREIGN KEY (NR_SEQ_ATIVACAO) 
 REFERENCES TASY.TABELA_SISTEMA_ATIVACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OBJETO_SCHEMATIC_ATIV TO NIVEL_1;

