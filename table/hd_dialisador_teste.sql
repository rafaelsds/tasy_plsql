ALTER TABLE TASY.HD_DIALISADOR_TESTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISADOR_TESTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISADOR_TESTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_DIALISE_DIALISADOR  NUMBER(10)         NOT NULL,
  NR_SEQ_DIALISE             NUMBER(10)         NOT NULL,
  NR_SEQ_DIALISADOR          NUMBER(10)         NOT NULL,
  NR_TESTE                   NUMBER(2),
  DT_TESTE                   DATE,
  IE_RESULTADO               VARCHAR2(2 BYTE),
  CD_PF_TESTE                VARCHAR2(10 BYTE),
  NR_SEQ_UNID_DIALISE        NUMBER(10)         NOT NULL,
  IE_POS_DIALISE             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIATE_ESTABEL_FK_I ON TASY.HD_DIALISADOR_TESTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIATE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIATE_HDDIADIA_FK_I ON TASY.HD_DIALISADOR_TESTE
(NR_SEQ_DIALISE_DIALISADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIATE_HDDIALE_FK_I ON TASY.HD_DIALISADOR_TESTE
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIATE_HDDIALI_FK_I ON TASY.HD_DIALISADOR_TESTE
(NR_SEQ_DIALISADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIATE_HDUNIDD_FK_I ON TASY.HD_DIALISADOR_TESTE
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIATE_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDIATE_PK ON TASY.HD_DIALISADOR_TESTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIATE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hd_dialisador_teste_befup_sign
before update ON TASY.HD_DIALISADOR_TESTE for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_teste = :new.nr_sequencia
and 	ie_opcao = 'TI'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia			<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec             <> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec                 <> :new.nm_usuario_nrec
		or :old.nr_teste                        <> :new.nr_teste
		or :old.dt_teste                        <> :new.dt_teste
		or :old.ie_resultado                    <> :new.ie_resultado
		or :old.cd_pf_teste                     <> :new.cd_pf_teste
		or :old.nr_seq_dialise                  <> :new.nr_seq_dialise
		or :old.nr_seq_dialisador               <> :new.nr_seq_dialisador
		or :old.nr_seq_dialise_dialisador       <> :new.nr_seq_dialise_dialisador
		or :old.cd_estabelecimento              <> :new.cd_estabelecimento
		or :old.nr_seq_unid_dialise             <> :new.nr_seq_unid_dialise
		or :old.ie_pos_dialise                  <> :new.ie_pos_dialise
		or (:old.nr_sequencia			is null		and	:new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec		is null		and	:new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec		is null		and	:new.nm_usuario_nrec		is not null)
		or (:old.nr_teste			is null		and	:new.nr_teste			is not null)
		or (:old.dt_teste			is null		and	:new.dt_teste			is not null)
		or (:old.ie_resultado			is null		and	:new.ie_resultado		is not null)
		or (:old.cd_pf_teste			is null		and	:new.cd_pf_teste		is not null)
		or (:old.nr_seq_dialise			is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_dialisador		is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_dialise_dialisador	is null		and	:new.nr_seq_dialise_dialisador	is not null)
		or (:old.cd_estabelecimento		is null		and	:new.cd_estabelecimento		is not null)
		or (:old.nr_seq_unid_dialise		is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.ie_pos_dialise			is null		and	:new.ie_pos_dialise		is not null)
		or (:old.dt_atualizacao_nrec		is not null	and	:new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec		is not null	and	:new.nm_usuario_nrec		is null)
		or (:old.nr_teste			is not null	and	:new.nr_teste			is null)
		or (:old.dt_teste			is not null	and	:new.dt_teste			is null)
		or (:old.ie_resultado			is not null	and	:new.ie_resultado		is null)
		or (:old.cd_pf_teste			is not null	and	:new.cd_pf_teste		is null)
		or (:old.nr_seq_dialise			is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_dialisador		is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_dialise_dialisador	is not null	and	:new.nr_seq_dialise_dialisador	is null)
		or (:old.cd_estabelecimento		is not null	and	:new.cd_estabelecimento		is null)
		or (:old.nr_seq_unid_dialise		is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.ie_pos_dialise			is not null	and	:new.ie_pos_dialise		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_teste = :new.nr_sequencia
and 	ie_opcao = 'TII'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia			<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec             <> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec                 <> :new.nm_usuario_nrec
		or :old.nr_teste                        <> :new.nr_teste
		or :old.dt_teste                        <> :new.dt_teste
		or :old.ie_resultado                    <> :new.ie_resultado
		or :old.cd_pf_teste                     <> :new.cd_pf_teste
		or :old.nr_seq_dialise                  <> :new.nr_seq_dialise
		or :old.nr_seq_dialisador               <> :new.nr_seq_dialisador
		or :old.nr_seq_dialise_dialisador       <> :new.nr_seq_dialise_dialisador
		or :old.cd_estabelecimento              <> :new.cd_estabelecimento
		or :old.nr_seq_unid_dialise             <> :new.nr_seq_unid_dialise
		or :old.ie_pos_dialise                  <> :new.ie_pos_dialise
		or (:old.nr_sequencia			is null		and	:new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec		is null		and	:new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec		is null		and	:new.nm_usuario_nrec		is not null)
		or (:old.nr_teste			is null		and	:new.nr_teste			is not null)
		or (:old.dt_teste			is null		and	:new.dt_teste			is not null)
		or (:old.ie_resultado			is null		and	:new.ie_resultado		is not null)
		or (:old.cd_pf_teste			is null		and	:new.cd_pf_teste		is not null)
		or (:old.nr_seq_dialise			is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_dialisador		is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_dialise_dialisador	is null		and	:new.nr_seq_dialise_dialisador	is not null)
		or (:old.cd_estabelecimento		is null		and	:new.cd_estabelecimento		is not null)
		or (:old.nr_seq_unid_dialise		is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.ie_pos_dialise			is null		and	:new.ie_pos_dialise		is not null)
		or (:old.dt_atualizacao_nrec		is not null	and	:new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec		is not null	and	:new.nm_usuario_nrec		is null)
		or (:old.nr_teste			is not null	and	:new.nr_teste			is null)
		or (:old.dt_teste			is not null	and	:new.dt_teste			is null)
		or (:old.ie_resultado			is not null	and	:new.ie_resultado		is null)
		or (:old.cd_pf_teste			is not null	and	:new.cd_pf_teste		is null)
		or (:old.nr_seq_dialise			is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_dialisador		is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_dialise_dialisador	is not null	and	:new.nr_seq_dialise_dialisador	is null)
		or (:old.cd_estabelecimento		is not null	and	:new.cd_estabelecimento		is null)
		or (:old.nr_seq_unid_dialise		is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.ie_pos_dialise			is not null	and	:new.ie_pos_dialise		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_DIALISADOR_TESTE ADD (
  CONSTRAINT HDDIATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISADOR_TESTE ADD (
  CONSTRAINT HDDIATE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HDDIATE_HDDIADIA_FK 
 FOREIGN KEY (NR_SEQ_DIALISE_DIALISADOR) 
 REFERENCES TASY.HD_DIALISE_DIALISADOR (NR_SEQUENCIA),
  CONSTRAINT HDDIATE_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIATE_HDDIALI_FK 
 FOREIGN KEY (NR_SEQ_DIALISADOR) 
 REFERENCES TASY.HD_DIALIZADOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HDDIATE_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISADOR_TESTE TO NIVEL_1;

