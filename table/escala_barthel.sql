ALTER TABLE TASY.ESCALA_BARTHEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_BARTHEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_BARTHEL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10),
  DT_LIBERACAO           DATE,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_ALIMENTACAO         NUMBER(2)              NOT NULL,
  IE_BANHO               NUMBER(1)              NOT NULL,
  IE_CUIDADO             NUMBER(1)              NOT NULL,
  IE_CAPACIDADE          NUMBER(2)              NOT NULL,
  IE_INTESTINAL          NUMBER(2)              NOT NULL,
  IE_URINARIO            NUMBER(2)              NOT NULL,
  IE_BANHEIRO            NUMBER(2)              NOT NULL,
  IE_TRANSPORTE          NUMBER(2)              NOT NULL,
  IE_MOBILIDADE          NUMBER(2)              NOT NULL,
  IE_ESCADAS             NUMBER(2)              NOT NULL,
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCBART_ATEPACI_FK_I ON TASY.ESCALA_BARTHEL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCBART_PESFISI_FK2_I ON TASY.ESCALA_BARTHEL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCBART_PK ON TASY.ESCALA_BARTHEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCBART_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_barthel_atual
before insert or update ON TASY.ESCALA_BARTHEL for each row
declare
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_liberacao is null)  then
	begin

	:new.qt_pontuacao := 	nvl(:new.ie_alimentacao,0)		+
				nvl(:new.ie_banho,0) 		+
				nvl(:new.ie_cuidado,0) 		+
				nvl(:new.ie_capacidade,0)		+
				nvl(:new.ie_intestinal,0) 		+
				nvl(:new.ie_urinario,0) 		+
				nvl(:new.ie_banheiro,0) 		+
				nvl(:new.ie_transporte,0) 		+
				nvl(:new.ie_mobilidade,0) 		+
				nvl(:new.ie_escadas,0);
	end;
end if;
<<Final>>
qt_reg_w	:= 0;

end escala_barthel_atual;
/


ALTER TABLE TASY.ESCALA_BARTHEL ADD (
  CONSTRAINT ESCBART_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_BARTHEL ADD (
  CONSTRAINT ESCBART_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCBART_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_BARTHEL TO NIVEL_1;

