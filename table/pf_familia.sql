ALTER TABLE TASY.PF_FAMILIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_FAMILIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_FAMILIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  CD_PESSOA_FAMILIA       VARCHAR2(10 BYTE),
  NR_SEQ_GRAU_PARENTESCO  NUMBER(10)            NOT NULL,
  IE_HABITACAO            VARCHAR2(1 BYTE)      NOT NULL,
  CD_PROFESSIONAL         VARCHAR2(10 BYTE),
  DS_GIVEN_NAME           VARCHAR2(255 BYTE),
  DS_FAMILY_NAME          VARCHAR2(255 BYTE),
  QT_AGE                  NUMBER(3),
  IE_GENDER               VARCHAR2(15 BYTE),
  CD_OCCUPATION           NUMBER(10),
  DT_DEATH                DATE,
  DS_OCCUPATION_FAMILY    VARCHAR2(255 BYTE),
  SI_FAMILIARITY          VARCHAR2(15 BYTE),
  SI_SITUATION            VARCHAR2(15 BYTE),
  SI_INLAW                VARCHAR2(15 BYTE),
  SI_BLOOD_TYPE           VARCHAR2(15 BYTE),
  SI_RH_FACTOR            VARCHAR2(15 BYTE),
  SI_KEY_PERSON           VARCHAR2(15 BYTE),
  NR_SEQ_MEMBER_SUP       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFFAMIL_CARGO_FK_I ON TASY.PF_FAMILIA
(CD_OCCUPATION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFFAMIL_PESFISI_FK_I ON TASY.PF_FAMILIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFFAMIL_PESFISI_FK2_I ON TASY.PF_FAMILIA
(CD_PESSOA_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFFAMIL_PESFISI_FK3_I ON TASY.PF_FAMILIA
(CD_PROFESSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFFAMIL_PFFAMIL_FK_I ON TASY.PF_FAMILIA
(NR_SEQ_MEMBER_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFFAMIL_PK ON TASY.PF_FAMILIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pf_familia_insert_update
before INSERT or update ON TASY.PF_FAMILIA FOR EACH ROW
DECLARE
qtd_member_w number(1);
dependence_w number(1);
ie_grau_parentesco_w GRAU_PARENTESCO.IE_GRAU_PARENTESCO%type;
pragma autonomous_transaction;

function get_qtd_member_family(member_type_p number,
							   ie_gender_p varchar default null)
return number
as
qtd_w number(1) := 0;
begin

	if (ie_gender_p is not null) then
		select count(*)
		into  qtd_w
		from pf_familia f,
		grau_parentesco g
		where f.cd_pessoa_fisica = :new.cd_pessoa_fisica
		and f.NR_SEQ_GRAU_PARENTESCO = g.nr_sequencia
		and f.ie_gender = ie_gender_p
		and f.nr_sequencia <> :new.nr_sequencia
		and g.IE_GRAU_PARENTESCO = member_type_p;
	else
		select count(*)
		into  qtd_w
		from pf_familia f,
		grau_parentesco g
		where f.cd_pessoa_fisica = :new.cd_pessoa_fisica
		and f.NR_SEQ_GRAU_PARENTESCO = g.nr_sequencia
		and f.nr_sequencia <> :new.nr_sequencia
		and g.IE_GRAU_PARENTESCO = member_type_p;
	end if;


	return qtd_w;

end get_qtd_member_family;

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
		if (inserting or ((updating) and ((:new.ie_gender <> :old.ie_gender) or (:new.NR_SEQ_GRAU_PARENTESCO <> :old.NR_SEQ_GRAU_PARENTESCO)))) then
			select    max(IE_GRAU_PARENTESCO) ie_grau_parentesco
			into    ie_grau_parentesco_w
			from    grau_parentesco g
			where   nr_sequencia = :new.NR_SEQ_GRAU_PARENTESCO;

			if (ie_grau_parentesco_w = 5) then -- 5 = Mae

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);

				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153133);
			end if;
			elsif (ie_grau_parentesco_w = 6) then -- 6 - Pai

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);

				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153135);
				end if;
			elsif (ie_grau_parentesco_w = 9) then -- 9 = Avo paterna(o)

				dependence_w := get_qtd_member_family(6); -- 6 = pai

				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153113);
				end if;

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);

				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153118);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153127);
					end if;
				end if;

			elsif (ie_grau_parentesco_w = 12) then -- 12 = Avo(o) materna(o)

				dependence_w := get_qtd_member_family(5); -- 5 = mae

				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153116);
				end if;

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);

				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153128);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153131);
					end if;
				end if;

			elsif (ie_grau_parentesco_w = 13) then -- 13 - Esposa(o)

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w);

				if (qtd_member_w > 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153136);
				end if;

			elsif (ie_grau_parentesco_w = 15) then -- 15 = Sogra(o)

				dependence_w := get_qtd_member_family(13); --13 - Esposa(o)

				if (dependence_w = 0) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1153117);
				end if;

				qtd_member_w := get_qtd_member_family(ie_grau_parentesco_w, :new.ie_gender);

				if (qtd_member_w > 0) then
					if (:new.ie_gender = 'M') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153582);
					elsif (:new.ie_gender = 'F') then
						Wheb_mensagem_pck.exibir_mensagem_abort(1153583);
					end if;
				end if;
			end if;
		end if;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pf_familia_before_del
before delete ON TASY.PF_FAMILIA for each row
declare
qtd_member_w		number(10) := 0;
ie_grau_parentesco_w	grau_parentesco.ie_grau_parentesco%type;

pragma autonomous_transaction;


function get_qtd_member_family(member_type_p number, nr_seq_member_sup_p number)
return number
as
qtd_w number(1) := 0;

begin

if	(nr_seq_member_sup_p is null) then
	select count(*)
	into	qtd_w
	from	pf_familia f,
		grau_parentesco g
	where	f.cd_pessoa_fisica = :old.cd_pessoa_fisica
	and	f.nr_seq_grau_parentesco = g.nr_sequencia
	and	f.nr_sequencia <> :old.nr_sequencia
	and	g.ie_grau_parentesco = member_type_p;

else
	select count(*)
	into	qtd_w
	from	pf_familia f,
		grau_parentesco g
	where	f.cd_pessoa_fisica = :old.cd_pessoa_fisica
	and	f.nr_seq_grau_parentesco = g.nr_sequencia
	and	f.nr_sequencia <> :old.nr_sequencia
	and	g.ie_grau_parentesco = member_type_p
	and	f.nr_seq_member_sup = nr_seq_member_sup_p;

end if;

return qtd_w;

end get_qtd_member_family;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	max(ie_grau_parentesco) ie_grau_parentesco
	into	ie_grau_parentesco_w
	from	grau_parentesco g
	where	nr_sequencia = :old.nr_seq_grau_parentesco;



	if	(ie_grau_parentesco_w = '6') then /*If delete a Father*/

		qtd_member_w := get_qtd_member_family('9',null); /*And exists Paternal grandfather/grandmother, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159074);
		end if;

	elsif	(ie_grau_parentesco_w = '5') then /*If delete a Mother*/

		qtd_member_w := get_qtd_member_family('12',null); /*And exists Maternal grandfather/grandmother, show a message*/


		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159075);
		end if;

	elsif	(ie_grau_parentesco_w = '13') then /*If delete a Spouse*/

		qtd_member_w := get_qtd_member_family('15',null); /*and exist a Father-in-law/Mother-in-low, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159076);
		end if;


	elsif	(ie_grau_parentesco_w = '3') then /*Id delete a Son/Daugther*/

		qtd_member_w := get_qtd_member_family('14',:old.nr_sequencia); /*and exist a Grandchild, show a message*/
		if	(qtd_member_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1159077);
		end if;
	end if;
end if;
end;
/


ALTER TABLE TASY.PF_FAMILIA ADD (
  CONSTRAINT PFFAMIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_FAMILIA ADD (
  CONSTRAINT PFFAMIL_PFFAMIL_FK 
 FOREIGN KEY (NR_SEQ_MEMBER_SUP) 
 REFERENCES TASY.PF_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT PFFAMIL_CARGO_FK 
 FOREIGN KEY (CD_OCCUPATION) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT PFFAMIL_PESFISI_FK3 
 FOREIGN KEY (CD_PROFESSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFFAMIL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFFAMIL_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FAMILIA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PF_FAMILIA TO NIVEL_1;

