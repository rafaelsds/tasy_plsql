ALTER TABLE TASY.CTB_REGRA_LOTE_DIST_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_LOTE_DIST_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_LOTE_DIST_CUSTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  CD_EMPRESA            NUMBER(4)               NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_CONTA_TRANSITORIA  VARCHAR2(20 BYTE)       NOT NULL,
  CD_HISTORICO          NUMBER(10)              NOT NULL,
  DT_INICIO_VIGENCIA    DATE                    NOT NULL,
  DT_FIM_VIGENCIA       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBRLDCUS_CONCONT_FK_I ON TASY.CTB_REGRA_LOTE_DIST_CUSTO
(CD_CONTA_TRANSITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRLDCUS_EMPRESA_FK_I ON TASY.CTB_REGRA_LOTE_DIST_CUSTO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRLDCUS_ESTABEL_FK_I ON TASY.CTB_REGRA_LOTE_DIST_CUSTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRLDCUS_HISPADR_FK_I ON TASY.CTB_REGRA_LOTE_DIST_CUSTO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBRLDCUS_PK ON TASY.CTB_REGRA_LOTE_DIST_CUSTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_regra_lote_dist_custo_bf
after insert or update ON TASY.CTB_REGRA_LOTE_DIST_CUSTO for each row
declare
nr_sequencia_w	ctb_regra_lote_dist_custo.nr_sequencia%type;

pragma autonomous_transaction;

begin

	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	ctb_regra_lote_dist_custo a
	where	a.nr_sequencia <> :new.nr_sequencia
	and	a.cd_empresa = :new.cd_empresa
	and	((:new.dt_inicio_vigencia between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,sysdate)) or -- inicio vigencia entre a vigencia de outra regra
	(nvl(:new.dt_fim_vigencia,sysdate) between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,sysdate)) or -- final vigencia entre outra regra
	(:new.dt_inicio_vigencia < a.dt_inicio_vigencia) and (nvl(:new.dt_fim_vigencia,sysdate) > a.dt_fim_vigencia)) -- range de datas maior que outra regra
	and	coalesce(a.cd_estabelecimento,nvl(:new.cd_estabelecimento,0)) = nvl(:new.cd_estabelecimento,0);

	if	(:new.dt_inicio_vigencia > :new.dt_fim_vigencia) then
		wheb_mensagem_pck.exibir_mensagem_abort(1105269);
	end if;

	if	(nvl(nr_sequencia_w,0) > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1105268);
	end if;

end ctb_regra_lote_dist_custo_bf;
/


CREATE OR REPLACE TRIGGER TASY.CTB_REGRA_LOTE_DIST_CUSTO_tp  after update ON TASY.CTB_REGRA_LOTE_DIST_CUSTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_TRANSITORIA,1,4000),substr(:new.CD_CONTA_TRANSITORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_TRANSITORIA',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_HISTORICO,1,4000),substr(:new.CD_HISTORICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_HISTORICO',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'CTB_REGRA_LOTE_DIST_CUSTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CTB_REGRA_LOTE_DIST_CUSTO ADD (
  CONSTRAINT CTBRLDCUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CTB_REGRA_LOTE_DIST_CUSTO ADD (
  CONSTRAINT CTBRLDCUS_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_TRANSITORIA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBRLDCUS_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CTBRLDCUS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBRLDCUS_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO));

GRANT SELECT ON TASY.CTB_REGRA_LOTE_DIST_CUSTO TO NIVEL_1;

