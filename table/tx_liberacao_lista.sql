ALTER TABLE TASY.TX_LIBERACAO_LISTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_LIBERACAO_LISTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_LIBERACAO_LISTA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_TIPO_PROF_LIB         VARCHAR2(15 BYTE)    NOT NULL,
  DT_LIBERACAO             DATE,
  NR_SEQ_RECEPTOR          NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DT_LIBERACAO_REGISTRO    DATE,
  NM_USUARIO_LIB_REGISTRO  VARCHAR2(15 BYTE),
  IE_NAO_LIBERADO          VARCHAR2(1 BYTE),
  IE_CONTRA_INDICADO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXLIBLIS_PK ON TASY.TX_LIBERACAO_LISTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXLIBLIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXLIBLIS_TXRECE_FK_I ON TASY.TX_LIBERACAO_LISTA
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXLIBLIS_TXRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_liberacao_lista_befupdate
before insert or update ON TASY.TX_LIBERACAO_LISTA for each row
declare

ie_profissional_w	varchar2(15);
ie_profissional_regra_w	varchar2(15);

begin

select 	max(ie_profissional)
into	ie_profissional_w
from 	usuario
where 	nm_usuario = :new.nm_usuario;

if (ie_profissional_w is not null) then

	select  max(ie_profissional)
	into	ie_profissional_regra_w
	from	tx_regra_lib_lista
	where	ie_tipo_prof_lib = :new.ie_tipo_prof_lib;

	if (ie_profissional_regra_w <> ie_profissional_w) then
		/*Usu�rio ' || :new.nm_usuario || ' possui classe profissional ' || substr(obter_valor_dominio(1631,ie_profissional_w),1,255) || '.' || chr(13) ||
		'Por�m o cadastro somente permite o lan�amento dos profissionais de classe ' || substr(obter_valor_dominio(1631,ie_profissional_regra_w),1,255));*/
		Wheb_mensagem_pck.exibir_mensagem_abort(261537,	'USUARIO='||:new.nm_usuario || ';' ||
														'PROFISSIONAL=' || substr(obter_valor_dominio(1631,ie_profissional_w),1,255) || ';' ||
														'PROFISSIONAL2=' || substr(obter_valor_dominio(1631,ie_profissional_regra_w),1,255));

	end if;
else
	--Usu�rio ' || :new.nm_usuario || ' n�o possui classe profissional definida.' || chr(13) ||  'Verificar cadastro do usu�rio ');
	Wheb_mensagem_pck.exibir_mensagem_abort(261540, 'USUARIO='||:new.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.TX_LIBERACAO_LISTA ADD (
  CONSTRAINT TXLIBLIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_LIBERACAO_LISTA ADD (
  CONSTRAINT TXLIBLIS_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_LIBERACAO_LISTA TO NIVEL_1;

