ALTER TABLE TASY.PLS_REGRA_CID_INTERNACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_CID_INTERNACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_CID_INTERNACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_REGRA             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_DOENCA_CID        VARCHAR2(10 BYTE),
  NR_SEQ_CLINICA       NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRCIN_CIDDOEN_FK_I ON TASY.PLS_REGRA_CID_INTERNACAO
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCIN_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCIN_ESTABEL_FK_I ON TASY.PLS_REGRA_CID_INTERNACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRCIN_PK ON TASY.PLS_REGRA_CID_INTERNACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCIN_PLSCLIN_FK_I ON TASY.PLS_REGRA_CID_INTERNACAO
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCIN_PLSCLIN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_CID_INTERNACAO_tp  after update ON TASY.PLS_REGRA_CID_INTERNACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_REGRA,1,500);gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_REGRA_CID_INTERNACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLINICA,1,4000),substr(:new.NR_SEQ_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLINICA',ie_log_w,ds_w,'PLS_REGRA_CID_INTERNACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOENCA_CID,1,4000),substr(:new.CD_DOENCA_CID,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA_CID',ie_log_w,ds_w,'PLS_REGRA_CID_INTERNACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_REGRA_CID_INTERNACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_CID_INTERNACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_CID_INTERNACAO ADD (
  CONSTRAINT PLSRCIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_CID_INTERNACAO ADD (
  CONSTRAINT PLSRCIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRCIN_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSRCIN_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.PLS_REGRA_CID_INTERNACAO TO NIVEL_1;

