ALTER TABLE TASY.CIRURGIA_AGENTE_ANESTESICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_AGENTE_ANESTESICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_AGENTE_ANESTESICO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_AGENTE              NUMBER(10),
  CD_MATERIAL                NUMBER(6),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  IE_DISP_INFUSAO            VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_MODO_ADM                VARCHAR2(5 BYTE),
  IE_TIPO_DOSAGEM            VARCHAR2(10 BYTE),
  QT_DOSE                    NUMBER(15,3),
  CD_UNIDADE_MEDIDA          VARCHAR2(30 BYTE),
  CD_UNID_MEDIDA_ADM         VARCHAR2(30 BYTE),
  IE_TIPO                    NUMBER(3)          NOT NULL,
  NR_SEQ_DERIVADO            NUMBER(10),
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  DT_BALANCO_HIDRICO         DATE,
  NR_SEQ_PEPO                NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_JUST_ALERGICO           VARCHAR2(255 BYTE),
  QT_DOSE_TOTAL              NUMBER(15,3),
  NR_SEQ_DIET_CPOE           NUMBER(10),
  NR_SEQ_MAT_CPOE            NUMBER(10),
  DS_SOLUCAO                 VARCHAR2(100 BYTE),
  IE_TERAPIA_ATUAL           VARCHAR2(1 BYTE),
  IE_INTEGRACAO              VARCHAR2(1 BYTE),
  NR_SEQ_EVENTO              NUMBER(10),
  NR_PROTOCOLO_PEPO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIAGANE_AGEANES_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQ_AGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIAGANE_CIRURGI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIAGANE_MATERIA_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_PEPOCIR_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIAGANE_PESFISI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIAGANE_PK ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIAGANE_PROCEDI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_SANDERI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIAGANE_TASASDI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_TASASDI_FK2_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_UNIMEDI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(CD_UNIDADE_MEDIDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_UNIMEDI_FK2_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(CD_UNID_MEDIDA_ADM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CIAGANE_VIAAPLI_FK_I ON TASY.CIRURGIA_AGENTE_ANESTESICO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIAGANE_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cirur_agente_anest_pend_atual
after insert or update ON TASY.CIRURGIA_AGENTE_ANESTESICO for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_libera_medic_pepo_w		varchar2(1);
ie_libera_material_pepo_w	varchar2(1);
ie_libera_agente_pepo_w		varchar2(1);
ie_libera_hemoder_pepo_w	varchar2(1);
ie_libera_terapia_pepo_w	varchar2(1);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
nr_seq_item_pepo_w		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_libera_medic_pepo),
	max(ie_libera_material_pepo),
	max(ie_libera_agente_pepo),
	max(ie_libera_hemoder_pepo),
	max(ie_libera_terapia_pepo)
into	ie_libera_medic_pepo_w,
	ie_libera_material_pepo_w,
	ie_libera_agente_pepo_w,
	ie_libera_hemoder_pepo_w,
	ie_libera_terapia_pepo_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(:new.ie_tipo = 1) and (nvl(ie_libera_agente_pepo_w,'N') = 'S') then --Agentes anestesicos
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CAAA';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCAAA';
	end if;
elsif	(:new.ie_tipo = 2) and (nvl(ie_libera_terapia_pepo_w,'N') = 'S') then --Terapia hidroeletrolitica
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CAAT';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCAAT';
	end if;
elsif	(:new.ie_tipo = 3) and (nvl(ie_libera_medic_pepo_w,'N') = 'S') then --Medicamentos
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CAAM';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCAAM';
	end if;
elsif	(:new.ie_tipo = 4) and (nvl(ie_libera_material_pepo_w,'N') = 'S') then --Materiais
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CAAMAT';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCAAMAT';
	end if;
elsif	(:new.ie_tipo = 5) and (nvl(ie_libera_hemoder_pepo_w,'N') = 'S') then --Hemocomponente
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CAAH';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCAAH';
	end if;
end if;

begin
if	(ie_tipo_w	is not null) then

	select	max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	cirurgia c
	where	c.nr_cirurgia = :new.nr_cirurgia;

	if 	(cd_pessoa_fisica_w is null) then
		select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	pepo_cirurgia c
		where	c.nr_sequencia = :new.nr_seq_pepo;
	end if;

	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
end if;
exception
	when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.CIRURGIA_AGENTE_ANESTESICO ADD (
  CONSTRAINT CIAGANE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA_AGENTE_ANESTESICO ADD (
  CONSTRAINT CIAGANE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIAGANE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CIAGANE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CIAGANE_AGEANES_FK 
 FOREIGN KEY (NR_SEQ_AGENTE) 
 REFERENCES TASY.AGENTE_ANESTESICO (NR_SEQUENCIA),
  CONSTRAINT CIAGANE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIAGANE_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT CIAGANE_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT CIAGANE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT CIAGANE_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MEDIDA_ADM) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CIAGANE_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT CIAGANE_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIAGANE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIRURGIA_AGENTE_ANESTESICO TO NIVEL_1;

