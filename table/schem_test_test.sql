ALTER TABLE TASY.SCHEM_TEST_TEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEM_TEST_TEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEM_TEST_TEST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EXECUTION     NUMBER(10),
  NR_SEQ_SCRIPT        NUMBER(10),
  NR_SEQ_SUITE         NUMBER(10),
  NR_SEQ_ROBOT         NUMBER(10),
  NR_SEQ_GRID          NUMBER(10),
  NR_SEQ_BROWSER       NUMBER(10),
  IE_JOBS              VARCHAR2(255 BYTE),
  IE_SWITCH            VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCTETES_PK ON TASY.SCHEM_TEST_TEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTETES_SCTEBRO_FK_I ON TASY.SCHEM_TEST_TEST
(NR_SEQ_BROWSER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTETES_SCTEGRI_FK_I ON TASY.SCHEM_TEST_TEST
(NR_SEQ_GRID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTETES_SCTEROB_FK_I ON TASY.SCHEM_TEST_TEST
(NR_SEQ_ROBOT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTETES_SCTESCR_FK_I ON TASY.SCHEM_TEST_TEST
(NR_SEQ_SCRIPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTETES_SCTESUI_FK_I ON TASY.SCHEM_TEST_TEST
(NR_SEQ_SUITE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEM_TEST_TEST ADD (
  CONSTRAINT SCTETES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SCHEM_TEST_TEST ADD (
  CONSTRAINT SCTETES_SCTEBRO_FK 
 FOREIGN KEY (NR_SEQ_BROWSER) 
 REFERENCES TASY.SCHEM_TEST_BROWSER (NR_SEQUENCIA),
  CONSTRAINT SCTETES_SCTEGRI_FK 
 FOREIGN KEY (NR_SEQ_GRID) 
 REFERENCES TASY.SCHEM_TEST_GRID (NR_SEQUENCIA),
  CONSTRAINT SCTETES_SCTEROB_FK 
 FOREIGN KEY (NR_SEQ_ROBOT) 
 REFERENCES TASY.SCHEM_TEST_ROBOT (NR_SEQUENCIA),
  CONSTRAINT SCTETES_SCTESCR_FK 
 FOREIGN KEY (NR_SEQ_SCRIPT) 
 REFERENCES TASY.SCHEM_TEST_SCRIPT (NR_SEQUENCIA),
  CONSTRAINT SCTETES_SCTESUI_FK 
 FOREIGN KEY (NR_SEQ_SUITE) 
 REFERENCES TASY.SCHEM_TEST_SUITE (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEM_TEST_TEST TO NIVEL_1;

