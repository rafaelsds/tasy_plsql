ALTER TABLE TASY.PLS_MONITOR_TAB_REF_TISS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TAB_REF_TISS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TAB_REF_TISS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TAB_REF  NUMBER(10)              NOT NULL,
  IE_VERSAO_TISS        VARCHAR2(20 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMTRT_PK ON TASY.PLS_MONITOR_TAB_REF_TISS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTRT_PLSRTTP_FK_I ON TASY.PLS_MONITOR_TAB_REF_TISS
(NR_SEQ_REGRA_TAB_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_MONITOR_TAB_REF_TISS ADD (
  CONSTRAINT PLSMTRT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TAB_REF_TISS ADD (
  CONSTRAINT PLSMTRT_PLSRTTP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TAB_REF) 
 REFERENCES TASY.PLS_REGRA_TAB_TISS_PROC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONITOR_TAB_REF_TISS TO NIVEL_1;

