ALTER TABLE TASY.FIS_EFD_ICMSIPI_C495
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_ICMSIPI_C495 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_ICMSIPI_C495
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  TX_ALIQ_ICMS         NUMBER(6,2),
  CD_ITEM              VARCHAR2(60 BYTE),
  QT_ITEM              NUMBER(15,5),
  QTD_CANC             NUMBER(15,3),
  CD_UNID              VARCHAR2(6 BYTE),
  VL_ITEM              NUMBER(15,2),
  VL_DESC              NUMBER(15,2),
  VL_CANC              NUMBER(15,2),
  VL_ACMO              NUMBER(15,2),
  VL_BC_ICMS           NUMBER(15,2),
  VL_ICMS              NUMBER(15,2),
  VL_ISEN              NUMBER(15,2),
  VL_NT                NUMBER(15,2),
  VL_ICMS_ST           NUMBER(15,2),
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ICMSC495_EFDICMSCON_FK_I ON TASY.FIS_EFD_ICMSIPI_C495
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ICMSC495_PK ON TASY.FIS_EFD_ICMSIPI_C495
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_EFD_ICMSIPI_C495 ADD (
  CONSTRAINT ICMSC495_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_EFD_ICMSIPI_C495 ADD (
  CONSTRAINT ICMSC495_EFDICMSCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_EFD_ICMSIPI_C495 TO NIVEL_1;

