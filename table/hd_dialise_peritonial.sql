ALTER TABLE TASY.HD_DIALISE_PERITONIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE_PERITONIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE_PERITONIAL
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  HR_DIALISE_PERITONIAL     DATE                NOT NULL,
  QT_VOLUME_DRENAGEM        NUMBER(4)           NOT NULL,
  QT_VOLUME_INFUSAO         NUMBER(4),
  DT_DIALISE_PERITONIAL     DATE                NOT NULL,
  QT_UF                     NUMBER(10),
  CD_PESSOA_FISICA          NUMBER(10),
  NR_SEQ_PROTOCOLO          NUMBER(10),
  QT_VOLUME_DRENAGEM_FINAL  NUMBER(4),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  IE_TROCA_DIURNA           VARCHAR2(1 BYTE),
  QT_PESO                   NUMBER(6,3),
  QT_PA_PRE                 NUMBER(3),
  QT_PA_POS                 NUMBER(3),
  QT_DIURESE                NUMBER(15,4),
  QT_KTV                    NUMBER(15,4),
  QT_EPO                    NUMBER(3),
  DT_INTERMITENTE           DATE,
  DS_PROBLEMA               VARCHAR2(255 BYTE),
  DS_CONDUTA                VARCHAR2(255 BYTE),
  NR_SEQ_PET                NUMBER(10),
  NR_SEQ_ORIFICIO           NUMBER(10),
  IE_ESQUEMA_TERAPIA        VARCHAR2(1 BYTE),
  QT_HORA_TERAPIA           NUMBER(2),
  QT_MIN_TERAPIA            NUMBER(2),
  QT_VOLUME_ULT_INFUSAO     NUMBER(15,4),
  IE_TIPO_DRENAGEM          VARCHAR2(1 BYTE),
  IE_DEXTROSE               VARCHAR2(1 BYTE),
  QT_VOLUME_TOTAL           NUMBER(15,4),
  QT_PRIMEIRA_BOLSA         NUMBER(15,2),
  QT_SEGUNDA_BOLSA          NUMBER(15,2),
  QT_ULTIMA_BOLSA           NUMBER(15,2),
  QT_CONCENTRACAO_GLICOSE   NUMBER(10),
  QT_QUARTA_BOLSA           NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIPER_HDCADPET_FK_I ON TASY.HD_DIALISE_PERITONIAL
(NR_SEQ_PET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIPER_HDCADPET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIPER_HDORIF_FK_I ON TASY.HD_DIALISE_PERITONIAL
(NR_SEQ_ORIFICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIPER_HDORIF_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDIPER_PK ON TASY.HD_DIALISE_PERITONIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIPER_PROTNPT_FK_I ON TASY.HD_DIALISE_PERITONIAL
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIPER_PROTNPT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.HD_DIALISE_PERITONIAL ADD (
  CONSTRAINT HDDIPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE_PERITONIAL ADD (
  CONSTRAINT HDDIPER_HDORIF_FK 
 FOREIGN KEY (NR_SEQ_ORIFICIO) 
 REFERENCES TASY.HD_ORIFICIO (NR_SEQUENCIA),
  CONSTRAINT HDDIPER_PROTNPT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_NPT (NR_SEQUENCIA),
  CONSTRAINT HDDIPER_HDCADPET_FK 
 FOREIGN KEY (NR_SEQ_PET) 
 REFERENCES TASY.HD_CADASTRO_PET (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE_PERITONIAL TO NIVEL_1;

