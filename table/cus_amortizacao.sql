ALTER TABLE TASY.CUS_AMORTIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CUS_AMORTIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CUS_AMORTIZACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_REFERENCIA        DATE                     NOT NULL,
  CD_CENTRO_CONTROLE   NUMBER(15)               NOT NULL,
  CD_NATUREZA_GASTO    NUMBER(15),
  VL_AMORTIZAR         NUMBER(15,2)             NOT NULL,
  QT_MES               NUMBER(3)                NOT NULL,
  VL_MENSAL            NUMBER(15,2)             NOT NULL,
  VL_PRIM_MES          NUMBER(15,2)             NOT NULL,
  DT_PRIM_MES          DATE                     NOT NULL,
  DT_LIBERACAO         DATE,
  DS_OBJETO            VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_NG            NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CUSAMOR_ESTABEL_FK_I ON TASY.CUS_AMORTIZACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CUSAMOR_I1_I ON TASY.CUS_AMORTIZACAO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUSAMOR_I1_I
  MONITORING USAGE;


CREATE INDEX TASY.CUSAMOR_NATGAST_FK_I ON TASY.CUS_AMORTIZACAO
(NR_SEQ_NG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CUSAMOR_NATGAST_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CUSAMOR_PK ON TASY.CUS_AMORTIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cus_amortizacao_atual
before insert or update ON TASY.CUS_AMORTIZACAO for each row
declare

vl_mensal_w			Number(15,2);

BEGIN

:new.dt_referencia		:= trunc(:new.dt_referencia, 'month');
:new.dt_prim_mes		:= trunc(:new.dt_prim_mes, 'month');

if	(:new.vl_amortizar <= 0) then
	/*O valor amortizar deve ser maior que zero');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266637);
end if;

if	(:new.qt_mes < 2)  then
	/*O numero de meses deve ser maior que 1;*/
	wheb_mensagem_pck.exibir_mensagem_abort(266638);
end if;

if	(:new.vl_amortizar <> nvl(:old.vl_amortizar,0)) or
	(:new.qt_mes <> :old.qt_mes) then
	vl_mensal_w			:= round((:new.vl_amortizar / :new.qt_mes),2);
	:new.vl_prim_mes		:= :new.vl_amortizar - (vl_mensal_w * (:new.qt_mes - 1));
	:new.vl_mensal		:= vl_mensal_w;
end if;

END;
/


ALTER TABLE TASY.CUS_AMORTIZACAO ADD (
  CONSTRAINT CUSAMOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CUS_AMORTIZACAO ADD (
  CONSTRAINT CUSAMOR_NATGAST_FK 
 FOREIGN KEY (NR_SEQ_NG) 
 REFERENCES TASY.NATUREZA_GASTO (NR_SEQUENCIA),
  CONSTRAINT CUSAMOR_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE),
  CONSTRAINT CUSAMOR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CUS_AMORTIZACAO TO NIVEL_1;

