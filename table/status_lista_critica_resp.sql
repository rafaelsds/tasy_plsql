ALTER TABLE TASY.STATUS_LISTA_CRITICA_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.STATUS_LISTA_CRITICA_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.STATUS_LISTA_CRITICA_RESP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_LISTA          NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  NR_SEQ_ORDEM          NUMBER(5)               NOT NULL,
  CD_PESSOA_RESP        VARCHAR2(10 BYTE)       NOT NULL,
  IE_RESPONSAVEL_FIM    VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.STLICRR_PESFISI_FK_I ON TASY.STATUS_LISTA_CRITICA_RESP
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.STLICRR_PK ON TASY.STATUS_LISTA_CRITICA_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.STLICRR_PK
  MONITORING USAGE;


CREATE INDEX TASY.STLICRR_SETATEN_FK_I ON TASY.STATUS_LISTA_CRITICA_RESP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.STLICRR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.STLICRR_STLISCR_FK_I ON TASY.STATUS_LISTA_CRITICA_RESP
(NR_SEQ_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.STLICRR_STLISCR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.STATUS_LISTA_CRITICA_RESP ADD (
  CONSTRAINT STLICRR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.STATUS_LISTA_CRITICA_RESP ADD (
  CONSTRAINT STLICRR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT STLICRR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT STLICRR_STLISCR_FK 
 FOREIGN KEY (NR_SEQ_LISTA) 
 REFERENCES TASY.STATUS_LISTA_CRITICA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.STATUS_LISTA_CRITICA_RESP TO NIVEL_1;

