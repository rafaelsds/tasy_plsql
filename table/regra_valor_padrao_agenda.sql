ALTER TABLE TASY.REGRA_VALOR_PADRAO_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_VALOR_PADRAO_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_VALOR_PADRAO_AGENDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PERFIL            NUMBER(5)                NOT NULL,
  NR_SEQ_TRANSPORTE    NUMBER(10),
  CD_AGENDA            NUMBER(10),
  CD_CONVENIO          NUMBER(5),
  CD_CATEGORIA         VARCHAR2(10 BYTE),
  CD_PLANO             VARCHAR2(10 BYTE),
  CD_TIPO_ACOMODACAO   NUMBER(4),
  CD_CID               VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REVAPAG_AGENDA_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_AGREGTR_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(NR_SEQ_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_AGREGTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_CATCONV_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_CIDDOEN_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_CONPLAN_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_CONVENI_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_ESTABEL_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REVAPAG_PERFIL_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REVAPAG_PK ON TASY.REGRA_VALOR_PADRAO_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REVAPAG_TIPACOM_FK_I ON TASY.REGRA_VALOR_PADRAO_AGENDA
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REVAPAG_TIPACOM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_VALOR_PADRAO_AGENDA ADD (
  CONSTRAINT REVAPAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_VALOR_PADRAO_AGENDA ADD (
  CONSTRAINT REVAPAG_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT REVAPAG_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REVAPAG_AGREGTR_FK 
 FOREIGN KEY (NR_SEQ_TRANSPORTE) 
 REFERENCES TASY.AGENDA_REGRA_TRANSPORTE (NR_SEQUENCIA),
  CONSTRAINT REVAPAG_CIDDOEN_FK 
 FOREIGN KEY (CD_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT REVAPAG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REVAPAG_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REVAPAG_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT REVAPAG_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REVAPAG_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO));

GRANT SELECT ON TASY.REGRA_VALOR_PADRAO_AGENDA TO NIVEL_1;

