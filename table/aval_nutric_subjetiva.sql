ALTER TABLE TASY.AVAL_NUTRIC_SUBJETIVA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AVAL_NUTRIC_SUBJETIVA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AVAL_NUTRIC_SUBJETIVA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  NR_ATENDIMENTO                 NUMBER(10)     NOT NULL,
  DT_AVALIACAO                   DATE           NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  IE_MUDOU_PESO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_PERDA_PESO                  VARCHAR2(1 BYTE) NOT NULL,
  QT_PERDA                       NUMBER(7,3),
  QT_PESO_ATUAL                  NUMBER(7,3)    NOT NULL,
  PR_PERDA                       NUMBER(7,3),
  IE_MUDANCA_DIETA               VARCHAR2(1 BYTE) NOT NULL,
  IE_DISFAGIA                    VARCHAR2(1 BYTE) NOT NULL,
  IE_NAUSEA                      VARCHAR2(1 BYTE) NOT NULL,
  IE_VOMITO                      VARCHAR2(1 BYTE) NOT NULL,
  IE_DIARREIA                    VARCHAR2(1 BYTE) NOT NULL,
  IE_ANOREXIA                    VARCHAR2(1 BYTE) NOT NULL,
  IE_CAPACIDADE_FUNC             VARCHAR2(1 BYTE) NOT NULL,
  IE_DIAGNOSTICO                 VARCHAR2(1 BYTE) NOT NULL,
  IE_PERDA_GORDURA               VARCHAR2(1 BYTE) NOT NULL,
  IE_EDEMA_SACRAL                VARCHAR2(1 BYTE) NOT NULL,
  IE_ASCITE                      VARCHAR2(1 BYTE) NOT NULL,
  IE_MUSCULO_ESTRIADO            VARCHAR2(1 BYTE) NOT NULL,
  IE_EDEMA_TORNOZELO             VARCHAR2(1 BYTE) NOT NULL,
  QT_PONTO                       NUMBER(3)      NOT NULL,
  DS_RESULTADO                   VARCHAR2(30 BYTE),
  IE_AVAL_NUT                    VARCHAR2(15 BYTE),
  DS_OBSERVACAO                  VARCHAR2(255 BYTE),
  QT_PESO_HABIT                  NUMBER(7,3)    NOT NULL,
  DT_LIBERACAO                   DATE,
  CD_PROFISSIONAL                VARCHAR2(10 BYTE),
  QT_ALTURA                      NUMBER(5,2),
  IE_PESO_DUAS_SEMANAS           VARCHAR2(3 BYTE),
  IE_ALIMENT_ULT_MES             VARCHAR2(3 BYTE),
  IE_ALIMENT_ATUAL               VARCHAR2(3 BYTE),
  IE_SEM_PROBLEMAS               VARCHAR2(1 BYTE),
  IE_SEM_VONTADE_COMER           VARCHAR2(1 BYTE),
  IE_CONSTIPACAO                 VARCHAR2(1 BYTE),
  IE_LESOES_BOCA                 VARCHAR2(1 BYTE),
  IE_BOCA_SECA                   VARCHAR2(1 BYTE),
  IE_DISGEUSIA_AGEUSIA           VARCHAR2(1 BYTE),
  QT_IMC                         NUMBER(6,1),
  IE_TIPO_AVALIACAO              VARCHAR2(1 BYTE),
  CD_PERFIL_ATIVO                NUMBER(5),
  IE_SITUACAO                    VARCHAR2(1 BYTE),
  DT_INATIVACAO                  DATE,
  NM_USUARIO_INATIVACAO          VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA               VARCHAR2(255 BYTE),
  IE_DIETA_HIPOCALORICA          VARCHAR2(1 BYTE),
  IE_DIETA_PASTOSA_HIPOCALORICA  VARCHAR2(1 BYTE),
  IE_DIET_LIQUIDA                VARCHAR2(1 BYTE),
  IE_JEJUM                       VARCHAR2(1 BYTE),
  IE_MUDANCA_PERSISTENTE         VARCHAR2(1 BYTE),
  NR_HORA                        NUMBER(2),
  NR_SEQ_ASSINATURA              NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO      NUMBER(10),
  IE_ORIGEM_PESO                 VARCHAR2(2 BYTE),
  IE_ORIGEM_ALTURA               VARCHAR2(2 BYTE),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_AVALIADOR_AUX               VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX               DATE,
  DS_DIAGNOSTICO_PRIMARIO        VARCHAR2(255 BYTE),
  QT_SEMANAS_INGESTAO            NUMBER(10),
  QT_SEMANAS_CAPACIDADE          NUMBER(10),
  NR_SEQ_REG_ELEMENTO            NUMBER(10),
  IE_NIVEL_ATENCAO               VARCHAR2(1 BYTE),
  CD_AVALIADOR_AUX               VARCHAR2(10 BYTE),
  IE_LIQUIDOS_HIPOCALORICOS      VARCHAR2(1 BYTE),
  IE_DIETA_SOLIDA_SUBOTIMA       VARCHAR2(1 BYTE),
  IE_INANICAO                    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AVANUSU_ATEPACI_FK_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVANUSU_EHRREEL_FK_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVANUSU_I1 ON TASY.AVAL_NUTRIC_SUBJETIVA
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVANUSU_PERFIL_FK_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVANUSU_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVANUSU_PESFISI_FK_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVANUSU_PESFISI_FK2_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AVANUSU_PK ON TASY.AVAL_NUTRIC_SUBJETIVA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVANUSU_TASASDI_FK_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVANUSU_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVANUSU_TASASDI_FK2_I ON TASY.AVAL_NUTRIC_SUBJETIVA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVANUSU_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Aval_Nutric_Subjetiva_Atual
BEFORE INSERT or UPDATE ON TASY.AVAL_NUTRIC_SUBJETIVA FOR EACH ROW
declare

qt_ponto_w		Number(10,0);
qt_reg_w	number(1);
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w		number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	ie_evento_disp		= 'LASD'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';


pragma autonomous_transaction;

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.pr_perda		:= dividir((:new.qt_peso_habit - :new.qt_peso_atual),:new.qt_peso_habit) * 100;
:new.qt_perda		:= :new.qt_peso_habit - :new.qt_peso_atual;
if	(:new.qt_perda < 0) then
	:new.qt_perda	:= 0;
end if;

qt_ponto_w		:= 0;
if	(:new.ie_mudou_peso = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
if (:new.PR_PERDA > 10) then
	qt_ponto_w	:= qt_ponto_w	+ 2;
elsif (:new.PR_PERDA > 0) then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
/*
if	(:new.ie_mudanca_dieta = 'H') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
elsif	(:new.ie_mudanca_dieta = 'P') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
elsif	(:new.ie_mudanca_dieta = 'L') then
	qt_ponto_w	:= qt_ponto_w	+ 2;
elsif	(:new.ie_mudanca_dieta = 'J') then
	qt_ponto_w	:= qt_ponto_w	+ 3;
elsif	(:new.ie_mudanca_dieta = 'T') then
	qt_ponto_w	:= qt_ponto_w	+ 4;
end if;
*/
if	(:new.ie_mudanca_dieta	= 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;

	if	(:new.IE_DIETA_HIPOCALORICA	= 'S') then
		qt_ponto_w	:= qt_ponto_w	+ 1;
	end if;
	if	(:new.IE_DIETA_PASTOSA_HIPOCALORICA	= 'S') then
		qt_ponto_w	:= qt_ponto_w	+ 2;
	end if;
	if	(:new.IE_DIET_LIQUIDA	= 'S') then
		qt_ponto_w	:= qt_ponto_w	+ 2;
	end if;
	if	(:new.IE_JEJUM	= 'S') then
		qt_ponto_w	:= qt_ponto_w	+ 3;
	end if;
	if	(:new.IE_MUDANCA_PERSISTENTE	= 'S') then
		qt_ponto_w	:= qt_ponto_w	+ 2;
	end if;

end if;

if	(:new.ie_disfagia = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
if	(:new.ie_nausea = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
if	(:new.ie_vomito = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
if	(:new.ie_diarreia = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;
if	(:new.ie_anorexia = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 2;
end if;

qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_capacidade_func);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_diagnostico);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_perda_gordura);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_edema_sacral);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_ascite);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_musculo_estriado);
qt_ponto_w	:= qt_ponto_w	+ Somente_Numero(:new.ie_edema_tornozelo);

if (nvl(:new.IE_PERDA_PESO, 'N') = 'S') then
	qt_ponto_w	:= qt_ponto_w	+ 1;
end if;

:new.qt_ponto		:= qt_ponto_w;

if	(qt_ponto_w >= 0) and
	(qt_ponto_w < 17) then
	:new.ds_resultado	:= wheb_mensagem_pck.get_texto(307036, null); -- Bem nutrido
elsif	(qt_ponto_w >= 17) and
	(qt_ponto_w < 22) then
	:new.ds_resultado	:= wheb_mensagem_pck.get_texto(307037, null); -- Moderadamente desnutrido
elsif	(qt_ponto_w >= 22) then
	:new.ds_resultado	:= wheb_mensagem_pck.get_texto(307038, null); -- Gravemente desnutrido
end if;

if	(:new.qt_altura is not null) and
	(:new.qt_peso_atual is not null)  then
	:new.qt_imc	:=	dividir(:new.qt_peso_atual,(dividir(:new.qt_altura,100) * dividir(:new.qt_altura,100)));
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin

	select	max(cd_estabelecimento),
		max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
		cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario);
		end;
	end loop;
	close C01;
	end;

	atualizar_prof_atend( :new.nr_atendimento, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.Aval_Nutric_Sub_pend_atual
after insert or update ON TASY.AVAL_NUTRIC_SUBJETIVA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then

	if	(:new.ie_tipo_avaliacao = 'O') then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'39');
	else
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'23');
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.aval_nutric_subjetiva_delete
after delete ON TASY.AVAL_NUTRIC_SUBJETIVA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.ie_tipo_avaliacao = 'O') then

	delete
	from   	pep_item_pendente
	where  	ie_tipo_pendencia = 'L'
	and	   	ie_escala = '39'
	and		nr_seq_escala  = :old.nr_sequencia;

else

	delete
	from   	pep_item_pendente
	where  	ie_tipo_pendencia = 'L'
	and	   	ie_escala = '23'
	and		nr_seq_escala  = :old.nr_sequencia;

end if;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.AVAL_NUTRIC_SUBJETIVA ADD (
  CONSTRAINT AVANUSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AVAL_NUTRIC_SUBJETIVA ADD (
  CONSTRAINT AVANUSU_PESFISI_FK2 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVANUSU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT AVANUSU_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT AVANUSU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT AVANUSU_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVANUSU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT AVANUSU_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AVAL_NUTRIC_SUBJETIVA TO NIVEL_1;

