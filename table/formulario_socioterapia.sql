ALTER TABLE TASY.FORMULARIO_SOCIOTERAPIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMULARIO_SOCIOTERAPIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMULARIO_SOCIOTERAPIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_CID_DOENCA            VARCHAR2(20 BYTE),
  DS_SEVERIDADE            VARCHAR2(20 BYTE),
  DT_DOENCA_DESDE          DATE,
  DS_GRAVIDADE_DISTURBIOS  VARCHAR2(255 BYTE),
  DS_GRAVIDADE_RESTRICAO   VARCHAR2(255 BYTE),
  DS_ENDERECO_SEGURADO     VARCHAR2(50 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  NR_ZIP_CODE_SEGURADO     NUMBER(5),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NM_CIDADE_SEGURADO       VARCHAR2(30 BYTE),
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  DS_TELEFONE_SEGURADO     VARCHAR2(30 BYTE),
  DS_TIPO_MORADIA          VARCHAR2(50 BYTE),
  NM_PARENTE               VARCHAR2(50 BYTE),
  DS_ENDERECO_PARENTE      VARCHAR2(50 BYTE),
  NR_ZIP_CODE_PARENTE      NUMBER(5),
  NM_CIDADE_PARENTE        VARCHAR2(30 BYTE),
  IE_ACOMP_JURIDICO        VARCHAR2(1 BYTE),
  DS_TELEFONE_PARENTE      VARCHAR2(30 BYTE),
  DS_MOTIVO_JURIDICO       VARCHAR2(100 BYTE),
  NM_ACOMP_JURIDICO        VARCHAR2(50 BYTE),
  DS_ENDERECO_JURIDICO     VARCHAR2(50 BYTE),
  NR_ZIP_CODE_JURIDICO     NUMBER(5),
  NM_CIDADE_JURIDICO       VARCHAR2(30 BYTE),
  DS_TELEFONE_JURIDICO     VARCHAR2(30 BYTE),
  NR_SESSOES_ESPERADAS     NUMBER(5),
  NR_DURACAO_ESPERADA      NUMBER(5),
  NR_SERVICOS_EXEC         NUMBER(5),
  NR_SESSOES_PEDIDAS       NUMBER(5),
  DS_PROGNOSTICO           VARCHAR2(255 BYTE),
  IE_TRAT_HOSPITALAR       VARCHAR2(1 BYTE),
  DS_MOTIVO_TRAT_HOSP      VARCHAR2(255 BYTE),
  DT_EMISSAO               DATE,
  CD_PROVEDOR_SERVICO      NUMBER(10),
  DS_OBJETIVO_TERAPIA      VARCHAR2(255 BYTE),
  IE_MUSTER                VARCHAR2(2 BYTE)     NOT NULL,
  NM_SOCIOTERAPEUTA        VARCHAR2(50 BYTE),
  DS_ENDERECO_TERAP        VARCHAR2(50 BYTE),
  NR_ZIP_CODE_TERAP        NUMBER(5),
  NM_CIDADE_TERAP          VARCHAR2(30 BYTE),
  NM_TITULAR_CONTA         VARCHAR2(50 BYTE),
  CD_CONTA_BANCARIA        VARCHAR2(22 BYTE),
  CD_BANCO_TITULAR         VARCHAR2(11 BYTE),
  NM_INSTITUICAO_FIN       VARCHAR2(30 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  CD_MEDICO                VARCHAR2(10 BYTE),
  NR_RQE                   VARCHAR2(20 BYTE),
  NR_BSNR                  VARCHAR2(25 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FORMSOC_ATEPACI_FK_I ON TASY.FORMULARIO_SOCIOTERAPIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORMSOC_PESFISI_FK_I ON TASY.FORMULARIO_SOCIOTERAPIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FORMSOC_PK ON TASY.FORMULARIO_SOCIOTERAPIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORMSOC_PROFISS_FK_I ON TASY.FORMULARIO_SOCIOTERAPIA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FORMULARIO_SOCIOTERAPIA_UPDATE
before update ON TASY.FORMULARIO_SOCIOTERAPIA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and     ((b.IE_TIPO_PEND_EMISSAO  = 'KV26' and :new.ie_muster = 26) or (b.IE_TIPO_PEND_EMISSAO  = 'KV27' and :new.ie_muster = 27) or (b.IE_TIPO_PEND_EMISSAO  = 'KV28' and :new.ie_muster = 28))
    and     obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (:new.ie_muster = 26 and  is_rule_exists_w ='S') then
    begin
    select	nvl(b.qt_tempo_normal,0),
		    nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO = 'KV26'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
                              end;
   elsif(:new.ie_muster = 27 and is_rule_exists_w ='S') then
   begin
   select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO ='KV27'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);

   end;
   elsif(:new.ie_muster = 28 and is_rule_exists_w ='S') then
   begin
   select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and     b.IE_TIPO_PEND_EMISSAO ='KV28'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);

   end;
   end if;

	end if;
end;
/


ALTER TABLE TASY.FORMULARIO_SOCIOTERAPIA ADD (
  CONSTRAINT FORMSOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMULARIO_SOCIOTERAPIA ADD (
  CONSTRAINT FORMSOC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FORMSOC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FORMSOC_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FORMULARIO_SOCIOTERAPIA TO NIVEL_1;

