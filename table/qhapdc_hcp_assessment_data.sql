ALTER TABLE TASY.QHAPDC_HCP_ASSESSMENT_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QHAPDC_HCP_ASSESSMENT_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.QHAPDC_HCP_ASSESSMENT_DATA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_INDIGENOUS_STATUS      NUMBER(15),
  CD_AUSTRALIAN_ISLANDER    NUMBER(15),
  CD_TYPE_ACCOMMODATION     NUMBER(15),
  CD_PENSION_STATUS         NUMBER(15),
  CD_PSYCHIATRIC            NUMBER(15),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_PREVIOUS_SPECIALISED   NUMBER(15),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  CD_MEN_HEALTH_LEG_STATUS  NUMBER(15),
  CD_EMPLOYMENT_STATUS      NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QHAPDC_HCP_ATEPACI_FK_I ON TASY.QHAPDC_HCP_ASSESSMENT_DATA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QHAPDC_HCP_PK ON TASY.QHAPDC_HCP_ASSESSMENT_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QHAPDC_HCP_ASSESSMENT_DATA ADD (
  CONSTRAINT QHAPDC_HCP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.QHAPDC_HCP_ASSESSMENT_DATA ADD (
  CONSTRAINT QHAPDC_HCP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.QHAPDC_HCP_ASSESSMENT_DATA TO NIVEL_1;

