ALTER TABLE TASY.PERFIL_ITEM_OFTALMOLOGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ITEM_OFTALMOLOGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ITEM_OFTALMOLOGIA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_PERFIL                    NUMBER(5)        NOT NULL,
  NR_SEQ_ITEM                  NUMBER(10)       NOT NULL,
  NR_SEQ_APRESENTACAO          NUMBER(10),
  NR_SEQ_SUPERIOR              NUMBER(10),
  DS_INSTITUICAO               VARCHAR2(40 BYTE),
  IE_VISUALIZA_INATIVO         VARCHAR2(1 BYTE),
  IE_QUEST_TEXTO_PADRAO        VARCHAR2(1 BYTE),
  IE_APRESENTA_VISUAL          VARCHAR2(1 BYTE),
  IE_APRESENTA_VISUAL_NO_ITEM  VARCHAR2(1 BYTE),
  IE_VISAO_PACIENTE            VARCHAR2(1 BYTE),
  IE_APRESENTACAO              VARCHAR2(15 BYTE),
  IE_SEMPRE_APRESENTAR         VARCHAR2(1 BYTE),
  IE_IMPRIMIR_LIBERAR          VARCHAR2(1 BYTE),
  IE_FORMA_LIBERAR             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEITOFT_OFTAITE_FK_I ON TASY.PERFIL_ITEM_OFTALMOLOGIA
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEITOFT_PEITOFT_FK_I ON TASY.PERFIL_ITEM_OFTALMOLOGIA
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEITOFT_PERFIL_FK_I ON TASY.PERFIL_ITEM_OFTALMOLOGIA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEITOFT_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEITOFT_PK ON TASY.PERFIL_ITEM_OFTALMOLOGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEITOFT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PERFIL_ITEM_OFTALMOLOGIA_tp  after update ON TASY.PERFIL_ITEM_OFTALMOLOGIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_FORMA_LIBERAR,1,4000),substr(:new.IE_FORMA_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_LIBERAR',ie_log_w,ds_w,'PERFIL_ITEM_OFTALMOLOGIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRIMIR_LIBERAR,1,4000),substr(:new.IE_IMPRIMIR_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRIMIR_LIBERAR',ie_log_w,ds_w,'PERFIL_ITEM_OFTALMOLOGIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PERFIL_ITEM_OFTALMOLOGIA ADD (
  CONSTRAINT PEITOFT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ITEM_OFTALMOLOGIA ADD (
  CONSTRAINT PEITOFT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PEITOFT_OFTAITE_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.OFTALMOLOGIA_ITEM (NR_SEQUENCIA),
  CONSTRAINT PEITOFT_PEITOFT_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.OFTALMOLOGIA_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PERFIL_ITEM_OFTALMOLOGIA TO NIVEL_1;

