ALTER TABLE TASY.ANALISE_ORTOGRAFICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANALISE_ORTOGRAFICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANALISE_ORTOGRAFICA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_DOMINIO                  NUMBER(5),
  CD_FUNCAO                   NUMBER(5),
  DS_ATRIBUTO                 VARCHAR2(4000 BYTE),
  DS_INFORMACAO               VARCHAR2(4000 BYTE),
  DS_ITEM                     VARCHAR2(40 BYTE),
  DS_LABEL                    VARCHAR2(255 BYTE),
  DS_LABEL_GRID               VARCHAR2(50 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DS_OPCOES                   VARCHAR2(2000 BYTE),
  DS_PARAMETRO                VARCHAR2(255 BYTE),
  DS_TEXTO                    VARCHAR2(255 BYTE),
  DS_VALOR_DOMINIO            VARCHAR2(255 BYTE),
  DS_VALORES                  VARCHAR2(2000 BYTE),
  DT_ORIGEM                   DATE,
  NM_ATRIBUTO                 VARCHAR2(50 BYTE),
  NM_CAMPO_TELA               VARCHAR2(50 BYTE),
  NM_TABELA                   VARCHAR2(50 BYTE),
  NM_TABELA_ORIGEM            VARCHAR2(50 BYTE),
  NM_USUARIO_ORIGEM           VARCHAR2(15 BYTE),
  NR_SEQ_COR                  NUMBER(10),
  NR_SEQ_LOTE                 NUMBER(10),
  NR_SEQ_OBJETO               NUMBER(10),
  NR_SEQ_OBJ_FILTRO           NUMBER(10),
  NR_SEQ_ORIGEM               NUMBER(10),
  NR_SEQ_PARAMETRO            NUMBER(5),
  NR_SEQ_VISAO                NUMBER(10),
  IE_DIFERENCA                VARCHAR2(1 BYTE),
  NR_SEQ_RELATORIO            NUMBER(10),
  DS_TITULO                   VARCHAR2(80 BYTE),
  NR_SEQ_CAMPO_RELAT          NUMBER(10),
  DS_CONTEUDO                 VARCHAR2(2000 BYTE),
  NR_SEQ_PARAM_RELAT          NUMBER(10),
  DS_VALOR_PARAMETRO          VARCHAR2(255 BYTE),
  IE_ORIGEM                   VARCHAR2(15 BYTE),
  VL_DOMINIO                  VARCHAR2(15 BYTE),
  DS_CLASSE_OBJETO            VARCHAR2(255 BYTE),
  DS_FORM                     VARCHAR2(10 BYTE),
  DS_NOME_OBJETO              VARCHAR2(255 BYTE),
  DS_CAMPO                    VARCHAR2(255 BYTE),
  DS_CONTEUDO_CAMPO           VARCHAR2(4000 BYTE),
  DS_METODO                   VARCHAR2(255 BYTE),
  NR_PARAMETRO                NUMBER(10),
  NR_OCORRENCIA               NUMBER(10),
  DS_CAMPO_PAI                VARCHAR2(255 BYTE),
  IE_PERMANECER               VARCHAR2(1 BYTE),
  NR_SEQ_IND_GESTAO           NUMBER(10),
  NR_SEQ_IND_GESTAO_ATRIB     NUMBER(10),
  NR_SEQ_SUBIND_GESTAO        NUMBER(10),
  NR_SEQ_SUBIND_GESTAO_ATRIB  NUMBER(10),
  CD_INTERFACE                NUMBER(5),
  NR_SEQ_REFERENCIA           NUMBER(10),
  CD_TIPO_LOTE_CONTABIL       NUMBER(10),
  CD_EXPRESSAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANLORTOG_ANLORTLT_FK_I ON TASY.ANALISE_ORTOGRAFICA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANLORTOG_ANLORTLT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ANLORTOG_PK ON TASY.ANALISE_ORTOGRAFICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANLORTOG_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ANALISE_ORTOGRAFICA_DELETE
before DELETE ON TASY.ANALISE_ORTOGRAFICA FOR EACH ROW
declare

dt_confirmacao_w	date;

begin
dt_confirmacao_w := sysdate;
/*
if	(nvl(:old.ie_diferenca,'N') = 'S') then
	insert	into logxxxxx_tasy
		(cd_log,
		ds_log,
		dt_atualizacao,
		nm_usuario)
	values	(55869,
		substr('CD_DOMINIO=' || :old.cd_dominio ||
		';CD_FUNCAO=' || :old.cd_funcao ||
		';DS_CAMPO=' || :old.ds_campo ||
		';DS_FORM=' || :old.ds_form ||
		';DS_METODO=' || :old.ds_metodo ||
		';DS_NOME_OBJETO=' || :old.ds_nome_objeto ||
		';IE_ORIGEM=' || :old.ie_origem ||
		';NM_ATRIBUTO=' || :old.nm_atributo ||
		';NM_TABELA=' || :old.nm_tabela ||
		';NR_OCORRENCIA=' || :old.nr_ocorrencia ||
		';NR_PARAMETRO=' || :old.nr_parametro ||
		';NR_SEQ_CAMPO_RELAT=' || :old.nr_seq_campo_relat ||
		';NR_SEQ_COR=' || :old.nr_seq_cor ||
		';NR_SEQ_LOTE=' || :old.nr_seq_lote ||
		';NR_SEQ_OBJETO=' || :old.nr_seq_objeto ||
		';NR_SEQ_OBJ_FILTRO=' || :old.nr_seq_obj_filtro ||
		';NR_SEQ_ORIGEM=' || :old.nr_seq_origem ||
		';NR_SEQ_PARAMETRO=' || :old.nr_seq_parametro ||
		';NR_SEQ_PARAM_RELAT=' || :old.nr_seq_param_relat ||
		';NR_SEQ_RELATORIO=' || :old.nr_seq_relatorio ||
		';NR_SEQ_VISAO=' || :old.nr_seq_visao ||
		';VL_DOMINIO=' || :old.vl_dominio,1,2000),
		sysdate,
		wheb_usuario_pck.get_nm_usuario);
end if;
*/
end;
/


ALTER TABLE TASY.ANALISE_ORTOGRAFICA ADD (
  CONSTRAINT ANLORTOG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ANALISE_ORTOGRAFICA ADD (
  CONSTRAINT ANLORTOG_ANLORTLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.ANALISE_ORTOGRAFICA_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ANALISE_ORTOGRAFICA TO NIVEL_1;

