ALTER TABLE TASY.OFT_CORRECAO_ATUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_CORRECAO_ATUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_CORRECAO_ATUAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CONSULTA            NUMBER(10)         NOT NULL,
  NR_SEQ_TIPO_LENTE          NUMBER(10),
  IE_UNIDADE_TEMPO           NUMBER(3),
  QT_TEMPO_USO               NUMBER(7,2),
  VL_OD_PL_DIO_ESF           NUMBER(5,2),
  VL_OD_PL_DIO_CIL           NUMBER(5,2),
  VL_OD_PL_EIXO              NUMBER(5,2),
  VL_OD_PL_ADICAO            NUMBER(5,2),
  VL_OE_PL_DIO_ESF           NUMBER(5,2),
  VL_OE_PL_DIO_CIL           NUMBER(5,2),
  VL_OE_PL_EIXO              NUMBER(5,2),
  VL_OE_PL_ADICAO            NUMBER(5,2),
  VL_OD_PP_DIO_ESF           NUMBER(5,2),
  VL_OD_PP_DIO_CIL           NUMBER(5,2),
  VL_OD_PP_EIXO              NUMBER(5,2),
  VL_OD_PP_ADICAO            NUMBER(5,2),
  VL_OE_PP_DIO_ESF           NUMBER(5,2),
  VL_OE_PP_DIO_CIL           NUMBER(5,2),
  VL_OE_PP_EIXO              NUMBER(5,2),
  VL_OE_PP_ADICAO            NUMBER(5,2),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DT_REGISTRO                DATE,
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  IE_TIPO                    VARCHAR2(15 BYTE),
  VL_OD_PL_AV_SC             NUMBER(10),
  VL_OD_PL_AV_CC             NUMBER(10),
  VL_OE_PL_AV_SC             NUMBER(10),
  VL_OE_PL_AV_CC             NUMBER(10),
  VL_OD_PL_COR_SC            NUMBER(10),
  VL_OD_PL_CON_SC            NUMBER(10),
  VL_OD_PL_CON_CC            NUMBER(10),
  VL_OE_PL_COR_SC            NUMBER(10),
  VL_OE_PL_CON_SC            NUMBER(10),
  VL_OD_PP_CON_SC            NUMBER(10),
  VL_OD_PP_COR_SC            NUMBER(10),
  VL_OD_PP_AV_CC             NUMBER(10),
  VL_OD_PP_CON_CC            NUMBER(10),
  VL_OE_PL_CON_CC            NUMBER(10),
  VL_OE_PP_AV_SC             NUMBER(10),
  VL_OE_PP_COR_SC            NUMBER(10),
  VL_OE_PP_CON_SC            NUMBER(10),
  VL_OE_PP_AV_CC             NUMBER(10),
  VL_OE_PP_CON_CC            NUMBER(10),
  VL_OD_PP_AV_SC             NUMBER(10),
  NR_SEQ_LENTE_OE            NUMBER(10),
  NR_SEQ_LENTE               NUMBER(10),
  QT_GRAU                    NUMBER(15,3),
  QT_GRAU_ESFERICO           NUMBER(15,3),
  QT_GRAU_CILINDRICO         NUMBER(15,3),
  QT_GRAU_LONGE              NUMBER(15,3),
  QT_EIXO                    NUMBER(15,3),
  QT_ADICAO                  NUMBER(15,3),
  VL_AV                      NUMBER(10),
  QT_CURVA_BASE              NUMBER(15,3),
  QT_CURVA_BASE_DOIS         NUMBER(15,3),
  QT_CURVA_BASE_UM           NUMBER(15,3),
  QT_DIAMETRO                NUMBER(15,3),
  QT_DIAMETRO_OE             NUMBER(15,3),
  QT_GRAU_ESFERICO_OE        NUMBER(15,3),
  QT_GRAU_OE                 NUMBER(15,3),
  QT_GRAU_CILINDRICO_OE      NUMBER(15,3),
  QT_GRAU_LONGE_OE           NUMBER(15,3),
  QT_EIXO_OE                 NUMBER(15,3),
  QT_ADICAO_OE               NUMBER(15,3),
  VL_AV_OE                   NUMBER(10),
  QT_CURVA_BASE_OE           NUMBER(15,3),
  QT_CURVA_BASE_UM_OE        NUMBER(15,3),
  QT_CURVA_BASE_DOIS_OE      NUMBER(15,3),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10),
  VL_OD_LENT_PL_ADIC         NUMBER(5,2),
  VL_OE_LENT_PL_ADIC         NUMBER(5,2),
  VL_OE_AV_CC                NUMBER(10),
  VL_OD_AV_CC                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTCOAT_OFCOFOR_FK_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOAT_OFTCONS_FK_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCOAT_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCOAT_OFTLENC_FK_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_LENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOAT_OFTTILE_FK_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_TIPO_LENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCOAT_OFTTILE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCOAT_PESFISI_FK_I ON TASY.OFT_CORRECAO_ATUAL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTCOAT_PK ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCOAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.OFTCOAT_TASASDI_FK_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCOAT_TASASDI_FK2_I ON TASY.OFT_CORRECAO_ATUAL
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_correcao_atual_pend_atual
after insert or update or delete ON TASY.OFT_CORRECAO_ATUAL for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into	ie_libera_exames_oft_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'AVNL';
		elsif	(:old.dt_liberacao is null) and
               (:new.dt_liberacao is not null) then
               ie_tipo_w := 'XAVNL';
		end if;

      select	max(a.nr_atendimento),
                  max(a.cd_pessoa_fisica)
      into	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;

		begin
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'AVNL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
    end if;

	commit;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.OFT_CORRECAO_ATUAL ADD (
  CONSTRAINT OFTCOAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_CORRECAO_ATUAL ADD (
  CONSTRAINT OFTCOAT_OFTLENC_FK 
 FOREIGN KEY (NR_SEQ_LENTE) 
 REFERENCES TASY.OFT_LENTE_CONTATO (NR_SEQUENCIA),
  CONSTRAINT OFTCOAT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCOAT_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTCOAT_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT OFTCOAT_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OFTCOAT_OFTTILE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LENTE) 
 REFERENCES TASY.OFT_TIPO_LENTE (NR_SEQUENCIA),
  CONSTRAINT OFTCOAT_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.OFT_CORRECAO_ATUAL TO NIVEL_1;

