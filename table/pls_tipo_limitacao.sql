ALTER TABLE TASY.PLS_TIPO_LIMITACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_LIMITACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_LIMITACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_LIMITACAO          VARCHAR2(255 BYTE)      NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  IE_TIPO_INCIDENCIA    VARCHAR2(1 BYTE),
  CD_SISTEMA_ANTERIOR   VARCHAR2(100 BYTE),
  IE_SIGLA              VARCHAR2(100 BYTE),
  NR_SEQ_LIMITACAO_ANT  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTILI_ESTABEL_FK_I ON TASY.PLS_TIPO_LIMITACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTILI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTILI_PK ON TASY.PLS_TIPO_LIMITACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TIPO_LIMITACAO ADD (
  CONSTRAINT PLSTILI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_LIMITACAO ADD (
  CONSTRAINT PLSTILI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_TIPO_LIMITACAO TO NIVEL_1;

