ALTER TABLE TASY.PLS_CARENCIA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CARENCIA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CARENCIA_PROC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_CARENCIA    NUMBER(10)            NOT NULL,
  IE_TIPO_GUIA            VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_AREA_PROCEDIMENTO    NUMBER(15),
  CD_ESPECIALIDADE        NUMBER(15),
  CD_GRUPO_PROC           NUMBER(15),
  NR_SEQ_TIPO_ACOMODACAO  NUMBER(10),
  IE_LIBERADO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_DOENCA_CID           VARCHAR2(10 BYTE),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_GRUPO_SERVICO    NUMBER(10),
  CD_CATEGORIA_CID        VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCAPR_AREPROC_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_CIDCATE_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_CATEGORIA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_CIDCATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_CIDDOEN_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_ESPPROC_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_GRUPROC_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_I2 ON TASY.PLS_CARENCIA_PROC
(NR_SEQ_TIPO_CARENCIA, NVL("NR_SEQ_TIPO_ACOMODACAO",(-1)), NVL("IE_TIPO_GUIA",'-1'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCAPR_PK ON TASY.PLS_CARENCIA_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAPR_PLSPRGS_FK_I ON TASY.PLS_CARENCIA_PROC
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_PLSTIAC_FK_I ON TASY.PLS_CARENCIA_PROC
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAPR_PLSTICA_FK_I ON TASY.PLS_CARENCIA_PROC
(NR_SEQ_TIPO_CARENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCAPR_PROCEDI_FK_I ON TASY.PLS_CARENCIA_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAPR_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CARENCIA_PROC ADD (
  CONSTRAINT PLSCAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CARENCIA_PROC ADD (
  CONSTRAINT PLSCAPR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSCAPR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSCAPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCAPR_PLSTICA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CARENCIA) 
 REFERENCES TASY.PLS_TIPO_CARENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSCAPR_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCAPR_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PLSCAPR_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSCAPR_CIDCATE_FK 
 FOREIGN KEY (CD_CATEGORIA_CID) 
 REFERENCES TASY.CID_CATEGORIA (CD_CATEGORIA_CID),
  CONSTRAINT PLSCAPR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO));

GRANT SELECT ON TASY.PLS_CARENCIA_PROC TO NIVEL_1;

