ALTER TABLE TASY.RXT_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_CAMPO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO_NREC      DATE                 NOT NULL,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_APLICACOES            NUMBER(10)           NOT NULL,
  NR_SEQ_FASE              NUMBER(10)           NOT NULL,
  NR_SEQ_CAMPO             NUMBER(10),
  QT_DOSE_TOTAL            NUMBER(15,2)         NOT NULL,
  QT_DOSE_DIARIA           NUMBER(15,2)         NOT NULL,
  QT_TAM_X                 NUMBER(15,2),
  QT_TAM_Y                 NUMBER(15,2),
  QT_DOSE_MONITOR          NUMBER(15,2),
  QT_X1                    NUMBER(15,2),
  QT_X2                    NUMBER(15,2),
  QT_Y1                    NUMBER(15,2),
  QT_Y2                    NUMBER(15,2),
  QT_SSD                   NUMBER(15,2),
  QT_ANGULO_GANTRY         NUMBER(15,2),
  QT_ANGULO_COLIMADOR      NUMBER(15,2),
  QT_ANGULO_MESA           NUMBER(15,2),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  NR_SEQ_POSICAO           NUMBER(10),
  IE_TIPO_X                VARCHAR2(1 BYTE),
  IE_TIPO_Y                VARCHAR2(1 BYTE),
  NR_SEQ_ENERGIA_RAD       NUMBER(10),
  DS_CAMPO                 VARCHAR2(80 BYTE),
  NR_SEQ_ENE_FOTONS        NUMBER(10),
  QT_ANGULO_GANTRY_FINAL   NUMBER(15,4),
  QT_ANG_COLIMADOR_FINAL   NUMBER(15,2),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DT_LIBERACAO             DATE,
  NM_USUARIO_LIB           VARCHAR2(15 BYTE),
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DT_ENVIO_FISICO          DATE,
  DS_OBSERVACAO_MEDICO     VARCHAR2(2000 BYTE),
  NR_SEQ_CAMPO_INATIVACAO  NUMBER(10),
  DT_LIB_FISICO            DATE,
  NR_SEQ_ATEND_CONS_PEPA   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RXTCAMP_ATCONSPEPA_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RXTCAMP_PK ON TASY.RXT_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTCAMP_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTCAMP_RXTCAMP_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_CAMPO_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTCAMP_RXTCAPR_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTCAMP_RXTCAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTCAMP_RXTENEE_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_ENERGIA_RAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTCAMP_RXTENEE_FK2_I ON TASY.RXT_CAMPO
(NR_SEQ_ENE_FOTONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTCAMP_RXTFATR_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_FASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTCAMP_RXTFATR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTCAMP_RXTPOSIC_FK_I ON TASY.RXT_CAMPO
(NR_SEQ_POSICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTCAMP_RXTPOSIC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RXT_CAMPO_tp  after update ON TASY.RXT_CAMPO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_X,1,4000),substr(:new.IE_TIPO_X,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_X',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_POSICAO,1,4000),substr(:new.NR_SEQ_POSICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_POSICAO',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ENERGIA_RAD,1,4000),substr(:new.NR_SEQ_ENERGIA_RAD,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ENERGIA_RAD',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_X1,1,4000),substr(:new.QT_X1,1,4000),:new.nm_usuario,nr_seq_w,'QT_X1',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_X2,1,4000),substr(:new.QT_X2,1,4000),:new.nm_usuario,nr_seq_w,'QT_X2',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_Y1,1,4000),substr(:new.QT_Y1,1,4000),:new.nm_usuario,nr_seq_w,'QT_Y1',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_Y2,1,4000),substr(:new.QT_Y2,1,4000),:new.nm_usuario,nr_seq_w,'QT_Y2',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_GANTRY,1,4000),substr(:new.QT_ANGULO_GANTRY,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_GANTRY',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_COLIMADOR,1,4000),substr(:new.QT_ANGULO_COLIMADOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_COLIMADOR',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_MESA,1,4000),substr(:new.QT_ANGULO_MESA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_MESA',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_TOTAL,1,4000),substr(:new.QT_DOSE_TOTAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_TOTAL',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_DIARIA,1,4000),substr(:new.QT_DOSE_DIARIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_DIARIA',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_X,1,4000),substr(:new.QT_TAM_X,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_X',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_Y,1,4000),substr(:new.QT_TAM_Y,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_Y',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_MONITOR,1,4000),substr(:new.QT_DOSE_MONITOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_MONITOR',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CAMPO,1,4000),substr(:new.DS_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMPO',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_APLICACOES,1,4000),substr(:new.NR_APLICACOES,1,4000),:new.nm_usuario,nr_seq_w,'NR_APLICACOES',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SSD,1,4000),substr(:new.QT_SSD,1,4000),:new.nm_usuario,nr_seq_w,'QT_SSD',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CAMPO,1,4000),substr(:new.NR_SEQ_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CAMPO',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ENE_FOTONS,1,4000),substr(:new.NR_SEQ_ENE_FOTONS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ENE_FOTONS',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_Y,1,4000),substr(:new.IE_TIPO_Y,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_Y',ie_log_w,ds_w,'RXT_CAMPO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RXT_CAMPO ADD (
  CONSTRAINT RXTCAMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_CAMPO ADD (
  CONSTRAINT RXTCAMP_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT RXTCAMP_RXTCAMP_FK 
 FOREIGN KEY (NR_SEQ_CAMPO_INATIVACAO) 
 REFERENCES TASY.RXT_CAMPO (NR_SEQUENCIA),
  CONSTRAINT RXTCAMP_RXTCAPR_FK 
 FOREIGN KEY (NR_SEQ_CAMPO) 
 REFERENCES TASY.RXT_CAMPO_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT RXTCAMP_RXTFATR_FK 
 FOREIGN KEY (NR_SEQ_FASE) 
 REFERENCES TASY.RXT_FASE_TRATAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RXTCAMP_RXTPOSIC_FK 
 FOREIGN KEY (NR_SEQ_POSICAO) 
 REFERENCES TASY.RXT_POSICAO (NR_SEQUENCIA),
  CONSTRAINT RXTCAMP_RXTENEE_FK 
 FOREIGN KEY (NR_SEQ_ENERGIA_RAD) 
 REFERENCES TASY.RXT_ENERGIA_EQUIP (NR_SEQUENCIA),
  CONSTRAINT RXTCAMP_RXTENEE_FK2 
 FOREIGN KEY (NR_SEQ_ENE_FOTONS) 
 REFERENCES TASY.RXT_ENERGIA_EQUIP (NR_SEQUENCIA));

GRANT SELECT ON TASY.RXT_CAMPO TO NIVEL_1;

