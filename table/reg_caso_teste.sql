ALTER TABLE TASY.REG_CASO_TESTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_CASO_TESTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_CASO_TESTE
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CUSTOMER             NUMBER(10),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PRODUCT              NUMBER(10),
  DS_DESCRICAO                VARCHAR2(255 BYTE),
  IE_TIPO_DOCUMENTO           VARCHAR2(1 BYTE)  NOT NULL,
  DS_INDENTIFICADOR           VARCHAR2(45 BYTE),
  DS_PRE_CONDICAO             VARCHAR2(4000 BYTE),
  NR_TEMPO_ESTIMADO           NUMBER(10),
  NR_SEQ_FEATURE              NUMBER(10),
  DT_LIBERACAO_VV             DATE,
  NR_SEQ_ESTAGIO              NUMBER(10),
  CD_VERSAO                   NUMBER(10),
  NR_SEQ_USABILITY_PRINCIPLE  NUMBER(10),
  IE_TIPO_EXECUCAO            VARCHAR2(1 BYTE),
  IE_OBRIGATORIO              VARCHAR2(1 BYTE),
  IE_TIPO_TESTE               VARCHAR2(1 BYTE),
  IE_TIPO_REVISAO             VARCHAR2(1 BYTE)  DEFAULT null,
  DT_REVISAO                  DATE              DEFAULT null,
  NR_SEQ_REVISAO              NUMBER(10)        DEFAULT null,
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  DT_APROVACAO                DATE,
  NM_USUARIO_APROVACAO        VARCHAR2(15 BYTE),
  NM_USUARIO_LIBERACAO        VARCHAR2(15 BYTE),
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO        VARCHAR2(255 BYTE),
  NR_SEQ_AGRUPADOR            NUMBER(10),
  NR_SEQ_TEST_SCRIPT          NUMBER(10),
  NR_SEQ_INTENCAO_USO         NUMBER(10),
  CD_CT_ID                    VARCHAR2(35 BYTE),
  CD_CT_ID_EXTERNAL           VARCHAR2(100 BYTE),
  IE_MINIMAL_COVERAGE         VARCHAR2(1 BYTE),
  NR_SEQ_INTEGRATED_TEST      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RECATE_PK ON TASY.REG_CASO_TESTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGCATEAG_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_AGRUPADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGCR_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_CUSTOMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGDIC_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_PRODUCT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGEST_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGFEC_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_FEATURE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGINTETES_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_INTEGRATED_TEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECATE_REGINU_FK_I ON TASY.REG_CASO_TESTE
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_caso_teste_after_hist
after update ON TASY.REG_CASO_TESTE for each row
declare

ie_ct_lancado_w			varchar2(1);
ie_tipo_alteracao_w		reg_caso_teste_hist.ie_tipo_alteracao%type;
ds_descricao_w			reg_caso_teste_hist.ds_descricao%type;
ds_pre_condicao_w		reg_caso_teste_hist.ds_pre_condicao%type;
dt_ultima_aprovacao_w		reg_caso_teste_hist.dt_aprovacao%type;
ie_acao_teste_alterada_w	varchar2(1);

begin

select	decode(count(1), 0, 'N', 'S')
into	ie_ct_lancado_w -- Determinar se o CT j?oi liberado em alguma revis?do documento de CT
from	reg_caso_teste_hist
where	nr_seq_caso_teste = :new.nr_sequencia
and	ie_tipo_alteracao = 'I'
and	nr_seq_reg_version_rev is not null;

if	(:new.dt_aprovacao is not null) and
	(:new.nm_usuario_aprovacao is not null) then

	delete	reg_caso_teste_hist
	where	nr_seq_caso_teste = :new.nr_sequencia
	and	nr_seq_reg_version_rev is null;

	reg_gravar_hist_acao_teste(:new.nr_sequencia, :new.nm_usuario, ie_acao_teste_alterada_w);

	if (ie_ct_lancado_w = 'S') then

		select	ds_descricao,
			ds_pre_condicao,
			dt_aprovacao
		into	ds_descricao_w,
			ds_pre_condicao_w,
			dt_ultima_aprovacao_w
		from	reg_caso_teste_hist
		where	nr_sequencia = (	select	max(nr_sequencia)
						from	reg_caso_teste_hist
						where	nr_seq_caso_teste = :new.nr_sequencia
						and	ie_tipo_alteracao in ('I', 'A')
						and	nr_seq_reg_version_rev is not null
					);

		if	(nvl(:old.ie_situacao, 'A') = 'A') and
			(:new.ie_situacao = 'I') then
			ie_tipo_alteracao_w := 'E';
		elsif	(:new.ds_descricao <> ds_descricao_w) or
			(ie_acao_teste_alterada_w = 'S') then
			ie_tipo_alteracao_w := 'A';
		end if;
	else
		if (nvl(:new.ie_situacao, 'A') = 'A') then
			ie_tipo_alteracao_w := 'I';
		end if;
	end if;
end if;

if (ie_tipo_alteracao_w is not null) then

	insert into reg_caso_teste_hist (
		nr_sequencia,
		nr_seq_customer,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_product,
		ds_descricao,
		ie_tipo_documento,
		dt_liberacao_vv,
		ds_pre_condicao,
		nr_tempo_estimado,
		nr_seq_feature,
		ie_tipo_execucao,
		ie_obrigatorio,
		ie_tipo_teste,
		cd_versao,
		nr_seq_usability_principle,
		nr_seq_reg_version_rev,
		nr_seq_estagio,
		nr_seq_caso_teste,
		ie_tipo_alteracao,
		dt_revisao,
		ie_tipo_revisao,
		nr_seq_revisao,
		ie_situacao,
		dt_aprovacao,
		nm_usuario_aprovacao,
		nm_usuario_liberacao,
		dt_inclusao_hist,
		dt_inativacao,
		nm_usuario_inativacao,
		ds_motivo_inativacao,
		nr_seq_agrupador,
		nr_seq_test_script,
		nr_seq_intencao_uso,
		cd_ct_id)
	values (
		reg_caso_teste_hist_seq.nextval,
		:new.nr_seq_customer,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.nr_seq_product,
		:new.ds_descricao,
		:new.ie_tipo_documento,
		:new.dt_liberacao_vv,
		:new.ds_pre_condicao,
		:new.nr_tempo_estimado,
		:new.nr_seq_feature,
		:new.ie_tipo_execucao,
		:new.ie_obrigatorio,
		:new.ie_tipo_teste,
		:new.cd_versao,
		:new.nr_seq_usability_principle,
		null,
		:new.nr_seq_estagio,
		:new.nr_sequencia,
		ie_tipo_alteracao_w,
		:new.dt_revisao,
		:new.ie_tipo_revisao,
		:new.nr_seq_revisao,
		:new.ie_situacao,
		:new.dt_aprovacao,
		:new.nm_usuario_aprovacao,
		:new.nm_usuario_liberacao,
		sysdate,
		:new.dt_inativacao,
		:new.nm_usuario_inativacao,
		:new.ds_motivo_inativacao,
		:new.nr_seq_agrupador,
		:new.nr_seq_test_script,
		:new.nr_seq_intencao_uso,
		:new.cd_ct_id);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.reg_caso_teste_after
after insert ON TASY.REG_CASO_TESTE for each row
declare

qt_scope_w		number(10);

begin

if (:new.ie_tipo_documento = 'T') then -- Verificação

	select	count(1)
	into	qt_scope_w
	from	reg_escopo_product_req
	where	nr_seq_product_req = :new.nr_seq_product;

	if (qt_scope_w > 0) then
		insert into reg_escopo_caso_teste (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_escopo,
			nr_seq_caso_teste
		)
			select	reg_escopo_caso_teste_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				nr_seq_escopo,
				:new.nr_sequencia
			from	reg_escopo_product_req
			where	nr_seq_product_req = :new.nr_seq_product;
	end if;

elsif (:new.ie_tipo_documento = 'V') then -- Validação

	select	count(1)
	into	qt_scope_w
	from	reg_escopo_customer_req
	where	nr_customer_req = :new.nr_seq_customer;

	if (qt_scope_w > 0) then
		insert into reg_escopo_caso_teste (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_escopo,
			nr_seq_caso_teste
		)
			select	reg_escopo_caso_teste_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				nr_seq_escopo,
				:new.nr_sequencia
			from	reg_escopo_customer_req
			where	nr_customer_req = :new.nr_seq_customer;
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.reg_caso_teste_before_insert
  before insert ON TASY.REG_CASO_TESTE   for each row
declare

  nr_seq_intencao_uso_w number(10);
  ds_prefixo_w varchar2(15);
  nr_sequencia_w varchar2(15);
  nr_seq_ct_w number(10);

begin
--Caso de teste de validacao
if	(:new.ie_tipo_documento = 'V') then

nr_seq_intencao_uso_w := :new.nr_seq_intencao_uso;

  if (:new.nr_seq_customer is not null ) then
	select iu.nr_sequencia,
	     iu.ds_prefixo
	into nr_seq_intencao_uso_w,
	     ds_prefixo_w
	from reg_area_customer ac,
	     reg_features_customer fc,
	     reg_intencao_uso iu,
	     reg_customer_requirement cr
	where ac.nr_sequencia = fc.nr_seq_area_customer
	and ac.nr_seq_intencao_uso = iu.nr_sequencia
	and fc.nr_sequencia = cr.nr_seq_features
	and cr.nr_sequencia = :new.nr_seq_customer;
  end if;

  if (:new.nr_seq_customer is not null and
	nr_seq_intencao_uso_w is not null and
	nr_seq_intencao_uso_w <> 2
     --:new.nr_seq_intencao_uso is not null and
     --:new.nr_seq_intencao_uso <> 2
     ) then

    --select iu.nr_sequencia, iu.ds_prefixo
    --  into nr_seq_intencao_uso_w, ds_prefixo_w
    --  from reg_customer_requirement urs, reg_intencao_uso iu
   --  where urs.nr_sequencia = :new.nr_seq_customer
   --    and urs.nr_seq_intencao_uso = iu.nr_sequencia;

    select max(a.nr_sequencia)
      into nr_seq_ct_w
      from reg_caso_teste a
     where a.nr_seq_intencao_uso = nr_seq_intencao_uso_w
     and   a.ie_tipo_documento = 'V';

    if nr_seq_ct_w is not null then

      select nvl(obter_somente_numero(cd_ct_id), 0) + 1
        into nr_sequencia_w
        from reg_caso_teste
       where nr_sequencia = nr_seq_ct_w;

    end if;

    :new.cd_ct_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_TCVAL_' ||
                     nvl(nr_sequencia_w, '1');


  elsif (:new.nr_seq_customer is not null and
        nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w = 2) then

    :new.cd_ct_id            := 'TASY_VTC_' || :new.nr_sequencia;
    --:new.nr_seq_intencao_uso := 2;

    end if;


end if;

--Caso de teste de verificacao
if	(:new.ie_tipo_documento = 'T') then

nr_seq_intencao_uso_w := :new.nr_seq_intencao_uso;

  if (:new.nr_seq_product is not null and :new.nr_seq_intencao_uso is null) then

	select iu.nr_sequencia,
           iu.ds_prefixo
      into nr_seq_intencao_uso_w,
           ds_prefixo_w
      from reg_product_requirement prs,
           reg_intencao_uso iu
     where prs.nr_sequencia = :new.nr_seq_product
       and prs.nr_seq_intencao_uso = iu.nr_sequencia;

   end if;


if (:new.nr_seq_product is not null and
        nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w <> 2) then

    select max(a.nr_sequencia)
      into nr_seq_ct_w
      from reg_caso_teste a
     where a.nr_seq_intencao_uso = nr_seq_intencao_uso_w;

    if (nr_seq_ct_w is not null) then

      select nvl(obter_somente_numero(cd_ct_id), 0) + 1
        into nr_sequencia_w
        from reg_caso_teste
       where nr_sequencia = nr_seq_ct_w;

    end if;

    :new.cd_ct_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_TCVER_' ||
                     nvl(nr_sequencia_w, '1');

  elsif (:new.nr_seq_product is not null and
        nr_seq_intencao_uso_w is not null and
        nr_seq_intencao_uso_w = 2 ) then

    :new.cd_ct_id            := 'TASY_TC_' || :new.nr_sequencia;
    --:new.nr_seq_intencao_uso := 2;

  end if;
end if;
--necessario para atribuir intencao de uso para quando esta inserindo pela funcao antiga
:new.nr_seq_intencao_uso := nr_seq_intencao_uso_w;

end;
/


ALTER TABLE TASY.REG_CASO_TESTE ADD (
  CONSTRAINT RECATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_CASO_TESTE ADD (
  CONSTRAINT RECATE_REGCATEAG_FK 
 FOREIGN KEY (NR_SEQ_AGRUPADOR) 
 REFERENCES TASY.REG_CASO_TESTE_AGRUPADOR (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGINTETES_FK 
 FOREIGN KEY (NR_SEQ_INTEGRATED_TEST) 
 REFERENCES TASY.REG_INTEGRATED_TEST (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGCR_FK 
 FOREIGN KEY (NR_SEQ_CUSTOMER) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGDIC_FK 
 FOREIGN KEY (NR_SEQ_PRODUCT) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGFEC_FK 
 FOREIGN KEY (NR_SEQ_FEATURE) 
 REFERENCES TASY.REG_FEATURES_CUSTOMER (NR_SEQUENCIA),
  CONSTRAINT RECATE_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_CASO_TESTE TO NIVEL_1;

