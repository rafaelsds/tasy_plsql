DROP TABLE TASY.COM_W_FATUR_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_W_FATUR_CLIENTE
(
  CD_CNPJ         VARCHAR2(14 BYTE),
  QT_HOR_SUP      NUMBER(22,2),
  QT_HOR_DES      NUMBER(22,2),
  VL_HOR_SUP      NUMBER(22,2),
  VL_HOR_DES      NUMBER(22,2),
  VL_TOTAL_HORAS  NUMBER(22,2),
  VL_TOTAL        NUMBER(22,2),
  VL_CDU          NUMBER(22,2),
  VL_IMPL         NUMBER(22,2),
  VL_TREINAMENTO  NUMBER(22,2),
  VL_MANUT        NUMBER(22,2),
  VL_LUT          NUMBER(22,2),
  VL_REPASSE      NUMBER(22,2),
  IE_ABC          VARCHAR2(1 BYTE)              NOT NULL,
  PR_CLIENTE      NUMBER(15,2)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.COM_W_FATUR_CLIENTE TO NIVEL_1;

