ALTER TABLE TASY.TEMPO_ADIC_ESPERA_PA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEMPO_ADIC_ESPERA_PA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEMPO_ADIC_ESPERA_PA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_CLINICA           NUMBER(5)                NOT NULL,
  QT_TEMPO             NUMBER(15)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TEADESP_I1 ON TASY.TEMPO_ADIC_ESPERA_PA
(IE_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TEADESP_PK ON TASY.TEMPO_ADIC_ESPERA_PA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TEADESP_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TEMPO_ADIC_ESPERA_PA_tp  after update ON TASY.TEMPO_ADIC_ESPERA_PA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'TEMPO_ADIC_ESPERA_PA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TEMPO_ADIC_ESPERA_PA ADD (
  CONSTRAINT TEADESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TEMPO_ADIC_ESPERA_PA TO NIVEL_1;

