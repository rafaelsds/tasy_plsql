ALTER TABLE TASY.CONTRATO_CENTRO_CUSTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO_CENTRO_CUSTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO_CENTRO_CUSTO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_CONTRATO     NUMBER(10)                NOT NULL,
  CD_CENTRO_CUSTO     NUMBER(8)                 NOT NULL,
  PR_ALOCACAO         NUMBER(6,3),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  DS_OBSERVACAO       VARCHAR2(255 BYTE),
  QT_ALOCACAO         NUMBER(9,3),
  VL_ALOCACAO         NUMBER(15,2),
  DT_INICIO_VIGENCIA  DATE,
  DT_FIM_VIGENCIA     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONCECU_CENCUST_FK_I ON TASY.CONTRATO_CENTRO_CUSTO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCECU_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONCECU_CONTRAT_FK_I ON TASY.CONTRATO_CENTRO_CUSTO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONCECU_PK ON TASY.CONTRATO_CENTRO_CUSTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCECU_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONTRATO_CENTRO_CUSTO ADD (
  CONSTRAINT CONCECU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO_CENTRO_CUSTO ADD (
  CONSTRAINT CONCECU_CONTRAT_FK_I 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONCECU_CENCUST_FK_I 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.CONTRATO_CENTRO_CUSTO TO NIVEL_1;

