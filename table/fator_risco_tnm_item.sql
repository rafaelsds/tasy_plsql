ALTER TABLE TASY.FATOR_RISCO_TNM_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FATOR_RISCO_TNM_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.FATOR_RISCO_TNM_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_ITEM_FATOR_RISCO  VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_VALOR             NUMBER(10),
  NR_SEQ_FATOR_RISCO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FARIIT_FARITNM_FK_I ON TASY.FATOR_RISCO_TNM_ITEM
(NR_SEQ_FATOR_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FARIIT_PK ON TASY.FATOR_RISCO_TNM_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FATOR_RISCO_TNM_ITEM ADD (
  CONSTRAINT FARIIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FATOR_RISCO_TNM_ITEM ADD (
  CONSTRAINT FARIIT_FARITNM_FK 
 FOREIGN KEY (NR_SEQ_FATOR_RISCO) 
 REFERENCES TASY.FATOR_RISCO_TNM (NR_SEQUENCIA));

GRANT SELECT ON TASY.FATOR_RISCO_TNM_ITEM TO NIVEL_1;

