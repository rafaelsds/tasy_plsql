ALTER TABLE TASY.W_DIRF_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIRF_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIRF_ARQUIVO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(4000 BYTE),
  NR_SEQ_APRESENTACAO  NUMBER(6),
  NR_SEQ_REGISTRO      NUMBER(10),
  CD_CGC               VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_DARF              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDIRFARQ_PEFICOE_FK_I ON TASY.W_DIRF_ARQUIVO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WDIRFARQ_PESJURI_FK_I ON TASY.W_DIRF_ARQUIVO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIRFARQ_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WDIRFARQ_PK ON TASY.W_DIRF_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIRFARQ_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_DIRF_ARQUIVO ADD (
  CONSTRAINT WDIRFARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DIRF_ARQUIVO ADD (
  CONSTRAINT WDIRFARQ_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT WDIRFARQ_PEFICOE_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.W_DIRF_ARQUIVO TO NIVEL_1;

