DROP TABLE TASY.MARCUS_SALDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MARCUS_SALDO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_MES_REF      NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  CD_CONTA_CONTABIL   VARCHAR2(20 BYTE)         NOT NULL,
  CD_CENTRO_CUSTO     NUMBER(8),
  VL_DEBITO           NUMBER(15,2)              NOT NULL,
  VL_CREDITO          NUMBER(15,2)              NOT NULL,
  VL_SALDO            NUMBER(15,2)              NOT NULL,
  VL_MOVIMENTO        NUMBER(15,2)              NOT NULL,
  VL_ENCERRAMENTO     NUMBER(15,2)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.MARCUS_SALDO TO NIVEL_1;

