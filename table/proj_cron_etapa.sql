ALTER TABLE TASY.PROJ_CRON_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CRON_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CRON_ETAPA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_CRONOGRAMA          NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_APRES               NUMBER(15)         NOT NULL,
  DS_ETAPA                   VARCHAR2(4000 BYTE),
  IE_FASE                    VARCHAR2(1 BYTE)   NOT NULL,
  DT_INICIO_PREV             DATE,
  DT_FIM_PREV                DATE,
  DT_INICIO_REAL             DATE,
  DT_FIM_REAL                DATE,
  QT_HORA_PREV               NUMBER(15,2)       NOT NULL,
  QT_HORA_REAL               NUMBER(15,2),
  NR_SEQ_ETAPA               NUMBER(10),
  QT_HORA_SALDO              NUMBER(15,2),
  PR_ETAPA                   NUMBER(15,2)       NOT NULL,
  DT_CADASTRO                DATE,
  DT_FINAL_MULTIPLIC         DATE,
  DT_PILOTO                  DATE,
  DT_ATIVACAO                DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA           NUMBER(10),
  DS_ATIVIDADE               VARCHAR2(255 BYTE),
  NR_SEQ_SUPERIOR            NUMBER(10),
  CD_CLASSIFICACAO           VARCHAR2(255 BYTE),
  NR_SEQ_ORDENACAO           NUMBER(15),
  IE_MODULO                  VARCHAR2(1 BYTE)   NOT NULL,
  CD_FUNCAO                  NUMBER(5),
  IE_ETAPA_EXEC_DESENV       VARCHAR2(15 BYTE),
  IE_MPO                     VARCHAR2(1 BYTE),
  IE_ADERENCIA               VARCHAR2(1 BYTE),
  IE_CADASTROS               VARCHAR2(1 BYTE),
  IE_PARAMETRIZACAO          VARCHAR2(1 BYTE),
  IE_TREINAMENTO             VARCHAR2(1 BYTE),
  IE_TESTE_PILOTO            VARCHAR2(1 BYTE),
  PR_PREVISTO                NUMBER(15,2),
  IE_OBRIGATORIO             VARCHAR2(1 BYTE),
  IE_TIPO_OBJ_PROJ_MIGR      VARCHAR2(15 BYTE),
  IE_ATIVIDADE_ADICIONAL     VARCHAR2(1 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_TIPO_ATIV_MET           VARCHAR2(15 BYTE),
  PR_CONSULTORIA             NUMBER(15,2),
  NR_PREDECESSORA            NUMBER(10),
  IE_REGRA_INI_FIM           VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_INTERNO             NUMBER(10)         NOT NULL,
  DT_LIBERACAO_ETAPA         DATE,
  QT_HORAS_ETAPA_ADIC        NUMBER(10,2),
  IE_MILESTONE               VARCHAR2(1 BYTE),
  PR_PREV_ETAPA              NUMBER(15,2),
  IE_VIRADA                  VARCHAR2(1 BYTE),
  NR_SEQ_SUB_PROJ            NUMBER(10),
  DT_ACORDO_EXTERNO          DATE,
  IE_PRODUTO                 VARCHAR2(15 BYTE),
  IE_GERA_OS                 VARCHAR2(1 BYTE),
  NR_SEQ_PROCESSO_ETAPA      NUMBER(10),
  IE_STATUS_ETAPA            VARCHAR2(1 BYTE),
  NR_SEQ_ETAPA_NPI           NUMBER(10),
  QT_DIAS                    NUMBER(15,2),
  IE_RECEITA_RECONHECIDA     VARCHAR2(1 BYTE),
  IE_PAPEL_EXECUTOR          VARCHAR2(15 BYTE),
  IE_ATIVIDADE               VARCHAR2(15 BYTE),
  NR_SEQ_AREA_FASE           NUMBER(10),
  IE_CHECKPOINT              VARCHAR2(1 BYTE),
  QT_DIAS_UTEIS              NUMBER(10),
  IE_TIPO_REVISAO            VARCHAR2(3 BYTE)   DEFAULT null,
  IE_BUG_FIXING              VARCHAR2(1 BYTE),
  IE_PLATAFORMA_IMPLANTACAO  VARCHAR2(1 BYTE),
  QT_MINUTOS_INTERNO         NUMBER(10),
  PR_PREVISTO_RAT            NUMBER(30,16),
  NR_SEQ_PROD_REQ            NUMBER(10),
  NR_SEQ_IDENTIFICADOR       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROCRET_FUNCAO_FK_I ON TASY.PROJ_CRON_ETAPA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCRET_I1 ON TASY.PROJ_CRON_ETAPA
(DT_INICIO_PREV, DT_FIM_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROCRET_PK ON TASY.PROJ_CRON_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCRET_PROCRET_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCRET_PROCRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCRET_PROCRON_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_CRONOGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCRET_PROETCR_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCRET_PROETCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCRET_PROJAGF_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_AREA_FASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCRET_PROJCRE_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCRET_PROJCRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCRET_PROJIDAU_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_IDENTIFICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCRET_PROPROJ_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_SUB_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCRET_PROPROJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCRET_PRPPROCETP_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_PROCESSO_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCRET_PRPPROCETP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCRET_REGPR_FK_I ON TASY.PROJ_CRON_ETAPA
(NR_SEQ_PROD_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROJ_CRON_ETAPA_ATUAL
BEFORE INSERT or UPDATE ON TASY.PROJ_CRON_ETAPA FOR EACH ROW
declare
nr_seq_ordem_serv_w 		number(10);
dt_fim_ativ_w			date;
dt_ini_ativ_w			date;
ie_origem_des_w			number(1);

BEGIN

Obter_valor_Dinamico_bv('select	count(1) from	proj_projeto pp, proj_cronograma pc where	pp.nr_sequencia = pc.nr_seq_proj and	ie_origem = ''D'' and	pc.nr_sequencia = :nr_seq_cronograma', 'nr_seq_cronograma=' || :new.nr_seq_cronograma || ';', ie_origem_des_w);

:new.dt_atualizacao := sysdate;

if	(:new.ie_regra_ini_fim is null) then
	:new.ie_regra_ini_fim := 'B';
	end if;

:new.qt_hora_saldo	:= nvl(:new.qt_hora_prev,0) - nvl(:new.qt_hora_real,0);

if	(:new.pr_etapa = 100) then
	if	(:new.dt_fim_real is null) then
		begin
		select	max(dt_fim_ativ)
		into	dt_fim_ativ_w
		from	proj_rat_ativ
		where	nr_seq_etapa_cron = :new.nr_sequencia;

		if 	(dt_fim_ativ_w is null) then
			:new.dt_fim_real	:= sysdate;
		else
			:new.dt_fim_real	:= dt_fim_ativ_w;
		end if;
		end;
	elsif	(:new.dt_inicio_real is null) then
		begin
		select	max(dt_inicio_ativ)
		into	dt_ini_ativ_w
		from	proj_rat_ativ
		where	nr_seq_etapa_cron = :new.nr_sequencia;

		if 	(dt_ini_ativ_w is null) then
			:new.dt_inicio_real	:= sysdate;
		else
			:new.dt_inicio_real	:= dt_fim_ativ_w;
		end if;
		end;
	end if;
end if;

/*Atualizar inicio/fim previsto nas OS da etapa*/
if	((:new.dt_fim_prev is not null ) and
	(:new.dt_fim_prev <> :old.dt_fim_prev )) or
	((:new.dt_inicio_prev is not null ) and
	(:new.dt_inicio_prev <> :old.dt_inicio_prev ))then
	begin
		update	man_ordem_servico a
		set	a.dt_inicio_previsto 	= nvl(:new.dt_inicio_prev,a.dt_inicio_previsto),
			a.dt_fim_previsto	= nvl(:new.dt_fim_prev,a.dt_fim_previsto)
		where	nr_seq_proj_cron_etapa  = :new.nr_sequencia;
	exception
	when others then
		nr_seq_ordem_serv_w := nr_seq_ordem_serv_w;
	end;

end if;
	if	(ie_origem_des_w > 0) then
		if (:new.qt_dias_uteis is null) or (:new.dt_inicio_prev <> :old.dt_inicio_prev) or (:new.dt_fim_prev <> :old.dt_fim_prev) then
			:new.qt_dias_uteis	:= OBTER_DIAS_UTEIS_PERIODO(nvl(:new.dt_inicio_prev, sysdate), nvl(:new.dt_fim_prev, sysdate), 1);
		end if;
	elsif	(:new.dt_inicio_real is not null) and --Mantem comportamento atual para Gest�o Projetos Philips
		(:new.dt_fim_prev is not null) and
		(nvl(:new.pr_previsto,0)	<> 100) then

		if 	(:new.qt_dias_uteis is null) or
			(:new.dt_inicio_real <> :old.dt_inicio_real) or
			(:new.dt_fim_prev <> :old.dt_fim_prev)  then
			:new.qt_dias_uteis	:= OBTER_DIAS_UTEIS_PERIODO(:new.dt_inicio_real, :new.dt_fim_prev, 1);
		end if;
	end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.PROJ_CRON_ETAPA_tp  after update ON TASY.PROJ_CRON_ETAPA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_RECEITA_RECONHECIDA,1,4000),substr(:new.IE_RECEITA_RECONHECIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEITA_RECONHECIDA',ie_log_w,ds_w,'PROJ_CRON_ETAPA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROJ_CRON_ETAPA ADD (
  CONSTRAINT PROCRET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_CRON_ETAPA ADD (
  CONSTRAINT PROCRET_REGPR_FK 
 FOREIGN KEY (NR_SEQ_PROD_REQ) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROJIDAU_FK 
 FOREIGN KEY (NR_SEQ_IDENTIFICADOR) 
 REFERENCES TASY.PROJ_IDENTIFICADOR_AUDIT (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROJAGF_FK 
 FOREIGN KEY (NR_SEQ_AREA_FASE) 
 REFERENCES TASY.PROJ_AREA_GERENCIA_FASE (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROJCRE_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PROJ_CRON_ESTRUT (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROETCR_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.PROJ_ETAPA_CRONOGRAMA (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROCRON_FK 
 FOREIGN KEY (NR_SEQ_CRONOGRAMA) 
 REFERENCES TASY.PROJ_CRONOGRAMA (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT PROCRET_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT PROCRET_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_SUB_PROJ) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PROCRET_PRPPROCETP_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO_ETAPA) 
 REFERENCES TASY.PRP_PROCESSO_ETAPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_CRON_ETAPA TO NIVEL_1;

