ALTER TABLE TASY.SUS_AIH_REG_CIVIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_AIH_REG_CIVIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_AIH_REG_CIVIL
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  NR_AIH           NUMBER(13),
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  DT_NASCIMENTO    DATE,
  NM_RECEM_NATO    VARCHAR2(70 BYTE)            NOT NULL,
  CD_CGC_CARTORIO  VARCHAR2(14 BYTE)            NOT NULL,
  NR_LIVRO         VARCHAR2(8 BYTE)             NOT NULL,
  NR_FOLHA         VARCHAR2(4 BYTE)             NOT NULL,
  NR_TERMO         VARCHAR2(8 BYTE)             NOT NULL,
  DT_EMISSAO_REG   DATE                         NOT NULL,
  NR_REGISTRO      NUMBER(15)                   NOT NULL,
  NR_SEQ_AIH       NUMBER(10),
  NR_ATENDIMENTO   NUMBER(10)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSAIRC_ATEPACI_FK_I ON TASY.SUS_AIH_REG_CIVIL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIRC_PESJURI_FK_I ON TASY.SUS_AIH_REG_CIVIL
(CD_CGC_CARTORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIRC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUSAIRC_PK ON TASY.SUS_AIH_REG_CIVIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIRC_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIRC_SUSAIH_FK_I ON TASY.SUS_AIH_REG_CIVIL
(NR_AIH, NR_SEQ_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIRC_SUSAIH_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Sus_Aih_reg_civil_Atual
AFTER INSERT or UPDATE ON TASY.SUS_AIH_REG_CIVIL FOR EACH ROW
DECLARE

nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
nr_atendimento_w		Number(10,0);
dt_entrada_w			Date;
dt_alta_w			Date;
qt_reg_w			Number(10,0);

BEGIN

select	nvl(min(nr_atendimento),0)
into	nr_atendimento_w
from	sus_aih a
where	a.nr_aih		= :new.nr_aih;

select	nvl(min(nr_sequencia),0)
into	nr_sequencia_w
from	procedimento_paciente
where	nr_atendimento	= nr_atendimento_w
and	ie_origem_proced	= 2
and	cd_procedimento	in (
	35001011,35006013,35007010,35023015,35027010,35025018,35080019, 35021012,35024011,
	35009012,35022019,35026014,35028017,35082011,35084014,35083018,35085010);

select	count(*)
into	qt_reg_w
from	procedimento_paciente
where	nr_atendimento	= nr_atendimento_w
and	cd_procedimento	= 99085011
and	ie_origem_proced	= 2;

select	max(trunc(dt_entrada,'dd')),
	max(nvl(dt_alta, sysdate + 300))
into	dt_entrada_w,
	dt_alta_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;

if	(:new.dt_nascimento	< dt_entrada_w) or
	(:new.dt_nascimento > dt_alta_w) then
	--r.aise_application_error(-20011,'A data de nascimento n�o pode ser menor que a entrada ou maior  que alta');
	wheb_mensagem_pck.exibir_mensagem_abort(263433);
end if;

if	(nr_sequencia_w > 0) and
	(qt_reg_w = 0) then
	Duplicar_proc_paciente(nr_sequencia_w, :new.nm_usuario, nr_sequencia_ww);
	update	procedimento_paciente
	set	cd_procedimento	= 99085011,
		cd_medico_executor	= null,
		ie_tipo_servico_sus	= 36,
		ie_tipo_ato_sus	= 46
	where	nr_sequencia		= nr_sequencia_ww;
	Atualiza_Preco_Proc_Sus(nr_sequencia_ww, :new.nm_usuario); /*Felipe OS 27032 15/12/2005*/
end if;

END;
/


ALTER TABLE TASY.SUS_AIH_REG_CIVIL ADD (
  CONSTRAINT SUSAIRC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_AIH_REG_CIVIL ADD (
  CONSTRAINT SUSAIRC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SUSAIRC_SUSAIH_FK 
 FOREIGN KEY (NR_AIH, NR_SEQ_AIH) 
 REFERENCES TASY.SUS_AIH (NR_AIH,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUSAIRC_PESJURI_FK 
 FOREIGN KEY (CD_CGC_CARTORIO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.SUS_AIH_REG_CIVIL TO NIVEL_1;

