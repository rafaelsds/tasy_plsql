ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PACIENTE_UNIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PACIENTE_UNIDADE
(
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  CD_SETOR_ATENDIMENTO         NUMBER(5)        NOT NULL,
  CD_UNIDADE_BASICA            VARCHAR2(10 BYTE) NOT NULL,
  CD_UNIDADE_COMPL             VARCHAR2(10 BYTE) NOT NULL,
  DT_ENTRADA_UNIDADE           DATE             NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_TIPO_ACOMODACAO           NUMBER(4),
  DT_SAIDA_UNIDADE             DATE,
  NR_ATEND_DIA                 NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(2000 BYTE),
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NM_USUARIO_ORIGINAL          VARCHAR2(15 BYTE),
  DT_SAIDA_INTERNO             DATE,
  IE_PASSAGEM_SETOR            VARCHAR2(1 BYTE),
  NR_ACOMPANHANTE              NUMBER(3),
  NR_SEQ_INTERNO               NUMBER(10)       NOT NULL,
  IE_CALCULAR_DIF_DIARIA       VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_TRANSF         NUMBER(10),
  DT_ENTRADA_REAL              DATE,
  NM_USUARIO_REAL              VARCHAR2(15 BYTE),
  IE_RADIACAO                  VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_DIF            NUMBER(10),
  NR_SEQ_MOT_DIF_DIARIA        NUMBER(10),
  CD_UNIDADE_EXTERNA           VARCHAR2(60 BYTE),
  DT_ALTA_MEDICO_SETOR         DATE,
  CD_MOTIVO_ALTA_SETOR         NUMBER(5),
  CD_PROCEDENCIA_SETOR         NUMBER(5),
  NR_SEQ_MOTIVO_INT            NUMBER(10),
  NR_SEQ_MOTIVO_INT_SUB        NUMBER(10),
  NR_SEQ_MOTIVO_PERM           NUMBER(10),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_CIRURGIA                  NUMBER(10),
  NR_SEQ_PEPO                  NUMBER(10),
  NR_SEQ_AGRUPAMENTO           NUMBER(10),
  QT_TEMPO_PREV                NUMBER(10),
  NR_SEQ_UNID_ANT              NUMBER(10),
  DT_SAIDA_TEMPORARIA          DATE,
  DT_RETORNO_SAIDA_TEMPORARIA  DATE,
  ID_LEITO_TEMP_CROSS          NUMBER(10),
  CD_DEPARTAMENTO              NUMBER(10),
  CD_PROCEDENCIA_ATEND         NUMBER(5),
  NR_SEQ_CLASSIF_ESP           NUMBER(10),
  IE_ANZICS_GENERATED          VARCHAR2(1 BYTE),
  DT_EST_RETURN                DATE,
  CD_EVOLUCAO                  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPACU_AGRUSET_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_AGRUSET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_ATEPACI_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          55M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_CIRURGI_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_CLAEPAC_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_CLASSIF_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_DEPMED_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_EVOPACI_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I1 ON TASY.ATEND_PACIENTE_UNIDADE
(DT_SAIDA_INTERNO, CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL, DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          145M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I2 ON TASY.ATEND_PACIENTE_UNIDADE
(NR_ATENDIMENTO, DT_ENTRADA_UNIDADE, CD_SETOR_ATENDIMENTO, DT_SAIDA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I3 ON TASY.ATEND_PACIENTE_UNIDADE
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL, DT_SAIDA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          112M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I4 ON TASY.ATEND_PACIENTE_UNIDADE
(NR_ATENDIMENTO, CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1712K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I6 ON TASY.ATEND_PACIENTE_UNIDADE
(CD_SETOR_ATENDIMENTO, DT_ENTRADA_UNIDADE, DT_SAIDA_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_I7 ON TASY.ATEND_PACIENTE_UNIDADE
(DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_MODIFAC_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOTIVO_DIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MODIFAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MODIFDI_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOT_DIF_DIARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MODIFDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MOINSES_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOTIVO_INT_SUB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MOINSES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MOINSET_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOTIVO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MOINSET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MOTALSE_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_MOTIVO_ALTA_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MOTALSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MOTTRPA_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOTIVO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MOTTRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_MOTTRPA_FK2_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_MOTIVO_PERM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_MOTTRPA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_PEPOCIR_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEPACU_PK ON TASY.ATEND_PACIENTE_UNIDADE
(NR_ATENDIMENTO, DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_PROCEDE_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_PROCEDENCIA_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_PROSETO_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_PROCEDENCIA_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_PROSETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACU_TIPACOM_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          45M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACU_TIPACOM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEPACU_UK ON TASY.ATEND_PACIENTE_UNIDADE
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACU_UNIATEN_FK_I ON TASY.ATEND_PACIENTE_UNIDADE
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unid_aftinsert
AFTER INSERT ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE

cd_classif_setor_w			varchar2(2);
QT_PAC_UNIDADE_w			number(3,0);
qt_em_unidade_w				number(3,0);
nr_seq_interno_unidade_w	number(10,0);
ie_integra_regul_w			regulacao_parametro.ie_integracao_ativa%type;
nr_seq_regulacao_w			regulacao_atendimento.nr_regulacao%type;
ie_internacao_w				varchar2(1);
nr_externo_w				unidade_atendimento.nr_externo%type;
qt_reg_w					number(10);
ie_tipo_atend_w				number(3);
cd_classif_setor_cross_w 	number(10);
qt_conv_int_cross_w			number(10);
ie_operacao_w				varchar2(1);
ie_cons_episodio_w			varchar2(1);
dt_cancelamento_w			atendimento_paciente.dt_cancelamento%type;
dt_alta_w					atendimento_paciente.dt_alta%type;
nr_seq_episodio_w			atendimento_paciente.nr_seq_episodio%type;
nr_seq_tipo_admissao_fat_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;
cd_pessoa_fisica_w			atendimento_paciente.cd_pessoa_fisica%type;
dt_entrada_w				atendimento_paciente.dt_entrada%type;
nr_atendimento_mae_w		atendimento_paciente.nr_atendimento_mae%type;
qt_tempo_regra_w     		wl_regra_item.qt_tempo_regra%type;
qt_tempo_normal_w			wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_w				wl_regra_item.nr_sequencia%type;
ie_escala_w					wl_regra_item.ie_escala%type;
ie_opcao_wl_w				wl_regra_item.ie_opcao_wl%type;
nm_tabela_w					vice_escala.nm_tabela%type;
cd_setor_atendimento_w		wl_perfil.cd_setor_atendimento%type;

cursor c01 is
	select	nvl(b.qt_tempo_regra, 0) qt_tempo_regra_w,
			nvl(b.qt_tempo_normal, 0) qt_tempo_normal_w,
			nvl(b.nr_sequencia, 0) nr_seq_regra_w,
			nvl(b.ie_escala, '') ie_escala_w,
			b.ie_opcao_wl ie_opcao_wl_w,
			(select	max(nm_tabela)
			from	vice_escala
			where	ie_escala = b.ie_escala) nm_tabela_w,
			c.cd_setor_atendimento cd_setor_atendimento_w
	from 	wl_regra_worklist a,
			wl_regra_item b,
			wl_regra_geracao c
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.nr_sequencia = c.nr_seq_regra_item
	and		b.ie_situacao = 'A'
	and		c.cd_setor_atendimento is not null
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'S'
								and		x.ie_situacao = 'A');

begin

cd_classif_setor_cross_w := obter_classif_setor_cross(:new.cd_setor_atendimento);
qt_conv_int_cross_w := obter_conv_int_cross(:new.nr_atendimento);

if	(:new.dt_saida_unidade is null) then
	select 	max(cd_classif_setor)
	into	cd_classif_setor_w
	from	setor_atendimento
	where	cd_setor_atendimento = :new.cd_setor_atendimento;

	if	(cd_classif_setor_w in ('1','5')) then

		select	max(nr_seq_interno)
		into	nr_seq_interno_unidade_w
		from 	unidade_atendimento a
		WHERE 	A.cd_unidade_basica 	= :new.cd_unidade_basica
		AND 	A.cd_unidade_compl  	= :new.cd_unidade_compl
		AND 	A.cd_setor_atendimento 	= :new.cd_setor_atendimento;

		insert into controle_atend_unidade(	nr_sequencia,
							nr_atendimento,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_interno,
							nr_seq_atepacu,
							dt_entrada)
					values	(	controle_atend_unidade_seq.nextval,
							:new.nr_atendimento,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							nr_seq_interno_unidade_w,
							:new.NR_SEQ_INTERNO,
							sysdate);

	end if;

end if;

select	nvl(max(ie_integracao_ativa),'N')
into	ie_integra_regul_w
from	regulacao_parametro;

if	(ie_integra_regul_w = 'S') then

	select	max(nr_sequencia)
	into	nr_seq_regulacao_w
	from	regulacao_atendimento
	where	nr_atendimento	= :new.nr_atendimento;

	if	(nvl(nr_seq_regulacao_w,0) > 0) then
		reg_movimentar_paciente(nr_seq_regulacao_w, :new.nr_atendimento,:new.nm_usuario);
	end if;
end if;

select	NVL(NR_EXTERNO,0)
into	nr_externo_w
from	unidade_atendimento
where	CD_UNIDADE_BASICA = :new.CD_UNIDADE_BASICA
and		CD_UNIDADE_COMPL = :new.CD_UNIDADE_COMPL
and		cd_setor_atendimento = :new.cd_setor_atendimento;

if	(nr_externo_w is not null) and
	(cd_classif_setor_cross_w > 0) and
	(qt_conv_int_cross_w > 0) then --adicionado consist�ncia para verificar se existe regra na classifica��o do setor do cross.

	select	max(ie_tipo_atendimento),
		max(nr_atendimento_mae)
	into	ie_tipo_atend_w,
		nr_atendimento_mae_w
	from	atendimento_Paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if(:new.NR_SEQ_UNID_ANT is null) then
		-- Interna��o

		select count(*)
		into 	qt_reg_w
		from 	diagnostico_doenca
		where 	nr_atendimento = :new.nr_atendimento;

		if ((qt_reg_w > 0) or (nr_atendimento_mae_w is not null)) and (nvl(ie_tipo_atend_w,0) = 1) then
			gravar_integracao_cross(277, 'NR_ATENDIMENTO='|| :new.nr_atendimento ||';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
		end if;
	else
		select NVL(NR_EXTERNO, 0)
		into nr_externo_w
		from unidade_atendimento
		where nr_seq_interno = :new.NR_SEQ_UNID_ANT;

		if(nr_externo_w is null) or (nr_atendimento_mae_w is not null) then
			-- Interna��o
			select 	count(*)
			into 	qt_reg_w
			from 	diagnostico_doenca
			where 	nr_atendimento = :new.nr_atendimento;

			if ((qt_reg_w > 0) or (nr_atendimento_mae_w is not null)) and (nvl(ie_tipo_atend_w,0) = 1) then
				gravar_integracao_cross(277, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
			end if;
		else
			-- Movimenta��o
			gravar_integracao_cross(278, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';NR_SEQ_INTERNO=' || :NEW.nr_seq_interno || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
		end if;
	end if;
else
	if	(:new.NR_SEQ_UNID_ANT is not null) and
		(cd_classif_setor_cross_w > 0) and
		(qt_conv_int_cross_w > 0) then

		select 	NVL(NR_EXTERNO, 0)
		into 	nr_externo_w
		from 	unidade_atendimento
		where 	nr_seq_interno = :new.NR_SEQ_UNID_ANT;

		if(nr_externo_w is not null) then
			--Saida
			gravar_integracao_cross(280, 'NR_ATENDIMENTO='|| :new.nr_atendimento ||';NR_SEQ_INTERNO=' || :NEW.nr_seq_interno ||';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
		end if;
	end if;
end if;

begin
select	max(dt_cancelamento),
	max(dt_alta)
into	dt_cancelamento_w,
	dt_alta_w
from	atendimento_Paciente
where	nr_atendimento	= :new.nr_atendimento;
exception
when others then
	dt_cancelamento_w 	:= null;
	dt_alta_w		:= null;
end;


if (obter_qtd_pac_atendunid_pragma(:new.nr_atendimento) <= 0) then
	begin
	ie_operacao_w := 'I';
	end;
else
	begin
	ie_operacao_w := 'A';
	end;
end if;

select	count(*)
into	qt_reg_w
from	intpd_fila_transmissao
where	ie_evento = '214'
and	ie_status = 'P'
and	nr_seq_documento = :new.nr_atendimento
and	nvl(ie_controle_tag,'0') = '0';

if	(qt_reg_w > 0) then
	update	intpd_fila_transmissao
	set	ie_controle_tag = '1'
	where	ie_evento = '214'
	and	ie_status = 'P'
	and	nr_seq_documento = :new.nr_atendimento
	and	nvl(ie_controle_tag,'0') = '0';
end if;

if	(dt_alta_w is null) and
	(dt_cancelamento_w is null) then
	begin

	intpd_enviar_atendimento(:new.nr_atendimento, ie_operacao_w, '1', :new.nm_usuario);
	/*O parametro ie_controle_tag_p � usado para controlar se foi uma movimenta��o de paciente.
		0 - Nao foi uma movimenta��o
		1 - Foi uma movimenta��o
	Neste caso, est� inserindo uma nova movimenta��o, ent�o � 1 */
	end;
end if;

open c01;
loop
fetch c01 into
	qt_tempo_regra_w,
	qt_tempo_normal_w,
	nr_seq_regra_w,
	ie_escala_w,
	ie_opcao_wl_w,
	nm_tabela_w,
	cd_setor_atendimento_w;
exit when c01%notfound;
	begin

	select	nr_seq_episodio,
			nr_seq_tipo_admissao_fat,
			cd_pessoa_fisica,
			dt_entrada
	into	nr_seq_episodio_w,
			nr_seq_tipo_admissao_fat_w,
			cd_pessoa_fisica_w,
			dt_entrada_w
	from 	atendimento_paciente
	where 	nr_atendimento = :new.nr_atendimento;

	select	nvl2(nr_atendimento_mae_w,'N','S')
	into	ie_cons_episodio_w
	from 	dual;

	if(obter_se_regra_geracao(nr_seq_regra_w,nr_seq_episodio_w, nr_seq_tipo_admissao_fat_w, :new.cd_setor_atendimento) = 'S' and cd_setor_atendimento_w = :new.cd_setor_atendimento) then
		-- Gera Tarefa na Task list para escalas e �ndices

		if (qt_tempo_normal_w > 0 and (ie_opcao_wl_w = 'A' or ie_opcao_wl_w = 'E')) then
			wl_gerar_finalizar_tarefa('S','I',:new.nr_atendimento,cd_pessoa_fisica_w,:new.nm_usuario,:new.dt_entrada_unidade+(qt_tempo_normal_w/24),'N',null,null,null,null,null,null,ie_escala_w,null,null,nr_seq_regra_w,null,null,null,null,null,nm_tabela_w,null,dt_entrada_w,nr_seq_episodio_w,null,null,null,null,ie_cons_episodio_w);
		elsif (qt_tempo_regra_w > 0 and ie_opcao_wl_w = 'D') then
			wl_gerar_finalizar_tarefa('S','I',:new.nr_atendimento,cd_pessoa_fisica_w,:new.nm_usuario,:new.dt_entrada_unidade+(qt_tempo_regra_w/24),'N',null,null,null,null,null,null,ie_escala_w,null,null,nr_seq_regra_w,null,null,null,null,null,nm_tabela_w,null,dt_entrada_w,nr_seq_episodio_w,null,null,null,null,ie_cons_episodio_w);
		end if;
	end if;
	end;
end loop;
close c01;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_after
after insert or update or delete ON TASY.ATEND_PACIENTE_UNIDADE for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
dt_saida_real_w		date;

ie_sem_acomodacao_w tipo_acomodacao.ie_sem_acomodacao%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(inserting) then
		insert_atend_unid_classif_esp(null, null, :new.nr_atendimento, :new.nr_seq_interno, :new.nm_usuario);
	end if;

	if	(:new.nr_sequencia = 1) and
		(inserting) then
		reg_integracao_p.ie_operacao := 'I';
		gerar_int_padrao.gravar_integracao('308',:new.nr_seq_interno,:new.nm_usuario, reg_integracao_p);
	end if;

	select	max(dt_saida_real)
	into	dt_saida_real_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;
	if (dt_saida_real_w is null) then
		reg_integracao_p.ie_saida_unidade	:= 'N';
		if	(inserting or updating) then
			if	(:old.dt_saida_unidade is not null and :new.dt_saida_unidade is null) or
				(:old.dt_saida_unidade is null and :new.dt_saida_unidade is not null)  then
				reg_integracao_p.ie_saida_unidade	:= 'S';
			end if;
		end if;
		reg_integracao_p.nr_atendimento		:= nvl(:new.nr_atendimento,:old.nr_atendimento);

		select nvl(max(t.ie_sem_acomodacao), 'N')
		into	reg_integracao_p.ie_passagem_sem_acomd
		from	unidade_atendimento c,
				tipo_acomodacao t
		where	:new.cd_setor_atendimento = c.cd_setor_atendimento
		and		:new.cd_unidade_basica = c.cd_unidade_basica
		and		:new.cd_unidade_compl = c.cd_unidade_compl
		and		t.cd_tipo_acomodacao = c.cd_tipo_acomodacao;

		if	(inserting) then
			reg_integracao_p.ie_operacao		:=	'I';
		elsif	(deleting) then
			reg_integracao_p.ie_operacao		:=	'E';
		elsif	(updating) then
			reg_integracao_p.ie_operacao		:=	'A';
		end if;

		if (:new.ie_passagem_setor = 'N') then
		   reg_integracao_p.ie_tipo_movimentacao := 'T';
		else
		   reg_integracao_p.ie_tipo_movimentacao := 'P';
		end if;

		if (:new.nr_sequencia = 1) then
		   reg_integracao_p.ie_primeiro_reg_setor := 'P';
		elsif (:new.nr_sequencia > 1) then
		   reg_integracao_p.ie_primeiro_reg_setor := 'S';
		end if;

		gerar_int_padrao.gravar_integracao('118',nvl(:new.nr_seq_interno,:old.nr_seq_interno),nvl(:new.nm_usuario,:old.nm_usuario), reg_integracao_p);
	end if;

	reg_integracao_p.ie_operacao	:= 'E';

	if	(:old.dt_saida_temporaria is null and :new.dt_saida_temporaria is not null) then
		reg_integracao_p.ie_operacao		:=	'I';
	elsif (:old.dt_retorno_saida_temporaria is null and :new.dt_retorno_saida_temporaria is not null) then
		reg_integracao_p.ie_operacao		:=	'A';
	end if;

	if (reg_integracao_p.ie_operacao <> 'E') then
		gerar_int_padrao.gravar_integracao('195',nvl(:new.nr_seq_interno,:old.nr_seq_interno),nvl(:new.nm_usuario,:old.nm_usuario), reg_integracao_p);
	end if;

	if(:new.cd_departamento <> :old.cd_departamento) then
		call_interface_file(932, 'carestream_ris_japan_l10n_pck.patient_dept_transfer_info ('||  :new.nr_seq_interno || ', 1, ''' || :new.nm_usuario || ''' , 0, null, ' || obter_estabelecimento_ativo || ', ''N'');' , :new.nm_usuario);
		call_interface_file(948, 'tosho_pck.patient_dept_transfer_info ('||  :new.nr_seq_interno || ', 1, ''' || :new.nm_usuario || ''' , 0, null, ' || obter_estabelecimento_ativo || ', ''N'');' , :new.nm_usuario);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PACIENTE_UNID_AFTUPDATE
AFTER UPDATE ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE
ie_alta_w				varchar2(1);
nr_externo_w			unidade_atendimento.nr_externo%type;
cd_classif_setor_cross_w 	number(10);
qt_conv_int_cross_w		number(10);
ie_operacao_w			varchar2(1);
ie_cancelado_w			varchar2(1);

begin

cd_classif_setor_cross_w := obter_classif_setor_cross(:new.cd_setor_atendimento);
qt_conv_int_cross_w := obter_conv_int_cross(:new.nr_atendimento);

select 	max(NR_EXTERNO)
into	nr_externo_w
from 	unidade_atendimento
where 	CD_UNIDADE_BASICA = :new.CD_UNIDADE_BASICA
and  	CD_UNIDADE_COMPL = :new.CD_UNIDADE_COMPL
and 	cd_setor_atendimento = :new.cd_setor_atendimento;

select 	decode(dt_alta, null, 'N', 'S'),
	decode(dt_cancelamento, null, 'N', 'S')
into 	ie_alta_w,
	ie_cancelado_w
from 	atendimento_paciente
where 	nr_atendimento = :new.nr_atendimento;

if  (nvl(:old.dt_saida_unidade,SYSDATE) <> nvl(:new.dt_saida_unidade,SYSDATE)) and
	(nr_externo_w is not null) and
	(cd_classif_setor_cross_w > 0) and
	(qt_conv_int_cross_w > 0) then

	if (:old.dt_saida_unidade is null and :new.dt_saida_unidade is not null and ie_alta_w = 'S') then
			-- Sa�da
			gravar_integracao_cross(280, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
	ELSIF (:old.dt_saida_unidade is not null and :new.dt_saida_unidade is null and ie_alta_w = 'N') then --(Desfazer alta)
		-- Interna��o
		gravar_integracao_cross(277, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
	end if;
end if;

/*S� manda para integra��o se ainda n�o teve alta. A informa��o de ALTA � enviada pela trigger ATENDIMENTO_PACIENTE_AFTUPDATE*/
if	(ie_cancelado_w = 'N') and
	(ie_alta_w = 'N') then
	ie_operacao_w := 'A';

	intpd_enviar_atendimento(:new.nr_atendimento, ie_operacao_w, '0', :new.nm_usuario);
	/*O parametro ie_controle_tag_p � usado para controlar se foi uma movimenta��o de paciente.
		0 - Nao foi uma movimenta��o
		1 - Foi uma movimenta��o
	Neste caso, n�o � uma inser��o na movimenta��o do paciente, ent�o � 0 */
end if;



END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PACIENTE_UNIDADE_tp  after update ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@DT_ENTRADA_UNIDADE='||to_char(:old.DT_ENTRADA_UNIDADE); ds_w:=substr(:new.CD_UNIDADE_COMPL,1,500);gravar_log_alteracao(substr(:old.CD_UNIDADE_COMPL,1,4000),substr(:new.CD_UNIDADE_COMPL,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_COMPL',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_TIPO_ACOMODACAO,1,500);gravar_log_alteracao(substr(:old.CD_TIPO_ACOMODACAO,1,4000),substr(:new.CD_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ACOMODACAO',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_UNIDADE_BASICA,1,500);gravar_log_alteracao(substr(:old.CD_UNIDADE_BASICA,1,4000),substr(:new.CD_UNIDADE_BASICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_BASICA',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ENTRADA_UNIDADE,1,4000),substr(:new.DT_ENTRADA_UNIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTRADA_UNIDADE',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_Delete
BEFORE DELETE ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE
ie_leito_livre_w	varchar2(1);
cd_estabelecimento_w	number(4,0);
qt_unid_atend_mat_w	number(8,0);
qt_unid_atend_proc_w	number(8,0);

BEGIN

gravar_log_exclusao('ATEND_PACIENTE_UNIDADE',:old.nm_usuario,
		'NR_ATENDIMENTO=' || :old.nr_atendimento || ', DT_ENTRADA_UNIDADE=' ||
		PKG_DATE_FORMATERS_TZ.to_varchar(:old.dt_entrada_unidade,'timestamp',ESTABLISHMENT_TIMEZONE_UTILS.getTimezone)
		||', NR_SEQ_INTERNO=' || :old.nr_seq_interno || ', CD_SETOR_ATENDIMENTO=' || :old.cd_setor_atendimento ,'N'
		|| ', get_ie_executar_trigger='|| wheb_usuario_pck.get_ie_executar_trigger);

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto final;
end if;

select 	count(*)
into	qt_unid_atend_mat_w
from 	material_atend_paciente
where 	nr_seq_atepacu = :old.nr_seq_interno
and	cd_motivo_exc_conta is null;

select 	count(*)
into	qt_unid_atend_proc_w
from 	procedimento_paciente
where 	nr_seq_atepacu = :old.nr_seq_interno
and	cd_motivo_exc_conta is null;

if	((qt_unid_atend_mat_w > 0) or (qt_unid_atend_proc_w > 0)) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267485);

end if;

if	((nvl(:old.ie_passagem_setor,'N') = 'N') or
	(nvl(:new.ie_passagem_setor,'L') = 'L')) then
	begin

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	setor_atendimento
	where	cd_setor_atendimento	=	:old.cd_setor_atendimento;

	select	nvl(max(ie_leito_livre),'N')
	into	ie_leito_livre_w
	from	parametro_atendimento
	where	cd_estabelecimento	=	cd_estabelecimento_w;

	UPDATE UNIDADE_ATENDIMENTO A
	SET A.NR_ATENDIMENTO      = null,
	    A.DT_ENTRADA_UNIDADE  = null,
	    A.IE_STATUS_UNIDADE   =
		decode(nvl(a.ie_higienizacao,'N'),'N',
			Decode(nvl(a.cd_paciente_reserva,a.nm_pac_reserva),null,'L','R'),decode(ie_leito_livre_w,'N',a.ie_higienizacao,'L')),
	    a.nm_usuario = nvl(:new.nm_usuario, 'TASY'),
	    a.dt_atualizacao = sysdate
	WHERE A.cd_unidade_basica 	= :old.cd_unidade_basica
	AND A.cd_unidade_compl  	= :old.cd_unidade_compl
	AND A.nr_atendimento    	= :old.nr_atendimento
	AND A.dt_entrada_unidade	= :old.dt_entrada_unidade
	AND A.cd_setor_atendimento 	= :old.cd_setor_atendimento
	AND A.cd_setor_atendimento in
	      (select cd_setor_atendimento
	       from setor_atendimento
	       where cd_classif_setor in (3,4,8));

	UPDATE UNIDADE_ATENDIMENTO A
	SET A.dt_higienizacao		= null,
    	    A.dt_inicio_higienizacao	= sysdate,
	    a.nm_usuario		= nvl(:new.nm_usuario,'TASY'),
	    a.dt_atualizacao		= sysdate,
	    a.nm_usuario_higienizacao	= :old.nm_usuario
	WHERE A.cd_unidade_basica 	= :old.cd_unidade_basica
	  AND A.cd_unidade_compl  	= :old.cd_unidade_compl
	  AND A.cd_setor_atendimento 	= :old.cd_setor_atendimento
	  AND a.ie_status_unidade	= 'H';

	Atualizar_Unidade_atendimento(:old.cd_setor_atendimento,
					:old.cd_unidade_basica,
					:old.cd_unidade_compl);
	end;
end if;

<<final>>
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_update
BEFORE UPDATE ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE
ie_tipo_convenio_w		number(2,0);
cd_classif_setor_w		Varchar2(02);
dt_entrada_w			Date;
qt_existe_tel_w			Number(3);
nr_ramal_w			Varchar2(5);
ie_transfere_prontuario_w	varchar2(1);
nr_seq_interno_w		number(10,0);
nr_seq_evento_w			number(10,0);
cd_pessoa_fisica_w		varchar2(10);
cd_estabelecimento_w		number(5,0);
qt_idade_w			number(10);
ie_alta_w			varchar2(1);
ie_higienizacao_w		varchar2(1);
ie_gera_eritel_w		varchar2(3);
ie_novo_status_w		varchar2(1);
ie_status_atual_w		varchar2(1);
ie_aguar_hig_transf_w		varchar2(1);
ie_considera_pac_leito_w	varchar2(1);
ie_integracao_epimed_w		number(2);
nr_seq_unidade_w		number(10);
nr_seq_regra_princ_w		number(10);
qt_regra_w			number(10);
ie_carater_inter_sus_w		varchar2(10);
ie_tipo_atendimento_w		varchar2(10);
nr_seq_classificacao_w		number(10);
ie_clinica_w			number(5);
ie_higienizar_transferencia_w	varchar2(1);
ie_aguard_hig_transferencia_w	varchar2(1);
ie_status_transferencia_w	varchar2(1);
ie_questiona_hig_transf_w	varchar2(1);
ie_tipo_w					Number(10);
cd_setor_atend_w			Varchar2(10);
cd_unid_bas_w				Varchar2(10);
cd_unid_compl_w				Varchar2(10);
ie_controle_hig_w			Varchar2(1);
ie_perm_hig_w				Varchar2(1);
ie_perm_hig_inic_w			Varchar2(1);
ie_higieniza_w				Varchar2(1);
qt_unid_bloq_w				Number(10);
ie_unid_bloq_w				Varchar2(1);
ie_aguard_hig_w				varchar2(1);
cd_tipo_acomodacao_leito_w		varchar2(5);
ie_considera_tipo_acomod_w		varchar2(1);
ie_move_paciente_w varchar2(02);
ie_recebe_paciente_w varchar2(02);
ie_aceita_outpatient_w varchar2(02);
ie_encounter_outpatient_w varchar2(02);
qt_reg_w	number(1);
ie_permite_data_menor_w			varchar2(1);

Cursor C02 is
	select	a.nr_seq_evento
	from	regra_envio_sms a,
		regra_envio_sms_atend b
	where	a.cd_estabelecimento	= cd_estabelecimento_w
	and	a.ie_evento_disp	= 'ERA'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(a.ie_situacao,'A') = 'A'
	and	a.nr_sequencia = b.nr_seq_regra(+)
	and	nvl(b.ie_carater_inter_sus,nvl(ie_carater_inter_sus_w,0)) = nvl(ie_carater_inter_sus_w,'0')
	and	nvl(b.ie_tipo_atendimento,nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,'0')
	and	nvl(b.ie_clinica,nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,'0')
	and	nvl(b.nr_seq_classificacao,nvl(nr_seq_classificacao_w,0)) = nvl(nr_seq_classificacao_w,'0');

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

if	(:new.cd_departamento = 0) then
	:new.cd_departamento := null;
end if;

Obter_Param_Usuario(916, 662, Obter_perfil_Ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_data_menor_w);

select 	nvl(max(1), 0)
into 	qt_reg_w
from 	departamento_medico;
if (qt_reg_w > 0) then
	select nvl(max(a.ie_move_paciente), 'N')
	into ie_move_paciente_w
	from departamento_medico a
	where a.cd_departamento = :old.cd_departamento;

	select nvl(max(a.ie_recebe_paciente), 'N')
	into ie_recebe_paciente_w
	from departamento_medico a
	where a.cd_departamento = :new.cd_departamento;

	select nvl(max(a.ie_aceita_outpatient), 'N')
	into ie_aceita_outpatient_w
	from departamento_medico a
	where a.cd_departamento = :new.cd_departamento;

	select
	 case
	   WHEN ie_tipo_atendimento <> 1 THEN 'S'
	   WHEN ie_tipo_atendimento =  1 THEN 'N'
	    ELSE 'N'
	 end INTO ie_encounter_outpatient_w
	from atendimento_paciente
	where nr_atendimento = :new.nr_atendimento;


	if	((nvl(:new.cd_departamento, 0) <> 0) and ((:new.cd_departamento) <> (:old.cd_departamento))) then
		if (ie_move_paciente_w = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(997494);
		elsif(ie_recebe_paciente_w = 'N') then
		 wheb_mensagem_pck.exibir_mensagem_abort(997511);
		elsif(ie_aceita_outpatient_w = 'N' AND ie_encounter_outpatient_w = 'S') then
	        wheb_mensagem_pck.exibir_mensagem_abort(997502);
		end if;
	end if;
end if;

if	((:new.cd_setor_atendimento) <> (:old.cd_setor_atendimento)) and
	((:new.dt_entrada_unidade) = (:old.dt_entrada_unidade)) and
	((:new.nr_seq_interno) = (:old.nr_seq_interno)) then

	insert 	into log_mov
			(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
		values	(sysdate, :new.nm_usuario, 98822, ' ' || wheb_mensagem_pck.get_texto(800315) || ': ' || :new.nr_atendimento ||
							  ' ' || wheb_mensagem_pck.get_texto(803148) || ': ' || :new.cd_setor_atendimento ||
							  ' ' || wheb_mensagem_pck.get_texto(803149) || ': ' || :old.cd_setor_atendimento ||
							  ' ' || wheb_mensagem_pck.get_texto(803150) || ': ' || obter_funcao_ativa ||
							  ' ' || wheb_mensagem_pck.get_texto(803152) || ': ' || obter_perfil_ativo);
end if;

if	(:new.dt_saida_unidade is not null) and
	(:new.dt_entrada_unidade is not null) and
	(:new.dt_entrada_unidade > :new.dt_saida_unidade) and
	(:new.IE_CALCULAR_DIF_DIARIA = :old.IE_CALCULAR_DIF_DIARIA) then
	Wheb_mensagem_pck.exibir_mensagem_abort( 262445 , 'DT_SAIDA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_ENTRADA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));

end if;

select	Obter_Valor_Param_Usuario(3111,105, obter_perfil_ativo, :new.nm_usuario,0)
into	ie_higienizacao_w
from	dual;

select	Obter_Valor_Param_Usuario(3111,109, obter_perfil_ativo, :new.nm_usuario,0)
into	ie_gera_eritel_w
from	dual;

select	dt_entrada,
	ie_tipo_convenio,
	cd_pessoa_fisica,
	cd_estabelecimento,
	decode(dt_alta, null,
		Decode(ie_higienizacao_w,'S','N','S'), 'S'),
	ie_carater_inter_sus,
	ie_tipo_atendimento,
	nr_seq_classificacao,
	ie_clinica
into	dt_entrada_w,
	ie_tipo_convenio_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	ie_alta_w,
	ie_carater_inter_sus_w,
	ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

select	nvl(max(ie_integracao_epimed),0),
	nvl(max(ie_considera_tipo_acomod),'N')
into	ie_integracao_epimed_w,
	ie_considera_tipo_acomod_w
from	parametro_atendimento
where	cd_estabelecimento =	cd_estabelecimento_w;

qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
if	(dt_entrada_w < sysdate) and
	(:new.IE_CALCULAR_DIF_DIARIA = :old.IE_CALCULAR_DIF_DIARIA) then
	if	(:new.dt_saida_unidade is not null) and
		(:new.dt_saida_unidade > sysdate) and
		(:new.dt_saida_unidade > sysdate + 120/86400) then
		Wheb_mensagem_pck.exibir_mensagem_abort( 379546 , 'DT_SAIDA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
	end if;

	if	(:new.dt_entrada_unidade is not null) and
		(:new.dt_entrada_unidade > sysdate) then
		Wheb_mensagem_pck.exibir_mensagem_abort( 379552 , 'DT_ENTRADA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
	end if;

	if	(ie_tipo_convenio_w <> 3) and
		(ie_permite_data_menor_w	= 'N') then -- Edgar 10/01/2005 OS 14154, N?o fazer esta consist?ncias qdo for SUS por causa da interna??o BPA
		if	(dt_entrada_w > :new.dt_entrada_unidade) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 379553 , 'DT_ENTRADA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_INICIO='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
		elsif	(dt_entrada_w > :new.dt_saida_unidade) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 379554 , 'DT_SAIDA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';DT_INICIO='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
		end if;
	end if;
end if;

if	(:new.dt_saida_interno is null) then
	:new.dt_saida_interno	:= to_date('30/12/2999','dd/mm/yyyy');
end if;

:new.dt_saida_interno	:= nvl(:new.DT_SAIDA_UNIDADE,to_date('30/12/2999','dd/mm/yyyy'));

select cd_classif_setor
into cd_classif_setor_w
from setor_atendimento
where cd_setor_atendimento = :new.cd_setor_atendimento;

if	(:new.dt_saida_unidade is not null) and
	(cd_classif_setor_w in ('1','5')) then

	update	controle_atend_unidade
	set	dt_saida = sysdate
	where	nr_seq_atepacu = :new.nr_seq_interno;

end if;

select	max(a.cd_tipo_acomodacao)
into	cd_tipo_acomodacao_leito_w
from	unidade_atendimento a
where	a.cd_unidade_basica 	= :new.cd_unidade_basica
and	a.cd_unidade_compl  	= :new.cd_unidade_compl
and	a.cd_setor_atendimento 	= :new.cd_setor_atendimento;

if	((nvl(:new.ie_passagem_setor,'N') = 'N') or
	(nvl(:new.ie_passagem_setor,'L') = 'L')) and
	(((ie_considera_tipo_acomod_w = 'S') and (substr(obter_se_sem_acomodacao(cd_tipo_acomodacao_leito_w),1,1) = 'N')) or
	 ((ie_considera_tipo_acomod_w = 'N') and (cd_classif_setor_w in (3, 4, 8, 11, 12)))) then
	begin
	if  	(:new.dt_saida_unidade is not null) and
		(nvl(:new.ie_radiacao,'N') = 'N') then
		begin
		:new.dt_saida_interno     	:= :new.dt_saida_unidade;
		/*begin
		Select 	decode(nvl(a.ie_higienizacao,'N'),'N',
				Decode(a.cd_paciente_reserva,null,Decode(a.cd_convenio_reserva,null,'L','R'),'R'),
					Decode(a.ie_higienizacao, 'A', Decode(ie_alta_w,'S',a.ie_higienizacao,'H'), a.ie_higienizacao))
		into	ie_novo_status_w
		from	UNIDADE_ATENDIMENTO a
		WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
		  AND A.cd_unidade_compl  	= :new.cd_unidade_compl
		  AND A.nr_atendimento    	= :new.nr_atendimento
		  AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento;

		exception
		when others then
			null;
		end; */

		select	Obter_Valor_Param_Usuario(3111,184, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento)
		into	ie_considera_pac_leito_w
		from	dual;

		if	(ie_considera_pac_leito_w = 'F') and
			(ie_alta_w = 'N') then
			ie_aguar_hig_transf_w := 'S';
		else
			ie_aguar_hig_transf_w := 'N';
		end if;

		Obter_param_Usuario(3111, 127, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_higienizar_transferencia_w);
		Obter_param_Usuario(3111, 137, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_aguard_hig_transferencia_w);
		Obter_param_Usuario(3111, 175, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_aguard_hig_w);
		Obter_param_Usuario(3111, 302, obter_perfil_ativo, :new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_questiona_hig_transf_w);

		If	(nvl(obter_funcao_ativa, 0) = 3111) then
			/*  Status para transfer?ncia - depende dos param 127 e 137  */
			If	(ie_higienizar_transferencia_w = 'S') then -- Higienizar o leito ao transferir - param 127
				ie_status_transferencia_w := 'H';
			Elsif	(ie_aguard_hig_transferencia_w = 'S') then -- Higienizar o leito ao transferir - param 137
				ie_status_transferencia_w := 'G';
			Else
				ie_status_transferencia_w := 'L';
			end if;
		else
			ie_status_transferencia_w := 'H';
		end if;
		SELECT	DECODE(COUNT(nr_seq_unid_bloq),0,'S','N')
		INTO	ie_unid_bloq_w
		FROM 	unidade_atendimento
		WHERE	cd_unidade_basica		= :new.cd_unidade_basica
		AND		cd_unidade_compl		= :new.cd_unidade_compl
		AND		cd_setor_atendimento	= :new.cd_setor_atendimento;

		if (ie_unid_bloq_w = 'S') then
			if	((((ie_questiona_hig_transf_w = 'S') and (ie_aguard_hig_w <> 'C')) or
				(ie_aguard_hig_w = 'S')) and
				(nvl(obter_funcao_ativa, 0) = 3111)) then -- questionar higieniza??o do leito - param 302

				UPDATE	UNIDADE_ATENDIMENTO A
				SET	A.NR_ATENDIMENTO      	= null,
					A.DT_ENTRADA_UNIDADE  	= null,
					A.NM_USUARIO		= :new.nm_usuario,
					A.DT_ATUALIZACAO		= sysdate
				WHERE	A.cd_unidade_basica 	= :new.cd_unidade_basica
				AND	A.cd_unidade_compl  	= :new.cd_unidade_compl
				AND	A.nr_atendimento    	= :new.nr_atendimento
				AND	A.dt_entrada_unidade	= :new.dt_entrada_unidade
				AND	A.cd_setor_atendimento 	= :new.cd_setor_atendimento;
			else
				UPDATE	UNIDADE_ATENDIMENTO A
				SET	A.NR_ATENDIMENTO      	= null,
					A.DT_ENTRADA_UNIDADE  	= null,
					A.IE_STATUS_UNIDADE   	=
					decode(nvl(a.ie_higienizacao,'N'),'N',
						Decode(nvl(a.cd_paciente_reserva,a.nm_pac_reserva),null,Decode(a.cd_convenio_reserva,null,'L','R'),'R'),
							Decode(a.ie_higienizacao, 'A', decode(ie_aguar_hig_transf_w,'S','G',Decode(ie_alta_w,'S', a.ie_higienizacao, nvl(ie_status_transferencia_w, a.ie_status_unidade))),
								decode(ie_aguard_hig_w, 'C', 'G', a.ie_higienizacao))),
					A.NM_USUARIO		= :new.nm_usuario,
					A.DT_ATUALIZACAO	= sysdate
				WHERE	A.cd_unidade_basica 	= :new.cd_unidade_basica
				AND	A.cd_unidade_compl  	= :new.cd_unidade_compl
				AND	A.nr_atendimento    	= :new.nr_atendimento
				AND	A.dt_entrada_unidade	= :new.dt_entrada_unidade
				AND	A.cd_setor_atendimento 	= :new.cd_setor_atendimento;
			end if;

			/*if	(ie_novo_status_w = 'A') and
				(ie_gera_eritel_w = 'S') then
				Insere_W_integracao_eritel	(:new.cd_setor_atendimento,
								:new.cd_unidade_basica,
								:new.cd_unidade_compl,
								:new.nr_atendimento,
								'N',
								:new.nm_usuario);
			end if;*/

			UPDATE UNIDADE_ATENDIMENTO A
			SET 	A.dt_higienizacao		= null,
				A.dt_inicio_higienizacao	= sysdate,
				A.NM_USUARIO		= :new.nm_usuario,
				A.DT_ATUALIZACAO		= sysdate,
				a.nm_usuario_higienizacao	= :new.nm_usuario
			WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
			  AND A.cd_unidade_compl  	= :new.cd_unidade_compl
			  AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento
			  AND A.nr_atendimento    	= :new.nr_atendimento
			  AND a.ie_status_unidade	= 'H';


			/* Ricardo 26/07/2005 - Alterado abaixo para setar a data de inicio de higieniza??o quando
						for utilizado a regra de Controla no cadastro da unidade. OS 21222 */
			UPDATE UNIDADE_ATENDIMENTO A
			SET 	A.dt_higienizacao		= null,
				A.dt_inicio_higienizacao	= sysdate,
				A.NM_USUARIO		= :new.nm_usuario,
				A.DT_ATUALIZACAO		= sysdate,
				a.nm_usuario_higienizacao	= :new.nm_usuario
			WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
			  AND A.cd_unidade_compl  	= :new.cd_unidade_compl
			  AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento
			  and a.nr_atendimento 		is null
			  AND a.ie_status_unidade	= 'H';
		end if;
		end;
	elsif	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade ) or
		(nvl(:new.dt_saida_unidade,sysdate + 1000) <> :old.dt_saida_unidade ) then
		begin
		:new.dt_saida_interno       	:= to_date('30/12/2999','dd/mm/yyyy');
		select	a.IE_STATUS_UNIDADE
		into	ie_status_atual_w
		from	UNIDADE_ATENDIMENTO A
		WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
		  AND A.cd_unidade_compl  	= :new.cd_unidade_compl
		  AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento;


		if	(cd_classif_setor_w <> 8) then -- Ao desfazer a alta n?o colocar o leito em 'P' caso for Home Care.

			UPDATE	UNIDADE_ATENDIMENTO A
			SET A.NR_ATENDIMENTO      	= :new.nr_atendimento,
			    A.DT_ENTRADA_UNIDADE  	= :new.dt_entrada_unidade,
			    A.IE_STATUS_UNIDADE   	= 'P',
			    A.NM_USUARIO		= :new.nm_usuario,
			    A.DT_ATUALIZACAO		= sysdate
			WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
			  AND A.cd_unidade_compl  	= :new.cd_unidade_compl
			  AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento;
		end if;

		if 	(ie_status_atual_w = 'A')
		and	(:new.dt_saida_unidade is null)
		and	(:old.dt_saida_unidade is not null)
		and	((ie_gera_eritel_w = 'S') or
			 (ie_gera_eritel_w = 'APT')) then
			Insere_W_integracao_eritel	(:new.cd_setor_atendimento,
							:new.cd_unidade_basica,
							:new.cd_unidade_compl,
							:new.nr_atendimento,
							'S',
							:new.nm_usuario,
							cd_estabelecimento_w);
		end if;
		end;
	else
		begin

		insert 	into log_mov
			(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
		values	(sysdate, :new.nm_usuario, 5750, ' ' || wheb_mensagem_pck.get_texto(800315) || ': ' || :new.nr_atendimento ||
							 ' ' || wheb_mensagem_pck.get_texto(796345) || ' ' || :new.cd_setor_atendimento ||
							 ' ' || wheb_mensagem_pck.get_texto(796346) || ': ' || :new.cd_unidade_basica ||
							 ' ' || wheb_mensagem_pck.get_texto(796347) || ': ' || :new.cd_unidade_compl ||
							 ' ' || wheb_mensagem_pck.get_texto(802055) || ': ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
		end;
	end if;

	if	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade ) or
		(:new.dt_saida_unidade <> nvl(:old.dt_saida_unidade, sysdate + 5000)) then
		Atualizar_Unidade_atendimento(
					:new.cd_setor_atendimento,
					:new.cd_unidade_basica,
					:new.cd_unidade_compl);

	end if;
	end;
elsif  (:new.dt_saida_unidade is not null)  then
	:new.dt_saida_interno     	:= :new.dt_saida_unidade;
end if;

if	(:new.dt_saida_unidade is not null) and
	(:old.dt_saida_unidade is null) then

	select	max(nr_seq_interno)
	into	nr_seq_unidade_w
	from	unidade_atendimento
	where	cd_setor_atendimento = :new.cd_setor_atendimento
	and		cd_unidade_basica    = :new.cd_unidade_basica
	and		cd_unidade_compl     = :new.cd_unidade_compl;

	select	count(1)
	into	qt_unid_bloq_w
	from	unidade_atendimento
	where	nr_seq_unid_bloq = nr_seq_unidade_w;

	if (qt_unid_bloq_w > 0) then
		select	Max(cd_setor_atendimento),
				Max(cd_unidade_basica),
				Max(cd_unidade_compl)
		into	cd_setor_atend_w,
				cd_unid_bas_w,
				cd_unid_compl_w
		from	unidade_atendimento
		where	nr_seq_unid_bloq = nr_seq_unidade_w;

		update	unidade_atendimento
		set		nr_seq_unid_bloq = null
		where	nr_seq_unid_bloq = nr_seq_unidade_w;

		Obter_param_Usuario(3111, 118, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_hig_w);
		Obter_param_Usuario(3111, 119, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_hig_inic_w);

		if (ie_aguard_hig_transferencia_w <> 'N') then
			ie_controle_hig_w := substr(obter_se_higieniza_leito(cd_setor_atend_w, cd_unid_bas_w, cd_unid_compl_w),1,1);
			if ((substr(obter_se_higieniza_leito(cd_setor_atend_w, cd_unid_bas_w, cd_unid_compl_w),1,1) = 'H') and
				(ie_aguard_hig_transferencia_w = 'S')) or
				(ie_aguard_hig_transferencia_w = 'C') or
				((ie_considera_pac_leito_w = 'S') and
				(ie_aguard_hig_transferencia_w = 'S') and
				(ie_controle_hig_w = 'A')) then
				ie_tipo_w := 8;
			end if;
		end if;

		if ((ie_higienizar_transferencia_w = 'S') and
			((ie_perm_hig_w = 'S') or
			(ie_perm_hig_inic_w = 'S') or
			((ie_perm_hig_inic_w = 'D') and
			(obter_se_regra_lib_setor_higie(cd_setor_atend_w, obter_perfil_ativo, :new.nm_usuario, 0) = 'S'))) and
			(ie_aguard_hig_transferencia_w = 'N')) then
			ie_tipo_w := 10;
		elsif ((ie_higienizar_transferencia_w = 'S') and
			((ie_perm_hig_w = 'S') or
			(ie_perm_hig_inic_w = 'S') or
			((ie_perm_hig_inic_w = 'D') and
			(obter_se_regra_lib_setor_higie(cd_setor_atend_w, obter_perfil_ativo, :new.nm_usuario, 0) = 'S'))) and
			(ie_aguard_hig_transferencia_w = 'N')) then
			ie_tipo_w := 10;
		end if;

		valida_higien_saida_real(ie_tipo_w, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, :new.cd_tipo_acomodacao, cd_setor_atend_w, cd_unid_bas_w, cd_unid_compl_w, ie_higieniza_w);

		if (ie_higieniza_w = 'S') then
			atualizar_higienizar_js(ie_tipo_w, cd_setor_atend_w, cd_unid_bas_w, cd_unid_compl_w, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,'N');
		end if;

	end if;

	gerar_lib_bloqueio_telefone(obter_estab_atend(:new.nr_atendimento),
					:new.nr_atendimento,
					:new.nm_usuario,
					:new.cd_setor_atendimento,
					:new.cd_unidade_basica,
					:new.cd_unidade_compl,
					12);
end if;

if	(:new.dt_saida_unidade is null) and
	(:old.dt_saida_unidade is not null) then
	gerar_lib_bloqueio_telefone(obter_estab_atend(:new.nr_atendimento),
					:new.nr_atendimento,
					:new.nm_usuario,
					:new.cd_setor_atendimento,
					:new.cd_unidade_basica,
					:new.cd_unidade_compl,
					10);
end if;

select	Obter_Valor_Param_Usuario(1007,12, obter_perfil_ativo, :new.nm_usuario,0)
into	ie_transfere_prontuario_w
from	dual;

if	(ie_transfere_prontuario_w = 'S') and (:new.cd_setor_atendimento <> :old.cd_setor_atendimento) then
	solicitar_prontuario_setor(:new.nr_atendimento,:new.cd_setor_atendimento,null,sysdate,:new.nm_usuario);
end if;

if	(:old.dt_saida_unidade is null) and (:new.dt_saida_unidade is not null) then
	select	nvl(max(nr_seq_interno),0)
	into	nr_seq_interno_w
	from	unidade_atendimento
	where	cd_unidade_basica	= :new.cd_unidade_basica
	and	cd_unidade_compl	= :new.cd_unidade_compl
	and	cd_setor_atendimento	= :new.cd_setor_atendimento;

	update	unidade_atendimento
	set	ie_status_unidade	= 'L',
		nr_seq_superior		= null,
		NM_USUARIO		= :new.nm_usuario,
		DT_ATUALIZACAO		= sysdate
	where	nr_seq_superior		= nr_seq_interno_w
	and	ie_status_unidade	= 'I';
end if;

if	(:new.dt_saida_unidade is not null) and
	(:old.dt_saida_unidade is null) and
	(:new.ie_radiacao = 'S') then
	update	unidade_atendimento
	set	ie_status_unidade	= 'I',
		ie_interditado_radiacao = 'S',
		NM_USUARIO		= :new.nm_usuario,
		DT_ATUALIZACAO		= sysdate
	where	cd_unidade_basica	= :old.cd_unidade_basica
	and	cd_unidade_compl	= :old.cd_unidade_compl
	and	cd_setor_atendimento	= :old.cd_setor_atendimento;
end if;

if	(:new.dt_entrada_real is not null) and
	(:old.dt_entrada_real is null) then

	open C02;
	loop
	fetch C02 into
		nr_seq_evento_w;
	exit when C02%notfound;
		begin

		gerar_evento_pac_trigger_setor(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario, :new.cd_unidade_basica, :new.cd_unidade_compl, :new.cd_setor_atendimento);

		end;
	end loop;
	close C02;

	:new.nm_usuario_real := wheb_usuario_pck.get_nm_usuario;
end if;

if	(ie_integracao_epimed_w = 1) and
	(cd_classif_setor_w = 4) and
	(:new.dt_saida_unidade is not null) then
		hsl_gerar_epimed_internacao(:new.cd_setor_atendimento,:new.dt_entrada_unidade,:new.nr_seq_interno,:new.nr_atendimento,:new.cd_unidade_basica,
				    :new.cd_unidade_compl,:new.nm_usuario,:new.dt_saida_unidade,cd_classif_setor_w,'A');
end if;

select 	count(*)
into	qt_regra_w
from	regra_leitos_inativacao
where	ie_situacao = 'A';

if	(:old.dt_entrada_unidade is not null) and
	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade and
	(nvl(pkg_i18n.get_user_locale, 'pt_BR') not in ('de_DE', 'de_AT'))) then
	Wheb_mensagem_pck.exibir_mensagem_abort(316262);
end if;

if (nvl(qt_regra_w,0) > 0) and
   (:old.dt_saida_unidade is null) and (:new.dt_saida_unidade is not null) then

	select	max(nr_seq_unid_princ)
	into	nr_seq_regra_princ_w
	from	regra_leitos_inativacao
	where	ie_situacao = 'A'
	and	nr_seq_unid_sec = (	select	max(nr_seq_interno)
					from	unidade_atendimento
					where 	cd_unidade_basica	= :new.cd_unidade_basica
					and 	cd_unidade_compl  	= :new.cd_unidade_compl
					and 	cd_setor_atendimento 	= :new.cd_setor_atendimento);

	if (nvl(nr_seq_regra_princ_w,0) = 0) then
		select	max(nr_seq_unid_sec)
		into	nr_seq_regra_princ_w
		from	regra_leitos_inativacao
		where	ie_situacao = 'A'
		and	nr_seq_unid_princ = (	select	max(nr_seq_interno)
						from	unidade_atendimento
						where 	cd_unidade_basica	= :new.cd_unidade_basica
						and 	cd_unidade_compl  	= :new.cd_unidade_compl
						and 	cd_setor_atendimento 	= :new.cd_setor_atendimento);
	end if;

	if (nvl(nr_seq_regra_princ_w, 0) > 0) then

		update	unidade_atendimento
		set	ie_situacao = 'A'
		where	nr_seq_interno = nr_seq_regra_princ_w;

	end if;
end if;

<<final>>
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.GERINT_atend_paciente_unidade
after insert ON TASY.ATEND_PACIENTE_UNIDADE for each row
declare


ie_prim_movimentacao_w			varchar2(1);
nr_seq_leito_w					unidade_atendimento.nr_seq_interno%type;
ie_leito_extra_w				varchar2(1);
ds_sep_bv_w						varchar2(50);
ie_integra_gerint_w				varchar2(1);

begin

/*			I N T E G R A � � O  -  G E R I N T
	Evento 3: Servi�o de Interna��o
	Evento 4: Servi�o de Interna��o em um leito extra
	Evento 6: Servi�o de Transfer�ncia do Leito da Interna��o*/

if	(obter_dados_param_atend(obter_estab_atend(:new.nr_atendimento),'GI') = 'S') and
	(obter_tipo_atendimento(:new.nr_atendimento) = 1) and
	(obter_tipo_convenio_atend(:new.nr_atendimento) = 3) then

	ie_prim_movimentacao_w := GERINT_OBTER_prim_mov(:new.nr_atendimento);

	select	max(nr_seq_interno),
			nvl(max(ie_temporario),'N')
	into	nr_seq_leito_w,
			ie_leito_extra_w
	from 	unidade_atendimento
	where 	cd_unidade_basica		= :new.cd_unidade_basica
	and 	cd_unidade_compl  	    = :new.cd_unidade_compl
	and 	cd_setor_atendimento 	= :new.cd_setor_atendimento;

	ds_sep_bv_w	:=	obter_separador_bv;

	if	(ie_prim_movimentacao_w = 'S') then

		if	(ie_leito_extra_w = 'S') then --Evento 4: Servi�o de Interna��o em um leito extra
			exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  						|| ds_sep_bv_w ||
																		'cd_estabelecimento_p=' 		|| obter_estab_atend(:new.nr_atendimento) 	|| ds_sep_bv_w ||
																		'id_evento_p=' 					|| '4'				 						|| ds_sep_bv_w ||
																		'nr_atendimento_p=' 			|| :new.nr_atendimento 						|| ds_sep_bv_w ||
																		'ie_leito_extra_p=' 			|| ie_leito_extra_w 						|| ds_sep_bv_w ||
																		'dt_alta_p=' 					|| null 									|| ds_sep_bv_w ||
																		'ds_motivo_alta_p=' 			|| null 									|| ds_sep_bv_w ||
																		'ds_justif_transferencia_p='	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_solic_internacao_p=' 	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_leito_p=' 				|| nr_seq_leito_w							|| ds_sep_bv_w ||
																		'ds_ident_leito_p='				|| null										|| ds_sep_bv_w ||
																		'nr_seq_classif_p=' 			|| null										|| ds_sep_bv_w ||
																		'ie_status_unidade_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cpf_paciente_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cartao_sus_p=' 				|| null										|| ds_sep_bv_w ||
																		'cd_cid_p='						|| null										|| ds_sep_bv_w ||
																		'cd_evolucao_p='				|| null);
		else	--Evento 3: Servi�o de Interna��o
			exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  						|| ds_sep_bv_w ||
																		'cd_estabelecimento_p=' 		|| obter_estab_atend(:new.nr_atendimento) 	|| ds_sep_bv_w ||
																		'id_evento_p=' 					|| '3'				 						|| ds_sep_bv_w ||
																		'nr_atendimento_p=' 			|| :new.nr_atendimento 						|| ds_sep_bv_w ||
																		'ie_leito_extra_p=' 			|| ie_leito_extra_w 						|| ds_sep_bv_w ||
																		'dt_alta_p=' 					|| null 									|| ds_sep_bv_w ||
																		'ds_motivo_alta_p=' 			|| null 									|| ds_sep_bv_w ||
																		'ds_justif_transferencia_p='	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_solic_internacao_p=' 	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_leito_p=' 				|| nr_seq_leito_w							|| ds_sep_bv_w ||
																		'ds_ident_leito_p='				|| null										|| ds_sep_bv_w ||
																		'nr_seq_classif_p=' 			|| null										|| ds_sep_bv_w ||
																		'ie_status_unidade_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cpf_paciente_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cartao_sus_p=' 				|| null										|| ds_sep_bv_w ||
																		'cd_cid_p='						|| null										|| ds_sep_bv_w ||
																		'cd_evolucao_p='				|| null);
		end if;

	else --Evento 6: Servi�o de Transfer�ncia do Leito da Interna��o

		select 	decode(count(*),0,'N','S')
		into	ie_integra_gerint_w
		from	gerint_solic_internacao
		where	nr_atendimento = :new.nr_atendimento
		and		ie_situacao not in ('L','N');

		if (ie_integra_gerint_w = 'S') and ((nvl(:new.ie_passagem_setor,'N') = 'N') or (nvl(:new.ie_passagem_setor,'L') = 'L')) then

			if	(nr_seq_leito_w > 0) then

				exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  										|| ds_sep_bv_w ||
																			'cd_estabelecimento_p=' 		|| obter_estab_atend(:new.nr_atendimento)					|| ds_sep_bv_w ||
																			'id_evento_p=' 					|| '6'				 										|| ds_sep_bv_w ||
																			'nr_atendimento_p=' 			|| :new.nr_atendimento 										|| ds_sep_bv_w ||
																			'ie_leito_extra_p=' 			|| ie_leito_extra_w	 										|| ds_sep_bv_w ||
																			'dt_alta_p=' 					|| null 													|| ds_sep_bv_w ||
																			'ds_motivo_alta_p=' 			|| null 													|| ds_sep_bv_w ||
																			'ds_justif_transferencia_p='	|| Obter_desc_motivo_transf_pac(:new.nr_seq_motivo_transf)	|| ds_sep_bv_w ||
																			'nr_seq_solic_internacao_p=' 	|| null 													|| ds_sep_bv_w ||
																			'nr_seq_leito_p=' 				|| nr_seq_leito_w											|| ds_sep_bv_w ||
																			'ds_ident_leito_p='				|| null														|| ds_sep_bv_w ||
																			'nr_seq_classif_p=' 			|| null														|| ds_sep_bv_w ||
																			'ie_status_unidade_p=' 			|| null														|| ds_sep_bv_w ||
																			'nr_cpf_paciente_p=' 			|| null														|| ds_sep_bv_w ||
																			'nr_cartao_sus_p=' 				|| null														|| ds_sep_bv_w ||
																			'cd_cid_p='						|| null														|| ds_sep_bv_w ||
																			'cd_evolucao_p='				|| null);
			end if;
		end if;
	end if;
end if;
/*		I N T E G R A � � O  -  G E R I N T  -  F I M		*/

end;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_Insert
BEFORE INSERT ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
DECLARE
dt_entrada_w			Date;
ie_data_futura_w			Varchar2(1) := 'S';
nr_ramal_w			Varchar2(5);
qt_existe_tel_w			Number(3);
cd_pessoa_fisica_w		varchar2(10);
ie_transfere_prontuario_w		varchar2(1);
qt_agenda_w			number(5,0);
cd_agenda_w			number(10,0) := null;
/* Rafael em 8/9/2007 OS68015 */
nr_prontuario_w			number(10,0);
cd_estabelecimento_w		number(4,0);
cd_estab_atend_w			number(10,0);
ie_tipo_atendimento_w		number(3,0);
ie_clinica_w			number(5,0);
ie_prontuario_w			varchar2(1);
ie_forma_gerar_w		varchar2(1);
ie_consiste_alta_adep_w		varchar2(1);
ie_gerar_pront_w		varchar2(1) := 'N';
IE_GERAR_PASSAGEM_ALTA_w	varchar2(1) := 'S';
dt_alta_w				date;
nr_seq_evento_w			number(10,0);
ie_tipo_convenio_w			number(3,0);
cd_classif_setor_w			varchar2(2);
QT_PAC_UNIDADE_w		number(3,0);
qt_em_unidade_w			number(3,0);
nr_seq_interno_unidade_w		number(10,0);
ie_permite_passagem_sus_w		varchar2(1) := 'S';
qt_idade_w			number(10);
NR_SEQ_LOCAL_PA_w		number(10);
ie_adic_pac_leito_agrup_w	varchar2(1);
ie_isol_agrup_w			varchar2(1);
nr_agrupamento_w		number(5);
ie_permite_menor_dt_entrada_w	varchar2(1);
nr_atendimento_w		number(10);
ie_tipo_convenio_ant_w		number(3);
nr_seq_queixa_ant_w		number(10);
ie_bloqueia_atendimento_w	varchar2(1);
ds_mensagem_w			varchar2(255);
cd_convenio_w			number(10);
cd_categoria_w			varchar(10);
cd_plano_w			varchar(10);
nr_seq_classificacao_w		number(10);
cd_procedencia_w		number(10);
nr_seq_tipo_acidente_w		number(10);
nr_seq_queixa_w			number(10);
cd_empresa_cat_w		number(10);
nr_seq_cobertura_w		number(10);
cd_tipo_acomodacao_w		number(10);
ie_consiste_regra_conv_w	varchar2(1);
ie_integracao_epimed_w		number(5);
ie_integracao_dynamics_w	varchar2(1);
ie_integracao_aghos_w		varchar2(1);
nr_seq_atepacu_w		number(10);
nr_internacao_w			number(10);
ds_sqlerror_w			varchar2(2000);
ds_erro_aghos_w			varchar2(2000);
ie_mostrar_erro_aghos_w		varchar2(1);
ie_bloq_unid_ant_w		varchar2(1);
nr_seq_unidade_new_w	number(10);
nr_seq_regra_princ_w		number(10);
nr_seq_regra_sec_w		number(10);
qt_regra_w			number(10);
ie_regra_excecao_w		varchar2(1);
idf_internacao_w		number(1);
cod_posto_enfermagem_w	varchar2(1);
cod_enfermaria_w		varchar2(1);
cod_leito_w				varchar2(1);
cod_digito_leito_w		varchar2(1);
des_usuario_w			varchar2(1);
qt_existe_w			number(10);
ie_existe_w			varchar2(1) := 'N';
nr_seq_empresa_w		empresa_integracao.nr_sequencia%type;
ds_param_integ_hl7_w		varchar2(4000) := '';
ie_permite_intern_desfecho_w	varchar2(1);
ie_existe_desfecho_w		number(1);
ie_considera_tipo_acomod_w	varchar2(1);
ie_retaguarda_atual_w		Varchar2(1);
ie_status_w			varchar2(1);
ie_permite_data_menor_w			varchar2(1);
ie_altera_data_acomodacao_w	varchar2(1);
ie_aceita_outpatient_w varchar2(02);
ie_encounter_outpatient_w varchar2(02);
unidade_atendimento_row_w   unidade_atendimento%rowtype;
unidade_atendimento2_row_w   unidade_atendimento%rowtype;
ds_leito_w					varchar2(10);

ie_setor_athena_w				VARCHAR2(1) := 'N';
ie_setor_supply_w				VARCHAR2(1) := 'N';

/* Fim Rafael em 8/9/2007 OS68015 */

Cursor C02 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_w
	and	a.ie_evento_disp	= 'ECC'
	and	nvl(:new.IE_PASSAGEM_SETOR,'N') = 'S'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	exists
		(select	1
		from	setor_atendimento x
		where	x.cd_setor_atendimento		= :new.cd_setor_atendimento
		and	x.cd_classif_setor		= 2)
	and	dt_alta_w is null
	and	nvl(a.ie_situacao,'A') = 'A';

Cursor C03 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_w
	and	a.ie_evento_disp	= 'T'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	nvl(:new.IE_PASSAGEM_SETOR,'N') = 'S'
	and	exists
		(select	1
		from	setor_atendimento x
		where	x.cd_setor_atendimento		= :new.cd_setor_atendimento
		and	x.cd_classif_setor		in (3,4))
	and	dt_alta_w is null
	and	nvl(a.ie_situacao,'A') = 'A';

Cursor C04 is
        select  a.nr_seq_evento
        from    regra_envio_sms a
        where   a.cd_estabelecimento    = cd_estabelecimento_w
        and     a.ie_evento_disp        = 'AAH'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
        and     ie_tipo_atendimento_w   = 8
        and     exists  (select 1
                        from    setor_atendimento x
                        where   x.cd_setor_atendimento          = :new.cd_setor_atendimento
                        and     x.cd_classif_setor              = 3)
	and	dt_alta_w is null
	and	nvl(a.ie_situacao,'A') = 'A';

Cursor C05 is
	select	nvl(ie_excecao,'N')
	from	regra_prontuario
	where	cd_estabelecimento					= cd_estabelecimento_w
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)		= ie_tipo_atendimento_w
	and	nvl(ie_clinica,ie_clinica_w)				= ie_clinica_w
	and	nvl(cd_setor_atendimento,:new.cd_setor_atendimento)	= :new.cd_setor_atendimento
	and	ie_tipo_regra						= 2
	order by nvl(cd_setor_atendimento,0), nvl(ie_clinica,0),nvl(ie_tipo_atendimento,0);

cursor locked_records is
	select *
	from 	unidade_atendimento unat
	where 	unat.cd_unidade_basica 	= :new.cd_unidade_basica
	and 	unat.cd_setor_atendimento 	= :new.cd_setor_atendimento
	and 	(((substr(obter_se_sem_acomodacao(unat.cd_tipo_acomodacao),1,1) = 'N') and (ie_considera_tipo_acomod_w = 'S')) or
			((ie_considera_tipo_acomod_w = 'N') and (obter_classif_setor(unat.cd_setor_atendimento) in (3,4,11,12))))
	for update;

cursor locked_records_2 is
	select *
	from 	unidade_atendimento
	where 	nr_seq_interno in(:new.nr_seq_unid_ant, nr_seq_unidade_new_w)
	for update;

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

if	(:new.cd_departamento = 0) then
	:new.cd_departamento := null;
end if;

  select nvl(max(a.ie_aceita_outpatient), 'N')
	into ie_aceita_outpatient_w
	from departamento_medico a
	where a.cd_departamento = :new.cd_departamento;

	select
	 case
	   WHEN ie_tipo_atendimento <> 1 THEN 'S'
	   WHEN ie_tipo_atendimento =  1 THEN 'N'
	    ELSE 'N'
	 end INTO ie_encounter_outpatient_w
	from atendimento_paciente
	where nr_atendimento = :new.nr_atendimento;

	if	(nvl(:new.cd_departamento ,0) <> 0) then
		if(ie_aceita_outpatient_w = 'N' AND ie_encounter_outpatient_w = 'S') then
       wheb_mensagem_pck.exibir_mensagem_abort(997502);
    end if;
	end if;

:new.cd_procedencia_atend := obter_procedencia_atend(:new.nr_atendimento, 'C');

Obter_Param_Usuario(916, 662, Obter_perfil_Ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_data_menor_w);

select	nvl(max(obter_valor_param_usuario(44,167, Obter_perfil_ativo, :new.nm_usuario, 0)),'S')
into	ie_adic_pac_leito_agrup_w
from	dual;

ie_consiste_alta_adep_w	:= 'S';
if	(obter_funcao_ativa = 1113) then
	select	nvl(max(obter_valor_param_usuario(1113,490, Obter_perfil_ativo, :new.nm_usuario, 0)),'S')
	into	ie_consiste_alta_adep_w
	from	dual;
elsif	(obter_funcao_ativa = 924) then
	select	nvl(max(obter_valor_param_usuario(924,994, Obter_perfil_ativo, :new.nm_usuario, 0)),'S')
	into	ie_consiste_alta_adep_w
	from	dual;
end if;

if	(:new.dt_saida_unidade is not null) and
	(:new.dt_entrada_unidade is not null) and
	(:new.dt_entrada_unidade > :new.dt_saida_unidade) then
	--A data de saida da unidade nao pode ser menor que a data de entrada.
	--Data saida: '||to_char(:new.dt_saida_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data entrada: '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss') || '');
	wheb_mensagem_pck.exibir_mensagem_abort(179690, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
							'DT_ENTRADA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
end if;

select	dt_entrada,
	cd_pessoa_fisica,
	cd_estabelecimento,
	dt_alta,
	ie_tipo_convenio
into	dt_entrada_w,
	cd_pessoa_fisica_w,
	cd_estab_atend_w,
	dt_alta_w,
	ie_tipo_convenio_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
select	nvl(max(IE_GERAR_PASSAGEM_ALTA), 'S'),
		nvl(max(ie_permite_passagem_sus),'S'),
		nvl(max(ie_considera_tipo_acomod),'N')
into	ie_gerar_passagem_alta_w,
		ie_permite_passagem_sus_w,
		ie_considera_tipo_acomod_w
from	parametro_atendimento
where	cd_estabelecimento		= cd_estab_atend_w;

if	(ie_gerar_passagem_alta_w = 'N') and
	(dt_alta_w is not null) and
	(ie_consiste_alta_adep_w = 'S') and
	(:new.dt_entrada_unidade > dt_alta_w) then
	--'A data de entrada da unidade nao pode ser maior que a data da alta. Verifique os parametros de atendimento. ');
	wheb_mensagem_pck.exibir_mensagem_abort(179694);
end if;

if	(dt_entrada_w < sysdate and nvl(pkg_i18n.get_user_locale,'pt_BR') not in ('de_DE', 'de_AT')) then
	if	(:new.dt_saida_unidade is not null) and
		(:new.dt_saida_unidade > (sysdate + 30/86400)) then
		--'A data de saida da unidade nao pode ser maior que a data atual.
		--Data saida: '||to_char(:new.dt_saida_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data atual: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || '');
		wheb_mensagem_pck.exibir_mensagem_abort(179695, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
								'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
	end if;

	if	(:new.dt_entrada_unidade is not null) and
		(:new.dt_entrada_unidade > (sysdate + 30/86400)) then
		--'A data de entrada na unidade nao pode ser maior que a data atual.
	--	Data entrada: '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data atual: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')|| '');
		wheb_mensagem_pck.exibir_mensagem_abort(379552, 'DT_ENTRADA_UNIDADE='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
								'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(sysdate, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
	end if;
	if	(ie_tipo_convenio_w <> 3) or (ie_permite_passagem_sus_w = 'N') then -- Edgar 10/01/2005 OS 14154, Nao fazer esta consistencias qdo for SUS por causa da internacao BPA
		if	(dt_entrada_w > :new.dt_entrada_unidade) and
			(ie_permite_data_menor_w = 'N') then
			--'A data de entrada na unidade nao pode ser menor que a data de inicio do atendimento.
			--Data entrada: '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data inicio atendimento: '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')|| '');
			wheb_mensagem_pck.exibir_mensagem_abort(179699, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
								'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone) );
		elsif	(dt_entrada_w > :new.dt_saida_unidade) and
			(ie_permite_data_menor_w = 'N') then
			--'A data de saida da unidade nao pode ser menor que a data de inicio do atendimento.
			--Data saida: '||to_char(:new.dt_saida_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data inicio atendimento: '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')|| '');
			wheb_mensagem_pck.exibir_mensagem_abort(179703, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_saida_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
								'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone) );
		end if;
	end if;
end if;

if	(:new.dt_entrada_unidade	< dt_entrada_w) and
	(dt_entrada_w < sysdate) and
	(ie_permite_data_menor_w = 'N') then
	--'A data de entrada na unidade nao pode ser menor que a data de inicio do atendimento.
	--Data entrada: '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data inicio atendimento: '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')|| '');
	wheb_mensagem_pck.exibir_mensagem_abort(179704, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
							'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));

end if;

select	nvl(max(obter_valor_param_usuario(916,662, Obter_perfil_ativo, :new.nm_usuario, 0)),'S')
into	ie_permite_menor_dt_entrada_w
from	dual;

if	(ie_permite_menor_dt_entrada_w = 'N') then

	if	(:new.dt_entrada_unidade	< dt_entrada_w) then
		--'A data de entrada na unidade nao pode ser menor que a data de inicio do atendimento.
				--Data entrada: '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')||' Data inicio atendimento: '||to_char(dt_entrada_w,'dd/mm/yyyy hh24:mi:ss')|| '');
		wheb_mensagem_pck.exibir_mensagem_abort(179706, 'DT_SAIDA='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||';'||
							'DT_ATUAL='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_w, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone));
	end if;
end if;

select 	max(cd_classif_setor)
into	cd_classif_setor_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(ie_permite_intern_desfecho_w = 'N') and
	(obter_tipo_atendimento(:new.nr_atendimento) = 3) and
	(cd_classif_setor_w in (3,4)) then

	begin
		select	1
		into	ie_existe_desfecho_w
		from	atendimento_alta z
		where	z.nr_atendimento = :new.nr_atendimento
		and	z.ie_desfecho = 'I'
		and	rownum = 1;
	exception
		when	no_data_found then
			ie_existe_desfecho_w := 0;
	end;

	if	(ie_existe_desfecho_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(276597);
	end if;
end if;

if	(:new.dt_saida_unidade is null) then

	if	(cd_classif_setor_w in ('1','5')) then

		select	max(nr_seq_interno)
		into	nr_seq_interno_unidade_w
		from 	unidade_atendimento a
		WHERE	A.cd_unidade_basica 	= :new.cd_unidade_basica
		AND	A.cd_unidade_compl  	= :new.cd_unidade_compl
		AND	A.cd_setor_atendimento 	= :new.cd_setor_atendimento;

		select 	nvl(max(QT_PAC_UNIDADE),0)
		into	QT_PAC_UNIDADE_w
		from	unidade_atendimento
		where	NR_SEQ_INTERNO = nr_seq_interno_unidade_w;

		if	(QT_PAC_UNIDADE_w > 0) then

			select 	count(*)
			into	qt_em_unidade_w
			from	controle_atend_unidade
			where	NR_SEQ_INTERNO = nr_seq_interno_unidade_w
			and	dt_saida is null;

			if	(qt_em_unidade_w >= QT_PAC_UNIDADE_w) then

				--'Nao e possivel gerar a passagem, pois ja existem pacientes ocupando este leito ');
				wheb_mensagem_pck.exibir_mensagem_abort(179707);

			end if;

		end if;

	end if;

end if;

if	(:new.dt_atualizacao_nrec is null) then
	:new.dt_atualizacao_nrec := sysdate;
end if;

if	(:new.dt_saida_unidade is null) then
	:new.dt_saida_interno  := to_date('30/12/2999','dd/mm/yyyy');
else
	:new.dt_saida_interno  := :new.dt_saida_unidade;
end if;

if	((nvl(:new.ie_passagem_setor,'N') = 'N') or
	(nvl(:new.ie_passagem_setor,'L') = 'L')) and
	(:new.dt_saida_unidade is null) and
	(nvl(ie_adic_pac_leito_agrup_w,'S') = 'N') then

	select	nvl(max(nr_agrupamento),0)
	into	nr_agrupamento_w
	from	unidade_atendimento
	where	cd_setor_atendimento = :new.cd_setor_atendimento
	and	cd_unidade_basica    = :new.cd_unidade_basica
	and	cd_unidade_compl     = :new.cd_unidade_compl;

	if	(nr_agrupamento_w > 0) then

		select	verifica_se_agrup_isol(:new.cd_setor_atendimento,nr_agrupamento_w)
		into	ie_isol_agrup_w
		from	dual;

		if	(ie_isol_agrup_w = 'S') then

			--'Nao e possivel adicionar paciente na unidade, pois no mesmo agrupamento ha leito/paciente isolado. Parametro[167] ');
			wheb_mensagem_pck.exibir_mensagem_abort(179708);

		end if;
	end if;
end if;

if	((nvl(:new.ie_passagem_setor,'N') = 'N') or
	(nvl(:new.ie_passagem_setor,'L') = 'L')) and
	(:new.dt_saida_unidade is null) then
	begin

	select  max(ie_bloq_unid_ant),
			max(nr_seq_interno),
			max(ie_retaguarda_atual)
	into	ie_bloq_unid_ant_w,
			nr_seq_unidade_new_w,
			ie_retaguarda_atual_w
	from 	unidade_atendimento
	where 	cd_unidade_basica 		= :new.cd_unidade_basica
	and 	cd_unidade_compl  		= :new.cd_unidade_compl
	and 	cd_setor_atendimento 	= :new.cd_setor_atendimento;

	open locked_records;
    loop
        fetch locked_records into unidade_atendimento_row_w;
        exit when locked_records%NOTFOUND;
    end loop;

	close locked_records;

	open locked_records_2;
    loop
        fetch locked_records_2 into unidade_atendimento2_row_w;
        exit when locked_records_2%NOTFOUND;
    end loop;

	close locked_records_2;

	-- O leito devera ser ocupado (ou nao) de acordo com a acomodacao de cada leito
	UPDATE UNIDADE_ATENDIMENTO A
	SET A.NR_ATENDIMENTO      	= :new.nr_atendimento,
		A.DT_ENTRADA_UNIDADE  	= :new.dt_entrada_unidade,
		A.IE_STATUS_UNIDADE   	= 'P',
		A.CD_PACIENTE_RESERVA 	= null,
		A.NM_PAC_RESERVA		= null,
		A.CD_CONVENIO_RESERVA	= null,
		A.NM_USUARIO		= :new.nm_usuario,
			A.DT_ATUALIZACAO		= sysdate
	WHERE	A.cd_unidade_basica 	= :new.cd_unidade_basica
	AND 	A.cd_unidade_compl  	= :new.cd_unidade_compl
	AND		A.cd_setor_atendimento 	= :new.cd_setor_atendimento
	AND		(((substr(obter_se_sem_acomodacao(a.cd_tipo_acomodacao),1,1) = 'N') and (ie_considera_tipo_acomod_w = 'S')) or
			((ie_considera_tipo_acomod_w = 'N') and (obter_classif_setor(a.cd_setor_atendimento) in (3,4,11,12))));

	Atualizar_Unidade_atendimento(:new.cd_setor_atendimento,
					:new.cd_unidade_basica,
					:new.cd_unidade_compl);

	if	(nvl(ie_bloq_unid_ant_w,'N') = 'S') then

		update 	unidade_atendimento
		set 	nr_seq_unid_bloq  = nr_seq_unidade_new_w
		where 	nr_seq_interno 	  = :new.nr_seq_unid_ant;
	end if;

	if (ie_retaguarda_atual_w = 'S') then
		update 	unidade_atendimento
		set 	ie_retaguarda_atual  = 'N'
		where 	nr_seq_interno 	  = nr_seq_unidade_new_w;
	end if;

	end;
end if;


if	((nvl(:new.ie_passagem_setor,'N') = 'N') or
	(nvl(:new.ie_passagem_setor,'L') = 'L')) and
	(:new.dt_saida_unidade is null) then
	begin
	select	max(NR_SEQ_LOCAL_PA)
	into	NR_SEQ_LOCAL_PA_w
	from 	unidade_atendimento a
	WHERE A.cd_unidade_basica 	= :new.cd_unidade_basica
	AND A.cd_unidade_compl  	= :new.cd_unidade_compl
	AND A.cd_setor_atendimento 	= :new.cd_setor_atendimento;

	if	(NR_SEQ_LOCAL_PA_w	is not null) then
		update	atendimento_paciente
		set	NR_SEQ_LOCAL_PA	= NR_SEQ_LOCAL_PA_w
		where	nr_atendimento	= :new.nr_atendimento;
	end if;
	exception
		when others then
		null;
	end;

end if;


gerar_lib_bloqueio_telefone(obter_estab_atend(:new.nr_atendimento),
				:new.nr_atendimento,
				:new.nm_usuario,
				:new.cd_setor_atendimento,
				:new.cd_unidade_basica,
				:new.cd_unidade_compl,
				10);

Obter_param_Usuario(1002, 87, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_status_w);
obter_param_usuario(1002, 138, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_altera_data_acomodacao_w);

if (ie_status_w = 'S') then
	update	gestao_vaga
	set	ie_status		= 'F',
		dt_acomodacao		= decode(ie_altera_data_acomodacao_w,'S',sysdate, dt_acomodacao),
		nr_atendimento		= :new.nr_atendimento,
		cd_setor_atual		= :new.cd_setor_atendimento,
		cd_tipo_acomod_atual	= :new.cd_tipo_acomodacao
	where	cd_setor_desejado	= :new.cd_setor_atendimento
	and	cd_unidade_basica	= :new.cd_unidade_basica
	and	cd_unidade_compl	= :new.cd_unidade_compl
	and	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	((ie_status		= 'R')
	or	(ie_status		= 'A'));
else
	update	gestao_vaga
	set	ie_status		= 'F',
		dt_acomodacao		= decode(ie_altera_data_acomodacao_w,'S',sysdate, dt_acomodacao),
		nr_atendimento		= :new.nr_atendimento,
		cd_tipo_acomod_atual	= :new.cd_tipo_acomodacao
	where	cd_setor_desejado	= :new.cd_setor_atendimento
	and	cd_unidade_basica	= :new.cd_unidade_basica
	and	cd_unidade_compl	= :new.cd_unidade_compl
	and	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	((ie_status		= 'R')
	or	(ie_status		= 'A'));
end if;

select	Obter_Valor_Param_Usuario(1007,12, obter_perfil_ativo, :new.nm_usuario,0)
into	ie_transfere_prontuario_w
from	dual;

if	(ie_transfere_prontuario_w = 'S') then

	select	count(*)
	into	qt_agenda_w
	from	agenda
	where	ie_situacao		= 'A'
	and	cd_tipo_agenda		= 5
	and	cd_setor_exclusivo 	= :new.cd_setor_atendimento;

	if	(qt_agenda_w = 1) then
		select	cd_agenda
		into	cd_agenda_w
		from	agenda
		where	ie_situacao		= 'A'
		and	cd_tipo_agenda		= 5
		and	cd_setor_exclusivo 	= :new.cd_setor_atendimento;
	end if;

	solicitar_prontuario_setor(:new.nr_atendimento,:new.cd_setor_atendimento,cd_agenda_w,sysdate,:new.nm_usuario);
end if;

/* Rafael em 8/9/2007 OS68015 */
-- obter dados pf
/* Matheus OS 182242 substituido pela function obter_prontuario_pf
select	nvl(max(nr_prontuario),0)
into	nr_prontuario_w
from	pessoa_fisica
where	cd_pessoa_fisica = obter_pessoa_atendimento(:new.nr_atendimento,'C');*/

nr_prontuario_w	:= nvl(obter_prontuario_pf(cd_estabelecimento_w, obter_pessoa_atendimento(:new.nr_atendimento,'C')),0);

-- obter dados atend
select	max(cd_estabelecimento),
	nvl(max(ie_tipo_atendimento),0),
	nvl(max(ie_clinica),0)
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

-- obter parametros
select	obter_valor_param_usuario(0,32,0,:new.nm_usuario,cd_estabelecimento_w)
into	ie_prontuario_w
from	dual;

select	obter_valor_param_usuario(5,24,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w)
into	ie_forma_gerar_w
from	dual;

Obter_param_Usuario(3111, 248, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_mostrar_erro_aghos_w);

if	(nr_prontuario_w = 0) and
	(ie_prontuario_w = 'R') then


	select	decode(count(*),0,'N','S')
	into	ie_gerar_pront_w
	from	regra_prontuario
	where	cd_estabelecimento					= cd_estabelecimento_w
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)		= ie_tipo_atendimento_w
	and	nvl(ie_clinica,ie_clinica_w)				= ie_clinica_w
	and	nvl(cd_setor_atendimento,:new.cd_setor_atendimento)	= :new.cd_setor_atendimento
	and	ie_tipo_regra							= 2;

	if	(ie_gerar_pront_w = 'S') then
		ie_regra_excecao_w	:= 'N';
		open C05;
		loop
		fetch C05 into
			ie_regra_excecao_w;
		exit when C05%notfound;
			begin
			ie_regra_excecao_w := ie_regra_excecao_w;
			end;
		end loop;
		close C05;

		if	(ie_regra_excecao_w	= 'N') then
			if	(ie_forma_gerar_w <> 'N') then
				gerar_prontuario_anual(ie_forma_gerar_w,:new.nm_usuario,nr_prontuario_w);

				update	pessoa_fisica
				set	nr_prontuario = nr_prontuario_w
				where	cd_pessoa_fisica = obter_pessoa_atendimento(:new.nr_atendimento,'C');
			else
				/*Matheus OS 182242*/
				gerar_prontuario_pac(cd_estabelecimento_w, obter_pessoa_atendimento(:new.nr_atendimento,'C'), 'N', :new.nm_usuario, nr_prontuario_w);
			end if;

		end if;
	end if;
end if;



/* Fim Rafael em 8/9/2007 OS68015 */


cons_regra_func_setor_atepacu(:new.cd_setor_atendimento,:new.nr_atendimento); /* Rafael em 12/5/8 OS92684 */
cd_convenio_w	:= obter_convenio_atendimento(:new.nr_atendimento);

open C02;
loop
fetch C02 into
	nr_seq_evento_w;
exit when C02%notfound;
	begin
	gerar_evento_pac_trigger_setor(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario, :new.cd_unidade_basica, :new.cd_unidade_compl, :new.cd_setor_atendimento);
	end;
end loop;
close C02;

/*open C03;
loop
fetch C03 into
	nr_seq_evento_w;
exit when C03%notfound;
	begin
	gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario);
	end;
end loop;
close C03; */

open C04;
loop
fetch C04 into
        nr_seq_evento_w;
exit when C04%notfound;
        begin
        gerar_evento_pac_trigger_setor(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario, :new.cd_unidade_basica, :new.cd_unidade_compl, :new.cd_setor_atendimento);

        end;
end loop;
close C04;

begin
enviar_comunic_tipo_acomod(:new.nr_atendimento,:new.cd_tipo_acomodacao,:new.cd_setor_atendimento,cd_estabelecimento_w,:new.nm_usuario);
end;

if (nvl(:new.ie_passagem_setor,'N') = 'S') then
	gerar_regra_frasco_pato(:new.nr_atendimento,null,ie_tipo_atendimento_w,null,null,ie_clinica_w,:new.cd_setor_atendimento,'P',:new.nm_usuario);

	if (obter_se_primeira_pass_setor(:new.nr_atendimento) = 'S') then
		gerar_regra_frasco_pato(:new.nr_atendimento,null,ie_tipo_atendimento_w,null,null,ie_clinica_w,:new.cd_setor_atendimento,'PR',:new.nm_usuario);
	end if;

end if;

gerar_regra_prontuario_gestao(ie_tipo_atendimento_w, cd_estab_atend_w, :new.nr_atendimento, cd_pessoa_fisica_w, :new.nm_usuario, null, null, null, ie_clinica_w, :new.cd_setor_atendimento, null, null);

Obter_Param_Usuario(916, 788, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_consiste_regra_conv_w);
if	(ie_consiste_regra_conv_w in ('S', 'T')) then

	select	max(b.nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_paciente b
	where	nr_atendimento < :new.nr_atendimento;

	select	a.ie_tipo_convenio
	into	ie_tipo_convenio_ant_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_w;

	select	a.nr_seq_queixa
	into	nr_seq_queixa_ant_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_w;

	qt_idade_w	:= substr(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'), 1, 3);

	select	max(cd_categoria),
		max(cd_plano_convenio),
		max(cd_empresa),
		max(nr_seq_cobertura),
		max(cd_tipo_acomodacao)
	into	cd_categoria_w,
		cd_plano_w,
		cd_empresa_cat_w,
		nr_seq_cobertura_w,
		cd_tipo_acomodacao_w
	from	atend_categoria_convenio
	where	nr_seq_interno = obter_atecaco_atendimento(:new.nr_atendimento);

	select	max(nr_seq_classificacao),
		max(ie_clinica),
		max(cd_procedencia),
		max(nr_seq_tipo_acidente),
		max(nr_seq_queixa)
	into	nr_seq_classificacao_w,
		ie_clinica_w,
		cd_procedencia_w,
		nr_seq_tipo_acidente_w,
		nr_seq_queixa_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	Obter_Se_Lib_Setor_Conv(
			cd_estabelecimento_w,
			cd_convenio_w,
			cd_categoria_w,
			ie_tipo_atendimento_w,
			:new.cd_setor_atendimento,
			cd_plano_w,
			nr_seq_classificacao_w,
			ds_mensagem_w,
			ie_bloqueia_atendimento_w,
			ie_clinica_w,
			cd_empresa_cat_w,
			cd_procedencia_w,
			nr_seq_cobertura_w,
			nr_seq_tipo_acidente_w,
			cd_tipo_acomodacao_w,
			Obter_medico_resp_atend(:new.nr_atendimento, 'C'),
			qt_idade_w,
			ie_tipo_convenio_ant_w,
			nr_seq_queixa_w,
			nr_seq_queixa_ant_w,
			sysdate,
			cd_pessoa_fisica_w);

	if	(ie_bloqueia_atendimento_w = 'B') then
		if	(ds_mensagem_w is null) then
			wheb_mensagem_pck.Exibir_Mensagem_Abort(150769);
		else
			wheb_mensagem_pck.Exibir_Mensagem_Abort(186140, 'MENSAGEM='||ds_mensagem_w);
		end if;
	end if;
end if;

select	nvl(max(ie_integracao_epimed),0),
	nvl(max(ie_integracao_dynamics),'N')
into	ie_integracao_epimed_w,
	ie_integracao_dynamics_w
from	parametro_atendimento
where	cd_estabelecimento =	cd_estabelecimento_w;

if	(ie_integracao_epimed_w = 1) then
	hsl_gerar_epimed_internacao(:new.cd_setor_atendimento,:new.dt_entrada_unidade,:new.nr_seq_interno,:new.nr_atendimento,:new.cd_unidade_basica,
				    :new.cd_unidade_compl,:new.nm_usuario,:new.dt_saida_unidade,cd_classif_setor_w,'T');
end if;

if	(ie_integracao_dynamics_w = 'S') then
	Gerar_crm_atendimentos(:new.nr_atendimento,:new.cd_setor_atendimento,:new.nm_usuario);
end if;

--Realizar alteracoes de movimentacao do paciente para o Matrix
if (obter_se_integracao_ativa(887,53) = 'S') then
   integrar_matrix_ws_v7(887,nr_atendimento_p => :new.nr_atendimento);
end if;

-- Atualizar o leito do paciente no Aghos ao interna-lo
ie_integracao_aghos_w := obter_dados_param_atend(cd_estabelecimento_w, 'AG');

If	(ie_integracao_aghos_w <> 'N') and false then

	select	max(nr_internacao)
	into	nr_internacao_w
	from	solicitacao_tasy_aghos
	where	nr_atendimento = :new.nr_atendimento;

	If	(nr_internacao_w is not null) then

		select	max(a.nr_seq_interno)
		into	nr_seq_atepacu_w
		from 	unidade_atendimento a
		where 	a.cd_unidade_basica	= :new.cd_unidade_basica
		and 	a.cd_unidade_compl  	= :new.cd_unidade_compl
		and 	a.cd_setor_atendimento 	= :new.cd_setor_atendimento
		and	a.nm_setor_integracao is not null;


		If	(nr_seq_atepacu_w > 0) and
			(nvl(:new.ie_passagem_setor, 'N') <> 'S') then

			If	(ie_integracao_aghos_w = 'S') then
				begin
					Aghos_transferencia_interna(:new.nr_atendimento, nr_seq_atepacu_w, :new.nm_usuario);
				exception
					when	others then

						If	(ie_mostrar_erro_aghos_w = 'S') then

							ds_sqlerror_w := sqlerrm;
							ds_erro_aghos_w := substr(Ajusta_mensagem_erro_aghos(ds_sqlerror_w), 1, 2000);

							wheb_mensagem_pck.exibir_mensagem_abort(228493, 'DS_MENSAGEM=' || nvl(ds_erro_aghos_w, ds_sqlerror_w));
						End if;

						insert into aghos_contigencia_mov (nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_erro,
							nr_atendimento,
							nr_internacao,
							nr_seq_atepacu,
							cd_setor_origem,
							dt_resolvida,
							nm_usuario_resol)
						values	(aghos_contigencia_mov_seq.nextval,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							ds_erro_aghos_w,
							:new.nr_atendimento,
							nr_internacao_w,
							:new.nr_seq_interno,
							:old.cd_setor_atendimento,
							null,
							null);
				end;
			elsif (nvl(obter_funcao_ativa, 916) not in (3111)) then
				Aghos_transferencia_interna_ws(:new.nr_atendimento, nr_seq_atepacu_w, :new.nm_usuario, 'N', idf_internacao_w, cod_posto_enfermagem_w, cod_enfermaria_w, cod_leito_w, cod_digito_leito_w, des_usuario_w);
			End if;
		End if;

	End if;
End if;

select 	count(*)
into	qt_regra_w
from	regra_leitos_inativacao
where	ie_situacao = 'A';

if (nvl(qt_regra_w,0) > 0) then

	select	max(nr_seq_unid_princ)
	into	nr_seq_regra_princ_w
	from	regra_leitos_inativacao
	where	ie_situacao = 'A'
	and	nr_seq_unid_sec = (	select	max(nr_seq_interno)
					from	unidade_atendimento
					where 	cd_unidade_basica	= :new.cd_unidade_basica
					and 	cd_unidade_compl  	= :new.cd_unidade_compl
					and 	cd_setor_atendimento 	= :new.cd_setor_atendimento);

	if (nvl(nr_seq_regra_princ_w,0) = 0) then
		select	max(nr_seq_unid_sec)
		into	nr_seq_regra_princ_w
		from	regra_leitos_inativacao
		where	ie_situacao = 'A'
		and	nr_seq_unid_princ = (	select	max(nr_seq_interno)
						from	unidade_atendimento
						where 	cd_unidade_basica	= :new.cd_unidade_basica
						and 	cd_unidade_compl  	= :new.cd_unidade_compl
						and 	cd_setor_atendimento 	= :new.cd_setor_atendimento);
	end if;

	if (nvl(nr_seq_regra_princ_w, 0) > 0) then

		update	unidade_atendimento
		set	ie_situacao = 'I'
		where	nr_seq_interno = nr_seq_regra_princ_w
		AND	ie_status_unidade = 'L';

	end if;
end if;

begin
select	1
into	qt_existe_w
from	dis_regra_setor
where	cd_setor_atendimento = :new.cd_setor_atendimento
and	rownum = 1;
exception
when others then
	qt_existe_w := 0;
end;

if	(qt_existe_w > 0) then
	intdisp_gerar_movto_eup(:new.nr_atendimento, :new.cd_setor_atendimento, :new.cd_unidade_basica, :new.dt_entrada_unidade, 'EPA');
end if;

-- Rotina para movimentacao de paciente na integracao dispensarios
swisslog_movimentacao_pac(:new.nr_atendimento, :new.cd_setor_atendimento, 0, 3, :new.nm_usuario);
ds_leito_w := substr(:new.cd_unidade_basica || ' - ' || :new.cd_unidade_compl,1,10);
gerar_int_dankia_pck.dankia_internar_paciente(:new.nr_atendimento, :new.cd_setor_atendimento, ds_leito_w, cd_estabelecimento_w, :new.nm_usuario);


SELECT DECODE(COUNT(*), 0, 'N', 'S')
INTO ie_setor_supply_w
FROM far_setores_integracao
WHERE nr_seq_empresa_int = 82
AND cd_setor_atendimento = :new.cd_setor_atendimento
AND ROWNUM = 1;

if	(ie_setor_supply_w = 'S') then
	supplypoint_mov_pac(:new.nr_atendimento, :new.cd_setor_atendimento, 0, 3, :new.nm_usuario);
end if;

SELECT DECODE(COUNT(*), 0, 'N', 'S')
INTO ie_setor_athena_w
FROM far_setores_integracao
WHERE nr_seq_empresa_int = 221
AND cd_setor_atendimento = :new.cd_setor_atendimento
AND ROWNUM = 1;

if	(ie_setor_athena_w = 'S') then
	integracao_athena_disp_pck.movimentacao_paciente(:new.nr_atendimento, :new.cd_setor_atendimento, 0, 3, :new.nm_usuario);
end if;

<<final>>
null;

end;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_Ins_BB
    AFTER INSERT ON TASY.ATEND_PACIENTE_UNIDADE     FOR EACH ROW
DECLARE

    bb_count_icu                NUMBER;
    bb_count_stepdown           NUMBER;
    bb_count_acute              NUMBER;
    bb_classif_setor            NUMBER;
    bb_nome_setor_anterior      VARCHAR2(200);
    bb_nome_setor               VARCHAR2(200);
    bb_ie_readmitido            VARCHAR2(1);
    bb_primeira_movimentacao    VARCHAR2(1);
    bb_cd_pessoa_fisica         PESSOA_FISICA.cd_pessoa_fisica%TYPE;
    bb_dt_nascimento            PESSOA_FISICA.dt_nascimento%TYPE;
    bb_ie_sexo                  PESSOA_FISICA.ie_sexo%TYPE;
    bb_qt_peso                  PESSOA_FISICA.qt_peso%TYPE;
    bb_qt_altura_cm             PESSOA_FISICA.qt_altura_cm%TYPE;
    bb_cd_procedencia           ATENDIMENTO_PACIENTE.cd_procedencia%TYPE;
    bb_dt_entrada               ATENDIMENTO_PACIENTE.dt_entrada%TYPE;
    bb_nr_seq_classif           UNIDADE_ATENDIMENTO.nr_seq_classif%TYPE;
    bb_nr_seq_interno_anterior  UNIDADE_ATENDIMENTO.nr_seq_interno%type;

BEGIN

  p_buscar_info_admissao_bb(
    :new.nr_atendimento,
    :new.cd_setor_atendimento,
    bb_count_icu,
    bb_count_stepdown,
    bb_count_acute,
    bb_nome_setor_anterior,
    bb_nome_setor,
    bb_ie_readmitido,
    bb_nr_seq_interno_anterior
  );

  SELECT
    pf.cd_pessoa_fisica,
    pf.dt_nascimento,
    pf.ie_sexo,
    pf.qt_peso,
    pf.qt_altura_cm,
    ap.cd_procedencia,
    ap.dt_entrada,
    ua.nr_seq_classif
  INTO
    bb_cd_pessoa_fisica,
    bb_dt_nascimento,
    bb_ie_sexo,
    bb_qt_peso,
    bb_qt_altura_cm,
    bb_cd_procedencia,
    bb_dt_entrada,
    bb_nr_seq_classif
  FROM
    ATENDIMENTO_PACIENTE ap,
    PESSOA_FISICA pf,
    UNIDADE_ATENDIMENTO ua
  WHERE
    ap.NR_ATENDIMENTO = :NEW.NR_ATENDIMENTO
    AND ua.CD_SETOR_ATENDIMENTO = :NEW.CD_SETOR_ATENDIMENTO
    AND ua.CD_UNIDADE_BASICA = :NEW.CD_UNIDADE_BASICA
    AND ua.CD_UNIDADE_COMPL = :NEW.CD_UNIDADE_COMPL
    AND pf.CD_PESSOA_FISICA = ap.CD_PESSOA_FISICA;

  IF (bb_nome_setor = 'ICU' OR bb_nome_setor = 'Stepdown' OR bb_nome_setor = 'Acute') THEN
    IF (bb_count_icu = 0 AND bb_count_stepdown = 0 AND bb_count_acute = 0) THEN
      bb_primeira_movimentacao := 'S';
    ELSE
      bb_primeira_movimentacao := 'N';
    END IF;

    IF (bb_nome_setor_anterior = bb_nome_setor) THEN
      p_integrar_admissao_bb
        (bb_integracao              => 'T',
        nr_atendimento_p            => :new.nr_atendimento,
        nr_seq_interno_p            => :new.nr_seq_interno,
        nm_setor_bb_p               => bb_nome_setor,
        nm_setor_anterior_bb_p      => bb_nome_setor_anterior,
        cd_setor_atendimento_p      => :new.cd_setor_atendimento,
        nr_seq_classif_p            => bb_nr_seq_classif,
        cd_unidade_basica_p         => :new.cd_unidade_basica,
        cd_unidade_compl_p          => :new.cd_unidade_compl,
        dt_entrada_unidade_p        => :new.dt_entrada_unidade,
        nr_seq_interno_anterior_p   => bb_nr_seq_interno_anterior);
    ELSE

      IF ((bb_count_icu = 0 AND bb_nome_setor = 'ICU') OR
          (bb_count_stepdown = 0 AND bb_nome_setor = 'Stepdown') OR
          (bb_count_acute = 0 AND bb_nome_setor = 'Acute')) THEN
        p_integrar_admissao_bb
          (bb_integracao              => 'A',
          nr_atendimento_p            => :new.nr_atendimento,
          nr_seq_interno_p            => :new.nr_seq_interno,
          cd_pessoa_fisica_p          => bb_cd_pessoa_fisica,
          nm_setor_bb_p               => bb_nome_setor,
          nm_setor_anterior_bb_p      => bb_nome_setor_anterior,
          cd_setor_atendimento_p      => :new.cd_setor_atendimento,
          nr_seq_classif_p            => bb_nr_seq_classif,
          cd_unidade_basica_p         => :new.cd_unidade_basica,
          cd_unidade_compl_p          => :new.cd_unidade_compl,
          cd_procedencia_p            => bb_cd_procedencia,
          dt_entrada_p                => bb_dt_entrada,
          dt_entrada_unidade_p        => :new.dt_entrada_unidade,
          dt_nascimento_p             => bb_dt_nascimento,
          ie_sexo_p                   => bb_ie_sexo,
          qt_peso_p                   => bb_qt_peso,
          qt_altura_cm_p              => bb_qt_altura_cm,
          ie_primeira_movimentacao    => bb_primeira_movimentacao);
      END IF;

      IF ((bb_count_icu > 0 AND bb_nome_setor = 'ICU') OR
          (bb_count_stepdown > 0 AND bb_nome_setor = 'Stepdown') OR
          (bb_count_acute > 0 AND bb_nome_setor = 'Acute')) THEN
        p_integrar_admissao_bb
          (bb_integracao              => 'R',
          nr_atendimento_p            => :new.nr_atendimento,
          nr_seq_interno_p            => :new.nr_seq_interno,
          nm_setor_bb_p               => bb_nome_setor,
          nm_setor_anterior_bb_p      => bb_nome_setor_anterior,
          cd_setor_atendimento_p      => :new.cd_setor_atendimento,
          nr_seq_classif_p            => bb_nr_seq_classif,
          cd_unidade_basica_p         => :new.cd_unidade_basica,
          cd_unidade_compl_p          => :new.cd_unidade_compl,
          cd_procedencia_p            => bb_cd_procedencia,
          dt_entrada_p                => bb_dt_entrada,
          dt_entrada_unidade_p        => :new.dt_entrada_unidade,
          dt_nascimento_p             => bb_dt_nascimento,
          ie_sexo_p                   => bb_ie_sexo,
          qt_peso_p                   => bb_qt_peso,
          qt_altura_cm_p              => bb_qt_altura_cm,
          ie_readmitido               => bb_ie_readmitido);
      END IF;
    END IF;

  ELSIF (bb_nome_setor_anterior IS NOT NULL AND bb_nome_setor IS NULL) THEN
    p_integrar_admissao_bb
      (bb_integracao              => 'D',
      nr_atendimento_p            => :new.nr_atendimento,
      dt_entrada_unidade_p        => :new.dt_entrada_unidade,
      nr_seq_interno_p            => bb_nr_seq_interno_anterior);
  END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unidade_atual
before insert or update ON TASY.ATEND_PACIENTE_UNIDADE for each row
declare
ds_origem_w			varchar2(1800);
ds_unidade_log_w			varchar2(4000);
ds_insert_w 	varchar2(60);
reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(1);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;


if (inserting) then
	select substr(dbms_utility.format_call_stack,1,1800)
	into ds_origem_w
	from dual;

	ds_unidade_log_w := :new.nr_atendimento ||' - '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss') || ' - ' || :new.nr_seq_interno ||'---'||ds_origem_w;

	insert 	into log_mov
			(DT_ATUALIZACAO,
			 NM_USUARIO,
			 CD_LOG,
			 DS_LOG)
		values	(sysdate,
			 :new.nm_usuario,
			 2286249,
			 ds_unidade_log_w);
end if;


if (updating) and
   (:old.dt_atualizacao is not null) and
   (:old.dt_atualizacao = :new.dt_atualizacao) then

   	select substr(dbms_utility.format_call_stack,1,1800)
	into ds_origem_w
	from dual;

	ds_unidade_log_w := :new.nr_atendimento ||' - '||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss') || ' - ' || :new.nr_seq_interno ||'---'||ds_origem_w;

	insert 	into log_mov
			(DT_ATUALIZACAO,
			 NM_USUARIO,
			 CD_LOG,
			 DS_LOG)
		values	(sysdate,
			 :new.nm_usuario,
			 5584024,
			 ds_unidade_log_w);

	:new.dt_atualizacao := sysdate;

end if;

if (:new.ie_passagem_Setor = 'S') then
	if (inserting) then
		ds_insert_w := substr(OBTER_DESC_EXPRESSAO(306417),1,60);
	elsif (updating) then
		ds_insert_w := substr(OBTER_DESC_EXPRESSAO(621767),1,60);
	end if;

	select substr(dbms_utility.format_call_stack,1,1800)
	into ds_origem_w
	from dual;

	ds_unidade_log_w :=
			SUBSTR( SUBSTR(OBTER_DESC_EXPRESSAO(325836), 1, 4000) ||':'||:new.nr_atendimento
			||' '|| ds_insert_w
			||' - '||SUBSTR(OBTER_DESC_EXPRESSAO(724202), 1, 4000) ||' '||ds_origem_w
			||' - '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||':'||:new.cd_setor_atendimento
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(341592), 1, 4000)||:new.cd_unidade_basica
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(341593), 1, 4000)||:new.cd_unidade_compl
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(646538), 1, 4000)||':'||to_char(:new.dt_entrada_unidade,'dd/mm/yyyy hh24:mi:ss')
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(288878), 1, 4000)||':'||to_char(:new.dt_saida_unidade,'dd/mm/yyyy hh24:mi:ss')
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(335149), 1, 4000)||':'||:new.ie_passagem_Setor
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(328225), 1, 4000)||':'||:new.nm_usuario
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(345366), 1, 4000)||':'||substr(obter_desc_funcao(obter_funcao_ativa),1,30)
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(330290), 1, 4000)||':'||substr(OBTER_DESC_PERFIL(obter_perfil_ativo),1,30)
			||' '||SUBSTR(OBTER_DESC_EXPRESSAO(328225), 1, 4000)||':'||substr(obter_usuario_ativo,1,30)
			,1,4000);


	insert 	into log_mov
			(DT_ATUALIZACAO,
			 NM_USUARIO,
			 CD_LOG,
			 DS_LOG)
		values	(sysdate,
			 :new.nm_usuario,
			 55840,
			 ds_unidade_log_w);

end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.atend_paciente_unid_ger_update
AFTER UPDATE ON TASY.ATEND_PACIENTE_UNIDADE FOR EACH ROW
BEGIN
   IF (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_DE') THEN
        IF  (((:new.cd_setor_atendimento) <> (:old.cd_setor_atendimento)) OR
             ((:new.cd_unidade_basica) <> (:old.cd_unidade_basica)) OR
             ((:new.cd_unidade_compl) <> (:old.cd_unidade_compl))) and
            (:new.dt_saida_unidade is null) then

            -- Reserva o novo leito
            UPDATE  unidade_atendimento unat
            SET unat.nr_atendimento         = :new.nr_atendimento,
                unat.dt_entrada_unidade     = :new.dt_entrada_unidade,
                unat.ie_status_unidade      = 'P',
                unat.nm_usuario         = :new.nm_usuario,
                unat.dt_atualizacao     = SYSDATE
            WHERE   unat.cd_unidade_basica      = :new.cd_unidade_basica
            AND unat.cd_unidade_compl       = :new.cd_unidade_compl
            AND unat.cd_setor_atendimento   = :new.cd_setor_atendimento
            and unat.ie_status_unidade      = 'L'
            and obter_se_sem_acomodacao(unat.cd_tipo_acomodacao) <> 'S';
            -- Liberar o leito antigo
            UPDATE  unidade_atendimento unat
            SET unat.nr_atendimento         = null,
                unat.dt_entrada_unidade     = null,
                unat.ie_status_unidade      = 'L',
                unat.nm_usuario         = :new.nm_usuario,
                unat.dt_atualizacao     = SYSDATE
            WHERE   unat.cd_unidade_basica      = :old.cd_unidade_basica
            AND unat.cd_unidade_compl       = :old.cd_unidade_compl
            and unat.nr_atendimento         = :old.nr_atendimento
            AND unat.cd_setor_atendimento   = :old.cd_setor_atendimento;
        END IF;
   END IF;
END;
/


DROP SYNONYM TASY_CONSULTA.ATEND_PACIENTE_UNIDADE;

CREATE SYNONYM TASY_CONSULTA.ATEND_PACIENTE_UNIDADE FOR TASY.ATEND_PACIENTE_UNIDADE;


ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE ADD (
  CONSTRAINT ATEPACU_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, DT_ENTRADA_UNIDADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT ATEPACU_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          48M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE ADD (
  CONSTRAINT ATEPACU_CLAEPAC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ESP) 
 REFERENCES TASY.CLASSIF_ESPECIAL_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT ATEPACU_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT ATEPACU_AGRUSET_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.AGRUPAMENTO_SETOR (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ATEPACU_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_MOTTRPA_FK2 
 FOREIGN KEY (NR_SEQ_MOTIVO_PERM) 
 REFERENCES TASY.MOTIVO_TRANSF_PAC (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPACU_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT ATEPACU_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL),
  CONSTRAINT ATEPACU_MOTTRPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TRANSF) 
 REFERENCES TASY.MOTIVO_TRANSF_PAC (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_MODIFAC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DIF) 
 REFERENCES TASY.MOTIVO_DIF_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_MODIFDI_FK 
 FOREIGN KEY (NR_SEQ_MOT_DIF_DIARIA) 
 REFERENCES TASY.MOTIVO_DIFERENCA_DIARIA (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_MOTALSE_FK 
 FOREIGN KEY (CD_MOTIVO_ALTA_SETOR) 
 REFERENCES TASY.MOTIVO_ALTA_SETOR (CD_MOTIVO_ALTA_SETOR),
  CONSTRAINT ATEPACU_PROSETO_FK 
 FOREIGN KEY (CD_PROCEDENCIA_SETOR) 
 REFERENCES TASY.PROCEDENCIA_SETOR (CD_PROCEDENCIA_SETOR),
  CONSTRAINT ATEPACU_MOINSET_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INT) 
 REFERENCES TASY.MOTIVO_INTERNACAO_SETOR (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_MOINSES_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INT_SUB) 
 REFERENCES TASY.MOTIVO_INT_SETOR_SUB (NR_SEQUENCIA),
  CONSTRAINT ATEPACU_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA_ATEND) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA));

GRANT SELECT ON TASY.ATEND_PACIENTE_UNIDADE TO NIVEL_1;

GRANT SELECT ON TASY.ATEND_PACIENTE_UNIDADE TO TASY_CONSULTA;

