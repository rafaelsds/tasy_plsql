ALTER TABLE TASY.PLS_ENVIO_LOTE_GISS_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ENVIO_LOTE_GISS_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ENVIO_LOTE_GISS_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TRIBUTO           NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSELGT_PK ON TASY.PLS_ENVIO_LOTE_GISS_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSELGT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSELGT_PLSLEGI_FK_I ON TASY.PLS_ENVIO_LOTE_GISS_TRIB
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSELGT_PLSLEGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSELGT_TRIBUTO_FK_I ON TASY.PLS_ENVIO_LOTE_GISS_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSELGT_TRIBUTO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ENVIO_LOTE_GISS_TRIB ADD (
  CONSTRAINT PLSELGT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ENVIO_LOTE_GISS_TRIB ADD (
  CONSTRAINT PLSELGT_PLSLEGI_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_ENVIO_GISS (NR_SEQUENCIA),
  CONSTRAINT PLSELGT_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.PLS_ENVIO_LOTE_GISS_TRIB TO NIVEL_1;

