ALTER TABLE TASY.CP_DIAG_INTERV_PLAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CP_DIAG_INTERV_PLAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.CP_DIAG_INTERV_PLAN
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_POSSIBLE_DIAG  NUMBER(10)              NOT NULL,
  DS_DISPLAY_NAME       VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TYPE_PLAN          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPDIINPL_CPPODIA_FK_I ON TASY.CP_DIAG_INTERV_PLAN
(NR_SEQ_POSSIBLE_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPDIINPL_PK ON TASY.CP_DIAG_INTERV_PLAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CP_DIAG_INTERV_PLAN ADD (
  CONSTRAINT CPDIINPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CP_DIAG_INTERV_PLAN ADD (
  CONSTRAINT CPDIINPL_CPPODIA_FK 
 FOREIGN KEY (NR_SEQ_POSSIBLE_DIAG) 
 REFERENCES TASY.CP_POSSIBLE_DIAG (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CP_DIAG_INTERV_PLAN TO NIVEL_1;

