ALTER TABLE TASY.LOTE_AUDIT_RECURSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_AUDIT_RECURSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_AUDIT_RECURSO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_LOTE                 VARCHAR2(20 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_INICIAL              DATE                  NOT NULL,
  DT_FINAL                DATE                  NOT NULL,
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(5)             NOT NULL,
  IE_ACAO_HISTORICO       NUMBER(1),
  DT_GERACAO              DATE,
  DT_ENVIO                DATE,
  DT_PREV_RETORNO         DATE,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_SITUACAO_LOTE        VARCHAR2(1 BYTE),
  NM_USUARIO_RESP         VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(400 BYTE),
  NR_SEQ_LOTE_AUDIT_HIST  NUMBER(10),
  NR_SEQ_RETORNO          NUMBER(10),
  NR_SEQ_RET_FILTRO       NUMBER(10),
  DT_REFERENCIA           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOAUREC_CONRETO_FK_I ON TASY.LOTE_AUDIT_RECURSO
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUREC_CONRETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUREC_CONRETO_FK2_I ON TASY.LOTE_AUDIT_RECURSO
(NR_SEQ_RET_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUREC_CONRETO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUREC_CONVENI_FK_I ON TASY.LOTE_AUDIT_RECURSO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUREC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUREC_ESTABEL_FK_I ON TASY.LOTE_AUDIT_RECURSO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUREC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOAUREC_LOTAUHI_FK_I ON TASY.LOTE_AUDIT_RECURSO
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOAUREC_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LOAUREC_PK ON TASY.LOTE_AUDIT_RECURSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOTE_AUDIT_RECURSO ADD (
  CONSTRAINT LOAUREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_AUDIT_RECURSO ADD (
  CONSTRAINT LOAUREC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT LOAUREC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT LOAUREC_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT LOAUREC_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA),
  CONSTRAINT LOAUREC_CONRETO_FK2 
 FOREIGN KEY (NR_SEQ_RET_FILTRO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_AUDIT_RECURSO TO NIVEL_1;

