ALTER TABLE TASY.REGRA_PREVISAO_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PREVISAO_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PREVISAO_ALTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_PERFIL              NUMBER(5),
  NM_USUARIO             VARCHAR2(50 BYTE),
  IE_PACIENTE_INTERNADO  VARCHAR2(1 BYTE),
  IE_PRESCR_ALTA         VARCHAR2(1 BYTE),
  IE_LIMPA_CAMPOS        VARCHAR2(1 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_USUARIO_DEST        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPRAL_ESTABEL_FK_I ON TASY.REGRA_PREVISAO_ALTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPRAL_PERFIL_FK_I ON TASY.REGRA_PREVISAO_ALTA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REPRAL_PK ON TASY.REGRA_PREVISAO_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPRAL_USUARIO_FK_I ON TASY.REGRA_PREVISAO_ALTA
(NM_USUARIO_DEST)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_PREVISAO_ALTA ADD (
  CONSTRAINT REPRAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PREVISAO_ALTA ADD (
  CONSTRAINT REPRAL_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_DEST) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT REPRAL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REPRAL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.REGRA_PREVISAO_ALTA TO NIVEL_1;

