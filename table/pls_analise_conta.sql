ALTER TABLE TASY.PLS_ANALISE_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ANALISE_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ANALISE_CONTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_PRESTADOR       NUMBER(10),
  CD_GUIA                VARCHAR2(20 BYTE),
  NR_SEQ_SEGURADO        NUMBER(10),
  IE_STATUS              VARCHAR2(3 BYTE)       NOT NULL,
  DT_ANALISE             DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_PROTOCOLO  NUMBER(10),
  DS_CONTA               VARCHAR2(4000 BYTE),
  DT_LIBERACAO_ANALISE   DATE,
  DT_INICIO_ANALISE      DATE,
  DT_FINAL_ANALISE       DATE,
  IE_AUDITORIA           VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIPO_GUIA           VARCHAR2(2 BYTE),
  NR_SEQ_CONGENERE       NUMBER(10),
  IE_ORIGEM_ANALISE      NUMBER(10),
  IE_PRE_ANALISE         VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_LIB       NUMBER(10),
  IE_STATUS_PRE_ANALISE  VARCHAR2(3 BYTE),
  IE_ATENDE_GLOSADO      VARCHAR2(1 BYTE),
  IE_ORIGEM_CONTA        VARCHAR2(3 BYTE),
  IE_PRECO               VARCHAR2(2 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_SEQ_ANALISE_REF     NUMBER(10),
  NM_PRESTADOR           VARCHAR2(255 BYTE),
  NM_COOPERATIVA         VARCHAR2(255 BYTE),
  NM_SEGURADO            VARCHAR2(255 BYTE),
  CD_COOPERATIVA         VARCHAR2(10 BYTE),
  NR_SEQ_GRUPO_AUDITOR   NUMBER(10),
  NM_PROC_STATUS         VARCHAR2(100 BYTE),
  IE_PCMSO               VARCHAR2(1 BYTE),
  NR_FATURA              VARCHAR2(30 BYTE),
  DT_PRAZO_ANALISE       DATE,
  NR_SEQ_REGRA_AGRUP     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSANCO_ESTABEL_FK_I ON TASY.PLS_ANALISE_CONTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSANCO_I1 ON TASY.PLS_ANALISE_CONTA
(CD_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_I3 ON TASY.PLS_ANALISE_CONTA
(NR_SEQ_ANALISE_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_I4 ON TASY.PLS_ANALISE_CONTA
(IE_TIPO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_I5 ON TASY.PLS_ANALISE_CONTA
(IE_PRE_ANALISE, IE_STATUS, NR_SEQ_GRUPO_AUDITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_I6 ON TASY.PLS_ANALISE_CONTA
(DT_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSANCO_PK ON TASY.PLS_ANALISE_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_PLSCONG_FK_I ON TASY.PLS_ANALISE_CONTA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANCO_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSANCO_PLSGRAU_FK_I ON TASY.PLS_ANALISE_CONTA
(NR_SEQ_GRUPO_AUDITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSANCO_PLSGRAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSANCO_PLSLOPC_FK_I ON TASY.PLS_ANALISE_CONTA
(NR_SEQ_LOTE_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANCO_PLSRGERAN_FK_I ON TASY.PLS_ANALISE_CONTA
(NR_SEQ_REGRA_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_analise_conta_fat
after update ON TASY.PLS_ANALISE_CONTA for each row
declare
cursor C01(	nr_seq_analise_pc	pls_conta_pos_estabelecido.nr_seq_analise%type) is
	select	nr_sequencia
	from	pls_conta
	where	nr_sequencia in ( select nr_seq_conta
				 from	pls_conta_pos_estabelecido
				 where	nr_seq_analise = nr_seq_analise_pc);


begin

if	(:new.ie_status = 'T') and (:old.ie_status <> 'T') and
	(:new.ie_origem_analise = '2') then

	for r_c01_w in C01(:new.nr_sequencia) loop

		update	pls_conta
		set	ie_status_fat = 'L'
		where	nr_sequencia = r_c01_w.nr_sequencia;

	end loop;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_analise_conta_atual
before insert or update ON TASY.PLS_ANALISE_CONTA for each row
declare
qt_conta_fechada_w	pls_integer;
ie_status_lote_w	pls_lote_protocolo_conta.ie_status%type;
ds_observacao_w		varchar2(4000);
qt_registro_w		pls_integer;

begin

--Se an�lise estava cancelada e por alguma raz�o teve seu status alterado
if ( ( nvl(:new.ie_status,'X')) <> 'C' and :old.ie_status = 'C') then



	ds_observacao_w := ' Status da an�lise era '||:old.ie_status||' e foi alterado para: '||:new.ie_status;



	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	ds_observacao_w ,sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);

end if;

if	((:new.ie_status = 'T') and (:old.ie_status	!= 'T')) and
	(:new.dt_final_analise	is null) then
	begin
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'Sem data Final ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
	end;
end if;

if	((:new.ie_status = 'T') and (:old.ie_status	!= 'T')) then
	begin
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise encerrada ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
	end;
end if;

select	count(1)
into	qt_conta_fechada_w
from	pls_conta
where	nr_seq_analise = :new.nr_sequencia
and	ie_status = 'F';

if	(:new.ie_status != 'T') and (qt_conta_fechada_w > 0) and
	(:new.ie_status <> :old.ie_status) then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise aberta com contas fechadas ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

if	(:new.ie_status = 'T') and (:new.ie_pre_analise = 'S') and (:old.ie_pre_analise != 'S') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'Retorno da an�lise para pr�-an�lise ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

select	max(ie_status)
into	ie_status_lote_w
from	pls_lote_protocolo_conta
where	nr_sequencia = :new.nr_seq_lote_protocolo;

-- Caso a an�lise entre em um lote "Aguardando guia" mas a an�lise n�o esteja com esse status.
if	(:new.ie_status <> :old.ie_status) and
	(:new.ie_status <> 'X') and (ie_status_lote_w = 'G') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise com status diferente de Aguardando guia em lote Aguardando guia ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

select 	count(1)
into	qt_registro_w
from 	pls_cta_analise_cons c
where 	c.ie_status != 'F';

if	(qt_registro_w > 0) and
	(:old.ie_status = 'D') and
	(:new.ie_status != 'D') then
	insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
						ie_tipo_historico, ie_tipo_item, nm_usuario,
						nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
						nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
						nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
						nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
						ds_call_stack)
				values 	(	'An�lise pendente de processamento com status liberado para auditoria ',sysdate,sysdate,
						'1',null,:new.nm_usuario,
						:new.nm_usuario,:new.nr_sequencia,null,
						null,null,null,
						null, null,null,null,
						null,null,pls_hist_analise_conta_seq.nextval,
						dbms_utility.format_call_stack);
end if;

-- somente atualizacao
if	(updating) then
	if	((:new.ie_status != 'T') and (:old.ie_status	= 'T')) then

		insert into pls_hist_analise_conta (	ds_observacao,dt_atualizacao, dt_atualizacao_nrec,
							ie_tipo_historico, ie_tipo_item, nm_usuario,
							nm_usuario_nrec, nr_seq_analise, nr_seq_conta,
							nr_seq_conta_glosa, nr_seq_conta_mat, nr_seq_conta_proc,
							nr_seq_glosa, nr_seq_grupo, nr_seq_item,nr_seq_ocor_benef,
							nr_seq_ocorrencia, nr_seq_proc_partic, nr_sequencia,
							ds_call_stack)
					values 	(	'An�lise reaberta ',sysdate,sysdate,
							'24',null,:new.nm_usuario,
							:new.nm_usuario,:new.nr_sequencia,null,
							null,null,null,
							null, null,null,null,
							null,null,pls_hist_analise_conta_seq.nextval,
							dbms_utility.format_call_stack);

	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_ANALISE_CONTA ADD (
  CONSTRAINT PLSANCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ANALISE_CONTA ADD (
  CONSTRAINT PLSANCO_PLSRGERAN_FK 
 FOREIGN KEY (NR_SEQ_REGRA_AGRUP) 
 REFERENCES TASY.PLS_REGRA_GERACAO_ANALISE (NR_SEQUENCIA),
  CONSTRAINT PLSANCO_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSANCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSANCO_PLSLOPC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PROTOCOLO) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSANCO_PLSGRAU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AUDITOR) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ANALISE_CONTA TO NIVEL_1;

