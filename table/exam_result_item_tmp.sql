DROP TABLE TASY.EXAM_RESULT_ITEM_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.EXAM_RESULT_ITEM_TMP
(
  NR_SEQUENCIA             NUMBER(10),
  DT_RESULTADO             DATE,
  NR_SEQ_EXAME             NUMBER(10),
  NR_SEQ_RESULTADO         NUMBER(10),
  DT_ATUALIZACAO           DATE,
  NM_USUARIO               VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_ATENDIMENTO           NUMBER(10),
  NR_PRESCRICAO            NUMBER(10),
  QT_RESULTADO             NUMBER(15,4),
  DS_RESULTADO             VARCHAR2(4000 BYTE),
  NR_SEQ_METODO            NUMBER(10),
  NR_SEQ_MATERIAL          NUMBER(10),
  PR_RESULTADO             NUMBER(9,4),
  DT_APROVACAO             DATE,
  NR_SEQ_PRESCR            NUMBER(6),
  DT_LIBERACAO             DATE,
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  DS_REFERENCIA            VARCHAR2(4000 BYTE),
  DS_UNIDADE_MEDIDA        VARCHAR2(40 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  QT_DECIMAIS              NUMBER(1),
  IE_NORMALIDADE           NUMBER(10),
  DT_DIGITACAO             DATE,
  DT_EXAME_EXT             DATE,
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE),
  IE_RESULTADO_REFERENCIA  VARCHAR2(3 BYTE),
  IE_RESULTADO_CRITICO     VARCHAR2(3 BYTE),
  DS_REFERENCIA_EXT        VARCHAR2(4000 BYTE),
  DS_FLAG                  VARCHAR2(100 BYTE),
  NR_SEQ_ATEND_CONS_PEPA   NUMBER(10),
  IE_FINAL                 VARCHAR2(1 BYTE),
  DS_COR_LEGENDA           VARCHAR2(255 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.EXAM_RESULT_ITEM_TMP TO NIVEL_1;

