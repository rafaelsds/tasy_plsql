ALTER TABLE TASY.CAN_GRAU_DIFERENCIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAN_GRAU_DIFERENCIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAN_GRAU_DIFERENCIACAO
(
  CD_GRAU              VARCHAR2(3 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_GRAU              VARCHAR2(60 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CANGRDI_PK ON TASY.CAN_GRAU_DIFERENCIACAO
(CD_GRAU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANGRDI_PK
  MONITORING USAGE;


ALTER TABLE TASY.CAN_GRAU_DIFERENCIACAO ADD (
  CONSTRAINT CANGRDI_PK
 PRIMARY KEY
 (CD_GRAU)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CAN_GRAU_DIFERENCIACAO TO NIVEL_1;

