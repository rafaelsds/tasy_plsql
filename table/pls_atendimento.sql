ALTER TABLE TASY.PLS_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATENDIMENTO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  IE_ORIGEM_ATENDIMENTO         VARCHAR2(1 BYTE),
  IE_TIPO_PESSOA                VARCHAR2(3 BYTE),
  IE_TIPO_OCORRENCIA            VARCHAR2(3 BYTE),
  DT_INICIO                     DATE            NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  DT_FIM_ATENDIMENTO            DATE,
  NR_SEQ_EVENTO                 NUMBER(10),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  CD_CGC                        VARCHAR2(14 BYTE),
  NR_SEQ_REGRA_TEMPO            NUMBER(10),
  NR_SEQ_SEGURADO               NUMBER(10),
  NR_SEQ_CONTRATO               NUMBER(10),
  NR_SEQ_PAGADOR                NUMBER(10),
  NR_SEQ_PRESTADOR              NUMBER(10),
  IE_STATUS                     VARCHAR2(1 BYTE) NOT NULL,
  DT_CONCLUSAO                  DATE,
  NM_CONTATO                    VARCHAR2(60 BYTE),
  NR_TELEFONE                   VARCHAR2(20 BYTE),
  NR_SEQ_CONGENERE              NUMBER(10),
  IE_TIPO_ATENDIMENTO           VARCHAR2(3 BYTE) NOT NULL,
  NR_SEQ_OPERADOR               NUMBER(10)      NOT NULL,
  DS_HISTORICO_ATEND_CHAT       LONG,
  NR_SEQ_MOTIVO_CONCLUSAO       NUMBER(10),
  DS_DIRETORIO_ARQUIVO          VARCHAR2(255 BYTE),
  IE_ESTAGIO                    VARCHAR2(10 BYTE),
  NR_SEQ_LOTE_NOTIF             NUMBER(10),
  CD_SETOR_ATENDIMENTO          NUMBER(5),
  NR_SEQ_AGENTE_MOTIVADOR       NUMBER(10),
  NR_PROTOCOLO_ATENDIMENTO      VARCHAR2(20 BYTE),
  NR_SEQ_AUDITORIA              NUMBER(10),
  NR_SEQ_PAGADOR_NOTIF          NUMBER(10),
  NR_SEQ_NOTIFICACAO            NUMBER(10),
  NR_SEQ_MPREV_PART_CICLO_ITEM  NUMBER(10),
  NR_SEQ_CAPTACAO               NUMBER(10),
  NR_SEQUENCIAL_DIA             NUMBER(6),
  NR_SEQ_SUBTIPO_PESSOA         NUMBER(10),
  NR_PROTOCOLO_REFERENCIA       VARCHAR2(100 BYTE),
  NR_SEQ_SOLIC_RESCISAO         NUMBER(10),
  NR_SEQ_LEAD                   NUMBER(10),
  NR_DDI_TELEFONE               VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE               VARCHAR2(3 BYTE),
  NR_SEQ_PROP_ONLINE            NUMBER(10),
  NR_SEQ_NOTIFICICACAO_NEG      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSATEN_CALSTIP_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_SUBTIPO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_ESTABEL_FK_I ON TASY.PLS_ATENDIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_I1 ON TASY.PLS_ATENDIMENTO
(DT_INICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_I2 ON TASY.PLS_ATENDIMENTO
(DT_FIM_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_I2
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_I3 ON TASY.PLS_ATENDIMENTO
(DT_CONCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_I3
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_I4 ON TASY.PLS_ATENDIMENTO
(NR_PROTOCOLO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_MPRCAPT_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_MPRPACI_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_MPREV_PART_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PESFISI_FK_I ON TASY.PLS_ATENDIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PESJURI_FK_I ON TASY.PLS_ATENDIMENTO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSATEN_PK ON TASY.PLS_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSAGMO_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_AGENTE_MOTIVADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSAGMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSATRT_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_REGRA_TEMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PLSAUDI_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSAUDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSCONG_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSEVOC_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSEVOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSNOAN_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_NOTIFICICACAO_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PLSNOAT_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_NOTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSNOAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSNOLO_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_LOTE_NOTIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSNOLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSNOPA_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_PAGADOR_NOTIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSNOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSOPER_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_OPERADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_PLSOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSATEN_PLSPRON_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_PROP_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PLSSCOM_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_LEAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_PLSSRES_FK_I ON TASY.PLS_ATENDIMENTO
(NR_SEQ_SOLIC_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSATEN_SETATEN_FK_I ON TASY.PLS_ATENDIMENTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSATEN_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_ATENDIMENTO_DELETE
before delete ON TASY.PLS_ATENDIMENTO for each row
declare

ds_call_stack_w	varchar2(2000);
nm_usuario_exclusao_w	varchar2(30);

begin

nm_usuario_exclusao_w := wheb_usuario_pck.get_nm_usuario;
ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);


gravar_log_tasy(32, wheb_mensagem_pck.get_texto(301081)||' '||:old.cd_pessoa_fisica || '  '||wheb_mensagem_pck.get_texto(796919)||'   ' || :old.ie_tipo_pessoa || '  '||wheb_mensagem_pck.get_texto(796920)||':  ' || :old.nr_protocolo_atendimento ||
		'  '||wheb_mensagem_pck.get_texto(796921)||':  '  || :old.NR_SEQ_SEGURADO || '  '||wheb_mensagem_pck.get_texto(796922)||':  ' || :old.NR_SEQ_CONTRATO || wheb_mensagem_pck.get_texto(796923) ||':  ' || :old.NR_SEQ_PAGADOR || wheb_mensagem_pck.get_texto(796924) ||'  ' || :old.NR_SEQ_PRESTADOR
		|| '  '||wheb_mensagem_pck.get_texto(796926)||'  ' || :old.IE_STATUS || wheb_mensagem_pck.get_texto(796927) ||'  ' || :old.DT_INICIO || '  nm_usuario:  ' || :old.nm_usuario_nrec || ' Call Stack:  ' || ds_call_stack_w, nm_usuario_exclusao_w );

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_atendimento_update
before update ON TASY.PLS_ATENDIMENTO for each row
declare
nr_seq_prot_atend_w	pls_protocolo_atendimento.nr_sequencia%type;

begin
if 	(:new.nr_seq_segurado is not null) and
	(nvl(:old.ie_status, 'X') <> nvl(:new.ie_status, 'X')) and
	(:old.ie_status in ('P','G')) and
	(:new.ie_status in ('C', 'M')) then

	begin
		select	nr_sequencia
		into	nr_seq_prot_atend_w
		from	pls_protocolo_atendimento
		where 	nr_seq_atend_pls	= :new.nr_sequencia
		and 	ie_status		= 1;
	exception
	when others then
		nr_seq_prot_atend_w	:= null;
	end;

	if (nr_seq_prot_atend_w is not null) then
		update	pls_protocolo_atendimento
		set	ie_status	= 3,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao 	= sysdate
		where	nr_sequencia	= nr_seq_prot_atend_w;
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_ATENDIMENTO ADD (
  CONSTRAINT PLSATEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATENDIMENTO ADD (
  CONSTRAINT PLSATEN_PLSSRES_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_RESCISAO) 
 REFERENCES TASY.PLS_SOLICITACAO_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSSCOM_FK 
 FOREIGN KEY (NR_SEQ_LEAD) 
 REFERENCES TASY.PLS_SOLICITACAO_COMERCIAL (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSPRON_FK 
 FOREIGN KEY (NR_SEQ_PROP_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSNOAN_FK 
 FOREIGN KEY (NR_SEQ_NOTIFICICACAO_NEG) 
 REFERENCES TASY.PLS_NOTIFICACAO_ATEND_NEG (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_MPRPACI_FK 
 FOREIGN KEY (NR_SEQ_MPREV_PART_CICLO_ITEM) 
 REFERENCES TASY.MPREV_PARTIC_CICLO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSATRT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TEMPO) 
 REFERENCES TASY.PLS_ATEND_REGRA_TEMPO (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_CALSTIP_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_PESSOA) 
 REFERENCES TASY.CALL_SUBTIPO_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSNOAT_FK 
 FOREIGN KEY (NR_SEQ_NOTIFICACAO) 
 REFERENCES TASY.PLS_NOTIFICACAO_ATEND (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSATEN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSATEN_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSATEN_PLSEVOC_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSOPER_FK 
 FOREIGN KEY (NR_SEQ_OPERADOR) 
 REFERENCES TASY.PLS_OPERADOR (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSNOLO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_NOTIF) 
 REFERENCES TASY.PLS_NOTIFICACAO_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PLSATEN_PLSAGMO_FK 
 FOREIGN KEY (NR_SEQ_AGENTE_MOTIVADOR) 
 REFERENCES TASY.PLS_AGENTE_MOTIVADOR (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSAUDI_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.PLS_AUDITORIA (NR_SEQUENCIA),
  CONSTRAINT PLSATEN_PLSNOPA_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR_NOTIF) 
 REFERENCES TASY.PLS_NOTIFICACAO_PAGADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATENDIMENTO TO NIVEL_1;

