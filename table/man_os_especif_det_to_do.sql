ALTER TABLE TASY.MAN_OS_ESPECIF_DET_TO_DO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_OS_ESPECIF_DET_TO_DO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_OS_ESPECIF_DET_TO_DO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DETALHE       NUMBER(10)               NOT NULL,
  NR_ORDEM             NUMBER(5)                NOT NULL,
  NM_ATIVIDADE         VARCHAR2(255 BYTE)       NOT NULL,
  QT_MIN_PREV          NUMBER(5),
  DS_ATIVIDADE         LONG                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSEDTD_MANOSEDE_FK_I ON TASY.MAN_OS_ESPECIF_DET_TO_DO
(NR_SEQ_DETALHE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANOSEDTD_PK ON TASY.MAN_OS_ESPECIF_DET_TO_DO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_OS_ESPECIF_DET_TO_DO ADD (
  CONSTRAINT MANOSEDTD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_OS_ESPECIF_DET_TO_DO ADD (
  CONSTRAINT MANOSEDTD_MANOSEDE_FK 
 FOREIGN KEY (NR_SEQ_DETALHE) 
 REFERENCES TASY.MAN_OS_ESPECIF_DETALHE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_OS_ESPECIF_DET_TO_DO TO NIVEL_1;

