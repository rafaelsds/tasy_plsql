ALTER TABLE TASY.PLS_OC_CTA_VAL_UTIL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_UTIL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_UTIL_ITEM
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA              NUMBER(10),
  QT_LIBERADA                   NUMBER(9,3),
  IE_TIPO_QTDE                  VARCHAR2(3 BYTE),
  QT_TIPO_QUANTIDADE            NUMBER(9,3),
  NR_SEQ_OC_CTA_COMB            NUMBER(10)      NOT NULL,
  IE_TIPO_PESSOA_QTDE           VARCHAR2(10 BYTE),
  IE_REGRA_QTDE                 VARCHAR2(10 BYTE),
  IE_SOMAR_ESTRUTURA            VARCHAR2(1 BYTE),
  IE_QT_LIB_POSTERIOR           VARCHAR2(1 BYTE),
  NR_SEQ_GRAU_PARTIC            NUMBER(10),
  IE_MEDICO_PRESTADOR           VARCHAR2(1 BYTE),
  IE_BUSCA_MEDICO_NOTA_SERVICO  VARCHAR2(1 BYTE),
  IE_MESMO_CID                  VARCHAR2(1 BYTE),
  IE_MESMA_CATEGORIA_CID        VARCHAR2(1 BYTE),
  IE_MESMA_ESPECIALIDADE        VARCHAR2(1 BYTE),
  IE_MESMO_MEDICO               VARCHAR2(1 BYTE),
  IE_GRAU_PARTICIPACAO          VARCHAR2(1 BYTE),
  IE_CAMPO_DT_VALIDAR           VARCHAR2(1 BYTE),
  IE_MEDICO_CONSISTENCIA        VARCHAR2(3 BYTE),
  IE_GUIA_DIFERENTE             VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.POCVUIT_PK ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.POCVUIT_PK
  MONITORING USAGE;


CREATE INDEX TASY.POCVUIT_PLSGRPA_FK_I ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.POCVUIT_PLSGRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.POCVUIT_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.POCVUIT_PLSOCCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.POCVUIT_PLSOCES_FK_I ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.POCVUIT_PLSOCES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_UTIL_ITEM_tp  after update ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_MESMO_CID,1,4000),substr(:new.IE_MESMO_CID,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMO_CID',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDICO_PRESTADOR,1,4000),substr(:new.IE_MEDICO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_PRESTADOR',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMA_CATEGORIA_CID,1,4000),substr(:new.IE_MESMA_CATEGORIA_CID,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMA_CATEGORIA_CID',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMA_ESPECIALIDADE,1,4000),substr(:new.IE_MESMA_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMA_ESPECIALIDADE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMO_MEDICO,1,4000),substr(:new.IE_MESMO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMO_MEDICO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAMPO_DT_VALIDAR,1,4000),substr(:new.IE_CAMPO_DT_VALIDAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAMPO_DT_VALIDAR',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAU_PARTICIPACAO,1,4000),substr(:new.IE_GRAU_PARTICIPACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_PARTICIPACAO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDICO_CONSISTENCIA,1,4000),substr(:new.IE_MEDICO_CONSISTENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO_CONSISTENCIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRAU_PARTIC,1,4000),substr(:new.NR_SEQ_GRAU_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRAU_PARTIC',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LIBERADA,1,4000),substr(:new.QT_LIBERADA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LIBERADA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TIPO_QUANTIDADE,1,4000),substr(:new.QT_TIPO_QUANTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_TIPO_QUANTIDADE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_QTDE,1,4000),substr(:new.IE_TIPO_QTDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_QTDE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMAR_ESTRUTURA,1,4000),substr(:new.IE_SOMAR_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMAR_ESTRUTURA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QT_LIB_POSTERIOR,1,4000),substr(:new.IE_QT_LIB_POSTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_QT_LIB_POSTERIOR',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_QTDE,1,4000),substr(:new.IE_REGRA_QTDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_QTDE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PESSOA_QTDE,1,4000),substr(:new.IE_TIPO_PESSOA_QTDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PESSOA_QTDE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BUSCA_MEDICO_NOTA_SERVICO,1,4000),substr(:new.IE_BUSCA_MEDICO_NOTA_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BUSCA_MEDICO_NOTA_SERVICO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_UTIL_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_UTIL_ITEM ADD (
  CONSTRAINT POCVUIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_UTIL_ITEM ADD (
  CONSTRAINT POCVUIT_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT POCVUIT_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT POCVUIT_PLSOCES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_OCORRENCIA_ESTRUTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_UTIL_ITEM TO NIVEL_1;

