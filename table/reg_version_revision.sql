ALTER TABLE TASY.REG_VERSION_REVISION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_VERSION_REVISION CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_VERSION_REVISION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_REVISION          NUMBER(10)               NOT NULL,
  DS_XML_STRUCTURE     CLOB,
  DT_CLOSED_REVISION   DATE,
  NR_SEQ_VERSION       NUMBER(10),
  DS_PDF_STRUCT_PATH   VARCHAR2(1000 BYTE),
  DS_XML_STRUCT_PATH   VARCHAR2(1000 BYTE),
  IE_DOCUMENT_TYPE     VARCHAR2(2 BYTE),
  DS_HASH_PDF          VARCHAR2(4000 BYTE),
  DS_PDF_STRUCTURE     BLOB                     DEFAULT null,
  NR_SEQ_DOCUMENTO     NUMBER(10),
  IE_STATUS_REVISAO    VARCHAR2(3 BYTE),
  CD_VERSAO            VARCHAR2(255 BYTE),
  DS_DOC_STRUCTURE     BLOB,
  IE_DOC_EXTENSION     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_PDF_STRUCTURE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_DOC_STRUCTURE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_XML_STRUCTURE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGVERREV_PK ON TASY.REG_VERSION_REVISION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGVERREV_REGUDO_FK_I ON TASY.REG_VERSION_REVISION
(NR_SEQ_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGVERREV_REGVERSCH_FK_I ON TASY.REG_VERSION_REVISION
(NR_SEQ_VERSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_VERSION_REVISION ADD (
  CONSTRAINT REGVERREV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_VERSION_REVISION ADD (
  CONSTRAINT REGVERREV_REGUDO_FK 
 FOREIGN KEY (NR_SEQ_DOCUMENTO) 
 REFERENCES TASY.REG_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT REGVERREV_REGVERSCH_FK 
 FOREIGN KEY (NR_SEQ_VERSION) 
 REFERENCES TASY.REG_VERION_SCHEDULER (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_VERSION_REVISION TO NIVEL_1;

