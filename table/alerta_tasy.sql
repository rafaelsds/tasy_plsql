ALTER TABLE TASY.ALERTA_TASY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALERTA_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALERTA_TASY
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_WHEB           NUMBER(10)              NOT NULL,
  NM_ALERTA             VARCHAR2(80 BYTE)       NOT NULL,
  DS_OBJETIVO           VARCHAR2(255 BYTE)      NOT NULL,
  DS_ACAO_USUARIO       VARCHAR2(255 BYTE)      NOT NULL,
  QT_MIN_REPETICAO      NUMBER(8)               NOT NULL,
  QT_SEG_LIMITE         NUMBER(3)               NOT NULL,
  DS_COMANDO            VARCHAR2(4000 BYTE)     NOT NULL,
  QT_VALOR_MINIMO       NUMBER(10,3)            NOT NULL,
  QT_VALOR_MAXIMO       NUMBER(10,3)            NOT NULL,
  DT_ULTIMO_PROC        DATE                    NOT NULL,
  DT_PROXIMO_PROC       DATE                    NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_COMANDO_ADIC       VARCHAR2(4000 BYTE),
  DS_REMETENTE_SMS      VARCHAR2(80 BYTE),
  DT_EXEC_COMANDO_ADIC  DATE,
  IE_DASHBOARD          VARCHAR2(1 BYTE),
  NR_SEQ_TIPO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ALETASY_ESTABEL_FK_I ON TASY.ALERTA_TASY
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ALETASY_PK ON TASY.ALERTA_TASY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALETASY_TPALERTASY_FK_I ON TASY.ALERTA_TASY
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ALERTA_TASY ADD (
  CONSTRAINT ALETASY_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ALERTA_TASY ADD (
  CONSTRAINT ALETASY_TPALERTASY_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ALERTA_TASY (NR_SEQUENCIA),
  CONSTRAINT ALETASY_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.ALERTA_TASY TO NIVEL_1;

