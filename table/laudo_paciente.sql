ALTER TABLE TASY.LAUDO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE
(
  NR_ATENDIMENTO                 NUMBER(10),
  DT_ENTRADA_UNIDADE             DATE           NOT NULL,
  NR_LAUDO                       NUMBER(10)     NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  CD_MEDICO_RESP                 VARCHAR2(10 BYTE),
  DS_TITULO_LAUDO                VARCHAR2(255 BYTE),
  DT_LAUDO                       DATE,
  CD_LAUDO_PADRAO                NUMBER(5),
  IE_NORMAL                      VARCHAR2(1 BYTE),
  DT_EXAME                       DATE,
  NR_PRESCRICAO                  NUMBER(14),
  DS_LAUDO                       LONG,
  DT_APROVACAO                   DATE,
  NM_USUARIO_APROVACAO           VARCHAR2(15 BYTE),
  CD_PROTOCOLO                   NUMBER(10),
  NR_SEQUENCIA                   NUMBER(10),
  CD_PROJETO                     NUMBER(6),
  NR_SEQ_PROC                    NUMBER(10),
  NR_SEQ_PRESCRICAO              NUMBER(6),
  DT_LIBERACAO                   DATE,
  DT_PREV_ENTREGA                DATE,
  DT_REAL_ENTREGA                DATE,
  QT_IMAGEM                      NUMBER(3)      NOT NULL,
  NR_CONTROLE                    NUMBER(15),
  DT_SEG_APROVACAO               DATE,
  NM_USUARIO_SEG_APROV           VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_PARADA           NUMBER(10),
  CD_SETOR_ATENDIMENTO           NUMBER(5),
  NR_EXAME                       VARCHAR2(20 BYTE),
  CD_MEDICO_AUX                  VARCHAR2(10 BYTE),
  DT_ENVELOPADO                  DATE,
  DT_IMPRESSAO                   DATE,
  DT_FIM_DIGITACAO               DATE,
  NM_USUARIO_DIGITACAO           VARCHAR2(15 BYTE),
  DT_INICIO_DIGITACAO            DATE,
  DT_INTEGRACAO                  DATE,
  NM_MEDICO_SOLICITANTE          VARCHAR2(60 BYTE),
  IE_MIDIA_ENTREGUE              NUMBER(3),
  CD_TECNICO_RESP                VARCHAR2(10 BYTE),
  CD_SETOR_USUARIO               NUMBER(5),
  NM_USUARIO_CANCEL              VARCHAR2(15 BYTE),
  DT_CANCELAMENTO                DATE,
  NM_USUARIO_LIBERACAO           VARCHAR2(15 BYTE),
  QT_CARACTERES                  NUMBER(15),
  NR_SEQ_PAC_PROT_EXT            NUMBER(10),
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE),
  NR_SEQ_RESULTADO_PADRAO        NUMBER(10),
  CD_ANESTESISTA                 VARCHAR2(10 BYTE),
  NM_USUARIO_FIM_DIGIT           VARCHAR2(15 BYTE),
  DT_CONFERENCIA                 DATE,
  NR_SEQ_SUPERIOR                NUMBER(10),
  IE_STATUS_LAUDO                VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_CANC             NUMBER(10),
  CD_LAUDO_EXTERNO               NUMBER(10),
  NR_AGRUPAMENTO                 NUMBER(10),
  CD_RESIDENTE                   VARCHAR2(10 BYTE),
  NR_SEQ_PAGINA                  NUMBER(10),
  IE_CD_LAUDO                    VARCHAR2(1 BYTE),
  IE_TIPO_MEDIDA                 VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_MEDIDA            NUMBER(10),
  NR_SEQ_ASSINATURA              NUMBER(10),
  DT_ASSINATURA                  DATE,
  DT_DESAPROVACAO                DATE,
  IE_BIRADS                      VARCHAR2(15 BYTE),
  IE_FORMATO                     VARCHAR2(15 BYTE),
  DS_INDICACAO_CLINICA_LAUDO     VARCHAR2(255 BYTE),
  IE_TUMOR                       VARCHAR2(1 BYTE),
  DS_ARQUIVO                     VARCHAR2(255 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO      NUMBER(10),
  QT_AUDIO                       NUMBER(10),
  NR_SEQ_MOTIVO_DESAP            NUMBER(10),
  NR_QUALIDADE_PREPARO           NUMBER(10),
  DS_OBSERVACAO_PREPARO          VARCHAR2(255 BYTE),
  IE_FORMATO_TEXTO               NUMBER(2),
  IE_GERAR_COMUNIC               VARCHAR2(1 BYTE),
  DT_GRAVACAO_LAUDO              DATE,
  CD_RESP_SEG_APROV              VARCHAR2(10 BYTE),
  IE_EXIGE_SEG_APROV             VARCHAR2(1 BYTE),
  CD_SETOR_EXECUCAO              NUMBER(5),
  DT_INTEGRACAO_INTERF           DATE,
  IE_LIBERADO_DIGITADOR          VARCHAR2(1 BYTE),
  NR_SEQ_SEG_ASSINATURA          NUMBER(10),
  NR_SEQ_SEG_ASSINAT_INATIVACAO  NUMBER(10),
  DT_UREASE                      DATE,
  IE_URGENTE                     VARCHAR2(1 BYTE),
  IE_UREASE                      VARCHAR2(2 BYTE),
  QT_DLP                         NUMBER(6,2),
  IE_ORIENTACAO_IMAGEM           VARCHAR2(1 BYTE),
  DS_DIMENSAO_IMAGEM             VARCHAR2(15 BYTE),
  IE_NIVEL_ATENCAO               VARCHAR2(1 BYTE),
  DS_UTC                         VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO               VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO             VARCHAR2(50 BYTE),
  IE_IMPRESSO                    VARCHAR2(1 BYTE),
  NR_SEQ_SOAP                    NUMBER(10),
  IE_DUVIDA                      VARCHAR2(1 BYTE),
  NR_CONTROLE_EXT                NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          760M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUPACI_ATEPACI_FK_I ON TASY.LAUDO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_ATESOAP_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_IMPACPROT_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_PAC_PROT_EXT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_IMPACPROT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_I1 ON TASY.LAUDO_PACIENTE
(DT_LAUDO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I10 ON TASY.LAUDO_PACIENTE
(NR_ATENDIMENTO, NR_PRESCRICAO, DT_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I11 ON TASY.LAUDO_PACIENTE
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO, NR_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I12 ON TASY.LAUDO_PACIENTE
(IE_URGENTE, NR_ATENDIMENTO, NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I2 ON TASY.LAUDO_PACIENTE
(DT_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          21M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_I2
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_I3 ON TASY.LAUDO_PACIENTE
(DT_LAUDO, DT_INTEGRACAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          29M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_I3
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_I4 ON TASY.LAUDO_PACIENTE
(NR_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I5 ON TASY.LAUDO_PACIENTE
(NR_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I6 ON TASY.LAUDO_PACIENTE
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          240K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_I6
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_I7 ON TASY.LAUDO_PACIENTE
(DT_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_I8 ON TASY.LAUDO_PACIENTE
(CD_LAUDO_EXTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_I8
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_LAPAMOC_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_LAPAMOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_LAUMEDG_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_GRUPO_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          472K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_LAUMEDG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_LDPMDES_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_MOTIVO_DESAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_MEDICO_FK_I ON TASY.LAUDO_PACIENTE
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_MEDICO_FK2_I ON TASY.LAUDO_PACIENTE
(CD_MEDICO_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_MOTLAPA_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_MOTIVO_PARADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_MOTLAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_PESFISI_FK_I ON TASY.LAUDO_PACIENTE
(CD_TECNICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PESFISI_FK2_I ON TASY.LAUDO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PESFISI_FK3_I ON TASY.LAUDO_PACIENTE
(CD_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PESFISI_FK4_I ON TASY.LAUDO_PACIENTE
(CD_RESIDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PESFISI_FK5_I ON TASY.LAUDO_PACIENTE
(CD_RESP_SEG_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAUPACI_PK ON TASY.LAUDO_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PRESMED_FK_I ON TASY.LAUDO_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PROESTU_FK_I ON TASY.LAUDO_PACIENTE
(CD_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_PROESTU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_PROPACI_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_PROTOCO_FK_I ON TASY.LAUDO_PACIENTE
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_RESPAPA_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_RESULTADO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_RESPAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_SETATEN_FK_I ON TASY.LAUDO_PACIENTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_SETATEN_FK2_I ON TASY.LAUDO_PACIENTE
(CD_SETOR_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_SETATEN_FK3_I ON TASY.LAUDO_PACIENTE
(CD_SETOR_EXECUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPACI_TASASDI_FK_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          472K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUPACI_TASASDI_FK2_I ON TASY.LAUDO_PACIENTE
(NR_SEQ_SEG_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_insert_hl7
after insert ON TASY.LAUDO_PACIENTE for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

cd_pessoa_fisica_w	varchar2(60);
nr_seq_interno_w	number(10);
nr_seq_superior_w	number(10);
nr_seq_prescricao_w	number(10);
nr_seq_proc_interno_w	number(10);


ie_status_result_w	varchar2(2);
ie_permite_proc_mdc_w	varchar2(2);
ie_permite_proc_isite_w varchar2(2);

begin

if 	(:new.nr_prescricao is not null) then

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao 	= :new.nr_prescricao;

	select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	max(nr_sequencia_prescricao),
		max(nr_seq_proc_interno)
	into	nr_seq_prescricao_w,
		nr_seq_proc_interno_w
	from	procedimento_paciente
	where	nr_sequencia	= :new.nr_seq_proc;

	select 	max(permite_proc_interno_integ(x.nr_seq_proc_interno,11))
	into	ie_permite_proc_mdc_w
	from 	procedimento_paciente x
	where	x.nr_prescricao = :new.nr_prescricao
	and	x.nr_sequencia_prescricao = nr_seq_prescricao_w;

	/*
	select	obter_integracao_proc_interno(null, null, nr_seq_proc_interno_w, 2)
	into	ie_permite_proc_isite_w
	from	dual;
	*/

	select	decode(count(*),0,'N','S')
	into	ie_permite_proc_isite_w
	from	regra_proc_interno_integra
	where	nr_seq_proc_interno	= nr_seq_proc_interno_w
	and	ie_tipo_integracao	= 2
	and	cd_integracao 		is not null;


	ds_sep_bv_w := obter_separador_bv;

	if	(:new.nr_seq_superior is null) then
		ie_status_result_w := 'P';
	else
		ie_status_result_w := 'C';
	end if;

	if	(ie_permite_proc_mdc_w = 'S') then --or (ie_permite_proc_isite_w = 'S') then --OS481347 09/08/2012 tbschulz - removido pois HSVP informou que ao salvar n�o deve ser enviado, somente na primeira aprova��o

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w  || ds_sep_bv_w ||
					'nr_atendimento='   || :new.nr_atendimento || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w    || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao  || ds_sep_bv_w ||
					'nr_seq_prescr='    || nr_seq_prescricao_w || ds_sep_bv_w ||
					'nr_seq_laudo='     || :new.nr_sequencia   || ds_sep_bv_w ||
					'ie_status_result=' || ie_status_result_w  || ds_sep_bv_w;

		gravar_agend_integracao(24, ds_param_integ_hl7_w);

	end if;


end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_update_hl7
after update ON TASY.LAUDO_PACIENTE for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
cd_pessoa_fisica_w	varchar2(60);
nr_seq_interno_w	number(10);
nr_seq_prescricao_w	number(10);
cd_estabelecimento_w	number(10);
ie_forma_aprovar_w	varchar2(10);
ie_permite_proc_isite_w varchar2(2);
ie_permite_proc_pacs   	varchar2(2);
nr_seq_proc_interno_w	number(10);
ie_status_result_w	varchar2(2) := 'F';
nr_prescricao_w		number(10);
ie_envia_vinculados_w	varchar2(1) := 'N';



Cursor C01 is -- OS 669701 / tratar o envio do laudo para os procedimentos vinculados
SELECT A.NR_PRESCRICAO, A.NR_SEQUENCIA_PRESCRICAO
  FROM PROCEDIMENTO_PACIENTE A
 WHERE A.NR_LAUDO = :new.nr_sequencia
   AND A.NR_SEQUENCIA <>
       (SELECT MAX(B.NR_SEQUENCIA)
          FROM PROCEDIMENTO_PACIENTE B
         WHERE A.NR_PRESCRICAO = B.NR_PRESCRICAO
           AND A.NR_SEQUENCIA_PRESCRICAO = B.NR_SEQUENCIA_PRESCRICAO)
 ORDER BY A.NR_SEQUENCIA;


begin

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

ie_forma_aprovar_w := nvl(Obter_Valor_Param_Usuario(28, 47, Obter_Perfil_Ativo, :new.nm_usuario, cd_estabelecimento_w),'N');

if	((ie_forma_aprovar_w = 'A') and (:old.dt_aprovacao is null) and (:new.dt_aprovacao is not null)) or
	((ie_forma_aprovar_w = 'L') and (:old.dt_liberacao is null) and (:new.dt_liberacao is not null)) or
	((ie_forma_aprovar_w = 'S') and (((:old.dt_seg_aprovacao is null) and (:new.dt_seg_aprovacao is not null)) or
											((:old.dt_aprovacao is null) and (:new.dt_aprovacao is not null) and (:new.dt_seg_aprovacao is null)))) then

	if (ie_forma_aprovar_w = 'S')  and ((:old.dt_aprovacao is null) and (:new.dt_aprovacao is not null) and (:new.dt_seg_aprovacao is null)) then
		if	(:new.nr_seq_superior is null) then
			ie_status_result_w := 'P';
		else
			ie_status_result_w := 'C';
		end if;
	end if;


	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao 	= :new.nr_prescricao;

	select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	max(nr_sequencia_prescricao)
	into	nr_seq_prescricao_w
	from	procedimento_paciente
	where	nr_sequencia	= :new.nr_seq_proc;

	select	max(x.nr_seq_proc_interno)
	into	nr_seq_proc_interno_w
	from 	prescr_procedimento x
	where	x.nr_prescricao = :new.nr_prescricao
	and	x.nr_sequencia 	= nr_seq_prescricao_w;

	select	decode(count(*),0,'N','S')
	into	ie_permite_proc_pacs
	from	proc_interno_integracao a
	where	a.nr_seq_proc_interno = nr_seq_proc_interno_w
	and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
	and	a.nr_seq_sistema_integ in (11,66);

	select	decode(count(*),0,'N','S')
	into	ie_permite_proc_isite_w
	from	regra_proc_interno_integra
	where	nr_seq_proc_interno	= nr_seq_proc_interno_w
	and	ie_tipo_integracao	= 2
	and	cd_integracao 		is not null;

	if	((ie_permite_proc_pacs = 'S') or ((ie_permite_proc_isite_w = 'S') and (ie_status_result_w = 'F'))) then

		ds_sep_bv_w := obter_separador_bv;

		ie_envia_vinculados_w	:=	'S';

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w  || ds_sep_bv_w ||
					'nr_atendimento='   || :new.nr_atendimento || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w    || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao  || ds_sep_bv_w ||
					'nr_seq_prescr='    || nr_seq_prescricao_w || ds_sep_bv_w ||
					'nr_seq_laudo='     || :new.nr_sequencia   || ds_sep_bv_w ||
					'ie_status_result=' || ie_status_result_w  || ds_sep_bv_w;


		gravar_agend_integracao(24, ds_param_integ_hl7_w);

	end if;

	if	(ie_envia_vinculados_w = 'S') then
		begin
		open C01;
		loop
		fetch C01 into
			nr_prescricao_w,
			nr_seq_prescricao_w;
		exit when C01%notfound;
			begin

			select	max(x.nr_seq_proc_interno)
			into	nr_seq_proc_interno_w
			from 	prescr_procedimento x
			where	x.nr_prescricao = nr_prescricao_w
			and	x.nr_sequencia 	= nr_seq_prescricao_w;

			select	decode(count(*),0,'N','S')
			into	ie_permite_proc_pacs
			from	proc_interno_integracao a
			where	a.nr_seq_proc_interno = nr_seq_proc_interno_w
			and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
			and	a.nr_seq_sistema_integ in (11,66);

			select	decode(count(*),0,'N','S')
			into	ie_permite_proc_isite_w
			from	regra_proc_interno_integra
			where	nr_seq_proc_interno	= nr_seq_proc_interno_w
			and	ie_tipo_integracao	= 2
			and	cd_integracao 		is not null;

			if	((ie_permite_proc_pacs = 'S') or (ie_permite_proc_isite_w = 'S') and (:new.dt_liberacao is not null)) then

				ds_sep_bv_w := obter_separador_bv;


				ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w  || ds_sep_bv_w ||
							'nr_atendimento='   || :new.nr_atendimento || ds_sep_bv_w ||
							'nr_seq_interno='   || nr_seq_interno_w    || ds_sep_bv_w ||
							'nr_prescricao='    || nr_prescricao_w     || ds_sep_bv_w ||
							'nr_seq_prescr='    || nr_seq_prescricao_w || ds_sep_bv_w ||
							'nr_seq_laudo='     || :new.nr_sequencia   || ds_sep_bv_w ||
							'ie_status_result=' || ie_status_result_w  || ds_sep_bv_w;


				gravar_agend_integracao(24, ds_param_integ_hl7_w);

			end if;


			end;
		end loop;
		close C01;
		end;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Laudo_Paciente_Delete
BEFORE DELETE ON TASY.LAUDO_PACIENTE FOR EACH ROW
DECLARE
qt_ditado_w	Number(10);
ie_status_w	Varchar2(10)	:= '20';

BEGIN

select	count(*)
into	qt_ditado_w
from	prescr_proc_ditado b,
	prescr_procedimento a
where	a.nr_seq_interno	= b.nr_seq_prescr_proc
and	a.nr_sequencia  in (	Select	b.nr_sequencia_prescricao
				from	procedimento_paciente b
				where	b.nr_prescricao = a.nr_prescricao
				and	b.nr_sequencia_prescricao = a.nr_sequencia
				and	b.nr_laudo = :old.nr_sequencia);

if	(qt_ditado_w > 0) then
	ie_status_w	:= '25';
end if;

Update	prescr_procedimento a
set	a.ie_status_execucao = ie_status_w,
	a.nm_usuario	= nvl(wheb_usuario_pck.get_nm_usuario, a.nm_usuario)
where	a.nr_prescricao = :old.nr_prescricao
and	a.nr_sequencia  in (	Select	b.nr_sequencia_prescricao
				from	procedimento_paciente b
				where	b.nr_prescricao = a.nr_prescricao
				and	b.nr_sequencia_prescricao = a.nr_sequencia
				and	b.nr_laudo = :old.nr_sequencia);

update	procedimento_paciente
set	nr_laudo = null,
	dt_vinc_proced_adic = null
where 	nr_laudo = :old.nr_sequencia;

END;
/


CREATE OR REPLACE TRIGGER TASY.LAUDO_PACIENTE_tp  after update ON TASY.LAUDO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_LAUDO,1,500);gravar_log_alteracao(substr(:old.NR_LAUDO,1,4000),substr(:new.NR_LAUDO,1,4000),:new.nm_usuario,nr_seq_w,'NR_LAUDO',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_PRESCRICAO,1,500);gravar_log_alteracao(substr(:old.NR_PRESCRICAO,1,4000),substr(:new.NR_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRESCRICAO',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_SEG_APROV,1,4000),substr(:new.NM_USUARIO_SEG_APROV,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_SEG_APROV',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_APROVACAO,1,4000),substr(:new.NM_USUARIO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_APROVACAO',ie_log_w,ds_w,'LAUDO_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_insert
BEFORE INSERT ON TASY.LAUDO_PACIENTE FOR EACH ROW
declare
nr_seq_status_pato_w	number(10);
nr_seq_proc_w number(15);
cd_pessoa_fisica_w	varchar2(10);
ie_vincula_proced_w varchar2(1);
ie_modifica_laudo_liberado_w varchar2(1);
qt_reg_w    			  NUMBER(1);
BEGIN

/*if	(:new.dt_cancelamento is null) and
	(:new.dt_liberacao is not null) and
	(:new.nr_prescricao > 0) and
	(:new.nr_seq_prescricao > 0) then
	Update	prescr_procedimento a
	set	a.ie_status_execucao = '40'
	where	a.nr_prescricao = :new.nr_prescricao
	and	a.nr_sequencia	= :new.nr_seq_prescricao;
end if;*/
IF (wheb_usuario_pck.get_ie_executar_trigger  = 'N') THEN
   GOTO Final;
END IF;

if((:new.nr_seq_proc is null)
	and (:new.nr_prescricao is not null)
	and (:new.nr_seq_prescricao is not null)
	and (wheb_usuario_pck.get_cd_funcao = 945)) then

	select nvl(max(nr_sequencia),0)
	into nr_seq_proc_w
	from procedimento_paciente
	where nr_prescricao = :new.nr_prescricao
	and   nr_sequencia_prescricao = :new.nr_seq_prescricao;

	if( nr_seq_proc_w > 0) then
		:new.nr_seq_proc := nr_seq_proc_w;
	end if;

end if;

if 	(:new.cd_pessoa_fisica is null) and
	(:new.nr_atendimento is not null) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	:new.cd_pessoa_fisica := cd_pessoa_fisica_w;

end if;

if 	(:new.nr_seq_superior is null) and --Laudo em digitacao
	(:new.dt_aprovacao is null) and
	(:new.dt_seg_aprovacao is null) and
	(:new.dt_liberacao is null) and
	(:new.dt_cancelamento is null) then

	if	(:new.ie_status_laudo <> 'LM')  or (:new.ie_status_laudo is null) then
		:new.ie_status_laudo := 'LD';
	end if;

elsif   (:new.nr_seq_superior is null) and --Laudo aprovacao parcial
	(:new.dt_aprovacao is not null) and
	(:new.dt_seg_aprovacao is null) and
	(:new.dt_liberacao is null) and
	(:new.dt_cancelamento is null) then

	:new.ie_status_laudo := 'LAP';

elsif	(:new.nr_seq_superior is null) and --Laudo liberado
	(:new.dt_liberacao is not null) and
	(:new.dt_cancelamento is null) then



select 	Max(nvl(ie_modifica_laudo_liberado,'N'))
	into	ie_modifica_laudo_liberado_w
	from	prescr_medica a,
			parametro_integracao_pacs b
	where   a.cd_estabelecimento = b.cd_estabelecimento
	and		a.nr_prescricao = :new.nr_prescricao;

  if (ie_modifica_laudo_liberado_w = 'N') then
    :new.ie_status_laudo := 'LL';
  end if;

elsif 	(:new.nr_seq_superior is null) and --Laudo cancelado
	(:new.dt_cancelamento is not null) then

	:new.ie_status_laudo := 'LC';

elsif 	(:new.nr_seq_superior is not null) and --Adendo em digitacao
	(:new.dt_aprovacao is null) and
	(:new.dt_seg_aprovacao is null) and
	(:new.dt_liberacao is null) and
	(:new.dt_cancelamento is null) then

	:new.ie_status_laudo := 'AD';

elsif	(:new.nr_seq_superior is not null) and --Adendo aprovacao parcial
	(:new.dt_aprovacao is not null) and
	(:new.dt_seg_aprovacao is null) and
	(:new.dt_liberacao is null) and
	(:new.dt_cancelamento is null) then

	:new.ie_status_laudo := 'AAP';

elsif	(:new.nr_seq_superior is not null) and --Adendo liberado
	(:new.dt_liberacao is not null) and
	(:new.dt_cancelamento is null) then

	:new.ie_status_laudo := 'AL';

elsif	(:new.nr_seq_superior is not null) and --Andendo cancelado
	(:new.dt_cancelamento is not null) then

	:new.ie_status_laudo := 'AC';

end if;

/*select 	max(nr_sequencia)
into	nr_seq_status_pato_w
from	PROCED_PATOLOGIA_STATUS
where	IE_STATUS_PATOLOGIA = 'LP';

if (:new.nr_prescricao is not null) and (:new.nr_seq_prescricao is not null) then
	if (nr_seq_status_pato_w is not null) then
		update 	prescr_procedimento
		set	nr_seq_status_pato = nr_seq_status_pato_w
		where	nr_prescricao = :new.nr_prescricao
		and	nr_sequencia = :new.nr_seq_prescricao;
	end if;
end if; */
<<Final>>
qt_reg_w :=0;
END;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_insert_titulo
before insert or update ON TASY.LAUDO_PACIENTE for each row
declare
   pragma	autonomous_transaction;
   ds_titulo_laudo_w	laudo_paciente.ds_titulo_laudo%type;
begin

if((:new.nr_seq_proc is not null)
	and (:new.ds_titulo_laudo is not null)
	and (obter_se_somente_numero(:new.ds_titulo_laudo) = 'S')) then


	select	substr(obter_desc_prescr_proc_laudo(p.cd_procedimento, p.ie_origem_proced, p.nr_seq_proc_interno, p.ie_lado, a.nr_sequencia),1,255)
	into	ds_titulo_laudo_w
	from	procedimento_paciente a,
		prescr_procedimento p
	where	a.nr_sequencia	= :new.nr_seq_proc
	and	a.nr_prescricao	= p.nr_prescricao
	and  p.nr_sequencia  = a.nr_sequencia_prescricao;

  :new.ds_titulo_laudo := ds_titulo_laudo_w;

  commit;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.LAUDO_PACIENTE_UPDATE
BEFORE UPDATE ON TASY.LAUDO_PACIENTE FOR EACH ROW
DECLARE
nr_seq_prescricao_w  	  NUMBER(6);
total_w                 NUMBER(10);
acao_w                  varchar2(11);
total_canc_w            number(10);
ie_status_exec_w  		  VARCHAR2(3);
ie_adic_pasta_medico_w    VARCHAR2(1);
ie_medico_w    		 	  VARCHAR2(1);
cd_medico_w    			  VARCHAR2(10);
ie_status_execucao_w  	  VARCHAR2(3);
ie_funcao_medico_w  	  VARCHAR2(3);
qt_reg_w    			  NUMBER(1);
nr_seq_status_pato_w  	  NUMBER(10);
nm_usuario_liberacao_w    VARCHAR2(15);
ie_medico_ja_cadastrado_w VARCHAR2(1);
nr_seq_evento_w    		  NUMBER(10);
cd_pessoa_fisica_w  	  VARCHAR2(10);

qt_laudos_w    INTEGER(10);
qt_registros_w INTEGER(10);

nr_seq_regra_dupla_chec_w NUMBER(10);

ie_prelaudo_w      VARCHAR2(1);
ie_status_preLaudo VARCHAR2(3);
ie_encaminhado_w   VARCHAR2(1);
ie_modifica_laudo_liberado_w varchar(1);

cd_procedimento_w           NUMBER(15);
cd_tipo_procedimento_w      NUMBER(3);
ie_origem_proced_w          NUMBER(10);
nr_seq_proc_interno_w       NUMBER(15);
cd_setor_atendimento_w      NUMBER(5);
ie_existe_regra_conferencia NUMBER(4);
ie_status_aprov             NUMBER(3);

qt_regra_wl_exame_w			wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_w				wl_regra_item.nr_sequencia%type;
qt_horas_marcar_ciente_w	number(10);
nr_seq_tipo_adm_fat_atd_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w			atendimento_paciente.nr_seq_episodio%type;
ie_gerar_pendencia_tl_w		subtipo_episodio.ie_gerar_pendencia%type;
cd_estabelecimento_w            prescr_medica.nr_prescricao%type;
count_w         number(5);


CURSOR c01 IS
  SELECT a.nr_seq_evento
  FROM   regra_envio_sms a
  WHERE  a.ie_evento_disp = 'LLREP'
  AND    NVL(a.ie_situacao,'A') = 'A';

CURSOR c02 IS
  SELECT a.nr_seq_evento
  FROM   regra_envio_sms a
  WHERE  a.ie_evento_disp = 'DCDL'
  AND    NVL(a.ie_situacao,'A') = 'A';

CURSOR c03 IS
  SELECT a.nr_seq_evento
  FROM   regra_envio_sms a
  WHERE  a.ie_evento_disp = 'AELP'
  AND    NVL(a.ie_situacao,'A') = 'A';

cursor c04 is
	select	nvl(b.qt_tempo_normal, 0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'EN'
								and		x.ie_situacao = 'A');

BEGIN
  IF (wheb_usuario_pck.get_ie_executar_trigger  = 'N') THEN
    GOTO Final;
  END IF;

  select  count(1)
          into  count_w
          from  intpd_eventos a,
              intpd_eventos_sistema b
          where a.nr_sequencia = b.nr_seq_evento
          and   a.ie_evento = 162
          and   a.ie_situacao = 'A'
          and   b.ie_situacao = 'A'
          and   rownum = 1;
      if (count_w>0) then

         select cd_estabelecimento
         into cd_estabelecimento_w
         from prescr_medica pm
         where pm.nr_prescricao = :new.nr_prescricao;

        wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_w);
      end if;


  SELECT NVL(obter_regra_laudo_dupla_chec(wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_cd_perfil), 0)
  INTO   nr_seq_regra_dupla_chec_w
  FROM   dual;

  IF (nr_seq_regra_dupla_chec_w > 0) THEN
    SELECT NVL(MAX(qt_laudos),0),
		   NVL(MAX(qt_registros),0)
    INTO   qt_laudos_w,
           qt_registros_w
    FROM   regra_laudo_dupla_chec
    WHERE  nr_sequencia = nr_seq_regra_dupla_chec_w;

    IF (qt_laudos_w > 0) AND
       (:OLD.dt_aprovacao IS NULL) AND
       (:NEW.dt_aprovacao IS NOT NULL) THEN
      IF (qt_laudos_w <=  (qt_registros_w + 1)) THEN
        UPDATE regra_laudo_dupla_chec
        SET    qt_registros =  0,
               dt_ultimo_envio = SYSDATE
        WHERE  nr_sequencia = nr_seq_regra_dupla_chec_w;

        :NEW.dt_liberacao := NULL;
        :NEW.nm_usuario_liberacao := NULL;
        :NEW.ie_exige_seg_aprov := 'S';

        OPEN C02;
        LOOP
        FETCH C02 INTO
          nr_seq_evento_w;
        EXIT WHEN C02%NOTFOUND;
          BEGIN
            gerar_evento_laudo_lab(nr_seq_evento_w, :NEW.nm_usuario, :NEW.nr_prescricao, NULL, NULL, 'S', :NEW.ds_titulo_laudo, 'L');
          END;
        END LOOP;
        CLOSE C02;
	  ELSE
        UPDATE regra_laudo_dupla_chec
        SET    qt_registros = qt_registros_w + 1
        WHERE  nr_sequencia = nr_seq_regra_dupla_chec_w;
      END IF;
    END IF;
  END IF;

  IF (:OLD.cd_pessoa_fisica IS NULL) AND
     (:NEW.cd_pessoa_fisica IS NULL) AND
     (:NEW.nr_atendimento IS NOT NULL) THEN
    SELECT MAX(cd_pessoa_fisica)
    INTO   cd_pessoa_fisica_w
    FROM   atendimento_paciente
    WHERE  nr_atendimento = :NEW.nr_atendimento;

	:NEW.cd_pessoa_fisica := cd_pessoa_fisica_w;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_fim_digitacao IS NULL) AND
     ((:NEW.dt_liberacao IS NOT NULL) or (:NEW.dt_aprovacao IS NOT NULL) OR (:NEW.dt_seg_aprovacao IS NOT NULL)) THEN
    :NEW.dt_fim_digitacao  := SYSDATE;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NULL) AND
     (:NEW.dt_aprovacao IS NULL) AND
     (:NEW.dt_fim_digitacao IS NULL) THEN
    UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = '26',
           a.nm_usuario = :NEW.nm_usuario
    WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia)
	AND ((a.ie_status_execucao = '30') OR
		 (a.ie_status_execucao = '36') OR
         (a.ie_status_execucao = '40'));
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NULL) AND
     (:NEW.dt_aprovacao IS NULL) AND
     (:NEW.dt_seg_aprovacao IS NULL) AND
     (:NEW.dt_envelopado IS NULL) AND
     (:NEW.dt_fim_digitacao IS NOT NULL) THEN
    SELECT NVL(MAX(ie_prelaudo), 'N')
    INTO   ie_prelaudo_w
    FROM   parametro_cdi
    WHERE  cd_estabelecimento = obter_estabelecimento_ativo;

    IF (ie_prelaudo_w = 'N') THEN
	  ie_status_preLaudo := '30';
    ELSE
	  SELECT MAX(ie_status_execucao)
      INTO   ie_status_execucao_w
      FROM   prescr_procedimento a
      WHERE  a.nr_prescricao = :NEW.nr_prescricao
      AND    a.nr_sequencia  = :NEW.nr_seq_prescricao;

	  SELECT NVL(MAX(ie_encaminhado), 'N')
	  INTO   ie_encaminhado_w
	  FROM   laudo_paciente_compl
	  WHERE  nr_seq_laudo = :new.nr_sequencia;

	  IF (ie_status_execucao_w = '26' AND ie_encaminhado_w = 'N') THEN
	    ie_status_preLaudo := '28';
	  ELSIF (ie_status_execucao_w = '26') THEN
	    ie_status_preLaudo := '26';
	  ELSE
	    ie_status_preLaudo := '30';
	  END IF;

	END IF;

    UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = ie_status_preLaudo,
           a.nm_usuario = :NEW.nm_usuario
    WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
                                FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia)
    AND ((a.ie_status_execucao = '26') OR
         (a.ie_status_execucao = '35') OR
		 (a.ie_status_execucao = '37') OR
		 (a.ie_status_execucao = '38') OR
         (a.ie_status_execucao = '40') OR
         (a.ie_status_execucao = '45'));


    SELECT MAX(nr_sequencia)
    INTO   nr_seq_status_pato_w
    FROM   proced_patologia_status
    WHERE  ie_status_patologia = 'LL';

    IF (:NEW.nr_prescricao IS NOT NULL) AND (:NEW.nr_seq_prescricao IS NOT NULL) THEN
      IF (nr_seq_status_pato_w IS NOT NULL) THEN
        UPDATE prescr_procedimento
        SET    nr_seq_status_pato = nr_seq_status_pato_w,
               nm_usuario = :NEW.nm_usuario
        WHERE  nr_prescricao = :NEW.nr_prescricao
        AND  nr_sequencia = :NEW.nr_seq_prescricao;
      END IF;
    END IF;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NULL) AND
     (:NEW.dt_aprovacao IS NOT NULL) THEN
    UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = '35',
		   a.nm_usuario = :NEW.nm_usuario
	WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia);
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:OLD.dt_liberacao IS NULL) THEN
	BEGIN
		IF (:NEW.nr_seq_prescricao IS NOT NULL) THEN
		BEGIN
			SELECT max(prescr_proc.cd_procedimento),
				 max(proc.cd_tipo_procedimento),
				 max(proc.ie_origem_proced),
				 max(prescr_proc.nr_seq_proc_interno),
				 max(prescr_proc.cd_setor_atendimento)
			INTO   cd_procedimento_w,
				   cd_tipo_procedimento_w,
				   ie_origem_proced_w,
				   nr_seq_proc_interno_w,
				   cd_setor_atendimento_w
			FROM   prescr_procedimento prescr_proc,
				   procedimento proc
			WHERE  prescr_proc.cd_procedimento = proc.cd_procedimento
			AND    prescr_proc.ie_origem_proced = proc.ie_origem_proced
			AND    prescr_proc.nr_prescricao = :NEW.nr_prescricao
			AND    prescr_proc.nr_sequencia = :NEW.nr_seq_prescricao;


			SELECT NVL(MAX(1), 0)
			INTO   ie_existe_regra_conferencia
			FROM   regra_conferencia_laudo
			WHERE  NVl(cd_estabelecimento, obter_estabelecimento_ativo) = obter_estabelecimento_ativo
			AND    NVL(cd_procedimento, NVL(cd_procedimento_w, 0)) = NVL(cd_procedimento_w, 0)
			AND    NVL(cd_tipo_procedimento, NVL(cd_tipo_procedimento_w, 0)) = NVL(cd_tipo_procedimento_w, 0)
			AND    NVL(ie_origem_proced, NVL(ie_origem_proced_w, 0)) = NVL(ie_origem_proced_w, 0)
			AND    NVL(nr_seq_proc_interno, NVL(nr_seq_proc_interno_w, 0)) = NVL(nr_seq_proc_interno_w, 0)
			AND    NVL(cd_setor_atendimento, NVl(cd_setor_atendimento_w, 0)) = NVL(cd_setor_atendimento_w, 0)
			AND    NVL(ie_ativo, 'N') = 'S';

			if (:new.nr_atendimento is not null) then

				select	max(nr_seq_tipo_admissao_fat),
						max(ie_tipo_atendimento),
						max(nr_seq_episodio)
				into	nr_seq_tipo_adm_fat_atd_w,
						ie_tipo_atendimento_w,
						nr_seq_episodio_w
				from	atendimento_paciente
				where	nr_atendimento = :new.nr_atendimento;

			end if;

			open c04;
			loop
			fetch c04 into
			qt_regra_wl_exame_w,
			nr_seq_regra_w;
			exit when c04%notfound;
				begin
					if	(qt_regra_wl_exame_w > 0 and obter_se_regra_geracao(nr_seq_regra_w,nr_seq_episodio_w,nr_seq_tipo_adm_fat_atd_w) = 'S') then

						-- Gera tarefa na funcao Lista de Tarefas (Task List) como pendencia para o usuario informar que estq ciente do exame
						wl_gerar_finalizar_tarefa('EN','I',:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nm_usuario,
									nvl(:new.dt_liberacao,sysdate)+(qt_regra_wl_exame_w/24),
									'N',null,null,null,:new.nr_prescricao,:new.nr_seq_prescricao,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_liberacao,nr_seq_episodio_w);
					end if;
				end;
			end loop;
				close c04;
		END;
		ELSE
		ie_existe_regra_conferencia := 0;
		END IF;

		IF ie_existe_regra_conferencia > 0 THEN
		ie_status_aprov := '37';
		ELSE
		ie_status_aprov := '40';
		END IF;

		UPDATE prescr_procedimento a
		SET    a.ie_status_execucao = ie_status_aprov,
			 a.nm_usuario = :NEW.nm_usuario
		WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								  FROM   prescr_procedimento b,
										 procedimento_paciente c
								  WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								  AND  b.nr_prescricao  = c.nr_prescricao
								  AND  c.nr_laudo       = :NEW.nr_sequencia);



		SELECT MAX(ie_status_execucao)
		INTO   ie_status_execucao_w
		FROM   prescr_procedimento a
		WHERE  a.nr_prescricao = :NEW.nr_prescricao
		AND    a.nr_sequencia  = :NEW.nr_seq_prescricao;

		IF (ie_status_execucao_w <> ie_status_aprov) THEN
		UPDATE prescr_procedimento a
		SET    a.ie_status_execucao = ie_status_aprov,
			   a.nm_usuario = :NEW.nm_usuario
		WHERE  a.nr_prescricao = :NEW.nr_prescricao
		AND    a.nr_sequencia IN (SELECT b.nr_sequencia_prescricao
								  FROM   procedimento_paciente b
								  WHERE  b.nr_prescricao = a.nr_prescricao
								  AND    b.nr_prescricao = :NEW.nr_prescricao
								  AND    b.nr_sequencia_prescricao = a.nr_sequencia
								  AND    b.nr_sequencia_prescricao = :NEW.nr_seq_prescricao);
		END IF;


		IF (:NEW.ie_gerar_comunic = 'S') THEN
		gerar_alerta_laudo_lib(:NEW.nr_sequencia, :NEW.nr_atendimento, :NEW.nm_usuario, 'N');
		END IF;

    END;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:NEW.dt_envelopado IS NULL) AND
     (:OLD.dt_envelopado IS NOT NULL) THEN
    IF (:NEW.dt_conferencia IS NULL) THEN
      UPDATE prescr_procedimento a
      SET    a.ie_status_execucao = '40',
             a.nm_usuario = :NEW.nm_usuario
      WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								  FROM   prescr_procedimento b,
										 procedimento_paciente c
								  WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								  AND    b.nr_prescricao = c.nr_prescricao
								  AND    c.nr_laudo = :NEW.nr_sequencia);
    ELSE
      UPDATE prescr_procedimento a
      SET    a.ie_status_execucao = '43',
			 a.nm_usuario = :NEW.nm_usuario
	  WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								  FROM   prescr_procedimento b,
										 procedimento_paciente c
								  WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								  AND  b.nr_prescricao = c.nr_prescricao
								  AND  c.nr_laudo = :NEW.nr_sequencia);
	END IF;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:OLD.dt_conferencia IS NOT NULL) AND
     (:NEW.dt_conferencia IS NULL) THEN
	UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = '40',
           a.nm_usuario = :NEW.nm_usuario
    WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND  b.nr_prescricao = c.nr_prescricao
								AND  c.nr_laudo = :NEW.nr_sequencia);
  END IF;

  IF (:NEW.dt_conferencia IS NOT NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:OLD.dt_liberacao IS NULL) THEN
    :NEW.dt_conferencia := NULL;
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:NEW.dt_conferencia IS NOT NULL) AND
     (:OLD.dt_conferencia IS NULL) THEN
    UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = '43',
           a.nm_usuario = obter_usuario_ativo
    WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
                                FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia);
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND
     (:NEW.dt_liberacao IS NOT NULL) AND
     (:NEW.dt_envelopado IS NOT NULL) AND
     (:OLD.dt_envelopado IS NULL) THEN
    OPEN C03;
    LOOP
    FETCH C03 INTO
      nr_seq_evento_w;
    EXIT WHEN C03%NOTFOUND;
      BEGIN
        gerar_evento_laudo_lab(nr_seq_evento_w, :NEW.nm_usuario, :NEW.nr_prescricao, NULL, NULL, 'S', :NEW.ds_titulo_laudo, 'L');
      END;
    END LOOP;
    CLOSE C03;

    UPDATE prescr_procedimento a
    SET    a.ie_status_execucao = '45',
           a.nm_usuario = :NEW.nm_usuario
	WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia);
  END IF;

  IF (:NEW.dt_cancelamento IS NULL) AND /* rafael em 23/07/2007 OS61842 */
     (:NEW.dt_liberacao IS NOT NULL) THEN
    SELECT NVL(MAX(nr_sequencia_prescricao),0)
    INTO   nr_seq_prescricao_w
    FROM   procedimento_paciente
    WHERE  nr_prescricao = :NEW.nr_prescricao
    AND    nr_laudo = :NEW.nr_sequencia;

    IF (nr_seq_prescricao_w > 0) THEN
      Gerar_aviso_result_prescr(:NEW.nr_prescricao, nr_seq_prescricao_w, NULL, NULL, NULL);

	  OPEN c01;
      LOOP
      FETCH c01 INTO
        nr_seq_evento_w;
      EXIT WHEN c01%NOTFOUND;
        gerar_evento_laudo_lab(nr_seq_evento_w,:NEW.nm_usuario,:NEW.nr_prescricao,NULL,NULL,'S',:NEW.ds_titulo_laudo,'L');
      END LOOP;
      CLOSE	c01;
    END IF;
  END IF;

  IF (:OLD.dt_cancelamento IS NULL) AND
     (:NEW.dt_cancelamento IS NOT NULL) THEN
    /* obter status execucao */
    SELECT DECODE(COUNT(*),0,'20','25')
    INTO   ie_status_exec_w
    FROM   prescr_proc_ditado b,
		   prescr_procedimento a
	WHERE  b.nr_seq_prescr_proc = a.nr_seq_interno
	AND    a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia);

	/* atualizar status execucao */
	UPDATE prescr_procedimento a
	SET    a.ie_status_execucao = ie_status_exec_w,
		   a.nm_usuario = :NEW.nm_usuario
	WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
								FROM   prescr_procedimento b,
									   procedimento_paciente c
								WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
								AND    b.nr_prescricao = c.nr_prescricao
								AND    c.nr_laudo = :NEW.nr_sequencia);

	/* atualizar vinculo procedimento */
	UPDATE procedimento_paciente
	SET    nr_laudo = NULL
	WHERE  nr_laudo = :NEW.nr_sequencia;
  END IF;

  IF (:OLD.dt_cancelamento IS NOT NULL) AND
     (:NEW.dt_cancelamento IS NULL) THEN
  /* obter status execucao */
    IF (:NEW.nr_seq_proc IS NOT NULL) THEN
      BEGIN
        SELECT MAX(ie_status_execucao)
        INTO   ie_status_execucao_w
        FROM   prescr_procedimento a
        WHERE  a.nr_prescricao = :NEW.nr_prescricao
        AND    a.nr_sequencia  = :NEW.nr_seq_prescricao;

        SELECT MAX(a.ie_status_exec)
        INTO   ie_status_exec_w
        FROM   prescr_proc_status a
        WHERE  nr_prescricao = :NEW.nr_prescricao
        AND    a.nr_seq_prescr IN (SELECT  b.nr_sequencia_prescricao
								   FROM  procedimento_paciente b
								   WHERE  b.nr_sequencia = :NEW.nr_seq_proc)
		AND  a.ie_status_exec <> ie_status_execucao_w
		AND  a.nr_seq_funcao = 28
		AND  a.dt_atualizacao_nrec  = (SELECT MAX(x.dt_atualizacao_nrec)
									   FROM   prescr_proc_status x
									   WHERE  x.nr_prescricao =  a.nr_prescricao
									   AND    a.nr_seq_funcao = 28
									   AND    x.nr_seq_prescr IN (SELECT g.nr_sequencia_prescricao
																  FROM   procedimento_paciente g
																  WHERE  g.nr_sequencia = :NEW.nr_seq_proc)
									   AND  x.ie_status_exec <> ie_status_execucao_w);
		/* atualizar status execucao */
	    UPDATE prescr_procedimento a
        SET    a.ie_status_execucao = ie_status_exec_w,
			   a.nm_usuario = :NEW.nm_usuario
		WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
									FROM   prescr_procedimento b,
										   procedimento_paciente c
									WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
									AND    b.nr_prescricao = c.nr_prescricao
									AND    c.nr_laudo = :NEW.nr_sequencia);

		/* atualizar vinculo procedimento */
		UPDATE procedimento_paciente
		SET    nr_laudo = :NEW.nr_sequencia
		WHERE  nr_sequencia = :NEW.nr_seq_proc;
	  END;
	END IF;
  END IF;

  IF (:OLD.nm_usuario_liberacao IS NULL) AND
     (:NEW.nm_usuario_liberacao IS NOT NULL) THEN
    nm_usuario_liberacao_w := :NEW.nm_usuario_liberacao;
  ELSIF (:OLD.nm_usuario_seg_aprov IS NULL) AND
        (:NEW.nm_usuario_seg_aprov IS NOT NULL) THEN
    nm_usuario_liberacao_w := :NEW.nm_usuario_aprovacao;
  END IF;

  /* almir em 01/07/2008 OS96910 */
  IF (nm_usuario_liberacao_w IS NOT NULL) THEN
    ie_adic_pasta_medico_w := NVL(obter_valor_param_usuario(28, 97, obter_perfil_ativo, nm_usuario_liberacao_w, 0),'N');
    ie_funcao_medico_w := NVL(obter_valor_param_usuario(28, 114, obter_perfil_ativo, nm_usuario_liberacao_w, 0),'0');

    IF (ie_adic_pasta_medico_w = 'S') THEN
      cd_medico_w := obter_pessoa_fisica_usuario(nm_usuario_liberacao_w,'C');

      BEGIN
        SELECT 'S'
        INTO   ie_medico_ja_cadastrado_w
        FROM   laudo_paciente_medico
        WHERE  cd_medico = cd_medico_w
        AND    nr_seq_laudo = :NEW.nr_sequencia;
        EXCEPTION
          WHEN OTHERS THEN
            ie_medico_ja_cadastrado_w := 'N';
      END;

      ie_medico_w := NVL(obter_se_medico(cd_medico_w,'M'),'N');

      IF (ie_medico_w = 'S') AND (ie_medico_ja_cadastrado_w = 'N') THEN
	    INSERT INTO laudo_paciente_medico (nr_sequencia,
										   cd_medico,
										   nr_seq_laudo,
										   dt_atualizacao,
										   nm_usuario,
										   dt_atualizacao_nrec,
										   nm_usuario_nrec,
										   ie_funcao_medico)
        VALUES(laudo_paciente_medico_seq.NEXTVAL,
			   cd_medico_w,
			   :NEW.nr_sequencia,
			   SYSDATE,
			   NVL(nm_usuario_liberacao_w, :NEW.nm_usuario),
			   SYSDATE,
			   NVL(nm_usuario_liberacao_w, :NEW.nm_usuario),
			   DECODE(ie_funcao_medico_w, '0', NULL, ie_funcao_medico_w));
	  END IF;
	END IF;
  END IF;

  IF (:NEW.DT_SEG_APROVACAO IS NOT NULL) OR
     (:NEW.dt_liberacao IS NOT NULL) THEN
    SELECT MAX(nr_sequencia)
    INTO   nr_seq_status_pato_w
    FROM   PROCED_PATOLOGIA_STATUS
    WHERE  IE_STATUS_PATOLOGIA = 'LB';

    IF (:NEW.nr_prescricao IS NOT NULL) AND (:NEW.nr_seq_prescricao IS NOT NULL) THEN
      IF (nr_seq_status_pato_w IS NOT NULL) THEN
        UPDATE prescr_procedimento a
        SET    a.nr_seq_status_pato = nr_seq_status_pato_w,
			   a.nm_usuario = :NEW.nm_usuario
        WHERE  a.nr_seq_interno IN (SELECT b.nr_seq_interno
									FROM   prescr_procedimento b,
										   procedimento_paciente c
									WHERE  b.nr_sequencia = c.nr_sequencia_prescricao
									AND    b.nr_prescricao = c.nr_prescricao
									AND    c.nr_laudo = :NEW.nr_sequencia);
	  END IF;
	END IF;
  END IF;

  IF (:NEW.nr_seq_superior IS NULL) AND --Laudo em digitacao
     (:NEW.dt_aprovacao IS NULL) AND
     (:NEW.dt_seg_aprovacao IS NULL) AND
     (:NEW.dt_liberacao IS NULL) AND
     (:NEW.dt_cancelamento IS NULL) THEN
    IF (:NEW.ie_status_laudo <> 'LM') OR (:NEW.ie_status_laudo IS NULL) THEN
      :NEW.ie_status_laudo := 'LD';
    END IF;
  ELSIF (:NEW.nr_seq_superior IS NULL) AND --Laudo aprovacao parcial
        (:NEW.dt_aprovacao IS NOT NULL) AND
        (:NEW.dt_seg_aprovacao IS NULL) AND
        (:NEW.dt_liberacao IS NULL) AND
        (:NEW.dt_cancelamento IS NULL) THEN
    :NEW.ie_status_laudo := 'LAP';
  ELSIF (:NEW.nr_seq_superior IS NULL) AND --Laudo liberado
        (:NEW.dt_liberacao IS NOT NULL) AND
        (:NEW.dt_cancelamento IS NULL) THEN

    select nvl(max(ie_modifica_laudo_liberado),'N')
    into ie_modifica_laudo_liberado_w
    from PARAMETRO_INTEGRACAO_PACS
    where cd_estabelecimento = obter_estabelecimento_ativo;

    if (ie_modifica_laudo_liberado_w = 'N') then
      :NEW.ie_status_laudo := 'LL';
    end if;
  ELSIF (:NEW.nr_seq_superior IS NULL) AND --Laudo cancelado
        (:NEW.dt_cancelamento IS NOT NULL) THEN
    :NEW.ie_status_laudo := 'LC';
  ELSIF (:NEW.nr_seq_superior IS NOT NULL) AND --AdCLOSEo em digitacao
        (:NEW.dt_aprovacao IS NULL) AND
        (:NEW.dt_seg_aprovacao IS NULL) AND
        (:NEW.dt_liberacao IS NULL) AND
        (:NEW.dt_cancelamento IS NULL) THEN
    :NEW.ie_status_laudo := 'AD';
  ELSIF (:NEW.nr_seq_superior IS NOT NULL) AND --AdCLOSEo aprovacao parcial
        (:NEW.dt_aprovacao IS NOT NULL) AND
        (:NEW.dt_seg_aprovacao IS NULL) AND
        (:NEW.dt_liberacao IS NULL) AND
        (:NEW.dt_cancelamento IS NULL) THEN
    :NEW.ie_status_laudo := 'AAP';
  ELSIF (:NEW.nr_seq_superior IS NOT NULL) AND --AdCLOSEo liberado
        (:NEW.dt_liberacao IS NOT NULL) AND
        (:NEW.dt_cancelamento IS NULL) THEN
    :NEW.ie_status_laudo := 'AL';
  ELSIF (:NEW.nr_seq_superior IS NOT NULL) AND --ANDCLOSEo cancelado
        (:NEW.dt_cancelamento IS NOT NULL) THEN
    :NEW.ie_status_laudo := 'AC';
  END IF;

  if ((:OLD.dt_liberacao is not null) and
      (:NEW.dt_liberacao is null) and
      (wheb_usuario_pck.is_evento_ativo(766) = 'S')) then
    Acao_W := 'Exclusao';
    integrar_unimed_rs_ws(766, :new.nr_prescricao, :new.nr_seq_prescricao, :new.nm_usuario, acao_w);
  end if;

  IF (:NEW.dt_impressao IS NOT NULL) AND
     (:OLD.dt_impressao IS NULL) THEN
    BEGIN
      INSERT INTO log_impressao_laudo(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_laudo,
				ie_tipo_laudo,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_observacao)
	  VALUES (log_impressao_laudo_seq.NEXTVAL,
			  SYSDATE,
			  :NEW.nm_usuario,
			  :NEW.nr_sequencia,
			  'I',
			  SYSDATE,
			  :NEW.nm_usuario,
			  obter_desc_expressao(290509) || '= ' || obter_funcao_ativa || ' - ' || obter_desc_expressao(717491) || '= '  || obter_perfil_ativo);
    EXCEPTION
	  WHEN OTHERS THEN
        NULL;
	END;
  END IF;

    IF (:new.nr_exame <> :old.nr_exame)THEN
      UPDATE prescr_procedimento
      SET nr_seq_lab = :new.nr_exame
      WHERE nr_prescricao = :old.nr_prescricao AND
            nr_sequencia = :old.nr_seq_prescricao;
    END IF;

  <<Final>>
  qt_reg_w  := 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.LAUDO_PACIENTE_ATUAL
before insert or update ON TASY.LAUDO_PACIENTE for each row
declare


begin
if	(nvl(:old.DT_LAUDO,sysdate+10) <> :new.DT_LAUDO) and
	(:new.DT_LAUDO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_LAUDO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_laudo_pac_cancel_proc_upd
before update ON TASY.LAUDO_PACIENTE for each row
declare


begin
    
    if :new.dt_cancelamento is not null and :old.dt_cancelamento is null and :new.nr_seq_proc is not null then
    
        :new.nr_seq_proc:=null;
        
        insert into hsj_log_alteracao_via_trigger 
        (
            nm_trigger,
            nm_tabela,
            nm_usuario,
            dt_atualizacao,
            ds_alteracao,
            nr_atendimento,
            nm_seq_atributo,
            nr_seq_atributo
        )
        values
        (   
            'HSJ_LAUDO_PAC_CANCEL_PROC_UPD',
            'LAUDO_PACIENTE',
            nvl(obter_usuario_ativo, :new.nm_usuario),
            sysdate,
            ':NEW.NR_SEQ_PROC:=NULL',
            :new.nr_atendimento,
            'NR_SEQUENCIA',
            :new.nr_sequencia
        );
        
    end if;
    
end;
/


ALTER TABLE TASY.LAUDO_PACIENTE ADD (
  CONSTRAINT LAUPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          13M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE ADD (
  CONSTRAINT LAUPACI_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT LAUPACI_PROESTU_FK 
 FOREIGN KEY (CD_PROJETO) 
 REFERENCES TASY.PROJETO_ESTUDO (CD_PROJETO),
  CONSTRAINT LAUPACI_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT LAUPACI_MOTLAPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PARADA) 
 REFERENCES TASY.MOTIVO_LAUDO_PARADO (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LAUPACI_MEDICO_FK2 
 FOREIGN KEY (CD_MEDICO_AUX) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT LAUPACI_PESFISI_FK 
 FOREIGN KEY (CD_TECNICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_USUARIO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LAUPACI_IMPACPROT_FK 
 FOREIGN KEY (NR_SEQ_PAC_PROT_EXT) 
 REFERENCES TASY.IMAGEM_PAC_PROT_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_RESPAPA_FK 
 FOREIGN KEY (NR_SEQ_RESULTADO_PADRAO) 
 REFERENCES TASY.RESULTADO_PADRAO_LAUDO_PAT (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_PESFISI_FK3 
 FOREIGN KEY (CD_ANESTESISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_LAPAMOC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.LAUDO_PACIENTE_MOT_CANC (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_PESFISI_FK4 
 FOREIGN KEY (CD_RESIDENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_LAUMEDG_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MEDIDA) 
 REFERENCES TASY.LAUDO_MEDIDA_GRUPO (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_PESFISI_FK5 
 FOREIGN KEY (CD_RESP_SEG_APROV) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LAUPACI_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_EXECUCAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT LAUPACI_LDPMDES_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESAP) 
 REFERENCES TASY.LAUDO_PACIENTE_MOT_DES (NR_SEQUENCIA),
  CONSTRAINT LAUPACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_SEG_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT INSERT ON TASY.LAUDO_PACIENTE TO AGENTPDF;

GRANT SELECT ON TASY.LAUDO_PACIENTE TO NIVEL_1;

