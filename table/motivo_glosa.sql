ALTER TABLE TASY.MOTIVO_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_GLOSA
(
  CD_MOTIVO_GLOSA       NUMBER(5)               NOT NULL,
  DS_MOTIVO_GLOSA       VARCHAR2(200 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_TIPO_GLOSA         VARCHAR2(1 BYTE)        NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_ACAO_GLOSA         VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_RESPOSTA           NUMBER(10),
  CD_SETOR_RESP         NUMBER(5),
  CD_ESTABELECIMENTO    NUMBER(4),
  NR_SEQ_GRUPO          NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_MOTIVO_GLOSA_CONV  VARCHAR2(20 BYTE),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOGLOSA_ESTABEL_FK_I ON TASY.MOTIVO_GLOSA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOGLOSA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOGLOSA_GRUPGLO_FK_I ON TASY.MOTIVO_GLOSA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOGLOSA_GRUPGLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOGLOSA_MOREGLO_FK_I ON TASY.MOTIVO_GLOSA
(CD_RESPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOGLOSA_MOREGLO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MOGLOSA_PK ON TASY.MOTIVO_GLOSA
(CD_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOGLOSA_SETATEN_FK_I ON TASY.MOTIVO_GLOSA
(CD_SETOR_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOGLOSA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOGLOSA_SETATEN_FK2_I ON TASY.MOTIVO_GLOSA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOGLOSA_SETATEN_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MOTIVO_GLOSA_tp  after update ON TASY.MOTIVO_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_MOTIVO_GLOSA);  ds_c_w:=null; ds_w:=substr(:new.DS_MOTIVO_GLOSA,1,500);gravar_log_alteracao(substr(:old.DS_MOTIVO_GLOSA,1,4000),substr(:new.DS_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_GLOSA',ie_log_w,ds_w,'MOTIVO_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MOTIVO_GLOSA ADD (
  CONSTRAINT MOGLOSA_PK
 PRIMARY KEY
 (CD_MOTIVO_GLOSA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_GLOSA ADD (
  CONSTRAINT MOGLOSA_MOREGLO_FK 
 FOREIGN KEY (CD_RESPOSTA) 
 REFERENCES TASY.MOTIVO_RESP_GLOSA (CD_RESPOSTA),
  CONSTRAINT MOGLOSA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_RESP) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MOGLOSA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MOGLOSA_GRUPGLO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT MOGLOSA_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.MOTIVO_GLOSA TO NIVEL_1;

