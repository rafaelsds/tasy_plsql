ALTER TABLE TASY.SSJ_EMA_INSCRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SSJ_EMA_INSCRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SSJ_EMA_INSCRICAO
(
  NR_SEQUENCIA          NUMBER(10),
  NM_PARTICIPANTE       VARCHAR2(255 BYTE),
  DS_EMAIL_PARTIC       VARCHAR2(255 BYTE),
  NM_PARTIC_PRINCIPAL   VARCHAR2(255 BYTE),
  IE_TIPO_PARTIC_PRINC  VARCHAR2(2 BYTE),
  DS_OBSERVACAO_DIA     VARCHAR2(255 BYTE),
  DS_OBSERVACAO_HORA    VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER TASY.SSJ_EMA_INSCRICAO_EVENT
AFTER INSERT
ON TASY.SSJ_EMA_INSCRICAO 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE


dt_1_1_w date:= to_date('03/07/2017 09:00','dd/mm/yyyy hh24:mi');
dt_1_2_w date:= to_date('03/07/2017 14:00','dd/mm/yyyy hh24:mi');

dt_2_1_w date:= to_date('05/07/2017 09:00','dd/mm/yyyy hh24:mi');
dt_2_2_w date:= to_date('05/07/2017 14:00','dd/mm/yyyy hh24:mi');

dt_3_1_w date:= to_date('06/07/2017 09:00','dd/mm/yyyy hh24:mi');
dt_3_2_w date:= to_date('06/07/2017 14:00','dd/mm/yyyy hh24:mi');

seq_w number(10);
nm_participante_w varchar2(255);
ds_email_partic_w varchar2(255);
nm_partic_principal_w varchar2(255);
ie_tipo_partic_princ_w varchar2(255);
hora_evento_w varchar2(255);
dia_evento_w varchar2(255);   
texto_w varchar2(200);

BEGIN
   
    

    seq_w := :new.nr_sequencia;
    nm_participante_w := :new.nm_participante;
    ds_email_partic_w := :new.ds_email_partic;
    nm_partic_principal_w :=  :new.nm_partic_principal;
    ie_tipo_partic_princ_w :=  :new.ie_tipo_partic_princ;
    hora_evento_w := :new.ds_observacao_hora;
    dia_evento_w := :new.ds_observacao_dia;

     
     /*        Dia            */
    if(dia_evento_w like '%1%')then
        if(hora_evento_w like '%1%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_1_1_w
            );
            
        
        end if;
        
        if(hora_evento_w like '%2%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_1_2_w
            );
            
        
        end if;
        
    end if;
    
    if(dia_evento_w like '%2%')then
        if(hora_evento_w like '%1%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_2_1_w
            );
            
        
        end if;
        
        if(hora_evento_w like '%2%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_2_2_w
            );
        
        end if;
    end if;    
    
    if(dia_evento_w like '%3%')then
        if(hora_evento_w like '%1%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_3_1_w
            );
        
        end if;
        
        if(hora_evento_w like '%2%')then
        
            insert into ssj_ema_inscricao_evento
            (   
                nr_sequencia, 
                nr_seq_inscricao,
                nm_participante, 
                ds_email_partic, 
                nm_partic_principal, 
                ie_tipo_partic_princ, 
                dt_evento
            )
            values
            (
                SSJ_EMA_INSCRICAO_EVENT_SEQ.nextval,
                seq_w,
                nm_participante_w,
                ds_email_partic_w,
                nm_partic_principal_w,
                ie_tipo_partic_princ_w,
                dt_3_2_w
            );
        
        end if;
         

    end if;
   
    texto_w:='Prazado(a) '||nm_participante_w||', 

Sua Inscri��o foi realizada com sucesso!

Contamos com sua presen�a!';

    ENVIAR_EMAIL('Confirma��o de Inscri��o',texto_w,'noreply@ssjose.com.br',ds_email_partic_w,'RAFAELSDS','A');  
       
END;
/


ALTER TABLE TASY.SSJ_EMA_INSCRICAO ADD (
  CONSTRAINT SSJ_CK_TIPO_PARTIC_PRINC
 CHECK (ie_tipo_partic_princ in('PJ','PF')),
  PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SSJ_EMA_INSCRICAO TO NIVEL_1;

