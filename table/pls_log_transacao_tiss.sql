ALTER TABLE TASY.PLS_LOG_TRANSACAO_TISS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOG_TRANSACAO_TISS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOG_TRANSACAO_TISS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR     NUMBER(10),
  CD_PRESTADOR         VARCHAR2(30 BYTE),
  NR_CPF               VARCHAR2(11 BYTE),
  CD_CGC               VARCHAR2(14 BYTE),
  NR_SEQ_TRANSACAO     VARCHAR2(20 BYTE),
  DT_GERACAO           DATE,
  DT_RESPOSTA          DATE,
  IE_TIPO_TRANSACAO    VARCHAR2(5 BYTE),
  IE_STATUS            VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLTRT_PK ON TASY.PLS_LOG_TRANSACAO_TISS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOG_TRANSACAO_TISS ADD (
  CONSTRAINT PLSLTRT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_LOG_TRANSACAO_TISS TO NIVEL_1;

