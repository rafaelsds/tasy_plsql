ALTER TABLE TASY.SCHEM_TEST_MAINTENANCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEM_TEST_MAINTENANCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEM_TEST_MAINTENANCE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SCRIPT        NUMBER(10),
  IE_STATUS            VARCHAR2(255 BYTE),
  IE_PRIORITY          VARCHAR2(255 BYTE),
  DS_INFO              VARCHAR2(1000 BYTE),
  NR_OS                NUMBER(10),
  DS_MOTIVE            NUMBER(10),
  DS_OWNER             VARCHAR2(255 BYTE),
  DS_SCHEDULE          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCTEMAI_PK ON TASY.SCHEM_TEST_MAINTENANCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEMAI_SCTESCR_FK_I ON TASY.SCHEM_TEST_MAINTENANCE
(NR_SEQ_SCRIPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTESTMAIN_I01 ON TASY.SCHEM_TEST_MAINTENANCE
(IE_STATUS, DS_OWNER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEM_TEST_MAINTENANCE ADD (
  CONSTRAINT SCTEMAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEM_TEST_MAINTENANCE TO NIVEL_1;

