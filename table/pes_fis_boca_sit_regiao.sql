ALTER TABLE TASY.PES_FIS_BOCA_SIT_REGIAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PES_FIS_BOCA_SIT_REGIAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PES_FIS_BOCA_SIT_REGIAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_PES_FIS_BOCA_SIT  NUMBER(10)           NOT NULL,
  CD_REGIAO_BOCA           VARCHAR2(20 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFIBORE_PFIBOSI_FK_I ON TASY.PES_FIS_BOCA_SIT_REGIAO
(NR_SEQ_PES_FIS_BOCA_SIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFIBORE_PK ON TASY.PES_FIS_BOCA_SIT_REGIAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFIBORE_TISSRGBO_FK_I ON TASY.PES_FIS_BOCA_SIT_REGIAO
(CD_REGIAO_BOCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PES_FIS_BOCA_SIT_REGIAO ADD (
  CONSTRAINT PFIBORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PES_FIS_BOCA_SIT_REGIAO ADD (
  CONSTRAINT PFIBORE_PFIBOSI_FK 
 FOREIGN KEY (NR_SEQ_PES_FIS_BOCA_SIT) 
 REFERENCES TASY.PES_FIS_BOCA_SITUACAO (NR_SEQUENCIA),
  CONSTRAINT PFIBORE_TISSRGBO_FK 
 FOREIGN KEY (CD_REGIAO_BOCA) 
 REFERENCES TASY.TISS_REGIAO_BOCA (CD_REGIAO));

GRANT SELECT ON TASY.PES_FIS_BOCA_SIT_REGIAO TO NIVEL_1;

