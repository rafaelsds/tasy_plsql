ALTER TABLE TASY.CM_CADASTRO_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CADASTRO_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CADASTRO_CONTROLE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_CONTROLE           DATE                    NOT NULL,
  NR_SEQ_TIPO           NUMBER(10),
  IE_LIMPO              VARCHAR2(1 BYTE),
  IE_ORGANICO           VARCHAR2(1 BYTE),
  IE_SUJIDADE           VARCHAR2(1 BYTE),
  IE_FISSURA            VARCHAR2(1 BYTE),
  IE_PROTEINA           VARCHAR2(1 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_SETOR_ORIGEM       NUMBER(10),
  IE_DEFEITO            VARCHAR2(1 BYTE),
  IE_FALTA_PECA         VARCHAR2(1 BYTE),
  NM_USUARIO_IDENTIFIC  VARCHAR2(15 BYTE),
  DS_ACAO_IMEDIATA      VARCHAR2(255 BYTE),
  IE_CORTANTE           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCADCO_CMTICCO_FK_I ON TASY.CM_CADASTRO_CONTROLE
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCADCO_CMTICCO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCADCO_PK ON TASY.CM_CADASTRO_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCADCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.CMCADCO_SETATEN_FK_I ON TASY.CM_CADASTRO_CONTROLE
(CD_SETOR_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCADCO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_cm_cadastro_controle_upd
before update or delete ON TASY.CM_CADASTRO_CONTROLE for each row
begin

    if(wheb_usuario_pck.get_cd_perfil not in(272, 1244))then
    
        if(updating and obter_usuario_ativo != :new.nm_usuario_nrec)then
        
            hsj_gerar_log('Usu�rio sem permiss�o para alterar o registro!'||hsj_enter||'Edi��o permitida apenas para o usu�rio que registrou a informa��o.');
            
        elsif(deleting and obter_usuario_ativo != :old.nm_usuario_nrec)then
        
            hsj_gerar_log('Usu�rio sem permiss�o para excluir o registro!');
            
        end if;
        
    end if;
    
end hsj_cm_cadastro_controle_upd;
/


ALTER TABLE TASY.CM_CADASTRO_CONTROLE ADD (
  CONSTRAINT CMCADCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CADASTRO_CONTROLE ADD (
  CONSTRAINT CMCADCO_CMTICCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.CM_TIPO_CAD_CONTROLE (NR_SEQUENCIA),
  CONSTRAINT CMCADCO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ORIGEM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CM_CADASTRO_CONTROLE TO NIVEL_1;

