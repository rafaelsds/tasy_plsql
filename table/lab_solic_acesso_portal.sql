ALTER TABLE TASY.LAB_SOLIC_ACESSO_PORTAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_SOLIC_ACESSO_PORTAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_SOLIC_ACESSO_PORTAL
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  DT_SOLICITACAO  DATE                          NOT NULL,
  NM_SOLICITANTE  VARCHAR2(60 BYTE)             NOT NULL,
  NR_CRM          VARCHAR2(20 BYTE)             NOT NULL,
  DS_EMAIL        VARCHAR2(255 BYTE)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LASOACP_PK ON TASY.LAB_SOLIC_ACESSO_PORTAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LASOACP_PK
  MONITORING USAGE;


ALTER TABLE TASY.LAB_SOLIC_ACESSO_PORTAL ADD (
  CONSTRAINT LASOACP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LAB_SOLIC_ACESSO_PORTAL TO NIVEL_1;

