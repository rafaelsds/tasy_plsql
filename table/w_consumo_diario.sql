DROP TABLE TASY.W_CONSUMO_DIARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CONSUMO_DIARIO
(
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  CD_MATERIAL       NUMBER(10)                  NOT NULL,
  NR_DIA            NUMBER(10),
  QT_CONSUMO        NUMBER(13,4),
  DT_CONSUMO        DATE,
  QT_MEDIA          NUMBER(13,4),
  QT_CONSUMO_TOTAL  NUMBER(13,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCONDIA_MATERIA_FK_I ON TASY.W_CONSUMO_DIARIO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONDIA_MATERIA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_CONSUMO_DIARIO ADD (
  CONSTRAINT WCONDIA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_CONSUMO_DIARIO TO NIVEL_1;

