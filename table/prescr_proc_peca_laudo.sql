ALTER TABLE TASY.PRESCR_PROC_PECA_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_PECA_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_PECA_LAUDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PECA          NUMBER(10),
  NR_SEQ_LAUDO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PPPLAU_LAUPACI_FK_I ON TASY.PRESCR_PROC_PECA_LAUDO
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PPPLAU_PK ON TASY.PRESCR_PROC_PECA_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PPPLAU_PK
  MONITORING USAGE;


CREATE INDEX TASY.PPPLAU_PREPRPE_FK_I ON TASY.PRESCR_PROC_PECA_LAUDO
(NR_SEQ_PECA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PPPLAU_PREPRPE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRESCR_PROC_PECA_LAUDO ADD (
  CONSTRAINT PPPLAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_PECA_LAUDO ADD (
  CONSTRAINT PPPLAU_LAUPACI_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PPPLAU_PREPRPE_FK 
 FOREIGN KEY (NR_SEQ_PECA) 
 REFERENCES TASY.PRESCR_PROC_PECA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROC_PECA_LAUDO TO NIVEL_1;

