ALTER TABLE TASY.CPOE_WEEK_DISPENS_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_WEEK_DISPENS_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_WEEK_DISPENS_RULE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  SI_DISPENSATION_DAY       VARCHAR2(1 BYTE),
  HR_DISPENSATION_TIME      DATE,
  IE_VIA_APLICACAO          VARCHAR2(5 BYTE),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  SI_ACTION_ON_HOLIDAY      VARCHAR2(2 BYTE),
  SI_DISPENSATION_END_DAY   VARCHAR2(1 BYTE),
  HR_DISPENSATION_END_TIME  DATE,
  SI_ACTION                 VARCHAR2(1 BYTE),
  NR_PRIORITY               NUMBER(3),
  DT_FIXED_START            DATE,
  DT_FIXED_END              DATE,
  DT_FIXED_LIMIT            DATE,
  SI_LIMIT_DAY              VARCHAR2(1 BYTE),
  HR_LIMIT_TIME             DATE,
  QT_DISPENSATION_DAYS      NUMBER(5),
  HR_BUSINESS_DAY_START     DATE,
  HR_BUSINESS_DAY_END       DATE,
  HR_SATURDAY_START         DATE,
  HR_SATURDAY_END           DATE,
  HR_SUNDAY_START           DATE,
  HR_SUNDAY_END             DATE,
  HR_HOLIDAY_START          DATE,
  HR_HOLIDAY_END            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CPOEWDR_PK ON TASY.CPOE_WEEK_DISPENS_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEWDR_SETATEN_FK_I ON TASY.CPOE_WEEK_DISPENS_RULE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEWDR_VIAAPLI_FK_I ON TASY.CPOE_WEEK_DISPENS_RULE
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_WEEK_DISPENS_RULE ADD (
  CONSTRAINT CPOEWDR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_WEEK_DISPENS_RULE ADD (
  CONSTRAINT CPOEWDR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CPOEWDR_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO));

GRANT SELECT ON TASY.CPOE_WEEK_DISPENS_RULE TO NIVEL_1;

