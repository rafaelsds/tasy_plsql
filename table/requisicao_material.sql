ALTER TABLE TASY.REQUISICAO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REQUISICAO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.REQUISICAO_MATERIAL
(
  NR_REQUISICAO               NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  CD_LOCAL_ESTOQUE            NUMBER(4),
  DT_SOLICITACAO_REQUISICAO   DATE              NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_OPERACAO_ESTOQUE         NUMBER(3)         NOT NULL,
  CD_PESSOA_REQUISITANTE      VARCHAR2(10 BYTE) NOT NULL,
  CD_PESSOA_ATENDENTE         VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_ESTABELECIMENTO_DESTINO  NUMBER(4),
  CD_LOCAL_ESTOQUE_DESTINO    NUMBER(4),
  CD_CENTRO_CUSTO             NUMBER(8),
  DT_BAIXA                    DATE,
  DT_LIBERACAO                DATE,
  DT_EMISSAO_SETOR            DATE,
  DT_EMISSAO_LOC_ESTOQUE      DATE,
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  NR_SEQ_ORDEM_SERV           NUMBER(10),
  IE_URGENTE                  VARCHAR2(1 BYTE)  NOT NULL,
  CD_SETOR_ENTREGA            NUMBER(5),
  IE_GERACAO                  VARCHAR2(2 BYTE),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  NR_SEQ_PROJ_GPI             NUMBER(10),
  NR_DOCUMENTO_EXTERNO        NUMBER(10),
  NR_SEQ_INV_ROUPA            NUMBER(10),
  NR_SEQ_PROTOCOLO            NUMBER(10),
  NM_USUARIO_RECEBEDOR        VARCHAR2(15 BYTE),
  DT_RECEBIMENTO              DATE,
  NR_ADIANTAMENTO             NUMBER(10),
  CD_PESSOA_SOLICITANTE       VARCHAR2(10 BYTE),
  DT_APROVACAO                DATE,
  NM_USUARIO_APROV            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10),
  NR_SEQ_JUSTIFICATIVA        NUMBER(10),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_ORIGEM_REQUISICAO        VARCHAR2(10 BYTE),
  DT_COMPRA                   DATE,
  NR_REQUISICAO_ORIG          NUMBER(10),
  NR_CIRURGIA                 NUMBER(10),
  NR_PRESCRICAO               NUMBER(14),
  NR_SEQ_SOLIC_KIT            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          46M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REQMATE_CENCUST_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_GPIPROJ_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_I1 ON TASY.REQUISICAO_MATERIAL
(DT_SOLICITACAO_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_LOCESTO_DEST_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_LOCAL_ESTOQUE_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_LOCESTO_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_MANORSE_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_MOJUREQ_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQMATE_MOJUREQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REQMATE_OPEESTO_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_PESFISI_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_PESSOA_REQUISITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_PESFISI_FK2_I ON TASY.REQUISICAO_MATERIAL
(CD_PESSOA_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REQMATE_PK ON TASY.REQUISICAO_MATERIAL
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_PROTREQ_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_REQMATE_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_REQUISICAO_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_ROPINVE_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_INV_ROUPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQMATE_ROPINVE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REQMATE_SETATEN_FK_I ON TASY.REQUISICAO_MATERIAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REQMATE_SETATEN_FK2_I ON TASY.REQUISICAO_MATERIAL
(CD_SETOR_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQMATE_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REQMATE_SOLKTMT_FK_I ON TASY.REQUISICAO_MATERIAL
(NR_SEQ_SOLIC_KIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQMATE_SOLKTMT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REQUISICAO_MATERIAL_tp  after update ON TASY.REQUISICAO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_REQUISICAO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'REQUISICAO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CIRURGIA,1,4000),substr(:new.NR_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CIRURGIA',ie_log_w,ds_w,'REQUISICAO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRESCRICAO,1,4000),substr(:new.NR_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRESCRICAO',ie_log_w,ds_w,'REQUISICAO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.requisicao_material_afupdate
after update ON TASY.REQUISICAO_MATERIAL for each row
declare

nr_sequencia_w			far_setores_integracao.nr_sequencia%type;
cd_local_estoque_w		local_estoque.cd_local_estoque%type;
cd_centro_custo_w		centro_custo.cd_centro_custo%type;
cd_local_estoque_req_w		local_estoque.cd_local_estoque%type;
ds_param_integ_hl7_w		varchar2(4000) := '';
ie_local_cc_swisslog_w		varchar2(1) := 'N';
ie_local_cc_supplypoint_w	varchar2(1) := 'N';
reg_integracao_p		gerar_int_padrao.reg_integracao;


cursor c01 is
	select	a.nr_sequencia
	from	far_setores_integracao a,
		empresa_integracao b
	where	b.nm_empresa = 'SWISSLOG'
	and	a.nr_seq_empresa_int = b.nr_sequencia;

cursor c02 is
	select	cd_local_estoque,
		cd_centro_custo
	from	far_local_cc_int
	where	nr_seq_far_setores = nr_sequencia_w;

cursor c03 is
	select	a.nr_sequencia
	from	far_setores_integracao a
	where	a.nr_seq_empresa_int = 82;

cursor c04 is
	select	cd_local_estoque,
		cd_centro_custo
	from	far_local_cc_int
	where	nr_seq_far_setores = nr_sequencia_w;

begin
if	(:new.dt_aprovacao is not null) and
	(:old.dt_aprovacao is null) then
	begin

	reg_integracao_p.ie_operacao		:=	'I';
	reg_integracao_p.cd_estab_documento		:=	:new.cd_estabelecimento;
	reg_integracao_p.cd_operacao_estoque	:=	:new.cd_operacao_estoque;
	reg_integracao_p.cd_local_estoque		:=	:new.cd_local_estoque;
	reg_integracao_p.cd_local_estoque_destino	:=	:new.cd_local_estoque_destino;
	reg_integracao_p.cd_centro_custo		:=	:new.cd_centro_custo;
	reg_integracao_p.cd_operacao_estoque		:=	:new.cd_operacao_estoque;


	gerar_int_padrao.gravar_integracao('30', :new.nr_requisicao, :new.nm_usuario, reg_integracao_p);

	open c01;
	loop
	fetch c01 into
		nr_sequencia_w;
	exit when c01%notfound or ie_local_cc_swisslog_w = 'S';
		begin
		open c02;
		loop
		fetch c02 into
			cd_local_estoque_w,
			cd_centro_custo_w;
		exit when c02%notfound;
			begin
			if	(:new.cd_local_estoque_destino is not null) and
				((cd_local_estoque_w > 0) and (cd_local_estoque_w = :new.cd_local_estoque_destino)) then
				ie_local_cc_swisslog_w := 'S';
				exit;
			elsif	(:new.cd_centro_custo is not null) and
				((cd_centro_custo_w > 0) and (cd_centro_custo_w = :new.cd_centro_custo)) then
				ie_local_cc_swisslog_w := 'S';
				exit;
			end if;
			end;
		end loop;
		close c02;
		end;
	end loop;
	close c01;

	select	nvl(max(cd_local_estoque_req),1)
	into	cd_local_estoque_req_w
	from	parametros_farmacia
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if	(:new.cd_local_estoque = cd_local_estoque_req_w) and (ie_local_cc_swisslog_w = 'S') then
		ds_param_integ_hl7_w := 'nr_requisicao=' || :new.nr_requisicao || obter_separador_bv;
		swisslog_gerar_integracao(440, ds_param_integ_hl7_w);
	end if;

	open c03;
	loop
	fetch c03 into
		nr_sequencia_w;
	exit when c03%notfound;
		begin
		open c04;
		loop
		fetch c04 into
			cd_local_estoque_w,
			cd_centro_custo_w;
		exit when c04%notfound;
			begin
			if	(:new.cd_local_estoque_destino is not null) and
				((cd_local_estoque_w > 0) and (cd_local_estoque_w = :new.cd_local_estoque_destino)) then
				ie_local_cc_supplypoint_w := 'S';
				exit;
			elsif	(:new.cd_centro_custo is not null) and
				((cd_centro_custo_w > 0) and (cd_centro_custo_w = :new.cd_centro_custo)) then
				ie_local_cc_supplypoint_w := 'S';
				exit;
			end if;
			end;
		end loop;
		close c04;
		end;
	end loop;
	close c03;

	if	(:new.cd_local_estoque = cd_local_estoque_req_w) and (ie_local_cc_supplypoint_w = 'S') then
		ds_param_integ_hl7_w := 'nr_requisicao=' || :new.nr_requisicao || obter_separador_bv;
		supplypoint_gerar_integracao(440, ds_param_integ_hl7_w);
	end if;
	exception
	when others then
		null;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.requisicao_material_afinsert
after insert ON TASY.REQUISICAO_MATERIAL for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_executa_trigger	varchar2(1);

begin

if	(:new.dt_aprovacao is not null) then

	begin

	reg_integracao_p.ie_operacao			:=	'I';
	reg_integracao_p.cd_estab_documento		:=	:new.cd_estabelecimento;
	reg_integracao_p.cd_operacao_estoque		:=	:new.cd_operacao_estoque;
	reg_integracao_p.cd_local_estoque		:=	:new.cd_local_estoque;
	reg_integracao_p.cd_local_estoque_destino	:=	:new.cd_local_estoque_destino;
	reg_integracao_p.cd_centro_custo		:=	:new.cd_centro_custo;

ie_executa_trigger	:=	wheb_usuario_pck.get_ie_executar_trigger;

		begin
		wheb_usuario_pck.set_ie_executar_trigger('S');
		gerar_int_padrao.gravar_integracao('30', :new.nr_requisicao, :new.nm_usuario, reg_integracao_p);
		wheb_usuario_pck.set_ie_executar_trigger(ie_executa_trigger);
		exception
		when others then
		wheb_usuario_pck.set_ie_executar_trigger(ie_executa_trigger);
		end;

	end;
end if;
end;
/


ALTER TABLE TASY.REQUISICAO_MATERIAL ADD (
  CONSTRAINT REQMATE_PK
 PRIMARY KEY
 (NR_REQUISICAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          9M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REQUISICAO_MATERIAL ADD (
  CONSTRAINT REQMATE_SOLKTMT_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_KIT) 
 REFERENCES TASY.SOLIC_KIT_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT REQMATE_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ENTREGA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REQMATE_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT REQMATE_ROPINVE_FK 
 FOREIGN KEY (NR_SEQ_INV_ROUPA) 
 REFERENCES TASY.ROP_INVENTARIO (NR_SEQUENCIA),
  CONSTRAINT REQMATE_PROTREQ_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROT_REQUISICAO (NR_SEQUENCIA),
  CONSTRAINT REQMATE_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REQMATE_MOJUREQ_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.MOTIVO_JUSTIF_REQ (NR_SEQUENCIA),
  CONSTRAINT REQMATE_REQMATE_FK 
 FOREIGN KEY (NR_REQUISICAO_ORIG) 
 REFERENCES TASY.REQUISICAO_MATERIAL (NR_REQUISICAO),
  CONSTRAINT REQMATE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT REQMATE_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_ESTOQUE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE),
  CONSTRAINT REQMATE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_REQUISITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REQMATE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REQMATE_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT REQMATE_LOCESTO_DEST_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE_DESTINO) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT REQMATE_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.REQUISICAO_MATERIAL TO NIVEL_1;

