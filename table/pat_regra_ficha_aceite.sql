ALTER TABLE TASY.PAT_REGRA_FICHA_ACEITE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_REGRA_FICHA_ACEITE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_REGRA_FICHA_ACEITE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10),
  NR_SEQ_TIPO          NUMBER(10),
  NR_SEQ_LOCAL         NUMBER(10),
  IE_GERAR             VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATRGFI_EMPRESA_FK_I ON TASY.PAT_REGRA_FICHA_ACEITE
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATRGFI_ESTABEL_FK_I ON TASY.PAT_REGRA_FICHA_ACEITE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATRGFI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATRGFI_PATGRTI_FK_I ON TASY.PAT_REGRA_FICHA_ACEITE
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATRGFI_PATGRTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATRGFI_PATLOCA_FK_I ON TASY.PAT_REGRA_FICHA_ACEITE
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATRGFI_PATLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATRGFI_PATTIBE_FK_I ON TASY.PAT_REGRA_FICHA_ACEITE
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATRGFI_PATTIBE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PATRGFI_PK ON TASY.PAT_REGRA_FICHA_ACEITE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATRGFI_PK
  MONITORING USAGE;


ALTER TABLE TASY.PAT_REGRA_FICHA_ACEITE ADD (
  CONSTRAINT PATRGFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_REGRA_FICHA_ACEITE ADD (
  CONSTRAINT PATRGFI_PATGRTI_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PAT_GRUPO_TIPO (NR_SEQUENCIA),
  CONSTRAINT PATRGFI_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PATRGFI_PATTIBE_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.PAT_TIPO_BEM (NR_SEQUENCIA),
  CONSTRAINT PATRGFI_PATLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.PAT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PATRGFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PAT_REGRA_FICHA_ACEITE TO NIVEL_1;

