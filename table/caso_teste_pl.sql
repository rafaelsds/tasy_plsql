ALTER TABLE TASY.CASO_TESTE_PL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CASO_TESTE_PL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CASO_TESTE_PL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_TITULO             VARCHAR2(200 BYTE),
  DS_DETALHE            VARCHAR2(4000 BYTE),
  NR_SEQ_CASO_TESTE     NUMBER(10),
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  DT_LIBERACAO          DATE,
  NR_ORDEM              NUMBER(10),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CSCTSTMAC_PK ON TASY.CASO_TESTE_PL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CASO_TESTE_PL ADD (
  CONSTRAINT CSCTSTMAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.CASO_TESTE_PL TO NIVEL_1;

