ALTER TABLE TASY.PLS_CP_CTA_LOG_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_LOG_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_LOG_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CONTA_PROC     NUMBER(10)              NOT NULL,
  DS_LOG                VARCHAR2(4000 BYTE)     NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  VL_CUSTO_OPERACIONAL  NUMBER(15,2),
  VL_MEDICO             NUMBER(15,2),
  VL_ANESTESISTA        NUMBER(15,2),
  VL_AUXILIAR           NUMBER(15,2),
  VL_FILME              NUMBER(15,2),
  VL_PROCEDIMENTO       NUMBER(15,2),
  IE_DESTINO_REGRA      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCCLPRO_I1 ON TASY.PLS_CP_CTA_LOG_PROC
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCCLPRO_I2 ON TASY.PLS_CP_CTA_LOG_PROC
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCCLPRO_I3 ON TASY.PLS_CP_CTA_LOG_PROC
(NR_SEQ_CONTA_PROC, IE_DESTINO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCCLPRO_PK ON TASY.PLS_CP_CTA_LOG_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_log_proc_delete
before delete ON TASY.PLS_CP_CTA_LOG_PROC for each row
declare

begin

-- ao deletar um registro na tabela de log principal o mesmo � salvo em uma tabela de
-- hist�rico, desta forma na tabela principal guardamos apenas o ultimo recalculo feito
-- todos os demais ficam armazenados na tabela de hist�rico
insert into pls_cp_cta_log_proc_hist (
	nr_sequencia, ds_log, dt_atualizacao,
	ie_destino_regra, nm_usuario, nr_seq_conta_proc,
	dt_atualizacao_nrec, nm_usuario_nrec, vl_procedimento,
	vl_anestesista, vl_auxiliar, vl_custo_operacional,
	vl_filme, vl_medico
) values (
	pls_cp_cta_log_proc_hist_seq.nextval, :old.ds_log, :old.dt_atualizacao,
	nvl(:old.ie_destino_regra,'P'), :old.nm_usuario, :old.nr_seq_conta_proc,
	sysdate, nvl(wheb_usuario_pck.get_nm_usuario, 'naoidentificado'), :old.vl_procedimento,
	:old.vl_anestesista, :old.vl_auxiliar, :old.vl_custo_operacional,
	:old.vl_filme, :old.vl_medico
	);

end pls_cp_cta_log_proc_delete;
/


ALTER TABLE TASY.PLS_CP_CTA_LOG_PROC ADD (
  CONSTRAINT PCCLPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_CP_CTA_LOG_PROC TO NIVEL_1;

