ALTER TABLE TASY.TITULO_REC_LIQ_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_REC_LIQ_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_REC_LIQ_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_LIQ           NUMBER(10)               NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)               NOT NULL,
  VL_ADICIONAL         NUMBER(15,2)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TITRELV_PK ON TASY.TITULO_REC_LIQ_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELV_TIRELIQ_FK_I ON TASY.TITULO_REC_LIQ_VALOR
(NR_TITULO, NR_SEQ_LIQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELV_TIRELIQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELV_TRAFINA_FK_I ON TASY.TITULO_REC_LIQ_VALOR
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELV_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_rec_liq_valor_after
after insert or update ON TASY.TITULO_REC_LIQ_VALOR for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_seq_liq is not null) and (:new.nr_titulo is not null) then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_seq_liq)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p :=	:new.nr_seq_liq;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;

end if;

end;
/


ALTER TABLE TASY.TITULO_REC_LIQ_VALOR ADD (
  CONSTRAINT TITRELV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_REC_LIQ_VALOR ADD (
  CONSTRAINT TITRELV_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRELV_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_LIQ) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TITULO_REC_LIQ_VALOR TO NIVEL_1;

