ALTER TABLE TASY.AP_LOTE_BLOQUEIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AP_LOTE_BLOQUEIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AP_LOTE_BLOQUEIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_LOTE             NUMBER(10)            NOT NULL,
  CD_ID_USUARIO           VARCHAR2(80 BYTE),
  DS_BLOCK_REASON         VARCHAR2(500 BYTE),
  IE_EXPLICIT_HOLD        VARCHAR2(1 BYTE),
  IE_MANUAL_HOLD_BY_USER  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APLOUBL_APLOTE_FK_I ON TASY.AP_LOTE_BLOQUEIO
(NR_SEQ_LOTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APLOUBL_PK ON TASY.AP_LOTE_BLOQUEIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AP_LOTE_BLOQUEIO ADD (
  CONSTRAINT APLOUBL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AP_LOTE_BLOQUEIO ADD (
  CONSTRAINT APLOUBL_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AP_LOTE_BLOQUEIO TO NIVEL_1;

