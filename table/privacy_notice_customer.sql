ALTER TABLE TASY.PRIVACY_NOTICE_CUSTOMER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRIVACY_NOTICE_CUSTOMER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRIVACY_NOTICE_CUSTOMER
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_PRIVACIDADE          BLOB,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_INITIAL_VIGENCIA     DATE,
  DT_FINAL_VIGENCIA       DATE,
  IE_NOTIFICATION         VARCHAR2(1 BYTE),
  NR_SEQUENCIA_IC         NUMBER(10),
  DS_PRIVACIDADE_VARCHAR  VARCHAR2(255 BYTE),
  IE_OBRIGA_ACEITE        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_PRIVACIDADE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRINOTCU_PK ON TASY.PRIVACY_NOTICE_CUSTOMER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.privacy_notice_customer_after AFTER
  DELETE ON TASY.PRIVACY_NOTICE_CUSTOMER FOR EACH row
DECLARE NR_SEQ_OUT_COMUNIC NUMBER(10);
  BEGIN
    IF (:OLD.NR_SEQUENCIA_IC IS NOT NULL) THEN
      INSERT_COMUNIC_PRIVACY_NOTICE(NULL,NULL, 'N', :OLD.NR_SEQUENCIA_IC, NR_SEQ_OUT_COMUNIC);
    END IF;
  END privacy_notice_customer_before;
/


CREATE OR REPLACE TRIGGER TASY.privacy_notice_customer_before before
  INSERT OR
  UPDATE ON TASY.PRIVACY_NOTICE_CUSTOMER FOR EACH row
DECLARE qt_blocker_w NUMBER(1);
  qt_nr_sequencia_w  NUMBER(1);
  NR_SEQ_OUT_COMUNIC NUMBER(10);
  pragma autonomous_transaction;
  BEGIN
    IF (:new.nr_sequencia >0 AND :new.dt_initial_vigencia <= :new.dt_final_vigencia) THEN
      SELECT NVL(MAX(1),0)
      INTO qt_blocker_w
      FROM PRIVACY_NOTICE_CUSTOMER
      WHERE nr_sequencia          <> :new.nr_sequencia
      AND ie_situacao              = 'A'
      AND (trunc(:new.dt_final_vigencia) < trunc(SYSDATE)
      OR trunc(:new.dt_initial_vigencia) < trunc(SYSDATE)
      OR :new.dt_initial_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      OR :new.dt_final_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA);
    ELSIF (:new.dt_initial_vigencia <= :new.dt_final_vigencia) THEN
      SELECT NVL(MAX(1),0)
      INTO qt_blocker_w
      FROM PRIVACY_NOTICE_CUSTOMER
      WHERE trunc(:new.dt_initial_vigencia) <= trunc(SYSDATE)
      OR trunc(:new.dt_final_vigencia)      <= trunc(SYSDATE)
      OR :new.dt_initial_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      OR :new.dt_final_vigencia BETWEEN DT_INITIAL_VIGENCIA AND DT_FINAL_VIGENCIA
      AND ie_situacao = 'A';
    ELSE
      qt_blocker_w:= 1;
    END IF;
    IF (qt_blocker_w = 1) THEN
      wheb_mensagem_pck.exibir_mensagem_abort(1062913);
    END IF;
    IF ((:NEW.ie_situacao   = 'I') AND (:OLD.ie_situacao IS NOT NULL)) THEN
      :NEW.IE_NOTIFICATION := 'N';
    END IF;
    IF ((qt_blocker_w = 0) AND (:NEW.IE_NOTIFICATION <> NVL(:OLD.IE_NOTIFICATION,'X'))) THEN
      INSERT_COMUNIC_PRIVACY_NOTICE(:NEW.DT_INITIAL_VIGENCIA, :NEW.nm_usuario, :NEW.IE_NOTIFICATION, :OLD.NR_SEQUENCIA_IC, NR_SEQ_OUT_COMUNIC);
      :new.NR_SEQUENCIA_IC := NR_SEQ_OUT_COMUNIC;
    END IF;
    COMMIT;
  END privacy_notice_customer_before;
/


ALTER TABLE TASY.PRIVACY_NOTICE_CUSTOMER ADD (
  CONSTRAINT PRINOTCU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PRIVACY_NOTICE_CUSTOMER TO NIVEL_1;

