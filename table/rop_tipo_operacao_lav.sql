ALTER TABLE TASY.ROP_TIPO_OPERACAO_LAV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_TIPO_OPERACAO_LAV CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_TIPO_OPERACAO_LAV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OPERACAO          VARCHAR2(255 BYTE)       NOT NULL,
  IE_EVENTO            VARCHAR2(15 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ROPTOLA_PK ON TASY.ROP_TIPO_OPERACAO_LAV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ROP_TIPO_OPERACAO_LAV_tp  after update ON TASY.ROP_TIPO_OPERACAO_LAV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_OPERACAO,1,4000),substr(:new.DS_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OPERACAO',ie_log_w,ds_w,'ROP_TIPO_OPERACAO_LAV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ROP_TIPO_OPERACAO_LAV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO,1,4000),substr(:new.IE_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EVENTO',ie_log_w,ds_w,'ROP_TIPO_OPERACAO_LAV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_TIPO_OPERACAO_LAV ADD (
  CONSTRAINT ROPTOLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ROP_TIPO_OPERACAO_LAV TO NIVEL_1;

