ALTER TABLE TASY.LAB_MOTIVO_RECOLETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_MOTIVO_RECOLETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_MOTIVO_RECOLETA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_MOTIVO_RECOLETA   VARCHAR2(60 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_UTILIZACAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LABMORE_PK ON TASY.LAB_MOTIVO_RECOLETA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LAB_MOTIVO_RECOLETA_INS_UPD 
before insert or update or delete ON TASY.LAB_MOTIVO_RECOLETA 
for each row
declare 
 
ie_operacao_p	number(1); 
 
begin 
 
if	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then 
 
	IF 	INSERTING THEN 
		ie_operacao_p	:= 1; 
	elsif	UPDATING then 
		ie_operacao_p	:= 2; 
	elsif	DELETING then 
		ie_operacao_p	:= 3; 
	end if; 
 
	integrar_tasylab(	 
				null, 
				null, 
				296, 
				null, 
				null, 
				null, 
				null, 
				null, 
				null, 
				:new.nr_sequencia, 
				'LAB_MOTIVO_RECOLETA', 
				ie_operacao_p, 
				'N'); 
	 
end if; 
 
end;
/


ALTER TABLE TASY.LAB_MOTIVO_RECOLETA ADD (
  CONSTRAINT LABMORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LAB_MOTIVO_RECOLETA TO NIVEL_1;

