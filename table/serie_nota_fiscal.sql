ALTER TABLE TASY.SERIE_NOTA_FISCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SERIE_NOTA_FISCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SERIE_NOTA_FISCAL
(
  CD_SERIE_NF          VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  IE_NUMERO_NOTA       VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ULTIMA_NF         VARCHAR2(20 BYTE)        NOT NULL,
  NR_MIN_CARACTERES    NUMBER(10),
  IE_COMPL_PAGO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SERNOFI_ESTABEL_FK_I ON TASY.SERIE_NOTA_FISCAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SERNOFI_PK ON TASY.SERIE_NOTA_FISCAL
(CD_SERIE_NF, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.serie_nota_fiscal_log
before insert or update or delete ON TASY.SERIE_NOTA_FISCAL for each row
declare

ds_log_w	varchar2(4000);

begin

if (inserting) then
	ds_log_w := 'Operacao: INSERT' || chr(13);
elsif (updating) then
	ds_log_w := 'Operacao: UPDATE' || chr(13);
elsif (deleting) then
	ds_log_w := 'Operacao: DELETE' || chr(13);
end if;

ds_log_w := ds_log_w || 'NR_ULTIMA_NF new: ' || :new.NR_ULTIMA_NF || ' NR_ULTIMA_NF old: ' || :old.NR_ULTIMA_NF || chr(13) ||
		'CD_SERIE_NF new: ' || :new.CD_SERIE_NF || ' CD_SERIE_NF old: ' || :old.CD_SERIE_NF || chr(13) ||
		'CD_ESTABELECIMENTO new: ' || :new.CD_ESTABELECIMENTO || ' CD_ESTABELECIMENTO old: ' || :old.CD_ESTABELECIMENTO || chr(13) ||
		'IE_COMPL_PAGO new: ' || :new.IE_COMPL_PAGO || ' IE_COMPL_PAGO old: ' || :old.IE_COMPL_PAGO || chr(13) ||
		'IE_NUMERO_NOTA new: ' || :new.IE_NUMERO_NOTA || ' IE_NUMERO_NOTA old: ' || :old.IE_NUMERO_NOTA || chr(13) ||
		'IE_SITUACAO new: ' || :new.IE_SITUACAO || ' IE_SITUACAO old: ' || :old.IE_SITUACAO || chr(13) ||
		'DT_ATUALIZACAO new: ' || :new.DT_ATUALIZACAO || ' DT_ATUALIZACAO old: ' || :old.DT_ATUALIZACAO || chr(13) ||
		'Usuario: ' || wheb_usuario_pck.get_nm_usuario || chr(13) ||
		'Local: ' || dbms_utility.format_call_stack || chr(13);


gravar_log_tasy(2809, ds_log_w ,'Tasy');

end;
/


CREATE OR REPLACE TRIGGER TASY.SERIE_NOTA_FISCAL_tp  after update ON TASY.SERIE_NOTA_FISCAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_SERIE_NF='||to_char(:old.CD_SERIE_NF)||'#@#@CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO);gravar_log_alteracao(substr(:old.NR_ULTIMA_NF,1,4000),substr(:new.NR_ULTIMA_NF,1,4000),:new.nm_usuario,nr_seq_w,'NR_ULTIMA_NF',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MIN_CARACTERES,1,4000),substr(:new.NR_MIN_CARACTERES,1,4000),:new.nm_usuario,nr_seq_w,'NR_MIN_CARACTERES',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NUMERO_NOTA,1,4000),substr(:new.IE_NUMERO_NOTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NUMERO_NOTA',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SERIE_NF,1,4000),substr(:new.CD_SERIE_NF,1,4000),:new.nm_usuario,nr_seq_w,'CD_SERIE_NF',ie_log_w,ds_w,'SERIE_NOTA_FISCAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SERIE_NOTA_FISCAL ADD (
  CONSTRAINT SERNOFI_PK
 PRIMARY KEY
 (CD_SERIE_NF, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SERIE_NOTA_FISCAL ADD (
  CONSTRAINT SERNOFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SERIE_NOTA_FISCAL TO NIVEL_1;

