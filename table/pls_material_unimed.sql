ALTER TABLE TASY.PLS_MATERIAL_UNIMED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MATERIAL_UNIMED CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MATERIAL_UNIMED
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_MATERIAL              NUMBER(10)           NOT NULL,
  CD_UNIDADE_MEDIDA        VARCHAR2(10 BYTE)    NOT NULL,
  NM_MATERIAL              VARCHAR2(255 BYTE),
  IE_TIPO                  NUMBER(3)            NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_CNPJ                  VARCHAR2(14 BYTE),
  NM_FABRICANTE            VARCHAR2(255 BYTE),
  NM_IMPORTADOR            VARCHAR2(255 BYTE),
  NR_REGISTRO_ANVISA       VARCHAR2(20 BYTE),
  DT_VALIDADE_ANVISA       DATE,
  DS_MOTIVO_ATIVO_INATIVO  VARCHAR2(255 BYTE),
  VL_FABRICA               NUMBER(15,4),
  VL_MAX_CONSUMIDOR        NUMBER(18,4),
  DT_INICIO_OBRIGATORIO    DATE,
  DS_MATERIAL              VARCHAR2(2000 BYTE),
  DS_ESPECIALIDADE         VARCHAR2(255 BYTE),
  DS_CLASSE                VARCHAR2(255 BYTE),
  DS_APRESENTACAO          VARCHAR2(2000 BYTE),
  VL_FATOR_CONVERSAO       NUMBER(15,2),
  IE_GENERICO              VARCHAR2(1 BYTE),
  DS_GRUPO_FARMACOLOGICO   VARCHAR2(255 BYTE),
  DS_CLASSE_FARMACOLOGICO  VARCHAR2(255 BYTE),
  DS_FORMA_FARMACEUTICO    VARCHAR2(255 BYTE),
  PR_ICMS                  NUMBER(7,2),
  VL_PMC                   NUMBER(15,4),
  DS_PRINCIPIO_ATIVO       VARCHAR2(255 BYTE),
  IE_ORIGEM                NUMBER(1),
  IE_INCONSISTENTE         VARCHAR2(3 BYTE),
  DT_VINCULO               DATE,
  IE_OPME                  VARCHAR2(3 BYTE),
  CD_REF_MATERIAL_FAB      VARCHAR2(60 BYTE),
  CD_ANTERIOR_MATERIAL     NUMBER(10),
  CD_ANTERIOR_MEDICAMENTO  NUMBER(10),
  IE_MAT_PLS_DUPLICADO     VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_DUP_MAT   DATE,
  NM_USUARIO_DUP_MAT       VARCHAR2(15 BYTE),
  CD_MATERIAL_TUSS         NUMBER(10),
  DS_VERSAO_TISS           VARCHAR2(20 BYTE),
  IE_PRODUTO               VARCHAR2(1 BYTE),
  IE_CODIFICACAO           VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  DT_FIM_IMPLANTACAO       DATE,
  VL_PRECO_UNICO           NUMBER(15,4),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  DS_UNIDADE_MEDIDA        VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4),
  IE_PRODUTO_MED           VARCHAR2(5 BYTE),
  IE_CONFAZ                VARCHAR2(5 BYTE),
  DT_INICIO_VIGENCIA_VAL   DATE,
  VL_TC                    NUMBER(2),
  NM_TECNICO               VARCHAR2(2000 BYTE),
  DS_EQUIV_TECNICA         VARCHAR2(2000 BYTE),
  TP_TABELA_TISS           VARCHAR2(2 BYTE),
  CD_MATERIAL_TISS         VARCHAR2(10 BYTE),
  CD_MATERIAL_TISS_ANT     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMAUN_ESTABEL_FK_I ON TASY.PLS_MATERIAL_UNIMED
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAUN_I1 ON TASY.PLS_MATERIAL_UNIMED
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAUN_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAUN_I2 ON TASY.PLS_MATERIAL_UNIMED
(CD_MATERIAL, DS_VERSAO_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAUN_I3 ON TASY.PLS_MATERIAL_UNIMED
(IE_INCONSISTENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAUN_I4 ON TASY.PLS_MATERIAL_UNIMED
(CD_MATERIAL, DT_INICIO_VIGENCIA_VAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMAUN_PK ON TASY.PLS_MATERIAL_UNIMED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAUN_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_material_unimed_update
before update ON TASY.PLS_MATERIAL_UNIMED for each row
begin
if	(:new.cd_cnpj <> :old.cd_cnpj) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o CNPJ, de ' || :old.cd_cnpj || ', para ' || :new.cd_cnpj || '. ',
		'CD_CNPJ',
		:old.cd_cnpj,
		:new.cd_cnpj);
end if;

if	(:new.cd_material <> :old.cd_material) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo do material, de ' || :old.cd_material || ', para ' || :new.cd_material || '. ',
		'CD_MATERIAL',
		:old.cd_material,
		:new.cd_material);
end if;

if	(:new.cd_unidade_medida <> :old.cd_unidade_medida) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo unidade de medida, de ' || :old.cd_unidade_medida || ', para ' || :new.cd_unidade_medida || '. ',
		'CD_UNIDADE_MEDIDA',
		:old.cd_unidade_medida,
		:new.cd_unidade_medida);
end if;

if	(:new.ds_apresentacao <> :old.ds_apresentacao) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a apresenta��o, de ' || :old.ds_apresentacao || ', para ' || :new.ds_apresentacao || '. ',
		'DS_APRESENTACAO',
		:old.ds_apresentacao,
		:new.ds_apresentacao);
end if;

if	(:new.ds_classe <> :old.ds_classe) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a classifica��o, de ' || :old.ds_classe || ', para ' || :new.ds_classe || '. ',
		'DS_CLASSE',
		:old.ds_classe,
		:new.ds_classe);
end if;

if	(:new.ds_classe_farmacologico <> :old.ds_classe_farmacologico) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a classe farmacol�gica, de ' || :old.ds_classe_farmacologico || ', para ' || :new.ds_classe_farmacologico || '. ',
		'DS_CLASSE_FARMACOLOGICO',
		:old.ds_classe_farmacologico,
		:new.ds_classe_farmacologico);
end if;

if	(:new.ds_especialidade <> :old.ds_especialidade) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a descri��o da especialidade, de ' || :old.ds_especialidade || ', para ' || :new.ds_especialidade || '. ',
		'DS_ESPECIALIDADE',
		:old.ds_especialidade,
		:new.ds_especialidade);
end if;

if	(:new.ds_forma_farmaceutico <> :old.ds_forma_farmaceutico) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a forma farmac�utica, de ' || :old.ds_forma_farmaceutico || ', para ' || :new.ds_forma_farmaceutico || '. ',
		'DS_FORMA_FARMACEUTICO',
		:old.ds_forma_farmaceutico,
		:new.ds_forma_farmaceutico);
end if;

if	(:new.ds_grupo_farmacologico <> :old.ds_grupo_farmacologico) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a descri��o do grupo farmacol�gico, de ' || :old.ds_grupo_farmacologico || ', para ' || :new.ds_grupo_farmacologico || '. ',
		'DS_GRUPO_FARMACOLOGICO',
		:old.ds_grupo_farmacologico,
		:new.ds_grupo_farmacologico);
end if;

if	(:new.ds_material <> :old.ds_material) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a descri��o do material, de ' || :old.ds_material || ', para ' || :new.ds_material || '. ',
		'DS_MATERIAL',
		:old.ds_material,
		:new.ds_material);
end if;

if	(:new.ds_motivo_ativo_inativo <> :old.ds_motivo_ativo_inativo) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o motivo ativo/inativo, de ' || :old.ds_motivo_ativo_inativo || ', para ' || :new.ds_motivo_ativo_inativo || '. ',
		'DS_MOTIVO_ATIVO_INATIVO',
		:old.ds_motivo_ativo_inativo,
		:new.ds_motivo_ativo_inativo);
end if;

if	(:new.ds_principio_ativo <> :old.ds_principio_ativo) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a descri��o do princ�pio ativo, de ' || :old.ds_principio_ativo || ', para ' || :new.ds_principio_ativo || '. ',
		'DS_PRINCIPIO_ATIVO',
		:old.ds_principio_ativo,
		:new.ds_principio_ativo);
end if;

if	(:new.dt_inicio_obrigatorio <> :old.dt_inicio_obrigatorio) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a data in�cio obrigat�rio, de ' || :old.dt_inicio_obrigatorio || ', para ' || :new.dt_inicio_obrigatorio || '. ',
		'DT_INICIO_OBRIGATORIO',
		:old.dt_inicio_obrigatorio,
		:new.dt_inicio_obrigatorio);
end if;

if	(:new.dt_validade_anvisa <> :old.dt_validade_anvisa) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a data de v�lide ANVISA do material, de ' || :old.dt_validade_anvisa || ', para ' || :new.dt_validade_anvisa || '. ',
		'DT_VALIDADE_ANVISA',
		:old.dt_validade_anvisa,
		:new.dt_validade_anvisa);
end if;

if	(:new.ie_generico <> :old.ie_generico) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado gen�rico, de ' || :old.ie_generico || ', para ' || :new.ie_generico || '. ',
		'IE_GENERICO',
		:old.ie_generico,
		:new.ie_generico);
end if;

if	(:new.ie_opme <> :old.ie_opme) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o OPME, de ' || :old.ie_opme || ', para ' || :new.ie_opme || '. ',
		'IE_OPME',
		:old.ie_opme,
		:new.ie_opme);
end if;

if	(:new.ie_origem <> :old.ie_origem) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a origem, de ' || :old.ie_origem || ', para ' || :new.ie_origem || '. ',
		'IE_ORIGEM',
		:old.ie_origem,
		:new.ie_origem);
end if;

if	(:new.ie_situacao <> :old.ie_situacao) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a situa��o, de ' || :old.ie_situacao || ', para ' || :new.ie_situacao || '. ',
		'IE_SITUACAO',
		:old.ie_situacao,
		:new.ie_situacao);
end if;

if	(:new.ie_tipo <> :old.ie_tipo) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o tipo, de ' || :old.ie_tipo || ', para ' || :new.ie_tipo || '. ',
		'IE_TIPO',
		:old.ie_tipo,
		:new.ie_tipo);
end if;

if	(:new.nm_fabricante <> :old.nm_fabricante) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o nome do fabricante, de ' || :old.nm_fabricante || ', para ' || :new.nm_fabricante || '. ',
		'NM_FABRICANTE',
		:old.nm_fabricante,
		:new.nm_fabricante);
end if;

if	(:new.nm_importador <> :old.nm_importador) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o importador, de ' || :old.nm_importador || ', para ' || :new.nm_importador || '. ',
		'NM_IMPORTADOR',
		:old.nm_importador,
		:new.nm_importador);
end if;

if	(:new.nm_material <> :old.nm_material) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o nome do material, de ' || :old.nm_material || ', para ' || :new.nm_material || '. ',
		'NM_MATERIAL',
		:old.nm_material,
		:new.nm_material);
end if;

if	(:new.nr_registro_anvisa <> :old.nr_registro_anvisa) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o n�mero de registro ANVISA, de ' || :old.nr_registro_anvisa || ', para ' || :new.nr_registro_anvisa || '. ',
		'NR_REGISTRO_ANVISA',
		:old.nr_registro_anvisa,
		:new.nr_registro_anvisa);
end if;

if	(:new.pr_icms <> :old.pr_icms) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a percentagem ICMS, de ' || :old.pr_icms || ', para ' || :new.pr_icms || '. ',
		'PR_ICMS',
		:old.pr_icms,
		:new.pr_icms);
end if;

if	(:new.vl_fabrica <> :old.vl_fabrica) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o valor f�brica, de ' || :old.vl_fabrica || ', para ' || :new.vl_fabrica || '. ',
		'VL_FABRICA',
		:old.vl_fabrica,
		:new.vl_fabrica);
end if;

if	(:new.vl_fator_conversao <> :old.vl_fator_conversao) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o fator convers�o, de ' || :old.vl_fator_conversao || ', para ' || :new.vl_fator_conversao || '. ',
		'VL_FATOR_CONVERSAO',
		:old.vl_fator_conversao,
		:new.vl_fator_conversao);
end if;

if	(:new.vl_max_consumidor <> :old.vl_max_consumidor) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o valor m�x. consumidor, de ' || :old.vl_max_consumidor || ', para ' || :new.vl_max_consumidor || '. ',
		'VL_MAX_CONSUMIDOR',
		:old.vl_max_consumidor,
		:new.vl_max_consumidor);
end if;

if	(:new.vl_pmc <> :old.vl_pmc) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o valor PMC, de ' || :old.vl_pmc || ', para ' || :new.vl_pmc || '. ',
		'VL_PMC',
		:old.vl_pmc,
		:new.vl_pmc);
end if;

if	(:new.cd_ref_material_fab <> :old.cd_ref_material_fab) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo de refer�ncia do material no fabricante, de ' || :old.cd_ref_material_fab || ', para ' || :new.cd_ref_material_fab || '. ',
		'CD_REF_MATERIAL_FAB',
		:old.cd_ref_material_fab,
		:new.cd_ref_material_fab);
end if;

if	(:new.cd_anterior_material <> :old.cd_anterior_material) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo anterior do material, de ' || :old.cd_anterior_material || ', para ' || :new.cd_anterior_material || '. ',
		'CD_ANTERIOR_MATERIAL',
		:old.cd_anterior_material,
		:new.cd_anterior_material);
end if;

if	(:new.cd_anterior_medicamento <> :old.cd_anterior_medicamento) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo anterior do medicamento, de ' || :old.cd_anterior_medicamento || ', para ' || :new.cd_anterior_medicamento || '. ',
		'CD_ANTERIOR_MEDICAMENTO',
		:old.cd_anterior_medicamento,
		:new.cd_anterior_medicamento);
end if;

if	(:new.ds_unidade_medida <> :old.ds_unidade_medida) then
	insert into	pls_material_unimed_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_mat_unimed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_material_unimed_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a descri��o unidade de medida, de ' || :old.ds_unidade_medida || ', para ' || :new.ds_unidade_medida || '. ',
		'DS_UNIDADE_MEDIDA',
		:old.cd_unidade_medida,
		:new.cd_unidade_medida);
end if;

end pls_material_unimed_update;
/


ALTER TABLE TASY.PLS_MATERIAL_UNIMED ADD (
  CONSTRAINT PLSMAUN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MATERIAL_UNIMED ADD (
  CONSTRAINT PLSMAUN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MATERIAL_UNIMED TO NIVEL_1;

