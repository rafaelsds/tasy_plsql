ALTER TABLE TASY.PESSOA_JURIDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA
(
  CD_CGC                      VARCHAR2(14 BYTE) NOT NULL,
  DS_RAZAO_SOCIAL             VARCHAR2(255 BYTE) NOT NULL,
  NM_FANTASIA                 VARCHAR2(80 BYTE) NOT NULL,
  CD_CEP                      VARCHAR2(15 BYTE) NOT NULL,
  DS_ENDERECO                 VARCHAR2(40 BYTE) NOT NULL,
  DS_BAIRRO                   VARCHAR2(40 BYTE),
  DS_MUNICIPIO                VARCHAR2(40 BYTE) NOT NULL,
  SG_ESTADO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  NR_TELEFONE                 VARCHAR2(15 BYTE),
  NR_ENDERECO                 VARCHAR2(10 BYTE),
  NR_FAX                      VARCHAR2(15 BYTE),
  DS_EMAIL                    VARCHAR2(60 BYTE),
  NM_PESSOA_CONTATO           VARCHAR2(255 BYTE),
  NR_RAMAL_CONTATO            NUMBER(5),
  NR_INSCRICAO_ESTADUAL       VARCHAR2(20 BYTE),
  CD_TIPO_PESSOA              NUMBER(3),
  CD_CONTA_CONTABIL           VARCHAR2(20 BYTE),
  IE_PROD_FABRIC              VARCHAR2(3 BYTE)  NOT NULL,
  DS_SITE_INTERNET            VARCHAR2(255 BYTE),
  IE_QUALIDADE                VARCHAR2(3 BYTE),
  CD_COND_PAGTO               NUMBER(10),
  QT_DIA_PRAZO_ENTREGA        NUMBER(3),
  DS_NOME_ABREV               VARCHAR2(18 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  VL_MINIMO_NF                NUMBER(15,2),
  DS_SENHA                    VARCHAR2(20 BYTE),
  CD_PF_RESP_TECNICO          VARCHAR2(10 BYTE),
  NR_ALVARA_SANITARIO         VARCHAR2(20 BYTE),
  NR_AUTOR_FUNC               VARCHAR2(30 BYTE),
  NR_INSCRICAO_MUNICIPAL      VARCHAR2(20 BYTE),
  CD_ANS                      VARCHAR2(20 BYTE),
  CD_REFERENCIA_FORNEC        VARCHAR2(20 BYTE),
  IE_TIPO_TITULO              VARCHAR2(2 BYTE),
  DT_VALIDADE_ALVARA_SANIT    DATE,
  DT_VALIDADE_AUTOR_FUNC      DATE,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_ALTERAR_SENHA            VARCHAR2(1 BYTE),
  NR_SEQ_PAIS                 NUMBER(10),
  CD_PORTADOR                 NUMBER(10),
  CD_TIPO_PORTADOR            NUMBER(5),
  CD_INTERNACIONAL            VARCHAR2(20 BYTE),
  CD_CNES                     VARCHAR2(20 BYTE),
  CD_MUNICIPIO_IBGE           VARCHAR2(6 BYTE),
  NR_SEQ_TIPO_LOGRADOURO      NUMBER(10),
  IE_STATUS_EXPORTAR          VARCHAR2(1 BYTE),
  DT_INTEGRACAO_EXTERNA       DATE,
  IE_TIPO_TRIBUTACAO          VARCHAR2(15 BYTE),
  NR_ALVARA_SANITARIO_MUNIC   VARCHAR2(20 BYTE),
  NR_CERTIFICADO_BOAS_PRAT    VARCHAR2(20 BYTE),
  DT_VALIDADE_ALVARA_MUNIC    DATE,
  DT_VALIDADE_RESP_TECNICO    DATE,
  DT_VALIDADE_CERT_BOAS_PRAT  DATE,
  CD_CNPJ_RAIZ                VARCHAR2(8 BYTE),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDI_FAX                  VARCHAR2(3 BYTE),
  NR_DDD_FAX                  VARCHAR2(3 BYTE),
  NR_REGISTRO_PLS             VARCHAR2(20 BYTE),
  DS_ORGAO_REG_RESP_TECNICO   VARCHAR2(10 BYTE),
  NR_REGISTRO_RESP_TECNICO    VARCHAR2(20 BYTE),
  DS_RESP_TECNICO             VARCHAR2(255 BYTE),
  NR_SEQ_CNAE                 NUMBER(10),
  CD_CGC_MANTENEDORA          VARCHAR2(14 BYTE),
  CD_OPERADORA_EMPRESA        NUMBER(10),
  CD_SISTEMA_ANT              VARCHAR2(20 BYTE),
  NR_SEQ_NAT_JURIDICA         NUMBER(10),
  IE_TRANSPORTE               VARCHAR2(1 BYTE),
  IE_FORMA_REVISAO            VARCHAR2(15 BYTE),
  DT_ULTIMA_REVISAO           DATE,
  NM_USUARIO_REVISAO          VARCHAR2(15 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DS_OBSERVACAO_COMPL         VARCHAR2(4000 BYTE),
  NR_CCM                      NUMBER(10),
  IE_FORNECEDOR_OPME          VARCHAR2(15 BYTE),
  DT_INTEGRACAO               DATE,
  IE_STATUS_ENVIO             VARCHAR2(15 BYTE),
  IE_TIPO_TRIB_MUNICIPAL      VARCHAR2(2 BYTE),
  NR_SEQ_REGIAO               NUMBER(10),
  NR_SEQ_IDENT_CNES           NUMBER(10),
  NR_CEI                      VARCHAR2(50 BYTE),
  IE_EMPREENDEDOR_INDIVIDUAL  VARCHAR2(15 BYTE),
  NR_MATRICULA_CEI            VARCHAR2(15 BYTE),
  CD_CURP                     VARCHAR2(18 BYTE),
  CD_RFC                      VARCHAR2(13 BYTE),
  DS_ORIENTACAO_COBRANCA      VARCHAR2(4000 BYTE),
  CD_CVM                      VARCHAR2(40 BYTE),
  DT_CRIACAO                  DATE,
  NR_SEQ_IDIOMA               NUMBER(10),
  IE_TIPO_INST_SAUDE          NUMBER(5),
  NR_SEQ_TIPO_ASEN            NUMBER(10),
  NR_AUTOR_TRANSP_RESID       VARCHAR2(20 BYTE),
  NR_AUTOR_RECEB_RESID        VARCHAR2(20 BYTE),
  NR_SEQ_PESSOA_ENDERECO      NUMBER(10),
  DS_EMAIL_NFE                VARCHAR2(255 BYTE),
  IE_TIPO_SECRETARIA          VARCHAR2(1 BYTE),
  CD_UF_IBGE                  NUMBER(2),
  IE_TIPO_CONTRIBUICAO        VARCHAR2(6 BYTE),
  CD_FACILITY_CODE            VARCHAR2(20 BYTE),
  DS_LATITUDE                 VARCHAR2(255 BYTE),
  DS_LONGITUDE                VARCHAR2(255 BYTE),
  IE_TIPO_CONTRIBUINTE        VARCHAR2(2 BYTE),
  DT_VALIDADE_PJ              DATE,
  IE_INSTITUICAO_MEDICA       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.CD_CGC IS 'Codigo do CGC';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_RAZAO_SOCIAL IS 'Razao Social';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NM_FANTASIA IS 'Nome de Fantasia';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.CD_CEP IS 'Codigo do Enderecamento Postal';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_ENDERECO IS 'Endereco';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_BAIRRO IS 'Nome do Bairro';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_MUNICIPIO IS 'Nome do Municipio';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.SG_ESTADO IS 'Sigla do Estado';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_COMPLEMENTO IS 'Complemento do Endere�o';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NR_TELEFONE IS 'Numero do Telefone';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NR_FAX IS 'Numero do Fax';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.DS_EMAIL IS 'Codigo do endereco na Internet';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NM_PESSOA_CONTATO IS 'Nome da Pessoa de Contato';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NR_RAMAL_CONTATO IS 'Numero do ramal da pessoa de contato';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.NR_INSCRICAO_ESTADUAL IS 'Numero da inscricao estadual';

COMMENT ON COLUMN TASY.PESSOA_JURIDICA.CD_TIPO_PESSOA IS 'Tipo de Pessoa Juridica';


CREATE INDEX TASY.PESJURI_CATIASS_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_TIPO_ASEN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJURI_CNESIDE_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_CNESIDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_CNSTILO_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJURI_CONCONT_FK_I ON TASY.PESSOA_JURIDICA
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_CONPAGA_FK_I ON TASY.PESSOA_JURIDICA
(CD_COND_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_I1 ON TASY.PESSOA_JURIDICA
(CD_CNPJ_RAIZ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_I1
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_I2 ON TASY.PESSOA_JURIDICA
(IE_STATUS_EXPORTAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_I2
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_PAIS_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_PAIS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_PESFISI_FK_I ON TASY.PESSOA_JURIDICA
(CD_PF_RESP_TECNICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJURI_PESJURI_FK_I ON TASY.PESSOA_JURIDICA
(CD_CGC_MANTENEDORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_PESSEND_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESJURI_PK ON TASY.PESSOA_JURIDICA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJURI_PLSCNAE_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_CNAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_PLSCNAE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_PORTADO_FK_I ON TASY.PESSOA_JURIDICA
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_SUPNATJ_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_NAT_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_SUPNATJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_SUSMUNI_FK_I ON TASY.PESSOA_JURIDICA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_SUSMURE_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJURI_SUSMURE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJURI_TASYIDI_FK_I ON TASY.PESSOA_JURIDICA
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJURI_TIPPEJU_FK_I ON TASY.PESSOA_JURIDICA
(CD_TIPO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_afterUpIn
AFTER INSERT OR UPDATE ON TASY.PESSOA_JURIDICA FOR EACH ROW
begin
	if (nvl(:old.CD_CEP, ' ') != nvl(:new.CD_CEP, ' ')
		or nvl(:old.DS_ENDERECO,' ') != nvl(:new.DS_ENDERECO, ' ')
		or nvl(:old.NR_ENDERECO, ' ') != nvl(:new.NR_ENDERECO, ' ')
		or nvl(:old.DS_BAIRRO , ' ')!= nvl(:new.DS_BAIRRO, ' ')
		or nvl(:old.DS_COMPLEMENTO, ' ') != nvl(:new.DS_COMPLEMENTO, ' ')
		or nvl(:old.DS_MUNICIPIO, ' ') != nvl(:new.DS_MUNICIPIO, ' ')
		or nvl(:old.CD_MUNICIPIO_IBGE, ' ') != nvl(:new.CD_MUNICIPIO_IBGE, ' ')
		or nvl(:old.SG_ESTADO, ' ') != nvl(:new.SG_ESTADO, ' ')
		or nvl(:old.NR_DDD_TELEFONE, ' ') != nvl(:new.NR_DDD_TELEFONE, ' ')
		or nvl(:old.NR_TELEFONE, ' ') != nvl(:new.NR_TELEFONE, ' ')) then
		insert into FIS_PESSOA_JURIDICA_HIST(
				NR_SEQUENCIA,
				CD_CEP,
				CD_CGC,
				CD_MUNICIPIO_IBGE,
				DS_BAIRRO,
				DS_COMPLEMENTO,
				DS_ENDERECO,
				DS_MUNICIPIO,
				DT_ATUALIZACAO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO,
				NM_USUARIO_NREC,
				NR_DDD_TELEFONE,
				NR_ENDERECO,
				NR_TELEFONE,
				SG_ESTADO)
			values(
				FIS_PESSOA_JURIDICA_HIST_SEQ.nextval,
				:new.CD_CEP,
				:new.CD_CGC,
				:new.CD_MUNICIPIO_IBGE,
				:new.DS_BAIRRO,
				:new.DS_COMPLEMENTO,
				:new.DS_ENDERECO,
				:new.DS_MUNICIPIO,
				sysdate,
				sysdate,
				:new.NM_USUARIO,
				:new.NM_USUARIO,
				:new.NR_DDD_TELEFONE,
				:new.NR_ENDERECO,
				:new.NR_TELEFONE,
				:new.SG_ESTADO);
	end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.Pessoa_Juridica_Atual
BEFORE INSERT OR UPDATE ON TASY.PESSOA_JURIDICA FOR EACH ROW
declare
qt_existe_saldo_w		Number(5);
i	 		Number;
cd_cgc_w		Varchar2(14);
vl_parametro_w		Varchar2(1)	:= 'N';
cd_codigo_pais_w		Varchar2(5)	:= 0;
ie_conta_vigente_w	Varchar2(1)	:= 'N';
qt_reg_w			number(1);
qt_existe_w		number(10);
qt_cnes_w		number(10);
qt_parametro_w		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.ie_situacao <> 'I') and (:new.ie_situacao = 'I') then
	select	nvl(sum(qt_estoque),0)
	into	qt_existe_saldo_w
	from	fornecedor_mat_consignado
	where	cd_fornecedor		= :new.cd_cgc
	and	dt_mesano_referencia	= trunc(sysdate,'mm');

	if	(qt_existe_saldo_w <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(187694);
	end if;
end if;

/*	Obter parametro "09 - Permite cadastro de pessoa jur�dica internacional" da fun��o "Pessoa juridica"	*/
begin
obter_param_usuario(6, 9, obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);

/*select	nvl(vl_parametro,vl_parametro_padrao)
into	vl_parametro_w
from	funcao_parametro
where	cd_funcao	= 6
and	nr_sequencia	= 9;*/
exception
	when others then
	vl_parametro_w	:= 'N';
end;

if	(:new.ie_empreendedor_individual is null) then
	:new.ie_empreendedor_individual := 'N';
end if;

select	nvl(max(cd_codigo_pais),0)
into	cd_codigo_pais_w
from	pais
where	nr_sequencia	= :new.nr_seq_pais;

if	((vl_parametro_w = 'N') or (cd_codigo_pais_w = '55') or (cd_codigo_pais_w = 'BRA')) and
	(nvl(:new.ie_empreendedor_individual,'N') <> 'S') and
	((:new.cd_cgc <> :old.cd_cgc) or
	(:old.cd_cgc is null)) then
	begin
	cd_cgc_w	:= :new.cd_cgc;
	if	(length(cd_cgc_w) <> 14) then
		wheb_mensagem_pck.exibir_mensagem_abort(187695);
	end if;

	FOR i IN 1..14 LOOP
		if	(substr(cd_cgc_w,i,1) not in ('1','2','3','4','5','6','7','8','9','0')) then
			wheb_mensagem_pck.exibir_mensagem_abort(187697);
		end if;
	END LOOP;
	end;
end if;

if	(:new.cd_conta_contabil is not null) and
	(:new.cd_conta_contabil <> :old.cd_conta_contabil) then
	select	substr(obter_se_conta_vigente(:new.cd_conta_contabil, sysdate),1,1)
	into	ie_conta_vigente_w
	from	dual;
	if	(ie_conta_vigente_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(187699);
	end if;
end if;

if	(:new.ds_senha is not null) and
	(:new.ds_senha <> :old.ds_senha) then
	exec_sql_dinamico_bv('','delete from controle_acesso_web where ds_login = :ds_login','ds_login='||:new.cd_cgc);
end if;

if	(:new.cd_cnpj_raiz is null) then
	:new.cd_cnpj_raiz		:= substr(:new.cd_cgc,1,8);
end if;

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pj b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PJ'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.nm_usuario <> 'INTEGR_TASY') then

	insert into sup_int_pj(
		nr_sequencia,
		ie_forma_integracao,
		dt_liberacao,
		cd_cgc,
		ds_razao_social,
		nm_fantasia,
		ds_nome_abrev,
		cd_municipio_ibge,
		cd_cep,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_bairro,
		ds_municipio,
		sg_estado,
		nr_telefone,
		nr_fax,
		nr_inscricao_estadual,
		nr_inscricao_municipal,
		cd_internacional,
		ds_site_internet,
		cd_pf_resp_tecnico,
		ds_orgao_reg_resp_tecnico,
		nr_autor_func,
		nr_alvara_sanitario,
		nr_alvara_sanitario_munic,
		nr_certificado_boas_prat,
		cd_ans,
		dt_validade_autor_func,
		dt_validade_alvara_sanit,
		dt_validade_cert_boas_prat,
		cd_cnes,
		dt_validade_resp_tecnico,
		nr_registro_resp_tecnico,
		dt_validade_alvara_munic,
		ds_resp_tecnico,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_situacao)
	values(	sup_int_pj_seq.nextval,
		'E',
		sysdate,
		:new.cd_cgc,
		:new.ds_razao_social,
		:new.nm_fantasia,
		:new.ds_nome_abrev,
		:new.cd_municipio_ibge,
		:new.cd_cep,
		:new.ds_endereco,
		:new.nr_endereco,
		:new.ds_complemento,
		:new.ds_bairro,
		:new.ds_municipio,
		:new.sg_estado,
		:new.nr_telefone,
		:new.nr_fax,
		:new.nr_inscricao_estadual,
		:new.nr_inscricao_municipal,
		:new.cd_internacional,
		:new.ds_site_internet,
		:new.cd_pf_resp_tecnico,
		:new.ds_orgao_reg_resp_tecnico,
		:new.nr_autor_func,
		:new.nr_alvara_sanitario,
		:new.nr_alvara_sanitario_munic,
		:new.nr_certificado_boas_prat,
		:new.cd_ans,
		:new.dt_validade_autor_func,
		:new.dt_validade_alvara_sanit,
		:new.dt_validade_cert_boas_prat,
		:new.cd_cnes,
		:new.dt_validade_resp_tecnico,
		:new.nr_registro_resp_tecnico,
		:new.dt_validade_alvara_munic,
		:new.ds_resp_tecnico,
		sysdate,
		'INTEGR_TASY',
		sysdate,
		'INTEGR_TASY',
		:new.ie_situacao);
end if;
/*
if	(:new.cd_cnes is not null) then
	select	count(*)
	into	qt_cnes_w
	from	cnes_identificacao
	where	cd_cnes	= :new.cd_cnes
	and	cd_cgc	<> :new.cd_cgc;

	if	(qt_cnes_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(187701);
	end if;
end if;
*/
if	((nvl(:new.ds_razao_social,'X') <> nvl(:old.ds_razao_social,'X')) or
	(nvl(:new.nm_fantasia,'X') <> nvl(:old.nm_fantasia,'X'))) then
	select 	count(1)
	into	qt_parametro_w
	from 	pls_web_param_guia_medico;

	if (qt_parametro_w > 0) then
		pls_atualiza_nome_busca_guia(	null,
						:new.cd_cgc,
						:new.ds_razao_social,
						:new.nm_fantasia,
						null,
						null);
	end if;
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_pj_atual
after update of ds_razao_social, nr_seq_pais, cd_cgc, cd_municipio_ibge, ds_endereco, nr_endereco, ds_complemento, ds_bairro ON TASY.PESSOA_JURIDICA for each row
declare
ie_efd_icmsipi_w	varchar2(1);

begin
obter_param_usuario(5500, 61, obter_perfil_ativo, obter_usuario_ativo , obter_estabelecimento_ativo, ie_efd_icmsipi_w);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if	(nvl(ie_efd_icmsipi_w,'N') = 'S') then
		-- REGISTRO 0150.NOME
		if  (nvl(:new.ds_razao_social,'XPTO') <> nvl(:old.ds_razao_social,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'03', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(nvl(:old.ds_razao_social, :old.ds_razao_social), 1, 100),
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.COD_PAIS
		if  (nvl(:new.nr_seq_pais,0) <> nvl(:old.nr_seq_pais,0)) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'04', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				to_char(:old.nr_seq_pais),
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.CNPJ
		if  (nvl(:new.cd_cgc,'XPTO') <> nvl(:old.cd_cgc,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'05', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				elimina_caracteres_especiais(:old.cd_cgc),
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.COD_MUN
		if (:old.cd_municipio_ibge is not null and :old.cd_municipio_ibge != '0') then
			if  (nvl(:new.cd_municipio_ibge,'XPTO') <> nvl(:old.cd_municipio_ibge,'XPTO')) then
				insert into fis_efd_icmsipi_alteracao(
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_campo_alterado,
					ds_valor_anterior,
					cd_cgc)
				values (fis_efd_icmsipi_alteracao_seq.nextval,
					sysdate,
					obter_usuario_ativo,
					'08', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
					elimina_caracteres_especiais(substr(:old.cd_municipio_ibge || substr(calcula_digito('MODULO10', :old.cd_municipio_ibge),1,1),1,7)),
					:new.cd_cgc);
			end if;
		end if;

		-- REGISTRO 0150.END
		if  (nvl(:new.ds_endereco,'XPTO') <> nvl(:old.ds_endereco,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'10', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_endereco,1,100),
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.NUM
		if  (nvl(:new.nr_endereco,'XPTO') <> nvl(:old.nr_endereco,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'11', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				:old.nr_endereco,
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.NUM
		if  (nvl(:new.ds_complemento,'XPTO') <> nvl(:old.ds_complemento,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'12', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_complemento,1,100),
				:new.cd_cgc);
		end if;

		-- REGISTRO 0150.NUM
		if  (nvl(:new.ds_bairro,'XPTO') <> nvl(:old.ds_bairro,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_cgc)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'13', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_bairro,1,100),
				:new.cd_cgc);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Pessoa_Juridica_afterinsert
after INSERT ON TASY.PESSOA_JURIDICA FOR EACH ROW
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_conta_pagamento_w	varchar2(20);
cd_conta_recebimento_w	varchar2(20);
cd_estabelecimento_w	number(6);
dt_inicio_vigencia_w    date;
dt_fim_vigencia_w       date;

begin


select	nvl(max(cd_conta_pagamento), 'X'),
	nvl(max(cd_conta_recebimento), 'X'),
	nvl(max(cd_estabelecimento), 0)
into	cd_conta_pagamento_w,
	cd_conta_recebimento_w,
	cd_estabelecimento_w
from	tipo_pessoa_juridica
where	cd_tipo_pessoa	= :new.cd_tipo_pessoa
and	nvl(ie_atualiza_pj, 'S') = 'S';



if	(cd_conta_pagamento_w <> 'X') then

begin
	select	dt_inicio_vigencia,
		dt_fim_vigencia
	into	dt_inicio_vigencia_w,
		dt_fim_vigencia_w
	from	conta_contabil
	where	cd_conta_contabil = cd_conta_pagamento_w;
	exception when others then
		dt_inicio_vigencia_w	:= null;
		dt_fim_vigencia_w	:= null;
end;


	insert into pessoa_jur_conta_cont(
		nr_sequencia,
		cd_cgc,
		cd_conta_contabil,
		dt_atualizacao,
		nm_usuario,
		cd_empresa,
		ie_tipo_conta,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		cd_estabelecimento)
	select	pessoa_jur_conta_cont_seq.nextval,
		:new.cd_cgc,
		cd_conta_pagamento_w,
		sysdate,
		:new.nm_usuario,
		a.cd_empresa,
		'P',
		sysdate,
		:new.nm_usuario,
		dt_inicio_vigencia_w,
		dt_fim_vigencia_w,
		decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
	from	empresa a;
end if;


if	(cd_conta_recebimento_w <> 'X') then

begin
	select	dt_inicio_vigencia,
		dt_fim_vigencia
	into	dt_inicio_vigencia_w,
		dt_fim_vigencia_w
	from	conta_contabil
	where	cd_conta_contabil = cd_conta_recebimento_w;
	exception when others then
		dt_inicio_vigencia_w	:= null;
		dt_fim_vigencia_w	:= null;
end;

	insert into pessoa_jur_conta_cont(
		nr_sequencia,
		cd_cgc,
		cd_conta_contabil,
		dt_atualizacao,
		nm_usuario,
		cd_empresa,
		ie_tipo_conta,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		cd_estabelecimento)
	select	pessoa_jur_conta_cont_seq.nextval,
		:new.cd_cgc,
		cd_conta_recebimento_w,
		sysdate,
		:new.nm_usuario,
		a.cd_empresa,
		'R',
		sysdate,
		:new.nm_usuario,
		dt_inicio_vigencia_w,
		dt_fim_vigencia_w,
		decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
	from	empresa a;
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa juridica */
gravar_agend_integracao(556,'CD_PESSOA_JURIDICA='||:new.cd_cgc||';CD_PESSOA_FISICA='||null||';'); --Fornecedor

reg_integracao_p.ie_operacao		:=	'I';
reg_integracao_p.cd_tipo_pessoa		:=	:new.cd_tipo_pessoa;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :new.cd_cgc,:new.nm_usuario, reg_integracao_p);

END;
/


CREATE OR REPLACE TRIGGER TASY.Pessoa_Juridica_afterupdate
after UPDATE ON TASY.PESSOA_JURIDICA FOR EACH ROW
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_conta_pagamento_w	varchar2(20);
CD_CONTA_RECEBIMENTO_W	varchar2(20);
CD_ESTABELECIMENTO_W	number(6);
QT_conta_pagamento_w	number(5);
QT_CONTA_RECEBIMENTO_W	number(5);

begin
    select NVL(max(CD_CONTA_PAGAMENTO), 'X'),
           NVL(max(CD_CONTA_RECEBIMENTO), 'X'),
           NVL(max(CD_ESTABELECIMENTO), 0)
      into CD_CONTA_PAGAMENTO_W,
           CD_CONTA_RECEBIMENTO_W,
           CD_ESTABELECIMENTO_W
      from TIPO_PESSOA_JURIDICA
     where CD_TIPO_PESSOA	= :new.CD_TIPO_PESSOA
       and NVL(IE_ATUALIZA_PJ, 'S') = 'S';

     select COUNT(1)
       into QT_CONTA_PAGAMENTO_W
       from PESSOA_JUR_CONTA_CONT
      where CD_CGC = :new.cd_cgc
        and IE_TIPO_CONTA = 'P'
        AND CD_CONTA_CONTABIL = cd_conta_pagamento_w;

     select COUNT(1)
       into QT_CONTA_RECEBIMENTO_W
       from PESSOA_JUR_CONTA_CONT
      where CD_CGC = :new.cd_cgc
        and IE_TIPO_CONTA = 'R'
        and CD_CONTA_CONTABIL = CD_CONTA_RECEBIMENTO_W;

    if(:new.CD_TIPO_PESSOA <> :old.CD_TIPO_PESSOA) and (CD_CONTA_PAGAMENTO_W <> 'X') AND (QT_CONTA_PAGAMENTO_W =0) then
       update PESSOA_JUR_CONTA_CONT
       set DT_FIM_VIGENCIA =sysdate
     where CD_CGC = :new.cd_cgc
       and IE_TIPO_CONTA <> 'T'
       and IE_TIPO_CONTA = 'P'
       and DT_INICIO_VIGENCIA is not null
       and DT_FIM_VIGENCIA is null;

     insert into pessoa_jur_conta_cont(
          nr_sequencia,
          cd_cgc,
          cd_conta_contabil,
          dt_atualizacao,
          nm_usuario,
          cd_empresa,
          ie_tipo_conta,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          dt_inicio_vigencia,
          dt_fim_vigencia,
          cd_estabelecimento)
        select	pessoa_jur_conta_cont_seq.nextval,
          :new.cd_cgc,
          cd_conta_pagamento_w,
          sysdate,
          :new.nm_usuario,
          a.cd_empresa,
          'P',
          sysdate,
          :new.nm_usuario,
          obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'I'),
          obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'F'),
          decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
        from	EMPRESA a;
  end if;

 if(:new.CD_TIPO_PESSOA <> :old.CD_TIPO_PESSOA) and (CD_CONTA_RECEBIMENTO_W <> 'X')  AND (QT_CONTA_RECEBIMENTO_W =0) then
    update PESSOA_JUR_CONTA_CONT
       set DT_FIM_VIGENCIA =sysdate
     where CD_CGC = :new.cd_cgc
       and IE_TIPO_CONTA <> 'T'
       and IE_TIPO_CONTA = 'R'
       and DT_INICIO_VIGENCIA is not null
       and DT_FIM_VIGENCIA is null;

	insert into pessoa_jur_conta_cont(
		nr_sequencia,
		cd_cgc,
		cd_conta_contabil,
		dt_atualizacao,
		nm_usuario,
		cd_empresa,
		ie_tipo_conta,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		cd_estabelecimento)
	select	pessoa_jur_conta_cont_seq.nextval,
		:new.cd_cgc,
		cd_conta_recebimento_w,
		sysdate,
		:new.nm_usuario,
		a.cd_empresa,
		'R',
		sysdate,
		:new.nm_usuario,
		obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'I'),
		obter_vigencia_conta_contabil(cd_conta_pagamento_w, 'F'),
		decode(cd_estabelecimento_w, 0, null, cd_estabelecimento_w)
	from	EMPRESA a;
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa juridica */
gravar_agend_integracao(556,'CD_PESSOA_JURIDICA='||:new.cd_cgc||';CD_PESSOA_FISICA='||null||';'); --Fornecedor

reg_integracao_p.ie_operacao		:=	'A';
reg_integracao_p.cd_tipo_pessoa		:=	:new.cd_tipo_pessoa;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :new.cd_cgc,:new.nm_usuario, reg_integracao_p);

END;
/


CREATE OR REPLACE TRIGGER TASY.pj_regra_pre_cad
before insert ON TASY.PESSOA_JURIDICA for each row
declare

  v_check  number;

begin

  if wheb_usuario_pck.get_ie_executar_trigger = 'S' then
    select nvl(max(nr_sequencia),0)
      into v_check
      from regra_pre_cad_pj
     where cd_tipo_pessoa = :new.cd_tipo_pessoa
       and ie_exige_aprov_cadastro = 'S';

    if v_check <> 0 then
      wheb_mensagem_pck.exibir_mensagem_abort(1064319);
    end if;
  end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_pls_rps
after update ON TASY.PESSOA_JURIDICA for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w 	varchar2(1):= 'N';

begin
begin
	if	(nvl(:new.cd_cgc,'0')		!= nvl(:old.cd_cgc,'0')) or
		(nvl(:new.ds_razao_social,'0')	!= nvl(:old.ds_razao_social,'0')) or
		(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) or
		(nvl(:new.cd_cnes,'0')		!= nvl(:old.cd_cnes,'0')) or
		(nvl(:new.ds_municipio,'0')	!= nvl(:old.ds_municipio,'0')) then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S' ) then
		pls_gerar_alt_prest_rps(:new.cd_cgc, null, null, :old.cd_cgc, :old.cd_municipio_ibge, :old.cd_cnes, :new.nm_usuario, null, null, null);
	end if;
exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JURIDICA_tp  after update ON TASY.PESSOA_JURIDICA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CGC);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CCM,1,4000),substr(:new.NR_CCM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CCM',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RAZAO_SOCIAL,1,4000),substr(:new.DS_RAZAO_SOCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RAZAO_SOCIAL',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FANTASIA,1,4000),substr(:new.NM_FANTASIA,1,4000),:new.nm_usuario,nr_seq_w,'NM_FANTASIA',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PESSOA,1,4000),substr(:new.CD_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PESSOA',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FAX,1,4000),substr(:new.NR_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROD_FABRIC,1,4000),substr(:new.IE_PROD_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROD_FABRIC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SITE_INTERNET,1,4000),substr(:new.DS_SITE_INTERNET,1,4000),:new.nm_usuario,nr_seq_w,'DS_SITE_INTERNET',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_ESTADUAL,1,4000),substr(:new.NR_INSCRICAO_ESTADUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_ESTADUAL',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NOME_ABREV,1,4000),substr(:new.DS_NOME_ABREV,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOME_ABREV',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_FAX,1,4000),substr(:new.NR_DDD_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_TELEFONE,1,4000),substr(:new.NR_DDI_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_TELEFONE,1,4000),substr(:new.NR_DDD_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_FAX,1,4000),substr(:new.NR_DDI_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUTOR_FUNC,1,4000),substr(:new.NR_AUTOR_FUNC,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUTOR_FUNC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PF_RESP_TECNICO,1,4000),substr(:new.CD_PF_RESP_TECNICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PF_RESP_TECNICO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO,1,4000),substr(:new.NR_ALVARA_SANITARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ANS,1,4000),substr(:new.CD_ANS,1,4000),:new.nm_usuario,nr_seq_w,'CD_ANS',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_ALVARA_SANIT',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_AUTOR_FUNC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_MUNICIPAL,1,4000),substr(:new.NR_INSCRICAO_MUNICIPAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_MUNICIPAL',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNES,1,4000),substr(:new.CD_CNES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNES',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PORTADOR,1,4000),substr(:new.CD_PORTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PORTADOR',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PORTADOR,1,4000),substr(:new.CD_TIPO_PORTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PORTADOR',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERNACIONAL,1,4000),substr(:new.CD_INTERNACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNACIONAL',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TRIBUTACAO,1,4000),substr(:new.IE_TIPO_TRIBUTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TRIBUTACAO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_RESP_TECNICO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_BOAS_PRAT,1,4000),substr(:new.NR_CERTIFICADO_BOAS_PRAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_BOAS_PRAT',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERT_BOAS_PRAT',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_REG_RESP_TECNICO,1,4000),substr(:new.DS_ORGAO_REG_RESP_TECNICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_REG_RESP_TECNICO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO_MUNIC,1,4000),substr(:new.NR_ALVARA_SANITARIO_MUNIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO_MUNIC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_ALVARA_MUNIC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_ALVARA_MUNIC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_ALVARA_MUNIC',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_REVISAO,1,4000),substr(:new.IE_FORMA_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_REVISAO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_LOGRADOURO,1,4000),substr(:new.NR_SEQ_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_LOGRADOURO',ie_log_w,ds_w,'PESSOA_JURIDICA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JURIDICA ADD (
  CONSTRAINT PESJURI_PK
 PRIMARY KEY
 (CD_CGC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JURIDICA ADD (
  CONSTRAINT PESJURI_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT PESJURI_CATIASS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ASEN) 
 REFERENCES TASY.CAT_TIPO_ASSENTAMENTO (NR_SEQUENCIA),
  CONSTRAINT PESJURI_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT PESJURI_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PESSOA) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA),
  CONSTRAINT PESJURI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PESJURI_CONPAGA_FK 
 FOREIGN KEY (CD_COND_PAGTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PESJURI_PESFISI_FK 
 FOREIGN KEY (CD_PF_RESP_TECNICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESJURI_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT PESJURI_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT PESJURI_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PESJURI_CNSTILO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LOGRADOURO) 
 REFERENCES TASY.CNS_TIPO_LOGRADOURO (NR_SEQUENCIA),
  CONSTRAINT PESJURI_PLSCNAE_FK 
 FOREIGN KEY (NR_SEQ_CNAE) 
 REFERENCES TASY.PLS_CNAE (NR_SEQUENCIA),
  CONSTRAINT PESJURI_PESJURI_FK 
 FOREIGN KEY (CD_CGC_MANTENEDORA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESJURI_SUPNATJ_FK 
 FOREIGN KEY (NR_SEQ_NAT_JURIDICA) 
 REFERENCES TASY.SUP_NATUREZA_JURIDICA (NR_SEQUENCIA),
  CONSTRAINT PESJURI_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA),
  CONSTRAINT PESJURI_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_JURIDICA TO NIVEL_1;

GRANT SELECT ON TASY.PESSOA_JURIDICA TO ROBOCMTECNOLOGIA;

