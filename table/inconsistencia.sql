ALTER TABLE TASY.INCONSISTENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INCONSISTENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.INCONSISTENCIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_INCONSISTENCIA       NUMBER(5)             NOT NULL,
  IE_TIPO_INCONSISTENCIA  NUMBER(3)             NOT NULL,
  DS_INCONSISTENCIA       VARCHAR2(60 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_ACAO_USUARIO         VARCHAR2(2000 BYTE),
  IE_FECHA_CONTA          VARCHAR2(1 BYTE),
  IE_FECHA_ATENDIMENTO    VARCHAR2(1 BYTE),
  IE_PERMITE_ALTERAR      VARCHAR2(1 BYTE),
  CD_SISTEMA_ORIGEM       VARCHAR2(20 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_FECHA_PROTOCOLO      VARCHAR2(1 BYTE)      NOT NULL,
  CD_EXP_INCONSISTENCIA   NUMBER(10),
  CD_EXP_ACAO_USUARIO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INCONSI_DICEXPR_FK_I ON TASY.INCONSISTENCIA
(CD_EXP_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INCONSI_DICEXPR_FK2_I ON TASY.INCONSISTENCIA
(CD_EXP_ACAO_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INCONSI_PK ON TASY.INCONSISTENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INCONSISTENCIA ADD (
  CONSTRAINT INCONSI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INCONSISTENCIA ADD (
  CONSTRAINT INCONSI_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_INCONSISTENCIA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT INCONSI_DICEXPR_FK2 
 FOREIGN KEY (CD_EXP_ACAO_USUARIO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.INCONSISTENCIA TO NIVEL_1;

