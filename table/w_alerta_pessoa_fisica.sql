ALTER TABLE TASY.W_ALERTA_PESSOA_FISICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ALERTA_PESSOA_FISICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ALERTA_PESSOA_FISICA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_ALERGIA_REACAO_ADV   VARCHAR2(2000 BYTE),
  DS_HABITO_VICIO         VARCHAR2(2000 BYTE),
  DS_DOENCAS_PREV_ATUAIS  VARCHAR2(2000 BYTE),
  DS_CIRURGIA_PESSOA      VARCHAR2(2000 BYTE),
  DS_MEDIC_USO            VARCHAR2(2000 BYTE),
  DS_RESTRICAO_PESSOA     VARCHAR2(2000 BYTE),
  DS_ACESSORIO_ORT_PROT   VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  NM_PESSOA_FISICA        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WALEPES_PK ON TASY.W_ALERTA_PESSOA_FISICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WALEPES_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_ALERTA_PESSOA_FISICA ADD (
  CONSTRAINT WALEPES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ALERTA_PESSOA_FISICA TO NIVEL_1;

