ALTER TABLE TASY.REGRA_CONSISTENCIA_ESCRIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CONSISTENCIA_ESCRIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CONSISTENCIA_ESCRIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_CONTA_BANCO   NUMBER(10),
  NR_SEQ_TIPO_COBR     NUMBER(10),
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  IE_CONDICAO          VARCHAR2(5 BYTE)         NOT NULL,
  DS_CONDICAO          VARCHAR2(255 BYTE),
  DS_CONSISTENCIA      VARCHAR2(255 BYTE)       NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCOESC_BANESTA_FK_I ON TASY.REGRA_CONSISTENCIA_ESCRIT
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOESC_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOESC_ESTABEL_FK_I ON TASY.REGRA_CONSISTENCIA_ESCRIT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGCOESC_PK ON TASY.REGRA_CONSISTENCIA_ESCRIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOESC_TABSIST_FK_I ON TASY.REGRA_CONSISTENCIA_ESCRIT
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOESC_TICOESC_FK_I ON TASY.REGRA_CONSISTENCIA_ESCRIT
(NR_SEQ_TIPO_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOESC_TICOESC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_CONSISTENCIA_ESCRIT ADD (
  CONSTRAINT REGCOESC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CONSISTENCIA_ESCRIT ADD (
  CONSTRAINT REGCOESC_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT REGCOESC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGCOESC_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA),
  CONSTRAINT REGCOESC_TICOESC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBR) 
 REFERENCES TASY.TIPO_COBR_ESCRIT (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_CONSISTENCIA_ESCRIT TO NIVEL_1;

