ALTER TABLE TASY.PRESCR_DIETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_DIETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_DIETA
(
  NR_PRESCRICAO           NUMBER(14)            NOT NULL,
  NR_SEQUENCIA            NUMBER(6)             NOT NULL,
  CD_DIETA                NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  QT_PARAMETRO            NUMBER(15,4),
  DS_HORARIOS             VARCHAR2(2000 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  CD_MOTIVO_BAIXA         NUMBER(3),
  DT_BAIXA                DATE,
  IE_DESTINO_DIETA        VARCHAR2(2 BYTE),
  IE_REFEICAO             VARCHAR2(3 BYTE),
  IE_SUSPENSO             VARCHAR2(1 BYTE),
  DT_SUSPENSAO            DATE,
  NM_USUARIO_SUSP         VARCHAR2(15 BYTE),
  DT_EMISSAO_DIETA        DATE,
  NR_SEQ_MOTIVO_SUSP      NUMBER(10),
  DS_MOTIVO_SUSP          VARCHAR2(255 BYTE),
  QT_HORAS_JEJUM          NUMBER(2),
  CD_PERFIL_ATIVO         NUMBER(5),
  CD_INTERVALO            VARCHAR2(7 BYTE),
  NR_SEQ_ASSINATURA       NUMBER(10),
  NR_SEQ_INTERNO          NUMBER(10),
  NR_SEQ_INTERF_FARM      NUMBER(10),
  NR_PRESCRICAO_ORIGINAL  NUMBER(14),
  DT_SUSPENSAO_PROGR      DATE,
  IE_CIENTE               VARCHAR2(1 BYTE),
  NR_SEQ_ANTERIOR         NUMBER(15),
  IE_ESTENDIDO            VARCHAR2(1 BYTE),
  IE_URGENCIA             VARCHAR2(1 BYTE),
  IE_DOSE_ESPEC_AGORA     VARCHAR2(1 BYTE),
  HR_DOSE_ESPECIAL        VARCHAR2(5 BYTE),
  DT_EXTENSAO             DATE,
  DT_IMPRESSAO_NUTRICAO   DATE,
  IE_VIA_APLICACAO        VARCHAR2(50 BYTE),
  NR_DIA_UTIL             NUMBER(5),
  IE_HORARIO_SUSP         VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA_SUSP  NUMBER(10),
  DT_REP_PT               DATE,
  DT_REP_PT2              DATE,
  DS_JUSTIFICATIVA        VARCHAR2(2000 BYTE),
  IE_ERRO                 NUMBER(3),
  DS_COR_ERRO             VARCHAR2(15 BYTE),
  DS_STACK                VARCHAR2(2000 BYTE),
  HR_PRIM_HORARIO         VARCHAR2(5 BYTE),
  CD_PROTOCOLO            NUMBER(10),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  NR_SEQ_ITEM_PROT        NUMBER(10),
  NR_SEQ_DIETA_CPOE       NUMBER(10),
  QT_MIN_INTERVALO        NUMBER(5),
  DT_DOSE_ESPECIAL        DATE,
  DT_PRIM_HORARIO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRESDIE_DIETA_FK_I ON TASY.PRESCR_DIETA
(CD_DIETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESDIE_INTPRES_FK_I ON TASY.PRESCR_DIETA
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESDIE_I1 ON TASY.PRESCR_DIETA
(NR_PRESCRICAO_ORIGINAL, NR_SEQ_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          480K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESDIE_I2 ON TASY.PRESCR_DIETA
(NR_SEQ_DIETA_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESDIE_MOTSUPR_FK_I ON TASY.PRESCR_DIETA
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESDIE_PERFIL_FK_I ON TASY.PRESCR_DIETA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESDIE_PK ON TASY.PRESCR_DIETA
(NR_PRESCRICAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESDIE_PRESMED_FK_I ON TASY.PRESCR_DIETA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESDIE_TASASDI_FK_I ON TASY.PRESCR_DIETA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_TASASDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESDIE_UK ON TASY.PRESCR_DIETA
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_UK
  MONITORING USAGE;


CREATE INDEX TASY.PRESDIE_VIAAPLI_FK_I ON TASY.PRESCR_DIETA
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2056K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESDIE_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.prescr_dieta_update_hl7_dixtal
AFTER UPDATE ON TASY.PRESCR_DIETA FOR EACH ROW
declare

nr_seq_interno_w		number(10);
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	number(10);
ie_leito_monit			varchar2(1);

BEGIN

if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then

	ds_sep_bv_w := obter_separador_bv;

	select	nr_atendimento, cd_pessoa_fisica
	into	nr_atendimento_w, cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = :new.nr_prescricao;

	select	max(obter_atepacu_paciente(nr_atendimento_w ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_w),'N')
	into	ie_leito_monit
	from 	dual;

	if (ie_leito_monit = 'S') then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w   || ds_sep_bv_w ||
					'nr_atendimento='    || nr_atendimento_w      || ds_sep_bv_w ||
					'nr_seq_interno='     || nr_seq_interno_w       || ds_sep_bv_w ||
					'nr_prescricao='       || :new.nr_prescricao     || ds_sep_bv_w ||
					'nr_seq_dieta='        || :new.nr_sequencia     || ds_sep_bv_w;
		gravar_agend_integracao(51, ds_param_integ_hl7_w);
		end;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Prescr_dieta_Update
BEFORE UPDATE ON TASY.PRESCR_DIETA FOR EACH ROW
BEGIN

/*Alterar_material_dieta(:new.ie_suspenso, :new.nr_prescricao, :new.nr_sequencia);
Problema de deadLock no banco
*/
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.Prescr_dieta_Insert
BEFORE INSERT ON TASY.PRESCR_DIETA FOR EACH ROW
declare

dt_rep_pt_w		date;
dt_rep_pt2_w		date;

BEGIN

if	(:new.nr_seq_interno is null) then
	select Prescr_dieta_seq.NextVal
	into	:new.nr_seq_interno
	from dual;
end if;

if	(:new.dt_rep_pt is null) and
	(:new.dt_rep_pt2 is null) then

	select	max(dt_rep_pt),
		max(dt_rep_pt2)
	into	dt_rep_pt_w,
		dt_rep_pt2_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;

end if;

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


ALTER TABLE TASY.PRESCR_DIETA ADD (
  CONSTRAINT PRESDIE_PK
 PRIMARY KEY
 (NR_PRESCRICAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRESDIE_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          168K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_DIETA ADD (
  CONSTRAINT PRESDIE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESDIE_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT PRESDIE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PRESDIE_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PRESDIE_DIETA_FK 
 FOREIGN KEY (CD_DIETA) 
 REFERENCES TASY.DIETA (CD_DIETA),
  CONSTRAINT PRESDIE_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PRESDIE_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_DIETA TO NIVEL_1;

