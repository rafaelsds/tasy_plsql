ALTER TABLE TASY.TITULO_PAGAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR
(
  NR_TITULO                     NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_EMISSAO                    DATE            NOT NULL,
  DT_VENCIMENTO_ORIGINAL        DATE            NOT NULL,
  DT_VENCIMENTO_ATUAL           DATE            NOT NULL,
  VL_TITULO                     NUMBER(15,2)    NOT NULL,
  VL_SALDO_TITULO               NUMBER(15,2)    NOT NULL,
  VL_SALDO_JUROS                NUMBER(15,2)    NOT NULL,
  VL_SALDO_MULTA                NUMBER(15,2)    NOT NULL,
  CD_MOEDA                      NUMBER(5)       NOT NULL,
  TX_JUROS                      NUMBER(7,4)     NOT NULL,
  TX_MULTA                      NUMBER(7,4)     NOT NULL,
  CD_TIPO_TAXA_JURO             NUMBER(10)      NOT NULL,
  CD_TIPO_TAXA_MULTA            NUMBER(10)      NOT NULL,
  TX_DESC_ANTECIPACAO           NUMBER(7,4),
  DT_LIMITE_ANTECIPACAO         DATE,
  VL_DIA_ANTECIPACAO            NUMBER(15,2),
  CD_TIPO_TAXA_ANTECIPACAO      NUMBER(10),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  IE_ORIGEM_TITULO              VARCHAR2(10 BYTE) NOT NULL,
  IE_TIPO_TITULO                VARCHAR2(2 BYTE) NOT NULL,
  NR_SEQ_NOTA_FISCAL            NUMBER(10),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  CD_CGC                        VARCHAR2(14 BYTE),
  NR_DOCUMENTO                  NUMBER(20),
  NR_BLOQUETO                   VARCHAR2(100 BYTE),
  DT_LIQUIDACAO                 DATE,
  NR_LOTE_CONTABIL              NUMBER(10),
  DS_OBSERVACAO_TITULO          VARCHAR2(4000 BYTE),
  NR_BORDERO                    NUMBER(10),
  VL_BORDERO                    NUMBER(15,2),
  VL_JUROS_BORDERO              NUMBER(15,2),
  VL_MULTA_BORDERO              NUMBER(15,2),
  VL_DESCONTO_BORDERO           NUMBER(15,2),
  IE_DESCONTO_DIA               VARCHAR2(1 BYTE),
  NR_TITULO_ORIGINAL            NUMBER(10),
  NR_PARCELAS                   NUMBER(5),
  NR_TOTAL_PARCELAS             NUMBER(5),
  DT_CONTABIL                   DATE,
  VL_IR                         NUMBER(15,2),
  NR_SEQ_TRANS_FIN_BAIXA        NUMBER(10),
  DT_LIBERACAO                  DATE,
  NM_USUARIO_LIB                VARCHAR2(15 BYTE),
  NR_SEQ_RPA                    NUMBER(10),
  NR_REPASSE_TERCEIRO           NUMBER(10),
  NR_NOSSO_NUMERO               VARCHAR2(20 BYTE),
  VL_IMPOSTO_MUNIC              NUMBER(15,2),
  VL_INSS                       NUMBER(15,2),
  NR_SEQ_TRANS_FIN_CONTAB       NUMBER(10),
  NR_SEQ_TRIBUTO                NUMBER(10),
  VL_OUT_DESP_BORDERO           NUMBER(15,2),
  DS_COMPL_CONTAB               VARCHAR2(255 BYTE),
  DT_INCLUSAO                   DATE,
  NM_USUARIO_ORIG               VARCHAR2(15 BYTE),
  DT_INTEGRACAO_EXTERNA         DATE,
  IE_STATUS_TRIBUTO             VARCHAR2(15 BYTE),
  NR_LOTE_TRANSF_TRIB           NUMBER(10),
  NR_ADIANT_PAGO                NUMBER(10),
  NR_SEQ_CONTRATO               NUMBER(10),
  CD_TRIBUTO                    NUMBER(3),
  CD_VARIACAO                   VARCHAR2(2 BYTE),
  IE_PERIODICIDADE              VARCHAR2(1 BYTE),
  CD_ESTAB_FINANCEIRO           NUMBER(4),
  NR_TITULO_EXTERNO             VARCHAR2(255 BYTE),
  VL_BLOQ_ASSOCIADO             NUMBER(15,2),
  VL_BLOQ_JUROS                 NUMBER(15,2),
  VL_BLOQ_DESC                  NUMBER(15,2),
  IE_PLS                        VARCHAR2(2 BYTE),
  NR_SEQ_PROT_CONTA             NUMBER(10),
  CD_TIPO_BAIXA_NEG             NUMBER(5),
  CD_DARF                       VARCHAR2(10 BYTE),
  CD_BANCO_PORTADOR             NUMBER(3),
  VL_OUTROS_ACRESCIMOS          NUMBER(15,2),
  NR_SEQ_REEMBOLSO              NUMBER(10),
  NR_SEQ_LOTE_RES_PLS           NUMBER(10),
  NR_ADIANT_REC                 NUMBER(10),
  NR_SEQ_ADIANT_DEV             NUMBER(10),
  NR_TITULO_TRANSF              NUMBER(10),
  IE_BLOQUETO                   VARCHAR2(1 BYTE),
  NR_SEQ_PROJ_REC               NUMBER(10),
  NR_SEQ_PROCESSO               NUMBER(10),
  NR_SEQ_TAXA_SAUDE             NUMBER(10),
  NR_SEQ_LOTE_AUDIT             NUMBER(10),
  NR_SEQ_PROVISAO               NUMBER(10),
  NR_SEQ_DISTRIBUICAO           NUMBER(10),
  NR_SEQ_PROJ_GPI               NUMBER(10),
  NR_SEQ_ETAPA_GPI              NUMBER(10),
  NR_SEQ_LOTE_AUDIT_HIST        NUMBER(10),
  NR_SEQ_NOTA_CREDITO           NUMBER(10),
  NR_SEQ_LOTE_EMPRESA           NUMBER(10),
  NR_SEQ_CLASSE                 NUMBER(10),
  NR_SEQ_BORDERO_NC             NUMBER(10),
  NR_SEQ_PLS_PAG_PREST          NUMBER(10),
  NR_SEQ_CONTA_GPI              NUMBER(10),
  NR_SEQ_PLS_LOTE_CAMARA        NUMBER(10),
  NR_CONTA                      VARCHAR2(20 BYTE),
  VL_OUTRAS_DED_BORDERO         NUMBER(15,2),
  DT_ULTIMA_DEVOLUCAO           DATE,
  NR_SEQ_LOTE_ENC_CONTAS        NUMBER(10),
  NR_BLOQUETO_LEITURA           VARCHAR2(255 BYTE),
  NR_SEQ_CTB_TRIBUTO            NUMBER(10),
  NR_SEQ_TIT_REC_TRIB           NUMBER(10),
  NR_TITULO_DEST                NUMBER(10),
  NR_SEQ_TRIB_PRESU             NUMBER(10),
  NR_SEQ_SEGURADO_MENS          NUMBER(10),
  IE_STATUS                     VARCHAR2(2 BYTE),
  NR_SEQ_PLS_LOTE_CONTEST       NUMBER(10),
  NR_SEQ_PLS_LOTE_DISC          NUMBER(10),
  IE_TIPO_PAGAMENTO             VARCHAR2(3 BYTE),
  NR_SEQ_GRUPO_PROD             NUMBER(10),
  VL_OUTRAS_DESPESAS            NUMBER(15,2),
  NR_LOTE_CONTABIL_CURTO_PRAZO  NUMBER(10),
  NR_SEQ_TF_CURTO_PRAZO         NUMBER(10),
  NR_SEQ_PLS_VENC_TRIB          NUMBER(10),
  NR_SISTEMA_ANT                VARCHAR2(255 BYTE),
  NR_SEQ_ORC_ITEM_GPI           NUMBER(10),
  NR_ORDEM_COMPRA               NUMBER(10),
  NR_SOLIC_COMPRA               NUMBER(10),
  NR_SEQ_NOTA_DEBITO            NUMBER(10),
  NR_SEQ_NOTA_DEB_CONCLUSAO     NUMBER(10),
  CD_CONTA_CONTABIL             VARCHAR2(20 BYTE),
  NR_SEQ_PROC_GRU               NUMBER(10),
  NR_SEQ_SEG_RESCISAO           NUMBER(10),
  NR_SEQ_CONTA_BCO              NUMBER(10),
  VL_TITULO_ESTRANG             NUMBER(15,2),
  VL_COTACAO                    NUMBER(21,10),
  QT_MIN_USUARIO_LIB            NUMBER(10),
  NR_SEQ_PARC_EMPREST           NUMBER(15),
  IE_INTEGRA_UNIMED             VARCHAR2(1 BYTE),
  NR_SEQ_PAGADOR                NUMBER(10),
  DS_STACK                      VARCHAR2(4000 BYTE),
  DS_TXID                       VARCHAR2(255 BYTE),
  DS_CHAVE_PIX                  VARCHAR2(255 BYTE),
  IE_TIPO_CHAVE_PIX             VARCHAR2(2 BYTE),
  DS_URL_PIX                    VARCHAR2(255 BYTE),
  DS_QRCODE                     VARCHAR2(4000 BYTE),
  VL_TITULO_ATUAL               NUMBER(15,2)    DEFAULT null,
  NR_CODIGO_CONTROLE            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          35M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPAGA_ADIDEV_FK_I ON TASY.TITULO_PAGAR
(NR_ADIANT_REC, NR_SEQ_ADIANT_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_ADIPAGO_FK_I ON TASY.TITULO_PAGAR
(NR_ADIANT_PAGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_BANCO_FK_I ON TASY.TITULO_PAGAR
(CD_BANCO_PORTADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_BANESTA_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_CONTA_BCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_BORDNC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_BORDERO_NC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_BORPAGA_FK_I ON TASY.TITULO_PAGAR
(NR_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_CLTITPA_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_CLASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_CLTITPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_CONCONT_FK_I ON TASY.TITULO_PAGAR
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_CONTRAT_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_CTBTRIB_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_CTB_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_DLDISTR_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_DISTRIBUICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_DLDISTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_EMPFINPAR_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PARC_EMPREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_ESTABEL_FK_I ON TASY.TITULO_PAGAR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_ESTABEL_FK2_I ON TASY.TITULO_PAGAR
(CD_ESTAB_FINANCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_FISTRIBPRE_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_TRIB_PRESU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_FISTRIBPRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_GPICRETA_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_GPIORIT_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_GPIORIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_GPIPLAN_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_CONTA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_GPIPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_GPIPROJ_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_GPIPROJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_GRPRFIN_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_I ON TASY.TITULO_PAGAR
(DT_VENCIMENTO_ATUAL, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_I2 ON TASY.TITULO_PAGAR
(IE_SITUACAO, VL_SALDO_TITULO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_I3 ON TASY.TITULO_PAGAR
(NR_TITULO_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_I4 ON TASY.TITULO_PAGAR
(NR_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_I5 ON TASY.TITULO_PAGAR
(CD_PESSOA_FISICA, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_I6 ON TASY.TITULO_PAGAR
(CD_PESSOA_FISICA, DT_VENCIMENTO_ATUAL, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_LOAUTIRE_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_LOTE_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_LOAUTIRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_LOTAUHI_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_LOTCONT_FK_I ON TASY.TITULO_PAGAR
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_LOTCONT_FK2_I ON TASY.TITULO_PAGAR
(NR_LOTE_TRANSF_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_LOTCONT_FK3_I ON TASY.TITULO_PAGAR
(NR_LOTE_CONTABIL_CURTO_PRAZO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_LOTENCO_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_LOTE_ENC_CONTAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_LOTENCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_MOEDA_FK_I ON TASY.TITULO_PAGAR
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_NOTCRED_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_NOTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_NOTCRED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_NOTFISC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_ORDCOMP_FK_I ON TASY.TITULO_PAGAR
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PESFISI_FK_I ON TASY.TITULO_PAGAR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PESJURI_FK_I ON TASY.TITULO_PAGAR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPAGA_PK ON TASY.TITULO_PAGAR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLCONPAG_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSDELC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_LOTE_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSDELC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSLOCC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PLS_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSLOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSLOCO_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PLS_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSLODI_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PLS_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSLOPO_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_LOTE_RES_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSOTPS_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_TAXA_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPAGP_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PLS_PAG_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPCTI_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROT_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPEON_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSPEON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSPGRU_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROC_GRU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPPVT_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PLS_VENC_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPRCO_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSPROC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PLSSEGU_FK2_I ON TASY.TITULO_PAGAR
(NR_SEQ_SEG_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_PLSSEME_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_SEGURADO_MENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PLSSEME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PRORECU_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PRORECU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PTUNDEB_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_NOTA_DEBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PTUNDEB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_PTUNOCC_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_NOTA_DEB_CONCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_PTUNOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_REPTERC_FK_I ON TASY.TITULO_PAGAR
(NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_TIPBACP_FK_I ON TASY.TITULO_PAGAR
(CD_TIPO_BAIXA_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_TIPBACP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_TITPAGA_FK_I ON TASY.TITULO_PAGAR
(NR_TITULO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_TITPAGA_FK2_I ON TASY.TITULO_PAGAR
(NR_TITULO_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_TITPAGA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_TITRETR_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_TIT_REC_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_TITTEIM_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_TRAFINA_FK_I ON TASY.TITULO_PAGAR
(NR_SEQ_TRANS_FIN_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_TRAFINA_FK2_I ON TASY.TITULO_PAGAR
(NR_SEQ_TRANS_FIN_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAGA_TRAFINA_FK3_I ON TASY.TITULO_PAGAR
(NR_SEQ_TF_CURTO_PRAZO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAGA_TRIBUTO_FK_I ON TASY.TITULO_PAGAR
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_TRIBUTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITPAGA_UK ON TASY.TITULO_PAGAR
(NR_SEQ_RPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAGA_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_log_tiss_update
before update ON TASY.TITULO_PAGAR for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Gravar log TISS para quando ocorrer pagamento de contas m�dicas (t�tulo liquidado)
para posterior envio a ANS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

ie_tipo_evento_w	pls_monitor_tiss_alt.ie_tipo_evento%type	:= null;
ie_gera_w		varchar2(1) := 'S';

Cursor c_contas(	nr_titulo_pc		pls_oc_cta_filtro.nr_sequencia%type,
			nr_seq_lote_res_pls_pc	titulo_pagar.nr_seq_lote_res_pls%type) is
	select 	distinct a.nr_seq_conta
	from	pls_conta_medica_resumo		a,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		c,
		pls_pagamento_item		d,
		pls_lote_pagamento		e
	where	e.nr_sequencia = c.nr_seq_lote
	and	a.nr_seq_lote_pgto = e.nr_sequencia
	and	a.nr_seq_pag_item = d.nr_sequencia
	and	c.nr_sequencia	= d.nr_seq_pagamento
	and	c.nr_sequencia	= b.nr_seq_pag_prestador
	and	b.nr_titulo	= nr_titulo_pc
	and	nr_seq_lote_res_pls_pc is null
	union all
	select distinct a.nr_sequencia
	from	pls_conta a,
		pls_protocolo_conta b,
		pls_prot_conta_titulo c,
		pls_lote_protocolo d,
		pls_lote_protocolo_venc e
	where	a.nr_seq_protocolo = b.nr_sequencia
	and	b.nr_sequencia = c.nr_seq_protocolo
	and	c.nr_seq_lote = d.nr_sequencia
	and	d.nr_sequencia = e.nr_seq_lote
	and	e.nr_titulo = nr_titulo_pc
	and	nr_seq_lote_res_pls_pc is not null
	union all
	select 	distinct a.nr_sequencia nr_seq_conta
	from	pls_conta			a,
		ptu_fatura			b
	where	a.nr_seq_fatura		= b.nr_sequencia
	and	b.nr_titulo		= nr_titulo_pc
	and	nr_seq_lote_res_pls_pc 	is null;

Cursor c_recursos(	nr_titulo_pc	pls_oc_cta_filtro.nr_sequencia%type) is
	select	d.nr_seq_conta,
		f.nr_seq_conta_proc,
		null nr_seq_conta_mat,
		d.nr_sequencia nr_seq_conta_rec,
		f.nr_sequencia nr_seq_proc_rec,
		null nr_seq_mat_rec
	from	pls_rec_glosa_proc		f,
		pls_rec_glosa_conta		d,
		pls_conta_rec_resumo_item	c,
		pls_pagamento_prestador		b,
		pls_pag_prest_vencimento	a
	where	c.nr_seq_prestador_pgto	= b.nr_seq_prestador
	and	b.nr_seq_lote           = c.nr_seq_lote_pgto
	and	c.nr_seq_conta_rec	= d.nr_sequencia
	and	a.nr_seq_pag_prestador	= b.nr_sequencia
	and	c.nr_seq_proc_rec	= f.nr_sequencia
	and	a.nr_titulo		= nr_titulo_pc
	union all
	select	d.nr_seq_conta,
		null nr_seq_conta_proc,
		f.nr_seq_conta_mat,
		d.nr_sequencia nr_seq_conta_rec,
		null nr_seq_proc_rec,
		f.nr_sequencia nr_seq_mat_rec
	from	pls_rec_glosa_mat		f,
		pls_rec_glosa_conta		d,
		pls_conta_rec_resumo_item	c,
		pls_pagamento_prestador		b,
		pls_pag_prest_vencimento	a
	where	c.nr_seq_prestador_pgto	= b.nr_seq_prestador
	and	b.nr_seq_lote           = c.nr_seq_lote_pgto
	and	c.nr_seq_conta_rec	= d.nr_sequencia
	and	a.nr_seq_pag_prestador	= b.nr_sequencia
	and	c.nr_seq_mat_rec	= f.nr_sequencia
	and	a.nr_titulo		= nr_titulo_pc;

Cursor c_reembolso	(	nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_conta,
		a.nr_sequencia nr_seq_conta_proc,
		null nr_seq_conta_mat
	from	pls_conta		b,
		pls_conta_proc		a
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and	b.ie_status		= 'F'
	and	a.ie_status		<> 'M'
	union all
	select	b.nr_sequencia nr_seq_conta,
		null nr_seq_conta_proc,
		a.nr_sequencia nr_seq_conta_mat
	from	pls_conta		b,
		pls_conta_mat		a
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_protocolo	= nr_seq_protocolo_p
	and	b.ie_status		= 'F'
	and	a.ie_status		<> 'M';

begin
-- Quando o t�tulo foi liquidado
if	(:old.dt_liquidacao is null) and
	(:new.dt_liquidacao is not null) and
	(:new.ie_situacao <> 'C')/*Tem que desconsiderar os t�tulos que foram cancelados*/ then
	-- Pagamento da conta m�dica
	ie_tipo_evento_w	:= 'PC';
elsif	(:old.dt_liquidacao is not null) and
	(:new.dt_liquidacao is null) and
	(:new.ie_situacao <> 'C')/*Tem que desconsiderar os t�tulos que foram cancelados*/ then
	ie_tipo_evento_w	:= 'EX';
end if;

if	(ie_tipo_evento_w is not null) then

	for r_c_contas_w in c_contas(:new.nr_titulo, :new.nr_seq_lote_res_pls) loop

		if	(ie_tipo_evento_w	= 'EX') then
			delete	pls_monitor_tiss_alt
			where	nr_seq_conta	= r_c_contas_w.nr_seq_conta
			and	ie_tipo_evento	= ie_tipo_evento_w;
		else
			ie_gera_w := 'S';

			if	(ie_tipo_evento_w = 'PC') then
				ie_gera_w := pls_obter_se_pgto_monit(r_c_contas_w.nr_seq_conta);
			end if;

			if	(ie_gera_w = 'S') then
				pls_tiss_gravar_log_monitor(	ie_tipo_evento_w,
								r_c_contas_w.nr_seq_conta,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								wheb_usuario_pck.get_nm_usuario,
								:new.dt_liquidacao);
			end if;
		end if;
	end loop;

	-- Recursos que foram pagos no t�tulo a pagar
	for r_c_recursos in c_recursos(:new.nr_titulo) loop

		if	(ie_tipo_evento_w	= 'EX') then
			delete	pls_monitor_tiss_alt
			where	nr_seq_conta	= r_c_recursos.nr_seq_conta
			and	ie_tipo_evento	= 'PR';
		else
			ie_gera_w := 'S';

			if	(ie_tipo_evento_w = 'PC') then
				ie_gera_w := pls_obter_se_pgto_monit(r_c_recursos.nr_seq_conta);
			end if;

			if	(ie_gera_w = 'S') then
				pls_tiss_gravar_log_monitor(	'PR',
								r_c_recursos.nr_seq_conta,
								null,
								null,
								r_c_recursos.nr_seq_conta_rec,
								null,
								null,
								null,
								null,
								null,
								null,
								wheb_usuario_pck.get_nm_usuario,
								:new.dt_liquidacao);
			end if;
		end if;
	end loop;

	--Reembolso
	if	(:new.nr_seq_reembolso is not null) then
		for r_c_reembolso in c_reembolso(:new.nr_seq_reembolso) loop
			if	(ie_tipo_evento_w	= 'EX') then
				delete	pls_monitor_tiss_alt
				where	nr_seq_conta	= r_c_reembolso.nr_seq_conta
				and	ie_tipo_evento	= 'PC';
			else
				ie_gera_w := 'S';

				if	(ie_tipo_evento_w = 'PC') then
					ie_gera_w := pls_obter_se_pgto_monit(r_c_reembolso.nr_seq_conta);
				end if;

				if	(ie_gera_w = 'S') then
					pls_tiss_gravar_log_monitor(	'PC',
									r_c_reembolso.nr_seq_conta,
									r_c_reembolso.nr_seq_conta_proc,
									r_c_reembolso.nr_seq_conta_mat,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									wheb_usuario_pck.get_nm_usuario,
									:new.dt_liquidacao);
				end if;
			end if;
		end loop;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_after_update
after update ON TASY.TITULO_PAGAR for each row
declare

qt_reg_w		number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/*OS 1430325 - Envio de classificacao de titulos a pagar*/
select  count(*)
into    qt_reg_w
from    intpd_fila_transmissao
where   nr_seq_documento                = to_char(:new.nr_titulo)
and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
and		ie_evento in ('53','179');

if (qt_reg_w = 0) then
	reg_integracao_w.cd_estab_documento    := :new.cd_estabelecimento;
	reg_integracao_w.ie_tipo_titulo_cpa    := :new.ie_tipo_titulo;
	gerar_int_padrao.gravar_integracao('53', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);

	/*OS 1602222 - Enviar status do titulo quando for liquidado.*/
	if (:new.ie_situacao = 'L') then
		gerar_int_padrao.gravar_integracao('179', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
	end if;
end if;

/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',nvl(:new.dt_vencimento_atual,:new.dt_vencimento_original),'A',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_befdelete
before delete ON TASY.TITULO_PAGAR for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TP',nvl(:old.dt_vencimento_atual,:old.dt_vencimento_original),'E',:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_update
before update ON TASY.TITULO_PAGAR for each row
declare

qt_reg_w			number(1);
ie_moeda_dif_w		varchar2(100);
cd_moeda_padrao_w	number(15);
dt_contabil_new_w		date;
dt_contabil_old_w		date;
ie_titulo_nf_w		varchar2(50);
ie_cancel_tit_a600_w	varchar2(2);
nr_seq_lote_camara_w	pls_lote_camara_comp.nr_sequencia%type;
dt_baixa_w		titulo_pagar_baixa.dt_baixa%type;
nr_sequencia_w		proj_desp_viagem_mes.nr_sequencia%type;
nr_seq_viagem_w		via_relat_desp.nr_seq_viagem%type;
ds_call_stack_w		varchar2(2000);

Cursor C01 is
	select	nr_seq_viagem
	from	via_relat_desp
	where	nr_seq_fech_proj = nr_sequencia_w;

BEGIN

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	obter_param_usuario(5511, 4, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_titulo_nf_w);

	if	(nvl(ie_titulo_nf_w,'N') = 'S') then

		select	nvl(max(a.dt_entrada_saida),:new.dt_contabil),
			nvl(max(a.dt_entrada_saida),:old.dt_contabil)
		into	dt_contabil_new_w,
			dt_contabil_old_w
		from	nota_fiscal a
		where	a.nr_sequencia	= :new.nr_seq_nota_fiscal;

	else
		dt_contabil_new_w		:= :new.dt_contabil;
		dt_contabil_old_w		:= :old.dt_contabil;
	end if;

	select	max(nvl(ie_moeda_dif, 'S')),
		max(cd_moeda_padrao)
	into	ie_moeda_dif_w,
		cd_moeda_padrao_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= :new.cd_estabelecimento;

	if	(ie_moeda_dif_w = 'N') and (cd_moeda_padrao_w <> :new.cd_moeda) then
		--r.aise_application_error(-20011, 'A moeda nao pode ser alterada para moeda diferente da moeda padrao!');
		wheb_mensagem_pck.exibir_mensagem_abort(267337);
	end if;

	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;
	if	(:new.vl_saldo_titulo < 0) or (:new.vl_titulo < 0) then
		--r.aise_application_error(-20011, 'O valor do titulo nao pode ser negativo!');
		wheb_mensagem_pck.exibir_mensagem_abort(267338);
	end if;

	if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null))
		or
		((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
		--R.AISE_APPLICATION_ERROR(-20011, 'O titulo deve ser de uma pessoa fisica ou juridica!');
		wheb_mensagem_pck.exibir_mensagem_abort(267339);
	end if;

	/* Alteracao da data emissao */
	if	((fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_emissao,'CP',:new.ie_tipo_titulo,null,null,null) = 'N') or
		 (fin_obter_se_mes_aberto(:new.cd_estabelecimento, :old.dt_emissao,'CP',:new.ie_tipo_titulo,null,null,null) = 'N')) and
		(:old.dt_emissao <> :new.dt_emissao) then
		--R.aise_application_error(-20011, 'O mes/dia financeiro do titulo ja esta fechado! Nao e possivel alterar a data de emissao.');
		wheb_mensagem_pck.exibir_mensagem_abort(267340);
	end if;

	if (trunc(:old.dt_emissao,'dd') <> trunc(:new.dt_emissao,'dd')) and (trunc(:new.dt_emissao,'dd') > trunc(:new.dt_vencimento_atual,'dd')) then
		wheb_mensagem_pck.exibir_mensagem_abort(471952);
	end if;

	if (:new.dt_liquidacao is not null) and
		(trunc(:old.dt_liquidacao,'dd') <> trunc(:new.dt_liquidacao,'dd')) and
		(trunc(:new.dt_liquidacao,'dd') < trunc(:new.dt_emissao,'dd')) then
		wheb_mensagem_pck.exibir_mensagem_abort(471972);
	end if;

	/* Alteracao da data contabil */
	if	((fin_obter_se_mes_aberto(:new.cd_estabelecimento, :new.dt_contabil,'CP',:new.ie_tipo_titulo,null,null,null) = 'N') or
		(fin_obter_se_mes_aberto(:new.cd_estabelecimento, :old.dt_contabil,'CP',:new.ie_tipo_titulo,null,null,null) = 'N')) and
		(:old.dt_contabil <> :new.dt_contabil) then
		--R.aise_application_error(-20011, 'O mes/dia financeiro do titulo ja esta fechado! Nao e possivel alterar a data contabil.');
		wheb_mensagem_pck.exibir_mensagem_abort(267341);
	end if;

	/* Cancelamento */
	if	(((dt_contabil_new_w is not null) and
		  (fin_obter_se_mes_aberto(:new.cd_estabelecimento, dt_contabil_new_w, 'CP',:new.ie_tipo_titulo,null,null,null) = 'N')) or
		 ((dt_contabil_old_w is not null) and
		  (fin_obter_se_mes_aberto(:new.cd_estabelecimento, dt_contabil_old_w, 'CP',:new.ie_tipo_titulo,null,null,null) = 'N'))) and
		(:new.ie_situacao = 'C') and
		(:old.ie_situacao <> :new.ie_situacao) then
		--R.aise_application_error(-20011, 'O mes/dia financeiro do titulo ja esta fechado! Nao e possivel cancelar o mesmo.');
		wheb_mensagem_pck.exibir_mensagem_abort(267342);
	end if;

	if (	dt_contabil_new_w	<>	dt_contabil_old_w) then
		/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),dt_contabil_new_w);
	end if;

	/* Consistir data de vencimento */
	if	(:new.dt_vencimento_atual <> :old.dt_vencimento_atual) then
		CONSISTIR_REGRA_DATA_VENC_CP(:new.cd_estabelecimento, :new.dt_inclusao, :new.dt_vencimento_atual, :new.nm_usuario);
	end if;

	--aldellandrea
	if	(:new.ie_origem_titulo = '19') and
		(:new.ie_situacao = 'C')then

		select	max(nvl(ie_permite_canc_tit_vinc_a600, 'S'))
		into	ie_cancel_tit_a600_w
		from	pls_parametros_camara
		where	cd_estabelecimento = :new.cd_estabelecimento;

		if	(ie_cancel_tit_a600_w = 'N') then

			select  max(y.nr_sequencia)
			into	nr_seq_lote_camara_w
			from 	pls_titulo_lote_camara z,
				pls_lote_camara_comp x,
				ptu_camara_compensacao y
			where 	x.nr_sequencia 		= y.nr_seq_lote_camara
			and 	x.nr_sequencia 		= z.nr_seq_lote_camara
			and 	z.nr_titulo_pagar  	= :new.nr_titulo;

			if	(nr_seq_lote_camara_w is not null) then
				Wheb_mensagem_pck.exibir_mensagem_abort(373291, 'NR_TITULO=' || :new.nr_titulo ||
										';NR_LOTE=' || nr_seq_lote_camara_w);
			end if;

		end if;
	end if;
	--fim
	/*Tratamento para atualizar a gestao de viagens*/
	if	(:new.ie_situacao = 'L') then
		begin

		begin
		select	max(dt_baixa)
		into	dt_baixa_w
		from	titulo_pagar_baixa
		where	nr_titulo = :new.nr_titulo;
		exception
		when others then
			dt_baixa_w := null;
		end;

		if	(dt_baixa_w is not null) then
			begin
			begin
			select	nr_sequencia
			into	nr_sequencia_w
			from	proj_desp_viagem_mes
			where	nr_titulo = :new.nr_titulo
			and	rownum = 1;
			exception
			when others then
				nr_sequencia_w := null;
			end;

			if	(nr_sequencia_w is not null) then
				begin
				open C01;
				loop
				fetch C01 into
					nr_seq_viagem_w;
				exit when C01%notfound;
					begin

					begin
					update	via_viagem
					set	ie_etapa_viagem = '6',
						nm_usuario_fim_viagem = :new.nm_usuario,
						dt_fim_viagem = dt_baixa_w,
						dt_baixa_titulo = dt_baixa_w,
						nm_usuario = :new.nm_usuario,
						dt_atualizacao = sysdate
					where	nr_sequencia = nr_seq_viagem_w;
					exception
					when others then
						nr_seq_viagem_w := null;
					end;

					end;
				end loop;
				close C01;
				end;
			end if;

			end;
		end if;

		end;
	end if;

	/*OS 1458006 - Gravar log de alteracao para o campo NR_SEQ_TRANS_FIN_BAIXA.
	Log para auxiliar o Suporte. Qualquer excessao aqui dentro nao disparar.*/
	begin
		if (:new.nr_seq_trans_fin_baixa <> :old.nr_seq_trans_fin_baixa) then

			ds_call_stack_w := substr('Old value: '||:old.nr_seq_trans_fin_baixa||' New value: '||:new.nr_seq_trans_fin_baixa||' CallStack Update: ' || substr(dbms_utility.format_call_stack,1,1950),1,2000);

			if (ds_call_stack_w is not null) then

				insert into log_tasy ( cd_log,
									   ds_log,
									   nm_usuario,
									   dt_atualizacao )
							values   ( 75395,
									   ds_call_stack_w,
									   :new.nm_usuario,
									   sysdate );
			end if;

		end if;
	exception when others then
		null;
	end;

end if;

<<Final>>

qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.SSJ_ENVIAR_EMAIL_DEVOLUCAO
AFTER INSERT
ON TASY.TITULO_PAGAR 

FOR EACH ROW
DECLARE

cd_estabelecimento_w number(10);
ie_tipo_titulo_w varchar2(10);
ds_texto_w varchar2(4000);
nr_titulo_w number(10);
nr_seq_segurado_w number(10);
nm_titular_w varchar2(300);
nm_segurado_w varchar2(300);
ds_dados_pagamento_w varchar2(500);
vl_titulo_w number(15,2);
ds_observacao_w varchar2(4000);

ie_origem_nota_w varchar2(5);
nr_seq_nota_credito_w number(10);
ds_pagador_w varchar2(255);
ds_observacao_nota_w varchar2(4000);
nr_titulo_receber_w number(10);
ds_historico_w varchar2(255);
ds_email_cco_w varchar2(255);
qt_historico_w number(10);

BEGIN
   
    cd_estabelecimento_w := :new.cd_estabelecimento;
    ie_tipo_titulo_w := :new.ie_tipo_titulo;
    nr_seq_nota_credito_w := :new.nr_seq_nota_credito;

    if(cd_estabelecimento_w = 2 and ie_tipo_titulo_w = '21' and nr_seq_nota_credito_w is not null)then
             
        nr_titulo_w := :new.nr_titulo;
        vl_titulo_w := :new.vl_titulo;
        
        select OBTER_NOME_PF_PJ(:new.cd_pessoa_fisica, :new.cd_cgc)
        into ds_pagador_w
        from dual;
        
        select  ie_origem,
                ds_observacao,
                nr_titulo_receber
        into    ie_origem_nota_w,
                ds_observacao_nota_w,
                nr_titulo_receber_w
        from NOTA_CREDITO
        where nr_sequencia = nr_seq_nota_credito_w;
        
       
        ds_texto_w:='Um t�tulo a pagar foi gerado atrav�s de uma nota de cr�dito';
        
         
        if(ie_origem_nota_w in('M','CE'))then
        
        --raise_application_error(-20001,'foi ate aqui '||nr_seq_nota_credito_w);
        
            select count(*)
            into qt_historico_w
            from NOTA_CREDITO_HIST
            where nr_sequencia = 
            (
                select max(nr_sequencia) 
                from NOTA_CREDITO_HIST 
                where nr_seq_nota_credito = nr_seq_nota_credito_w
            ); 
        
            if(qt_historico_w >0)then
            
                select nm_usuario||': '||ds_historico
                into ds_historico_w
                from NOTA_CREDITO_HIST
                where nr_sequencia = 
                (
                    select max(nr_sequencia) 
                    from NOTA_CREDITO_HIST 
                    where nr_seq_nota_credito = nr_seq_nota_credito_w
                );
                
            end if;
            
            
            ds_texto_w:= ds_texto_w||'

T�tulo: '||nr_titulo_w||'   Valor: '||vl_titulo_w||'     

Pagador: '|| ds_pagador_w ||'

Observa��o da nota de cr�dito: '||ds_observacao_nota_w;

            
            if(nr_titulo_receber_w is not null)then
                ds_texto_w:= ds_texto_w||', do t�tulo a receber '||nr_titulo_receber_w;
            end if;

                
            if(ds_historico_w is not null)then
                ds_texto_w:= ds_texto_w||'

Observa��o do usu�rio '||ds_historico_w;
            end if;
            

            ds_texto_w:= ds_texto_w||'

T�tulo gerado pelo usu�rio: '|| :new.nm_usuario ||', na data: '||:new.dt_atualizacao;


        elsif(ie_origem_nota_w = 'RC')then
        
        
            select  nvl(substr(obter_nome_pf(a.CD_PF_DEVOLUCAO),1,100),substr(obter_nome_pf(b.CD_PESSOA_FISICA),1,100))nm_pessoa,
                    case when a.cd_banco is not null then
                        'Banco: '||(select g.ds_BANCO from banco g where g.cd_banco = a.cd_banco)||'   Ag�ncia: '||a.CD_AGENCIA_BANCARIA||'/'||a.IE_DIGITO_AGENCIA||'   Conta: '||a.CD_CONTA||'/'||a.IE_DIGITO_CONTA 
                    end dados_pagamento,
                    a.DS_OBSERVACAO
            into    nm_titular_w,
                    ds_dados_pagamento_w,
                    ds_observacao_w        
            from PLS_SOLICITACAO_RESCISAO a
            left join PLS_SOLIC_RESC_CONTATO b on(a.nr_sequencia = b.nr_seq_solicitacao)
            join PLS_SOLIC_RESCISAO_FIN c on(c.nr_seq_solicitacao = a.nr_sequencia)
            join PLS_SOLIC_RESC_FIN_VENC d on(d.nr_seq_solic_resc_fin = c.nr_sequencia)
            where d.nr_seq_nota_credito = nr_seq_nota_credito_w;
        
    
        ds_texto_w:=ds_texto_w||'
     
T�tulo: '||nr_titulo_w||'  Valor: '||vl_titulo_w||'

Pagador do t�tulo: '||ds_pagador_w||'

T�tular Respons�vel: '||nm_titular_w;


        if(ds_dados_pagamento_w is not null)then
            ds_texto_w:= ds_texto_w||'          

Informa��es de Pagamento 
'||ds_dados_pagamento_w;
        end if;

        if(ds_observacao_w is not null)then
            ds_texto_w:= ds_texto_w||'           

Observa��o: '||ds_observacao_w;
        end if;


        ds_texto_w:= ds_texto_w||'

T�tulo gerado pelo usu�rio: '|| :new.nm_usuario ||', na data: '||:new.dt_atualizacao;

        end if;
    
        select DS_EMAIL
        into ds_email_cco_w
        from USUARIO
        where upper(NM_USUARIO) = upper(:new.nm_usuario);

        if(ds_email_cco_w is not null)then
            enviar_email('Gera��o de T�tulo a Pagar - Nota de Cr�dito',ds_texto_w,'noreply@ssjose.com.br','contabilidade@ssjose.com.br','dwonatam.ssj','M',ds_email_cco_w);
        else
            enviar_email('Gera��o de T�tulo a Pagar - Nota de Cr�dito',ds_texto_w,'noreply@ssjose.com.br','contabilidade@ssjose.com.br','dwonatam.ssj','M');
        end if;
        
    end if;
    
END SSJ_ENVIAR_EMAIL_DEVOLUCAO;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_lib_update
before update ON TASY.TITULO_PAGAR for each row
declare

qt_reg_w			number(1);
ie_att_libs_w		varchar2(1);
ds_resultado_p		varchar2(1);

pragma autonomous_transaction;
BEGIN
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then

	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;

	obter_param_usuario(851,214,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_att_libs_w);

	if (ie_att_libs_w = 'C') then
		ATUALIZAR_LIB_TITULO_PAGAR(:new.nr_titulo, :new.nm_usuario, ds_resultado_p);

		if	(ds_resultado_p	= 'S') then
			:new.dt_liberacao 	:= sysdate;
			:new.nm_usuario_lib 	:= :new.nm_usuario;
		elsif(ds_resultado_p = 'N') then
			:new.dt_liberacao 	:= null;
			:new.nm_usuario_lib 	:= null;
		end if;
	end if;

end if;

<<Final>>

commit;

qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_insert
BEFORE INSERT ON TASY.TITULO_PAGAR FOR EACH ROW
declare

ie_titulo_nf_w		varchar2(1);
ie_ajusta_emissao_w	varchar2(1);
dt_emissao_w		date;
qt_dias_venc_w		varchar2(255);
ie_tit_lote_repasse_w	varchar2(255);
ie_consistir_cpf_w	varchar2(1);
ie_titulo_zerado_w	varchar2(1);
cd_empresa_w		number(10,0);
nr_lote_contabil_w	number(15,0);
nr_seq_regra_w		number(10);
ie_liberar_w		varchar2(1);
ds_resultado_w		varchar2(1);
ie_brasileiro_w		varchar2(10);
qt_dias_venc_uteis_w	varchar(255);
dt_mes_competencia_w	date;
dt_geracao_titulos_w 	date;
ie_data_lote_prod_med_w	pls_parametro_contabil.ie_data_lote_prod_med%type;
nr_fatura_w		ptu_fatura.nr_sequencia%type;
ds_call_stack_w		varchar2(2000);
ie_contab_trib_nota_w 	varchar2(1);
ie_origem_tit_w		titulo_pagar.ie_origem_titulo%type;

pragma autonomous_transaction;
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

obter_param_usuario(5511,4,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_titulo_nf_w);
obter_param_usuario(851,258,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_ajusta_emissao_w);

select	nvl(max(ie_consistir_cpf),'N'),
        nvl(max(ie_titulo_zerado),'S'),
        nvl(max(ie_contab_trib_nota),'N')
into	ie_consistir_cpf_w,
        ie_titulo_zerado_w,
        ie_contab_trib_nota_w
from 	parametros_contas_pagar
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(nvl(ie_titulo_nf_w,'N') = 'S') then
	select	nvl(max(a.dt_entrada_saida),:new.dt_emissao)
	into	dt_emissao_w
	from	nota_fiscal a
	where	a.nr_sequencia	= :new.nr_seq_nota_fiscal;
else
	dt_emissao_w := :new.dt_emissao;
end if;

if 	(nvl(ie_ajusta_emissao_w,'N') = 'S') and (trunc(:new.dt_vencimento_atual,'dd') < trunc(sysdate,'dd')) and
	(trunc(:new.dt_emissao,'dd') > trunc(:new.dt_vencimento_atual,'dd')) then
	:new.dt_emissao := :new.dt_vencimento_atual;
end if;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, dt_emissao_w,'CP',:new.ie_tipo_titulo,null,null,null) = 'N') then
	/* O mes/dia financeiro de emissao do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes/dia.
	Dt emissao: dt_emissao_w */
	wheb_mensagem_pck.exibir_mensagem_abort(191637,	'DT_EMISSAO_W=' || to_char(dt_emissao_w, 'dd/mm/yyyy'));
end if;

select  max(ie_origem_titulo)
into    ie_origem_tit_w
from    titulo_pagar
where   nr_titulo = :new.nr_titulo_original;

if ((ie_origem_tit_w in (2,4,7,14,21,10,23,24,26)) or ((ie_origem_tit_w = 1) and (ie_contab_trib_nota_w = 'S'))) then
    /* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
    philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),NVL(:new.dt_contabil,dt_emissao_w));
end if;

if	(fin_obter_se_mes_aberto(:new.cd_estabelecimento, NVL(:new.dt_contabil,dt_emissao_w),'CP',:new.ie_tipo_titulo,null,null,null) = 'N') then
	/* O mes/dia financeiro da data contabil do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes/dia. */
	wheb_mensagem_pck.exibir_mensagem_abort(191638);
end if;

if	(:new.vl_saldo_titulo < 0) or (:new.vl_titulo < 0) then
	/* O valor do titulo nao pode ser negativo! */
	wheb_mensagem_pck.exibir_mensagem_abort(191639);
end if;

if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null))
	or
	((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
	/* O titulo deve ser de uma pessoa fisica ou juridica! */
	wheb_mensagem_pck.exibir_mensagem_abort(191640);
end if;

if (trunc(:new.dt_emissao,'dd') > trunc(:new.dt_vencimento_atual,'dd')) then
	wheb_mensagem_pck.exibir_mensagem_abort(471962);
end if;

obter_param_usuario(851,120,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,qt_dias_venc_w);

if	(nvl(to_number(qt_dias_venc_w),0) > 0) and
	((trunc(:new.dt_vencimento_atual) - trunc(:new.dt_emissao)) < to_number(qt_dias_venc_w)) then
	/* Nao foi possivel salvar o titulo! O vencimento deve ser qt_dias_venc_w dias maior do que a emissao.
	Parametro [120] da funcao Titulos a Pagar */
	wheb_mensagem_pck.exibir_mensagem_abort(191641,	'QT_DIAS_VENC_W=' || qt_dias_venc_w);
end if;

obter_param_usuario(851,249,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,qt_dias_venc_uteis_w);

if	(nvl(to_number(qt_dias_venc_uteis_w),0) > 0) and
	((trunc(:new.dt_vencimento_atual,'dd') <=  trunc(obter_dia_util_periodo(:new.cd_estabelecimento, sysdate, to_number(qt_dias_venc_uteis_w)),'dd')) ) then
	wheb_mensagem_pck.exibir_mensagem_abort(324340,	'QT_DIAS_VENC_UTEIS_W=' || qt_dias_venc_uteis_w);
end if;

if	(ie_consistir_cpf_w = 'S') and
	((:new.cd_pessoa_fisica is not null) and (:new.cd_cgc is null)) and
	(Obter_Cpf_Pessoa_Fisica(:new.cd_pessoa_fisica) is null) then
	/* Pessoa fisica sem CPF cadastrado! */
	ie_brasileiro_w := obter_se_brasileiro(:new.cd_pessoa_fisica);
	if ie_brasileiro_w = 'S' then
		wheb_mensagem_pck.exibir_mensagem_abort(191642);
	end if;
end if;

if	(ie_titulo_zerado_w = 'N') and
	(:new.vl_titulo = 0) then
	/* O valor do titulo nao pode ser zero! Verifique os parametros do contas a pagar. */
	wheb_mensagem_pck.exibir_mensagem_abort(191643);
end if;

obter_param_usuario(89,88,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_tit_lote_repasse_w);

if	((nvl(ie_tit_lote_repasse_w, 'S') = 'N') or
	(nvl(ie_tit_lote_repasse_w, 'S') = 'M')) and
	(:new.nr_repasse_terceiro is not null) then

	select	max(cd_empresa)
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = :new.cd_estabelecimento;

	select	nvl(max(a.nr_lote_contabil), 0)
	into	nr_lote_contabil_w
	from	lote_contabil a
	where	a.cd_tipo_lote_contabil				= 16
	and	obter_empresa_estab(a.cd_estabelecimento) 	= cd_empresa_w
	and	decode(ie_tit_lote_repasse_w,'N',trunc(a.dt_referencia,'dd'),trunc(a.dt_referencia,'mm')) = decode(ie_tit_lote_repasse_w,'N',trunc(nvl(:new.dt_contabil, :new.dt_emissao),'dd'),trunc(nvl(:new.dt_contabil, :new.dt_emissao),'mm'))
	and	exists	(	select	1
				from	movimento_contabil z
				where	z.nr_lote_contabil	= a.nr_lote_contabil);

	if	(nr_lote_contabil_w > 0) then
		if	(nvl(ie_tit_lote_repasse_w, 'S') = 'N') then
		/* Ja existe o lote contabil nr_lote_contabil_w para a data contabil/emissao deste titulo (dt_contabil_w)! Parametro [88] */
		wheb_mensagem_pck.exibir_mensagem_abort(191644,	'NR_LOTE_CONTABIL_W=' || to_char(nr_lote_contabil_w) ||
								';DT_CONTABIL_W=' || to_char(nvl(:new.dt_contabil, :new.dt_emissao), 'dd/mm/yyyy'));
		elsif	(nvl(ie_tit_lote_repasse_w, 'S') = 'M') then
		/* Ja existe o lote contabil nr_lote_contabil_w para o mes contabil/emissao deste titulo (dt_contabil_w)! Parametro [88] */
		wheb_mensagem_pck.exibir_mensagem_abort(275220,	'NR_LOTE_CONTABIL_W=' || to_char(nr_lote_contabil_w) ||
								';DT_CONTABIL_W=' || to_char(nvl(:new.dt_contabil, :new.dt_emissao), 'dd/mm/yyyy'));
		end if;
	end if;
end if;

obter_regra_lib_tit_pagar(	:new.nr_seq_nota_fiscal,
			:new.vl_titulo,
			:new.cd_cgc,
			:new.cd_estabelecimento,
			:new.nm_usuario,
			nr_seq_regra_w,
			ie_liberar_w,
			ds_resultado_w,
			dt_emissao_w,
			:new.nr_seq_proj_rec,
			:new.ie_tipo_titulo,
			:new.nr_seq_classe,
			:new.ie_origem_titulo);

/* ahoffelder - OS 311355 - 19/04/2011 - liberar o titulo automaticamente */
if	(ds_resultado_w	= 'S') and (ie_liberar_w	= 'S') then
	:new.dt_liberacao	:= sysdate;
	:new.nm_usuario_lib	:= :new.nm_usuario;
end if;

:new.nr_lote_contabil	:= nvl(:new.nr_lote_contabil,0);
:new.DT_INCLUSAO	:= :new.dt_atualizacao;
:new.NM_USUARIO_ORIG	:= :new.nm_usuario;

/*fcastro - os 938490 - 25/09/2015 - carrregar dt_contabil do titulo a pagar para origem de titulo ops - pagamento de producao medica e ops - intercambio*/

/*ops - pagamento de producao medica*/
if	(:new.ie_origem_titulo = '20' and :new.nr_seq_pls_pag_prest <> 0)then
	select 	max(dt_mes_competencia),
		max(dt_geracao_titulos)
	into 	dt_mes_competencia_w,
		dt_geracao_titulos_w
	from 	pls_lote_pagamento lp,
		pls_pagamento_prestador pg
	where 	:new.nr_seq_pls_pag_prest = pg.nr_sequencia
	and 	pg.nr_seq_lote = lp.nr_sequencia;

	select 	max(ie_data_lote_prod_med)
	into 	ie_data_lote_prod_med_w
	from 	pls_parametro_contabil
    	where 	cd_estabelecimento = :new.cd_estabelecimento;

	if(ie_data_lote_prod_med_w = 'C')then
		:new.dt_contabil := dt_mes_competencia_w;
	else
		:new.dt_contabil := nvl(dt_geracao_titulos_w,sysdate);
	end if;

elsif (:new.ie_origem_titulo = '16') then /*ops - intercambio*/

	select	max(nr_sequencia),
		max(dt_mes_competencia)
	into	nr_fatura_w,
		dt_mes_competencia_w
	from	ptu_fatura
	where	nr_titulo = :new.nr_titulo;

	if (nr_fatura_w <> 0) then
		:new.dt_contabil :=	dt_mes_competencia_w;
	end if;

end if;

if	(:new.vl_titulo_atual is null) then
	:new.vl_titulo_atual := :new.vl_titulo;
end if;

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);

/*OS 1458006 - Gravar log de alteracao para o campo NR_SEQ_TRANS_FIN_BAIXA.
Log para auxiliar o Suporte. Qualquer excessao aqui dentro nao disparar.*/
begin
	if (:new.nr_seq_trans_fin_baixa is not null) then

		ds_call_stack_w := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,1980),1,2000);

		if (ds_call_stack_w is not null) then
			insert into log_tasy ( cd_log,
								   ds_log,
								   nm_usuario,
								   dt_atualizacao )
						values   ( 75395,
								   ds_call_stack_w,
								   :new.nm_usuario,
								   sysdate );
			commit;
		end if;

	end if;
exception when others then
	null;
end;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_afterinsert
AFTER INSERT or UPDATE ON TASY.TITULO_PAGAR FOR EACH ROW
declare

vl_movimento_w		ctb_documento.vl_movimento%type;
dt_documento_w		ctb_documento.dt_atualizacao%type;
ie_contab_curto_prazo_w parametros_contas_pagar.ie_contab_curto_prazo%type;
nr_seq_trans_financ_w	ctb_documento.nr_seq_trans_financ%type;
qt_reg_w		number(10);
reg_integracao_w	gerar_int_padrao.reg_integracao;
ie_situacao_ctb_w	ctb_documento.ie_situacao_ctb%type  := 'P';
nr_seq_info_ctb_w	ctb_documento.nr_seq_info%type;
ie_concil_contab_w	pls_visible_false.ie_concil_contab%type;

cursor c01 is
	select	a.nm_atributo,
		7 cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 7
	and 	a.nm_atributo in ( 'VL_TITULO', 'VL_CURTO_PRAZO', 'VL_LONGO_PRAZO')
	union all
	select	a.nm_atributo,
		37 cd_tipo_lote_contab
	from	atributo_contab a
	where	a.cd_tipo_lote_contab = 37
	and	a.nm_atributo = 'VL_TITULO_PAGAR'
	and	:new.ie_origem_titulo = '19'
	and	ie_concil_contab_w = 'S'
	and	((:new.nr_seq_pls_lote_contest is not null) or (:new.nr_seq_pls_lote_disc is not null))
	and	:new.ie_situacao <> 'C';
c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
begin
select  nvl(ie_contab_curto_prazo, 'N')
into    ie_contab_curto_prazo_w
from    parametros_contas_pagar
where   cd_estabelecimento = :new.cd_estabelecimento;
exception when others then
ie_contab_curto_prazo_w := 'N';
end;

begin
select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = :new.cd_estabelecimento;
exception when others then
	ie_concil_contab_w := 'N';
end;

begin
select  x.ie_contab_curto_prazo
into    ie_contab_curto_prazo_w
from    (
        select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_curto_prazo, 'N'), 'N') ie_contab_curto_prazo
    from    ctb_param_lote_contas_pag a
    where   a.cd_empresa = obter_empresa_estab(:new.cd_estabelecimento)
    and     nvl(a.cd_estab_exclusivo, :new.cd_estabelecimento) = :new.cd_estabelecimento
    order by (nvl(a.cd_estab_exclusivo,0)) desc ) x
where rownum = 1;
exception when others then
  ie_contab_curto_prazo_w	:= 'N';
end;


    if	(inserting) then
    begin

        /*OS 1430325 - Envio de titulos a pagar*/
        select  count(*)
        into    qt_reg_w
        from    intpd_fila_transmissao
        where   nr_seq_documento                = to_char(:new.nr_titulo)
        and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
        and	ie_evento in ('53');

        if	(qt_reg_w = 0) then
		reg_integracao_w.cd_estab_documento    := :new.cd_estabelecimento;
		reg_integracao_w.ie_tipo_titulo_cpa    := :new.ie_tipo_titulo;
            	gerar_int_padrao.gravar_integracao('53', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
        end if;

        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',nvl(:new.dt_vencimento_atual,:new.dt_vencimento_original),'I',:new.nm_usuario);

        if(nvl(:new.nr_repasse_terceiro, 0) = 0) then
        begin
            open c01;
            loop
            fetch c01 into
                c01_w;
            exit when c01%notfound;
           	begin

                vl_movimento_w:= :new.vl_titulo;
                dt_documento_w:= null;
		if 	(c01_w.cd_tipo_lote_contab = 7) then
			if	(c01_w.nm_atributo = 'VL_TITULO') then
				begin
				dt_documento_w		:= :new.dt_contabil;
				nr_seq_trans_financ_w	:= :new.nr_seq_trans_fin_contab;
				end;
			elsif	((c01_w.nm_atributo = 'VL_CURTO_PRAZO') or (c01_w.nm_atributo = 'VL_LONGO_PRAZO')) then
				begin
				vl_movimento_w	:= null;
				if	(ie_contab_curto_prazo_w = 'S') then
					begin
					dt_documento_w:= :new.dt_contabil;
					if	(c01_w.nm_atributo = 'VL_CURTO_PRAZO') then
						begin
						dt_documento_w:= pkg_date_utils.add_month(:new.dt_vencimento_atual,-12,0);
						end;
					end if;

					if	(pkg_date_utils.add_month(:new.dt_vencimento_atual,-12,0) >= :new.dt_contabil) and (c01_w.nm_atributo = 'VL_CURTO_PRAZO')
						or (:new.dt_vencimento_atual > pkg_date_utils.add_month(:new.dt_contabil,12,0)) and (c01_w.nm_atributo = 'VL_LONGO_PRAZO') then
						begin
						if	(nvl(:new.nr_seq_tf_curto_prazo, 0) <> 0) then
							begin
							nr_seq_trans_financ_w	:= :new.nr_seq_tf_curto_prazo;
							vl_movimento_w		:= :new.vl_titulo;
							end;
						end if;
						end;
					end if;
					end;
				end if;
				end;
			end if;
		elsif	(c01_w.cd_tipo_lote_contab = 37) then
			vl_movimento_w		:= :new.vl_titulo;
			dt_documento_w		:= :new.dt_contabil;
			nr_seq_trans_financ_w	:= :new.nr_seq_trans_fin_contab;
			nr_seq_info_ctb_w	:= case c01_w.nm_atributo
						   when 'VL_RECEBIDO' then 14
						   when 'VL_REC_GLOSA' then 14
						   when	'VL_PAGO' then 13
						   when 'VL_PAG_GLOSA' then 13
						   when 'VL_TITULO_RECEBER' then 46
						   when 'VL_TITULO_PAGAR' then 47
						   when 'VL_REC_MAIOR'	then 14
						   end;

			ctb_concil_financeira_pck.ctb_gravar_documento  (  	:new.cd_estabelecimento,
										dt_documento_w,
										37,
										nr_seq_trans_financ_w,
										nr_seq_info_ctb_w,
										:new.nr_titulo,
										null,
										null,
										vl_movimento_w,
										'TITULO_PAGAR',
										c01_w.nm_atributo,
										:new.nm_usuario);
		end if;

		if	(nvl(vl_movimento_w, 0) <> 0 and c01_w.cd_tipo_lote_contab <> 37) then
			begin
			ie_situacao_ctb_w  := 'P';
			if	(:new.nr_seq_trans_fin_contab is not null) and
				(c01_w.nm_atributo  = 'VL_TITULO') then
				select  decode(count(a.nr_sequencia),0,'P','N')
				into  ie_situacao_ctb_w
				from  trans_financ_contab a
				where  a.nr_seq_trans_financ  = :new.nr_seq_trans_fin_contab
				and  a.ie_regra_conta  = 'INF'
				and  a.nm_atributo    = c01_w.nm_atributo;

				if 	(ie_situacao_ctb_w = 'P') then
					begin
					select  decode(count(a.nr_sequencia),0,'P','N')
					into  ie_situacao_ctb_w
					from  trans_financ_contab a
					where  a.nr_seq_trans_financ  = :new.nr_seq_trans_fin_contab
					and  a.ie_regra_centro_custo  = 'INF'
					and  a.nm_atributo    = c01_w.nm_atributo;
					end;
				end if;

			end if;

			ctb_concil_financeira_pck.ctb_gravar_documento  (  	:new.cd_estabelecimento,
										dt_documento_w,
										7,
										nr_seq_trans_financ_w,
										2,
										:new.nr_titulo,
										null,
										null,
										vl_movimento_w,
										'TITULO_PAGAR',
										c01_w.nm_atributo,
										:new.nm_usuario,
										ie_situacao_ctb_w,
										'');


			end;
		end if;
		end;
	end loop;
	close c01;
        end;
        end if;
    end;
    end if;

    if	(updating) then
        begin
	ie_situacao_ctb_w  := 'P';
	if	(:new.nr_seq_trans_fin_contab is not null) and
		(nvl(:new.nr_seq_trans_fin_contab,0) != nvl(:old.nr_seq_trans_fin_contab,0)) then
		select  decode(count(a.nr_sequencia),0,'P','N')
		into  ie_situacao_ctb_w
		from  trans_financ_contab a
		where  a.nr_seq_trans_financ  = :new.nr_seq_trans_fin_contab
		and  a.ie_regra_conta  = 'INF'
		and  a.nm_atributo    = 'VL_TITULO';

		if	(ie_situacao_ctb_w = 'P') then
			begin
			select  decode(count(a.nr_sequencia),0,'P','N')
			into  ie_situacao_ctb_w
			from  trans_financ_contab a
			where  a.nr_seq_trans_financ  = :new.nr_seq_trans_fin_contab
			and  a.ie_regra_centro_custo  = 'INF'
			and  a.nm_atributo    = 'VL_TITULO';
			end;
		end if;
 	end if;

        update  ctb_documento
        set  	nr_seq_trans_financ   = :new.nr_seq_trans_fin_contab,
             	ie_situacao_ctb    = decode(ie_situacao_ctb,'P',ie_situacao_ctb_w,ie_situacao_ctb)
        where  	nr_documento     = :new.nr_titulo
        and	nvl(nr_seq_doc_compl, 0)= 0
        and	nm_tabela     = 'TITULO_PAGAR'
        and 	nm_atributo in ('VL_TITULO')
        and	nvl(nr_lote_contabil,0)  = 0;

        update	ctb_documento
        set	nr_seq_trans_financ = :new.nr_seq_tf_curto_prazo
        where	nr_documento = :new.nr_titulo
        and 	nvl(nr_seq_doc_compl, 0) = 0
        and	nm_tabela = 'TITULO_PAGAR'
        and 	nm_atributo in ('VL_CURTO_PRAZO', 'VL_LONGO_PRAZO')
  	and	nvl(nr_lote_contabil,0)	= 0;

        end;
    end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_ptu_fatura
before update ON TASY.TITULO_PAGAR for each row
declare

ds_aplicacao_w		varchar2(255);
nr_seq_ptu_fatura_w	ptu_fatura.nr_sequencia%type;

begin
if	(:new.ie_origem_titulo = '16') then
	if	(pls_se_aplicacao_tasy = 'S') then
		ds_aplicacao_w 		:= 'Aplicacao TASY ;';
	else
		ds_aplicacao_w 		:= 'Banco ;';
	end if;

	insert into tasy_log_alteracao
		(nr_sequencia,
		nm_tabela,
		ds_chave_simples,
		ds_chave_composta,
		dt_atualizacao,
		nm_usuario,
		ds_descricao,
		nr_seq_justificativa,
		ds_justificativa)
	values(	tasy_log_alteracao_seq.nextval,
		'TITULO_PAGAR',
		:new.nr_titulo,
		null,
		sysdate,
		:new.nm_usuario,
		substr(	'NR_TITULO='||:new.nr_titulo||',IE_STATUS_OLD='||:old.ie_status||', IE_STATUS_NEW='||:new.ie_status||
			', M�quina='||wheb_usuario_pck.get_machine||' - '||ds_aplicacao_w,1,255),
		null,
		null);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_tp  after update ON TASY.TITULO_PAGAR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_TITULO);  ds_c_w:=null; ds_w:=substr(:new.CD_TRIBUTO,1,500);gravar_log_alteracao(substr(:old.CD_TRIBUTO,1,4000),substr(:new.CD_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TRIBUTO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_PAGAMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PAGAMENTO,1,4000),substr(:new.IE_TIPO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PAGAMENTO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_SALDO_JUROS,1,500);gravar_log_alteracao(substr(:old.VL_SALDO_JUROS,1,4000),substr(:new.VL_SALDO_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'VL_SALDO_JUROS',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.TX_MULTA,1,500);gravar_log_alteracao(substr(:old.TX_MULTA,1,4000),substr(:new.TX_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_MULTA',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_CONTABIL,1,500);gravar_log_alteracao(to_char(:old.DT_CONTABIL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CONTABIL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CONTABIL',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_RPA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_RPA,1,4000),substr(:new.NR_SEQ_RPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RPA',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TRANS_FIN_BAIXA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FIN_BAIXA,1,4000),substr(:new.NR_SEQ_TRANS_FIN_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FIN_BAIXA',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_EMISSAO,1,500);gravar_log_alteracao(to_char(:old.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_SALDO_MULTA,1,500);gravar_log_alteracao(substr(:old.VL_SALDO_MULTA,1,4000),substr(:new.VL_SALDO_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_SALDO_MULTA',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.TX_JUROS,1,500);gravar_log_alteracao(substr(:old.TX_JUROS,1,4000),substr(:new.TX_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'TX_JUROS',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_BORDERO,1,500);gravar_log_alteracao(substr(:old.NR_BORDERO,1,4000),substr(:new.NR_BORDERO,1,4000),:new.nm_usuario,nr_seq_w,'NR_BORDERO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_TITULO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_TITULO,1,4000),substr(:new.IE_TIPO_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TITULO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_NOSSO_NUMERO,1,500);gravar_log_alteracao(substr(:old.NR_NOSSO_NUMERO,1,4000),substr(:new.NR_NOSSO_NUMERO,1,4000),:new.nm_usuario,nr_seq_w,'NR_NOSSO_NUMERO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_DOCUMENTO,1,500);gravar_log_alteracao(substr(:old.NR_DOCUMENTO,1,4000),substr(:new.NR_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOCUMENTO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_BLOQUETO,1,500);gravar_log_alteracao(substr(:old.NR_BLOQUETO,1,4000),substr(:new.NR_BLOQUETO,1,4000),:new.nm_usuario,nr_seq_w,'NR_BLOQUETO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO_TITULO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO_TITULO,1,4000),substr(:new.DS_OBSERVACAO_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO_TITULO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TRANS_FIN_CONTAB,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FIN_CONTAB,1,4000),substr(:new.NR_SEQ_TRANS_FIN_CONTAB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FIN_CONTAB',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_TITULO,1,500);gravar_log_alteracao(substr(:old.NR_TITULO,1,4000),substr(:new.NR_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_TIPO_BAIXA_NEG,1,500);gravar_log_alteracao(substr(:old.CD_TIPO_BAIXA_NEG,1,4000),substr(:new.CD_TIPO_BAIXA_NEG,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_BAIXA_NEG',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_OUTROS_ACRESCIMOS,1,500);gravar_log_alteracao(substr(:old.VL_OUTROS_ACRESCIMOS,1,4000),substr(:new.VL_OUTROS_ACRESCIMOS,1,4000),:new.nm_usuario,nr_seq_w,'VL_OUTROS_ACRESCIMOS',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PROJ_REC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PROJ_REC,1,4000),substr(:new.NR_SEQ_PROJ_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJ_REC',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CLASSE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSE,1,4000),substr(:new.NR_SEQ_CLASSE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSE',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_OUTRAS_DESPESAS,1,500);gravar_log_alteracao(substr(:old.VL_OUTRAS_DESPESAS,1,4000),substr(:new.VL_OUTRAS_DESPESAS,1,4000),:new.nm_usuario,nr_seq_w,'VL_OUTRAS_DESPESAS',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORIGEM_TITULO,1,500);gravar_log_alteracao(substr(:old.IE_ORIGEM_TITULO,1,4000),substr(:new.IE_ORIGEM_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_TITULO',ie_log_w,ds_w,'TITULO_PAGAR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TITULO_PAGAR ADD (
  CONSTRAINT TITPAGA_PK
 PRIMARY KEY
 (NR_TITULO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TITPAGA_UK
 UNIQUE (NR_SEQ_RPA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR ADD (
  CONSTRAINT TITPAGA_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT TITPAGA_PTUNDEB_FK 
 FOREIGN KEY (NR_SEQ_NOTA_DEBITO) 
 REFERENCES TASY.PTU_NOTA_DEBITO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PTUNOCC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_DEB_CONCLUSAO) 
 REFERENCES TASY.PTU_NOTA_DEB_CONCLUSAO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITPAGA_PLSPGRU_FK 
 FOREIGN KEY (NR_SEQ_PROC_GRU) 
 REFERENCES TASY.PLS_PROCESSO_GRU (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSSEGU_FK2 
 FOREIGN KEY (NR_SEQ_SEG_RESCISAO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_EMPFINPAR_FK 
 FOREIGN KEY (NR_SEQ_PARC_EMPREST) 
 REFERENCES TASY.EMPREST_FINANC_PARC (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_REEMBOLSO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_BORDNC_FK 
 FOREIGN KEY (NR_SEQ_BORDERO_NC) 
 REFERENCES TASY.BORDERO_NC (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSE) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSPAGP_FK 
 FOREIGN KEY (NR_SEQ_PLS_PAG_PREST) 
 REFERENCES TASY.PLS_PAGAMENTO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_GPIPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_GPI) 
 REFERENCES TASY.GPI_PLANO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_LOTENCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENC_CONTAS) 
 REFERENCES TASY.LOTE_ENCONTRO_CONTAS (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_CTBTRIB_FK 
 FOREIGN KEY (NR_SEQ_CTB_TRIBUTO) 
 REFERENCES TASY.CTB_TRIBUTO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TITRETR_FK 
 FOREIGN KEY (NR_SEQ_TIT_REC_TRIB) 
 REFERENCES TASY.TITULO_RECEBER_TRIB (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TITPAGA_FK2 
 FOREIGN KEY (NR_TITULO_DEST) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT TITPAGA_FISTRIBPRE_FK 
 FOREIGN KEY (NR_SEQ_TRIB_PRESU) 
 REFERENCES TASY.FIS_TRIBUTO_PRESUMIDO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSSEME_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO_MENS) 
 REFERENCES TASY.PLS_SEGURADO_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_LOTCONT_FK3 
 FOREIGN KEY (NR_LOTE_CONTABIL_CURTO_PRAZO) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAGA_TRAFINA_FK3 
 FOREIGN KEY (NR_SEQ_TF_CURTO_PRAZO) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSPPVT_FK 
 FOREIGN KEY (NR_SEQ_PLS_VENC_TRIB) 
 REFERENCES TASY.PLS_PAG_PREST_VENC_TRIB (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_ADIDEV_FK 
 FOREIGN KEY (NR_ADIANT_REC, NR_SEQ_ADIANT_DEV) 
 REFERENCES TASY.ADIANTAMENTO_DEV (NR_ADIANTAMENTO,NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_TRANSF) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT TITPAGA_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PLS_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT TITPAGA_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANT_PAGO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO),
  CONSTRAINT TITPAGA_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_BORPAGA_FK 
 FOREIGN KEY (NR_BORDERO) 
 REFERENCES TASY.BORDERO_PAGAMENTO (NR_BORDERO),
  CONSTRAINT TITPAGA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TITPAGA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAGA_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_FISCAL) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TITPAGA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TITPAGA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_REPTERC_FK 
 FOREIGN KEY (NR_REPASSE_TERCEIRO) 
 REFERENCES TASY.REPASSE_TERCEIRO (NR_REPASSE_TERCEIRO),
  CONSTRAINT TITPAGA_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_CONTAB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TITTEIM_FK 
 FOREIGN KEY (NR_SEQ_TRIBUTO) 
 REFERENCES TASY.TITULO_PAGAR_IMPOSTO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_TRANSF_TRIB) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAGA_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_FINANCEIRO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TITPAGA_PLSPCTI_FK 
 FOREIGN KEY (NR_SEQ_PROT_CONTA) 
 REFERENCES TASY.PLS_PROT_CONTA_TITULO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA_NEG) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA),
  CONSTRAINT TITPAGA_BANCO_FK 
 FOREIGN KEY (CD_BANCO_PORTADOR) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT TITPAGA_PLSLOPO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RES_PLS) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITPAGA_PLSOTPS_FK 
 FOREIGN KEY (NR_SEQ_TAXA_SAUDE) 
 REFERENCES TASY.PLS_TPS (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSPEON_FK 
 FOREIGN KEY (NR_SEQ_PROVISAO) 
 REFERENCES TASY.PLS_PEONA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_LOAUTIRE_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT) 
 REFERENCES TASY.LOTE_AUDIT_TIT_REC (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_DLDISTR_FK 
 FOREIGN KEY (NR_SEQ_DISTRIBUICAO) 
 REFERENCES TASY.DL_DISTRIBUICAO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_PLSDELC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_EMPRESA) 
 REFERENCES TASY.PLS_DESC_LOTE_COMISSAO (NR_SEQUENCIA),
  CONSTRAINT TITPAGA_NOTCRED_FK 
 FOREIGN KEY (NR_SEQ_NOTA_CREDITO) 
 REFERENCES TASY.NOTA_CREDITO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_PAGAR TO NIVEL_1;

