ALTER TABLE TASY.MAN_TIPO_ORDEM_SERV_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_TIPO_ORDEM_SERV_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_TIPO_ORDEM_SERV_LIB
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_TIPO_ORDEM_SERV     NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_TRABALHO      NUMBER(10),
  NR_SEQ_GRUPO_PLANEJAMENTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANTOSL_MANGRPL_FK_I ON TASY.MAN_TIPO_ORDEM_SERV_LIB
(NR_SEQ_GRUPO_PLANEJAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTOSL_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTOSL_MANGRTR_FK_I ON TASY.MAN_TIPO_ORDEM_SERV_LIB
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTOSL_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANTOSL_MANTIOS_FK_I ON TASY.MAN_TIPO_ORDEM_SERV_LIB
(NR_SEQ_TIPO_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANTOSL_PK ON TASY.MAN_TIPO_ORDEM_SERV_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANTOSL_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_TIPO_ORDEM_SERV_LIB ADD (
  CONSTRAINT MANTOSL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_TIPO_ORDEM_SERV_LIB ADD (
  CONSTRAINT MANTOSL_MANTIOS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ORDEM_SERV) 
 REFERENCES TASY.MAN_TIPO_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT MANTOSL_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANTOSL_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJAMENTO) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_TIPO_ORDEM_SERV_LIB TO NIVEL_1;

