ALTER TABLE TASY.MAN_ROTA_INSP_ATIV_REAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ROTA_INSP_ATIV_REAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ROTA_INSP_ATIV_REAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ROTA_INSPECAO  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_USUARIO_EXEC       VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATIVIDADE          DATE                    NOT NULL,
  DT_FIM_ATIVIDADE      DATE,
  QT_MINUTO             NUMBER(10)              NOT NULL,
  DS_ATIVIDADE          VARCHAR2(2000 BYTE),
  NR_SEQ_FUNCAO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANRIAR_MANROIN_FK_I ON TASY.MAN_ROTA_INSP_ATIV_REAL
(NR_SEQ_ROTA_INSPECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRIAR_MANROIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANRIAR_MANTIFU_FK_I ON TASY.MAN_ROTA_INSP_ATIV_REAL
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRIAR_MANTIFU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANRIAR_PK ON TASY.MAN_ROTA_INSP_ATIV_REAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRIAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.MANRIAR_USUARIO_FK_I ON TASY.MAN_ROTA_INSP_ATIV_REAL
(NM_USUARIO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRIAR_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.MAN_ROTA_INSP_ATIV_REAL ADD (
  CONSTRAINT MANRIAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ROTA_INSP_ATIV_REAL ADD (
  CONSTRAINT MANRIAR_MANROIN_FK 
 FOREIGN KEY (NR_SEQ_ROTA_INSPECAO) 
 REFERENCES TASY.MAN_ROTA_INSPECAO (NR_SEQUENCIA),
  CONSTRAINT MANRIAR_MANTIFU_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.MAN_TIPO_FUNCAO (NR_SEQUENCIA),
  CONSTRAINT MANRIAR_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_EXEC) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.MAN_ROTA_INSP_ATIV_REAL TO NIVEL_1;

