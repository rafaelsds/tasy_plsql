ALTER TABLE TASY.CONTA_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_CONVENIO
(
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  DT_ACERTO_CONTA          DATE                 NOT NULL,
  IE_STATUS_ACERTO         NUMBER(1)            NOT NULL,
  DT_PERIODO_INICIAL       DATE                 NOT NULL,
  DT_PERIODO_FINAL         DATE                 NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_LOTE_CONVENIO         VARCHAR2(20 BYTE),
  DS_PROCED_PARAMETRO      VARCHAR2(240 BYTE),
  CD_SETOR_PARAMETRO       NUMBER(5),
  IE_TIPO_ATEND_PARAMETRO  NUMBER(3),
  DT_MESANO_REFERENCIA     DATE,
  DT_MESANO_CONTABIL       DATE,
  DT_VENCIMENTO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CONCONV_PK ON TASY.CONTA_CONVENIO
(CD_CONVENIO, DT_ACERTO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCONV_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONTA_CONVENIO ADD (
  CONSTRAINT CONCONV_PK
 PRIMARY KEY
 (CD_CONVENIO, DT_ACERTO_CONTA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_CONVENIO ADD (
  CONSTRAINT CONCONV_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CONTA_CONVENIO TO NIVEL_1;

