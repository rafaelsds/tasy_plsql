ALTER TABLE TASY.CHECKUP_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHECKUP_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHECKUP_ETAPA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CHECKUP       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_ETAPA         NUMBER(10)               NOT NULL,
  DT_PREVISTA          DATE                     NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  DT_CONFIRMACAO       DATE,
  NM_USUARIO_CONF      VARCHAR2(15 BYTE),
  NM_CONTATO_CONF      VARCHAR2(60 BYTE),
  DT_INICIO_REAL       DATE,
  DT_FIM_ETAPA         DATE,
  IE_FORMA_CONF        VARCHAR2(3 BYTE),
  DT_CANCELAMENTO      DATE,
  NM_USUARIO_CANCEL    VARCHAR2(15 BYTE),
  NM_CONTATO_CANCEL    VARCHAR2(60 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_PERFIL_ADICIONAL  VARCHAR2(255 BYTE),
  DS_SETOR_ADICIONAL   VARCHAR2(255 BYTE),
  NM_USUARIO_DESTINO   VARCHAR2(2000 BYTE),
  NR_SEQ_STATUS_DOC    NUMBER(10),
  IE_CHAMADO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEETAP_CHECKUP_FK_I ON TASY.CHECKUP_ETAPA
(NR_SEQ_CHECKUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEETAP_I1 ON TASY.CHECKUP_ETAPA
(CD_PESSOA_FISICA, NR_SEQ_CHECKUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEETAP_PESFISI_FK_I ON TASY.CHECKUP_ETAPA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEETAP_PESFISI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CHEETAP_PK ON TASY.CHECKUP_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEETAP_STDOCHE_FK_I ON TASY.CHECKUP_ETAPA
(NR_SEQ_STATUS_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEETAP_STDOCHE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.checkup_etapa_atual
before insert or update ON TASY.CHECKUP_ETAPA for each row
declare

pragma autonomous_transaction;

qt_max_paciente_w 	number(10);
qt_paciente_etapa_w	number(10);
nr_seq_checkup_w 	number(10);
dt_ini_real_w	date;
dt_fim_etapa_w	date;
dt_cancel_etapa_w	date;
nm_paciente_w	varchar2(255);

begin

if	((:old.dt_inicio_real is null) and (:new.dt_inicio_real is not null)) then
	begin
	select	count(*)
	into	qt_paciente_etapa_w
	from	checkup_etapa a
	where	a.nr_seq_etapa = :new.nr_seq_etapa
	and	a.dt_inicio_real is not null
	and	trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate);

	select 	nvl(a.qt_maxima_paciente,0)
	into	qt_max_paciente_w
	from  	etapa_checkup a
	where 	a.nr_sequencia = :new.nr_seq_etapa;

	select	max(a.nr_seq_checkup),
		max(a.dt_inicio_real),
		max(a.dt_fim_etapa),
		max(a.dt_cancelamento)
	into	nr_seq_checkup_w,
		dt_ini_real_w,
		dt_fim_etapa_w,
		dt_cancel_etapa_w
	from	checkup_etapa a
	where	a.nr_seq_etapa = :new.nr_seq_etapa
	and	a.dt_inicio_real is not null
	and	trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate);

	if	(qt_max_paciente_w > 0) and
		(qt_paciente_etapa_w >= qt_max_paciente_w) and
		(nr_seq_checkup_w is not null) and
		(dt_ini_real_w is not null) and
		(dt_fim_etapa_w is null) and
		(dt_cancel_etapa_w is null) then
		begin
		select	max(obter_nome_pf(a.cd_pessoa_fisica))
		into	nm_paciente_w
		from	checkup a
		where	a.nr_sequencia = nr_seq_checkup_w;
		-- Essa etapa esta sendo ocupada pela pelo paciente '|| nm_paciente_w ||'#@#@'
		Wheb_mensagem_pck.exibir_mensagem_abort(264554,'NM_PACIENTE_W=' || NM_PACIENTE_W);
		end;
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CHECKUP_ETAPA_LOG
After INSERT or UPDATE or DELETE ON TASY.CHECKUP_ETAPA FOR EACH ROW
DECLARE


ds_old_values_w		varchar2(4000) := '';
ds_new_values_w		varchar2(4000) := '';
nm_atributo_w		varchar2(50);
ds_label_w			varchar2(50);
ie_tipo_atributo_w	varchar2(10);
ie_tipo_log_w		varchar2(10);


nm_usuario_w		varchar2(50);
nr_sequencia_w		number(10);
nr_seq_checkup_w	checkup.nr_sequencia%type;

cursor c01 is
	select NM_ATRIBUTO,
		decode(CD_EXP_LABEL, null, NM_ATRIBUTO, OBTER_DESC_EXPRESSAO(CD_EXP_LABEL)) DS_LABEL,
		IE_TIPO_ATRIBUTO
	from tabela_atributo
	where nm_tabela = 'CHECKUP_ETAPA'
	  and ie_tipo_atributo not in ('FUNCTION', 'VISUAL')
	order by nm_atributo;

BEGIN
if (Obter_Valor_Param_Usuario(996, 70, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
	if UPDATING then
		open c01;
		loop
		fetch c01 into
			nm_atributo_w,
			ds_label_w,
			ie_tipo_atributo_w;
		exit when c01%notfound;

			if (UPDATING (nm_atributo_w)) then

			if (nm_atributo_w = 'DT_ATUALIZACAO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_ATUALIZACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_ATUALIZACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'NM_USUARIO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO, 1, 4000);
			elsif (nm_atributo_w = 'NR_SEQ_CHECKUP') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_CHECKUP, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_CHECKUP, 1, 4000);
			elsif (nm_atributo_w = 'NR_SEQ_ETAPA') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_ETAPA, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_ETAPA, 1, 4000);
			elsif (nm_atributo_w = 'CD_PESSOA_FISICA') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_PESSOA_FISICA, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_PESSOA_FISICA, 1, 4000);
			elsif (nm_atributo_w = 'DT_CANCELAMENTO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_CANCELAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_CANCELAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'NM_USUARIO_CANCEL') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_CANCEL, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_CANCEL, 1, 4000);
			elsif (nm_atributo_w = 'NM_CONTATO_CANCEL') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_CONTATO_CANCEL, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_CONTATO_CANCEL, 1, 4000);
			elsif (nm_atributo_w = 'DT_PREVISTA') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_PREVISTA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_PREVISTA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'DT_CONFIRMACAO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_CONFIRMACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_CONFIRMACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'NM_USUARIO_CONF') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_CONF, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_CONF, 1, 4000);
			elsif (nm_atributo_w = 'NM_CONTATO_CONF') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_CONTATO_CONF, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_CONTATO_CONF, 1, 4000);
			elsif (nm_atributo_w = 'DT_INICIO_REAL') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_INICIO_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_INICIO_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'NR_SEQUENCIA') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQUENCIA, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQUENCIA, 1, 4000);
			elsif (nm_atributo_w = 'DT_ATUALIZACAO_NREC') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_ATUALIZACAO_NREC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_ATUALIZACAO_NREC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'NM_USUARIO_NREC') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_NREC, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_NREC, 1, 4000);
			elsif (nm_atributo_w = 'IE_FORMA_CONF') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_FORMA_CONF, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_FORMA_CONF, 1, 4000);
			elsif (nm_atributo_w = 'DT_FIM_ETAPA') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_FIM_ETAPA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_FIM_ETAPA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
			elsif (nm_atributo_w = 'DS_PERFIL_ADICIONAL') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.DS_PERFIL_ADICIONAL, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.DS_PERFIL_ADICIONAL, 1, 4000);
			elsif (nm_atributo_w = 'DS_SETOR_ADICIONAL') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.DS_SETOR_ADICIONAL, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.DS_SETOR_ADICIONAL, 1, 4000);
			elsif (nm_atributo_w = 'NM_USUARIO_DESTINO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_DESTINO, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_DESTINO, 1, 4000);
			elsif (nm_atributo_w = 'NR_SEQ_STATUS_DOC') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_STATUS_DOC, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_STATUS_DOC, 1, 4000);
			elsif (nm_atributo_w = 'IE_CHAMADO') then
			  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_CHAMADO, 1, 4000);
			  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_CHAMADO, 1, 4000);
			end if;

			end if;

		end loop;
		close c01;
		if (nvl(length(ds_old_values_w), 0) > 0 or nvl(length(ds_new_values_w), 0) > 0) then

			ds_old_values_w := substr(ds_old_values_w, 4, 4000);
			ds_new_values_w := substr(ds_new_values_w, 4, 4000);

			insert into log_checkup(
				nr_sequencia,
				dt_log,
				nm_usuario,
				ie_tipo_log,
				nm_tabela,
				nr_seq_tabela,
				ds_log_anterior,
				ds_log_novo,
				ds_obs_anterior,
				ds_obs_novo,
				cd_estabelecimento,
				CD_SETOR_ATENDIMENTO)
			values (
				log_checkup_seq.nextval,
				sysdate,
				:new.nm_usuario,
				'UPDATE',
				'CHECKUP_ETAPA',
				:NEW.nr_sequencia,
				ds_old_values_w,
				ds_new_values_w,
				null,
				null,
				obter_estabelecimento_ativo,
				obter_setor_ativo);
		end if;

	else
		if INSERTING then
			ie_tipo_log_w := 'INSERT';
			nm_usuario_w := :new.nm_usuario;
			nr_sequencia_w := :new.nr_sequencia;
			nr_seq_checkup_w := :new.nr_seq_checkup;
		elsif DELETING then
			ie_tipo_log_w := 'DELETE';
			nm_usuario_w := :old.nm_usuario;
			nr_sequencia_w := :old.nr_sequencia;
			nr_seq_checkup_w := :old.nr_seq_checkup;
		end if;

		select	decode(CD_EXP_LABEL, null, NM_ATRIBUTO, OBTER_DESC_EXPRESSAO(CD_EXP_LABEL)) || ': ' || nr_seq_checkup_w DS_LABEL
		into ds_new_values_w
		from tabela_atributo
		where nm_tabela = 'CHECKUP_ETAPA'
		  and nm_atributo = 'NR_SEQ_CHECKUP';

		insert into log_checkup(
			nr_sequencia,
			dt_log,
			nm_usuario,
			ie_tipo_log,
			nm_tabela,
			nr_seq_tabela,
			ds_log_anterior,
			ds_log_novo,
			ds_obs_anterior,
			ds_obs_novo,
			cd_estabelecimento,
			CD_SETOR_ATENDIMENTO)
		values	(
			log_checkup_seq.nextval,
			sysdate,
			nm_usuario_w,
			ie_tipo_log_w,
			'CHECKUP_ETAPA',
			nr_sequencia_w,
			null,
			ds_new_values_w,
			null,
			null,
			obter_estabelecimento_ativo,
			obter_setor_ativo);

	end if;
end if;
END;
/


ALTER TABLE TASY.CHECKUP_ETAPA ADD (
  CONSTRAINT CHEETAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHECKUP_ETAPA ADD (
  CONSTRAINT CHEETAP_CHECKUP_FK 
 FOREIGN KEY (NR_SEQ_CHECKUP) 
 REFERENCES TASY.CHECKUP (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CHEETAP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHEETAP_STDOCHE_FK 
 FOREIGN KEY (NR_SEQ_STATUS_DOC) 
 REFERENCES TASY.STATUS_DOC_CHECKUP (NR_SEQUENCIA));

GRANT SELECT ON TASY.CHECKUP_ETAPA TO NIVEL_1;

