ALTER TABLE TASY.PESSOA_DOC_FOTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_DOC_FOTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_DOC_FOTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_PESSOA_DOC        NUMBER(10),
  IM_DOCUMENTO             LONG RAW,
  DS_TITULO                VARCHAR2(255 BYTE),
  NR_SEQ_APRES             NUMBER(5),
  NR_SEQ_AGEINT_ITEM       NUMBER(10),
  IE_APRESENTAR_AUTOATEND  VARCHAR2(1 BYTE),
  NR_SEQ_PRESCR_PROC       NUMBER(6),
  DT_ENTREGA               DATE,
  DT_VALIDADE              DATE,
  CD_IDENTIFICACAO         VARCHAR2(100 BYTE),
  IM_DOCUMENTO_BLOB        BLOB,
  IE_FORMA_UPLOAD          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (IM_DOCUMENTO_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESDOCF_PESDOCU_FK_I ON TASY.PESSOA_DOC_FOTO
(NR_SEQ_PESSOA_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESDOCF_PESDOCU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESDOCF_PK ON TASY.PESSOA_DOC_FOTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_DOC_FOTO_tp  after update ON TASY.PESSOA_DOC_FOTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_APRESENTAR_AUTOATEND,1,4000),substr(:new.IE_APRESENTAR_AUTOATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRESENTAR_AUTOATEND',ie_log_w,ds_w,'PESSOA_DOC_FOTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'PESSOA_DOC_FOTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'PESSOA_DOC_FOTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_DOC_FOTO ADD (
  CONSTRAINT PESDOCF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_DOC_FOTO ADD (
  CONSTRAINT PESDOCF_PESDOCU_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_DOC) 
 REFERENCES TASY.PESSOA_DOCUMENTACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_DOC_FOTO TO NIVEL_1;

