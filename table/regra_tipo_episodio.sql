ALTER TABLE TASY.REGRA_TIPO_EPISODIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TIPO_EPISODIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TIPO_EPISODIO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DS_MENSAGEM               VARCHAR2(2000 BYTE),
  IE_POSSUI_CID             VARCHAR2(1 BYTE),
  IE_STATUS                 VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_EPISODIO      NUMBER(10),
  QT_DIAS_EPISODIO          NUMBER(5),
  QT_VISITAS                NUMBER(3),
  IE_COMPORTAMENTO          VARCHAR2(1 BYTE),
  NR_SEQ_SUBTIPO_EPISODIO   NUMBER(10),
  QT_SIMULTANEO             NUMBER(3),
  QT_DIAS_INICIO_EPISODIO   NUMBER(5),
  IE_TIPO_ATENDIMENTO       NUMBER(3),
  IE_TIPO_CONVENIO          NUMBER(2),
  NR_SEQ_CLASSIFICACAO      NUMBER(10),
  NR_SEQ_QUEIXA             NUMBER(10),
  NR_SEQ_TIPO_ADMISSAO_FAT  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RETIPEPI_CASUEP_FK_I ON TASY.REGRA_TIPO_EPISODIO
(NR_SEQ_SUBTIPO_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RETIPEPI_CATPEP_FK_I ON TASY.REGRA_TIPO_EPISODIO
(NR_SEQ_TIPO_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RETIPEPI_CLAATEN_FK_I ON TASY.REGRA_TIPO_EPISODIO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RETIPEPI_PK ON TASY.REGRA_TIPO_EPISODIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RETIPEPI_QUEPACI_FK_I ON TASY.REGRA_TIPO_EPISODIO
(NR_SEQ_QUEIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_TIPO_EPISODIO ADD (
  CONSTRAINT RETIPEPI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TIPO_EPISODIO ADD (
  CONSTRAINT RETIPEPI_CASUEP_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_EPISODIO) 
 REFERENCES TASY.SUBTIPO_EPISODIO (NR_SEQUENCIA),
  CONSTRAINT RETIPEPI_CATPEP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EPISODIO) 
 REFERENCES TASY.TIPO_EPISODIO (NR_SEQUENCIA),
  CONSTRAINT RETIPEPI_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT RETIPEPI_QUEPACI_FK 
 FOREIGN KEY (NR_SEQ_QUEIXA) 
 REFERENCES TASY.QUEIXA_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_TIPO_EPISODIO TO NIVEL_1;

