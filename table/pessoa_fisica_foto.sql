ALTER TABLE TASY.PESSOA_FISICA_FOTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_FOTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_FOTO
(
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  IM_PESSOA_FISICA            LONG RAW          NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_VALIDADE                 DATE,
  IE_FOTO_PERM_TWS            VARCHAR2(1 BYTE),
  IE_FORMA_UPLOAD             VARCHAR2(1 BYTE)  DEFAULT null,
  IE_SINCRONIZADO_REC_FACIAL  VARCHAR2(1 BYTE),
  DT_ULT_SINCR_REC_FACIAL     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFIFO_I1 ON TASY.PESSOA_FISICA_FOTO
(CD_PESSOA_FISICA, IE_SINCRONIZADO_REC_FACIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFIFO_PK ON TASY.PESSOA_FISICA_FOTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_foto_atual
before insert or update ON TASY.PESSOA_FISICA_FOTO for each row
declare

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
		if	(inserting) or
			(:old.dt_atualizacao <> :new.dt_atualizacao) then
			:new.ie_sincronizado_rec_facial := 'N';
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_pessoa_fisica_foto
after insert or update ON TASY.PESSOA_FISICA_FOTO 
for each row
declare

nr_seq_pessoa_doc_w         number(10);
nr_seq_pessoa_doc_foto_w    number(10);
jobno                       number;

begin

    nr_seq_pessoa_doc_w:= pessoa_documentacao_seq.nextval;
    nr_seq_pessoa_doc_foto_w := pessoa_doc_foto_seq.nextval;
    
    insert into pessoa_documentacao
    (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        cd_pessoa_fisica,
        nr_seq_documento,
        ie_entregue,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        ie_situacao,
        ie_doc_externo
    )
    values
    (
        nr_seq_pessoa_doc_w,
        sysdate,
        :new.nm_usuario,
        :new.cd_pessoa_fisica,
        43,
        'N',
        sysdate,
        :new.nm_usuario,
        'A',
        'N'
    );
    
    insert into pessoa_doc_foto
    (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_pessoa_doc,
        ds_titulo,
        ie_apresentar_autoatend
    )
    values
    (
        nr_seq_pessoa_doc_foto_w,
        sysdate,
        :new.nm_usuario,
        sysdate,
        :new.nm_usuario,
        nr_seq_pessoa_doc_w,
        'Foto Paciente',
        'S'
    );

    dbms_job.submit(jobno, 'copia_campo_long_raw(  ''pessoa_fisica_foto'',
                           ''im_pessoa_fisica'',
                           ''and cd_pessoa_fisica = '||:new.cd_pessoa_fisica||''',
                           ''pessoa_doc_foto'',
                           ''im_documento'',
                           ''and nr_sequencia = '||nr_seq_pessoa_doc_foto_w||''',
                           ''S''); ');
end;
/


ALTER TABLE TASY.PESSOA_FISICA_FOTO ADD (
  CONSTRAINT PESFIFO_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_FOTO ADD (
  CONSTRAINT PESFIFO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PESSOA_FISICA_FOTO TO NIVEL_1;

