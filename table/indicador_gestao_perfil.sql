ALTER TABLE TASY.INDICADOR_GESTAO_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INDICADOR_GESTAO_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.INDICADOR_GESTAO_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PERFIL            NUMBER(5)                NOT NULL,
  NR_SEQ_INDICADOR     NUMBER(10)               NOT NULL,
  NR_SEQ_APRESENT      NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDGEPE_INDGEST_FK_I ON TASY.INDICADOR_GESTAO_PERFIL
(NR_SEQ_INDICADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDGEPE_PERFIL_FK_I ON TASY.INDICADOR_GESTAO_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDGEPE_PK ON TASY.INDICADOR_GESTAO_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INDICADOR_GESTAO_PERFIL_tp  after update ON TASY.INDICADOR_GESTAO_PERFIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENT,1,4000),substr(:new.NR_SEQ_APRESENT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENT',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INDICADOR,1,4000),substr(:new.NR_SEQ_INDICADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INDICADOR',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'INDICADOR_GESTAO_PERFIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.INDICADOR_GESTAO_PERFIL ADD (
  CONSTRAINT INDGEPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INDICADOR_GESTAO_PERFIL ADD (
  CONSTRAINT INDGEPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT INDGEPE_INDGEST_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.INDICADOR_GESTAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.INDICADOR_GESTAO_PERFIL TO NIVEL_1;

