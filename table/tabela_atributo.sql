ALTER TABLE TASY.TABELA_ATRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_ATRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_ATRIBUTO
(
  NM_TABELA                 VARCHAR2(50 BYTE)   NOT NULL,
  NM_ATRIBUTO               VARCHAR2(50 BYTE)   NOT NULL,
  NR_SEQUENCIA_CRIACAO      NUMBER(10)          NOT NULL,
  IE_OBRIGATORIO            VARCHAR2(1 BYTE),
  IE_TIPO_ATRIBUTO          VARCHAR2(10 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_TAMANHO                NUMBER(10),
  QT_DECIMAIS               NUMBER(10),
  IE_CRIAR_ALTERAR          VARCHAR2(1 BYTE),
  DS_ATRIBUTO               VARCHAR2(4000 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  QT_TAMANHO_CALCULO        NUMBER(5),
  QT_SEQ_INICIO             NUMBER(10),
  QT_SEQ_INCREMENTO         NUMBER(10),
  DT_CRIACAO                DATE                NOT NULL,
  VL_DEFAULT                VARCHAR2(50 BYTE),
  IE_COMPONENTE             VARCHAR2(5 BYTE),
  DS_VALORES                VARCHAR2(2000 BYTE),
  CD_DOMINIO                NUMBER(5),
  QT_TAM_DELPHI             NUMBER(4),
  DS_LABEL                  VARCHAR2(60 BYTE),
  NR_SEQ_APRESENT           NUMBER(3),
  DS_LABEL_GRID             VARCHAR2(50 BYTE),
  NR_SEQ_ORDEM              NUMBER(3),
  QT_COLUNA                 NUMBER(2),
  DS_MASCARA                VARCHAR2(42 BYTE),
  QT_DESLOC_DIREITA         NUMBER(3),
  QT_TAM_LABEL              NUMBER(3),
  QT_TAM_GRID               NUMBER(5,2),
  IE_READONLY               VARCHAR2(1 BYTE),
  IE_TABSTOP                VARCHAR2(1 BYTE),
  NR_SEQ_LOCALIZADOR        NUMBER(10),
  QT_ALTURA                 NUMBER(3),
  IE_CRIAR_DESCRICAO        VARCHAR2(1 BYTE)    NOT NULL,
  NM_ATRIBUTO_PAI           VARCHAR2(60 BYTE),
  IE_LOG_EXCLUSAO           VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_TABSTOP            NUMBER(3),
  NR_SEQ_GRID               NUMBER(3),
  NR_SEQ_ORDEM_SERV         NUMBER(10),
  DS_FILTER                 VARCHAR2(60 BYTE),
  DS_LABEL_LONGO            VARCHAR2(2000 BYTE),
  IE_CONVERTE_NEGATIVO      VARCHAR2(1 BYTE),
  QT_CACHE                  NUMBER(10),
  IE_LOG_UPDATE             VARCHAR2(1 BYTE)    NOT NULL,
  IE_ATUALIZAR_VERSAO       VARCHAR2(1 BYTE)    NOT NULL,
  IE_STATUS                 VARCHAR2(1 BYTE),
  DS_COR                    VARCHAR2(15 BYTE),
  IE_CRIAR_DESC_FK          VARCHAR2(1 BYTE),
  QT_TAM_FONTE              NUMBER(3),
  DS_ESTILO_FONTE           VARCHAR2(30 BYTE),
  IE_REGRA_UNID_MEDIDA      VARCHAR2(1 BYTE),
  IE_APLICABILIDADE_ESTILO  VARCHAR2(1 BYTE),
  IE_UNIDADE_MEDIDA         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_DEFAULT_BANCO          VARCHAR2(1 BYTE),
  IE_DBL_CLICK_READONLY     VARCHAR2(1 BYTE),
  IE_TIPO_BOTAO             VARCHAR2(3 BYTE),
  IE_TOTALIZAR              VARCHAR2(15 BYTE),
  DS_COMANDO                VARCHAR2(2000 BYTE),
  CD_EXP_DESC               NUMBER(10),
  CD_EXP_LABEL              NUMBER(10),
  CD_EXP_LABEL_GRID         NUMBER(10),
  CD_EXP_VALORES            NUMBER(10),
  CD_EXP_LABEL_LONGO        NUMBER(10),
  IE_INFORMACAO_SENSIVEL    VARCHAR2(1 BYTE),
  IE_TIPO_DATE              VARCHAR2(15 BYTE),
  NR_SEQ_DIC_OBJETO         NUMBER(10),
  NM_TIME_ZONE_ATTRIBUTE    VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ATRIBUT_PK ON TASY.TABELA_ATRIBUTO
(NM_TABELA, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATRIBUT_TABSIST_FK_I ON TASY.TABELA_ATRIBUTO
(NM_TABELA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICEXPR_FK_I ON TASY.TABELA_ATRIBUTO
(CD_EXP_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICEXPR_FK2_I ON TASY.TABELA_ATRIBUTO
(CD_EXP_LABEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICEXPR_FK3_I ON TASY.TABELA_ATRIBUTO
(CD_EXP_LABEL_GRID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICEXPR_FK4_I ON TASY.TABELA_ATRIBUTO
(CD_EXP_VALORES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICEXPR_FK5_I ON TASY.TABELA_ATRIBUTO
(CD_EXP_LABEL_LONGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_DICOBJE_FK_I ON TASY.TABELA_ATRIBUTO
(NR_SEQ_DIC_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_I1 ON TASY.TABELA_ATRIBUTO
(DT_ATUALIZACAO, IE_LOG_UPDATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_I2 ON TASY.TABELA_ATRIBUTO
(UPPER("NM_TABELA"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATRI_TIPLOCA_FK_I ON TASY.TABELA_ATRIBUTO
(NR_SEQ_LOCALIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Tabela_Atributo_Delete
BEFORE DELETE ON Tabela_atributo
FOR EACH ROW
declare

nr_sequencia_w		Number(10);

begin

nr_sequencia_w	:= 1;

end;
/


CREATE OR REPLACE TRIGGER TASY.TABELA_ATRIBUTO_tp  after update ON TASY.TABELA_ATRIBUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NM_TABELA='||to_char(:old.NM_TABELA)||'#@#@NM_ATRIBUTO='||to_char(:old.NM_ATRIBUTO); ds_w:=substr(:new.NM_ATRIBUTO,1,500);gravar_log_alteracao(substr(:old.NM_ATRIBUTO,1,4000),substr(:new.NM_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UNIDADE_MEDIDA,1,4000),substr(:new.IE_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_UNIDADE_MEDIDA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DECIMAIS,1,4000),substr(:new.QT_DECIMAIS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DECIMAIS',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRIAR_ALTERAR,1,4000),substr(:new.IE_CRIAR_ALTERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_ALTERAR',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_GRID,1,4000),substr(:new.QT_TAM_GRID,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_GRID',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SEQ_INICIO,1,4000),substr(:new.QT_SEQ_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_SEQ_INICIO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SEQ_INCREMENTO,1,4000),substr(:new.QT_SEQ_INCREMENTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_SEQ_INCREMENTO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAMANHO,1,4000),substr(:new.QT_TAMANHO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAMANHO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENT,1,4000),substr(:new.NR_SEQ_APRESENT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENT',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LABEL_GRID,1,4000),substr(:new.DS_LABEL_GRID,1,4000),:new.nm_usuario,nr_seq_w,'DS_LABEL_GRID',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DEFAULT,1,4000),substr(:new.VL_DEFAULT,1,4000),:new.nm_usuario,nr_seq_w,'VL_DEFAULT',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM,1,4000),substr(:new.NR_SEQ_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COLUNA,1,4000),substr(:new.QT_COLUNA,1,4000),:new.nm_usuario,nr_seq_w,'QT_COLUNA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_READONLY,1,4000),substr(:new.IE_READONLY,1,4000),:new.nm_usuario,nr_seq_w,'IE_READONLY',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TABSTOP,1,4000),substr(:new.IE_TABSTOP,1,4000),:new.nm_usuario,nr_seq_w,'IE_TABSTOP',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TABSTOP,1,4000),substr(:new.NR_SEQ_TABSTOP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TABSTOP',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRID,1,4000),substr(:new.NR_SEQ_GRID,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRID',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRIAR_DESCRICAO,1,4000),substr(:new.IE_CRIAR_DESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_DESCRICAO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ATRIBUTO_PAI,1,4000),substr(:new.NM_ATRIBUTO_PAI,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO_PAI',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ALTURA,1,4000),substr(:new.QT_ALTURA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DESLOC_DIREITA,1,4000),substr(:new.QT_DESLOC_DIREITA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DESLOC_DIREITA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPONENTE,1,4000),substr(:new.IE_COMPONENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPONENTE',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VALORES,1,4000),substr(:new.DS_VALORES,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALORES',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_DELPHI,1,4000),substr(:new.QT_TAM_DELPHI,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_DELPHI',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LABEL,1,4000),substr(:new.DS_LABEL,1,4000),:new.nm_usuario,nr_seq_w,'DS_LABEL',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOMINIO,1,4000),substr(:new.CD_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOMINIO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_LABEL,1,4000),substr(:new.QT_TAM_LABEL,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_LABEL',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAMANHO_CALCULO,1,4000),substr(:new.QT_TAMANHO_CALCULO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAMANHO_CALCULO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCALIZADOR,1,4000),substr(:new.NR_SEQ_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCALIZADOR',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MASCARA,1,4000),substr(:new.DS_MASCARA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ATRIBUTO,1,4000),substr(:new.DS_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ATRIBUTO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CRIACAO,1,4000),substr(:new.DT_CRIACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CRIACAO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATRIBUTO,1,4000),substr(:new.IE_TIPO_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATRIBUTO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONVERTE_NEGATIVO,1,4000),substr(:new.IE_CONVERTE_NEGATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONVERTE_NEGATIVO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CACHE,1,4000),substr(:new.QT_CACHE,1,4000),:new.nm_usuario,nr_seq_w,'QT_CACHE',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_VERSAO,1,4000),substr(:new.IE_ATUALIZAR_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_VERSAO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COR,1,4000),substr(:new.DS_COR,1,4000),:new.nm_usuario,nr_seq_w,'DS_COR',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRIAR_DESC_FK,1,4000),substr(:new.IE_CRIAR_DESC_FK,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_DESC_FK',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_UNID_MEDIDA,1,4000),substr(:new.IE_REGRA_UNID_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_UNID_MEDIDA',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TAM_FONTE,1,4000),substr(:new.QT_TAM_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_TAM_FONTE',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ESTILO_FONTE,1,4000),substr(:new.DS_ESTILO_FONTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_ESTILO_FONTE',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICABILIDADE_ESTILO,1,4000),substr(:new.IE_APLICABILIDADE_ESTILO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICABILIDADE_ESTILO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FILTER,1,4000),substr(:new.DS_FILTER,1,4000),:new.nm_usuario,nr_seq_w,'DS_FILTER',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LABEL_LONGO,1,4000),substr(:new.DS_LABEL_LONGO,1,4000),:new.nm_usuario,nr_seq_w,'DS_LABEL_LONGO',ie_log_w,ds_w,'TABELA_ATRIBUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TABELA_ATRIBUTO ADD (
  CONSTRAINT ATRIBUT_PK
 PRIMARY KEY
 (NM_TABELA, NM_ATRIBUTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_ATRIBUTO ADD (
  CONSTRAINT ATRIBUT_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA)
    ON DELETE CASCADE,
  CONSTRAINT TABATRI_TIPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZADOR) 
 REFERENCES TASY.TIPO_LOCALIZAR (NR_SEQUENCIA),
  CONSTRAINT TABATRI_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_DESC) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TABATRI_DICEXPR_FK2 
 FOREIGN KEY (CD_EXP_LABEL) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TABATRI_DICEXPR_FK3 
 FOREIGN KEY (CD_EXP_LABEL_GRID) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TABATRI_DICEXPR_FK4 
 FOREIGN KEY (CD_EXP_VALORES) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TABATRI_DICEXPR_FK5 
 FOREIGN KEY (CD_EXP_LABEL_LONGO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT TABATRI_DICOBJE_FK 
 FOREIGN KEY (NR_SEQ_DIC_OBJETO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TABELA_ATRIBUTO TO NIVEL_1;

