ALTER TABLE TASY.PLS_ALERTA_EVENTO_CONT_DES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ALERTA_EVENTO_CONT_DES CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ALERTA_EVENTO_CONT_DES
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_EVENTO_CONTROLE  NUMBER(10)            NOT NULL,
  IE_STATUS_ENVIO         VARCHAR2(2 BYTE)      NOT NULL,
  IE_FORMA_ENVIO          VARCHAR2(3 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_PESSOA_DEST     VARCHAR2(2 BYTE),
  DT_ENVIO_MENSAGEM       DATE,
  ID_SMS                  NUMBER(10),
  NM_USUARIO_DESTINO      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  DS_MENSAGEM             VARCHAR2(4000 BYTE),
  NR_TELEF_CELULAR        VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSAECD_PESFISI_FK_I ON TASY.PLS_ALERTA_EVENTO_CONT_DES
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSAECD_PK ON TASY.PLS_ALERTA_EVENTO_CONT_DES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAECD_PLSAEVC_FK_I ON TASY.PLS_ALERTA_EVENTO_CONT_DES
(NR_SEQ_EVENTO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAECD_PLSAEVC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAECD_USUARIO_FK_I ON TASY.PLS_ALERTA_EVENTO_CONT_DES
(NM_USUARIO_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAECD_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ALERTA_EVENTO_CONT_DES ADD (
  CONSTRAINT PLSAECD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ALERTA_EVENTO_CONT_DES ADD (
  CONSTRAINT PLSAECD_PLSAEVC_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_CONTROLE) 
 REFERENCES TASY.PLS_ALERTA_EVENTO_CONTROLE (NR_SEQUENCIA),
  CONSTRAINT PLSAECD_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_DESTINO) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT PLSAECD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PLS_ALERTA_EVENTO_CONT_DES TO NIVEL_1;

