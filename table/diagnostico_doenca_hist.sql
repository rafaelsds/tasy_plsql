ALTER TABLE TASY.DIAGNOSTICO_DOENCA_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIAGNOSTICO_DOENCA_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIAGNOSTICO_DOENCA_HIST
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  IE_STATUS                VARCHAR2(3 BYTE)     NOT NULL,
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  CD_RESPONSAVEL           VARCHAR2(10 BYTE)    NOT NULL,
  CD_DOENCA                VARCHAR2(10 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_DIAGNOSTICO           DATE,
  NR_ATENDIMENTO           NUMBER(10),
  IE_STATUS_PROBLEMA       VARCHAR2(10 BYTE),
  DT_DISEASE_END           DATE,
  NR_SEQ_INTERNO           VARCHAR2(50 BYTE),
  IE_TIPO_DIAGNOSTICO      NUMBER(3),
  IE_CLASSIFICACAO_DOENCA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIADOHI_CIDDOEN_FK_I ON TASY.DIAGNOSTICO_DOENCA_HIST
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOHI_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOHI_DIASTAT_FK_I ON TASY.DIAGNOSTICO_DOENCA_HIST
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOHI_DIASTAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOHI_I1 ON TASY.DIAGNOSTICO_DOENCA_HIST
(IE_STATUS, CD_PESSOA_FISICA, CD_DOENCA, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOHI_PESFISI_FK_I ON TASY.DIAGNOSTICO_DOENCA_HIST
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOHI_PESFISI_FK2_I ON TASY.DIAGNOSTICO_DOENCA_HIST
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIADOHI_PK ON TASY.DIAGNOSTICO_DOENCA_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOHI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_HIST_ATUAL
BEFORE INSERT OR UPDATE ON TASY.DIAGNOSTICO_DOENCA_HIST FOR EACH ROW
DECLARE
dt_diagnostico_w	Date;
nr_atendimento_w	Number(10);
qt_reg_w		number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.ie_status in ('P','S')) then
	begin
	select	max(a.dt_diagnostico),
		max(a.nr_atendimento)
	into	dt_diagnostico_w,
		nr_atendimento_w
	from	diagnostico_doenca a,
		diagnostico_medico b,
		atendimento_paciente x
	where	a.nr_atendimento	= x.nr_atendimento
	and	x.nr_atendimento	= b.nr_atendimento
	and	x.cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	a.cd_doenca		= :new.cd_doenca
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.dt_diagnostico	= b.dt_diagnostico;
	exception
		when others then
		dt_diagnostico_w	:= to_date('02/01/1800','DD/MM/YYYY');
		nr_atendimento_w	:= 0;
	end;

	if	(:new.ie_status = 'P') then
		update	diagnostico_doenca
		set	ie_classificacao_doenca	= 'P'
		where	nr_atendimento	= nr_atendimento_w
		and	cd_doenca	= :new.cd_doenca
		and	dt_diagnostico	= dt_diagnostico_w;
	elsif	(:new.ie_status = 'S') then
		update	diagnostico_doenca
		set	ie_classificacao_doenca	= 'S'
		where	nr_atendimento	= nr_atendimento_w
		and	cd_doenca	= :new.cd_doenca
		and	dt_diagnostico	= dt_diagnostico_w;
	end if;
end if;
<<Final>>
qt_reg_w	:= 0;



END;
/


ALTER TABLE TASY.DIAGNOSTICO_DOENCA_HIST ADD (
  CONSTRAINT DIADOHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIAGNOSTICO_DOENCA_HIST ADD (
  CONSTRAINT DIADOHI_PESFISI_FK2 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DIADOHI_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DIADOHI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DIADOHI_DIASTAT_FK 
 FOREIGN KEY (IE_STATUS) 
 REFERENCES TASY.DIAGNOSTICO_STATUS (IE_STATUS));

GRANT SELECT ON TASY.DIAGNOSTICO_DOENCA_HIST TO NIVEL_1;

