ALTER TABLE TASY.PLS_BENEF_FLEX_SIB_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_BENEF_FLEX_SIB_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_BENEF_FLEX_SIB_ATRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FLEX_SIB      NUMBER(10)               NOT NULL,
  IE_ATRIBUTO_ENVIO    VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLBFSAT_PK ON TASY.PLS_BENEF_FLEX_SIB_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLBFSAT_PLBFSIB_FK_I ON TASY.PLS_BENEF_FLEX_SIB_ATRIB
(NR_SEQ_FLEX_SIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_BENEF_FLEX_SIB_ATRIB ADD (
  CONSTRAINT PLBFSAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_BENEF_FLEX_SIB_ATRIB ADD (
  CONSTRAINT PLBFSAT_PLBFSIB_FK 
 FOREIGN KEY (NR_SEQ_FLEX_SIB) 
 REFERENCES TASY.PLS_BENEF_FLEX_SIB (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_BENEF_FLEX_SIB_ATRIB TO NIVEL_1;

