ALTER TABLE TASY.TEMPLATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEMPLATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEMPLATE
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  TEMPLATE_TYPE                 VARCHAR2(255 BYTE) NOT NULL,
  NM_TEMPLATE                   VARCHAR2(255 BYTE),
  CD_TITLE_FONT_FAMILY          VARCHAR2(255 BYTE),
  CD_TITLE_LAYOUT_STYLE         VARCHAR2(255 BYTE),
  IE_TITLE_STYLE_BOLD           VARCHAR2(1 BYTE),
  IE_TITLE_STYLE_ITALIC         VARCHAR2(1 BYTE),
  IE_TITLE_STYLE_UNDERLINE      VARCHAR2(1 BYTE),
  TITLE_FONT_SIZE               VARCHAR2(255 BYTE),
  CD_TITLE_ALIGNMENT            VARCHAR2(255 BYTE),
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_SUB_TITLE_STYLE_BOLD       VARCHAR2(255 BYTE),
  IE_SUB_TITLE_STYLE_ITALIC     VARCHAR2(1 BYTE),
  IE_SUB_TITLE_STYLE_UNDERLINE  VARCHAR2(1 BYTE),
  SUB_TITLE_FONT_SIZE           NUMBER(10),
  CD_SUB_TITLE_ALIGNMENT        VARCHAR2(255 BYTE),
  IE_MIC_RES_POS_ONLY           VARCHAR2(1 BYTE),
  IE_MIC_RES_POS_NOPRINT        VARCHAR2(1 BYTE),
  IE_MIC_RES_NEG_ONLY           VARCHAR2(1 BYTE),
  IE_MIC_RES_NEG_NOPRINT        VARCHAR2(1 BYTE),
  IE_MAP                        VARCHAR2(1 BYTE),
  IE_FORMAT                     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TEMPLATE_PK ON TASY.TEMPLATE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TEMPLATE ADD (
  CONSTRAINT TEMPLATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TEMPLATE TO NIVEL_1;

