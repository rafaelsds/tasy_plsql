ALTER TABLE TASY.ESCALA_SAPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SAPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SAPS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_TIPO_ADMISSAO       NUMBER(2)              NOT NULL,
  IE_AIDS                VARCHAR2(1 BYTE)       NOT NULL,
  IE_MALIG_HEMATOLOGICA  VARCHAR2(1 BYTE)       NOT NULL,
  IE_CANCRO_METASTATICO  VARCHAR2(1 BYTE)       NOT NULL,
  QT_GLASGOW             NUMBER(2),
  QT_IDADE               NUMBER(3),
  QT_PA_SISTOLICA        NUMBER(3),
  QT_FREQ_CARDIACA       NUMBER(3),
  QT_TEMP                NUMBER(4,1),
  QT_REL_PAO2_FIO2       NUMBER(3),
  QT_DIURESE             NUMBER(5,2),
  QT_UREIA               NUMBER(17,4),
  QT_GLOBULOS_BRANCOS    NUMBER(15,3),
  QT_POTASSIO            NUMBER(15,2),
  QT_SODIO               NUMBER(5),
  QT_BICARBONATO         NUMBER(3),
  QT_BILIRRUBINA         NUMBER(15,2),
  QT_DIAS_ANTES_UTI      NUMBER(3),
  IE_ORIGEM_PACIENTE     NUMBER(1)              NOT NULL,
  IE_CATEGORIA_CLINICA   NUMBER(1),
  IE_INTOXICACAO         VARCHAR2(1 BYTE)       NOT NULL,
  QT_PTO_TIPO_ADM        NUMBER(15),
  QT_PTO_DOENCA_CRO      NUMBER(15),
  QT_PTO_GLASGOW         NUMBER(15),
  QT_PTO_IDADE           NUMBER(15),
  QT_PTO_PA_SITOLICA     NUMBER(15),
  QT_PTO_FREC_CARD       NUMBER(15),
  QT_PTO_TEMP            NUMBER(15),
  QT_PTO_PAO2_FIO2       NUMBER(15),
  QT_PTO_DIURESE         NUMBER(15),
  QT_PTO_UREIA           NUMBER(15),
  QT_PTO_GLOB_BRANCO     NUMBER(15),
  QT_PTO_POTASSIO        NUMBER(15),
  QT_PTO_SODIO           NUMBER(15),
  QT_PTO_BICARBONATO     NUMBER(15),
  QT_PTO_BILIRRUBINA     NUMBER(15),
  QT_SAPS                NUMBER(15)             NOT NULL,
  QT_PTO_DIAS_UTI        NUMBER(19,4),
  QT_PTO_ORIGEM_PAC      NUMBER(19,4),
  QT_PTO_CATEG_CLINICA   NUMBER(19,4),
  QT_PTO_INTOXIC         NUMBER(19,4),
  QT_SAPS_EXPAND         NUMBER(19,4)           NOT NULL,
  PR_MORTALIDADE         NUMBER(5,2)            NOT NULL,
  QT_PTO_SEXO            NUMBER(19,4),
  PR_MORT_EXPAND         NUMBER(5,2)            NOT NULL,
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSAPS_ATEPACI_FK_I ON TASY.ESCALA_SAPS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAPS_EHRREGI_FK_I ON TASY.ESCALA_SAPS
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAPS_I1 ON TASY.ESCALA_SAPS
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSAPS_PERFIL_FK_I ON TASY.ESCALA_SAPS
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSAPS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSAPS_PESFISI_FK_I ON TASY.ESCALA_SAPS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSAPS_PK ON TASY.ESCALA_SAPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSAPS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_saps_atual
before insert or update ON TASY.ESCALA_SAPS for each row
declare

qt_idade_w		number(5,4):= 0;
logit_expandido_w	number(15,4):= 0;
logit_w			number(15,4):= 0;
cd_pessoa_fisica_w	varchar2(10);
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (philips_param_pck.get_cd_pais() <> 7) then
	/* Obter pessoa f�sica */
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;


	/* Pontua��o tipo de admiss�o */
	if	(:new.ie_tipo_admissao = 1) then
		:new.qt_pto_tipo_adm:= 0;
	elsif	(:new.ie_tipo_admissao = 2) then
		:new.qt_pto_tipo_adm:= 8;
	elsif	(:new.ie_tipo_admissao = 3) then
		:new.qt_pto_tipo_adm:= 6;
	end if;

	/* Pontua��o Doen�as cr�nicas */
	if	(:new.ie_aids = 'S') then
		:new.qt_pto_doenca_cro:= 17;
	elsif	(:new.ie_malig_hematologica = 'S') then
		:new.qt_pto_doenca_cro:= 10;
	elsif	(:new.ie_cancro_metastatico = 'S') then
		:new.qt_pto_doenca_cro:= 9;
	elsif	(:new.ie_cancro_metastatico = 'N') and
		(:new.ie_malig_hematologica = 'N') and
		(:new.ie_aids = 'N') then
		:new.qt_pto_doenca_cro:= 0;
	end if;

	/* Pontua��o Glasgow */
	if	(:new.qt_glasgow is null) then
		:new.qt_pto_glasgow:= 0;
	elsif	(:new.qt_glasgow >= 14) and
		(:new.qt_glasgow <= 15) then
		:new.qt_pto_glasgow:= 0;
	elsif	(:new.qt_glasgow >= 11) and
		(:new.qt_glasgow <= 13) then
		:new.qt_pto_glasgow:= 5;
	elsif	(:new.qt_glasgow >= 9) and
		(:new.qt_glasgow <= 10) then
		:new.qt_pto_glasgow:= 7;
	elsif	(:new.qt_glasgow >= 6) and
		(:new.qt_glasgow <= 8) then
		:new.qt_pto_glasgow:= 13;
	elsif	(:new.qt_glasgow <= 6) then
		:new.qt_pto_glasgow:= 26;
	end if;

	/* Pontua��o de idade */
	if	(:old.qt_idade is null) then
		select	nvl(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'),0)
		into	:new.qt_idade
		from	dual;
	end if;

	if	(:new.qt_idade is null) then
		:new.qt_pto_idade:= 0;
	elsif	(:new.qt_idade < 40) then
		:new.qt_pto_idade:= 0;
		qt_idade_w:= 0;
	elsif	(:new.qt_idade >= 40) and
		(:new.qt_idade <= 59) then
		:new.qt_pto_idade:= 7;
		qt_idade_w:= 0.1639;
	elsif	(:new.qt_idade >= 60) and
		(:new.qt_idade <= 69) then
		:new.qt_pto_idade:= 12;
		qt_idade_w:= 0.2739;
	elsif	(:new.qt_idade >= 70) and
		(:new.qt_idade <= 74) then
		:new.qt_pto_idade:= 15;
		qt_idade_w:= 0.3690;
	elsif	(:new.qt_idade >= 75) and
		(:new.qt_idade <= 79) then
		:new.qt_pto_idade:= 16;
		qt_idade_w:= 0.3690;
	elsif	(:new.qt_idade >= 80) then
		:new.qt_pto_idade:= 18;
		qt_idade_w:= 0.6645;
	end if;

	/* Pontua��o PA Sist�lica */
	if	(:new.qt_pa_sistolica is null) then
		:new.qt_pto_pa_sitolica:= 0;
	elsif	(:new.qt_pa_sistolica < 70) then
		:new.qt_pto_pa_sitolica:= 13;
	elsif	(:new.qt_pa_sistolica >= 70) and
		(:new.qt_pa_sistolica <= 99) then
		:new.qt_pto_pa_sitolica:= 5;
	elsif	(:new.qt_pa_sistolica >= 100) and
		(:new.qt_pa_sistolica <= 199) then
		:new.qt_pto_pa_sitolica:= 0;
	elsif	(:new.qt_pa_sistolica >= 200) then
		:new.qt_pto_pa_sitolica:= 2;
	end if;

	/* Pontua��o frequ�ncia c�rdiaca */
	if	(:new.qt_freq_cardiaca is null) then
		:new.qt_pto_frec_card:= 0;
	elsif	(:new.qt_freq_cardiaca < 40) then
		:new.qt_pto_frec_card:= 11;
	elsif	(:new.qt_freq_cardiaca >= 40) and
		(:new.qt_freq_cardiaca <= 69) then
		:new.qt_pto_frec_card:= 2;
	elsif	(:new.qt_freq_cardiaca >= 70) and
		(:new.qt_freq_cardiaca <= 119) then
		:new.qt_pto_frec_card:= 0;
	elsif	(:new.qt_freq_cardiaca >= 120) and
		(:new.qt_freq_cardiaca <= 159) then
		:new.qt_pto_frec_card:= 4;
	elsif	(:new.qt_freq_cardiaca >= 160) then
		:new.qt_pto_frec_card:= 7;
	end if;

	/* Pontua��o da temperatura */
	if	(:new.qt_temp is null) then
		:new.qt_pto_temp:= 0;
	elsif	(:new.qt_temp < 39) then
		:new.qt_pto_temp:= 0;
	elsif	(:new.qt_temp >= 39) then
		:new.qt_pto_temp:= 3;
	end if;

	/* Pontua��o da PA0�FIO� */
	if	(:new.qt_rel_pao2_fio2 is null) then
		:new.qt_pto_pao2_fio2:= 0;
	elsif	(:new.qt_rel_pao2_fio2 < 100) then
		:new.qt_pto_pao2_fio2:= 11;
	elsif	(:new.qt_rel_pao2_fio2 >= 100) and
		(:new.qt_rel_pao2_fio2 <= 199) then
		:new.qt_pto_pao2_fio2:= 9;
	elsif	(:new.qt_rel_pao2_fio2 >= 200) then
		:new.qt_pto_pao2_fio2:= 6;
	end if;

	/* Pontua��o de Ur�ia */
	if	(:new.qt_ureia is null) then
		:new.qt_pto_ureia:= 0;
	elsif	(:new.qt_ureia < 28) then
		:new.qt_pto_ureia:= 0;
	elsif	(:new.qt_ureia >= 28) and
		(:new.qt_ureia <= 83) then
		:new.qt_pto_ureia:= 6;
	elsif	(:new.qt_ureia >= 84) then
		:new.qt_pto_ureia:= 10;
	end if;

	/* Pontua��o da Diurese */
	if	(:new.qt_diurese is null) then
		:new.qt_pto_diurese:= 0;
	elsif	(:new.qt_diurese < 0.5) then
		:new.qt_pto_diurese:= 11;
	elsif	(:new.qt_diurese >= 0.5) and
		(:new.qt_diurese <= 0.9) then
		:new.qt_pto_diurese:= 4;
	elsif	(:new.qt_diurese >= 1) then
		:new.qt_pto_diurese:= 0;
	end if;

	/* Pontua��o dos gl�bulos brancos */
	if	(:new.qt_globulos_brancos is null) then
		:new.qt_pto_glob_branco:= 0;
	elsif	(:new.qt_globulos_brancos < 1000) then
		:new.qt_pto_glob_branco:= 12;
	elsif	(:new.qt_globulos_brancos < 20000) then
		:new.qt_pto_glob_branco:= 0;
	else
		:new.qt_pto_glob_branco:= 3;
	end if;

	/* Pontua��o Pot�ssio */
	if	(:new.qt_potassio is null) then
		:new.qt_pto_potassio:= 0;
	elsif	(:new.qt_potassio < 3) or
		(:new.qt_potassio >= 5) then
		:new.qt_pto_potassio:= 3;
	elsif	(:new.qt_potassio >= 3) and
		(:new.qt_potassio <= 4.9) then
		:new.qt_pto_potassio:= 0;
	end if;

	/* Pontua��o S�dio */
	if	(:new.qt_sodio is null) then
		:new.qt_pto_sodio:= 0;
	elsif	(:new.qt_sodio >= 145) then
		:new.qt_pto_sodio:= 1;
	elsif	(:new.qt_sodio >= 125) and
		(:new.qt_sodio <= 144) then
		:new.qt_pto_sodio:= 0;
	elsif	(:new.qt_sodio < 125) then
		:new.qt_pto_sodio:= 5;
	end if;

	/* Pontua��o Bicarbonato */
	if	(:new.qt_bicarbonato is null) then
		:new.qt_pto_bicarbonato:= 0;
	elsif	(:new.qt_bicarbonato < 15) then
		:new.qt_pto_bicarbonato:= 6;
	elsif	(:new.qt_bicarbonato >= 15) and
		(:new.qt_bicarbonato <= 19) then
		:new.qt_pto_bicarbonato:= 3;
	elsif	(:new.qt_bicarbonato >= 20) then
		:new.qt_pto_bicarbonato:= 0;
	end if;

	/* Pontua��o Bilirrubina */
	if	(:new.qt_bilirrubina is null) then
		:new.qt_pto_bilirrubina:= 0;
	elsif	(:new.qt_bilirrubina < 4) then
		:new.qt_pto_bilirrubina:= 0;
	elsif	(:new.qt_bilirrubina >= 4) and
		(:new.qt_bilirrubina <= 5.9) then
		:new.qt_pto_bilirrubina:= 4;
	elsif	(:new.qt_bilirrubina >= 6) then
		:new.qt_pto_bilirrubina:= 9;
	end if;

	/* Pontua��o do Sexo */
	select	decode(nvl(obter_sexo_pf(cd_pessoa_fisica_w,'C'),0),'M',0.2083,0)
	into	:new.qt_pto_sexo
	from	dual;

	/* Pontua��o do Dias antes da UTI */
	if	(:new.qt_dias_antes_uti = 1) then
		:new.qt_pto_dias_uti:= 0.0986;
	elsif	(:new.qt_dias_antes_uti = 2) then
		:new.qt_pto_dias_uti:= 0.1944;
	elsif	(:new.qt_dias_antes_uti >= 3) and
		(:new.qt_dias_antes_uti <= 9) then
		:new.qt_pto_dias_uti:= 0.5284;
	elsif	(:new.qt_dias_antes_uti > 1) then
		:new.qt_pto_dias_uti:= 0.9323;
	end if;

	/* Pontua��o Origem Paciente */
	if	(:new.ie_origem_paciente = 1) then
		:new.qt_pto_origem_pac:= 0;
	elsif	(:new.ie_origem_paciente = 2) then
		:new.qt_pto_origem_pac:= 0.2606;
	elsif	(:new.ie_origem_paciente = 3) then
		:new.qt_pto_origem_pac:= 0.3381;
	end if;

	/* Pontua��o Categoria Cl�nica */
	if	(:new.ie_categoria_clinica = 1) then
		:new.qt_pto_categ_clinica:= 0.6555;
	elsif	(:new.ie_categoria_clinica = 2) then
		:new.qt_pto_categ_clinica:= 0;
	end if;

	/* Pontua��o da Intoxica��o */
	select	decode(:new.ie_intoxicacao,'N',1.6693,0)
	into	:new.qt_pto_intoxic
	from	dual;
else
	/* Obter pessoa f�sica */
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;


	/* Pontua��o tipo de admiss�o */
	if	(:new.ie_tipo_admissao = 1) then
		:new.qt_pto_tipo_adm:= 0;
	elsif	(:new.ie_tipo_admissao = 2) then
		:new.qt_pto_tipo_adm:= 8;
	elsif	(:new.ie_tipo_admissao = 3) then
		:new.qt_pto_tipo_adm:= 6;
	end if;

	/* Pontua��o Doen�as cr�nicas */
	if	(:new.ie_aids = '3') then
		:new.qt_pto_doenca_cro:= 17;
	elsif	(:new.ie_aids = '2') then
		:new.qt_pto_doenca_cro:= 10;
	elsif	(:new.ie_aids = '1') then
		:new.qt_pto_doenca_cro:= 9;
	else
		:new.qt_pto_doenca_cro:= 0;
	end if;

	/* Pontua��o Glasgow */
	if	(:new.qt_glasgow is null) then
		:new.qt_pto_glasgow:= 0;
	elsif	(:new.qt_glasgow >= 14) and
		(:new.qt_glasgow <= 15) then
		:new.qt_pto_glasgow:= 0;
	elsif	(:new.qt_glasgow >= 11) and
		(:new.qt_glasgow <= 13) then
		:new.qt_pto_glasgow:= 5;
	elsif	(:new.qt_glasgow >= 9) and
		(:new.qt_glasgow <= 10) then
		:new.qt_pto_glasgow:= 7;
	elsif	(:new.qt_glasgow >= 6) and
		(:new.qt_glasgow <= 8) then
		:new.qt_pto_glasgow:= 13;
	elsif	(:new.qt_glasgow <= 6) then
		:new.qt_pto_glasgow:= 26;
	end if;

	/* Pontua��o de idade */
	if	(:old.qt_idade is null) then
		select	nvl(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'),0)
		into	:new.qt_idade
		from	dual;
	end if;

	if	(:new.qt_idade is null) then
		:new.qt_pto_idade:= 0;
	elsif	(:new.qt_idade < 40) then
		:new.qt_pto_idade:= 0;
		qt_idade_w:= 0;
	elsif	(:new.qt_idade >= 40) and
		(:new.qt_idade <= 59) then
		:new.qt_pto_idade:= 7;
		qt_idade_w:= 0.1639;
	elsif	(:new.qt_idade >= 60) and
		(:new.qt_idade <= 69) then
		:new.qt_pto_idade:= 12;
		qt_idade_w:= 0.2739;
	elsif	(:new.qt_idade >= 70) and
		(:new.qt_idade <= 74) then
		:new.qt_pto_idade:= 15;
		qt_idade_w:= 0.3690;
	elsif	(:new.qt_idade >= 75) and
		(:new.qt_idade <= 79) then
		:new.qt_pto_idade:= 16;
		qt_idade_w:= 0.3690;
	elsif	(:new.qt_idade >= 80) then
		:new.qt_pto_idade:= 18;
		qt_idade_w:= 0.6645;
	end if;

	/* Pontua��o PA Sist�lica */
	if	(:new.qt_pa_sistolica is null) then
		:new.qt_pto_pa_sitolica:= 0;
	elsif	(:new.qt_pa_sistolica < 70) then
		:new.qt_pto_pa_sitolica:= 13;
	elsif	(:new.qt_pa_sistolica >= 70) and
		(:new.qt_pa_sistolica <= 99) then
		:new.qt_pto_pa_sitolica:= 5;
	elsif	(:new.qt_pa_sistolica >= 100) and
		(:new.qt_pa_sistolica <= 199) then
		:new.qt_pto_pa_sitolica:= 0;
	elsif	(:new.qt_pa_sistolica >= 200) then
		:new.qt_pto_pa_sitolica:= 2;
	end if;

	/* Pontua��o frequ�ncia c�rdiaca */
	if	(:new.qt_freq_cardiaca is null) then
		:new.qt_pto_frec_card:= 0;
	elsif	(:new.qt_freq_cardiaca < 40) then
		:new.qt_pto_frec_card:= 11;
	elsif	(:new.qt_freq_cardiaca >= 40) and
		(:new.qt_freq_cardiaca <= 69) then
		:new.qt_pto_frec_card:= 2;
	elsif	(:new.qt_freq_cardiaca >= 70) and
		(:new.qt_freq_cardiaca <= 119) then
		:new.qt_pto_frec_card:= 0;
	elsif	(:new.qt_freq_cardiaca >= 120) and
		(:new.qt_freq_cardiaca <= 159) then
		:new.qt_pto_frec_card:= 4;
	elsif	(:new.qt_freq_cardiaca >= 160) then
		:new.qt_pto_frec_card:= 7;
	end if;

	/* Pontua��o da temperatura */
	if	(:new.qt_temp is null) then
		:new.qt_pto_temp:= 0;
	elsif	(:new.qt_temp < 39) then
		:new.qt_pto_temp:= 0;
	elsif	(:new.qt_temp >= 39) then
		:new.qt_pto_temp:= 3;
	end if;

	/* Pontua��o da PA0�FIO� */
	if	(:new.qt_rel_pao2_fio2 is null) then
		:new.qt_pto_pao2_fio2:= 0;
	elsif	(:new.qt_rel_pao2_fio2 < 100) then
		:new.qt_pto_pao2_fio2:= 11;
	elsif	(:new.qt_rel_pao2_fio2 >= 100) and
		(:new.qt_rel_pao2_fio2 <= 199) then
		:new.qt_pto_pao2_fio2:= 9;
	elsif	(:new.qt_rel_pao2_fio2 >= 200) then
		:new.qt_pto_pao2_fio2:= 6;
	end if;

	/* Pontua��o de Ur�ia */
	if	(:new.qt_ureia is null) then
		:new.qt_pto_ureia:= 0;
	elsif	(:new.qt_ureia < 0.6) then
		:new.qt_pto_ureia:= 0;
	elsif	(:new.qt_ureia >= 0.6) and
		(:new.qt_ureia <= 1.8) then
		:new.qt_pto_ureia:= 6;
	elsif	(:new.qt_ureia >= 1.8) then
		:new.qt_pto_ureia:= 10;
	end if;

	/* Pontua��o da Diurese */
	if	(:new.qt_diurese is null) then
		:new.qt_pto_diurese:= 0;
	elsif	(:new.qt_diurese < 0.5) then
		:new.qt_pto_diurese:= 11;
	elsif	(:new.qt_diurese >= 0.5) and
		(:new.qt_diurese <= 0.9) then
		:new.qt_pto_diurese:= 4;
	elsif	(:new.qt_diurese >= 1) then
		:new.qt_pto_diurese:= 0;
	end if;

	/* Pontua��o dos gl�bulos brancos */
	if	(:new.qt_globulos_brancos is null) then
		:new.qt_pto_glob_branco:= 0;
	elsif	(:new.qt_globulos_brancos < 1) then
		:new.qt_pto_glob_branco:= 12;
	elsif	(:new.qt_globulos_brancos >= 20) then
		:new.qt_pto_glob_branco:= 3;
	elsif	(:new.qt_globulos_brancos >= 1
		and :new.qt_globulos_brancos < 20) then
		:new.qt_pto_glob_branco:= 0;
	end if;

	/* Pontua��o Pot�ssio */
	if	(:new.qt_potassio is null) then
		:new.qt_pto_potassio:= 0;
	elsif	(:new.qt_potassio < 3) or
		(:new.qt_potassio >= 5) then
		:new.qt_pto_potassio:= 3;
	elsif	(:new.qt_potassio >= 3) and
		(:new.qt_potassio <= 4.9) then
		:new.qt_pto_potassio:= 0;
	end if;

	/* Pontua��o S�dio */
	if	(:new.qt_sodio is null) then
		:new.qt_pto_sodio:= 0;
	elsif	(:new.qt_sodio >= 145) then
		:new.qt_pto_sodio:= 1;
	elsif	(:new.qt_sodio >= 125) and
		(:new.qt_sodio <= 144) then
		:new.qt_pto_sodio:= 0;
	elsif	(:new.qt_sodio < 125) then
		:new.qt_pto_sodio:= 5;
	end if;

	/* Pontua��o Bicarbonato */
	if	(:new.qt_bicarbonato is null) then
		:new.qt_pto_bicarbonato:= 0;
	elsif	(:new.qt_bicarbonato < 15) then
		:new.qt_pto_bicarbonato:= 6;
	elsif	(:new.qt_bicarbonato >= 15) and
		(:new.qt_bicarbonato <= 19) then
		:new.qt_pto_bicarbonato:= 3;
	elsif	(:new.qt_bicarbonato >= 20) then
		:new.qt_pto_bicarbonato:= 0;
	end if;

	/* Pontua��o Bilirrubina */
	if	(:new.qt_bilirrubina is null) then
		:new.qt_pto_bilirrubina:= 0;
	elsif	(:new.qt_bilirrubina < 68.4) then
		:new.qt_pto_bilirrubina:= 0;
	elsif	(:new.qt_bilirrubina >= 68.4) and
		(:new.qt_bilirrubina <= 102.6) then
		:new.qt_pto_bilirrubina:= 4;
	elsif	(:new.qt_bilirrubina >= 102.6) then
		:new.qt_pto_bilirrubina:= 9;
	end if;

	/* Pontua��o do Sexo */
	select	decode(nvl(obter_sexo_pf(cd_pessoa_fisica_w,'C'),0),'M',0.2083,0)
	into	:new.qt_pto_sexo
	from	dual;

	/* Pontua��o do Dias antes da UTI */
	if	(:new.qt_dias_antes_uti = 1) then
		:new.qt_pto_dias_uti:= 0.0986;
	elsif	(:new.qt_dias_antes_uti = 2) then
		:new.qt_pto_dias_uti:= 0.1944;
	elsif	(:new.qt_dias_antes_uti >= 3) and
		(:new.qt_dias_antes_uti <= 9) then
		:new.qt_pto_dias_uti:= 0.5284;
	elsif	(:new.qt_dias_antes_uti > 1) then
		:new.qt_pto_dias_uti:= 0.9323;
	end if;

	/* Pontua��o Origem Paciente */
	if	(:new.ie_origem_paciente = 1) then
		:new.qt_pto_origem_pac:= 0;
	elsif	(:new.ie_origem_paciente = 2) then
		:new.qt_pto_origem_pac:= 0.2606;
	elsif	(:new.ie_origem_paciente = 3) then
		:new.qt_pto_origem_pac:= 0.3381;
	end if;

	/* Pontua��o Categoria Cl�nica */
	if	(:new.ie_categoria_clinica = 1) then
		:new.qt_pto_categ_clinica:= 0.6555;
	elsif	(:new.ie_categoria_clinica = 2) then
		:new.qt_pto_categ_clinica:= 0;
	end if;

	/* Pontua��o da Intoxica��o */
	select	decode(:new.ie_intoxicacao,'N',1.6693,0)
	into	:new.qt_pto_intoxic
	from	dual;
end if;

/* Obter a pontua��o total SAPS II */
:new.qt_saps:= nvl(:new.qt_pto_tipo_adm,0)	+ nvl(:new.qt_pto_doenca_cro,0)		+ nvl(:new.qt_pto_glasgow,0)	+
	       nvl(:new.qt_pto_idade,0)		+ nvl(:new.qt_pto_pa_sitolica,0)	+ nvl(:new.qt_pto_frec_card,0)	+
	       nvl(:new.qt_pto_temp,0)		+ nvl(:new.qt_pto_pao2_fio2,0)		+ nvl(:new.qt_pto_ureia,0)	+
	       nvl(:new.qt_pto_diurese,0)	+ nvl(:new.qt_pto_glob_branco,0)	+ nvl(:new.qt_pto_potassio,0)	+
	       nvl(:new.qt_pto_sodio,0)		+ nvl(:new.qt_pto_bicarbonato,0)	+ nvl(:new.qt_pto_bilirrubina,0);

/* Obter a pontua��o total SAPS II (extendido) */
:new.qt_saps_expand:= 0.0742 *	nvl(:new.qt_saps,0) 		+ nvl(qt_idade_w,0) +
				nvl(:new.qt_pto_sexo,0)		+ nvl(:new.qt_pto_dias_uti,0) +
				nvl(:new.qt_pto_origem_pac,0)	+ nvl(:new.qt_pto_categ_clinica,0) +
				nvl(:new.qt_pto_intoxic,0);


/* Percentual de mortalidade SAPS II */
logit_w := -7.7631 + 0.0737 * (:new.qt_saps) + 0.9971 * ln((:new.qt_saps)+1);

:new.pr_mortalidade:= dividir(exp(logit_w), 1 + exp(logit_w)) * 100;

/* Percentual de mortalidade SAPS II expandido*/
logit_expandido_w:= -14.476 + 0.0844 * :new.qt_saps_expand + 6.6158 * log(2.71828,(:new.qt_saps_expand + 1));

:new.pr_mort_expand:= dividir(exp(logit_expandido_w), 1 + exp(logit_expandido_w)) * 100;
<<Final>>
qt_reg_w	:= 0;


end;
/


ALTER TABLE TASY.ESCALA_SAPS ADD (
  CONSTRAINT ESCSAPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SAPS ADD (
  CONSTRAINT ESCSAPS_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCSAPS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSAPS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSAPS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_SAPS TO NIVEL_1;

