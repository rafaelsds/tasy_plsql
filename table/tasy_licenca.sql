ALTER TABLE TASY.TASY_LICENCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_LICENCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_LICENCA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_LICENCA           VARCHAR2(80 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_BANCO     VARCHAR2(10 BYTE)        NOT NULL,
  NM_SENHA_BANCO       VARCHAR2(10 BYTE)        NOT NULL,
  NM_USUARIO_AVISO     VARCHAR2(15 BYTE)        NOT NULL,
  CD_LICENCA           VARCHAR2(30 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  QT_USUARIO           NUMBER(5)                NOT NULL,
  IE_ACAO_EXCESSO      VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TASLICE_PK ON TASY.TASY_LICENCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASLICE_USUARIO_FK_I ON TASY.TASY_LICENCA
(NM_USUARIO_AVISO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TASLICE_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TASY_LICENCA ADD (
  CONSTRAINT TASLICE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_LICENCA ADD (
  CONSTRAINT TASLICE_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_AVISO) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.TASY_LICENCA TO NIVEL_1;

