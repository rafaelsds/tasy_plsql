ALTER TABLE TASY.W_PRESCR_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PRESCR_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PRESCR_MATERIAL
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_PRESCRICAO            NUMBER(14),
  NR_SEQUENCIA_PRESCRICAO  NUMBER(6),
  NR_AGRUPAMENTO           NUMBER(7),
  IE_AGRUPADOR             NUMBER(2),
  QT_UNITARIA              NUMBER(18,6),
  CD_MATERIAL              NUMBER(6),
  DS_MATERIAL              VARCHAR2(100 BYTE),
  QT_MATERIAL              NUMBER(15,3),
  QT_DOSE                  NUMBER(15,3),
  CD_UNIDADE_MEDIDA_DOSE   VARCHAR2(5 BYTE),
  IE_VIA_APLICACAO         VARCHAR2(5 BYTE),
  QT_TOTAL_DISPENSAR       NUMBER(18,6),
  CD_INTERVALO             VARCHAR2(7 BYTE),
  NM_USUARIO               VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA        VARCHAR2(5 BYTE),
  DS_HORARIOS              VARCHAR2(254 BYTE),
  HR_PRIM_HORARIO          VARCHAR2(5 BYTE),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(2000 BYTE),
  CD_FORNEC_CONSIGNADO     VARCHAR2(14 BYTE),
  NR_OCORRENCIA            NUMBER(15),
  IE_URGENCIA              VARCHAR2(1 BYTE),
  IE_UTILIZA_KIT           VARCHAR2(1 BYTE),
  IE_SUSPENSO              VARCHAR2(1 BYTE),
  IE_ORIGEM_INF            VARCHAR2(1 BYTE),
  IE_BAIXA_INTEIRA         VARCHAR2(1 BYTE),
  CD_GRUPO_MAT             NUMBER(15),
  IE_CONSIGNADO            VARCHAR2(1 BYTE),
  DS_FORNECEDOR            VARCHAR2(60 BYTE),
  DS_VIA                   VARCHAR2(40 BYTE),
  DS_ORIGEM                VARCHAR2(100 BYTE),
  NR_SEQ_ITEM_ORIGEM       NUMBER(6),
  CD_ITEM_ORIGEM           NUMBER(15),
  CD_KIT_MAT               NUMBER(5),
  DS_KIT_MAT               VARCHAR2(100 BYTE),
  QT_ATENDIDO              NUMBER(15,3),
  IE_SALDO_ATEND           VARCHAR2(1 BYTE),
  QT_DEVOLVIDO             NUMBER(15,3),
  QT_DIAS_SOLICITADO       NUMBER(3),
  QT_DIAS_LIBERADO         NUMBER(3),
  DT_PRIM_HORARIO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPRMAT_PK ON TASY.W_PRESCR_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRMAT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atual_w_prescr_material
before insert or update ON TASY.W_PRESCR_MATERIAL for each row
declare

begin

if (:new.hr_prim_horario is not null) and
   ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
	:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/


ALTER TABLE TASY.W_PRESCR_MATERIAL ADD (
  CONSTRAINT WPRMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PRESCR_MATERIAL TO NIVEL_1;

