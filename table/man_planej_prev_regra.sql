ALTER TABLE TASY.MAN_PLANEJ_PREV_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_PLANEJ_PREV_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_PLANEJ_PREV_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PLANEJ_PREV      NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_REGRA                VARCHAR2(80 BYTE)     NOT NULL,
  QT_CONTADOR             NUMBER(15)            NOT NULL,
  NR_SEQ_PLANEJ_CONTADOR  NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANPLPC_MANPLPC_FK_I ON TASY.MAN_PLANEJ_PREV_REGRA
(NR_SEQ_PLANEJ_PREV)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANPLPC_MANPLPC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANPLPC_MANPLPR_FK2_I ON TASY.MAN_PLANEJ_PREV_REGRA
(NR_SEQ_PLANEJ_CONTADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANPLPC_MANPLPR_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANPLPC_PK ON TASY.MAN_PLANEJ_PREV_REGRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_PLANEJ_PREV_REGRA ADD (
  CONSTRAINT MANPLPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_PLANEJ_PREV_REGRA ADD (
  CONSTRAINT MANPLPC_MANPLPC_FK 
 FOREIGN KEY (NR_SEQ_PLANEJ_PREV) 
 REFERENCES TASY.MAN_PLANEJ_PREV (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANPLPC_MANPLPR_FK2 
 FOREIGN KEY (NR_SEQ_PLANEJ_CONTADOR) 
 REFERENCES TASY.MAN_PLANEJ_PREV (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_PLANEJ_PREV_REGRA TO NIVEL_1;

