ALTER TABLE TASY.LIB_ITENS_SV_PHILIPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LIB_ITENS_SV_PHILIPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LIB_ITENS_SV_PHILIPS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  CD_INTEGRACAO        NUMBER(10),
  QT_MIN_ITEM_PHILIPS  NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LIISPH_PK ON TASY.LIB_ITENS_SV_PHILIPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIISPH_SVINPH_FK_I ON TASY.LIB_ITENS_SV_PHILIPS
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lib_itens_sv_philips_atual
before insert or update ON TASY.LIB_ITENS_SV_PHILIPS for each row
begin

if (:new.qt_min_item_philips mod 5 <> 0) then
    /*Intervalo deve ser m�ltiplo de 5*/
    wheb_mensagem_pck.exibir_mensagem_abort(389311);
end if;

end;
/


ALTER TABLE TASY.LIB_ITENS_SV_PHILIPS ADD (
  CONSTRAINT LIISPH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LIB_ITENS_SV_PHILIPS ADD (
  CONSTRAINT LIISPH_SVINPH_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.SV_INTEGRACAO_PHILIPS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LIB_ITENS_SV_PHILIPS TO NIVEL_1;

