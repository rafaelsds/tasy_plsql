ALTER TABLE TASY.ESCALA_ICPP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ICPP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ICPP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PONTOS              NUMBER(3),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  IE_PART_ACOMP          VARCHAR2(1 BYTE),
  IE_PART_ACOMP2         VARCHAR2(1 BYTE),
  IE_PART_ACOMP3         VARCHAR2(1 BYTE),
  IE_PART_ACOMP4         VARCHAR2(1 BYTE),
  IE_APOIO_FAM           VARCHAR2(1 BYTE),
  IE_APOIO_FAM2          VARCHAR2(1 BYTE),
  IE_APOIO_FAM3          VARCHAR2(1 BYTE),
  IE_APOIO_FAM4          VARCHAR2(1 BYTE),
  IE_ATIVIDADE           VARCHAR2(1 BYTE),
  IE_ATIVIDADE2          VARCHAR2(1 BYTE),
  IE_ATIVIDADE3          VARCHAR2(1 BYTE),
  IE_ATIVIDADE4          VARCHAR2(1 BYTE),
  IE_OXIGENACAO          VARCHAR2(1 BYTE),
  IE_OXIGENACAO2         VARCHAR2(1 BYTE),
  IE_OXIGENACAO3         VARCHAR2(1 BYTE),
  IE_OXIGENACAO4         VARCHAR2(1 BYTE),
  IE_MOB_DEAM            VARCHAR2(1 BYTE),
  IE_MOB_DEAM2           VARCHAR2(1 BYTE),
  IE_MOB_DEAM3           VARCHAR2(1 BYTE),
  IE_MOB_DEAM4           VARCHAR2(1 BYTE),
  IE_ALIM_HID            VARCHAR2(1 BYTE),
  IE_ALIM_HID2           VARCHAR2(1 BYTE),
  IE_ALIM_HID3           VARCHAR2(1 BYTE),
  IE_ALIM_HID4           VARCHAR2(1 BYTE),
  IE_ELIMINICACAO        VARCHAR2(1 BYTE),
  IE_ELIMINICACAO2       VARCHAR2(1 BYTE),
  IE_ELIMINICACAO3       VARCHAR2(1 BYTE),
  IE_ELIMINICACAO4       VARCHAR2(1 BYTE),
  IE_HIG_CORP            VARCHAR2(1 BYTE),
  IE_HIG_CORP2           VARCHAR2(1 BYTE),
  IE_HIG_CORP3           VARCHAR2(1 BYTE),
  IE_HIG_CORP4           VARCHAR2(1 BYTE),
  IE_INT_CONTROLE        VARCHAR2(1 BYTE),
  IE_INT_CONTROLE2       VARCHAR2(1 BYTE),
  IE_INT_CONTROLE3       VARCHAR2(1 BYTE),
  IE_INT_CONTROLE4       VARCHAR2(1 BYTE),
  IE_TERAP_MED           VARCHAR2(1 BYTE),
  IE_TERAP_MED2          VARCHAR2(1 BYTE),
  IE_TERAP_MED3          VARCHAR2(1 BYTE),
  IE_TERAP_MED4          VARCHAR2(1 BYTE),
  IE_INTEGRIDADE         VARCHAR2(1 BYTE),
  IE_INTEGRIDADE2        VARCHAR2(1 BYTE),
  IE_INTEGRIDADE3        VARCHAR2(1 BYTE),
  IE_INTEGRIDADE4        VARCHAR2(1 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCICPP_ATEPACI_FK_I ON TASY.ESCALA_ICPP
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCICPP_PESFISI_FK_I ON TASY.ESCALA_ICPP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCICPP_PK ON TASY.ESCALA_ICPP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_ICPP_ATUAL
before insert or update ON TASY.ESCALA_ICPP for each row
declare

begin
:new.qt_pontos := 0;

-- Participa��o do acompanhante
if	(:new.IE_PART_ACOMP = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_PART_ACOMP2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_PART_ACOMP3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_PART_ACOMP4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Rede de apoio e suporte familiar
if	(:new.IE_APOIO_FAM = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_APOIO_FAM2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_APOIO_FAM3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_APOIO_FAM4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Atividade
if	(:new.IE_ATIVIDADE = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_ATIVIDADE2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_ATIVIDADE3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_ATIVIDADE4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Oxigena��o
if	(:new.IE_OXIGENACAO = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_OXIGENACAO2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_OXIGENACAO3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_OXIGENACAO4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Mobilidade e deambula��o
if	(:new.IE_MOB_DEAM = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_MOB_DEAM2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_MOB_DEAM3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_MOB_DEAM4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Alimenta��o e  hidrata��o
if	(:new.IE_ALIM_HID = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_ALIM_HID2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_ALIM_HID3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_ALIM_HID4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Elimina��es
if	(:new.IE_ELIMINICACAO = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_ELIMINICACAO2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_ELIMINICACAO3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_ELIMINICACAO4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Higiene e cuidado corporal
if	(:new.IE_HIG_CORP = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_HIG_CORP2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_HIG_CORP3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_HIG_CORP4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Intervalo de aferi��o de controles
if	(:new.IE_INT_CONTROLE = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_INT_CONTROLE2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_INT_CONTROLE3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_INT_CONTROLE4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Terap�utica medicamentosa
if	(:new.IE_TERAP_MED = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_TERAP_MED2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_TERAP_MED3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_TERAP_MED4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

-- Integridade cut�neo mucosa
if	(:new.IE_INTEGRIDADE = 'S') then
	:new.qt_pontos := :new.qt_pontos + 1;
elsif	(:new.IE_INTEGRIDADE2 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 2;
elsif	(:new.IE_INTEGRIDADE3 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 3;
elsif	(:new.IE_INTEGRIDADE4 = 'S') then
	:new.qt_pontos := :new.qt_pontos + 4;
end if;

end;
/


ALTER TABLE TASY.ESCALA_ICPP ADD (
  CONSTRAINT ESCICPP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ICPP ADD (
  CONSTRAINT ESCICPP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCICPP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_ICPP TO NIVEL_1;

