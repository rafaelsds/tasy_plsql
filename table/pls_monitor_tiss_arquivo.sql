ALTER TABLE TASY.PLS_MONITOR_TISS_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_ARQUIVO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE_MONITOR  NUMBER(10)               NOT NULL,
  DT_GERACAO_ARQUIVO   DATE,
  NM_ARQUIVO           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_CONTEUDO          CLOB,
  NR_ARQUIVO           NUMBER(10),
  CD_PROTOCOLO_ANS     VARCHAR2(255 BYTE),
  IE_STATUS            VARCHAR2(2 BYTE),
  DS_ERRO              VARCHAR2(4000 BYTE),
  DS_NOME_ANTERIOR     VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_CONTEUDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTAR_I1 ON TASY.PLS_MONITOR_TISS_ARQUIVO
(NM_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTAR_PK ON TASY.PLS_MONITOR_TISS_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTAR_PLSMOTL_FK_I ON TASY.PLS_MONITOR_TISS_ARQUIVO
(NR_SEQ_LOTE_MONITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_MONITOR_TISS_ARQUIVO ADD (
  CONSTRAINT PLSMTAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_ARQUIVO ADD (
  CONSTRAINT PLSMTAR_PLSMOTL_FK 
 FOREIGN KEY (NR_SEQ_LOTE_MONITOR) 
 REFERENCES TASY.PLS_MONITOR_TISS_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_ARQUIVO TO NIVEL_1;

