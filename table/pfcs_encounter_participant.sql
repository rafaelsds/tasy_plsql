ALTER TABLE TASY.PFCS_ENCOUNTER_PARTICIPANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_ENCOUNTER_PARTICIPANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_ENCOUNTER_PARTICIPANT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ENCOUNTER     NUMBER(10)               DEFAULT null,
  CD_TYPE_PARTIC       VARCHAR2(30 BYTE),
  START_DATE           DATE,
  END_DATE             DATE,
  NR_SEQ_PRACTITIONER  NUMBER(10),
  PERIOD_START         DATE                     DEFAULT null,
  PERIOD_END           DATE                     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCSENPART_PFCSENC_FK_I ON TASY.PFCS_ENCOUNTER_PARTICIPANT
(NR_SEQ_ENCOUNTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSENPART_PFCSPRACT_FK_I ON TASY.PFCS_ENCOUNTER_PARTICIPANT
(NR_SEQ_PRACTITIONER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSENPART_PK ON TASY.PFCS_ENCOUNTER_PARTICIPANT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PFCS_ENCOUNTER_PARTICIPANT ADD (
  CONSTRAINT PFCSENPART_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PFCS_ENCOUNTER_PARTICIPANT ADD (
  CONSTRAINT PFCSENPART_PFCSENC_FK 
 FOREIGN KEY (NR_SEQ_ENCOUNTER) 
 REFERENCES TASY.PFCS_ENCOUNTER (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSENPART_PFCSPRACT_FK 
 FOREIGN KEY (NR_SEQ_PRACTITIONER) 
 REFERENCES TASY.PFCS_PRACTITIONER (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PFCS_ENCOUNTER_PARTICIPANT TO NIVEL_1;

