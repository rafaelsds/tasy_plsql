ALTER TABLE TASY.PLS_CONTESTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTESTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTESTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA         NUMBER(10)               NOT NULL,
  IE_STATUS            VARCHAR2(3 BYTE)         NOT NULL,
  VL_ORIGINAL          NUMBER(15,2)             NOT NULL,
  VL_CONTA             NUMBER(15,2)             NOT NULL,
  VL_ATUAL             NUMBER(15,2),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  NR_SEQ_FATURA_CONTA  NUMBER(10),
  NR_NOTA              VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCTST_PK ON TASY.PLS_CONTESTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCTST_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCTST_PLSCOME_FK_I ON TASY.PLS_CONTESTACAO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTST_PLSFACO_FK_I ON TASY.PLS_CONTESTACAO
(NR_SEQ_FATURA_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCTST_PLSFACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCTST_PLSLOCO_FK_I ON TASY.PLS_CONTESTACAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCTST_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_contestacao_delete
before delete ON TASY.PLS_CONTESTACAO for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - Controle de Contesta��es
Finalidade: Desbloquear os itens da fatura
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

nr_seq_fatura_w		number(10);
ds_aplicacao_w		Varchar2(50);

begin
select	max(nr_seq_pls_fatura)
into	nr_seq_fatura_w
from	pls_lote_contestacao
where	nr_sequencia = :old.nr_seq_lote;

if	(nr_seq_fatura_w is not null) then
	update	pls_fatura_proc
	set	ie_liberado	= 'S'
	where	nr_seq_fatura_conta in	(select	x.nr_sequencia
					from	pls_fatura_conta	x,
						pls_fatura_evento	z,
						pls_fatura		w
					where	w.nr_sequencia	= z.nr_seq_fatura
					and	z.nr_sequencia	= x.nr_seq_fatura_evento
					and	x.nr_seq_conta	= :old.nr_seq_conta
					and	w.nr_sequencia	= nr_seq_fatura_w);

	update	pls_fatura_mat
	set	ie_liberado	= 'S'
	where	nr_seq_fatura_conta in	(select	x.nr_sequencia
					from	pls_fatura_conta	x,
						pls_fatura_evento	z,
						pls_fatura		w
					where	w.nr_sequencia	= z.nr_seq_fatura
					and	z.nr_sequencia	= x.nr_seq_fatura_evento
					and	x.nr_seq_conta	= :old.nr_seq_conta
					and	w.nr_sequencia	= nr_seq_fatura_w);
end if;

if	(pls_se_aplicacao_tasy = 'S') then
	ds_aplicacao_w 		:= 'Aplicacao TASY ;';
else
	ds_aplicacao_w 		:= 'Banco ;';
end if;

insert into log_exclusao
	(nm_tabela,
	dt_atualizacao,
	nm_usuario,
	ds_chave)
values(	'PLS_CONTESTACAO',
	sysdate,
	:old.nm_usuario,
	substr('NR_SEQUENCIA='||:old.nr_sequencia||', NM_USUARIO='||:old.nm_usuario||', NM_USUARIO_NREC='||:old.nm_usuario_nrec||
	',NR_SEQ_CONTA='||:old.nr_seq_conta||', NR_SEQ_LOTE='||:old.nr_seq_lote||', M�quina='||wheb_usuario_pck.get_machine||' - '||ds_aplicacao_w,1,255));

end;
/


ALTER TABLE TASY.PLS_CONTESTACAO ADD (
  CONSTRAINT PLSCTST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTESTACAO ADD (
  CONSTRAINT PLSCTST_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCTST_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCTST_PLSFACO_FK 
 FOREIGN KEY (NR_SEQ_FATURA_CONTA) 
 REFERENCES TASY.PLS_FATURA_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTESTACAO TO NIVEL_1;

