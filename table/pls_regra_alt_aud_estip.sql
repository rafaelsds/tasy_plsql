ALTER TABLE TASY.PLS_REGRA_ALT_AUD_ESTIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ALT_AUD_ESTIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ALT_AUD_ESTIP
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_INICIO_VIGENCIA        DATE                NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA           DATE,
  IE_TIPO_GUIA              VARCHAR2(2 BYTE),
  QT_DIAS_SOLICITACAO_GUIA  NUMBER(5),
  IE_ALTERA_VALIDADE        VARCHAR2(1 BYTE),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_CONTRATO_INT       NUMBER(10),
  CD_ESTABELECIMENTO        NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREAAE_ESTABEL_FK_I ON TASY.PLS_REGRA_ALT_AUD_ESTIP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREAAE_PK ON TASY.PLS_REGRA_ALT_AUD_ESTIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAAE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAAE_PLSCONT_FK_I ON TASY.PLS_REGRA_ALT_AUD_ESTIP
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAAE_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAAE_PLSINCA_FK_I ON TASY.PLS_REGRA_ALT_AUD_ESTIP
(NR_SEQ_CONTRATO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAAE_PLSINCA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_ALT_AUD_ESTIP ADD (
  CONSTRAINT PLSREAAE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_ALT_AUD_ESTIP ADD (
  CONSTRAINT PLSREAAE_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREAAE_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_INT) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREAAE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_ALT_AUD_ESTIP TO NIVEL_1;

