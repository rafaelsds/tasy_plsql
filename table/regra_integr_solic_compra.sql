ALTER TABLE TASY.REGRA_INTEGR_SOLIC_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_INTEGR_SOLIC_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_INTEGR_SOLIC_COMPRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_SERVICO      VARCHAR2(15 BYTE)        NOT NULL,
  IE_INTEGRACAO        VARCHAR2(15 BYTE)        NOT NULL,
  CD_PERFIL            NUMBER(5),
  CD_CENTRO_CUSTO      NUMBER(8)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REINSCO_CENCUST_FK_I ON TASY.REGRA_INTEGR_SOLIC_COMPRA
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REINSCO_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REINSCO_PERFIL_FK_I ON TASY.REGRA_INTEGR_SOLIC_COMPRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REINSCO_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REINSCO_PK ON TASY.REGRA_INTEGR_SOLIC_COMPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_INTEGR_SOLIC_COMPRA_UP
before insert or update ON TASY.REGRA_INTEGR_SOLIC_COMPRA for each row
declare

qt_registro_w		number(10);
PRAGMA AUTONOMOUS_TRANSACTION;
begin

select	count(*)
into	qt_registro_w
from	regra_integr_solic_compra
where	ie_tipo_servico		= :new.ie_tipo_servico
and	ie_integracao		= :new.ie_integracao
and	nvl(cd_perfil,0)	= nvl(:new.cd_perfil,0)
and	nvl(cd_centro_custo,0)	= nvl(:new.cd_centro_custo,0);

if	(qt_registro_w > 0) then
	/*(-20011,'J� existe uma regra cadastrada com esse Tipo de solicita��o e Tipo de integra��o. N�o � permitido regras duplicadas.');*/
	wheb_mensagem_pck.Exibir_mensagem_abort(182216);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.REGRA_INTEGR_SOLIC_COMPRA_tp  after update ON TASY.REGRA_INTEGR_SOLIC_COMPRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_SERVICO,1,4000),substr(:new.IE_TIPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SERVICO',ie_log_w,ds_w,'REGRA_INTEGR_SOLIC_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'REGRA_INTEGR_SOLIC_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'REGRA_INTEGR_SOLIC_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTEGRACAO,1,4000),substr(:new.IE_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTEGRACAO',ie_log_w,ds_w,'REGRA_INTEGR_SOLIC_COMPRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_INTEGR_SOLIC_COMPRA ADD (
  CONSTRAINT REINSCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_INTEGR_SOLIC_COMPRA ADD (
  CONSTRAINT REINSCO_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT REINSCO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.REGRA_INTEGR_SOLIC_COMPRA TO NIVEL_1;

