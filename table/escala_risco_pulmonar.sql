ALTER TABLE TASY.ESCALA_RISCO_PULMONAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_RISCO_PULMONAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_RISCO_PULMONAR
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NR_SEQ_SAEP                    NUMBER(10),
  DT_AVALIACAO                   DATE           NOT NULL,
  DT_LIBERACAO                   DATE,
  CD_PROFISSIONAL                VARCHAR2(10 BYTE) NOT NULL,
  IE_ANEURISMA                   VARCHAR2(1 BYTE),
  QT_ANEURISMA                   NUMBER(2)      NOT NULL,
  IE_TORACICA                    VARCHAR2(1 BYTE) NOT NULL,
  QT_TORACICA                    NUMBER(2)      NOT NULL,
  IE_ABNOMINAL_ALTA              VARCHAR2(1 BYTE) NOT NULL,
  QT_ABNOMINAL_ALTA              NUMBER(2)      NOT NULL,
  IE_CABECA_PESCOCO              VARCHAR2(1 BYTE) NOT NULL,
  QT_CABECA_PESCOCO              NUMBER(2)      NOT NULL,
  IE_NEUROCIRURGIA               VARCHAR2(1 BYTE) NOT NULL,
  QT_NEUROCIRURGIA               NUMBER(2)      NOT NULL,
  IE_VASCULAR_ARTERIAL           VARCHAR2(1 BYTE) NOT NULL,
  QT_VASCULAR_ARTERIAL           NUMBER(2)      NOT NULL,
  IE_ANESTESIA_GERAL             VARCHAR2(1 BYTE) NOT NULL,
  QT_ANESTESIA_GERAL             NUMBER(2)      NOT NULL,
  IE_EMERGENCIA                  VARCHAR2(1 BYTE) NOT NULL,
  QT_EMERGENCIA                  NUMBER(2)      NOT NULL,
  IE_TRANSFUSAO                  VARCHAR2(1 BYTE) NOT NULL,
  QT_TRANSFUSAO                  NUMBER(2)      NOT NULL,
  QT_IDADE                       NUMBER(3)      NOT NULL,
  QT_PONTOS_IDADE                NUMBER(2)      NOT NULL,
  IE_STATUS_FUNCIONAL            NUMBER(2)      NOT NULL,
  IE_DIMINUICAO_PESO             VARCHAR2(1 BYTE) NOT NULL,
  QT_DIMINUICAO_PESO             NUMBER(2)      NOT NULL,
  IE_DPOC                        VARCHAR2(1 BYTE) NOT NULL,
  QT_DPOC                        NUMBER(2)      NOT NULL,
  IE_ACIDENTE_VASCULAR_CEREBRAL  VARCHAR2(1 BYTE) NOT NULL,
  QT_ACIDENTE_VASCULAR_CEREBRAL  NUMBER(2)      NOT NULL,
  IE_DIMINUICAO_CONSCIENCIA      VARCHAR2(1 BYTE) NOT NULL,
  QT_DIMINUICAO_CONSCIENCIA      NUMBER(2)      NOT NULL,
  QT_UREIA                       NUMBER(15,3),
  QT_PONTOS_UREIA                NUMBER(2)      NOT NULL,
  IE_CORTICOIDE_CRONICO          VARCHAR2(1 BYTE) NOT NULL,
  QT_CORTICOIDE_CRONICO          NUMBER(2)      NOT NULL,
  IE_TABAGISMO                   VARCHAR2(1 BYTE) NOT NULL,
  QT_TABAGISMO                   NUMBER(2)      NOT NULL,
  IE_ALCOOL                      VARCHAR2(1 BYTE) NOT NULL,
  QT_ALCOOL                      NUMBER(2)      NOT NULL,
  QT_PONTOS                      NUMBER(5)      NOT NULL,
  NR_ATENDIMENTO                 NUMBER(10),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DT_INATIVACAO                  DATE,
  NM_USUARIO_INATIVACAO          VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA               VARCHAR2(255 BYTE),
  NR_SEQ_AVAL_PRE                NUMBER(10),
  NR_SEQ_REG_ELEMENTO            NUMBER(10),
  NR_HORA                        NUMBER(2),
  NR_SEQ_ASSINATURA              NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO      NUMBER(10),
  IE_NIVEL_ATENCAO               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESRIPUL_ATEPACI_FK_I ON TASY.ESCALA_RISCO_PULMONAR
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESRIPUL_AVAPRAN_FK_I ON TASY.ESCALA_RISCO_PULMONAR
(NR_SEQ_AVAL_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESRIPUL_PESFISI_FK_I ON TASY.ESCALA_RISCO_PULMONAR
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESRIPUL_PK ON TASY.ESCALA_RISCO_PULMONAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESRIPUL_SAEPERO_FK_I ON TASY.ESCALA_RISCO_PULMONAR
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESRIPUL_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESRIPUL_TASASDI_FK_I ON TASY.ESCALA_RISCO_PULMONAR
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESRIPUL_TASASDI_FK2_I ON TASY.ESCALA_RISCO_PULMONAR
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_risco_pulmonar_atual
before insert or update ON TASY.ESCALA_RISCO_PULMONAR for each row
declare

qt_status_funcional_w	number(2,0);
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if (:new.ie_aneurisma = 'S') then
	:new.qt_aneurisma := 15;
else
	:new.qt_aneurisma := 0;
end if;

if (:new.ie_toracica = 'S') then
	:new.qt_toracica := 14;
else
	:new.qt_toracica := 0;
end if;

if (:new.ie_abnominal_alta = 'S') then
	:new.qt_abnominal_alta := 10;
else
	:new.qt_abnominal_alta := 0;
end if;

if (:new.ie_cabeca_pescoco = 'S') then
	:new.qt_cabeca_pescoco := 8;
else
	:new.qt_cabeca_pescoco := 0;
end if;

if (:new.ie_neurocirurgia = 'S') then
	:new.qt_neurocirurgia := 8;
else
	:new.qt_neurocirurgia := 0;
end if;

if (:new.ie_vascular_arterial = 'S') then
	:new.qt_vascular_arterial := 3;
else
	:new.qt_vascular_arterial := 0;
end if;

if (:new.ie_anestesia_geral = 'S') then
	:new.qt_anestesia_geral := 4;
else
	:new.qt_anestesia_geral := 0;
end if;

if (:new.ie_emergencia = 'S') then
	:new.qt_emergencia := 3;
else
	:new.qt_emergencia := 0;
end if;

if (:new.ie_transfusao = 'S') then
	:new.qt_transfusao := 3;
else
	:new.qt_transfusao := 0;
end if;

if (:new.qt_idade >= 80) then
	:new.qt_pontos_idade := 17;
elsif (:new.qt_idade >= 70) then
	:new.qt_pontos_idade := 13;
elsif (:new.qt_idade >= 60) then
	:new.qt_pontos_idade := 9;
elsif (:new.qt_idade >= 50) then
	:new.qt_pontos_idade := 4;
else
	:new.qt_pontos_idade := 0;
end if;

if (nvl(:new.ie_status_funcional,-1) = -1) then
	qt_status_funcional_w := 0;
else
	qt_status_funcional_w := :new.ie_status_funcional;
end if;

if (:new.ie_diminuicao_peso = 'S') then
	:new.qt_diminuicao_peso := 7;
else
	:new.qt_diminuicao_peso := 0;
end if;

if (:new.ie_dpoc = 'S') then
	:new.qt_dpoc := 5;
else
	:new.qt_dpoc := 0;
end if;

if (:new.ie_acidente_vascular_cerebral = 'S') then
	:new.qt_acidente_vascular_cerebral := 4;
else
	:new.qt_acidente_vascular_cerebral := 0;
end if;

if (:new.ie_diminuicao_consciencia = 'S') then
	:new.qt_diminuicao_consciencia := 4;
else
	:new.qt_diminuicao_consciencia := 0;
end if;

if (:new.qt_ureia < 16) then
	:new.qt_pontos_ureia := 4;
elsif (:new.qt_ureia >= 44) and (:new.qt_ureia <= 60) then
	:new.qt_pontos_ureia := 2;
elsif (:new.qt_ureia > 60) then
	:new.qt_pontos_ureia := 3;
else
	:new.qt_pontos_ureia := 0;
end if;

if (:new.ie_corticoide_cronico = 'S') then
	:new.qt_corticoide_cronico := 3;
else
	:new.qt_corticoide_cronico := 0;
end if;

if (:new.ie_tabagismo = 'S') then
	:new.qt_tabagismo := 3;
else
	:new.qt_tabagismo := 0;
end if;

if (:new.ie_alcool = 'S') then
	:new.qt_alcool := 2;
else
	:new.qt_alcool := 0;
end if;

:new.qt_pontos := 	:new.qt_aneurisma +
					:new.qt_toracica +
					:new.qt_abnominal_alta +
					:new.qt_cabeca_pescoco +
					:new.qt_neurocirurgia +
					:new.qt_vascular_arterial +
					:new.qt_anestesia_geral +
					:new.qt_emergencia +
					:new.qt_transfusao +
					:new.qt_pontos_idade +
					qt_status_funcional_w +
					:new.qt_diminuicao_peso +
					:new.qt_dpoc +
					:new.qt_acidente_vascular_cerebral +
					:new.qt_diminuicao_consciencia +
					:new.qt_pontos_ureia +
					:new.qt_corticoide_cronico +
					:new.qt_tabagismo +
					:new.qt_alcool;
<<Final>>
qt_reg_w	:= 0;

end escala_risco_pulmonar_atual;
/


ALTER TABLE TASY.ESCALA_RISCO_PULMONAR ADD (
  CONSTRAINT ESRIPUL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_RISCO_PULMONAR ADD (
  CONSTRAINT ESRIPUL_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESRIPUL_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT ESRIPUL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESRIPUL_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_AVAL_PRE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA),
  CONSTRAINT ESRIPUL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESRIPUL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_RISCO_PULMONAR TO NIVEL_1;

