ALTER TABLE TASY.UNITARIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNITARIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNITARIZACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SOLIC_UNITARIZACAO  NUMBER(10)             NOT NULL,
  DT_INICIO_PROCESSO     DATE,
  DT_FIM_PROCESSO        DATE,
  DT_LIBERACAO           DATE,
  DT_GERACAO_LOTE        DATE,
  CD_PESSOA_MANIPULACAO  VARCHAR2(10 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  DS_LOTE_FORNEC         VARCHAR2(20 BYTE),
  QT_MATERIAL            NUMBER(13,4)           NOT NULL,
  DT_ENTREGA             DATE,
  CD_PESSOA_ENTREGA      VARCHAR2(10 BYTE),
  DT_VALIDADE_LOTE       DATE,
  IE_TIPO_PROCESSO       VARCHAR2(15 BYTE),
  IE_PERDA_ESTABILIDADE  VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL_UNIT      NUMBER(10),
  CD_PESSOA_SUPERVISAO   VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UNITRZC_PESFISI_FK_I ON TASY.UNITARIZACAO
(CD_PESSOA_MANIPULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNITRZC_PESFISI_FK2_I ON TASY.UNITARIZACAO
(CD_PESSOA_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNITRZC_PESFISI_FK3_I ON TASY.UNITARIZACAO
(CD_PESSOA_SUPERVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNITRZC_PK ON TASY.UNITARIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNITRZC_SOLUNIT_FK_I ON TASY.UNITARIZACAO
(NR_SOLIC_UNITARIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNITRZC_SOLUNIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNITRZC_UNITLOC_FK_I ON TASY.UNITARIZACAO
(NR_SEQ_LOCAL_UNIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNITRZC_UNITLOC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.unitarizacao_atual
before insert or update ON TASY.UNITARIZACAO for each row
declare
ie_manipulacao_w	varchar2(1);
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;
begin
if	(:new.cd_pessoa_manipulacao is not null) then
	select	nvl(min(ie_manipulacao),'N')
	into	ie_manipulacao_w
	from	sup_pessoa_unitarizacao
	where	cd_estabelecimento = cd_estabelecimento_w
	and	cd_pessoa_fisica = :new.cd_pessoa_manipulacao
	and	ie_situacao = 'A';

	if	(ie_manipulacao_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(266345);
		--'Pessoa f�sica n�o permitida para manipula��o!');
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.UNITARIZACAO_tp  after update ON TASY.UNITARIZACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_MANIPULACAO,1,4000),substr(:new.CD_PESSOA_MANIPULACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_MANIPULACAO',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_SUPERVISAO,1,4000),substr(:new.CD_PESSOA_SUPERVISAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SUPERVISAO',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PROCESSO,1,4000),substr(:new.IE_TIPO_PROCESSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PROCESSO',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERDA_ESTABILIDADE,1,4000),substr(:new.IE_PERDA_ESTABILIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERDA_ESTABILIDADE',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCAL_UNIT,1,4000),substr(:new.NR_SEQ_LOCAL_UNIT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCAL_UNIT',ie_log_w,ds_w,'UNITARIZACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.UNITARIZACAO ADD (
  CONSTRAINT UNITRZC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNITARIZACAO ADD (
  CONSTRAINT UNITRZC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_MANIPULACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT UNITRZC_SOLUNIT_FK 
 FOREIGN KEY (NR_SOLIC_UNITARIZACAO) 
 REFERENCES TASY.SOLIC_UNITARIZACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT UNITRZC_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_ENTREGA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT UNITRZC_UNITLOC_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_UNIT) 
 REFERENCES TASY.UNITARIZACAO_LOCAL (NR_SEQUENCIA),
  CONSTRAINT UNITRZC_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_SUPERVISAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.UNITARIZACAO TO NIVEL_1;

