ALTER TABLE TASY.MEDICO_ESPECIALIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICO_ESPECIALIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICO_ESPECIALIDADE
(
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  CD_ESPECIALIDADE         NUMBER(5)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_PRIORIDADE        NUMBER(3)            NOT NULL,
  NR_SEQ_CBO_SAUDE         NUMBER(10),
  IE_PLANO_SAUDE           VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE_CNS     NUMBER(10),
  NR_RQE                   VARCHAR2(20 BYTE),
  CD_ESPECIALIDADE_INST    NUMBER(10),
  DT_ESPECIALIDADE         DATE,
  IE_FORMA_OBTENCAO_ESPEC  VARCHAR2(15 BYTE),
  IE_RESIDENCIA_SAUDE      VARCHAR2(1 BYTE),
  IE_POS_GRAD_360          VARCHAR2(1 BYTE),
  IE_ATIVO_CIPS            VARCHAR2(1 BYTE),
  NR_BSNR                  VARCHAR2(25 BYTE),
  DT_VENCIMENTO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.MEDICO_ESPECIALIDADE.CD_PESSOA_FISICA IS 'Codigo da Pessoa fisica';

COMMENT ON COLUMN TASY.MEDICO_ESPECIALIDADE.CD_ESPECIALIDADE IS 'Codigo da Especialidade Medica';


CREATE INDEX TASY.MEDESPE_CBOSAUD_FK_I ON TASY.MEDICO_ESPECIALIDADE
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDESPE_CBOSAUD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDESPE_ESPINST_FK_I ON TASY.MEDICO_ESPECIALIDADE
(CD_ESPECIALIDADE_INST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDESPE_ESPINST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDESPE_ESPMEDI_FK_I ON TASY.MEDICO_ESPECIALIDADE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDESPE_MEDICO_FK_I ON TASY.MEDICO_ESPECIALIDADE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDESPE_PK ON TASY.MEDICO_ESPECIALIDADE
(CD_PESSOA_FISICA, CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.medico_espec_befupdate_log
before update ON TASY.MEDICO_ESPECIALIDADE for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into medico_especialidade_log(
		nr_sequencia,
		cd_especialidade,
		cd_pessoa_fisica,
		nr_seq_prioridade,
		nr_seq_cbo_saude,
		dt_atualizacao,
		ie_plano_saude,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_rqe,
		cd_especialidade_inst,
		cd_especialidade_cns)
	values( medico_especialidade_log_seq.NextVal,
		:old.cd_especialidade,
		:new.cd_pessoa_fisica,
		:old.nr_seq_prioridade,
		:old.nr_seq_cbo_saude,
		sysdate,
		:old.ie_plano_saude,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.nr_rqe,
		:old.cd_especialidade_inst,
		:old.cd_especialidade_cns);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.medico_especialidade_atual
after insert or update or delete ON TASY.MEDICO_ESPECIALIDADE for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if (DELETING) then
		begin
		send_physician_intpd(:old.cd_pessoa_fisica, :old.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');
		end;
	else
		begin
		send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');
		end;
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MEDICO_ESPECIALIDADE_tp  after update ON TASY.MEDICO_ESPECIALIDADE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_PESSOA_FISICA='||to_char(:old.CD_PESSOA_FISICA)||'#@#@CD_ESPECIALIDADE='||to_char(:old.CD_ESPECIALIDADE);gravar_log_alteracao(substr(:old.NR_SEQ_CBO_SAUDE,1,4000),substr(:new.NR_SEQ_CBO_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBO_SAUDE',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_INST,1,4000),substr(:new.CD_ESPECIALIDADE_INST,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_INST',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PLANO_SAUDE,1,4000),substr(:new.IE_PLANO_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PLANO_SAUDE',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_CNS,1,4000),substr(:new.CD_ESPECIALIDADE_CNS,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_CNS',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRIORIDADE,1,4000),substr(:new.NR_SEQ_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRIORIDADE',ie_log_w,ds_w,'MEDICO_ESPECIALIDADE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MEDICO_ESPECIALIDADE ADD (
  CONSTRAINT MEDESPE_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA, CD_ESPECIALIDADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDICO_ESPECIALIDADE ADD (
  CONSTRAINT MEDESPE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT MEDESPE_MEDICO_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT MEDESPE_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT MEDESPE_ESPINST_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_INST) 
 REFERENCES TASY.ESPECIALIDADE_INSTITUICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MEDICO_ESPECIALIDADE TO NIVEL_1;

