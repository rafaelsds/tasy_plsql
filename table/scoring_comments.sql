ALTER TABLE TASY.SCORING_COMMENTS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCORING_COMMENTS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCORING_COMMENTS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_RECORD_TYPE       VARCHAR2(3 BYTE),
  CD_HOSPITAL          VARCHAR2(20 BYTE),
  NR_CASE              VARCHAR2(12 BYTE),
  NR_RECORD_ID         VARCHAR2(64 BYTE),
  DT_ADMISSION         VARCHAR2(8 BYTE),
  DS_COMMENT1          VARCHAR2(200 BYTE),
  DS_COMMENT2          VARCHAR2(200 BYTE),
  DS_COMMENT3          VARCHAR2(200 BYTE),
  DS_COMMENT4          VARCHAR2(200 BYTE),
  DS_COMMENT5          VARCHAR2(200 BYTE),
  DS_COMMENT6          VARCHAR2(200 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCCOMM_PK ON TASY.SCORING_COMMENTS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCORING_COMMENTS ADD (
  CONSTRAINT SCCOMM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

