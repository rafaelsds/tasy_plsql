ALTER TABLE TASY.INTPD_DEVOL_MAT_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_DEVOL_MAT_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_DEVOL_MAT_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MATPACI       NUMBER(10),
  NR_SEQ_PROPACI       NUMBER(10),
  NR_SEQ_ORIGINAL      NUMBER(10),
  QT_DEVOLUCAO         NUMBER(13,4)             NOT NULL,
  DT_DEVOLUCAO         DATE                     NOT NULL,
  DT_INTEGRACAO        DATE,
  NR_ATENDIMENTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPDDMP_MATPACI_FK_I ON TASY.INTPD_DEVOL_MAT_PROC
(NR_SEQ_MATPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPDDMP_PK ON TASY.INTPD_DEVOL_MAT_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDDMP_PROPACI_FK_I ON TASY.INTPD_DEVOL_MAT_PROC
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.intpd_devol_mat_proc_after
after insert ON TASY.INTPD_DEVOL_MAT_PROC for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin

	if (:new.nr_seq_propaci is not null) then

		reg_integracao_p.ie_operacao				:= 'I';
		reg_integracao_p.cd_estab_documento 		:= wheb_usuario_pck.get_cd_estabelecimento();
		reg_integracao_p.ie_status					:= 'P';
		reg_integracao_p.nr_seq_item_documento_p 	:= '4';
		reg_integracao_p.nr_seq_agrupador			:= null;

		gerar_int_padrao.gravar_integracao('161', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);

	end if;

	if (:new.nr_seq_matpaci is not null) then

		reg_integracao_p.ie_operacao				:= 'I';
		reg_integracao_p.cd_estab_documento 		:= wheb_usuario_pck.get_cd_estabelecimento();
		reg_integracao_p.ie_status					:= 'P';
		reg_integracao_p.nr_seq_item_documento_p 	:= '3';
		reg_integracao_p.nr_seq_agrupador			:= null;

		gerar_int_padrao.gravar_integracao('161', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);

	end if;

end intpd_devol_mat_proc_after;
/


ALTER TABLE TASY.INTPD_DEVOL_MAT_PROC ADD (
  CONSTRAINT INTPDDMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTPD_DEVOL_MAT_PROC ADD (
  CONSTRAINT INTPDDMP_MATPACI_FK 
 FOREIGN KEY (NR_SEQ_MATPACI) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INTPDDMP_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INTPD_DEVOL_MAT_PROC TO NIVEL_1;

