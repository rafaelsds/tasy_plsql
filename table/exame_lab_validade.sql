ALTER TABLE TASY.EXAME_LAB_VALIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LAB_VALIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LAB_VALIDADE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  NR_SEQ_EXAME_LAB     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXALABVA_EXALABO_FK_I ON TASY.EXAME_LAB_VALIDADE
(NR_SEQ_EXAME_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EXALABVA_PK ON TASY.EXAME_LAB_VALIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EXAME_LAB_VALIDADE ADD (
  CONSTRAINT EXALABVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.EXAME_LAB_VALIDADE ADD (
  CONSTRAINT EXALABVA_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME_LAB) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME));

