ALTER TABLE TASY.PIX_MOVIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PIX_MOVIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PIX_MOVIMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_COBRANCA      NUMBER(10)               NOT NULL,
  DS_ENDTOEND_ID       VARCHAR2(255 BYTE),
  DS_TXID              VARCHAR2(255 BYTE),
  VL_MOVIMENTO         NUMBER(15,2),
  DS_INFO_PAGADOR      VARCHAR2(140 BYTE),
  DT_MOVIMENTO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PIXMOV_PIXCOBR_FK_I ON TASY.PIX_MOVIMENTO
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PIXMOV_PK ON TASY.PIX_MOVIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PIX_MOVIMENTO_tp  after update ON TASY.PIX_MOVIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_ENDTOEND_ID,1,4000),substr(:new.DS_ENDTOEND_ID,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDTOEND_ID',ie_log_w,ds_w,'PIX_MOVIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MOVIMENTO,1,4000),substr(:new.VL_MOVIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MOVIMENTO',ie_log_w,ds_w,'PIX_MOVIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TXID,1,4000),substr(:new.DS_TXID,1,4000),:new.nm_usuario,nr_seq_w,'DS_TXID',ie_log_w,ds_w,'PIX_MOVIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PIX_MOVIMENTO ADD (
  CONSTRAINT PIXMOV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PIX_MOVIMENTO ADD (
  CONSTRAINT PIXMOV_PIXCOBR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.PIX_COBRANCA (NR_SEQUENCIA));

