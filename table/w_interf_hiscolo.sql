ALTER TABLE TASY.W_INTERF_HISCOLO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTERF_HISCOLO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTERF_HISCOLO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  NR_SEQ_PROTOCOLO       NUMBER(10),
  NR_SEQ_SISCOLO         NUMBER(10),
  NR_INTERNO_CONTA       NUMBER(10),
  NR_ATENDIMENTO         NUMBER(10),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  CD_CNPJ_UNIDADE_SAUDE  VARCHAR2(14 BYTE),
  DT_EMISSAO             DATE,
  DT_LIBERACAO           DATE,
  NR_SEQ_UNIDADE_SAUDE   NUMBER(10),
  C_CNES                 VARCHAR2(7 BYTE),
  C_EXAME                VARCHAR2(14 BYTE),
  C_ID_NOME              VARCHAR2(70 BYTE),
  C_NOMEMAE              VARCHAR2(70 BYTE),
  C_ID_APEL              VARCHAR2(30 BYTE),
  D_DTNASC               DATE,
  C_ID_IDENT             VARCHAR2(12 BYTE),
  C_ID_EMIS              VARCHAR2(5 BYTE),
  C_ID_UFIDE             VARCHAR2(2 BYTE),
  C_ID_CIC               VARCHAR2(11 BYTE),
  C_ID_ESCO              VARCHAR2(1 BYTE),
  C_ENDERECO             VARCHAR2(35 BYTE),
  C_NUMERO               VARCHAR2(6 BYTE),
  C_COMPLEM              VARCHAR2(15 BYTE),
  C_BAIRRO               VARCHAR2(15 BYTE),
  C_ID_UF                VARCHAR2(2 BYTE),
  C_IBGE                 VARCHAR2(7 BYTE),
  C_ID_CEP               VARCHAR2(8 BYTE),
  C_ID_REFE              VARCHAR2(35 BYTE),
  C_ID_FONE              VARCHAR2(11 BYTE),
  C_ID_SUS               VARCHAR2(15 BYTE),
  C_US_UF                VARCHAR2(2 BYTE),
  C_US_IBGE              VARCHAR2(7 BYTE),
  C_US_CNES              VARCHAR2(7 BYTE),
  C_US_NOME              VARCHAR2(40 BYTE),
  C_PRON                 VARCHAR2(12 BYTE),
  C_CIT_ESC              VARCHAR2(1 BYTE),
  C_CIT_GLA              VARCHAR2(1 BYTE),
  C_CIT_IND              VARCHAR2(1 BYTE),
  C_CIT_ESCA             VARCHAR2(1 BYTE),
  C_CIT_GLAN             VARCHAR2(1 BYTE),
  C_CIT_OUTR             VARCHAR2(40 BYTE),
  C_COL_COLP             VARCHAR2(1 BYTE),
  C_COL_PNIC             VARCHAR2(1 BYTE),
  C_COL_PINV             VARCHAR2(1 BYTE),
  C_COL_FRIO             VARCHAR2(1 BYTE),
  C_COL_CURE             VARCHAR2(1 BYTE),
  C_COL_CAF              VARCHAR2(1 BYTE),
  C_COL_EXER             VARCHAR2(1 BYTE),
  C_COL_RCAN             VARCHAR2(1 BYTE),
  C_COL_BIOP             VARCHAR2(1 BYTE),
  C_COL_ADIC             VARCHAR2(50 BYTE),
  C_RES_TIPO             VARCHAR2(1 BYTE),
  C_RES_MACR             VARCHAR2(100 BYTE),
  C_RES_BIOP             VARCHAR2(1 BYTE),
  C_RES_FRAG             VARCHAR2(2 BYTE),
  C_RES_PECA             VARCHAR2(1 BYTE),
  C_RES_TAM1             VARCHAR2(4 BYTE),
  C_RES_TAM2             VARCHAR2(4 BYTE),
  C_RES_MARG             VARCHAR2(4 BYTE),
  C_RES_LOCA             VARCHAR2(1 BYTE),
  C_BEN_META             VARCHAR2(1 BYTE),
  C_BEN_POLI             VARCHAR2(1 BYTE),
  C_BEN_CERV             VARCHAR2(1 BYTE),
  C_BEN_ALTE             VARCHAR2(1 BYTE),
  C_NEO_NICA             VARCHAR2(1 BYTE),
  C_NEO_ADEN             VARCHAR2(1 BYTE),
  C_NEO_OUTR             VARCHAR2(40 BYTE),
  C_DIF_GRAU             VARCHAR2(1 BYTE),
  C_EXT_PROF             VARCHAR2(4 BYTE),
  C_EXT_VASC             VARCHAR2(1 BYTE),
  C_EXT_PERI             VARCHAR2(1 BYTE),
  C_EXT_PARA             VARCHAR2(1 BYTE),
  C_EXT_CORP             VARCHAR2(1 BYTE),
  C_EXT_VAGI             VARCHAR2(1 BYTE),
  C_EXT_LEXA             VARCHAR2(2 BYTE),
  C_EXT_LCOM             VARCHAR2(2 BYTE),
  C_MAR_MARG             VARCHAR2(1 BYTE),
  C_DIAG_DES             VARCHAR2(100 BYTE),
  C_COM_FRAG             VARCHAR2(2 BYTE),
  C_COM_BLOC             VARCHAR2(2 BYTE),
  C_MAT_INS              VARCHAR2(1 BYTE),
  C_MAT_INSA             VARCHAR2(70 BYTE),
  D_EXAME                DATE,
  D_RECEBE               DATE,
  D_LIBERA               DATE,
  C_PAT_CNS              VARCHAR2(15 BYTE),
  C_PAT_CIC              VARCHAR2(11 BYTE),
  C_ERRO                 VARCHAR2(100 BYTE),
  C_ID_RACA              VARCHAR2(2 BYTE),
  C_ID_IDAD              VARCHAR2(2 BYTE),
  C_ETNIA                VARCHAR2(3 BYTE),
  C_NACIONAL             VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINHISC_PK ON TASY.W_INTERF_HISCOLO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINHISC_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INTERF_HISCOLO ADD (
  CONSTRAINT WINHISC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INTERF_HISCOLO TO NIVEL_1;

