ALTER TABLE TASY.W_SUSAIH_INTERF_REGCIVIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_SUSAIH_INTERF_REGCIVIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_SUSAIH_INTERF_REGCIVIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_REGISTRO_DN       NUMBER(11),
  NM_RECEM_NATO        VARCHAR2(70 BYTE),
  CD_CGC_CARTORIO      VARCHAR2(14 BYTE),
  NR_LIVRO             VARCHAR2(8 BYTE),
  NR_FOLHA             VARCHAR2(4 BYTE),
  NR_TERMO             VARCHAR2(8 BYTE),
  DT_EMISSAO_RN        DATE,
  NR_AIH               NUMBER(13),
  NR_INTERNO_CONTA     NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_REGISTRO_CIVIL    VARCHAR2(1312 BYTE),
  DS_RAZAO_SOCIAL      VARCHAR2(20 BYTE),
  NR_SEQ_REG_CIVIL     NUMBER(3),
  NR_LINHA_PROCED      VARCHAR2(3 BYTE),
  NR_MATRICULA_RN      VARCHAR2(32 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WSUSIRC_CONPACI_I ON TASY.W_SUSAIH_INTERF_REGCIVIL
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WSUSIRC_PK ON TASY.W_SUSAIH_INTERF_REGCIVIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WSUSIRC_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_SUSAIH_INTERF_REGCIVIL ADD (
  CONSTRAINT WSUSIRC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_SUSAIH_INTERF_REGCIVIL TO NIVEL_1;

