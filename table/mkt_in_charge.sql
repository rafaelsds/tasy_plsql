ALTER TABLE TASY.MKT_IN_CHARGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MKT_IN_CHARGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MKT_IN_CHARGE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  DS_NOTE                 VARCHAR2(500 BYTE),
  NR_SEQ_PRODUTO          NUMBER(10),
  NR_SEQ_MERCADO          NUMBER(10),
  NR_SEQ_PRODUTO_MERCADO  NUMBER(10),
  NR_SEQ_PRODUTO_AREA     NUMBER(10),
  NR_SEQ_AREA_MERCADO     NUMBER(10),
  NR_SEQ_AREA_MODULO      NUMBER(10),
  NR_SEQ_MODULO_MERCADO   NUMBER(10),
  NR_SEQ_MODULO_FUNCAO    NUMBER(10),
  NR_SEQ_FUNCAO_MERCADO   NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MKTINCHAR_MKTAREAMER_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_AREA_MERCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTAREAMOD_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_AREA_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTFUNMERC_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_FUNCAO_MERCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTMERCADO_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_MERCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTMODFUN_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_MODULO_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTMODMERC_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_MODULO_MERCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTPRODARE_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_PRODUTO_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTPROD_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_MKTPRODMER_FK_I ON TASY.MKT_IN_CHARGE
(NR_SEQ_PRODUTO_MERCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MKTINCHAR_PESFISI_FK_I ON TASY.MKT_IN_CHARGE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MKTINCHAR_PK ON TASY.MKT_IN_CHARGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MKT_IN_CHARGE ADD (
  CONSTRAINT MKTINCHAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MKT_IN_CHARGE ADD (
  CONSTRAINT MKTINCHAR_MKTAREAMER_FK 
 FOREIGN KEY (NR_SEQ_AREA_MERCADO) 
 REFERENCES TASY.MKT_AREA_MERCADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTAREAMOD_FK 
 FOREIGN KEY (NR_SEQ_AREA_MODULO) 
 REFERENCES TASY.MKT_AREA_MODULO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTFUNMERC_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO_MERCADO) 
 REFERENCES TASY.MKT_FUNCAO_MERCADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTMERCADO_FK 
 FOREIGN KEY (NR_SEQ_MERCADO) 
 REFERENCES TASY.MKT_MERCADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTMODFUN_FK 
 FOREIGN KEY (NR_SEQ_MODULO_FUNCAO) 
 REFERENCES TASY.MKT_MODULO_FUNCAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTMODMERC_FK 
 FOREIGN KEY (NR_SEQ_MODULO_MERCADO) 
 REFERENCES TASY.MKT_MODULO_MERCADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTPRODARE_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO_AREA) 
 REFERENCES TASY.MKT_PRODUTO_AREA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTPRODMER_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO_MERCADO) 
 REFERENCES TASY.MKT_PRODUTO_MERCADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_MKTPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.MKT_PRODUTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MKTINCHAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MKT_IN_CHARGE TO NIVEL_1;

