ALTER TABLE TASY.DSB_IND_DIMENSAO_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DSB_IND_DIMENSAO_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.DSB_IND_DIMENSAO_PAIS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PAIS              NUMBER(5)                NOT NULL,
  NR_SEQ_DIMENSAO      NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FILTRO        NUMBER(10),
  NR_SEQ_DIMENSION     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DSINDIPAIS_INDDIM_FK_I ON TASY.DSB_IND_DIMENSAO_PAIS
(NR_SEQ_DIMENSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSINDIPAIS_INDFIL_FK_I ON TASY.DSB_IND_DIMENSAO_PAIS
(NR_SEQ_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSINDIPAIS_INDGEAT_FK_I ON TASY.DSB_IND_DIMENSAO_PAIS
(NR_SEQ_DIMENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DSINDIPAIS_PK ON TASY.DSB_IND_DIMENSAO_PAIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSINDIPAIS_TASYPAI_FK_I ON TASY.DSB_IND_DIMENSAO_PAIS
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DSB_IND_DIMENSAO_PAIS ADD (
  CONSTRAINT DSINDIPAIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DSB_IND_DIMENSAO_PAIS ADD (
  CONSTRAINT DSINDIPAIS_INDFIL_FK 
 FOREIGN KEY (NR_SEQ_FILTRO) 
 REFERENCES TASY.IND_FILTRO (NR_SEQUENCIA),
  CONSTRAINT DSINDIPAIS_TASYPAI_FK 
 FOREIGN KEY (CD_PAIS) 
 REFERENCES TASY.TASY_PAIS (CD_PAIS),
  CONSTRAINT DSINDIPAIS_INDGEAT_FK 
 FOREIGN KEY (NR_SEQ_DIMENSAO) 
 REFERENCES TASY.INDICADOR_GESTAO_ATRIB (NR_SEQUENCIA),
  CONSTRAINT DSINDIPAIS_INDDIM_FK 
 FOREIGN KEY (NR_SEQ_DIMENSION) 
 REFERENCES TASY.IND_DIMENSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DSB_IND_DIMENSAO_PAIS TO NIVEL_1;

