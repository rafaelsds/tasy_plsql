ALTER TABLE TASY.PLS_REC_REGRA_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REC_REGRA_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REC_REGRA_VALOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_VALOR_REC_CALCULADO  VARCHAR2(1 BYTE),
  IE_ACAO                 VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_PRESTADOR        NUMBER(10),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE)    NOT NULL,
  DT_INICIO_VIGENCIA      DATE,
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA         DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_SEQ_MOTIVO_GLOSA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRECVAL_ESTABEL_FK_I ON TASY.PLS_REC_REGRA_VALOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRECVAL_PK ON TASY.PLS_REC_REGRA_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECVAL_PLSPRES_FK_I ON TASY.PLS_REC_REGRA_VALOR
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECVAL_TISSMGL_FK_I ON TASY.PLS_REC_REGRA_VALOR
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REC_REGRA_VALOR_tp  after update ON TASY.PLS_REC_REGRA_VALOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_REC_REGRA_VALOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACAO,1,4000),substr(:new.IE_ACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO',ie_log_w,ds_w,'PLS_REC_REGRA_VALOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA,1,4000),substr(:new.DS_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA',ie_log_w,ds_w,'PLS_REC_REGRA_VALOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR_REC_CALCULADO,1,4000),substr(:new.IE_VALOR_REC_CALCULADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_REC_CALCULADO',ie_log_w,ds_w,'PLS_REC_REGRA_VALOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_rec_regra_valor_atual
before update ON TASY.PLS_REC_REGRA_VALOR for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia, 'F');
end;
/


ALTER TABLE TASY.PLS_REC_REGRA_VALOR ADD (
  CONSTRAINT PLSRECVAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REC_REGRA_VALOR ADD (
  CONSTRAINT PLSRECVAL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRECVAL_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRECVAL_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REC_REGRA_VALOR TO NIVEL_1;

