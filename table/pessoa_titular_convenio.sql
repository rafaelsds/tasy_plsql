ALTER TABLE TASY.PESSOA_TITULAR_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_TITULAR_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_TITULAR_CONVENIO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  CD_PESSOA_TITULAR        VARCHAR2(10 BYTE),
  CD_CATEGORIA             VARCHAR2(10 BYTE),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  DT_VALIDADE_CARTEIRA     DATE,
  CD_USUARIO_CONVENIO      VARCHAR2(30 BYTE),
  CD_PLANO_CONVENIO        VARCHAR2(10 BYTE),
  CD_EMPRESA_REFER         NUMBER(10),
  CD_USUARIO_CONVENIO_TIT  VARCHAR2(30 BYTE),
  IE_GRAU_DEPENDENCIA      VARCHAR2(15 BYTE),
  NR_SEQ_SERV_EMP_REFER    NUMBER(10),
  NR_SEQ_PESSOA_DOC        NUMBER(10),
  NR_APOLICE               VARCHAR2(50 BYTE),
  IE_TIPO_CONVENIADO       NUMBER(3),
  CD_COMPLEMENTO           VARCHAR2(30 BYTE),
  CD_CGC_ESTAB_SAUDE       VARCHAR2(14 BYTE),
  NR_PRIORIDADE            NUMBER(4),
  EKVK_CD_PESSOA_FISICA    VARCHAR2(15 BYTE),
  EKVK_NM_PAIS             VARCHAR2(100 BYTE),
  EKVK_NR_CARTAO           VARCHAR2(30 BYTE),
  EKVK_DT_FIM              DATE,
  EKVK_DT_INICIO           DATE,
  EKVK_NR_SEQ_TIPO_DOC     NUMBER(10),
  EKVK_NR_CONV             VARCHAR2(10 BYTE),
  EKVK_SG_CONV             VARCHAR2(4 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESTICO_CATCONV_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESTICO_CONVENI_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESTICO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESTICO_EKVKTPDOC_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(EKVK_NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESTICO_EMPREFE_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_EMPRESA_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESTICO_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESTICO_EMRESER_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(NR_SEQ_SERV_EMP_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESTICO_EMRESER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESTICO_PESDOCU_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(NR_SEQ_PESSOA_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESTICO_PESFISI_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESTICO_PESFISI_FK2_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_PESSOA_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESTICO_PESJURI_FK_I ON TASY.PESSOA_TITULAR_CONVENIO
(CD_CGC_ESTAB_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESTICO_PK ON TASY.PESSOA_TITULAR_CONVENIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESTICO_UK ON TASY.PESSOA_TITULAR_CONVENIO
(CD_PESSOA_FISICA, CD_PESSOA_TITULAR, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, 
CD_USUARIO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESTICO_UK2 ON TASY.PESSOA_TITULAR_CONVENIO
(CD_PESSOA_FISICA, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, CD_USUARIO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.insurance_insert_update
before insert or update on 	pessoa_titular_convenio
	for each row
declare
medicare_count_w  varchar2(10);
pragma autonomous_transaction;

begin

	if	((:new.cd_usuario_convenio is not null
		and 	:new.cd_usuario_convenio <> :old.cd_usuario_convenio
		and 	:old.cd_usuario_convenio is not null)
		or  	(:old.cd_usuario_convenio is null
		and 	:new.cd_usuario_convenio is not null)) then

		select 	count(*)
		into 	medicare_count_w
		from  	pessoa_titular_convenio a,
			convenio b
		where 	replace(a.cd_usuario_convenio,' ','') = replace(:new.cd_usuario_convenio,' ','')
		and 	a.cd_convenio = :new.cd_convenio
		and 	a.cd_convenio = b.cd_convenio
		and 	b.ie_tipo_convenio = 12;

		if 	(medicare_count_w != 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1111951);
		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_TITULAR_CONVENIO_tp  after update ON TASY.PESSOA_TITULAR_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PLANO_CONVENIO,1,4000),substr(:new.CD_PLANO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO_CONVENIO',ie_log_w,ds_w,'PESSOA_TITULAR_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CARTEIRA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CARTEIRA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CARTEIRA',ie_log_w,ds_w,'PESSOA_TITULAR_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_USUARIO_CONVENIO,1,4000),substr(:new.CD_USUARIO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_USUARIO_CONVENIO',ie_log_w,ds_w,'PESSOA_TITULAR_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_titular_convenio_insert
before insert or update ON TASY.PESSOA_TITULAR_CONVENIO for each row
declare
ds_param_integ_hl7_w	varchar2(500);
nr_prioridade_w		pessoa_titular_convenio.nr_prioridade%type;
qt_pessoa_fisica_w      number(10);
cd_pessoa_fisica_w      pessoa_titular_convenio.cd_pessoa_fisica%type;
ds_error_w	            varchar2(254);
qt_newborn_w            number(10);

pragma autonomous_transaction;

begin

    if(nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE', 'de_AT'))then
        if (:new.nr_prioridade is null) then
            select	max(c.nr_prioridade_padrao)
            into	nr_prioridade_w
            from 	convenio c
            where	c.cd_convenio = :new.cd_convenio;

            if (nr_prioridade_w is null)then
                select  nvl(max(ptc.nr_prioridade),0)
                into   	nr_prioridade_w
                from    pessoa_titular_convenio ptc,
                    convenio  c
                where   ptc.cd_convenio = c.cd_convenio
                and     c.ie_tipo_convenio <> 1
                and     ptc.cd_pessoa_fisica = :new.cd_pessoa_fisica;

                if	(nr_prioridade_w < 9999) then
                    nr_prioridade_w := nr_prioridade_w + 1;
                end if;
            end if;

            :new.nr_prioridade := nr_prioridade_w;
        end if;

        select  count(*) qt_pessoa_fisica,
                max(cd_pessoa_fisica) cd_pessoa_fisica
        into    qt_pessoa_fisica_w,
                cd_pessoa_fisica_w
        from (  select  ptc.cd_usuario_convenio,
                        ptc.cd_pessoa_fisica
                from    pessoa_titular_convenio ptc,
                        pessoa_fisica_egk pfe
                        ,atendimento_paciente ap
                where   ptc.cd_usuario_convenio = pfe.versicherten_id
                and     ptc.cd_usuario_convenio =  :new.cd_usuario_convenio
                and pfe.ie_tipo_cartao = '1'
                and ap.cd_pessoa_fisica=ptc.cd_pessoa_fisica
                and ap.nr_seq_tipo_admissao_fat<>9
                and ap.nr_seq_classificacao<>13
                        group by    ptc.cd_usuario_convenio,
                                    ptc.cd_pessoa_fisica
                        order by ptc.cd_usuario_convenio);

        select count(*)
        into   qt_newborn_w
        from   atendimento_paciente ap
        where  ap.cd_pessoa_fisica=:new.cd_pessoa_fisica
        and    ap.nr_seq_tipo_admissao_fat = 9
        and    ap.nr_seq_classificacao = 13;

        if(qt_pessoa_fisica_w > 0 and cd_pessoa_fisica_w <> :new.cd_pessoa_fisica and qt_newborn_w = 0 )then
            ds_error_w := obter_desc_expressao(972995);
            ds_error_w := replace(ds_error_w, '#@CD_USUARIO_CONVENIO#@', :new.cd_usuario_convenio);
            ds_error_w := replace(ds_error_w, '#@CD_PESSOA_FISICA#@', cd_pessoa_fisica_w);
            wheb_mensagem_pck.exibir_mensagem_abort(ds_error_w);
        end if;

    end if;


commit;
end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_titular_convenio_update
after update ON TASY.PESSOA_TITULAR_CONVENIO for each row
declare


begin

	if ((nvl(:old.cd_convenio,0) <> nvl(:new.cd_convenio,0)) or
		(nvl(:old.cd_categoria,'X') <> nvl(:new.cd_categoria,'X')) or
		(nvl(:old.cd_plano_convenio,'X') <> nvl(:new.cd_plano_convenio,'X')) or
    (nvl(:old.ie_tipo_conveniado,0) <> nvl(:new.ie_tipo_conveniado,0)) or
		(nvl(:old.cd_pessoa_titular,0) <> nvl(:new.cd_pessoa_titular,0)) or
		(nvl(:old.ie_grau_dependencia,'X') <> nvl(:new.ie_grau_dependencia,'X')) or
		(nvl(:old.cd_usuario_convenio,'X') <> nvl(:new.cd_usuario_convenio,'X')) or

		(nvl(:old.dt_inicio_vigencia,sysdate) <> nvl(:new.dt_inicio_vigencia,sysdate)) or
		(nvl(:old.dt_fim_vigencia,sysdate) <> nvl(:new.dt_fim_vigencia,sysdate)) or
		(nvl(:old.dt_validade_carteira,sysdate) <> nvl(:new.dt_validade_carteira,sysdate)) or
		(nvl(:old.cd_usuario_convenio_tit,'X') <> nvl(:new.cd_usuario_convenio_tit,'X')) or
		(nvl(:old.cd_empresa_refer,0) <> nvl(:new.cd_empresa_refer,0))) then

		call_bifrost_content('patient.insurance.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);

	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_titular_convenio_bft
after insert or delete ON TASY.PESSOA_TITULAR_CONVENIO for each row
declare
cd_pessoa_dc_w    varchar2(10);
ds_param_integ_hl7_w    varchar2(255);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
call_bifrost_content('patient.insurance.update','person_json_pck.get_patient_message_clob('||nvl(:new.cd_pessoa_fisica,:old.cd_pessoa_fisica)||')', nvl(:new.nm_usuario,:old.nm_usuario));
end if;

end;
/


ALTER TABLE TASY.PESSOA_TITULAR_CONVENIO ADD (
  CONSTRAINT PESTICO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESTICO_UK
 UNIQUE (CD_PESSOA_FISICA, CD_PESSOA_TITULAR, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, CD_USUARIO_CONVENIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESTICO_UK2
 UNIQUE (CD_PESSOA_FISICA, CD_CONVENIO, CD_CATEGORIA, DT_INICIO_VIGENCIA, CD_USUARIO_CONVENIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_TITULAR_CONVENIO ADD (
  CONSTRAINT PESTICO_PESJURI_FK 
 FOREIGN KEY (CD_CGC_ESTAB_SAUDE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESTICO_EKVKTPDOC_FK 
 FOREIGN KEY (EKVK_NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.EKVK_TIPO_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT PESTICO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT PESTICO_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_TITULAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESTICO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PESTICO_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT PESTICO_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REFER) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT PESTICO_EMRESER_FK 
 FOREIGN KEY (NR_SEQ_SERV_EMP_REFER) 
 REFERENCES TASY.EMPRESA_REFERENCIA_SERV (NR_SEQUENCIA),
  CONSTRAINT PESTICO_PESDOCU_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_DOC) 
 REFERENCES TASY.PESSOA_DOCUMENTACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_TITULAR_CONVENIO TO NIVEL_1;

GRANT SELECT ON TASY.PESSOA_TITULAR_CONVENIO TO ROBOCMTECNOLOGIA;

