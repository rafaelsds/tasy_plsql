ALTER TABLE TASY.VALOR_FLUXO_CAIXA_AGRUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VALOR_FLUXO_CAIXA_AGRUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.VALOR_FLUXO_CAIXA_AGRUP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MACRO             VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.VLFXCXAGP_PK ON TASY.VALOR_FLUXO_CAIXA_AGRUP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.VALOR_FLUXO_CAIXA_AGRUP ADD (
  CONSTRAINT VLFXCXAGP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.VALOR_FLUXO_CAIXA_AGRUP TO NIVEL_1;

