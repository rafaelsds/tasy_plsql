ALTER TABLE TASY.QHAPDC_CHARGABLE_STATUS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QHAPDC_CHARGABLE_STATUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QHAPDC_CHARGABLE_STATUS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_CHARGEABLE_CODE         VARCHAR2(30 BYTE),
  DS_CHARGEABLE_DESCRIPTION  VARCHAR2(30 BYTE),
  DT_START_DATE              DATE,
  DT_END_DATE                DATE,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QHAPDCCS_PK ON TASY.QHAPDC_CHARGABLE_STATUS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QHAPDC_CHARGABLE_STATUS ADD (
  CONSTRAINT QHAPDCCS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.QHAPDC_CHARGABLE_STATUS TO NIVEL_1;

