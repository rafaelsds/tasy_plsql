DROP TABLE TASY.HSJ_PROC_REPASSE_EXTRA_SUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_PROC_REPASSE_EXTRA_SUS
(
  CD_PROCEDIMENTO  NUMBER(15),
  DS_PROCEDIMENTO  VARCHAR2(240 BYTE),
  CD_PREMIO        VARCHAR2(1 BYTE),
  DT_COMPETENCIA   DATE,
  VL_PREMIO        NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_PROC_REPASSE_EXTRA_SUS TO NIVEL_1;

