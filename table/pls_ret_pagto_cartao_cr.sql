ALTER TABLE TASY.PLS_RET_PAGTO_CARTAO_CR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RET_PAGTO_CARTAO_CR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RET_PAGTO_CARTAO_CR
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_PARTE_RETORNO           NUMBER(10)         NOT NULL,
  NR_SEQ_SOLIC_PAGTO_CARTAO  NUMBER(10)         NOT NULL,
  DT_PROCESSAMENTO           DATE               NOT NULL,
  IE_EVENTO                  VARCHAR2(15 BYTE),
  CD_MOEDA                   NUMBER(3),
  NR_SEQ_PAIS                NUMBER(10),
  DS_TID                     VARCHAR2(20 BYTE),
  NR_NSU                     VARCHAR2(40 BYTE)  DEFAULT null,
  CD_AUTORIZACAO             VARCHAR2(6 BYTE),
  NR_PAGAMENTO               VARCHAR2(36 BYTE),
  IE_COMERCIO_ELETRONICO     VARCHAR2(2 BYTE),
  IE_STATUS                  VARCHAR2(2 BYTE),
  CD_RETORNO                 VARCHAR2(32 BYTE)  DEFAULT null,
  DS_MENSAGEM_RETORNO        VARCHAR2(512 BYTE),
  VL_SOLICITACAO             NUMBER(15,2),
  DS_TOKEN                   VARCHAR2(65 BYTE),
  NR_SEQ_BANDEIRA            NUMBER(10)         DEFAULT null,
  DS_BANDEIRA                VARCHAR2(60 BYTE)  DEFAULT null,
  IE_TIPO_CARTAO             VARCHAR2(1 BYTE)   DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPCR_PK ON TASY.PLS_RET_PAGTO_CARTAO_CR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPCR_PLSSPCC_FK_I ON TASY.PLS_RET_PAGTO_CARTAO_CR
(NR_SEQ_SOLIC_PAGTO_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ret_pagto_cart_before
before insert or update ON TASY.PLS_RET_PAGTO_CARTAO_CR for each row
declare
nr_seq_bandeira_w   bandeira_cartao_cr.nr_sequencia%type;
begin
	if (:new.ds_bandeira is not null) then
		select 	max(nr_sequencia)
		into 	nr_seq_bandeira_w
		from 	bandeira_cartao_cr
		where 	trim(upper(ds_bandeira)) = trim(upper(:new.ds_bandeira));

		:new.nr_seq_bandeira := nr_seq_bandeira_w;
	end if;
end;
/


ALTER TABLE TASY.PLS_RET_PAGTO_CARTAO_CR ADD (
  CONSTRAINT PLSRPCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RET_PAGTO_CARTAO_CR ADD (
  CONSTRAINT PLSRPCR_PLSSPCC_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_PAGTO_CARTAO) 
 REFERENCES TASY.PLS_SOLIC_PAGTO_CARTAO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RET_PAGTO_CARTAO_CR TO NIVEL_1;

