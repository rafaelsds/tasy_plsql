DROP TABLE TASY.W_PLANEJAMENTO_OS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLANEJAMENTO_OS
(
  NM_USUARIO     VARCHAR2(15 BYTE)              NOT NULL,
  COL_1          NUMBER(10),
  COL_2          NUMBER(10),
  COL_3          NUMBER(10),
  COL_4          NUMBER(10),
  COL_5          NUMBER(10),
  COL_6          NUMBER(10),
  COL_7          NUMBER(10),
  COL_8          NUMBER(10),
  COL_9          NUMBER(10),
  COL_10         NUMBER(10),
  STATUS_COL_1   NUMBER(10)                     DEFAULT null,
  STATUS_COL_10  NUMBER(10)                     DEFAULT null,
  STATUS_COL_2   NUMBER(10)                     DEFAULT null,
  STATUS_COL_3   NUMBER(10)                     DEFAULT null,
  STATUS_COL_4   NUMBER(10)                     DEFAULT null,
  STATUS_COL_5   NUMBER(10)                     DEFAULT null,
  STATUS_COL_6   NUMBER(10)                     DEFAULT null,
  STATUS_COL_7   NUMBER(10)                     DEFAULT null,
  STATUS_COL_8   NUMBER(10)                     DEFAULT null,
  STATUS_COL_9   NUMBER(10)                     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_PLANEJAMENTO_OS TO NIVEL_1;

