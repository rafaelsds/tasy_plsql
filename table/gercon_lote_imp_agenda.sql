ALTER TABLE TASY.GERCON_LOTE_IMP_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GERCON_LOTE_IMP_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GERCON_LOTE_IMP_AGENDA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_CMCE                  VARCHAR2(12 BYTE),
  IE_TIPO_CONSULTA         VARCHAR2(1 BYTE),
  DT_AGENDA                VARCHAR2(10 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  HR_INICIAL               VARCHAR2(5 BYTE),
  HR_FINAL                 VARCHAR2(5 BYTE),
  NR_SEQ_AGENDAMENTO_PROF  VARCHAR2(10 BYTE),
  NM_PROFISSIONAL          VARCHAR2(255 BYTE),
  NR_CPF_PROFISSIONAL      VARCHAR2(11 BYTE),
  DS_SALA                  VARCHAR2(255 BYTE),
  NR_CARTAO_SUS            VARCHAR2(15 BYTE),
  NR_CPF                   VARCHAR2(11 BYTE),
  NM_PACIENTE              VARCHAR2(255 BYTE),
  DT_NASCIMENTO            VARCHAR2(10 BYTE),
  IE_SEXO                  VARCHAR2(9 BYTE),
  DS_ESTADO_CIVIL          VARCHAR2(19 BYTE),
  DS_RACA                  VARCHAR2(14 BYTE),
  NM_MAE                   VARCHAR2(255 BYTE),
  DS_NACIONALIDADE         VARCHAR2(12 BYTE),
  NR_TELEFONE              VARCHAR2(255 BYTE),
  DS_LOGRADOURO            VARCHAR2(255 BYTE),
  NR_LOGRADOURO            VARCHAR2(255 BYTE),
  DS_COMPLEMENTO           VARCHAR2(255 BYTE),
  DS_BAIRRO                VARCHAR2(255 BYTE),
  NR_ENDERECO_IBGE         VARCHAR2(6 BYTE),
  DS_MUNICIPIO             VARCHAR2(255 BYTE),
  CD_CEP                   VARCHAR2(8 BYTE),
  DS_UNIDADE_EXECUTANTE    VARCHAR2(60 BYTE),
  CD_ESPECIALIDADE         VARCHAR2(19 BYTE),
  DS_ESPECIALIDADE         VARCHAR2(255 BYTE),
  CD_CID_PRINCIPAL         VARCHAR2(255 BYTE),
  DS_CID_PRINCIPAL         VARCHAR2(255 BYTE),
  DS_SITUACAO              VARCHAR2(255 BYTE),
  NR_SEQ_AGENDAMENTO_CONS  NUMBER(10),
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GELOIMAG_AGECONS_FK_I ON TASY.GERCON_LOTE_IMP_AGENDA
(NR_SEQ_AGENDAMENTO_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GELOIMAG_GELOIM_FK_I ON TASY.GERCON_LOTE_IMP_AGENDA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GELOIMAG_PESFISI_FK_I ON TASY.GERCON_LOTE_IMP_AGENDA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GELOIMAG_PK ON TASY.GERCON_LOTE_IMP_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.gercon_lote_imp_agenda_bfr_ins
before insert ON TASY.GERCON_LOTE_IMP_AGENDA for each row
declare
	cd_pessoa_fisica_w gercon_lote_imp_agenda.cd_pessoa_fisica%type;
begin
	:new.nm_usuario := wheb_usuario_pck.get_nm_usuario;
	:new.nm_usuario_nrec := wheb_usuario_pck.get_nm_usuario;

	select	GERCON_LOTE_IMP_AGENDA_SEQ.nextval
	into	:new.nr_sequencia
	from	dual;

	:new.dt_atualizacao := sysdate;
	:new.dt_atualizacao_nrec := sysdate;

	select	nvl(max(cd_pessoa_fisica),'0')
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	nr_cpf = nvl(:new.nr_cpf,'0')
	and		nr_cpf <> '0';


	if	(cd_pessoa_fisica_w = '0') then
		begin

			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;

			insert into pessoa_fisica(	CD_PESSOA_FISICA,
										NR_CARTAO_NAC_SUS,
										NM_PESSOA_FISICA,
										NR_CPF,
										DT_NASCIMENTO,
										IE_SEXO,
										IE_ESTADO_CIVIL,
										NR_SEQ_COR_PELE,
										CD_NACIONALIDADE,
										IE_TIPO_PESSOA,
										DT_ATUALIZACAO,
										DT_ATUALIZACAO_NREC,
										NM_USUARIO
										)
							values	(	cd_pessoa_fisica_w,
										:new.nr_cartao_sus,
										:new.nm_paciente,
										:new.nr_cpf,
										to_date(:new.dt_nascimento),
										substr(nvl(upper(:new.ie_sexo),'I'),1,1),
										obter_conversao_externa_int(null,'PESSOA_FISICA','DS_ESTADO_CIVIL',NVL(:new.DS_ESTADO_CIVIL,''),'GERCON'),
										obter_conversao_externa_int(null,'PESSOA_FISICA','DS_RACA',NVL(:new.DS_RACA,''),'GERCON'),
										obter_conversao_externa_int(null,'PESSOA_FISICA','DS_NACIONALIDADE',NVL(:new.DS_NACIONALIDADE, ''),'GERCON'),
										2,
										SYSDATE,
										SYSDATE,
										wheb_usuario_pck.get_nm_usuario
										);

			insert into compl_pessoa_fisica	(	CD_PESSOA_FISICA,
												DS_ENDERECO,
												NR_ENDERECO,
												DS_COMPLEMENTO,
												DS_BAIRRO,
												CD_MUNICIPIO_IBGE,
												DS_MUNICIPIO,
												CD_CEP,
												NM_CONTATO,
												NR_SEQUENCIA,
												IE_TIPO_COMPLEMENTO,
												NM_USUARIO,
												DT_ATUALIZACAO,
												DT_ATUALIZACAO_NREC
												)
									values	(	cd_pessoa_fisica_w,
												:new.ds_logradouro,
												:new.nr_logradouro,
												:new.ds_complemento,
												:new.ds_bairro,
												:new.nr_endereco_ibge,
												:new.ds_municipio,
												:new.cd_cep,
												:new.nm_paciente,
												1,
												1,
												wheb_usuario_pck.get_nm_usuario,
												SYSDATE,
												SYSDATE
												);

		end;
	end if;
	:new.cd_pessoa_fisica := cd_pessoa_fisica_w;

	select	nvl(max(cd_pessoa_fisica),'')
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	nr_cpf = nvl(:new.nr_cpf_profissional,0)
	and		nr_cpf <> '0';

	:new.cd_profissional := cd_pessoa_fisica_w;

end;
/


ALTER TABLE TASY.GERCON_LOTE_IMP_AGENDA ADD (
  CONSTRAINT GELOIMAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GERCON_LOTE_IMP_AGENDA ADD (
  CONSTRAINT GELOIMAG_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDAMENTO_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT GELOIMAG_GELOIM_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.GERCON_LOTE_IMP (NR_SEQUENCIA),
  CONSTRAINT GELOIMAG_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.GERCON_LOTE_IMP_AGENDA TO NIVEL_1;

