ALTER TABLE TASY.PLS_PARAM_ESTIPULANTE_TWS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAM_ESTIPULANTE_TWS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAM_ESTIPULANTE_TWS
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_VALIDAR_REPASSE_CONTRATO    VARCHAR2(1 BYTE),
  IE_DATA_REPASSE                VARCHAR2(2 BYTE),
  IE_LIBERAR_INCLUSAO            VARCHAR2(2 BYTE),
  IE_CARREGAR_DADOS_CPF          VARCHAR2(1 BYTE),
  IE_DIA_ALT_PROD                VARCHAR2(3 BYTE),
  DS_DIRETORIO_ANEXO_ADESAO      VARCHAR2(255 BYTE),
  IE_APRESENTAR_DT_LIB_REAJUSTE  VARCHAR2(1 BYTE),
  IE_LIBERAR_RESCISAO            VARCHAR2(1 BYTE),
  DS_DIRETORIO_ANEXO_RESSCISAO   VARCHAR2(255 BYTE),
  IE_EXIGE_APROV_CARTEIRA        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPETWS_ESTABEL_FK_I ON TASY.PLS_PARAM_ESTIPULANTE_TWS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPETWS_PK ON TASY.PLS_PARAM_ESTIPULANTE_TWS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PARAM_ESTIPULANTE_TWS ADD (
  CONSTRAINT PLSPETWS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_PARAM_ESTIPULANTE_TWS ADD (
  CONSTRAINT PLSPETWS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_PARAM_ESTIPULANTE_TWS TO NIVEL_1;

