ALTER TABLE TASY.CA_OSMOSE_MANUTENCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CA_OSMOSE_MANUTENCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CA_OSMOSE_MANUTENCAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_REGISTRO                 DATE              NOT NULL,
  NR_SEQ_MOTIVO_MANUTENCAO    NUMBER(10),
  QT_PRESSAO_ENTR_PRE_TRAT    NUMBER(3),
  QT_PRESSAO_SAIDA_PRE_TRAT   NUMBER(3),
  QT_PRESSAO_ENTR_MEMBRANA    NUMBER(3),
  QT_PRESSAO_SAIDA_MEMBRANA   NUMBER(3),
  QT_FLUXO_PRODUCAO           NUMBER(3),
  QT_FLUXO_REJEITO            NUMBER(3),
  QT_FLUXO_RECIRCULACAO       NUMBER(3),
  QT_PERC_REJEICAO            NUMBER(3),
  QT_TOTAL_SODIO_DISSOL       NUMBER(3),
  NR_SEQ_OSMOSE               NUMBER(10),
  QT_COND_ENTR_PRE_TRAT       NUMBER(3),
  QT_COND_SAIDA_PRE_TRAT      NUMBER(3),
  QT_FERRO_ENTR_PRE_TRAT      NUMBER(3),
  QT_FERRO_SAIDA_PRE_TRAT     NUMBER(3),
  QT_DUREZA_ANTES_ABRANDADOR  NUMBER(3),
  QT_DUREZA_APOS_ABRANDADOR   NUMBER(3),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  IE_NIVEL_SAL                VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO          NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAOSMAN_CAOSMOS_FK_I ON TASY.CA_OSMOSE_MANUTENCAO
(NR_SEQ_OSMOSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAOSMAN_CAOSMOS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CAOSMAN_ESTABEL_FK_I ON TASY.CA_OSMOSE_MANUTENCAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAOSMAN_HDMOTDAPA_FK_I ON TASY.CA_OSMOSE_MANUTENCAO
(NR_SEQ_MOTIVO_MANUTENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAOSMAN_HDMOTDAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CAOSMAN_PESFISI_FK_I ON TASY.CA_OSMOSE_MANUTENCAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAOSMAN_PK ON TASY.CA_OSMOSE_MANUTENCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAOSMAN_PK
  MONITORING USAGE;


ALTER TABLE TASY.CA_OSMOSE_MANUTENCAO ADD (
  CONSTRAINT CAOSMAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CA_OSMOSE_MANUTENCAO ADD (
  CONSTRAINT CAOSMAN_CAOSMOS_FK 
 FOREIGN KEY (NR_SEQ_OSMOSE) 
 REFERENCES TASY.CA_OSMOSE (NR_SEQUENCIA),
  CONSTRAINT CAOSMAN_HDMOTDAPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_MANUTENCAO) 
 REFERENCES TASY.HD_MOTIVO_OSM_MANUTENCAO (NR_SEQUENCIA),
  CONSTRAINT CAOSMAN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CAOSMAN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CA_OSMOSE_MANUTENCAO TO NIVEL_1;

