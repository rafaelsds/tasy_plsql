ALTER TABLE TASY.TISS_REGRA_EXEC_HONOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_EXEC_HONOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_EXEC_HONOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5),
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  IE_REGRA                VARCHAR2(3 BYTE)      NOT NULL,
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  CD_MEDICO_EXECUTOR      VARCHAR2(10 BYTE),
  IE_EXEC                 VARCHAR2(1 BYTE),
  IE_FATUR_MEDICO         VARCHAR2(2 BYTE),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  IE_GRAU_PARTIC_TISS     VARCHAR2(2 BYTE),
  CD_INTERNO              VARCHAR2(60 BYTE),
  DS_PRESTADOR_TISS       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSREH_CONVENI_FK_I ON TASY.TISS_REGRA_EXEC_HONOR
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREH_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREH_ESTABEL_FK_I ON TASY.TISS_REGRA_EXEC_HONOR
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREH_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSREH_PESFISI_FK_I ON TASY.TISS_REGRA_EXEC_HONOR
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREH_PESJURI_FK_I ON TASY.TISS_REGRA_EXEC_HONOR
(CD_CGC_PRESTADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREH_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSREH_PK ON TASY.TISS_REGRA_EXEC_HONOR
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSREH_SETATEN_FK_I ON TASY.TISS_REGRA_EXEC_HONOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSREH_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_EXEC_HONOR_tp  after update ON TASY.TISS_REGRA_EXEC_HONOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_INTERNO,1,500);gravar_log_alteracao(substr(:old.CD_INTERNO,1,4000),substr(:new.CD_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNO',ie_log_w,ds_w,'TISS_REGRA_EXEC_HONOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_EXEC_HONOR ADD (
  CONSTRAINT TISSREH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_EXEC_HONOR ADD (
  CONSTRAINT TISSREH_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSREH_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSREH_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSREH_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TISSREH_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.TISS_REGRA_EXEC_HONOR TO NIVEL_1;

