ALTER TABLE TASY.ATEND_ANAL_BIOQ_PORT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_ANAL_BIOQ_PORT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_ANAL_BIOQ_PORT
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ANALISE                 DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CIRURGIA                NUMBER(10),
  QT_PH                      NUMBER(15,4),
  QT_PO2                     NUMBER(15,4),
  QT_PCO2                    NUMBER(15,4),
  QT_HCO3                    NUMBER(15,4),
  QT_BE                      NUMBER(15,4),
  QT_SATO2                   NUMBER(15,4),
  QT_HT                      NUMBER(15,4),
  QT_HB                      NUMBER(15,4),
  QT_NA                      NUMBER(15,4),
  QT_K                       NUMBER(15,4),
  QT_CA                      NUMBER(15,4),
  QT_GLICOSE                 NUMBER(15,4),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  QT_TCA                     NUMBER(15),
  QT_HEPARINA                NUMBER(15),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_ORIGEM_SANGUE           VARCHAR2(15 BYTE),
  QT_LACTATO                 NUMBER(15,2),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  QT_FIO2                    NUMBER(15,4),
  IE_LOCAL                   VARCHAR2(15 BYTE),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  QT_CLORO                   NUMBER(3),
  QT_CO2                     NUMBER(3,1),
  QT_MG                      NUMBER(2,1),
  QT_TEMP                    NUMBER(4,1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEANBP_ATCONSPEPA_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANBP_ATEPACI_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANBP_CIRURGI_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANBP_I1 ON TASY.ATEND_ANAL_BIOQ_PORT
(DT_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANBP_I1
  MONITORING USAGE;


CREATE INDEX TASY.ATEANBP_I2 ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_ATENDIMENTO, DT_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANBP_PEPOCIR_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANBP_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEANBP_PERFIL_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANBP_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEANBP_PESFISI_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEANBP_PK ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANBP_TASASDI_FK_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANBP_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEANBP_TASASDI_FK2_I ON TASY.ATEND_ANAL_BIOQ_PORT
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANBP_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atend_anal_bioq_port_after
after insert or update ON TASY.ATEND_ANAL_BIOQ_PORT for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_sinal_vital_w	varchar2(10);
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_lib_sinal_vital)
into	ie_lib_sinal_vital_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;


if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'ABP';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XABP';
	end if;

	begin
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
	end if;
	exception
		when others then
		null;
	end;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ATEND_ANAL_BIOQ_PORT ADD (
  CONSTRAINT ATEANBP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_ANAL_BIOQ_PORT ADD (
  CONSTRAINT ATEANBP_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEANBP_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEANBP_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEANBP_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEANBP_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEANBP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEANBP_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ATEANBP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ATEND_ANAL_BIOQ_PORT TO NIVEL_1;

