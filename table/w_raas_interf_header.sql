ALTER TABLE TASY.W_RAAS_INTERF_HEADER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_RAAS_INTERF_HEADER CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RAAS_INTERF_HEADER
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_ATEND       NUMBER(10),
  DS_INDICADOR_CABECALHO  VARCHAR2(5 BYTE),
  DT_COMPETENCIA          DATE,
  QT_FOLHAS_ARQUIVO       NUMBER(6),
  NR_CONTROLE             NUMBER(4),
  NM_ORGAO_RESPONSAVEL    VARCHAR2(30 BYTE),
  DS_SIGLA_ORG_RESP       VARCHAR2(6 BYTE),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  NM_ORGAO_DESTINO        VARCHAR2(40 BYTE),
  IE_INDICADOR_DESTINO    VARCHAR2(15 BYTE),
  DT_GERACAO              DATE,
  DS_VERSAO_RAAS          VARCHAR2(15 BYTE),
  DS_VERSAO_BDSIA         VARCHAR2(7 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WRAASIH_PK ON TASY.W_RAAS_INTERF_HEADER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WRAASIH_RALOTAT_FK_I ON TASY.W_RAAS_INTERF_HEADER
(NR_SEQ_LOTE_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_RAAS_INTERF_HEADER ADD (
  CONSTRAINT WRAASIH_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.W_RAAS_INTERF_HEADER ADD (
  CONSTRAINT WRAASIH_RALOTAT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ATEND) 
 REFERENCES TASY.RAAS_LOTE_ATENDIMENTOS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_RAAS_INTERF_HEADER TO NIVEL_1;

