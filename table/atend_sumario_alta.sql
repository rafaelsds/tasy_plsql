ALTER TABLE TASY.ATEND_SUMARIO_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_SUMARIO_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_SUMARIO_ALTA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ALTA                    DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_MOTIVO_ALTA             NUMBER(3),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_TIPO_SUMARIO            VARCHAR2(1 BYTE)   NOT NULL,
  CD_MEDICO_RESP             VARCHAR2(10 BYTE)  NOT NULL,
  CD_SETOR_DESTINO           NUMBER(5),
  IE_NECROPSIA               VARCHAR2(1 BYTE),
  CD_MEDICO_RESP_ALTA        VARCHAR2(10 BYTE)  NOT NULL,
  DS_CAUSA_MORTE             VARCHAR2(255 BYTE),
  CD_SETOR_ATUAL             NUMBER(5),
  CD_CGC                     VARCHAR2(14 BYTE),
  QT_PERIODO                 NUMBER(10),
  IE_PERIODO                 VARCHAR2(15 BYTE),
  NR_SEQ_DESTINO             NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_ACAO                    VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE           NUMBER(5),
  DT_ENTRADA                 DATE,
  DS_MOTIVO_ENTRADA          VARCHAR2(255 BYTE),
  DT_APPROVAL                DATE,
  NM_APRROVAL_USER           VARCHAR2(10 BYTE),
  DT_DISAPPROVAL             DATE,
  NM_DISAPPROVAL_USER        VARCHAR2(10 BYTE),
  DS_DISAPPROVAL_JUSTIF      VARCHAR2(255 BYTE),
  IE_CORONER_CASE            VARCHAR2(1 BYTE),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  CD_ESPECIALIDADE_MED       NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATESUAL_ATEPACI_FK_I ON TASY.ATEND_SUMARIO_ALTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_DESSUAL_FK_I ON TASY.ATEND_SUMARIO_ALTA
(NR_SEQ_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_ESPMEDI_FK_I ON TASY.ATEND_SUMARIO_ALTA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_MEDICO_FK_I ON TASY.ATEND_SUMARIO_ALTA
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_MEDICO_FK2_I ON TASY.ATEND_SUMARIO_ALTA
(CD_MEDICO_RESP_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_MOTALTA_FK_I ON TASY.ATEND_SUMARIO_ALTA
(CD_MOTIVO_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESUAL_MOTALTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESUAL_NAISINS_FK_I ON TASY.ATEND_SUMARIO_ALTA
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_PESJURI_FK_I ON TASY.ATEND_SUMARIO_ALTA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATESUAL_PK ON TASY.ATEND_SUMARIO_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESUAL_PK
  MONITORING USAGE;


CREATE INDEX TASY.ATESUAL_SETATEN_FK_I ON TASY.ATEND_SUMARIO_ALTA
(CD_SETOR_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESUAL_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESUAL_SETATEN_FK2_I ON TASY.ATEND_SUMARIO_ALTA
(CD_SETOR_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESUAL_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESUAL_TASASDI_FK_I ON TASY.ATEND_SUMARIO_ALTA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESUAL_TASASDI_FK2_I ON TASY.ATEND_SUMARIO_ALTA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_sumario_alta_pend_atual
after insert or update or delete ON TASY.ATEND_SUMARIO_ALTA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_sumario_alta_w	varchar2(10) := 'S';
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin

	if	(inserting) or
		(updating) then

		if	(nvl(ie_lib_sumario_alta_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'SUM';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XSUM';
			end if;


			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
			end if;


		end if;
	elsif	(deleting) then

		delete	from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'SUM'
		and	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L') = 'L';

		commit;
	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.atend_sumario_alta_delete
before delete ON TASY.ATEND_SUMARIO_ALTA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'),
									 obter_desc_expressao(487401),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 wheb_usuario_pck.get_nm_usuario);


end;
/


CREATE OR REPLACE TRIGGER TASY.atend_sumario_alta_afupdate
  AFTER UPDATE ON TASY.ATEND_SUMARIO_ALTA   FOR EACH ROW
DECLARE
    CURSOR c01 IS
      SELECT nr_sequencia
      FROM   wl_worklist
      WHERE  nr_seq_disc_summary = :new.nr_sequencia
             AND dt_final_real IS NULL;
    c01_w c01%ROWTYPE;
BEGIN
    IF ( :new.ie_situacao = 'I' )
       AND ( :old.ie_situacao = 'A' ) THEN
      OPEN c01;

      LOOP
          FETCH c01 INTO c01_w;

          exit WHEN c01%NOTFOUND;

          BEGIN
              UPDATE wl_worklist
              SET    dt_final_real = SYSDATE,
                     nm_usuario = :new.nm_usuario,
                     dt_atualizacao = SYSDATE
              WHERE  nr_sequencia = c01_w.nr_sequencia
                     AND dt_final_real IS NULL;

              INSERT INTO wl_worklist_historico
                          (nr_sequencia,
                           nr_seq_worklist,
                           ds_motivo,
                           nm_usuario,
                           dt_atualizacao)
              VALUES     ( wl_worklist_historico_seq.NEXTVAL,
                          c01_w.nr_sequencia,
                          Obter_desc_expressao(965060),
                          :new.nm_usuario,
                          SYSDATE);
          END;
      END LOOP;

      CLOSE c01;
    END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_SUMARIO_ALTA_ATUAL
before insert or update ON TASY.ATEND_SUMARIO_ALTA for each row
declare

nr_seq_mhr_doc_w			mhr_doc_meta.nr_sequencia%TYPE;
patient_code_w			mhr_doc_meta.cd_pessoa_fisica%TYPE;



begin
if	(nvl(:old.DT_ALTA,sysdate+10) <> :new.DT_ALTA) and
	(:new.DT_ALTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ALTA, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_ATEND_SUMARIO_ALTA
before insert or update ON TASY.ATEND_SUMARIO_ALTA for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_SUMARIO_SBIS_IN
before insert or update ON TASY.ATEND_SUMARIO_ALTA for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);

begin
IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 :new.DT_ALTA,
										 obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
										 obter_pessoa_fisica_usuario(:new.nm_usuario,'C'),
										 obter_desc_expressao(487401),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
else
	ie_inativacao_w := 'N';

	if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
		ie_inativacao_w := 'S';
	end if;

	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 obter_pessoa_fisica_usuario(:new.nm_usuario,'C'),
										 obter_desc_expressao(487401),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
end if;
end;
/


ALTER TABLE TASY.ATEND_SUMARIO_ALTA ADD (
  CONSTRAINT ATESUAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_SUMARIO_ALTA ADD (
  CONSTRAINT ATESUAL_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT ATESUAL_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT ATESUAL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATESUAL_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT ATESUAL_MEDICO_FK2 
 FOREIGN KEY (CD_MEDICO_RESP_ALTA) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT ATESUAL_MOTALTA_FK 
 FOREIGN KEY (CD_MOTIVO_ALTA) 
 REFERENCES TASY.MOTIVO_ALTA (CD_MOTIVO_ALTA),
  CONSTRAINT ATESUAL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_DESTINO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATESUAL_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATUAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATESUAL_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ATESUAL_DESSUAL_FK 
 FOREIGN KEY (NR_SEQ_DESTINO) 
 REFERENCES TASY.DESTINO_SUMARIO_ALTA (NR_SEQUENCIA),
  CONSTRAINT ATESUAL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATESUAL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_SUMARIO_ALTA TO NIVEL_1;

