ALTER TABLE TASY.OBJETO_SISTEMA_DESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OBJETO_SISTEMA_DESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.OBJETO_SISTEMA_DESCR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_OBJETO            VARCHAR2(50 BYTE)        NOT NULL,
  IE_BANCO             VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_DESCRICAO         VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OBJSIDE_OBJSIST_FK_I ON TASY.OBJETO_SISTEMA_DESCR
(NM_OBJETO, IE_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OBJSIDE_PK ON TASY.OBJETO_SISTEMA_DESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OBJSIDE_PK
  MONITORING USAGE;


ALTER TABLE TASY.OBJETO_SISTEMA_DESCR ADD (
  CONSTRAINT OBJSIDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OBJETO_SISTEMA_DESCR ADD (
  CONSTRAINT OBJSIDE_OBJSIST_FK 
 FOREIGN KEY (NM_OBJETO, IE_BANCO) 
 REFERENCES TASY.OBJETO_SISTEMA (NM_OBJETO,IE_BANCO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OBJETO_SISTEMA_DESCR TO NIVEL_1;

