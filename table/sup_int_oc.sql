ALTER TABLE TASY.SUP_INT_OC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_OC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_OC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_FORMA_INTEGRACAO     VARCHAR2(15 BYTE)     NOT NULL,
  DT_LIBERACAO            DATE,
  DT_LEITURA              DATE,
  DT_CONFIRMA_INTEGRACAO  DATE,
  NR_ORDEM_COMPRA         NUMBER(10),
  NR_DOCUMENTO_EXTERNO    NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  CD_CGC_FORNECEDOR       VARCHAR2(14 BYTE),
  CD_CONDICAO_PAGAMENTO   NUMBER(10)            NOT NULL,
  CD_COMPRADOR            VARCHAR2(10 BYTE)     NOT NULL,
  DT_ORDEM_COMPRA         DATE                  NOT NULL,
  CD_MOEDA                NUMBER(5)             NOT NULL,
  CD_PESSOA_SOLICITANTE   VARCHAR2(10 BYTE)     NOT NULL,
  CD_CGC_TRANSPORTADOR    VARCHAR2(14 BYTE),
  IE_FRETE                VARCHAR2(1 BYTE)      NOT NULL,
  VL_FRETE                NUMBER(15,2),
  PR_DESCONTO             NUMBER(13,4),
  PR_DESC_PGTO_ANTEC      NUMBER(13,4),
  PR_JUROS_NEGOCIADO      NUMBER(13,4),
  DS_PESSOA_CONTATO       VARCHAR2(255 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  CD_LOCAL_ENTREGA        NUMBER(4),
  DT_ENTREGA              DATE                  NOT NULL,
  IE_AVISO_CHEGADA        VARCHAR2(1 BYTE)      NOT NULL,
  VL_DESPESA_ACESSORIA    NUMBER(13,2),
  NR_SEQ_SUBGRUPO_COMPRA  NUMBER(10),
  PR_DESC_FINANCEIRO      NUMBER(13,4),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  IE_URGENTE              VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_FORMA_PAGTO      NUMBER(10),
  VL_DESCONTO             NUMBER(13,2),
  CD_CENTRO_CUSTO         NUMBER(8),
  NR_SEQ_MOTIVO_CANCEL    NUMBER(10),
  NR_ORDEM_CONTROLE       NUMBER(10),
  NR_ANO_CONTROLE         NUMBER(4),
  NR_SERIE_CONTROLE       NUMBER(5),
  NM_COMPRADOR            VARCHAR2(255 BYTE),
  CD_FUNC_COMPRADOR       VARCHAR2(15 BYTE),
  IE_TIPO_OC              VARCHAR2(15 BYTE),
  IE_SITUACAO_OC          VARCHAR2(15 BYTE),
  NR_ORDEM_EXISTENTE      NUMBER(10),
  NR_SERIE_OC             VARCHAR2(15 BYTE),
  NR_ANO_ORDEM            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINOCO_PK ON TASY.SUP_INT_OC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINOCO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_oc_atual
before update ON TASY.SUP_INT_OC for each row
declare
pragma autonomous_transaction;

qt_existe_erros_w			number(10);
nr_ordem_compra_w		number(10);
qt_existe_w			number(10);
nr_sequencia_w			number(10);
qt_existe_oc_w			number(10);
nr_ordem_existente_w		number(10);
nr_ano_ordem_w			number(4);
nr_serie_oc_w			number(2);

begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_evento = 'OC'
and	ie_forma = 'R'
and	ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(:new.ie_forma_integracao = 'R') then
	begin

	nr_ordem_compra_w	:= 0;
	:new.dt_leitura		:= sysdate;

	if	(nvl(:new.nr_ordem_existente,0) > 0) and
		(nvl(:new.nr_ano_ordem,0) > 0) and
		(:new.nr_serie_oc is not null) then

		begin
		select	count(*)
		into	qt_existe_oc_w
		from	ordem_compra
		where	nr_documento_externo = :new.nr_ordem_existente || :new.nr_serie_oc || :new.nr_ano_ordem;
		exception when others then
			qt_existe_oc_w	:= 0;
		end;

	else
		select	to_number(substr(to_char(:new.nr_documento_externo),1,length(:new.nr_documento_externo)-5)),
			to_number(substr(to_char(:new.nr_documento_externo),length(:new.nr_documento_externo)-4,1)),
			to_number(substr(to_char(:new.nr_documento_externo),length(:new.nr_documento_externo)-3,4))
		into	nr_ordem_existente_w,
			nr_serie_oc_w,
			nr_ano_ordem_w
		from	dual;

		begin
		select	count(*)
		into	qt_existe_oc_w
		from	ordem_compra
		where	to_number(substr(to_char(nr_documento_externo),1,length(to_char(nr_documento_externo))-5)) = nr_ordem_existente_w
		and  	to_number(substr(to_char(nr_documento_externo),length(to_char(nr_documento_externo))-4,1)) = nr_serie_oc_w
		and	to_number(substr(to_char(nr_documento_externo),length(nr_documento_externo)-3,4)) = nr_ano_ordem_w;
		exception when others then
			qt_existe_oc_w	:= 0;
		end;
	end if;

	if	(qt_existe_oc_w = 0) then
		begin

		consiste_sup_int_oc(
			:new.nr_sequencia,
			:new.cd_estabelecimento,
			:new.cd_cgc_fornecedor,
			:new.cd_pessoa_fisica,
			:new.cd_condicao_pagamento,
			:new.cd_comprador,
			:new.cd_moeda,
			:new.cd_pessoa_solicitante,
			:new.cd_cgc_transportador,
			:new.ie_frete,
			:new.cd_local_entrega,
			:new.nr_seq_forma_pagto,
			:new.cd_centro_custo,
			:new.dt_entrega);

		select	count(*)
		into	qt_existe_erros_w
		from	sup_int_oc_consist
		where	nr_sequencia = :new.nr_sequencia;

		if	(qt_existe_erros_w = 0) then

			gerar_ordem_compra_integracao(
				:new.nr_sequencia,
				WHEB_MENSAGEM_PCK.get_texto(799532),
				:new.cd_estabelecimento,
				:new.cd_cgc_fornecedor,
				:new.cd_condicao_pagamento,
				:new.cd_comprador,
				:new.cd_moeda,
				:new.cd_pessoa_solicitante,
				:new.cd_cgc_transportador,
				:new.ie_frete,
				:new.vl_frete,
				:new.pr_desconto,
				:new.pr_desc_pgto_antec,
				:new.pr_juros_negociado,
				:new.ds_pessoa_contato,
				:new.ds_observacao,
				:new.cd_local_entrega,
				:new.dt_entrega,
				:new.ie_aviso_chegada,
				:new.vl_despesa_acessoria,
				:new.nr_seq_subgrupo_compra,
				:new.pr_desc_financeiro,
				:new.cd_pessoa_fisica,
				:new.ie_urgente,
				:new.nr_seq_forma_pagto,
				:new.nr_documento_externo,
				:new.vl_desconto,
				:new.cd_centro_custo,
				nr_ordem_compra_w);

		/* Era aqui antes */

		end if;

		end;
	else
		begin

		atualiza_ordem_compra_integ(
			:new.nr_sequencia,
			WHEB_MENSAGEM_PCK.get_texto(799532),
			:new.cd_estabelecimento,
			:new.cd_cgc_fornecedor,
			:new.cd_condicao_pagamento,
			:new.cd_comprador,
			:new.cd_moeda,
			:new.cd_pessoa_solicitante,
			:new.cd_cgc_transportador,
			:new.ie_frete,
			:new.vl_frete,
			:new.pr_desconto,
			:new.pr_desc_pgto_antec,
			:new.pr_juros_negociado,
			:new.ds_pessoa_contato,
			:new.ds_observacao,
			:new.cd_local_entrega,
			:new.dt_entrega,
			:new.ie_aviso_chegada,
			:new.vl_despesa_acessoria,
			:new.nr_seq_subgrupo_compra,
			:new.pr_desc_financeiro,
			:new.cd_pessoa_fisica,
			:new.ie_urgente,
			:new.nr_seq_forma_pagto,
			:new.nr_documento_externo,
			:new.vl_desconto,
			:new.cd_centro_custo,
			:new.nr_seq_motivo_cancel,
			nr_ordem_compra_w);

		end;
	end if;

	if	(nr_ordem_compra_w <> 0) then
		begin
		:new.dt_confirma_integracao 		:= sysdate;
		:new.nr_ordem_compra			:= nr_ordem_compra_w;

		select	count(*)
		into	qt_existe_w
		from	sup_parametro_integracao a,
			sup_int_regra_oc b
		where	a.nr_sequencia = b.nr_seq_integracao
		and	a.cd_estabelecimento = :new.cd_estabelecimento
		and	a.ie_evento = 'OC'
		and	a.ie_forma = 'E'
		and	a.ie_situacao = 'A'
		and	b.ie_situacao = 'A'
		and	nvl(b.cd_local_entrega,:new.cd_local_entrega) = :new.cd_local_entrega
		and	ie_lib_aprov = 'R';

		if	(qt_existe_w > 0) then
			begin

			select	count(*)
			into	qt_existe_w
			from	sup_int_oc
			where	nr_ordem_compra = nr_ordem_compra_w
			and	ie_forma_integracao = 'E';

			if	(qt_existe_w = 0) then
				begin

				select	SUP_INT_OC_seq.nextval
				into	nr_sequencia_w
				from	dual;

				insert into sup_int_oc(
					nr_sequencia,
					ie_forma_integracao,
					nr_ordem_compra,
					cd_estabelecimento,
					cd_cgc_fornecedor,
					cd_condicao_pagamento,
					cd_comprador,
					dt_ordem_compra,
					cd_moeda,
					cd_pessoa_solicitante,
					cd_cgc_transportador,
					ie_frete,
					vl_frete,
					pr_desconto,
					pr_desc_pgto_antec,
					pr_juros_negociado,
					ds_pessoa_contato,
					ds_observacao,
					cd_local_entrega,
					dt_entrega,
					ie_aviso_chegada,
					vl_despesa_acessoria,
					nr_seq_subgrupo_compra,
					pr_desc_financeiro,
					cd_pessoa_fisica,
					ie_urgente,
					nr_seq_forma_pagto,
					nr_documento_externo,
					vl_desconto,
					cd_centro_custo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_liberacao) values (
						nr_sequencia_w,
						'E',
						nr_ordem_compra_w,
						:new.cd_estabelecimento,
						:new.cd_cgc_fornecedor,
						:new.cd_condicao_pagamento,
						:new.cd_comprador,
						:new.dt_ordem_compra,
						:new.cd_moeda,
						:new.cd_pessoa_solicitante,
						:new.cd_cgc_transportador,
						:new.ie_frete,
						:new.vl_frete,
						:new.pr_desconto,
						:new.pr_desc_pgto_antec,
						:new.pr_juros_negociado,
						:new.ds_pessoa_contato,
						:new.ds_observacao,
						:new.cd_local_entrega,
						:new.dt_entrega,
						:new.ie_aviso_chegada,
						:new.vl_despesa_acessoria,
						:new.nr_seq_subgrupo_compra,
						:new.pr_desc_financeiro,
						:new.cd_pessoa_fisica,
						:new.ie_urgente,
						:new.nr_seq_forma_pagto,
						:new.nr_documento_externo,
						:new.vl_desconto,
						:new.cd_centro_custo,
						sysdate,
						'INTEGRACAO', --Nome de usuario necessário para integração Tasy - Protheus
						sysdate,
						'INTEGRACAO', --Nome de usuario necessário para integração Tasy - Protheus
						sysdate);

				end;
			else
				begin

				select	max(nr_sequencia)
				into	nr_sequencia_w
				from	sup_int_oc
				where	nr_ordem_compra = nr_ordem_compra_w;

				end;
			end if;

			envia_sup_int_oc_item(nr_ordem_compra_w,
					nr_sequencia_w,
					:new.cd_estabelecimento,
					'INTEGRACAO'); --Nome de usuario necessário para integração Tasy - Protheus

			end;
		else
			begin

			if	(:new.ie_forma_integracao = 'R') then
				begin
				envia_sup_int_oc_item(nr_ordem_compra_w,
					:new.nr_sequencia,
					:new.cd_estabelecimento,
					'INTEGRACAO'); --Nome de usuario necessário para integração Tasy - Protheus
				end;
			end if;

			end;
		end if;

		end;
	end if;

	end;
end if;

commit;

end;
/


ALTER TABLE TASY.SUP_INT_OC ADD (
  CONSTRAINT SUINOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_OC TO NIVEL_1;

