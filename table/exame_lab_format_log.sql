ALTER TABLE TASY.EXAME_LAB_FORMAT_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LAB_FORMAT_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LAB_FORMAT_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_PADRAO            VARCHAR2(1 BYTE),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_MAPA_LAUDO        VARCHAR2(1 BYTE),
  DS_REGRA             VARCHAR2(255 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  DS_FORMATO           CLOB,
  NR_SEQ_EXAME         NUMBER(10),
  NR_SEQ_FORMATO       NUMBER(10),
  NR_SEQ_METODO        NUMBER(10),
  NR_SEQ_SUPERIOR      NUMBER(10),
  NR_SEQ_MATERIAL      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_FORMATO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.EXLAFOL_PK ON TASY.EXAME_LAB_FORMAT_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EXAME_LAB_FORMAT_LOG ADD (
  CONSTRAINT EXLAFOL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.EXAME_LAB_FORMAT_LOG TO NIVEL_1;

