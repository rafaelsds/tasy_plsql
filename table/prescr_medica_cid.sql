ALTER TABLE TASY.PRESCR_MEDICA_CID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_MEDICA_CID CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_MEDICA_CID
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PRESCRICAO        NUMBER(15)               NOT NULL,
  CD_DOENCA_CID        VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_DIETA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREMEDC_CIDDOEN_FK_I ON TASY.PRESCR_MEDICA_CID
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEDC_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREMEDC_PK ON TASY.PRESCR_MEDICA_CID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEDC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PREMEDC_PRESMED_FK_I ON TASY.PRESCR_MEDICA_CID
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEDC_PRESMED_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRESCR_MEDICA_CID ADD (
  CONSTRAINT PREMEDC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_MEDICA_CID ADD (
  CONSTRAINT PREMEDC_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PREMEDC_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_MEDICA_CID TO NIVEL_1;

