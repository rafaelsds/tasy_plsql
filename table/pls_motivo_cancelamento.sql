ALTER TABLE TASY.PLS_MOTIVO_CANCELAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOTIVO_CANCELAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOTIVO_CANCELAMENTO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_MOTIVO_CANCELAMENTO         VARCHAR2(10 BYTE) NOT NULL,
  DS_MOTIVO_CANCELAMENTO         VARCHAR2(255 BYTE) NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_INADIMPLENCIA               VARCHAR2(1 BYTE),
  IE_PORTABILIDADE               VARCHAR2(1 BYTE) NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  NR_SEQ_MOTIVO_ANT              NUMBER(10),
  CD_INTERCAMBIO                 VARCHAR2(10 BYTE),
  IE_OBITO                       VARCHAR2(1 BYTE),
  IE_LIBERACAO_MOTIVO            VARCHAR2(2 BYTE),
  IE_GERAR_LEAD_RESCISAO         VARCHAR2(1 BYTE),
  DS_MENSAGEM_WEB                VARCHAR2(4000 BYTE),
  IE_SITUACAO_TRABALHISTA        VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_REPROV_INCLUSAO  NUMBER(10),
  IE_CONTRATACAO_CONCLUIDA       VARCHAR2(1 BYTE),
  IE_SOLICITACAO_RESCISAO        VARCHAR2(1 BYTE),
  IE_ISENTAR_MULTA               VARCHAR2(1 BYTE),
  CD_MOTIVO_EXCLUSAO             VARCHAR2(10 BYTE),
  IE_INICIATIVA_BENEFICIARIO     VARCHAR2(1 BYTE),
  IE_DIREITO_PORTABILIDADE       VARCHAR2(1 BYTE) DEFAULT null,
  IE_CARREGAR_CONTATO_WEB        VARCHAR2(1 BYTE),
  IE_PERMITE_ALT_DATA_SOLIC      VARCHAR2(1 BYTE),
  IE_EXIGE_INF_FIN_DEVOLUCAO     VARCHAR2(1 BYTE),
  IE_IMPEDIR_REATIVACAO          VARCHAR2(1 BYTE),
  IE_IMPEDIR_MIGRACAO            VARCHAR2(1 BYTE),
  IE_PERMITE_DEVOLUCAO_AUTOM     VARCHAR2(1 BYTE),
  IE_OBRIGATORIEDADE_TELEFONE    VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMOCA_ESTABEL_FK_I ON TASY.PLS_MOTIVO_CANCELAMENTO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMOCA_PK ON TASY.PLS_MOTIVO_CANCELAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMOCA_PLSINBR_FK_I ON TASY.PLS_MOTIVO_CANCELAMENTO
(NR_SEQ_MOTIVO_REPROV_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_MOTIVO_CANCELAMENTO_tp  after update ON TASY.PLS_MOTIVO_CANCELAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIEDADE_TELEFONE,1,4000),substr(:new.IE_OBRIGATORIEDADE_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIEDADE_TELEFONE',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO_CANCELAMENTO,1,4000),substr(:new.DS_MOTIVO_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_CANCELAMENTO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INADIMPLENCIA,1,4000),substr(:new.IE_INADIMPLENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INADIMPLENCIA',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTABILIDADE,1,4000),substr(:new.IE_PORTABILIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTABILIDADE',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_TRABALHISTA,1,4000),substr(:new.IE_SITUACAO_TRABALHISTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_TRABALHISTA',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTRATACAO_CONCLUIDA,1,4000),substr(:new.IE_CONTRATACAO_CONCLUIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTRATACAO_CONCLUIDA',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_REPROV_INCLUSAO,1,4000),substr(:new.NR_SEQ_MOTIVO_REPROV_INCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_REPROV_INCLUSAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INICIATIVA_BENEFICIARIO,1,4000),substr(:new.IE_INICIATIVA_BENEFICIARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INICIATIVA_BENEFICIARIO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ISENTAR_MULTA,1,4000),substr(:new.IE_ISENTAR_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ISENTAR_MULTA',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLICITACAO_RESCISAO,1,4000),substr(:new.IE_SOLICITACAO_RESCISAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITACAO_RESCISAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_INF_FIN_DEVOLUCAO,1,4000),substr(:new.IE_EXIGE_INF_FIN_DEVOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_INF_FIN_DEVOLUCAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPEDIR_MIGRACAO,1,4000),substr(:new.IE_IMPEDIR_MIGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPEDIR_MIGRACAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARREGAR_CONTATO_WEB,1,4000),substr(:new.IE_CARREGAR_CONTATO_WEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARREGAR_CONTATO_WEB',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_DEVOLUCAO_AUTOM,1,4000),substr(:new.IE_PERMITE_DEVOLUCAO_AUTOM,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_DEVOLUCAO_AUTOM',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIREITO_PORTABILIDADE,1,4000),substr(:new.IE_DIREITO_PORTABILIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIREITO_PORTABILIDADE',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_ALT_DATA_SOLIC,1,4000),substr(:new.IE_PERMITE_ALT_DATA_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_ALT_DATA_SOLIC',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPEDIR_REATIVACAO,1,4000),substr(:new.IE_IMPEDIR_REATIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPEDIR_REATIVACAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERCAMBIO,1,4000),substr(:new.CD_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERCAMBIO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBITO,1,4000),substr(:new.IE_OBITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBITO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIBERACAO_MOTIVO,1,4000),substr(:new.IE_LIBERACAO_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERACAO_MOTIVO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_LEAD_RESCISAO,1,4000),substr(:new.IE_GERAR_LEAD_RESCISAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_LEAD_RESCISAO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_CANCELAMENTO,1,4000),substr(:new.CD_MOTIVO_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_CANCELAMENTO',ie_log_w,ds_w,'PLS_MOTIVO_CANCELAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MOTIVO_CANCELAMENTO ADD (
  CONSTRAINT PLSMOCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MOTIVO_CANCELAMENTO ADD (
  CONSTRAINT PLSMOCA_PLSINBR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REPROV_INCLUSAO) 
 REFERENCES TASY.PLS_INCLUSAO_BENEF_REPROV (NR_SEQUENCIA),
  CONSTRAINT PLSMOCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MOTIVO_CANCELAMENTO TO NIVEL_1;

