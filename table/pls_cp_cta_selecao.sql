ALTER TABLE TASY.PLS_CP_CTA_SELECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_SELECAO CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_CP_CTA_SELECAO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_ID_TRANSACAO     NUMBER(10)                NOT NULL,
  IE_TIPO_REGISTRO    VARCHAR2(2 BYTE)          NOT NULL,
  NR_SEQ_CONTA        NUMBER(10)                NOT NULL,
  NR_SEQ_FILTRO       NUMBER(10)                NOT NULL,
  IE_VALIDO           VARCHAR2(1 BYTE)          NOT NULL,
  IE_VALIDO_TEMP      VARCHAR2(1 BYTE)          NOT NULL,
  IE_EXCECAO          VARCHAR2(1 BYTE)          NOT NULL,
  NR_SEQ_CONTA_PROC   NUMBER(10),
  NR_SEQ_PROC_PARTIC  NUMBER(10),
  NR_SEQ_CONTA_MAT    NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.PCPCTSE_I1 ON TASY.PLS_CP_CTA_SELECAO
(NR_ID_TRANSACAO, IE_VALIDO, NR_SEQ_FILTRO, NR_SEQ_CONTA, NR_SEQ_CONTA_PROC, 
NR_SEQ_PROC_PARTIC);


CREATE INDEX TASY.PCPCTSE_I2 ON TASY.PLS_CP_CTA_SELECAO
(NR_ID_TRANSACAO, IE_VALIDO, NR_SEQ_FILTRO, NR_SEQ_CONTA, NR_SEQ_CONTA_MAT);


CREATE INDEX TASY.PCPCTSE_I3 ON TASY.PLS_CP_CTA_SELECAO
(NR_ID_TRANSACAO, IE_EXCECAO);


CREATE UNIQUE INDEX TASY.PCPCTSE_PK ON TASY.PLS_CP_CTA_SELECAO
(NR_SEQUENCIA);


CREATE UNIQUE INDEX TASY.PCPCTSE_UK ON TASY.PLS_CP_CTA_SELECAO
(NR_ID_TRANSACAO, NR_SEQ_FILTRO, NR_SEQ_CONTA, NR_SEQ_CONTA_PROC, NR_SEQ_PROC_PARTIC, 
NR_SEQ_CONTA_MAT);


ALTER TABLE TASY.PLS_CP_CTA_SELECAO ADD (
  CONSTRAINT PCPCTSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT PCPCTSE_UK
 UNIQUE (NR_ID_TRANSACAO, NR_SEQ_FILTRO, NR_SEQ_CONTA, NR_SEQ_CONTA_PROC, NR_SEQ_PROC_PARTIC, NR_SEQ_CONTA_MAT));

GRANT SELECT ON TASY.PLS_CP_CTA_SELECAO TO NIVEL_1;

