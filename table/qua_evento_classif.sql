ALTER TABLE TASY.QUA_EVENTO_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_EVENTO_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_EVENTO_CLASSIF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_EVENTO         NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_CLASSIFICACAO      VARCHAR2(80 BYTE)       NOT NULL,
  IE_CLASSIFICACAO      VARCHAR2(10 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_EXP_CLASSIFICACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAEVCL_DICEXPR_FK_I ON TASY.QUA_EVENTO_CLASSIF
(CD_EXP_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUAEVCL_PK ON TASY.QUA_EVENTO_CLASSIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVCL_QUAEVEN_FK_I ON TASY.QUA_EVENTO_CLASSIF
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUA_EVENTO_CLASSIF ADD (
  CONSTRAINT QUAEVCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_EVENTO_CLASSIF ADD (
  CONSTRAINT QUAEVCL_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CLASSIFICACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT QUAEVCL_QUAEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.QUA_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_EVENTO_CLASSIF TO NIVEL_1;

