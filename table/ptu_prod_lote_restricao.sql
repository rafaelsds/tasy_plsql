ALTER TABLE TASY.PTU_PROD_LOTE_RESTRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_PROD_LOTE_RESTRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_PROD_LOTE_RESTRICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10),
  NR_SEQ_CONTRATO      NUMBER(10),
  NR_CONTRATO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTPLRES_ESTABEL_FK_I ON TASY.PTU_PROD_LOTE_RESTRICAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTPLRES_PK ON TASY.PTU_PROD_LOTE_RESTRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTPLRES_PLSCONT_FK_I ON TASY.PTU_PROD_LOTE_RESTRICAO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTPLRES_PTUMOPL_FK_I ON TASY.PTU_PROD_LOTE_RESTRICAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_prod_lote_restricao_atual
before insert or update ON TASY.PTU_PROD_LOTE_RESTRICAO for each row
declare

nr_seq_contrato_w	pls_contrato.nr_sequencia%type;

begin

if	(:new.nr_contrato is not null) and
	(:new.nr_contrato <> nvl(:old.nr_contrato,0)) then

	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado n�o existe. Verifique!
	end if;
else
	:new.nr_seq_contrato	:= null;
end if;

end;
/


ALTER TABLE TASY.PTU_PROD_LOTE_RESTRICAO ADD (
  CONSTRAINT PTPLRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_PROD_LOTE_RESTRICAO ADD (
  CONSTRAINT PTPLRES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PTPLRES_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PTPLRES_PTUMOPL_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_MOV_PRODUTO_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_PROD_LOTE_RESTRICAO TO NIVEL_1;

