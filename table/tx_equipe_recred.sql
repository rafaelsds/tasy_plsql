ALTER TABLE TASY.TX_EQUIPE_RECRED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_EQUIPE_RECRED CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_EQUIPE_RECRED
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPE        NUMBER(10)               NOT NULL,
  NR_RECREDENCIAMENTO  NUMBER(5)                NOT NULL,
  NR_PORTARIA          NUMBER(5)                NOT NULL,
  DT_RECREDENCIAMENTO  DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXEQRE_PK ON TASY.TX_EQUIPE_RECRED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXEQRE_TXEQTR_FK_I ON TASY.TX_EQUIPE_RECRED
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQRE_TXEQTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXEQRE_UK ON TASY.TX_EQUIPE_RECRED
(NR_SEQ_EQUIPE, NR_RECREDENCIAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXEQRE_UK
  MONITORING USAGE;


ALTER TABLE TASY.TX_EQUIPE_RECRED ADD (
  CONSTRAINT TXEQRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TXEQRE_UK
 UNIQUE (NR_SEQ_EQUIPE, NR_RECREDENCIAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_EQUIPE_RECRED ADD (
  CONSTRAINT TXEQRE_TXEQTR_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.TX_EQUIPE_TRANSPL (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_EQUIPE_RECRED TO NIVEL_1;

