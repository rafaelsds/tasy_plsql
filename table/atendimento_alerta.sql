ALTER TABLE TASY.ATENDIMENTO_ALERTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_ALERTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_ALERTA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ALERTA                  DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_ALERTA                  VARCHAR2(2000 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_FIM_ALERTA              DATE,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_TIPO_ALERTA         NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4),
  CD_PESSOA_ALERTA           VARCHAR2(10 BYTE),
  NR_SEQ_PRECAUCAO           NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_ALERTA              NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEALER_ATCONSPEPA_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALER_ATEPACI_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALER_ATEPREC_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_PRECAUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALER_ATEPREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALER_ESTABEL_FK_I ON TASY.ATENDIMENTO_ALERTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALER_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALER_I1 ON TASY.ATENDIMENTO_ALERTA
(NM_USUARIO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALER_I1
  MONITORING USAGE;


CREATE INDEX TASY.ATEALER_PERFIL_FK_I ON TASY.ATENDIMENTO_ALERTA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALER_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALER_PESFISI_FK_I ON TASY.ATENDIMENTO_ALERTA
(CD_PESSOA_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEALER_PK ON TASY.ATENDIMENTO_ALERTA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALER_TASASDI_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALER_TASASDI_FK2_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALER_TIALATE_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_TIPO_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALER_TIALATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALER_TIALATEO_FK_I ON TASY.ATENDIMENTO_ALERTA
(NR_SEQ_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atendimento_alerta_insert
before insert ON TASY.ATENDIMENTO_ALERTA for each row
declare

ie_liberar_alerta_w		varchar2(1);

begin

Obter_Param_Usuario(916, 715, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_liberar_alerta_w);

if	(nvl(ie_liberar_alerta_w,'N') = 'S') then
	begin
	:new.DT_LIBERACAO := sysdate;
	end;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_ALERTA_atual
before insert or update ON TASY.ATENDIMENTO_ALERTA for each row
declare


begin
if	(nvl(:old.DT_ALERTA,sysdate+10) <> :new.DT_ALERTA) and
	(:new.DT_ALERTA is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ALERTA,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_alerta_delete
before delete ON TASY.ATENDIMENTO_ALERTA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
                   nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'),
									 obter_desc_expressao(322454),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
                   wheb_usuario_pck.get_nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_ALERTA_tp  after update ON TASY.ATENDIMENTO_ALERTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_ALERTA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ALERTA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ALERTA',ie_log_w,ds_w,'ATENDIMENTO_ALERTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_ALERTA_SBIS_IN
before insert or update ON TASY.ATENDIMENTO_ALERTA for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);

begin

IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
									     obter_pessoa_atendimento(:new.nr_atendimento,'C'),
									     obter_desc_expressao(322454),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
else
	ie_inativacao_w := 'N';

	if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
		ie_inativacao_w := 'S';
	end if;

	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 obter_pessoa_atendimento(:new.nr_atendimento,'C'),
										 obter_desc_expressao(322454),
										 :new.nr_atendimento,
										 :new.dt_liberacao,
										 :new.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_alerta_pend_atual
after insert or update ON TASY.ATENDIMENTO_ALERTA for each row
declare

qt_reg_w				number(1);
ie_lib_alerta_pac_w		parametro_medico.ie_lib_alerta_pac%type;
cd_pessoa_fisica_w		pep_item_pendente.cd_pessoa_fisica%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	max(ie_lib_alerta_pac)
into	ie_lib_alerta_pac_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if (nvl(ie_lib_alerta_pac_w,'N') = 'S') then

	select 	substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)
	into	cd_pessoa_fisica_w
	from	dual;

	if (:new.dt_liberacao is null) then
		Gerar_registro_pendente_PEP('ALA', :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario);
	elsif (:old.dt_liberacao is null) and (:new.dt_liberacao is not null) then
		Gerar_registro_pendente_PEP('XALA', :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario);
	end if;

end if;

<<Final>>
qt_reg_w := 0;

end;
/


ALTER TABLE TASY.ATENDIMENTO_ALERTA ADD (
  CONSTRAINT ATEALER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_ALERTA ADD (
  CONSTRAINT ATEALER_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEALER_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEALER_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEALER_TIALATEO_FK 
 FOREIGN KEY (NR_SEQ_ALERTA) 
 REFERENCES TASY.TIPO_ALERTA_ATEND_OPTION (NR_SEQUENCIA),
  CONSTRAINT ATEALER_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEALER_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEALER_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ATEALER_TIALATE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ALERTA) 
 REFERENCES TASY.TIPO_ALERTA_ATEND (NR_SEQUENCIA),
  CONSTRAINT ATEALER_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_ALERTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEALER_ATEPREC_FK 
 FOREIGN KEY (NR_SEQ_PRECAUCAO) 
 REFERENCES TASY.ATENDIMENTO_PRECAUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_ALERTA TO NIVEL_1;

