ALTER TABLE TASY.SPA_ROTA_APROV_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_ROTA_APROV_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_ROTA_APROV_RESP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ESTRUTURA      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TIPO_RESPONSAVEL   VARCHAR2(1 BYTE)        NOT NULL,
  NM_USUARIO_APROVACAO  VARCHAR2(15 BYTE),
  CD_CARGO              NUMBER(10),
  NR_ORDEM              NUMBER(3)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SPRPAPR_CARGO_FK_I ON TASY.SPA_ROTA_APROV_RESP
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPRPAPR_CARGO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SPRPAPR_PK ON TASY.SPA_ROTA_APROV_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPRPAPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SPRPAPR_SPROAPE_FK_I ON TASY.SPA_ROTA_APROV_RESP
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPRPAPR_SPROAPE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SPA_ROTA_APROV_RESP_tp  after update ON TASY.SPA_ROTA_APROV_RESP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_RESPONSAVEL,1,4000),substr(:new.IE_TIPO_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RESPONSAVEL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM,1,4000),substr(:new.NR_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_APROVACAO,1,4000),substr(:new.NM_USUARIO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_APROVACAO',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SPA_ROTA_APROV_RESP ADD (
  CONSTRAINT SPRPAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_ROTA_APROV_RESP ADD (
  CONSTRAINT SPRPAPR_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT SPRPAPR_SPROAPE_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.SPA_ROTA_APROV_ESTRUT (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_ROTA_APROV_RESP TO NIVEL_1;

