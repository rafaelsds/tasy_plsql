ALTER TABLE TASY.ESCALA_ASIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ASIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ASIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_MOTOR_C5_L          VARCHAR2(3 BYTE),
  IE_MOTOR_C5_R          VARCHAR2(3 BYTE),
  IE_MOTOR_C6_R          VARCHAR2(3 BYTE),
  IE_MOTOR_C6_L          VARCHAR2(3 BYTE),
  IE_MOTOR_C7_L          VARCHAR2(3 BYTE),
  IE_MOTOR_C7_R          VARCHAR2(3 BYTE),
  IE_MOTOR_C8_R          VARCHAR2(3 BYTE),
  IE_MOTOR_C8_L          VARCHAR2(3 BYTE),
  IE_MOTOR_T1_L          VARCHAR2(3 BYTE),
  IE_MOTOR_T1_R          VARCHAR2(3 BYTE),
  IE_MOTOR_L2_R          VARCHAR2(3 BYTE),
  IE_MOTOR_L2_L          VARCHAR2(3 BYTE),
  IE_MOTOR_L3_L          VARCHAR2(3 BYTE),
  IE_MOTOR_L3_R          VARCHAR2(3 BYTE),
  IE_MOTOR_L4_R          VARCHAR2(3 BYTE),
  IE_MOTOR_L4_L          VARCHAR2(3 BYTE),
  IE_MOTOR_L5_L          VARCHAR2(3 BYTE),
  IE_MOTOR_L5_R          VARCHAR2(3 BYTE),
  IE_MOTOR_S1_R          VARCHAR2(3 BYTE),
  IE_MOTOR_S1_L          VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C2_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C2_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C3_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C3_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C4_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C4_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C5_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C5_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C6_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C6_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C7_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C7_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C8_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_C8_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T1_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T1_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T2_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T2_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T3_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T3_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T4_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T4_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T5_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T5_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T6_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T6_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T7_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T7_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T8_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T8_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T9_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T9_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T10_R      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T10_L      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T11_L      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T11_R      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T12_R      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_T12_L      VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L1_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L1_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L2_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L2_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L3_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L3_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L4_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L4_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L5_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_L5_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S1_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S1_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S2_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S2_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S3_R       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S3_L       VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S4_5_R     VARCHAR2(3 BYTE),
  IE_SENS_TOQ_S4_5_L     VARCHAR2(3 BYTE),
  IE_SENS_AGU_C2_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C2_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C3_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C3_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C4_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C4_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C5_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C5_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C6_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C6_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C7_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C7_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C8_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_C8_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T1_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T1_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T2_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T2_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T3_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T3_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T4_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T4_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T5_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T5_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T6_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T6_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T7_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T7_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T8_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T8_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T9_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T9_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_T10_R      VARCHAR2(3 BYTE),
  IE_SENS_AGU_T10_L      VARCHAR2(3 BYTE),
  IE_SENS_AGU_T11_L      VARCHAR2(3 BYTE),
  IE_SENS_AGU_T11_R      VARCHAR2(3 BYTE),
  IE_SENS_AGU_T12_R      VARCHAR2(3 BYTE),
  IE_SENS_AGU_T12_L      VARCHAR2(3 BYTE),
  IE_SENS_AGU_L1_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L1_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L2_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L2_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L3_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L3_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L4_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L4_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L5_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_L5_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S1_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S1_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S2_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S2_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S3_R       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S3_L       VARCHAR2(3 BYTE),
  IE_SENS_AGU_S4_5_R     VARCHAR2(3 BYTE),
  IE_SENS_AGU_S4_5_L     VARCHAR2(3 BYTE),
  IE_CONTR_ANAL_VOL      VARCHAR2(1 BYTE),
  IE_SENSIBILIDADE_ANAL  VARCHAR2(1 BYTE),
  QT_PONT_MOTOR_SUP_L    NUMBER(4),
  QT_PONT_MOTOR_SUP_R    NUMBER(4),
  QT_PONT_MOTOR_SUP      NUMBER(4),
  QT_PONT_MOTOR_INF_L    NUMBER(4),
  QT_PONT_MOTOR_INF_R    NUMBER(4),
  QT_PONT_MOTOR_INF      NUMBER(4),
  QT_PONT_SENS_TOQ_L     NUMBER(4),
  QT_PONT_SENS_TOQ_R     NUMBER(4),
  QT_PONT_SENS_TOQ       NUMBER(4),
  QT_PONT_AGU_TOQ_R      NUMBER(4),
  QT_PONT_AGU_TOQ_L      NUMBER(4),
  QT_PONT_AGU_TOQ        NUMBER(4),
  IE_COMPLETO            VARCHAR2(1 BYTE),
  IE_ASIA_SCORE          VARCHAR2(1 BYTE),
  IE_NEURO_SENS_L        VARCHAR2(4 BYTE),
  IE_NEURO_SENS_R        VARCHAR2(4 BYTE),
  IE_NEURO_MOT_R         VARCHAR2(4 BYTE),
  IE_NEURO_MOT_L         VARCHAR2(4 BYTE),
  IE_ZONE_SENS_R         VARCHAR2(4 BYTE),
  IE_ZONE_SENS_L         VARCHAR2(4 BYTE),
  IE_ZONE_MOT_L          VARCHAR2(4 BYTE),
  IE_ZONE_MOT_R          VARCHAR2(4 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCASIA_ATEPACI_FK_I ON TASY.ESCALA_ASIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCASIA_PESFISI_FK_I ON TASY.ESCALA_ASIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCASIA_PK ON TASY.ESCALA_ASIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_asia_atual
before insert or update ON TASY.ESCALA_ASIA for each row
declare
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.QT_PONT_MOTOR_SUP_R	:=	somente_numero(:new.IE_MOTOR_C5_R) +
					somente_numero(:new.IE_MOTOR_C6_R) +
					somente_numero(:new.IE_MOTOR_C7_R) +
					somente_numero(:new.IE_MOTOR_C8_R) +
					somente_numero(:new.IE_MOTOR_T1_R);
:new.QT_PONT_MOTOR_SUP_L	:=	somente_numero(:new.IE_MOTOR_C5_L) +
					somente_numero(:new.IE_MOTOR_C6_L) +
					somente_numero(:new.IE_MOTOR_C7_L) +
					somente_numero(:new.IE_MOTOR_C8_L) +
					somente_numero(:new.IE_MOTOR_T1_L);
:new.QT_PONT_MOTOR_SUP		:=	:new.QT_PONT_MOTOR_SUP_R + :new.QT_PONT_MOTOR_SUP_L;

:new.QT_PONT_MOTOR_INF_R	:=	somente_numero(:new.IE_MOTOR_L2_R) +
					somente_numero(:new.IE_MOTOR_L3_R) +
					somente_numero(:new.IE_MOTOR_L4_R) +
					somente_numero(:new.IE_MOTOR_L5_R) +
					somente_numero(:new.IE_MOTOR_S1_R);
:new.QT_PONT_MOTOR_INF_L	:=	somente_numero(:new.IE_MOTOR_L2_L) +
					somente_numero(:new.IE_MOTOR_L3_L) +
					somente_numero(:new.IE_MOTOR_L4_L) +
					somente_numero(:new.IE_MOTOR_L5_L) +
					somente_numero(:new.IE_MOTOR_S1_L);
:new.QT_PONT_MOTOR_INF		:=	:new.QT_PONT_MOTOR_INF_R + :new.QT_PONT_MOTOR_INF_L;

:new.QT_PONT_AGU_TOQ_R		:=	somente_numero(:new.IE_SENS_AGU_C2_R) +
					somente_numero(:new.IE_SENS_AGU_C3_R) +
					somente_numero(:new.IE_SENS_AGU_C4_R) +
					somente_numero(:new.IE_SENS_AGU_C5_R) +
					somente_numero(:new.IE_SENS_AGU_C6_R) +
					somente_numero(:new.IE_SENS_AGU_C7_R) +
					somente_numero(:new.IE_SENS_AGU_C8_R) +
					somente_numero(:new.IE_SENS_AGU_T1_R) +
					somente_numero(:new.IE_SENS_AGU_T2_R) +
					somente_numero(:new.IE_SENS_AGU_T3_R) +
					somente_numero(:new.IE_SENS_AGU_T4_R) +
					somente_numero(:new.IE_SENS_AGU_T5_R) +
					somente_numero(:new.IE_SENS_AGU_T6_R) +
					somente_numero(:new.IE_SENS_AGU_T7_R) +
					somente_numero(:new.IE_SENS_AGU_T8_R) +
					somente_numero(:new.IE_SENS_AGU_T9_R) +
					somente_numero(:new.IE_SENS_AGU_T10_R) +
					somente_numero(:new.IE_SENS_AGU_T11_R) +
					somente_numero(:new.IE_SENS_AGU_T12_R) +
					somente_numero(:new.IE_SENS_AGU_L1_R) +
					somente_numero(:new.IE_SENS_AGU_L2_R) +
					somente_numero(:new.IE_SENS_AGU_L3_R) +
					somente_numero(:new.IE_SENS_AGU_L4_R) +
					somente_numero(:new.IE_SENS_AGU_L5_R) +
					somente_numero(:new.IE_SENS_AGU_S1_R) +
					somente_numero(:new.IE_SENS_AGU_S2_R) +
					somente_numero(:new.IE_SENS_AGU_S3_R) +
					somente_numero(:new.IE_SENS_AGU_S4_5_R);
:new.QT_PONT_AGU_TOQ_L		:=	somente_numero(:new.IE_SENS_AGU_C2_L) +
					somente_numero(:new.IE_SENS_AGU_C3_L) +
					somente_numero(:new.IE_SENS_AGU_C4_L) +
					somente_numero(:new.IE_SENS_AGU_C5_L) +
					somente_numero(:new.IE_SENS_AGU_C6_L) +
					somente_numero(:new.IE_SENS_AGU_C7_L) +
					somente_numero(:new.IE_SENS_AGU_C8_L) +
					somente_numero(:new.IE_SENS_AGU_T1_L) +
					somente_numero(:new.IE_SENS_AGU_T2_L) +
					somente_numero(:new.IE_SENS_AGU_T3_L) +
					somente_numero(:new.IE_SENS_AGU_T4_L) +
					somente_numero(:new.IE_SENS_AGU_T5_L) +
					somente_numero(:new.IE_SENS_AGU_T6_L) +
					somente_numero(:new.IE_SENS_AGU_T7_L) +
					somente_numero(:new.IE_SENS_AGU_T8_L) +
					somente_numero(:new.IE_SENS_AGU_T9_L) +
					somente_numero(:new.IE_SENS_AGU_T10_L) +
					somente_numero(:new.IE_SENS_AGU_T11_L) +
					somente_numero(:new.IE_SENS_AGU_T12_L) +
					somente_numero(:new.IE_SENS_AGU_L1_L) +
					somente_numero(:new.IE_SENS_AGU_L2_L) +
					somente_numero(:new.IE_SENS_AGU_L3_L) +
					somente_numero(:new.IE_SENS_AGU_L4_L) +
					somente_numero(:new.IE_SENS_AGU_L5_L) +
					somente_numero(:new.IE_SENS_AGU_S1_L) +
					somente_numero(:new.IE_SENS_AGU_S2_L) +
					somente_numero(:new.IE_SENS_AGU_S3_L) +
					somente_numero(:new.IE_SENS_AGU_S4_5_L);
:new.QT_PONT_AGU_TOQ		:=	:new.QT_PONT_AGU_TOQ_R + :new.QT_PONT_AGU_TOQ_L;

:new.QT_PONT_SENS_TOQ_R		:=	somente_numero(:new.IE_SENS_TOQ_C2_R) +
					somente_numero(:new.IE_SENS_TOQ_C3_R) +
					somente_numero(:new.IE_SENS_TOQ_C4_R) +
					somente_numero(:new.IE_SENS_TOQ_C5_R) +
					somente_numero(:new.IE_SENS_TOQ_C6_R) +
					somente_numero(:new.IE_SENS_TOQ_C7_R) +
					somente_numero(:new.IE_SENS_TOQ_C8_R) +
					somente_numero(:new.IE_SENS_TOQ_T1_R) +
					somente_numero(:new.IE_SENS_TOQ_T2_R) +
					somente_numero(:new.IE_SENS_TOQ_T3_R) +
					somente_numero(:new.IE_SENS_TOQ_T4_R) +
					somente_numero(:new.IE_SENS_TOQ_T5_R) +
					somente_numero(:new.IE_SENS_TOQ_T6_R) +
					somente_numero(:new.IE_SENS_TOQ_T7_R) +
					somente_numero(:new.IE_SENS_TOQ_T8_R) +
					somente_numero(:new.IE_SENS_TOQ_T9_R) +
					somente_numero(:new.IE_SENS_TOQ_T10_R) +
					somente_numero(:new.IE_SENS_TOQ_T11_R) +
					somente_numero(:new.IE_SENS_TOQ_T12_R) +
					somente_numero(:new.IE_SENS_TOQ_L1_R) +
					somente_numero(:new.IE_SENS_TOQ_L2_R) +
					somente_numero(:new.IE_SENS_TOQ_L3_R) +
					somente_numero(:new.IE_SENS_TOQ_L4_R) +
					somente_numero(:new.IE_SENS_TOQ_L5_R) +
					somente_numero(:new.IE_SENS_TOQ_S1_R) +
					somente_numero(:new.IE_SENS_TOQ_S2_R) +
					somente_numero(:new.IE_SENS_TOQ_S3_R) +
					somente_numero(:new.IE_SENS_TOQ_S4_5_R);
:new.QT_PONT_SENS_TOQ_L		:=	somente_numero(:new.IE_SENS_TOQ_C2_L) +
					somente_numero(:new.IE_SENS_TOQ_C3_L) +
					somente_numero(:new.IE_SENS_TOQ_C4_L) +
					somente_numero(:new.IE_SENS_TOQ_C5_L) +
					somente_numero(:new.IE_SENS_TOQ_C6_L) +
					somente_numero(:new.IE_SENS_TOQ_C7_L) +
					somente_numero(:new.IE_SENS_TOQ_C8_L) +
					somente_numero(:new.IE_SENS_TOQ_T1_L) +
					somente_numero(:new.IE_SENS_TOQ_T2_L) +
					somente_numero(:new.IE_SENS_TOQ_T3_L) +
					somente_numero(:new.IE_SENS_TOQ_T4_L) +
					somente_numero(:new.IE_SENS_TOQ_T5_L) +
					somente_numero(:new.IE_SENS_TOQ_T6_L) +
					somente_numero(:new.IE_SENS_TOQ_T7_L) +
					somente_numero(:new.IE_SENS_TOQ_T8_L) +
					somente_numero(:new.IE_SENS_TOQ_T9_L) +
					somente_numero(:new.IE_SENS_TOQ_T10_L) +
					somente_numero(:new.IE_SENS_TOQ_T11_L) +
					somente_numero(:new.IE_SENS_TOQ_T12_L) +
					somente_numero(:new.IE_SENS_TOQ_L1_L) +
					somente_numero(:new.IE_SENS_TOQ_L2_L) +
					somente_numero(:new.IE_SENS_TOQ_L3_L) +
					somente_numero(:new.IE_SENS_TOQ_L4_L) +
					somente_numero(:new.IE_SENS_TOQ_L5_L) +
					somente_numero(:new.IE_SENS_TOQ_S1_L) +
					somente_numero(:new.IE_SENS_TOQ_S2_L) +
					somente_numero(:new.IE_SENS_TOQ_S3_L) +
					somente_numero(:new.IE_SENS_TOQ_S4_5_L);
:new.QT_PONT_SENS_TOQ		:=	:new.QT_PONT_SENS_TOQ_R + :new.QT_PONT_SENS_TOQ_L;


<<Final>>
qt_reg_w	:= 0;


end escala_asia_atual;
/


ALTER TABLE TASY.ESCALA_ASIA ADD (
  CONSTRAINT ESCASIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ASIA ADD (
  CONSTRAINT ESCASIA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCASIA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_ASIA TO NIVEL_1;

