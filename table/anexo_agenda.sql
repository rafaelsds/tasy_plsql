ALTER TABLE TASY.ANEXO_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANEXO_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANEXO_AGENDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_STATUS        NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_TIPO_ANEXO    NUMBER(10),
  IM_DOCUMENTO         LONG RAW,
  NR_SEQ_LISTA_ESPERA  NUMBER(10),
  NM_USUARIO_EXCLUSAO  VARCHAR2(15 BYTE),
  DT_EXCLUSAO          DATE,
  DS_MOTIVO_EXCLUSAO   VARCHAR2(255 BYTE),
  NR_SEQ_DOCUMENTO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANEXAGE_AGEPACI_FK_I ON TASY.ANEXO_AGENDA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANEXAGE_AGLIESP_FK_I ON TASY.ANEXO_AGENDA
(NR_SEQ_LISTA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ANEXAGE_PK ON TASY.ANEXO_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANEXAGE_STANAGE_FK_I ON TASY.ANEXO_AGENDA
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANEXAGE_STANAGE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANEXAGE_TIANAGE_FK_I ON TASY.ANEXO_AGENDA
(NR_SEQ_TIPO_ANEXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANEXAGE_TIANAGE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.anexo_agenda_update
before update ON TASY.ANEXO_AGENDA for each row
declare

begin

if	(:old.nr_seq_status <> :new.nr_seq_status) or
	((nvl(:old.ds_observacao,'*') = nvl(:new.ds_observacao,'*')) and
	 (:old.nr_seq_status = :new.nr_seq_status)) or
	((:old.nr_seq_status is null) and (:new.nr_seq_status is not null)) or
	((:old.nr_seq_status is not null) and (:new.nr_seq_status is null)) then

	insert into anexo_agenda_hist (
		nr_sequencia,
		nr_seq_anexo,
		nm_usuario,
		dt_atualizacao,
		nr_seq_status_ant,
		nr_seq_status_atual,
		ds_observacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(	anexo_agenda_hist_seq.NextVal,
		:new.nr_sequencia,
		'tasy',
		sysdate,
		:old.nr_seq_status,
		:new.nr_seq_status,
		:old.ds_observacao,
		:new.nm_usuario,
		sysdate);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.anexo_agenda_after_update
after update or insert ON TASY.ANEXO_AGENDA for each row
declare

nr_seq_evento_w		number(10);
qt_idade_w		number(10);
cd_convenio_w		number(15);
cd_medico_w		varchar2(15);
cd_pessoa_fisica_w	varchar2(15);
ie_sexo_w		varchar2(15);
nr_seq_proc_interno_w	number(10);
ie_evento_w		varchar2(15);
cd_estabelecimento_w	number(4);
cd_agenda_w		number(10);
hr_inicio_w		date;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
dt_cancelamento_w	date;
cd_motivo_cancel_w	varchar2(15);
nr_atendimento_w	number(10);
ds_observacao_w		varchar2(255);


cursor c01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	ie_evento_disp			= ie_evento_w
	and	nvl(qt_idade_w,0) between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_sexo,nvl(ie_sexo_w,'XPTO'))  = NVL(ie_sexo_w,'XPTO')
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	(obter_se_proc_rec_alerta(nr_seq_proc_interno_w,nr_sequencia,cd_procedimento_w,ie_origem_proced_w) = 'S')
	and	(obter_se_regra_envio(nr_sequencia,nr_atendimento_w) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	nvl(ie_situacao,'A') = 'A';
	--and	(obter_se_mat_rec_alerta(cd_material_w,nr_sequencia) = 'S')

begin

if (:new.nr_seq_agenda is not null) then

	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

	select	cd_pessoa_fisica,
		nvl(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),nvl(qt_idade_paciente,0)),
		Obter_Sexo_PF(cd_pessoa_fisica,'C'),
		cd_medico,
		cd_convenio,
		nr_seq_proc_interno,
		cd_agenda,
		hr_inicio,
		cd_procedimento,
		ie_origem_proced,
		dt_cancelamento,
		cd_motivo_cancelamento,
		nr_atendimento,
		substr(ds_observacao,1,255)
	into	cd_pessoa_fisica_w,
		qt_idade_w,
		ie_sexo_w,
		cd_medico_w,
		cd_convenio_w,
		nr_seq_proc_interno_w,
		cd_agenda_w,
		hr_inicio_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_cancelamento_w,
		cd_motivo_cancel_w,
		nr_atendimento_w,
		ds_observacao_w
	from	agenda_paciente
	where	nr_sequencia = :new.nr_seq_agenda;

	if	(inserting) then
		ie_evento_w	:= 'IAN';
	else
		ie_evento_w	:= 'AAN';
	end if;


	open c01;
	loop
	fetch c01 into
		nr_seq_evento_w;
	exit when c01%notfound;
		begin
		gerar_evento_agenda_trigger(	nr_seq_evento_w,
						null,
						cd_pessoa_fisica_w,
						null,
						:new.nm_usuario,
						cd_agenda_w,
						hr_inicio_w,
						cd_medico_w,
						cd_procedimento_w,
						ie_origem_proced_w,
						dt_cancelamento_w,
						null,
						null,
						null,
						cd_convenio_w,
						cd_motivo_cancel_w,
						'S',
						null,
						null,
						null,
						null,
						null,
						ds_observacao_w);
		end;
	end loop;
	close c01;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.anexo_agenda_delete
before delete ON TASY.ANEXO_AGENDA for each row
declare

nr_seq_evento_w		number(10);
qt_idade_w		number(10);
cd_convenio_w		number(15);
cd_medico_w		varchar2(15);
cd_pessoa_fisica_w	varchar2(15);
ie_sexo_w		varchar2(15);
nr_seq_proc_interno_w	number(10);
ie_evento_w		varchar2(15);
cd_estabelecimento_w	number(4);
cd_agenda_w		number(10);
hr_inicio_w		date;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
dt_cancelamento_w	date;
cd_motivo_cancel_w	varchar2(15);
nr_atendimento_w	number(10);
ds_observacao_w		varchar2(255);


cursor c01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento		= cd_estabelecimento_w
	and	ie_evento_disp			= 'EAN'
	and	nvl(qt_idade_w,0) between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_sexo,nvl(ie_sexo_w,'XPTO'))  = NVL(ie_sexo_w,'XPTO')
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and 	(obter_se_convenio_rec_alerta(cd_convenio_w,nr_sequencia) = 'S')
	and	(obter_se_proc_rec_alerta(nr_seq_proc_interno_w,nr_sequencia,cd_procedimento_w,ie_origem_proced_w) = 'S')
	and	(obter_se_regra_envio(nr_sequencia,nr_atendimento_w) = 'S')
	and	(obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	nvl(ie_situacao,'A') = 'A';
	--and	(obter_se_mat_rec_alerta(cd_material_w,nr_sequencia) = 'S')

begin

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
select	cd_pessoa_fisica,
	nvl(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),nvl(qt_idade_paciente,0)),
	Obter_Sexo_PF(cd_pessoa_fisica,'C'),
	cd_medico,
	cd_convenio,
	nr_seq_proc_interno,
	cd_agenda,
	hr_inicio,
	cd_procedimento,
	ie_origem_proced,
	dt_cancelamento,
	cd_motivo_cancelamento,
	nr_atendimento,
	substr(ds_observacao,1,255)
into	cd_pessoa_fisica_w,
	qt_idade_w,
	ie_sexo_w,
	cd_medico_w,
	cd_convenio_w,
	nr_seq_proc_interno_w,
	cd_agenda_w,
	hr_inicio_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	dt_cancelamento_w,
	cd_motivo_cancel_w,
	nr_atendimento_w,
	ds_observacao_w
from	agenda_paciente
where	nr_sequencia = :old.nr_seq_agenda;



open c01;
loop
fetch c01 into
	nr_seq_evento_w;
exit when c01%notfound;
	begin
	gerar_evento_agenda_trigger(	nr_seq_evento_w,
					null,
					cd_pessoa_fisica_w,
					null,
					:old.nm_usuario,
					cd_agenda_w,
					hr_inicio_w,
					cd_medico_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					dt_cancelamento_w,
					null,
					null,
					null,
					cd_convenio_w,
					cd_motivo_cancel_w,
					'S',
					null,
					null,
					null,
					null,
					null,
					ds_observacao_w);
	end;
end loop;
close c01;

exception
when others then
	cd_estabelecimento_w	:= 0;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.ANEXO_AGENDA_tp  after update ON TASY.ANEXO_AGENDA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_DOCUMENTO,1,4000),substr(:new.NR_SEQ_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DOCUMENTO',ie_log_w,ds_w,'ANEXO_AGENDA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ANEXO_AGENDA ADD (
  CONSTRAINT ANEXAGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ANEXO_AGENDA ADD (
  CONSTRAINT ANEXAGE_TIANAGE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ANEXO) 
 REFERENCES TASY.TIPO_ANEXO_AGENDA (NR_SEQUENCIA),
  CONSTRAINT ANEXAGE_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA_ESPERA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ANEXAGE_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ANEXAGE_STANAGE_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.STATUS_ANEXO_AGENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ANEXO_AGENDA TO NIVEL_1;

