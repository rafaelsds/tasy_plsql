ALTER TABLE TASY.PEP_MED_FATUR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEP_MED_FATUR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEP_MED_FATUR
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_MEDICO                  VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_FATURAMENTO             DATE               NOT NULL,
  NR_DOC_CONVENIO            VARCHAR2(20 BYTE),
  IE_FUNCAO_MEDICO           VARCHAR2(10 BYTE),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  CD_CNPJ_EXEC               VARCHAR2(14 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_REGRA_COBRANCA      NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONTA_TERCEIRO      NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPMEFA_ATEPACI_FK_I ON TASY.PEP_MED_FATUR
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPMEFA_CONTTER_FK_I ON TASY.PEP_MED_FATUR
(NR_SEQ_CONTA_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_CONTTER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_ESTABEL_FK_I ON TASY.PEP_MED_FATUR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_I1 ON TASY.PEP_MED_FATUR
(DT_FATURAMENTO, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_I1
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_PEPREGO_FK_I ON TASY.PEP_MED_FATUR
(NR_SEQ_REGRA_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_PEPREGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_PERFIL_FK_I ON TASY.PEP_MED_FATUR
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_PESFISI_FK2_I ON TASY.PEP_MED_FATUR
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPMEFA_PESJURI_FK_I ON TASY.PEP_MED_FATUR
(CD_CNPJ_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEPMEFA_PK ON TASY.PEP_MED_FATUR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPMEFA_TASASDI_FK_I ON TASY.PEP_MED_FATUR
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPMEFA_TASASDI_FK2_I ON TASY.PEP_MED_FATUR
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPMEFA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PEP_MED_FATUR_ATUAL
before insert or update ON TASY.PEP_MED_FATUR for each row
declare


begin
if	(nvl(:old.DT_FATURAMENTO,sysdate+10) <> :new.DT_FATURAMENTO) and
	(:new.DT_FATURAMENTO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_FATURAMENTO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_MED_FATUR_AFTER_INSERT
after insert or update or delete ON TASY.PEP_MED_FATUR for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_libera_partic_w	varchar2(10);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);
ie_lib_fatur_med_w	varchar2(15);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (inserting) or (updating) then
	select	max(ie_lib_fatur_med)
	into	ie_lib_fatur_med_w
	from	parametro_medico
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;


	if	(nvl(ie_lib_fatur_med_w,'N') = 'S') then
		select	max(c.nr_atendimento),
					max(c.cd_pessoa_fisica)
		into		nr_atendimento_w,
					cd_pessoa_fisica_w
		from		atendimento_paciente c
		where		c.nr_atendimento = :new.nr_atendimento;

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'LFM';
		elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
			ie_tipo_w := 'XLFM';
		end if;

		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
	end if;
elsif (deleting) then
	begin
	delete	pep_item_pendente
	where	nr_seq_registro = :old.nr_sequencia
	and	nvl(ie_tipo_pendencia,'L') = 'L'
	and	ie_tipo_registro = 'LFM';

	commit;
	exception
	when others then
		null;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PEP_MED_FATUR ADD (
  CONSTRAINT PEPMEFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEP_MED_FATUR ADD (
  CONSTRAINT PEPMEFA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PEPMEFA_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPMEFA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PEPMEFA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_EXEC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PEPMEFA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PEPMEFA_PEPREGO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_COBRANCA) 
 REFERENCES TASY.PEP_REGRA_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT PEPMEFA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPMEFA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPMEFA_CONTTER_FK 
 FOREIGN KEY (NR_SEQ_CONTA_TERCEIRO) 
 REFERENCES TASY.CONTA_TERCEIRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEP_MED_FATUR TO NIVEL_1;

