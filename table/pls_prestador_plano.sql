ALTER TABLE TASY.PLS_PRESTADOR_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_PLANO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR       NUMBER(10)             NOT NULL,
  NR_SEQ_PLANO           NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_PERMITE             VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA     DATE,
  DT_FIM_VIGENCIA        DATE,
  IE_TIPO_ATENDIMENTO    VARCHAR2(1 BYTE)       NOT NULL,
  IE_CARATER_INTERNACAO  VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRPL_PK ON TASY.PLS_PRESTADOR_PLANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPL_PLSPLAN_FK_I ON TASY.PLS_PRESTADOR_PLANO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPL_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_PLANO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_PLANO_tp  after update ON TASY.PLS_PRESTADOR_PLANO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR_PLANO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRESTADOR_PLANO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PLANO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PLANO,1,4000),substr(:new.NR_SEQ_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PLANO',ie_log_w,ds_w,'PLS_PRESTADOR_PLANO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_plano_delete
after delete ON TASY.PLS_PRESTADOR_PLANO for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_PLANO',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_plano_atual
before insert or update ON TASY.PLS_PRESTADOR_PLANO for each row
declare
cd_estabelecimento_plano_w      pls_plano.cd_estabelecimento%type;
cd_estabelecimento_prestador_w  pls_prestador.cd_estabelecimento%type;

begin
if	(:new.nr_seq_plano is not null) then
	select	max(a.cd_estabelecimento)
	into	cd_estabelecimento_plano_w
	from	pls_plano a
	where	a.nr_sequencia = :new.nr_seq_plano;
end if;

if	(:new.nr_seq_prestador is not null) then
	select  max(b.cd_estabelecimento)
	into    cd_estabelecimento_prestador_w
	from    pls_prestador b
	where   b.nr_sequencia = :new.nr_seq_prestador;
end if;

if      (cd_estabelecimento_plano_w != cd_estabelecimento_prestador_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(1063764);
end if;

end pls_prestador_plano_atual;
/


ALTER TABLE TASY.PLS_PRESTADOR_PLANO ADD (
  CONSTRAINT PLSPRPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_PLANO ADD (
  CONSTRAINT PLSPRPL_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPL_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_PLANO TO NIVEL_1;

