ALTER TABLE TASY.ES_NGTB_COBFATURATASY_COPI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ES_NGTB_COBFATURATASY_COPI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ES_NGTB_COBFATURATASY_COPI
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  ID_COPI_CD_SEQUENCIAL     NUMBER(10)          NOT NULL,
  COPI_DS_CODIGOCLIENTE     VARCHAR2(20 BYTE)   NOT NULL,
  COPI_NR_FATURA            VARCHAR2(20 BYTE)   NOT NULL,
  COPI_NR_FATURATASY        VARCHAR2(32 BYTE)   NOT NULL,
  COPI_DH_VENCIMENTO        DATE                NOT NULL,
  COPI_DH_EMISSAO           DATE                NOT NULL,
  COPI_DS_COMPETENCIA       VARCHAR2(7 BYTE),
  COPI_DS_STATUS            VARCHAR2(1 BYTE)    NOT NULL,
  COPI_VL_FATURA            NUMBER(11,2)        NOT NULL,
  COPI_VL_RECEBIDO          NUMBER(11,2),
  COPI_VL_RECEBIDOJUROS     NUMBER(11,2),
  COPI_VL_RECEBIDOMULTA     NUMBER(11,2),
  COPI_VL_RECEBIDOIMPOSTO   NUMBER(11,2),
  COPI_VL_DESCONTO          NUMBER(11,2),
  COPI_DH_RECEBIMENTO       DATE,
  COPI_DS_TIPOBAIXA         VARCHAR2(40 BYTE),
  COPI_DH_PRORROGACAO       DATE,
  COPI_DS_NOSSONUMERO       VARCHAR2(20 BYTE),
  COPI_DS_TIPOFATURA        VARCHAR2(40 BYTE)   NOT NULL,
  COPI_DH_CANCELAMENTO      DATE,
  COPI_DS_CENTROCUSTO       VARCHAR2(100 BYTE)  NOT NULL,
  COPI_DH_REGISTROTASY      DATE                NOT NULL,
  COPI_DH_LEITURAPLUSOFT    DATE,
  COPI_DS_STATUSINTEGRACAO  VARCHAR2(1 BYTE),
  COPI_DS_MSGERRO           VARCHAR2(500 BYTE),
  COPI_DS_TIPOCOBRANCA      VARCHAR2(40 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESCOBFAT_PK ON TASY.ES_NGTB_COBFATURATASY_COPI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ES_NGTB_COBFATURATASY_UPDATE
before update ON TASY.ES_NGTB_COBFATURATASY_COPI for each row
declare

ds_log_w	varchar2(4000);

begin

if	(:old.copi_dh_cancelamento <> :new.copi_dh_cancelamento) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_cancelamento' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_cancelamento,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_cancelamento,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_emissao <> :new.copi_dh_emissao) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_emissao' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_emissao,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_emissao,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_leituraplusoft <> :new.copi_dh_leituraplusoft) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_leituraplusoft' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_leituraplusoft,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_leituraplusoft,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_prorrogacao <> :new.copi_dh_prorrogacao) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_prorrogacao' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_prorrogacao,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_prorrogacao,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_recebimento <> :new.copi_dh_recebimento) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_recebimento' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_recebimento,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_recebimento,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_registrotasy <> :new.copi_dh_registrotasy) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_registrotasy' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_registrotasy,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_registrotasy,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_dh_vencimento <> :new.copi_dh_vencimento) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_dh_vencimento' || ';' || 'VL_ANTIGO=' || to_char(:old.copi_dh_vencimento,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.copi_dh_vencimento,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_centrocusto <> :new.copi_ds_centrocusto) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_centrocusto' || ';' || 'VL_ANTIGO=' || :old.copi_ds_centrocusto || ';' || 'VL_NOVO=' || :new.copi_ds_centrocusto) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_codigocliente <> :new.copi_ds_codigocliente) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_codigocliente' || ';' || 'VL_ANTIGO=' || :old.copi_ds_codigocliente || ';' || 'VL_NOVO=' || :new.copi_ds_codigocliente) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_competencia <> :new.copi_ds_competencia) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_competencia' || ';' || 'VL_ANTIGO=' || :old.copi_ds_competencia || ';' || 'VL_NOVO=' || :new.copi_ds_competencia) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_msgerro <> :new.copi_ds_msgerro) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_msgerro' || ';' || 'VL_ANTIGO=' || :old.copi_ds_msgerro || ';' || 'VL_NOVO=' || :new.copi_ds_msgerro) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_nossonumero <> :new.copi_ds_nossonumero) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_nossonumero' || ';' || 'VL_ANTIGO=' || :old.copi_ds_nossonumero || ';' || 'VL_NOVO=' || :new.copi_ds_nossonumero) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_status <> :new.copi_ds_status) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_status' || ';' || 'VL_ANTIGO=' || :old.copi_ds_status || ';' || 'VL_NOVO=' || :new.copi_ds_status) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_statusintegracao <> :new.copi_ds_statusintegracao) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_statusintegracao' || ';' || 'VL_ANTIGO=' || :old.copi_ds_statusintegracao || ';' || 'VL_NOVO=' || :new.copi_ds_statusintegracao) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_tipobaixa <> :new.copi_ds_tipobaixa) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_tipobaixa' || ';' || 'VL_ANTIGO=' || :old.copi_ds_tipobaixa || ';' || 'VL_NOVO=' || :new.copi_ds_tipobaixa) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_ds_tipofatura <> :new.copi_ds_tipofatura) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_ds_tipofatura' || ';' || 'VL_ANTIGO=' || :old.copi_ds_tipofatura || ';' || 'VL_NOVO=' || :new.copi_ds_tipofatura) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_nr_fatura <> :new.copi_nr_fatura) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_nr_fatura' || ';' || 'VL_ANTIGO=' || :old.copi_nr_fatura || ';' || 'VL_NOVO=' || :new.copi_nr_fatura) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_nr_faturatasy <> :new.copi_nr_faturatasy) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_nr_faturatasy' || ';' || 'VL_ANTIGO=' || :old.copi_nr_faturatasy || ';' || 'VL_NOVO=' || :new.copi_nr_faturatasy) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_desconto <> :new.copi_vl_desconto) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_desconto' || ';' || 'VL_ANTIGO=' || :old.copi_vl_desconto || ';' || 'VL_NOVO=' || :new.copi_vl_desconto) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_fatura <> :new.copi_vl_fatura) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_fatura' || ';' || 'VL_ANTIGO=' || :old.copi_vl_fatura || ';' || 'VL_NOVO=' || :new.copi_vl_fatura) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_recebido <> :new.copi_vl_recebido) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_recebido' || ';' || 'VL_ANTIGO=' || :old.copi_vl_recebido || ';' || 'VL_NOVO=' || :new.copi_vl_recebido) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_recebidoimposto <> :new.copi_vl_recebidoimposto) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_recebidoimposto' || ';' || 'VL_ANTIGO=' || :old.copi_vl_recebidoimposto || ';' || 'VL_NOVO=' || :new.copi_vl_recebidoimposto) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_recebidojuros <> :new.copi_vl_recebidojuros) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_recebidojuros' || ';' || 'VL_ANTIGO=' || :old.copi_vl_recebidojuros || ';' || 'VL_NOVO=' || :new.copi_vl_recebidojuros) || chr(13) || chr(10),1,4000);
end if;

if	(:old.copi_vl_recebidomulta <> :new.copi_vl_recebidomulta) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=copi_vl_recebidomulta' || ';' || 'VL_ANTIGO=' || :old.copi_vl_recebidomulta || ';' || 'VL_NOVO=' || :new.copi_vl_recebidomulta) || chr(13) || chr(10),1,4000);
end if;

GERAR_CRM_COBRANCA_LOG(ds_log_w,'ES_NGTB_COBFATURATASY_COPI',:new.nm_usuario);

end;
/


ALTER TABLE TASY.ES_NGTB_COBFATURATASY_COPI ADD (
  CONSTRAINT ESCOBFAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ES_NGTB_COBFATURATASY_COPI TO NIVEL_1;

