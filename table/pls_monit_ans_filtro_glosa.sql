ALTER TABLE TASY.PLS_MONIT_ANS_FILTRO_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONIT_ANS_FILTRO_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONIT_ANS_FILTRO_GLOSA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ANS_FILTRO    NUMBER(10)               NOT NULL,
  CD_GLOSA_TISS        VARCHAR2(10 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSANSGLO_PK ON TASY.PLS_MONIT_ANS_FILTRO_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSANSGLO_PLSLANSF_FK_I ON TASY.PLS_MONIT_ANS_FILTRO_GLOSA
(NR_SEQ_ANS_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_MONIT_ANS_FILTRO_GLOSA_tp  after update ON TASY.PLS_MONIT_ANS_FILTRO_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_GLOSA_TISS,1,4000),substr(:new.CD_GLOSA_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_GLOSA_TISS',ie_log_w,ds_w,'PLS_MONIT_ANS_FILTRO_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MONIT_ANS_FILTRO_GLOSA ADD (
  CONSTRAINT PLSANSGLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONIT_ANS_FILTRO_GLOSA ADD (
  CONSTRAINT PLSANSGLO_PLSLANSF_FK 
 FOREIGN KEY (NR_SEQ_ANS_FILTRO) 
 REFERENCES TASY.PLS_LOTE_ANS_FILTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONIT_ANS_FILTRO_GLOSA TO NIVEL_1;

