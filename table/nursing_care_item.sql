ALTER TABLE TASY.NURSING_CARE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NURSING_CARE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.NURSING_CARE_ITEM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_ITEM                VARCHAR2(80 BYTE)      NOT NULL,
  NR_SEQ_APRES           NUMBER(3)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIFICACAO   VARCHAR2(1 BYTE),
  DS_SQL                 VARCHAR2(4000 BYTE),
  NR_SEQ_ITEM_SUP        NUMBER(10),
  NR_SEQ_NIVEL           NUMBER(1),
  NR_SEQ_VERSAO          NUMBER(10),
  NR_SEQ_RESULT_DEFAULT  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NURCARI_NURCARI_FK_I ON TASY.NURSING_CARE_ITEM
(NR_SEQ_ITEM_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NURCARI_NURCARR_FK_I ON TASY.NURSING_CARE_ITEM
(NR_SEQ_RESULT_DEFAULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NURCARI_NURCARV_FK_I ON TASY.NURSING_CARE_ITEM
(NR_SEQ_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NURCARI_PK ON TASY.NURSING_CARE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NURSING_CARE_ITEM ADD (
  CONSTRAINT NURCARI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NURSING_CARE_ITEM ADD (
  CONSTRAINT NURCARI_NURCARI_FK 
 FOREIGN KEY (NR_SEQ_ITEM_SUP) 
 REFERENCES TASY.NURSING_CARE_ITEM (NR_SEQUENCIA),
  CONSTRAINT NURCARI_NURCARR_FK 
 FOREIGN KEY (NR_SEQ_RESULT_DEFAULT) 
 REFERENCES TASY.NURSING_CARE_RESULT (NR_SEQUENCIA),
  CONSTRAINT NURCARI_NURCARV_FK 
 FOREIGN KEY (NR_SEQ_VERSAO) 
 REFERENCES TASY.NURSING_CARE_VERSION (NR_SEQUENCIA));

