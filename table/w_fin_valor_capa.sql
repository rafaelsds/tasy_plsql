DROP TABLE TASY.W_FIN_VALOR_CAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_FIN_VALOR_CAPA
(
  DS_MACRO        VARCHAR2(60 BYTE)             NOT NULL,
  VL_RESULTADO    NUMBER(18,4)                  NOT NULL,
  NR_SEQ_FORMULA  NUMBER(10),
  DS_SQL          VARCHAR2(4000 BYTE),
  NM_USUARIO      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WFNVLCP_I1 ON TASY.W_FIN_VALOR_CAPA
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFNVLCP_I1
  MONITORING USAGE;


GRANT SELECT ON TASY.W_FIN_VALOR_CAPA TO NIVEL_1;

