ALTER TABLE TASY.PLS_COOPERADO_TIPO_DOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COOPERADO_TIPO_DOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COOPERADO_TIPO_DOC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10)             NOT NULL,
  DT_INICIO_VIGENCIA     DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA        DATE,
  NR_SEQ_COOPERADO       NUMBER(10)             NOT NULL,
  DS_OBSERVACAO          VARCHAR2(400 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCOTD_PK ON TASY.PLS_COOPERADO_TIPO_DOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOTD_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOTD_PLSCOOR_FK_I ON TASY.PLS_COOPERADO_TIPO_DOC
(NR_SEQ_COOPERADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOTD_PLSCOOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOTD_PLSTIDO_FK_I ON TASY.PLS_COOPERADO_TIPO_DOC
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOTD_PLSTIDO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_COOPERADO_TIPO_DOC_tp  after update ON TASY.PLS_COOPERADO_TIPO_DOC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_TIPO_DOCUMENTO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_DOCUMENTO,1,2000),substr(:new.NR_SEQ_TIPO_DOCUMENTO,1,2000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_DOCUMENTO',ie_log_w,ds_w,'PLS_COOPERADO_TIPO_DOC',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_FIM_VIGENCIA,1,500);gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,2000),substr(:new.DT_FIM_VIGENCIA,1,2000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_COOPERADO_TIPO_DOC',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INICIO_VIGENCIA,1,500);gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,2000),substr(:new.DT_INICIO_VIGENCIA,1,2000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_COOPERADO_TIPO_DOC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COOPERADO_TIPO_DOC ADD (
  CONSTRAINT PLSCOTD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COOPERADO_TIPO_DOC ADD (
  CONSTRAINT PLSCOTD_PLSTIDO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.PLS_TIPO_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOTD_PLSCOOR_FK 
 FOREIGN KEY (NR_SEQ_COOPERADO) 
 REFERENCES TASY.PLS_COOPERADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COOPERADO_TIPO_DOC TO NIVEL_1;

