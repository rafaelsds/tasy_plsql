ALTER TABLE TASY.ESCALA_BARROS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_BARROS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_BARROS
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_PERDA_PESO             NUMBER(10)          NOT NULL,
  IE_CONDICAO_CLINICA       NUMBER(3)           NOT NULL,
  IE_AUTO_ESTIMA            VARCHAR2(10 BYTE)   NOT NULL,
  IE_DISPOSICAO_FISICA      VARCHAR2(10 BYTE)   NOT NULL,
  IE_CAPACIDADE_RELACIONAR  VARCHAR2(10 BYTE)   NOT NULL,
  IE_DISP_TRABALHO          VARCHAR2(10 BYTE)   NOT NULL,
  IE_INTERESSE_SEXO         VARCHAR2(10 BYTE)   NOT NULL,
  QT_PONTUACAO              NUMBER(5,2),
  IE_COMPLICACAO            VARCHAR2(3 BYTE),
  IE_REOPERACAO             VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCBARR_ATEPACI_FK_I ON TASY.ESCALA_BARROS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCBARR_PESFISI_FK_I ON TASY.ESCALA_BARROS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCBARR_PK ON TASY.ESCALA_BARROS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCBARR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_baros_tr
before insert or update ON TASY.ESCALA_BARROS for each row
declare
qt_ponto_w	number(3,2) := 0;
qt_reg_w	number(1);

begin
if  (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;


if (:new.ie_auto_estima = '1') then
	qt_ponto_w := -1;
elsif (:new.ie_auto_estima = '2') then
	qt_ponto_w := qt_ponto_w - 0.5;
elsif (:new.ie_auto_estima = '4') then
	qt_ponto_w := qt_ponto_w + 0.5;
elsif (:new.ie_auto_estima = '5') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (:new.ie_disposicao_fisica = '1') then
	qt_ponto_w := qt_ponto_w -0.5;
elsif (:new.ie_disposicao_fisica = '2') then
	qt_ponto_w := qt_ponto_w - 0.25;
elsif (:new.ie_disposicao_fisica = '4') then
	qt_ponto_w := qt_ponto_w + 0.25;
elsif (:new.ie_disposicao_fisica = '5') then
	qt_ponto_w := qt_ponto_w + 0.5;
end if;

if (:new.ie_capacidade_relacionar = '1') then
	qt_ponto_w := qt_ponto_w - 0.5;
elsif (:new.ie_capacidade_relacionar = '2') then
	qt_ponto_w := qt_ponto_w - 0.25;
elsif (:new.ie_capacidade_relacionar = '4') then
	qt_ponto_w := qt_ponto_w + 0.25;
elsif (:new.ie_capacidade_relacionar = '5') then
	qt_ponto_w := qt_ponto_w + 0.5;
end if;

if (:new.ie_disp_trabalho = '1') then
	qt_ponto_w := qt_ponto_w - 0.5;
elsif (:new.ie_disp_trabalho = '2') then
	qt_ponto_w := qt_ponto_w - 0.25;
elsif (:new.ie_disp_trabalho = '4') then
	qt_ponto_w := qt_ponto_w + 0.25;
elsif (:new.ie_disp_trabalho = '5') then
	qt_ponto_w := qt_ponto_w + 0.5;
end if;

if (:new.ie_interesse_sexo = '1') then
	qt_ponto_w := qt_ponto_w - 0.5;
elsif (:new.ie_interesse_sexo = '2') then
	qt_ponto_w := qt_ponto_w - 0.25;
elsif (:new.ie_interesse_sexo = '4') then
	qt_ponto_w := qt_ponto_w + 0.25;
elsif (:new.ie_interesse_sexo = '5') then
 	qt_ponto_w := qt_ponto_w + 0.5;
end if;

if (:new.ie_complicacao = 'ME') then
	qt_ponto_w := qt_ponto_w - 0.2;
elsif (:new.ie_complicacao = 'MA') then
	qt_ponto_w := qt_ponto_w - 1;
end if;

if (:new.ie_reoperacao = 'S') then
	qt_ponto_w := qt_ponto_w - 1;
end if;

qt_ponto_w := qt_ponto_w  + :new.ie_perda_peso;
qt_ponto_w := qt_ponto_w  + :new.ie_condicao_clinica;

:new.qt_pontuacao := qt_ponto_w;

<<Final>>
qt_reg_w := 0;
end escala_baros_tr;
/


ALTER TABLE TASY.ESCALA_BARROS ADD (
  CONSTRAINT ESCBARR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_BARROS ADD (
  CONSTRAINT ESCBARR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCBARR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_BARROS TO NIVEL_1;

