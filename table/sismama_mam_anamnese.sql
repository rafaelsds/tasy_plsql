ALTER TABLE TASY.SISMAMA_MAM_ANAMNESE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SISMAMA_MAM_ANAMNESE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SISMAMA_MAM_ANAMNESE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_SISMAMA         NUMBER(10),
  IE_NODULO_CAROCO       VARCHAR2(1 BYTE),
  IE_RISCO_ELEVADO       VARCHAR2(1 BYTE),
  IE_MAMA_EXAM_PROF      VARCHAR2(1 BYTE),
  IE_ULTIMA_MAMOGRAFIA   VARCHAR2(1 BYTE),
  DT_ULTIMA_MAMOGRAFIA   VARCHAR2(4 BYTE),
  DT_ULTIMA_MAMOGRAFIA2  VARCHAR2(4 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SISMANA_PK ON TASY.SISMAMA_MAM_ANAMNESE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISMANA_SISMATE_FK_I ON TASY.SISMAMA_MAM_ANAMNESE
(NR_SEQ_SISMAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.sismama_mam_anamnese_atual
before insert or update ON TASY.SISMAMA_MAM_ANAMNESE for each row
declare

nr_data_mamo_w	number(10);
nr_atendimento_w	number(10);
dt_nascimento_w		date;

begin

if	(:new.DT_ULTIMA_MAMOGRAFIA	is not null) then
	nr_data_mamo_w	:= somente_numero(:new.DT_ULTIMA_MAMOGRAFIA);

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	SISMAMA_ATENDIMENTO
	where	nr_sequencia	= :new.nr_seq_sismama;

	select	max(a.dt_nascimento)
	into	dt_nascimento_w
	from	atendimento_paciente b,
		pessoa_fisica a
	where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	b.nr_atendimento	= nr_atendimento_w;


	if	(nr_data_mamo_w	> 0) then

		if	((nr_data_mamo_w	> to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate),'yyyy')) or
			(dt_nascimento_w is not null and nr_data_mamo_w	< to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_nascimento_w),'yyyy'))) then
			WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(235879);

		end if;

	end if;


end if;

end;
/


ALTER TABLE TASY.SISMAMA_MAM_ANAMNESE ADD (
  CONSTRAINT SISMANA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SISMAMA_MAM_ANAMNESE ADD (
  CONSTRAINT SISMANA_SISMATE_FK 
 FOREIGN KEY (NR_SEQ_SISMAMA) 
 REFERENCES TASY.SISMAMA_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SISMAMA_MAM_ANAMNESE TO NIVEL_1;

