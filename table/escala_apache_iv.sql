ALTER TABLE TASY.ESCALA_APACHE_IV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_APACHE_IV CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_APACHE_IV
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  QT_TEMP                NUMBER(4,1),
  QT_PA_SIST             NUMBER(15,4),
  QT_PA_DIAST            NUMBER(15,4),
  QT_FC                  NUMBER(15,4),
  QT_FREQ_RESP           NUMBER(15,4),
  QT_ALTITUDE            NUMBER(15,2),
  QT_FIO2                NUMBER(3),
  QT_PH                  NUMBER(15,4),
  QT_PO2                 NUMBER(15,4),
  QT_PCO2                NUMBER(15,4),
  QT_NA                  NUMBER(15,4),
  QT_GLICOSE             NUMBER(15,4),
  QT_CREATININA          NUMBER(6,2),
  QT_UREIA               NUMBER(17,4),
  QT_DIURESE             NUMBER(6,2),
  QT_ALBUMINA            NUMBER(15,2),
  QT_BILIRRUBINA         NUMBER(15,2),
  QT_GLOB_VERMELHOS      NUMBER(15,3),
  QT_GLOB_BRANCOS        NUMBER(15,3),
  IE_GLASGOW_INDIS       VARCHAR2(2 BYTE),
  IE_GLASGOW_OCULAR      NUMBER(1),
  IE_GLASGOW_VERBAL      NUMBER(1),
  IE_GLASGOW_MOTORA      NUMBER(1),
  QT_ESCALA_GLASGOW      NUMBER(2),
  QT_IDADE               NUMBER(3),
  IE_RENAL               VARCHAR2(1 BYTE),
  IE_AIDS                VARCHAR2(1 BYTE),
  IE_HEPATICA            VARCHAR2(1 BYTE),
  IE_LINFOMA             VARCHAR2(1 BYTE),
  IE_CANCER_METASTATICO  VARCHAR2(1 BYTE),
  IE_LEUC_MILEOMA_MULT   VARCHAR2(1 BYTE),
  IE_IMUNOSUPRESSAO      VARCHAR2(1 BYTE),
  IE_CIRROSE             VARCHAR2(1 BYTE),
  IE_ORIGEM_PACIENTE     NUMBER(1),
  QT_DIAS_ANTES_UTI      NUMBER(3),
  IE_EMERGENCIA          VARCHAR2(1 BYTE),
  IE_READMISSAO          VARCHAR2(1 BYTE),
  IE_VENTILACAO          VARCHAR2(1 BYTE),
  NR_SEQ_DIAG            NUMBER(10),
  IE_TERAPIA_TROMBO      VARCHAR2(1 BYTE),
  QT_PONTUACAO           NUMBER(5),
  QT_PONTOS_APS          NUMBER(5),
  QT_PONTOS_LOGIT        NUMBER(15,7),
  PR_MORTALIDADE         NUMBER(4,2),
  QT_DIAS_UTI            NUMBER(5,2),
  QT_TEMP_MAX            NUMBER(4,1),
  QT_PA_SIST_MAX         NUMBER(15,4),
  QT_PA_DIAST_MAX        NUMBER(15,4),
  QT_FC_MAX              NUMBER(15,4),
  QT_FREQ_RESP_MAX       NUMBER(15,4),
  QT_NA_MAX              NUMBER(15,4),
  QT_GLICOSE_MAX         NUMBER(15,4),
  QT_CREATININA_MAX      NUMBER(6,2),
  QT_UREIA_MAX           NUMBER(17,4),
  QT_GLOB_VERMELHOS_MAX  NUMBER(15,3),
  QT_GLOB_BRANCOS_MAX    NUMBER(15,3),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAPIV_ATEPACI_FK_I ON TASY.ESCALA_APACHE_IV
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAPIV_DIDOAPI_FK_I ON TASY.ESCALA_APACHE_IV
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAPIV_DIDOAPI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAPIV_I1 ON TASY.ESCALA_APACHE_IV
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAPIV_PESFISI_FK_I ON TASY.ESCALA_APACHE_IV
(CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAPIV_PK ON TASY.ESCALA_APACHE_IV
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAPIV_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_apache_IV_atual
before insert or update ON TASY.ESCALA_APACHE_IV for each row
declare

qt_var_w							number;
qt_pressao_atmosferica_w			number(15,2);
qt_pt_temperatura_w					number(2,0);
qt_pt_temperatura_min_w				number(2,0);
qt_pt_temperatura_max_w				number(2,0);
qt_pt_pressao_arterial_w			number(2,0);
qt_pt_pressao_arterial_min_w		number(2,0);
qt_pt_pressao_arterial_max_w		number(2,0);
qt_pt_fc_w							number(2,0);
qt_pt_fc_min_w						number(2,0);
qt_pt_fc_max_w						number(2,0);
qt_pt_freq_resp_w					number(2,0);
qt_pt_freq_resp_min_w				number(2,0);
qt_pt_freq_resp_max_w				number(2,0);
qt_pt_po2_w							number(2,0);
qt_pt_ph_w							number(2,0);
qt_pt_na_w							number(2,0);
qt_pt_na_min_w						number(2,0);
qt_pt_na_max_w						number(2,0);
qt_pt_creatinina_w					number(2,0);
qt_pt_creatinina_min_w				number(2,0);
qt_pt_creatinina_max_w				number(2,0);
qt_pt_diurese_w						number(2,0);
qt_pt_ureia_w						number(2,0);
qt_pt_ureia_min_w					number(2,0);
qt_pt_ureia_max_w					number(2,0);
qt_pt_albumina_w					number(2,0);
qt_pt_bilirrubina_w					number(2,0);
qt_pt_glicose_w						number(2,0);
qt_pt_glicose_min_w					number(2,0);
qt_pt_glicose_max_w					number(2,0);
qt_pt_glob_vermelhos_w				number(2,0);
qt_pt_glob_vermelhos_min_w			number(2,0);
qt_pt_glob_vermelhos_max_w			number(2,0);
qt_pt_glob_brancos_w				number(2,0);
qt_pt_glob_brancos_min_w			number(2,0);
qt_pt_glob_brancos_max_w			number(2,0);
qt_pt_glasgow_w						number(2,0) := 0;
qt_pt_idade_w						number(2,0);
qt_pt_doencas_cronicas_w			number(2,0);
qt_pt_idade1_w						number;
qt_pt_idade2_w						number;
qt_pt_idade3_w						number;
qt_pt_idade4_w						number;
qt_pt_idade5_w						number;
qt_pt_aps1_w						number;
qt_pt_aps2_w						number;
qt_pt_aps3_w						number;
qt_pt_aps4_w						number;
qt_pt_aps5_w						number;
qt_logit_w							number;
qt_pt_diag							number;
qt_los_w							number := 0;
qt_los1_w							number := 0;
qt_los2_w							number := 0;
qt_los3_w							number := 0;
qt_los4_w							number := 0;
qt_reg_w							number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.qt_ph is null) or (:new.qt_ph = 0) then
	:new.qt_ph	:=	7.4; -- apacheIV.js: alert("You left PH field empty, if no ABG's is available a normal value will be assigned automatically");
end if;

if	(:new.qt_pco2 is null) or (:new.qt_pco2 = 0) then
	:new.qt_pco2	:=	40; -- 5.3 se for International Units (SI). / apacheIV.js: alert("You left PCO2 field empty, if no ABG's is available a normal value will be assigned automatically");
end if;

-- temperatura
if		(:new.qt_temp	<=	32.9) then
	qt_pt_temperatura_min_w	:=	20;
elsif	(:new.qt_temp	<=	33.4) then
	qt_pt_temperatura_min_w	:=	16;
elsif	(:new.qt_temp	<=	33.9) then
	qt_pt_temperatura_min_w	:=	13;
elsif	(:new.qt_temp	<=	34.9) then
	qt_pt_temperatura_min_w	:=	8;
elsif	(:new.qt_temp	<=	35.9) then
	qt_pt_temperatura_min_w	:=	2;
elsif	(:new.qt_temp	<=	39.9) then
	qt_pt_temperatura_min_w	:=	0;
else
	qt_pt_temperatura_min_w	:=	4;
end if;

if		(:new.qt_temp_max	<=	32.9) then
	qt_pt_temperatura_max_w	:=	20;
elsif	(:new.qt_temp_max	<=	33.4) then
	qt_pt_temperatura_max_w	:=	16;
elsif	(:new.qt_temp_max	<=	33.9) then
	qt_pt_temperatura_max_w	:=	13;
elsif	(:new.qt_temp_max	<=	34.9) then
	qt_pt_temperatura_max_w	:=	8;
elsif	(:new.qt_temp_max	<=	35.9) then
	qt_pt_temperatura_max_w	:=	2;
elsif	(:new.qt_temp_max	<=	39.9) then
	qt_pt_temperatura_max_w	:=	0;
else
	qt_pt_temperatura_max_w	:=	4;
end if;

if (qt_pt_temperatura_max_w > qt_pt_temperatura_min_w) then
	qt_pt_temperatura_w	:=	qt_pt_temperatura_max_w;
else
	qt_pt_temperatura_w	:=	qt_pt_temperatura_min_w;
end if;

-- press�o arterial
qt_var_w	:=	dividir((:new.qt_pa_diast * 2 + :new.qt_pa_sist),3);
if 		(qt_var_w	<=	39) then
	qt_pt_pressao_arterial_min_w	:=	23;
elsif	(qt_var_w	<=	59) then
	qt_pt_pressao_arterial_min_w	:=	15;
elsif	(qt_var_w	<=	69) then
	qt_pt_pressao_arterial_min_w	:=	7;
elsif	(qt_var_w	<=	79) then
	qt_pt_pressao_arterial_min_w	:=	6;
elsif	(qt_var_w	<=	99) then
	qt_pt_pressao_arterial_min_w	:=	0;
elsif	(qt_var_w	<=	119) then
	qt_pt_pressao_arterial_min_w	:=	4;
elsif	(qt_var_w	<=	129) then
	qt_pt_pressao_arterial_min_w	:=	7;
elsif	(qt_var_w	<=	139) then
	qt_pt_pressao_arterial_min_w	:=	9;
else
	qt_pt_pressao_arterial_min_w	:=	10;
end if;

qt_var_w	:=	dividir((:new.qt_pa_diast_max * 2 + :new.qt_pa_sist_max),3);
if 		(qt_var_w	<=	39) then
	qt_pt_pressao_arterial_max_w	:=	23;
elsif	(qt_var_w	<=	59) then
	qt_pt_pressao_arterial_max_w	:=	15;
elsif	(qt_var_w	<=	69) then
	qt_pt_pressao_arterial_max_w	:=	7;
elsif	(qt_var_w	<=	79) then
	qt_pt_pressao_arterial_max_w	:=	6;
elsif	(qt_var_w	<=	99) then
	qt_pt_pressao_arterial_max_w	:=	0;
elsif	(qt_var_w	<=	119) then
	qt_pt_pressao_arterial_max_w	:=	4;
elsif	(qt_var_w	<=	129) then
	qt_pt_pressao_arterial_max_w	:=	7;
elsif	(qt_var_w	<=	139) then
	qt_pt_pressao_arterial_max_w	:=	9;
else
	qt_pt_pressao_arterial_max_w	:=	10;
end if;

if (qt_pt_pressao_arterial_max_w > qt_pt_pressao_arterial_min_w) then
	qt_pt_pressao_arterial_w	:=	qt_pt_pressao_arterial_max_w;
else
	qt_pt_pressao_arterial_w	:=	qt_pt_pressao_arterial_min_w;
end if;

-- freq��ncia card�aca
if		(:new.qt_fc	<=	39) then
	qt_pt_fc_min_w	:=	8;
elsif	(:new.qt_fc	<=	49) then
	qt_pt_fc_min_w	:=	5;
elsif	(:new.qt_fc	<=	99) then
	qt_pt_fc_min_w	:=	0;
elsif	(:new.qt_fc	<=	109) then
	qt_pt_fc_min_w	:=	1;
elsif	(:new.qt_fc	<=	119) then
	qt_pt_fc_min_w	:=	5;
elsif	(:new.qt_fc	<=	139) then
	qt_pt_fc_min_w	:=	7;
elsif	(:new.qt_fc	<=	154) then
	qt_pt_fc_min_w	:=	13;
else
	qt_pt_fc_min_w	:=	17;
end if;

if		(:new.qt_fc_max	<=	39) then
	qt_pt_fc_max_w	:=	8;
elsif	(:new.qt_fc_max	<=	49) then
	qt_pt_fc_max_w	:=	5;
elsif	(:new.qt_fc_max	<=	99) then
	qt_pt_fc_max_w	:=	0;
elsif	(:new.qt_fc_max	<=	109) then
	qt_pt_fc_max_w	:=	1;
elsif	(:new.qt_fc_max	<=	119) then
	qt_pt_fc_max_w	:=	5;
elsif	(:new.qt_fc_max	<=	139) then
	qt_pt_fc_max_w	:=	7;
elsif	(:new.qt_fc_max	<=	154) then
	qt_pt_fc_max_w	:=	13;
else
	qt_pt_fc_max_w	:=	17;
end if;

if (qt_pt_fc_max_w > qt_pt_fc_min_w) then
	qt_pt_fc_w	:=	qt_pt_fc_max_w;
else
	qt_pt_fc_w	:=	qt_pt_fc_min_w;
end if;

-- freq��ncia respirat�ria
if		(:new.qt_freq_resp	<=	5) then
	qt_pt_freq_resp_min_w	:=	17;
elsif	(:new.qt_freq_resp	<=	11) then
	qt_pt_freq_resp_min_w	:=	8;
elsif	(:new.qt_freq_resp	<=	13) then
	qt_pt_freq_resp_min_w	:=	7;
elsif	(:new.qt_freq_resp	<=	24) then
	qt_pt_freq_resp_min_w	:=	0;
elsif	(:new.qt_freq_resp	<=	34) then
	qt_pt_freq_resp_min_w	:=	6;
elsif	(:new.qt_freq_resp	<=	39) then
	qt_pt_freq_resp_min_w	:=	9;
elsif	(:new.qt_freq_resp	<=	49) then
	qt_pt_freq_resp_min_w	:=	11;
else
	qt_pt_freq_resp_min_w	:=	18;
end if;

if		(:new.qt_freq_resp_max	<=	5) then
	qt_pt_freq_resp_max_w	:=	17;
elsif	(:new.qt_freq_resp_max	<=	11) then
	qt_pt_freq_resp_max_w	:=	8;
elsif	(:new.qt_freq_resp_max	<=	13) then
	qt_pt_freq_resp_max_w	:=	7;
elsif	(:new.qt_freq_resp_max	<=	24) then
	qt_pt_freq_resp_max_w	:=	0;
elsif	(:new.qt_freq_resp_max	<=	34) then
	qt_pt_freq_resp_max_w	:=	6;
elsif	(:new.qt_freq_resp_max	<=	39) then
	qt_pt_freq_resp_max_w	:=	9;
elsif	(:new.qt_freq_resp_max	<=	49) then
	qt_pt_freq_resp_max_w	:=	11;
else
	qt_pt_freq_resp_max_w	:=	18;
end if;

if (qt_pt_freq_resp_max_w > qt_pt_freq_resp_min_w) then
	qt_pt_freq_resp_w	:=	qt_pt_freq_resp_max_w;
else
	qt_pt_freq_resp_w	:=	qt_pt_freq_resp_min_w;
end if;

-- Oxigena��o
qt_pressao_atmosferica_w	:=	round(760 * exp(dividir(:new.qt_altitude,7924)),2); -- Converte metros para mmHg;
if ((:new.qt_fio2	>=	50)	or	(:new.ie_ventilacao	=	'S')) then
	qt_var_w	:=	(:new.qt_fio2/100 * (qt_pressao_atmosferica_w - 47)) - dividir(:new.qt_pco2,0.8) - :new.qt_po2;
	qt_var_w	:=	round(qt_var_w,2);
	if		(qt_var_w	<	100) then
		qt_pt_po2_w	:=	0;
	elsif	(qt_var_w	<=	249) then
		qt_pt_po2_w	:=	7;
	elsif	(qt_var_w	<=	349) then
		qt_pt_po2_w	:=	9;
	elsif	(qt_var_w	<=	499) then
		qt_pt_po2_w	:=	11;
	else
		qt_pt_po2_w	:=	14;
	end if;
else
	if		(:new.qt_po2	<=	49) then
		qt_pt_po2_w	:=	15;
	elsif	(:new.qt_po2	<=	69) then
		qt_pt_po2_w	:=	5;
	elsif	(:new.qt_po2	<=	79) then
		qt_pt_po2_w	:=	2;
	else
		qt_pt_po2_w	:=	0;
	end if;
end if;

-- PH
if		(:new.qt_ph	<	7.2) then
	if	(:new.qt_pco2	<	50) then
		qt_pt_ph_w	:=	12;
	else
		qt_pt_ph_w	:=	4;
	end if;
elsif	(:new.qt_ph	<	7.3) then
	if		(:new.qt_pco2	<	30) then
		qt_pt_ph_w	:=	9;
	elsif	(:new.qt_pco2	<	40) then
		qt_pt_ph_w	:=	6;
	elsif	(:new.qt_pco2	<	50) then
		qt_pt_ph_w	:=	3;
	else
		qt_pt_ph_w	:=	2;
	end if;
elsif	(:new.qt_ph	<	7.35) then
	if		(:new.qt_pco2	<	30) then
		qt_pt_ph_w	:=	9;
	elsif	(:new.qt_pco2	<	45) then
		qt_pt_ph_w	:=	0;
	else
		qt_pt_ph_w	:=	1;
	end if;
elsif	(:new.qt_ph	<	7.45) then
	if 		(:new.qt_pco2	<	30) then
		qt_pt_ph_w	:=	5;
	elsif	(:new.qt_pco2	<	45) then
		qt_pt_ph_w	:=	0;
	else
		qt_pt_ph_w	:=	1;
	end if;
elsif	(:new.qt_ph	<	7.5) then
	if		(:new.qt_pco2	<	30) then
		qt_pt_ph_w	:=	5;
	elsif	(:new.qt_pco2	<	35) then
		qt_pt_ph_w	:=	0;
	elsif	(:new.qt_pco2	<	45) then
		qt_pt_ph_w	:=	2;
	else
		qt_pt_ph_w	:=	12;
	end if;
elsif	(:new.qt_ph	<	7.6) then
	if (:new.qt_pco2	<	40) then
		qt_pt_ph_w	:=	3;
	else
		qt_pt_ph_w	:=	12;
	end if;
else
	if		(:new.qt_pco2	<	25) then
		qt_pt_ph_w	:=	0;
	elsif	(:new.qt_pco2	<	40) then
		qt_pt_ph_w	:=	3;
	else
		qt_pt_ph_w	:=	12;
	end if;
end if;

-- Sodium
if		(:new.qt_na	<=	119) then
	qt_pt_na_min_w	:= 3;
elsif	(:new.qt_na	<=	134) then
	qt_pt_na_min_w	:=	2;
elsif	(:new.qt_na	<=	154) then
	qt_pt_na_min_w	:=	0;
else
	qt_pt_na_min_w	:=	4;
end if;

if		(:new.qt_na_max	<=	119) then
	qt_pt_na_max_w	:= 3;
elsif	(:new.qt_na_max	<=	134) then
	qt_pt_na_max_w	:=	2;
elsif	(:new.qt_na_max	<=	154) then
	qt_pt_na_max_w	:=	0;
else
	qt_pt_na_max_w	:=	4;
end if;

if (qt_pt_na_max_w > qt_pt_na_min_w) then
	qt_pt_na_w	:=	qt_pt_na_max_w;
else
	qt_pt_na_w	:=	qt_pt_na_min_w;
end if;

-- Creatinina COM Acute Renal Failure
if	((:new.ie_renal = 'N') or (:new.ie_renal is null)) and
	(:new.qt_diurese	<	410) and
	(:new.qt_creatinina	>=	1.5) then
	qt_pt_creatinina_w	:=	10;
else
-- Creatinina SEM Acute Renal Failure
	if		(:new.qt_creatinina	<=	0.4) then
		qt_pt_creatinina_min_w	:=	3;
	elsif	(:new.qt_creatinina	<=	1.4) then
		qt_pt_creatinina_min_w	:=	0;
	elsif	(:new.qt_creatinina	<=	1.94) then
		qt_pt_creatinina_min_w	:=	4;
	else
		qt_pt_creatinina_min_w	:=	7;
	end if;

	if		(:new.qt_creatinina_max	<=	0.4) then
		qt_pt_creatinina_max_w	:=	3;
	elsif	(:new.qt_creatinina_max	<=	1.4) then
		qt_pt_creatinina_max_w	:=	0;
	elsif	(:new.qt_creatinina_max	<=	1.94) then
		qt_pt_creatinina_max_w	:=	4;
	else
		qt_pt_creatinina_max_w	:=	7;
	end if;

	if (qt_pt_creatinina_max_w > qt_pt_creatinina_min_w) then
		qt_pt_creatinina_w	:=	qt_pt_creatinina_max_w;
	else
		qt_pt_creatinina_w	:=	qt_pt_creatinina_min_w;
	end if;
end if;

-- Diurese
if		(:new.qt_diurese	<=	399) then
	qt_pt_diurese_w	:=	15;
elsif	(:new.qt_diurese	<=	599) then
	qt_pt_diurese_w	:=	8;
elsif	(:new.qt_diurese	<=	899) then
	qt_pt_diurese_w	:=	7;
elsif	(:new.qt_diurese	<=	1499) then
	qt_pt_diurese_w	:=	5;
elsif	(:new.qt_diurese	<=	1999) then
	qt_pt_diurese_w	:=	4;
elsif	(:new.qt_diurese	<=	3999) then
	qt_pt_diurese_w	:=	0;
else
	qt_pt_diurese_w	:=	1;
end if;

-- Ureia
if		(:new.qt_ureia	<=	16.9) then
	qt_pt_ureia_min_w	:=	0;
elsif	(:new.qt_ureia	<=	19) then
	qt_pt_ureia_min_w	:=	2;
elsif	(:new.qt_ureia	<=	39) then
	qt_pt_ureia_min_w	:=	7;
elsif	(:new.qt_ureia	<=	79) then
	qt_pt_ureia_min_w	:=	11;
else
	qt_pt_ureia_min_w	:=	12;
end if;

if		(:new.qt_ureia_max	<=	16.9) then
	qt_pt_ureia_max_w	:=	0;
elsif	(:new.qt_ureia_max	<=	19) then
	qt_pt_ureia_max_w	:=	2;
elsif	(:new.qt_ureia_max	<=	39) then
	qt_pt_ureia_max_w	:=	7;
elsif	(:new.qt_ureia_max	<=	79) then
	qt_pt_ureia_max_w	:=	11;
else
	qt_pt_ureia_max_w	:=	12;
end if;

if (qt_pt_ureia_max_w > qt_pt_ureia_min_w) then
	qt_pt_ureia_w	:=	qt_pt_ureia_max_w;
else
	qt_pt_ureia_w	:=	qt_pt_ureia_min_w;
end if;

-- Albumina
if		(:new.qt_albumina	<=	1.9) then
	qt_pt_albumina_w	:=	11;
elsif	(:new.qt_albumina	<=	2.4) then
	qt_pt_albumina_w	:=	6;
elsif	(:new.qt_albumina	<=	4.4) then
	qt_pt_albumina_w	:=	0;
else
	qt_pt_albumina_w	:=	4;
end if;

-- Bilirrubina
if		(:new.qt_bilirrubina	<=	1.9) then
	qt_pt_bilirrubina_w	:=	0;
elsif	(:new.qt_bilirrubina	<=	2.9) then
	qt_pt_bilirrubina_w	:=	5;
elsif	(:new.qt_bilirrubina	<=	4.9) then
	qt_pt_bilirrubina_w	:=	6;
elsif	(:new.qt_bilirrubina	<=	7.9) then
	qt_pt_bilirrubina_w	:=	8;
else
	qt_pt_bilirrubina_w	:=	16;
end if;

-- Glicose
if		(:new.qt_glicose	<=	39) then
	qt_pt_glicose_min_w	:=	8;
elsif	(:new.qt_glicose	<=	59) then
	qt_pt_glicose_min_w	:=	9;
elsif	(:new.qt_glicose	<=	199) then
	qt_pt_glicose_min_w	:=	0;
elsif	(:new.qt_glicose	<=	349) then
	qt_pt_glicose_min_w	:=	3;
else
	qt_pt_glicose_min_w	:=	5;
end if;

if		(:new.qt_glicose_max	<=	39) then
	qt_pt_glicose_max_w	:=	8;
elsif	(:new.qt_glicose_max	<=	59) then
	qt_pt_glicose_max_w	:=	9;
elsif	(:new.qt_glicose_max	<=	199) then
	qt_pt_glicose_max_w	:=	0;
elsif	(:new.qt_glicose_max	<=	349) then
	qt_pt_glicose_max_w	:=	3;
else
	qt_pt_glicose_max_w	:=	5;
end if;

if (qt_pt_glicose_max_w > qt_pt_glicose_min_w) then
	qt_pt_glicose_w	:=	qt_pt_glicose_max_w;
else
	qt_pt_glicose_w	:=	qt_pt_glicose_min_w;
end if;

-- Gl�bulos vermelhos
if		(:new.qt_glob_vermelhos	<	40.9) then
	qt_pt_glob_vermelhos_min_w	:=	3;
elsif	(:new.qt_glob_vermelhos	<=	49) then
	qt_pt_glob_vermelhos_min_w	:=	0;
else
	qt_pt_glob_vermelhos_min_w	:=	3;
end if;

if		(:new.qt_glob_vermelhos_max	<	40.9) then
	qt_pt_glob_vermelhos_max_w	:=	3;
elsif	(:new.qt_glob_vermelhos_max	<=	49) then
	qt_pt_glob_vermelhos_max_w	:=	0;
else
	qt_pt_glob_vermelhos_max_w	:=	3;
end if;

if (qt_pt_glob_vermelhos_max_w > qt_pt_glob_vermelhos_min_w) then
	qt_pt_glob_vermelhos_w	:=	qt_pt_glob_vermelhos_max_w;
else
	qt_pt_glob_vermelhos_w	:=	qt_pt_glob_vermelhos_min_w;
end if;

-- Gl�bulos brancos
if		(:new.qt_glob_brancos	<	1) then
	qt_pt_glob_brancos_min_w	:=	19;
elsif	(:new.qt_glob_brancos	<=	2.9) then
	qt_pt_glob_brancos_min_w	:=	5;
elsif	(:new.qt_glob_brancos	<=	19.9) then
	qt_pt_glob_brancos_min_w	:=	0;
elsif	(:new.qt_glob_brancos	<=	24.9) then
	qt_pt_glob_brancos_min_w	:=	1;
else
	qt_pt_glob_brancos_min_w	:=	5;
end if;

if		(:new.qt_glob_brancos_max	<	1) then
	qt_pt_glob_brancos_max_w	:=	19;
elsif	(:new.qt_glob_brancos_max	<=	2.9) then
	qt_pt_glob_brancos_max_w	:=	5;
elsif	(:new.qt_glob_brancos_max	<=	19.9) then
	qt_pt_glob_brancos_max_w	:=	0;
elsif	(:new.qt_glob_brancos_max	<=	24.9) then
	qt_pt_glob_brancos_max_w	:=	1;
else
	qt_pt_glob_brancos_max_w	:=	5;
end if;

if (qt_pt_glob_brancos_max_w > qt_pt_glob_brancos_min_w) then
	qt_pt_glob_brancos_w	:=	qt_pt_glob_brancos_max_w;
else
	qt_pt_glob_brancos_w	:=	qt_pt_glob_brancos_min_w;
end if;

-- Glasgow
if	(:new.ie_glasgow_ocular in (4,3,2)) then
	if	(:new.ie_glasgow_motora = 6) then
		if		(:new.ie_glasgow_verbal = 4) then
			qt_pt_glasgow_w	:=	3;
		elsif	(:new.ie_glasgow_verbal = 3) then
			qt_pt_glasgow_w	:=	10;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	10;
		elsif	(:new.ie_glasgow_verbal = 1) then
			qt_pt_glasgow_w	:=	15;
		else
			qt_pt_glasgow_w	:=	0;
		end if;
	elsif	(:new.ie_glasgow_motora = 5) then
		if		(:new.ie_glasgow_verbal = 4) then
			qt_pt_glasgow_w	:=	8;
		elsif	(:new.ie_glasgow_verbal = 3) then
			qt_pt_glasgow_w	:=	13;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	13;
		elsif	(:new.ie_glasgow_verbal = 1) then
			qt_pt_glasgow_w	:=	15;
		else
			qt_pt_glasgow_w	:=	3;
		end if;
	elsif	(:new.ie_glasgow_motora in (4,3)) then
		if		(:new.ie_glasgow_verbal = 4) then
			qt_pt_glasgow_w	:=	13;
		elsif	(:new.ie_glasgow_verbal = 3) then
			qt_pt_glasgow_w	:=	24;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	24;
		elsif	(:new.ie_glasgow_verbal = 1) then
			qt_pt_glasgow_w	:=	24;
		else
			qt_pt_glasgow_w	:=	3;
		end if;
	elsif	(:new.ie_glasgow_motora in (2,1)) then
		if		(:new.ie_glasgow_verbal = 4) then
			qt_pt_glasgow_w	:=	13;
		elsif	(:new.ie_glasgow_verbal = 3) then
			qt_pt_glasgow_w	:=	29;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	29;
		elsif	(:new.ie_glasgow_verbal = 1) then
			qt_pt_glasgow_w	:=	29;
		else
			qt_pt_glasgow_w	:=	3;
		end if;
	end if;
elsif	(:new.ie_glasgow_ocular = 1) then
	if	(:new.ie_glasgow_motora in (6,5)) then
		if	(:new.ie_glasgow_verbal in (5,4,3,2)) then
			qt_pt_glasgow_w	:=	0;
		else
			qt_pt_glasgow_w	:=	16;
		end if;
	elsif	(:new.ie_glasgow_motora in (4,3)) then
		if		(:new.ie_glasgow_verbal in (5,4,3)) then
			qt_pt_glasgow_w	:=	0;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	24;
		else
			qt_pt_glasgow_w	:=	33;
		end if;
	elsif	(:new.ie_glasgow_motora in (2,1)) then
		if		(:new.ie_glasgow_verbal in (5,4,3)) then
			qt_pt_glasgow_w	:=	0;
		elsif	(:new.ie_glasgow_verbal = 2) then
			qt_pt_glasgow_w	:=	29;
		else
			qt_pt_glasgow_w	:=	48;
		end if;
	end if;
end if;

-- Idade
if		(:new.qt_idade	<=	44) then
	qt_pt_idade_w	:=	0;
elsif	(:new.qt_idade	<=	59) then
	qt_pt_idade_w	:=	5;
elsif	(:new.qt_idade	<=	64) then
	qt_pt_idade_w	:=	11;
elsif	(:new.qt_idade	<=	69) then
	qt_pt_idade_w	:=	13;
elsif	(:new.qt_idade	<=	74) then
	qt_pt_idade_w	:=	16;
elsif	(:new.qt_idade	<=	84) then
	qt_pt_idade_w	:=	17;
else
	qt_pt_idade_w	:=	24;
end if;

-- Doencas Cronicas
if		(:new.ie_aids	=	'S') then
	qt_pt_doencas_cronicas_w	:=	23;
elsif 	(:new.ie_hepatica	=	'S') then
	qt_pt_doencas_cronicas_w	:=	16;
elsif 	(:new.ie_linfoma	=	'S') then
	qt_pt_doencas_cronicas_w	:=	13;
elsif 	(:new.ie_cancer_metastatico	=	'S') then
	qt_pt_doencas_cronicas_w	:=	11;
elsif 	(:new.ie_leuc_mileoma_mult	=	'S') then
	qt_pt_doencas_cronicas_w	:=	10;
elsif 	(:new.ie_imunosupressao	=	'S') then
	qt_pt_doencas_cronicas_w	:=	10;
elsif 	(:new.ie_cirrose	=	'S') then
	qt_pt_doencas_cronicas_w	:=	4;
else
	qt_pt_doencas_cronicas_w	:=	0;
end if;

:new.qt_pontos_aps	:=	qt_pt_temperatura_w +
						qt_pt_pressao_arterial_w +
						qt_pt_fc_w +
						qt_pt_freq_resp_w +
						qt_pt_po2_w +
						qt_pt_ph_w +
						qt_pt_na_w +
						qt_pt_creatinina_w +
						qt_pt_glob_vermelhos_w +
						qt_pt_glob_brancos_w +
						qt_pt_glasgow_w +
						qt_pt_diurese_w +
						qt_pt_ureia_w +
						qt_pt_bilirrubina_w +
						qt_pt_albumina_w +
						qt_pt_glicose_w;

-- Pontua��o Apache IV // APACHE Score
:new.qt_pontuacao	:=	:new.qt_pontos_aps +
						qt_pt_idade_w +
						qt_pt_doencas_cronicas_w;

:new.qt_escala_glasgow	:=	nvl(:new.ie_glasgow_ocular,0) + nvl(:new.ie_glasgow_verbal,0) + nvl(:new.ie_glasgow_motora,0);

-- C�lculos do percentual de mortalidade // Predicted Mortality Calculations
qt_pt_idade1_w	:= 0;
qt_pt_idade2_w	:= 0;
qt_pt_idade3_w	:= 0;
qt_pt_idade4_w	:= 0;
qt_pt_idade5_w	:= 0;

if	((:new.qt_idade - 27) > 0) then
	qt_pt_idade1_w	:=	power((:new.qt_idade - 27), 3);
end if;
if	((:new.qt_idade - 51) > 0) then
	qt_pt_idade2_w	:=	power((:new.qt_idade - 51), 3);
end if;
if	((:new.qt_idade - 64) > 0) then
	qt_pt_idade3_w	:=	power((:new.qt_idade - 64), 3);
end if;
if	((:new.qt_idade - 74) > 0) then
	qt_pt_idade4_w	:=	power((:new.qt_idade - 74), 3);
end if;
if	((:new.qt_idade - 86) > 0) then
	qt_pt_idade5_w	:=	power((:new.qt_idade - 86), 3);
end if;

qt_pt_aps1_w	:= 0;
qt_pt_aps2_w	:= 0;
qt_pt_aps3_w	:= 0;
qt_pt_aps4_w	:= 0;
qt_pt_aps5_w	:= 0;

if	((:new.qt_pontos_aps - 10) > 0) then
	qt_pt_aps1_w	:=	power((:new.qt_pontos_aps - 10), 3);
end if;
if	((:new.qt_pontos_aps - 22) > 0) then
	qt_pt_aps2_w	:=	power((:new.qt_pontos_aps - 22), 3);
end if;
if	((:new.qt_pontos_aps - 32) > 0) then
	qt_pt_aps3_w	:=	power((:new.qt_pontos_aps - 32), 3);
end if;
if	((:new.qt_pontos_aps - 48) > 0) then
	qt_pt_aps4_w	:=	power((:new.qt_pontos_aps - 48), 3);
end if;
if	((:new.qt_pontos_aps - 89) > 0) then
	qt_pt_aps5_w	:=	power((:new.qt_pontos_aps - 89), 3);
end if;

qt_logit_w	:=	-5.950471952;
qt_logit_w	:=	qt_logit_w + (:new.qt_idade		* ( 0.024177455));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade1_w	* (-0.00000438862));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade2_w	* ( 0.0000501422));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade3_w	* (-0.000127787));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade4_w	* ( 0.000109606));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade5_w	* (-0.0000275723));

qt_logit_w	:=	qt_logit_w + (:new.qt_pontos_aps	*	( 0.055634916));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps1_w			*	( 0.00000871852));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps2_w			*	(-0.0000451101));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps3_w			*	( 0.00005038));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps4_w			*	(-0.0000131231));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps5_w			*	(-8.65349E-07));

if	(:new.ie_ventilacao	=	'S') then
	qt_logit_w	:=	qt_logit_w + 0.271760036;
end if;

qt_logit_w	:=	qt_logit_w + (dividir(:new.qt_po2,dividir(:new.qt_fio2,100))*(-0.000397068));

if		(:new.ie_origem_paciente = 1) then --Sala de emerg�ncia ou UTI m�vel
	qt_logit_w	:=	qt_logit_w + (-0.583828121);
elsif	(:new.ie_origem_paciente = 2) then --Outro hospital
	qt_logit_w	:=	qt_logit_w + 0.022106266;
elsif	(:new.ie_origem_paciente = 3) then --Enfermaria neste hospital
	qt_logit_w	:=	qt_logit_w + 0.017149193;
end if;

if (:new.ie_emergencia = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.249073458;
end if;

qt_los_w	:=	sqrt(:new.qt_dias_antes_uti);
if	((qt_los_w - 0.121) > 0) then
	qt_los1_w	:=	power((qt_los_w - 0.121), 3);
end if;
if	((qt_los_w - 0.423) > 0) then
	qt_los2_w	:=	power((qt_los_w - 0.423), 3);
end if;
if	((qt_los_w - 0.794) > 0) then
	qt_los3_w	:=	power((qt_los_w - 0.794), 3);
end if;
if	((qt_los_w - 2.806) > 0) then
	qt_los4_w	:=	power((qt_los_w - 2.806), 3);
end if;

qt_logit_w	:=	qt_logit_w + (qt_los_w	*-0.310487496);
qt_logit_w	:=	qt_logit_w + (qt_los1_w	*1.474672511);
qt_logit_w	:=	qt_logit_w + (qt_los2_w	*-2.8618857);
qt_logit_w	:=	qt_logit_w + (qt_los3_w	*1.42165901);
qt_logit_w	:=	qt_logit_w + (qt_los4_w	*-0.034445822);

if		(:new.ie_aids = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.958100516;
elsif	(:new.ie_hepatica = 'S') then
	qt_logit_w	:=	qt_logit_w + 1.037379925;
elsif	(:new.ie_linfoma = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.743471748;
elsif	(:new.ie_cancer_metastatico = 'S') then
	qt_logit_w	:=	qt_logit_w + 1.086423752;
elsif	(:new.ie_leuc_mileoma_mult = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.969308299;
elsif	(:new.ie_imunosupressao = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.435581083;
elsif	(:new.ie_cirrose = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.814665088;
end if;

if (:new.ie_glasgow_indis = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.785764316;
else
	qt_logit_w	:=	qt_logit_w + ((15-:new.qt_escala_glasgow) * 0.039117532);
end if;

if (:new.nr_seq_diag is not null) then
	qt_pt_diag := null;
	select	b.qt_pt_mortalidade
	into	qt_pt_diag
	from 	diag_doenca_apache_iv a,
			map_doenca_apache_iv b
	where	a.nr_seq_mapeamento = b.nr_sequencia
	and		a.nr_sequencia = :new.nr_seq_diag;

	if (qt_pt_diag is not null) then
		qt_logit_w := qt_logit_w + qt_pt_diag;
	end if;
end if;

if (:new.ie_terapia_trombo = 'S') then
	qt_logit_w	:=	qt_logit_w	+	(-0.579874039);
end if;

:new.pr_mortalidade		:=	dividir(round(dividir(exp(qt_logit_w),(1 + exp(qt_logit_w)))*10000,2),100);
:new.qt_pontos_logit	:=	qt_logit_w;

-- Previs�o de dias de Estadia // Predicted LOS Calculations

qt_logit_w	:=	1.673887925;
qt_logit_w	:=	qt_logit_w + (:new.qt_idade		* ( 0.017603395));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade1_w	* (-7.68259E-06));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade2_w	* ( 3.95667E-05));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade3_w	* (-0.000166793));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade4_w	* ( 0.000228156));
qt_logit_w	:=	qt_logit_w + (qt_pt_idade5_w	* (-9.32478E-05));

qt_logit_w	:=	qt_logit_w + (:new.qt_pontos_aps	*	( 0.04442699));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps1_w			*	(-5.83049E-05));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps2_w			*	( 0.000297008));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps3_w			*	(-0.000404434));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps4_w			*	( 0.000189251));
qt_logit_w	:=	qt_logit_w + (qt_pt_aps5_w			*	(-2.35199E-05));

if	(:new.ie_ventilacao	=	'S') then
	qt_logit_w	:=	qt_logit_w + 1.835309541;
end if;

qt_logit_w	:=	qt_logit_w + (dividir(:new.qt_po2,dividir(:new.qt_fio2,100))*(-0.004581842));

if		(:new.ie_origem_paciente = 1) then
	qt_logit_w	:=	qt_logit_w + (-0.599591763);
elsif	(:new.ie_origem_paciente = 2) then
	qt_logit_w	:=	qt_logit_w + 0.855505043;
elsif	(:new.ie_origem_paciente = 3) then
	qt_logit_w	:=	qt_logit_w + 0.006529382;
end if;

if (:new.ie_emergencia = 'S') then
	qt_logit_w	:=	qt_logit_w + 1.040690632;
end if;

qt_los_w	:=	sqrt(:new.qt_dias_antes_uti);

qt_los_w	:=	sqrt(qt_los_w);
if	((qt_los_w - 0.121) > 0) then
	qt_los1_w	:=	power((qt_los_w - 0.121), 3);
end if;
if	((qt_los_w - 0.423) > 0) then
	qt_los2_w	:=	power((qt_los_w - 0.423), 3);
end if;
if	((qt_los_w - 0.794) > 0) then
	qt_los3_w	:=	power((qt_los_w - 0.794), 3);
end if;
if	((qt_los_w - 2.806) > 0) then
	qt_los4_w	:=	power((qt_los_w - 2.806), 3);
end if;

qt_logit_w	:=	qt_logit_w + (qt_los_w	*0.459823129);
qt_logit_w	:=	qt_logit_w + (qt_los1_w	*0.397791937);
qt_logit_w	:=	qt_logit_w + (qt_los2_w	*(-0.945210953));
qt_logit_w	:=	qt_logit_w + (qt_los3_w	*0.588651266);
qt_logit_w	:=	qt_logit_w + (qt_los4_w	*(-0.041232251));

if		(:new.ie_aids = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.10285942);
elsif	(:new.ie_hepatica = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.16012995);
elsif	(:new.ie_linfoma = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.28079854);
elsif	(:new.ie_cancer_metastatico = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.491932974);
elsif	(:new.ie_leuc_mileoma_mult = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.803754341);
elsif	(:new.ie_imunosupressao = 'S') then
	qt_logit_w	:=	qt_logit_w + (-0.07438064);
elsif	(:new.ie_cirrose = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.362658613;
end if;

if	(:new.ie_readmissao = 'S') then
	qt_logit_w	:=	qt_logit_w + 0.540368459;
end if;

if (:new.nr_seq_diag is not null) then
	qt_pt_diag := null;

	select	b.qt_pt_uti
	into	qt_pt_diag
	from 	diag_doenca_apache_iv a,
			map_doenca_apache_iv b
	where	a.nr_seq_mapeamento = b.nr_sequencia
	and		a.nr_sequencia = :new.nr_seq_diag;

	if (qt_pt_diag is not null) then
		qt_logit_w := qt_logit_w + qt_pt_diag;
	end if;
end if;

if (:new.ie_terapia_trombo = 'S') then
	qt_logit_w	:=	qt_logit_w	+	(0.062385214);
end if;

if (:new.ie_glasgow_indis = 'S') then
	qt_logit_w	:=	qt_logit_w + 1.789326613;
else
	qt_logit_w	:=	qt_logit_w + ((15-:new.qt_escala_glasgow) * (-0.015182904));
end if;

:new.qt_dias_uti	:=	round(qt_logit_w,2);
<<Final>>
qt_reg_w	:= 0;


end Escala_apache_IV_atual;
/


ALTER TABLE TASY.ESCALA_APACHE_IV ADD (
  CONSTRAINT ESCAPIV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_APACHE_IV ADD (
  CONSTRAINT ESCAPIV_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAPIV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAPIV_DIDOAPI_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.DIAG_DOENCA_APACHE_IV (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_APACHE_IV TO NIVEL_1;

