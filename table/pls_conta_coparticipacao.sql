ALTER TABLE TASY.PLS_CONTA_COPARTICIPACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_COPARTICIPACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_COPARTICIPACAO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_CONTA                  NUMBER(10)      NOT NULL,
  TX_COPARTICIPACAO             NUMBER(7,4)     NOT NULL,
  VL_COPARTICIPACAO             NUMBER(15,2)    NOT NULL,
  NR_SEQ_REGRA                  NUMBER(10),
  NR_SEQ_MENSALIDADE_SEG        NUMBER(10),
  VL_COPARTICIPACAO_UNIT        NUMBER(15,2),
  CD_CONTA_CRED                 VARCHAR2(20 BYTE),
  CD_CONTA_DEB                  VARCHAR2(20 BYTE),
  CD_HISTORICO                  NUMBER(10),
  NR_SEQ_GRUPO_ANS              NUMBER(10),
  NR_SEQ_REGRA_CTB_DEB          NUMBER(10),
  NR_SEQ_REGRA_CTB_CRED         NUMBER(10),
  CD_CONTA_CRED_ANTECIP         VARCHAR2(20 BYTE),
  CD_CONTA_DEB_ANTECIP          VARCHAR2(20 BYTE),
  CD_HISTORICO_BAIXA            NUMBER(10),
  NR_SEQ_REGRA_EXCLUSAO         NUMBER(10),
  IE_CALCULO_COPARTICIPACAO     VARCHAR2(3 BYTE),
  VL_BASE_COPARTIC              NUMBER(15,2),
  IE_GERAR_MENSALIDADE          VARCHAR2(1 BYTE),
  CD_CENTRO_CUSTO               NUMBER(8),
  CD_CONTA_CRED_PROVISAO        VARCHAR2(20 BYTE),
  CD_CONTA_DEB_PROVISAO         VARCHAR2(20 BYTE),
  IE_COBRAR_MENSALIDADE         VARCHAR2(1 BYTE),
  CD_CLASSIF_CRED               VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB                VARCHAR2(40 BYTE),
  CD_CLASSIF_CRED_ANT           VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB_ANT            VARCHAR2(40 BYTE),
  QT_LIBERADA_COPARTIC          NUMBER(15,2),
  NR_SEQ_ESQUEMA                NUMBER(10),
  NR_SEQ_ESQUEMA_PREV           NUMBER(10),
  IE_GLOSA                      VARCHAR2(1 BYTE),
  CD_HISTORICO_PROVISAO         NUMBER(10),
  CD_CLASSIF_DEB_PROVISAO       VARCHAR2(40 BYTE),
  CD_CLASSIF_CRED_PROVISAO      VARCHAR2(40 BYTE),
  VL_PROVISAO                   NUMBER(15,2),
  NR_LOTE_CONTABIL_PROV         NUMBER(10),
  NR_SEQ_DISC_PROC              NUMBER(10),
  NR_SEQ_DISC_MAT               NUMBER(10),
  NR_SEQ_REGRA_INTERCAMBIO      NUMBER(10),
  CD_CONTA_ANTEC_BAIXA          VARCHAR2(20 BYTE),
  IE_ORIGEM_COPARTIC            VARCHAR2(2 BYTE),
  IE_STATUS_MENSALIDADE         VARCHAR2(1 BYTE),
  CD_SISTEMA_ANT                VARCHAR2(255 BYTE),
  IE_ESTORNO_CUSTO              VARCHAR2(1 BYTE),
  DT_ESTORNO                    DATE,
  NR_LOTE_CONTABIL_ESTORNO      NUMBER(10),
  NR_SEQ_REGRA_ORIGEM           NUMBER(10),
  IE_ORIGEM_REGRA               VARCHAR2(1 BYTE),
  IE_STATUS_COPARTICIPACAO      VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO_INTERNACAO    NUMBER(15),
  IE_ORIGEM_PROCED_INTERNACAO   NUMBER(10),
  VL_COPARTIC_MENS              NUMBER(15,2),
  TX_COPARTIC_MENS              NUMBER(7,4),
  NR_SEQ_REGRA_LIMITE_COPARTIC  NUMBER(10),
  NR_SEQ_CONTA_PROC             NUMBER(10),
  NR_SEQ_CONTA_MAT              NUMBER(10),
  NR_SEQ_REC_FUTURA             NUMBER(10),
  NR_SEQ_REGRA_COPARTIC         NUMBER(10),
  NR_SEQ_PROCESSO_COPARTIC      NUMBER(10),
  DT_FECHAMENTO_DISCUSSAO       DATE,
  IE_ATO_COOPERADO              VARCHAR2(1 BYTE),
  IE_TIPO_PROTOCOLO             VARCHAR2(3 BYTE),
  IE_TIPO_GUIA                  VARCHAR2(2 BYTE),
  DT_MES_COMPETENCIA            DATE,
  IE_TIPO_SEGURADO              VARCHAR2(3 BYTE),
  NR_SEQ_PRESTADOR_ATEND        NUMBER(10),
  NR_SEQ_PRESTADOR_EXEC         NUMBER(10),
  IE_TIPO_PRESTADOR_EXEC        VARCHAR2(2 BYTE),
  IE_TIPO_PRESTADOR_ATEND       VARCHAR2(2 BYTE),
  NR_SEQ_SEGURADO               NUMBER(10),
  VL_ATO_COOPERADO              NUMBER(15,2),
  VL_ATO_AUXILIAR               NUMBER(15,2),
  VL_ATO_NAO_COOPERADO          NUMBER(15,2),
  NR_SEQ_PROTOCOLO              NUMBER(10),
  DT_COMPETENCIA_MENS           DATE,
  IE_PRECO                      VARCHAR2(2 BYTE),
  NR_SEQ_PAGADOR                NUMBER(10),
  NR_SEQ_PROC_REC               NUMBER(10),
  NR_SEQ_MAT_REC                NUMBER(10),
  NR_SEQ_CONTA_REC              NUMBER(10),
  DS_JUSTIFICATIVA_CANC         VARCHAR2(4000 BYTE),
  NR_SEQ_MOTIVO_CANCEL          NUMBER(10),
  NR_SEQ_REGRA_PRECO_COPART     NUMBER(10),
  IE_VL_PAG_PRESTADOR           VARCHAR2(1 BYTE),
  IE_INICIOU_COBRANCA           NUMBER(1),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOCOP_ANSGRDE_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_GRUPO_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_ANSGRDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CENCUST_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK2_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK3_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_CRED_ANTECIP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK4_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_DEB_ANTECIP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK5_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_CRED_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK6_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_DEB_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_CONCONT_FK7_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_CONTA_ANTEC_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_CONCONT_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_HISPADR_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_HISPADR_FK2_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_HISTORICO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_HISPADR_FK3_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_HISTORICO_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_HISPADR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_I1 ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_MENSALIDADE_SEG, NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_I2 ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA, NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_I3 ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA, NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_I4 ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_SEGURADO, NR_SEQ_MENSALIDADE_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_I5 ON TASY.PLS_CONTA_COPARTICIPACAO
(IE_STATUS_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_LOTCONT_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_LOTE_CONTABIL_ESTORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_LOTCONT_FK2_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_LOTE_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOCOP_PK ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLCONPAG_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSCOMAT_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSCOME_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSCOPRO_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSCORF_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_REC_FUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSDIMA_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_DISC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSDIMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSDISP_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_DISC_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSDISP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSESCO_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSESCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSESCO_FK1_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_ESQUEMA_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSESCO_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSMCCP_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSMESE_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_MENSALIDADE_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSPJCO_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PROCESSO_COPARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSPRCO_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSPRES_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PRESTADOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSPRES_FK2_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PRESTADOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSREIN_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_REGRA_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSREIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSREPAR_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_REGRA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSREPAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSREPAR_FK1_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PLSREPAR_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOCOP_PLSRGCO_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSRGCP_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_REGRA_COPARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PLSSEGU_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PRGLMAT_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_MAT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PRGLMAT_FK1_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA_REC, NR_SEQ_MAT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PRGPROC_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_PROC_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PRGPROC_FK1_I ON TASY.PLS_CONTA_COPARTICIPACAO
(NR_SEQ_CONTA_REC, NR_SEQ_PROC_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOCOP_PROCEDI_FK_I ON TASY.PLS_CONTA_COPARTICIPACAO
(CD_PROCEDIMENTO_INTERNACAO, IE_ORIGEM_PROCED_INTERNACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOCOP_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_conta_copartic_alt_pag
before insert ON TASY.PLS_CONTA_COPARTICIPACAO for each row
declare
dt_pagador_w			date	:= null;
ie_data_pagador_copartic_w	pls_parametros.ie_data_pagador_copartic%type;

begin

if	(:new.nr_seq_pagador is null) then
	select	nvl(max(ie_data_pagador_copartic),'N')
	into	ie_data_pagador_copartic_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if	(ie_data_pagador_copartic_w = 'S') then
		select	max(dt_atendimento_referencia)
		into	dt_pagador_w
		from	pls_conta
		where	nr_sequencia	= :new.nr_seq_conta;
	end if;

	:new.nr_seq_pagador := pls_obter_pagador_benef(	:new.nr_seq_segurado,
							nvl(dt_pagador_w,sysdate),
							'3');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conta_copartic_alt
before update or insert ON TASY.PLS_CONTA_COPARTICIPACAO for each row
declare

ds_log_call_w 		Varchar2(1500);
ds_observacao_w		Varchar2(2000);
ie_opcao_w		Varchar2(3);
nr_seq_conta_w		pls_conta_proc.nr_sequencia%type;

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(:new.nr_seq_regra_exclusao is not null) and
		(:new.vl_coparticipacao	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(259055);
	end if;
end if;


if	( updating )then


	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

	if	( pls_se_aplicacao_tasy = 'N') then
		ds_observacao_w	:= 'Alteração efetuada via update fora do Tasy.';
		if	(nvl(:new.vl_coparticipacao,0) <> nvl(:old.vl_coparticipacao,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Valor coparticipação: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.vl_coparticipacao||' - Modificado: '||:new.vl_coparticipacao||chr(13)||chr(10);
		end if;

		if	(nvl(:new.nr_seq_mensalidade_seg,0) <> nvl(:old.nr_seq_mensalidade_seg,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Seq mensalidade seg.: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_mensalidade_seg||' - Modificado: '||:new.nr_seq_mensalidade_seg||chr(13)||chr(10);
		end if;

		if	(nvl(:new.ie_status_mensalidade,'X') <> nvl(:old.ie_status_mensalidade,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status mensalidade: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);
		end if;

		if	(nvl(:new.ie_status_coparticipacao,'X') <> nvl(:old.ie_status_coparticipacao,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status coparticipação alterado fora do tasy: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10);
		end if;

		if	(nvl(:new.ie_gerar_mensalidade,'X') <> nvl(:old.ie_gerar_mensalidade,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Mensalidade gerada: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_gerar_mensalidade||' - Modificado: '||:new.ie_gerar_mensalidade||chr(13)||chr(10);
		end if;
	end if;

	if ((:old.dt_estorno is null) and (:new.dt_estorno is not null)) then

		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'dt_estorno: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.dt_estorno||' - Modificado: '||:new.dt_estorno||chr(13)||chr(10)||
							'Status coparticipação: '||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10)||
							'Status mensalidade: '||chr(13)||chr(10)||
								chr(9)||'Anterior(cancelada): '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);


	else

		if	( (:new.ie_status_coparticipacao = 'N') and   (:new.ie_status_coparticipacao <> nvl(:old.ie_status_coparticipacao,'N'))) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
								'Status coparticipação: '||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_status_coparticipacao||' - Modificado: '||:new.ie_status_coparticipacao||chr(13)||chr(10);
		end if;

		if	((nvl(:new.ie_status_mensalidade,'X') <> nvl(:old.ie_status_mensalidade,'X')) and (nvl(:old.ie_status_mensalidade,'X') = 'C')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
								'Status mensalidade: '||chr(13)||chr(10)||
								chr(9)||'Anterior(cancelada): '||:old.ie_status_mensalidade||' - Modificado: '||:new.ie_status_mensalidade||chr(13)||chr(10);
		end if;

		if	(nvl(:new.nr_seq_conta,0) <> nvl(:old.nr_seq_conta,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta||' - Modificado: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;

		if	(nvl(:new.nr_seq_conta_proc,0) <> nvl(:old.nr_seq_conta_proc,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta proc: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta_proc||' - Modificado: '||:new.nr_seq_conta_proc||chr(13)||chr(10);
		end if;

		if	(nvl(:new.nr_seq_conta_mat,0) <> nvl(:old.nr_seq_conta_mat,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Conta mat: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_conta_mat||' - Modificado: '||:new.nr_seq_conta_mat||chr(13)||chr(10);
		end if;

	end if;

	if	(ds_observacao_w is not null) then
		insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,vl_coparticipacao,
				vl_coparticipacao_old,nr_seq_mensalidade_seg,nr_seq_mensalidade_seg_old,ie_status_mensalidade,ie_status_mensalidade_old,
				ie_status_coparticipacao,ie_status_coparticipacao_old,ie_gerar_mensalidade,ie_gerar_mensalidade_old,
				nm_maquina,ds_log_call,ds_log,
				ie_tipo_registro)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
				:new.nr_sequencia,:new.nr_seq_conta,:new.nr_seq_conta_proc,:new.nr_seq_conta_mat,:new.vl_coparticipacao,
				:old.vl_coparticipacao,:new.nr_seq_mensalidade_seg,:old.nr_seq_mensalidade_seg,:new.ie_status_mensalidade,:old.ie_status_mensalidade,
				:new.ie_status_coparticipacao,:old.ie_status_coparticipacao,:new.ie_gerar_mensalidade,:old.ie_gerar_mensalidade,
				wheb_usuario_pck.get_machine,ds_log_call_w,substr(ds_observacao_w,1,2000),
				'A');
	end if;
end if;



if	(inserting) then
	if	(:new.nr_seq_conta_proc is not null) then
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from	pls_conta_proc
		where	nr_sequencia	= :new.nr_seq_conta_proc;

		if	(nr_seq_conta_w <> :new.nr_seq_conta) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta insert: '||chr(13)||chr(10)||
						chr(9)||'Conta proc: '||nr_seq_conta_w||' - Conta copart: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;
	end if;

	if	(:new.nr_seq_conta_mat is not null) then
		select	max(nr_seq_conta)
		into	nr_seq_conta_w
		from	pls_conta_mat
		where	nr_sequencia	= :new.nr_seq_conta_mat;

		if	(nr_seq_conta_w <> :new.nr_seq_conta) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta insert: '||chr(13)||chr(10)||
						chr(9)||'Conta mat: '||nr_seq_conta_w||' - Conta copart: '||:new.nr_seq_conta||chr(13)||chr(10);
		end if;
	end if;

	if	(ds_observacao_w is not null) then

		ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

		insert	into	pls_conta_copartic_log
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_conta_copartic,nr_seq_conta,nr_seq_conta_proc,nr_seq_conta_mat,
				nm_maquina,ds_log_call,ds_log,
				ie_tipo_registro)
		values	(	pls_conta_copartic_log_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
				:new.nr_sequencia,:new.nr_seq_conta,:new.nr_seq_conta_proc,:new.nr_seq_conta_mat,
				wheb_usuario_pck.get_machine,ds_log_call_w,substr(ds_observacao_w,1,2000),
				'A');
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conta_copartic_delete
before delete ON TASY.PLS_CONTA_COPARTICIPACAO for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - pls_conta_coparticipacao
Finalidade: Não permitir a exclusão da coparticipacao caso esta possua mensalidade gerada para a mesma
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

nr_seq_mensalidade_w		number(10);

begin/*
select	max(old.NR_SEQ_MENSALIDADE_SEG)
into	nr_seq_mensalidade_w
from	dual;*/

if	(:old.nr_seq_mensalidade_seg is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(221427);
end if;


end;
/


ALTER TABLE TASY.PLS_CONTA_COPARTICIPACAO ADD (
  CONSTRAINT PLSCOCOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          960K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_COPARTICIPACAO ADD (
  CONSTRAINT PLSCOCOP_PLSRGCO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCOCOP_PRGLMAT_FK1 
 FOREIGN KEY (NR_SEQ_CONTA_REC, NR_SEQ_MAT_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT (NR_SEQ_CONTA_REC,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCOCOP_PRGPROC_FK1 
 FOREIGN KEY (NR_SEQ_CONTA_REC, NR_SEQ_PROC_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC (NR_SEQ_CONTA_REC,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCOCOP_PLSMCCP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_MOTIVO_CANCEL_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_INTERNACAO, IE_ORIGEM_PROCED_INTERNACAO) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCOCOP_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_COPARTIC) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSPJCO_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO_COPARTIC) 
 REFERENCES TASY.PLS_PROCESSO_JUD_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSCORF_FK 
 FOREIGN KEY (NR_SEQ_REC_FUTURA) 
 REFERENCES TASY.PLS_CONTA_RECEITA_FUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL_ESTORNO) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOCOP_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_PROV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOCOP_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_ATEND) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_EXEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PRGLMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PRGPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSREPAR_FK1 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOCOP_ANSGRDE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ANS) 
 REFERENCES TASY.ANS_GRUPO_DESPESA (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_CONCONT_FK3 
 FOREIGN KEY (CD_CONTA_CRED_ANTECIP) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_CONCONT_FK4 
 FOREIGN KEY (CD_CONTA_DEB_ANTECIP) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_BAIXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOCOP_CONCONT_FK7 
 FOREIGN KEY (CD_CONTA_ANTEC_BAIXA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_PLSREIN_FK 
 FOREIGN KEY (NR_SEQ_REGRA_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSREPAR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ORIGEM) 
 REFERENCES TASY.PLS_REGRA_COPARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_DEB_PROVISAO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSESCO_FK1 
 FOREIGN KEY (NR_SEQ_ESQUEMA_PREV) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_PROVISAO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOCOP_PLSDIMA_FK 
 FOREIGN KEY (NR_SEQ_DISC_MAT) 
 REFERENCES TASY.PLS_DISCUSSAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSDISP_FK 
 FOREIGN KEY (NR_SEQ_DISC_PROC) 
 REFERENCES TASY.PLS_DISCUSSAO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PLSCOCOP_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_CRED_PROVISAO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOCOP_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_PLSMESE_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE_SEG) 
 REFERENCES TASY.PLS_MENSALIDADE_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSCOCOP_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.PLS_CONTA_COPARTICIPACAO TO NIVEL_1;

