ALTER TABLE TASY.ORDEM_COMPRA_ITEM_RATEIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ITEM_RATEIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ITEM_RATEIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_ORDEM_COMPRA         NUMBER(10)            NOT NULL,
  NR_ITEM_OCI             NUMBER(5)             NOT NULL,
  CD_CENTRO_CUSTO         NUMBER(8),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  CD_CONTA_FINANC         NUMBER(10),
  VL_RATEIO               NUMBER(15,2),
  VL_FRETE                NUMBER(15,2),
  VL_DESCONTO             NUMBER(15,2),
  VL_SEGURO               NUMBER(15,2),
  VL_DESPESA_ACESSORIA    NUMBER(15,2),
  NR_SEQ_CRITERIO_RATEIO  NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  QT_RATEIO               NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCOIRA_CENCUST_FK_I ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOIRA_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOIRA_CONCONT_FK_I ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOIRA_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOIRA_CONFINA_FK_I ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOIRA_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOIRA_CRITRAT_FK_I ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(NR_SEQ_CRITERIO_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOIRA_CRITRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCOIRA_ORCOITE_FK_I ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCOIRA_PK ON TASY.ORDEM_COMPRA_ITEM_RATEIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOIRA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_RATEIO_tp  after update ON TASY.ORDEM_COMPRA_ITEM_RATEIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_RATEIO,1,4000),substr(:new.QT_RATEIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_RATEIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_OCI,1,4000),substr(:new.NR_ITEM_OCI,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_OCI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_FINANC,1,4000),substr(:new.CD_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_RATEIO,1,4000),substr(:new.VL_RATEIO,1,4000),:new.nm_usuario,nr_seq_w,'VL_RATEIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FRETE,1,4000),substr(:new.VL_FRETE,1,4000),:new.nm_usuario,nr_seq_w,'VL_FRETE',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESCONTO,1,4000),substr(:new.VL_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_SEGURO,1,4000),substr(:new.VL_SEGURO,1,4000),:new.nm_usuario,nr_seq_w,'VL_SEGURO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESPESA_ACESSORIA,1,4000),substr(:new.VL_DESPESA_ACESSORIA,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESPESA_ACESSORIA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CRITERIO_RATEIO,1,4000),substr(:new.NR_SEQ_CRITERIO_RATEIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CRITERIO_RATEIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_RATEIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORDEM_COMPRA_ITEM_RATEIO ADD (
  CONSTRAINT ORCOIRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ITEM_RATEIO ADD (
  CONSTRAINT ORCOIRA_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT ORCOIRA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT ORCOIRA_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT ORCOIRA_CRITRAT_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO_RATEIO) 
 REFERENCES TASY.CTB_CRITERIO_RATEIO (NR_SEQUENCIA),
  CONSTRAINT ORCOIRA_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_ITEM_RATEIO TO NIVEL_1;

