ALTER TABLE TASY.PLS_REGRA_PRECO_TX_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_TX_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_TX_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA          NUMBER(10)              NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  QT_EXEC_MIN           NUMBER(9,3)             NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  QT_EXEC_MAX           NUMBER(9,3),
  TX_PROCEDIMENTO       NUMBER(15,4),
  TX_MEDICO             NUMBER(15,4),
  TX_CUSTO_OPERACIONAL  NUMBER(15,4),
  TX_MATERIAL           NUMBER(15,4),
  NR_SEQ_TAXA_ITEM      NUMBER(10),
  TX_ANESTESISTA        NUMBER(7,4),
  TX_AUXILIARES         NUMBER(15,4),
  IE_VIA_ACESSO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPTP_PK ON TASY.PLS_REGRA_PRECO_TX_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPTP_PLSRPQP_FK_I ON TASY.PLS_REGRA_PRECO_TX_PROC
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPTP_PLSRPQP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPTP_PLSTAIT_FK_I ON TASY.PLS_REGRA_PRECO_TX_PROC
(NR_SEQ_TAXA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPTP_PLSTAIT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_TX_PROC_tp  after update ON TASY.PLS_REGRA_PRECO_TX_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EXEC_MIN,1,4000),substr(:new.QT_EXEC_MIN,1,4000),:new.nm_usuario,nr_seq_w,'QT_EXEC_MIN',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EXEC_MAX,1,4000),substr(:new.QT_EXEC_MAX,1,4000),:new.nm_usuario,nr_seq_w,'QT_EXEC_MAX',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_PROCEDIMENTO,1,4000),substr(:new.TX_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'TX_PROCEDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AUXILIARES,1,4000),substr(:new.TX_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'TX_AUXILIARES',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_CUSTO_OPERACIONAL,1,4000),substr(:new.TX_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_CUSTO_OPERACIONAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MATERIAL,1,4000),substr(:new.TX_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TAXA_ITEM,1,4000),substr(:new.NR_SEQ_TAXA_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TAXA_ITEM',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_ANESTESISTA,1,4000),substr(:new.TX_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_ANESTESISTA',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MEDICO,1,4000),substr(:new.TX_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'TX_MEDICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_TX_PROC ADD (
  CONSTRAINT PLSRPTP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_TX_PROC ADD (
  CONSTRAINT PLSRPTP_PLSRPQP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_PRECO_QTDE_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSRPTP_PLSTAIT_FK 
 FOREIGN KEY (NR_SEQ_TAXA_ITEM) 
 REFERENCES TASY.PLS_TAXA_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_TX_PROC TO NIVEL_1;

