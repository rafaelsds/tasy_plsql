ALTER TABLE TASY.AGENDA_INTEGRADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_INTEGRADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_INTEGRADA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_INICIO_AGENDAMENTO     DATE                NOT NULL,
  DT_FIM_AGENDAMENTO        DATE,
  NR_SEQ_STATUS             NUMBER(10)          NOT NULL,
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_CONVENIO               NUMBER(5),
  CD_CATEGORIA              VARCHAR2(10 BYTE),
  CD_PLANO                  VARCHAR2(10 BYTE),
  NR_DOC_CONVENIO           VARCHAR2(20 BYTE),
  DT_VALIDADE_CARTEIRA      DATE,
  CD_USUARIO_CONVENIO       VARCHAR2(30 BYTE),
  NM_CONTATO                VARCHAR2(80 BYTE),
  NR_TELEFONE               VARCHAR2(60 BYTE),
  CD_PROFISSIONAL           VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_AGENDA_EXTERNA         NUMBER(15),
  DT_PREVISTA               DATE,
  QT_PESO                   NUMBER(6,3),
  QT_ALTURA_CM              NUMBER(5,2),
  IE_TURNO                  VARCHAR2(2 BYTE),
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  IE_TIPO_ATENDIMENTO       NUMBER(3),
  NM_PACIENTE               VARCHAR2(60 BYTE),
  DT_NASCIMENTO             DATE,
  NR_SEQ_ORCAMENTO          NUMBER(10),
  NR_SEQ_ATEND_PLS          NUMBER(10),
  NR_SEQ_EVENTO_ATEND       NUMBER(10),
  DS_OBS_FINAL              VARCHAR2(2000 BYTE),
  VL_MATERIAL               NUMBER(15,2),
  VL_MEDICAMENTO            NUMBER(15,2),
  NR_SEQ_MOT_CANCEL         NUMBER(10),
  CD_EMPRESA                NUMBER(10),
  CD_MEDICO_SOLICITANTE     VARCHAR2(10 BYTE),
  DS_INDICACAO              VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO_CLASSIF_PAC   NUMBER(10),
  NR_SEQ_COBERTURA          NUMBER(10),
  NM_MEDICO_EXTERNO         VARCHAR2(60 BYTE),
  CRM_MEDICO_EXTERNO        VARCHAR2(60 BYTE),
  IE_BLOQUEIO_CHECKLIST     VARCHAR2(3 BYTE),
  CD_CONVENIO_ANT           NUMBER(5),
  NR_SEQ_FORMA_INDICACAO    NUMBER(10),
  NR_SEQ_CLASSIFICACAO      NUMBER(10),
  CD_PROCEDENCIA            NUMBER(5),
  QT_IDADE_GESTACIONAL      NUMBER(2),
  IE_TIPO_ACOMOD            VARCHAR2(2 BYTE),
  IE_SEXO                   VARCHAR2(1 BYTE),
  QT_IDADE_PAC              NUMBER(3),
  NR_SEQ_REGIAO             NUMBER(10),
  CD_MEDICO_EXEC            VARCHAR2(10 BYTE),
  CD_ESTAB_AGENDA           NUMBER(4),
  NR_SEQ_REGRA_ATEND        NUMBER(10),
  NR_SEQ_FORMA_LAUDO        NUMBER(10),
  IE_EXTERNO                VARCHAR2(1 BYTE),
  NR_SEQ_PENDENCIA          NUMBER(10),
  NR_CONTROLE_SUS           VARCHAR2(10 BYTE),
  CD_REGULACAO_SUS          VARCHAR2(30 BYTE),
  CD_CHAVE_REGULACAO_SUS    VARCHAR2(30 BYTE),
  NR_SEQ_OPM                NUMBER(10),
  NR_SEQ_PROP_ADES_PLS      NUMBER(10),
  DT_ULTIMA_MENSTRUACAO     DATE,
  QT_IG_SEMANA              NUMBER(3),
  QT_IG_DIA                 NUMBER(3),
  NR_DDI                    VARCHAR2(3 BYTE),
  NR_SEQ_SEGURADO           NUMBER(10),
  NR_SEQ_PAC_SENHA_FILA     NUMBER(10),
  NM_FAMILIA                VARCHAR2(60 BYTE),
  IE_AGEND_COLETIVO         VARCHAR2(1 BYTE),
  NR_SEQ_CAPTACAO           NUMBER(10),
  NR_SEQ_PARTIC_CICLO_ITEM  NUMBER(10),
  IE_FORMA_AGENDAMENTO      NUMBER(3),
  NR_SEQ_SOLIC_TRANSPORTE   NUMBER(10),
  IE_COD_USUARIO_MAE_RESP   VARCHAR2(1 BYTE),
  CD_TIPO_ACOMODACAO        NUMBER(4),
  NR_SEQ_LISTA_ESPERA       NUMBER(10),
  IE_MULTIPLO               VARCHAR2(5 BYTE),
  CD_EMPRESA_SOLICITANTE    NUMBER(10),
  CNPJ_SOLICITANTE          VARCHAR2(14 BYTE),
  IE_API_INTEGRACAO         VARCHAR2(1 BYTE),
  NR_SEQ_PERSON_NAME        NUMBER(10),
  IE_AGENDAMENTO_WSUITE     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEINTE_AGIFRIN_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_FORMA_INDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_AGIFRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_AGIMOTC_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_MOT_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_AGIMOTC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_AGLIESP_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_LISTA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_AGTRGAT_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_AGTRGAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CATCONV_FK_I ON TASY.AGENDA_INTEGRADA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CDEXT_I ON TASY.AGENDA_INTEGRADA
(CD_AGENDA_EXTERNA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_CLAATEN_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CONPLAN_FK_I ON TASY.AGENDA_INTEGRADA
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CONVCOB_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CONVCOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CONVENI_FK_I ON TASY.AGENDA_INTEGRADA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_CONVENI_FK2_I ON TASY.AGENDA_INTEGRADA
(CD_CONVENIO_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_CONVENI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_EMPREFE_FK_I ON TASY.AGENDA_INTEGRADA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_ESTABEL_FK_I ON TASY.AGENDA_INTEGRADA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_ESTABEL_FK2_I ON TASY.AGENDA_INTEGRADA
(CD_ESTAB_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_ESTABEL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_FORENLA_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_FORMA_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_I1 ON TASY.AGENDA_INTEGRADA
(CD_ESTABELECIMENTO, NR_SEQ_STATUS, DT_INICIO_AGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_I1
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_I2 ON TASY.AGENDA_INTEGRADA
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_I3 ON TASY.AGENDA_INTEGRADA
(NR_SEQ_STATUS, TRUNC("DT_INICIO_AGENDAMENTO"), CD_PROFISSIONAL, NM_PACIENTE, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_MEDICO_FK_I ON TASY.AGENDA_INTEGRADA
(CD_MEDICO_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_MEDICO_FK2_I ON TASY.AGENDA_INTEGRADA
(CD_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_MPRCAPT_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_MPRPACI_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_PARTIC_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_ORCPACI_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_ORCAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_ORPROPM_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_PESFISI_FK_I ON TASY.AGENDA_INTEGRADA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_PESFISI_FK2_I ON TASY.AGENDA_INTEGRADA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEINTE_PK ON TASY.AGENDA_INTEGRADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_PLSASEV_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_EVENTO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_PLSASEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_PLSATEN_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_ATEND_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_PLSATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_PLSPRAD_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_PROP_ADES_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_PLSSEGU_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_PROCEDE_FK_I ON TASY.AGENDA_INTEGRADA
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_QTPENAG_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_PENDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINTE_TICLAPA_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_TIPO_CLASSIF_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINTE_TICLAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINTE_TRANSSO_FK_I ON TASY.AGENDA_INTEGRADA
(NR_SEQ_SOLIC_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AGENDA_INTEGRADA_tp  after update ON TASY.AGENDA_INTEGRADA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_CONVENIO,1,500);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'AGENDA_INTEGRADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PACIENTE,1,4000),substr(:new.NM_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'NM_PACIENTE',ie_log_w,ds_w,'AGENDA_INTEGRADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAC_SENHA_FILA,1,4000),substr(:new.NR_SEQ_PAC_SENHA_FILA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAC_SENHA_FILA',ie_log_w,ds_w,'AGENDA_INTEGRADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'AGENDA_INTEGRADA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_cancel_agend_integrada_upd AFTER UPDATE ON TASY.AGENDA_INTEGRADA FOR EACH ROW
DECLARE

qt_reg_w number(10);

BEGIN

      if(:new.nr_seq_status  = 3 and nvl(:old.nr_seq_status,0) != 3)then
       
        select count(*)
        into qt_reg_w
        from AGENDA_INTEGRADA_ITEM a
        join hsj_agenda_integrada_alerta b on(a.nr_sequencia = b.nr_seq_agend_int_item)
        where nr_seq_agenda_int = :new.nr_sequencia
        and exists
        (
            select 1
            from ATENDIMENTO_ALERTA w
            where w.nr_sequencia = b.nr_seq_alerta_atend
            and w.ie_situacao = 'A'
            and w.dt_inativacao is null
        );

        if(qt_reg_w > 0)then
                           
            for v_dados in
            (
                select b.NR_SEQ_ALERTA_ATEND
                into qt_reg_w
                from AGENDA_INTEGRADA_ITEM a
                join hsj_agenda_integrada_alerta b on(a.nr_sequencia = b.nr_seq_agend_int_item)
                where nr_seq_agenda_int = :new.nr_sequencia
                and exists
                (
                    select 1
                    from ATENDIMENTO_ALERTA w
                    where w.nr_sequencia = b.nr_seq_alerta_atend
                    and w.ie_situacao = 'A'
                    and w.dt_inativacao is null
                )
            )loop
            
                update ATENDIMENTO_ALERTA set ie_situacao = 'I', dt_inativacao = sysdate, nm_usuario_inativacao = obter_usuario_ativo
                where nr_sequencia = v_dados.nr_seq_alerta_atend
                and ie_situacao = 'A'; 
                
            end loop;

        end if;
        
      end if;
      
END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_integrada_Atual
before insert or update ON TASY.AGENDA_INTEGRADA FOR EACH ROW
DECLARE

cd_profissional_w		varchar2(10);
atrib_oldvalue_w		varchar2(255);
atrib_newvalue_w		varchar2(255);
qt_itens_cons_w			number(10)	:= 0;
qt_itens_exame_w		number(10)	:= 0;
cd_agenda_w				number(10);
nr_sequencia_w			number(10);
dt_agenda_w				date;
ds_erro_w				varchar2(255);
ie_atualiza_medico_req_w	varchar2(1);
nr_seq_ageint_item_w		number(10,0);
nr_seq_agenda_cons_w		number(10,0);
nr_seq_agenda_exame_w		number(10,0);
nm_pessoa_contato       varchar2(50);
ie_status_integrada_w		agenda_integrada_status.ie_status_tasy%type;
ie_atualiza_obs_w		varchar2(1);
nm_paciente_w			varchar2(60);
ie_save_insurance_holder_w	varchar2(1);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_agenda_cons
	from	agenda_integrada_item
	where	nr_seq_agenda_int = :new.nr_sequencia
	and	ie_tipo_agendamento = 'S'
	and	cd_medico_req <> :new.cd_medico_solicitante;

Cursor C02 is
	select	nr_sequencia,
		nr_seq_agenda_exame
	from	agenda_integrada_item
	where	nr_seq_agenda_int = :new.nr_sequencia
	and	ie_tipo_agendamento = 'E'
	and	cd_medico_req <> :new.cd_medico_solicitante;

Cursor C03 is
	select	nr_seq_agenda_cons,
			nr_seq_agenda_exame
	from	agenda_integrada_item
	where	nr_seq_agenda_int = :new.nr_sequencia
	and	ie_tipo_agendamento in ('S','C','E');

BEGIN

ie_save_insurance_holder_w := obter_parametro_agenda(wheb_usuario_pck.get_cd_estabelecimento, 'IE_SAVE_INSURANCE_HOLDER', 'N');
if	(ie_save_insurance_holder_w = 'S') and
	(:new.cd_pessoa_fisica is not null) and
	(:new.cd_convenio is not null) and
	((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
	 (nvl(:new.cd_pessoa_fisica, '0') <> nvl(:old.cd_pessoa_fisica, '0')) or
	 (nvl(:new.cd_categoria, 0) <> nvl(:old.cd_categoria, 0)) or
	 (nvl(:new.cd_usuario_convenio, '0') <> nvl(:old.cd_usuario_convenio, '0'))) then
	insere_atualiza_titular_conv(
				:new.nm_usuario,
				:new.cd_convenio,
				:new.cd_categoria,
				:new.cd_pessoa_fisica,
				:new.cd_plano,
				null,
				:new.dt_validade_carteira,
				:new.dt_validade_carteira,
				null,
				:new.cd_usuario_convenio,
				null,
				'N',
				'2');
  end if;

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

  if  (:new.cd_pessoa_fisica is not null) then
      if  (nvl(:new.cd_pessoa_fisica,'X') <> nvl(:old.cd_pessoa_fisica,'X')) /* Maxwell em 10/Jun/2019 Verificar altera��o do Nome/Codigo  */
         or (nvl(:new.nm_paciente,'X') <> nvl(:old.nm_paciente,'X'))   then
         /* obter nome pf */
         select   substr(obter_nome_pf(:new.cd_pessoa_fisica),1,60)
         into  nm_paciente_w
         from  pessoa_fisica
         where  cd_pessoa_fisica = :new.cd_pessoa_fisica;
         /* atualizar nome pf */
         :new.nm_paciente := nm_paciente_w;
      end if;
   end if;


	Obter_Param_Usuario(869, 81, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_atualiza_medico_req_w);

	select 	nvl(max(ie_atual_obs_agendas),'N')
	into	ie_atualiza_obs_w
	from	parametro_agenda_integrada
	where	nvl(cd_estabelecimento,:new.cd_estabelecimento) = :new.cd_estabelecimento;

	if	(((:old.cd_convenio is not null) and
		(:old.cd_convenio	<> :new.cd_convenio)) or
		((:old.cd_convenio is null) and
		(:new.cd_convenio is not null))) or
		(((:old.cd_categoria is not null) and
		(:old.cd_categoria	<> :new.cd_categoria)) or
		((:old.cd_categoria is null) and
		(:new.cd_categoria is not null))) or
		(((:old.cd_plano is not null) and
		(:old.cd_plano	<> :new.cd_plano)) or
		((:old.cd_plano is null) and
		(:new.cd_plano is not null))) or
		(((:old.nr_seq_cobertura is not null) and
		(:old.nr_seq_cobertura	<> :new.nr_seq_cobertura)) or
		((:old.nr_seq_cobertura is null) and
		(:new.nr_seq_cobertura is not null))) or
		(((:old.cd_pessoa_fisica is not null) and
		(:old.cd_pessoa_fisica	<> :new.cd_pessoa_fisica)) or
		((:old.cd_pessoa_fisica is null) and
		(:new.cd_pessoa_fisica is not null))) or
		(NVL(:old.ie_tipo_atendimento,-999) <> NVL(:new.ie_tipo_atendimento,-999) )
		then
		Calcular_Valor_Proc_Ageint(
				:new.nr_sequencia,
				:new.cd_convenio,
				:new.cd_categoria,
				:new.cd_estabelecimento,
				:new.dt_inicio_agendamento,
				:new.cd_plano,
				:new.nm_usuario,
				:new.cd_usuario_convenio,
				:new.cd_pessoa_fisica,
				:new.ie_tipo_atendimento,
				:new.nr_seq_cobertura);
		Calcular_Valor_Proc_Adic(
				:new.nr_sequencia,
				:new.cd_convenio,
				:new.cd_categoria,
				:new.cd_estabelecimento,
				:new.dt_inicio_agendamento,
				:new.cd_plano,
				:new.nm_usuario,
				:new.cd_usuario_convenio,
				:new.cd_pessoa_fisica,
				:new.ie_tipo_atendimento,
				:new.nr_seq_cobertura);
		Calcular_Valor_Proc_Lab_Ageint(
				:new.nr_sequencia,
				:new.cd_convenio,
				:new.cd_categoria,
				:new.cd_estabelecimento,
				:new.dt_inicio_agendamento,
				:new.cd_plano,
				:new.nm_usuario,
				:new.cd_usuario_convenio,
				:new.cd_pessoa_fisica,
				:new.ie_tipo_atendimento,
				null,
				'S');
		:new.cd_convenio_ant	:= :old.cd_convenio;
	end if;

	if      (:old.nr_seq_status <> :new.nr_seq_status) or
		(:old.nr_seq_status is null) then
		insert into ageint_historico
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ageint,
			dt_inicio_agendamento,
			dt_fim_Agendamento,
			nr_Seq_status,
			nm_usuario_hist,
			dt_Atualizacao_hist)
		values
			(ageint_historico_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			:new.dt_inicio_agendamento,
			:new.dt_fim_agendamento,
			:new.nr_seq_status,
			:new.nm_usuario,
			sysdate);
	end if;

	if	(:new.ds_observacao is not null) and
		((:old.ds_observacao <> :new.ds_observacao) or
		(:old.ds_observacao is null)) then
		begin
			atrib_oldvalue_w := substr(:old.ds_observacao,1,255);
			atrib_newvalue_w := substr(:new.ds_observacao,1,255);

			select	count(*)
			into	qt_itens_cons_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= :new.nr_sequencia
			and		ie_tipo_agendamento	= 'C';

			select	count(*)
			into	qt_itens_exame_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= :new.nr_sequencia
			and		ie_tipo_agendamento	= 'E';

			if	(qt_itens_cons_w > 0)then

				select	a.cd_agenda,
						a.nr_sequencia,
						a.dt_agenda
				into	cd_agenda_w,
						nr_sequencia_w,
						dt_agenda_w
				from	agenda_consulta a,
						agenda_integrada_item b
				where	a.nr_sequencia 		= b.nr_seq_agenda_cons
				and		b.nr_seq_agenda_int	= :new.nr_sequencia;

				gerar_agenda_consulta_hist(cd_agenda_w,nr_sequencia_w,'AO',:new.nm_usuario, wheb_mensagem_pck.get_texto(791452) || ' ' || atrib_oldvalue_w || ' ' || wheb_mensagem_pck.get_texto(791442) || ' ' || atrib_newvalue_w, :old.cd_pessoa_fisica, :old.nm_paciente, dt_agenda_w);
			end if;

			if	(qt_itens_exame_w > 0)then

				select	a.cd_agenda,
						a.nr_sequencia,
						a.hr_inicio
				into	cd_agenda_w,
						nr_sequencia_w,
						dt_agenda_w
				from	agenda_paciente a,
						agenda_integrada_item b
				where	a.nr_sequencia 		= b.nr_seq_agenda_exame
				and		b.nr_seq_agenda_int	= :new.nr_sequencia;

				gerar_agenda_paciente_hist(cd_agenda_w,nr_sequencia_w,'O',:new.nm_usuario, wheb_mensagem_pck.get_texto(791452) || ' ' || atrib_oldvalue_w || ' ' || wheb_mensagem_pck.get_texto(791442) || ' ' || atrib_newvalue_w, :old.cd_pessoa_fisica, :old.nm_paciente, dt_agenda_w, obter_perfil_ativo);
			end if;


			if (ie_atualiza_obs_w = 'S') then
				open C03;
				loop
				fetch C03 into
					nr_seq_agenda_cons_w,
					nr_seq_agenda_exame_w;
				exit when C03%notfound;
					begin
						if (nr_seq_agenda_cons_w is not null) then
							update	agenda_consulta
							set		ds_observacao = substr(:new.ds_observacao,1,2000)
							where	nr_sequencia = nr_seq_agenda_cons_w;
						end if;

						if (nr_seq_agenda_exame_w is not null) then
							update	agenda_paciente
							set		ds_observacao = substr(:new.ds_observacao,1,4000)
							where	nr_sequencia = nr_seq_agenda_exame_w;
						end if;
					end;
				end loop;
				close C03;
			end if;
		exception
		when others then
			ds_erro_w	:= substr(sqlerrm,1,255);
		end;
		end if;

	select	max(cd_pessoa_fisica)
	into	cd_profissional_w
	from	usuario
	where	nm_usuario	= :new.nm_usuario;

	select	nvl(max(ie_status_tasy),'EA')
	into	ie_status_integrada_w
	from	agenda_integrada_status
	where	nr_sequencia = :new.nr_seq_status
	and		ie_situacao = 'A';

	if	(cd_profissional_w is not null) and
		(ie_status_integrada_w <> 'AG')then
		:new.cd_profissional	:= cd_profissional_w;
	end if;

	if	(ie_atualiza_medico_req_w = 'S') and
		(:old.cd_medico_solicitante is not null) and
		(:new.cd_medico_solicitante is not null) and
		(:old.cd_medico_solicitante <> :new.cd_medico_solicitante) then

		open C01;
		loop
		fetch C01 into
			nr_seq_ageint_item_w,
			nr_seq_agenda_cons_w;
		exit when C01%notfound;
			begin
			update	agenda_integrada_item
			set	cd_medico_req = :new.cd_medico_solicitante
			where	nr_sequencia = nr_seq_ageint_item_w;

			if	(nr_seq_agenda_cons_w is not null) then
				update	agenda_consulta
				set	cd_medico_req = :new.cd_medico_solicitante
				where	nr_sequencia = nr_seq_agenda_cons_w;
			end if;
			end;
		end loop;
		close C01;

		open C02;
		loop
		fetch C02 into
			nr_seq_ageint_item_w,
			nr_seq_agenda_exame_w;
		exit when C02%notfound;
			begin
			update	agenda_integrada_item
			set	cd_medico_req 	= :new.cd_medico_solicitante
			where	nr_sequencia 	= nr_seq_ageint_item_w;

			if	(nr_seq_agenda_exame_w is not null) then
				update	agenda_paciente
				set	cd_medico	= :new.cd_medico_solicitante
				where	nr_sequencia 	= nr_seq_agenda_exame_w;
			end if;
			end;
		end loop;
		close C02;


	end if;


	if	(:old.nm_contato <> :new.nm_contato) then

		open C03;
		loop
		fetch C03 into
			nr_seq_agenda_cons_w,
			nr_seq_agenda_exame_w;
		exit when C03%notfound;
			begin
				if (nr_seq_agenda_cons_w is not null) then
					update	agenda_consulta
					set		nm_pessoa_contato = :new.nm_contato
					where	nr_sequencia = nr_seq_agenda_cons_w;
				end if;

				if (nr_seq_agenda_exame_w is not null) then
					update	agenda_paciente
					set		nm_pessoa_contato = :new.nm_contato
					where	nr_sequencia = nr_seq_agenda_exame_w;
				end if;

			end;
		end loop;
		close C03;

	end if;

end if;

END;
/


ALTER TABLE TASY.AGENDA_INTEGRADA ADD (
  CONSTRAINT AGEINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_INTEGRADA ADD (
  CONSTRAINT AGEINTE_FORENLA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_LAUDO) 
 REFERENCES TASY.FORMA_ENTREGA_LAUDO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_QTPENAG_FK 
 FOREIGN KEY (NR_SEQ_PENDENCIA) 
 REFERENCES TASY.QT_PENDENCIA_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA_ESPERA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROP_ADES_PLS) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEINTE_ORPROPM_FK 
 FOREIGN KEY (NR_SEQ_OPM) 
 REFERENCES TASY.ORDEM_PRODUCAO_OPM (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_AGENDA) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGEINTE_AGTRGAT_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.AGEINT_REGIAO_ATEND (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_MEDICO_FK2 
 FOREIGN KEY (CD_MEDICO_EXEC) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT AGEINTE_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT AGEINTE_AGIFRIN_FK 
 FOREIGN KEY (NR_SEQ_FORMA_INDICACAO) 
 REFERENCES TASY.AGEINT_FORMA_INDICACAO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AGEINTE_ORCPACI_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO) 
 REFERENCES TASY.ORCAMENTO_PACIENTE (NR_SEQUENCIA_ORCAMENTO),
  CONSTRAINT AGEINTE_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PLS) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_PLSASEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_ATEND) 
 REFERENCES TASY.PLS_ATENDIMENTO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_AGIMOTC_FK 
 FOREIGN KEY (NR_SEQ_MOT_CANCEL) 
 REFERENCES TASY.AGEINT_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT AGEINTE_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLICITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT AGEINTE_TICLAPA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CLASSIF_PAC) 
 REFERENCES TASY.TIPO_CLASSIFICAO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_CONVCOB_FK 
 FOREIGN KEY (NR_SEQ_COBERTURA) 
 REFERENCES TASY.CONVENIO_COBERTURA (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_CONVENI_FK2 
 FOREIGN KEY (CD_CONVENIO_ANT) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGEINTE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINTE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGEINTE_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT AGEINTE_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGEINTE_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_MPRPACI_FK 
 FOREIGN KEY (NR_SEQ_PARTIC_CICLO_ITEM) 
 REFERENCES TASY.MPREV_PARTIC_CICLO_ITEM (NR_SEQUENCIA),
  CONSTRAINT AGEINTE_TRANSSO_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_TRANSPORTE) 
 REFERENCES TASY.TRANS_SOLICITACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_INTEGRADA TO NIVEL_1;

