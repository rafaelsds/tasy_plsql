ALTER TABLE TASY.ESCALA_PIM2
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PIM2 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PIM2
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  DT_AVALIACAO            DATE                  NOT NULL,
  DT_LIBERACAO            DATE,
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  IE_ADM_ELETIVA          VARCHAR2(1 BYTE),
  QT_PTO_ADM_ELETIVA      NUMBER(15,4)          NOT NULL,
  IE_PROC_RECUPERACAO     VARCHAR2(1 BYTE),
  QT_PTO_PROC_RECUP       NUMBER(15,4)          NOT NULL,
  IE_CIRURGIA_CARDIACA    VARCHAR2(1 BYTE),
  QT_PTO_CIRUR_CARD       NUMBER(15,4)          NOT NULL,
  IE_DIAG_ALTO_RISCO      VARCHAR2(1 BYTE),
  QT_PTO_DIAG_ALTO_RISCO  NUMBER(15,4)          NOT NULL,
  IE_RESPOSTA_PUPILAS     VARCHAR2(1 BYTE),
  QT_PTO_RESP_PUPILAS     NUMBER(15,4)          NOT NULL,
  IE_VENTILACAO_MECANICA  VARCHAR2(1 BYTE),
  QT_PTO_VENT_MEC         NUMBER(15,4)          NOT NULL,
  QT_PA_SITOLICA          NUMBER(3),
  QT_PTO_PA_SISTOLICA     NUMBER(16,5)          NOT NULL,
  QT_PTO_PAO2_FIO2        NUMBER(15,4)          NOT NULL,
  QT_PH_SANGUE            NUMBER(15,4),
  QT_PTO_PH_SANGUE        NUMBER(15,4)          NOT NULL,
  QT_TX_MORTALIDADE       NUMBER(5,1)           NOT NULL,
  IE_DIAG_BAIXO_RISCO     VARCHAR2(1 BYTE),
  QT_REL_PAO2             NUMBER(5,1),
  QT_REL_FIO2             NUMBER(5,1),
  CD_PERFIL_ATIVO         NUMBER(5),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  NR_HORA                 NUMBER(2),
  NR_SEQ_REG_ELEMENTO     NUMBER(10),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAPIM_ATEPACI_FK_I ON TASY.ESCALA_PIM2
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAPIM_EHRREEL_FK_I ON TASY.ESCALA_PIM2
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAPIM_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAPIM_I1 ON TASY.ESCALA_PIM2
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAPIM_PERFIL_FK_I ON TASY.ESCALA_PIM2
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAPIM_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAPIM_PESFISI_FK_I ON TASY.ESCALA_PIM2
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAPIM_PK ON TASY.ESCALA_PIM2
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_pim2_atual
before insert or update ON TASY.ESCALA_PIM2 for each row
declare
logit_w				number(16,5) := 0;
qt_pto_valores_w		number(10,0) := 0;
qt_pontuacao_w			number(16,5) := 0;
qt_fio2_pao2_w			number(16,5) := 0;
qt_reg_w	number(1);
begin

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Pontua��o da admiss�o eletiva */
if	(:new.ie_adm_eletiva = 'N') then
	:new.qt_pto_adm_eletiva	:= 0;
elsif	(:new.ie_adm_eletiva = 'S') then
	:new.qt_pto_adm_eletiva := -0.9282;
else
	:new.qt_pto_adm_eletiva := 0;
end if;

/* Pontua��o do procedimento de recupera��o */
if	(:new.ie_proc_recuperacao = 'N') then
	:new.qt_pto_proc_recup := 0;
elsif	(:new.ie_proc_recuperacao = 'S') then
	:new.qt_pto_proc_recup := -1.0244;
else
	:new.qt_pto_proc_recup := 0;
end if;

/* Pontua��o da cirurgia card�aca */
if	(:new.ie_cirurgia_cardiaca = 'N') then
	:new.qt_pto_cirur_card := 0;
elsif	(:new.ie_cirurgia_cardiaca = 'S') then
	:new.qt_pto_cirur_card := 0.7507;
else
	:new.qt_pto_cirur_card := 0;
end if;

/* Pontua��o de diagn�stico de alto risco */
if	(:new.ie_diag_alto_risco = '0') and
	(:new.ie_diag_baixo_risco is null) then
	:new.qt_pto_diag_alto_risco := 0;
elsif	(:new.ie_diag_baixo_risco is null) and
	((:new.ie_diag_alto_risco = '1') or
	(:new.ie_diag_alto_risco = '2') or
	(:new.ie_diag_alto_risco = '3') or
	(:new.ie_diag_alto_risco = '4') or
	(:new.ie_diag_alto_risco = '5') or
	(:new.ie_diag_alto_risco = '6') or
	(:new.ie_diag_alto_risco = '7') or
	(:new.ie_diag_alto_risco = '8') or
	(:new.ie_diag_alto_risco = '9')) then
	:new.qt_pto_diag_alto_risco := 1.6829;
elsif	(:new.ie_diag_baixo_risco = '0') and
	(:new.ie_diag_alto_risco is null) then
	:new.qt_pto_diag_alto_risco := 0;
elsif	(:new.ie_diag_alto_risco is null) and
	((:new.ie_diag_baixo_risco = '1') or
	(:new.ie_diag_baixo_risco = '2') or
	(:new.ie_diag_baixo_risco = '3') or
	(:new.ie_diag_baixo_risco = '4') or
	(:new.ie_diag_baixo_risco = '5')) then
	:new.qt_pto_diag_alto_risco := -1.5770;
else
	:new.qt_pto_diag_alto_risco := 0;
end if;

/* Pontua��o das pupilas */
if	(:new.ie_resposta_pupilas = 'N') then
	:new.qt_pto_resp_pupilas	:= 0;
elsif	(:new.ie_resposta_pupilas = 'S') then
	:new.qt_pto_resp_pupilas	:= 3.0791;
else
	:new.qt_pto_resp_pupilas	:= 0;
end if;

/* Pontua��o da ventila��o mec�nica */
if	(:new.ie_ventilacao_mecanica = 'N') then
	:new.qt_pto_vent_mec	:= 0;
elsif	(:new.ie_ventilacao_mecanica = 'S') then
	:new.qt_pto_vent_mec	:= 1.3352;
else
	:new.qt_pto_vent_mec	:= 0;
end if;

qt_fio2_pao2_w := dividir((:new.qt_rel_fio2 * 100), :new.qt_rel_pao2);

:new.qt_pto_pa_sistolica := 0.01395;

:new.qt_pto_ph_sangue := 0.1040;

:new.qt_pto_pao2_fio2 := 0.2888;

if	(:new.ie_adm_eletiva = 'S') or
	(:new.ie_proc_recuperacao = 'S') or
	(:new.ie_cirurgia_cardiaca = 'S') or
	(:new.ie_diag_alto_risco = 'A') or
	(:new.ie_diag_alto_risco = 'B') or
	(:new.ie_resposta_pupilas = 'S') or
	(:new.ie_ventilacao_mecanica = 'S') then
	qt_pto_valores_w	:= 1;
else
	qt_pto_valores_w	:= 0;
end if;

qt_pontuacao_w		:=	nvl(:new.qt_pto_adm_eletiva,0)	+	nvl(:new.qt_pto_proc_recup,0)		+
				nvl(:new.qt_pto_cirur_card,0)	+	nvl(:new.qt_pto_diag_alto_risco,0)	+
				nvl(:new.qt_pto_resp_pupilas,0)	+	nvl(:new.qt_pto_vent_mec,0);


logit_w := (-4.8841) + (qt_pto_valores_w * qt_pontuacao_w) + nvl((0.01395 * (abs(:new.qt_pa_sitolica - 120))),0) +
		nvl((0.1040 * abs(:new.qt_ph_sangue)),0) + (0.2888 * trunc((qt_fio2_pao2_w),1));

:new.qt_tx_mortalidade := dividir(exp(logit_w), 1 + exp(logit_w)) * 100;

<<Final>>
qt_reg_w	:= 0;

exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(206726);
end;

end;
/


ALTER TABLE TASY.ESCALA_PIM2 ADD (
  CONSTRAINT ESCAPIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PIM2 ADD (
  CONSTRAINT ESCAPIM_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCAPIM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAPIM_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAPIM_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_PIM2 TO NIVEL_1;

