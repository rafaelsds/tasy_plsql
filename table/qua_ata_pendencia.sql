ALTER TABLE TASY.QUA_ATA_PENDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_ATA_PENDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_ATA_PENDENCIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ATA            NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_PENDENCIA          VARCHAR2(255 BYTE)      NOT NULL,
  DT_PREV_SOLUCAO       DATE                    NOT NULL,
  CD_PESSOA_RESP        VARCHAR2(10 BYTE),
  DT_SOLUCAO_REAL       DATE,
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  NR_SEQ_ORDEM_SERVICO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAAPEN_MANORSE_FK_I ON TASY.QUA_ATA_PENDENCIA
(NR_SEQ_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPEN_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAAPEN_PESFISI_FK_I ON TASY.QUA_ATA_PENDENCIA
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUAAPEN_PK ON TASY.QUA_ATA_PENDENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUAAPEN_QUAATA_FK_I ON TASY.QUA_ATA_PENDENCIA
(NR_SEQ_ATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPEN_QUAATA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUA_ATA_PENDENCIA ADD (
  CONSTRAINT QUAAPEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_ATA_PENDENCIA ADD (
  CONSTRAINT QUAAPEN_QUAATA_FK 
 FOREIGN KEY (NR_SEQ_ATA) 
 REFERENCES TASY.QUA_ATA (NR_SEQUENCIA),
  CONSTRAINT QUAAPEN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUAAPEN_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_ATA_PENDENCIA TO NIVEL_1;

