ALTER TABLE TASY.SSJ_APP_DICAS_SAUDE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SSJ_APP_DICAS_SAUDE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SSJ_APP_DICAS_SAUDE
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  DT_ATUALIZACAO  DATE,
  DT_EXPIRACAO    DATE,
  DS_TITULO       VARCHAR2(255 BYTE),
  DS_DICA         VARCHAR2(1000 BYTE),
  BL_IMAGEM       BLOB,
  NM_IMAGEM       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (BL_IMAGEM) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.SSJ_APP_DICAS_SAUDE ADD (
  PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT DELETE, INSERT, SELECT ON TASY.SSJ_APP_DICAS_SAUDE TO APPSSJ;

GRANT SELECT ON TASY.SSJ_APP_DICAS_SAUDE TO NIVEL_1;

