ALTER TABLE TASY.SMMOWNERLINKS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SMMOWNERLINKS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SMMOWNERLINKS
(
  MOWNERID              NUMBER(10),
  MOWNERTYPE            NUMBER(10),
  MOWNEEID              NUMBER(10),
  MOWNEETYPE            NUMBER(10),
  ASSOCIATION_ID_       NUMBER(10),
  ASSOCIATION_REMARKS_  VARCHAR2(256 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SMMOWNEEINDEX ON TASY.SMMOWNERLINKS
(MOWNEEID, MOWNEETYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SMMOWNEEINDEX
  MONITORING USAGE;


CREATE INDEX TASY.SMMOWNERINDEX ON TASY.SMMOWNERLINKS
(MOWNERID, MOWNERTYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SMMOWNERINDEX
  MONITORING USAGE;


ALTER TABLE TASY.SMMOWNERLINKS ADD (
  PRIMARY KEY
 (MOWNERID, MOWNEEID, ASSOCIATION_ID_, MOWNERTYPE, MOWNEETYPE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SMMOWNERLINKS TO NIVEL_1;

