ALTER TABLE TASY.INTERFACEAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERFACEAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERFACEAMENTO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  DS_INTEGRACAO                  VARCHAR2(255 BYTE) NOT NULL,
  NR_SEQ_XML_ENVIO               NUMBER(10),
  NR_SEQ_XML_RETORNO             NUMBER(10),
  CD_INTERFACE_ENVIO             NUMBER(5),
  CD_INTERFACE_RETORNO           NUMBER(5),
  QT_TEMPO_INTERFACE             NUMBER(5),
  QT_TEMPO_IMPORTACAO            NUMBER(5),
  DS_DIRETORIO_GERADOS           VARCHAR2(255 BYTE),
  DS_DIRETORIO_PROCURA           VARCHAR2(255 BYTE),
  DS_DIRETORIO_IMPORTADOS        VARCHAR2(255 BYTE),
  DS_DIRETORIO_ERRO              VARCHAR2(255 BYTE),
  IE_USUARIO_EQUIPAPMENTO        VARCHAR2(1 BYTE),
  IE_IGNORAR_PONTUACAO           VARCHAR2(1 BYTE),
  IE_LOGIN_ALTERNATIVO           VARCHAR2(1 BYTE),
  IE_ESTABELECIMENTO_PRESCRICAO  VARCHAR2(255 BYTE),
  IE_XML                         VARCHAR2(1 BYTE) NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTERFAC_ESTABEL_FK_I ON TASY.INTERFACEAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTERFAC_INTERFA_FK_I ON TASY.INTERFACEAMENTO
(CD_INTERFACE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTERFAC_INTERFA__RET_FK_I ON TASY.INTERFACEAMENTO
(CD_INTERFACE_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTERFAC_PK ON TASY.INTERFACEAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTERFAC_XMLPROJ_RET_FK_I ON TASY.INTERFACEAMENTO
(NR_SEQ_XML_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTERFACEAMENTO ADD (
  CONSTRAINT INTERFAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERFACEAMENTO ADD (
  CONSTRAINT INTERFAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT INTERFAC_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE_ENVIO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT INTERFAC_INTERFA__RET_FK 
 FOREIGN KEY (CD_INTERFACE_RETORNO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE));

GRANT SELECT ON TASY.INTERFACEAMENTO TO NIVEL_1;

