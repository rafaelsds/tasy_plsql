ALTER TABLE TASY.CM_CONJUNTO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CONJUNTO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CONJUNTO_ITEM
(
  NR_SEQ_CONJUNTO      NUMBER(10),
  NR_SEQ_ITEM          NUMBER(10),
  QT_ITEM              NUMBER(9,2)              NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_CODIGO            VARCHAR2(100 BYTE),
  IE_INDISPENSAVEL     VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_INTERNO       NUMBER(10),
  DT_SUBST_FIXO        DATE,
  NM_USUARIO_FIXO      VARCHAR2(15 BYTE),
  NR_SEQ_CONJ_FIXO     NUMBER(10),
  IE_STATUS_ITEM       VARCHAR2(15 BYTE),
  NR_SEQ_ITEM_FIXO     NUMBER(10),
  NR_SEQ_APRESENTACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCOITE_CMCONJU_FK_I ON TASY.CM_CONJUNTO_ITEM
(NR_SEQ_CONJUNTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOITE_CMCONJU_FK2_I ON TASY.CM_CONJUNTO_ITEM
(NR_SEQ_CONJ_FIXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOITE_CMCONJU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOITE_CMITEM_FK_I ON TASY.CM_CONJUNTO_ITEM
(NR_SEQ_ITEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOITE_CMITEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOITE_CMITEM_FK2_I ON TASY.CM_CONJUNTO_ITEM
(NR_SEQ_ITEM_FIXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOITE_CMITEM_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCOITE_PK ON TASY.CM_CONJUNTO_ITEM
(NR_SEQ_CONJUNTO, NR_SEQ_ITEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOITE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_conjunto_item_insert
before insert ON TASY.CM_CONJUNTO_ITEM for each row
declare

nr_seq_item_w	cm_conjunto_item.nr_seq_item%type;
ie_unico_w		cm_item.ie_unico%type;
qt_reg_con_w	number(20);

pragma autonomous_transaction;

begin

	select 	max(nvl(ie_unico,'N'))
	into	ie_unico_w
	from 	cm_item
	where	nr_sequencia = :new.nr_seq_item;

	select 	count(nr_seq_item)
	into	nr_seq_item_w
	from 	cm_conjunto_item
	where	nr_seq_item = :new.nr_seq_item;

	if ((ie_unico_w = 'S') and (nr_seq_item_w > 0)) then
		--Este item � �nico e j� est� associado em outro conjunto
		wheb_mensagem_pck.exibir_mensagem_abort(1035195);
	end if;

	select	count(*)
	into	qt_reg_con_w
	from	cm_conjunto_cont
	where	ie_status_conjunto not in (10,6)
	and		nr_seq_item = :new.nr_seq_item;

	if	(nvl(qt_reg_con_w ,0) > 0) then
		--N�o � poss�vel inserir o item no conjunto. O item j� est� em processo de esteriliza��o individual.
		wheb_mensagem_pck.exibir_mensagem_abort(1035492);
	end if;

end;
/


ALTER TABLE TASY.CM_CONJUNTO_ITEM ADD (
  CONSTRAINT CMCOITE_PK
 PRIMARY KEY
 (NR_SEQ_CONJUNTO, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CONJUNTO_ITEM ADD (
  CONSTRAINT CMCOITE_CMCONJU_FK 
 FOREIGN KEY (NR_SEQ_CONJUNTO) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCOITE_CMITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.CM_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCOITE_CMCONJU_FK2 
 FOREIGN KEY (NR_SEQ_CONJ_FIXO) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMCOITE_CMITEM_FK2 
 FOREIGN KEY (NR_SEQ_ITEM_FIXO) 
 REFERENCES TASY.CM_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_CONJUNTO_ITEM TO NIVEL_1;

