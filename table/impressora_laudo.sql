ALTER TABLE TASY.IMPRESSORA_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMPRESSORA_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMPRESSORA_LAUDO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_COMPUTADOR     NUMBER(10),
  DS_CAMINHO_IMAGEM     VARCHAR2(500 BYTE),
  DS_CAMINHO_TEXTO      VARCHAR2(500 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPR_LAUDO_COMPUTA_FK_I ON TASY.IMPRESSORA_LAUDO
(NR_SEQ_COMPUTADOR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPR_LAUDO_ESTABEL_FK_I ON TASY.IMPRESSORA_LAUDO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPR_LAUDO_PK ON TASY.IMPRESSORA_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPR_LAUDO_SETATEN_FK_I ON TASY.IMPRESSORA_LAUDO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPR_LAUDO_UK ON TASY.IMPRESSORA_LAUDO
(CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO, NR_SEQ_COMPUTADOR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMPRESSORA_LAUDO ADD (
  CONSTRAINT IMPR_LAUDO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT IMPR_LAUDO_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO, NR_SEQ_COMPUTADOR)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.IMPRESSORA_LAUDO ADD (
  CONSTRAINT IMPR_LAUDO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT IMPR_LAUDO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT IMPR_LAUDO_COMPUTA_FK 
 FOREIGN KEY (NR_SEQ_COMPUTADOR) 
 REFERENCES TASY.COMPUTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.IMPRESSORA_LAUDO TO NIVEL_1;

