ALTER TABLE TASY.TISS_REGRA_PREST_EQUIPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_PREST_EQUIPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_PREST_EQUIPE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  IE_TISS_TIPO_GUIA       VARCHAR2(2 BYTE),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  SG_CONSELHO             VARCHAR2(20 BYTE)     NOT NULL,
  NR_CONSELHO             VARCHAR2(20 BYTE)     NOT NULL,
  UF_CONSELHO             VARCHAR2(20 BYTE)     NOT NULL,
  IE_GRAU_PARTIC          VARCHAR2(20 BYTE)     NOT NULL,
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_CBOS                 VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRPE_CONVENI_FK_I ON TASY.TISS_REGRA_PREST_EQUIPE
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRPE_ESTABEL_FK_I ON TASY.TISS_REGRA_PREST_EQUIPE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRPE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRPE_PESJURI_FK_I ON TASY.TISS_REGRA_PREST_EQUIPE
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRPE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRPE_PK ON TASY.TISS_REGRA_PREST_EQUIPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRPE_SETATEN_FK_I ON TASY.TISS_REGRA_PREST_EQUIPE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRPE_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_PREST_EQUIPE_tp  after update ON TASY.TISS_REGRA_PREST_EQUIPE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TISS_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_TISS_TIPO_GUIA,1,4000),substr(:new.IE_TISS_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TISS_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.CD_CGC_PRESTADOR,1,4000),substr(:new.CD_CGC_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_PRESTADOR',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.SG_CONSELHO,1,500);gravar_log_alteracao(substr(:old.SG_CONSELHO,1,4000),substr(:new.SG_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'SG_CONSELHO',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_SETOR_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.UF_CONSELHO,1,500);gravar_log_alteracao(substr(:old.UF_CONSELHO,1,4000),substr(:new.UF_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'UF_CONSELHO',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GRAU_PARTIC,1,500);gravar_log_alteracao(substr(:old.IE_GRAU_PARTIC,1,4000),substr(:new.IE_GRAU_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_PARTIC',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RESPONSAVEL_CREDITO,1,500);gravar_log_alteracao(substr(:old.IE_RESPONSAVEL_CREDITO,1,4000),substr(:new.IE_RESPONSAVEL_CREDITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESPONSAVEL_CREDITO',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_CONSELHO,1,500);gravar_log_alteracao(substr(:old.NR_CONSELHO,1,4000),substr(:new.NR_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONSELHO',ie_log_w,ds_w,'TISS_REGRA_PREST_EQUIPE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_PREST_EQUIPE ADD (
  CONSTRAINT TISSRPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_PREST_EQUIPE ADD (
  CONSTRAINT TISSRPE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSRPE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRPE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT TISSRPE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.TISS_REGRA_PREST_EQUIPE TO NIVEL_1;

