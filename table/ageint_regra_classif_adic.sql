ALTER TABLE TASY.AGEINT_REGRA_CLASSIF_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_REGRA_CLASSIF_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_REGRA_CLASSIF_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  HR_INICIO            DATE,
  HR_FIM               DATE,
  DT_FIM_VIGENCIA      DATE,
  DT_INICIO_VIGENCIA   DATE,
  IE_CLASSIF_AGENDA    VARCHAR2(5 BYTE)         NOT NULL,
  IE_TIPO_CONVENIO     NUMBER(2),
  CD_CONVENIO          NUMBER(5),
  NR_TEMPO_LIB         NUMBER(10)               NOT NULL,
  CD_AGENDA            NUMBER(10),
  CD_MEDICO            VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGRCLAD_AGENDA_FK_I ON TASY.AGEINT_REGRA_CLASSIF_ADIC
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRCLAD_CONVENI_FK_I ON TASY.AGEINT_REGRA_CLASSIF_ADIC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRCLAD_ESTABEL_FK_I ON TASY.AGEINT_REGRA_CLASSIF_ADIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRCLAD_PESFISI_FK_I ON TASY.AGEINT_REGRA_CLASSIF_ADIC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGRCLAD_PK ON TASY.AGEINT_REGRA_CLASSIF_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEINT_REGRA_CLASSIF_ADIC ADD (
  CONSTRAINT AGRCLAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGEINT_REGRA_CLASSIF_ADIC ADD (
  CONSTRAINT AGRCLAD_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT AGRCLAD_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGRCLAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGRCLAD_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGEINT_REGRA_CLASSIF_ADIC TO NIVEL_1;

