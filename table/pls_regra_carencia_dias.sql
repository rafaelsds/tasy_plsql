ALTER TABLE TASY.PLS_REGRA_CARENCIA_DIAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_CARENCIA_DIAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_CARENCIA_DIAS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA          NUMBER(10)              NOT NULL,
  NR_SEQ_TIPO_CARENCIA  NUMBER(10)              NOT NULL,
  QT_DIAS               NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRECD_PK ON TASY.PLS_REGRA_CARENCIA_DIAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECD_PLSRECI_FK_I ON TASY.PLS_REGRA_CARENCIA_DIAS
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRECD_PLSRECI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRECD_PLSTICA_FK_I ON TASY.PLS_REGRA_CARENCIA_DIAS
(NR_SEQ_TIPO_CARENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRECD_PLSTICA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_CARENCIA_DIAS ADD (
  CONSTRAINT PLSRECD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_CARENCIA_DIAS ADD (
  CONSTRAINT PLSRECD_PLSRECI_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_CARENCIA_ISENCAO (NR_SEQUENCIA),
  CONSTRAINT PLSRECD_PLSTICA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CARENCIA) 
 REFERENCES TASY.PLS_TIPO_CARENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_CARENCIA_DIAS TO NIVEL_1;

