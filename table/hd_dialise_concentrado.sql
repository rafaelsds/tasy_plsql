ALTER TABLE TASY.HD_DIALISE_CONCENTRADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE_CONCENTRADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE_CONCENTRADO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ATUALIZA_ESTOQUE  VARCHAR2(1 BYTE),
  CD_LOTE_FORNECEDOR   NUMBER(10),
  NR_SEQ_DIALISE       NUMBER(10),
  CD_MATERIAL          NUMBER(6),
  DT_INCLUSAO          DATE                     NOT NULL,
  DT_CONTA             DATE,
  NR_SEQ_MAQUINA       NUMBER(10),
  NR_SEQ_PONTO_ACESSO  NUMBER(10),
  DT_RETIRADA          DATE,
  CD_PF_INCLUSAO       VARCHAR2(10 BYTE),
  CD_PF_RETIRADA       VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDICON_HDDIALE_FK_I ON TASY.HD_DIALISE_CONCENTRADO
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDICON_HDMAQDI_FK_I ON TASY.HD_DIALISE_CONCENTRADO
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDICON_HDPONAC_FK_I ON TASY.HD_DIALISE_CONCENTRADO
(NR_SEQ_PONTO_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDICON_HDPONAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDICON_MATERIA_FK_I ON TASY.HD_DIALISE_CONCENTRADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDICON_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDICON_PESFISI_FK_I ON TASY.HD_DIALISE_CONCENTRADO
(CD_PF_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDICON_PESFISI_FK2_I ON TASY.HD_DIALISE_CONCENTRADO
(CD_PF_RETIRADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDDICON_PK ON TASY.HD_DIALISE_CONCENTRADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_dial_concentrado_befup_sign
before update ON TASY.HD_DIALISE_CONCENTRADO for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado = :new.nr_sequencia
and 	a.ie_opcao = 'MD'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_ponto_acesso	<> :new.nr_seq_ponto_acesso
		or :old.ie_atualiza_estoque	<> :new.ie_atualiza_estoque
		or :old.cd_lote_fornecedor	<> :new.cd_lote_fornecedor
		or :old.cd_material		<> :new.cd_material
		or :old.dt_conta		<> :new.dt_conta
		or :old.dt_inclusao		<> :new.dt_inclusao
		or :old.cd_pf_inclusao		<> :new.cd_pf_inclusao
		or (:old.nr_seq_maquina 	is null		and :new.nr_seq_maquina 	is not null)
		or (:old.nr_seq_ponto_acesso 	is null		and :new.nr_seq_ponto_acesso 	is not null)
		or (:old.ie_atualiza_estoque 	is null		and :new.ie_atualiza_estoque 	is not null)
		or (:old.cd_lote_fornecedor 	is null		and :new.cd_lote_fornecedor 	is not null)
		or (:old.cd_material 		is null		and :new.cd_material 		is not null)
		or (:old.dt_conta 		is null		and :new.dt_conta 		is not null)
		or (:old.dt_inclusao 		is null		and :new.dt_inclusao 		is not null)
		or (:old.cd_pf_inclusao 	is null		and :new.cd_pf_inclusao 	is not null)
		or (:old.nr_seq_maquina 	is not null	and :new.nr_seq_maquina 	is null)
		or (:old.nr_seq_ponto_acesso 	is not null	and :new.nr_seq_ponto_acesso 	is null)
		or (:old.ie_atualiza_estoque 	is not null	and :new.ie_atualiza_estoque 	is null)
		or (:old.cd_lote_fornecedor 	is not null	and :new.cd_lote_fornecedor 	is null)
		or (:old.cd_material 		is not null	and :new.cd_material 		is null)
		or (:old.dt_conta 		is not null	and :new.dt_conta 		is null)
		or (:old.dt_inclusao 		is not null	and :new.dt_inclusao 		is null)
		or (:old.cd_pf_inclusao 	is not null	and :new.cd_pf_inclusao 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado_ret = :new.nr_sequencia
and 	a.ie_opcao = 'C'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada	<> :new.dt_retirada
		or :old.cd_pf_retirada	<> :new.cd_pf_retirada
		or (:old.dt_retirada 	is null		and :new.dt_retirada 	is not null)
		or (:old.cd_pf_retirada is null		and :new.cd_pf_retirada is not null)
		or (:old.dt_retirada 	is not null	and :new.dt_retirada 	is null)
		or (:old.cd_pf_retirada	is not null	and :new.cd_pf_retirada is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado = :new.nr_sequencia
and 	a.ie_opcao = 'IC'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_ponto_acesso	<> :new.nr_seq_ponto_acesso
		or :old.ie_atualiza_estoque	<> :new.ie_atualiza_estoque
		or :old.cd_lote_fornecedor	<> :new.cd_lote_fornecedor
		or :old.cd_material		<> :new.cd_material
		or :old.nr_seq_dialise		<> :new.nr_seq_dialise
		or :old.dt_inclusao		<> :new.dt_inclusao
		or :old.cd_pf_inclusao		<> :new.cd_pf_inclusao
		or (:old.nr_seq_maquina 	is null		and :new.nr_seq_maquina 	is not null)
		or (:old.nr_seq_ponto_acesso 	is null		and :new.nr_seq_ponto_acesso 	is not null)
		or (:old.ie_atualiza_estoque 	is null		and :new.ie_atualiza_estoque 	is not null)
		or (:old.cd_lote_fornecedor 	is null		and :new.cd_lote_fornecedor 	is not null)
		or (:old.cd_material 		is null		and :new.cd_material 		is not null)
		or (:old.nr_seq_dialise		is null		and :new.nr_seq_dialise		is not null)
		or (:old.dt_inclusao 		is null		and :new.dt_inclusao 		is not null)
		or (:old.cd_pf_inclusao 	is null		and :new.cd_pf_inclusao 	is not null)
		or (:old.nr_seq_maquina 	is not null	and :new.nr_seq_maquina 	is null)
		or (:old.nr_seq_ponto_acesso 	is not null	and :new.nr_seq_ponto_acesso 	is null)
		or (:old.ie_atualiza_estoque 	is not null	and :new.ie_atualiza_estoque 	is null)
		or (:old.cd_lote_fornecedor 	is not null	and :new.cd_lote_fornecedor 	is null)
		or (:old.cd_material 		is not null	and :new.cd_material 		is null)
		or (:old.nr_seq_dialise		is not null	and :new.nr_seq_dialise		is null)
		or (:old.dt_inclusao 		is not null	and :new.dt_inclusao 		is null)
		or (:old.cd_pf_inclusao 	is not null	and :new.cd_pf_inclusao 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado = :new.nr_sequencia
and 	a.ie_opcao = 'SD'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_ponto_acesso	<> :new.nr_seq_ponto_acesso
		or :old.ie_atualiza_estoque	<> :new.ie_atualiza_estoque
		or :old.cd_lote_fornecedor	<> :new.cd_lote_fornecedor
		or :old.cd_material		<> :new.cd_material
		or :old.dt_conta		<> :new.dt_conta
		or :old.dt_inclusao		<> :new.dt_inclusao
		or :old.cd_pf_inclusao		<> :new.cd_pf_inclusao
		or (:old.nr_seq_maquina 	is null		and :new.nr_seq_maquina 	is not null)
		or (:old.nr_seq_ponto_acesso 	is null		and :new.nr_seq_ponto_acesso 	is not null)
		or (:old.ie_atualiza_estoque 	is null		and :new.ie_atualiza_estoque 	is not null)
		or (:old.cd_lote_fornecedor 	is null		and :new.cd_lote_fornecedor 	is not null)
		or (:old.cd_material 		is null		and :new.cd_material 		is not null)
		or (:old.dt_conta		is null		and :new.dt_conta		is not null)
		or (:old.dt_inclusao 		is null		and :new.dt_inclusao 		is not null)
		or (:old.cd_pf_inclusao 	is null		and :new.cd_pf_inclusao 	is not null)
		or (:old.nr_seq_maquina 	is not null	and :new.nr_seq_maquina 	is null)
		or (:old.nr_seq_ponto_acesso 	is not null	and :new.nr_seq_ponto_acesso 	is null)
		or (:old.ie_atualiza_estoque 	is not null	and :new.ie_atualiza_estoque 	is null)
		or (:old.cd_lote_fornecedor 	is not null	and :new.cd_lote_fornecedor 	is null)
		or (:old.cd_material 		is not null	and :new.cd_material 		is null)
		or (:old.dt_conta		is not null	and :new.dt_conta		is null)
		or (:old.dt_inclusao 		is not null	and :new.dt_inclusao 		is null)
		or (:old.cd_pf_inclusao 	is not null	and :new.cd_pf_inclusao 	is null)) then
			wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado_ret = :new.nr_sequencia
and 	a.ie_opcao = 'SC'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada	<> :new.dt_retirada
		or :old.cd_pf_retirada	<> :new.cd_pf_retirada
		or (:old.dt_retirada 	is null		and :new.dt_retirada 	is not null)
		or (:old.cd_pf_retirada is null		and :new.cd_pf_retirada is not null)
		or (:old.dt_retirada 	is not null	and :new.dt_retirada 	is null)
		or (:old.cd_pf_retirada	is not null	and :new.cd_pf_retirada is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado = :new.nr_sequencia
and 	a.ie_opcao = 'SC'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_ponto_acesso	<> :new.nr_seq_ponto_acesso
		or :old.ie_atualiza_estoque	<> :new.ie_atualiza_estoque
		or :old.cd_lote_fornecedor	<> :new.cd_lote_fornecedor
		or :old.cd_material		<> :new.cd_material
		or :old.nr_seq_dialise		<> :new.nr_seq_dialise
		or :old.dt_inclusao		<> :new.dt_inclusao
		or :old.cd_pf_inclusao		<> :new.cd_pf_inclusao
		or (:old.nr_seq_maquina 	is null		and :new.nr_seq_maquina 	is not null)
		or (:old.nr_seq_ponto_acesso 	is null		and :new.nr_seq_ponto_acesso 	is not null)
		or (:old.ie_atualiza_estoque 	is null		and :new.ie_atualiza_estoque 	is not null)
		or (:old.cd_lote_fornecedor 	is null		and :new.cd_lote_fornecedor 	is not null)
		or (:old.cd_material 		is null		and :new.cd_material 		is not null)
		or (:old.nr_seq_dialise		is null		and :new.nr_seq_dialise		is not null)
		or (:old.dt_inclusao 		is null		and :new.dt_inclusao 		is not null)
		or (:old.cd_pf_inclusao 	is null		and :new.cd_pf_inclusao 	is not null)
		or (:old.nr_seq_maquina 	is not null	and :new.nr_seq_maquina 	is null)
		or (:old.nr_seq_ponto_acesso 	is not null	and :new.nr_seq_ponto_acesso 	is null)
		or (:old.ie_atualiza_estoque 	is not null	and :new.ie_atualiza_estoque 	is null)
		or (:old.cd_lote_fornecedor 	is not null	and :new.cd_lote_fornecedor 	is null)
		or (:old.cd_material 		is not null	and :new.cd_material 		is null)
		or (:old.nr_seq_dialise		is not null	and :new.nr_seq_dialise		is null)
		or (:old.dt_inclusao 		is not null	and :new.dt_inclusao 		is null)
		or (:old.cd_pf_inclusao 	is not null	and :new.cd_pf_inclusao 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado_ret = :new.nr_sequencia
and 	a.ie_opcao = 'RC'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada	<> :new.dt_retirada
		or :old.cd_pf_retirada	<> :new.cd_pf_retirada
		or (:old.dt_retirada 	is null		and :new.dt_retirada 	is not null)
		or (:old.cd_pf_retirada is null		and :new.cd_pf_retirada is not null)
		or (:old.dt_retirada 	is not null	and :new.dt_retirada 	is null)
		or (:old.cd_pf_retirada	is not null	and :new.cd_pf_retirada is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and 	b.nr_seq_concentrado_ret = :new.nr_sequencia
and 	a.ie_opcao = 'FD'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada	<> :new.dt_retirada
		or :old.cd_pf_retirada	<> :new.cd_pf_retirada
		or (:old.dt_retirada 	is null		and :new.dt_retirada 	is not null)
		or (:old.cd_pf_retirada is null		and :new.cd_pf_retirada is not null)
		or (:old.dt_retirada 	is not null	and :new.dt_retirada 	is null)
		or (:old.cd_pf_retirada	is not null	and :new.cd_pf_retirada is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_DIALISE_CONCENTRADO ADD (
  CONSTRAINT HDDICON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE_CONCENTRADO ADD (
  CONSTRAINT HDDICON_PESFISI_FK2 
 FOREIGN KEY (CD_PF_RETIRADA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDICON_PESFISI_FK 
 FOREIGN KEY (CD_PF_INCLUSAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDICON_HDPONAC_FK 
 FOREIGN KEY (NR_SEQ_PONTO_ACESSO) 
 REFERENCES TASY.HD_PONTO_ACESSO (NR_SEQUENCIA),
  CONSTRAINT HDDICON_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDICON_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT HDDICON_HDMAQDI_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.HD_MAQUINA_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE_CONCENTRADO TO NIVEL_1;

