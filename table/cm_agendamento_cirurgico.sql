ALTER TABLE TASY.CM_AGENDAMENTO_CIRURGICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_AGENDAMENTO_CIRURGICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_AGENDAMENTO_CIRURGICO
(
  NR_SEQUENCIA          NUMBER(10),
  DT_ATUALIZACAO        DATE,
  NM_MEDICO             VARCHAR2(255 BYTE),
  DS_LOGIN              VARCHAR2(50 BYTE),
  NM_PACIENTE           VARCHAR2(255 BYTE),
  DT_NASCIMENTO         DATE,
  NM_MAE                VARCHAR2(255 BYTE),
  DS_CONVENIO           VARCHAR2(255 BYTE),
  DT_INTERNACAO         DATE,
  DT_CIRURGIA           DATE,
  HR_INICIO             VARCHAR2(8 BYTE),
  DS_CID                VARCHAR2(255 BYTE),
  DS_CIRURGIA_PROPOSTA  VARCHAR2(255 BYTE),
  NR_DURACAO            NUMBER(10),
  DS_LATERALIDADE       VARCHAR2(255 BYTE),
  IE_RESERVA_HEMO       VARCHAR2(1 BYTE),
  IE_RESERVA_UTI        VARCHAR2(1 BYTE),
  IE_ALERGIA            VARCHAR2(1 BYTE),
  DS_MATERIAIS          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.CM_AGENDAMENTO_CIRURGICO ADD (
  CONSTRAINT CM_CK_RESERVA_HEMO
 CHECK (ie_reserva_hemo in('S','N')),
  CONSTRAINT CM_CK_RESERVA_UTI
 CHECK (ie_reserva_uti in('S','N')),
  CONSTRAINT CM_CK_ALERGIA
 CHECK (ie_alergia in('S','N')),
  PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_AGENDAMENTO_CIRURGICO TO NIVEL_1;

