ALTER TABLE TASY.MPREV_CAMP_GRUPO_COLETIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_CAMP_GRUPO_COLETIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_CAMP_GRUPO_COLETIVO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CAMPANHA        NUMBER(10)             NOT NULL,
  NR_SEQ_GRUPO_COLETIVO  NUMBER(10)             NOT NULL,
  DT_INCLUSAO            DATE                   NOT NULL,
  DT_EXCLUSAO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRCAGC_MPRCAMP_FK_I ON TASY.MPREV_CAMP_GRUPO_COLETIVO
(NR_SEQ_CAMPANHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAGC_MPRCAMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRCAGC_MPRGRUC_FK_I ON TASY.MPREV_CAMP_GRUPO_COLETIVO
(NR_SEQ_GRUPO_COLETIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAGC_MPRGRUC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPRCAGC_PK ON TASY.MPREV_CAMP_GRUPO_COLETIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAGC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MPREV_CAMP_GRUPO_COL_ATUAL
before insert or update ON TASY.MPREV_CAMP_GRUPO_COLETIVO FOR EACH ROW
DECLARE

BEGIN

if	(trunc(:new.dt_exclusao) < trunc(:new.dt_inclusao)) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(830880);
end if;

END;
/


ALTER TABLE TASY.MPREV_CAMP_GRUPO_COLETIVO ADD (
  CONSTRAINT MPRCAGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_CAMP_GRUPO_COLETIVO ADD (
  CONSTRAINT MPRCAGC_MPRCAMP_FK 
 FOREIGN KEY (NR_SEQ_CAMPANHA) 
 REFERENCES TASY.MPREV_CAMPANHA (NR_SEQUENCIA),
  CONSTRAINT MPRCAGC_MPRGRUC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_COLETIVO) 
 REFERENCES TASY.MPREV_GRUPO_COLETIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_CAMP_GRUPO_COLETIVO TO NIVEL_1;

