ALTER TABLE TASY.PLS_ANALISE_PROCEDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ANALISE_PROCEDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ANALISE_PROCEDIMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SOLICITACAO   NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAPRO_PK ON TASY.PLS_ANALISE_PROCEDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAPRO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAPRO_PLSASOL_FK_I ON TASY.PLS_ANALISE_PROCEDIMENTO
(NR_SEQ_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAPRO_PLSASOL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAPRO_PROCEDI_FK_I ON TASY.PLS_ANALISE_PROCEDIMENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAPRO_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_ANALISE_PROCEDIMENTO ADD (
  CONSTRAINT PLSAPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ANALISE_PROCEDIMENTO ADD (
  CONSTRAINT PLSAPRO_PLSASOL_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO) 
 REFERENCES TASY.PLS_ANALISE_SOLICITACAO (NR_SEQUENCIA),
  CONSTRAINT PLSAPRO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_ANALISE_PROCEDIMENTO TO NIVEL_1;

