DROP TABLE TASY.W_VIPE_HORARIOS_T CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_VIPE_HORARIOS_T
(
  DT_HORARIO  DATE,
  NM_USUARIO  VARCHAR2(15 BYTE)                 NOT NULL
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_VIPE_HORARIOS_T TO NIVEL_1;

