ALTER TABLE TASY.AGENDA_REGIAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_REGIAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_REGIAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_AGENDA            NUMBER(10)               NOT NULL,
  NR_SEQ_REGIAO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEREGI_AGENDA_FK_I ON TASY.AGENDA_REGIAO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGI_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGI_AGTRGAT_FK_I ON TASY.AGENDA_REGIAO
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGI_AGTRGAT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGEREGI_PK ON TASY.AGENDA_REGIAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGI_PK
  MONITORING USAGE;


ALTER TABLE TASY.AGENDA_REGIAO ADD (
  CONSTRAINT AGEREGI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_REGIAO ADD (
  CONSTRAINT AGEREGI_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT AGEREGI_AGTRGAT_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.AGEINT_REGIAO_ATEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_REGIAO TO NIVEL_1;

