ALTER TABLE TASY.FUNCAO_POPUP_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_POPUP_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_POPUP_REGRA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NM_USUARIO_REGRA            VARCHAR2(15 BYTE),
  CD_PERFIL                   NUMBER(5),
  CD_FUNCAO                   NUMBER(5),
  NR_SEQ_OBJETO               NUMBER(10)        NOT NULL,
  IE_ENABLE                   VARCHAR2(1 BYTE)  NOT NULL,
  IE_VISIBLE                  VARCHAR2(1 BYTE)  NOT NULL,
  IE_TIPO_REGRA               VARCHAR2(1 BYTE),
  NR_SEQ_APRESENT             NUMBER(10),
  NR_SEQ_LOTE_PAR_REGRA_ITEM  NUMBER(10),
  IE_PADRAO                   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNPORE_DICOBJE_FK_I ON TASY.FUNCAO_POPUP_REGRA
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPORE_ESTABEL_FK_I ON TASY.FUNCAO_POPUP_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNPORE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FUNPORE_FUNCAO_FK_I ON TASY.FUNCAO_POPUP_REGRA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPORE_I1 ON TASY.FUNCAO_POPUP_REGRA
(IE_TIPO_REGRA, NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPORE_PERFIL_FK_I ON TASY.FUNCAO_POPUP_REGRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNPORE_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FUNPORE_PK ON TASY.FUNCAO_POPUP_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPORE_USUARIO_FK_I ON TASY.FUNCAO_POPUP_REGRA
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.funcao_popup_regra_atual
before insert or update ON TASY.FUNCAO_POPUP_REGRA for each row
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	funcao_popup_regra
	where	nr_seq_objeto		= :new.nr_seq_objeto
	and	((cd_estabelecimento	= :new.cd_estabelecimento) 	or (cd_estabelecimento is null 	and :new.cd_estabelecimento is null))
	and	((cd_perfil		= :new.cd_perfil) 		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((nm_usuario_regra	= :new.nm_usuario_regra) 	or (nm_usuario_regra is null 	and :new.nm_usuario_regra is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/


ALTER TABLE TASY.FUNCAO_POPUP_REGRA ADD (
  CONSTRAINT FUNPORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_POPUP_REGRA ADD (
  CONSTRAINT FUNPORE_DICOBJE_FK 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT FUNPORE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FUNPORE_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT FUNPORE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT FUNPORE_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.FUNCAO_POPUP_REGRA TO NIVEL_1;

