ALTER TABLE TASY.PLS_CONTA_COPARTIC_APROP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_COPARTIC_APROP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_COPARTIC_APROP
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_CONTA_COPARTICIPACAO  NUMBER(10)       NOT NULL,
  NR_SEQ_CENTRO_APROPRIACAO    NUMBER(10)       NOT NULL,
  VL_APROPRIACAO               NUMBER(15,2)     NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_PAGADOR               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCCAP_PK ON TASY.PLS_CONTA_COPARTIC_APROP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCCAP_PLCONPAG_FK_I ON TASY.PLS_CONTA_COPARTIC_APROP
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCCAP_PLSCEAP_FK_I ON TASY.PLS_CONTA_COPARTIC_APROP
(NR_SEQ_CENTRO_APROPRIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCCAP_PLSCOCOP_FK_I ON TASY.PLS_CONTA_COPARTIC_APROP
(NR_SEQ_CONTA_COPARTICIPACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conta_copart_aprop_alt_pag
before insert ON TASY.PLS_CONTA_COPARTIC_APROP 
for each row
declare

nr_seq_pagador_conta_copart_w	pls_contrato_pagador.nr_sequencia%type;
nr_seq_pag_conta_copar_aprop_w	pls_contrato_pagador.nr_sequencia%type;

begin

nr_seq_pagador_conta_copart_w	:= null;

select	max(b.nr_seq_pagador)
into	nr_seq_pagador_conta_copart_w
from	pls_conta_coparticipacao a,
	pls_segurado b
where	b.nr_sequencia = a.nr_seq_segurado
and	a.nr_sequencia = :new.nr_seq_conta_coparticipacao;

if	(nr_seq_pagador_conta_copart_w is not null) then
	select	max(nr_seq_pagador_item)
	into	nr_seq_pag_conta_copar_aprop_w
	from	pls_pagador_item_mens
	where	nr_seq_pagador = nr_seq_pagador_conta_copart_w
	and	ie_tipo_item = '3'
	and	nr_seq_centro_apropriacao = :new.nr_seq_centro_apropriacao;

	if	(nr_seq_pag_conta_copar_aprop_w is null) then
		select	max(nr_seq_pagador_item)
		into	nr_seq_pag_conta_copar_aprop_w
		from	pls_pagador_item_mens
		where	nr_seq_pagador = nr_seq_pagador_conta_copart_w
		and	ie_tipo_item = '3'
		and	nr_seq_centro_apropriacao is null;

		if	(nr_seq_pag_conta_copar_aprop_w is null) then
			nr_seq_pag_conta_copar_aprop_w	:= nr_seq_pagador_conta_copart_w;
		else
			nr_seq_pagador_conta_copart_w	:= nr_seq_pag_conta_copar_aprop_w;
		end if;
	else
		nr_seq_pagador_conta_copart_w	:= nr_seq_pag_conta_copar_aprop_w;
	end if;
end if;

:new.nr_seq_pagador := nr_seq_pagador_conta_copart_w;

end pls_conta_copart_aprop_alt_pag;
/


ALTER TABLE TASY.PLS_CONTA_COPARTIC_APROP ADD (
  CONSTRAINT PLSCCAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_COPARTIC_APROP ADD (
  CONSTRAINT PLSCCAP_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCCAP_PLSCEAP_FK 
 FOREIGN KEY (NR_SEQ_CENTRO_APROPRIACAO) 
 REFERENCES TASY.PLS_CENTRO_APROPRIACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCCAP_PLSCOCOP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_COPARTICIPACAO) 
 REFERENCES TASY.PLS_CONTA_COPARTICIPACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CONTA_COPARTIC_APROP TO NIVEL_1;

