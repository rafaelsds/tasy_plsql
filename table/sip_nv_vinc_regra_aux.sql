DROP TABLE TASY.SIP_NV_VINC_REGRA_AUX CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.SIP_NV_VINC_REGRA_AUX
(
  NR_SEQ_NV_DADOS    NUMBER(10)                 NOT NULL,
  IE_TIPO_CASAMENTO  VARCHAR2(2 BYTE)           NOT NULL
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.SNVVRAU_I1 ON TASY.SIP_NV_VINC_REGRA_AUX
(NR_SEQ_NV_DADOS, IE_TIPO_CASAMENTO);


GRANT SELECT ON TASY.SIP_NV_VINC_REGRA_AUX TO NIVEL_1;

