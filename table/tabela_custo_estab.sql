ALTER TABLE TASY.TABELA_CUSTO_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_CUSTO_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_CUSTO_ESTAB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_TIPO_TABELA_CUSTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TABCUSEST_PK ON TASY.TABELA_CUSTO_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABCUSEST_PK
  MONITORING USAGE;


CREATE INDEX TASY.TABCUSEST_TIPTABCU_FK_I ON TASY.TABELA_CUSTO_ESTAB
(CD_TIPO_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABCUSEST_TIPTABCU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TABELA_CUSTO_ESTAB ADD (
  CONSTRAINT TABCUSEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_CUSTO_ESTAB ADD (
  CONSTRAINT TABCUSEST_TIPTABCU_FK 
 FOREIGN KEY (CD_TIPO_TABELA_CUSTO) 
 REFERENCES TASY.TIPO_TABELA_CUSTO (CD_TIPO_TABELA_CUSTO));

GRANT SELECT ON TASY.TABELA_CUSTO_ESTAB TO NIVEL_1;

