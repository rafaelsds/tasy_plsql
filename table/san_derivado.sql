ALTER TABLE TASY.SAN_DERIVADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_DERIVADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_DERIVADO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DS_DERIVADO              VARCHAR2(50 BYTE)    NOT NULL,
  SG_SIGLA                 VARCHAR2(5 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  QT_DIAS_VALIDADE         NUMBER(4)            NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  IE_TIPO_DERIVADO         VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  QT_VOLUME                NUMBER(15,3),
  QT_VARIACAO              NUMBER(15,3),
  IE_HEMOGLOBINA           VARCHAR2(1 BYTE),
  IE_HEMATOCRITO           VARCHAR2(1 BYTE),
  IE_PLAQUETAS             VARCHAR2(1 BYTE),
  IE_TAP                   VARCHAR2(1 BYTE),
  QT_ESTOQUE_MINIMO        NUMBER(13,4),
  NR_SEQ_PROC_INTERNO      NUMBER(10),
  IE_MOSTRA_PRESCRICAO     VARCHAR2(1 BYTE),
  IE_ADM_POOL              VARCHAR2(2 BYTE),
  CD_CODIGO_VIGILANCIA     VARCHAR2(10 BYTE),
  QT_TEMP_ARMAZENAMENTO    NUMBER(7,3),
  NR_BOLSA                 VARCHAR2(50 BYTE),
  NR_SEQ_APRESENT          NUMBER(10),
  IE_CONTA_ADEP            VARCHAR2(1 BYTE),
  QT_DIAS_ANT_VALIDADE     NUMBER(4)            NOT NULL,
  DS_OBSERVACAO_EXAME      VARCHAR2(255 BYTE),
  IE_INR                   VARCHAR2(1 BYTE),
  IE_IRRADIADO             VARCHAR2(1 BYTE),
  IE_VIA_APLICACAO         VARCHAR2(10 BYTE),
  IE_PERMITE_ALIQUOTADO    VARCHAR2(1 BYTE),
  IE_PERMITE_FILTRADO      VARCHAR2(1 BYTE),
  IE_PERMITE_IRRADIADO     VARCHAR2(1 BYTE),
  IE_PERMITE_LAVADO        VARCHAR2(1 BYTE),
  IE_FIBRINOGENIO          VARCHAR2(1 BYTE),
  QT_DENSIDADE_HEMO        NUMBER(10,3),
  NR_SEQ_TIPO_PG           NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4),
  IE_TIPO_HEMOCOMPONENTE   VARCHAR2(3 BYTE),
  IE_OBRIGA_AMBOS          VARCHAR2(1 BYTE),
  QT_ARMAZENAMENTO_QUAL    NUMBER(5),
  IE_ARMAZENAMENTO_CQ      VARCHAR2(1 BYTE),
  IE_OBRIGA_TTPA           VARCHAR2(1 BYTE),
  IE_OBRIGA_ALBUMINA       VARCHAR2(1 BYTE),
  IE_OBRIGA_CALCIO         VARCHAR2(1 BYTE),
  IE_OBRIGA_MAGNESIO       VARCHAR2(1 BYTE),
  IE_OBRIGA_BILI_DIR       VARCHAR2(1 BYTE),
  IE_OBRIGA_BILI_IND       VARCHAR2(1 BYTE),
  IE_OBRIGA_HEMOGLOBINA_S  VARCHAR2(1 BYTE),
  IE_OBRIGA_COOMBS         VARCHAR2(1 BYTE),
  IE_OBRIGA_COAGULOPATIA   VARCHAR2(1 BYTE),
  IE_MOSTRA_SOLICITADOS    VARCHAR2(1 BYTE),
  CD_INTERVALO             VARCHAR2(7 BYTE),
  IE_EXIGE_BARRAS          VARCHAR2(1 BYTE),
  DS_COR                   VARCHAR2(15 BYTE),
  IE_ALTERAR_QUANTIDADE    VARCHAR2(1 BYTE),
  CD_EXTERNO               VARCHAR2(40 BYTE),
  IE_BOLSA_PEDIATRICA      VARCHAR2(1 BYTE),
  IE_REPRODUCAO_AUTO       VARCHAR2(1 BYTE),
  QT_HORA_INFUSAO          NUMBER(2),
  QT_TEMPO_INFUSAO         NUMBER(5),
  IE_CONTA_RESERVA         VARCHAR2(1 BYTE),
  IE_CONTA_TRANSFUSAO      VARCHAR2(1 BYTE),
  IE_PROCED_HEMOTERAPICO   VARCHAR2(1 BYTE),
  IE_PERMITE_AFERESE       VARCHAR2(1 BYTE),
  IE_PERMITE_POOL          VARCHAR2(1 BYTE),
  IE_PERMITE_FENOTIPADO    VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_LOC      VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANDERI_ESTABEL_FK_I ON TASY.SAN_DERIVADO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDERI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANDERI_PK ON TASY.SAN_DERIVADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDERI_PROCEDI_FK_I ON TASY.SAN_DERIVADO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDERI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDERI_PROINTE_FK_I ON TASY.SAN_DERIVADO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDERI_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDERI_TIPPEGA_FK_I ON TASY.SAN_DERIVADO
(NR_SEQ_TIPO_PG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDERI_TIPPEGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAN_DERIV_SEARCH ON TASY.SAN_DERIVADO
(SUBSTRB("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_DERIVADO"),1,4000))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.san_derivado_insert_update
before insert or update ON TASY.SAN_DERIVADO for each row
declare

begin

if	(:new.CD_PROCEDIMENTO is null) and
	(:new.NR_SEQ_PROC_INTERNO is null) then
	exibir_erro_abortar(obter_desc_expressao(937263) || chr(13) || chr(10),null);
end if;

end;
/


ALTER TABLE TASY.SAN_DERIVADO ADD (
  CONSTRAINT SANDERI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_DERIVADO ADD (
  CONSTRAINT SANDERI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANDERI_TIPPEGA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PG) 
 REFERENCES TASY.TIPO_PERDA_GANHO (NR_SEQUENCIA),
  CONSTRAINT SANDERI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT SANDERI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.SAN_DERIVADO TO NIVEL_1;

