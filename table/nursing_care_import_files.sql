ALTER TABLE TASY.NURSING_CARE_IMPORT_FILES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NURSING_CARE_IMPORT_FILES CASCADE CONSTRAINTS;

CREATE TABLE TASY.NURSING_CARE_IMPORT_FILES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10)               NOT NULL,
  DS_NAME_FILE         VARCHAR2(100 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUCAIMFI_NUCAIM_FK_I ON TASY.NURSING_CARE_IMPORT_FILES
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NUCAIMFI_PK ON TASY.NURSING_CARE_IMPORT_FILES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NURSING_CARE_IMPORT_FILES ADD (
  CONSTRAINT NUCAIMFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NURSING_CARE_IMPORT_FILES ADD (
  CONSTRAINT NUCAIMFI_NUCAIM_FK 
 FOREIGN KEY (NR_SEQ_IMPORT) 
 REFERENCES TASY.NURSING_CARE_IMPORT (NR_SEQUENCIA)
    ON DELETE CASCADE);

