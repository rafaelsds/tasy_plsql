ALTER TABLE TASY.MAN_REGRA_ORCAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_REGRA_ORCAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_REGRA_ORCAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOCALIZACAO   NUMBER(10)               NOT NULL,
  DT_INICIAL           DATE                     NOT NULL,
  DT_FINAL             DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_LIBERACAO         DATE,
  DT_INATIVACAO        DATE,
  DS_MENSAGEM          VARCHAR2(2000 BYTE),
  DS_INFORMACOES       VARCHAR2(4000 BYTE),
  IE_MOMENTO_REGRA     VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANREGRAOR_MANLOCA_FK_I ON TASY.MAN_REGRA_ORCAMENTO
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANREGRAOR_PK ON TASY.MAN_REGRA_ORCAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_regra_orcamento_insert_upd
before insert or update ON TASY.MAN_REGRA_ORCAMENTO for each row
declare

qt_regra_bloqueio_w	number(10);

BEGIN
if	(:new.ie_momento_regra = 'E') then
	select	count(*)
	into	qt_regra_bloqueio_w
	from	man_regra_orcamento_bloq
	where	nr_seq_orcamento = :new.nr_sequencia
	and		ie_regra_bloqueio <> 'A';

	if (qt_regra_bloqueio_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(457159);
		/*Quando utilizar a op��o "Ao acesar a OS" no campo "Momento regra", o campo "Regra de bloqueio", dever� ter a op��o "Avisar" selecionada. Favor ajustar.*/
	end if;
end if;

END;
/


ALTER TABLE TASY.MAN_REGRA_ORCAMENTO ADD (
  CONSTRAINT MANREGRAOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_REGRA_ORCAMENTO ADD (
  CONSTRAINT MANREGRAOR_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_REGRA_ORCAMENTO TO NIVEL_1;

