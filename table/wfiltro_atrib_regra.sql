ALTER TABLE TASY.WFILTRO_ATRIB_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WFILTRO_ATRIB_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.WFILTRO_ATRIB_REGRA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_WFILTRO              NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  CD_PERFIL                   NUMBER(10),
  NM_USUARIO_REGRA            VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_PAR_REGRA_ITEM  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WFATRREG_DIOBFIL_FK_I ON TASY.WFILTRO_ATRIB_REGRA
(NR_SEQ_WFILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WFATRREG_ESTABEL_FK_I ON TASY.WFILTRO_ATRIB_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFATRREG_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WFATRREG_PERFIL_FK_I ON TASY.WFILTRO_ATRIB_REGRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFATRREG_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WFATRREG_PK ON TASY.WFILTRO_ATRIB_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFATRREG_PK
  MONITORING USAGE;


CREATE INDEX TASY.WFATRREG_USUARIO_FK_I ON TASY.WFILTRO_ATRIB_REGRA
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFATRREG_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.wfiltro_atrib_regra_atual
before insert or update ON TASY.WFILTRO_ATRIB_REGRA for each row
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	wfiltro_atrib_regra
	where	nr_seq_wfiltro		= :new.nr_seq_wfiltro
	and	((cd_estabelecimento	= :new.cd_estabelecimento)	or (cd_estabelecimento is null 	and :new.cd_estabelecimento is null))
	and	((cd_perfil			= :new.cd_perfil)		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((nm_usuario_regra		= :new.nm_usuario_regra)	or (nm_usuario_regra is null 	and :new.nm_usuario_regra is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/


ALTER TABLE TASY.WFILTRO_ATRIB_REGRA ADD (
  CONSTRAINT WFATRREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.WFILTRO_ATRIB_REGRA ADD (
  CONSTRAINT WFATRREG_DIOBFIL_FK 
 FOREIGN KEY (NR_SEQ_WFILTRO) 
 REFERENCES TASY.DIC_OBJETO_FILTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WFATRREG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT WFATRREG_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT WFATRREG_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.WFILTRO_ATRIB_REGRA TO NIVEL_1;

