ALTER TABLE TASY.MAN_REGRA_DATA_FREQUENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_REGRA_DATA_FREQUENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_REGRA_DATA_FREQUENCIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PLANEJ_PREV   NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_GERACAO           DATE,
  HR_GERACAO           VARCHAR2(4 BYTE),
  IE_DATA_HORA         VARCHAR2(1 BYTE)         NOT NULL,
  DT_DIA_SEMANA        NUMBER(2),
  NR_SEQ_EQUIPAMENTO   NUMBER(10),
  DT_INICIO_DESEJADO   DATE,
  DT_HR_GERACAO        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANREDF_MANEQUI_FK_I ON TASY.MAN_REGRA_DATA_FREQUENCIA
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANREDF_MANPLPR_FK_I ON TASY.MAN_REGRA_DATA_FREQUENCIA
(NR_SEQ_PLANEJ_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANREDF_PK ON TASY.MAN_REGRA_DATA_FREQUENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_regra_data_frequencia_atua
before insert or update ON TASY.MAN_REGRA_DATA_FREQUENCIA for each row
declare

begin

begin
if	(nvl(:old.hr_geracao,'X') <> nvl(:new.hr_geracao,'X')) then
	if	(:new.hr_geracao is not null) then
		:new.dt_hr_geracao := pkg_date_utils.get_Time(sysdate,:new.hr_geracao);
	else
		:new.dt_hr_geracao := null;
	end if;
elsif	(nvl(:old.dt_hr_geracao,pkg_date_utils.get_Time('')) <> nvl(:new.dt_hr_geracao,pkg_date_utils.get_Time(''))) then
	:new.hr_geracao := lpad(pkg_date_utils.extract_field('HOUR',:new.dt_hr_geracao),2,0) || lpad(pkg_date_utils.extract_field('MINUTE',:new.dt_hr_geracao),2,0);
end if;

if	(:new.hr_geracao is not null) and
	(:new.dt_hr_geracao is null) then
	:new.dt_hr_geracao := pkg_date_utils.get_Time(sysdate,:new.hr_geracao);
elsif	(:new.dt_hr_geracao is not null) and
	(:new.hr_geracao is null) then
	:new.hr_geracao := lpad(pkg_date_utils.extract_field('HOUR',:new.dt_hr_geracao),2,0) || lpad(pkg_date_utils.extract_field('MINUTE',:new.dt_hr_geracao),2,0);
end if;		

exception
when others then
	null;
end;

end;
/


ALTER TABLE TASY.MAN_REGRA_DATA_FREQUENCIA ADD (
  CONSTRAINT MANREDF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_REGRA_DATA_FREQUENCIA ADD (
  CONSTRAINT MANREDF_MANPLPR_FK 
 FOREIGN KEY (NR_SEQ_PLANEJ_PREV) 
 REFERENCES TASY.MAN_PLANEJ_PREV (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANREDF_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_REGRA_DATA_FREQUENCIA TO NIVEL_1;

