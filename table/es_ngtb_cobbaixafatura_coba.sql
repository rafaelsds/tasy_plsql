ALTER TABLE TASY.ES_NGTB_COBBAIXAFATURA_COBA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ES_NGTB_COBBAIXAFATURA_COBA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ES_NGTB_COBBAIXAFATURA_COBA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  ID_COBA_CD_SEQUENCIAL     NUMBER(10)          NOT NULL,
  COBA_DS_CODIGOCLIENTE     VARCHAR2(17 BYTE)   NOT NULL,
  COBA_DS_CNPJ              VARCHAR2(14 BYTE),
  COBA_DS_CENTROCUSTO       VARCHAR2(100 BYTE)  NOT NULL,
  COBA_DS_FATURA            VARCHAR2(20 BYTE)   NOT NULL,
  COBA_VL_RECEBIDO          NUMBER(11,2)        NOT NULL,
  COBA_VL_RECEBIDOJUROS     NUMBER(11,2),
  COBA_VL_RECEBIDOMULTA     NUMBER(11,2),
  COBA_VL_IMPOSTO           NUMBER(11,2),
  COBA_VL_DESCONTO          NUMBER(11,2),
  COBA_DH_RECEBIMENTO       DATE                NOT NULL,
  COBA_DS_TIPOBAIXA         VARCHAR2(40 BYTE)   NOT NULL,
  COBA_DH_REGISTROTASY      DATE                NOT NULL,
  COBA_DH_LEITURAPLUSOFT    DATE,
  COBA_DS_STATUSINTEGRACAO  VARCHAR2(1 BYTE),
  COBA_DS_MSGERRO           VARCHAR2(500 BYTE),
  COBA_NR_SEQBAIXA          VARCHAR2(10 BYTE)   NOT NULL,
  COBA_IN_TIPOOPERACAO      VARCHAR2(1 BYTE)    NOT NULL,
  COBA_DS_STATUS            VARCHAR2(1 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESCOBBX_PK ON TASY.ES_NGTB_COBBAIXAFATURA_COBA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ES_NGTB_COBBAIXAFATURA_UPDATE
before update ON TASY.ES_NGTB_COBBAIXAFATURA_COBA for each row
declare

ds_log_w	varchar2(4000);

begin

if	(:old.coba_dh_leituraplusoft <> :new.coba_dh_leituraplusoft)then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_dh_leituraplusoft' || ';' || 'VL_ANTIGO=' || to_char(:old.coba_dh_leituraplusoft,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.coba_dh_leituraplusoft,'dd/mm/yyyy hh24:mi:ss')),1,4000);
end if;

if	(:old.coba_dh_recebimento <> :new.coba_dh_recebimento) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_dh_recebimento' || ';' || 'VL_ANTIGO=' || to_char(:old.coba_dh_recebimento,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.coba_dh_recebimento,'dd/mm/yyyy hh24:mi:ss')),1,4000);
end if;

if	(:old.coba_dh_registrotasy <> :new.coba_dh_registrotasy) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_dh_registrotasy' || ';' || 'VL_ANTIGO=' || to_char(:old.coba_dh_registrotasy,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.coba_dh_registrotasy,'dd/mm/yyyy hh24:mi:ss')),1,4000);
end if;

if	(:old.coba_ds_centrocusto <> :new.coba_ds_centrocusto) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_centrocusto' || ';' || 'VL_ANTIGO=' || :old.coba_ds_centrocusto || ';' || 'VL_NOVO=' || :new.coba_ds_centrocusto),1,4000);
end if;

if	(:old.coba_ds_cnpj <> :new.coba_ds_cnpj) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_cnpj' || ';' || 'VL_ANTIGO=' || :old.coba_ds_cnpj || ';' || 'VL_NOVO=' || :new.coba_ds_cnpj),1,4000);
end if;

if	(:old.coba_ds_codigocliente <> :new.coba_ds_codigocliente) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_codigocliente' || ';' || 'VL_ANTIGO=' || :old.coba_ds_codigocliente || ';' || 'VL_NOVO=' || :new.coba_ds_codigocliente),1,4000);
end if;

if	(:old.coba_ds_fatura <> :new.coba_ds_fatura) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_fatura' || ';' || 'VL_ANTIGO=' || :old.coba_ds_fatura || ';' || 'VL_NOVO=' || :new.coba_ds_fatura),1,4000);
end if;

if	(:old.coba_ds_msgerro <> :new.coba_ds_msgerro) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_msgerro' || ';' || 'VL_ANTIGO=' || :old.coba_ds_msgerro || ';' || 'VL_NOVO=' || :new.coba_ds_msgerro),1,4000);
end if;

if	(:old.coba_ds_status <> :new.coba_ds_status) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_status' || ';' || 'VL_ANTIGO=' || :old.coba_ds_status || ';' || 'VL_NOVO=' || :new.coba_ds_status),1,4000);
end if;

if	(:old.coba_ds_statusintegracao <> :new.coba_ds_statusintegracao) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_statusintegracao' || ';' || 'VL_ANTIGO=' || :old.coba_ds_statusintegracao || ';' || 'VL_NOVO=' || :new.coba_ds_statusintegracao),1,4000);
end if;

if	(:old.coba_ds_tipobaixa <> :new.coba_ds_tipobaixa) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_ds_tipobaixa' || ';' || 'VL_ANTIGO=' || :old.coba_ds_tipobaixa || ';' || 'VL_NOVO=' || :new.coba_ds_tipobaixa),1,4000);
end if;

if	(:old.coba_in_tipooperacao <>  :new.coba_in_tipooperacao) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_in_tipooperacao' || ';' || 'VL_ANTIGO=' || :old.coba_in_tipooperacao || ';' || 'VL_NOVO=' || :new.coba_in_tipooperacao),1,4000);
end if;

if	(:old.coba_nr_seqbaixa <> :new.coba_nr_seqbaixa) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_nr_seqbaixa' || ';' || 'VL_ANTIGO=' || :old.coba_nr_seqbaixa || ';' || 'VL_NOVO=' || :new.coba_nr_seqbaixa),1,4000);
end if;

if	(:old.coba_vl_desconto <> :new.coba_vl_desconto) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_vl_desconto' || ';' || 'VL_ANTIGO=' || :old.coba_vl_desconto || ';' || 'VL_NOVO=' || :new.coba_vl_desconto),1,4000);
end if;

if	(:old.coba_vl_imposto <> :new.coba_vl_imposto) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_vl_imposto' || ';' || 'VL_ANTIGO=' || :old.coba_vl_imposto || ';' || 'VL_NOVO=' || :new.coba_vl_imposto),1,4000);
end if;

if	(:old.coba_vl_recebido <> :new.coba_vl_recebido) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_vl_recebido' || ';' || 'VL_ANTIGO=' || :old.coba_vl_recebido || ';' || 'VL_NOVO=' || :new.coba_vl_recebido),1,4000);
end if;

if	(:old.coba_vl_recebidojuros <> :new.coba_vl_recebidojuros) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_vl_recebidojuros' || ';' || 'VL_ANTIGO=' || :old.coba_vl_recebidojuros || ';' || 'VL_NOVO=' || :new.coba_vl_recebidojuros),1,4000);
end if;

if	(:old.coba_vl_recebidomulta <> :new.coba_vl_recebidomulta) then
	ds_log_w	:= substr(ds_log_w || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=coba_vl_recebidomulta' || ';' || 'VL_ANTIGO=' || :old.coba_vl_recebidomulta || ';' || 'VL_NOVO=' || :new.coba_vl_recebidomulta),1,4000);
end if;

GERAR_CRM_COBRANCA_LOG(ds_log_w,'ES_NGTB_ERROINTEGR_ERIN',:new.nm_usuario);

end;
/


ALTER TABLE TASY.ES_NGTB_COBBAIXAFATURA_COBA ADD (
  CONSTRAINT ESCOBBX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ES_NGTB_COBBAIXAFATURA_COBA TO NIVEL_1;

