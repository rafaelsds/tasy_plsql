ALTER TABLE TASY.COM_CLIENTE_PROP_ESTIM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLIENTE_PROP_ESTIM CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLIENTE_PROP_ESTIM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PROPOSTA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_FASE          NUMBER(3)                NOT NULL,
  NR_SEQ_APRES         NUMBER(15)               NOT NULL,
  NR_SEQ_MOD_IMPL      NUMBER(10)               NOT NULL,
  DS_COMPLEMENTO       VARCHAR2(120 BYTE),
  QT_HORA              NUMBER(15)               NOT NULL,
  NR_SEQ_ESTIMATIVA    NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NR_PREDECESSORA      NUMBER(10),
  CD_CONSULTOR         VARCHAR2(10 BYTE),
  CD_COORDENADOR       VARCHAR2(10 BYTE),
  DT_INI_PREV_ESTIM    DATE,
  DT_FIM_PREV_ESTIM    DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLPM_COMCLPR_FK_I ON TASY.COM_CLIENTE_PROP_ESTIM
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLPM_COMCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLPM_COMESTI_FK_I ON TASY.COM_CLIENTE_PROP_ESTIM
(NR_SEQ_ESTIMATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLPM_COMESTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLPM_MODIMPL_FK_I ON TASY.COM_CLIENTE_PROP_ESTIM
(NR_SEQ_MOD_IMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCLPM_PK ON TASY.COM_CLIENTE_PROP_ESTIM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLPM_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_CLIENTE_PROP_ESTIM ADD (
  CONSTRAINT COMCLPM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLIENTE_PROP_ESTIM ADD (
  CONSTRAINT COMCLPM_COMCLPR_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.COM_CLIENTE_PROPOSTA (NR_SEQUENCIA),
  CONSTRAINT COMCLPM_MODIMPL_FK 
 FOREIGN KEY (NR_SEQ_MOD_IMPL) 
 REFERENCES TASY.MODULO_IMPLANTACAO (NR_SEQUENCIA),
  CONSTRAINT COMCLPM_COMESTI_FK 
 FOREIGN KEY (NR_SEQ_ESTIMATIVA) 
 REFERENCES TASY.COM_ESTIMATIVA (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_CLIENTE_PROP_ESTIM TO NIVEL_1;

