ALTER TABLE TASY.LOG_ENVIO_CROSS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_ENVIO_CROSS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_ENVIO_CROSS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_ENVIO           NUMBER(10)             NOT NULL,
  CD_MOTIVO_RETORNO      NUMBER(10),
  DS_MOTIVO_RETORNO      VARCHAR2(4000 BYTE),
  NR_IDENT_MOVIMENTACAO  NUMBER(10),
  NR_IDENT_RECEBIMENTO   NUMBER(10),
  IE_TIPO_LOG            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LECROSS_DECROSS_FK_I ON TASY.LOG_ENVIO_CROSS
(NR_SEQ_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LECROSS_DECROSS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LECROSS_PK ON TASY.LOG_ENVIO_CROSS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LECROSS_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOG_ENVIO_CROSS ADD (
  CONSTRAINT LECROSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_ENVIO_CROSS ADD (
  CONSTRAINT LECROSS_DECROSS_FK 
 FOREIGN KEY (NR_SEQ_ENVIO) 
 REFERENCES TASY.DADOS_ENVIO_CROSS (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_ENVIO_CROSS TO NIVEL_1;

