ALTER TABLE TASY.CIRURGIA_EVENTO_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_EVENTO_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_EVENTO_ADIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_CIRURGIA            NUMBER(10),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  NR_SEQ_TOPOGRAFIA      NUMBER(10),
  NR_ATENDIMENTO         NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PRESSAO             NUMBER(5,1),
  DT_INICIO_EVENTO       DATE,
  DT_FIM_EVENTO          DATE,
  NR_SEQ_TIPO_CLAMP      NUMBER(10),
  NR_SEQ_TIPO_EVENTO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIREVAD_ATEPACI_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIREVAD_CIREVAR_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(NR_SEQ_TIPO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIREVAD_CIRURGI_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIREVAD_PESFISI_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIREVAD_PK ON TASY.CIRURGIA_EVENTO_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIREVAD_TIPOCLA_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(NR_SEQ_TIPO_CLAMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIREVAD_TOPDOR_FK_I ON TASY.CIRURGIA_EVENTO_ADIC
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cirurgia_evento_adic_atual
before insert ON TASY.CIRURGIA_EVENTO_ADIC for each row
declare
ie_tipo_evento_adicional_w		varchar2(2);
qt_existe_inicio_w				number(10);

begin

null;


end;
/


CREATE OR REPLACE TRIGGER TASY.cirurgia_evento_adic_update
after update ON TASY.CIRURGIA_EVENTO_ADIC for each row
declare

ie_iniciar_equipamento_w	cirurgia_evento_adic_regra.ie_iniciar_equipamento%type;

begin

	select 	nvl(max(reg.ie_iniciar_equipamento),'N')
	into	ie_iniciar_equipamento_w
	from	cirurgia_evento_adic_regra reg
	where	reg.nr_sequencia = :old.nr_seq_tipo_evento;

	if (ie_iniciar_equipamento_w = 'S' and :new.dt_inativacao is not null) then
		update   equipamento_cirurgia
		set      dt_inicio = null
		where    ((nr_cirurgia = :old.nr_cirurgia) and (:old.nr_cirurgia is not null))
		and      dt_inicio = :old.dt_liberacao
		and      dt_liberacao is null;
	end if;

	if (ie_iniciar_equipamento_w = 'S' and :new.dt_liberacao is not null and :new.dt_inativacao is null) then
		update   equipamento_cirurgia
		set      dt_inicio = :new.dt_liberacao
		where    ((nr_cirurgia = :old.nr_cirurgia) and (:old.nr_cirurgia is not null))
		and      dt_inicio = :old.dt_inicio_evento
		and      dt_liberacao is null;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_EVENTO_ADIC_tp  after update ON TASY.CIRURGIA_EVENTO_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_CIRURGIA,1,4000),substr(:new.NR_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CIRURGIA',ie_log_w,ds_w,'CIRURGIA_EVENTO_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CIRURGIA_EVENTO_ADIC ADD (
  CONSTRAINT CIREVAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CIRURGIA_EVENTO_ADIC ADD (
  CONSTRAINT CIREVAD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CIREVAD_CIREVAR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EVENTO) 
 REFERENCES TASY.CIRURGIA_EVENTO_ADIC_REGRA (NR_SEQUENCIA),
  CONSTRAINT CIREVAD_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT CIREVAD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIREVAD_TIPOCLA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CLAMP) 
 REFERENCES TASY.TIPO_CLAMPEAMENTO (NR_SEQUENCIA),
  CONSTRAINT CIREVAD_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIRURGIA_EVENTO_ADIC TO NIVEL_1;

