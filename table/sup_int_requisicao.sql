ALTER TABLE TASY.SUP_INT_REQUISICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_REQUISICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_REQUISICAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_FORMA_INTEGRACAO         VARCHAR2(15 BYTE),
  DT_LIBERACAO                DATE,
  DT_LEITURA                  DATE,
  DT_CONFIRMA_INTEGRACAO      DATE,
  NR_REQUISICAO               NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_SOLICITACAO_REQUISICAO   DATE              NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_PESSOA_REQUISITANTE      VARCHAR2(10 BYTE) NOT NULL,
  CD_PESSOA_ATENDENTE         VARCHAR2(10 BYTE),
  CD_PESSOA_SOLICITANTE       VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_ESTABELECIMENTO_DESTINO  NUMBER(4),
  CD_SETOR_ENTREGA            NUMBER(5),
  CD_CENTRO_CUSTO             NUMBER(8),
  DT_BAIXA                    DATE,
  DT_LIB_REQUISICAO           DATE,
  DT_EMISSAO_SETOR            DATE,
  DT_EMISSAO_LOC_ESTOQUE      DATE,
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  DT_APROVACAO                DATE,
  NR_SEQ_JUSTIFICATIVA        NUMBER(10),
  NR_SEQ_ORDEM_SERV           NUMBER(10),
  IE_URGENTE                  VARCHAR2(1 BYTE)  NOT NULL,
  IE_GERACAO                  VARCHAR2(2 BYTE),
  NR_DOCUMENTO_EXTERNO        NUMBER(10),
  NR_SEQ_INV_ROUPA            NUMBER(10),
  NR_SEQ_PROTOCOLO            NUMBER(10),
  NR_ATENDIMENTO              NUMBER(10),
  NR_ADIANTAMENTO             NUMBER(10),
  NM_USUARIO_APROV            VARCHAR2(15 BYTE),
  NM_USUARIO_RECEBEDOR        VARCHAR2(15 BYTE),
  DT_RECEBIMENTO              DATE,
  NR_SEQ_PROJ_GPI             NUMBER(10),
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  IE_STATUS_INTEGRACAO        VARCHAR2(1 BYTE),
  IE_CONVERSAO_DADOS          VARCHAR2(1 BYTE),
  CD_LOCAL_ESTOQUE_DESTINO    VARCHAR2(80 BYTE),
  CD_LOCAL_ESTOQUE            VARCHAR2(80 BYTE) NOT NULL,
  CD_OPERACAO_ESTOQUE         VARCHAR2(80 BYTE) NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINREQ_PK ON TASY.SUP_INT_REQUISICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINREQ_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_requisicao_atual
before  update ON TASY.SUP_INT_REQUISICAO for each row
declare
qt_existe_erros_w	number(10);
nr_seq_nota_w    	number(10);
qt_existe_w        	number(10);
qt_existe_inc_w      	number(10);
nr_requisicao_w    	number(10);


begin

if	(:new.ie_conversao_dados = 'S') then

	if	(:new.cd_estabelecimento is not null) then
		begin
		select	a.cd_estabelecimento
		into	:new.cd_estabelecimento
		from	estabelecimento a
		where	a.cd_interno =  :new.cd_estabelecimento
		and	rownum = 1;
		exception
		when others then
			grava_sup_int_req_consist(:new.nr_sequencia, wheb_mensagem_pck.get_texto(313247));
		end;
	end if;
end if;

select  count(*)
into    qt_existe_w
from    sup_parametro_integracao
where   cd_estabelecimento = :new.cd_estabelecimento
and     ie_evento = 'RM'
and     ie_forma = 'R'
and     ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(:new.ie_forma_integracao = 'R') then
	begin

	if	(:new.ie_conversao_dados = 'S') then

		if	(:new.nm_usuario is not null) then
			begin
			select  cd_pessoa_fisica
			into    :new.cd_pessoa_requisitante
			from    usuario
			where   nm_usuario = :new.nm_usuario
			and	ie_situacao = 'A'
			and	rownum = 1;
			exception
			when others then
				grava_sup_int_req_consist(:new.nr_sequencia, wheb_mensagem_pck.get_texto(313250));
			end;
		end if;

		if	(:new.cd_centro_custo is not null) then
			begin
			select  cd_centro_custo
			into    :new.cd_centro_custo
			from    centro_custo
			where   cd_sistema_contabil = to_char(:new.cd_centro_custo)
			and	rownum = 1;
			exception
			when others then
				grava_sup_int_req_consist(:new.nr_sequencia, wheb_mensagem_pck.get_texto(313254));
			end;
		end if;

		:new.cd_local_estoque := OBTER_CONVERSAO_INTERNA(null, 'LOCAL_ESTOQUE', 'CD_LOCAL_ESTOQUE', :new.cd_local_estoque);

		:new.cd_local_estoque_destino := OBTER_CONVERSAO_INTERNA(null, 'LOCAL_ESTOQUE', 'CD_LOCAL_ESTOQUE', :new.cd_local_estoque_destino);

		:new.cd_operacao_estoque := OBTER_CONVERSAO_INTERNA(null, 'OPERACAO_ESTOQUE', 'CD_OPERACAO_ESTOQUE', :new.cd_operacao_estoque);


	end if;

	:new.dt_leitura  := sysdate;

	--projeto: 6407 requisição de materiais e medicamentos
		--rf 02 ? sgh > tasy - consistir as requisições
	consiste_sup_int_requisicao (	:new.nr_sequencia,
					:new.cd_estabelecimento,
					:new.cd_local_estoque,
					:new.cd_operacao_estoque,
					:new.cd_pessoa_requisitante,
					:new.cd_estabelecimento_destino,
					:new.cd_local_estoque_destino,
					:new.cd_setor_atendimento,
					:new.cd_centro_custo);



	--rf 03 - sgh > tasy - registrar requisição no tasy
	select  count(*)
	into    qt_existe_inc_w
	from    sup_int_req_consist
	where   nr_sequencia = :new.nr_sequencia;

	if	(qt_existe_inc_w = 0) then

		gerar_req_sup_int(	:new.nr_sequencia,
					:new.cd_estabelecimento,
					:new.cd_local_estoque,
					:new.dt_solicitacao_requisicao,
					:new.dt_lib_requisicao,
					:new.cd_operacao_estoque,
					:new.cd_pessoa_requisitante,
					:new.cd_pessoa_solicitante,
					:new.cd_setor_atendimento,
					:new.cd_estabelecimento_destino,
					:new.cd_local_estoque_destino,
					:new.cd_setor_entrega,
					:new.cd_centro_custo,
					:new.ds_observacao,
					:new.nm_usuario,
					nr_requisicao_w,
					:new.nr_documento_externo);

			if 	(nr_requisicao_w is not null) then
				:new.nr_requisicao      := nr_requisicao_w;
				:new.dt_confirma_integracao  := sysdate;

				update  requisicao_material
				set     ie_origem_requisicao = 'SGH'
				where  nr_requisicao = nr_requisicao_w;
			end if;
	end if;
        end;
end if;
end;
/


ALTER TABLE TASY.SUP_INT_REQUISICAO ADD (
  CONSTRAINT SUINREQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_REQUISICAO TO NIVEL_1;

