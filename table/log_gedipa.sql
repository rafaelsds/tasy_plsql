ALTER TABLE TASY.LOG_GEDIPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_GEDIPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_GEDIPA
(
  DT_LOG              DATE,
  NR_LOG              NUMBER(5),
  NM_OBJETO_EXECUCAO  VARCHAR2(50 BYTE),
  NM_OBJETO_CHAMADO   VARCHAR2(50 BYTE),
  DS_PARAMETROS       VARCHAR2(2000 BYTE),
  DS_LOG              VARCHAR2(2000 BYTE),
  NR_SEQUENCIA        NUMBER(10),
  NR_SEQ_PROCESSO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGEDIP_ADEPROC_FK_I ON TASY.LOG_GEDIPA
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGEDIP_I1 ON TASY.LOG_GEDIPA
(NR_LOG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGEDIP_I1
  MONITORING USAGE;


CREATE INDEX TASY.LOGEDIP_I2 ON TASY.LOG_GEDIPA
(DT_LOG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOGEDIP_PK ON TASY.LOG_GEDIPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGEDIP_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOG_GEDIPA ADD (
  CONSTRAINT LOGEDIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_GEDIPA ADD (
  CONSTRAINT LOGEDIP_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOG_GEDIPA TO NIVEL_1;

