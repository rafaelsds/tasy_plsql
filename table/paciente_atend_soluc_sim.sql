DROP TABLE TASY.PACIENTE_ATEND_SOLUC_SIM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATEND_SOLUC_SIM
(
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_CALC_AUT           VARCHAR2(1 BYTE)        NOT NULL,
  QT_TEMPO_APLICACAO    NUMBER(15,4),
  IE_BOMBA_INFUSAO      VARCHAR2(1 BYTE),
  IE_ESQUEMA_ALTERNADO  VARCHAR2(1 BYTE)        NOT NULL,
  NR_AGRUPAMENTO        NUMBER(6)               NOT NULL,
  NR_SEQ_SOLUCAO        NUMBER(10)              NOT NULL,
  NR_SEQ_ATENDIMENTO    NUMBER(10)              NOT NULL,
  IE_ACM                VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.PACIENTE_ATEND_SOLUC_SIM TO NIVEL_1;

