ALTER TABLE TASY.PEP_RITUAL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEP_RITUAL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEP_RITUAL_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RITUAL        NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPRIIT_I1 ON TASY.PEP_RITUAL_ITEM
(NR_SEQUENCIA, NR_SEQ_RITUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRIIT_PEPRITU_FK_I ON TASY.PEP_RITUAL_ITEM
(NR_SEQ_RITUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPRIIT_PK ON TASY.PEP_RITUAL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRIIT_PROITEM_FK_I ON TASY.PEP_RITUAL_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPRIIT_UK ON TASY.PEP_RITUAL_ITEM
(NR_SEQ_RITUAL, NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRIIT_UK
  MONITORING USAGE;


ALTER TABLE TASY.PEP_RITUAL_ITEM ADD (
  CONSTRAINT PEPRIIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PEPRIIT_UK
 UNIQUE (NR_SEQ_RITUAL, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEP_RITUAL_ITEM ADD (
  CONSTRAINT PEPRIIT_PEPRITU_FK 
 FOREIGN KEY (NR_SEQ_RITUAL) 
 REFERENCES TASY.PEP_RITUAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPRIIT_PROITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.PRONTUARIO_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEP_RITUAL_ITEM TO NIVEL_1;

