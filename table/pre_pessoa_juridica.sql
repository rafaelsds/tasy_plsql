ALTER TABLE TASY.PRE_PESSOA_JURIDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRE_PESSOA_JURIDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRE_PESSOA_JURIDICA
(
  CD_CGC                      VARCHAR2(14 BYTE) NOT NULL,
  CD_PORTADOR                 NUMBER(10)        DEFAULT null,
  DS_RAZAO_SOCIAL             VARCHAR2(80 BYTE) NOT NULL,
  CD_TIPO_PORTADOR            NUMBER(5)         DEFAULT null,
  NM_FANTASIA                 VARCHAR2(80 BYTE) NOT NULL,
  CD_CEP                      VARCHAR2(15 BYTE) NOT NULL,
  DS_ENDERECO                 VARCHAR2(40 BYTE) NOT NULL,
  DS_BAIRRO                   VARCHAR2(40 BYTE),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  DS_MUNICIPIO                VARCHAR2(40 BYTE) NOT NULL,
  SG_ESTADO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  NR_TELEFONE                 VARCHAR2(15 BYTE),
  NR_ENDERECO                 VARCHAR2(10 BYTE),
  NR_FAX                      VARCHAR2(15 BYTE),
  DS_EMAIL                    VARCHAR2(60 BYTE),
  NM_PESSOA_CONTATO           VARCHAR2(255 BYTE),
  NR_RAMAL_CONTATO            NUMBER(5),
  NR_INSCRICAO_ESTADUAL       VARCHAR2(20 BYTE),
  CD_TIPO_PESSOA              NUMBER(3)         NOT NULL,
  CD_CONTA_CONTABIL           VARCHAR2(20 BYTE),
  IE_PROD_FABRIC              VARCHAR2(3 BYTE)  NOT NULL,
  NR_INSCRICAO_MUNICIPAL      VARCHAR2(20 BYTE),
  DS_SITE_INTERNET            VARCHAR2(255 BYTE),
  IE_QUALIDADE                VARCHAR2(3 BYTE),
  IE_TIPO_TRIBUTACAO          VARCHAR2(15 BYTE),
  CD_COND_PAGTO               NUMBER(10),
  QT_DIA_PRAZO_ENTREGA        NUMBER(3),
  DS_NOME_ABREV               VARCHAR2(18 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  VL_MINIMO_NF                NUMBER(15,2),
  CD_PF_RESP_TECNICO          VARCHAR2(10 BYTE),
  DS_RESP_TECNICO             VARCHAR2(255 BYTE),
  DS_ORGAO_REG_RESP_TECNICO   VARCHAR2(10 BYTE),
  NR_ALVARA_SANITARIO         VARCHAR2(20 BYTE),
  NR_CERTIFICADO_BOAS_PRAT    VARCHAR2(20 BYTE),
  NR_ALVARA_SANITARIO_MUNIC   VARCHAR2(20 BYTE),
  NR_AUTOR_FUNC               VARCHAR2(20 BYTE),
  DT_VALIDADE_ALVARA_SANIT    DATE,
  DT_VALIDADE_ALVARA_MUNIC    DATE,
  DT_VALIDADE_AUTOR_FUNC      DATE,
  DT_VALIDADE_CERT_BOAS_PRAT  DATE,
  DT_VALIDADE_RESP_TECNICO    DATE,
  CD_ANS                      VARCHAR2(20 BYTE),
  IE_APROVACAO                VARCHAR2(1 BYTE)  NOT NULL,
  CD_REFERENCIA_FORNEC        VARCHAR2(20 BYTE),
  NR_REGISTRO_RESP_TECNICO    VARCHAR2(20 BYTE),
  IE_ALTERAR_SENHA            VARCHAR2(1 BYTE),
  NR_SEQ_PAIS                 NUMBER(10),
  CD_INTERNACIONAL            VARCHAR2(20 BYTE),
  NR_SEQ_IDIOMA               NUMBER(10),
  NR_REGISTRO_PLS             VARCHAR2(20 BYTE),
  NR_CEI                      VARCHAR2(50 BYTE),
  DT_LIBERACAO                DATE,
  NR_SEQ_CNAE                 NUMBER(10),
  NR_SEQ_NAT_JURIDICA         NUMBER(10),
  DT_INATIVACAO               DATE,
  CD_CNES                     VARCHAR2(20 BYTE),
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDI_FAX                  VARCHAR2(3 BYTE),
  NR_DDD_FAX                  VARCHAR2(3 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_SENHA                    VARCHAR2(20 BYTE),
  CD_MUNICIPIO_IBGE           VARCHAR2(6 BYTE),
  NR_SEQ_TIPO_LOGRADOURO      NUMBER(10),
  IE_STATUS_EXPORTAR          VARCHAR2(1 BYTE),
  CD_CGC_MANTENEDORA          VARCHAR2(14 BYTE),
  CD_SISTEMA_ANT              VARCHAR2(20 BYTE),
  IE_TRANSPORTE               VARCHAR2(1 BYTE),
  IE_FORMA_REVISAO            VARCHAR2(15 BYTE),
  DT_ULTIMA_REVISAO           DATE,
  NM_USUARIO_REVISAO          VARCHAR2(15 BYTE),
  DT_INTEGRACAO               DATE,
  CD_CVM                      VARCHAR2(40 BYTE),
  NR_SEQ_TIPO_ASEN            NUMBER(10),
  DT_INTEGRACAO_EXTERNA       DATE,
  CD_CNPJ_RAIZ                VARCHAR2(8 BYTE),
  CD_OPERADORA_EMPRESA        NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DS_OBSERVACAO_COMPL         VARCHAR2(4000 BYTE),
  NR_CCM                      NUMBER(10),
  IE_FORNECEDOR_OPME          VARCHAR2(15 BYTE),
  IE_STATUS_ENVIO             VARCHAR2(15 BYTE),
  NR_SEQ_REGIAO               NUMBER(10),
  IE_EMPREENDEDOR_INDIVIDUAL  VARCHAR2(15 BYTE),
  IE_TIPO_TRIB_MUNICIPAL      VARCHAR2(2 BYTE),
  NR_SEQ_IDENT_CNES           NUMBER(10),
  NR_MATRICULA_CEI            VARCHAR2(15 BYTE),
  CD_CURP                     VARCHAR2(18 BYTE),
  CD_RFC                      VARCHAR2(13 BYTE),
  DS_ORIENTACAO_COBRANCA      VARCHAR2(4000 BYTE),
  DT_CRIACAO                  DATE,
  IE_TIPO_INST_SAUDE          NUMBER(5),
  NR_AUTOR_TRANSP_RESID       VARCHAR2(20 BYTE),
  NR_AUTOR_RECEB_RESID        VARCHAR2(20 BYTE),
  NR_SEQ_PESSOA_ENDERECO      NUMBER(10),
  NR_SEQUENCIA                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.PREPJUR_CATIASS_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_TIPO_ASEN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_CNESIDE_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_CNSTILO_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_CONCONT_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_CONPAGA_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_COND_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_PAIS_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_PESFISI_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_PF_RESP_TECNICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_PESSEND_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE UNIQUE INDEX TASY.PREPJUR_PK ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPJUR_PLSCNAE_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_CNAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_PREPJUR_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_CGC_MANTENEDORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_SUPNATJ_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_NAT_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_SUSMUNI_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_SUSMURE_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_TASYIDI_FK_I ON TASY.PRE_PESSOA_JURIDICA
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PREPJUR_TIPPEJU_FK_I ON TASY.PRE_PESSOA_JURIDICA
(CD_TIPO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE OR REPLACE TRIGGER TASY.PRE_PESSOA_JURIDICA_tp  after update ON TASY.PRE_PESSOA_JURIDICA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CGC);  ds_c_w:=null; ds_w:=substr(:new.IE_APROVACAO,1,500);gravar_log_alteracao(substr(:old.IE_APROVACAO,1,4000),substr(:new.IE_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APROVACAO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_TELEFONE,1,4000),substr(:new.NR_DDD_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_TELEFONE',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_FAX,1,4000),substr(:new.NR_DDD_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_FAX',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_BOAS_PRAT,1,4000),substr(:new.NR_CERTIFICADO_BOAS_PRAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_BOAS_PRAT',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CCM,1,4000),substr(:new.NR_CCM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CCM',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUTOR_FUNC,1,4000),substr(:new.NR_AUTOR_FUNC,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUTOR_FUNC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO_MUNIC,1,4000),substr(:new.NR_ALVARA_SANITARIO_MUNIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO_MUNIC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO,1,4000),substr(:new.NR_ALVARA_SANITARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FANTASIA,1,4000),substr(:new.NM_FANTASIA,1,4000),:new.nm_usuario,nr_seq_w,'NM_FANTASIA',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TRIBUTACAO,1,4000),substr(:new.IE_TIPO_TRIBUTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TRIBUTACAO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROD_FABRIC,1,4000),substr(:new.IE_PROD_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROD_FABRIC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_REVISAO,1,4000),substr(:new.IE_FORMA_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_REVISAO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_RESP_TECNICO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERT_BOAS_PRAT',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_AUTOR_FUNC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_ALVARA_SANIT',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_ALVARA_MUNIC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_ALVARA_MUNIC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_ALVARA_MUNIC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SITE_INTERNET,1,4000),substr(:new.DS_SITE_INTERNET,1,4000),:new.nm_usuario,nr_seq_w,'DS_SITE_INTERNET',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RAZAO_SOCIAL,1,4000),substr(:new.DS_RAZAO_SOCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RAZAO_SOCIAL',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_REG_RESP_TECNICO,1,4000),substr(:new.DS_ORGAO_REG_RESP_TECNICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_REG_RESP_TECNICO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NOME_ABREV,1,4000),substr(:new.DS_NOME_ABREV,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOME_ABREV',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ANS,1,4000),substr(:new.CD_ANS,1,4000),:new.nm_usuario,nr_seq_w,'CD_ANS',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNES,1,4000),substr(:new.CD_CNES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNES',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERNACIONAL,1,4000),substr(:new.CD_INTERNACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNACIONAL',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PF_RESP_TECNICO,1,4000),substr(:new.CD_PF_RESP_TECNICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PF_RESP_TECNICO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PESSOA,1,4000),substr(:new.CD_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PESSOA',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_LOGRADOURO,1,4000),substr(:new.NR_SEQ_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_LOGRADOURO',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_MUNICIPAL,1,4000),substr(:new.NR_INSCRICAO_MUNICIPAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_MUNICIPAL',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_ESTADUAL,1,4000),substr(:new.NR_INSCRICAO_ESTADUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_ESTADUAL',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FAX,1,4000),substr(:new.NR_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_FAX',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_TELEFONE,1,4000),substr(:new.NR_DDI_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_TELEFONE',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_FAX,1,4000),substr(:new.NR_DDI_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_FAX',ie_log_w,ds_w,'PRE_PESSOA_JURIDICA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pre_pj_beforeinsert
before insert ON TASY.PRE_PESSOA_JURIDICA for each row
declare

  v_nr_sequencia number;
  v_cgc varchar2(14);

begin

  if  (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
    -- Verifica se h� regra cadastrada para o tipo de pessoa juridica
    select MAX(nr_sequencia)
      into v_nr_sequencia
      from ( select nr_sequencia
               from regra_pre_cad_pj
              where cd_tipo_pessoa = :new.cd_tipo_pessoa
                and ie_exige_aprov_cadastro = 'S'
              union
             select nr_sequencia
               from regra_pre_cad_pj
              where cd_tipo_pessoa is null
                and ie_exige_aprov_cadastro = 'S' );

    if v_nr_sequencia is null then
      wheb_mensagem_pck.exibir_mensagem_abort(1063717);
    end if;

    -- Verifica se a pessoa jur�dica j� n�o existe na base de dados
    select MAX(cd_cgc) into v_cgc from pessoa_juridica where cd_cgc = :new.cd_cgc;

    if v_cgc is not null then
      wheb_mensagem_pck.exibir_mensagem_abort(1063587);
    end if;
  end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pre_pj_log_aprovacao
after insert or update ON TASY.PRE_PESSOA_JURIDICA for each row
begin

  if wheb_usuario_pck.get_ie_executar_trigger = 'S' then
    if inserting then
    -- Criando novo registro
      insert into pre_pessoa_juridica_log (
                  nr_sequencia
                , nr_seq_pre_pj
                , dt_atualizacao
                , nm_usuario
                , dt_atualizacao_nrec
                , nm_usuario_nrec
                , cd_cgc
                , ds_atributo
                , ds_valor_old
                , ds_valor_new
                )
           values (
                  pre_pessoa_juridica_log_seq.nextval
                , :new.nr_sequencia
                , SYSDATE
                , obter_usuario_ativo
                , SYSDATE
                , obter_usuario_ativo
                , :new.cd_cgc
                , 'IE_APROVACAO'
                , NULL
                , :new.ie_aprovacao );

    end if;

    if updating ('IE_APROVACAO') then
    -- Altera��o do IE_APROVACAO
      insert into pre_pessoa_juridica_log (
                  nr_sequencia
                , nr_seq_pre_pj
                , dt_atualizacao
                , nm_usuario
                , dt_atualizacao_nrec
                , nm_usuario_nrec
                , cd_cgc
                , ds_atributo
                , ds_valor_old
                , ds_valor_new
                )
           values (
                  pre_pessoa_juridica_log_seq.nextval
                , :new.nr_sequencia
                , SYSDATE
                , obter_usuario_ativo
                , SYSDATE
                , obter_usuario_ativo
                , :new.cd_cgc
                , 'IE_APROVACAO'
                , :old.ie_aprovacao
                , :new.ie_aprovacao );

    end if;
  end if;

end;
/


ALTER TABLE TASY.PRE_PESSOA_JURIDICA ADD (
  CONSTRAINT PREPJUR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PRE_PESSOA_JURIDICA ADD (
  CONSTRAINT PREPJUR_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_SUPNATJ_FK 
 FOREIGN KEY (NR_SEQ_NAT_JURIDICA) 
 REFERENCES TASY.SUP_NATUREZA_JURIDICA (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_PESFISI_FK 
 FOREIGN KEY (CD_PF_RESP_TECNICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PREPJUR_CNSTILO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LOGRADOURO) 
 REFERENCES TASY.CNS_TIPO_LOGRADOURO (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PREPJUR_CONPAGA_FK 
 FOREIGN KEY (CD_COND_PAGTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PREPJUR_PLSCNAE_FK 
 FOREIGN KEY (NR_SEQ_CNAE) 
 REFERENCES TASY.PLS_CNAE (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PREPJUR_PREPJUR_FK 
 FOREIGN KEY (CD_CGC_MANTENEDORA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PREPJUR_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PESSOA) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA),
  CONSTRAINT PREPJUR_CATIASS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ASEN) 
 REFERENCES TASY.CAT_TIPO_ASSENTAMENTO (NR_SEQUENCIA),
  CONSTRAINT PREPJUR_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRE_PESSOA_JURIDICA TO NIVEL_1;

