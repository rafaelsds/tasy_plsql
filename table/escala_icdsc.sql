ALTER TABLE TASY.ESCALA_ICDSC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ICDSC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ICDSC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_NIVEL_CONSCIENCIA_ALT  VARCHAR2(3 BYTE)    NOT NULL,
  IE_DESATENCAO             VARCHAR2(1 BYTE),
  IE_DESORIENTACAO          VARCHAR2(1 BYTE),
  IE_ALUCINACAO             VARCHAR2(1 BYTE),
  IE_AGITACAO               VARCHAR2(1 BYTE),
  IE_FALA_EMO_INAP          VARCHAR2(1 BYTE),
  IE_DISTURBIO_SONO         VARCHAR2(1 BYTE),
  IE_FLUTUACAO              VARCHAR2(1 BYTE),
  QT_PONTO                  NUMBER(1),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCICDS_ATEPACI_FK_I ON TASY.ESCALA_ICDSC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCICDS_PESFISI_FK_I ON TASY.ESCALA_ICDSC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCICDS_PK ON TASY.ESCALA_ICDSC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCICDS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_icdsc_atual
before insert or update ON TASY.ESCALA_ICDSC for each row
declare
qt_reg_w	number(1);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.ie_nivel_consciencia_alt = 'D') or
	(:new.ie_nivel_consciencia_alt = 'E') then
	:new.qt_ponto := null;
else
	:new.qt_ponto := 0;

	if	(:new.ie_nivel_consciencia_alt = 'A') or
		(:new.ie_nivel_consciencia_alt = 'C') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_desatencao = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_desorientacao = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_alucinacao = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_agitacao = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_fala_emo_inap = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_disturbio_sono = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;

	if	(:new.ie_flutuacao = 'S') then
		:new.qt_ponto :=	:new.qt_ponto + 1;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

end escala_icdsc_atual;
/


ALTER TABLE TASY.ESCALA_ICDSC ADD (
  CONSTRAINT ESCICDS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ICDSC ADD (
  CONSTRAINT ESCICDS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCICDS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_ICDSC TO NIVEL_1;

