ALTER TABLE TASY.PESSOA_DOCUMENTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_DOCUMENTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_DOCUMENTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_DOCUMENTO     NUMBER(10)               NOT NULL,
  IE_ENTREGUE          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  DT_ENTREGA           DATE,
  DT_VALIDADE          DATE,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NR_SEQ_AGEINT        NUMBER(10),
  DS_TITULO            VARCHAR2(255 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  NR_PRESCRICAO        NUMBER(14),
  IE_DOC_EXTERNO       VARCHAR2(1 BYTE),
  CD_CONVENIO          NUMBER(5),
  NM_USUARIO_EXCLUSAO  VARCHAR2(15 BYTE),
  DT_EXCLUSAO          DATE,
  DS_MOTIVO_EXCLUSAO   VARCHAR2(255 BYTE),
  SI_SPECIAL_LICENSE   VARCHAR2(3 BYTE),
  SI_MEDICAL_LICENSE   VARCHAR2(15 BYTE),
  NR_DOCUMENT          VARCHAR2(255 BYTE),
  DT_START             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESDOCU_AGEINTE_FK_I ON TASY.PESSOA_DOCUMENTACAO
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESDOCU_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESDOCU_CONVENI_FK_I ON TASY.PESSOA_DOCUMENTACAO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESDOCU_I1 ON TASY.PESSOA_DOCUMENTACAO
(CD_PESSOA_FISICA, NR_SEQ_DOCUMENTO, IE_SITUACAO, 1)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESDOCU_PESFISI_FK_I ON TASY.PESSOA_DOCUMENTACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESDOCU_PK ON TASY.PESSOA_DOCUMENTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESDOCU_TIPODOC_FK_I ON TASY.PESSOA_DOCUMENTACAO
(NR_SEQ_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESDOCU_TIPODOC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PESSOA_DOCUMENTACAO_AFTUPDATE
after update ON TASY.PESSOA_DOCUMENTACAO for each row
declare


ie_res_w   				varchar2(1);
cd_pessoa_usuario_w		varchar2(10);
cd_pf_usuario_w			varchar2(10);
ds_param_integ_hl7_w 	varchar2(4000);
cd_estabelecimento_w	number(5);

begin
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if	(:new.IE_ENTREGUE <> :old.IE_ENTREGUE) and (:new.IE_ENTREGUE = 'S' )then


		Select 	nvl(max('S'),'N')
		into	ie_res_w
		from   	conversao_meio_externo b
		where  	nm_tabela = 'PESSOA_DOCUMENTACAO'
		and		nm_atributo = 'IE_DOC_EXTERNO'
		and		b.cd_interno  = :new.NR_SEQ_DOCUMENTO
		and		b.CD_EXTERNO = 'ACTPF'
		and    	b.ie_sistema_externo = 'TERUNIMED16';


		if (ie_res_w = 'S') then

			Select  max(Obter_Pf_Usuario(:new.nm_usuario, 'C')),
					max(wheb_usuario_pck.get_cd_estabelecimento)
			into	cd_pessoa_usuario_w,
					cd_estabelecimento_w
			from    dual;

			if (:new.cd_pessoa_fisica = cd_pessoa_usuario_w) then


					ds_param_integ_hl7_w := 'cd_transacao='              || '00880'|| ';' ||
											'cd_estabelecimento='        || cd_estabelecimento_w|| ';' ||
											'cd_pf_usuario='             || cd_pessoa_usuario_w      || ';' ;


					gravar_agend_integracao(682, ds_param_integ_hl7_w);

			end if;

		end if;


	end if;

end if;

exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_DOCUMENTACAO_tp  after update ON TASY.PESSOA_DOCUMENTACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PESSOA_DOCUMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'PESSOA_DOCUMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'PESSOA_DOCUMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENTREGUE,1,4000),substr(:new.IE_ENTREGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENTREGUE',ie_log_w,ds_w,'PESSOA_DOCUMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DOCUMENTO,1,4000),substr(:new.NR_SEQ_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DOCUMENTO',ie_log_w,ds_w,'PESSOA_DOCUMENTACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_documentacao_befinsert
before insert ON TASY.PESSOA_DOCUMENTACAO for each row
declare
nr_seq_ageint_w	agenda_integrada.nr_sequencia%type;
ie_existe_w	varchar(1);
begin

if (nvl(:new.nr_seq_ageint,0) > 0) then

	select	max(a.nr_seq_agenda_int)
	into	nr_seq_ageint_w
	from	agenda_integrada_item a,
		agenda_paciente b
	where 	a.nr_seq_agenda_exame = b.nr_sequencia
	and	b.nr_sequencia = :new.nr_seq_ageint;

	if (nvl(nr_seq_ageint_w,0) > 0) then

		begin
			select 'S'
			into	ie_existe_w
			from 	agenda_integrada
			where 	nr_sequencia = nr_seq_ageint_w
			and	rownum = 1;

			:new.nr_seq_ageint := nr_seq_ageint_w;

		exception
		when others then
			ie_existe_w := 'N';
		end;

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_documentacao_befupdate
before insert or update ON TASY.PESSOA_DOCUMENTACAO for each row
declare

begin

if (pkg_date_utils.start_of(:new.dt_entrega, 'DAY') > pkg_date_utils.start_of(:new.dt_validade, 'DAY')) then
	wheb_mensagem_pck.exibir_mensagem_abort(1044540);
end if;

end;
/


ALTER TABLE TASY.PESSOA_DOCUMENTACAO ADD (
  CONSTRAINT PESDOCU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_DOCUMENTACAO ADD (
  CONSTRAINT PESDOCU_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PESDOCU_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PESDOCU_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_DOCUMENTO) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT PESDOCU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_DOCUMENTACAO TO NIVEL_1;

