ALTER TABLE TASY.ATEND_PSI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PSI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PSI
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_AVALIACAO            DATE                  NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  QT_ANO                  NUMBER(3)             NOT NULL,
  IE_ENFERMAGEM           VARCHAR2(1 BYTE)      NOT NULL,
  IE_NEOPLASIA            VARCHAR2(1 BYTE)      NOT NULL,
  IE_HEPATICA             VARCHAR2(1 BYTE)      NOT NULL,
  IE_ICC                  VARCHAR2(1 BYTE)      NOT NULL,
  IE_AVC                  VARCHAR2(1 BYTE)      NOT NULL,
  IE_RENAL                VARCHAR2(1 BYTE)      NOT NULL,
  IE_ESTADO_MENTAL        VARCHAR2(1 BYTE)      NOT NULL,
  IE_FR                   VARCHAR2(1 BYTE)      NOT NULL,
  IE_PAS                  VARCHAR2(1 BYTE)      NOT NULL,
  IE_TEMP                 VARCHAR2(1 BYTE)      NOT NULL,
  IE_FC                   VARCHAR2(1 BYTE)      NOT NULL,
  IE_PH_ARTERIAL          VARCHAR2(1 BYTE)      NOT NULL,
  IE_UREIA                VARCHAR2(1 BYTE)      NOT NULL,
  IE_SODIO                VARCHAR2(1 BYTE)      NOT NULL,
  IE_GLICOSE              VARCHAR2(1 BYTE)      NOT NULL,
  IE_HEMATOCRITO          VARCHAR2(1 BYTE)      NOT NULL,
  IE_PO2                  VARCHAR2(1 BYTE)      NOT NULL,
  IE_DERRAME_PLEURAL      VARCHAR2(1 BYTE)      NOT NULL,
  QT_ESC_ANO              NUMBER(3)             NOT NULL,
  QT_ESC_ENF              NUMBER(3)             NOT NULL,
  QT_ESC_NEO              NUMBER(3)             NOT NULL,
  QT_ESC_HEP              NUMBER(3)             NOT NULL,
  QT_ESC_ICC              NUMBER(3)             NOT NULL,
  QT_ESC_AVC              NUMBER(3)             NOT NULL,
  QT_ESC_REN              NUMBER(3)             NOT NULL,
  QT_ESC_MENTAL           NUMBER(3)             NOT NULL,
  QT_ESC_FR               NUMBER(3)             NOT NULL,
  QT_ESC_PAS              NUMBER(3)             NOT NULL,
  QT_ESC_TEMP             NUMBER(3)             NOT NULL,
  QT_ESC_FC               NUMBER(3)             NOT NULL,
  QT_ESC_PH_ARTERIAL      NUMBER(3)             NOT NULL,
  QT_ESC_UREIA            NUMBER(3)             NOT NULL,
  QT_ESC_SODIO            NUMBER(3)             NOT NULL,
  QT_ESC_GLICOSE          NUMBER(3)             NOT NULL,
  QT_ESC_HEMATOCRITO      NUMBER(3)             NOT NULL,
  QT_ESC_PO2              NUMBER(3)             NOT NULL,
  QT_ESC_DERRAME_PLEURAL  NUMBER(3)             NOT NULL,
  QT_ESCORE               NUMBER(3)             NOT NULL,
  PR_RISCO                NUMBER(4,1)           NOT NULL,
  DT_LIBERACAO            DATE,
  CD_PERFIL_ATIVO         NUMBER(5),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  NR_HORA                 NUMBER(2),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPSI_ATEPACI_FK_I ON TASY.ATEND_PSI
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPSI_I1 ON TASY.ATEND_PSI
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPSI_PERFIL_FK_I ON TASY.ATEND_PSI
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPSI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPSI_PESFISI_FK_I ON TASY.ATEND_PSI
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPSI_PK ON TASY.ATEND_PSI
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_PSI_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ATEND_PSI FOR EACH ROW
declare

ie_sexo_w				Varchar2(1);
qt_reg_w	number(1);

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_esc_ano			:= 0;
:new.qt_esc_enf			:= 0;
:new.qt_esc_neo			:= 0;
:new.qt_esc_hep			:= 0;
:new.qt_esc_icc			:= 0;
:new.qt_esc_avc			:= 0;
:new.qt_esc_ren			:= 0;
:new.qt_esc_mental			:= 0;
:new.qt_esc_fr			:= 0;
:new.qt_esc_pas			:= 0;
:new.qt_esc_temp			:= 0;
:new.qt_esc_fc			:= 0;
:new.qt_esc_ph_arterial		:= 0;
:new.qt_esc_ureia			:= 0;
:new.qt_esc_sodio			:= 0;
:new.qt_esc_glicose			:= 0;
:new.qt_esc_hematocrito		:= 0;
:new.qt_esc_po2			:= 0;
:new.qt_esc_derrame_pleural		:= 0;
:new.qt_escore			:= 0;
:new.pr_risco				:= 0;
select	max(ie_sexo)
into	ie_sexo_w
from	atendimento_paciente_v
where	nr_atendimento		= :new.nr_atendimento;
:new.qt_ano				:= nvl(:new.qt_ano,0);

if	(ie_sexo_w = 'F') then
	:new.qt_esc_ano		:= :new.qt_ano - 10;
else
	:new.qt_esc_ano		:= :new.qt_ano;
end if;

if	(:new.ie_enfermagem= 'S') then
	:new.qt_esc_enf		:= 10;
end if;
if	(:new.ie_neoplasia= 'S') then
	:new.qt_esc_neo		:= 30;
end if;
if	(:new.ie_hepatica= 'S') then
	:new.qt_esc_hep		:= 20;
end if;
if	(:new.ie_icc= 'S') then
	:new.qt_esc_icc		:= 10;
end if;
if	(:new.ie_avc= 'S') then
	:new.qt_esc_avc		:= 10;
end if;
if	(:new.ie_renal= 'S') then
	:new.qt_esc_ren		:= 10;
end if;
if	(:new.ie_estado_mental= 'S') then
	:new.qt_esc_mental		:= 20;
end if;
if	(:new.ie_fr= 'S') then
	:new.qt_esc_fr		:= 20;
end if;
if	(:new.ie_pas= 'S') then
	:new.qt_esc_pas		:= 20;
end if;
if	(:new.ie_temp= 'S') then
	:new.qt_esc_temp		:= 15;
end if;
if	(:new.ie_fc= 'S') then
	:new.qt_esc_fc		:= 10;
end if;
if	(:new.ie_ph_arterial= 'S') then
	:new.qt_esc_ph_arterial	:= 30;
end if;
if	(:new.ie_ureia= 'S') then
	:new.qt_esc_ureia		:= 20;
end if;
if	(:new.ie_sodio= 'S') then
	:new.qt_esc_sodio		:= 10;
end if;
if	(:new.ie_glicose= 'S') then
	:new.qt_esc_glicose		:= 10;
end if;
if	(:new.ie_hematocrito= 'S') then
	:new.qt_esc_hematocrito	:= 10;
end if;
if	(:new.ie_po2= 'S') then
	:new.qt_esc_po2		:= 10;
end if;
if	(:new.ie_derrame_pleural= 'S') then
	:new.qt_esc_derrame_pleural	:= 10;
end if;

:new.qt_escore	:=
	:new.qt_esc_ano +
	:new.qt_esc_enf +
	:new.qt_esc_neo +
	:new.qt_esc_hep +
	:new.qt_esc_icc +
	:new.qt_esc_avc +
	:new.qt_esc_ren +
	:new.qt_esc_mental +
	:new.qt_esc_fr +
	:new.qt_esc_pas +
	:new.qt_esc_temp +
	:new.qt_esc_fc +
	:new.qt_esc_ph_arterial +
	:new.qt_esc_ureia +
	:new.qt_esc_sodio +
	:new.qt_esc_glicose +
	:new.qt_esc_hematocrito +
	:new.qt_esc_po2 +
	:new.qt_esc_derrame_pleural;

if	(:new.qt_escore <= 70) then
	:new.pr_risco			:= 0.6;
elsif	(:new.qt_escore <= 90) then
	:new.pr_risco			:= 0.9;
elsif	(:new.qt_escore <= 130) then
	:new.pr_risco			:= 9.3;
elsif	(:new.qt_escore > 130) then
	:new.pr_risco			:= 27;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ATEND_PSI ADD (
  CONSTRAINT ATEPSI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PSI ADD (
  CONSTRAINT ATEPSI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPSI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPSI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ATEND_PSI TO NIVEL_1;

