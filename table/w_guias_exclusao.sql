ALTER TABLE TASY.W_GUIAS_EXCLUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_GUIAS_EXCLUSAO CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_GUIAS_EXCLUSAO
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_SEQ_CTA_ALT  NUMBER(10)
)
ON COMMIT DELETE ROWS
NOCACHE;


CREATE UNIQUE INDEX TASY.WGUEXCL_PK ON TASY.W_GUIAS_EXCLUSAO
(NR_SEQUENCIA);


ALTER TABLE TASY.W_GUIAS_EXCLUSAO ADD (
  CONSTRAINT WGUEXCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_GUIAS_EXCLUSAO TO NIVEL_1;

