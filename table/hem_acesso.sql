ALTER TABLE TASY.HEM_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC          NUMBER(10),
  IE_VASO_ACESSO       VARCHAR2(15 BYTE)        NOT NULL,
  IE_ACESSO            VARCHAR2(15 BYTE)        NOT NULL,
  IE_LAT_ACESSO        VARCHAR2(15 BYTE),
  IE_TIPO_ACESSO       VARCHAR2(15 BYTE),
  NR_INTRODUTOR        NUMBER(10)               NOT NULL,
  NR_SEQ_CIRURGIA      NUMBER(10),
  NR_SEQ_OCLUSOR       NUMBER(10),
  QT_OCLUSOR           NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMACESS_CIRURGI_FK_I ON TASY.HEM_ACESSO
(NR_SEQ_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMACESS_HEMOCLU_FK_I ON TASY.HEM_ACESSO
(NR_SEQ_OCLUSOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMACESS_HEMPROC_FK_I ON TASY.HEM_ACESSO
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMACESS_PK ON TASY.HEM_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEM_ACESSO ADD (
  CONSTRAINT HEMACESS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_ACESSO ADD (
  CONSTRAINT HEMACESS_HEMPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.HEM_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMACESS_CIRURGI_FK 
 FOREIGN KEY (NR_SEQ_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT HEMACESS_HEMOCLU_FK 
 FOREIGN KEY (NR_SEQ_OCLUSOR) 
 REFERENCES TASY.HEM_OCLUSORES (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_ACESSO TO NIVEL_1;

