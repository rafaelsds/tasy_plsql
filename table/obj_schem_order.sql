ALTER TABLE TASY.OBJ_SCHEM_ORDER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OBJ_SCHEM_ORDER CASCADE CONSTRAINTS;

CREATE TABLE TASY.OBJ_SCHEM_ORDER
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_OBJ_SCHEMATIC        NUMBER(10),
  NR_SEQ_OBJ_PADRAO           NUMBER(10),
  DS_REGRA                    VARCHAR2(255 BYTE),
  NR_SEQ_LOTE_PAR_REGRA_ITEM  NUMBER(10),
  NR_SEQ_FUNCAO_SCHEMATIC     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OBJSCHOR_FUNSCHE_FK_I ON TASY.OBJ_SCHEM_ORDER
(NR_SEQ_FUNCAO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OBJSCHOR_I1 ON TASY.OBJ_SCHEM_ORDER
(NR_SEQ_OBJ_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OBJSCHOR_PK ON TASY.OBJ_SCHEM_ORDER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.obj_schem_order_insert
after insert ON TASY.OBJ_SCHEM_ORDER for each row
declare

begin

if	(:new.nr_seq_obj_schematic is not null) then
	begin
	insert into obj_schem_order_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_obj_schematic,
		nr_ordem)
	select	obj_schem_order_item_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		a.nr_sequencia,
		a.nr_seq_apres
	from	objeto_schematic a
	where	a.nr_seq_obj_sup = :new.nr_seq_obj_schematic;
	end;
elsif	(:new.nr_seq_funcao_schematic is not null) then
	begin
	insert into obj_schem_order_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_regra,
		nr_seq_obj_schematic,
		nr_ordem)
	select	obj_schem_order_item_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		x.nr_sequencia,
		rownum * 3 nr_seq_apres
	from	(select	a.nr_sequencia
		from	funcao b,
			objeto_schematic a
		where	b.cd_funcao = a.cd_funcao_externa
		and	a.nr_seq_funcao_schematic = :new.nr_seq_funcao_schematic
		and	a.ie_tipo_componente = 'WAE'
		order by substr(obter_desc_expressao(a.cd_exp_conf_usage,obter_desc_expressao(a.cd_exp_desc_obj,obter_desc_expressao(b.cd_exp_funcao,a.ds_objeto))),1,255)) x;
	end;
end if;

end;
/


ALTER TABLE TASY.OBJ_SCHEM_ORDER ADD (
  CONSTRAINT OBJSCHOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OBJ_SCHEM_ORDER ADD (
  CONSTRAINT OBJSCHOR_FUNSCHE_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO_SCHEMATIC) 
 REFERENCES TASY.FUNCAO_SCHEMATIC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.OBJ_SCHEM_ORDER TO NIVEL_1;

