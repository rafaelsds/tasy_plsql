DROP TABLE TASY.PLS_CONTA_TISS_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_TISS_IMP
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CONTA_IMP            NUMBER(10),
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_VALIDACAO_BENEF_TISS     VARCHAR2(10 BYTE),
  CD_AUSENCIA_VAL_BENEF_TISS  VARCHAR2(2 BYTE),
  CD_IDENT_BIOMETRIA_BENEF    VARCHAR2(4000 BYTE),
  CD_TEMPLATE_BIOMET_BENEF    VARCHAR2(4000 BYTE),
  IE_TIPO_IDENT_BENEF         VARCHAR2(2 BYTE),
  CD_ASSINAT_DIGITAL_PREST    VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCTATIMP_ESTABEL_FK_I ON TASY.PLS_CONTA_TISS_IMP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTATIMP_PLSCTIM_FK_I ON TASY.PLS_CONTA_TISS_IMP
(NR_SEQ_CONTA_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_CONTA_TISS_IMP ADD (
  CONSTRAINT PLSCTATIMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCTATIMP_PLSCTIM_FK 
 FOREIGN KEY (NR_SEQ_CONTA_IMP) 
 REFERENCES TASY.PLS_CONTA_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CONTA_TISS_IMP TO NIVEL_1;

