ALTER TABLE TASY.TAB_ATRIBUTO_IDIOMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAB_ATRIBUTO_IDIOMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAB_ATRIBUTO_IDIOMA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NM_TABELA             VARCHAR2(50 BYTE)       NOT NULL,
  NM_ATRIBUTO           VARCHAR2(50 BYTE)       NOT NULL,
  NM_ATRIBUTO_TRADUCAO  VARCHAR2(50 BYTE)       NOT NULL,
  DS_DESCRICAO          VARCHAR2(255 BYTE)      NOT NULL,
  NR_SEQ_VISAO          NUMBER(10),
  CD_FUNCAO             NUMBER(5)               NOT NULL,
  NR_SEQ_IDIOMA         NUMBER(10)              NOT NULL,
  DS_TRADUCAO           VARCHAR2(255 BYTE),
  DS_TRADUCAO_LONG      VARCHAR2(4000 BYTE),
  IE_NECESSITA_REVISAO  VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAIDIOM_FUNCAO_FK_I ON TASY.TAB_ATRIBUTO_IDIOMA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TAIDIOM_PK ON TASY.TAB_ATRIBUTO_IDIOMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAIDIOM_PK
  MONITORING USAGE;


CREATE INDEX TASY.TAIDIOM_TABSIST_FK_I ON TASY.TAB_ATRIBUTO_IDIOMA
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAIDIOM_TABVISA_FK_I ON TASY.TAB_ATRIBUTO_IDIOMA
(NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAIDIOM_TASYIDI_FK_I ON TASY.TAB_ATRIBUTO_IDIOMA
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TAIDIOM_TASYIDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TAB_ATRIBUTO_IDIOMA ADD (
  CONSTRAINT TAIDIOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TAB_ATRIBUTO_IDIOMA ADD (
  CONSTRAINT TAIDIOM_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT TAIDIOM_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT TAIDIOM_TABVISA_FK 
 FOREIGN KEY (NR_SEQ_VISAO) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA),
  CONSTRAINT TAIDIOM_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA));

GRANT SELECT ON TASY.TAB_ATRIBUTO_IDIOMA TO NIVEL_1;

