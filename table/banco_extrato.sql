ALTER TABLE TASY.BANCO_EXTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_EXTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_EXTRATO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_INICIO            DATE,
  DT_FINAL             DATE,
  VL_SALDO_INICIAL     NUMBER(15,2),
  VL_SALDO_FINAL       NUMBER(15,2),
  DT_IMPORTACAO        DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANEXTR_BANESTA_FK_I ON TASY.BANCO_EXTRATO
(NR_SEQ_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BANEXTR_BANESTA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.BANEXTR_PK ON TASY.BANCO_EXTRATO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BANEXTR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.banco_extrato_atual
before insert or update ON TASY.BANCO_EXTRATO for each row
declare

cd_estabelecimento_w	number(10);
ie_consistir_extrato_w	varchar2(2);
qt_registro_w		number(10);

pragma autonomous_transaction;
begin

select	max(a.cd_estabelecimento)
into	cd_estabelecimento_w
from	banco_estabelecimento a
where	a.nr_sequencia = :new.nr_seq_conta;

if	(cd_estabelecimento_w is not null) then

	select	max(ie_consistir_extrato)
	into	ie_consistir_extrato_w
	from	parametro_controle_banc
	where	cd_estabelecimento = cd_estabelecimento_w;

	if (ie_consistir_extrato_w = 'S') then
		select 	count(*)
		into	qt_registro_w
		from	banco_extrato a
		where	trunc(a.dt_inicio,'dd') = trunc(:new.dt_inicio,'dd')
		and	trunc(a.dt_final,'dd') = trunc(:new.dt_final,'dd')
		and	a.nr_seq_conta = :new.nr_seq_conta
		and	a.nr_sequencia <> :new.nr_sequencia;

		if (qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(261498);
		end if;

	end if;
end if;

end;
/


ALTER TABLE TASY.BANCO_EXTRATO ADD (
  CONSTRAINT BANEXTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_EXTRATO ADD (
  CONSTRAINT BANEXTR_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.BANCO_EXTRATO TO NIVEL_1;

