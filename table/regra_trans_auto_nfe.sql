ALTER TABLE TASY.REGRA_TRANS_AUTO_NFE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TRANS_AUTO_NFE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TRANS_AUTO_NFE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO            NUMBER(2),
  DT_FINAL             NUMBER(2),
  HR_INICIO            DATE                     NOT NULL,
  HR_FINAL             DATE                     NOT NULL,
  IE_COMPETENCIA       VARCHAR2(1 BYTE)         NOT NULL,
  IE_FORMA_GERACAO     VARCHAR2(1 BYTE),
  QT_NOTAS_LOTE        NUMBER(10),
  QT_ESPERA_CONSULTA   NUMBER(10),
  IE_SINCRONO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RETRANFE_ESTABEL_FK_I ON TASY.REGRA_TRANS_AUTO_NFE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RETRANFE_PK ON TASY.REGRA_TRANS_AUTO_NFE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_TRANS_AUTO_NFE_ATUAL
before update or insert ON TASY.REGRA_TRANS_AUTO_NFE for each row
begin

if	(:new.ie_forma_geracao = 'U') and
	(:new.qt_notas_lote is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(320512);
elsif	(:new.ie_forma_geracao = 'L') and
	(:new.qt_notas_lote is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(320513);
elsif	(:new.ie_forma_geracao = 'L') and
	(:new.qt_notas_lote = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(320514);
end if;

end;
/


ALTER TABLE TASY.REGRA_TRANS_AUTO_NFE ADD (
  CONSTRAINT RETRANFE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TRANS_AUTO_NFE ADD (
  CONSTRAINT RETRANFE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_TRANS_AUTO_NFE TO NIVEL_1;

