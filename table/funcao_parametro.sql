ALTER TABLE TASY.FUNCAO_PARAMETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_PARAMETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_PARAMETRO
(
  CD_FUNCAO                      NUMBER(5)      NOT NULL,
  NR_SEQUENCIA                   NUMBER(5)      NOT NULL,
  DS_PARAMETRO                   VARCHAR2(255 BYTE) NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  CD_DOMINIO                     NUMBER(5),
  VL_PARAMETRO_PADRAO            VARCHAR2(255 BYTE),
  VL_PARAMETRO                   VARCHAR2(255 BYTE),
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  NR_SEQ_APRESENTACAO            NUMBER(6),
  IE_NIVEL_LIBERACAO             VARCHAR2(3 BYTE) NOT NULL,
  DS_DEPENDENCIA                 VARCHAR2(255 BYTE),
  NR_SEQ_AGRUP                   NUMBER(10),
  DS_OBSERVACAO_CLIENTE          VARCHAR2(4000 BYTE),
  DS_SQL                         VARCHAR2(2000 BYTE),
  NR_SEQ_LOCALIZADOR             NUMBER(10),
  IE_SITUACAO_DELPHI             VARCHAR2(1 BYTE),
  IE_SITUACAO_SWING              VARCHAR2(1 BYTE),
  IE_AUDITORIA                   VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIFICACAO           NUMBER(10),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM                   NUMBER(10),
  DT_LIBERACAO                   DATE,
  NM_USUARIO_LIB                 VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_PHILIPS         DATE,
  NM_USUARIO_PHILIPS             VARCHAR2(15 BYTE),
  CD_EXP_PARAMETRO               NUMBER(10),
  CD_EXP_OBSERVACAO              NUMBER(10),
  CD_EXP_DEPENDENCIA             NUMBER(10),
  IE_SITUACAO_HTML5              VARCHAR2(1 BYTE),
  IE_RECOMENDA_NIVEL_PRIMARIO    VARCHAR2(1 BYTE),
  IE_RECOMENDA_NIVEL_SECUNDARIO  VARCHAR2(1 BYTE),
  IE_RECOMENDA_NIVEL_TERCIARIO   VARCHAR2(1 BYTE),
  IE_CONSISTIR_VALOR             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNPARA_CLASPAR_FK_I ON TASY.FUNCAO_PARAMETRO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_DICEXPR_FK_I ON TASY.FUNCAO_PARAMETRO
(CD_EXP_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNPARA_DICEXPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FUNPARA_DICEXPR_FK2_I ON TASY.FUNCAO_PARAMETRO
(CD_EXP_OBSERVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_DICEXPR_FK3_I ON TASY.FUNCAO_PARAMETRO
(CD_EXP_DEPENDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_DOMINIO_FK_I ON TASY.FUNCAO_PARAMETRO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    40
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_FUNCAO_FK_I ON TASY.FUNCAO_PARAMETRO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    40
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_FUNGRPR_FK_I ON TASY.FUNCAO_PARAMETRO
(NR_SEQ_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_I1 ON TASY.FUNCAO_PARAMETRO
(IE_NIVEL_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNPARA_PK ON TASY.FUNCAO_PARAMETRO
(CD_FUNCAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPARA_TIPLOCA_FK_I ON TASY.FUNCAO_PARAMETRO
(NR_SEQ_LOCALIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAMETRO_tp  after update ON TASY.FUNCAO_PARAMETRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_FUNCAO='||to_char(:old.CD_FUNCAO)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA); ds_w:=substr(:new.DS_PARAMETRO,1,500);gravar_log_alteracao(substr(:old.DS_PARAMETRO,1,4000),substr(:new.DS_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PARAMETRO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOMINIO,1,4000),substr(:new.CD_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOMINIO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO_PADRAO,1,4000),substr(:new.VL_PARAMETRO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO_PADRAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NIVEL_LIBERACAO,1,4000),substr(:new.IE_NIVEL_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NIVEL_LIBERACAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM,1,4000),substr(:new.NR_SEQ_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AGRUP,1,4000),substr(:new.NR_SEQ_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGRUP',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCALIZADOR,1,4000),substr(:new.NR_SEQ_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCALIZADOR',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_HTML5,1,4000),substr(:new.IE_SITUACAO_HTML5,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_HTML5',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_DELPHI,1,4000),substr(:new.IE_SITUACAO_DELPHI,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_DELPHI',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_SWING,1,4000),substr(:new.IE_SITUACAO_SWING,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_SWING',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITORIA,1,4000),substr(:new.IE_AUDITORIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITORIA',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO_CLIENTE,1,4000),substr(:new.DS_OBSERVACAO_CLIENTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO_CLIENTE',ie_log_w,ds_w,'FUNCAO_PARAMETRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAMETRO_dt_param
after UPDATE ON TASY.FUNCAO_PARAMETRO for each row
declare
	ie_consistir_valor_w	varchar2(1);
	vl_parametro_padrao_w	varchar2(255);
	ds_user_w 		varchar2(100);
begin

select	max(USERNAME)
into	ds_user_w
from	v$session
 where 	audsid = (select userenv('sessionid') from dual);

if	(ds_user_w = 'TASY') then
	update	funcao
	set	dt_atualizacao_param = sysdate
	where	cd_funcao = :new.cd_funcao;
end	if;

if	(:new.ie_consistir_valor = 'S') and
	(:old.vl_parametro <> :new.vl_parametro) and
	(somente_numero(:new.vl_parametro) <  (somente_numero(:new.vl_parametro_padrao))) then
	wheb_mensagem_pck.exibir_mensagem_abort(55208);
end	if;

end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_parametro_subject FOR
  DELETE OR INSERT OR UPDATE ON TASY.FUNCAO_PARAMETRO 
COMPOUND TRIGGER
  apply_sync     BOOLEAN := false;
  apply_config   BOOLEAN := false;
  AFTER EACH ROW IS BEGIN
    apply_sync := psa_is_auth_parameter(
      :new.cd_funcao,
      :new.nr_sequencia
    ) = 'TRUE' OR apply_sync;

    apply_config := psa_is_config_parameter(
      :new.cd_funcao,
      :new.nr_sequencia
    ) = 'TRUE' OR apply_config;

  END AFTER EACH ROW;
  AFTER STATEMENT IS BEGIN
    IF
      apply_config
    THEN
      psa_configure;
    END IF;
    IF
      apply_sync
    THEN
      psa_synchronize_subject;
    END IF;
  END AFTER STATEMENT;
END;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAMETRO_BP
BEFORE INSERT OR UPDATE ON TASY.FUNCAO_PARAMETRO FOR EACH ROW
BEGIN
  IF INSTR(:NEW.VL_PARAMETRO, '#@') > 0 THEN
    wheb_mensagem_pck.exibir_mensagem_abort(1040016);
  END IF;
END;
/


ALTER TABLE TASY.FUNCAO_PARAMETRO ADD (
  CONSTRAINT FUNPARA_PK
 PRIMARY KEY
 (CD_FUNCAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_PARAMETRO ADD (
  CONSTRAINT FUNPARA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_PARAMETRO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT FUNPARA_DICEXPR_FK2 
 FOREIGN KEY (CD_EXP_OBSERVACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT FUNPARA_DICEXPR_FK3 
 FOREIGN KEY (CD_EXP_DEPENDENCIA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT FUNPARA_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO)
    ON DELETE CASCADE,
  CONSTRAINT FUNPARA_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT FUNPARA_FUNGRPR_FK 
 FOREIGN KEY (NR_SEQ_AGRUP) 
 REFERENCES TASY.FUNCAO_GRUPO_PARAM (NR_SEQUENCIA),
  CONSTRAINT FUNPARA_TIPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZADOR) 
 REFERENCES TASY.TIPO_LOCALIZAR (NR_SEQUENCIA),
  CONSTRAINT FUNPARA_CLASPAR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_PARAMETRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.FUNCAO_PARAMETRO TO NIVEL_1;

