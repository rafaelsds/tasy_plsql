ALTER TABLE TASY.MAN_CUSTO_DIVERSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_CUSTO_DIVERSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_CUSTO_DIVERSO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM_SERV          NUMBER(10)         NOT NULL,
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  CD_MATERIAL                NUMBER(6),
  VL_ITEM                    NUMBER(15,2)       NOT NULL,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  CD_UNIDADE_MEDIDA          VARCHAR2(30 BYTE),
  QT_ITEM                    NUMBER(15,2),
  VL_CUSTO_UNITARIO          NUMBER(15,2),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  CD_CNPJ                    VARCHAR2(14 BYTE),
  NR_DOCUMENTO               VARCHAR2(20 BYTE),
  DT_CUSTO_DIVERSO           DATE,
  NR_SEQ_TIPO_CUSTO_DIVERSO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANCUDI_MANORSE_FK_I ON TASY.MAN_CUSTO_DIVERSO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCUDI_MANTICD_FK_I ON TASY.MAN_CUSTO_DIVERSO
(NR_SEQ_TIPO_CUSTO_DIVERSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_MANTICD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCUDI_MATERIA_FK_I ON TASY.MAN_CUSTO_DIVERSO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCUDI_PESFISI_FK_I ON TASY.MAN_CUSTO_DIVERSO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANCUDI_PESJURI_FK_I ON TASY.MAN_CUSTO_DIVERSO
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANCUDI_PK ON TASY.MAN_CUSTO_DIVERSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_PK
  MONITORING USAGE;


CREATE INDEX TASY.MANCUDI_PROINTE_FK_I ON TASY.MAN_CUSTO_DIVERSO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANCUDI_UNIMEDI_FK_I ON TASY.MAN_CUSTO_DIVERSO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANCUDI_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.MAN_CUSTO_DIVERSO ADD (
  CONSTRAINT MANCUDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_CUSTO_DIVERSO ADD (
  CONSTRAINT MANCUDI_MANTICD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CUSTO_DIVERSO) 
 REFERENCES TASY.MAN_TIPO_CUSTO_DIVERSO (NR_SEQUENCIA),
  CONSTRAINT MANCUDI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MANCUDI_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MANCUDI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MANCUDI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT MANCUDI_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANCUDI_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.MAN_CUSTO_DIVERSO TO NIVEL_1;

