ALTER TABLE TASY.W_INTEGRACAO_PF_ATUALIZA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTEGRACAO_PF_ATUALIZA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTEGRACAO_PF_ATUALIZA
(
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_USUARIO_CONVENIO       VARCHAR2(30 BYTE)   NOT NULL,
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  IE_TIPO_PESSOA            NUMBER(1)           NOT NULL,
  IE_TIPO_COMPLEMENTO       NUMBER(2),
  NM_CONTATO                VARCHAR2(60 BYTE),
  DT_NASCIMENTO             DATE,
  DS_ENDERECO               VARCHAR2(100 BYTE),
  IE_SEXO                   VARCHAR2(1 BYTE),
  CD_CEP                    VARCHAR2(15 BYTE),
  IE_ESTADO_CIVIL           VARCHAR2(2 BYTE),
  NR_ENDERECO               NUMBER(5),
  NR_CPF                    VARCHAR2(11 BYTE),
  DS_COMPLEMENTO            VARCHAR2(40 BYTE),
  NR_IDENTIDADE             VARCHAR2(15 BYTE),
  DS_BAIRRO                 VARCHAR2(40 BYTE),
  NR_TELEFONE_CELULAR       VARCHAR2(40 BYTE),
  DT_ATUALIZACAO            DATE                NOT NULL,
  DS_MUNICIPIO              VARCHAR2(40 BYTE),
  IE_GRAU_INSTRUCAO         NUMBER(2),
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  SG_ESTADO                 VARCHAR2(2 BYTE),
  DT_ATUALIZACAO_NREC       DATE,
  NR_TELEFONE               VARCHAR2(15 BYTE),
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_RAMAL                  NUMBER(5),
  CD_RELIGIAO               NUMBER(5),
  CD_NACIONALIDADE          VARCHAR2(8 BYTE),
  DS_EMAIL                  VARCHAR2(255 BYTE),
  CD_MUNICIPIO_IBGE         VARCHAR2(6 BYTE),
  DT_ADMISSAO               DATE,
  DT_DESLIGAMENTO           DATE,
  DS_ORGAO_EMISSOR_CI       VARCHAR2(40 BYTE),
  IE_SOS                    VARCHAR2(1 BYTE),
  IE_REGULAMENTADO          VARCHAR2(1 BYTE),
  CD_FORMACAO               VARCHAR2(15 BYTE),
  NR_SEQ_PAIS               NUMBER(10),
  NR_DDD_TELEFONE           VARCHAR2(3 BYTE),
  DT_EMISSAO_CI             DATE,
  SG_EMISSORA_CI            VARCHAR2(2 BYTE),
  CD_PLANO_CONVENIO         VARCHAR2(60 BYTE),
  NR_DDD_CELULAR            VARCHAR2(3 BYTE),
  IE_TIPO_CODIGO_EXTERNO    VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA_EXTERNO  VARCHAR2(20 BYTE)   NOT NULL,
  NM_PESSOA_FISICA          VARCHAR2(60 BYTE)   NOT NULL,
  CD_CATEGORIA              VARCHAR2(10 BYTE),
  CD_OPERACAO               VARCHAR2(10 BYTE),
  IE_STATUS                 VARCHAR2(10 BYTE),
  NM_MAE                    VARCHAR2(60 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WINTPFA_NACIONA_FK_I ON TASY.W_INTEGRACAO_PF_ATUALIZA
(CD_NACIONALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTPFA_NACIONA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WINTPFA_PAIS_FK_I ON TASY.W_INTEGRACAO_PF_ATUALIZA
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTPFA_PAIS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WINTPFA_PK ON TASY.W_INTEGRACAO_PF_ATUALIZA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTPFA_PK
  MONITORING USAGE;


CREATE INDEX TASY.WINTPFA_RELIGIA_FK_I ON TASY.W_INTEGRACAO_PF_ATUALIZA
(CD_RELIGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTPFA_RELIGIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WINTPFA_SUSMUNI_FK_I ON TASY.W_INTEGRACAO_PF_ATUALIZA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTPFA_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.W_INTEGRACAO_PF_ATUALIZA_tp  after update ON TASY.W_INTEGRACAO_PF_ATUALIZA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_CONTATO,1,500);gravar_log_alteracao(substr(:old.NM_CONTATO,1,4000),substr(:new.NM_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NM_CONTATO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_EMISSOR_CI,1,4000),substr(:new.DS_ORGAO_EMISSOR_CI,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_EMISSOR_CI',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EMISSAO_CI,1,4000),substr(:new.DT_EMISSAO_CI,1,4000),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CI',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAU_INSTRUCAO,1,4000),substr(:new.IE_GRAU_INSTRUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_INSTRUCAO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PESSOA,1,4000),substr(:new.IE_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PESSOA',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_CELULAR,1,4000),substr(:new.NR_DDD_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_CELULAR',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_EMISSORA_CI,1,4000),substr(:new.SG_EMISSORA_CI,1,4000),:new.nm_usuario,nr_seq_w,'SG_EMISSORA_CI',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL,1,4000),substr(:new.NR_RAMAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RELIGIAO,1,4000),substr(:new.CD_RELIGIAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_RELIGIAO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NACIONALIDADE,1,4000),substr(:new.CD_NACIONALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_NACIONALIDADE',ie_log_w,ds_w,'W_INTEGRACAO_PF_ATUALIZA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.W_INTEGR_PF_ATUALIZA_INSERT
BEFORE INSERT ON TASY.W_INTEGRACAO_PF_ATUALIZA FOR EACH ROW
DECLARE

cd_pessoa_fisica_w	varchar2(10);
qt_existe_w_usuario_w	number(10,0);
qt_existe_compl_pf_w	number(10,0);
nr_sequencia_compl_w	number(10,0);
ds_erro_w 		varchar2(1800);


begin

if 	(:new.CD_PESSOA_FISICA_EXTERNO is not null) and
	(:new.IE_TIPO_CODIGO_EXTERNO is not null) then

	select 	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from 	pf_codigo_externo
	where 	CD_PESSOA_FISICA_EXTERNO = :new.CD_PESSOA_FISICA_EXTERNO
	and 	IE_TIPO_CODIGO_EXTERNO   = 'TS';

	if	(cd_pessoa_fisica_w is not null) then -- Existe a pessoa

		begin
		update	pessoa_fisica
		set	CD_NACIONALIDADE	=	:new.CD_NACIONALIDADE,
			CD_PESSOA_FISICA	= 	:new.CD_PESSOA_FISICA,
			CD_RELIGIAO		= 	:new.CD_RELIGIAO,
			DS_ORGAO_EMISSOR_CI	= 	:new.DS_ORGAO_EMISSOR_CI,
			DT_ATUALIZACAO		=	:new.DT_ATUALIZACAO,
			DT_EMISSAO_CI		=	:new.DT_EMISSAO_CI,
			DT_NASCIMENTO		=	:new.DT_NASCIMENTO,
			IE_ESTADO_CIVIL		=	:new.IE_ESTADO_CIVIL,
			IE_GRAU_INSTRUCAO	=	:new.IE_GRAU_INSTRUCAO,
			IE_SEXO			=	:new.IE_SEXO,
			IE_TIPO_PESSOA		=	:new.IE_TIPO_PESSOA,
			NM_PESSOA_FISICA	=	:new.NM_PESSOA_FISICA,
			NM_USUARIO		=	:new.NM_USUARIO,
			NR_CPF			=	:new.NR_CPF,
			NR_DDD_CELULAR		=	:new.NR_DDD_CELULAR,
			NR_IDENTIDADE		=	:new.NR_IDENTIDADE,
			NR_TELEFONE_CELULAR	=	:new.NR_TELEFONE_CELULAR,
			SG_EMISSORA_CI		=	:new.SG_EMISSORA_CI
		where 	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		exception
			when others then
			null;
			/*ds_erro_w:= SQLERRM;
			insert into logxxxx_tasy( cd_log, ds_log, nm_usuario, dt_atualizacao)
				values (7211, '1- Update PF ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
		end;

		select 	count(*)
		into	qt_existe_w_usuario_w
		from 	W_USUARIO_CONVENIO
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;

		if	(qt_existe_w_usuario_w	= 0) then

			begin
			insert into W_USUARIO_CONVENIO
				(CD_PLANO_CONVENIO,
				CD_USUARIO_CONVENIO,
				DT_ADMISSAO,
				DT_DESLIGAMENTO,
				IE_SOS,
				IE_REGULAMENTADO,
				CD_FORMACAO,
				CD_PESSOA_FISICA,
				CD_CATEGORIA,
				IE_STATUS,
				nm_pessoa_fisica)
			values 	(:new.CD_PLANO_CONVENIO,
				:new.CD_USUARIO_CONVENIO,
				:new.DT_ADMISSAO,
				:new.DT_DESLIGAMENTO,
				:new.IE_SOS,
				:new.IE_REGULAMENTADO,
				:new.CD_FORMACAO,
				cd_pessoa_fisica_w,
				:new.CD_CATEGORIA,
				:NEW.IE_STATUS,
				:new.NM_PESSOA_FISICA);
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '2- Insert W_USUARIO... ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end;
		else

			begin
			update 	W_USUARIO_CONVENIO
			set 	CD_PLANO_CONVENIO	=	:new.CD_PLANO_CONVENIO,
				CD_USUARIO_CONVENIO	=	:new.CD_USUARIO_CONVENIO,
				DT_ADMISSAO		=	:new.DT_ADMISSAO,
				DT_DESLIGAMENTO		=	:new.DT_DESLIGAMENTO,
				IE_SOS			=	:new.IE_SOS,
				IE_REGULAMENTADO	=	:new.IE_REGULAMENTADO,
				CD_FORMACAO		=	:new.CD_FORMACAO,
				CD_CATEGORIA		= 	:new.CD_CATEGORIA,
				IE_STATUS		=	:new.IE_STATUS
			where 	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '3- Update W_USUARIO... ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end;

		end if;

		select 	count(*)
		into	qt_existe_compl_pf_w
		from 	compl_pessoa_fisica
		where 	cd_pessoa_fisica = cd_pessoa_fisica_w
		and 	ie_tipo_complemento = :new.ie_tipo_complemento;

		if	(qt_existe_compl_pf_w = 0) then

			select	(nvl(max(nr_sequencia),0)+1)
			into 	nr_sequencia_compl_w
			from 	compl_pessoa_fisica
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w;

			--Complemento da Pessoa F�sica
			begin
			insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					cd_cep,
					cd_municipio_ibge,
					ds_bairro,
					ds_complemento,
					ds_email,
					ds_endereco,
					ds_municipio,
					nm_contato,
					nr_ddd_telefone,
					nr_endereco,
					nr_ramal,
					nr_seq_pais,
					nr_telefone,
					sg_estado)
				values 	(cd_pessoa_fisica_w,
					nr_sequencia_compl_w,
					:new.ie_tipo_complemento,
					:new.dt_atualizacao,
					:new.nm_usuario,
					:new.cd_cep,
					:new.cd_municipio_ibge,
					:new.ds_bairro,
					:new.ds_complemento,
					:new.ds_email,
					:new.ds_endereco,
					:new.ds_municipio,
					:new.nm_contato,
					:new.nr_ddd_telefone,
					:new.nr_endereco,
					:new.nr_ramal,
					:new.nr_seq_pais,
					:new.nr_telefone,
					:new.sg_estado);
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '4- Insert  Compl_PF ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end ;


			--Complemento da Pessoa F�sica (M�e)
			begin
			insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					nm_contato)
				values 	(cd_pessoa_fisica_w,
					nr_sequencia_compl_w + 1,
					5,
					:new.dt_atualizacao,
					:new.nm_usuario,
					:new.nm_mae);
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '4.1- Insert  Compl_PF(Mae) ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end ;

		else

			--Complemento da Pessoa F�sica
			begin
			update compl_pessoa_fisica
			set	dt_atualizacao		=	:new.dt_atualizacao,
				nm_usuario		=	:new.nm_usuario,
				cd_cep			=	:new.cd_cep,
				cd_municipio_ibge	=	:new.cd_municipio_ibge,
				ds_bairro		=	:new.ds_bairro,
				ds_complemento		=	:new.ds_complemento,
				ds_email		=	:new.ds_email,
				ds_endereco		=	:new.ds_endereco,
				ds_municipio		=	:new.ds_municipio,
				nm_contato		=	:new.nm_contato,
				nr_ddd_telefone		=	:new.nr_ddd_telefone,
				nr_endereco		=	:new.nr_endereco,
				nr_ramal		=	:new.nr_ramal,
				nr_seq_pais		=	:new.nr_seq_pais,
				nr_telefone		=	:new.nr_telefone,
				sg_estado		=	:new.sg_estado
			where	cd_pessoa_fisica 	= 	cd_pessoa_fisica_w
			and 	ie_tipo_complemento 	= 	:new.ie_tipo_complemento;
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '5- Update Compl_PF ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end;


			--Complemento da Pessoa F�sica (M�e)
			begin
			update compl_pessoa_fisica
			set	dt_atualizacao		=	:new.dt_atualizacao,
				nm_usuario		=	:new.nm_usuario,
				nm_contato		=	:new.nm_mae
			where	cd_pessoa_fisica 	= 	cd_pessoa_fisica_w
			and 	ie_tipo_complemento 	= 	5;
			exception
				when others then
				null;
				/*ds_erro_w:= SQLERRM;
				insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
					values (7211, '5.1- Update Compl_PF(Mae) ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
			end;

		end if;

	else -- N�o tem a PF

		select 	pessoa_fisica_seq.nextVal
		into	cd_pessoa_fisica_w
		from 	dual;

		begin
		insert into pessoa_fisica
			(CD_NACIONALIDADE,
			CD_PESSOA_FISICA,
			CD_RELIGIAO,
			DS_ORGAO_EMISSOR_CI,
			DT_ATUALIZACAO,
			DT_EMISSAO_CI,
			DT_NASCIMENTO,
			IE_ESTADO_CIVIL,
			IE_GRAU_INSTRUCAO,
			IE_SEXO,
			IE_TIPO_PESSOA,
			NM_PESSOA_FISICA,
			NM_USUARIO,
			NR_CPF,
			NR_DDD_CELULAR,
			NR_IDENTIDADE,
			NR_TELEFONE_CELULAR,
			SG_EMISSORA_CI)
		values	(
			:new.CD_NACIONALIDADE,
			cd_pessoa_fisica_w,
			:new.CD_RELIGIAO,
			:new.DS_ORGAO_EMISSOR_CI,
			:new.DT_ATUALIZACAO,
			:new.DT_EMISSAO_CI,
			:new.DT_NASCIMENTO,
			:new.IE_ESTADO_CIVIL,
			:new.IE_GRAU_INSTRUCAO,
			:new.IE_SEXO,
			:new.IE_TIPO_PESSOA,
			:new.NM_PESSOA_FISICA,
			:new.NM_USUARIO,
			:new.NR_CPF,
			:new.NR_DDD_CELULAR,
			:new.NR_IDENTIDADE,
			:new.NR_TELEFONE_CELULAR,
			:new.SG_EMISSORA_CI);
		exception
			when others then
			null;
			/*ds_erro_w:= SQLERRM;
			insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
				values (7211, '6- Insert PF ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
		end;

		begin
		insert into W_USUARIO_CONVENIO
				(CD_PLANO_CONVENIO,
				CD_USUARIO_CONVENIO,
				DT_ADMISSAO,
				DT_DESLIGAMENTO,
				IE_SOS,
				IE_REGULAMENTADO,
				CD_FORMACAO,
				CD_PESSOA_FISICA,
				CD_CATEGORIA,
				IE_STATUS,
				NM_PESSOA_FISICA)
			values 	(:new.CD_PLANO_CONVENIO,
				:new.CD_USUARIO_CONVENIO,
				:new.DT_ADMISSAO,
				:new.DT_DESLIGAMENTO,
				:new.IE_SOS,
				:new.IE_REGULAMENTADO,
				:new.CD_FORMACAO,
				cd_pessoa_fisica_w,
				:new.CD_CATEGORIA,
				:new.IE_STATUS,
				:new.NM_PESSOA_FISICA);
		exception
			when others then
			null;
			/*ds_erro_w:= SQLERRM;
			insert into logxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
				values (7211, '7- Insert W_USUARIO... ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
		end;


		select	(nvl(max(nr_sequencia),0)+1)
		into 	nr_sequencia_compl_w
		from 	compl_pessoa_fisica
		where 	cd_pessoa_fisica = cd_pessoa_fisica_w;

			--Complemento da Pessoa F�sica
		begin
		insert into compl_pessoa_fisica(
					cd_pessoa_fisica,
					nr_sequencia,
					ie_tipo_complemento,
					dt_atualizacao,
					nm_usuario,
					cd_cep,
					cd_municipio_ibge,
					ds_bairro,
					ds_complemento,
					ds_email,
					ds_endereco,
					ds_municipio,
					nm_contato,
					nr_ddd_telefone,
					nr_endereco,
					nr_ramal,
					nr_seq_pais,
					nr_telefone,
					sg_estado)
				values 	(cd_pessoa_fisica_w,
					nr_sequencia_compl_w,
					:new.ie_tipo_complemento,
					:new.dt_atualizacao,
					:new.nm_usuario,
					:new.cd_cep,
					:new.cd_municipio_ibge,
					:new.ds_bairro,
					:new.ds_complemento,
					:new.ds_email,
					:new.ds_endereco,
					:new.ds_municipio,
					:new.nm_contato,
					:new.nr_ddd_telefone,
					:new.nr_endereco,
					:new.nr_ramal,
					:new.nr_seq_pais,
					:new.nr_telefone,
					:new.sg_estado);
		exception
			when others then
			null;
			/*ds_erro_w:= SQLERRM;
			insert into logxxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
				values (7211, '8- Insert Compl_PF ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
		end;


		--Complemento da Pessoa F�sica (M�e)
		begin
		insert into compl_pessoa_fisica(
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				dt_atualizacao,
				nm_usuario,
				nm_contato)
			values 	(cd_pessoa_fisica_w,
				nr_sequencia_compl_w + 1,
				5,
				:new.dt_atualizacao,
				:new.nm_usuario,
				:new.nm_mae);
		exception
			when others then
			null;
			/*ds_erro_w:= SQLERRM;
			insert into logxxxxx_tasy ( cd_log, ds_log, nm_usuario, dt_atualizacao)
				values (7211, '8.1- Insert  Compl_PF(Mae) ' || cd_pessoa_fisica_w || ' - ' || ds_erro_w,'Integracao',sysdate);*/
		end ;

	end if;

end if;

end;
/


ALTER TABLE TASY.W_INTEGRACAO_PF_ATUALIZA ADD (
  CONSTRAINT WINTPFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_INTEGRACAO_PF_ATUALIZA ADD (
  CONSTRAINT WINTPFA_NACIONA_FK 
 FOREIGN KEY (CD_NACIONALIDADE) 
 REFERENCES TASY.NACIONALIDADE (CD_NACIONALIDADE),
  CONSTRAINT WINTPFA_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT WINTPFA_RELIGIA_FK 
 FOREIGN KEY (CD_RELIGIAO) 
 REFERENCES TASY.RELIGIAO (CD_RELIGIAO),
  CONSTRAINT WINTPFA_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE));

GRANT SELECT ON TASY.W_INTEGRACAO_PF_ATUALIZA TO NIVEL_1;

