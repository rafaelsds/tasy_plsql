ALTER TABLE TASY.PLS_REAJUSTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REAJUSTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REAJUSTE
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  DT_REAJUSTE                     DATE          NOT NULL,
  TX_REAJUSTE                     NUMBER(7,4),
  IE_INDICE_REAJUSTE              VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_REAJUSTE                VARCHAR2(1 BYTE) NOT NULL,
  DS_OFICIO_ANS                   VARCHAR2(255 BYTE),
  NR_PROTOCOLO_ANS                VARCHAR2(60 BYTE),
  DT_PERIODO_INICIAL              DATE,
  DT_PERIODO_FINAL                DATE,
  DS_OBSERVACAO                   VARCHAR2(2000 BYTE),
  IE_STATUS                       VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_CONTRATO                 NUMBER(10),
  CD_ESTABELECIMENTO              NUMBER(4)     NOT NULL,
  IE_REAJUSTE_TABELA              VARCHAR2(1 BYTE) NOT NULL,
  DT_AUTORIZACAO_ANS              DATE,
  TX_DEFLATOR                     NUMBER(11,8),
  NR_SEQ_LOTE_REFERENCIA          NUMBER(10),
  NR_SEQ_REGRA                    NUMBER(10),
  IE_APLICACAO_REAJUSTE           VARCHAR2(2 BYTE),
  IE_REAJUSTAR_INSCRICAO          VARCHAR2(1 BYTE),
  IE_REAJUSTAR_VL_MANUTENCAO      VARCHAR2(1 BYTE),
  IE_INDICE_REAJUSTE_CONTRATO     NUMBER(10),
  IE_REAJUSTAR_COPARTIC           VARCHAR2(1 BYTE),
  TX_REAJUSTE_COPARTIC            NUMBER(7,4),
  IE_REAJ_NAO_APLICADO            VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_DEFLATOR          NUMBER(10),
  NR_SEQ_GRUPO_CONTRATO           NUMBER(10),
  TX_REAJUSTE_COPARTIC_MAX        NUMBER(7,4),
  TX_REAJUSTE_INSCRICAO           NUMBER(7,4),
  NR_CONTRATO                     NUMBER(10),
  IE_REAJUSTAR_VIA_ADIC           VARCHAR2(1 BYTE),
  TX_VIA_ADICIONAL                NUMBER(7,4),
  NR_SEQ_INTERCAMBIO              NUMBER(10),
  IE_TIPO_CONTRATO                VARCHAR2(2 BYTE),
  NR_SEQ_PLANO                    NUMBER(10),
  IE_REGULAMENTACAO               VARCHAR2(2 BYTE),
  NR_SEQ_PROG_REAJUSTE            NUMBER(10),
  NR_SEQ_LOTE_REAJ_COLET          NUMBER(10),
  IE_TIPO_LOTE                    VARCHAR2(1 BYTE),
  NR_SEQ_SEGURADO                 NUMBER(10),
  IE_VINCULO_COPARTICIPACAO       VARCHAR2(2 BYTE),
  NR_SEQ_LOTE_DEFLATOR            NUMBER(10),
  DT_REAJUSTE_AUX                 VARCHAR2(7 BYTE),
  IE_STATUS_BENEF                 VARCHAR2(1 BYTE),
  NR_SEQ_REAJUSTE_DESFAZER        NUMBER(10),
  NR_SEQ_GRUPO_INTERCAMBIO        NUMBER(10),
  IE_REAJUSTAR_BENEF_CANCELADO    VARCHAR2(2 BYTE),
  DS_DOCUMENTACAO                 VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO_REAJUSTE            NUMBER(10),
  IE_CARACTERISTICA               VARCHAR2(2 BYTE),
  DS_JUSTIFICATIVA                VARCHAR2(255 BYTE),
  DT_APLICACAO_REAJUSTE           DATE,
  IE_TIPO_OPERACAO_CONTRATO       VARCHAR2(3 BYTE),
  TX_REAJUSTE_CORRETO             NUMBER(7,4),
  TX_REAJUSTE_PROPOSTO            NUMBER(7,4),
  TX_REAJUSTE_COPART_CORRETO      NUMBER(7,4),
  TX_REAJUSTE_COPART_MAX_CORRETO  NUMBER(7,4),
  TX_DEFLATOR_COPARTIC            NUMBER(11,8),
  TX_DEFLATOR_COPARTIC_MAX        NUMBER(11,8),
  NR_SEQ_GRUPO_PRODUTO            NUMBER(10),
  IE_TIPO_AGRUPAMENTO_EXCLUSIVO   NUMBER(2),
  TX_REAJUSTE_INSC_CORRETO        NUMBER(7,4),
  TX_DEFLATOR_INSC                NUMBER(7,4),
  TX_REAJUSTE_VIA_CART_CORRETO    NUMBER(7,4),
  TX_DEFLATOR_VIA_CART            NUMBER(7,4),
  DT_INICIO_ANALISE               DATE,
  DT_FIM_ANALISE                  DATE,
  NR_SEQ_PROCESSO                 NUMBER(10),
  DT_INICIO_GERACAO               DATE,
  DT_FIM_GERACAO                  DATE,
  DT_LIBERACAO                    DATE          DEFAULT null,
  NM_USUARIO_LIBERACAO            VARCHAR2(15 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREAJ_ESTABEL_FK_I ON TASY.PLS_REAJUSTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREAJ_PK ON TASY.PLS_REAJUSTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSCONT_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSINCA_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSMRDE_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_MOTIVO_DEFLATOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSMRDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSPJUL_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSPLAN_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSPREJ_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_PROG_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSPREJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSPRLT_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_LOTE_REAJ_COLET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSPRLT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSREAJ_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_LOTE_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSREAJ_FK2_I ON TASY.PLS_REAJUSTE
(NR_SEQ_LOTE_DEFLATOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSREAJ_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSREAJ_FK3_I ON TASY.PLS_REAJUSTE
(NR_SEQ_REAJUSTE_DESFAZER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSRERE_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSRERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSRGIN_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_GRUPO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSRGIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSRGPD_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_GRUPO_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREAJ_PLSSEGU_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREAJ_PLSTIRE_FK_I ON TASY.PLS_REAJUSTE
(NR_SEQ_TIPO_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREAJ_PLSTIRE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_reajuste_rpc
before update or insert or delete ON TASY.PLS_REAJUSTE for each row
declare
ie_consistir_envio_rpc_w	varchar2(1);

begin

select	max(ie_consistir_envio_rpc)
into	ie_consistir_envio_rpc_w
from	pls_parametros
where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_consistir_envio_rpc_w,'N') = 'S') and
	(:new.ie_tipo_reajuste = 'C') then
	if	(updating) then
		if	(:new.ie_status <> :old.ie_status) and
			(:new.ie_status = '2') and
			(pls_obter_se_rpc_enviado(:new.dt_aplicacao_reajuste) = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort(360248);
		end if;
	elsif	(inserting) and
		(pls_obter_se_rpc_enviado(:new.dt_aplicacao_reajuste) = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(360248); --N�o � poss�vel gerar reajuste pois j� foi realizado o envio do RPC para o per�odo.
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_REAJUSTE ADD (
  CONSTRAINT PLSREAJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REAJUSTE ADD (
  CONSTRAINT PLSREAJ_PLSRGPD_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRODUTO) 
 REFERENCES TASY.PLS_GRUPO_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSPJUL_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_JUDICIAL_LIMINAR (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSTIRE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_REAJUSTE) 
 REFERENCES TASY.PLS_TIPO_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSREAJ_FK3 
 FOREIGN KEY (NR_SEQ_REAJUSTE_DESFAZER) 
 REFERENCES TASY.PLS_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSRGIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_GRUPO_INTER (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSMRDE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEFLATOR) 
 REFERENCES TASY.PLS_MOTIVO_REAJ_DEFLATOR (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSPREJ_FK 
 FOREIGN KEY (NR_SEQ_PROG_REAJUSTE) 
 REFERENCES TASY.PLS_PROG_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSPRLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_REAJ_COLET) 
 REFERENCES TASY.PLS_PROG_REAJ_COLET_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSREAJ_FK2 
 FOREIGN KEY (NR_SEQ_LOTE_DEFLATOR) 
 REFERENCES TASY.PLS_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREAJ_PLSREAJ_FK 
 FOREIGN KEY (NR_SEQ_LOTE_REFERENCIA) 
 REFERENCES TASY.PLS_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSREAJ_PLSRERE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_REAJUSTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REAJUSTE TO NIVEL_1;

