ALTER TABLE TASY.PRESCR_MAT_HOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_MAT_HOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_MAT_HOR
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_PRESCRICAO                NUMBER(14)       NOT NULL,
  NR_SEQ_MATERIAL              NUMBER(6)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(20 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DS_HORARIO                   VARCHAR2(15 BYTE),
  DT_HORARIO                   DATE,
  QT_DOSE                      NUMBER(15,4),
  QT_DISPENSAR                 NUMBER(15,4),
  CD_MATERIAL                  NUMBER(10),
  DS_TURNO                     VARCHAR2(80 BYTE),
  IE_AGRUPADOR                 NUMBER(2)        NOT NULL,
  QT_DISPENSAR_HOR             NUMBER(15,4),
  NR_OCORRENCIA                NUMBER(15,4),
  CD_UNIDADE_MEDIDA            VARCHAR2(30 BYTE),
  CD_UNIDADE_MEDIDA_DOSE       VARCHAR2(30 BYTE),
  IE_URGENTE                   VARCHAR2(1 BYTE) NOT NULL,
  DT_EMISSAO_FARMACIA          DATE,
  DT_FIM_HORARIO               DATE,
  DT_SUSPENSAO                 DATE,
  DS_COR                       VARCHAR2(15 BYTE),
  IE_HORARIO_ESPECIAL          VARCHAR2(1 BYTE),
  QT_HOR_REAPRAZAMENTO         NUMBER(10),
  NM_USUARIO_REAPRAZAMENTO     VARCHAR2(15 BYTE),
  NR_SEQ_TURNO                 NUMBER(10),
  DT_DISP_FARMACIA             DATE,
  IE_APRAZADO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  IE_DOSE_ESPECIAL             VARCHAR2(1 BYTE),
  IE_CHECAGEM                  VARCHAR2(1 BYTE),
  NM_USUARIO_CHECAGEM          VARCHAR2(15 BYTE),
  DT_CHECAGEM                  DATE,
  CD_SETOR_EXEC                NUMBER(5),
  IE_CONTROLADO                VARCHAR2(1 BYTE),
  NR_SEQ_LOTE                  NUMBER(10),
  NR_SEQ_CLASSIF               NUMBER(10),
  IE_CLASSIF_URGENTE           VARCHAR2(3 BYTE),
  IE_PADRONIZADO               VARCHAR2(1 BYTE),
  NM_USUARIO_ADM               VARCHAR2(15 BYTE),
  NM_USUARIO_SUSP              VARCHAR2(15 BYTE),
  QT_CONTA                     NUMBER(15,4),
  NR_SEQ_PROCESSO              NUMBER(10),
  IE_DISPENSAR_FARM            VARCHAR2(1 BYTE),
  NR_SEQ_SUPERIOR              NUMBER(10),
  IE_TIPO_ITEM_PROCESSO        VARCHAR2(15 BYTE),
  NR_AGRUPAMENTO               NUMBER(7,1),
  IE_ADEP                      VARCHAR2(1 BYTE),
  CD_UNIDADE_MEDIDA_CONTA      VARCHAR2(30 BYTE),
  NR_SEQ_DIGITO                NUMBER(1),
  QT_HORARIO                   NUMBER(15,4),
  CD_UNID_MED_HOR              VARCHAR2(30 BYTE),
  NR_SEQ_ETIQUETA              NUMBER(10),
  IE_NEC_ETIQUETA              VARCHAR2(1 BYTE),
  NR_SEQ_AREA_PREP             NUMBER(10),
  NR_SEQ_REGRA_AREA_PREP       NUMBER(10),
  NR_SEQ_MOTIVO_SUSP           NUMBER(10),
  DS_MOTIVO_SUSP               VARCHAR2(255 BYTE),
  NR_SEQ_DIALISE               NUMBER(10),
  IE_GEDIPA                    VARCHAR2(1 BYTE),
  IE_SEPARADO                  VARCHAR2(1 BYTE),
  CD_MOTIVO_BAIXA              NUMBER(3),
  NR_ETAPA_SOL                 NUMBER(3),
  NR_SEQ_SOLUCAO               NUMBER(6),
  DS_DILUICAO                  VARCHAR2(2000 BYTE),
  IE_ETAPA_ESPECIAL            VARCHAR2(1 BYTE),
  IE_GERAR_LOTE                VARCHAR2(1 BYTE),
  DT_INICIO_HORARIO            DATE,
  NM_USUARIO_INICIO            VARCHAR2(15 BYTE),
  NM_USUARIO_INTER             VARCHAR2(15 BYTE),
  DT_INTERRUPCAO               DATE,
  DT_LIB_HORARIO               DATE,
  IE_SOMENTE_PT                VARCHAR2(1 BYTE),
  DT_PREV_FIM_HORARIO          DATE,
  IE_PENDENTE                  VARCHAR2(1 BYTE),
  IE_TRANSFERIDO               VARCHAR2(1 BYTE),
  NR_ORDEM_COMPRA              NUMBER(10),
  IE_CIENTE                    VARCHAR2(1 BYTE),
  DT_RECUSA                    DATE,
  IE_SUSPENSO_ADEP             VARCHAR2(1 BYTE),
  NR_MOTIVO_DISP               NUMBER(15),
  NR_SEQ_ASSINATURA_SUSP       NUMBER(10),
  CD_LOCAL_ESTOQUE             NUMBER(15),
  NR_SEQ_LOTE_FORNEC           NUMBER(10),
  NR_SEQ_REGRA_DISP            NUMBER(10),
  DS_HORARIO_CHAR              VARCHAR2(20 BYTE),
  NR_SEQ_JEJUM_SUSP            NUMBER(10),
  QT_MINUTOS_AGORA             NUMBER(5),
  NR_SEQ_HOR_GLIC              NUMBER(15),
  DT_BLOQUEIO                  DATE,
  DT_PRIMEIRA_CHECAGEM         DATE,
  IE_ADMINISTRAR               VARCHAR2(1 BYTE),
  DT_REINICIO                  DATE,
  DS_STACK                     VARCHAR2(2000 BYTE),
  QT_BIPADO                    NUMBER(15,4),
  IE_CHECADO_LEITO             VARCHAR2(1 BYTE),
  NR_SEQ_ETAPA_GEDIPA          NUMBER(10),
  NR_ATENDIMENTO               NUMBER(15),
  NR_SEQ_ETIQUETA_REAPROV      NUMBER(10),
  NM_USUARIO_EMISSAO_FARMACIA  VARCHAR2(15 BYTE),
  IE_HORARIO_ADEP              VARCHAR2(1 BYTE),
  CD_LOCAL_ESTOQUE_BAIXA       NUMBER(10),
  NR_SEQ_JUSTIF_BLOQUEIO       NUMBER(10),
  DT_DESBLOQUEIO_DIETA         DATE,
  NM_USUARIO_BLOQUEIO          VARCHAR2(15 BYTE),
  NR_REGRA_LOCAL_DISP          NUMBER(10),
  QT_MINUTOS_APRAZ_CG          NUMBER(3),
  NR_SEQ_MAT_COMPL             NUMBER(10),
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREMAHO_ADEPAPR_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_AREA_PREP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_ADEPAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_ADEPRAP_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_REGRA_AREA_PREP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_ADEPRAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_ADEPRFR_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_ETIQUETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_ADEPRFR_FK2_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_ETIQUETA_REAPROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_ADEPRFR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_ADEPROC_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_ADEPROC_I ON TASY.PRESCR_MAT_HOR
(DT_HORARIO, NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_ADEPROC_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_APLOTE_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_CLALODF_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_CLALODF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_DIEJUBL_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_JUSTIF_BLOQUEIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_DIEJUBL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_DISRELOC_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_REGRA_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_DISRELOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_GEETPRO_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_ETAPA_GEDIPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_GEETPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_HDDIALE_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I1 ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, DT_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I2 ON TASY.PRESCR_MAT_HOR
(DT_HORARIO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_I2
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_I3 ON TASY.PRESCR_MAT_HOR
(DT_SUSPENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_I3
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_I4 ON TASY.PRESCR_MAT_HOR
(IE_PENDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_I4
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_I5 ON TASY.PRESCR_MAT_HOR
(DT_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I6 ON TASY.PRESCR_MAT_HOR
(NR_SEQ_HOR_GLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I7 ON TASY.PRESCR_MAT_HOR
(NR_SEQ_PROCESSO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I8 ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, IE_CONTROLADO, IE_PADRONIZADO, IE_CLASSIF_URGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_I9 ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, NR_SEQ_MATERIAL, DT_HORARIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_MATERIA_FK_I ON TASY.PRESCR_MAT_HOR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_MATLOFO_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_MOTSUPR_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_ORDCOMP_FK_I ON TASY.PRESCR_MAT_HOR
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_ORDCOMP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREMAHO_PK ON TASY.PRESCR_MAT_HOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_PREMHOC_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_MAT_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_PRESMAT_FK_I ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_PRESMAT_FK2_I ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_PRESOLU_FK_I ON TASY.PRESCR_MAT_HOR
(NR_PRESCRICAO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_REGTUDI_FK_I ON TASY.PRESCR_MAT_HOR
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAHO_SETATEN_FK_I ON TASY.PRESCR_MAT_HOR
(CD_SETOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_UNIMEDI_FK_I ON TASY.PRESCR_MAT_HOR
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_UNIMEDI_FK2_I ON TASY.PRESCR_MAT_HOR
(CD_UNIDADE_MEDIDA_DOSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_UNIMEDI_FK3_I ON TASY.PRESCR_MAT_HOR
(CD_UNIDADE_MEDIDA_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_UNIMEDI_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAHO_UNIMEDI_FK4_I ON TASY.PRESCR_MAT_HOR
(CD_UNID_MED_HOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAHO_UNIMEDI_FK4_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Prescr_mat_hor_insert
After Insert ON TASY.PRESCR_MAT_HOR FOR EACH ROW
declare

ie_tipo_item_w		varchar2(20);
nr_atendimento_w	number(10);
cd_pessoa_fisica_w	varchar2(10);

BEGIN

if	(:new.ie_agrupador = 1) then
	ie_tipo_item_w	:= 'M';
elsif	(:new.ie_agrupador = 2) then
	ie_tipo_item_w	:= 'MAT';
elsif	(:new.ie_agrupador = 8) then
	ie_tipo_item_w	:= 'SNE';
elsif	(:new.ie_agrupador = 4) then
	ie_tipo_item_w	:= 'SOL';
elsif	(:new.ie_agrupador = 12) then
	ie_tipo_item_w	:= 'S';
elsif	(:new.ie_agrupador = 16) then
	ie_tipo_item_w	:= 'LD';
end if;

nr_atendimento_w	:= :new.nr_atendimento;

if	(nvl(:new.nr_atendimento,0) = 0) then
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = :new.nr_prescricao;
end if;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	prescr_medica
where	nr_prescricao = :new.nr_prescricao;

if	(:new.dt_lib_horario is not null) then
	Atualizar_adep_controle_SC(:new.nm_usuario, nr_atendimento_w, ie_tipo_item_w, 'S', :new.nr_prescricao);
	Atualizar_plt_controle(null, nr_atendimento_w, cd_pessoa_fisica_w, ie_tipo_item_w, 'S', :new.nr_prescricao);
else
	Atualizar_plt_controle(:new.nm_usuario, nr_atendimento_w, cd_pessoa_fisica_w, ie_tipo_item_w, 'S', :new.nr_prescricao);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_mat_hor_ins_upd_del
before insert or update or delete ON TASY.PRESCR_MAT_HOR for each row
declare
	cd_estabelecimento_w 		estabelecimento.cd_estabelecimento%type;
	cd_perfil_w					perfil.cd_perfil%type;
    nm_usuario_w    			usuario.nm_usuario%type;
	ie_ajustar_disp_w			varchar2(255 byte);
	ie_info_rastre_prescr_w		varchar2(1 char);
	ds_alteracao_rastre_w		varchar2(2000 char);
	qt_itens_lote_w				number(5);
begin
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	cd_perfil_w				:= obter_perfil_ativo;
	nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;

	ie_info_rastre_prescr_w := obter_se_info_rastre_prescr('L', nm_usuario_w, cd_perfil_w, cd_estabelecimento_w);

	if (ie_info_rastre_prescr_w = 'S') then
		obter_param_usuario(924, 179, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_ajustar_disp_w);

		if (not deleting) then
			if ((:new.qt_dispensar_hor is null) or
				(ie_ajustar_disp_w <> 'N' and trunc(:new.qt_dispensar_hor) <> :new.qt_dispensar_hor) or
				(updating and nvl(:old.qt_dispensar_hor,-1) <> nvl(:new.qt_dispensar_hor,-1))) then
				ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || 'qt_dispensar_hor(',1,2000);

				if (updating) then
					ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || to_char(:old.qt_dispensar_hor) || '/',1,2000);
				end if;

				ds_alteracao_rastre_w := substr(ds_alteracao_rastre_w || to_char(:new.qt_dispensar_hor) || '); ' || chr(13),1,2000);
			end if;
		else
			ds_alteracao_rastre_w := 'DELETE' || chr(13);
		end if;

		if (ds_alteracao_rastre_w is not null) then
			ds_alteracao_rastre_w := substr('Alteracoes / prescr_mat_hor_ins_upd_del = ' || chr(13) ||
				ds_alteracao_rastre_w ||
				'FUNCAO(' || to_char(obter_funcao_ativa) || '); ' ||
				'PERFIL(' || to_char(obter_perfil_ativo) || ')',1,2000);

			gerar_log_prescricao(
				nr_prescricao_p		=> :new.nr_prescricao,
				nr_seq_item_p		=> :new.nr_seq_material,
				ie_agrupador_p		=> :new.ie_agrupador,
				nr_seq_horario_p	=> :new.nr_sequencia,
				ie_tipo_item_p		=> null,
				ds_log_p			=> ds_alteracao_rastre_w,
				nm_usuario_p		=> nm_usuario_w,
				nr_seq_objeto_p		=> 80667,
				ie_commit_p			=> 'N');
		end if;
	end if;

	if	(deleting) and
		(:old.nr_seq_lote is not null) then

		excluir_horario_lote_prescr(:old.nr_seq_lote,:old.nr_sequencia,nvl(nm_usuario_w,:old.nm_usuario));
	end if;
exception
when others then
	gravar_log_tasy(88098, to_char(sqlerrm), nm_usuario_w);
end;
/


CREATE OR REPLACE TRIGGER TASY.Prescr_mat_hor_atual
BEFORE INSERT OR UPDATE ON TASY.PRESCR_MAT_HOR FOR EACH ROW
declare

dt_validade_w	date;
ds_log_w	varchar2(2000) := '';
nr_seq_log_w	number(10);
nr_atendimento_w number(15);
ie_status_processo_w adep_processo.ie_status_processo%type;
nr_seq_cpoe_w		prescr_material.nr_seq_dieta_cpoe%type;
ie_verificado_w		varchar(1 char);

BEGIN
if	(nvl(:old.DT_HORARIO,sysdate+10) <> :new.DT_HORARIO) and
	(:new.DT_HORARIO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_HORARIO, 'HV');
end if;

if	(:new.nr_seq_turno is not null) and
	((:old.nr_seq_turno is null) or (:new.nr_seq_turno <> :old.nr_seq_turno)) then
	select	nvl(max(ds_turno),null)
	into	:new.ds_turno
	from	regra_turno_disp
	where	nr_sequencia	= :new.nr_seq_turno;
end if;

if	((:new.dt_fim_horario	is not null) or
	 (:new.dt_suspensao	is not null)) and
	(:old.dt_primeira_checagem is not null) then
	:new.dt_primeira_checagem	:= null;
end if;


if  ((nvl(:new.nr_atendimento,0) > 0) and (nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0))) then
	select nvl(max(nr_atendimento),0)
	into nr_atendimento_w
	from prescr_medica
	where nr_prescricao = :new.nr_prescricao;

	if ((nr_atendimento_w > 0) and (nr_atendimento_w <> nvl(:new.nr_atendimento,0))) then
		Wheb_mensagem_pck.exibir_mensagem_abort(173298);
	end if;
end if;


if (:new.nr_seq_processo is not null) then
	select  nvl(max(a.ie_status_processo), 'G')
	into 	ie_status_processo_w
	from 	adep_processo a
	where 	a.nr_sequencia = :new.nr_seq_processo;

	if (:old.dt_primeira_checagem is null
		and :new.dt_primeira_checagem is not null
		and ie_status_processo_w = 'P') then
		update 	adep_processo
		set 	dt_primeira_checagem = :new.dt_primeira_checagem
		where 	nr_sequencia = :new.nr_seq_processo;
	else
		update 	adep_processo
		set 	dt_primeira_checagem = null
		where 	nr_sequencia = :new.nr_seq_processo;
	end if;
end if;

if (nvl(:old.nr_sequencia,0) = 0) then
	:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);
end if;


if (inserting) then

	select	max(nvl(a.nr_seq_mat_cpoe,a.nr_seq_dieta_cpoe))
	into	nr_seq_cpoe_w
	from	prescr_material a
	where	a.nr_sequencia = :new.nr_seq_material
	and		a.nr_prescricao = :new.nr_prescricao;

	select	nvl(max('S'), 'N')
	into	ie_verificado_w
	from	registro_validacao_adep
	where	nr_seq_cpoe = nr_seq_cpoe_w;

	if (ie_verificado_w = 'S') then
		:new.ie_ciente := 'S';
	end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_mat_hor_update
before update ON TASY.PRESCR_MAT_HOR for each row
declare

qt_existe_w				number(5);
ie_status_lote_w		varchar2(3);
ie_tipo_item_w			varchar2(10);
nr_atendimento_w		number(15);
cd_setor_atendimento_w	number(5);
qt_existe_regra_w		number(5);
ie_transf_atend_lote_w	varchar2(1) := 'N';
nr_seq_lote_w			ap_lote.nr_sequencia%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ds_log_w				varchar2(2000);
dt_alteracao_w			date;
qt_lotes_susp_w			number(5);
nr_lote_agrupamento_w	ap_lote.nr_lote_agrupamento%type;

ie_grava_log_gedipa_w	varchar2(1 char);
ie_info_rastre_prescr_w	varchar2(1 char);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	begin
		select	max(a.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	prescr_medica	a
		where	a.nr_prescricao	= :new.nr_prescricao;

		select	ie_grava_log_gedipa
		into	ie_grava_log_gedipa_w
		from	parametros_farmacia
		where	cd_estabelecimento = nvl(cd_estabelecimento_w,wheb_usuario_pck.get_cd_estabelecimento)
		and		rownum = 1;
	exception
	when others then
		ie_grava_log_gedipa_w := 'S';
	end;

	begin
		ie_info_rastre_prescr_w := obter_se_info_rastre_prescr('O', :new.nm_usuario, obter_perfil_ativo, cd_estabelecimento_w);
	exception
	when others then
		ie_info_rastre_prescr_w := 'N';
	end;


	if	(:new.dt_suspensao is not null) and
		(:old.dt_suspensao is null) then
		begin
		update	ap_lote_item
		set	dt_supensao	= sysdate,
			nm_usuario_susp	= :new.nm_usuario_susp
		where	nr_seq_lote	= :new.nr_seq_lote
		and	nr_seq_mat_hor	= :new.nr_sequencia;

		if	(:new.ie_agrupador	= 4) then

			update	adep_processo
			set		ie_inconsistencia	= 'S',
					dt_cancelamento		= sysdate,
					nm_usuario_cancelamento = :new.nm_usuario,
					dt_suspensao = sysdate,
					nm_usuario_susp = :new.nm_usuario
			where	nr_sequencia		= :new.nr_seq_processo;
		else

			update	adep_processo
			set		ie_inconsistencia	= 'S'
			where	nr_sequencia		= :new.nr_seq_processo;

		end if;

		/*Verifica se todos os itens pendentes, ja estao suspensos.. se sim. suspende o lote*/
		select	count(*)
		into	qt_existe_w
		from	ap_lote_item
		where	nr_seq_lote	= :new.nr_seq_lote
		and	dt_supensao is null;

		-- Somente se ele estiver com Status Gerado
		if	(qt_existe_w = 0) then
			select	max(ie_status_lote)
			into	ie_status_lote_w
			from	ap_lote
			where	nr_sequencia	= :new.nr_seq_lote;

			select	nvl(max(b.cd_setor_atendimento),0)
			into	cd_setor_atendimento_w
			from	prescr_medica b,
				ap_lote a
			where	a.nr_prescricao = b.nr_prescricao
			and	nr_sequencia = :new.nr_seq_lote;

			if	(cd_setor_atendimento_w > 0) then
				select	nvl(max(ie_transf_atend_lote),'N')
				into	ie_transf_atend_lote_w
				from	setor_atendimento
				where	cd_setor_atendimento = cd_setor_atendimento_w;
			end if;

			if	(ie_status_lote_w = 'G') or (ie_transf_atend_lote_w = 'S') then
				update	ap_lote
				set	ie_status_lote = 'S'
				where	nr_sequencia	= :new.nr_seq_lote;

				insert into ap_lote_historico(
					nr_sequencia,			dt_atualizacao,
					nm_usuario,			nr_seq_lote,
					ds_evento,			ds_log)
				values(	ap_lote_historico_seq.nextval,	sysdate,
					nvl(:new.nm_usuario_susp, 'Tasy'),			:new.nr_seq_lote,
					--'Suspensao do lote',			'Lote suspenso pela trigger PRESC_MAT_HOR_UPDATE.');
					wheb_mensagem_pck.get_texto(311523),	wheb_mensagem_pck.get_texto(311524));

				select	nvl(max(nr_lote_agrupamento),0)
				into	nr_lote_agrupamento_w
				from 	ap_lote
				where 	nr_sequencia = :new.nr_seq_lote;

				if	(nr_lote_agrupamento_w > 0) then

					select	count(*)
					into	qt_lotes_susp_w
					from 	ap_lote
					where 	nr_lote_agrupamento = nr_lote_agrupamento_w
					and 	nvl(ie_status_lote,'G') <> 'S';

					update	ap_lote
					set		ie_status_lote = 'S'
					where	nr_sequencia	= nr_lote_agrupamento_w
					and 	qt_lotes_susp_w = 0;

					insert into ap_lote_historico(
					nr_sequencia,			dt_atualizacao,
					nm_usuario,			nr_seq_lote,
					ds_evento,			ds_log)
					values(	ap_lote_historico_seq.nextval,	sysdate,
					nvl(:new.nm_usuario_susp, 'Tasy'),			nr_lote_agrupamento_w,
					--'Suspensao do lote',			'Lote suspenso pela trigger PRESC_MAT_HOR_UPDATE.');
					wheb_mensagem_pck.get_texto(311523),	wheb_mensagem_pck.get_texto(311524));

				end if;
			end if;
		end if;

		update	gedi_medic_atend
		set	ie_status	= 'S'
		where	nr_seq_horario	= :new.nr_sequencia;

		nr_atendimento_w	:= :new.nr_atendimento;

		if	(nvl(:new.nr_atendimento,0) = 0) then
			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = :new.nr_prescricao;
		end if;

		susp_itens_gedi_medic_atend(nr_atendimento_w, :new.nr_prescricao, :new.cd_material, :new.nr_sequencia, :new.nr_seq_lote, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento,1));
		integracao_athena_disp_pck.suspender_item_prescricao(:new.nr_prescricao, :new.nr_seq_material, :new.nr_sequencia, :new.nm_usuario, 2, :new.nr_seq_lote);
		supplypoint_susp_prescr(:new.nr_prescricao, :new.nr_seq_material, :new.nr_sequencia, :new.nm_usuario, 2, :new.nr_seq_lote);
		gerar_int_dankia_pck.dankia_suspender_horario(:new.nr_sequencia, :new.nr_prescricao, :new.dt_suspensao);

		end;
	elsif	(:new.dt_suspensao is null) and
		(:old.dt_suspensao is not null) then
		begin

		update	ap_lote_item
		set	dt_supensao	= null,
			nm_usuario_susp	= null,
			ds_maquina_susp	= null
		where	nr_seq_lote	= :new.nr_seq_lote
		and	nr_seq_mat_hor	= :new.nr_sequencia;

		update	adep_processo
		set		ie_inconsistencia	= 'N',
				dt_cancelamento		= null,
				nm_usuario_cancelamento = null,
				dt_suspensao = null,
				nm_usuario_susp = null
		where	nr_sequencia		= :new.nr_seq_processo
		and	ie_inconsistencia	= 'S';


		-- Somente se ele estiver com Status Suspenso.. ele volta para Gerado
		select	max(ie_status_lote)
		into	ie_status_lote_w
		from	ap_lote
		where	nr_sequencia	= :new.nr_seq_lote;

		if	(ie_status_lote_w = 'S') then
			update	ap_lote
			set	ie_status_lote = 'G'
			where	nr_sequencia	= :new.nr_seq_lote;

			insert into ap_lote_historico(
				nr_sequencia,			dt_atualizacao,
				nm_usuario,			nr_seq_lote,
				ds_evento,			ds_log)
			values(	ap_lote_historico_seq.nextval,	sysdate,
				:new.nm_usuario,			:new.nr_seq_lote,
				--'Estorno da suspensao do lote',	'Lote voltou para o Status = Gerado pela trigger PRESC_MAT_HOR_UPDATE.');
				wheb_mensagem_pck.get_texto(311525),	wheb_mensagem_pck.get_texto(311526));

			select	nvl(max(nr_lote_agrupamento),0)
			into	nr_lote_agrupamento_w
			from 	ap_lote
			where 	nr_sequencia = :new.nr_seq_lote;

			if	(nr_lote_agrupamento_w > 0) then

				update	ap_lote
				set		ie_status_lote = 'G'
				where	nr_sequencia	= nr_lote_agrupamento_w;

				insert into ap_lote_historico(
				nr_sequencia,			dt_atualizacao,
				nm_usuario,			nr_seq_lote,
				ds_evento,			ds_log)
				values(	ap_lote_historico_seq.nextval,	sysdate,
				:new.nm_usuario,			nr_lote_agrupamento_w,
				--'Estorno da suspensao do lote',	'Lote voltou para o Status = Gerado pela trigger PRESC_MAT_HOR_UPDATE.');
				wheb_mensagem_pck.get_texto(311525),	wheb_mensagem_pck.get_texto(311526));

			end if;
		end if;
		end;
	end if;

	/*
	if	(:new.dt_emissao_farmacia is not null) and
		(:old.dt_emissao_farmacia is null) and
		(:new.nr_seq_processo is not null) then
		begin
		update	adep_processo
		set	ie_separado	= 'S'
		where	nr_sequencia	= :new.nr_seq_processo;
		end;
	end if;
	*/
	if 	( :new.ie_Agrupador = 12) then
		update	nut_atend_serv_dia
		set 	dt_fim_horario	= :new.dt_fim_horario,
			nm_usuario_adm	= :new.nm_usuario_adm,
			dt_suspensao	= :new.dt_suspensao,
			nm_usuario_susp	= :new.nm_usuario_susp
		where 	nr_seq_horario = :new.nr_sequencia;
	end if;

	if	(:old.dt_suspensao is null) and
		(:new.dt_suspensao is not null) then
		begin
		update	adep_processo_item
		set	dt_suspensao 		= sysdate,
			nm_usuario_suspensao 	= :new.nm_usuario
		where	nr_seq_processo		= :new.nr_seq_processo
		and	nr_seq_horario		= :new.nr_sequencia;
		end;
	elsif	(:old.dt_suspensao is not null) and
		(:new.dt_suspensao is null) then
		begin
		update	adep_processo_item
		set	dt_suspensao 		= null,
			nm_usuario_suspensao 	= null
		where	nr_seq_processo		= :new.nr_seq_processo
		and	nr_seq_horario		= :new.nr_sequencia;
		end;
	end if;

	if	(:old.dt_fim_horario	is null) and
		(:new.dt_fim_horario is not null) and
		(:new.ie_agrupador = 1)	then
		begin

		Gerar_Ganho_Perda_Medic_Dilu(:new.nr_prescricao,:new.nr_seq_material,null,nvl(obter_usuario_ativo,:new.nm_usuario), :new.nr_sequencia);
		exception
		when others then
			null;
		end;
	end if;

	if	(:new.ie_situacao = 'I') and
		(nvl(:old.ie_situacao,'A') <> 'I') then

		update	ap_lote_item
		set	dt_supensao	= sysdate,
			nm_usuario_susp	= :new.nm_usuario
		where	nr_seq_lote	= :new.nr_seq_lote
		and	nr_seq_mat_hor	= :new.nr_sequencia;

		update	adep_processo
		set	ie_inconsistencia	= 'S'
		where	nr_sequencia		= :new.nr_seq_processo;

		/*Verifica se todos os itens pendentes, ja estao suspensos.. se sim. suspende o lote*/
		select	count(*)
		into	qt_existe_w
		from	ap_lote_item
		where	nr_seq_lote	= :new.nr_seq_lote
		and	dt_supensao is null;

		-- Somente se ele estiver com Status Gerado
		if	(qt_existe_w = 0) then
			select	max(ie_status_lote)
			into	ie_status_lote_w
			from	ap_lote
			where	nr_sequencia	= :new.nr_seq_lote;
			if	(ie_status_lote_w = 'G') then
				update	ap_lote
				set	ie_status_lote = 'S'
				where	nr_sequencia	= :new.nr_seq_lote;

				insert into ap_lote_historico(
					nr_sequencia,			dt_atualizacao,
					nm_usuario,			nr_seq_lote,
					ds_evento,			ds_log)
				values(	ap_lote_historico_seq.nextval,	sysdate,
					:new.nm_usuario,			:new.nr_seq_lote,
					--'Suspensao do lote',			'Lote suspenso pela trigger PRESC_MAT_HOR_UPDATE.');
					wheb_mensagem_pck.get_texto(311523),	wheb_mensagem_pck.get_texto(311524));

				select	nvl(max(nr_lote_agrupamento),0)
				into	nr_lote_agrupamento_w
				from 	ap_lote
				where 	nr_sequencia = :new.nr_seq_lote;

				if	(nr_lote_agrupamento_w > 0) then

					select	count(*)
					into	qt_lotes_susp_w
					from 	ap_lote
					where 	nr_lote_agrupamento = nr_lote_agrupamento_w
					and 	nvl(ie_status_lote,'G') <> 'S';

					update	ap_lote
					set		ie_status_lote = 'S'
					where	nr_sequencia	= nr_lote_agrupamento_w
					and 	qt_lotes_susp_w = 0;

					insert into ap_lote_historico(
					nr_sequencia,			dt_atualizacao,
					nm_usuario,			nr_seq_lote,
					ds_evento,			ds_log)
					values(	ap_lote_historico_seq.nextval,	sysdate,
					nvl(:new.nm_usuario_susp, 'Tasy'),			nr_lote_agrupamento_w,
					--'Suspensao do lote',			'Lote suspenso pela trigger PRESC_MAT_HOR_UPDATE.');
					wheb_mensagem_pck.get_texto(311523),	wheb_mensagem_pck.get_texto(311524));

				end if;
			end if;
		end if;
		:new.nr_seq_lote	:= null;
	end if;

	if	(:new.dt_horario 	<> nvl(:old.dt_horario,sysdate -1)) or
		(nvl(:new.dt_suspensao, sysdate - 1) 	<> nvl(:old.dt_suspensao,sysdate - 1)) or
		(nvl(:new.dt_fim_horario, sysdate - 1) <> nvl(:old.dt_fim_horario,sysdate - 1)) or
		(:new.dt_lib_horario <> nvl(:old.dt_lib_horario,sysdate - 1)) or
		(nvl(:new.dt_recusa, sysdate - 1) 	<> nvl(:old.dt_recusa,sysdate -1)) or
		(nvl(:new.dt_bloqueio, sysdate - 1) 	<> nvl(:old.dt_bloqueio,sysdate -1)) then

		if	(:new.ie_agrupador = 1) then
			ie_tipo_item_w	:= 'M';
		elsif	(:new.ie_agrupador = 2) then
			ie_tipo_item_w	:= 'MAT';
		elsif	(:new.ie_agrupador = 8) then
			ie_tipo_item_w	:= 'SNE';
		elsif	(:new.ie_agrupador = 4) then
			ie_tipo_item_w	:= 'SOL';
		elsif	(:new.ie_agrupador = 12) then
			ie_tipo_item_w	:= 'S';
		elsif	(:new.ie_agrupador = 16) then
			ie_tipo_item_w	:= 'LD';
		end if;

		nr_atendimento_w	:= :new.nr_atendimento;

		begin

		if	(nvl(:new.nr_atendimento,0) = 0) then
			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = :new.nr_prescricao;
		end if;

		Atualizar_adep_controle_SC(:new.nm_usuario, nr_atendimento_w, ie_tipo_item_w, 'S', :new.nr_prescricao);
		Atualizar_plt_controle(null, nr_atendimento_w, null, ie_tipo_item_w, 'S', :new.nr_prescricao);
		exception when others then
		null;
		end;
	end if;

	begin
		select	cd_setor_atendimento
		into	cd_setor_atendimento_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao
		and	 	rownum = 1;
	exception
	when others then
		cd_setor_atendimento_w := null;
	end;

	if	(cd_setor_atendimento_w is not null) then
		/* Rorina de atualizacao do local de estoque para integracao de dispensario DANKIA
		Tem a trigger disp_int_ap_lote_local que atualiza o local de estoque*/
		begin

		select	count(nr_sequencia)
		into	qt_existe_regra_w
		from	dis_regra_setor
		where	cd_setor_atendimento = cd_setor_atendimento_w;

		if	(qt_existe_regra_w > 0) and (:new.dt_suspensao is not null) then

			update	dispensario_mat_hor
			set	dt_suspensao	= :new.dt_suspensao,
				ds_motivo_susp	= :new.ds_motivo_susp,
				ie_lido		= ''
			where	nr_seq_mat_hor	= :new.nr_sequencia;

		end if;
		exception
			when others then
			null;
		end;

		/* Rorina de integracao de dispensario Pyxis */
		begin
		select	count(a.nr_sequencia)
		into	qt_existe_regra_w
		from	dis_regra_setor a,
				dis_regra_local_setor b
		where	a.nr_sequencia = b.nr_seq_dis_regra_setor
		and 	a.cd_setor_atendimento = cd_setor_atendimento_w
		and 	b.cd_local_estoque is not null;

		if	(qt_existe_regra_w > 0) and (:old.dt_suspensao is null) and (:new.dt_suspensao is not null) then
			intdisp_gerar_mat_hor_susp(:new.nr_prescricao,:new.nr_sequencia,2,:new.nm_usuario);
		end if;

		exception
			when others then
			null;
		end;
	end if;

	if	(nvl(:old.qt_dispensar_hor,0) <> nvl(:new.qt_dispensar_hor,0)) then
		nr_seq_lote_w := nvl(:new.nr_seq_lote,0);
		if	(nvl(:new.nr_seq_lote,0) = 0)  then
			select	nvl(max(nr_seq_lote),0)
			into	nr_seq_lote_w
			from	ap_lote_item
			where	nr_seq_mat_hor = :new.nr_sequencia;
		end if;

		update	ap_lote_item a
		set	a.qt_dispensar = :new.qt_dispensar_hor
		where	a.nr_seq_mat_hor = :new.nr_sequencia
		and exists (select 1 from ap_lote b where b.nr_sequencia = a.nr_seq_lote and b.ie_status_lote = 'G');

		if	(nr_seq_lote_w > 0) then
			insert into ap_lote_historico(
				nr_sequencia,				dt_atualizacao,
				nm_usuario,				nr_seq_lote,
				ds_evento,				ds_log)
			values(	ap_lote_historico_seq.nextval,		sysdate,
				nvl(:new.nm_usuario, 'Tasy'),		:new.nr_seq_lote,
				wheb_mensagem_pck.get_texto(322408),	wheb_mensagem_pck.get_texto(322410,'OLD_QT='||nvl(:old.qt_dispensar_hor,0)||';NEW_QT='||nvl(:new.qt_dispensar_hor,0)));
				/* Codigo () = Alteracao item | Codigo () = Alterada a quantidade a dispensar do item na tabela PRESCR_MAT_HOR. Quantidade (old/new) = */
		end if;
	end if;

	if	(:old.dt_suspensao is null) and
		(:new.dt_suspensao is not null) then
		-- Rotina para suspencao do item da prescricao na integracao
		swisslog_suspender_prescr(:new.nr_prescricao,0,:new.nr_sequencia,:new.nm_usuario,3);
	end if;

	if	(ie_grava_log_gedipa_w = 'S') and
		(nvl(:old.nr_seq_processo,0) <> nvl(:new.nr_seq_processo,0)) then
		insert into log_gedipa (	nr_sequencia, dt_log, nr_log,
									nm_objeto_execucao, nm_objeto_chamado,
									ds_parametros,
									ds_log, nr_seq_processo)
						values (	obter_nextval_sequence('log_gedipa'), sysdate, 2100,
									'PRESCR_MAT_HOR_UPDATE', 'PRESCR_MAT_HOR_UPDATE',
									substr('NR_SEQ_PROCESSO OLD/NEW(' || to_char(:old.nr_seq_processo) || '/' || to_char(:new.nr_seq_processo) || ') NR_SEQ_HORARIO = ' || :new.nr_sequencia,1,1999),
									substr(wheb_mensagem_pck.get_texto(311528) || chr(10) || 'STACK: ' || dbms_utility.format_call_stack,1,1999),
									nvl(:new.nr_seq_processo,:old.nr_seq_processo));
	end if;

	if	(nvl(:old.nr_seq_turno,0) <> nvl(:new.nr_seq_turno,0)) then
		ds_log_w := ds_log_w || ' TURNO OLD/NEW(' ||:old.nr_seq_turno || '/' || :new.nr_seq_turno || ')';
	end if;
	if	(nvl(:old.cd_local_estoque,0) <> nvl(:new.cd_local_estoque,0)) then
		ds_log_w := ds_log_w || ' CD_LOCAL_ESTOQUE OLD/NEW(' ||:old.cd_local_estoque || '/' || :new.cd_local_estoque || ')';
	end if;
	if	(nvl(:old.nr_seq_classif,0) <> nvl(:new.nr_seq_classif,0)) then
		ds_log_w := ds_log_w || ' NR_SEQ_CLASSIF OLD/NEW(' ||:old.nr_seq_classif || '/' || :new.nr_seq_classif || ')';
	end if;
	if	(nvl(:old.nr_seq_lote,0) <> nvl(:new.nr_seq_lote,0)) then
		ds_log_w := ds_log_w || ' NR_SEQ_LOTE OLD/NEW(' ||:old.nr_seq_lote || '/' || :new.nr_seq_lote || ')';
	end if;

	if	((nvl(:old.dt_suspensao, sysdate) <> nvl(:new.dt_suspensao,sysdate)) or (:old.dt_suspensao is null and :new.dt_suspensao is not null)) then
		ds_log_w := substr(ds_log_w || ' DT_SUSPENSAO OLD/NEW(' || to_char(:old.DT_SUSPENSAO, 'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.DT_SUSPENSAO, 'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
	end if;

	if	(nvl(:old.nm_usuario_susp, 'XPTO') <> nvl(:new.nm_usuario_susp, 'XPTO')) then
		ds_log_w	:= substr(ds_log_w || ' NM_USUARIO_SUSP OLD/NEW(' || :old.nm_usuario_susp || '/' || :new.nm_usuario_susp||'); ',1,1800);
	end if;

	if	(:old.dt_lib_horario is null and :new.dt_lib_horario is not null or nvl(:old.dt_lib_horario, sysdate) <> nvl(:new.dt_lib_horario, sysdate)) then
		ds_log_w	:= substr(ds_log_w || ' DT_LIB_HORARIO(' || nvl(to_char(:old.dt_lib_horario, 'dd/mm/yyyy hh24:mi:ss'),'null') || ' / ' || nvl(to_char(:new.dt_lib_horario, 'dd/mm/yyyy hh24:mi:ss'),'null')||'); ',1,1800);
	end if;

	if	(nvl(:old.ie_gerar_lote, 'XPTO') <> nvl(:new.ie_gerar_lote, 'XPTO')) then
		ds_log_w	:= substr(ds_log_w || ' IE_GERAR_LOTE(' || nvl(:old.ie_gerar_lote,'XPTO') || '/' || nvl(:new.ie_gerar_lote,'XPTO')||'); ',1,1800);
	end if;

	if 	(ie_grava_log_gedipa_w = 'S' and (nvl(:old.ie_gedipa, 'XPTO') <> nvl(:new.ie_gedipa, 'XPTO'))) then
		ds_log_w	:= substr(ds_log_w || ' IE_GEDIPA OLD/NEW(' || nvl(:old.ie_gedipa,'XPTO') || '/' || nvl(:new.ie_gedipa,'XPTO')||'); ',1,1800);
	end if;

	if 	(ie_grava_log_gedipa_w = 'S' and (nvl(:old.nr_seq_superior,0) <> nvl(:new.nr_seq_superior,0))) then
		ds_log_w	:= substr(ds_log_w || ' NR_SEQ_SUPERIOR OLD/NEW(' || :old.nr_seq_superior || '/' || :new.nr_seq_superior ||'); ',1,1800);
	end if;

	if 	(((ie_info_rastre_prescr_w = 'S') or (ie_grava_log_gedipa_w = 'S')) and (nvl(:old.nr_seq_area_prep,0) <> nvl(:new.nr_seq_area_prep,0))) then
		ds_log_w 	:= substr(ds_log_w || ' NR_SEQ_AREA_PREP OLD/NEW(' || :old.nr_seq_area_prep || '/' || :new.nr_seq_area_prep ||'); ',1,1800);
	end if;

	if 	(((ie_info_rastre_prescr_w = 'S') or (ie_grava_log_gedipa_w = 'S')) and (nvl(:old.nr_seq_regra_area_prep,0) <> nvl(:new.nr_seq_regra_area_prep,0))) then
		ds_log_w 	:= substr(ds_log_w || ' NR_SEQ_REGRA_AREA_PREP OLD/NEW(' || :old.nr_seq_regra_area_prep || '/' || :new.nr_seq_regra_area_prep ||'); ',1,1800);
	end if;

	if 	(ie_grava_log_gedipa_w = 'S' and (nvl(:old.nr_seq_etiqueta,0) <> nvl(:new.nr_seq_etiqueta,0))) then
		ds_log_w 	:= substr(ds_log_w || ' NR_SEQ_ETIQUETA OLD/NEW(' || :old.nr_seq_etiqueta || '/' || :new.nr_seq_etiqueta ||'); ',1,1800);
	end if;

    if 	(ie_grava_log_gedipa_w = 'S' and (nvl(:old.nr_atendimento,0) <> nvl(:new.nr_atendimento,0))) then
        ds_log_w 	:= substr(ds_log_w || ' NR_ATENDIMENTO OLD/NEW(' || :old.nr_atendimento || '/' || :new.nr_atendimento ||'); ',1,1800);
    end if;

	if	(ds_log_w is not null) then
		ds_log_w :=  wheb_mensagem_pck.get_texto(952753)/*'Sequencia: '*/ || :new.nr_sequencia ||' - '|| wheb_mensagem_pck.get_texto(952754)/*' - prescricao - '*/ ||' - '|| :new.nr_prescricao || ' - ' || ds_log_w || ' - ' ||
		substr(dbms_utility.format_call_stack,1,1800);
		gravar_log_tasy(30, ds_log_w, :new.nm_usuario);

		if (ie_info_rastre_prescr_w = 'S') then
			gerar_log_prescr_mat(:new.nr_prescricao, :new.nr_sequencia, :new.ie_agrupador, null, null, ds_log_w, :new.nm_usuario, 'N');
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.smart_prescr_mat_hor
after update or insert ON TASY.PRESCR_MAT_HOR FOR EACH ROW
declare
	reg_integracao_w			gerar_int_padrao.reg_integracao;
	qt_medicamento_w        	number(10);
    ie_acm_sn_w             	varchar2(1);
	ie_primeiro_aprazamento_w   varchar2(1);

	pragma autonomous_transaction;

begin
begin
	select nvl(max(OBTER_SE_ACM_SN(IE_ACM,IE_SE_NECESSARIO)),'N')
	into ie_acm_sn_w
	from prescr_material
	where nr_sequencia = :NEW.NR_SEQ_MATERIAL
	and nr_prescricao  = :new.NR_PRESCRICAO;

    select 	nvl(max('N'),'S')
	into 	ie_primeiro_aprazamento_w
	from 	prescr_mat_hor
	where 	nr_seq_material = :new.nr_seq_material
	and 	nr_prescricao = :new.nr_prescricao
	and 	ie_aprazado = 'S'
	and 	obter_se_medic_cih(cd_material) = 'S';

	if 	((((:OLD.DT_SUSPENSAO IS NULL)
	    AND(:NEW.DT_SUSPENSAO IS NOT NULL))
	    OR (:NEW.DT_HORARIO 	<> :OLD.DT_HORARIO)
		AND (UPDATING))
			OR
		((NVL(:NEW.IE_APRAZADO,'N') = 'S')
		and (INSERTING)))
		and (obter_se_medic_cih(:new.cd_material) = 'S')
	then
		begin
			reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;

			if  ((ie_acm_sn_w = 'S') and (ie_primeiro_aprazamento_w = 'S')) then
				gerar_int_padrao.gravar_integracao('305', :new.NR_PRESCRICAO , :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :NEW.NR_SEQ_MATERIAL || ';DS_OPERACAO=CREATE');
				commit;
			else
				gerar_int_padrao.gravar_integracao('305', :new.NR_PRESCRICAO , :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :NEW.NR_SEQ_MATERIAL || ';DS_OPERACAO=UPDATE' );
				commit;
			end if;
		END;
	end if;
exception when others then
	null;
end;

END;
/


ALTER TABLE TASY.PRESCR_MAT_HOR ADD (
  CONSTRAINT PREMAHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_MAT_HOR ADD (
  CONSTRAINT PREMAHO_DIEJUBL_FK 
 FOREIGN KEY (NR_SEQ_JUSTIF_BLOQUEIO) 
 REFERENCES TASY.DIETA_JUSTIF_BLOQUEIO (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_PREMHOC_FK 
 FOREIGN KEY (NR_SEQ_MAT_COMPL) 
 REFERENCES TASY.PRESCR_MAT_HOR_COMPL (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_ADEPRFR_FK2 
 FOREIGN KEY (NR_SEQ_ETIQUETA_REAPROV) 
 REFERENCES TASY.ADEP_PROCESSO_FRAC (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_PRESOLU_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PRESCR_SOLUCAO (NR_PRESCRICAO,NR_SEQ_SOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT PREMAHO_PRESMAT_FK2 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_SUPERIOR) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAHO_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAHO_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT PREMAHO_DISRELOC_FK 
 FOREIGN KEY (NR_SEQ_REGRA_DISP) 
 REFERENCES TASY.DIS_REGRA_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_GEETPRO_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GEDIPA) 
 REFERENCES TASY.GEDIPA_ETAPA_PRODUCAO (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_REGTUDI_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.REGRA_TURNO_DISP (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREMAHO_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_DOSE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREMAHO_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAHO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PREMAHO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PREMAHO_CLALODF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_LOTE_DISP_FAR (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_CONTA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREMAHO_UNIMEDI_FK4 
 FOREIGN KEY (CD_UNID_MED_HOR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREMAHO_ADEPRFR_FK 
 FOREIGN KEY (NR_SEQ_ETIQUETA) 
 REFERENCES TASY.ADEP_PROCESSO_FRAC (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_ADEPAPR_FK 
 FOREIGN KEY (NR_SEQ_AREA_PREP) 
 REFERENCES TASY.ADEP_AREA_PREP (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_ADEPRAP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_AREA_PREP) 
 REFERENCES TASY.ADEP_REGRA_AREA_PREP (NR_SEQUENCIA),
  CONSTRAINT PREMAHO_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_MAT_HOR TO NIVEL_1;

