ALTER TABLE TASY.PTU_PRESTADOR_QUALIFIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_PRESTADOR_QUALIFIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_PRESTADOR_QUALIFIC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR      NUMBER(10)              NOT NULL,
  CD_CNPJ_CPF           VARCHAR2(15 BYTE)       NOT NULL,
  CD_INSTITUICAO_ACRED  VARCHAR2(5 BYTE)        DEFAULT null,
  IE_NIVEL_ACREDITACAO  NUMBER(2)               NOT NULL,
  NR_REFERENCIA_END     NUMBER(2)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUPQLF_PK ON TASY.PTU_PRESTADOR_QUALIFIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUPQLF_PTUPRES_FK_I ON TASY.PTU_PRESTADOR_QUALIFIC
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_PRESTADOR_QUALIFIC ADD (
  CONSTRAINT PTUPQLF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_PRESTADOR_QUALIFIC ADD (
  CONSTRAINT PTUPQLF_PTUPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PTU_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_PRESTADOR_QUALIFIC TO NIVEL_1;

