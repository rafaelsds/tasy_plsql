ALTER TABLE TASY.BANDEIRA_CARTAO_CAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANDEIRA_CARTAO_CAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANDEIRA_CARTAO_CAIXA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_BANDEIRA      NUMBER(10)               NOT NULL,
  NR_SEQ_CAIXA         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANCARCAI_BANCACR_FK_I ON TASY.BANDEIRA_CARTAO_CAIXA
(NR_SEQ_BANDEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BANCARCAI_CAIXA_FK_I ON TASY.BANDEIRA_CARTAO_CAIXA
(NR_SEQ_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BANCARCAI_PK ON TASY.BANDEIRA_CARTAO_CAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BANDEIRA_CARTAO_CAIXA ADD (
  CONSTRAINT BANCARCAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.BANDEIRA_CARTAO_CAIXA ADD (
  CONSTRAINT BANCARCAI_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT BANCARCAI_CAIXA_FK 
 FOREIGN KEY (NR_SEQ_CAIXA) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA));

GRANT SELECT ON TASY.BANDEIRA_CARTAO_CAIXA TO NIVEL_1;

