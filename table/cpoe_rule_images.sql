ALTER TABLE TASY.CPOE_RULE_IMAGES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_RULE_IMAGES CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_RULE_IMAGES
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_RULE_PROC_IMAGES  NUMBER(10)           NOT NULL,
  DS_NOME_ARQUIVO          VARCHAR2(255 BYTE)   NOT NULL,
  IM_ARQUIVO               LONG RAW
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CPOERULEIM_PK ON TASY.CPOE_RULE_IMAGES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOERULEIM_RULEPROCIM_FK_I ON TASY.CPOE_RULE_IMAGES
(NR_SEQ_RULE_PROC_IMAGES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_RULE_IMAGES ADD (
  CONSTRAINT CPOERULEIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_RULE_IMAGES ADD (
  CONSTRAINT CPOERULEIM_RULEPROCIM_FK 
 FOREIGN KEY (NR_SEQ_RULE_PROC_IMAGES) 
 REFERENCES TASY.CPOE_RULE_PROC_IMAGES (NR_SEQUENCIA)
    ON DELETE CASCADE);

