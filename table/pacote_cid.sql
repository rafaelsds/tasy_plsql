ALTER TABLE TASY.PACOTE_CID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACOTE_CID CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACOTE_CID
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_DOENCA            VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_PACOTE        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCTECID_CIDDOEN_FK_I ON TASY.PACOTE_CID
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCTECID_PACOTE_FK_I ON TASY.PACOTE_CID
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCTECID_PK ON TASY.PACOTE_CID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PACOTE_CID_tp  after update ON TASY.PACOTE_CID FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'PACOTE_CID',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACOTE_CID ADD (
  CONSTRAINT PCTECID_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PACOTE_CID ADD (
  CONSTRAINT PCTECID_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PCTECID_PACOTE_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PACOTE (NR_SEQ_PACOTE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PACOTE_CID TO NIVEL_1;

