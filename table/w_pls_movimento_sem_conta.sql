ALTER TABLE TASY.W_PLS_MOVIMENTO_SEM_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_MOVIMENTO_SEM_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_MOVIMENTO_SEM_CONTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ITEM                VARCHAR2(20 BYTE),
  DS_ITEM                VARCHAR2(255 BYTE),
  IE_TIPO_ITEM           VARCHAR2(3 BYTE),
  VL_ITEM                NUMBER(15,2),
  DT_REFERENCIA          DATE,
  NR_LOTE_CONTABIL       NUMBER(10),
  IE_PROC_MAT_ITEM       VARCHAR2(2 BYTE),
  IE_DEB_CRED            VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_TIPO_ATO_COOPERADO  VARCHAR2(2 BYTE),
  IE_CONTA_ANTECIPACAO   VARCHAR2(1 BYTE),
  CD_TIPO_LOTE_CONTABIL  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSMSC_PK ON TASY.W_PLS_MOVIMENTO_SEM_CONTA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSMSC_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_MOVIMENTO_SEM_CONTA ADD (
  CONSTRAINT WPLSMSC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_MOVIMENTO_SEM_CONTA TO NIVEL_1;

