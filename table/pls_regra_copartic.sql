ALTER TABLE TASY.PLS_REGRA_COPARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_TIPO_COPARTICIPACAO  NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA          DATE,
  DT_FIM_VIGENCIA             DATE,
  IE_BENEFICIARIO             VARCHAR2(1 BYTE),
  IE_PRESTADOR                VARCHAR2(1 BYTE),
  IE_CONTA_MEDICA             VARCHAR2(1 BYTE),
  IE_UTILIZACAO               VARCHAR2(1 BYTE),
  NR_SEQ_PLANO                NUMBER(10),
  NR_SEQ_CONTRATO             NUMBER(10),
  NR_SEQ_INTERCAMBIO          NUMBER(10),
  IE_TIPO_ATENDIMENTO         VARCHAR2(1 BYTE),
  VL_MAXIMO_COPARTIC          NUMBER(15,2),
  IE_GUIA                     VARCHAR2(1 BYTE),
  NR_SEQ_PROPOSTA             NUMBER(10),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIBERACAO        VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_REAJ_COPARTIC   NUMBER(10),
  NR_SEQ_REGRA_REAJUSTADA     NUMBER(10),
  CD_SISTEMA_ANTERIOR         VARCHAR2(255 BYTE),
  IE_FORMA_COBR_INTERNACAO    VARCHAR2(1 BYTE),
  IE_INTERNACAO               VARCHAR2(1 BYTE),
  NR_ORDEM_PRIORIDADE         NUMBER(10),
  NR_SEQ_MOT_REEMBOLSO        NUMBER(10),
  IE_TP_TRANSACAO             VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCP_PK ON TASY.PLS_REGRA_COPARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSCONT_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSINCA_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSLORC_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_LOTE_REAJ_COPARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSMORE_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_MOT_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSPLAN_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSPRAD_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_REGRA_REAJUSTADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCP_PLTICOP_FK_I ON TASY.PLS_REGRA_COPARTIC
(NR_SEQ_TIPO_COPARTICIPACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copartic_update
before update ON TASY.PLS_REGRA_COPARTIC for each row
declare

begin
if	(:new.dt_inicio_vigencia <> :old.dt_inicio_vigencia)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.dt_inicio_vigencia, :new.dt_inicio_vigencia, 'PLS_REGRA_COPARTIC', 'DT_INICIO_VIGENCIA', :new.nm_usuario);
end if;
if	(:new.dt_fim_vigencia <> :old.dt_fim_vigencia)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.dt_fim_vigencia, :new.dt_fim_vigencia, 'PLS_REGRA_COPARTIC', 'DT_FIM_VIGENCIA', :new.nm_usuario);
end if;
if	(:new.nr_seq_tipo_coparticipacao <> :old.nr_seq_tipo_coparticipacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.nr_seq_tipo_coparticipacao, :new.nr_seq_tipo_coparticipacao, 'PLS_REGRA_COPARTIC', 'NR_SEQ_TIPO_COPARTICIPACAO', :new.nm_usuario);
end if;
if	(:new.ie_tipo_atendimento <> :old.ie_tipo_atendimento)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_tipo_atendimento, :new.ie_tipo_atendimento, 'PLS_REGRA_COPARTIC', 'IE_TIPO_ATENDIMENTO', :new.nm_usuario);
end if;
if	(:new.ie_forma_cobr_internacao <> :old.ie_forma_cobr_internacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_forma_cobr_internacao, :new.ie_forma_cobr_internacao, 'PLS_REGRA_COPARTIC', 'IE_FORMA_COBR_INTERNACAO', :new.nm_usuario);
end if;
if	(:new.vl_maximo_copartic <> :old.vl_maximo_copartic)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.vl_maximo_copartic, :new.vl_maximo_copartic, 'PLS_REGRA_COPARTIC', 'VL_MAXIMO_COPARTIC', :new.nm_usuario);
end if;
if	(:new.dt_liberacao <> :old.dt_liberacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.dt_liberacao, :new.dt_liberacao, 'PLS_REGRA_COPARTIC', 'DT_LIBERACAO', :new.nm_usuario);
end if;
if	(:new.nm_usuario_liberacao <> :old.nm_usuario_liberacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.nm_usuario_liberacao, :new.nm_usuario_liberacao, 'PLS_REGRA_COPARTIC', 'NM_USUARIO_LIBERACAO', :new.nm_usuario);
end if;
if	(:new.nr_ordem_prioridade <> :old.nr_ordem_prioridade)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.nr_ordem_prioridade, :new.nr_ordem_prioridade, 'PLS_REGRA_COPARTIC', 'NR_ORDEM_PRIORIDADE', :new.nm_usuario);
end if;
if	(:new.ie_beneficiario <> :old.ie_beneficiario)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_beneficiario, :new.ie_beneficiario, 'PLS_REGRA_COPARTIC', 'IE_BENEFICIARIO', :new.nm_usuario);
end if;
if	(:new.ie_conta_medica <> :old.ie_conta_medica)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_conta_medica, :new.ie_conta_medica, 'PLS_REGRA_COPARTIC', 'IE_CONTA_MEDICA', :new.nm_usuario);
end if;
if	(:new.ie_utilizacao <> :old.ie_utilizacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_utilizacao, :new.ie_utilizacao, 'PLS_REGRA_COPARTIC', 'IE_UTILIZACAO', :new.nm_usuario);
end if;
if	(:new.ie_prestador <> :old.ie_prestador)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_prestador, :new.ie_prestador, 'PLS_REGRA_COPARTIC', 'IE_PRESTADOR', :new.nm_usuario);
end if;
if	(:new.ie_guia <> :old.ie_guia)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_guia, :new.ie_guia, 'PLS_REGRA_COPARTIC', 'IE_GUIA', :new.nm_usuario);
end if;
if	(:new.ie_internacao <> :old.ie_internacao)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.ie_internacao, :new.ie_internacao, 'PLS_REGRA_COPARTIC', 'IE_INTERNACAO', :new.nm_usuario);
end if;
if	(:new.nr_seq_mot_reembolso <> :old.nr_seq_mot_reembolso)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_sequencia, :old.nr_seq_mot_reembolso, :new.nr_seq_mot_reembolso, 'PLS_REGRA_COPARTIC', 'NR_SEQ_MOT_REEMBOLSO', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC ADD (
  CONSTRAINT PLSRGCP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC ADD (
  CONSTRAINT PLSRGCP_PLSMORE_FK 
 FOREIGN KEY (NR_SEQ_MOT_REEMBOLSO) 
 REFERENCES TASY.PLS_MOTIVO_REEMBOLSO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSLORC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_REAJ_COPARTIC) 
 REFERENCES TASY.PLS_LOTE_REAJ_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_REAJUSTADA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSRGCP_PLTICOP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COPARTICIPACAO) 
 REFERENCES TASY.PLS_TIPO_COPARTICIPACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC TO NIVEL_1;

