ALTER TABLE TASY.PLS_LOTE_DISCUSSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_DISCUSSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_DISCUSSAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_CONTEST         NUMBER(10)        NOT NULL,
  DT_REFERENCIA               DATE              NOT NULL,
  IE_STATUS                   VARCHAR2(3 BYTE)  NOT NULL,
  DT_FECHAMENTO               DATE,
  NR_SEQ_LOTE_CONTA           NUMBER(10),
  NR_NOTA_CREDITO_DEBITO      VARCHAR2(30 BYTE),
  NR_LOTE_CONTABIL            NUMBER(10)        NOT NULL,
  VL_APRESENTADO              NUMBER(15,2),
  IE_TIPO_LOTE                VARCHAR2(3 BYTE),
  NR_TITULO_RECEBER           NUMBER(10),
  NR_TITULO_PAGAR             NUMBER(10),
  IE_PRIMEIRA_DISCUSSAO       VARCHAR2(1 BYTE),
  VL_TOT_ACO_SERVICO          NUMBER(15,2),
  VL_TOT_ACO_TAXA             NUMBER(15,2),
  NR_TITULO_RECEBER_NDR       NUMBER(10),
  NR_NOTA_CREDITO_DEBITO_NDR  VARCHAR2(30 BYTE),
  IE_TIPO_ARQUIVO             NUMBER(5),
  NR_TITULO_PAGAR_NDR         NUMBER(10),
  NM_USUARIO_CANCELAMENTO     VARCHAR2(15 BYTE),
  DT_CANCELAMENTO             DATE,
  DS_HASH_A550                VARCHAR2(255 BYTE),
  NR_LOTE_CONTABIL_CANCEL     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLODI_I1 ON TASY.PLS_LOTE_DISCUSSAO
(DT_FECHAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLODI_LOTCONT_FK_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_LOTCONT_FK2_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_LOTE_CONTABIL_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSLODI_PK ON TASY.PLS_LOTE_DISCUSSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_PLSLOCO_FK_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_SEQ_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_PLSLOPC_FK_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_SEQ_LOTE_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_PLSLOPC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_TITPAGA_FK_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_TITPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_TITPAGA_FK2_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_TITULO_PAGAR_NDR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_TITPAGA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_TITRECE_FK_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLODI_TITRECE_FK2_I ON TASY.PLS_LOTE_DISCUSSAO
(NR_TITULO_RECEBER_NDR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLODI_TITRECE_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_LOTE_DISCUSSAO_BEF_INSERT
BEFORE INSERT ON TASY.PLS_LOTE_DISCUSSAO FOR EACH ROW
DECLARE

ie_status_w	pls_lote_contestacao.ie_status%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	/* Quando incluir uma nova discuss�o, mudar o status do lote de contesta��o caso esteja em aberto */
	select	a.ie_status
	into	ie_status_w
	from	pls_lote_contestacao a
	where	a.nr_sequencia	= :new.nr_seq_lote_contest;

	if	(ie_status_w not in ('E','D','C')) then
		wheb_mensagem_pck.exibir_mensagem_abort(239436);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_lote_discussao_insert
after insert ON TASY.PLS_LOTE_DISCUSSAO for each row
declare
ie_status_w		pls_lote_contestacao.ie_status%type := 'D'; -- Contesta��o Em discuss�o
nr_seq_pls_fatura_w	pls_lote_contestacao.nr_seq_pls_fatura%type;
nr_seq_ptu_fatura_w	pls_lote_contestacao.nr_seq_ptu_fatura%type;
qt_integral_w		pls_integer;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	select	max(nr_seq_pls_fatura),
		max(nr_seq_ptu_fatura)
	into	nr_seq_pls_fatura_w,
		nr_seq_ptu_fatura_w
	from	pls_lote_contestacao
	where	nr_sequencia	= :new.nr_seq_lote_contest;

	-- No momento que importa um A550 de faturamento e gerar discuss�o - OS 605067
	-- Contesta��o da OPS - Faturamento
	if	(nr_seq_pls_fatura_w is not null) then
		if	(:new.ie_status = 'A') then
			-- Contesta��o Enviada/Recebida
			ie_status_w := 'E';
		end if;

		-- Quando incluir uma nova discuss�o, mudar o status do lote de contesta��o caso esteja em aberto
		update	pls_lote_contestacao
		set	ie_status	= ie_status_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_lote_contest
		and	ie_status	= 'E';

		-- compatibilidade com a rotina de altera��o de status, se chamar ela aqui pode gerar o erro de mutante
		if	(ie_status_w = 'C') then

			update	pls_contestacao_discussao
			set	ie_status	= 'E',
				nm_usuario	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	nr_seq_lote	= :new.nr_sequencia;
		end if;
	end if;

	-- Contesta��o da OPS - Contas de Interc�mbio (A500)
	if	(nr_seq_ptu_fatura_w is not null) then
		qt_integral_w := 0;
		if	(:new.ie_tipo_arquivo in (5,6,7,8,9)) then
			-- Verificar se o decurso de prazo � complementar ou parcial
			if	(:new.ie_tipo_arquivo in (9)) then
				-- Se existir ao menos um questionamento com tipo de acordo 10, ent�o a discuss�o dever� ser concluida
				-- Feito na OS 909630 - dia 16-03-2017
				select	count(1)
				into	qt_integral_w
				from	ptu_camara_contestacao	c,
					ptu_questionamento	q
				where	c.nr_sequencia		= q.nr_seq_contestacao
				and	c.nr_seq_lote_contest	= :new.nr_seq_lote_contest
				and	q.ie_tipo_acordo	in ('10', '15'); -- Encerrado pelo administrador
			end if;

			-- Contesta��o Concluida
			if	(qt_integral_w > 0) then
				ie_status_w := 'C';
			end if;
		end if;

		update	pls_lote_contestacao
		set	ie_status	= ie_status_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_lote_contest
		and	ie_status	in ('E','D');


		-- compatibilidade com a rotina de altera��o de status, se chamar ela aqui pode gerar o erro de mutante
		if	(ie_status_w = 'C') then

			update	pls_contestacao_discussao
			set	ie_status	= 'E',
				nm_usuario	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	nr_seq_lote	= :new.nr_sequencia;
		end if;
	end if;

end if;

end;
/


ALTER TABLE TASY.PLS_LOTE_DISCUSSAO ADD (
  CONSTRAINT PLSLODI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_DISCUSSAO ADD (
  CONSTRAINT PLSLODI_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_CANCEL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSLODI_TITPAGA_FK2 
 FOREIGN KEY (NR_TITULO_PAGAR_NDR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PLSLODI_TITRECE_FK2 
 FOREIGN KEY (NR_TITULO_RECEBER_NDR) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT PLSLODI_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSLODI_PLSLOPC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CONTA) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSLODI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSLODI_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PLSLODI_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO));

GRANT SELECT ON TASY.PLS_LOTE_DISCUSSAO TO NIVEL_1;

