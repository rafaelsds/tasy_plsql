ALTER TABLE TASY.MATERIAL_FAQ_RESPOSTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_FAQ_RESPOSTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_FAQ_RESPOSTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PERGUNTA      NUMBER(10)               NOT NULL,
  DS_RESPOSTA          VARCHAR2(2000 BYTE)      NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATFAQR_MATFAQP_FK_I ON TASY.MATERIAL_FAQ_RESPOSTA
(NR_SEQ_PERGUNTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATFAQR_PK ON TASY.MATERIAL_FAQ_RESPOSTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATFAQR_PK
  MONITORING USAGE;


ALTER TABLE TASY.MATERIAL_FAQ_RESPOSTA ADD (
  CONSTRAINT MATFAQR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_FAQ_RESPOSTA ADD (
  CONSTRAINT MATFAQR_MATFAQP_FK 
 FOREIGN KEY (NR_SEQ_PERGUNTA) 
 REFERENCES TASY.MATERIAL_FAQ_PERGUNTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MATERIAL_FAQ_RESPOSTA TO NIVEL_1;

