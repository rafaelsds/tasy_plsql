ALTER TABLE TASY.CONVENIO_RECEB_ESTORNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RECEB_ESTORNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RECEB_ESTORNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RECEB         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ESTORNO           DATE                     NOT NULL,
  VL_ESTORNO           NUMBER(15,2)             NOT NULL,
  NR_LOTE_CONTABIL     NUMBER(10)               NOT NULL,
  NR_SEQ_TRANS_FIN     NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  VL_ADIC_ESTORNO      NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          400K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREES_CONRECE_FK_I ON TASY.CONVENIO_RECEB_ESTORNO
(NR_SEQ_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREES_LOTCONT_FK_I ON TASY.CONVENIO_RECEB_ESTORNO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREES_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONREES_PK ON TASY.CONVENIO_RECEB_ESTORNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREES_TRAFINA_FK_I ON TASY.CONVENIO_RECEB_ESTORNO
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREES_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Convenio_Receb_Estorno_delete
BEFORE DELETE ON TASY.CONVENIO_RECEB_ESTORNO FOR EACH ROW
begin

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_receb,null,'RC',:old.dt_estorno,'E',:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.Convenio_Receb_Estorno_update
AFTER UPDATE ON TASY.CONVENIO_RECEB_ESTORNO FOR EACH ROW
begin

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_seq_receb,null,'RC',:new.dt_estorno,'A',:new.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_ESTORNO_INSERT
AFTER INSERT or update ON TASY.CONVENIO_RECEB_ESTORNO FOR EACH ROW
DECLARE

cd_cgc_convenio_w		varchar2(30);
NR_SEQ_CONTA_BANCO_w	number(10,0);
NR_SEQ_MOVTO_W			number(10,0);
nr_seq_trans_fin_w		number(10,0);
qt_movto_w				number(5,0);
ie_banco_w				varchar2(1);
vl_transacao_estrang_w	number(15,2);
tx_cambial_w			convenio_Receb.tx_cambial%type;
cd_moeda_transacao_w	number(5);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
vl_moeda_original_w		convenio_receb.vl_moeda_original%type;
vl_transacao_estorno_w	number(15,2);
vl_receb_w				number(15,2);
vl_movimento_w			number(15,2);
nr_seq_movto_origem_w	movto_trans_financ.nr_sequencia%type;
ctb_param_lote_receb_conv_w	ctb_param_lote_receb_conv%rowtype;

cursor c01 is
select		a.nm_atributo,
		a.cd_tipo_lote_contab
from		atributo_contab a
where 		a.cd_tipo_lote_contab = 11
and 		a.nm_atributo in ('VL_RECEBIMENTO', 'VL_DEPOSITO', 'VL_ADICIONAL_ESTORNO');

c01_w		c01%rowtype;

begin

nr_seq_trans_fin_w 	:= :new.nr_seq_trans_fin;

if (nvl(nr_seq_trans_fin_w, 0) = 0) then
	begin

	select  nr_seq_trans_fin
	into    nr_seq_trans_fin_w
	from    Convenio_Receb a
	where   a.nr_sequencia		= :new.nr_seq_receb;

	exception when others then
		nr_seq_trans_fin_w		:= null;
	end;
end if;

if (inserting) then

    select	b.cd_cgc,
        a.NR_SEQ_CONTA_BANCO,
        nvl(:new.nr_seq_trans_fin,a.NR_SEQ_TRANS_FIN), /* Francisco - OS 76386 - 19/12/2007 - Tratei para pegar primeiro a transação do estorno */
        Nvl(a.tx_cambial,0),
        a.cd_estabelecimento,
        a.vl_moeda_original,
        Nvl(vl_recebimento,0)
    into	cd_cgc_convenio_w,
        NR_SEQ_CONTA_BANCO_w,
        NR_SEQ_TRANS_FIN_w,
        tx_cambial_w,
        cd_estabelecimento_w,
        vl_moeda_original_w,
        vl_receb_w
    from	convenio b,
        convenio_receb a
    where	a.cd_convenio	= b.cd_convenio
    and	a.nr_sequencia	= :new.nr_seq_receb;

    IF	(NR_SEQ_CONTA_BANCO_w IS NOT NULL) AND
        (NR_SEQ_TRANS_FIN_w IS NOT NULL) THEN

        select	max(ie_banco)
        into	ie_banco_w
        from	transacao_financeira
        where	nr_sequencia	= nr_seq_trans_fin_w;

        select	count(*),
		Nvl(max(vl_cotacao),0),
		nvl(max(cd_moeda),0),
		Nvl(sum(vl_transacao_estrang),0),
		max(nr_sequencia)
        into	qt_movto_w,
		vl_cotacao_w,
		cd_moeda_transacao_w,
		vl_transacao_estrang_w,
		nr_seq_movto_origem_w
        from	MOVTO_TRANS_FINANC
        where	nr_seq_conv_receb = :new.nr_seq_receb
	and	nvl(ie_estorno,'N') = 'N'
	and	nr_Seq_movto_orig is null;

        if	(qt_movto_w > 0) and
		(ie_banco_w <> 'N') then

            SELECT	MOVTO_TRANS_FINANC_SEQ.NEXTVAL
            INTO	NR_SEQ_MOVTO_W
            FROM	DUAL;

            if	(vl_cotacao_w > 0) and
                (cd_moeda_Transacao_w > 0) and
                (vl_moeda_original_w > 0) then

                begin
                if 	(Nvl(obter_moeda_padrao_empresa(cd_estabelecimento_w,'E'),cd_moeda_transacao_w) <> cd_moeda_transacao_w) and
                    (vl_cotacao_w = tx_cambial_w) and
                    (:NEW.VL_ESTORNO <> 0) then

                    vl_transacao_estorno_w := Round(:NEW.VL_ESTORNO / vl_cotacao_w,2);

                end if;
                exception
                when others then
                    vl_transacao_estorno_w := 0;
                end;

            end if;


            INSERT	INTO MOVTO_TRANS_FINANC
                (NR_SEQUENCIA,
                DT_TRANSACAO,
                NR_SEQ_TRANS_FINANC,
                VL_TRANSACAO,
                NR_SEQ_BANCO,
                NR_SEQ_CONV_RECEB,
                DT_REFERENCIA_SALDO,
                DT_ATUALIZACAO,
                NM_USUARIO,
                nr_lote_contabil,
                ie_conciliacao,
                ds_historico,
                cd_cgc,
                VL_TRANSACAO_ESTRANG,
                vl_cotacao,
		ie_estorno,
		nr_seq_movto_orig)
            VALUES	(NR_SEQ_MOVTO_W,
                :NEW.DT_ESTORNO,
                NR_SEQ_TRANS_FIN_w,
                :NEW.VL_ESTORNO * -1,
                NR_SEQ_CONTA_BANCO_W,
                :new.nr_seq_receb,
                TRUNC(:NEW.DT_ESTORNO,'MM'),
                SYSDATE,
                :NEW.NM_USUARIO,
                0,
                'N',
                'Estorno de recebimento de convênio',
                cd_cgc_convenio_w,
                vl_transacao_estorno_w * -1,
                vl_cotacao_w,
		'S',
		nr_seq_movto_origem_w);

        end if;

    end if;

	begin
	select  x.*
	into    ctb_param_lote_receb_conv_w
	from    (
			select  a.*
			from    ctb_param_lote_receb_conv a
			where   a.cd_empresa    = obter_empresa_estab(cd_estabelecimento_w)
			and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
			order   by nvl(a.cd_estab_exclusivo, 0) desc
			) x
	where   rownum = 1;
	exception
		when others then
			ctb_param_lote_receb_conv_w := null;
	end;

	open c01;
	loop
	fetch c01 into
	c01_w;
	exit when c01%notfound;
		begin
		vl_movimento_w := 0;

		if	((c01_w.nm_atributo = 'VL_RECEBIMENTO' or c01_w.nm_atributo = 'VL_DEPOSITO') and ctb_param_lote_receb_conv_w.ie_contab_estorno_receb = 'S') then
			vl_movimento_w := (:new.vl_estorno * -1);

		elsif	(c01_w.nm_atributo = 'VL_ADICIONAL_ESTORNO' and ctb_param_lote_receb_conv_w.ie_contab_vl_adic_estorno = 'S') then
			vl_movimento_w := :new.vl_adic_estorno;
		end if;

		if	(nvl(vl_movimento_w, 0) <> 0 and nvl(nr_seq_trans_fin_w, 0) <> 0) then
			begin

			ctb_concil_financeira_pck.ctb_gravar_documento	(
										cd_estabelecimento_w,
										trunc(:new.dt_estorno),
										c01_w.cd_tipo_lote_contab,
										nr_seq_trans_fin_w,
										8,
										:new.nr_seq_receb,
										:new.nr_sequencia,
										null,
										vl_movimento_w,
										'CONVENIO_RECEB_ESTORNO',
										c01_w.nm_atributo,
										:new.nm_usuario);

			end;
		end if;
		end;
	end loop;
	close c01;

elsif (updating) then

	update  ctb_documento
	set     vl_movimento 		= (:new.vl_estorno * -1),
		nr_seq_trans_financ	= nr_seq_trans_fin_w
	where   nm_atributo		in('VL_RECEBIMENTO','VL_DEPOSITO')
	and     nm_tabela  		= 'CONVENIO_RECEB_ESTORNO'
	and     nr_documento		= :new.nr_seq_receb
	and	nr_seq_doc_compl 	= :new.nr_sequencia;

	update  ctb_documento
	set     vl_movimento		= :new.vl_adic_estorno,
		nr_seq_trans_financ 	= nr_seq_trans_fin_w
	where   nm_atributo 	 	= 'VL_ADICIONAL_ESTORNO'
	and     nm_tabela   		= 'CONVENIO_RECEB_ESTORNO'
	and     nr_documento 		= :new.nr_seq_receb
	and	nr_seq_doc_compl	= :new.nr_sequencia;
end if;
/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_seq_receb,null,'RC',:new.dt_estorno,'I',:new.nm_usuario);

end Convenio_Receb_Estorno_Insert;
/


ALTER TABLE TASY.CONVENIO_RECEB_ESTORNO ADD (
  CONSTRAINT CONREES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RECEB_ESTORNO ADD (
  CONSTRAINT CONREES_CONRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEB) 
 REFERENCES TASY.CONVENIO_RECEB (NR_SEQUENCIA),
  CONSTRAINT CONREES_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CONREES_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_RECEB_ESTORNO TO NIVEL_1;

