ALTER TABLE TASY.PLS_AGRAVO_CPT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AGRAVO_CPT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AGRAVO_CPT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_AGRAVO        NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_TIPO_CPT      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSAGCP_ESTABEL_FK_I ON TASY.PLS_AGRAVO_CPT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGCP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSAGCP_PK ON TASY.PLS_AGRAVO_CPT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGCP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAGCP_PLSAGRA_FK_I ON TASY.PLS_AGRAVO_CPT
(NR_SEQ_AGRAVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGCP_PLSAGRA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAGCP_PLSTICA_FK_I ON TASY.PLS_AGRAVO_CPT
(NR_SEQ_TIPO_CPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAGCP_PLSTICA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_AGRAVO_CPT ADD (
  CONSTRAINT PLSAGCP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AGRAVO_CPT ADD (
  CONSTRAINT PLSAGCP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSAGCP_PLSTICA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CPT) 
 REFERENCES TASY.PLS_TIPO_CARENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSAGCP_PLSAGRA_FK 
 FOREIGN KEY (NR_SEQ_AGRAVO) 
 REFERENCES TASY.PLS_AGRAVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AGRAVO_CPT TO NIVEL_1;

