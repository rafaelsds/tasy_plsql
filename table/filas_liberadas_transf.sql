ALTER TABLE TASY.FILAS_LIBERADAS_TRANSF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FILAS_LIBERADAS_TRANSF CASCADE CONSTRAINTS;

CREATE TABLE TASY.FILAS_LIBERADAS_TRANSF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_FILA_ESPERA    NUMBER(10),
  NR_SEQ_LOCAL_CHAMADA  NUMBER(10),
  IE_PERMITE_TRANSF     VARCHAR2(1 BYTE),
  CD_PERFIL             NUMBER(5)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FILITRAN_FILESPS_FK_I ON TASY.FILAS_LIBERADAS_TRANSF
(NR_SEQ_FILA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILITRAN_FILESPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FILITRAN_MAQLOSE_FK_I ON TASY.FILAS_LIBERADAS_TRANSF
(NR_SEQ_LOCAL_CHAMADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILITRAN_MAQLOSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FILITRAN_PERFIL_FK_I ON TASY.FILAS_LIBERADAS_TRANSF
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FILITRAN_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FILITRAN_PK ON TASY.FILAS_LIBERADAS_TRANSF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FILAS_LIBERADAS_TRANSF ADD (
  CONSTRAINT FILITRAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FILAS_LIBERADAS_TRANSF ADD (
  CONSTRAINT FILITRAN_FILESPS_FK 
 FOREIGN KEY (NR_SEQ_FILA_ESPERA) 
 REFERENCES TASY.FILA_ESPERA_SENHA (NR_SEQUENCIA),
  CONSTRAINT FILITRAN_MAQLOSE_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_CHAMADA) 
 REFERENCES TASY.MAQUINA_LOCAL_SENHA (NR_SEQUENCIA),
  CONSTRAINT FILITRAN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.FILAS_LIBERADAS_TRANSF TO NIVEL_1;

