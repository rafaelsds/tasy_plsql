ALTER TABLE TASY.PROCEDIMENTO_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO_PARTICIPANTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_PARTIC             NUMBER(10)          NOT NULL,
  IE_FUNCAO                 VARCHAR2(10 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_CGC                    VARCHAR2(14 BYTE),
  IE_VALOR_INFORMADO        VARCHAR2(1 BYTE),
  IE_EMITE_CONTA            VARCHAR2(3 BYTE),
  VL_PARTICIPANTE           NUMBER(15,2),
  VL_CONTA                  NUMBER(15,2),
  NR_LOTE_CONTABIL          NUMBER(10),
  NR_CONTA_MEDICO           NUMBER(10),
  IE_TIPO_SERVICO_SUS       NUMBER(3),
  IE_TIPO_ATO_SUS           NUMBER(3),
  QT_PONTO_SUS              NUMBER(6,2),
  VL_PONTO_SUS              NUMBER(13,4),
  VL_ORIGINAL               NUMBER(15,2),
  IE_RESPONSAVEL_CREDITO    VARCHAR2(5 BYTE),
  PR_PROCEDIMENTO           NUMBER(7,4),
  CD_ESPECIALIDADE          NUMBER(5),
  PR_FATURAR                NUMBER(15,4),
  CD_MEDICO_CONVENIO        VARCHAR2(15 BYTE),
  NR_DOC_HONOR_CONV         VARCHAR2(20 BYTE),
  NR_SEQ_PROC_CRIT_REPASSE  NUMBER(10),
  IE_TISS_TIPO_GUIA         VARCHAR2(2 BYTE),
  NR_CIRURGIA               NUMBER(10),
  CD_CBO                    VARCHAR2(6 BYTE),
  IE_DOC_EXECUTOR           NUMBER(1),
  IE_PARTICIPOU_SUS         VARCHAR2(1 BYTE),
  CD_MEDICO_EXEC_TISS       VARCHAR2(10 BYTE),
  CD_CGC_PRESTADOR_TISS     VARCHAR2(14 BYTE),
  DS_PROC_TISS              VARCHAR2(255 BYTE),
  DS_PRESTADOR_TISS         VARCHAR2(255 BYTE),
  F                         VARCHAR2(10 BYTE),
  CD_PRESTADOR_TISS         VARCHAR2(40 BYTE),
  CD_PROC_CONV              VARCHAR2(20 BYTE),
  DS_PROC_CONV              VARCHAR2(255 BYTE),
  CD_CGC_PREST_SOLIC_TISS   VARCHAR2(14 BYTE),
  VL_REPASSE_CALC           NUMBER(15,2),
  CD_MEDICO_HONOR_TISS      VARCHAR2(10 BYTE),
  CD_REGRA_REPASSE          NUMBER(5),
  CD_CGC_PREST_HONOR_TISS   VARCHAR2(14 BYTE),
  CD_MEDICO_EXEC_CONTA      VARCHAR2(10 BYTE),
  NR_SEQ_CRIT_HONORARIO     NUMBER(10),
  DS_STACK                  VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPART_ESPMEDI_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPART_LOTCONT_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPART_PESFISI_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_PESFISI_FK2_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_MEDICO_EXEC_TISS)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_PESFISI_FK3_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_MEDICO_HONOR_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_PESFISI_FK4_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_MEDICO_EXEC_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_PESJURI_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPART_PESJURI_FK2_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_CGC_PRESTADOR_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_PESJURI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPART_PESJURI_FK3_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_CGC_PREST_SOLIC_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          688K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_PESJURI_FK3_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROPART_PK ON TASY.PROCEDIMENTO_PARTICIPANTE
(NR_SEQUENCIA, NR_SEQ_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_PROPACI_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPART_SUSCBOU_FK_I ON TASY.PROCEDIMENTO_PARTICIPANTE
(CD_CBO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPART_SUSCBOU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.procedimento_partic_update
before update ON TASY.PROCEDIMENTO_PARTICIPANTE for each row
declare

qt_reg_w		number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final;
end if;

if	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) then
	:new.cd_medico_convenio	:= null;
end if;

<<final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.PROCEDIMENTO_PARTIC_INSERT
BEFORE INSERT  ON TASY.PROCEDIMENTO_PARTICIPANTE FOR EACH ROW
DECLARE

ie_bloqueia_medico_estab_w		varchar2(10);
qt_registro_w				number(10);
cd_estabelecimento_w			number(5);
nr_atendimento_w			number(10);
ie_tipo_atendimento_w			number(3);
ie_bloqueia_medico_conv_w		varchar2(10);
ie_regra_w				varchar2(1);
cd_convenio_w                   	number(5);
ds_convenio_w				varchar2(255);
cd_empresa_w				number(4,0);
BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

select	max(nr_atendimento),
	max(cd_convenio)
into	nr_atendimento_w,
	cd_convenio_w
from	procedimento_paciente
where	nr_sequencia	= :new.nr_sequencia;

select	max(cd_estabelecimento),
	max(ie_tipo_atendimento)
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;

ie_bloqueia_medico_estab_w	:= nvl(obter_valor_param_usuario(67,285,Obter_Perfil_ativo,:new.nm_usuario,0),'N');
if	(ie_bloqueia_medico_estab_w	= 'S') and
	(:new.cd_pessoa_fisica	is not null) then

	select	nvl(max(cd_empresa),0)
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;

	select	count(*)
	into	qt_registro_w
	from	medico_estabelecimento
	where	cd_pessoa_fisica				= :new.cd_pessoa_fisica
	and	nvl(cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w
	and	nvl(cd_empresa, cd_empresa_w)			= cd_empresa_w;

	if	(qt_registro_w		= 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(241554);
	end if;
end if;

ie_bloqueia_medico_conv_w	:= nvl(obter_valor_param_usuario(1123,75,Obter_Perfil_ativo,:new.nm_usuario,0),'N');
if	(ie_bloqueia_medico_conv_w = 'S') and
	(:new.cd_pessoa_fisica is not null) and
	(cd_convenio_w is not null) then
	select	nvl(max(Obter_convenio_regra_atend(:new.cd_pessoa_fisica,cd_convenio_w,ie_tipo_atendimento_w,cd_estabelecimento_w,'P',null,null)), 'S')
	into	ie_regra_w
	from	dual;
	if	(ie_regra_w = 'N') then
		select	max(substr(obter_nome_convenio(cd_convenio_w),1,40))
		into	ds_convenio_w
		from	dual;
		wheb_mensagem_pck.exibir_mensagem_abort(241556, 'DS_CONVENIO='||ds_convenio_w);
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.procedimento_partic_delete
before delete ON TASY.PROCEDIMENTO_PARTICIPANTE for each row
declare
 ds_procedimento_w		varchar2(255);
 cd_procedimento_w		number(15);
 nr_seq_proc_interno_w		number(10);
 nr_seq_exame_w			number(10);
 nr_atendimento_w			number(10);
 nr_interno_conta_w		number(10);
 qt_lote_pre_fat_w			number(10) := 0;
 ie_excluir_proc_repasse_w		parametro_faturamento.ie_excluir_proc_repasse%type;
 cd_estabelecimento_w		atendimento_paciente.cd_estabelecimento%type;
 vl_repasse_pend_w		procedimento_repasse.vl_repasse%type;
 vl_repasse_w 			procedimento_repasse.vl_repasse%type;

pragma autonomous_transaction;

begin

select	a.cd_procedimento,
	b.ds_procedimento,
	nvl(a.nr_seq_proc_interno,0),
	nvl(a.nr_seq_exame,0),
	nr_atendimento,
	nr_interno_conta
into	cd_procedimento_w,
	ds_procedimento_w,
	nr_seq_proc_interno_w,
	nr_seq_exame_w,
	nr_atendimento_w,
	nr_interno_conta_w
from	procedimento_paciente a,
	procedimento b
where	a.cd_procedimento  = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	a.nr_sequencia	   = :old.nr_sequencia;

select	max(nvl(cd_estabelecimento,0))
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

select	max(nvl(ie_excluir_proc_repasse,'N'))
into	ie_excluir_proc_repasse_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_excluir_proc_repasse_w = 'S') then
	select	count(1)
	into	qt_lote_pre_fat_w
	from	pre_fatur_conta	a
	where	a.nr_interno_conta	= nr_interno_conta_w
	and	rownum			= 1;

	select	nvl(sum(a.vl_repasse),0)
	into	vl_repasse_pend_w
	from	procedimento_repasse	a
	where	a.nr_repasse_terceiro is not null
	and	a.nr_seq_procedimento	= :old.nr_sequencia
	and	nr_seq_partic		= :old.nr_seq_partic;
end if;

select	nvl(sum(a.vl_repasse),0)
into	vl_repasse_w
from	procedimento_repasse	a
where	a.nr_seq_procedimento	= :old.nr_sequencia
and	nr_seq_partic		= :old.nr_seq_partic;

if	((ie_excluir_proc_repasse_w = 'N') or
	(vl_repasse_pend_w > 0) or
	(qt_lote_pre_fat_w = 0)) and
	(vl_repasse_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(188917,	'nr_sequencia=' || :old.nr_sequencia ||
							';nr_interno_conta=' || nr_interno_conta_w ||
							';vl_repasse=' || vl_repasse_w ||
							';vl_repasse_pend=' || vl_repasse_pend_w ||
							';qt_lote_pre_fat=' || qt_lote_pre_fat_w);
	/* sequ�ncia: #@nr_sequencia#@
	conta: #@nr_interno_conta#@
	este procedimento possui repasse gerado!
	n�o � poss�vel excluir o procedimento.
	valor do repasse: #@vl_repasse#@ */
elsif	(ie_excluir_proc_repasse_w = 'S') then
	delete	from procedimento_repasse
	where	nr_seq_procedimento	= :old.nr_sequencia
	and	nr_seq_partic		= :old.nr_seq_partic;
end if;


insert into proc_partic_log (dt_atualizacao,
				dt_atualizacao_nrec,
				ie_acao,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_propaci,
				nr_sequencia,
				vl_participante,
				cd_procedimento,
				ds_procedimento,
				nr_seq_proc_interno,
				nr_seq_exame,
				ie_funcao,
				ie_emite_conta,
				nr_seq_partic,
				vl_conta,
				cd_especialidade,
				cd_medico_convenio,
				cd_cgc,
				nr_atendimento,
				nr_interno_conta,
				cd_pessoa_fisica)
values( sysdate,
        sysdate,
        'E',
        wheb_usuario_pck.get_nm_usuario,
        wheb_usuario_pck.get_nm_usuario,
        :old.nr_sequencia,
        proc_pac_log_seq.nextval,
        :old.vl_participante,
	    cd_procedimento_w,
		ds_procedimento_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w,
		:old.ie_funcao,
		:old.ie_emite_conta,
		:old.nr_seq_partic,
		:old.vl_conta,
		:old.cd_especialidade,
		:old.cd_medico_convenio,
		:old.cd_cgc,
		nr_atendimento_w,
		nr_interno_conta_w,
		:old.cd_pessoa_fisica);

commit;

end;
/


ALTER TABLE TASY.PROCEDIMENTO_PARTICIPANTE ADD (
  CONSTRAINT PROPART_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_PARTIC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO_PARTICIPANTE ADD (
  CONSTRAINT PROPART_PESJURI_FK3 
 FOREIGN KEY (CD_CGC_PREST_SOLIC_TISS) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PROPART_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_HONOR_TISS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPART_PESFISI_FK4 
 FOREIGN KEY (CD_MEDICO_EXEC_CONTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPART_PROPACI_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROPART_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PROPART_SUSCBOU_FK 
 FOREIGN KEY (CD_CBO) 
 REFERENCES TASY.SUS_CBO (CD_CBO),
  CONSTRAINT PROPART_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_EXEC_TISS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPART_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_PRESTADOR_TISS) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PROPART_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PROPART_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPART_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PROCEDIMENTO_PARTICIPANTE TO NIVEL_1;

