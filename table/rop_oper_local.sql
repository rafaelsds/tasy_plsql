ALTER TABLE TASY.ROP_OPER_LOCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_OPER_LOCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_OPER_LOCAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_OPERACAO      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ROPOPLO_PK ON TASY.ROP_OPER_LOCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPOPLO_PK
  MONITORING USAGE;


CREATE INDEX TASY.ROPOPLO_ROPLOCA_FK_I ON TASY.ROP_OPER_LOCAL
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPOPLO_ROPLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPOPLO_ROPOPER_FK_I ON TASY.ROP_OPER_LOCAL
(NR_SEQ_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ROP_OPER_LOCAL_tp  after update ON TASY.ROP_OPER_LOCAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_OPERACAO,1,4000),substr(:new.NR_SEQ_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPERACAO',ie_log_w,ds_w,'ROP_OPER_LOCAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_OPER_LOCAL ADD (
  CONSTRAINT ROPOPLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_OPER_LOCAL ADD (
  CONSTRAINT ROPOPLO_ROPOPER_FK 
 FOREIGN KEY (NR_SEQ_OPERACAO) 
 REFERENCES TASY.ROP_OPERACAO (NR_SEQUENCIA),
  CONSTRAINT ROPOPLO_ROPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_OPER_LOCAL TO NIVEL_1;

