ALTER TABLE TASY.PTU_QUESTIONAMENTO_REEMB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_QUESTIONAMENTO_REEMB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_QUESTIONAMENTO_REEMB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CONTESTACAO      NUMBER(10)            NOT NULL,
  NR_LOTE                 NUMBER(8)             NOT NULL,
  NR_NOTA                 VARCHAR2(20 BYTE),
  CD_UNIMED               NUMBER(4),
  ID_BENEF                VARCHAR2(13 BYTE),
  NM_BENEF                VARCHAR2(25 BYTE),
  DT_SERVICO              DATE,
  DT_REEMBOLSO            DATE,
  TP_PARTICIP             VARCHAR2(1 BYTE),
  TP_TABELA               NUMBER(1),
  CD_SERVICO              NUMBER(15),
  VL_DIF_VL_INTER         NUMBER(15,2),
  VL_SERV                 NUMBER(15,2),
  VL_RECONH_SERV          NUMBER(15,2),
  VL_ACORDO_SERV          NUMBER(15,2),
  DT_ACORDO               DATE,
  TP_ACORDO               VARCHAR2(10 BYTE),
  QT_COBRADA              NUMBER(12,4),
  QT_RECONH               NUMBER(12,4),
  QT_ACORDADA             NUMBER(12,4),
  NR_CNPJ_CPF             VARCHAR2(14 BYTE),
  NM_PRESTADOR            VARCHAR2(40 BYTE),
  NM_PROFISSIONAL         VARCHAR2(40 BYTE),
  SG_CONS_PROF            VARCHAR2(12 BYTE),
  NR_CONS_PROF            VARCHAR2(15 BYTE),
  SG_UF_CONS_PROF         VARCHAR2(2 BYTE),
  NR_AUTORIZ              NUMBER(10),
  NR_SEQ_A500             NUMBER(10),
  NR_SEQ_NOTA_COBR_REEMB  NUMBER(10),
  NR_SEQ_CONTA            NUMBER(10),
  NR_SEQ_CONTA_PROC       NUMBER(10),
  NR_SEQ_CONTA_MAT        NUMBER(10),
  NR_SEQ_ARQUIVO          NUMBER(10),
  NR_NOTA_NUMERICO        VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUQURE_PK ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQURE_PLSCOMAT_FK_I ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQURE_PLSCOME_FK_I ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQURE_PLSCOPRO_FK_I ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQURE_PTUCAMC_FK_I ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQ_CONTESTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUQURE_PTUNOCR_FK_I ON TASY.PTU_QUESTIONAMENTO_REEMB
(NR_SEQ_NOTA_COBR_REEMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_questionamento_reemb_atual
before insert or update ON TASY.PTU_QUESTIONAMENTO_REEMB for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

begin
:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/


ALTER TABLE TASY.PTU_QUESTIONAMENTO_REEMB ADD (
  CONSTRAINT PTUQURE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_QUESTIONAMENTO_REEMB ADD (
  CONSTRAINT PTUQURE_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PTUQURE_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PTUQURE_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PTUQURE_PTUCAMC_FK 
 FOREIGN KEY (NR_SEQ_CONTESTACAO) 
 REFERENCES TASY.PTU_CAMARA_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT PTUQURE_PTUNOCR_FK 
 FOREIGN KEY (NR_SEQ_NOTA_COBR_REEMB) 
 REFERENCES TASY.PTU_NOTA_COBRANCA_REEMB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_QUESTIONAMENTO_REEMB TO NIVEL_1;

