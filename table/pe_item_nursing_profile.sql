ALTER TABLE TASY.PE_ITEM_NURSING_PROFILE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_ITEM_NURSING_PROFILE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_ITEM_NURSING_PROFILE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_RESULT          NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_SCRIPT              VARCHAR2(4000 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_TITULO              VARCHAR2(255 BYTE)     NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEITNUPR_PEITERE_FK_I ON TASY.PE_ITEM_NURSING_PROFILE
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEITNUPR_PK ON TASY.PE_ITEM_NURSING_PROFILE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PE_ITEM_NURSING_PROFILE ADD (
  CONSTRAINT PEITNUPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PE_ITEM_NURSING_PROFILE ADD (
  CONSTRAINT PEITNUPR_PEITERE_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.PE_ITEM_RESULTADO (NR_SEQUENCIA)
    ON DELETE CASCADE);

