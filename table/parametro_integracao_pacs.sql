ALTER TABLE TASY.PARAMETRO_INTEGRACAO_PACS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_INTEGRACAO_PACS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_INTEGRACAO_PACS
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_LIMPAR_DATA_INTEGRACAO      VARCHAR2(1 BYTE),
  NM_USUARIO_PIXEON              VARCHAR2(80 BYTE),
  DS_SENHA_PIXEON                VARCHAR2(80 BYTE),
  IE_DUPLICAR_PROC_LADO          VARCHAR2(1 BYTE),
  IE_SOMENTE_AUTORIZADOS         VARCHAR2(1 BYTE),
  IE_GERAR_NOVO_LAUDO            VARCHAR2(1 BYTE),
  IE_VERIFICACAO_ENVIO_LAUDO     VARCHAR2(10 BYTE),
  IE_GRAVAR_LOG_INTEGRACAO       VARCHAR2(1 BYTE),
  IE_DESC_PROC_TITULO_LAUDO      VARCHAR2(1 BYTE),
  IE_REJEITAR_LAUDO              VARCHAR2(1 BYTE),
  IE_ORIGEM_INF_DT_LAUDO         VARCHAR2(10 BYTE),
  IE_APROV_LAUDO_WEBSERVICE      VARCHAR2(1 BYTE),
  IE_ALTERA_STATUS_EXEC          VARCHAR2(1 BYTE),
  QT_DIAS_INT_WEBS               NUMBER(10),
  IE_ENVIA_LADO                  VARCHAR2(1 BYTE),
  IE_PDF_PIXEON                  VARCHAR2(1 BYTE),
  IE_CANCELAR_LAUDO              VARCHAR2(1 BYTE),
  IE_PRIMEIRA_ASSINATURA         VARCHAR2(1 BYTE),
  IE_APROV_LAUDO_PAT_WEBSERVICE  VARCHAR2(1 BYTE) DEFAULT null,
  IE_ENVIA_TUSS_SYNAPSE          VARCHAR2(1 BYTE),
  IE_RECEBE_PATHOX_PROC          VARCHAR2(1 BYTE),
  IE_MODALDIADE_PROPRIA          VARCHAR2(1 BYTE),
  IE_INTEGRA_SEM_ASSINATURA      VARCHAR2(1 BYTE),
  IE_N_GERAR_KIT_MATERIAL        VARCHAR2(1 BYTE),
  IE_ENVIA_DT_EXAME              VARCHAR2(1 BYTE),
  IE_STATUS_LAUDO                VARCHAR2(15 BYTE),
  IE_MODIFICA_LAUDO_LIBERADO     VARCHAR2(1 BYTE),
  IE_ENVIA_PROC_INTERNO_GE       VARCHAR2(2 BYTE),
  IE_INSERE_MEDICO_ASSISTENTE    VARCHAR2(1 BYTE),
  IE_ENVIA_LAUDO_CARESTREAM      VARCHAR2(1 BYTE),
  IE_PATHOX_ANEXO                VARCHAR2(1 BYTE),
  IE_ENVIA_TOPOGRAFIA            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPACS_ESTABEL_FK_I ON TASY.PARAMETRO_INTEGRACAO_PACS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPACS_PK ON TASY.PARAMETRO_INTEGRACAO_PACS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTPACS_PK
  MONITORING USAGE;


ALTER TABLE TASY.PARAMETRO_INTEGRACAO_PACS ADD (
  CONSTRAINT INTPACS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETRO_INTEGRACAO_PACS ADD (
  CONSTRAINT INTPACS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PARAMETRO_INTEGRACAO_PACS TO NIVEL_1;

