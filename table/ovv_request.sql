ALTER TABLE TASY.OVV_REQUEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OVV_REQUEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.OVV_REQUEST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_VETERAN           VARCHAR2(9 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_OPV_TYPE          VARCHAR2(3 BYTE),
  DS_ADDRESS           VARCHAR2(40 BYTE),
  NR_POSTCODE          VARCHAR2(4 BYTE),
  NM_ALIAS_FAMILY      VARCHAR2(40 BYTE),
  NM_ALIAS_FIRST       VARCHAR2(40 BYTE),
  DT_BIRTH             DATE,
  NM_FAMILY            VARCHAR2(40 BYTE),
  NM_FIRST             VARCHAR2(40 BYTE),
  IE_GENDER            VARCHAR2(1 BYTE),
  NM_SECOND            VARCHAR2(40 BYTE),
  CD_PROVIDER          VARCHAR2(8 BYTE),
  CD_SEQ_TRANSACTION   VARCHAR2(25 BYTE)        NOT NULL,
  IE_STATUS_TASY       VARCHAR2(10 BYTE)        NOT NULL,
  NR_ATENDIMENTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.OVVR_PK ON TASY.OVV_REQUEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OVV_REQUEST ADD (
  CONSTRAINT OVVR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.OVV_REQUEST TO NIVEL_1;

