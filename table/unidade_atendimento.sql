ALTER TABLE TASY.UNIDADE_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_ATENDIMENTO
(
  CD_UNIDADE_BASICA            VARCHAR2(10 BYTE) NOT NULL,
  CD_UNIDADE_COMPL             VARCHAR2(10 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO         NUMBER(5)        NOT NULL,
  CD_TIPO_ACOMODACAO           NUMBER(4)        NOT NULL,
  IE_TEMPORARIO                VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  IE_STATUS_UNIDADE            VARCHAR2(3 BYTE),
  NR_ATENDIMENTO               NUMBER(10),
  DT_ENTRADA_UNIDADE           DATE,
  CD_PACIENTE_RESERVA          VARCHAR2(10 BYTE),
  NM_USUARIO_RESERVA           VARCHAR2(15 BYTE),
  IE_CLASSE_ACOMODACAO         VARCHAR2(1 BYTE),
  IE_SEXO_PACIENTE             VARCHAR2(1 BYTE),
  IE_SEXO_FIXO                 VARCHAR2(1 BYTE),
  IE_HIGIENIZACAO              VARCHAR2(1 BYTE),
  DT_HIGIENIZACAO              DATE,
  NM_USUARIO_HIGIENIZACAO      VARCHAR2(15 BYTE),
  DT_INICIO_HIGIENIZACAO       DATE,
  NR_SEQ_INTERNO               NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  CD_MOTIVO_INTERDICAO         NUMBER(10),
  NR_ATENDIMENTO_ACOMP         NUMBER(10),
  NR_RAMAL                     NUMBER(8),
  DT_CRIACAO                   DATE             NOT NULL,
  QT_MAX_VISITANTE             NUMBER(3),
  QT_MAX_ACOMP                 NUMBER(3),
  QT_IDADE_MINIMA              NUMBER(5,2),
  QT_IDADE_MAXIMA              NUMBER(5,2),
  DS_OCUPACAO                  VARCHAR2(5 BYTE),
  NR_SEQ_SUPERIOR              NUMBER(10),
  NR_AGRUPAMENTO               NUMBER(5),
  QT_PESO_MAXIMO               NUMBER(6,3),
  QT_ALTURA_MAXIMA             NUMBER(5),
  CD_CONVENIO_RESERVA          NUMBER(5),
  IE_STATUS_ANT_UNIDADE        VARCHAR2(3 BYTE),
  QT_TEMPO_PREV_HIGIEN         NUMBER(10),
  NR_SEQ_MOTIVO_RESERVA        NUMBER(10),
  NR_SEQ_COMPUTADOR            NUMBER(10),
  NM_USUARIO_FIM_HIGIENIZACAO  VARCHAR2(15 BYTE),
  IE_TIPO_LIMPEZA              VARCHAR2(15 BYTE),
  NR_SEQ_COMPUTADOR_LEITO      NUMBER(10),
  NR_SEQ_APRESENT              NUMBER(10),
  QT_PAC_UNIDADE               NUMBER(3),
  DS_UNIDADE_ATEND             VARCHAR2(80 BYTE),
  IE_LEITO_MONITORADO          VARCHAR2(1 BYTE),
  IE_PERMITE_ALT_EUP           VARCHAR2(1 BYTE),
  IE_INATIVAR_LEITO_TEMP       VARCHAR2(1 BYTE),
  IE_RADIOTERAPIA              VARCHAR2(1 BYTE),
  IE_INTERDITADO_RADIACAO      VARCHAR2(1 BYTE),
  IE_EXIBIR_CC                 VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL_PA              NUMBER(10),
  QT_IDADE_DIAS_MAX            NUMBER(3),
  QT_IDADE_DIAS_MIN            NUMBER(3),
  IE_TIPO_RESERVA              VARCHAR2(1 BYTE),
  IE_INATIVA_SETOR_AUTO        VARCHAR2(1 BYTE),
  IE_MOSTRA_GESTAO_DISP        VARCHAR2(1 BYTE),
  IE_NECESSITA_ISOL_RESERVA    VARCHAR2(1 BYTE),
  NR_SEQ_UNIDADE_RN            NUMBER(10),
  QT_MAX_DIARIO                NUMBER(3),
  QT_MAX_SIMULTANEO            NUMBER(3),
  CD_MOTIVO_MANUTENCAO         NUMBER(10),
  NM_SETOR_INTEGRACAO          VARCHAR2(40 BYTE),
  NR_ATENDIMENTO_ANT           NUMBER(10),
  DS_AGRUPAMENTO               VARCHAR2(20 BYTE),
  NR_SEQ_CLASSIF               NUMBER(10),
  QT_DIAS_PREV_INTERD          NUMBER(10),
  NR_SEQ_MOT_ISOL              NUMBER(10),
  DT_INTERDICAO                DATE,
  IE_OBSERVACAO_PA             VARCHAR2(1 BYTE),
  NR_INTERNACAO_AGHOS          NUMBER(10),
  DT_AGUARD_HIGIENIZACAO       DATE,
  IE_CONTROLE_FAIXA_ETARIA     VARCHAR2(1 BYTE),
  NR_EXTERNO                   NUMBER(15),
  NR_SEQ_UNID_BLOQ             NUMBER(10),
  IE_BLOQ_UNID_ANT             VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_EQUIP_CA        NUMBER(10),
  IE_LEITO_ADAPTADO            VARCHAR2(1 BYTE),
  NM_PAC_RESERVA               VARCHAR2(60 BYTE),
  IE_LEITO_RETAGUARDA          VARCHAR2(1 BYTE),
  IE_RETAGUARDA_ATUAL          VARCHAR2(1 BYTE),
  NM_LEITO_INTEGRACAO          VARCHAR2(40 BYTE),
  IE_IGNORAR_CHECKLIST         VARCHAR2(1 BYTE),
  IE_BLOQUEIO_TRANSF           VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_PAC_LEITO     NUMBER(10),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_LOCATION              NUMBER(10),
  NR_SEQ_MODELO                NUMBER(10),
  NR_SEQ_ROOM_NUMBER           NUMBER(5,5),
  IE_GERINT                    VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UNIATEN_ATEPACI_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_ATEPACI_FK_I2 ON TASY.UNIDADE_ATENDIMENTO
(NR_ATENDIMENTO_ACOMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_CLPALE_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_CLASSIF_PAC_LEITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_CLSUNAT_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_CLSUNAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_COMPUTA_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_COMPUTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_COMPUTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_COMPUTA_FK2_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_COMPUTADOR_LEITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_COMPUTA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_GREQPCA_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_GRUPO_EQUIP_CA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_GREQPCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_I1 ON TASY.UNIDADE_ATENDIMENTO
(IE_SITUACAO, IE_STATUS_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_I2 ON TASY.UNIDADE_ATENDIMENTO
(NR_ATENDIMENTO_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_I2
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_MOTINLE_FK_I ON TASY.UNIDADE_ATENDIMENTO
(CD_MOTIVO_INTERDICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_MOTREUN_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_MOTIVO_RESERVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_MOTREUN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_MTVISLT_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_MOT_ISOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_MTVISLT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_PALOCAL_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_LOCAL_PA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_PALOCAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIATEN_PEPOMOD_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_PESFISI_FK_I ON TASY.UNIDADE_ATENDIMENTO
(CD_PACIENTE_RESERVA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_PFCSLOCAL_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_LOCATION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNIATEN_PK ON TASY.UNIDADE_ATENDIMENTO
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_QUARATD_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_ROOM_NUMBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_SETATEN_FK_I ON TASY.UNIDADE_ATENDIMENTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_TIPACOM_FK_I ON TASY.UNIDADE_ATENDIMENTO
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNIATEN_UK ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_UNIATEN_FK_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEN_UNIATEN_FK2_I ON TASY.UNIDADE_ATENDIMENTO
(NR_SEQ_UNIDADE_RN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEN_UNIATEN_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.UNIDADE_ATENDIMENTO_tp  after update ON TASY.UNIDADE_ATENDIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_SETOR_ATENDIMENTO='||to_char(:old.CD_SETOR_ATENDIMENTO)||'#@#@CD_UNIDADE_BASICA='||to_char(:old.CD_UNIDADE_BASICA)||'#@#@CD_UNIDADE_COMPL='||to_char(:old.CD_UNIDADE_COMPL);gravar_log_alteracao(substr(:old.CD_TIPO_ACOMODACAO,1,4000),substr(:new.CD_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ACOMODACAO',ie_log_w,ds_w,'UNIDADE_ATENDIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.gatilho_Philips_log_status
BEFORE update ON TASY.UNIDADE_ATENDIMENTO FOR EACH ROW
DECLARE
BEGIN

if (nvl(:new.IE_STATUS_UNIDADE,'N') <> nvl(:old.IE_STATUS_UNIDADE,'N') ) then

 insert into tabela_philips_log_status (ds_stack) values  ('if   '||
' :new.IE_STATUS_UNIDADE :'||:new.IE_STATUS_UNIDADE||
' :old.IE_STATUS_UNIDADE '||:old.IE_STATUS_UNIDADE||
' :new.NR_SEQ_INTERNO '||:new.NR_SEQ_INTERNO||
'  dt_atualizacao   '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')||
'  Usuário: '||OBTER_USUARIO_ATIVO||
'  Perfil:  '||OBTER_PERFIL_ATIVO||
'  Função:  '||OBTER_FUNCAO_ATIVA||
'  Setor    '||OBTER_SETOR_ATIVO||
' Stack:  '||substr(dbms_utility.format_call_stack,1,4000));

else if(:new.IE_STATUS_UNIDADE <> :old.IE_STATUS_UNIDADE) then

 insert into tabela_philips_log_status (ds_stack2) values  ('else   '||
' :new.IE_STATUS_UNIDADE :'||:new.IE_STATUS_UNIDADE||
' :old.IE_STATUS_UNIDADE '||:old.IE_STATUS_UNIDADE||
' :new.NR_SEQ_INTERNO '||:new.NR_SEQ_INTERNO||
'  dt_atualizacao   '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')||
'  Usuário: '||OBTER_USUARIO_ATIVO||
'  Perfil:  '||OBTER_PERFIL_ATIVO||
'  Função:  '||OBTER_FUNCAO_ATIVA||
'  Setor    '||OBTER_SETOR_ATIVO||
' Stack:  '||substr(dbms_utility.format_call_stack,1,4000));

end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.UNIDADE_ATENDIMENTO_AFTUPDATE
AFTER UPDATE ON TASY.UNIDADE_ATENDIMENTO FOR EACH ROW
DECLARE

classif_setor_cross_w 	number(10);
ie_existe_w				varchar2(1);
ie_existe_regra_w		varchar2(1);
ie_integra_w			varchar2(1);
bl_enviou_checkout		boolean := false;
nr_resultado_w			integer;
BEGIN

classif_setor_cross_w := obter_classif_setor_cross(:new.cd_setor_atendimento);

if  (:old.IE_STATUS_UNIDADE <> :new.IE_STATUS_UNIDADE) and
	(:new.NR_EXTERNO is not null) and
	(classif_setor_cross_w > 0) then

	if(:new.IE_STATUS_UNIDADE = 'I') then
		-- Bloqueio
		gravar_integracao_cross(279, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';NR_SEQ_INTERNO=' || :NEW.NR_SEQ_INTERNO ||  ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
	else
		IF (:old.IE_STATUS_UNIDADE = 'I') then
			-- Desbloqueio
			gravar_integracao_cross(285, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';NR_SEQ_INTERNO=' || :NEW.NR_SEQ_INTERNO || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
		end if;
	end if;
end if;

if  (:new.ie_status_unidade = 'P') and (:new.nr_atendimento is not null) then

	select	count(*)
	into	nr_resultado_w
	from 	integra_voice_hcs
	where	ie_evento = 'CI'
	and 	nr_atendimento = :new.nr_atendimento;

	if	(nr_resultado_w = 0) then
		bl_enviou_checkout := true;

		insert into integra_voice_hcs
			(NR_SEQUENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			NR_ATENDIMENTO,
			CD_SETOR_ATEND_ANT,
			CD_UNID_BASIC_ANT,
			CD_UNID_COMPL_ANT,
			CD_SETOR_ATENDIMENTO,
			CD_UNIDADE_BASICA,
			CD_UNIDADE_COMPL,
			IE_STATUS_UNIDADE,
			IE_EVENTO,
			DS_MENSAGEM)
		values
			(
			integra_voice_hcs_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_atendimento,
			null,
			null,
			null,
			:new.cd_setor_atendimento,
			:new.cd_unidade_basica,
			:new.cd_unidade_compl,
			null,
			'CI',
			null);
	end if;
end if;

ie_existe_regra_w := obter_regra_voice_hcs(:new.ie_status_unidade);

if(:old.ie_status_unidade <> :new.ie_status_unidade) and (not bl_enviou_checkout) then

			begin
				select	nvl('S','N')
				into ie_existe_w
				from integra_voice_hcs
				where nr_atendimento = :new.nr_atendimento
				and cd_setor_atendimento = :new.cd_setor_atendimento
				and cd_unidade_basica = :new.cd_unidade_basica
				and cd_unidade_compl = :new.cd_unidade_compl
				and DT_ATUALIZACAO = sysdate;
			exception
			when others then
			ie_existe_w := 'N';
			end;
				if (ie_existe_w = 'N') then

				if(:old.ie_status_unidade = 'E') and (:new.ie_status_unidade <> 'E') and (obter_regra_voice_hcs('LM') = 'S') then

					insert into integra_voice_hcs
						(NR_SEQUENCIA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						NR_ATENDIMENTO,
						CD_SETOR_ATEND_ANT,
						CD_UNID_BASIC_ANT,
						CD_UNID_COMPL_ANT,
						CD_SETOR_ATENDIMENTO,
						CD_UNIDADE_BASICA,
						CD_UNIDADE_COMPL,
						IE_STATUS_UNIDADE,
						IE_EVENTO,
						DS_MENSAGEM)
					values
						(
						integra_voice_hcs_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						:new.nr_atendimento,
						null,
						null,
						null,
						:new.cd_setor_atendimento,
						:new.cd_unidade_basica,
						:new.cd_unidade_compl,
						'LM',
						'RS',
						null);
					end if;

				if  (ie_existe_regra_w = 'S') or (ie_existe_regra_w = 'V' and (:new.ie_status_unidade in ('E','I', 'C'))) then
					insert into integra_voice_hcs
							(NR_SEQUENCIA,
							DT_ATUALIZACAO,
							NM_USUARIO,
							DT_ATUALIZACAO_NREC,
							NM_USUARIO_NREC,
							NR_ATENDIMENTO,
							CD_SETOR_ATEND_ANT,
							CD_UNID_BASIC_ANT,
							CD_UNID_COMPL_ANT,
							CD_SETOR_ATENDIMENTO,
							CD_UNIDADE_BASICA,
							CD_UNIDADE_COMPL,
							IE_STATUS_UNIDADE,
							IE_EVENTO,
							DS_MENSAGEM)
						values
							(
							integra_voice_hcs_seq.nextval,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							:new.nr_atendimento,
							null,
							null,
							null,
							:new.cd_setor_atendimento,
							:new.cd_unidade_basica,
							:new.cd_unidade_compl,
							:new.ie_status_unidade,
							'RS',
							null);
				end if;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.GERINT_Unidade_Atendimento
after update ON TASY.UNIDADE_ATENDIMENTO for each row
declare

cd_estabelecimento_w	number(5);
ds_sep_bv_w				varchar2(50);

begin

/*			I N T E G R A � � O  -  G E R I N T
	Evento 1: Servi�o de Bloqueio de Leito
	Evento 2: Servi�o de Desbloqueio de Leito*/

select	max(cd_estabelecimento_base)
into	cd_estabelecimento_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

ds_sep_bv_w	:=	obter_separador_bv;

if	(obter_dados_param_atend(cd_estabelecimento_w,'GI') = 'S') and
	(:new.nm_leito_integracao is not null) then

	if	(:old.ie_status_unidade not in ('M', 'O', 'E', 'I', 'H', 'R', 'G')) and
		(:new.ie_status_unidade in ('M', 'O', 'E', 'I', 'H', 'R', 'G')) then

		exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  		|| ds_sep_bv_w ||
																	'cd_estabelecimento_p=' 		|| cd_estabelecimento_w 	|| ds_sep_bv_w ||
																	'id_evento_p=' 					|| 1 						|| ds_sep_bv_w ||
																	'nr_atendimento_p=' 			|| :new.nr_atendimento 		|| ds_sep_bv_w ||
																	'ie_leito_extra_p=' 			|| null 					|| ds_sep_bv_w ||
																	'dt_alta_p=' 					|| null 					|| ds_sep_bv_w ||
																	'ds_motivo_alta_p=' 			|| null 					|| ds_sep_bv_w ||
																	'ds_justif_transferencia_p='	|| null 					|| ds_sep_bv_w ||
																	'nr_seq_solic_internacao_p=' 	|| null 					|| ds_sep_bv_w ||
																	'nr_seq_leito_p=' 				|| :new.nr_seq_interno		|| ds_sep_bv_w ||
																	'ds_ident_leito_p='				|| :new.nm_leito_integracao	|| ds_sep_bv_w ||
																	'nr_seq_classif_p=' 			|| :new.nr_seq_classif		|| ds_sep_bv_w ||
																	'ie_status_unidade_p=' 			|| :new.ie_status_unidade	|| ds_sep_bv_w ||
																	'nr_cpf_paciente_p=' 			|| null						|| ds_sep_bv_w ||
																	'nr_cartao_sus_p=' 				|| null						|| ds_sep_bv_w ||
																	'cd_cid_p='						|| null						|| ds_sep_bv_w ||
																	'cd_evolucao_p='				|| null);

	end if;

	if	(:old.ie_status_unidade in ('M', 'O', 'E', 'I', 'H', 'R', 'G')) and
		(:new.ie_status_unidade not in ('M', 'O', 'E', 'I', 'H', 'R', 'G')) then

		exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  		|| ds_sep_bv_w ||
																	'cd_estabelecimento_p=' 		|| cd_estabelecimento_w 	|| ds_sep_bv_w ||
																	'id_evento_p=' 					|| 2 						|| ds_sep_bv_w ||
																	'nr_atendimento_p=' 			|| :new.nr_atendimento 		|| ds_sep_bv_w ||
																	'ie_leito_extra_p=' 			|| null 					|| ds_sep_bv_w ||
																	'dt_alta_p=' 					|| null 					|| ds_sep_bv_w ||
																	'ds_motivo_alta_p=' 			|| null 					|| ds_sep_bv_w ||
																	'ds_justif_transferencia_p='	|| null 					|| ds_sep_bv_w ||
																	'nr_seq_solic_internacao_p=' 	|| null 					|| ds_sep_bv_w ||
																	'nr_seq_leito_p=' 				|| :new.nr_seq_interno		|| ds_sep_bv_w ||
																	'ds_ident_leito_p='				|| :new.nm_leito_integracao	|| ds_sep_bv_w ||
																	'nr_seq_classif_p=' 			|| :new.nr_seq_classif		|| ds_sep_bv_w ||
																	'ie_status_unidade_p=' 			|| :new.ie_status_unidade	|| ds_sep_bv_w ||
																	'nr_cpf_paciente_p=' 			|| null						|| ds_sep_bv_w ||
																	'nr_cartao_sus_p=' 				|| null						|| ds_sep_bv_w ||
																	'cd_cid_p='						|| null						|| ds_sep_bv_w ||
																	'cd_evolucao_p='				|| null);
	end if;

end if;
/*		I N T E G R A � � O  -  G E R I N T  -  F I M		*/

end;
/


CREATE OR REPLACE TRIGGER TASY.Unidade_Atendimento_Update
BEFORE UPDATE ON TASY.UNIDADE_ATENDIMENTO FOR EACH ROW
DECLARE

nr_sequencia_w			Number(10,0);
ie_mudou_status_w			Varchar2(01);
ie_paciente_isolado_w		varchar2(01);
nr_acompanhante_w		Number(15,0);
cd_pessoa_fisica_w		Varchar2(10);
dt_nascimento_w			Date;
dt_obito_w			Date;
qt_idade_w			Number(10);
qt_registro_w			number(10);
nr_atendimento_w			Number(10);
nr_seq_hist_w			Number(10);
nm_usuario_w			Varchar2(15);
ie_grupo_w			varchar2(1);

nr_seq_unid_status_p312_w		Number(10,0);
ie_status_unidade_p312_w		Varchar2(15) := null;
ie_status_unid_p312_w		Varchar2(15);
ie_utiliza_portaria_w			Varchar2(1);
qt_classif_w			Number(10,0);
qt_existe_classif_w			Number(10,0);
ie_classif_p312_w			Varchar2(05);
qt_itens_w			number(5);
qt_reg_w				number(1);
ds_observacao_w			varchar2(255);
dt_alta_w				date;
ds_origem_w			varchar2(1800);
Ie_reg_Atepacu_w			varchar2(1);
ds_unidade_log_w			varchar2(4000);
ie_gera_eritel_w			varchar2(3);
cd_estabelecimento_w		number(5);
cd_pessoa_reserva_w		varchar2(10);
nm_pac_reserva_w		varchar2(255);
nr_seq_vaga_w			number(10);
cd_paciente_reserva_w		varchar2(10);
ie_per_reserva_gv_reservado_w	varchar2(1);
nr_atendimento_ww		number(10);
nr_atend_w			number(10);
ie_inativa_leito_w		varchar2(1);
ie_consiste_leito_hig_alta_w	varchar2(1);
nr_internacao_w			number(10);
ie_serv_aguardando_checklist_w	varchar2(1);
ie_serv_aguard_hig_w	varchar2(1);
ie_isola_leito_w	varchar2(1);
ie_pac_isolado_w	varchar2(1);

nr_seq_higienizacao_w	number(10);
nr_seq_gv_w		number(10);
ie_atualizar_gv_w	varchar2(1) := 'N';

ie_atualizar_sts_gv_w varchar2(1) := 'N';
nm_paciente_w		varchar2(200);
cd_pessoa_atend_ant_w	varchar2(15);
cd_pessoa_fisica_atual_w	varchar2(15);
nr_seq_hist_update_w	number(10);
ie_consistir_idade_w		varchar2(1);
ie_alojamento_w 		varchar2(1);
nr_atendimento_acomp_temp_w	unidade_atendimento.nr_atendimento%type;
cd_paciente_reserva_temp_w	unidade_atendimento.cd_paciente_reserva%type;
nm_paciente_reserva_temp_w	unidade_atendimento.nm_pac_reserva%type;
dt_first_history_w	date;
nm_first_history_w		unidade_atend_hist.nm_usuario%type;
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if	(obter_funcao_ativa = 44) and -- se a funcao ativa e a ocupacao hospitalar
	(:new.ie_status_unidade = 'G') and -- se o novo status e aguardando higienizacao
	(:old.ie_status_unidade	= 'O') and -- se o status anterior e isolamento (sem o paciente no leito)
	(:new.ie_higienizacao = 'N') then -- se o higienizacao esta como "Nao controla"
	:new.ie_status_unidade := 'L'; -- poe o status para Livre.
end if;

if	(:new.ie_status_unidade <> 'R') and
	(:old.ie_status_unidade	= 'R') and
	(:old.cd_paciente_reserva is not null) and
	(:old.nr_internacao_aghos is not null) then

	if	(:new.ie_status_unidade = 'P') then

		begin
			select	nr_internacao
			into	nr_internacao_w
			from	solicitacao_tasy_aghos
			where	nr_internacao = :new.nr_internacao_aghos
			and	nr_atendimento = :new.nr_atendimento
			and	rownum = 1;
		exception
			when	no_data_found then
				nr_internacao_w := 1;
			when	others then
				nr_internacao_w := null;
		end;

		if	(nr_internacao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(228964, 'PACIENTE='	|| substr(obter_nome_pf(:old.cd_paciente_reserva), 1, 80) || ';' ||
									'INTERNACAO='	|| :new.nr_internacao_aghos);
		else
			:new.cd_paciente_reserva := null;
			:new.nr_internacao_aghos := null;
		end if;

	else
		wheb_mensagem_pck.exibir_mensagem_abort(228964, 'PACIENTE='	|| substr(obter_nome_pf(:old.cd_paciente_reserva), 1, 80) || ';' ||
								'INTERNACAO='	|| :new.nr_internacao_aghos);
	end if;
end if;

if	(((:new.ie_status_unidade	= 'L') and
	(:old.ie_status_unidade	= 'R')) or
	((:new.ie_situacao	= 'I') and
	(:old.ie_situacao	= 'A'))) and
	(:new.cd_paciente_reserva is not null) then
	:new.cd_paciente_reserva	:= null;
	:new.nm_usuario_reserva		:= null;
	:new.nm_pac_reserva		:= null;
end if;

if	(:new.nr_atendimento is not null) and
	(:old.nr_atendimento is not null) and
	(:new.nr_atendimento	<> :old.nr_atendimento) then

	select  max(cd_pessoa_fisica)
	into	cd_pessoa_atend_ant_w
	from	atendimento_paciente
	where	nr_atendimento = :old.nr_atendimento;

	select  max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_atual_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if	(cd_pessoa_fisica_atual_w <> cd_pessoa_atend_ant_w) then -- Confirmar se nao se trata de um agrupamento de atendimentos do mesmo paciente.
			--'Esta unidade ja possui um atendimento vinculado. Nao pode estar vinculada a um novo atendimento.
		wheb_mensagem_pck.exibir_mensagem_abort(182885);
	end if;
end if;

if	( :new.ie_status_unidade <> :old.ie_status_unidade) and
	( :new.nr_atendimento is not null) and
	( :old.ie_status_unidade = 'P') and
	( :new.ie_status_unidade = 'L') then
	--'Nao e possivel  colocar para Livre um leito que possui atendimento!'
		insert into log_mov(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
		values	(	sysdate,
				:new.nm_usuario,
				55833,
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(326147), 1, 4000)||' '||:new.nr_atendimento||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||'/'||SUBSTR(OBTER_DESC_EXPRESSAO(705911), 1, 4000)||': '||:new.cd_setor_atendimento||'/'||:new.cd_unidade_basica||'/'||:new.cd_unidade_compl||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298822), 1, 4000)||': '|| ds_origem_w);

end if;

if	( :new.ie_status_unidade <> :old.ie_status_unidade) and
	( :new.nr_atendimento is not null) and
	( :old.ie_status_unidade = 'P') and
	( :new.ie_status_unidade = 'O') then
	--'Nao e possivel realizar o isolamento de um leito que possui paciente!'
	wheb_mensagem_pck.exibir_mensagem_abort(261037);
end if;

if	( :new.ie_status_unidade <> :old.ie_status_unidade) and
	( :new.nr_atendimento is not null) and
	( :old.ie_status_unidade = 'P') and
	( :new.ie_status_unidade = 'G') then
	--'Nao e possivel colocar um leito em Aguardando Higienizacao, quando existe paciente no mesmo!'
	wheb_mensagem_pck.exibir_mensagem_abort(272433);
end if;

Obter_param_Usuario(3111, 243, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_consiste_leito_hig_alta_w);
Obter_param_Usuario(44, 273, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_serv_aguardando_checklist_w);
Obter_param_Usuario(44, 274, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_serv_aguard_hig_w);

if	(ie_consiste_leito_hig_alta_w = 'S') and
	((:old.ie_status_unidade = 'A') or /*Verifica se  o status antigo e 'Em processo de alta'*/
	 (:old.ie_status_unidade = 'H')) and /*Verifica se  o status antigo e 'Em higienizacao'*/
	(:new.ie_status_unidade = 'P') then
	select	max(nr_atend_alta)
	into	nr_atend_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	select   max(nr_atendimento)
	into	 nr_atendimento_ww
	from	 unidade_atend_hist
	where	 :new.nr_seq_interno 		= nr_seq_unidade
	and	 nr_sequencia			= (select   max(x.nr_sequencia)
						  from	    unidade_atend_hist x
						  where	    :new.nr_seq_interno		= x.nr_seq_unidade
						  and	    x.nr_atendimento is not null);

	if	((:old.nr_atendimento	<> nr_atend_w) or nvl(nr_atend_w,0) = 0) and
		(:new.nr_atendimento 	<> nr_atendimento_ww) then -- Nao vai aparecer a mensagem caso se trate de um estorno!
		if	(:old.ie_status_unidade = 'A') then
			wheb_mensagem_pck.exibir_mensagem_abort(215401); --Esta Unidade esta em processo de alta, favor entrar em contato com o setor de TI'
		end if;

		if	(:old.ie_status_unidade = 'H') then
			wheb_mensagem_pck.exibir_mensagem_abort(220216); --Esta Unidade esta em higienizacao, favor entrar em contato com o setor de TI'
		end if;
	end if;
end if;

select	max(cd_estabelecimento_base)
into	cd_estabelecimento_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

select 	nvl(max(ie_per_reserva_gv_reservado),'N'),
	nvl(max(IE_CONSISTIR_IDADE),'N')
into	ie_per_reserva_gv_reservado_w,
	ie_consistir_idade_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_w;

/*Edilson em 29/03/05 OS 16677*/
if	(:new.ie_status_unidade = 'P') and
	(:new.nr_atendimento is null) and
	(:old.nr_atendimento is not null) then
	:new.ie_status_unidade	:= 'L';
end if;

if	(:new.ie_inativar_leito_temp = 'S') and
	(:new.ie_temporario = 'S') and
	(:new.ie_status_unidade <> :old.ie_status_unidade) and
	(:new.ie_status_unidade = 'L') and
	(:new.nr_seq_unid_bloq is null) then
	:new.ie_situacao := 'I';
end if;

if	(:new.ie_status_unidade <> 'H') then
	:new.qt_tempo_prev_higien	:= null;
end if;

if 	((:old.ie_status_unidade = 'P') and (:new.ie_status_unidade = 'P')) and /* se o status ja era 'P', e continua igual */
	((:old.ie_situacao <> 'I') and (:new.ie_situacao = 'I')) then           /*  e a situacao era ativa, e esta passando para inativa */

	select 	substr(obter_pessoa_atendimento(:old.nr_atendimento, 'N'),1,200)
	into	nm_paciente_w
	from	dual;

	--'Nao e possivel inativar o leito X*, pois o paciente "Y", atendimento "Z" esta  alocado no mesmo.'
	wheb_mensagem_pck.exibir_mensagem_abort(268810,	'CD_UNIDADE='||:old.cd_unidade_basica||' '||:old.cd_unidade_compl|| ';' ||
							'NM_PACIENTE='||nm_paciente_w|| ';' ||
							'NR_ATENDIMENTO='||:old.nr_atendimento);
end if;

if 	((:old.ie_situacao = 'I') and (:new.ie_situacao = 'I')) and                  /* se a situacao ja era inativa, e continua igual */
	((:old.ie_status_unidade <> 'P') and (:new.ie_status_unidade = 'P')) then    /* se o status era <> 'P', e esta passando para 'P' */
	--'O leito X* esta inativo, nao pode receber um paciente.'
	wheb_mensagem_pck.exibir_mensagem_abort(268811,'CD_UNIDADE='||:old.cd_unidade_basica||' '||:old.cd_unidade_compl);
end if;

if	(:new.nr_atendimento is not null) and
	(:new.nr_atendimento <> :old.nr_atendimento) and
	(:new.ie_status_unidade = 'P') and
	(:old.ie_status_unidade = 'H') then
	--'Esse leito nao pode receber um paciente pois esta em Higienizacao.
	wheb_mensagem_pck.exibir_mensagem_abort(210478);

end if;

if	(:new.ie_status_unidade <> :old.ie_status_unidade) then
	:new.ie_status_ant_unidade := :old.ie_status_unidade;

	select	nvl(max(nr_seq_higienizacao),1)
	into	nr_seq_higienizacao_w
	from	unidade_hig_hist
	where	nr_seq_unidade = :new.nr_seq_interno
	and		ie_status_unidade = 'L';

	if	(:new.ie_status_unidade = 'L') then
		nr_seq_higienizacao_w 	:= nr_seq_higienizacao_w + 1;
	end if;

	if	((nvl(nr_seq_higienizacao_w,0) > 0) and
		(nvl(:new.nr_seq_interno,0) > 0)) then
		insert	into unidade_hig_hist(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_unidade,
						dt_historico,
						ie_status_unidade,
						nr_seq_higienizacao,
						nr_atendimento)
					values
						(unidade_hig_hist_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						:new.nr_seq_interno,
						sysdate,
						:new.ie_status_unidade,
						nr_seq_higienizacao_w,
						nvl(:new.nr_atendimento,null)
						);
	end if;
end if;

if	(:new.ie_status_unidade <> :old.ie_status_unidade) and
	(:new.ie_status_unidade = 'P') then
	:new.IE_TIPO_RESERVA := null;
end if;

cd_paciente_reserva_w	:= :new.cd_paciente_reserva;

if	((:new.ie_status_unidade = 'L') and
	 (:new.cd_paciente_reserva is null) and
	 (:new.nm_pac_reserva is null)) or
	((:new.ie_status_unidade = 'L') and
	 (ie_per_reserva_gv_reservado_w = 'S')) then
	begin
	:new.nr_atendimento		:= null;
	:new.dt_entrada_unidade		:= null;
	:new.cd_paciente_reserva	:= null;
	:new.nm_pac_reserva		:= null;
	:new.nm_usuario_reserva		:= null;
	:new.cd_motivo_interdicao	:= null;
	:new.nr_atendimento_acomp	:= null;

	cd_pessoa_reserva_w	:= OBTER_NOME_PF_RESERVA_GV(:new.cd_unidade_basica,:new.cd_unidade_compl,:new.cd_setor_atendimento);
	nm_pac_reserva_w	:= null;

	if	(cd_pessoa_reserva_w is null) then
		select 	max(nm_pac_reserva)
		into	nm_pac_reserva_w
		from	fila_espera_reserva
		where	nr_sequencia = (select  nvl(min(nr_sequencia),0)
					from	fila_espera_reserva
					where	ie_status  = 'R'
					and	cd_unidade_basica = :new.cd_unidade_basica
					and	cd_unidade_compl = :new.cd_unidade_compl
					and	cd_setor_atendimento = :new.cd_setor_atendimento);
	end if;

	update	fila_espera_reserva
	set	ie_status = 'U'
	where	(cd_pessoa_reserva = cd_pessoa_reserva_w or nm_pac_reserva = nm_pac_reserva_w)
	and 	ie_status = 'R';


	if	((cd_pessoa_reserva_w <> 'X') or (cd_pessoa_reserva_w is null and nm_pac_reserva_w is not null))and
		(ie_per_reserva_gv_reservado_w = 'S') then
		begin

	--	select 	max(cd_pessoa_fisica)
	--	into	cd_pessoa_reserva_w
	--	from	gestao_vaga
	--	where	nr_sequencia = nr_seq_vaga_w;

		:new.ie_status_unidade		:= 'R';
		:new.cd_paciente_reserva	:= cd_pessoa_reserva_w;
		:new.nm_usuario_reserva		:= :new.nm_usuario;
		:new.nm_pac_reserva		:= nm_pac_reserva_w;
		end;
	end if;
	end;
end if;

if	(:old.ie_status_unidade = 'I') and
	(:new.ie_status_unidade <> 'I') and
	(:old.ie_interditado_radiacao = 'S') then
	:new.ie_interditado_radiacao := 'N';
end if;

if	(:new.nr_atendimento is not null) and
	(:old.nr_atendimento is null) then
	if	(:new.qt_idade_minima is not null) or
		(:new.qt_idade_maxima is not null) then
		begin
		if (obter_funcao_ativa not in (916,3111,3113)) or (ie_consistir_idade_w <> 'A') then
			select	a.cd_pessoa_fisica,
				b.dt_nascimento,
				nvl(b.dt_obito,sysdate),
				a.cd_estabelecimento
			into	cd_pessoa_fisica_w,
				dt_nascimento_w,
				dt_obito_w,
				cd_estabelecimento_w
			from	pessoa_fisica b,
				atendimento_paciente a
			where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
			and	a.nr_atendimento	= :new.nr_atendimento;

			select	obter_idade(dt_nascimento_w,dt_obito_w,'A')
			into	qt_idade_w
			from	dual;

			if	(:new.qt_idade_minima is not null) and
				(:new.qt_idade_minima > qt_idade_w) then
				--'A idade minima permitida para o leito e '||to_char(:new.qt_idade_minima)||' ano(s).
				wheb_mensagem_pck.exibir_mensagem_abort(182886,'QT_IDADE_MINIMA='||:new.qt_idade_minima);
			end if;

			if	(:new.qt_idade_maxima is not null) and
				(:new.qt_idade_maxima < qt_idade_w) then
				--'A idade maxima permitida para o leito e '||to_char(:new.qt_idade_maxima)||' ano(s).
				wheb_mensagem_pck.exibir_mensagem_abort(182887,'QT_IDADE_MAXIMA='||:new.qt_idade_maxima);
			end if;
		end if;

		end;
	end if;
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert into log_mov(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
		values	(	sysdate,
				:new.nm_usuario,
				55829,
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(326147), 1, 4000)||' '||:new.nr_atendimento||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||'/'||SUBSTR(OBTER_DESC_EXPRESSAO(705911), 1, 4000)||': '||:new.cd_setor_atendimento||'/'||:new.cd_unidade_basica||'/'||:new.cd_unidade_compl||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298822), 1, 4000)||': '|| ds_origem_w
				);



end if;

if	(:new.ie_status_unidade = 'L')
and	((:old.ie_status_unidade <> 'L') or
	 (:old.ie_status_unidade is null))
and	(:new.nr_atendimento is not null) then

	-- 'O Leito nao pode ir para o status "Livre" enquanto estiver vinculado a um atendimento!
	wheb_mensagem_pck.exibir_mensagem_abort(182884);

end if;

if	(((:new.cd_paciente_reserva is null) and (:old.cd_paciente_reserva is not null)) or
	 ((:new.nm_pac_reserva is null) and (:old.nm_pac_reserva is not null))) and
	(:new.ds_observacao is not null) then
	:new.ds_observacao		:= null;
end if;

if	(:new.cd_convenio_reserva is null) and
	(:old.cd_convenio_reserva is not null) and
	(:new.ds_observacao is not null) then
	:new.ds_observacao		:= null;
end if;

if 	(((:new.cd_paciente_reserva is null) and (:old.cd_paciente_reserva is not null)) or
	 ((:new.nm_pac_reserva is null) and (:old.nm_pac_reserva is not null)) 		or
	 ((:new.cd_convenio_reserva is null) and	(:old.cd_convenio_reserva is not null))) and
	(:new.nr_seq_motivo_reserva is not null) then
	:new.nr_seq_motivo_reserva := null;
end if;

/* Matheus OS 57361 em 18/06/2007 */
select	count(*)
into	qt_registro_w
from	unidade_atend_prev;

if	(qt_registro_w > 0) then
	begin
	/* Matheus OS 64383 06/08/2007 este select*/
	select	nvl(max(ie_paciente_isolado),'N')
	into	ie_paciente_isolado_w
	from	atendimento_paciente
	where	nr_atendimento = :old.nr_atendimento;
	if	(:new.ie_status_unidade	<> :old.ie_status_unidade) and
		((:old.ie_status_unidade 	= 'O') or (ie_paciente_isolado_w = 'S')) then
		Man_gerar_OS_Planej_Evento(:new.cd_setor_atendimento, :new.cd_unidade_basica, :new.cd_unidade_compl, 'L');
	elsif	(:new.ie_status_unidade	<> :old.ie_status_unidade) and
		(:new.ie_status_unidade in ('L','H','A')) then 	/* Matheus OS 57361 21/06/07 Acrescentado Higienizacao e processo de alta*/
		Man_gerar_OS_Planej_Evento(:new.cd_setor_atendimento, :new.cd_unidade_basica, :new.cd_unidade_compl, 'B');
	end	if;
	end;
end if;

if	(:new.ie_situacao		<> :old.ie_situacao) or
	(:new.ie_temporario		<> :old.ie_temporario) or
	(nvl(:new.cd_motivo_interdicao,0)	<> nvl(:old.cd_motivo_interdicao,0)) or
	(:new.ie_status_unidade	<> :old.ie_status_unidade) then

	select	unidade_atend_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	if	(:new.nr_atendimento is null) then
		select	max(nr_sequencia)
		into	nr_seq_hist_w
		from	unidade_atend_hist
		where	nr_seq_unidade = :new.nr_seq_interno
		and	dt_historico = (select	max(dt_historico)
					from	unidade_atend_hist
					where	nr_seq_unidade	= :new.nr_seq_interno);

		select	max(nr_atendimento)
		into	nr_atendimento_w
		from	unidade_atend_hist
		where	nr_sequencia = nr_seq_hist_w;
	else
		nr_atendimento_w:= :new.nr_atendimento;
	end if;

	nm_usuario_w	:= :new.nm_usuario;
	if	(:new.nm_usuario_higienizacao is not null) and
		((:new.ie_status_unidade = 'H') or
		 (:old.ie_status_unidade = 'H')) then
		nm_usuario_w	:= :new.nm_usuario_higienizacao;
	end if;

	if	 (:new.ds_observacao <> :old.ds_observacao) or
		((:new.ds_observacao is not null) and (:old.ds_observacao is null)) then
		ds_observacao_w	:= :new.ds_observacao;
	else
		ds_observacao_w := '';
	end if;

	select	max(nr_sequencia)
	into	nr_seq_hist_update_w
	from	unidade_atend_hist
	where	nr_seq_unidade = :new.nr_seq_interno
	and		dt_fim_historico is null;

	select 	max(nr_atendimento)
	into	nr_atendimento_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;

	insert into unidade_atend_hist
		(nr_sequencia, nr_seq_unidade, dt_historico,
		dt_atualizacao, nm_usuario, ie_situacao,
		ie_status_unidade, cd_motivo_interdicao,
		ie_temporario, dt_fim_historico, nr_atendimento, ds_observacao,nm_usuario_nrec, cd_funcao, nr_seq_mot_isol )
	values
		(nr_sequencia_w, :new.nr_seq_interno, sysdate,
		sysdate, nvl(nm_usuario_w, 'TASY'), :new.ie_situacao,
		nvl(:new.ie_status_unidade,'L'), decode(:new.ie_status_unidade,'I',:new.cd_motivo_interdicao,null),
		:new.ie_temporario, null, decode(:new.ie_status_unidade,'R',null,'I',null,nr_atendimento_w), ds_observacao_w,:new.nm_usuario, obter_funcao_ativa,
		:new.nr_seq_mot_isol);

	update	unidade_atend_hist
	set		dt_fim_historico 	= sysdate,
			nm_usuario 			= :new.nm_usuario,
			cd_funcao_fim 		= obter_funcao_ativa
	where	nr_seq_unidade 		= :new.nr_seq_interno
	and		dt_fim_historico 	is null
	and		((:new.ie_status_unidade <> :old.ie_status_unidade)
	or		(:new.ie_situacao	<> :old.ie_situacao)
	or  	(:new.ie_temporario	<> :old.ie_temporario)
	or		(nvl(:new.cd_motivo_interdicao,0) <> nvl(:old.cd_motivo_interdicao,0)))
	and		nr_sequencia		= nr_seq_hist_update_w;
	if	(:old.ie_status_unidade is not null) and
		(:new.ie_status_unidade is not null) and
		(:old.ie_status_unidade <> 'G')	and
		(:new.ie_status_unidade = 'G') 	then
		:new.dt_aguard_higienizacao := sysdate;
	elsif	(nvl(:old.nr_atendimento,0) <> 0) and
		(nvl(:new.nr_atendimento,0) = 0) then
		:new.dt_aguard_higienizacao := null;
	end if;
end if;

if	((:new.dt_higienizacao <> :old.dt_higienizacao) or (:old.dt_higienizacao is null)) and
	(:new.dt_higienizacao is not null) and
	(:new.dt_inicio_higienizacao is not null) then
	select	unid_atend_higienizacao_seq.nextval
	into	nr_sequencia_w
	from	dual;
	insert	into unid_atend_higienizacao
		(nr_sequencia, nr_seq_unidade,
		dt_atualizacao, nm_usuario,
		dt_inicio, dt_fim, ie_tipo_limpeza, nm_usuario_nrec, dt_atualizacao_nrec)
	values	(nr_sequencia_w, :new.nr_seq_interno,
		:new.dt_atualizacao,	:new.nm_usuario,
		:new.dt_inicio_higienizacao, :new.dt_higienizacao, :new.ie_tipo_limpeza, :new.nm_usuario, sysdate);
end if;

select max(nr_atendimento_acomp),
       max(cd_paciente_reserva),
       max(nm_paciente_reserva)
into   nr_atendimento_acomp_temp_w,
       cd_paciente_reserva_temp_w,
       nm_paciente_reserva_temp_w
from   w_unid_atend_temp
where  nr_seq_interno = :new.nr_seq_interno;

if (nr_atendimento_acomp_temp_w is not null) and
   (:new.nr_atendimento_acomp is null) then
   update	atendimento_acompanhante
   set		dt_saida	   = sysdate
   where	nr_atendimento	   = nr_atendimento_acomp_temp_w
   and		(cd_pessoa_fisica  = cd_paciente_reserva_temp_w or nm_acompanhante = nm_paciente_reserva_temp_w)
   and		dt_saida is null;
end if;

if	(nvl(:new.nr_atendimento_acomp,0) <> nvl(:old.nr_atendimento_acomp,0)) then
	if	(:new.nr_atendimento_acomp is not null) and
		(:old.nr_atendimento_acomp is null) then


		select	nvl(max(nr_acompanhante),0) + 1
		into	nr_acompanhante_w
		from	atendimento_acompanhante
		where	nr_atendimento	= :new.nr_atendimento_acomp;

		select	max(ie_alojamento)
		into	ie_alojamento_w
		from	atendimento_acompanhante
		where	nr_atendimento	= :new.nr_atendimento_acomp;

		select 	count(*)
		into	qt_itens_w
		from	atendimento_acompanhante
		where	nr_atendimento = :new.nr_atendimento_acomp
		and	dt_acompanhante = sysdate;
		if	(qt_itens_w = 0) then

		    select  count(*)
		    into    qt_itens_w
		    from    atendimento_acompanhante
		    where   nr_atendimento    = :new.nr_atendimento_acomp
		    and     cd_pessoa_fisica  = :new.cd_paciente_reserva
		    and     dt_saida is null;

			if(nvl(qt_itens_w,0) = 0)then
				insert into atendimento_acompanhante(
							 nr_atendimento,
							 dt_acompanhante,
							 nr_acompanhante,
							 dt_atualizacao,
							 nm_usuario,
							 cd_pessoa_fisica,
							 nr_seq_interno,
							 dt_atualizacao_nrec,
							 nm_usuario_nrec,
							 nm_acompanhante,
							 ie_alojamento)
				values(	:new.nr_atendimento_acomp,
							sysdate,
							nr_acompanhante_w,
							sysdate,
							:new.nm_usuario,
							:new.cd_paciente_reserva,
							:new.nr_seq_interno,
							sysdate,
							:new.nm_usuario,
							:new.nm_pac_reserva,
							ie_alojamento_w);
			end if;
		end if;


	elsif	(:new.nr_atendimento_acomp is null) and
		(:old.nr_atendimento_acomp is not null) then
		update	atendimento_acompanhante
		set	dt_saida	= sysdate
		where	nr_atendimento	= :old.nr_atendimento_acomp
		and	(cd_pessoa_fisica	= :old.cd_paciente_reserva or nm_acompanhante = :old.nm_pac_reserva)
		and	dt_saida is null;
	end if;
end if;

/*if	(:new.nr_atendimento is null) and
	(:old.nr_atendimento is not null) then
	avisar_higienizacao_camareira(:new.cd_setor_atendimento, :new.cd_unidade_basica, :new.cd_unidade_compl);
end if;*/

if	((nvl(:old.cd_paciente_reserva,-1) <> nvl(:new.cd_paciente_reserva,-1)) and (:new.cd_paciente_reserva is not null)) or
	((nvl(:old.nm_pac_reserva,-1) <> nvl(:new.nm_pac_reserva,-1)) and (:new.nm_pac_reserva is not null)) then
	insert into reserva_leito_hist(
		nr_sequencia,
		cd_pessoa_fisica,
 		dt_reserva,
 		nm_usuario_reserva,
		nr_seq_interno,
		NM_USUARIO,
		nm_pac_reserva)
	values(
		reserva_leito_hist_seq.nextval,
		:new.cd_paciente_reserva,
		sysdate,
		:new.nm_usuario_reserva,
		:new.nr_seq_interno,
		:new.NM_USUARIO,
		:new.nm_pac_reserva);
end if;

if	(nvl(:old.cd_convenio_reserva,-1) <> nvl(:new.cd_convenio_reserva,-1)) and (:new.cd_convenio_reserva is not null) then
	insert into reserva_leito_hist(
		nr_sequencia,
		cd_convenio,
 		dt_reserva,
 		nm_usuario_reserva,
		nr_seq_interno,
		NM_USUARIO,
		nm_pac_reserva)
	values(
		reserva_leito_hist_seq.nextval,
		:new.cd_convenio_reserva,
		sysdate,
		:new.nm_usuario_reserva,
		:new.nr_seq_interno,
		:new.NM_USUARIO,
		:new.nm_pac_reserva);
end if;

if	((nvl(:old.cd_paciente_reserva,-1) <> nvl(:new.cd_paciente_reserva,-1)) and (:new.cd_paciente_reserva is null)) and
	((nvl(:old.nm_pac_reserva,-1) <> nvl(:new.nm_pac_reserva,-1)) and (:new.nm_pac_reserva is null)) and
	(:old.nm_usuario_reserva is not null) then
	insert into reserva_leito_hist(
		nr_sequencia,
		cd_pessoa_fisica,
 		dt_reserva,
 		nm_usuario_reserva,
		nr_seq_interno,
		dt_liberacao,
		NM_USUARIO,
		nm_pac_reserva)
	values(
		reserva_leito_hist_seq.nextval,
		:old.cd_paciente_reserva,
		sysdate,
		:old.nm_usuario_reserva,
		:old.nr_seq_interno,
		sysdate,
		:new.NM_USUARIO,
		:old.nm_pac_reserva);
end if;

if	(nvl(:old.cd_convenio_reserva,-1) <> nvl(:new.cd_convenio_reserva,-1)) and
	(:new.cd_convenio_reserva is null)  and
	(:old.nm_usuario_reserva is not null) then
	insert into reserva_leito_hist(
		nr_sequencia,
		cd_convenio,
 		dt_reserva,
 		nm_usuario_reserva,
		nr_seq_interno,
		NM_USUARIO,
		dt_liberacao,
		nm_pac_reserva)
	values(
		reserva_leito_hist_seq.nextval,
		:old.cd_convenio_reserva,
		sysdate,
		:old.nm_usuario_reserva,
		:old.nr_seq_interno,
		:new.NM_USUARIO,
		sysdate,
		:old.nm_pac_reserva);
end if;


/* PORTARIA N 312 - Jerusa OS 90025 - 05/05/2008 */
/* Parametro[92] - Utiliza portaria n 312 (Menu do sistema) */
select	nvl(max(obter_valor_param_usuario(0, 92, obter_perfil_ativo, :new.nm_usuario, 0)), 'N')
into	ie_utiliza_portaria_w
from	dual;

if	(ie_utiliza_portaria_w = 'S') then
	begin

	select	count(*)
	into	qt_existe_classif_w
	from	unidade_atend_classif_p312
	where	nr_seq_unid_atend = :new.nr_seq_interno;

	if	(qt_existe_classif_w > 0) then
		begin
		/* Classificacao da unidade de atendimento */
		if	(:new.ie_temporario = 'S') and
			(:old.ie_temporario = 'N') then
			begin
			select	count(*)
			into	qt_classif_w
			from	unidade_atend_classif_p312
			where	nr_seq_unid_atend = :new.nr_seq_interno
			and	ie_classif_p312 in (4,5)
			and	dt_fim_classif is null;

			if	(qt_classif_w > 0) then
				update	unidade_atend_classif_p312
				set	dt_fim_classif	= sysdate,
					nm_usuario	= :new.nm_usuario
				where	nr_seq_unid_atend = :new.nr_seq_interno
				and	ie_classif_p312 in (4,5)
				and	dt_fim_classif is null;
			end if;

			insert into unidade_atend_classif_p312
				(nr_sequencia,
				nr_seq_unid_atend,
				ie_classif_p312,
				dt_inicio_classif,
				dt_atualizacao,
				nm_usuario)
			values	(unidade_atend_classif_p312_seq.nextval,
				:new.nr_seq_interno,
				22,
				sysdate,
				sysdate,
				:new.nm_usuario);
			end;
		elsif	(:new.ie_temporario = 'N') then
			begin
			if	(:old.ie_temporario = 'S') then
				begin
				select	count(*)
				into	qt_classif_w
				from	unidade_atend_classif_p312
				where	nr_seq_unid_atend = :new.nr_seq_interno
				and	ie_classif_p312 = 22
				and	dt_fim_classif is null;

				if	(qt_classif_w > 0) then
					update	unidade_atend_classif_p312
					set	dt_fim_classif	= sysdate,
						nm_usuario	= :new.nm_usuario
					where	nr_seq_unid_atend = :new.nr_seq_interno
					and	ie_classif_p312 = 22
					and	dt_fim_classif is null;
				end if;
				end;
			end if;
			if	(:new.ie_situacao = 'A') then
				begin
				if	(:old.ie_temporario = 'N') and
					(:old.ie_situacao = 'I') then
					begin
					select	count(*)
					into	qt_classif_w
					from	unidade_atend_classif_p312
					where	nr_seq_unid_atend = :new.nr_seq_interno
					and	ie_classif_p312 = 4
					and	dt_fim_classif is null;

					if	(qt_classif_w > 0) then
						update	unidade_atend_classif_p312
						set	dt_fim_classif	= sysdate,
							nm_usuario	= :new.nm_usuario
						where	nr_seq_unid_atend = :new.nr_seq_interno
						and	ie_classif_p312 = 4
						and	dt_fim_classif is null;
					end if;
					end;
				end if;

				select	count(*)
				into	qt_classif_w
				from	unidade_atend_classif_p312
				where	nr_seq_unid_atend = :new.nr_seq_interno
				and	ie_classif_p312 in (4,5)
				and	dt_fim_classif is not null;

				if	(qt_classif_w > 0) then
					insert into unidade_atend_classif_p312
						(nr_sequencia,
						nr_seq_unid_atend,
						ie_classif_p312,
						dt_inicio_classif,
						dt_atualizacao,
						nm_usuario)
					select	unidade_atend_classif_p312_seq.nextval,
						:new.nr_seq_interno,
						vl_dominio,
						sysdate,
						sysdate,
						:new.nm_usuario
					from	valor_dominio_v
					where	cd_dominio = 2175
					and	vl_dominio in (4,5);
				end if;
				end;
			elsif	(:new.ie_situacao = 'I') then
				begin
				if	(:old.ie_temporario = 'N') and
					(:old.ie_situacao = 'A') then
					begin
					select	count(*)
					into	qt_classif_w
					from	unidade_atend_classif_p312
					where	nr_seq_unid_atend = :new.nr_seq_interno
					and	ie_classif_p312 in (4,5)
					and	dt_fim_classif is null;

					if	(qt_classif_w > 0) then
						update	unidade_atend_classif_p312
						set	dt_fim_classif	= sysdate,
							nm_usuario	= :new.nm_usuario
						where	nr_seq_unid_atend = :new.nr_seq_interno
						and	ie_classif_p312 in (4,5)
						and	dt_fim_classif is null;
					end if;
					end;
				end if;

				select	count(*)
				into	qt_classif_w
				from	unidade_atend_classif_p312
				where	nr_seq_unid_atend = :new.nr_seq_interno
				and	ie_classif_p312 = 4
				and	dt_fim_classif is not null;

				if	(qt_classif_w > 0) then
					insert into unidade_atend_classif_p312
						(nr_sequencia,
						nr_seq_unid_atend,
						ie_classif_p312,
						dt_inicio_classif,
						dt_atualizacao,
						nm_usuario)
					values	(unidade_atend_classif_p312_seq.nextval,
						:new.nr_seq_interno,
						4,
						sysdate,
						sysdate,
						:new.nm_usuario);
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end if;

	/* Status da unidade de atendimento */
	if	(:new.ie_situacao = 'I') or
		(:new.ie_status_unidade in ('I', 'O', 'L', 'M', 'P', 'E')) or
		(:new.nr_atendimento is not null) then
		begin
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_unid_status_p312_w
		from	unidade_atend_status_p312
		where	nr_seq_unid_atend = :new.nr_seq_interno
		and	dt_final is null;

		if	(:new.ie_status_unidade = 'L') and
			(:new.ie_situacao <> 'I') then
			ie_status_unidade_p312_w	:= 'LV';
		elsif	((:new.ie_status_unidade in ('I', 'O', 'E')) and
			 (:new.ie_situacao <> 'I')) then
			ie_status_unidade_p312_w	:= 'LB';
		elsif	(:new.ie_status_unidade in ('M', 'P')) or
			(:new.nr_atendimento is not null) and
			(:new.ie_situacao <> 'I') then
			ie_status_unidade_p312_w	:= 'LO';
		end if;

		if	(:new.ie_situacao = 'I') then
			ie_status_unidade_p312_w	:= 'LD';
		end if;

		ie_status_unid_p312_w	:= ' ';
		if	(nr_seq_unid_status_p312_w > 0) then
			begin
			select	ie_status_unidade
			into	ie_status_unid_p312_w
			from	unidade_atend_status_p312
			where	nr_sequencia = nr_seq_unid_status_p312_w
			and	dt_final is null;

			if	(ie_status_unidade_p312_w is not null) and
				(ie_status_unidade_p312_w <> ie_status_unid_p312_w) then
				update	unidade_atend_status_p312
				set	dt_final = sysdate
				where	nr_sequencia = nr_seq_unid_status_p312_w;
			end if;
			end;
		end if;

		if	(ie_status_unidade_p312_w is not null) and
			(ie_status_unidade_p312_w <> ie_status_unid_p312_w) then
			insert into unidade_atend_status_p312(
				nr_sequencia,
				ie_status_unidade,
				dt_final,
				dt_inicio,
				dt_atualizacao,
				nm_usuario,
				nr_seq_unid_atend)
			values	(
				unidade_atend_status_p312_seq.nextval,
				ie_status_unidade_p312_w,
				null,
				sysdate,
				sysdate,
				:new.nm_usuario,
				:new.nr_seq_interno);
		end if;
		end;
	end if;
	end;
end if;

if	(nvl(:new.nr_atendimento,0) <> :old.nr_atendimento) then

	select	max(dt_alta) --Sair dos grupos apenas ao receber alta
	into	dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento	= :old.nr_atendimento;


	select	nvl(max('S'),'N')
	into	ie_grupo_w
	from	pac_grupo_atend
	where	nr_atendimento = :old.nr_atendimento;

	if	(ie_grupo_w = 'S') and
		(dt_alta_w is not null )then
		update	pac_grupo_atend
		set	dt_saida 	= sysdate,
			nm_usuario	= :new.nm_usuario
		where	nr_atendimento 	= :old.nr_atendimento;
	end if;

end if;

ie_inativa_leito_w := 	Obter_Valor_Param_Usuario(1,23, obter_perfil_ativo, :new.nm_usuario,0);

if	(ie_inativa_leito_w = 'S') and
	(:new.ie_status_unidade = 'I') and
	(obter_funcao_ativa = 1) then
	:new.ie_situacao := 'I';
end if;

ie_gera_eritel_w := 	Obter_Valor_Param_Usuario(3111,109, obter_perfil_ativo, :new.nm_usuario,0);
if	((ie_gera_eritel_w = 'S') or
                (ie_gera_eritel_w = 'APT')) and
	(nvl(:new.IE_STATUS_UNIDADE, 'X') = 'A') and
	(:new.nr_atendimento is null ) and
	(:old.nr_atendimento is not null) then
	Insere_W_integracao_eritel	(:new.cd_setor_atendimento,
					:new.cd_unidade_basica,
					:new.cd_unidade_compl,
					:old.nr_atendimento,
					'N',
					:new.nm_usuario,
					cd_estabelecimento_w);

end if;


ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
ds_unidade_log_w := substr(SUBSTR(OBTER_DESC_EXPRESSAO(724202), 1, 4000)||' '||ds_origem_w||' - '||SUBSTR(OBTER_DESC_EXPRESSAO(325836), 1, 4000)||':'||:new.nr_atendimento
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||':'||:new.cd_setor_atendimento
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(341592), 1, 4000)||:new.cd_unidade_basica
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(341593), 1, 4000)||:new.cd_unidade_compl
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(646538), 1, 4000)||':'||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_entrada_unidade, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(326269), 1, 4000)||':'||:new.ie_status_unidade
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(328225), 1, 4000)||':'||:new.nm_usuario
		||' '||SUBSTR(OBTER_DESC_EXPRESSAO(345366), 1, 4000)||':'||substr(obter_desc_funcao(obter_funcao_ativa),1,30)
        ||' '||SUBSTR(OBTER_DESC_EXPRESSAO(330290), 1, 4000)||':'||substr(OBTER_DESC_PERFIL(obter_perfil_ativo),1,30)
        ||' '||SUBSTR(OBTER_DESC_EXPRESSAO(328225), 1, 4000)||':'||substr(obter_usuario_ativo,1,30)
        ,1,4000);

insert 	into log_mov
		(DT_ATUALIZACAO,
		 NM_USUARIO,
		 CD_LOG,
		 DS_LOG)
	values	(sysdate,
		 :new.nm_usuario,
		 55839,
		 ds_unidade_log_w);


if	(:old.nr_atendimento is not null) and
	(:new.nr_atendimento is null) then
	:new.nr_atendimento_ant	:= :old.nr_atendimento;
end if;

if	(:old.ie_status_unidade is not null) and
	(:old.ie_status_unidade <> :new.ie_status_unidade) then
	gerar_higienizacao_unid_status(sysdate, :new.nm_usuario, null, 'ASL', :new.nr_seq_interno, :old.ie_status_unidade, :new.ie_status_unidade, :new.cd_setor_atendimento, null, null, :new.ie_higienizacao);
end if;

if (:old.ie_situacao  = 'I' and :new.ie_situacao = 'A') and
   ((:new.cd_paciente_reserva is not null) or (:new.nm_pac_reserva is not null)) then
	:new.cd_paciente_reserva	:= null;
	:new.nm_pac_reserva		:= null;
	:new.nm_usuario_reserva		:= null;
end if;

if	(ie_serv_aguardando_checklist_w  = 'S') and
	(:new.ie_status_unidade = 'A') and
	(:old.ie_status_unidade <> 'A') then
	update	sl_unid_atend
	set		ie_status_serv = 'E'
	where	nr_sequencia   = (	select max(nr_sequencia)
								from	sl_unid_atend
								where	nr_seq_unidade = :new.nr_seq_interno
								and		ie_status_serv <> 'E'
							 );
end if;

if	(ie_serv_aguard_hig_w = 'S') and
	(:new.ie_status_unidade = 'G') and
	(:old.ie_status_unidade <> 'G') then
	update	sl_unid_atend
	set		ie_status_serv = 'P'
	where	nr_sequencia   = (	select max(nr_sequencia)
								from	sl_unid_atend
								where	nr_seq_unidade = :new.nr_seq_interno
								and		ie_status_serv <> 'E');
end if;


if	((:new.ie_status_unidade = 'O') and
	(:old.ie_status_unidade <> 'O')) or
	((:new.ie_status_unidade <> 'O') and
	(:old.ie_status_unidade = 'O'))	then
		ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert into log_mov(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
		values	(	sysdate,
				:new.nm_usuario,
				55830,
				substr(' '||SUBSTR(OBTER_DESC_EXPRESSAO(298822), 1, 4000)||': '|| ds_origem_w||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298829), 1, 4000)||': '||:old.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(735512), 1, 4000)||': '||:new.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||'/'||SUBSTR(OBTER_DESC_EXPRESSAO(705911), 1, 4000)||': '||:new.cd_setor_atendimento||'/'||:new.cd_unidade_basica||'/'||:new.cd_unidade_compl,1,2000)
				);

end if;

if	(:new.ie_status_unidade = 'H') and
	(:old.ie_status_unidade = 'P')	then
		ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert into log_tasy(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
		values	(	sysdate,
				:new.nm_usuario,
				55831,
				substr(' '||SUBSTR(OBTER_DESC_EXPRESSAO(298822), 1, 4000)||': '|| ds_origem_w||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298829), 1, 4000)||': '||:old.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(735512), 1, 4000)||': '||:new.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||'/'||SUBSTR(OBTER_DESC_EXPRESSAO(705911), 1, 4000)||': '||:new.cd_setor_atendimento||'/'||:new.cd_unidade_basica||'/'||:new.cd_unidade_compl,1,2000)
				);
end if;

if	(:new.ie_status_unidade = 'L') and
	(:old.ie_status_unidade = 'P')	then
		ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert into log_tasy(	DT_ATUALIZACAO,
				NM_USUARIO,
				CD_LOG,
				DS_LOG)
		values	(	sysdate,
				:new.nm_usuario,
				55847,
				substr(' '||SUBSTR(OBTER_DESC_EXPRESSAO(298822), 1, 4000)||': '|| ds_origem_w||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298829), 1, 4000)||': '||:old.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(735512), 1, 4000)||': '||:new.ie_status_unidade||
				' '||SUBSTR(OBTER_DESC_EXPRESSAO(298359), 1, 4000)||'/'||SUBSTR(OBTER_DESC_EXPRESSAO(705911), 1, 4000)||': '||:new.cd_setor_atendimento||'/'||:new.cd_unidade_basica||'/'||:new.cd_unidade_compl,1,2000)
				);
end if;


Obter_param_Usuario(44, 279,  obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_atualizar_gv_w);

if (ie_atualizar_gv_w = 'S') and (:new.ie_status_unidade = 'H') then

	select 	min(nr_sequencia)
	into	nr_seq_gv_w
	from	gestao_vaga
	where	cd_setor_desejado = :new.cd_setor_atendimento
	and	cd_unidade_basica = :new.cd_unidade_basica
	and	cd_unidade_compl  = :new.cd_unidade_compl
	and	dt_prevista = (	select 	min(dt_prevista)
						from	gestao_vaga
						where	cd_setor_desejado = :new.cd_setor_atendimento
						and	cd_unidade_basica = :new.cd_unidade_basica
						and	cd_unidade_compl  = :new.cd_unidade_compl
						and	dt_prevista >= sysdate
						and	ie_status not in ('L','D','C'));-- Desistiu, Cancelada

	if (nvl(nr_seq_gv_w,0) > 0) then

		update	gestao_vaga
		set	ie_status = 'L'
		where	nr_sequencia = nr_seq_gv_w;

	end if;
end if;


--mmmacedo 603149
Obter_param_Usuario(1002, 143,  obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo,ie_atualizar_sts_gv_w );

if (ie_atualizar_sts_gv_w = 'S') and
   (:new.dt_inicio_higienizacao is not null) and
   (:old.dt_higienizacao is null) and
   (:new.dt_higienizacao is not null) then

	select 	min(nr_sequencia)
	into	nr_seq_gv_w
	from	gestao_vaga
	where	cd_setor_desejado = :new.cd_setor_atendimento
	and	cd_unidade_basica = :new.cd_unidade_basica
	and	cd_unidade_compl  = :new.cd_unidade_compl
	and	dt_prevista = (	select 	min(dt_prevista)
						from	gestao_vaga
						where	cd_setor_desejado = :new.cd_setor_atendimento
						and	cd_unidade_basica = :new.cd_unidade_basica
						and	cd_unidade_compl  = :new.cd_unidade_compl
						and	dt_prevista >= sysdate
						and	ie_status not in ('N','D','C'));-- Negada, Desistiu, Cancelada

	if (nvl(nr_seq_gv_w,0) > 0) then

		update	gestao_vaga
		set	ie_status = 'N'
		where	nr_sequencia = nr_seq_gv_w;

	end if;
end if;

--Tiago Braz 568695
Obter_param_Usuario(3111, 298, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_isola_leito_w);

if	(ie_isola_leito_w = 'S') and
	(:old.ie_status_unidade = 'P') and
	(:new.ie_status_unidade <> 'O') then

	select	max(ie_paciente_isolado)
	into	ie_pac_isolado_w
	from	atendimento_paciente
	where	nr_atendimento = :old.nr_atendimento;

	if (ie_pac_isolado_w = 'S') then
		:new.ie_status_unidade		:= 'O';
	end if;
end if;


if (:old.ie_higienizacao is not null) and (:new.ie_higienizacao is not null) and
   (:old.ie_higienizacao <> :new.ie_higienizacao) then

	insert into log_mov
			(dt_atualizacao,
			cd_log,
			nm_usuario,
			ds_log)
	values (	sysdate,
			4567,
			:new.nm_usuario,
			SUBSTR(wheb_mensagem_pck.get_texto(549680, 'NM_USUARIO='||:new.nm_usuario||';OLD_IE_HIGIEN='||:old.ie_higienizacao||';NEW_IE_HIGIEN='||:new.ie_higienizacao), 1, 4000)
			);
end if;

if	(:old.ie_status_unidade = 'O') and
	(:new.ie_status_unidade <> 'O') then
	gerar_higienizacao_leito_unid(sysdate, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,'LPI', :new.nr_seq_interno, null);
end if;

IF (:OLD.ie_status_unidade = 'R') AND (:NEW.ie_status_unidade = 'L') THEN
	gerar_higienizacao_leito_unid(SYSDATE, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento,'LLR', :NEW.nr_seq_interno, NULL);
END IF;

if	(:old.ie_situacao <> :new.ie_situacao) then
	if	(:new.ie_situacao = 'A') then
		gerar_evento_est_atend(:new.nm_usuario,cd_estabelecimento_w,'ALE', :new.nr_seq_interno);
	elsif	(:new.ie_situacao = 'I') then
		gerar_evento_est_atend(:new.nm_usuario,cd_estabelecimento_w,'ILE', :new.nr_seq_interno);
	end if;
end if;

if 	(:old.ie_status_unidade <> 'H') and
	(:new.ie_status_unidade = 'G') then
	:new.dt_inicio_higienizacao := null;
	:new.dt_higienizacao := null;
	:new.nm_usuario_higienizacao := null;
	:new.nm_usuario_fim_higienizacao := null;
end if;

if (:new.dt_atualizacao_nrec is null) then
	select 	max(dt_atualizacao),
		max(nm_usuario)
	into	dt_first_history_w,
		nm_first_history_w
	from 	unidade_atend_hist
	where	nr_sequencia = (select  min(nr_sequencia)
				from	unidade_atend_hist
				where 	nr_seq_unidade = :new.nr_seq_interno);

	if (dt_first_history_w is not null) then
		:new.dt_atualizacao_nrec := dt_first_history_w;
		:new.nm_usuario_nrec 	 := nm_first_history_w;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.UNIDADE_ATENDIMENTO ADD (
  CONSTRAINT UNIATEN_PK
 PRIMARY KEY
 (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT UNIATEN_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNIDADE_ATENDIMENTO ADD (
  CONSTRAINT UNIATEN_PEPOMOD_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PEPO_MODELO (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_QUARATD_FK 
 FOREIGN KEY (NR_SEQ_ROOM_NUMBER) 
 REFERENCES TASY.QUARTO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_CLPALE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PAC_LEITO) 
 REFERENCES TASY.CLASSIF_PAC_LEITO (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_GREQPCA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_EQUIP_CA) 
 REFERENCES TASY.GRUPO_EQUIPAMENTO_CA (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_MOTINLE_FK 
 FOREIGN KEY (CD_MOTIVO_INTERDICAO) 
 REFERENCES TASY.MOTIVO_INTERDICAO_LEITO (CD_MOTIVO),
  CONSTRAINT UNIATEN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT UNIATEN_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT UNIATEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT UNIATEN_ATEPACI_FK2 
 FOREIGN KEY (NR_ATENDIMENTO_ACOMP) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT UNIATEN_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT UNIATEN_PESFISI_FK 
 FOREIGN KEY (CD_PACIENTE_RESERVA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT UNIATEN_MOTREUN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RESERVA) 
 REFERENCES TASY.MOTIVO_RESERVA_UNIDADE (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_COMPUTA_FK 
 FOREIGN KEY (NR_SEQ_COMPUTADOR) 
 REFERENCES TASY.COMPUTADOR (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_COMPUTA_FK2 
 FOREIGN KEY (NR_SEQ_COMPUTADOR_LEITO) 
 REFERENCES TASY.COMPUTADOR (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_PALOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_PA) 
 REFERENCES TASY.PA_LOCAL (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_UNIATEN_FK2 
 FOREIGN KEY (NR_SEQ_UNIDADE_RN) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT UNIATEN_CLSUNAT_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_UNIDADE_ATEND (NR_SEQUENCIA),
  CONSTRAINT UNIATEN_MTVISLT_FK 
 FOREIGN KEY (NR_SEQ_MOT_ISOL) 
 REFERENCES TASY.MOTIVO_ISOLAMENTO_LEITO (NR_SEQUENCIA));

GRANT SELECT ON TASY.UNIDADE_ATENDIMENTO TO NIVEL_1;

