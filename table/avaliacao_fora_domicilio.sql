ALTER TABLE TASY.AVALIACAO_FORA_DOMICILIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AVALIACAO_FORA_DOMICILIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AVALIACAO_FORA_DOMICILIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AVALIACAO     NUMBER(10),
  IE_TIPO_AVALIACAO    VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AVAFODO_MEDTIAV_FK_I ON TASY.AVALIACAO_FORA_DOMICILIO
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AVAFODO_PK ON TASY.AVALIACAO_FORA_DOMICILIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AVALIACAO_FORA_DOMICILIO ADD (
  CONSTRAINT AVAFODO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AVALIACAO_FORA_DOMICILIO ADD (
  CONSTRAINT AVAFODO_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AVALIACAO_FORA_DOMICILIO TO NIVEL_1;

