ALTER TABLE TASY.PLS_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MATERIAL
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_DESPESA         VARCHAR2(2 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  NR_SEQ_ESTRUT_MAT       NUMBER(10)            NOT NULL,
  QT_CONVERSAO            NUMBER(13,4),
  CD_TISS_BRASINDICE      VARCHAR2(15 BYTE),
  CD_SIMPRO               NUMBER(15),
  QT_CONVERSAO_SIMPRO     NUMBER(13,4),
  DS_MEDICAMENTO          VARCHAR2(255 BYTE),
  CD_UNIDADE_MEDIDA       VARCHAR2(30 BYTE)     NOT NULL,
  DS_MATERIAL             VARCHAR2(255 BYTE)    NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_MATERIAL             NUMBER(10),
  NR_SEQ_MATERIAL_UNIMED  NUMBER(10),
  DS_MATERIAL_SEM_ACENTO  VARCHAR2(255 BYTE),
  CD_MATERIAL_OPS         VARCHAR2(20 BYTE),
  IE_ORIGEM               VARCHAR2(3 BYTE),
  DS_NOME_COMERCIAL       VARCHAR2(255 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  DT_INCLUSAO             DATE                  NOT NULL,
  DT_EXCLUSAO             DATE,
  CD_MATERIAL_OPS_ORIG    VARCHAR2(30 BYTE),
  IE_REVISAR              VARCHAR2(5 BYTE),
  NR_SEQ_MAT_UNI_FED_SC   NUMBER(10),
  QT_CONVERSAO_PREST      NUMBER(15,4),
  DT_VALIDADE             DATE,
  NR_SEQ_GRUPO_AJUSTE     NUMBER(10),
  NR_SEQ_TIPO_USO         NUMBER(10),
  CD_FARMACIA_COM         VARCHAR2(30 BYTE),
  QT_CONVERSAO_FARM       NUMBER(13,4),
  CD_MATERIAL_A900        VARCHAR2(30 BYTE),
  QT_CONVERSAO_A900       NUMBER(13,4),
  IE_PIS_COFINS           VARCHAR2(1 BYTE),
  DT_LIMITE_UTILIZACAO    DATE,
  NR_SEQ_MARCA            NUMBER(10),
  NR_REGISTRO_ANVISA      VARCHAR2(60 BYTE),
  DS_MOTIVO_EXCLUSAO      VARCHAR2(4000 BYTE),
  IE_NAO_GERA_SIMPRO      VARCHAR2(1 BYTE),
  IE_NAO_GERA_BRASINDICE  VARCHAR2(1 BYTE),
  NR_SEQ_TUSS_MAT_ITEM    NUMBER(10),
  CD_MATERIAL_TUSS_A900   NUMBER(10),
  DS_VERSAO_TISS          VARCHAR2(20 BYTE),
  IE_TIPO_TABELA          VARCHAR2(10 BYTE),
  CD_MATERIAL_OPS_NUMBER  NUMBER(20),
  DT_REFERENCIA           DATE,
  DT_INCLUSAO_REF         DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  CD_GRUPO_PROC           VARCHAR2(3 BYTE),
  DS_GRUPO_PROC           VARCHAR2(255 BYTE),
  IE_SISTEMA_ANT          VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMAT_ESTABEL_FK_I ON TASY.PLS_MATERIAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I ON TASY.PLS_MATERIAL
(CD_MATERIAL_OPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I11 ON TASY.PLS_MATERIAL
(CD_MATERIAL_OPS_NUMBER, DT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I12 ON TASY.PLS_MATERIAL
(CD_MATERIAL_OPS_NUMBER, NVL("DT_REFERENCIA",TO_DATE(' 1899-12-30 00:00:00', 'syyyy-mm-dd hh24:mi:ss')))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I13 ON TASY.PLS_MATERIAL
(CD_MATERIAL_OPS, NVL("DT_REFERENCIA",TO_DATE(' 1899-12-30 00:00:00', 'syyyy-mm-dd hh24:mi:ss')))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I14 ON TASY.PLS_MATERIAL
(CD_TISS_BRASINDICE, DT_INCLUSAO_REF, DT_FIM_VIGENCIA_REF, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I16 ON TASY.PLS_MATERIAL
(NR_SEQ_TUSS_MAT_ITEM, DT_INCLUSAO_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I17 ON TASY.PLS_MATERIAL
(CD_MATERIAL_A900)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_I2 ON TASY.PLS_MATERIAL
(NR_SEQ_MATERIAL_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_I2
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_I3 ON TASY.PLS_MATERIAL
(IE_TIPO_DESPESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_MARCA_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_MATERIA_FK_I ON TASY.PLS_MATERIAL
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMAT_PK ON TASY.PLS_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_PLSESMAT_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_PLSGAMT_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_GRUPO_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_PLSGAMT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_PLSMUFS_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_MAT_UNI_FED_SC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_PLSMUFS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_PLSTUMA_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_TIPO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_PLSTUMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_SIMMATE_FK_I ON TASY.PLS_MATERIAL
(CD_SIMPRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMAT_TUSSITE_FK_I ON TASY.PLS_MATERIAL
(NR_SEQ_TUSS_MAT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_TUSSITE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMAT_UNIMEDI_FK_I ON TASY.PLS_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMAT_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_material_delete
before delete ON TASY.PLS_MATERIAL for each row
declare
ie_possui_reg_w		number(5);

begin

select	count(*)
into	ie_possui_reg_w
from	material_tipo_local_est
where	cd_material = :old.cd_material;

if	(ie_possui_reg_w > 0) then
	update	material_tipo_local_est
	set	ie_operadora	= 'X'
	where	cd_material	= :old.cd_material;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_MATERIAL_tp  after update ON TASY.PLS_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIMITE_UTILIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_LIMITE_UTILIZACAO,1,4000),substr(:new.DT_LIMITE_UTILIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIMITE_UTILIZACAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MATERIAL,1,500);gravar_log_alteracao(substr(:old.DS_MATERIAL,1,4000),substr(:new.DS_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ESTABELECIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_CONVERSAO_SIMPRO,1,500);gravar_log_alteracao(substr(:old.QT_CONVERSAO_SIMPRO,1,4000),substr(:new.QT_CONVERSAO_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_SIMPRO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_UNIDADE_MEDIDA,1,500);gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MEDICAMENTO,1,500);gravar_log_alteracao(substr(:old.DS_MEDICAMENTO,1,4000),substr(:new.DS_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MEDICAMENTO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ESTRUT_MAT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUT_MAT,1,4000),substr(:new.NR_SEQ_ESTRUT_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUT_MAT',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_CONVERSAO,1,500);gravar_log_alteracao(substr(:old.QT_CONVERSAO,1,4000),substr(:new.QT_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_TISS_BRASINDICE,1,500);gravar_log_alteracao(substr(:old.CD_TISS_BRASINDICE,1,4000),substr(:new.CD_TISS_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'CD_TISS_BRASINDICE',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_SIMPRO,1,500);gravar_log_alteracao(substr(:old.CD_SIMPRO,1,4000),substr(:new.CD_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SIMPRO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_MATERIAL_UNIMED,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL_UNIMED,1,4000),substr(:new.NR_SEQ_MATERIAL_UNIMED,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL_UNIMED',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL_OPS,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL_OPS,1,4000),substr(:new.CD_MATERIAL_OPS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_OPS',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MATERIAL_SEM_ACENTO,1,500);gravar_log_alteracao(substr(:old.DS_MATERIAL_SEM_ACENTO,1,4000),substr(:new.DS_MATERIAL_SEM_ACENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL_SEM_ACENTO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INCLUSAO,1,500);gravar_log_alteracao(substr(:old.DT_INCLUSAO,1,4000),substr(:new.DT_INCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INCLUSAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_EXCLUSAO,1,500);gravar_log_alteracao(substr(:old.DT_EXCLUSAO,1,4000),substr(:new.DT_EXCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EXCLUSAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ORIGEM,1,500);gravar_log_alteracao(substr(:old.IE_ORIGEM,1,4000),substr(:new.IE_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_NOME_COMERCIAL,1,500);gravar_log_alteracao(substr(:old.DS_NOME_COMERCIAL,1,4000),substr(:new.DS_NOME_COMERCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOME_COMERCIAL',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL_OPS_ORIG,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL_OPS_ORIG,1,4000),substr(:new.CD_MATERIAL_OPS_ORIG,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_OPS_ORIG',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REVISAR,1,500);gravar_log_alteracao(substr(:old.IE_REVISAR,1,4000),substr(:new.IE_REVISAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REVISAR',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_MAT_UNI_FED_SC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MAT_UNI_FED_SC,1,4000),substr(:new.NR_SEQ_MAT_UNI_FED_SC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MAT_UNI_FED_SC',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_CONVERSAO_PREST,1,500);gravar_log_alteracao(substr(:old.QT_CONVERSAO_PREST,1,4000),substr(:new.QT_CONVERSAO_PREST,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_PREST',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_VALIDADE,1,500);gravar_log_alteracao(substr(:old.DT_VALIDADE,1,4000),substr(:new.DT_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_VALIDADE',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_FARMACIA_COM,1,500);gravar_log_alteracao(substr(:old.CD_FARMACIA_COM,1,4000),substr(:new.CD_FARMACIA_COM,1,4000),:new.nm_usuario,nr_seq_w,'CD_FARMACIA_COM',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_CONVERSAO_FARM,1,500);gravar_log_alteracao(substr(:old.QT_CONVERSAO_FARM,1,4000),substr(:new.QT_CONVERSAO_FARM,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_FARM',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL_A900,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL_A900,1,4000),substr(:new.CD_MATERIAL_A900,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_A900',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_CONVERSAO_A900,1,500);gravar_log_alteracao(substr(:old.QT_CONVERSAO_A900,1,4000),substr(:new.QT_CONVERSAO_A900,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_A900',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_USO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_USO,1,4000),substr(:new.NR_SEQ_TIPO_USO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_USO',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_GRUPO_AJUSTE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_AJUSTE,1,4000),substr(:new.NR_SEQ_GRUPO_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_AJUSTE',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_REGISTRO_ANVISA,1,500);gravar_log_alteracao(substr(:old.NR_REGISTRO_ANVISA,1,4000),substr(:new.NR_REGISTRO_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_ANVISA',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PIS_COFINS,1,500);gravar_log_alteracao(substr(:old.IE_PIS_COFINS,1,4000),substr(:new.IE_PIS_COFINS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PIS_COFINS',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_MARCA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_DESPESA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA,1,4000),substr(:new.IE_TIPO_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA',ie_log_w,ds_w,'PLS_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_material_insert
BEFORE INSERT OR UPDATE ON TASY.PLS_MATERIAL FOR EACH ROW
declare

qt_registros_w	pls_integer := 0;

begin

if	(:new.ds_material_sem_acento is null) or
	(:new.ds_material <> :old.ds_material ) then
	:new.ds_material_sem_acento	:= upper(elimina_acentuacao(:new.ds_material));
end if;

if	(:new.cd_material_ops is not null) and
	(:old.cd_material_ops is null) then
	:new.cd_material_ops_orig	:= :new.cd_material_ops;
end if;


if	(:new.DT_EXCLUSAO is not null) then
	select	count(1)
	into	qt_registros_w
	from	pls_material_a900
	where	nr_seq_material		= :new.nr_sequencia
	and	(dt_fim_vigencia	is null
	or	dt_fim_vigencia		> :new.dt_exclusao);

	if	(qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort('Existem registros A900 vinculados a este material que n�o possuem fim de vig�ncia,' ||
						' ou est�o com a data de fim de vig�ncia superior a data de exclus�o do material.' ||
						chr(13) || chr(10) || 'Necess�rio realizar revis�o dos registros A900.');
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.pls_material_atual_tot
before insert or update or delete ON TASY.PLS_MATERIAL for each row
declare
nr_seq_material_w	pls_material.nr_sequencia%type;

cursor	C01 is
	select	a.nr_sequencia nr_seq_grupo
	from	pls_preco_grupo_material a
	where	exists (select	1
			from	pls_preco_material b
			where	a.nr_sequencia = b.nr_seq_grupo
			and	b.nr_seq_estrutura_mat is not null);
begin

-- se existir diferen�a de estrutura, manda atualizar a tabela que grava a estrutura e os materiais
if	((nvl(:new.nr_seq_estrut_mat, -1) != nvl(:old.nr_seq_estrut_mat, -1))) then

	 -- se tiver novo valor usa ele, sen�o busca do antigo
	 nr_seq_material_w := null;
	 if	(:new.nr_sequencia is not null) then
		nr_seq_material_w := :new.nr_sequencia;
	 elsif	(:old.nr_sequencia is not null) then
		nr_seq_material_w := :old.nr_sequencia;
	 end if;

	-- marca para atualizar todos grupos de materiais que possuam pelo menos uma estrutura informada
	for r_c01_w in c01 loop
		pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_MATERIAL_TM',
									wheb_usuario_pck.get_nm_usuario,
									'PLS_MATERIAL_ATUAL_TOT',
									'nr_seq_grupo_p=' || r_c01_w.nr_seq_grupo);
	end loop;

	if	(nr_seq_material_w is not null) then
		pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_ESTRUTURA_MATERIAL_TM',
									wheb_usuario_pck.get_nm_usuario,
									'PLS_MATERIAL_ATUAL_TOT',
									'nr_seq_material_p=' || nr_seq_material_w);

		if	(not inserting) then
			pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_ESTRUTURA_OCOR_TM',
										wheb_usuario_pck.get_nm_usuario,
										'PLS_MATERIAL_ATUAL_TOT',
										'nr_seq_material_p=' || nr_seq_material_w);
		end if;
	end if;
end if;

-- se for atualiza��o ou insert
-- tratamento para melhorar a performance durante a busca de materiais OPS nesta tabela
if	(inserting or updating) then

	-- Se a data de inclus�o for maior que a data de exclus�o, exibi a mensagem
	if	(:new.dt_exclusao is not null) and
		(:new.dt_exclusao <= :new.dt_inclusao) then -- removido o Trunc, pois em alguns cadastro do A900, pode acontecer da exclus�o ser no mesmo dia que a inclus�o(!), por isto tem que considerar a hora
		wheb_mensagem_pck.exibir_mensagem_abort(1033387,'NR_SEQ_MATERIAL_P='||to_char(:new.nr_sequencia)||';DT_EXCLUSAO_P='||to_char(:new.dt_exclusao, 'dd/mm/yyyy hh24:mi:ss')||';'); -- A data de exclus�o deve ser maior que a data de inclus�o.
	end if;

	-- se mudou o material
	if	(nvl(:new.cd_material_ops, '-1') != nvl(:old.cd_material_ops, '-1')) then
		:new.cd_material_ops_number := pls_util_pck.converte_para_numero(:new.cd_material_ops);
	end if;

	-- se mudou alguma data
	if	((nvl(:new.dt_limite_utilizacao, to_date('30/12/1899', 'dd/mm/yyyy')) != nvl(:old.dt_limite_utilizacao, to_date('30/12/1899', 'dd/mm/yyyy'))) or
		 (nvl(:new.dt_exclusao, to_date('30/12/1899', 'dd/mm/yyyy')) != nvl(:old.dt_exclusao, to_date('30/12/1899', 'dd/mm/yyyy')))) then
		:new.dt_referencia := nvl(:new.dt_limite_utilizacao, :new.dt_exclusao);
	end if;
end if;

end pls_material_atual_tot;
/


CREATE OR REPLACE TRIGGER TASY.pls_material_dt_ref
before insert or update ON TASY.PLS_MATERIAL for each row
declare

begin

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inclusao_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inclusao, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(nvl(:new.dt_limite_utilizacao, :new.dt_exclusao), 'F');

end pls_material_dt_ref;
/


CREATE OR REPLACE TRIGGER TASY.pls_material_update
before update ON TASY.PLS_MATERIAL for each row
declare
qt_registro_w		pls_integer;
dt_referencia_w		date := trunc(:new.dt_atualizacao);
cd_empresa_w		estabelecimento.cd_empresa%type := obter_empresa_estab(:old.cd_estabelecimento);

begin
select	count(1)
into	qt_registro_w
from	regra_resp_revisao_mat
where	cd_empresa	= cd_empresa_w
and	dt_referencia_w between dt_inicio_vigencia and nvl(fim_dia(dt_fim_vigencia),dt_referencia_w);

if	(qt_registro_w > 0) then
	select	count(1)
	into	qt_registro_w
	from	material_tipo_local_est
	where	cd_material = :old.cd_material;

	if	(qt_registro_w > 0) and
		(:old.ie_revisar = :new.ie_revisar) then
		:new.ie_revisar	:= 'R';
		gerar_comunic_revisao_mat(:new.cd_material, :new.nr_sequencia, dt_referencia_w,:old.cd_estabelecimento, :new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_MATERIAL ADD (
  CONSTRAINT PLSMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MATERIAL ADD (
  CONSTRAINT PLSMAT_TUSSITE_FK 
 FOREIGN KEY (NR_SEQ_TUSS_MAT_ITEM) 
 REFERENCES TASY.TUSS_MATERIAL_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_PLSMUFS_FK 
 FOREIGN KEY (NR_SEQ_MAT_UNI_FED_SC) 
 REFERENCES TASY.PLS_MAT_UNIMED_FED_SC (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_PLSGAMT_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AJUSTE) 
 REFERENCES TASY.PLS_GRUPO_AJUSTE_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_PLSTUMA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_USO) 
 REFERENCES TASY.PLS_TIPO_USO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSMAT_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSMAT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PLSMAT_SIMMATE_FK 
 FOREIGN KEY (CD_SIMPRO) 
 REFERENCES TASY.SIMPRO_CADASTRO (CD_SIMPRO),
  CONSTRAINT PLSMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PLS_MATERIAL TO NIVEL_1;

