DROP TABLE TASY.SUP_INT_PF_CONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_PF_CONSIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_INCONSISTENCIA    NUMBER(5)                NOT NULL,
  DS_MENSAGEM          VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUINPFC_SUINPEJ_FK_I ON TASY.SUP_INT_PF_CONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINPFC_SUINPEJ_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUP_INT_PF_CONSIST ADD (
  CONSTRAINT SUINPFC_SUINPEJ_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.SUP_INT_PJ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_INT_PF_CONSIST TO NIVEL_1;

