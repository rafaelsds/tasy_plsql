ALTER TABLE TASY.PLS_OC_CTA_VAL_AUT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_AUT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_AUT_ITEM
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_OC_CTA_COMB         NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_VALIDA_AUT_ITEM         VARCHAR2(1 BYTE),
  IE_SENHA_INTERNA           VARCHAR2(1 BYTE),
  IE_SENHA_EXTERNA           VARCHAR2(1 BYTE),
  IE_VALIDA_QTD_UTIL         VARCHAR2(1 BYTE)   NOT NULL,
  IE_SOMENTE_ITEM_AUTOR      VARCHAR2(1 BYTE),
  QT_MINIMA                  NUMBER(10),
  IE_CONSIDERAR_GRAU_PARTIC  VARCHAR2(2 BYTE),
  IE_CONSIDERAR_SEMELHANTE   VARCHAR2(2 BYTE),
  IE_VALIDA_PROC_PTU_AUT     VARCHAR2(2 BYTE),
  NR_SEQ_TAB_BAIXO_RISCO     NUMBER(10),
  IE_SOLIC_AUT_NEGA          VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCAI_PK ON TASY.PLS_OC_CTA_VAL_AUT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCAI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCAI_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_AUT_ITEM
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCAI_PLSOCCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCAI_PLSTBRC_FK_I ON TASY.PLS_OC_CTA_VAL_AUT_ITEM
(NR_SEQ_TAB_BAIXO_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_AUT_ITEM_tp  after update ON TASY.PLS_OC_CTA_VAL_AUT_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_VALIDA_QTD_UTIL,1,4000),substr(:new.IE_VALIDA_QTD_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_QTD_UTIL',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_AUT_ITEM,1,4000),substr(:new.IE_VALIDA_AUT_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_AUT_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_INTERNA,1,4000),substr(:new.IE_SENHA_INTERNA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_INTERNA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_EXTERNA,1,4000),substr(:new.IE_SENHA_EXTERNA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_EXTERNA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TAB_BAIXO_RISCO,1,4000),substr(:new.NR_SEQ_TAB_BAIXO_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TAB_BAIXO_RISCO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERAR_SEMELHANTE,1,4000),substr(:new.IE_CONSIDERAR_SEMELHANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_SEMELHANTE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERAR_GRAU_PARTIC,1,4000),substr(:new.IE_CONSIDERAR_GRAU_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERAR_GRAU_PARTIC',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMA,1,4000),substr(:new.QT_MINIMA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_PROC_PTU_AUT,1,4000),substr(:new.IE_VALIDA_PROC_PTU_AUT,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_PROC_PTU_AUT',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_ITEM_AUTOR,1,4000),substr(:new.IE_SOMENTE_ITEM_AUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_ITEM_AUTOR',ie_log_w,ds_w,'PLS_OC_CTA_VAL_AUT_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_AUT_ITEM ADD (
  CONSTRAINT PLSOCAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_AUT_ITEM ADD (
  CONSTRAINT PLSOCAI_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSOCAI_PLSTBRC_FK 
 FOREIGN KEY (NR_SEQ_TAB_BAIXO_RISCO) 
 REFERENCES TASY.PLS_TAB_BAIXO_RISCO_CTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_AUT_ITEM TO NIVEL_1;

