ALTER TABLE TASY.SCHEMATIC_BUILD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEMATIC_BUILD CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEMATIC_BUILD
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  CD_VERSAO       VARCHAR2(10 BYTE),
  NR_BUILD        NUMBER(10),
  NM_TABELA       VARCHAR2(50 BYTE),
  NR_SEQ_TABELA   NUMBER(10),
  CD_FUNCAO       NUMBER(5),
  IE_ACAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCHAMBUILD_PK ON TASY.SCHEMATIC_BUILD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEMATIC_BUILD ADD (
  CONSTRAINT SCHAMBUILD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEMATIC_BUILD TO NIVEL_1;

