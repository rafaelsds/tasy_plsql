ALTER TABLE TASY.PLS_USUARIO_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_USUARIO_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_USUARIO_WEB
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NM_USUARIO_WEB              VARCHAR2(100 BYTE) NOT NULL,
  CD_PESSOA_FISICA_RESP       VARCHAR2(10 BYTE),
  DS_SENHA                    VARCHAR2(64 BYTE) NOT NULL,
  DS_EMAIL                    VARCHAR2(255 BYTE),
  NR_SEQ_PRESTADOR            NUMBER(10),
  IE_SOLICITACAO_AUTORIZACAO  VARCHAR2(1 BYTE),
  IE_UPLOAD_CONTAS_MEDICAS    VARCHAR2(1 BYTE),
  IE_CONTAS_MEDICAS_ON_LINE   VARCHAR2(1 BYTE),
  DS_OBSERVACAO               VARCHAR2(2000 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  IE_TIPO_COMPLEMENTO         NUMBER(2),
  IE_EXIGE_BIOMETRIA          VARCHAR2(1 BYTE),
  NR_SEQ_PERFIL_WEB           NUMBER(10),
  DT_ALTERACAO_SENHA          DATE,
  NR_SEQ_LOCAL_ATEND          NUMBER(10),
  QT_TENTATIVA_ACESSO         NUMBER(10),
  DS_TEC                      VARCHAR2(15 BYTE),
  IE_BIOMETRIA_TASY_AGENT     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSUSWE_ESTABEL_FK_I ON TASY.PLS_USUARIO_WEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSUSWE_LOCATME_FK_I ON TASY.PLS_USUARIO_WEB
(NR_SEQ_LOCAL_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSUSWE_PESFISI_FK_I ON TASY.PLS_USUARIO_WEB
(CD_PESSOA_FISICA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSUSWE_PK ON TASY.PLS_USUARIO_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSUSWE_PLSPERW_FK_I ON TASY.PLS_USUARIO_WEB
(NR_SEQ_PERFIL_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSUSWE_PLSPERW_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSUSWE_PLSPRES_FK_I ON TASY.PLS_USUARIO_WEB
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSUSWE_PLSPRES_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSUSWE_UK ON TASY.PLS_USUARIO_WEB
(NM_USUARIO_WEB, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_usuario_web_tr
before insert or update ON TASY.PLS_USUARIO_WEB for each row
declare

/*nm_machine_w			v$session.machine%type;
osuser_w			v$session.osuser%type;
ds_programa_w			varchar2(255);
ds_action_w			v$session.action%type;
nm_id_sid_w			v$session.sid%type;
nm_id_serial_w			v$session.serial#%type;*/

begin

null;

/*begin
	select	max(machine),
		max(osuser),
		substr(max(program), 1, 255),
		max(action),
		max(sid),
		max(serial#)
	into	nm_machine_w,
		osuser_w,
		ds_programa_w,
		ds_action_w,
		nm_id_sid_w,
		nm_id_serial_w
	from 	v$session
	where	audsid = userenv('sessionid');
exception
when others then
	nm_machine_w		:= null;
	osuser_w		:= null;
	ds_programa_w		:= null;
	ds_action_w		:= null;
	nm_id_sid_w		:= null;
	nm_id_serial_w		:= null;
end;

insert into pls_log_execucao_temp (	nr_sequencia,
					ds,
					ds_processo,
					data,
					nm_usuario,
					nm_id_sid,
					nm_id_serial,
					ds_extra)
				values (pls_log_execucao_temp_seq.nextval,
					'PLS_USUARIO_WEB',
					'PLS_USUARIO_WEB',
					sysdate,
					:new.nm_usuario,
					nm_id_sid_w,
					nm_id_serial_w,
					'Seq PLS_USUARIO_WEB: '		|| :new.nr_sequencia || chr(10) || chr(10) ||
					'AplicacaoTasy: '		|| pls_se_aplicacao_tasy || chr(10) ||
					'M�quina: '			|| nm_machine_w || chr(10) ||
					'Osuser: '			|| osuser_w || chr(10) ||
					'Program: '			|| ds_programa_w || chr(10) ||
					'Action: '			|| ds_action_w || chr(10) || chr(10) ||
					'Nm usuario Old: ' 		|| :old.nm_usuario || chr(10) ||
					'Nm usuario New: ' 		|| :new.nm_usuario || chr(10) ||
					'Dt atualizacao Old: ' 		|| :old.dt_atualizacao || chr(10) ||
					'Dt atualizacao New: ' 		|| :new.dt_atualizacao || chr(10) ||
					'Dt alteracao senha Old: ' 	|| :old.dt_alteracao_senha || chr(10) ||
					'Dt alteracao senha New: ' 	|| :new.dt_alteracao_senha || chr(10) || chr(10) ||
					'Nm usuario nrec Old: '		|| :old.nm_usuario_nrec || chr(10) ||
					'Nm usuario nrec New: '		|| :new.nm_usuario_nrec || chr(10) ||
					'Dt atualizacao nrec Old: ' 	|| :old.dt_atualizacao_nrec || chr(10) ||
					'Dt atualizacao nrec New: ' 	|| :new.dt_atualizacao_nrec || chr(10) || chr(10) || dbms_utility.format_call_stack);*/
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_usuario_web_insert
BEFORE INSERT OR UPDATE ON TASY.PLS_USUARIO_WEB FOR EACH ROW
declare

begin

if	(length(:new.nm_usuario_web) > 15) then
	-- Nome de usu�rio com mais de 15 caracteres.
	wheb_mensagem_pck.exibir_mensagem_abort(252368, null);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.PLS_USUARIO_WEB_tp  after update ON TASY.PLS_USUARIO_WEB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_USUARIO_WEB,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_WEB,1,4000),substr(:new.NM_USUARIO_WEB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_WEB',ie_log_w,ds_w,'PLS_USUARIO_WEB',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA_RESP,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA_RESP,1,4000),substr(:new.CD_PESSOA_FISICA_RESP,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA_RESP',ie_log_w,ds_w,'PLS_USUARIO_WEB',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_USUARIO_WEB',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SOLICITACAO_AUTORIZACAO,1,500);gravar_log_alteracao(substr(:old.IE_SOLICITACAO_AUTORIZACAO,1,4000),substr(:new.IE_SOLICITACAO_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITACAO_AUTORIZACAO',ie_log_w,ds_w,'PLS_USUARIO_WEB',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_EMAIL,1,500);gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PLS_USUARIO_WEB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_USUARIO_WEB ADD (
  CONSTRAINT PLSUSWE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSUSWE_UK
 UNIQUE (NM_USUARIO_WEB, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_USUARIO_WEB ADD (
  CONSTRAINT PLSUSWE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSUSWE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSUSWE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSUSWE_PLSPERW_FK 
 FOREIGN KEY (NR_SEQ_PERFIL_WEB) 
 REFERENCES TASY.PLS_PERFIL_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSUSWE_LOCATME_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ATEND) 
 REFERENCES TASY.LOCAL_ATENDIMENTO_MEDICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_USUARIO_WEB TO NIVEL_1;

