ALTER TABLE TASY.REG_TC_SO_PENDENCIES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_TC_SO_PENDENCIES CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_TC_SO_PENDENCIES
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_EV_ITEM        NUMBER(10)              NOT NULL,
  NR_SEQ_SERVICE_ORDER  NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECTCSO_MANORSE_FK_I ON TASY.REG_TC_SO_PENDENCIES
(NR_SEQ_SERVICE_ORDER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RECTCSO_PK ON TASY.REG_TC_SO_PENDENCIES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECTCSO_REGTCEI_FK_I ON TASY.REG_TC_SO_PENDENCIES
(NR_SEQ_EV_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_tc_so_pendencies_before
before insert or update ON TASY.REG_TC_SO_PENDENCIES for each row
declare

ie_classificacao_cliente_w	man_ordem_servico.ie_classificacao_cliente%type;

begin

	if	(phi_is_base_philips = 'S') then
		begin
			select	max(mos.ie_classificacao_cliente)
			into	ie_classificacao_cliente_w
			from	man_ordem_servico mos
			where	mos.nr_sequencia = :new.nr_seq_service_order;

			if (nvl(ie_classificacao_cliente_w, 'X') <> 'A') then
				begin
					wheb_mensagem_pck.exibir_mensagem_abort(1131764);
				end;
			end if;
		end;
	end if;
end;
/


ALTER TABLE TASY.REG_TC_SO_PENDENCIES ADD (
  CONSTRAINT RECTCSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_TC_SO_PENDENCIES ADD (
  CONSTRAINT RECTCSO_REGTCEI_FK 
 FOREIGN KEY (NR_SEQ_EV_ITEM) 
 REFERENCES TASY.REG_TC_EVIDENCE_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_TC_SO_PENDENCIES TO NIVEL_1;

