ALTER TABLE TASY.WL_REGRA_WORKLIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WL_REGRA_WORKLIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.WL_REGRA_WORKLIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WLREWO_PK ON TASY.WL_REGRA_WORKLIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WLREWO_WLITEM_FK_I ON TASY.WL_REGRA_WORKLIST
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.wl_regra_worklist_atual
before insert ON TASY.WL_REGRA_WORKLIST for each row
declare
ie_regra_worklist_w		char(1);

begin

select	nvl2(max(nr_sequencia),'S','N')
into	ie_regra_worklist_w
from	wl_regra_worklist
where	nr_seq_item = :new.nr_seq_item
and		nr_seq_item = (	select	nr_sequencia
						from	wl_item
						where	nr_sequencia = nr_seq_item
						and		ie_situacao = 'A');

if (ie_regra_worklist_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(1026121);
end if;

end;
/


ALTER TABLE TASY.WL_REGRA_WORKLIST ADD (
  CONSTRAINT WLREWO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.WL_REGRA_WORKLIST ADD (
  CONSTRAINT WLREWO_WLITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.WL_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.WL_REGRA_WORKLIST TO NIVEL_1;

