ALTER TABLE TASY.BANCO_ESCRIT_BARRAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_ESCRIT_BARRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_ESCRIT_BARRAS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_BANCO_ESCRIT  NUMBER(10),
  CD_BARRAS            VARCHAR2(255 BYTE)       NOT NULL,
  NR_TITULO            NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_CGC               VARCHAR2(14 BYTE),
  NR_SEQ_LOTE          NUMBER(10),
  CD_PESSOA_EXTERNO    VARCHAR2(60 BYTE),
  NM_PESSOA_EXTERNO    VARCHAR2(255 BYTE),
  DT_EMISSAO           DATE,
  NR_PAGAMENTO         NUMBER(20),
  CD_LANCAMENTO        VARCHAR2(60 BYTE),
  DT_LIMITE_DESCONTO   DATE,
  DS_SACADOR_AVALISTA  VARCHAR2(255 BYTE),
  NR_DOCUMENTO         VARCHAR2(60 BYTE),
  NR_NOSSO_NUMERO      VARCHAR2(20 BYTE),
  VL_DESCONTO          NUMBER(15,2),
  VL_ACRESCIMO         NUMBER(15,2),
  IE_SELECIONADO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BAESBAR_BANESCR_FK_I ON TASY.BANCO_ESCRIT_BARRAS
(NR_SEQ_BANCO_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAESBAR_BANESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BAESBAR_BCOESLO_FK_I ON TASY.BANCO_ESCRIT_BARRAS
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAESBAR_BCOESLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BAESBAR_PESFISI_FK_I ON TASY.BANCO_ESCRIT_BARRAS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BAESBAR_PESJURI_FK_I ON TASY.BANCO_ESCRIT_BARRAS
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAESBAR_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.BAESBAR_PK ON TASY.BANCO_ESCRIT_BARRAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAESBAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.BAESBAR_TITPAGA_FK_I ON TASY.BANCO_ESCRIT_BARRAS
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAESBAR_TITPAGA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.BANCO_ESCRIT_BARRAS ADD (
  CONSTRAINT BAESBAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_ESCRIT_BARRAS ADD (
  CONSTRAINT BAESBAR_BANESCR_FK 
 FOREIGN KEY (NR_SEQ_BANCO_ESCRIT) 
 REFERENCES TASY.BANCO_ESCRITURAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT BAESBAR_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT BAESBAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT BAESBAR_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT BAESBAR_BCOESLO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.BANCO_ESCRIT_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.BANCO_ESCRIT_BARRAS TO NIVEL_1;

