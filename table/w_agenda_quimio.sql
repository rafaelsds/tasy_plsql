DROP TABLE TASY.W_AGENDA_QUIMIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_AGENDA_QUIMIO
(
  NR_SEQUENCIA         NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_HORARIO           DATE,
  IE_STATUS            VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL         NUMBER(10),
  NR_SEQ_PEND_AGENDA   NUMBER(10),
  NR_SEQ_AGEQUI        NUMBER(10),
  NR_SEQ_PACIENTE      NUMBER(10),
  NR_SEQ_AGEINT_ITEM   NUMBER(10),
  NR_SEQ_ITEM_MARC     NUMBER(10),
  IE_STATUS_AGENDA     VARCHAR2(15 BYTE),
  CD_TURNO             VARCHAR2(1 BYTE),
  IE_RESERVA           VARCHAR2(1 BYTE),
  NR_SEQ_ATENDIMENTO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WAGEQUI_AGEQUIM_FK_I ON TASY.W_AGENDA_QUIMIO
(NR_SEQ_AGEQUI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WAGEQUI_AGEQUIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WAGEQUI_I1 ON TASY.W_AGENDA_QUIMIO
(NR_SEQ_LOCAL, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WAGEQUI_I1
  MONITORING USAGE;


CREATE INDEX TASY.WAGEQUI_I2 ON TASY.W_AGENDA_QUIMIO
(NM_USUARIO, NR_SEQ_LOCAL, NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WAGEQUI_I3 ON TASY.W_AGENDA_QUIMIO
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WAGEQUI_I3
  MONITORING USAGE;


CREATE INDEX TASY.WAGEQUI_I4 ON TASY.W_AGENDA_QUIMIO
(NR_SEQ_LOCAL, DT_HORARIO, IE_STATUS)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WAGEQUI_I5 ON TASY.W_AGENDA_QUIMIO
(TRUNC("DT_HORARIO"), IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WAGEQUI_QTLOCAL_FK_I ON TASY.W_AGENDA_QUIMIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WAGEQUI_QTLOCAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WAGEQUI_QTPENAG_FK_I ON TASY.W_AGENDA_QUIMIO
(NR_SEQ_PEND_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WAGEQUI_QTPENAG_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.w_agenda_quimio_atual
before insert or update ON TASY.W_AGENDA_QUIMIO for each row
declare

hr_quebra_turno_w		varchar2(05);
hr_quebra_turno_not_w		varchar2(05);
cd_turno_w			number(10,0) := 0;

begin

/*select	max(nvl(hr_quebra_turno, '12')),
	max(hr_quebra_noturno)
into	hr_quebra_turno_w,
	hr_quebra_turno_not_w
from	qt_local
where	nr_sequencia = :new.nr_seq_local;

:new.cd_turno := 0;

if	((to_number(to_char(:new.dt_horario, 'hh24')) > somente_numero(hr_quebra_turno_w)) or
	(to_number(to_char(:new.dt_horario, 'hh24')) = somente_numero(hr_quebra_turno_w))) then
	cd_turno_w	:= 1;
else
	cd_turno_w	:= 0;
end if;

if	(hr_quebra_turno_not_w is not null) and
	(to_number(to_char(:new.dt_horario, 'hh24')) >= somente_numero(hr_quebra_turno_not_w)) then
	cd_turno_w	:= 3;
end if;

if	(:new.cd_turno	<> cd_turno_w) then
	:new.cd_turno	:= cd_turno_w;
end if;*/

cd_turno_w	:= 0;

end;
/


ALTER TABLE TASY.W_AGENDA_QUIMIO ADD (
  CONSTRAINT WAGEQUI_AGEQUIM_FK 
 FOREIGN KEY (NR_SEQ_AGEQUI) 
 REFERENCES TASY.AGENDA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT WAGEQUI_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT WAGEQUI_QTPENAG_FK 
 FOREIGN KEY (NR_SEQ_PEND_AGENDA) 
 REFERENCES TASY.QT_PENDENCIA_AGENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_AGENDA_QUIMIO TO NIVEL_1;

