ALTER TABLE TASY.BANCO_INSTRUCAO_BOLETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_INSTRUCAO_BOLETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_INSTRUCAO_BOLETO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_BANCO               NUMBER(3)              NOT NULL,
  NR_LINHA               NUMBER(5)              NOT NULL,
  DS_INSTRUCAO           VARCHAR2(255 BYTE)     NOT NULL,
  DS_REGRA               VARCHAR2(255 BYTE),
  DS_VALOR_REGRA         VARCHAR2(50 BYTE),
  IE_CONDICAO            VARCHAR2(1 BYTE),
  IE_POSICAO             VARCHAR2(1 BYTE),
  IE_APLICACAO_MENSAGEM  VARCHAR2(15 BYTE),
  QT_TAMANHO             NUMBER(10),
  NR_SEQ_GRUPO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANINBO_BANCO_FK_I ON TASY.BANCO_INSTRUCAO_BOLETO
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BANINBO_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BANINBO_BCGPINST_FK_I ON TASY.BANCO_INSTRUCAO_BOLETO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BANINBO_ESTABEL_FK_I ON TASY.BANCO_INSTRUCAO_BOLETO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BANINBO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.BANINBO_PK ON TASY.BANCO_INSTRUCAO_BOLETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BANINBO_PK
  MONITORING USAGE;


ALTER TABLE TASY.BANCO_INSTRUCAO_BOLETO ADD (
  CONSTRAINT BANINBO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_INSTRUCAO_BOLETO ADD (
  CONSTRAINT BANINBO_BCGPINST_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.BANCO_GRUPO_INSTRUCAO (NR_SEQUENCIA),
  CONSTRAINT BANINBO_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT BANINBO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.BANCO_INSTRUCAO_BOLETO TO NIVEL_1;

