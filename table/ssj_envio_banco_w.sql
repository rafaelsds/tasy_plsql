DROP TABLE TASY.SSJ_ENVIO_BANCO_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.SSJ_ENVIO_BANCO_W
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_CONTEUDO          VARCHAR2(4000 BYTE)      NOT NULL,
  NR_SEQ_APRES         NUMBER(10),
  NR_SEQ_APRES_2       NUMBER(10),
  NR_TITULO            NUMBER(10),
  IE_TIPO_REGISTRO     VARCHAR2(1 BYTE),
  NR_SEQ_LOTE          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.SSJ_ENVIO_BANCO_W TO NIVEL_1;

