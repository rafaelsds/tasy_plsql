ALTER TABLE TASY.W_TABELA_ATRIB_PARAM_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_TABELA_ATRIB_PARAM_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TABELA_ATRIB_PARAM_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TABELA        NUMBER(10)               NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  VL_ATRIBUTO          VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WTABATCO_PK ON TASY.W_TABELA_ATRIB_PARAM_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WTABATCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.WTABATCO_WTPARACO_FK_I ON TASY.W_TABELA_ATRIB_PARAM_CONV
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WTABATCO_WTPARACO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_TABELA_ATRIB_PARAM_CONV ADD (
  CONSTRAINT WTABATCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_TABELA_ATRIB_PARAM_CONV ADD (
  CONSTRAINT WTABATCO_WTPARACO_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.W_TABELA_PARAM_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_TABELA_ATRIB_PARAM_CONV TO NIVEL_1;

