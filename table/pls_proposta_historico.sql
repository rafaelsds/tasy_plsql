ALTER TABLE TASY.PLS_PROPOSTA_HISTORICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROPOSTA_HISTORICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROPOSTA_HISTORICO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PROPOSTA         NUMBER(10)            NOT NULL,
  DT_HISTORICO            DATE                  NOT NULL,
  DS_HISTORICO            VARCHAR2(4000 BYTE)   NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_ORIGEM_HISTORICO     NUMBER(3)             NOT NULL,
  DT_LIBERACAO_HISTORICO  DATE,
  NM_USUARIO_LIBERACAO    VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_HIST        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRHI_PK ON TASY.PLS_PROPOSTA_HISTORICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRHI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRHI_PLSPRAD_FK_I ON TASY.PLS_PROPOSTA_HISTORICO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRHI_PLTHIPRO_FK_I ON TASY.PLS_PROPOSTA_HISTORICO
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SSJ_INSERIR_HIST_ANALISE2
AFTER INSERT
ON TASY.PLS_PROPOSTA_HISTORICO 

FOR EACH ROW
DECLARE

nr_seq_proposta_w number(10);
nr_seq_analise_w number(10);
qt_analise_w number(10);
nm_usuario_w varchar2(255);
ds_historico_w varchar2(4000);

cursor c_analise is
    select nr_sequencia nr_seq_analise
    from PLS_ANALISE_ADESAO
    where nr_seq_proposta = nr_seq_proposta_w;
v_analise c_analise%rowtype;


BEGIN

    nr_seq_proposta_w:= :new.nr_seq_proposta;
    nm_usuario_w:= :new.nm_usuario;
    ds_historico_w:= :new.ds_historico;
   
    
    select count(nr_sequencia)
    into qt_analise_w
    from PLS_ANALISE_ADESAO
    where nr_seq_proposta = nr_seq_proposta_w;
    

    if(qt_analise_w >0)then
    
        open c_analise;
            loop
            fetch c_analise into v_analise;
            exit when c_analise%notfound;
            
            insert into PLS_ANALISE_HISTORICO
            (
                nr_sequencia,
                nr_seq_analise,
                cd_estabelecimento,
                dt_historico,
                nm_usuario_historico,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                ds_titulo,
                ds_historico
            )
            values
            (
                PLS_ANALISE_HISTORICO_SEQ.nextval,  --nr_sequencia,
                v_analise.nr_seq_analise,           --nr_seq_analise,
                2,                                  --cd_estabelecimento,
                sysdate,                            --dt_historico,
                nm_usuario_w,                       --nm_usuario_historico,
                sysdate,                            --dt_atualizacao,
                nm_usuario_w,                       --nm_usuario,
                sysdate,                            --dt_atualizacao_nrec,
                nm_usuario_w,                       --nm_usuario_nrec,
                'Histórico da Proposta',            --ds_titulo,
                ds_historico_w                      --ds_historico,
            );
            
            end loop;
        close c_analise;
        
    end if;
 
END SSJ_INSERIR_HIST_ANALISE2;
/


ALTER TABLE TASY.PLS_PROPOSTA_HISTORICO ADD (
  CONSTRAINT PLSPRHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROPOSTA_HISTORICO ADD (
  CONSTRAINT PLSPRHI_PLTHIPRO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.PLS_TIPO_HIST_PROP (NR_SEQUENCIA),
  CONSTRAINT PLSPRHI_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROPOSTA_HISTORICO TO NIVEL_1;

