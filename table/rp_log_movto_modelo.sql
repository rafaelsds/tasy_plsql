ALTER TABLE TASY.RP_LOG_MOVTO_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RP_LOG_MOVTO_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RP_LOG_MOVTO_MODELO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_LOG               DATE,
  QT_VAGAS_RESTANTES   NUMBER(5),
  QT_LISTA_ESPERA      NUMBER(5),
  NR_SEQ_MODELO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RPLOGMOD_PK ON TASY.RP_LOG_MOVTO_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLOGMOD_PK
  MONITORING USAGE;


CREATE INDEX TASY.RPLOGMOD_RPMODAG_FK_I ON TASY.RP_LOG_MOVTO_MODELO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLOGMOD_RPMODAG_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RP_LOG_MOVTO_MODELO ADD (
  CONSTRAINT RPLOGMOD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RP_LOG_MOVTO_MODELO ADD (
  CONSTRAINT RPLOGMOD_RPMODAG_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.RP_MODELO_AGENDAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RP_LOG_MOVTO_MODELO TO NIVEL_1;

