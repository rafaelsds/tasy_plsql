ALTER TABLE TASY.GQA_PEND_JUSTIFICATIVA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GQA_PEND_JUSTIFICATIVA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GQA_PEND_JUSTIFICATIVA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_CONTINUAR_PROTOCOLO  VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DS_MSG_ULT_EXECUTOR     VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.GQAJUS_PK ON TASY.GQA_PEND_JUSTIFICATIVA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GQA_PEND_JUSTIFICATIVA ADD (
  CONSTRAINT GQAJUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.GQA_PEND_JUSTIFICATIVA TO NIVEL_1;

