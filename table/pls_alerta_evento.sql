ALTER TABLE TASY.PLS_ALERTA_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ALERTA_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ALERTA_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ALERTA        NUMBER(10)               NOT NULL,
  IE_EVENTO            VARCHAR2(5 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSALEE_PK ON TASY.PLS_ALERTA_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSALEE_PLSALER_FK_I ON TASY.PLS_ALERTA_EVENTO
(NR_SEQ_ALERTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ALERTA_EVENTO ADD (
  CONSTRAINT PLSALEE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ALERTA_EVENTO ADD (
  CONSTRAINT PLSALEE_PLSALER_FK 
 FOREIGN KEY (NR_SEQ_ALERTA) 
 REFERENCES TASY.PLS_ALERTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_ALERTA_EVENTO TO NIVEL_1;

