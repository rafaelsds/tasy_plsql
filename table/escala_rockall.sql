ALTER TABLE TASY.ESCALA_ROCKALL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ROCKALL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ROCKALL
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  DT_AVALIACAO             DATE                 NOT NULL,
  DT_LIBERACAO             DATE,
  QT_IDADE                 NUMBER(3)            NOT NULL,
  QT_PONTOS_IDADE          NUMBER(1)            NOT NULL,
  QT_PA_SISTOLICA          NUMBER(3)            NOT NULL,
  QT_FREQ_CARDIACA         NUMBER(3)            NOT NULL,
  QT_PONTOS_ESTADO         NUMBER(1)            NOT NULL,
  IE_DOENCAS_ASSOCIADAS    NUMBER(1)            NOT NULL,
  QT_PONTOS_DOENCAS_ASSOC  NUMBER(1)            NOT NULL,
  IE_DIAGNOSTICO           NUMBER(1)            NOT NULL,
  QT_PONTOS_DIAGNOSTICO    NUMBER(1)            NOT NULL,
  IE_SINAIS_HEMORRAGIA     NUMBER(1)            NOT NULL,
  QT_PONTOS_SINAIS         NUMBER(1)            NOT NULL,
  QT_ROCKALL               NUMBER(2)            NOT NULL,
  CD_PERFIL_ATIVO          NUMBER(5),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  NR_HORA                  NUMBER(2),
  IE_MOBILIDADE            NUMBER(1),
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCROCK_ATEPACI_FK_I ON TASY.ESCALA_ROCKALL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCROCK_I1 ON TASY.ESCALA_ROCKALL
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCROCK_PERFIL_FK_I ON TASY.ESCALA_ROCKALL
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCROCK_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCROCK_PESFISI_FK_I ON TASY.ESCALA_ROCKALL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCROCK_PK ON TASY.ESCALA_ROCKALL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCROCK_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_rockall_atual
before insert or update ON TASY.ESCALA_ROCKALL for each row
declare
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.qt_idade < 60) then
	:new.qt_pontos_idade := 0;
elsif	(:new.qt_idade > 80) then
	:new.qt_pontos_idade := 2;
else
	:new.qt_pontos_idade := 1;
end if;

if	(:new.qt_pa_sistolica >= 100) and (:new.qt_freq_cardiaca < 100) then
	:new.qt_pontos_estado := 0;
elsif	(:new.qt_pa_sistolica >= 100) and (:new.qt_freq_cardiaca >= 100) then
	:new.qt_pontos_estado := 1;
else
	:new.qt_pontos_estado := 2;
end if;

:new.qt_pontos_doencas_assoc	:=	nvl(:new.ie_doencas_associadas,0);
:new.qt_pontos_diagnostico		:=	nvl(:new.ie_diagnostico,0);
:new.qt_pontos_sinais			:=	nvl(:new.ie_sinais_hemorragia,0);
:new.qt_rockall					:=	:new.qt_pontos_idade +
									:new.qt_pontos_estado +
									:new.qt_pontos_doencas_assoc +
									:new.qt_pontos_diagnostico +
									:new.qt_pontos_sinais;
<<Final>>
qt_reg_w	:= 0;
end Escala_rockall_atual;
/


ALTER TABLE TASY.ESCALA_ROCKALL ADD (
  CONSTRAINT ESCROCK_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ROCKALL ADD (
  CONSTRAINT ESCROCK_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCROCK_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCROCK_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_ROCKALL TO NIVEL_1;

