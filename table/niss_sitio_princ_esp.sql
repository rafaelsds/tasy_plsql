ALTER TABLE TASY.NISS_SITIO_PRINC_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NISS_SITIO_PRINC_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.NISS_SITIO_PRINC_ESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PRINC         NUMBER(10)               NOT NULL,
  NR_SEQ_ESPEC         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NISIPRE_NISSIES_FK_I ON TASY.NISS_SITIO_PRINC_ESP
(NR_SEQ_ESPEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NISIPRE_NISSIES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NISIPRE_NISSIPR_FK_I ON TASY.NISS_SITIO_PRINC_ESP
(NR_SEQ_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NISIPRE_PK ON TASY.NISS_SITIO_PRINC_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NISIPRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.NISS_SITIO_PRINC_ESP ADD (
  CONSTRAINT NISIPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NISS_SITIO_PRINC_ESP ADD (
  CONSTRAINT NISIPRE_NISSIES_FK 
 FOREIGN KEY (NR_SEQ_ESPEC) 
 REFERENCES TASY.NISS_SITIO_ESPECIFICO (NR_SEQUENCIA),
  CONSTRAINT NISIPRE_NISSIPR_FK 
 FOREIGN KEY (NR_SEQ_PRINC) 
 REFERENCES TASY.NISS_SITIO_PRINCIPAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.NISS_SITIO_PRINC_ESP TO NIVEL_1;

