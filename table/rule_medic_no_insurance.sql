ALTER TABLE TASY.RULE_MEDIC_NO_INSURANCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RULE_MEDIC_NO_INSURANCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.RULE_MEDIC_NO_INSURANCE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RUMENOIN_MATERIA_FK_I ON TASY.RULE_MEDIC_NO_INSURANCE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RUMENOIN_PK ON TASY.RULE_MEDIC_NO_INSURANCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RULE_MEDIC_NO_INSURANCE ADD (
  CONSTRAINT RUMENOIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RULE_MEDIC_NO_INSURANCE ADD (
  CONSTRAINT RUMENOIN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

