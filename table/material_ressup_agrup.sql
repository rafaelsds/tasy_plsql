ALTER TABLE TASY.MATERIAL_RESSUP_AGRUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_RESSUP_AGRUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_RESSUP_AGRUP
(
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_MATERIAL_GENERICO  NUMBER(10)              NOT NULL,
  CD_MATERIAL           NUMBER(10)              NOT NULL,
  QT_COMPRAR            NUMBER(15,4)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MARESAG_ESTABEL_FK_I ON TASY.MATERIAL_RESSUP_AGRUP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MARESAG_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MARESAG_MATERIA_FK_I ON TASY.MATERIAL_RESSUP_AGRUP
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MARESAG_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MARESAG_MATERIA_FK2_I ON TASY.MATERIAL_RESSUP_AGRUP
(CD_MATERIAL_GENERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MARESAG_MATERIA_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MARESAG_PK ON TASY.MATERIAL_RESSUP_AGRUP
(CD_ESTABELECIMENTO, CD_MATERIAL_GENERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MARESAG_PK
  MONITORING USAGE;


ALTER TABLE TASY.MATERIAL_RESSUP_AGRUP ADD (
  CONSTRAINT MARESAG_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_MATERIAL_GENERICO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_RESSUP_AGRUP ADD (
  CONSTRAINT MARESAG_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MARESAG_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_GENERICO) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MARESAG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MATERIAL_RESSUP_AGRUP TO NIVEL_1;

