ALTER TABLE TASY.PROJETO_RECURSO_FIN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJETO_RECURSO_FIN CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJETO_RECURSO_FIN
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROJ_REC      NUMBER(10)               NOT NULL,
  CD_CONTA_FINANC      NUMBER(10)               NOT NULL,
  DT_REFERENCIA        DATE                     NOT NULL,
  VL_MOVIMENTO         NUMBER(15,2)             NOT NULL,
  IE_DEB_CRED          VARCHAR2(1 BYTE)         NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRREFIN_CONFINA_FK_I ON TASY.PROJETO_RECURSO_FIN
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREFIN_CONFINA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRREFIN_PK ON TASY.PROJETO_RECURSO_FIN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREFIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRREFIN_PRORECU_FK_I ON TASY.PROJETO_RECURSO_FIN
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREFIN_PRORECU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.projeto_recurso_fin_after
after insert or update ON TASY.PROJETO_RECURSO_FIN for each row
begin

if (inserting) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_proj_rec,null,'PR',:new.dt_referencia,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_proj_rec,null,'PR',:new.dt_referencia,'A',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.projeto_recurso_fin_delete
before delete ON TASY.PROJETO_RECURSO_FIN for each row
begin

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_proj_rec,null,'PR',:old.dt_referencia,'E',:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.projeto_recurso_fin_bf_ins_upd
before insert or update ON TASY.PROJETO_RECURSO_FIN for each row
begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


ALTER TABLE TASY.PROJETO_RECURSO_FIN ADD (
  CONSTRAINT PRREFIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJETO_RECURSO_FIN ADD (
  CONSTRAINT PRREFIN_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT PRREFIN_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROJETO_RECURSO_FIN TO NIVEL_1;

