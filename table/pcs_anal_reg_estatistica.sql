ALTER TABLE TASY.PCS_ANAL_REG_ESTATISTICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_ANAL_REG_ESTATISTICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_ANAL_REG_ESTATISTICA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  PR_CONFIANCA_T       VARCHAR2(5 BYTE),
  CD_TIPO_ANALISE      VARCHAR2(5 BYTE),
  NR_SEQ_REGRA         NUMBER(10),
  PR_CONFIANCA         VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCSANALRE_PCSREGE_FK_I ON TASY.PCS_ANAL_REG_ESTATISTICA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCSANALRE_PK ON TASY.PCS_ANAL_REG_ESTATISTICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PCS_ANAL_REG_ESTATISTICA ADD (
  CONSTRAINT PCSANALRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PCS_ANAL_REG_ESTATISTICA ADD (
  CONSTRAINT PCSANALRE_PCSREGE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PCS_REGRA_ESTATISTICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PCS_ANAL_REG_ESTATISTICA TO NIVEL_1;

