ALTER TABLE TASY.PAINEL_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAINEL_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAINEL_USUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PAINEL        NUMBER(10),
  CD_PERFIL            NUMBER(5),
  NM_USUARIO_REGRA     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAIUSUAR_PAIINDIC_FK_I ON TASY.PAINEL_USUARIO
(NR_SEQ_PAINEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAIUSUAR_PAIINDIC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PAIUSUAR_PERFIL_FK_I ON TASY.PAINEL_USUARIO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAIUSUAR_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PAIUSUAR_PK ON TASY.PAINEL_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PAINEL_USUARIO ADD (
  CONSTRAINT PAIUSUAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAINEL_USUARIO ADD (
  CONSTRAINT PAIUSUAR_PAIINDIC_FK 
 FOREIGN KEY (NR_SEQ_PAINEL) 
 REFERENCES TASY.PAINEL_INDICADORES (NR_SEQUENCIA),
  CONSTRAINT PAIUSUAR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.PAINEL_USUARIO TO NIVEL_1;

