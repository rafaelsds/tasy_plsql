ALTER TABLE TASY.PLS_CONTA_OBSERVACAO_A500
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_OBSERVACAO_A500 CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_OBSERVACAO_A500
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(100 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA         NUMBER(10),
  NR_SEQ_CONTA_PROC    NUMBER(10),
  NR_SEQ_CONTA_MAT     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCTAOBS_PK ON TASY.PLS_CONTA_OBSERVACAO_A500
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAOBS_PLSCOMAT_FK_I ON TASY.PLS_CONTA_OBSERVACAO_A500
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAOBS_PLSCOME_FK_I ON TASY.PLS_CONTA_OBSERVACAO_A500
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAOBS_PLSCOPRO_FK_I ON TASY.PLS_CONTA_OBSERVACAO_A500
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conta_obs_a500_atual
before update or delete ON TASY.PLS_CONTA_OBSERVACAO_A500 for each row
declare

begin
-- verifica se deve executar essa trigger, a mesma apresenta problema de trigger mutante quando executada
-- no processo de excluir protocolos do portal
if	(pls_util_pck.ie_exec_trig_conta_obs_w = 'S') then

	if	(deleting) then

		insert into pls_conta_log (
			nr_sequencia,dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
			nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
			dt_alteracao, ds_alteracao
		) values (
			pls_conta_log_seq.nextval, sysdate, :old.nm_usuario,
			sysdate, :new.nm_usuario, :old.nr_seq_conta,
			:old.nr_seq_conta_proc, null, :old.nm_usuario,
			sysdate, 'Exclus�o - ds_observacao anterior : '||:old.ds_observacao||' ds_observacao nova :  ' || pls_util_pck.enter_w||' '||dbms_utility.format_call_stack
		);
	end if;

	if	(updating) then

		insert into pls_conta_log (
			nr_sequencia,dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta,
			nr_seq_conta_proc, nr_seq_conta_mat, nm_usuario_alteracao,
			dt_alteracao, ds_alteracao
		) values (
			pls_conta_log_seq.nextval, sysdate, :new.nm_usuario,
			sysdate, :new.nm_usuario, :new.nr_seq_conta,
			:new.nr_seq_conta_proc, null, :new.nm_usuario,
			sysdate, 'Modifica��o - ds_observacao anterior : '||:old.ds_observacao||' ds_observacao nova:  '||:new.ds_observacao || pls_util_pck.enter_w ||' '||dbms_utility.format_call_stack
		);
	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_CONTA_OBSERVACAO_A500 ADD (
  CONSTRAINT PLSCTAOBS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_OBSERVACAO_A500 ADD (
  CONSTRAINT PLSCTAOBS_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCTAOBS_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCTAOBS_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CONTA_OBSERVACAO_A500 TO NIVEL_1;

