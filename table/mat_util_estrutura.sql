ALTER TABLE TASY.MAT_UTIL_ESTRUTURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_UTIL_ESTRUTURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_UTIL_ESTRUTURA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO            NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE                   NOT NULL,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)      NOT NULL,
  CD_EMPRESA             NUMBER(4),
  CD_ESTABELECIMENTO     NUMBER(4),
  CD_PERFIL              NUMBER(15),
  IE_PERMITE             VARCHAR2(1 BYTE)       NOT NULL,
  IE_PERMITE_VISUALIZAR  VARCHAR2(1 BYTE)       NOT NULL,
  NM_USUARIO_LIB         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATUTES_EMPRESA_FK_I ON TASY.MAT_UTIL_ESTRUTURA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATUTES_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATUTES_ESTABEL_FK_I ON TASY.MAT_UTIL_ESTRUTURA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATUTES_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATUTES_MATTIES_FK_I ON TASY.MAT_UTIL_ESTRUTURA
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATUTES_PERFIL_FK_I ON TASY.MAT_UTIL_ESTRUTURA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATUTES_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATUTES_PK ON TASY.MAT_UTIL_ESTRUTURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAT_UTIL_ESTRUTURA ADD (
  CONSTRAINT MATUTES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAT_UTIL_ESTRUTURA ADD (
  CONSTRAINT MATUTES_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT MATUTES_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT MATUTES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATUTES_MATTIES_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MAT_TIPO_ESTRUTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAT_UTIL_ESTRUTURA TO NIVEL_1;

