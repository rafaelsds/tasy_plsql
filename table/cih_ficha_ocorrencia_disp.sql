ALTER TABLE TASY.CIH_FICHA_OCORRENCIA_DISP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_FICHA_OCORRENCIA_DISP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_FICHA_OCORRENCIA_DISP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL_INFECCAO  NUMBER(10)             NOT NULL,
  NR_SEQ_ATEND_DISP      NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHFDIS_ATEPADI_FK_I ON TASY.CIH_FICHA_OCORRENCIA_DISP
(NR_SEQ_ATEND_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHFDIS_CIHLOIN_FK_I ON TASY.CIH_FICHA_OCORRENCIA_DISP
(NR_SEQ_LOCAL_INFECCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHFDIS_CIHLOIN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CIHFDIS_PK ON TASY.CIH_FICHA_OCORRENCIA_DISP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHFDIS_PK
  MONITORING USAGE;


ALTER TABLE TASY.CIH_FICHA_OCORRENCIA_DISP ADD (
  CONSTRAINT CIHFDIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_FICHA_OCORRENCIA_DISP ADD (
  CONSTRAINT CIHFDIS_CIHLOIN_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_INFECCAO) 
 REFERENCES TASY.CIH_LOCAL_INFECCAO (NR_SEQUENCIA),
  CONSTRAINT CIHFDIS_ATEPADI_FK 
 FOREIGN KEY (NR_SEQ_ATEND_DISP) 
 REFERENCES TASY.ATEND_PAC_DISPOSITIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIH_FICHA_OCORRENCIA_DISP TO NIVEL_1;

