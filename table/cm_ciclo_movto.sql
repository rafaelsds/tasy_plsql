ALTER TABLE TASY.CM_CICLO_MOVTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CICLO_MOVTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CICLO_MOVTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CICLO         NUMBER(10),
  DT_MOVIMENTO         DATE                     NOT NULL,
  IE_TIPO_MOVTO        VARCHAR2(15 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NR_SEQ_CICLO_LAV     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCIMOV_CMCICLO_FK_I ON TASY.CM_CICLO_MOVTO
(NR_SEQ_CICLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCIMOV_CMCICLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCIMOV_CMCILAV_FK_I ON TASY.CM_CICLO_MOVTO
(NR_SEQ_CICLO_LAV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCIMOV_CMCILAV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCIMOV_PK ON TASY.CM_CICLO_MOVTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCIMOV_PK
  MONITORING USAGE;


ALTER TABLE TASY.CM_CICLO_MOVTO ADD (
  CONSTRAINT CMCIMOV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CICLO_MOVTO ADD (
  CONSTRAINT CMCIMOV_CMCICLO_FK 
 FOREIGN KEY (NR_SEQ_CICLO) 
 REFERENCES TASY.CM_CICLO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCIMOV_CMCILAV_FK 
 FOREIGN KEY (NR_SEQ_CICLO_LAV) 
 REFERENCES TASY.CM_CICLO_LAVACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_CICLO_MOVTO TO NIVEL_1;

