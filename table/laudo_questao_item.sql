ALTER TABLE TASY.LAUDO_QUESTAO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_QUESTAO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_QUESTAO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LAUDO_GRUPO   NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM_QUESTAO  NUMBER(10)               NOT NULL,
  DT_LIBERACAO         DATE,
  DS_TEXTO             LONG
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAQUITE_ITQULAU_FK_I ON TASY.LAUDO_QUESTAO_ITEM
(NR_SEQ_ITEM_QUESTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAQUITE_ITQULAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAQUITE_LAGRQUE_FK_I ON TASY.LAUDO_QUESTAO_ITEM
(NR_SEQ_LAUDO_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAQUITE_LAGRQUE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LAQUITE_PK ON TASY.LAUDO_QUESTAO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAQUITE_PK
  MONITORING USAGE;


ALTER TABLE TASY.LAUDO_QUESTAO_ITEM ADD (
  CONSTRAINT LAQUITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_QUESTAO_ITEM ADD (
  CONSTRAINT LAQUITE_LAGRQUE_FK 
 FOREIGN KEY (NR_SEQ_LAUDO_GRUPO) 
 REFERENCES TASY.LAUDO_GRUPO_QUESTAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LAQUITE_ITQULAU_FK 
 FOREIGN KEY (NR_SEQ_ITEM_QUESTAO) 
 REFERENCES TASY.ITEM_QUESTAO_LAUDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAUDO_QUESTAO_ITEM TO NIVEL_1;

