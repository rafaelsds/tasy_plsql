ALTER TABLE TASY.ORDEM_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA
(
  NR_ORDEM_COMPRA            NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4),
  CD_CGC_FORNECEDOR          VARCHAR2(14 BYTE),
  CD_CONDICAO_PAGAMENTO      NUMBER(10)         NOT NULL,
  CD_COMPRADOR               VARCHAR2(10 BYTE)  NOT NULL,
  DT_ORDEM_COMPRA            DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_MOEDA                   NUMBER(5)          NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INCLUSAO                DATE               NOT NULL,
  CD_PESSOA_SOLICITANTE      VARCHAR2(10 BYTE)  NOT NULL,
  CD_CGC_TRANSPORTADOR       VARCHAR2(14 BYTE),
  IE_FRETE                   VARCHAR2(1 BYTE)   NOT NULL,
  VL_FRETE                   NUMBER(15,2),
  PR_MULTA_ATRASO            NUMBER(13,4),
  PR_DESCONTO                NUMBER(13,4),
  PR_DESC_PGTO_ANTEC         NUMBER(13,4),
  PR_JUROS_NEGOCIADO         NUMBER(13,4),
  DT_EMISSAO                 DATE,
  DS_PESSOA_CONTATO          VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  CD_MOTIVO_ALTERACAO        NUMBER(5),
  CD_LOCAL_ENTREGA           NUMBER(4),
  DT_ENTREGA                 DATE               NOT NULL,
  NR_PRESCRICAO              NUMBER(14),
  DT_APROVACAO               DATE,
  DT_BAIXA                   DATE,
  DT_LIBERACAO               DATE,
  IE_AVISO_CHEGADA           VARCHAR2(1 BYTE)   NOT NULL,
  NR_REQUISICAO              NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  NR_ORDEM_AGRUP             NUMBER(10),
  IE_EMITE_OBS               VARCHAR2(1 BYTE)   NOT NULL,
  IE_URGENTE                 VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_INTERNO             NUMBER(10),
  NR_ATENDIMENTO             NUMBER(10),
  NM_USUARIO_IMP             VARCHAR2(15 BYTE),
  IE_SOMENTE_PAGTO           VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_CANCEL       NUMBER(10),
  VL_DESPESA_ACESSORIA       NUMBER(13,2),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_SUBGRUPO_COMPRA     NUMBER(10),
  PR_DESC_FINANCEIRO         NUMBER(13,4),
  DT_LEITURA                 DATE,
  DT_ACEITE                  DATE,
  NM_USUARIO_ACEITE          VARCHAR2(16 BYTE),
  NR_SEQ_FORMA_PAGTO         NUMBER(10),
  NR_DOCUMENTO_EXTERNO       VARCHAR2(40 BYTE),
  DT_FATURAMENTO             DATE,
  VL_DESCONTO                NUMBER(13,2),
  CD_CONVENIO                NUMBER(5),
  CD_ESTAB_TRANSF            NUMBER(4),
  IE_TIPO_ORDEM              VARCHAR2(1 BYTE),
  NR_SEQ_RESSUP_FORNEC       NUMBER(10),
  IE_FORMA_EXPORTAR          VARCHAR2(15 BYTE),
  DT_ENVIO_EMAIL             DATE,
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_REG_LIC_COMPRA      NUMBER(10),
  IE_LIBERACAO_PORTAL        VARCHAR2(1 BYTE),
  NR_SEQ_NF_REPASSE          NUMBER(10),
  NR_SEQ_LICITACAO           NUMBER(10),
  NR_SEQ_TIPO_COMPRA         NUMBER(10),
  NR_DOCUMENTO_INTERNO       VARCHAR2(100 BYTE),
  NR_PROCESSO_INTERNO        VARCHAR2(100 BYTE),
  NR_SEQ_MOD_COMPRA          NUMBER(10),
  NR_SEQ_AGENDA_PAC          NUMBER(10),
  NR_CONTROLE_EDITAL         VARCHAR2(100 BYTE),
  DS_DADOS_IMPORTACAO        VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_URGENTE      NUMBER(10),
  CD_CENTRO_CUSTO            NUMBER(8),
  IE_ORIGEM_IMP              VARCHAR2(80 BYTE),
  DT_ENVIO_INTEGRACAO_OPME   DATE,
  DT_ESTORNO_LIB             DATE,
  NM_USUARIO_ESTORNO_LIB     VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_ESTORNO_LIB  NUMBER(10),
  DT_CONFIRMA_ENTREGA        DATE,
  NM_USUARIO_CONF_ENTR       VARCHAR2(15 BYTE),
  CD_LOCAL_TRANSF            NUMBER(4),
  NR_SEQ_PROTOCOLO           NUMBER(10),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  NR_ORDEM_ORIG              NUMBER(10),
  NR_SEQ_MOTIVO_BAIXA        NUMBER(10),
  VL_DESPESA_DOC             NUMBER(13,2),
  NR_SEQ_VIA_FATURA          NUMBER(10),
  NR_SEQ_LIC                 NUMBER(10),
  NR_DECLARACAO_IMP          VARCHAR2(255 BYTE),
  IE_SISTEMA_ORIGEM          VARCHAR2(15 BYTE),
  IE_NECESSITA_ENVIAR_INT    VARCHAR2(1 BYTE),
  NM_USUARIO_ALTERA_INT      VARCHAR2(15 BYTE),
  DS_OBS_INTERNA             VARCHAR2(4000 BYTE),
  NR_SOLIC_COMPRA_GERADA     NUMBER(10),
  NR_SEQ_ATEND_OPME          NUMBER(10),
  NR_SEQ_CONTRATO            NUMBER(10),
  NR_SEQ_MOTIVO_SOLIC        NUMBER(10),
  VL_SEGURO                  NUMBER(13,2),
  CD_PERFIL                  NUMBER(5),
  DS_EMAIL_REGRA_PJ          VARCHAR2(4000 BYTE),
  DS_CONTATO_REGRA_PJ        VARCHAR2(4000 BYTE),
  IE_ORCADO                  VARCHAR2(1 BYTE),
  DT_VALIDADE                DATE,
  NM_USUARIO_APROV           VARCHAR2(15 BYTE),
  VL_CONV_MOEDA              NUMBER(13,4),
  NR_SEQ_AUTOR_CIR           NUMBER(10),
  NR_TITULO_RECEBER          NUMBER(10),
  CD_CNPJ_SEGURADORA         VARCHAR2(14 BYTE),
  CD_SETOR_ENTREGA           NUMBER(5),
  NR_SEQ_GUIA_PLANO          NUMBER(10),
  DS_STACK                   VARCHAR2(2000 BYTE),
  DT_IMPRESSAO               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORDCOMP_AGEPACI_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_AGENDA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_ATEPACI_FK_I ON TASY.ORDEM_COMPRA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_CENCUST_FK_I ON TASY.ORDEM_COMPRA
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_COMPRAD_FK_I ON TASY.ORDEM_COMPRA
(CD_COMPRADOR, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_CONPAGA_FK_I ON TASY.ORDEM_COMPRA
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_CONTRAT_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_CONVENI_FK_I ON TASY.ORDEM_COMPRA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_ESTABEL_FK_I ON TASY.ORDEM_COMPRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_ESTABEL_FK2_I ON TASY.ORDEM_COMPRA
(CD_ESTAB_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_FORCHEG_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_FORMA_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_I1 ON TASY.ORDEM_COMPRA
(DT_ORDEM_COMPRA, CD_ESTABELECIMENTO, DT_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_I2 ON TASY.ORDEM_COMPRA
(CD_ESTABELECIMENTO, DT_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_LICLICI_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_LIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_LICLICI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_LOCENTR_FK_I ON TASY.ORDEM_COMPRA
(CD_LOCAL_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_LOCENTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_LOCESTO_FK2_I ON TASY.ORDEM_COMPRA
(CD_LOCAL_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_LOCESTO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_MOEDA_FK_I ON TASY.ORDEM_COMPRA
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_MOSOCOM_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_MOTIVO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_MOSOCOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_MOTCASO_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_MOTCASO_FK2_I ON TASY.ORDEM_COMPRA
(NR_SEQ_MOTIVO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_MOTCASO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_MOURGCO_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_MOTIVO_URGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_MOURGCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_NOTFISC_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_NF_REPASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA
(NR_ORDEM_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_PESFISI_FK_I ON TASY.ORDEM_COMPRA
(CD_PESSOA_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_PESFISI_FK1_I ON TASY.ORDEM_COMPRA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_PESJURI_FK_I ON TASY.ORDEM_COMPRA
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_PESJURI_FK1_I ON TASY.ORDEM_COMPRA
(CD_CNPJ_SEGURADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_PESJURI_FK1_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ORDCOMP_PK ON TASY.ORDEM_COMPRA
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_PLSGUIA_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_GUIA_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_REGLICO_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_REG_LIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_REGLICO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_REGLIMC_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_MOD_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_REGLIMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_REGLITC_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_TIPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_REGLITC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_REPRTES_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_REPRTES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_REQMATE_FK_I ON TASY.ORDEM_COMPRA
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_REQMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_RESSFOR_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_RESSUP_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_RESSFOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_SETATEN_FK_I ON TASY.ORDEM_COMPRA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_SETATEN_FK2_I ON TASY.ORDEM_COMPRA
(CD_SETOR_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_SUSUCOM_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_SUBGRUPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_SUSUCOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_TITRECE_FK_I ON TASY.ORDEM_COMPRA
(NR_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORDCOMP_TRANSPO_FK_I ON TASY.ORDEM_COMPRA
(CD_CGC_TRANSPORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDCOMP_VIAFATU_FK_I ON TASY.ORDEM_COMPRA
(NR_SEQ_VIA_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOMP_VIAFATU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Ordem_Compra_Delete
BEFORE DELETE ON TASY.ORDEM_COMPRA FOR EACH ROW
DECLARE

nr_seq_interno_w	Number(10,0);
ie_seq_interna_w	varchar2(1);
qt_existe_w		Number(10,0);

BEGIN

select	nvl(max(ie_seq_interna),'N')
into	ie_seq_interna_w
from	parametro_compras
where	cd_estabelecimento	= :old.cd_estabelecimento;

if	(:old.nr_seq_interno is not null) and
	(ie_seq_interna_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(266265);
	--'A ordem de compra item sequencia interna. N�o pode ser excluida');
end if;

if	(:old.nr_seq_reg_lic_compra is not null) then

	update	reg_lic_compra_item
	set	nr_ordem_compra = ''
	where	nr_ordem_compra = :old.nr_ordem_compra;

	select	count(*)
	into	qt_existe_w
	from	reg_lic_compra_item
	where	nr_seq_reg_lic_compra = :old.nr_seq_reg_lic_compra
	and	nr_ordem_compra is not null;

	if	(qt_existe_w = 0) then
		update	reg_lic_compra
		set	dt_geracao_ordem_compra	= '',
			nm_usuario_geracao_oc	= ''
		where	nr_sequencia = :old.nr_seq_reg_lic_compra;
	end if;
end if;

update	material_autor_cirurgia
set	nr_ordem_compra	= null
where	nr_ordem_compra = :old.nr_ordem_compra;

END;
/


CREATE OR REPLACE TRIGGER TASY.INTPD_ORDEM_COMPRA
before insert or update or delete ON TASY.ORDEM_COMPRA for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_registros_w			number(10);

begin

if	(inserting or updating) then

	reg_integracao_p.cd_estab_documento	:= :new.cd_estabelecimento;
	reg_integracao_p.ie_tipo_ordem		:= :new.ie_tipo_ordem;
	reg_integracao_p.nr_seq_tipo_compra	:= :new.nr_seq_tipo_compra;
	reg_integracao_p.nr_seq_mod_compra	:= :new.nr_seq_mod_compra;
	reg_integracao_p.ds_id_origin		:= :new.ie_origem_imp;

	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :new.nr_ordem_compra
	and	ie_evento = '2'
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w = 0) and /*Somente faz a inser��o caso n�o tenha nenhum registro dessa ordem de compra na fila. Se tiver, manda altera��o no final dessa trigger*/
		(:old.dt_aprovacao is null) and
		(:new.dt_aprovacao is not null) and
		(:new.dt_baixa is null) then

		reg_integracao_p.ie_operacao		:= 'I';
		gerar_int_padrao.gravar_integracao('2', :new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);

	elsif	(updating) and /*Quando j� tem um registro desta ordem de compra na fila, manda o update*/
		(qt_registros_w > 0) then

		begin
		select	1
		into	qt_registros_w
		from	intpd_fila_transmissao
		where	nr_seq_documento = :new.nr_ordem_compra
		and	ie_evento = '2'
		and	nvl(ie_status,'P') not in ('P','R')
		and	rownum = 1;
		exception
		when others then
			qt_registros_w := 0;
		end;

		if	(qt_registros_w > 0) then

			reg_integracao_p.ie_operacao		:= 'A';
			gerar_int_padrao.gravar_integracao('2',:new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);
		end if;
	end if;
elsif	(deleting) then

	reg_integracao_p.cd_estab_documento	:= null;
	reg_integracao_p.ie_tipo_ordem		:= null;
	reg_integracao_p.nr_seq_tipo_compra	:= null;
	reg_integracao_p.nr_seq_mod_compra	:= null;
	reg_integracao_p.ds_id_origin		:= null;


	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :old.nr_ordem_compra
	and	ie_evento = '2'
	and	ie_status in ('S')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then
		reg_integracao_p.ie_operacao		:= 'E';
		gerar_int_padrao.gravar_integracao('2',:old.nr_ordem_compra,:old.nm_usuario, reg_integracao_p);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_tp  after update ON TASY.ORDEM_COMPRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_ORDEM_COMPRA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_GUIA_PLANO,1,4000),substr(:new.NR_SEQ_GUIA_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GUIA_PLANO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AUTOR_CIR,1,4000),substr(:new.NR_SEQ_AUTOR_CIR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AUTOR_CIR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ_SEGURADORA,1,4000),substr(:new.CD_CNPJ_SEGURADORA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ_SEGURADORA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TITULO_RECEBER,1,4000),substr(:new.NR_TITULO_RECEBER,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO_RECEBER',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ENTREGA,1,4000),substr(:new.CD_SETOR_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DOCUMENTO_EXTERNO,1,4000),substr(:new.NR_DOCUMENTO_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOCUMENTO_EXTERNO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_BAIXA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_BAIXA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_BAIXA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INCLUSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INCLUSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INCLUSAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_SOLICITANTE,1,4000),substr(:new.CD_PESSOA_SOLICITANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SOLICITANTE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_TRANSPORTADOR,1,4000),substr(:new.CD_CGC_TRANSPORTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_TRANSPORTADOR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_MULTA_ATRASO,1,4000),substr(:new.PR_MULTA_ATRASO,1,4000),:new.nm_usuario,nr_seq_w,'PR_MULTA_ATRASO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESCONTO,1,4000),substr(:new.PR_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESCONTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESC_PGTO_ANTEC,1,4000),substr(:new.PR_DESC_PGTO_ANTEC,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESC_PGTO_ANTEC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_ALTERACAO,1,4000),substr(:new.CD_MOTIVO_ALTERACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_ALTERACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ENTREGA,1,4000),substr(:new.CD_LOCAL_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ENTREGA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ENTREGA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRESCRICAO,1,4000),substr(:new.NR_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRESCRICAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_FORNECEDOR,1,4000),substr(:new.CD_CGC_FORNECEDOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_FORNECEDOR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ORDEM_COMPRA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ORDEM_COMPRA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AVISO_CHEGADA,1,4000),substr(:new.IE_AVISO_CHEGADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AVISO_CHEGADA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_COMPRADOR,1,4000),substr(:new.CD_COMPRADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_COMPRADOR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FRETE,1,4000),substr(:new.VL_FRETE,1,4000),:new.nm_usuario,nr_seq_w,'VL_FRETE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRETE,1,4000),substr(:new.IE_FRETE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRETE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PESSOA_CONTATO,1,4000),substr(:new.DS_PESSOA_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PESSOA_CONTATO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REQUISICAO,1,4000),substr(:new.NR_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_REQUISICAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_APROVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_APROVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_APROVACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_JUROS_NEGOCIADO,1,4000),substr(:new.PR_JUROS_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'PR_JUROS_NEGOCIADO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INTERNO,1,4000),substr(:new.NR_SEQ_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INTERNO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_URGENTE,1,4000),substr(:new.IE_URGENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_URGENTE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_AGRUP,1,4000),substr(:new.NR_ORDEM_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_AGRUP',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EMITE_OBS,1,4000),substr(:new.IE_EMITE_OBS,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMITE_OBS',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_PAGTO,1,4000),substr(:new.IE_SOMENTE_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_PAGTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_IMP,1,4000),substr(:new.NM_USUARIO_IMP,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_IMP',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_CANCEL,1,4000),substr(:new.NR_SEQ_MOTIVO_CANCEL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_CANCEL',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESPESA_ACESSORIA,1,4000),substr(:new.VL_DESPESA_ACESSORIA,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESPESA_ACESSORIA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESC_FINANCEIRO,1,4000),substr(:new.PR_DESC_FINANCEIRO,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESC_FINANCEIRO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SUBGRUPO_COMPRA,1,4000),substr(:new.NR_SEQ_SUBGRUPO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SUBGRUPO_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_LEITURA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LEITURA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LEITURA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ACEITE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ACEITE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ACEITE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ACEITE,1,4000),substr(:new.NM_USUARIO_ACEITE,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_ACEITE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FORMA_PAGTO,1,4000),substr(:new.NR_SEQ_FORMA_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FORMA_PAGTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FATURAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FATURAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FATURAMENTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESCONTO,1,4000),substr(:new.VL_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTAB_TRANSF,1,4000),substr(:new.CD_ESTAB_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTAB_TRANSF',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ORDEM,1,4000),substr(:new.IE_TIPO_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ORDEM',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RESSUP_FORNEC,1,4000),substr(:new.NR_SEQ_RESSUP_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RESSUP_FORNEC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_EXPORTAR,1,4000),substr(:new.IE_FORMA_EXPORTAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_EXPORTAR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ENVIO_EMAIL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ENVIO_EMAIL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ENVIO_EMAIL',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CIRURGIA,1,4000),substr(:new.NR_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CIRURGIA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REG_LIC_COMPRA,1,4000),substr(:new.NR_SEQ_REG_LIC_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REG_LIC_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LIBERACAO_PORTAL,1,4000),substr(:new.IE_LIBERACAO_PORTAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERACAO_PORTAL',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOD_COMPRA,1,4000),substr(:new.NR_SEQ_MOD_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOD_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_NF_REPASSE,1,4000),substr(:new.NR_SEQ_NF_REPASSE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_NF_REPASSE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DOCUMENTO_INTERNO,1,4000),substr(:new.NR_DOCUMENTO_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOCUMENTO_INTERNO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PROCESSO_INTERNO,1,4000),substr(:new.NR_PROCESSO_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PROCESSO_INTERNO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_COMPRA,1,4000),substr(:new.NR_SEQ_TIPO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA_PAC,1,4000),substr(:new.NR_SEQ_AGENDA_PAC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA_PAC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LICITACAO,1,4000),substr(:new.NR_SEQ_LICITACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LICITACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTROLE_EDITAL,1,4000),substr(:new.NR_CONTROLE_EDITAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTROLE_EDITAL',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DADOS_IMPORTACAO,1,4000),substr(:new.DS_DADOS_IMPORTACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DADOS_IMPORTACAO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_URGENTE,1,4000),substr(:new.NR_SEQ_MOTIVO_URGENTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_URGENTE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_IMP,1,4000),substr(:new.IE_ORIGEM_IMP,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_IMP',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ESTORNO_LIB,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ESTORNO_LIB,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ESTORNO_LIB',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_ESTORNO_LIB,1,4000),substr(:new.NR_SEQ_MOTIVO_ESTORNO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_ESTORNO_LIB',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ESTORNO_LIB,1,4000),substr(:new.NM_USUARIO_ESTORNO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_ESTORNO_LIB',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ENVIO_INTEGRACAO_OPME,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ENVIO_INTEGRACAO_OPME,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ENVIO_INTEGRACAO_OPME',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_ORIG,1,4000),substr(:new.NR_ORDEM_ORIG,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_ORIG',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOLO,1,4000),substr(:new.NR_SEQ_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOLO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_TRANSF,1,4000),substr(:new.CD_LOCAL_TRANSF,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_TRANSF',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_CONF_ENTR,1,4000),substr(:new.NM_USUARIO_CONF_ENTR,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_CONF_ENTR',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CONFIRMA_ENTREGA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CONFIRMA_ENTREGA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CONFIRMA_ENTREGA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIB,1,4000),substr(:new.NM_USUARIO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIB',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESPESA_DOC,1,4000),substr(:new.VL_DESPESA_DOC,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESPESA_DOC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_BAIXA,1,4000),substr(:new.NR_SEQ_MOTIVO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_BAIXA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LIC,1,4000),substr(:new.NR_SEQ_LIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LIC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DECLARACAO_IMP,1,4000),substr(:new.NR_DECLARACAO_IMP,1,4000),:new.nm_usuario,nr_seq_w,'NR_DECLARACAO_IMP',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_VIA_FATURA,1,4000),substr(:new.NR_SEQ_VIA_FATURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_VIA_FATURA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBS_INTERNA,1,4000),substr(:new.DS_OBS_INTERNA,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBS_INTERNA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SISTEMA_ORIGEM,1,4000),substr(:new.IE_SISTEMA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_SISTEMA_ORIGEM',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NECESSITA_ENVIAR_INT,1,4000),substr(:new.IE_NECESSITA_ENVIAR_INT,1,4000),:new.nm_usuario,nr_seq_w,'IE_NECESSITA_ENVIAR_INT',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ALTERA_INT,1,4000),substr(:new.NM_USUARIO_ALTERA_INT,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_ALTERA_INT',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SOLIC_COMPRA_GERADA,1,4000),substr(:new.NR_SOLIC_COMPRA_GERADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SOLIC_COMPRA_GERADA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ATEND_OPME,1,4000),substr(:new.NR_SEQ_ATEND_OPME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ATEND_OPME',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_SOLIC,1,4000),substr(:new.NR_SEQ_MOTIVO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_SOLIC',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_SEGURO,1,4000),substr(:new.VL_SEGURO,1,4000),:new.nm_usuario,nr_seq_w,'VL_SEGURO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL_REGRA_PJ,1,4000),substr(:new.DS_EMAIL_REGRA_PJ,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL_REGRA_PJ',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONTATO_REGRA_PJ,1,4000),substr(:new.DS_CONTATO_REGRA_PJ,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONTATO_REGRA_PJ',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORCADO,1,4000),substr(:new.IE_ORCADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORCADO',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_APROV,1,4000),substr(:new.NM_USUARIO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_APROV',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CONV_MOEDA,1,4000),substr(:new.VL_CONV_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'VL_CONV_MOEDA',ie_log_w,ds_w,'ORDEM_COMPRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ordem_compra_afterupdate
after update ON TASY.ORDEM_COMPRA for each row
declare
qt_existe_w			number(10);
ie_momento_integ_w		varchar2(1);
nr_seq_integracao_w		number(10);
nr_seq_regra_w			number(10);

pragma autonomous_transaction;

begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a
where	a.ie_evento = 'OC'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A'
and	exists	(select	1
		from	sup_int_regra_oc x
		where	x.nr_seq_integracao = a.nr_sequencia
		and	ie_situacao = 'A');

if	(qt_existe_w > 0) then
	begin

	select	max(nr_sequencia)
	into	nr_seq_integracao_w
	from	sup_parametro_integracao a
	where	a.ie_evento = 'OC'
	and	a.ie_forma = 'E'
	and	a.ie_situacao = 'A';

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_regra_w
	from	sup_int_regra_oc
	where	nr_seq_integracao = nr_seq_integracao_w
	and	cd_local_entrega = :new.cd_local_entrega
	and	ie_situacao = 'A';

	if	(nr_seq_regra_w <> 0) then
		begin

		select	nvl(max(ie_lib_aprov),'L')
		into	ie_momento_integ_w
		from	sup_int_regra_oc
		where	nr_sequencia = nr_seq_regra_w;

		end;
	else
		begin

		select	nvl(max(nr_sequencia),0)
		into	nr_seq_regra_w
		from	sup_int_regra_oc
		where	nr_seq_integracao = nr_seq_integracao_w
		and	ie_situacao = 'A';

		select	nvl(max(ie_lib_aprov),'L')
		into	ie_momento_integ_w
		from	sup_int_regra_oc
		where	nr_sequencia = nr_seq_regra_w;

		end;
	end if;

	if	((ie_momento_integ_w = 'A') and
		(:old.dt_aprovacao is null) and
		(:new.dt_aprovacao is not null)) or
		((ie_momento_integ_w = 'L') and
		(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null)) then
		begin

		envia_sup_int_oc(
			nr_seq_regra_w,
			:new.nr_ordem_compra,
			:new.cd_estabelecimento,
			:new.cd_cgc_fornecedor,
			:new.cd_condicao_pagamento,
			:new.cd_comprador,
			:new.dt_ordem_compra,
			:new.cd_moeda,
			:new.cd_pessoa_solicitante,
			:new.cd_cgc_transportador,
			:new.ie_frete,
			:new.vl_frete,
			:new.pr_desconto,
			:new.pr_desc_pgto_antec,
			:new.pr_juros_negociado,
			:new.ds_pessoa_contato,
			:new.ds_observacao,
			:new.cd_local_entrega,
			:new.dt_entrega,
			:new.ie_aviso_chegada,
			:new.vl_despesa_acessoria,
			:new.nr_seq_subgrupo_compra,
			:new.pr_desc_financeiro,
			:new.cd_pessoa_fisica,
			:new.ie_urgente,
			:new.nr_seq_forma_pagto,
			:new.nr_documento_externo,
			:new.vl_desconto,
			:new.cd_centro_custo,
			:new.nm_usuario,
			'','','',:new.nm_usuario);

		end;
	end if;

	end;
end if;

if(:old.dt_aprovacao is null and :new.dt_aprovacao is not null) then
begin

	select	count(*)
	into	qt_existe_w
	from	cliente_integracao
	where	NR_SEQ_INF_INTEGRACAO = 1510
	and	IE_SITUACAO = 'A';

	if (qt_existe_w > 0) then
	begin
		gravar_agend_integracao(819, 'NR_ORDEM_COMPRA_P=' || :new.nr_ordem_compra
		|| ';CD_ESTABELECIMENTO_P=' || wheb_usuario_pck.get_cd_estabelecimento() || ';');
	end;
	end if;
end;
end if;

if(:new.DT_BAIXA is not null) then
	reagendar_item_oc_fluxo(:new.nr_ordem_compra, 'OC', 'A', :new.nm_usuario);
	reagendar_vencimento_oc_fluxo(:new.nr_ordem_compra, 'OC', 'A', :new.nm_usuario);
end if;

commit;
end;
/


CREATE OR REPLACE TRIGGER TASY.ordem_compra_tie
after update ON TASY.ORDEM_COMPRA for each row
declare

ds_retorno_integracao_w		clob;

begin

if (wheb_usuario_pck.get_nm_usuario is not null) then
    if (:new.dt_aprovacao is not null) and (:old.dt_aprovacao is null) then
        select bifrost.send_integration(
			'purchase.order.send.request',
			'com.philips.tasy.integration.purchaseorder.outbound.PurchaseOrderCallback',
			''||:new.nr_ordem_compra||'',
			wheb_usuario_pck.get_nm_usuario)
		into ds_retorno_integracao_w
        from dual;
    end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Ordem_Compra_Atual
BEFORE UPDATE ON TASY.ORDEM_COMPRA FOR EACH ROW
DECLARE

nr_cot_compra_w		number(10);
nr_solic_compra_w		number(10);
qt_existe_w			number(5);
ds_historico_w		ordem_compra_hist.ds_historico%type;
ds_estab_antigo_w	varchar2(255);
ds_estab_novo_w		varchar2(255);


cursor C01 IS
select	distinct
	nr_cot_compra
from	ordem_compra_item
where	nr_ordem_compra = :new.nr_ordem_compra;

cursor c02 is
select	distinct nr_solic_compra
from(
	select	nr_solic_compra
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_w
	and	nr_solic_compra > 0
	union
	select	nr_solic_compra
	from	cot_compra_solic_agrup
	where	nr_cot_compra = nr_cot_compra_w
	and	nr_solic_compra > 0);

begin

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	inserir_historico_ordem_compra(
		:new.nr_ordem_compra,
		'S',
		wheb_mensagem_pck.get_texto(306352) /*'Libera��o da ordem de compra'*/,
		wheb_mensagem_pck.get_texto(306353) /*'A ordem de compra foi liberada para aprova��o.'*/,
		:new.nm_usuario);
end if;

begin

select	count(*)
into	qt_existe_w
from	nota_fiscal_item
where 	nr_ordem_compra = :new.nr_ordem_compra;

exception when others then
	qt_existe_w := 0;
end;

if	((:new.nr_seq_nf_repasse is not null) or
	(qt_existe_w > 0)) and
	(nvl(:old.cd_estabelecimento,0) <> nvl(:new.cd_estabelecimento,0)) then
	Wheb_mensagem_pck.exibir_mensagem_abort(360562); /*N�o � permitido alterar o estabelecimento em ordens de compra que j� possuem nota fiscal*/

end if;

if	(nvl(:old.cd_estabelecimento,0) <> nvl(:new.cd_estabelecimento,0)) then

	select	obter_nome_estab(:old.cd_estabelecimento),
		obter_nome_estab(:new.cd_estabelecimento)
	into	ds_estab_antigo_w,
		ds_estab_novo_w
	from	dual;

	ds_historico_w := Wheb_mensagem_pck.get_Texto(360645,
							'CD_ESTAB_OLD_W='|| ds_estab_antigo_w||
							';CD_ESTAB_NEW_W='||ds_estab_novo_w);

	inserir_historico_ordem_compra(
		:new.nr_ordem_compra,
		'S',
		Wheb_mensagem_pck.get_Texto(360643), /* Troca de estabelecimento */
		ds_historico_w,
		:new.nm_usuario);

end if;

if	(:old.dt_liberacao is not null) and
	(:new.dt_liberacao is null) then
	inserir_historico_ordem_compra(
		:new.nr_ordem_compra,
		'S',
		Wheb_mensagem_pck.get_Texto(300822), /* Estorno da libera��o */
		substr(dbms_utility.format_call_stack,1,2000),
		:new.nm_usuario);
end if;

if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null) and
	(:new.dt_baixa is null) then

	inserir_historico_ordem_compra(
		:new.nr_ordem_compra,
		'S',
		wheb_mensagem_pck.get_texto(306354) /*'Aprova��o da ordem de compra'*/,
		wheb_mensagem_pck.get_texto(306355) /*'A ordem de compra foi aprovada.'*/,
		:new.nm_usuario);

	open C01;
	loop
	fetch C01 into
		nr_cot_compra_w;
	exit when C01%notfound;
		begin

		open C02;
		loop
		fetch C02 into
			nr_solic_compra_w;
		exit when C02%notfound;
			begin
			begin
			gerar_hist_solic_sem_commit(
				nr_solic_compra_w,
				wheb_mensagem_pck.get_texto(306354) /*'Aprova��o da ordem de compra'*/,
				/*Aprova��o da ordem de compra #@NR_ORDEM_COMPRA#@, que foi originada atrav�s desta solicita��o de compras.*/
				wheb_mensagem_pck.get_texto(306357, 'NR_ORDEM_COMPRA=' || :new.nr_ordem_compra),
				'AOC',
				:new.nm_usuario);
			exception
			when others then
				null;
			end;

			end;
		end loop;
		close C02;

		end;
	end loop;
	close C01;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Ordem_Compra_Insert
BEFORE INSERT OR UPDATE ON TASY.ORDEM_COMPRA FOR EACH ROW
DECLARE
ie_seq_interna_w			Varchar2(01);
nr_seq_interno_w			Number(10,0);
ie_somente_pagto_w		Varchar2(1);
ie_baixar_oc_pagto_w		Varchar2(1);
qt_reg_w				number(1);
qt_reprovada_w			number(10);
qt_itens_w			number(10);
gerar_material_marca_w		varchar2(1);
qt_registro_w			number(10);
vl_frete_rateado_w		ordem_compra.vl_frete%type;
qt_itens_oc_w			ordem_compra_item.nr_item_oci%type;
nr_ordem_compra_w		ordem_compra_item_trib.nr_ordem_compra%type;
nr_item_oci_w			ordem_compra_item_trib.nr_item_oci%type;
qt_material_w			ordem_compra_item.qt_material%type;
vl_unitario_material_w	ordem_compra_item.vl_unitario_material%type;
vl_desconto_w			ordem_compra_item.vl_desconto%type;
cd_tributo_w			ordem_compra_item_trib.cd_tributo%type;
pr_tributo_w			ordem_compra_item_trib.pr_tributo%type;
ie_desc_nf_w			tributo.ie_desc_nf%type;
vl_tributo_w			ordem_compra_item_trib.vl_tributo%type;
ie_status_w  number;

cursor c02 is
select	ocit.nr_ordem_compra,
		ocit.nr_item_oci,
		ocit.cd_tributo,
		ocit.pr_tributo,
		obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 1),
		obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 2),
		obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 3)
from	ordem_compra_item_trib ocit,
		tributo t
where	ocit.nr_ordem_compra = :new.nr_ordem_compra
and		ocit.cd_tributo = t.cd_tributo
and		t.ie_tipo_tributo = 'IPI'
and		nvl(obter_inf_itens_oc(ocit.nr_ordem_compra, ocit.nr_item_oci, 4),0) = 0;

BEGIN

if (INSERTING)then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select count(*)
into ie_status_w
from ORDEM_COMPRA_ITEM
where NR_ORDEM_COMPRA = :new.NR_ORDEM_COMPRA and obter_bloq_canc_proj_rec(nr_seq_proj_rec) > 0;

if (ie_status_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

if	(:new.cd_cgc_fornecedor is not null) and
	(:new.cd_pessoa_fisica is not null) then
	--(-20011,'Nao e correto informar pessoa fisica e juridica');
	wheb_mensagem_pck.exibir_mensagem_abort(177123);
end if;

if	(:new.cd_cgc_fornecedor is null) and
	(:new.cd_estab_transf is not null) and
	(:new.ie_tipo_ordem = 'T') then
	begin
	select	cd_cgc
	into	:new.cd_cgc_fornecedor
	from	estabelecimento
	where	cd_estabelecimento = :new.cd_estab_transf;
	exception
	when others then
		:new.cd_cgc_fornecedor	:=	null;
	end;
end if;


if	(:new.cd_cgc_fornecedor is null) and
	(:new.cd_pessoa_fisica is null) then
	--(-20011,'Nao foi informado pessoa fisica ou juridica');
	wheb_mensagem_pck.exibir_mensagem_abort(177125);
end if;

if	(:new.nr_seq_interno is null) and
	(:old.nr_ordem_compra is null) then
	select	nvl(max(ie_seq_interna),'N')
	into	ie_seq_interna_w
	from	parametro_compras
	where	cd_estabelecimento	= :new.cd_estabelecimento;

	/* Locar a tabela e obter o numero da ordem de compra */
	if	(ie_seq_interna_w = 'S') then
		nr_seq_interno_w	:= obter_nr_interno_oc;
		:new.nr_seq_interno	:= nr_seq_interno_w;
	end if;
end if;

if	(nvl(:new.cd_pessoa_solicitante,'X') <>  nvl(:old.cd_pessoa_solicitante,'X')) then
	begin
	wheb_usuario_pck.set_ie_executar_trigger('N');
	update	ordem_compra_item
	set	cd_pessoa_solicitante	= :new.cd_pessoa_solicitante
	where	nr_ordem_compra		= :old.nr_ordem_compra;
	wheb_usuario_pck.set_ie_executar_trigger('S');
	exception
	when others then
		wheb_usuario_pck.set_ie_executar_trigger('S');
	end;
end if;

select	count(*)
into	qt_registro_w
from	ordem_compra_item
where	nr_ordem_compra = :new.nr_ordem_compra
and	dt_aprovacao is not null;

ie_somente_pagto_w	:= nvl(:new.ie_somente_pagto,'N');

if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null)  and
	(ie_somente_pagto_w = 'S') and
	(qt_registro_w > 0)	then
	begin
	gerar_titulo_ordem(	:new.nr_ordem_compra,
				:new.cd_estabelecimento,
				trunc(:new.dt_aprovacao,'dd'), /*  Francisco - 30/05 - Troquei dt_emissao por essa */
				nvl(:new.cd_moeda,1),
				nvl(:new.pr_juros_negociado,0),
				nvl(:new.pr_multa_atraso,0),
				nvl(:new.cd_pessoa_fisica,''),
				nvl(:new.cd_cgc_fornecedor,''),
				:new.nm_usuario,
				substr(:new.ds_observacao,1,254));

	select	nvl(max(ie_baixar_oc_pagto),'S')
	into	ie_baixar_oc_pagto_w
	from	parametro_compras
	where	cd_estabelecimento	= :new.cd_estabelecimento;
	if	(ie_baixar_oc_pagto_w = 'S') then
		:new.dt_baixa := sysdate;
	end if;

	end;
end if;

if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null)  and
	(qt_registro_w > 0) then
	begin

	select	count(*)
	into	qt_reprovada_w
	from	ordem_compra_item
	where	nr_ordem_Compra = :new.nr_ordem_compra
	and	nr_seq_aprovacao is not null
	and	dt_reprovacao is not null;

	select	count(*)
	into	qt_itens_w
	from	ordem_compra_item
	where	nr_ordem_compra = :new.nr_ordem_compra;

	if	(qt_itens_w <> qt_reprovada_w) then
		begin

		gerar_adiantamento_oc(
			:new.nr_ordem_compra,
			:new.cd_cgc_fornecedor,
			:new.cd_pessoa_fisica,
			:new.cd_moeda,
			:new.cd_estabelecimento,
			:new.nm_usuario,
			:new.vl_conv_moeda);
		end;
	end if;

	select	nvl(max(ie_gerar_material_marca),'N')
	into	gerar_material_marca_w
	from	parametro_compras
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if	(gerar_material_marca_w = 'S') then
		gerar_material_marca_oc(:new.nr_ordem_compra,:new.cd_estabelecimento,:new.nm_usuario);
	end if;

	end;
end if;

qt_itens_oc_w := obter_inf_itens_oc(:new.nr_ordem_compra, null, null);

if	(updating) and (qt_itens_oc_w > 0) then
	begin
	vl_frete_rateado_w := :new.vl_frete / qt_itens_oc_w;

	open c02;
	loop
	fetch c02 into
		nr_ordem_compra_w,
		nr_item_oci_w,
		cd_tributo_w,
		pr_tributo_w,
		qt_material_w,
		vl_unitario_material_w,
		vl_desconto_w;
	exit when c02%notfound;
		begin
		select	nvl(ie_desc_nf,'N')
		into	ie_desc_nf_w
		from	tributo
		where	cd_tributo = cd_tributo_w;

		if (:new.ie_frete = 'F') and (:new.vl_frete > 0) and ((:new.ie_frete <> :old.ie_frete) or (:new.vl_frete <> :old.vl_frete)) then
			begin
			if (ie_desc_nf_w = 'S') then
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) - vl_desconto_w + vl_frete_rateado_w);
			else
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) + vl_frete_rateado_w);
			end if;
			end;
		elsif (:old.ie_frete = 'F' and :new.ie_frete <> 'F') or (:old.vl_frete > 0 and nvl(:new.vl_frete,0) = 0) then
			begin
			if (ie_desc_nf_w = 'S') then
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) - vl_desconto_w);
			else
				vl_tributo_w	:= (pr_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w));
			end if;
			end;
		end if;

		if (vl_tributo_w is not null) then
			update	ordem_compra_item_trib
			set		vl_tributo = vl_tributo_w
			where	nr_ordem_compra = nr_ordem_compra_w
			and		nr_item_oci = nr_item_oci_w
			and		cd_tributo = cd_tributo_w;
		end if;
		end;
	end loop;
	close c02;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ORDEM_COMPRA ADD (
  CONSTRAINT ORDCOMP_PK
 PRIMARY KEY
 (NR_ORDEM_COMPRA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA ADD (
  CONSTRAINT ORDCOMP_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORDCOMP_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT ORDCOMP_PESJURI_FK1 
 FOREIGN KEY (CD_CNPJ_SEGURADORA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ORDCOMP_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ENTREGA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORDCOMP_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PLANO) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_MOTCASO_FK2 
 FOREIGN KEY (NR_SEQ_MOTIVO_BAIXA) 
 REFERENCES TASY.MOTIVO_CANCEL_SC_OC (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_VIAFATU_FK 
 FOREIGN KEY (NR_SEQ_VIA_FATURA) 
 REFERENCES TASY.VIA_FATURA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_LICLICI_FK 
 FOREIGN KEY (NR_SEQ_LIC) 
 REFERENCES TASY.LIC_LICITACAO (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_MOSOCOM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SOLIC) 
 REFERENCES TASY.MOTIVO_SOLIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_REGLITC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPRA) 
 REFERENCES TASY.REG_LIC_TIPO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_COMPRAD_FK 
 FOREIGN KEY (CD_COMPRADOR, CD_ESTABELECIMENTO) 
 REFERENCES TASY.COMPRADOR (CD_PESSOA_FISICA,CD_ESTABELECIMENTO),
  CONSTRAINT ORDCOMP_FORCHEG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_PAGTO) 
 REFERENCES TASY.FORMA_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORDCOMP_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT ORDCOMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ORDCOMP_LOCENTR_FK 
 FOREIGN KEY (CD_LOCAL_ENTREGA) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT ORDCOMP_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT ORDCOMP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORDCOMP_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ORDCOMP_TRANSPO_FK 
 FOREIGN KEY (CD_CGC_TRANSPORTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ORDCOMP_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_AGRUP) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT ORDCOMP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ORDCOMP_MOTCASO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.MOTIVO_CANCEL_SC_OC (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_SUSUCOM_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO_COMPRA) 
 REFERENCES TASY.SUP_SUBGRUPO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_REQMATE_FK 
 FOREIGN KEY (NR_REQUISICAO) 
 REFERENCES TASY.REQUISICAO_MATERIAL (NR_REQUISICAO)
    ON DELETE CASCADE,
  CONSTRAINT ORDCOMP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT ORDCOMP_RESSFOR_FK 
 FOREIGN KEY (NR_SEQ_RESSUP_FORNEC) 
 REFERENCES TASY.RESSUP_FORNEC (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_REGLICO_FK 
 FOREIGN KEY (NR_SEQ_REG_LIC_COMPRA) 
 REFERENCES TASY.REG_LIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NF_REPASSE) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ORDCOMP_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_PAC) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_REGLIMC_FK 
 FOREIGN KEY (NR_SEQ_MOD_COMPRA) 
 REFERENCES TASY.REG_LIC_MOD_COMPRA (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_MOURGCO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_URGENTE) 
 REFERENCES TASY.MOTIVO_URGENCIA_COMPRAS (NR_SEQUENCIA),
  CONSTRAINT ORDCOMP_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_TRANSF) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ORDCOMP_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT ORDCOMP_LOCESTO_FK2 
 FOREIGN KEY (CD_LOCAL_TRANSF) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT ORDCOMP_REPRTES_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.REGRA_PROT_TRANSF_ESTAB (NR_SEQUENCIA));

GRANT SELECT ON TASY.ORDEM_COMPRA TO NIVEL_1;

