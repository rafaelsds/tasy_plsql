ALTER TABLE TASY.FIS_PESSOA_JURIDICA_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_PESSOA_JURIDICA_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_PESSOA_JURIDICA_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  DS_EMAIL             VARCHAR2(80 BYTE),
  DS_ENDERECO          VARCHAR2(100 BYTE),
  CD_CEP               VARCHAR2(15 BYTE),
  NR_ENDERECO          VARCHAR2(10 BYTE),
  DS_COMPLEMENTO       VARCHAR2(255 BYTE),
  DS_BAIRRO            VARCHAR2(80 BYTE),
  DS_MUNICIPIO         VARCHAR2(40 BYTE),
  SG_ESTADO            VARCHAR2(15 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE),
  NR_DDD_TELEFONE      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PJHIST_PESJURI_FK_I ON TASY.FIS_PESSOA_JURIDICA_HIST
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PJHIST_PK ON TASY.FIS_PESSOA_JURIDICA_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FIS_PESSOA_JURIDICA_HIST_tp  after update ON TASY.FIS_PESSOA_JURIDICA_HIST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'FIS_PESSOA_JURIDICA_HIST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FIS_PESSOA_JURIDICA_HIST ADD (
  CONSTRAINT PJHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_PESSOA_JURIDICA_HIST ADD (
  CONSTRAINT PJHIST_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_PESSOA_JURIDICA_HIST TO NIVEL_1;

