ALTER TABLE TASY.PESSOA_FISICA_PRECAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_PRECAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_PRECAD
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  IE_TIPO_PESSOA                 NUMBER(1)      NOT NULL,
  NM_PESSOA_FISICA               VARCHAR2(60 BYTE) NOT NULL,
  DT_NASCIMENTO                  DATE,
  IE_SEXO                        VARCHAR2(1 BYTE),
  IE_ESTADO_CIVIL                VARCHAR2(2 BYTE),
  NR_CPF                         VARCHAR2(11 BYTE),
  NR_IDENTIDADE                  VARCHAR2(15 BYTE),
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NR_TELEFONE_CELULAR            VARCHAR2(40 BYTE),
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  IE_GRAU_INSTRUCAO              NUMBER(2),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  DT_LIBERACAO                   DATE,
  CD_NACIONALIDADE               VARCHAR2(8 BYTE),
  NM_USUARIO_LIB                 VARCHAR2(15 BYTE),
  NR_SEQ_PROCESSO                NUMBER(10)     NOT NULL,
  CD_EMPRESA                     NUMBER(4),
  DS_ORGAO_EMISSOR_CI            VARCHAR2(40 BYTE),
  CD_ESTABELECIMENTO             NUMBER(4),
  CD_MEDICO                      VARCHAR2(10 BYTE),
  DS_OBSERVACAO                  VARCHAR2(2000 BYTE),
  NM_PESSOA_FISICA_SEM_ACENTO    VARCHAR2(60 BYTE),
  DS_APELIDO                     VARCHAR2(60 BYTE),
  IE_NF_CORREIO                  VARCHAR2(15 BYTE),
  CD_MUNICIPIO_IBGE              VARCHAR2(6 BYTE),
  NM_ABREVIADO                   VARCHAR2(80 BYTE),
  NR_DDD_CELULAR                 VARCHAR2(3 BYTE),
  NR_DDI_CELULAR                 VARCHAR2(3 BYTE),
  DS_EMPRESA_PF                  VARCHAR2(255 BYTE),
  NR_PASSAPORTE                  VARCHAR2(255 BYTE),
  CD_TIPO_PJ                     NUMBER(3),
  DS_CATEGORIA_CNH               VARCHAR2(30 BYTE),
  IE_FORNECEDOR                  VARCHAR2(1 BYTE),
  DS_MUNICIPIO_NASC_ESTRANGEIRO  VARCHAR2(255 BYTE),
  NM_USUARIO_APROV               VARCHAR2(15 BYTE),
  DT_APROVACAO                   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFPRCAD_EMPRESA_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_ESTABEL_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_MEDICO_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_NACIONA_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_NACIONALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFPRCAD_PK ON TASY.PESSOA_FISICA_PRECAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_PROPREC_FK_I ON TASY.PESSOA_FISICA_PRECAD
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_SUSMUNI_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFPRCAD_TIPPEJU_FK_I ON TASY.PESSOA_FISICA_PRECAD
(CD_TIPO_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_PRECAD_tp  after update ON TASY.PESSOA_FISICA_PRECAD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NACIONALIDADE,1,4000),substr(:new.CD_NACIONALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_NACIONALIDADE',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PJ,1,4000),substr(:new.CD_TIPO_PJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PJ',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_APELIDO,1,4000),substr(:new.DS_APELIDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_APELIDO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMPRESA_PF,1,4000),substr(:new.DS_EMPRESA_PF,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMPRESA_PF',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_EMISSOR_CI,1,4000),substr(:new.DS_ORGAO_EMISSOR_CI,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_EMISSOR_CI',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAU_INSTRUCAO,1,4000),substr(:new.IE_GRAU_INSTRUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_INSTRUCAO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_CORREIO,1,4000),substr(:new.IE_NF_CORREIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_CORREIO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PESSOA,1,4000),substr(:new.IE_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PESSOA',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ABREVIADO,1,4000),substr(:new.NM_ABREVIADO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ABREVIADO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA_SEM_ACENTO,1,4000),substr(:new.NM_PESSOA_FISICA_SEM_ACENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA_SEM_ACENTO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_CELULAR,1,4000),substr(:new.NR_DDD_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_CELULAR,1,4000),substr(:new.NR_DDI_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_CELULAR',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PASSAPORTE,1,4000),substr(:new.NR_PASSAPORTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_PASSAPORTE',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'PESSOA_FISICA_PRECAD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_FISICA_PRECAD ADD (
  CONSTRAINT PFPRCAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PESSOA_FISICA_PRECAD ADD (
  CONSTRAINT PFPRCAD_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PFPRCAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PFPRCAD_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PFPRCAD_NACIONA_FK 
 FOREIGN KEY (CD_NACIONALIDADE) 
 REFERENCES TASY.NACIONALIDADE (CD_NACIONALIDADE),
  CONSTRAINT PFPRCAD_PROPREC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_PRE_CADASTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFPRCAD_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PFPRCAD_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PJ) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA));

GRANT SELECT ON TASY.PESSOA_FISICA_PRECAD TO NIVEL_1;

