ALTER TABLE TASY.PRESCR_SOLUCAO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_SOLUCAO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_SOLUCAO_EVENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_PRESCRICAO           NUMBER(14),
  NR_SEQ_SOLUCAO          NUMBER(6),
  IE_TIPO_DOSAGEM         VARCHAR2(5 BYTE),
  QT_DOSAGEM              NUMBER(15,4),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  IE_ALTERACAO            NUMBER(3)             NOT NULL,
  DT_ALTERACAO            DATE                  NOT NULL,
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  QT_VOL_INFUNDIDO        NUMBER(15,4),
  QT_VOL_DESPREZADO       NUMBER(15,4),
  IE_EVENTO_VALIDO        VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_MOTIVO           NUMBER(10),
  NR_SEQ_MATERIAL         NUMBER(6),
  IE_TIPO_SOLUCAO         NUMBER(1)             NOT NULL,
  NR_SEQ_PROCEDIMENTO     NUMBER(6),
  NR_SEQ_NUT              NUMBER(10),
  NR_SEQ_NUT_NEO          NUMBER(10),
  IE_FORMA_INFUSAO        VARCHAR2(1 BYTE),
  DS_DURACAO              VARCHAR2(20 BYTE),
  QT_VOL_RESTANTE         NUMBER(15,2),
  DT_PREV_TERMINO         DATE,
  QT_VOL_PARCIAL          NUMBER(15,2),
  QT_VOLUME_FASE          NUMBER(15,2),
  NR_ATENDIMENTO          NUMBER(10),
  QT_VOLUME_PARCIAL       NUMBER(15,4),
  DS_JUSTIFICATIVA        VARCHAR2(2000 BYTE),
  DT_PREV_TERMINO_ETAPA   DATE,
  NR_SEQ_ESQUEMA          NUMBER(10),
  DT_APRAZAMENTO          DATE,
  DT_APRAZAMENTO_ORIG     DATE,
  NR_ETAPA_EVENTO         NUMBER(3),
  IE_STATUS_ANTERIOR      VARCHAR2(5 BYTE),
  DT_INICIO_PCA           DATE,
  DT_FIM_PCA              DATE,
  NR_DISPARO_PCA          NUMBER(5),
  NR_DISPARO_EFETIVO      NUMBER(5),
  NR_SEQ_NUT_ATEND        NUMBER(10),
  IE_TIPO_ANALGESIA       VARCHAR2(15 BYTE),
  IE_PCA_MODO_PROG        VARCHAR2(15 BYTE),
  QT_VOL_INFUSAO_PCA      NUMBER(10,1),
  IE_UM_FLUXO_PCA         VARCHAR2(15 BYTE),
  QT_BOLUS_PCA            NUMBER(10,1),
  IE_UM_BOLUS_PCA         VARCHAR2(15 BYTE),
  QT_INTERVALO_BLOQUEIO   NUMBER(10),
  QT_DOSE_INICIAL_PCA     NUMBER(10,1),
  IE_UM_DOSE_INICIO_PCA   VARCHAR2(15 BYTE),
  QT_LIMITE_QUATRO_HORA   NUMBER(10,1),
  IE_UM_LIMITE_PCA        VARCHAR2(15 BYTE),
  QT_LIMITE_UMA_HORA      NUMBER(10,1),
  IE_UM_LIMITE_HORA_PCA   VARCHAR2(15 BYTE),
  QT_DOSE_TERAPEUTICA     NUMBER(15,4),
  NR_UNID_TERAPEUTICA     NUMBER(10),
  NR_SEQ_RESERVA          NUMBER(15),
  NR_SEQ_ITEM_RESERVA     NUMBER(10),
  NR_SEQ_PROD_RESERVA     NUMBER(10),
  QT_TEMPO_INFUSAO        NUMBER(15,4),
  NR_SEQ_ASSINATURA       NUMBER(15),
  NR_SEQ_LOTE             NUMBER(10),
  QT_MIN_PAUSA            NUMBER(5),
  DT_HORARIO              DATE,
  CD_FUNCAO               NUMBER(10),
  NR_SEQ_MOT_LOTE_GEDIPA  NUMBER(10),
  NR_SEQ_TIPO_OBS         NUMBER(10),
  IE_MOSTRA_ADEP          VARCHAR2(1 BYTE),
  IE_ALTE                 NUMBER(10),
  DS_STACK                VARCHAR2(2000 BYTE),
  NR_SEQ_MOTIVO_ADM       NUMBER(10),
  CD_EVOLUCAO             NUMBER(10),
  NM_USUARIO_CONF         VARCHAR2(15 BYTE),
  NR_SEQ_CPOE             NUMBER(10),
  NR_SEQ_MOTIVO_SUSP      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRSOLEV_ADEMOIN_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRSOLEV_ADEMOIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRSOLEV_ADETIOB_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_TIPO_OBS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_ATEPACI_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_EVOPACI_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_I1 ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_MOGELOG_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_MOT_LOTE_GEDIPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRSOLEV_MOGELOG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRSOLEV_MOTSUPR_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_NUTASED_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_NUT_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRSOLEV_NUTASED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRSOLEV_NUTPACI_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_NUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_NUTPACT_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_NUT_NEO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_PESFISI_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRSOLEV_PK ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_PRESMAT_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_PRESCRICAO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_PRESOES_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRSOLEV_PRESOES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRSOLEV_PRESOLU_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_PRESCRICAO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRSOLEV_PRESPRO_FK_I ON TASY.PRESCR_SOLUCAO_EVENTO
(NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_solucao_evento_insert
after INSERT ON TASY.PRESCR_SOLUCAO_EVENTO FOR EACH ROW
DECLARE

cd_setor_atendimento_w		number(15,0);
nr_seq_atend_w			number(15,0);
nr_seq_tipo_w			number(15,0);
cd_perfil_w			    number(5);
ie_todas_gp_w			varchar2(2);
dt_medicao_w			Date;
nr_seq_derivado_w		prescr_procedimento.nr_seq_derivado%type;
ie_bomba_infusao_w		prescr_sol_perda_ganho.ie_bomba_infusao%type;

cursor c01 is
select	nr_seq_tipo
from	prescr_sol_perda_ganho
where	ie_evento		= :new.ie_alteracao
and	nvl(ie_tipo_solucao,1)	= :new.ie_tipo_solucao
and	nvl(ie_gerar_evento_solucao,'S') = 'S'
and	((cd_material is null) or (cd_material in(	select	a.cd_material
							from	prescr_material a
							where	a.nr_sequencia_solucao	= :new.nr_seq_solucao
							and	a.nr_prescricao		= :new.nr_prescricao)))
and	((cd_setor_Atendimento is null) or
	 (cd_setor_Atendimento	= cd_setor_atendimento_w))
and	((cd_perfil is null) or
	 (cd_perfil = cd_perfil_w))
and	((ie_bomba_infusao is null) or
	 (ie_bomba_infusao = ie_bomba_infusao_w))
and	((nvl(nr_seq_derivado,0) = 0) or
	(nvl(ie_tipo_solucao,1) <> 3) or
    ((nvl(ie_tipo_solucao,1) = 3) and (nvl(nr_seq_derivado,nr_seq_derivado_w) = nr_seq_derivado_w)));

BEGIN

if	(:new.ie_tipo_solucao = 3) and (:new.nr_seq_procedimento is not null) then
	select	nvl(max(nr_seq_derivado),0)
	into	nr_seq_derivado_w
	from	prescr_procedimento
	where	nr_prescricao = :new.nr_prescricao
	and		nr_sequencia = :new.nr_seq_procedimento;
end if;

if	(:new.ie_tipo_solucao = 1) then
	begin
	update	prescr_solucao
	set	nr_seq_evento	= :new.nr_sequencia
	where	nr_prescricao	= :new.nr_prescricao
	and	nr_seq_solucao	= :new.nr_seq_solucao;
	end;

	select	max(ie_bomba_infusao)
	into	ie_bomba_infusao_w
	from	prescr_solucao
	where	nr_prescricao	= :new.nr_prescricao
	and		nr_seq_solucao	= :new.nr_seq_solucao;

end if;

if	(:new.qt_volume_parcial is not null) and
	(:new.nr_atendimento is not null) then

	Obter_Param_Usuario(1113,403,obter_perfil_ativo,:new.nm_usuario,0,ie_todas_gp_w);

	select	max(cd_setor_atendimento),
			max(cd_perfil_ativo)
	into	cd_setor_atendimento_w,
			cd_perfil_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;

	dt_medicao_w := :new.dt_alteracao;

	if	(ie_todas_gp_w = 'N') then

		select	max(nr_seq_tipo)
		into	nr_seq_tipo_w
		from	prescr_sol_perda_ganho
		where	ie_evento		= :new.ie_alteracao
		and	nvl(ie_tipo_solucao,1)	= :new.ie_tipo_solucao
		and	nvl(ie_gerar_evento_solucao,'S') = 'S'
		and	((cd_material is null) or (cd_material in(	select	a.cd_material
									from	prescr_material a
									where	a.nr_sequencia_solucao	= :new.nr_seq_solucao
									and	a.nr_prescricao		= :new.nr_prescricao)))
		and	((cd_setor_Atendimento is null) or
			 (cd_setor_Atendimento	= cd_setor_atendimento_w))
		and	((cd_perfil is null) or
			 (cd_perfil = cd_perfil_w))
		and	((nvl(nr_seq_derivado,0) = 0) or
			(nvl(ie_tipo_solucao,1) <> 3) or
			((nvl(ie_tipo_solucao,1) = 3) and (nvl(nr_seq_derivado,nr_seq_derivado_w) = nr_seq_derivado_w)));

		if	(nr_seq_tipo_w is not null) then

			select	atendimento_perda_ganho_seq.nextval
			into	nr_seq_atend_w
			from	dual;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				ds_observacao,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_evento_adep)
			values	(nr_seq_atend_w,
				:new.nr_atendimento,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				nvl(:new.qt_volume_parcial, :new.qt_vol_infundido),
				:new.ds_observacao,
				nvl(dt_medicao_w,sysdate),
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_fisica,
				'A',
				sysdate,
				nvl(dt_medicao_w,sysdate),
				1,
				:new.nr_sequencia);
		end if;
	else

		open C01;
		loop
		fetch C01 into
			nr_seq_tipo_w;
		exit when C01%notfound;

			select	atendimento_perda_ganho_seq.nextval
			into	nr_seq_atend_w
			from	dual;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				ds_observacao,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_evento_adep)
			values	(nr_seq_atend_w,
				:new.nr_atendimento,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				nvl(:new.qt_volume_parcial, :new.qt_vol_infundido),
				:new.ds_observacao,
				nvl(dt_medicao_w,sysdate),
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_fisica,
				'A',
				sysdate,
				nvl(dt_medicao_w,sysdate),
				1,
				:new.nr_sequencia);
		end loop;
		close C01;
	end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_solucao_evento_Update
BEFORE UPDATE ON TASY.PRESCR_SOLUCAO_EVENTO FOR EACH ROW
DECLARE

ie_status_w	varchar2(5);
BEGIN

if ((nvl(:old.ie_evento_valido,'S') = 'S') and
  (nvl(:new.ie_evento_valido,'S') = 'N')) then

	update EVOLUCAO_PACIENTE
	set ie_situacao 	= 'I',
		dt_inativacao 	= sysdate,
		nm_usuario_inativacao = :new.nm_usuario
	where cd_evolucao 	= :old.cd_evolucao
	and ie_situacao 	= 'A';
end if;

if	(:new.ie_evento_valido	= 'N') then
	update	atendimento_perda_ganho
	set	dt_inativacao		= sysdate,
		nm_usuario_inativacao	= :new.nm_usuario,
    ie_situacao = 'I'
	where	nr_seq_evento_Adep	= :new.nr_sequencia;
end if;

if	(:old.ie_evento_valido	= 'S') and --So executar se estiver inativando.
	(:new.ie_evento_valido	= 'N') and
	(:new.ie_tipo_solucao	=  2 ) and
	(:new.nr_seq_nut_atend is not null) then

	Select	decode(:new.ie_alteracao, 1, 'I', 2, 'INT', 3, 'I', 4, 'T')
	into	ie_status_w
	from	dual;

	update	nut_atend_serv_dia
	set	ie_status_adep	= decode(ie_status_w,	'T', 'I',
						      'INT', 'I',
							'I', 'N',
							'N'),
		nm_usuario_adm	= null,
		dt_fim_horario	= null
	where	nr_sequencia	= :new.nr_seq_nut_atend;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_solucao_evento_befins
BEFORE INSERT ON TASY.PRESCR_SOLUCAO_EVENTO FOR EACH ROW
DECLARE

nr_atendimento_w	number(10,0);

BEGIN

if	(:new.ie_alteracao not in (1,2,3,4,5,9,12,13,16,24,25,26,27,28,31,33,37,57)) and
	(:new.ds_observacao is not null) then
	:new.ds_observacao := null;
end if;

select	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = :new.nr_prescricao;

if	(nr_atendimento_w > 0) then
	:new.nr_atendimento := nr_atendimento_w;
end if;

if	(:new.cd_funcao is null) then
	:new.cd_funcao	:= obter_funcao_ativa;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_solucao_evento_atual
BEFORE INSERT OR UPDATE ON TASY.PRESCR_SOLUCAO_EVENTO FOR EACH ROW
BEGIN

if	(nvl(:old.nr_sequencia,0) = 0) then
	:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);
end if;


if	(   nvl(:new.nr_seq_cpoe, :old.nr_seq_cpoe) is null
		and nvl(:new.ie_tipo_solucao, :old.ie_tipo_solucao) = 1 ) then

    :new.nr_seq_cpoe	:= obter_nr_seq_cpoe_sol(
				nr_prescricao_p     =>  nvl(:new.nr_prescricao, :old.nr_prescricao),
				nr_seq_solucao_p    =>  nvl(:new.nr_seq_solucao, :old.nr_seq_solucao)
				);
end if;

if	( coalesce(:new.qt_vol_restante, :old.qt_vol_restante, 0) < 0 ) then
    :new.qt_vol_restante    :=  0;
end if;

END;
/


ALTER TABLE TASY.PRESCR_SOLUCAO_EVENTO ADD (
  CONSTRAINT PRSOLEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_SOLUCAO_EVENTO ADD (
  CONSTRAINT PRSOLEV_ADETIOB_FK 
 FOREIGN KEY (NR_SEQ_TIPO_OBS) 
 REFERENCES TASY.ADEP_TIPO_OBSERVACAO (NR_SEQUENCIA),
  CONSTRAINT PRSOLEV_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PRSOLEV_MOGELOG_FK 
 FOREIGN KEY (NR_SEQ_MOT_LOTE_GEDIPA) 
 REFERENCES TASY.MOTIVO_GERACAO_LOTE_GEDIPA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_ADEMOIN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.ADEP_MOTIVO_INTERRUPCAO (NR_SEQUENCIA),
  CONSTRAINT PRSOLEV_PRESOLU_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PRESCR_SOLUCAO (NR_PRESCRICAO,NR_SEQ_SOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRSOLEV_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_NUTPACI_FK 
 FOREIGN KEY (NR_SEQ_NUT) 
 REFERENCES TASY.NUT_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PRSOLEV_NUTPACT_FK 
 FOREIGN KEY (NR_SEQ_NUT_NEO) 
 REFERENCES TASY.NUT_PAC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRSOLEV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PRSOLEV_PRESOES_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PRESCR_SOLUCAO_ESQUEMA (NR_SEQUENCIA),
  CONSTRAINT PRSOLEV_NUTASED_FK 
 FOREIGN KEY (NR_SEQ_NUT_ATEND) 
 REFERENCES TASY.NUT_ATEND_SERV_DIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_SOLUCAO_EVENTO TO NIVEL_1;

