ALTER TABLE TASY.TISS_CAMPO_ESP_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_CAMPO_ESP_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_CAMPO_ESP_COMPL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  DS_CAMPO             VARCHAR2(255 BYTE),
  DS_ANTERIOR          VARCHAR2(100 BYTE),
  DS_POSTERIOR         VARCHAR2(100 BYTE),
  NR_SEQ_APRESENTACAO  NUMBER(10),
  VL_PADRAO            VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TISSCEC_PK ON TASY.TISS_CAMPO_ESP_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCEC_PK
  MONITORING USAGE;


CREATE INDEX TASY.TISSCEC_TISSRCE_FK_I ON TASY.TISS_CAMPO_ESP_COMPL
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_CAMPO_ESP_COMPL_tp  after update ON TASY.TISS_CAMPO_ESP_COMPL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_CAMPO,1,500);gravar_log_alteracao(substr(:old.DS_CAMPO,1,4000),substr(:new.DS_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMPO',ie_log_w,ds_w,'TISS_CAMPO_ESP_COMPL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ANTERIOR,1,500);gravar_log_alteracao(substr(:old.DS_ANTERIOR,1,4000),substr(:new.DS_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_ANTERIOR',ie_log_w,ds_w,'TISS_CAMPO_ESP_COMPL',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_PADRAO,1,500);gravar_log_alteracao(substr(:old.VL_PADRAO,1,4000),substr(:new.VL_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PADRAO',ie_log_w,ds_w,'TISS_CAMPO_ESP_COMPL',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_APRESENTACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'TISS_CAMPO_ESP_COMPL',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_POSTERIOR,1,500);gravar_log_alteracao(substr(:old.DS_POSTERIOR,1,4000),substr(:new.DS_POSTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_POSTERIOR',ie_log_w,ds_w,'TISS_CAMPO_ESP_COMPL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_CAMPO_ESP_COMPL ADD (
  CONSTRAINT TISSCEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_CAMPO_ESP_COMPL ADD (
  CONSTRAINT TISSCEC_TISSRCE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.TISS_REGRA_CAMPO_ESP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TISS_CAMPO_ESP_COMPL TO NIVEL_1;

