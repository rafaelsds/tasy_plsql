ALTER TABLE TASY.ORCAMENTO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORCAMENTO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORCAMENTO_PACIENTE
(
  NR_SEQUENCIA_ORCAMENTO   NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  DT_ORCAMENTO             DATE                 NOT NULL,
  CD_CONVENIO              NUMBER(5)            NOT NULL,
  CD_CATEGORIA             VARCHAR2(10 BYTE)    NOT NULL,
  CD_CONDICAO_PAGAMENTO    NUMBER(10)           NOT NULL,
  IE_STATUS_ORCAMENTO      NUMBER(2)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_VALIDADE              DATE,
  NR_ATENDIMENTO           NUMBER(10),
  CD_MOTIVO_CANCELAMENTO   VARCHAR2(3 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  DT_APROVACAO             DATE,
  NM_USUARIO_APROVACAO     VARCHAR2(15 BYTE),
  VL_DESCONTO              NUMBER(15,2),
  CD_TABELA_CUSTO          NUMBER(5),
  DT_PREVISTA_EXEC         DATE,
  VL_DEPOSITO              NUMBER(15,2)         NOT NULL,
  CD_TIPO_ACOMODACAO       NUMBER(5),
  QT_DIA_INTERNACAO        NUMBER(5),
  QT_MIN_CC                NUMBER(5),
  IE_CALCULO_VALOR         VARCHAR2(1 BYTE)     NOT NULL,
  PR_CALCULO               NUMBER(15,4),
  CD_PLANO                 VARCHAR2(10 BYTE),
  IE_TIPO_ORCAMENTO        NUMBER(2),
  DS_SOLICITANTE           VARCHAR2(255 BYTE),
  NR_SAC                   VARCHAR2(45 BYTE),
  CD_MEDICO                VARCHAR2(10 BYTE),
  PR_DESCONTO              NUMBER(5,2),
  CD_USUARIO_CONVENIO      VARCHAR2(30 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_TIPO_ATENDIMENTO      NUMBER(3),
  IE_TIPO_ANESTESIA        VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  NR_DOC_GUIA              VARCHAR2(30 BYTE),
  QT_DIAS_UTI              NUMBER(10),
  CD_DOENCA_CID            VARCHAR2(10 BYTE),
  QT_ALTURA                NUMBER(3),
  QT_PESO                  NUMBER(6,3),
  QT_MG_CARBOPLATINA       NUMBER(15,4),
  NR_CICLOS                NUMBER(3),
  QT_AUC                   NUMBER(4,1),
  QT_CREATININA            NUMBER(5,2),
  QT_CLEARANCE_CREATININA  NUMBER(14,2),
  NR_CICLO_ATUAL           NUMBER(10),
  QT_REDUTOR_SC            NUMBER(15,2),
  CD_PROCEDENCIA           NUMBER(5),
  IE_CLINICA               NUMBER(5),
  CD_PESSOA_RESPONSAVEL    VARCHAR2(10 BYTE),
  QT_SUPERF_CORP           NUMBER(15,4),
  DT_CANCELAMENTO          DATE,
  NR_SEQ_AGEINT            NUMBER(10),
  NR_SEQ_TABELA_CUSTO      NUMBER(10),
  IE_CARATER_INTER_SUS     VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO     NUMBER(10),
  CD_EXTERNO               VARCHAR2(10 BYTE),
  DS_DIAS_APLIC_FILTRO     VARCHAR2(255 BYTE),
  PR_CALCULO_MAT           NUMBER(15,4),
  PR_CALCULO_PROC          NUMBER(15,4),
  NM_PESSOA_ORCAMENTO      VARCHAR2(60 BYTE),
  DT_NASCIMENTO            DATE,
  IE_SEXO                  VARCHAR2(1 BYTE),
  NR_TELEFONE              VARCHAR2(40 BYTE),
  NR_SEQ_MOTIVO_DESC       NUMBER(10),
  NR_DDI_TELEFONE          VARCHAR2(3 BYTE),
  NR_PRESCRICAO            NUMBER(14),
  NR_SEQ_AGECONS           NUMBER(10),
  DT_RETORNO_COTACAO       DATE,
  IE_NEGOCIADO             VARCHAR2(1 BYTE),
  IE_STATUS_CIRURGIA       NUMBER(10),
  DT_COTACAO               DATE,
  CD_SETOR_RESP            NUMBER(5),
  DS_CICLOS_APLIC_FILTRO   VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCPACI_AGEINTE_FK_I ON TASY.ORCAMENTO_PACIENTE
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_ATEPACI_FK_I ON TASY.ORCAMENTO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_CATCONV_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_CIDDOEN_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_CLAATEN_FK_I ON TASY.ORCAMENTO_PACIENTE
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_CONPAGA_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_CONPLAN_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_ESTABEL_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_MOTDEOR_FK_I ON TASY.ORCAMENTO_PACIENTE
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_MOTDEOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_PESFISI_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_PESFISI_FK2_I ON TASY.ORCAMENTO_PACIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_PESFISI_FK3_I ON TASY.ORCAMENTO_PACIENTE
(CD_PESSOA_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCPACI_PK ON TASY.ORCAMENTO_PACIENTE
(NR_SEQUENCIA_ORCAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_PRESMED_FK_I ON TASY.ORCAMENTO_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_PROCEDE_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_SETATEN_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_SETATEN_FK2_I ON TASY.ORCAMENTO_PACIENTE
(CD_SETOR_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPACI_SUSCAIN_FK_I ON TASY.ORCAMENTO_PACIENTE
(IE_CARATER_INTER_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_SUSCAIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_TABCUST_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_TABCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_TABCUST_FK_I2 ON TASY.ORCAMENTO_PACIENTE
(NR_SEQ_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_TABCUST_FK_I2
  MONITORING USAGE;


CREATE INDEX TASY.ORCPACI_TIPACOM_FK_I ON TASY.ORCAMENTO_PACIENTE
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPACI_TIPACOM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ORCAMENTO_PACIENTE_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ORCAMENTO_PACIENTE FOR EACH ROW
declare

BEGIN

:new.qt_superf_corp:= round(obter_superficie_corp_red_ped(:new.qt_peso, :new.qt_altura, nvl(:new.qt_redutor_sc,0), :new.cd_pessoa_fisica, :new.nm_usuario),3);

if	(:new.CD_MOTIVO_CANCELAMENTO is not null) then
	:new.dt_cancelamento:= sysdate;
end if;

if	(:new.CD_MOTIVO_CANCELAMENTO is null) then
	:new.dt_cancelamento:= null;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.ORCAMENTO_PACIENTE_UPDATE
AFTER UPDATE ON TASY.ORCAMENTO_PACIENTE FOR EACH ROW
declare

Cursor C01 is
	select	nm_usuario_envio,
		cd_perfil
	from	REGRA_ENVIO_CI_ORCAMENTO
	where	ie_status_orcamento = :new.ie_status_orcamento
	order by nm_usuario_envio,
		cd_perfil;

Cursor C02 is
	select	distinct
		cd_protocolo,
		cd_material
	from 	orcamento_paciente_mat
	where	nr_sequencia_orcamento = :new.nr_sequencia_orcamento
	and	cd_protocolo is not null
	order by cd_protocolo;

nm_usuario_envio_w            	varchar2(15);
cd_perfil_w                     number(5);
ds_erro_w			varchar2(2000);
cd_material_w			number(6);
cd_protocolo_w			number(10);
cd_material_ww			varchar2(2000);
cd_protocolo_ww			varchar2(2000);
qt_reg_w			number(1);
varAlerta_w			varchar2(1);
ds_alerta_w			varchar2(255);
nr_seq_alerta_w			number(10,0);
vl_orcamento_w			number(15,2);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

varAlerta_w:= nvl(Obter_Valor_Param_Usuario(106,69,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento),'N');

if	(varAlerta_w = 'S') and
	(nvl(:old.nr_atendimento,0) <> nvl(:new.nr_atendimento,0)) and
	(nvl(:old.nr_atendimento,0) = 0) then

	obter_valor_orcamento(:new.nr_sequencia_orcamento, vl_orcamento_w);

	ds_alerta_w 	:= 'Existe(m) Or�amento(s) vinculado(s) � esse atendimento';

	ds_alerta_w 	:= ds_alerta_w 	||chr(13) ||chr(10)	||'Seq. Or�amento: '||:new.nr_sequencia_orcamento;
	ds_alerta_w 	:= ds_alerta_w 	|| chr(13)||chr(10) 	||'Usu�rio que realizou a vincula��o: '|| wheb_usuario_pck.get_nm_usuario;
	ds_alerta_w 	:= ds_alerta_w 	|| chr(13)||chr(10)	||'Data da vincula��o: '||sysdate;
	ds_alerta_w	:= ds_alerta_w	|| chr(13)||chr(10)	||'Valor do Or�amento: '|| vl_orcamento_w;

	select	atendimento_alerta_seq.nextval
	into	nr_seq_alerta_w
	from	dual;

	insert into atendimento_alerta 	( 	nr_sequencia,
						nr_atendimento,
						dt_alerta,
						dt_atualizacao,
						nm_usuario,
						ds_alerta,
						ie_situacao,
						dt_fim_alerta)
				values 	(	nr_seq_alerta_w,
						:new.nr_atendimento,
						sysdate,
						sysdate,
						:new.nm_usuario,
						substr(ds_alerta_w,1,2000),
						'A',
						null);
end if;

IF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 1) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Em Aprova��o',
		sysdate);

ELSIF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 2) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Or�amento Aprovado',
		sysdate);

ELSIF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 3) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Cancelado p/ Paciente',
		sysdate);

ELSIF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 4) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Cancelado p/ Estabelecimento',
		sysdate);

ELSIF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 5) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Aguardando cota��o',
		sysdate);

ELSIF	(:old.IE_STATUS_ORCAMENTO <> :new.IE_STATUS_ORCAMENTO) and
	(:new.IE_STATUS_ORCAMENTO = 6) THEN

	insert into orcamento_historico
		(nr_sequencia,
		nr_sequencia_orcamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_historico,
		ds_historico,
		dt_liberacao)
	values (orcamento_historico_seq.nextVal,
		:new.nr_sequencia_orcamento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		'Aguardando documenta��o',
		sysdate);

END IF;

if	(nvl(:old.ie_status_orcamento,0) <> nvl(:new.ie_status_orcamento,0)) then

	open C01;
	loop
	fetch C01 into
		nm_usuario_envio_w,
		cd_perfil_w;
	exit when C01%notfound;
		begin

		cd_protocolo_ww	:= '';
		cd_material_ww	:= '';
		open C02;
		loop
		fetch C02 into
			cd_protocolo_w,
			cd_material_w;
		exit when C02%notfound;
			begin

			cd_protocolo_ww	:= cd_protocolo_ww || cd_protocolo_w || ', ';
			cd_material_ww := cd_material_ww || cd_material_w || ', ';

			end;
		end loop;
		close C02;

		cd_protocolo_ww	:= substr(cd_protocolo_ww,1, length(cd_protocolo_ww) -2);
		cd_material_ww	:= substr(cd_material_ww,1, length(cd_material_ww) -2);

		begin
		insert into comunic_interna (
			ds_titulo,
			ds_comunicado,
			cd_perfil,
			nm_usuario_destino,
			dt_atualizacao,
			dt_comunicado,
			ie_gerencial,
			nm_usuario,
			nr_sequencia,
			ie_geral,
			dt_liberacao)
		values	(
			'Solicita��o de aprova��o para o or�amento ' || :new.nr_sequencia_orcamento,
			'Seq. Or�amento: ' || :new.nr_sequencia_orcamento || chr(13) ||
			'Paciente: ' || substr(obter_nome_pf(:new.cd_pessoa_fisica ),1,60) || chr(13) ||
			'Conv�nio: ' || substr(obter_nome_convenio(:new.cd_convenio),1,60) || chr(13) ||
			'Categoria:' || substr(obter_categoria_convenio(:new.cd_convenio, :new.cd_categoria),1,60) || chr(13) ||
			'Plano: '  || substr(obter_plano_convenio(:new.cd_convenio, :new.cd_plano),1,60) || chr(13) ||
			'Protocolo: ' || cd_protocolo_ww || chr(13) ||
			'Medica��o: ' || cd_material_ww,
			cd_perfil_w,
			nm_usuario_envio_w,
			sysdate,
			sysdate,
			'N',
			:new.nm_usuario,
			comunic_interna_seq.nextval,
			'N',
			sysdate);
		exception
			when others then
			ds_erro_w	:= '';
		end;

		end;
	end loop;
	close C01;

end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.ORCAMENTO_PACIENTE_tp  after update ON TASY.ORCAMENTO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA_ORCAMENTO);  ds_c_w:=null; ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CARATER_INTER_SUS,1,500);gravar_log_alteracao(substr(:old.IE_CARATER_INTER_SUS,1,4000),substr(:new.IE_CARATER_INTER_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARATER_INTER_SUS',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PLANO,1,4000),substr(:new.CD_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORCAMENTO_PACIENTE ADD (
  CONSTRAINT ORCPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA_ORCAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORCAMENTO_PACIENTE ADD (
  CONSTRAINT ORCPACI_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_RESP) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORCPACI_MOTDEOR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO_ORCAMENTO (NR_SEQUENCIA),
  CONSTRAINT ORCPACI_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA),
  CONSTRAINT ORCPACI_TABCUST_FK 
 FOREIGN KEY (NR_SEQ_TABELA_CUSTO) 
 REFERENCES TASY.TABELA_CUSTO (NR_SEQUENCIA),
  CONSTRAINT ORCPACI_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT ORCPACI_SUSCAIN_FK 
 FOREIGN KEY (IE_CARATER_INTER_SUS) 
 REFERENCES TASY.SUS_CARATER_INTERNACAO (CD_CARATER_INTERNACAO),
  CONSTRAINT ORCPACI_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT ORCPACI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORCPACI_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT ORCPACI_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORCPACI_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT ORCPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ORCPACI_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT ORCPACI_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT ORCPACI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ORCPACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORCPACI_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT ORCPACI_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ORCPACI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO));

GRANT SELECT ON TASY.ORCAMENTO_PACIENTE TO NIVEL_1;

