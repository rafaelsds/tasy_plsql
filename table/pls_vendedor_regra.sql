ALTER TABLE TASY.PLS_VENDEDOR_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_VENDEDOR_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_VENDEDOR_REGRA
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_VENDEDOR               NUMBER(10)      NOT NULL,
  IE_COMISSAO                   VARCHAR2(2 BYTE),
  NR_PARCELA_INICIAL            NUMBER(5)       NOT NULL,
  NR_PARCELA_FINAL              NUMBER(5)       NOT NULL,
  IE_RECEBE_VALORES             VARCHAR2(1 BYTE) NOT NULL,
  IE_RECEBE_SEM_PAGTO           VARCHAR2(1 BYTE) NOT NULL,
  VL_PREMIO                     NUMBER(15,2),
  TX_PREMIO                     NUMBER(7,4),
  TX_JUROS                      NUMBER(7,4),
  TX_MULTA                      NUMBER(7,4),
  DT_INICIO_VIGENCIA            DATE            NOT NULL,
  DT_FIM_VIGENCIA               DATE,
  IE_ACAO_CONTRATO              VARCHAR2(2 BYTE),
  IE_TIPO_ITEM_MENSALIDADE      VARCHAR2(50 BYTE),
  IE_TIPO_PESSOA                VARCHAR2(3 BYTE),
  IE_APROVACAO_BENEF_ATIVO      VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_INCLUSAO        NUMBER(10),
  IE_BONIFICACAO                VARCHAR2(2 BYTE),
  NR_SEQ_PLANO                  NUMBER(10),
  NR_SEQ_SCA                    NUMBER(10),
  NR_PARCELA_INICIAL_SCA        NUMBER(10),
  NR_PARCELA_FINAL_SCA          NUMBER(10),
  QT_IDADE_INICIAL              NUMBER(5),
  QT_IDADE_FINAL                NUMBER(5),
  NR_SEQ_GRUPO_PRODUTO          NUMBER(10),
  IE_PRIMEIRA_PARC_INTEGRAL     VARCHAR2(1 BYTE),
  IE_CALCULO_GRUPO              VARCHAR2(1 BYTE),
  QT_DIAS_CONTRATO              NUMBER(10),
  QT_DIAS_CONTRATO_FIM          NUMBER(5),
  IE_PORTABILIDADE              VARCHAR2(1 BYTE),
  IE_DIFERENCA_MIGRACAO         VARCHAR2(1 BYTE),
  QT_MESES_META                 NUMBER(10),
  NR_CONTRATO                   NUMBER(10),
  NR_SEQ_CONTRATO               NUMBER(10),
  IE_VALOR_EMBUTIDO_SCA         VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_CONTRATO         NUMBER(10),
  DS_REGRA                      VARCHAR2(255 BYTE),
  IE_VL_INTEGRAL_PRIMEIRA_MENS  VARCHAR2(1 BYTE),
  IE_DIF_ALTERACAO_PROD         VARCHAR2(1 BYTE),
  NR_PARCELA_INI_ADAPT          NUMBER(5),
  NR_PARCELA_FIN_ADAPT          NUMBER(5),
  QT_DIAS_TOLERANCIA_VOLTA      NUMBER(5),
  IE_MIGRACAO_VALOR_SUPERIOR    VARCHAR2(1 BYTE),
  IE_PARCELA_RETROATIVA         VARCHAR2(1 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE),
  IE_CONSIDERAR_CARENCIAS       VARCHAR2(1 BYTE),
  QT_DIAS_ATE_REATIVACAO        NUMBER(5),
  IE_INCREMENTO_RESTRITIVO      VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_RELAC            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSVERE_PK ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVERE_PLSGRCO_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_GRUPO_RELAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVERE_PLSMOISE_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_MOTIVO_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVERE_PLSMOISE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVERE_PLSPGRP_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_GRUPO_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVERE_PLSPGRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVERE_PLSPLAN_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVERE_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVERE_PLSPLAN_FK2_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVERE_PLSPLAN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVERE_PLSPRGC_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVERE_PLSVEND_FK_I ON TASY.PLS_VENDEDOR_REGRA
(NR_SEQ_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_VENDEDOR_REGRA_tp  after update ON TASY.PLS_VENDEDOR_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MESES_META,1,4000),substr(:new.QT_MESES_META,1,4000),:new.nm_usuario,nr_seq_w,'QT_MESES_META',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_VENDEDOR,1,4000),substr(:new.NR_SEQ_VENDEDOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_VENDEDOR',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMISSAO,1,4000),substr(:new.IE_COMISSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMISSAO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELA_INICIAL,1,4000),substr(:new.NR_PARCELA_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELA_INICIAL',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELA_FINAL,1,4000),substr(:new.NR_PARCELA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELA_FINAL',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEBE_VALORES,1,4000),substr(:new.IE_RECEBE_VALORES,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEBE_VALORES',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEBE_SEM_PAGTO,1,4000),substr(:new.IE_RECEBE_SEM_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEBE_SEM_PAGTO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PREMIO,1,4000),substr(:new.VL_PREMIO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PREMIO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_PREMIO,1,4000),substr(:new.TX_PREMIO,1,4000),:new.nm_usuario,nr_seq_w,'TX_PREMIO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_JUROS,1,4000),substr(:new.TX_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'TX_JUROS',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MULTA,1,4000),substr(:new.TX_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_MULTA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACAO_CONTRATO,1,4000),substr(:new.IE_ACAO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_CONTRATO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ITEM_MENSALIDADE,1,4000),substr(:new.IE_TIPO_ITEM_MENSALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ITEM_MENSALIDADE',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PESSOA,1,4000),substr(:new.IE_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PESSOA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APROVACAO_BENEF_ATIVO,1,4000),substr(:new.IE_APROVACAO_BENEF_ATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APROVACAO_BENEF_ATIVO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_INCLUSAO,1,4000),substr(:new.NR_SEQ_MOTIVO_INCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_INCLUSAO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BONIFICACAO,1,4000),substr(:new.IE_BONIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BONIFICACAO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PLANO,1,4000),substr(:new.NR_SEQ_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PLANO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SCA,1,4000),substr(:new.NR_SEQ_SCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SCA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELA_INICIAL_SCA,1,4000),substr(:new.NR_PARCELA_INICIAL_SCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELA_INICIAL_SCA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELA_FINAL_SCA,1,4000),substr(:new.NR_PARCELA_FINAL_SCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELA_FINAL_SCA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_INICIAL,1,4000),substr(:new.QT_IDADE_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_INICIAL',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_FINAL,1,4000),substr(:new.QT_IDADE_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_FINAL',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CONTRATO,1,4000),substr(:new.QT_DIAS_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CONTRATO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIMEIRA_PARC_INTEGRAL,1,4000),substr(:new.IE_PRIMEIRA_PARC_INTEGRAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIMEIRA_PARC_INTEGRAL',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CONTRATO_FIM,1,4000),substr(:new.QT_DIAS_CONTRATO_FIM,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CONTRATO_FIM',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRODUTO,1,4000),substr(:new.NR_SEQ_GRUPO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRODUTO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CALCULO_GRUPO,1,4000),substr(:new.IE_CALCULO_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CALCULO_GRUPO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTABILIDADE,1,4000),substr(:new.IE_PORTABILIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTABILIDADE',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIFERENCA_MIGRACAO,1,4000),substr(:new.IE_DIFERENCA_MIGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIFERENCA_MIGRACAO',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_VENDEDOR_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_VENDEDOR_REGRA ADD (
  CONSTRAINT PLSVERE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_VENDEDOR_REGRA ADD (
  CONSTRAINT PLSVERE_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSMOISE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INCLUSAO) 
 REFERENCES TASY.PLS_MOTIVO_INCLUSAO_SEG (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_SCA) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSPGRP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRODUTO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSVERE_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_RELAC) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_VENDEDOR_REGRA TO NIVEL_1;

