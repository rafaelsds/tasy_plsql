ALTER TABLE TASY.CTB_REGISTRO_CONCIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGISTRO_CONCIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGISTRO_CONCIL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_MES_REF         NUMBER(10)             NOT NULL,
  DS_CONCILIACAO         VARCHAR2(255 BYTE)     NOT NULL,
  CD_ESTAB_EXCLUSIVO     NUMBER(4),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_INICIAL             DATE                   NOT NULL,
  DT_FINAL               DATE                   NOT NULL,
  CD_TIPO_LOTE_CONTABIL  NUMBER(10),
  NM_USUARIO_ABERTURA    VARCHAR2(15 BYTE),
  DT_ABERTURA            DATE,
  DT_FECHAMENTO          DATE,
  NM_USUARIO_FECHAMENTO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBRECON_CTBMERE_FK_I ON TASY.CTB_REGISTRO_CONCIL
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRECON_ESTABEL_FK_I ON TASY.CTB_REGISTRO_CONCIL
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBRECON_PK ON TASY.CTB_REGISTRO_CONCIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRECON_TIPLOCO_FK_I ON TASY.CTB_REGISTRO_CONCIL
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CTB_REGISTRO_CONCIL ADD (
  CONSTRAINT CTBRECON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGISTRO_CONCIL ADD (
  CONSTRAINT CTBRECON_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBRECON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBRECON_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL));

GRANT SELECT ON TASY.CTB_REGISTRO_CONCIL TO NIVEL_1;

