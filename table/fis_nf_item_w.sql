ALTER TABLE TASY.FIS_NF_ITEM_W
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_NF_ITEM_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_NF_ITEM_W
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ITEM_NF           NUMBER(5),
  QT_ITEM_NF           NUMBER(13,4),
  VL_UNITARIO_ITEM_NF  NUMBER(13,4),
  DS_SERVICO           VARCHAR2(255 BYTE),
  NR_SEQ_TRANSMISSAO   NUMBER(10),
  NR_SEQ_NOTA_FISCAL   NUMBER(10),
  CD_ATIVIDADE         VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.FISNFITW_PK ON TASY.FIS_NF_ITEM_W
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_NF_ITEM_W ADD (
  CONSTRAINT FISNFITW_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_NF_ITEM_W TO NIVEL_1;

