ALTER TABLE TASY.PE_REGRA_RESULT_ESCALA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_REGRA_RESULT_ESCALA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_REGRA_RESULT_ESCALA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_RESULT_ESCALA     NUMBER(10),
  NR_SEQ_REGRA             NUMBER(10),
  NR_SEQ_RESULT_SAE        NUMBER(10),
  IE_RESULT_SCORE_FLEX_II  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERERES_EIEITRE_FK_I ON TASY.PE_REGRA_RESULT_ESCALA
(IE_RESULT_SCORE_FLEX_II)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERERES_PEREITE_FK_I ON TASY.PE_REGRA_RESULT_ESCALA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERERES_PK ON TASY.PE_REGRA_RESULT_ESCALA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PE_REGRA_RESULT_ESCALA ADD (
  CONSTRAINT PERERES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_REGRA_RESULT_ESCALA ADD (
  CONSTRAINT PERERES_PEREITE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PE_REGRA_ITEM_ESCALA (NR_SEQUENCIA),
  CONSTRAINT PERERES_EIEITRE_FK 
 FOREIGN KEY (IE_RESULT_SCORE_FLEX_II) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM_RESULT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_REGRA_RESULT_ESCALA TO NIVEL_1;

