ALTER TABLE TASY.DETAIL_NODE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DETAIL_NODE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DETAIL_NODE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_NODE              VARCHAR2(60 BYTE),
  NR_SEQ_SUPERIOR      NUMBER(10),
  CD_EXP_LABEL         NUMBER(10),
  IE_KIND              VARCHAR2(20 BYTE),
  NR_LINE              NUMBER(10),
  CD_COMPONENT_TYPE    NUMBER(10),
  NM_TABELA            VARCHAR2(50 BYTE),
  NM_ATRIBUTO          VARCHAR2(60 BYTE),
  NR_SEQ_VISAO         NUMBER(10),
  QT_WIDTH             NUMBER(10),
  QT_HEIGHT            NUMBER(10),
  IE_REQUIRED          VARCHAR2(1 BYTE),
  IE_VISIBLE           VARCHAR2(1 BYTE),
  CD_LABEL_EXP         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DETNODE_DETNODE_FK_I ON TASY.DETAIL_NODE
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DETNODE_DICEXPR_FK_I ON TASY.DETAIL_NODE
(CD_EXP_LABEL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DETNODE_PK ON TASY.DETAIL_NODE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DETNODE_TABATRI_FK_I ON TASY.DETAIL_NODE
(NM_TABELA, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DETNODE_TABVIAT_FK_I ON TASY.DETAIL_NODE
(NR_SEQ_VISAO, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DETAIL_NODE ADD (
  CONSTRAINT DETNODE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DETAIL_NODE ADD (
  CONSTRAINT DETNODE_DETNODE_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.DETAIL_NODE (NR_SEQUENCIA),
  CONSTRAINT DETNODE_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_LABEL) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DETNODE_TABATRI_FK 
 FOREIGN KEY (NM_TABELA, NM_ATRIBUTO) 
 REFERENCES TASY.TABELA_ATRIBUTO (NM_TABELA,NM_ATRIBUTO),
  CONSTRAINT DETNODE_TABVIAT_FK 
 FOREIGN KEY (NR_SEQ_VISAO, NM_ATRIBUTO) 
 REFERENCES TASY.TABELA_VISAO_ATRIBUTO (NR_SEQUENCIA,NM_ATRIBUTO));

GRANT SELECT ON TASY.DETAIL_NODE TO NIVEL_1;

