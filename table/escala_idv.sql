ALTER TABLE TASY.ESCALA_IDV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_IDV CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_IDV
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_GLASGOW             NUMBER(2)              NOT NULL,
  QT_VOLUME_CORRENTE     NUMBER(2)              NOT NULL,
  QT_FIO2                NUMBER(2)              NOT NULL,
  QT_FREQ_RESPIRATORIA   NUMBER(2)              NOT NULL,
  PR_SATURACAO           NUMBER(3)              NOT NULL,
  QT_PAO2                NUMBER(3)              NOT NULL,
  QT_PACO2               NUMBER(3)              NOT NULL,
  DT_AVALIACAO           DATE,
  QT_PRESSAO_INSP        NUMBER(3)              NOT NULL,
  QT_ANO                 NUMBER(3)              NOT NULL,
  QT_PRESSAO_SUP         NUMBER(3)              NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  QT_PONTUACAO           NUMBER(10)             NOT NULL,
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAIDV_ATEPACI_FK_I ON TASY.ESCALA_IDV
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAIDV_PESFISI_FK_I ON TASY.ESCALA_IDV
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAIDV_PK ON TASY.ESCALA_IDV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAIDV_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_Idv_atual
before insert or update ON TASY.ESCALA_IDV for each row
declare
qt_pontuacao_w		number(10);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
qt_pontuacao_w	 := 0;

if	(:new.qt_glasgow > 10) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_glasgow between 8 and 10) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;

if	(:new.qt_fio2 < 40) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_fio2 between 40 and 50) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
elsif	(:new.qt_fio2 between 50 and 60) then
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;

if	(:new.qt_pao2 > 90) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_pao2 between 80 and 90) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;

if	(:new.qt_paco2 < 45) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_paco2 between 45 and 55) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;


if	(:new.pr_saturacao > 95) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.pr_saturacao between 92 and 95) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;


if	(:new.qt_volume_corrente > 7) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_volume_corrente between 5 and 7) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;


if	(:new.qt_freq_respiratoria < 25) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_freq_respiratoria between 25 and 35) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;


if	(:new.QT_PRESSAO_INSP > 25) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.QT_PRESSAO_INSP between 20 and 25) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;




if	(:new.QT_PRESSAO_SUP < 10) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.QT_PRESSAO_SUP between 10 and 15) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;

if	(:new.qt_ano < 60) then
	qt_pontuacao_w	:= qt_pontuacao_w + 3;
elsif	(:new.qt_ano  between 60 and 80) then
	qt_pontuacao_w	:= qt_pontuacao_w + 2;
else
	qt_pontuacao_w	:= qt_pontuacao_w + 1;
end if;

:new.qt_pontuacao := qt_pontuacao_w;

end;
/


ALTER TABLE TASY.ESCALA_IDV ADD (
  CONSTRAINT ESCAIDV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_IDV ADD (
  CONSTRAINT ESCAIDV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAIDV_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_IDV TO NIVEL_1;

