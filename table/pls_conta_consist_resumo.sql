ALTER TABLE TASY.PLS_CONTA_CONSIST_RESUMO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_CONSIST_RESUMO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_CONSIST_RESUMO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTA         NUMBER(10)               NOT NULL,
  DS_INCONSISTENCIA    VARCHAR2(4000 BYTE)      NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCCRE_PK ON TASY.PLS_CONTA_CONSIST_RESUMO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCCRE_PLSCOME_FK_I ON TASY.PLS_CONTA_CONSIST_RESUMO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCCRE_PLSCOME_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CONTA_CONSIST_RESUMO ADD (
  CONSTRAINT PLSCCRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_CONSIST_RESUMO ADD (
  CONSTRAINT PLSCCRE_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CONTA_CONSIST_RESUMO TO NIVEL_1;

