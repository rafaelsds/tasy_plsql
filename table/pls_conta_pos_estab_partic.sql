ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_POS_ESTAB_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_POS_ESTAB_PARTIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_POS     NUMBER(10)               NOT NULL,
  NR_SEQ_PROC_PARTIC   NUMBER(10),
  VL_LIBERADO          NUMBER(15,2)             NOT NULL,
  VL_PARTICIPANTE_POS  NUMBER(15,2),
  VL_ADMINISTRACAO     NUMBER(15,2),
  CD_MEDICO            VARCHAR2(10 BYTE),
  NR_SEQ_GRAU_PARTIC   NUMBER(10),
  NR_SEQ_CBO_SAUDE     NUMBER(10),
  IE_ALTERADO_POS      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCPEP_CBOSAUD_FK_I ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPEP_MEDICO_FK_I ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCPEP_PK ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPEP_PLSCOVB_FK_I ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(NR_SEQ_CONTA_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPEP_PLSGRPA_FK_I ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPEP_PLSPRPAR_FK_I ON TASY.PLS_CONTA_POS_ESTAB_PARTIC
(NR_SEQ_PROC_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conta_pos_estab_part_atual
before insert or update ON TASY.PLS_CONTA_POS_ESTAB_PARTIC for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

nr_seq_conta_w			pls_conta.nr_sequencia%type;
ie_parametro_w			pls_parametros.ie_calculo_pos_estab%type;
nr_contador_w			pls_integer := 0;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := nvl(wheb_usuario_pck.get_cd_estabelecimento,obter_estabelecimento_ativo);
dt_referencia_w			date := trunc(sysdate);
ie_cad_conta_medica_w		pls_controle_estab.ie_cad_conta_medica%type;

begin
-- VERIFICAR SE A REGRA EST� PARA 'DEPENDE DA REGRA'
select	max(ie_calculo_pos_estab)
into	ie_parametro_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(max(ie_cad_conta_medica),'N')
into	ie_cad_conta_medica_w
from	pls_controle_estab;

if	(nvl(ie_parametro_w,'C') = 'R') and
	(cd_estabelecimento_w is not null) then
	-- VERIFICAR SE EXISTE REGRA ATIVA ENTRE INICIO E FIM VIGENCIA DAS REGRAS CADASTRADAS.

	if	(ie_cad_conta_medica_w = 'S') then
		select	count(1)
		into	nr_contador_w
		from	pls_regra_preco_pos_estab
		where	ie_situacao 		= 'A'
		and	cd_estabelecimento 	= cd_estabelecimento_w
		and	dt_referencia_w between trunc(nvl(dt_inicio_vigencia, dt_referencia_w)) and nvl(dt_fim_vigencia, sysdate);
	else
		select	count(1)
		into	nr_contador_w
		from	pls_regra_preco_pos_estab
		where	ie_situacao 		= 'A'
		and	dt_referencia_w between trunc(nvl(dt_inicio_vigencia, dt_referencia_w)) and nvl(dt_fim_vigencia, sysdate);
	end if;

	if	(nr_contador_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(340424);
	end if;
end if;

if	(:new.vl_participante_pos < 0) or
	(:new.vl_administracao < 0) then
	select	max(nr_seq_conta)
	into	nr_seq_conta_w
	from	pls_conta_pos_estabelecido
	where	nr_sequencia = :new.nr_seq_conta_pos;

	wheb_mensagem_pck.exibir_mensagem_abort( 283705, 'NR_SEQ_CONTA=' || nr_seq_conta_w || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_seq_conta_pos );
end if;

end;
/


ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_PARTIC ADD (
  CONSTRAINT PLSCPEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_POS_ESTAB_PARTIC ADD (
  CONSTRAINT PLSCPEP_PLSCOVB_FK 
 FOREIGN KEY (NR_SEQ_CONTA_POS) 
 REFERENCES TASY.PLS_CONTA_POS_ESTABELECIDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCPEP_PLSPRPAR_FK 
 FOREIGN KEY (NR_SEQ_PROC_PARTIC) 
 REFERENCES TASY.PLS_PROC_PARTICIPANTE (NR_SEQUENCIA),
  CONSTRAINT PLSCPEP_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PLSCPEP_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSCPEP_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_POS_ESTAB_PARTIC TO NIVEL_1;

