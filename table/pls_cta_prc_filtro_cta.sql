ALTER TABLE TASY.PLS_CTA_PRC_FILTRO_CTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CTA_PRC_FILTRO_CTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CTA_PRC_FILTRO_CTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_FILTRO   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCTAF_PK ON TASY.PLS_CTA_PRC_FILTRO_CTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAF_PLSCLPF_FK_I ON TASY.PLS_CTA_PRC_FILTRO_CTA
(NR_SEQ_LOTE_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_CTA_PRC_FILTRO_CTA ADD (
  CONSTRAINT PLSCTAF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CTA_PRC_FILTRO_CTA ADD (
  CONSTRAINT PLSCTAF_PLSCLPF_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FILTRO) 
 REFERENCES TASY.PLS_CTA_LOTE_PROC_FILTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CTA_PRC_FILTRO_CTA TO NIVEL_1;

