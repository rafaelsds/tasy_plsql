ALTER TABLE TASY.XML_PARAMETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.XML_PARAMETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.XML_PARAMETRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_APRESENTACAO  NUMBER(5)                NOT NULL,
  CD_PARAMETRO         VARCHAR2(50 BYTE)        NOT NULL,
  DS_PARAMETRO         VARCHAR2(50 BYTE)        NOT NULL,
  IE_TIPO_ATRIBUTO     VARCHAR2(10 BYTE)        NOT NULL,
  IE_COMPONENTE        VARCHAR2(5 BYTE)         NOT NULL,
  QT_TAMANHO_CAMPO     NUMBER(3),
  VL_PADRAO            VARCHAR2(255 BYTE),
  DS_SQL               VARCHAR2(2000 BYTE),
  NR_SEQ_PROJETO       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.XMLPRMT_PK ON TASY.XML_PARAMETRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.XMLPRMT_PK
  MONITORING USAGE;


CREATE INDEX TASY.XMLPRMT_XMLPROJ_FK_I ON TASY.XML_PARAMETRO
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.XML_PARAMETRO ADD (
  CONSTRAINT XMLPRMT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.XML_PARAMETRO ADD (
  CONSTRAINT XMLPRMT_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.XML_PARAMETRO TO NIVEL_1;

