ALTER TABLE TASY.LATAM_VIAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LATAM_VIAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LATAM_VIAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PESSOAS           NUMBER(10)               NOT NULL,
  QT_DIAS_VIAGEM       NUMBER(10)               NOT NULL,
  NR_SEQ_PARAMETRO     NUMBER(10),
  DS_VIAGEM            VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LATAMVIA_LATAMPAR_FK_I ON TASY.LATAM_VIAGEM
(NR_SEQ_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVIA_LATAMPAR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LATAMVIA_PK ON TASY.LATAM_VIAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVIA_PK
  MONITORING USAGE;


ALTER TABLE TASY.LATAM_VIAGEM ADD (
  CONSTRAINT LATAMVIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LATAM_VIAGEM ADD (
  CONSTRAINT LATAMVIA_LATAMPAR_FK 
 FOREIGN KEY (NR_SEQ_PARAMETRO) 
 REFERENCES TASY.LATAM_PARAMETROS (NR_SEQUENCIA));

GRANT SELECT ON TASY.LATAM_VIAGEM TO NIVEL_1;

