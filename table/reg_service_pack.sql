ALTER TABLE TASY.REG_SERVICE_PACK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_SERVICE_PACK CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_SERVICE_PACK
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTENCAO_USO  NUMBER(10),
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DS_VERSION           VARCHAR2(255 BYTE)       NOT NULL,
  CD_BUILD             NUMBER(10),
  CD_VERSAO            VARCHAR2(15 BYTE),
  DS_HOTFIX            VARCHAR2(25 BYTE),
  NR_SEQ_PLAN_CONTROL  NUMBER(10),
  NR_PACOTE_INICIAL    NUMBER(10),
  NR_PACOTE_FINAL      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REG_SP_PK ON TASY.REG_SERVICE_PACK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REG_SP_REGINU_FK_I ON TASY.REG_SERVICE_PACK
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REG_SP_REGPTC_FK_I ON TASY.REG_SERVICE_PACK
(NR_SEQ_PLAN_CONTROL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_service_pack_after
before insert ON TASY.REG_SERVICE_PACK for each row
declare

begin

	select	ds_version,
		ds_build,
		ds_hotfix
	into	:new.cd_versao,
		:new.cd_build,
		:new.ds_hotfix
	from	table(phi_ordem_servico_pck.get_version_info2(:new.ds_version));

end reg_service_pack_after;
/


ALTER TABLE TASY.REG_SERVICE_PACK ADD (
  CONSTRAINT REG_SP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REG_SERVICE_PACK ADD (
  CONSTRAINT REG_SP_REGPTC_FK 
 FOREIGN KEY (NR_SEQ_PLAN_CONTROL) 
 REFERENCES TASY.REG_PLANO_TESTE_CONTROLE (NR_SEQUENCIA),
  CONSTRAINT REG_SP_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_SERVICE_PACK TO NIVEL_1;

