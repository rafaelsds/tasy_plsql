ALTER TABLE TASY.NUT_ELEMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_ELEMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_ELEMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DS_ELEMENTO              VARCHAR2(255 BYTE)   NOT NULL,
  IE_TIPO_ELEMENTO         VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_APRESENT          NUMBER(3)            NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_UNIDADE_MEDIDA        VARCHAR2(30 BYTE),
  QT_MIN                   NUMBER(15,3)         NOT NULL,
  QT_MAX                   NUMBER(15,3)         NOT NULL,
  IE_GERAR_PED             VARCHAR2(1 BYTE)     NOT NULL,
  IE_PRIM_FASE             VARCHAR2(1 BYTE)     NOT NULL,
  IE_SEG_FASE              VARCHAR2(1 BYTE)     NOT NULL,
  IE_TERC_FASE             VARCHAR2(1 BYTE)     NOT NULL,
  DS_PADRAO                VARCHAR2(20 BYTE),
  IE_NPT_ADULTO            VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_IMP               NUMBER(3)            NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_REP_HE                VARCHAR2(1 BYTE)     NOT NULL,
  IE_HEMODIALISE           VARCHAR2(1 BYTE)     NOT NULL,
  IE_OSMOLARIDADE          VARCHAR2(1 BYTE),
  VL_MULTIPL_OSMOLARIDADE  NUMBER(15,2),
  IE_NPT_PEDIATRICA        VARCHAR2(1 BYTE),
  DS_PADRAO_PED            VARCHAR2(100 BYTE),
  IE_UNID_MED              VARCHAR2(15 BYTE),
  QT_DOSE_PADRAO           NUMBER(15,4),
  IE_REDISTRIBUIR          VARCHAR2(1 BYTE),
  IE_LIMITE_CONCENTRACAO   VARCHAR2(1 BYTE),
  QT_LIMITE_LITRO          NUMBER(15,4),
  QT_VOL_RECALCULAR        NUMBER(15,4),
  IE_SOMAR_VOLUME          VARCHAR2(1 BYTE),
  IE_QUAR_FASE             VARCHAR2(1 BYTE),
  IE_GLUTAMINA             VARCHAR2(1 BYTE),
  IE_DIETA_ENTERAL         VARCHAR2(1 BYTE),
  IE_MEDIC_SOLUCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.NUTELEM_PK ON TASY.NUT_ELEMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTELEM_UNIMEDI_FK_I ON TASY.NUT_ELEMENTO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nut_elemento_atual
before insert or update ON TASY.NUT_ELEMENTO for each row
declare

begin

if 	(:new.qt_min is not null) and
	(:new.qt_max is not null) and
	(:new.qt_min <> 0) and
	(:new.qt_max <> 0) and
	(:new.qt_min > :new.qt_max) then
	begin
	Wheb_mensagem_pck.exibir_mensagem_abort(392333);
	end;
end if;

end;
/


ALTER TABLE TASY.NUT_ELEMENTO ADD (
  CONSTRAINT NUTELEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_ELEMENTO ADD (
  CONSTRAINT NUTELEM_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.NUT_ELEMENTO TO NIVEL_1;

