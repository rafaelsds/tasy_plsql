ALTER TABLE TASY.MAT_ATEND_PAC_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_ATEND_PAC_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_ATEND_PAC_LOG
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_MAT            NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(100 BYTE)      NOT NULL,
  NR_ATENDIMENTO        NUMBER(10),
  NR_INTERNO_CONTA      NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  DT_ATENDIMENTO        DATE,
  IE_ACAO               VARCHAR2(2 BYTE)        NOT NULL,
  QT_MATERIAL           NUMBER(9,3),
  VL_MATERIAL           NUMBER(15,2),
  CD_PERFIL             NUMBER(5),
  CD_FUNCAO             NUMBER(5),
  IE_VALOR_INFORMADO    VARCHAR2(1 BYTE),
  NR_SEQ_PROC_PACOTE    NUMBER(10),
  NR_SEQ_PROC_PRINC     NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  DT_ENTRADA_UNIDADE    DATE,
  NR_SEQ_ATEPACU        NUMBER(10),
  NR_PRESCRICAO         NUMBER(10),
  DS_MODULE             VARCHAR2(255 BYTE),
  CD_LOCAL_ESTOQUE      NUMBER(4),
  QT_MATERIAL_ANT       NUMBER(9,3),
  NR_SEQ_KIT_ESTOQUE    NUMBER(10),
  CD_MATERIAL_KIT       NUMBER(5),
  NR_CIRURGIA           NUMBER(10),
  DS_CALL_STACK         VARCHAR2(2000 BYTE),
  IE_AUDITORIA          VARCHAR2(1 BYTE),
  NR_REQUISICAO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          160M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MAATPAL_I1 ON TASY.MAT_ATEND_PAC_LOG
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          46M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MAATPAL_I2 ON TASY.MAT_ATEND_PAC_LOG
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAATPAL_I2
  MONITORING USAGE;


CREATE INDEX TASY.MAATPAL_I3 ON TASY.MAT_ATEND_PAC_LOG
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAATPAL_I3
  MONITORING USAGE;


CREATE INDEX TASY.MAATPAL_I4 ON TASY.MAT_ATEND_PAC_LOG
(CD_FUNCAO, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAATPAL_I4
  MONITORING USAGE;


CREATE INDEX TASY.MAATPAL_I5 ON TASY.MAT_ATEND_PAC_LOG
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAATPAL_I5
  MONITORING USAGE;


CREATE INDEX TASY.MAATPAL_I6 ON TASY.MAT_ATEND_PAC_LOG
(NR_ATENDIMENTO, IE_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MAATPAL_PK ON TASY.MAT_ATEND_PAC_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          27M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MAATPAL_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAT_ATEND_PAC_LOG ADD (
  CONSTRAINT MAATPAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          27M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MAT_ATEND_PAC_LOG TO NIVEL_1;

