ALTER TABLE TASY.ESCALA_TENETTI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_TENETTI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_TENETTI
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  DT_AVALIACAO                DATE              NOT NULL,
  CD_PROFISSIONAL             VARCHAR2(10 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_LIBERACAO                DATE,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  IE_EQUILIBRIO_SENTADO       NUMBER(3)         NOT NULL,
  QT_PONTUACAO                NUMBER(5),
  IE_LEVANTAR_CADEIRA         NUMBER(3)         NOT NULL,
  IE_EQUILIBRIO_IMEDIATO      NUMBER(3)         NOT NULL,
  IE_EQUILIBRIO_EM_PE         NUMBER(3)         NOT NULL,
  IE_EQUILIBRIO_OLHO_FECHADO  NUMBER(3)         NOT NULL,
  IE_EQUILIBRIO_GIRAR         NUMBER(3)         NOT NULL,
  IE_EMPURRAO_TORAX           NUMBER(3),
  IE_GIRAR_PESCOCO            NUMBER(3),
  IE_EQUILIBRIO_APOIO         NUMBER(3),
  IE_EXTENSAO_TRONCO          NUMBER(3),
  IE_ALCANCAR                 NUMBER(3),
  IE_SENTAR                   NUMBER(3)         NOT NULL,
  NR_SEQ_ASSINATURA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO   NUMBER(10),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCTENE_ATEPACI_FK_I ON TASY.ESCALA_TENETTI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCTENE_I1 ON TASY.ESCALA_TENETTI
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCTENE_PESFISI_FK_I ON TASY.ESCALA_TENETTI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCTENE_PK ON TASY.ESCALA_TENETTI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCTENE_TASASDI_FK_I ON TASY.ESCALA_TENETTI
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCTENE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCTENE_TASASDI_FK2_I ON TASY.ESCALA_TENETTI
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCTENE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATEND_ESCALA_TENETTI_ATUAL
before insert or update ON TASY.ESCALA_TENETTI for each row
declare

begin

:new.qt_pontuacao := 	nvl(:new.IE_EQUILIBRIO_SENTADO,0) +
			nvl(:new.IE_LEVANTAR_CADEIRA,0)   +
			nvl(:new.IE_EQUILIBRIO_IMEDIATO,0) +
			nvl(:new.IE_EQUILIBRIO_EM_PE,0) +
			nvl(:new.IE_EQUILIBRIO_OLHO_FECHADO,0) +
			nvl(:new.IE_EQUILIBRIO_GIRAR,0) +
			nvl(:new.IE_EMPURRAO_TORAX,0) +
			nvl(:new.IE_GIRAR_PESCOCO,0) +
			nvl(:new.IE_EQUILIBRIO_APOIO,0) +
			nvl(:new.IE_EXTENSAO_TRONCO,0) +
			nvl(:new.IE_ALCANCAR,0) +
			nvl(:new.IE_SENTAR,0);

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_tenetti_pend_atual
after insert or update ON TASY.ESCALA_TENETTI for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'91');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_TENETTI_delete
after delete ON TASY.ESCALA_TENETTI for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '91'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_TENETTI ADD (
  CONSTRAINT ESCTENE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_TENETTI ADD (
  CONSTRAINT ESCTENE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCTENE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCTENE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCTENE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_TENETTI TO NIVEL_1;

