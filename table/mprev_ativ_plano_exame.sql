ALTER TABLE TASY.MPREV_ATIV_PLANO_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_ATIV_PLANO_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_ATIV_PLANO_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATIVIDADE     NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME_LAB     NUMBER(10),
  NM_OUTRO_EXAME       VARCHAR2(255 BYTE),
  IE_FINALIDADE        VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRATPE_MPRATPL_FK_I ON TASY.MPREV_ATIV_PLANO_EXAME
(NR_SEQ_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRATPE_PK ON TASY.MPREV_ATIV_PLANO_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_ATIV_PLANO_EXAME ADD (
  CONSTRAINT MPRATPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_ATIV_PLANO_EXAME ADD (
  CONSTRAINT MPRATPE_MPRATPL_FK 
 FOREIGN KEY (NR_SEQ_ATIVIDADE) 
 REFERENCES TASY.MPREV_ATIVIDADE_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_ATIV_PLANO_EXAME TO NIVEL_1;

