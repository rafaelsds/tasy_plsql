DROP TABLE TASY.W_HD_EVOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_HD_EVOLUCAO
(
  NM_PESSOA_FISICA  VARCHAR2(60 BYTE),
  DT_INICIO_TRAT    DATE,
  IE_PRIM_M         VARCHAR2(10 BYTE),
  IE_LAUDO_M        VARCHAR2(10 BYTE),
  IE_MENSAL_M       VARCHAR2(10 BYTE),
  IE_PRIM_E         VARCHAR2(10 BYTE),
  IE_MENSAL_E       VARCHAR2(10 BYTE),
  IE_PRIM_N         VARCHAR2(10 BYTE),
  IE_MENSAL_N       VARCHAR2(10 BYTE),
  IE_PRIM_P         VARCHAR2(10 BYTE),
  IE_MENSAL_P       VARCHAR2(10 BYTE),
  IE_PRIM_ASS       VARCHAR2(10 BYTE),
  IE_MENSAL_ASS     VARCHAR2(10 BYTE),
  DS_UNIDADE        VARCHAR2(80 BYTE),
  IE_TIPO           VARCHAR2(1 BYTE),
  QT_PACIENTES      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_HD_EVOLUCAO TO NIVEL_1;

