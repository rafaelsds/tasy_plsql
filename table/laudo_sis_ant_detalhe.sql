ALTER TABLE TASY.LAUDO_SIS_ANT_DETALHE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_SIS_ANT_DETALHE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_SIS_ANT_DETALHE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_NOME              VARCHAR2(100 BYTE),
  NR_SEQ_LAU_ANT       NUMBER(10),
  DS_ARQUIVO           CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LASISAD_LASIAN_FK_I ON TASY.LAUDO_SIS_ANT_DETALHE
(NR_SEQ_LAU_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LASISAD_PK ON TASY.LAUDO_SIS_ANT_DETALHE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAUDO_SIS_ANT_DETALHE ADD (
  CONSTRAINT LASISAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.LAUDO_SIS_ANT_DETALHE ADD (
  CONSTRAINT LASISAD_LASIAN_FK 
 FOREIGN KEY (NR_SEQ_LAU_ANT) 
 REFERENCES TASY.LAUDO_SIS_ANTIGO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LAUDO_SIS_ANT_DETALHE TO NIVEL_1;

