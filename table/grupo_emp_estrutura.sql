ALTER TABLE TASY.GRUPO_EMP_ESTRUTURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GRUPO_EMP_ESTRUTURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GRUPO_EMP_ESTRUTURA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO          NUMBER(10),
  CD_EMPRESA            NUMBER(4)               NOT NULL,
  IE_TIPO_ESTRUTURA     NUMBER(1),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA_SUP  NUMBER(10),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  IE_EVENTO_SOCIETARIO  VARCHAR2(15 BYTE),
  PR_PARTICIPACAO       NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GRUEMPEST_EMPRESA_FK_I ON TASY.GRUPO_EMP_ESTRUTURA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GRUEMPEST_GRUEMPEST_FK_I ON TASY.GRUPO_EMP_ESTRUTURA
(NR_SEQ_ESTRUTURA_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GRUEMPEST_GRUEMP_FK_I ON TASY.GRUPO_EMP_ESTRUTURA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GRUEMPEST_PK ON TASY.GRUPO_EMP_ESTRUTURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GRUPO_EMP_ESTRUTURA_ATUAL before insert or update ON TASY.GRUPO_EMP_ESTRUTURA 
for each row
declare
dt_inicio_vigencia_w	date;
dt_fim_vigencia_w	date;

begin

select	dt_inicio_vigencia,
	dt_fim_vigencia
into	dt_inicio_vigencia_w,
	dt_fim_vigencia_w
from	grupo_empresa
where	nr_sequencia	= :new.nr_seq_grupo;

if	(dt_fim_vigencia_w is not null) and
	(:new.dt_inicio_vigencia is not null) and
	(trunc(:new.dt_inicio_vigencia) > trunc(dt_fim_vigencia_w)) then
	wheb_mensagem_pck.exibir_mensagem_abort(1083685);
	/*A vig�ncia da empresa n�o est� dentro da vig�ncia do grupo de empresas*/
end if;

if ((:new.pr_participacao > 100) or (:new.pr_participacao <= 0)) then
  wheb_mensagem_pck.exibir_mensagem_abort(1084390);
end if;

begin
if	(dt_fim_vigencia_w is not null) and
	(dt_fim_vigencia_w < trunc(sysdate,'dd')) then
	wheb_mensagem_pck.exibir_mensagem_abort(1083686);
	/*Este grupo de empresa n�o est� vigente. N�o � poss�vel alterar ou incluir empresa em grupo j� finalizado*/
end if;
end;


end;
/


CREATE OR REPLACE TRIGGER TASY.GRUPO_EMP_ESTRUTURA_tp  after update ON TASY.GRUPO_EMP_ESTRUTURA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'GRUPO_EMP_ESTRUTURA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'GRUPO_EMP_ESTRUTURA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.GRUPO_EMP_ESTRUTURA ADD (
  CONSTRAINT GRUEMPEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GRUPO_EMP_ESTRUTURA ADD (
  CONSTRAINT GRUEMPEST_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT GRUEMPEST_GRUEMPEST_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA_SUP) 
 REFERENCES TASY.GRUPO_EMP_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT GRUEMPEST_GRUEMP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_EMPRESA (NR_SEQUENCIA));

GRANT SELECT ON TASY.GRUPO_EMP_ESTRUTURA TO NIVEL_1;

