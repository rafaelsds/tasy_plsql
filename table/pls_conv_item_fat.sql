ALTER TABLE TASY.PLS_CONV_ITEM_FAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONV_ITEM_FAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONV_ITEM_FAT
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DS_REGRA                  VARCHAR2(255 BYTE)  NOT NULL,
  DT_INICIO_VIGENCIA        DATE                NOT NULL,
  DT_FIM_VIGENCIA           DATE,
  DT_INICIO_VIGENCIA_REF    DATE,
  DT_FIM_VIGENCIA_REF       DATE,
  NR_SEQ_GRUPO_PRESTADOR    NUMBER(10),
  NR_SEQ_GRUPO_OPERADORA    NUMBER(10),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_INTERCAMBIO        NUMBER(10),
  IE_CONSIDERA_TODOS_ITENS  VARCHAR2(5 BYTE),
  DT_ALTERACAO_ESTRUTURA    DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCVITEM_PK ON TASY.PLS_CONV_ITEM_FAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVITEM_PLSCONT_FK_I ON TASY.PLS_CONV_ITEM_FAT
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVITEM_PLSINCA_FK_I ON TASY.PLS_CONV_ITEM_FAT
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVITEM_PLSPGOP_FK_I ON TASY.PLS_CONV_ITEM_FAT
(NR_SEQ_GRUPO_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVITEM_PLSPRGP_FK_I ON TASY.PLS_CONV_ITEM_FAT
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conv_item_fat_atual
before insert or update ON TASY.PLS_CONV_ITEM_FAT for each row
declare

begin

/* esta trigger foi criada para alimentar os campos de data de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
 o campo inicial vigencia ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
 com a data zero do oracle 31/12/1899, j� o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/
:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia,'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia,'F');
:new.dt_alteracao_estrutura := sysdate;


end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CONV_ITEM_FAT_tp  after update ON TASY.PLS_CONV_ITEM_FAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_OPERADORA,1,4000),substr(:new.NR_SEQ_GRUPO_OPERADORA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_OPERADORA',ie_log_w,ds_w,'PLS_CONV_ITEM_FAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CONV_ITEM_FAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CONV_ITEM_FAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_GRUPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRESTADOR',ie_log_w,ds_w,'PLS_CONV_ITEM_FAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CONV_ITEM_FAT ADD (
  CONSTRAINT PLSCVITEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONV_ITEM_FAT ADD (
  CONSTRAINT PLSCVITEM_PLSPGOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_OPERADORA) 
 REFERENCES TASY.PLS_PRECO_GRUPO_OPERADORA (NR_SEQUENCIA),
  CONSTRAINT PLSCVITEM_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCVITEM_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCVITEM_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONV_ITEM_FAT TO NIVEL_1;

