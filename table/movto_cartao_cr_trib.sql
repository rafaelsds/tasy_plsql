ALTER TABLE TASY.MOVTO_CARTAO_CR_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVTO_CARTAO_CR_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVTO_CARTAO_CR_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOVTO_CARTAO  NUMBER(10)               NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  VL_IMPOSTO           NUMBER(15,2)             NOT NULL,
  TX_TRIBUTO           NUMBER(15,4),
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  NR_SEQ_REGRA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MCCT_ESTABEL_FK_I ON TASY.MOVTO_CARTAO_CR_TRIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MCCT_FISREGCRT_FK_I ON TASY.MOVTO_CARTAO_CR_TRIB
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MCCT_MOVCACR_FK_I ON TASY.MOVTO_CARTAO_CR_TRIB
(NR_SEQ_MOVTO_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MCCT_PK ON TASY.MOVTO_CARTAO_CR_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MCCT_TRAFINA_FK_I ON TASY.MOVTO_CARTAO_CR_TRIB
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MCCT_TRIBUTO_FK_I ON TASY.MOVTO_CARTAO_CR_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.movto_cartao_cr_trib_after
after delete ON TASY.MOVTO_CARTAO_CR_TRIB for each row
declare
vl_imposto_w		movto_cartao_cr_parcela.vl_imposto%type;
nr_sequencia_w		movto_cartao_cr_parcela.nr_sequencia%type;
vl_despesa_W		movto_cartao_cr_parcela.vl_despesa%type;

/*Cursor para buscar as parcelas do cartao.*/
Cursor C01 is
	select	a.nr_sequencia,
			a.vl_despesa
	from	movto_cartao_cr_parcela a
	where	a.nr_seq_movto	= :old.nr_seq_movto_cartao
	order by a.nr_sequencia;


/*Trigger criada devido a necessidade de atualizar o valor do imposto na parcela sempre que um tributo for excluido*/
begin

if (:old.cd_tributo is not null) and (:old.tx_tributo is not null) then

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		vl_despesa_W;
	exit when C01%notfound;
		begin

			if (nvl(vl_despesa_w,0) > 0) and (nvl(:old.tx_tributo,0) > 0) then

					vl_imposto_w := (vl_despesa_w * (:old.tx_tributo / 100))*-1;

					update	movto_cartao_cr_parcela a
					set		a.vl_imposto	= nvl(a.vl_imposto,0) + nvl(vl_imposto_w,0)
					where	a.nr_sequencia	= nr_sequencia_w
					and		a.nr_seq_movto	= :old.nr_seq_movto_cartao;
			end if;

		end;
	end loop;
	close C01;
end if;

end;
/


ALTER TABLE TASY.MOVTO_CARTAO_CR_TRIB ADD (
  CONSTRAINT MCCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVTO_CARTAO_CR_TRIB ADD (
  CONSTRAINT MCCT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MCCT_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT MCCT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MCCT_FISREGCRT_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.FIS_REG_TRIB_CARTAO (NR_SEQUENCIA),
  CONSTRAINT MCCT_MOVCACR_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_CARTAO) 
 REFERENCES TASY.MOVTO_CARTAO_CR (NR_SEQUENCIA));

GRANT SELECT ON TASY.MOVTO_CARTAO_CR_TRIB TO NIVEL_1;

