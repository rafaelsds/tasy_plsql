ALTER TABLE TASY.SPA_HISTORICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_HISTORICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_HISTORICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SPA           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_STATUS_ANT        NUMBER(2),
  IE_STATUS_ATUAL      NUMBER(2),
  IE_TIPO_HISTORICO    NUMBER(2),
  DS_PARECER           VARCHAR2(255 BYTE),
  IE_ORIGEM_HISTORICO  VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  DT_LIBERACAO         DATE,
  DS_PARECER_CLOB      CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_PARECER_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SPAHIST_PK ON TASY.SPA_HISTORICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPAHIST_SPA_FK_I ON TASY.SPA_HISTORICO
(NR_SEQ_SPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SPA_HISTORICO ADD (
  CONSTRAINT SPAHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_HISTORICO ADD (
  CONSTRAINT SPAHIST_SPA_FK 
 FOREIGN KEY (NR_SEQ_SPA) 
 REFERENCES TASY.SPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_HISTORICO TO NIVEL_1;

