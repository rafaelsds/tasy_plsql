ALTER TABLE TASY.PROJ_DEPENDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_DEPENDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_DEPENDENCIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_PROJETO        NUMBER(10)              NOT NULL,
  NR_SEQ_ATIV_CRON      NUMBER(10),
  NR_SEQ_ORDEM_SERV     NUMBER(10),
  NR_SEQ_PROJ_DEP       NUMBER(10),
  IE_NIVEL_DEPENDENCIA  VARCHAR2(15 BYTE)       NOT NULL,
  IE_STATUS             VARCHAR2(1 BYTE)        NOT NULL,
  CD_FUNCAO             NUMBER(5),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  NR_SEQ_GERENCIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRODEPE_FUNCAO_FK_I ON TASY.PROJ_DEPENDENCIA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODEPE_GERWHEB_FK_I ON TASY.PROJ_DEPENDENCIA
(NR_SEQ_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODEPE_MANORSE_FK_I ON TASY.PROJ_DEPENDENCIA
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODEPE_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODEPE_PESFISI_FK_I ON TASY.PROJ_DEPENDENCIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRODEPE_PK ON TASY.PROJ_DEPENDENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODEPE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRODEPE_PROCRET_FK_I ON TASY.PROJ_DEPENDENCIA
(NR_SEQ_ATIV_CRON)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODEPE_PROPROJ_FK_I ON TASY.PROJ_DEPENDENCIA
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODEPE_PROPROJ_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_dependencia_insert
after insert ON TASY.PROJ_DEPENDENCIA for each row
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	(:new.nr_seq_ordem_serv is not null) then
		begin
		gerar_nec_vinc_dep_proj(:new.nr_seq_projeto, :new.nr_sequencia, :new.nr_seq_ordem_serv, wheb_usuario_pck.get_nm_usuario);
		end;
	end if;
	end;
end if;
end;
/


ALTER TABLE TASY.PROJ_DEPENDENCIA ADD (
  CONSTRAINT PRODEPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_DEPENDENCIA ADD (
  CONSTRAINT PRODEPE_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA),
  CONSTRAINT PRODEPE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRODEPE_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_ATIV_CRON) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT PRODEPE_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PRODEPE_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PRODEPE_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO));

GRANT SELECT ON TASY.PROJ_DEPENDENCIA TO NIVEL_1;

