ALTER TABLE TASY.MEDIC_FICHA_LIMITE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIC_FICHA_LIMITE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIC_FICHA_LIMITE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_FICHA_TECNICA     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_DOSE_MAX          NUMBER(18,6)             NOT NULL,
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)        NOT NULL,
  IE_DOSE_LIMITE       VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDFILIM_MEDFITE_FK_I ON TASY.MEDIC_FICHA_LIMITE
(NR_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDFILIM_PK ON TASY.MEDIC_FICHA_LIMITE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDFILIM_UNIMEDI_FK_I ON TASY.MEDIC_FICHA_LIMITE
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MEDIC_FICHA_LIMITE ADD (
  CONSTRAINT MEDFILIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MEDIC_FICHA_LIMITE ADD (
  CONSTRAINT MEDFILIM_MEDFITE_FK 
 FOREIGN KEY (NR_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT MEDFILIM_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.MEDIC_FICHA_LIMITE TO NIVEL_1;

