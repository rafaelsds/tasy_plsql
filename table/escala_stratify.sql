ALTER TABLE TASY.ESCALA_STRATIFY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_STRATIFY CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_STRATIFY
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_CONFUSO             VARCHAR2(1 BYTE)       NOT NULL,
  IE_QUEDA               VARCHAR2(1 BYTE)       NOT NULL,
  IE_DESORIENTADO        VARCHAR2(1 BYTE)       NOT NULL,
  IE_AGITADO             VARCHAR2(1 BYTE)       NOT NULL,
  IE_QUEDA_6_MESES       VARCHAR2(1 BYTE)       NOT NULL,
  IE_OCULOS              VARCHAR2(1 BYTE)       NOT NULL,
  IE_VISAO_TURVA         VARCHAR2(1 BYTE)       NOT NULL,
  IE_GLAUCOMA            VARCHAR2(1 BYTE)       NOT NULL,
  IE_ALTERACAO_DIURESE   VARCHAR2(1 BYTE)       NOT NULL,
  IE_TRANSFERENCIA       NUMBER(3),
  IE_MOBILIDADE          NUMBER(3),
  QT_SCORE               NUMBER(3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSTRA_ATEPACI_FK_I ON TASY.ESCALA_STRATIFY
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTRA_I1 ON TASY.ESCALA_STRATIFY
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTRA_PESFISI_FK_I ON TASY.ESCALA_STRATIFY
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSTRA_PK ON TASY.ESCALA_STRATIFY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_STRATIFY_ATUAL
before insert or update ON TASY.ESCALA_STRATIFY for each row
declare

qt_historia_w		number(10) := 0;
qt_mental_w			number(10) := 0;
qt_visao_w			number(10) := 0;
qt_toilet_w			number(10) := 0;
qt_trans_mob_w		number(10) := 0;

begin

if	(:new.IE_QUEDA = 'S') then
	qt_historia_w	:=	1;
end if;
if	(:new.IE_QUEDA_6_MESES = 'S') then
	qt_historia_w	:=	1;
end if;

if	(:new.IE_CONFUSO = 'S') then
	qt_mental_w	:= 	1;
end if;
if	(:new.IE_AGITADO = 'S') then
	qt_mental_w	:=  1;
end if;
if	(:new.IE_DESORIENTADO = 'S') then
	qt_mental_w	:= 	1;
end if;

if	(:new.IE_OCULOS = 'S') then
	qt_visao_w	:=  1;
end if;
if	(:new.IE_VISAO_TURVA = 'S') then
	qt_visao_w	:=  1;
end if;
if	(:new.IE_GLAUCOMA = 'S') then
	qt_visao_w	:=  1;
end if;

if	(:new.IE_ALTERACAO_DIURESE = 'S') then
	qt_toilet_w	:=  1;
end if;

qt_trans_mob_w	:= 	:new.IE_MOBILIDADE + :new.IE_TRANSFERENCIA;

if	(qt_trans_mob_w in (3,4,5,6)) then
	qt_trans_mob_w	:= 1;
elsif	(qt_trans_mob_w in (0,1,2)) then
	qt_trans_mob_w	:= 0;
end if;

:new.QT_SCORE	:=	(6 * qt_historia_w) + (14 * qt_mental_w) + (qt_visao_w) + (2 * qt_toilet_w) + (7 * qt_trans_mob_w);

end;
/


ALTER TABLE TASY.ESCALA_STRATIFY ADD (
  CONSTRAINT ESCSTRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_STRATIFY ADD (
  CONSTRAINT ESCSTRA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSTRA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_STRATIFY TO NIVEL_1;

