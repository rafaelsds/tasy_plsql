ALTER TABLE TASY.USUARIO_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_PERFIL
(
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_PERFIL           NUMBER(5)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO_ATUAL    VARCHAR2(15 BYTE)         NOT NULL,
  DS_OBSERVACAO       VARCHAR2(4000 BYTE),
  NR_SEQ_APRES        NUMBER(10),
  DT_VALIDADE         DATE,
  DT_LIBERACAO        DATE,
  NR_SEQUENCIA        NUMBER(10),
  DS_UTC              VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO    VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO  VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.USUARIO_PERFIL.NM_USUARIO IS 'Nome Abreviado do Usuario';

COMMENT ON COLUMN TASY.USUARIO_PERFIL.CD_PERFIL IS 'Codigo do Perfil';

COMMENT ON COLUMN TASY.USUARIO_PERFIL.DT_ATUALIZACAO IS 'Data Atualizacao';

COMMENT ON COLUMN TASY.USUARIO_PERFIL.NM_USUARIO_ATUAL IS 'Nome Usuario';


CREATE INDEX TASY.USUPERF_PERFIL_FK_I ON TASY.USUARIO_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    40
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUPERF_PK ON TASY.USUARIO_PERFIL
(NM_USUARIO, CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUPERF_USUARIO_FK_I ON TASY.USUARIO_PERFIL
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    40
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.USUARIO_PERFIL_INSERT
BEFORE INSERT OR UPDATE ON TASY.USUARIO_PERFIL FOR EACH ROW
DECLARE
BEGIN
	IF INSERTING THEN
		:NEW.DT_LIBERACAO := SYSDATE;
		SELECT	USUARIO_PERFIL_SEQ.NEXTVAL INTO :NEW.NR_SEQUENCIA FROM DUAL;
	END IF;
	:NEW.DS_UTC_ATUALIZACAO := OBTER_DATA_UTC(SYSDATE,'HV');
	:NEW.DS_UTC           := OBTER_DATA_UTC(:NEW.DT_ATUALIZACAO,'HV');
	:NEW.IE_HORARIO_VERAO := OBTER_SE_HORARIO_VERAO(:NEW.DT_ATUALIZACAO);
END;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_USUARIO_PERFIL_DELETE
after delete ON TASY.USUARIO_PERFIL FOR EACH ROW
DECLARE
NM_USUARIO_W VARCHAR(50);

BEGIN

   IF :OLD.CD_PERFIL = 1574 THEN
        begin
            
           SELECT DS_LOGIN 
           INTO NM_USUARIO_W
           FROM USUARIO WHERE NM_USUARIO = :OLD.NM_USUARIO;
           
                      
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NM_USUARIO_W := 'N';
        END;
        
        IF NM_USUARIO_W <> 'N' THEN
                delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w) and GROUPNAME = 'WIFI';
                delete from hsj_autenticacao_wifi where lower(NM_USUARIO) = lower(nm_usuario_w);
        
        END IF;
   END IF;
   
    IF :OLD.CD_PERFIL = 1613 THEN
        begin
            
           SELECT DS_LOGIN 
           INTO NM_USUARIO_W
           FROM USUARIO WHERE NM_USUARIO = :OLD.NM_USUARIO;
           
                      
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NM_USUARIO_W := 'N';
        END;
        
        IF NM_USUARIO_W <> 'N' THEN
                delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w) and GROUPNAME = 'PROXY';
                delete from hsj_autenticacao_wifi where lower(NM_USUARIO) = lower(nm_usuario_w);
        
        END IF;
   
   END IF;     
        
     IF :OLD.CD_PERFIL = 1614 THEN
        begin
            
           SELECT DS_LOGIN 
           INTO NM_USUARIO_W
           FROM USUARIO WHERE NM_USUARIO = :OLD.NM_USUARIO;
           
                      
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NM_USUARIO_W := 'N';
        END;
        
        IF NM_USUARIO_W <> 'N' THEN
                delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w);
                delete from hsj_autenticacao_wifi where lower(NM_USUARIO) = lower(nm_usuario_w);
        
        END IF;
   END IF;
          
end;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_USUARIO_PERFIL_INSERT
before insert ON TASY.USUARIO_PERFIL for each row
declare

    nm_usuario_w varchar(20);

begin
    :new.dt_liberacao := sysdate;
    
    
    
    if :new.cd_perfil = 1614 then
    
        begin
        
            select ds_login
            into nm_usuario_w
            from usuario where nm_usuario = :new.nm_usuario;
            
            exception when no_data_found then
                nm_usuario_w := 'N';
        
        end;
          IF NM_USUARIO_W <> 'N' THEN
                  delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w);
          END IF;
        
    end if;
    
    
     if :new.cd_perfil = 1613 then
    
        begin
        
            select ds_login
            into nm_usuario_w
            from usuario where nm_usuario = :new.nm_usuario;
            
            exception when no_data_found then
                NM_USUARIO_W := 'N';
        
        end;
            IF NM_USUARIO_W <> 'N' THEN
            BEGIN
                delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w);
                insert into radius.RADUSERGROUP (USERNAME, GROUPNAME) values (lower(nm_usuario_w),'PROXY');
            END;
            END IF;
    end if;
    
     if :new.cd_perfil = 1574 then
    
        begin
        
            select ds_login
            into nm_usuario_w
            from usuario where nm_usuario = :new.nm_usuario;
            
            exception when no_data_found then
                NM_USUARIO_W := 'N';
        
        end;
            IF NM_USUARIO_W <> 'N' THEN
            BEGIN
                delete from radius.RADUSERGROUP where lower(username) = lower(nm_usuario_w);
                insert into radius.RADUSERGROUP (USERNAME, GROUPNAME) values (lower(nm_usuario_w),'WIFI');
            END;
            END IF;
    end if;
     
end;
/


ALTER TABLE TASY.USUARIO_PERFIL ADD (
  CONSTRAINT USUPERF_PK
 PRIMARY KEY
 (NM_USUARIO, CD_PERFIL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_PERFIL ADD (
  CONSTRAINT USUPERF_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT USUPERF_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO) 
 REFERENCES TASY.USUARIO (NM_USUARIO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.USUARIO_PERFIL TO NIVEL_1;

