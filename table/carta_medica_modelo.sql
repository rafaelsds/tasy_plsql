ALTER TABLE TASY.CARTA_MEDICA_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARTA_MEDICA_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARTA_MEDICA_MODELO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_MODELO              CLOB,
  CD_PERFIL              NUMBER(5),
  NM_MODELO              VARCHAR2(255 BYTE),
  IE_PRELIMINAR          VARCHAR2(1 BYTE),
  QT_DIVISOR             NUMBER(3),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DS_TITULO_DEFINITIVO   VARCHAR2(255 BYTE),
  DS_TITULO_ADICIONAL    VARCHAR2(255 BYTE),
  IE_METODO_BUSCA_INFO   VARCHAR2(1 BYTE),
  IE_EXIGE_CID           VARCHAR2(1 BYTE),
  IE_MODELO_CIRURGIA     VARCHAR2(1 BYTE),
  IE_CABECALHO_EDITAVEL  VARCHAR2(1 BYTE),
  NR_SEQ_MOVIMENTACAO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_MODELO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAMEMOD_ESTABEL_FK_I ON TASY.CARTA_MEDICA_MODELO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAMEMOD_PERFIL_FK_I ON TASY.CARTA_MEDICA_MODELO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAMEMOD_PK ON TASY.CARTA_MEDICA_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CARTA_MEDICA_MODELO ADD (
  CONSTRAINT CAMEMOD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CARTA_MEDICA_MODELO ADD (
  CONSTRAINT CAMEMOD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CAMEMOD_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.CARTA_MEDICA_MODELO TO NIVEL_1;

