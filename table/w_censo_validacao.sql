DROP TABLE TASY.W_CENSO_VALIDACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CENSO_VALIDACAO
(
  CD_SETOR_ATENDIMENTO  NUMBER(10)              NOT NULL,
  IE_DIA31              NUMBER(10),
  NR_ORDENACAO          NUMBER(10),
  DS_UNIDADE            VARCHAR2(50 BYTE),
  NR_SEQ_INTERNO        NUMBER(10),
  DT_REFERENCIA         DATE,
  IE_DIA01              NUMBER(10),
  IE_DIA02              NUMBER(10),
  IE_DIA03              NUMBER(10),
  IE_DIA04              NUMBER(10),
  IE_DIA05              NUMBER(10),
  IE_DIA06              NUMBER(10),
  IE_DIA07              NUMBER(10),
  IE_DIA08              NUMBER(10),
  IE_DIA09              NUMBER(10),
  IE_DIA10              NUMBER(10),
  IE_DIA11              NUMBER(10),
  IE_DIA12              NUMBER(10),
  IE_DIA13              NUMBER(10),
  IE_DIA14              NUMBER(10),
  IE_DIA15              NUMBER(10),
  IE_DIA16              NUMBER(10),
  IE_DIA17              NUMBER(10),
  IE_DIA18              NUMBER(10),
  IE_DIA19              NUMBER(10),
  IE_DIA20              NUMBER(10),
  IE_DIA21              NUMBER(10),
  IE_DIA22              NUMBER(10),
  IE_DIA23              NUMBER(10),
  IE_DIA24              NUMBER(10),
  IE_DIA25              NUMBER(10),
  IE_DIA26              NUMBER(10),
  IE_DIA27              NUMBER(10),
  IE_DIA28              NUMBER(10),
  IE_DIA29              NUMBER(10),
  IE_DIA30              NUMBER(10),
  DS_SETOR_ATENDIMENTO  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCESVAL_I1 ON TASY.W_CENSO_VALIDACAO
(DT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCESVAL_I1
  MONITORING USAGE;


GRANT SELECT ON TASY.W_CENSO_VALIDACAO TO NIVEL_1;

