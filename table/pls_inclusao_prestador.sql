ALTER TABLE TASY.PLS_INCLUSAO_PRESTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_INCLUSAO_PRESTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_INCLUSAO_PRESTADOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_PRESTADOR         VARCHAR2(60 BYTE)        NOT NULL,
  SG_ESTADO            VARCHAR2(2 BYTE),
  NR_TELEFONE          VARCHAR2(10 BYTE)        NOT NULL,
  NR_CPF               VARCHAR2(11 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  NM_MAE               VARCHAR2(60 BYTE),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  IE_ORIGEM_CADASTRO   VARCHAR2(1 BYTE)         NOT NULL,
  DT_NASCIMENTO        DATE,
  DT_ACEITACAO         DATE,
  DT_REJEICAO          DATE,
  CD_CGC               VARCHAR2(14 BYTE),
  UF_CONSELHO          VARCHAR2(15 BYTE),
  NR_CRM               VARCHAR2(20 BYTE),
  NR_SEQ_CONSELHO      NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4),
  NR_DDI_TELEFONE      VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSINPR_CONPROF_FK_I ON TASY.PLS_INCLUSAO_PRESTADOR
(NR_SEQ_CONSELHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSINPR_CONPROF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSINPR_ESTABEL_FK_I ON TASY.PLS_INCLUSAO_PRESTADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSINPR_PK ON TASY.PLS_INCLUSAO_PRESTADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSINPR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SSJ_INSERIR_SOLICIT_PRESTADOR
AFTER INSERT
ON TASY.PLS_INCLUSAO_PRESTADOR 

FOR EACH ROW
DECLARE

texto_w varchar2(4000);
nr_cpf_w varchar2(14);
cd_cgc_w varchar2(14);
ie_tipo_w varchar2(200);
nr_documento varchar2(200);
ds_email_w varchar2(300);
ds_observacao_w varchar2(300);

BEGIN
    
    nr_cpf_w := :new.nr_cpf;    
    cd_cgc_w := :new.cd_cgc;
    ds_email_w := :new.ds_email;
    ds_observacao_w := :new.ds_observacao;
    
    if(nr_cpf_w is not null)then
        nr_documento:= 'CPF: '||nr_cpf_w;
        ie_tipo_w:= 'PF';
    elsif(cd_cgc_w is not null)then
        nr_documento:= 'CNPJ: '||cd_cgc_w;
        ie_tipo_w:= 'PJ';
    end if;
    
    
    texto_w:='Prestador: '||:new.NM_PRESTADOR||'
Tipo: '||ie_tipo_w||'   '||nr_documento||'
Telefone:  '||to_char(:new.nr_telefone);

    
    if(ds_email_w is not null)then
    
        texto_w:=texto_w||'
E-mail: '||ds_email_w;
    
    end if;

    if(ds_observacao_w is not null)then
    
        texto_w:=texto_w||'
Observação: '||ds_observacao_w;
    
    end if;

    ENVIAR_EMAIL('Solicitação de Credenciamento',texto_w,'noreply@ssjose.com.br','credenciamento@ssjose.com.br;credenciamento2@ssjose.com.br','RAFAELSDS','A'); 

END SSJ_INSERIR_SOLICIT_PRESTADOR;
/


ALTER TABLE TASY.PLS_INCLUSAO_PRESTADOR ADD (
  CONSTRAINT PLSINPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_INCLUSAO_PRESTADOR ADD (
  CONSTRAINT PLSINPR_CONPROF_FK 
 FOREIGN KEY (NR_SEQ_CONSELHO) 
 REFERENCES TASY.CONSELHO_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT PLSINPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_INCLUSAO_PRESTADOR TO NIVEL_1;

