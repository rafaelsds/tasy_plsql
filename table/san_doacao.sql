ALTER TABLE TASY.SAN_DOACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_DOACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_DOACAO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE) NOT NULL,
  NR_SEQ_TIPO                   NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_DOACAO                     DATE,
  QT_COLETADA                   NUMBER(4),
  CD_PF_REALIZOU                VARCHAR2(10 BYTE) NOT NULL,
  NR_SANGUE                     VARCHAR2(20 BYTE),
  NR_BOLSA                      VARCHAR2(20 BYTE),
  QT_PESO                       NUMBER(6,3),
  QT_BPM_PULSO                  NUMBER(3),
  QT_TEMPERATURA                NUMBER(4,2),
  QT_PA_SISTOLICA               NUMBER(3),
  QT_PA_DIASTOLICA              NUMBER(3),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE),
  IE_AVALIACAO_FINAL            VARCHAR2(1 BYTE),
  IE_STATUS                     NUMBER(1),
  NR_SEQ_ANTIC                  NUMBER(10),
  NR_SEC_SAUDE                  VARCHAR2(20 BYTE),
  NR_CONECTOR                   VARCHAR2(20 BYTE),
  NM_USUARIO_LIB                VARCHAR2(15 BYTE),
  DT_LIBERACAO                  DATE,
  CD_PESSOA_DEST                VARCHAR2(60 BYTE),
  NR_MARCA_BOLSA                NUMBER(10),
  QT_MIN_COLETA                 NUMBER(10)      NOT NULL,
  IE_AUTO_EXCLUSAO              VARCHAR2(1 BYTE),
  NR_MOTIVO_DESISTENCIA         NUMBER(10),
  DT_TRIAGEM                    DATE,
  NM_USUARIO_SEG_LIB            VARCHAR2(15 BYTE),
  IE_TIPO_COLETA                NUMBER(2),
  NR_SEQ_DERIVADO               NUMBER(10),
  DT_RETORNO                    DATE,
  IE_TERAPEUTICA                VARCHAR2(1 BYTE),
  QT_DERIVADO                   NUMBER(10),
  QT_ESTIMADA                   NUMBER(5),
  QT_PROCESSADA                 NUMBER(5),
  QT_ANTIGOAG                   NUMBER(5),
  NR_LOTE_ANTIGOAG              VARCHAR2(20 BYTE),
  NR_LOTE_SF                    VARCHAR2(20 BYTE),
  NR_LOTE_KIT                   VARCHAR2(20 BYTE),
  DT_COLETA                     DATE,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  PR_HEMATOCRITO                NUMBER(10,4),
  IE_TIPO_BOLSA                 VARCHAR2(5 BYTE),
  CD_PESSOA_COLETA              VARCHAR2(10 BYTE),
  IE_BRACO_PUNCIONADO           VARCHAR2(1 BYTE),
  DT_VALIDADE_BOLSA             DATE,
  DT_INUTILIZACAO               DATE,
  IE_TIPO_DOADOR                VARCHAR2(15 BYTE),
  IE_STATUS_ANT                 NUMBER(1),
  DT_INICIO_COLETA              DATE,
  DT_TRIAGEM_FISICA             DATE,
  NR_ATENDIMENTO                NUMBER(10),
  NR_SEQ_STATUS                 NUMBER(10),
  QT_HEMOGLOBINA                NUMBER(10,3),
  DS_DESTINARIO                 VARCHAR2(90 BYTE),
  QT_ALTURA                     NUMBER(4,1),
  IE_REALIZA_NAT                VARCHAR2(1 BYTE),
  CD_PESSOA_ENTREGA             VARCHAR2(10 BYTE),
  CD_PESSOA_RECEBIMENTO         VARCHAR2(10 BYTE),
  DT_FABRICACAO_BOLSA           DATE,
  NR_LOTE_BOLSA                 VARCHAR2(20 BYTE),
  NM_USUARIO_LIB_IMPED          VARCHAR2(15 BYTE),
  NM_USUARIO_INI_COLETA         VARCHAR2(15 BYTE),
  NM_USUARIO_FIM_COLETA         VARCHAR2(15 BYTE),
  DT_LIB_IMPEDIMENTO            DATE,
  NR_SEQ_MAQUINA                NUMBER(10),
  DT_FIM_COLETA                 DATE,
  NR_SEQ_LOCAL                  NUMBER(10),
  NR_SEQ_TRANSFUSAO             NUMBER(10),
  NR_SEQ_RESERVA                NUMBER(10),
  DT_VALIDADE_LOTE_ANTIG        DATE,
  DT_VALIDADE_LOTE_SF           DATE,
  DT_FABRICACAO_LOTE_SF         DATE,
  DT_FABRICACAO_LOTE_ANTIG      DATE,
  QT_PESO_TOTAL_BOLSA           NUMBER(10,3),
  NM_USUARIO_CONFERENCIA        VARCHAR2(15 BYTE),
  DT_CONFERENCIA                DATE,
  DT_FABRICACAO_ANTI            DATE,
  DT_VALIDADE_KIT_AFERESE       DATE,
  DT_INICIO_AFERESE             DATE,
  DT_FIM_AFERESE                DATE,
  QT_PLAQUETA                   NUMBER(10),
  DT_INICIO_DEGERMACAO          DATE,
  NM_USUARIO_INI_DEGERMACAO     VARCHAR2(15 BYTE),
  DT_FIM_DEGERMACAO             DATE,
  NM_USUARIO_FIM_DEGERMACAO     VARCHAR2(15 BYTE),
  NM_SOLUCAO_FISIO              VARCHAR2(255 BYTE),
  QT_PA_SISTOLICA_POS           NUMBER(3),
  QT_PA_DIASTOLICA_POS          NUMBER(3),
  IE_RESP_QUESTIONARIO          VARCHAR2(1 BYTE),
  NR_SEQ_CAMPANHA               NUMBER(10),
  DT_CONFERENCIA_SEG            DATE,
  NM_USUARIO_CONFERENCIA_SEG    VARCHAR2(15 BYTE),
  CD_BARRAS_INTEGRACAO          VARCHAR2(20 BYTE),
  NR_SEQ_DOACAO_AMOSTRA         NUMBER(10),
  IE_REALIZA_CRIOPRECIPITADO    VARCHAR2(1 BYTE),
  IE_BRACO_RETORNO              VARCHAR2(1 BYTE),
  DT_FABRICACAO_KIT             DATE,
  DT_INICIO_TRIAGEM_FISICA      DATE,
  NM_USUARIO_INI_TRI_FISICA     VARCHAR2(15 BYTE),
  DT_INICIO_TRIAGEM             DATE,
  NM_USUARIO_INI_TRIAGEM        VARCHAR2(15 BYTE),
  DT_INICIO_FILTRAGEM           DATE,
  NM_USUARIO_INICIO_FILTRAGEM   VARCHAR2(15 BYTE),
  DT_FIM_FILTRAGEM              DATE,
  NM_USUARIO_FIM_FILTRAGEM      VARCHAR2(15 BYTE),
  DT_FIM_REPOUSO                DATE,
  QT_PESO_BOLSA_PADRAO          NUMBER(10,3),
  QT_VOLUME_REAL                NUMBER(10,3),
  NM_USUARIO_INI_REPOUSO        VARCHAR2(15 BYTE),
  NR_SEQ_DERIVADO_PADRAO        NUMBER(10),
  DT_INICIO_REPOUSO             DATE,
  NM_USUARIO_FIM_REPOUSO        VARCHAR2(15 BYTE),
  NM_USUARIO_FIM_PROD           VARCHAR2(15 BYTE),
  DT_FIM_PROD_DOACAO            DATE,
  DS_JUSTIFICATIVA_DESISTENCIA  VARCHAR2(255 BYTE),
  CD_BARRAS                     VARCHAR2(80 BYTE),
  QT_VOLUME_ESTIMADO            NUMBER(4),
  NR_SEQ_EQUIPAMENTO_BOLSA      NUMBER(10),
  IE_FRACIONAR_BOLSA            VARCHAR2(1 BYTE),
  NR_SEQ_CHEGADA_COLETA         NUMBER(5),
  DT_LIBERACAO_CADASTRO         DATE,
  QT_FREQ_CARDIACA              NUMBER(3),
  NM_MARCA_ANTICOAGULANTE       VARCHAR2(80 BYTE),
  NM_MARCA_SOL_FISIO            VARCHAR2(80 BYTE),
  QT_AMOSTRA                    NUMBER(3),
  DS_LOCAL_DESISTENCIA          VARCHAR2(80 BYTE),
  DT_RECEBIMENTO_BOLSA          DATE,
  NM_USUARIO_RECEBIMENTO        VARCHAR2(15 BYTE),
  NM_USUARIO_ORIGINAL           VARCHAR2(15 BYTE),
  QT_VOLUME_ATUAL               NUMBER(4),
  DS_ORIENTACOES                VARCHAR2(255 BYTE),
  QT_LEUCOCITO                  NUMBER(10),
  DT_INICIO_COLETA_REAL         DATE,
  DT_FIM_COLETA_REAL            DATE,
  VL_FLUXO                      NUMBER(15,4),
  NR_SEQ_INUTIL                 NUMBER(10),
  DT_CONFERENCIA_INTEGRACAO     DATE,
  NM_USUARIO_CONF_INTEGRACAO    VARCHAR2(15 BYTE),
  NR_OPCAO_FRAC                 NUMBER(5),
  QT_VOLUME_ESTIMADO_YIELD      NUMBER(4,2),
  QT_VOLUME_COLETADO_YIELD      NUMBER(4,2),
  DS_JUSTIFICATIVA_LIB          VARCHAR2(255 BYTE),
  IE_APTO_AMOSTRA               VARCHAR2(1 BYTE),
  DT_ENTREGA_EXAME              DATE,
  IE_DMS                        VARCHAR2(1 BYTE),
  CD_PESSOA_INICIO_COLETA       VARCHAR2(10 BYTE),
  CD_PESSOA_FIM_COLETA          VARCHAR2(10 BYTE),
  NR_CENTRIFUGA                 VARCHAR2(10 BYTE),
  CD_RESULT_COD                 VARCHAR2(10 BYTE),
  HR_TOTAL                      VARCHAR2(10 BYTE),
  NR_PROGRAMA                   VARCHAR2(10 BYTE),
  NM_PROGRAMA                   VARCHAR2(50 BYTE),
  NM_USUARIO_INUT               VARCHAR2(15 BYTE),
  DS_MOTIVO_INUTIL              VARCHAR2(255 BYTE),
  NR_SEQ_ISBT                   NUMBER(6),
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_BARRAS_BOLSA               VARCHAR2(80 BYTE),
  QT_VOLUME_ANTIC               NUMBER(4),
  DT_IMPRESSAO                  DATE,
  NM_USUARIO_IMPRESSAO          VARCHAR2(15 BYTE),
  QT_SATURACAO_O2               NUMBER(3),
  NR_SEQ_ANTIC_AFERESE          NUMBER(10),
  DT_EXAME_APROV                DATE,
  NM_USUARIO_EXAME_APROV        VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_INUTIL          NUMBER(10),
  NR_SEQ_TOPOGRAFIA             NUMBER(10),
  IE_LADO                       VARCHAR2(2 BYTE),
  IE_COND_SAT_O2                VARCHAR2(2 BYTE),
  IE_MEMBRO_SAT_O2              VARCHAR2(3 BYTE),
  NR_SEQ_PAC_SENHA_FILA         NUMBER(10),
  CD_ISBT_ANTIGENO              VARCHAR2(18 BYTE),
  DT_FIM_TRIAGEM_FISICA         DATE,
  CD_TEST_ESP_GER_ISBT          VARCHAR2(5 BYTE),
  DT_TRIAGEM_CLINICA            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANDOAC_ESTABEL_FK_I ON TASY.SAN_DOACAO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_I1 ON TASY.SAN_DOACAO
(IE_STATUS, DT_DOACAO, IE_AVALIACAO_FINAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_I2 ON TASY.SAN_DOACAO
(IE_STATUS, IE_AVALIACAO_FINAL, NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PACSEFI_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK_I ON TASY.SAN_DOACAO
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK_I2 ON TASY.SAN_DOACAO
(CD_PF_REALIZOU)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK3_I ON TASY.SAN_DOACAO
(CD_PESSOA_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK4_I ON TASY.SAN_DOACAO
(CD_PESSOA_COLETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK5_I ON TASY.SAN_DOACAO
(CD_PESSOA_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK6_I ON TASY.SAN_DOACAO
(CD_PESSOA_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK7_I ON TASY.SAN_DOACAO
(CD_PESSOA_INICIO_COLETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_PESFISI_FK8_I ON TASY.SAN_DOACAO
(CD_PESSOA_FIM_COLETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANDOAC_PK ON TASY.SAN_DOACAO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_SANANTI_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_ANTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANANTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANANTI_FK2_I ON TASY.SAN_DOACAO
(NR_SEQ_ANTIC_AFERESE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_SANCADO_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_CAMPANHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANCADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANDERI_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_DERIVADO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANDERI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANDERI_FK2_I ON TASY.SAN_DOACAO
(NR_SEQ_DERIVADO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANDERI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANEQBO_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_EQUIPAMENTO_BOLSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANEQBO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANINUT_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_INUTIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANINUT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANLODO_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANLODO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANMABO_FK_I ON TASY.SAN_DOACAO
(NR_MARCA_BOLSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANMABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANMAQU_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANMAQU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANMODE_FK_I ON TASY.SAN_DOACAO
(NR_MOTIVO_DESISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANMOIN_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_MOTIVO_INUTIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDOAC_SANRESE_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_RESERVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANRESE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANSTDO_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANSTDO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANTIDO_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_TIPO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANTIDO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_SANTRAN_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_TRANSFUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDOAC_SANTRAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANDOAC_TOPDOR_FK_I ON TASY.SAN_DOACAO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.San_Doacao_Atual
before insert or update ON TASY.SAN_DOACAO for each row
declare

nr_seq_derivado_padrao_w	number(10,0);
nr_seq_tipo_bolsa_w			varchar2(2);
nr_valor_parametro_w		varchar2(2);

begin

nr_valor_parametro_w := Obter_Valor_Param_Usuario(450,362, obter_perfil_ativo, :new.nm_usuario,obter_estabelecimento_ativo);

if	(updating) and
	(:new.ie_tipo_coleta is not null) then

	select	max(nr_seq_derivado)
	into	nr_seq_derivado_padrao_w
	from	san_regra_coleta_derivado
	where	ie_tipo_coleta = :new.ie_tipo_coleta
	and	nvl(ie_situacao,'A') = 'A';

	:new.nr_seq_derivado_padrao := nr_seq_derivado_padrao_w;

end if;

if	(nr_valor_parametro_w is null) and
	((:new.dt_fim_coleta is null) and (:old.dt_fim_coleta is null)) then

	select 	max(ie_tipo_bolsa)
	into	nr_seq_tipo_bolsa_w
	from 	san_regra_tipo_bolsa
	where 	nr_seq_tipo_doacao = :new.nr_seq_tipo
	and   	ie_tipo_coleta = :new.ie_tipo_coleta
	and   	ie_situacao = 'A';

	if	(nr_seq_tipo_bolsa_w is not null) then

		:new.ie_tipo_bolsa := nr_seq_tipo_bolsa_w;

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SAN_DOACAO_tp  after update ON TASY.SAN_DOACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DOACAO,1,4000),substr(:new.DT_DOACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_DOACAO',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SANGUE,1,4000),substr(:new.NR_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SANGUE',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COLETA,1,4000),substr(:new.IE_TIPO_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COLETA',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_BOLSA,1,4000),substr(:new.NR_BOLSA,1,4000),:new.nm_usuario,nr_seq_w,'NR_BOLSA',ie_log_w,ds_w,'SAN_DOACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.San_Doacao_Update
before update ON TASY.SAN_DOACAO for each row
declare
ie_sexo_w		varchar2(1);
qt_reg_w		number(1);
ie_altera_volume_w	varchar2(1);
ds_parametro_459_w	varchar2(10);
ie_gerar_nr_sangue_w	san_parametro.ie_gerar_nr_sangue%type;
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	nvl(max(ie_altera_volume),'S')
into	ie_altera_volume_w
from	san_parametro
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(max(ie_gerar_nr_sangue), 'N')
into	ie_gerar_nr_sangue_w
from	san_parametro
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	((ie_altera_volume_w = 'S') and (:new.qt_peso is not null) and (nvl(:new.qt_peso,0) <> nvl(:old.qt_peso,0))) then

	select	nvl(max(ie_sexo),'X')
	into	ie_sexo_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	if	(ie_sexo_w = 'F') then
		:new.qt_coletada := :new.qt_peso * 8;
	elsif	(ie_sexo_w = 'M') then
		:new.qt_coletada := :new.qt_peso * 9;
	end if;

	if	(:new.qt_coletada > 500) then
		:new.qt_coletada := 500;
	end if;
end if;
if	(:old.dt_fim_coleta <> :new.dt_fim_coleta) or (:old.dt_fim_coleta is null and :new.dt_fim_coleta is not null) then
	:new.dt_fim_coleta	:= to_date(to_char(nvl(:old.dt_fim_coleta,sysdate),'dd/mm/yyyy')||' '||to_char(:new.dt_fim_coleta,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
end if;


if	((:old.cd_pessoa_fisica is not null) and
	 (:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica)) then

	ds_parametro_459_w := obter_valor_param_usuario(450,459,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);

	if	(:new.dt_liberacao_cadastro is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(283961);
	elsif	(nvl(ds_parametro_459_w,'S') = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(283966);
	end if;
end if;

if	(:new.nr_sangue is not null) and
	(ie_gerar_nr_sangue_w = 'S') then

	if 	(:new.nr_sequencia <> san_existe_nr_sangue(:new.nr_sequencia, :new.nr_sangue)) then
		gravar_log_atendimento_pragma(120, null, obter_expressao_dic_objeto(75481) || '(' || :new.nr_sangue || ')', wheb_usuario_pck.get_nm_usuario);
		wheb_mensagem_pck.exibir_mensagem_abort(75481);
	end if;

end if;

if	 ((:old.qt_peso <> :new.qt_peso) or (:old.qt_peso is null and  :new.qt_peso is not null))  then

	update 	pessoa_fisica
	set	qt_peso = :new.qt_peso
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

end if;

if	 ((:old.qt_altura <> :new.qt_altura) or (:old.qt_altura is null and  :new.qt_altura is not null))   then

	update 	pessoa_fisica
	set	qt_altura_cm = :new.qt_altura
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.SAN_DOACAO ADD (
  CONSTRAINT SANDOAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_DOACAO ADD (
  CONSTRAINT SANDOAC_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANCADO_FK 
 FOREIGN KEY (NR_SEQ_CAMPANHA) 
 REFERENCES TASY.SAN_CAMPANHA_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANDERI_FK2 
 FOREIGN KEY (NR_SEQ_DERIVADO_PADRAO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANEQBO_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO_BOLSA) 
 REFERENCES TASY.SAN_EQUIPAMENTO_BOLSA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANINUT_FK 
 FOREIGN KEY (NR_SEQ_INUTIL) 
 REFERENCES TASY.SAN_INUTILIZACAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_PESFISI_FK7 
 FOREIGN KEY (CD_PESSOA_INICIO_COLETA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_PESFISI_FK8 
 FOREIGN KEY (CD_PESSOA_FIM_COLETA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_SANMODE_FK 
 FOREIGN KEY (NR_MOTIVO_DESISTENCIA) 
 REFERENCES TASY.SAN_MOTIVO_DESISTENCIA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANMABO_FK 
 FOREIGN KEY (NR_MARCA_BOLSA) 
 REFERENCES TASY.SAN_MARCA_BOLSA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANDOAC_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_DEST) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_SANTIDO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.SAN_TIPO_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_PESFISI_FK2 
 FOREIGN KEY (CD_PF_REALIZOU) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_SANANTI_FK 
 FOREIGN KEY (NR_SEQ_ANTIC) 
 REFERENCES TASY.SAN_ANTICOAGULANTE (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_PESFISI_FK4 
 FOREIGN KEY (CD_PESSOA_COLETA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_SANSTDO_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.SAN_STATUS_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_PESFISI_FK5 
 FOREIGN KEY (CD_PESSOA_ENTREGA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_PESFISI_FK6 
 FOREIGN KEY (CD_PESSOA_RECEBIMENTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANDOAC_SANMAQU_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.SAN_MAQUINA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANLODO_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.SAN_LOCAL_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANRESE_FK 
 FOREIGN KEY (NR_SEQ_RESERVA) 
 REFERENCES TASY.SAN_RESERVA (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANTRAN_FK 
 FOREIGN KEY (NR_SEQ_TRANSFUSAO) 
 REFERENCES TASY.SAN_TRANSFUSAO (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANANTI_FK2 
 FOREIGN KEY (NR_SEQ_ANTIC_AFERESE) 
 REFERENCES TASY.SAN_ANTICOAGULANTE (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_SANMOIN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INUTIL) 
 REFERENCES TASY.SAN_MOTIVO_INUTIL (NR_SEQUENCIA),
  CONSTRAINT SANDOAC_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_DOACAO TO NIVEL_1;

