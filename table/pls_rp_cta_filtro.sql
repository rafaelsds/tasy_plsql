ALTER TABLE TASY.PLS_RP_CTA_FILTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RP_CTA_FILTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RP_CTA_FILTRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RP_COMBINADA  NUMBER(10)               NOT NULL,
  NM_FILTRO            VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_FILTRO_CONTA      VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_BENEF      VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_PREST      VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_PRODUTO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_PROC       VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPCFIL_PK ON TASY.PLS_RP_CTA_FILTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPCFIL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRPCFIL_PRCCOMB_FK_I ON TASY.PLS_RP_CTA_FILTRO
(NR_SEQ_RP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPCFIL_PRCCOMB_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_RP_CTA_FILTRO_tp  after update ON TASY.PLS_RP_CTA_FILTRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_RP_COMBINADA,1,4000),substr(:new.NR_SEQ_RP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RP_COMBINADA',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FILTRO,1,4000),substr(:new.NM_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NM_FILTRO',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_CONTA,1,4000),substr(:new.IE_FILTRO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_CONTA',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PREST,1,4000),substr(:new.IE_FILTRO_PREST,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PREST',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PRODUTO,1,4000),substr(:new.IE_FILTRO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PRODUTO',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROC,1,4000),substr(:new.IE_FILTRO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROC',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_BENEF,1,4000),substr(:new.IE_FILTRO_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_BENEF',ie_log_w,ds_w,'PLS_RP_CTA_FILTRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_RP_CTA_FILTRO ADD (
  CONSTRAINT PRPCFIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RP_CTA_FILTRO ADD (
  CONSTRAINT PRPCFIL_PRCCOMB_FK 
 FOREIGN KEY (NR_SEQ_RP_COMBINADA) 
 REFERENCES TASY.PLS_RP_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RP_CTA_FILTRO TO NIVEL_1;

