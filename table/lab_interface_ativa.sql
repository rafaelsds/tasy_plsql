ALTER TABLE TASY.LAB_INTERFACE_ATIVA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_INTERFACE_ATIVA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_INTERFACE_ATIVA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DS_DIR_PROBLEMA           VARCHAR2(500 BYTE),
  IE_UTILIZA_USUARIO_EQUIP  VARCHAR2(1 BYTE)    NOT NULL,
  IE_IGNORA_PONTUACAO       VARCHAR2(1 BYTE)    NOT NULL,
  IE_LOGIN_ALTERNATIVO      VARCHAR2(1 BYTE)    NOT NULL,
  IE_ESTAB_PRESCRICAO       VARCHAR2(1 BYTE)    NOT NULL,
  DS_DIR_DEPOIS_IMPORT      VARCHAR2(500 BYTE),
  DS_DIR_IMPORT             VARCHAR2(500 BYTE),
  DS_DIR_EXPORT             VARCHAR2(500 BYTE),
  NR_SEQ_RETORNO            NUMBER(10),
  NR_SEQ_ENVIO              NUMBER(10),
  NR_SEQ_EQUIP              NUMBER(5)           NOT NULL,
  IE_TIPO                   VARCHAR2(1 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LABINTATI_EQUILAB_FK_I ON TASY.LAB_INTERFACE_ATIVA
(NR_SEQ_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LABINTATI_LABINTENV_FK_I ON TASY.LAB_INTERFACE_ATIVA
(NR_SEQ_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LABINTATI_LABINTRET_FK_I ON TASY.LAB_INTERFACE_ATIVA
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABINTATI_PK ON TASY.LAB_INTERFACE_ATIVA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lab_interface_ativa_update
before update ON TASY.LAB_INTERFACE_ATIVA for each row
declare

active_interfaces_count_w number(10);
cd_interface_w 			 	number(10,0);
cd_message_w				number(10,0);

begin

	cd_message_w := 0;

	if ((:new.nr_seq_envio is null) and (:new.nr_seq_retorno is null)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(338086);
	end if;

	if	(:new.ie_tipo <> :old.ie_tipo) then
		Wheb_mensagem_pck.exibir_mensagem_abort(338039);
	end if;

	if (((:old.ie_situacao = 'I') and (:new.ie_situacao = 'A'))
		or ((:new.ie_situacao = 'A') and (:new.nr_seq_retorno <> :old.nr_seq_retorno))
		or ((:new.ie_situacao = 'A') and (:new.nr_seq_envio <> :old.nr_seq_envio))) then

		select 	get_active_interface_count(:new.nr_seq_retorno, :new.nr_seq_retorno)
		into cd_message_w
		from dual;

		if(cd_message_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(cd_message_w);
		end if;

	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.lab_interface_ativa_delete
before delete ON TASY.LAB_INTERFACE_ATIVA for each row
begin
	Wheb_mensagem_pck.exibir_mensagem_abort(338088);
end;
/


CREATE OR REPLACE TRIGGER TASY.lab_interface_ativa_insert
before insert ON TASY.LAB_INTERFACE_ATIVA for each row
declare

active_interfaces_count_w 	number(10,0);
cd_interface_w 			 	number(10,0);
cd_message_w				number(10,0);


begin

	if ((:new.nr_seq_envio is null) and (:new.nr_seq_retorno is null)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(338086);
	end if;

	select 	get_active_interface_count(:new.nr_seq_envio, :new.nr_seq_retorno)
	into cd_message_w
	from dual;

	if(cd_message_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(cd_message_w);
	end if;

end;
/


ALTER TABLE TASY.LAB_INTERFACE_ATIVA ADD (
  CONSTRAINT LABINTATI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_INTERFACE_ATIVA ADD (
  CONSTRAINT LABINTATI_EQUILAB_FK 
 FOREIGN KEY (NR_SEQ_EQUIP) 
 REFERENCES TASY.EQUIPAMENTO_LAB (CD_EQUIPAMENTO),
  CONSTRAINT LABINTATI_LABINTENV_FK 
 FOREIGN KEY (NR_SEQ_ENVIO) 
 REFERENCES TASY.LAB_INTERFACE_ENVIO (NR_SEQUENCIA),
  CONSTRAINT LABINTATI_LABINTRET_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.LAB_INTERFACE_RETORNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_INTERFACE_ATIVA TO NIVEL_1;

