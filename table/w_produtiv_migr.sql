ALTER TABLE TASY.W_PRODUTIV_MIGR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PRODUTIV_MIGR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PRODUTIV_MIGR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  DT_REFERENCIA           DATE                  NOT NULL,
  NM_USUARIO_COLABORADOR  VARCHAR2(15 BYTE),
  QT_OS_REVISAO           NUMBER(10)            NOT NULL,
  QT_OS_ANTIGA            NUMBER(10)            NOT NULL,
  QT_HORAS_MIGRACAO       NUMBER(15,2)          NOT NULL,
  QT_HORAS_REVISAO        NUMBER(15,2)          NOT NULL,
  QT_HORAS_NECESSIDADES   NUMBER(15,2)          NOT NULL,
  QT_HORAS_TERCEIROS      NUMBER(15,2)          NOT NULL,
  QT_HORAS_COLABORADOR    NUMBER(15,2)          NOT NULL,
  QT_OS_REVISAO_NOVA      NUMBER(10)            NOT NULL,
  QT_OS_REVISAO_ATUAL     NUMBER(10)            NOT NULL,
  QT_OS_ANTIGA_ATUAL      NUMBER(10)            NOT NULL,
  IE_ANALISE              VARCHAR2(2 BYTE)      NOT NULL,
  NR_SEQ_AREA             NUMBER(10),
  QT_HORAS_OUT_ATIV       NUMBER(15,2),
  QT_HORAS_TESTE          NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPRODMI_PK ON TASY.W_PRODUTIV_MIGR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRODMI_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PRODUTIV_MIGR ADD (
  CONSTRAINT WPRODMI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PRODUTIV_MIGR TO NIVEL_1;

