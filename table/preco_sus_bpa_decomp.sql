DROP TABLE TASY.PRECO_SUS_BPA_DECOMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_SUS_BPA_DECOMP
(
  DT_COMPETENCIA        DATE                    NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(1)               NOT NULL,
  VL_TPH                NUMBER(11,2)            NOT NULL,
  VL_TAXA_SALA          NUMBER(11,2)            NOT NULL,
  VL_HONORARIO_MEDICO   NUMBER(11,2)            NOT NULL,
  VL_ANESTESIA          NUMBER(11,2)            NOT NULL,
  VL_MATMED             NUMBER(11,2)            NOT NULL,
  VL_CONTRASTE          NUMBER(11,2)            NOT NULL,
  VL_FILME_RX           NUMBER(11,2)            NOT NULL,
  VL_GESSO              NUMBER(11,2)            NOT NULL,
  VL_QUIMIOTERAPIA      NUMBER(11,2)            NOT NULL,
  VL_DIALISE            NUMBER(11,2)            NOT NULL,
  VL_SADT_RX            NUMBER(11,2)            NOT NULL,
  VL_SADT_PC            NUMBER(11,2)            NOT NULL,
  VL_SADT_OUTROS        NUMBER(11,2)            NOT NULL,
  VL_FILME_RESSONANCIA  NUMBER(11,2)            NOT NULL,
  VL_OUTROS             NUMBER(11,2)            NOT NULL,
  VL_PROCEDIMENTO       NUMBER(11,2)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  VL_PLANTONISTA        NUMBER(15,2),
  IE_VERSAO             VARCHAR2(20 BYTE),
  IE_CESSAO_CREDITO     VARCHAR2(1 BYTE),
  IE_PAB                VARCHAR2(1 BYTE),
  IE_FAEC               VARCHAR2(1 BYTE),
  CD_RUB                VARCHAR2(4 BYTE),
  DS_AUX                VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.PRECO_SUS_BPA_DECOMP TO NIVEL_1;

