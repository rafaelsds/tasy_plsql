ALTER TABLE TASY.AG_PORTAL_PASSO1
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AG_PORTAL_PASSO1 CASCADE CONSTRAINTS;

CREATE TABLE TASY.AG_PORTAL_PASSO1
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DT_INICIAL           DATE                     NOT NULL,
  DT_FINAL             DATE                     NOT NULL,
  IE_TURNO             VARCHAR2(1 BYTE)         NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGPORPA1_PK ON TASY.AG_PORTAL_PASSO1
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AG_PORTAL_PASSO1 ADD (
  CONSTRAINT AGPORPA1_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AG_PORTAL_PASSO1 TO NIVEL_1;

