ALTER TABLE TASY.PLS_RET_AUDITORIA_ESTIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RET_AUDITORIA_ESTIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RET_AUDITORIA_ESTIP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_GUIA_CONSULTA         VARCHAR2(1 BYTE),
  IE_GUIA_SPSADT           VARCHAR2(1 BYTE),
  IE_GUIA_INTERNACAO       VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_AUDITOR     NUMBER(10),
  IE_STATUS_ITEM_ANALISE   NUMBER(2),
  IE_GUIA_PRORROGACAO_INT  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRAES_PK ON TASY.PLS_RET_AUDITORIA_ESTIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRAES_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRAES_PLSGRAU_FK_I ON TASY.PLS_RET_AUDITORIA_ESTIP
(NR_SEQ_GRUPO_AUDITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRAES_PLSGRAU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_RET_AUDITORIA_ESTIP ADD (
  CONSTRAINT PLSRAES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RET_AUDITORIA_ESTIP ADD (
  CONSTRAINT PLSRAES_PLSGRAU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AUDITOR) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RET_AUDITORIA_ESTIP TO NIVEL_1;

