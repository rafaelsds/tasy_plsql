ALTER TABLE TASY.PRESCR_PROC_MAT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_MAT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_MAT_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_PRESCRICAO           NUMBER(10)            NOT NULL,
  NR_SEQ_PRESCR           NUMBER(10)            NOT NULL,
  NR_SEQ_PRESCR_PROC_MAT  NUMBER(10)            NOT NULL,
  IE_STATUS               NUMBER(2)             NOT NULL,
  CD_BARRAS               VARCHAR2(100 BYTE),
  DT_INTEGRACAO           DATE,
  NR_SEQ_FRASCO           NUMBER(10),
  IE_SUSPENSO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          48M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPEMIT_FRAEXLA_FK_I ON TASY.PRESCR_PROC_MAT_ITEM
(NR_SEQ_FRASCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPEMIT_FRAEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPEMIT_I1 ON TASY.PRESCR_PROC_MAT_ITEM
(IE_STATUS, DT_INTEGRACAO, NR_SEQ_PRESCR, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPEMIT_I2 ON TASY.PRESCR_PROC_MAT_ITEM
(NR_PRESCRICAO, NR_SEQ_PRESCR, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPEMIT_I3 ON TASY.PRESCR_PROC_MAT_ITEM
(CD_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPEMIT_PK ON TASY.PRESCR_PROC_MAT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPEMIT_PRESPRO_FK_I ON TASY.PRESCR_PROC_MAT_ITEM
(NR_PRESCRICAO, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPEMIT_PRPRMAT_FK_I ON TASY.PRESCR_PROC_MAT_ITEM
(NR_SEQ_PRESCR_PROC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_proc_mat_item_upd
BEFORE UPDATE ON TASY.PRESCR_PROC_MAT_ITEM FOR EACH ROW
DECLARE

nr_prescricao_w			prescR_medica.nr_prescricao%TYPE;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%TYPE;
nr_seq_exame_w			exame_laboratorio.nr_seq_exame%TYPE;
nr_atendimento_w		atendimento_paciente.nr_atendimento%TYPE;
ie_status_envio_w		lab_parametro.ie_status_envio%TYPE;
nr_ordem_amostra_w		exame_laboratorio.nr_ordem_amostra%type;
ie_formato_resultado_w		VARCHAR2(10);
qt_prescr_proc_material_w	NUMBER(10);
ds_sep_bv_w			varchar2(100);
DS_PARAM_INTEG_HL7_W		varchar2(2000);

BEGIN

	IF	(wheb_usuario_pck.is_evento_ativo(84) = 'S') THEN

		SELECT	MAX(nr_prescricao)
		INTO	nr_prescricao_w
		FROM	prescr_proc_material
		WHERE	nr_sequencia = :NEW.nr_seq_prescr_proc_mat;

		SELECT	MAX(nr_atendimento),
			MAX(cd_estabelecimento)
		INTO	nr_atendimento_w,
			cd_estabelecimento_w
		FROM	prescr_medica a
		WHERE	a.nr_prescricao = nr_prescricao_w;

		SELECT	MAX(ie_status_envio)
		INTO	ie_status_envio_w
		FROM	lab_parametro
		WHERE	cd_estabelecimento = cd_estabelecimento_w;


		IF	((:NEW.ie_status =  ie_status_envio_w) AND (:OLD.ie_status < ie_status_envio_w)) THEN



			/*SELECT	MAX(a.nr_seq_exame)
			INTO	nr_seq_exame_w
			FROM	prescR_procedimento a
			WHERE	a.nr_prescricao = nr_prescricao_w
			AND	a.nr_sequencia = :NEW.nr_seq_prescr;*/

			nr_seq_exame_w	:= lab_obter_exame_pragma(nr_prescricao_w, :NEW.nr_seq_prescr);

			SELECT	MAX(ie_formato_resultado)
			INTO	ie_formato_resultado_w
			FROM	exame_laboratorio
			WHERE	nr_seq_exame = nr_seq_exame_w;

			select	count(*)
			into	NR_ORDEM_AMOSTRA_w
			from	exame_laboratorio
			where	nr_seq_superior = nr_seq_exame_w
			and	nr_ordem_amostra is not null;

			ds_sep_bv_w := obter_separador_bv;

			IF	(ie_formato_resultado_w NOT IN ('SM','SDM')) and
				(NR_ORDEM_AMOSTRA_w > 0) THEN

				ds_param_integ_hl7_w :=	'nr_atendimento='   || nr_atendimento_w   || ds_sep_bv_w ||
							'nr_prescricao='    || nr_prescricao_w || ds_sep_bv_w ||
							'nr_seq_presc='	    || :NEW.nr_seq_prescr  || ds_sep_bv_w||
							'nr_seq_prescr_proc_mat='||:new.nr_seq_prescr_proc_mat|| ds_sep_bv_w ;

				gravar_agend_integracao(84, ds_param_integ_hl7_w);

			end if;

		END IF;

	END IF;


   if (:new.ie_status = 20) and (:old.ie_status < 20) and
      (wheb_usuario_pck.is_evento_ativo(705) = 'S') then
      abbott_enviar_prescr(:new.nr_prescricao, :new.nr_seq_prescr, :new.nr_seq_prescr_proc_mat);
   elsif (:new.ie_status = 10) and (:old.ie_status > :new.ie_status) and
      (wheb_usuario_pck.is_evento_ativo(705) = 'S') then
      abbott_enviar_prescr(:new.nr_prescricao, :new.nr_seq_prescr, :new.nr_seq_prescr_proc_mat, 'RR');
   end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_proc_mat_item_log
BEFORE UPDATE ON TASY.PRESCR_PROC_MAT_ITEM FOR EACH ROW
BEGIN
    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        IF (:old.ie_status <> :new.ie_status) THEN
            gravar_log_lab_pragma(
                CD_LOG_P => 17,
                DS_LOG_P => substr(
                     ':NEW.NR_SEQUENCIA: '           || to_char(:new.NR_SEQUENCIA)
                || ' - :NEW.DT_ATUALIZACAO: '         || to_char(:new.DT_ATUALIZACAO, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :NEW.NM_USUARIO: '             || to_char(:new.NM_USUARIO)
                || ' - :NEW.DT_ATUALIZACAO_NREC: '    || to_char(:new.DT_ATUALIZACAO_NREC, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :NEW.NM_USUARIO_NREC: '        || to_char(:new.NM_USUARIO_NREC)
                || ' - :NEW.NR_PRESCRICAO: '          || to_char(:new.NR_PRESCRICAO)
                || ' - :NEW.NR_SEQ_PRESCR: '          || to_char(:new.NR_SEQ_PRESCR)
                || ' - :NEW.NR_SEQ_PRESCR_PROC_MAT: ' || to_char(:new.NR_SEQ_PRESCR_PROC_MAT)
                || ' - :NEW.IE_STATUS: '              || to_char(:new.IE_STATUS)
                || ' - :NEW.CD_BARRAS: '              || to_char(:new.CD_BARRAS)
                || ' - :NEW.DT_INTEGRACAO: '          || to_char(:new.DT_INTEGRACAO, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :NEW.NR_SEQ_FRASCO: '          || to_char(:new.NR_SEQ_FRASCO)
                || ' - :NEW.IE_SUSPENSO: '            || to_char(:new.IE_SUSPENSO)
                || ' - :OLD.NR_SEQUENCIA: '           || to_char(:old.NR_SEQUENCIA)
                || ' - :OLD.DT_ATUALIZACAO: '         || to_char(:old.DT_ATUALIZACAO, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :OLD.NM_USUARIO: '             || to_char(:old.NM_USUARIO)
                || ' - :OLD.DT_ATUALIZACAO_NREC: '    || to_char(:old.DT_ATUALIZACAO_NREC, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :OLD.NM_USUARIO_NREC: '        || to_char(:old.NM_USUARIO_NREC)
                || ' - :OLD.NR_PRESCRICAO: '          || to_char(:old.NR_PRESCRICAO)
                || ' - :OLD.NR_SEQ_PRESCR: '          || to_char(:old.NR_SEQ_PRESCR)
                || ' - :OLD.NR_SEQ_PRESCR_PROC_MAT: ' || to_char(:old.NR_SEQ_PRESCR_PROC_MAT)
                || ' - :OLD.IE_STATUS: '              || to_char(:old.IE_STATUS)
                || ' - :OLD.CD_BARRAS: '              || to_char(:old.CD_BARRAS)
                || ' - :OLD.DT_INTEGRACAO: '          || to_char(:old.DT_INTEGRACAO, 'dd/mm/yyyy hh24:mi:ss')
                || ' - :OLD.NR_SEQ_FRASCO: '          || to_char(:old.NR_SEQ_FRASCO)
                || ' - :OLD.IE_SUSPENSO: '            || to_char(:old.IE_SUSPENSO)
                || ' - STACK_TRACE: '                 || dbms_utility.format_call_stack, 1, 3999),
                NM_USUARIO_P => :new.nm_usuario,
                NR_PRESCRICAO_P => :new.nr_prescricao
            );
        END IF;
    END IF;
END;
/


ALTER TABLE TASY.PRESCR_PROC_MAT_ITEM ADD (
  CONSTRAINT PEPEMIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          12M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_MAT_ITEM ADD (
  CONSTRAINT PEPEMIT_PRPRMAT_FK 
 FOREIGN KEY (NR_SEQ_PRESCR_PROC_MAT) 
 REFERENCES TASY.PRESCR_PROC_MATERIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPEMIT_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPEMIT_FRAEXLA_FK 
 FOREIGN KEY (NR_SEQ_FRASCO) 
 REFERENCES TASY.FRASCO_EXAME_LAB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROC_MAT_ITEM TO NIVEL_1;

