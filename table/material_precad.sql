ALTER TABLE TASY.MATERIAL_PRECAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_PRECAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_PRECAD
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DS_MATERIAL                    VARCHAR2(255 BYTE) NOT NULL,
  DS_REDUZIDA                    VARCHAR2(255 BYTE) NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  CD_CLASSE_MATERIAL             NUMBER(5)      NOT NULL,
  CD_UNIDADE_MEDIDA_COMPRA       VARCHAR2(30 BYTE) NOT NULL,
  CD_UNIDADE_MEDIDA_CONSUMO      VARCHAR2(30 BYTE) NOT NULL,
  IE_MATERIAL_ESTOQUE            VARCHAR2(1 BYTE) NOT NULL,
  IE_RECEITA                     VARCHAR2(1 BYTE) NOT NULL,
  IE_COBRA_PACIENTE              VARCHAR2(1 BYTE) NOT NULL,
  IE_BAIXA_INTEIRA               VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  QT_DIAS_VALIDADE               NUMBER(5),
  DT_ATUALIZACAO_NREC            DATE,
  DT_CADASTRAMENTO               DATE           NOT NULL,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  DT_LIBERACAO                   DATE,
  NR_MINIMO_COTACAO              NUMBER(1),
  NM_USUARIO_LIB                 VARCHAR2(15 BYTE),
  QT_ESTOQUE_MAXIMO              NUMBER(13,4),
  QT_ESTOQUE_MINIMO              NUMBER(13,4),
  NR_SEQ_PROCESSO                NUMBER(10)     NOT NULL,
  QT_PONTO_PEDIDO                NUMBER(13,4),
  NR_CODIGO_BARRAS               VARCHAR2(14 BYTE),
  IE_VIA_APLICACAO               VARCHAR2(5 BYTE),
  IE_OBRIG_VIA_APLICACAO         VARCHAR2(1 BYTE),
  IE_DISPONIVEL_MERCADO          VARCHAR2(1 BYTE),
  QT_MINIMO_MULTIPLO_SOLIC       NUMBER(13,4)   NOT NULL,
  QT_CONV_COMPRA_ESTOQUE         NUMBER(13,4)   NOT NULL,
  CD_UNIDADE_MEDIDA_SOLIC        VARCHAR2(30 BYTE),
  IE_PRESCRICAO                  VARCHAR2(1 BYTE),
  CD_KIT_MATERIAL                NUMBER(5),
  QT_CONVERSAO_MG                NUMBER(15,6),
  IE_TIPO_MATERIAL               VARCHAR2(3 BYTE) NOT NULL,
  IE_PADRONIZADO                 VARCHAR2(1 BYTE),
  QT_CONV_ESTOQUE_CONSUMO        NUMBER(13,4)   NOT NULL,
  CD_MATERIAL_CONTA              NUMBER(6),
  IE_PRECO_COMPRA                VARCHAR2(1 BYTE),
  IE_MATERIAL_DIRETO             VARCHAR2(1 BYTE),
  IE_CONSIGNADO                  VARCHAR2(1 BYTE),
  IE_UTILIZACAO_SUS              VARCHAR2(1 BYTE),
  IE_MONOFASICO                  VARCHAR2(1 BYTE),
  QT_LIMITE_PESSOA               NUMBER(15,4),
  CD_UNID_MED_LIMITE             VARCHAR2(30 BYTE),
  DS_ORIENTACAO_USO              VARCHAR2(2000 BYTE),
  IE_TIPO_FONTE_PRESCR           VARCHAR2(3 BYTE),
  IE_DIAS_UTIL_MEDIC             VARCHAR2(1 BYTE),
  DS_ORIENTACAO_USUARIO          VARCHAR2(4000 BYTE),
  IE_INF_ULTIMA_COMPRA           VARCHAR2(1 BYTE) NOT NULL,
  CD_MEDICAMENTO                 NUMBER(10),
  IE_CONTROLE_MEDICO             NUMBER(1)      NOT NULL,
  IE_BAIXA_ESTOQ_PAC             VARCHAR2(1 BYTE) NOT NULL,
  QT_HORAS_UTIL_PAC              NUMBER(7,2),
  QT_DIA_INTERV_RESSUP           NUMBER(3),
  QT_DIA_RESSUP_FORN             NUMBER(3),
  QT_DIA_ESTOQUE_MINIMO          NUMBER(2),
  CD_UNIDADE_MEDIDA_ESTOQUE      VARCHAR2(30 BYTE) NOT NULL,
  NR_CERTIFICADO_APROVACAO       VARCHAR2(20 BYTE),
  QT_MAX_PRESCRICAO              NUMBER(15,4),
  QT_CONSUMO_MENSAL              NUMBER(15,4),
  IE_CURVA_ABC                   VARCHAR2(1 BYTE),
  IE_CLASSIF_XYZ                 VARCHAR2(1 BYTE),
  QT_PRIORIDADE_COML             NUMBER(5),
  CD_FABRICANTE                  VARCHAR2(80 BYTE),
  NR_SEQ_GRUPO_REC               NUMBER(10),
  CD_SISTEMA_ANT                 VARCHAR2(20 BYTE),
  NR_SEQ_FABRIC                  NUMBER(10),
  NR_SEQ_DIVISAO                 NUMBER(10),
  IE_CUSTO_BENEF                 VARCHAR2(1 BYTE),
  IE_BOMBA_INFUSAO               VARCHAR2(1 BYTE) NOT NULL,
  IE_DILUICAO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_SOLUCAO                     VARCHAR2(1 BYTE) NOT NULL,
  IE_MISTURA                     VARCHAR2(1 BYTE) NOT NULL,
  CD_CLASSIF_FISCAL              VARCHAR2(80 BYTE),
  IE_ABRIGO_LUZ                  VARCHAR2(1 BYTE) NOT NULL,
  IE_UMIDADE_CONTROLADA          VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_FICHA_TECNICA           NUMBER(10),
  DS_PRECAUCAO_ORDEM             VARCHAR2(255 BYTE),
  CD_UNID_MED_CONCETRACAO        VARCHAR2(30 BYTE),
  CD_UNID_MED_BASE_CONC          VARCHAR2(30 BYTE),
  DS_MATERIAL_SEM_ACENTO         VARCHAR2(255 BYTE),
  QT_DIA_PROFILATICO             NUMBER(3),
  QT_DIA_TERAPEUTICO             NUMBER(3),
  NR_SEQ_FAMILIA                 NUMBER(10),
  NR_SEQ_LOCALIZACAO             NUMBER(10),
  QT_MIN_APLICACAO               NUMBER(15),
  IE_FABRIC_PROPRIA              VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_FARM              NUMBER(10),
  IE_GRAVAR_OBS_PRESCR           VARCHAR2(15 BYTE) NOT NULL,
  QT_DOSE_PRESCRICAO             NUMBER(15,3),
  CD_UNIDADE_MEDIDA_PRESCR       VARCHAR2(30 BYTE),
  CD_UNID_TERAPEUTICA            VARCHAR2(15 BYTE),
  CD_INTERVALO_PADRAO            VARCHAR2(7 BYTE),
  IE_OBRIGA_JUSTIFICATIVA        VARCHAR2(1 BYTE),
  IE_ANCORA_SOLUCAO              VARCHAR2(1 BYTE),
  IE_PROTOCOLO_TEV               VARCHAR2(1 BYTE),
  IE_TIPO_SOLUCAO_CPOE           VARCHAR2(3 BYTE),
  IE_PADRAO_CPOE                 VARCHAR2(3 BYTE),
  IE_QUIMICO_PERIGOSO            VARCHAR2(1 BYTE),
  IE_TERMOLABIL                  VARCHAR2(15 BYTE),
  QT_MAX_DIA_APLIC               NUMBER(3),
  IE_OBRIGA_JUST_DOSE            VARCHAR2(2 BYTE),
  DT_VALIDADE_CERTIFICADO_APROV  DATE,
  IE_CLASSIF_MEDIC               VARCHAR2(1 BYTE),
  IE_DOSE_LIMITE                 VARCHAR2(15 BYTE),
  IE_FOTOSENSIVEL                VARCHAR2(1 BYTE),
  IE_HIGROSCOPICO                VARCHAR2(1 BYTE),
  IE_SEXO                        VARCHAR2(2 BYTE),
  IE_FABRICANTE_DIST             VARCHAR2(1 BYTE),
  IE_MENSAGEM_SONDA              VARCHAR2(1 BYTE),
  IE_EXIGE_LADO                  VARCHAR2(1 BYTE),
  IE_MOSTRAR_ORIENTACAO          VARCHAR2(2 BYTE),
  IE_GERAR_LOTE                  VARCHAR2(1 BYTE),
  IE_GERA_LOTE_SEPARADO          VARCHAR2(1 BYTE),
  IE_JUSTIFICATIVA_PADRAO        VARCHAR2(1 BYTE),
  IE_JUSTIFICA_DIAS_UTIL         VARCHAR2(1 BYTE),
  QT_CONCENTRACAO_TOTAL          NUMBER(15,4),
  CD_UNID_MED_CONC_TOTAL         VARCHAR2(30 BYTE),
  NR_SEQ_MODELO_DIALISADOR       NUMBER(10),
  DS_DOSE_EMBALAGEM              VARCHAR2(15 BYTE),
  CD_UNID_MED_DOSE_EMB           VARCHAR2(30 BYTE),
  IE_TAMANHO_EMBALAGEM           VARCHAR2(15 BYTE),
  IE_ALTO_RISCO                  VARCHAR2(1 BYTE),
  IE_EDITAR_DOSE                 VARCHAR2(1 BYTE),
  DS_HINT                        VARCHAR2(255 BYTE),
  IE_EXTRAVAZAMENTO              VARCHAR2(15 BYTE),
  IE_CONSISTE_DUPL               VARCHAR2(1 BYTE),
  IE_APLICAR                     VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_RISCO           NUMBER(10),
  DS_MENSAGEM                    VARCHAR2(2000 BYTE),
  IE_IAT                         VARCHAR2(1 BYTE),
  QT_CONV_COMPRA_EST             NUMBER(15,4),
  CD_UNID_MEDIDA                 VARCHAR2(30 BYTE),
  QT_COMPRA_MELHOR               NUMBER(15,4),
  NR_REGISTRO_MS                 VARCHAR2(20 BYTE),
  CD_TIPO_RECOMENDACAO           NUMBER(10),
  NR_REGISTRO_ANVISA             VARCHAR2(60 BYTE),
  DT_VALIDADE_REG_ANVISA         DATE,
  IE_CONSISTE_DIAS               VARCHAR2(1 BYTE),
  IE_STATUS_ENVIO                VARCHAR2(15 BYTE),
  DT_INTEGRACAO                  DATE,
  DT_REVISAO_FISPQ               DATE,
  NR_DIAS_JUSTIF                 NUMBER(3),
  QT_OVERFILL                    NUMBER(5,2),
  IE_OBJETIVO                    VARCHAR2(1 BYTE),
  IE_CLEARANCE                   VARCHAR2(1 BYTE),
  CD_DCB                         VARCHAR2(20 BYTE),
  IE_DUPLA_CHECAGEM              VARCHAR2(1 BYTE),
  NR_SEQ_DOSE_TERAP              NUMBER(15),
  QT_DOSE_TERAPEUTICA            NUMBER(15,4),
  IE_MANIPULADO                  VARCHAR2(1 BYTE),
  IE_ESTERILIZAVEL               VARCHAR2(1 BYTE),
  DS_ABREV_SOLUCAO               VARCHAR2(255 BYTE),
  IE_CONSISTE_ESTOQUE            VARCHAR2(1 BYTE),
  QT_PRIORIDADE_APRES            NUMBER(5),
  IE_LATEX                       VARCHAR2(1 BYTE),
  QT_MEIA_VIDA                   NUMBER(6,3),
  CD_MATERIAL_SUBS               NUMBER(6),
  IE_PERMITE_FRACIONADO          VARCHAR2(1 BYTE),
  IE_ALTA_VIGILANCIA             VARCHAR2(1 BYTE),
  IE_NOVA_MOLECULA               VARCHAR2(1 BYTE),
  IE_TIPO_SOLUCAO                VARCHAR2(2 BYTE),
  QT_MINIMA_PLANEJ               NUMBER(15,4),
  QT_MAXIMA_PLANEJ               NUMBER(15,4),
  IE_AVALIACAO_REP               VARCHAR2(1 BYTE),
  IE_MEDIC_PACIENTE              VARCHAR2(1 BYTE),
  IE_VOLUMOSO                    VARCHAR2(2 BYTE),
  IE_RESTRITO                    VARCHAR2(1 BYTE),
  QT_COMPRIMENTO_CM              NUMBER(15,2),
  QT_LARGURA_CM                  NUMBER(15,2),
  QT_PESO_KG                     NUMBER(15,2),
  QT_ALTURA_CM                   NUMBER(15,2),
  IE_VANCOMICINA                 VARCHAR2(1 BYTE),
  IE_EMBALAGEM_MULTIPLA          VARCHAR2(1 BYTE),
  IE_MULTIDOSE                   VARCHAR2(1 BYTE),
  IE_PACTUADO                    VARCHAR2(1 BYTE),
  IE_OBRIGA_TEMPO_APLIC          VARCHAR2(1 BYTE),
  IE_PERECIVEL                   VARCHAR2(1 BYTE),
  IE_CATALOGO                    VARCHAR2(1 BYTE),
  QT_DIA_INTERV_PROFILAXIA       NUMBER(5),
  CD_CNPJ_CADASTRO               VARCHAR2(14 BYTE),
  IE_PERMITE_LACTANTE            VARCHAR2(1 BYTE),
  IE_MEDIC_SORO_ORAL             VARCHAR2(1 BYTE),
  IE_UNID_MED_PADRAO             VARCHAR2(1 BYTE),
  IE_NARCOTICO                   VARCHAR2(1 BYTE),
  DT_APROVACAO                   DATE,
  NM_USUARIO_APROV               VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATECAD_ANVFOFO_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_FORMA_FARM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_CIHMEDI_FK_I ON TASY.MATERIAL_PRECAD
(CD_MEDICAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_CLAMATE_FK_I ON TASY.MATERIAL_PRECAD
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_CLRISCM_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_CLASSIF_RISCO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_ESTABEL_FK_I ON TASY.MATERIAL_PRECAD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_GRURECE_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_HDTIPDI_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_MODELO_DIALISADOR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_INTPRES_FK_I ON TASY.MATERIAL_PRECAD
(CD_INTERVALO_PADRAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_KITMATE_FK_I ON TASY.MATERIAL_PRECAD
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECADL_MATCONT_FK_I ON TASY.MATERIAL_PRECAD
(CD_MATERIAL_CONTA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECADL_UNIMEDI_LIMITE_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNID_MED_LIMITE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MATDIVI_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_DIVISAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MATECAD_FK2_I ON TASY.MATERIAL_PRECAD
(CD_MATERIAL_SUBS)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MATFABR_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_FABRIC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MATFAMI_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MATLOCA_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_MEDFITE_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_PESJURI_FK_I ON TASY.MATERIAL_PRECAD
(CD_CNPJ_CADASTRO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATECAD_PK ON TASY.MATERIAL_PRECAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_PROPREC_FK_I ON TASY.MATERIAL_PRECAD
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_TIPRECO_FK_I ON TASY.MATERIAL_PRECAD
(CD_TIPO_RECOMENDACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_COMPRADO_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNIDADE_MEDIDA_ESTOQUE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_CONC_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNID_MED_CONCETRACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_CONSUMIDO_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNIDADE_MEDIDA_CONSUMO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_FK2_I ON TASY.MATERIAL_PRECAD
(CD_UNID_MEDIDA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_FK3_I ON TASY.MATERIAL_PRECAD
(CD_UNID_MED_DOSE_EMB)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_F6_I ON TASY.MATERIAL_PRECAD
(CD_UNID_MED_BASE_CONC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_PRESCR_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNIDADE_MEDIDA_PRESCR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_UNIMEDI_SOLIC_FK_I ON TASY.MATERIAL_PRECAD
(CD_UNIDADE_MEDIDA_SOLIC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATECAD_VIAAPLI_FK_I ON TASY.MATERIAL_PRECAD
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_PRECAD_tp  after update ON TASY.MATERIAL_PRECAD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_UNID_MED_LIMITE,1,4000),substr(:new.CD_UNID_MED_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_LIMITE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MEDIDA,1,4000),substr(:new.CD_UNID_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MEDIDA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_CONC_TOTAL,1,4000),substr(:new.CD_UNID_MED_CONC_TOTAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_CONC_TOTAL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_CONCETRACAO,1,4000),substr(:new.CD_UNID_MED_CONCETRACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_CONCETRACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_BASE_CONC,1,4000),substr(:new.CD_UNID_MED_BASE_CONC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_BASE_CONC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_SOLIC,1,4000),substr(:new.CD_UNIDADE_MEDIDA_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_SOLIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_PRESCR,1,4000),substr(:new.CD_UNIDADE_MEDIDA_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_PRESCR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_ESTOQUE,1,4000),substr(:new.CD_UNIDADE_MEDIDA_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_ESTOQUE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_CONSUMO,1,4000),substr(:new.CD_UNIDADE_MEDIDA_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_CONSUMO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_COMPRA,1,4000),substr(:new.CD_UNIDADE_MEDIDA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_COMPRA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_RECOMENDACAO,1,4000),substr(:new.CD_TIPO_RECOMENDACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_RECOMENDACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICAMENTO,1,4000),substr(:new.CD_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICAMENTO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_SUBS,1,4000),substr(:new.CD_MATERIAL_SUBS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_SUBS',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_VALIDADE,1,4000),substr(:new.QT_DIAS_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_VALIDADE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_PROFILATICO,1,4000),substr(:new.QT_DIA_PROFILATICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_PROFILATICO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_INTERV_PROFILAXIA,1,4000),substr(:new.QT_DIA_INTERV_PROFILAXIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_INTERV_PROFILAXIA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_ESTOQUE_CONSUMO,1,4000),substr(:new.QT_CONV_ESTOQUE_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_ESTOQUE_CONSUMO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONVERSAO_MG,1,4000),substr(:new.QT_CONVERSAO_MG,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_MG',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_ESTOQUE,1,4000),substr(:new.QT_CONV_COMPRA_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_ESTOQUE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_EST,1,4000),substr(:new.QT_CONV_COMPRA_EST,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_EST',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COMPRIMENTO_CM,1,4000),substr(:new.QT_COMPRIMENTO_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_COMPRIMENTO_CM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COMPRA_MELHOR,1,4000),substr(:new.QT_COMPRA_MELHOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_COMPRA_MELHOR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ALTURA_CM,1,4000),substr(:new.QT_ALTURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA_CM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MODELO_DIALISADOR,1,4000),substr(:new.NR_SEQ_MODELO_DIALISADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MODELO_DIALISADOR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCALIZACAO,1,4000),substr(:new.NR_SEQ_LOCALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCALIZACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FORMA_FARM,1,4000),substr(:new.NR_SEQ_FORMA_FARM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FORMA_FARM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FICHA_TECNICA,1,4000),substr(:new.NR_SEQ_FICHA_TECNICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FICHA_TECNICA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_CONTA,1,4000),substr(:new.CD_MATERIAL_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_CONTA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERVALO_PADRAO,1,4000),substr(:new.CD_INTERVALO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO_PADRAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FABRICANTE,1,4000),substr(:new.CD_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_FABRICANTE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DCB,1,4000),substr(:new.CD_DCB,1,4000),:new.nm_usuario,nr_seq_w,'CD_DCB',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ_CADASTRO,1,4000),substr(:new.CD_CNPJ_CADASTRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ_CADASTRO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIF_FISCAL,1,4000),substr(:new.CD_CLASSIF_FISCAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIF_FISCAL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FAMILIA,1,4000),substr(:new.NR_SEQ_FAMILIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FAMILIA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FABRIC,1,4000),substr(:new.NR_SEQ_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FABRIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DOSE_TERAP,1,4000),substr(:new.NR_SEQ_DOSE_TERAP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DOSE_TERAP',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DIVISAO,1,4000),substr(:new.NR_SEQ_DIVISAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DIVISAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_RISCO,1,4000),substr(:new.NR_SEQ_CLASSIF_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_RISCO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_MS,1,4000),substr(:new.NR_REGISTRO_MS,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_MS',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_ANVISA,1,4000),substr(:new.NR_REGISTRO_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_ANVISA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIAS_JUSTIF,1,4000),substr(:new.NR_DIAS_JUSTIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIAS_JUSTIF',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_APROVACAO,1,4000),substr(:new.NR_CERTIFICADO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_APROVACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VOLUMOSO,1,4000),substr(:new.IE_VOLUMOSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VOLUMOSO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_APLICACAO,1,4000),substr(:new.IE_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_APLICACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VANCOMICINA,1,4000),substr(:new.IE_VANCOMICINA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VANCOMICINA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SOLUCAO,1,4000),substr(:new.IE_TIPO_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SOLUCAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MATERIAL,1,4000),substr(:new.IE_TIPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MATERIAL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_FONTE_PRESCR,1,4000),substr(:new.IE_TIPO_FONTE_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FONTE_PRESCR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TERMOLABIL,1,4000),substr(:new.IE_TERMOLABIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TERMOLABIL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_ENVIO,1,4000),substr(:new.IE_STATUS_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_ENVIO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLUCAO,1,4000),substr(:new.IE_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLUCAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRITO,1,4000),substr(:new.IE_RESTRITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRITO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEITA,1,4000),substr(:new.IE_RECEITA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEITA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROTOCOLO_TEV,1,4000),substr(:new.IE_PROTOCOLO_TEV,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROTOCOLO_TEV',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESCRICAO,1,4000),substr(:new.IE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESCRICAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_COMPRA,1,4000),substr(:new.IE_PRECO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_COMPRA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_LACTANTE,1,4000),substr(:new.IE_PERMITE_LACTANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_LACTANTE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_FRACIONADO,1,4000),substr(:new.IE_PERMITE_FRACIONADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_FRACIONADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERECIVEL,1,4000),substr(:new.IE_PERECIVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERECIVEL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRAO_CPOE,1,4000),substr(:new.IE_PADRAO_CPOE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO_CPOE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PACTUADO,1,4000),substr(:new.IE_PACTUADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PACTUADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIG_VIA_APLICACAO,1,4000),substr(:new.IE_OBRIG_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIG_VIA_APLICACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_TEMPO_APLIC,1,4000),substr(:new.IE_OBRIGA_TEMPO_APLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_TEMPO_APLIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_JUSTIFICATIVA,1,4000),substr(:new.IE_OBRIGA_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_JUSTIFICATIVA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_JUST_DOSE,1,4000),substr(:new.IE_OBRIGA_JUST_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_JUST_DOSE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBJETIVO,1,4000),substr(:new.IE_OBJETIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBJETIVO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOVA_MOLECULA,1,4000),substr(:new.IE_NOVA_MOLECULA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOVA_MOLECULA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MULTIDOSE,1,4000),substr(:new.IE_MULTIDOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MULTIDOSE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOSTRAR_ORIENTACAO,1,4000),substr(:new.IE_MOSTRAR_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOSTRAR_ORIENTACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MONOFASICO,1,4000),substr(:new.IE_MONOFASICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MONOFASICO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MISTURA,1,4000),substr(:new.IE_MISTURA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MISTURA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MENSAGEM_SONDA,1,4000),substr(:new.IE_MENSAGEM_SONDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MENSAGEM_SONDA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDIC_PACIENTE,1,4000),substr(:new.IE_MEDIC_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDIC_PACIENTE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MATERIAL_ESTOQUE,1,4000),substr(:new.IE_MATERIAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_ESTOQUE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MANIPULADO,1,4000),substr(:new.IE_MANIPULADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MANIPULADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LATEX,1,4000),substr(:new.IE_LATEX,1,4000),:new.nm_usuario,nr_seq_w,'IE_LATEX',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICATIVA_PADRAO,1,4000),substr(:new.IE_JUSTIFICATIVA_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICATIVA_PADRAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICA_DIAS_UTIL,1,4000),substr(:new.IE_JUSTIFICA_DIAS_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICA_DIAS_UTIL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INF_ULTIMA_COMPRA,1,4000),substr(:new.IE_INF_ULTIMA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INF_ULTIMA_COMPRA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IAT,1,4000),substr(:new.IE_IAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_IAT',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_HIGROSCOPICO,1,4000),substr(:new.IE_HIGROSCOPICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_HIGROSCOPICO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAVAR_OBS_PRESCR,1,4000),substr(:new.IE_GRAVAR_OBS_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAVAR_OBS_PRESCR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_LOTE,1,4000),substr(:new.IE_GERAR_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_LOTE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_LOTE_SEPARADO,1,4000),substr(:new.IE_GERA_LOTE_SEPARADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_LOTE_SEPARADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FOTOSENSIVEL,1,4000),substr(:new.IE_FOTOSENSIVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FOTOSENSIVEL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FABRIC_PROPRIA,1,4000),substr(:new.IE_FABRIC_PROPRIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FABRIC_PROPRIA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FABRICANTE_DIST,1,4000),substr(:new.IE_FABRICANTE_DIST,1,4000),:new.nm_usuario,nr_seq_w,'IE_FABRICANTE_DIST',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXTRAVAZAMENTO,1,4000),substr(:new.IE_EXTRAVAZAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXTRAVAZAMENTO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_LADO,1,4000),substr(:new.IE_EXIGE_LADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_LADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTERILIZAVEL,1,4000),substr(:new.IE_ESTERILIZAVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTERILIZAVEL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EMBALAGEM_MULTIPLA,1,4000),substr(:new.IE_EMBALAGEM_MULTIPLA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMBALAGEM_MULTIPLA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EDITAR_DOSE,1,4000),substr(:new.IE_EDITAR_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_EDITAR_DOSE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DUPLA_CHECAGEM,1,4000),substr(:new.IE_DUPLA_CHECAGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_DUPLA_CHECAGEM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOSE_LIMITE,1,4000),substr(:new.IE_DOSE_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOSE_LIMITE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DISPONIVEL_MERCADO,1,4000),substr(:new.IE_DISPONIVEL_MERCADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DISPONIVEL_MERCADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DILUICAO,1,4000),substr(:new.IE_DILUICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DILUICAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIAS_UTIL_MEDIC,1,4000),substr(:new.IE_DIAS_UTIL_MEDIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIAS_UTIL_MEDIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_BENEF,1,4000),substr(:new.IE_CUSTO_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_BENEF',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CURVA_ABC,1,4000),substr(:new.IE_CURVA_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CURVA_ABC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLE_MEDICO,1,4000),substr(:new.IE_CONTROLE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE_MEDICO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_ESTOQUE,1,4000),substr(:new.IE_CONSISTE_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_ESTOQUE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_DUPL,1,4000),substr(:new.IE_CONSISTE_DUPL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_DUPL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_DIAS,1,4000),substr(:new.IE_CONSISTE_DIAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_DIAS',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIGNADO,1,4000),substr(:new.IE_CONSIGNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIGNADO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBRA_PACIENTE,1,4000),substr(:new.IE_COBRA_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBRA_PACIENTE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLEARANCE,1,4000),substr(:new.IE_CLEARANCE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLEARANCE',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_MEDIC,1,4000),substr(:new.IE_CLASSIF_MEDIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_MEDIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CATALOGO,1,4000),substr(:new.IE_CATALOGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CATALOGO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BOMBA_INFUSAO,1,4000),substr(:new.IE_BOMBA_INFUSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BOMBA_INFUSAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_INTEIRA,1,4000),substr(:new.IE_BAIXA_INTEIRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_INTEIRA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_ESTOQ_PAC,1,4000),substr(:new.IE_BAIXA_ESTOQ_PAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_ESTOQ_PAC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AVALIACAO_REP,1,4000),substr(:new.IE_AVALIACAO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_AVALIACAO_REP',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICAR,1,4000),substr(:new.IE_APLICAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICAR',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANCORA_SOLUCAO,1,4000),substr(:new.IE_ANCORA_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ANCORA_SOLUCAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTO_RISCO,1,4000),substr(:new.IE_ALTO_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTO_RISCO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTA_VIGILANCIA,1,4000),substr(:new.IE_ALTA_VIGILANCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTA_VIGILANCIA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_REG_ANVISA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERTIFICADO_APROV',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REVISAO_FISPQ,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REVISAO_FISPQ,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REVISAO_FISPQ',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CADASTRAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CADASTRAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CADASTRAMENTO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIORIDADE_COML,1,4000),substr(:new.QT_PRIORIDADE_COML,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE_COML',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REDUZIDA,1,4000),substr(:new.DS_REDUZIDA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REDUZIDA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PRECAUCAO_ORDEM,1,4000),substr(:new.DS_PRECAUCAO_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_PRECAUCAO_ORDEM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_USUARIO,1,4000),substr(:new.DS_ORIENTACAO_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_USUARIO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIORIDADE_APRES,1,4000),substr(:new.QT_PRIORIDADE_APRES,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE_APRES',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO_KG,1,4000),substr(:new.QT_PESO_KG,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO_KG',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_OVERFILL,1,4000),substr(:new.QT_OVERFILL,1,4000),:new.nm_usuario,nr_seq_w,'QT_OVERFILL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_MULTIPLO_SOLIC,1,4000),substr(:new.QT_MINIMO_MULTIPLO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_MULTIPLO_SOLIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MENSAGEM,1,4000),substr(:new.DS_MENSAGEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_MENSAGEM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATERIAL,1,4000),substr(:new.DS_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMA_PLANEJ,1,4000),substr(:new.QT_MINIMA_PLANEJ,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMA_PLANEJ',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_APLICACAO,1,4000),substr(:new.QT_MIN_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_APLICACAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MEIA_VIDA,1,4000),substr(:new.QT_MEIA_VIDA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MEIA_VIDA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAX_PRESCRICAO,1,4000),substr(:new.QT_MAX_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_PRESCRICAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAXIMA_PLANEJ,1,4000),substr(:new.QT_MAXIMA_PLANEJ,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAXIMA_PLANEJ',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAX_DIA_APLIC,1,4000),substr(:new.QT_MAX_DIA_APLIC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_DIA_APLIC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LIMITE_PESSOA,1,4000),substr(:new.QT_LIMITE_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LIMITE_PESSOA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LARGURA_CM,1,4000),substr(:new.QT_LARGURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_LARGURA_CM',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_HORAS_UTIL_PAC,1,4000),substr(:new.QT_HORAS_UTIL_PAC,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_UTIL_PAC',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_TERAPEUTICA,1,4000),substr(:new.QT_DOSE_TERAPEUTICA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_TERAPEUTICA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_PRESCRICAO,1,4000),substr(:new.QT_DOSE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_PRESCRICAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_TERAPEUTICO,1,4000),substr(:new.QT_DIA_TERAPEUTICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_TERAPEUTICO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HINT,1,4000),substr(:new.DS_HINT,1,4000),:new.nm_usuario,nr_seq_w,'DS_HINT',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ABREV_SOLUCAO,1,4000),substr(:new.DS_ABREV_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ABREV_SOLUCAO',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_TERAPEUTICA,1,4000),substr(:new.CD_UNID_TERAPEUTICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_TERAPEUTICA',ie_log_w,ds_w,'MATERIAL_PRECAD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MATERIAL_PRECAD ADD (
  CONSTRAINT MATECAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MATERIAL_PRECAD ADD (
  CONSTRAINT MATECAD_HDTIPDI_FK 
 FOREIGN KEY (NR_SEQ_MODELO_DIALISADOR) 
 REFERENCES TASY.HD_MODELO_DIALISADOR (NR_SEQUENCIA),
  CONSTRAINT MATECAD_MATLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MATERIAL_LOCAL (NR_SEQUENCIA),
  CONSTRAINT MATECAD_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT MATECAD_ANVFOFO_FK 
 FOREIGN KEY (NR_SEQ_FORMA_FARM) 
 REFERENCES TASY.ANVISA_FORMA_FORMACEUTICA (NR_SEQUENCIA),
  CONSTRAINT MATECAD_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT MATECAD_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT MATECAD_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRIC) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MATECAD_MATDIVI_FK 
 FOREIGN KEY (NR_SEQ_DIVISAO) 
 REFERENCES TASY.MAT_DIVISAO (NR_SEQUENCIA),
  CONSTRAINT MATECAD_CLRISCM_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_RISCO) 
 REFERENCES TASY.CLASSIF_RISCO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT MATECAD_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT MATECADL_UNIMEDI_LIMITE_FK 
 FOREIGN KEY (CD_UNID_MED_LIMITE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNID_MED_DOSE_EMB) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_CONC_FK 
 FOREIGN KEY (CD_UNID_MED_CONCETRACAO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_F6 
 FOREIGN KEY (CD_UNID_MED_BASE_CONC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_SOLIC_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_SOLIC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_PRESCR_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_COMPRADO_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_ESTOQUE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_CONSUMIDO_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_CONSUMO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATECAD_TIPRECO_FK 
 FOREIGN KEY (CD_TIPO_RECOMENDACAO) 
 REFERENCES TASY.TIPO_RECOMENDACAO (CD_TIPO_RECOMENDACAO),
  CONSTRAINT MATECAD_CIHMEDI_FK 
 FOREIGN KEY (CD_MEDICAMENTO) 
 REFERENCES TASY.CIH_MEDICAMENTO (CD_MEDICAMENTO),
  CONSTRAINT MATECAD_MATECAD_FK2 
 FOREIGN KEY (CD_MATERIAL_SUBS) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATECADL_MATCONT_FK 
 FOREIGN KEY (CD_MATERIAL_CONTA) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATECAD_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT MATECAD_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO_PADRAO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT MATECAD_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_CADASTRO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATECAD_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT MATECAD_PROPREC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_PRE_CADASTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATECAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MATERIAL_PRECAD TO NIVEL_1;

