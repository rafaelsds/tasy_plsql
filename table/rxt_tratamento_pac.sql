ALTER TABLE TASY.RXT_TRATAMENTO_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_TRATAMENTO_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_TRATAMENTO_PAC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TUMOR         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RXTTRATPAC_PK ON TASY.RXT_TRATAMENTO_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTTRATPAC_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTTRATPAC_RXTTIPO_FK_I ON TASY.RXT_TRATAMENTO_PAC
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTTRATPAC_RXTTIPO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTTRATPAC_RXTTUMO_FK_I ON TASY.RXT_TRATAMENTO_PAC
(NR_SEQ_TUMOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTTRATPAC_RXTTUMO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RXT_TRATAMENTO_PAC ADD (
  CONSTRAINT RXTTRATPAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_TRATAMENTO_PAC ADD (
  CONSTRAINT RXTTRATPAC_RXTTIPO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.RXT_TIPO (NR_SEQUENCIA),
  CONSTRAINT RXTTRATPAC_RXTTUMO_FK 
 FOREIGN KEY (NR_SEQ_TUMOR) 
 REFERENCES TASY.RXT_TUMOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RXT_TRATAMENTO_PAC TO NIVEL_1;

