ALTER TABLE TASY.PLS_REGRA_FATURAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_FATURAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_FATURAMENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NM_REGRA                   VARCHAR2(255 BYTE) NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_FORMA_IMPEDIMENTO_COBR  VARCHAR2(5 BYTE),
  IE_REGRA_RESTRITIVA        VARCHAR2(1 BYTE),
  IE_CONTA_FECHADA           VARCHAR2(1 BYTE),
  IE_SOMENTE_RESUMO          VARCHAR2(1 BYTE),
  IE_DIV_FAT_TAXA            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRFAT_ESTABEL_FK_I ON TASY.PLS_REGRA_FATURAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRFAT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRFAT_PK ON TASY.PLS_REGRA_FATURAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_FATURAMENTO_tp  after update ON TASY.PLS_REGRA_FATURAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONTA_FECHADA,1,4000),substr(:new.IE_CONTA_FECHADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_FECHADA',ie_log_w,ds_w,'PLS_REGRA_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_RESUMO,1,4000),substr(:new.IE_SOMENTE_RESUMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_RESUMO',ie_log_w,ds_w,'PLS_REGRA_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_IMPEDIMENTO_COBR,1,4000),substr(:new.IE_FORMA_IMPEDIMENTO_COBR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_IMPEDIMENTO_COBR',ie_log_w,ds_w,'PLS_REGRA_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_REGRA_FATURAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_FATURAMENTO ADD (
  CONSTRAINT PLSRFAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_FATURAMENTO ADD (
  CONSTRAINT PLSRFAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_FATURAMENTO TO NIVEL_1;

