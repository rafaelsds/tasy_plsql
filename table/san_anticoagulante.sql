ALTER TABLE TASY.SAN_ANTICOAGULANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_ANTICOAGULANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_ANTICOAGULANTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DS_ANTICOAGULANTE         VARCHAR2(60 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_DIAS_PRORROG_VALIDADE  NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANANTI_PK ON TASY.SAN_ANTICOAGULANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANANTI_PK
  MONITORING USAGE;


ALTER TABLE TASY.SAN_ANTICOAGULANTE ADD (
  CONSTRAINT SANANTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SAN_ANTICOAGULANTE TO NIVEL_1;

