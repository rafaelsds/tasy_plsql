ALTER TABLE TASY.PRESCR_PROTESE_MS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROTESE_MS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROTESE_MS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_PRESCRICAO              NUMBER(14)         NOT NULL,
  DT_SUSPENSAO               DATE,
  IE_SUSPENSO                VARCHAR2(1 BYTE),
  IE_NIVEL_AMPUTACAO         VARCHAR2(15 BYTE)  NOT NULL,
  IE_AMPUTACAO               VARCHAR2(15 BYTE),
  IE_POLEGAR                 VARCHAR2(1 BYTE),
  IE_DEDO_2                  VARCHAR2(1 BYTE),
  IE_DEDO_3                  VARCHAR2(1 BYTE),
  IE_DEDO_4                  VARCHAR2(1 BYTE),
  IE_DEDO_5                  VARCHAR2(1 BYTE),
  IE_TIPO_PROTESE            VARCHAR2(15 BYTE),
  IE_MATERIAL                VARCHAR2(15 BYTE),
  IE_ENDOESQUELETICA         VARCHAR2(15 BYTE),
  IE_SUSPENSAO               VARCHAR2(15 BYTE),
  IE_PROPULSAO               VARCHAR2(15 BYTE),
  IE_ARTICULACAO_COTOVELO    VARCHAR2(15 BYTE),
  IE_ARTICULACAO_PUNHO       VARCHAR2(15 BYTE),
  IE_APARELHO_PREENSOR       VARCHAR2(15 BYTE),
  IE_REVESTIMENTO_COSMETICO  VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  CD_CID_PRINCIPAL           VARCHAR2(4 BYTE),
  NR_SEQ_PROC_INTERNO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREPROM_CIDDOEN_FK1_I ON TASY.PRESCR_PROTESE_MS
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROM_CIDDOEN_FK1_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREPROM_PK ON TASY.PRESCR_PROTESE_MS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PREPROM_PRESMED_FK_I ON TASY.PRESCR_PROTESE_MS
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPROM_PROCEDI_FK_I ON TASY.PRESCR_PROTESE_MS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROM_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPROM_PROINTE_FK_I ON TASY.PRESCR_PROTESE_MS
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPROM_PROINTE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRESCR_PROTESE_MS ADD (
  CONSTRAINT PREPROM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROTESE_MS ADD (
  CONSTRAINT PREPROM_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PREPROM_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PREPROM_CIDDOEN_FK1 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PREPROM_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROTESE_MS TO NIVEL_1;

