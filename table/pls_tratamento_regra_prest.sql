ALTER TABLE TASY.PLS_TRATAMENTO_REGRA_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TRATAMENTO_REGRA_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TRATAMENTO_REGRA_PREST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_TRAT    NUMBER(10)               NOT NULL,
  NR_SEQ_PRESTADOR     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTRRP_PK ON TASY.PLS_TRATAMENTO_REGRA_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRRP_PLSPRES_FK_I ON TASY.PLS_TRATAMENTO_REGRA_PREST
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRP_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTRRP_PLSTRRE_FK_I ON TASY.PLS_TRATAMENTO_REGRA_PREST
(NR_SEQ_REGRA_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTRRP_PLSTRRE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_TRATAMENTO_REGRA_PREST_tp  after update ON TASY.PLS_TRATAMENTO_REGRA_PREST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_TRATAMENTO_REGRA_PREST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TRATAMENTO_REGRA_PREST ADD (
  CONSTRAINT PLSTRRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TRATAMENTO_REGRA_PREST ADD (
  CONSTRAINT PLSTRRP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTRRP_PLSTRRE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TRAT) 
 REFERENCES TASY.PLS_TRATAMENTO_REGRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TRATAMENTO_REGRA_PREST TO NIVEL_1;

