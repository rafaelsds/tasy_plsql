ALTER TABLE TASY.TIPO_ACOMOD_ALTA_DIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_ACOMOD_ALTA_DIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_ACOMOD_ALTA_DIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_TIPO_ACOMODACAO     NUMBER(4)              NOT NULL,
  CD_CONVENIO            NUMBER(6),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_HORA_INICIAL        NUMBER(10)             NOT NULL,
  QT_HORA_FINAL          NUMBER(10)             NOT NULL,
  CD_PROCEDIMENTO        NUMBER(15),
  IE_ORIGEM_PROCED       NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_PROC_INTERNO    NUMBER(10),
  IE_EXCLUIR_DIARIA_JOB  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TPACALD_CONVENI_FK_I ON TASY.TIPO_ACOMOD_ALTA_DIA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TPACALD_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TPACALD_PK ON TASY.TIPO_ACOMOD_ALTA_DIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TPACALD_PK
  MONITORING USAGE;


CREATE INDEX TASY.TPACALD_PROCEDI_FK_I ON TASY.TIPO_ACOMOD_ALTA_DIA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TPACALD_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TPACALD_PROINTE_FK_I ON TASY.TIPO_ACOMOD_ALTA_DIA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TPACALD_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TPACALD_TIPACOM_FK_I ON TASY.TIPO_ACOMOD_ALTA_DIA
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TPACALD_TIPACOM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TIPO_ACOMOD_ALTA_DIA ADD (
  CONSTRAINT TPACALD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_ACOMOD_ALTA_DIA ADD (
  CONSTRAINT TPACALD_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT TPACALD_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT TPACALD_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT TPACALD_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.TIPO_ACOMOD_ALTA_DIA TO NIVEL_1;

