ALTER TABLE TASY.ESCALA_MASCC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MASCC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MASCC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_IDADE               NUMBER(3)              NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SINTOMA             NUMBER(3)              NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PAS                 NUMBER(5,2)            NOT NULL,
  IE_BRONQUITE           VARCHAR2(1 BYTE)       NOT NULL,
  IE_CORTICOIDE          VARCHAR2(1 BYTE),
  IE_BRONCODILATADORES   VARCHAR2(1 BYTE),
  IE_ONCO_HEMATOLOGICA   VARCHAR2(1 BYTE)       NOT NULL,
  IE_HIDRATACAO_ENDO     VARCHAR2(1 BYTE)       NOT NULL,
  IE_FEBRE               VARCHAR2(1 BYTE)       NOT NULL,
  QT_PONTUACAO           NUMBER(3),
  NR_HORA                NUMBER(2),
  NR_SEQ_REG_ELEMENTO    NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMASC_ATEPACI_FK_I ON TASY.ESCALA_MASCC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMASC_EHRREEL_FK_I ON TASY.ESCALA_MASCC
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMASC_PESFISI_FK_I ON TASY.ESCALA_MASCC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMASC_PK ON TASY.ESCALA_MASCC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMASC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_mascc_atual
before insert or update ON TASY.ESCALA_MASCC for each row
declare
qt_reg_w		number(1);
qt_pontos_w		number(10) := 0;
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

qt_pontos_w	:= qt_pontos_w + :new.ie_sintoma;

if	(:new.qt_pas >= 90)	then
	qt_pontos_w	:= qt_pontos_w + 5;
end if;

if	(nvl(:new.ie_bronquite,'N') = 'N')	then
	qt_pontos_w	:= qt_pontos_w + 4;
end if;

/*if	(:new.ie_corticoide = 'S')	then
	qt_pontos_w	:= qt_pontos_w -4;
end if;

if	(:new.ie_broncodilatadores = 'S')	then
	qt_pontos_w	:= qt_pontos_w -4;
end if;*/

if	(nvl(:new.ie_onco_hematologica,'N') = 'N')	then
	qt_pontos_w	:= qt_pontos_w + 4;
end if;

if	(nvl(:new.ie_hidratacao_endo,'N') = 'N')	then
	qt_pontos_w	:= qt_pontos_w + 3;
end if;

if	(nvl(:new.ie_febre,'N') = 'S')	then
	qt_pontos_w	:= qt_pontos_w + 3;
end if;

if	(:new.qt_idade < 60)	then
	qt_pontos_w	:= qt_pontos_w + 2;
end if;

:new.qt_pontuacao	:= qt_pontos_w;

<<Final>>
qt_reg_w	:= 0;


end escala_mascc_atual;
/


ALTER TABLE TASY.ESCALA_MASCC ADD (
  CONSTRAINT ESCMASC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MASCC ADD (
  CONSTRAINT ESCMASC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMASC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMASC_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_MASCC TO NIVEL_1;

