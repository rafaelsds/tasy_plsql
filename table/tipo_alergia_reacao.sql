ALTER TABLE TASY.TIPO_ALERGIA_REACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_ALERGIA_REACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_ALERGIA_REACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REACAO        NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIALREA_MEDREAL_FK_I ON TASY.TIPO_ALERGIA_REACAO
(NR_SEQ_REACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIALREA_MEDREAL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIALREA_PK ON TASY.TIPO_ALERGIA_REACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIALREA_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIALREA_TIPALER_FK_I ON TASY.TIPO_ALERGIA_REACAO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIALREA_TIPALER_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TIPO_ALERGIA_REACAO ADD (
  CONSTRAINT TIALREA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_ALERGIA_REACAO ADD (
  CONSTRAINT TIALREA_MEDREAL_FK 
 FOREIGN KEY (NR_SEQ_REACAO) 
 REFERENCES TASY.MED_REACAO_ALERGICA (NR_SEQUENCIA),
  CONSTRAINT TIALREA_TIPALER_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ALERGIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_ALERGIA_REACAO TO NIVEL_1;

