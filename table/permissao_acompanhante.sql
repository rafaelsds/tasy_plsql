ALTER TABLE TASY.PERMISSAO_ACOMPANHANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERMISSAO_ACOMPANHANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERMISSAO_ACOMPANHANTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_IDADE_INICIAL     NUMBER(5),
  QT_IDADE_FINAL       NUMBER(5),
  IE_PERMITE_ACOMP     VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_CONVENIO          NUMBER(5),
  NR_SEQ_CLASSIF_PF    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERACOP_CLASPES_FK_I ON TASY.PERMISSAO_ACOMPANHANTE
(NR_SEQ_CLASSIF_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERACOP_CONVENI_FK_I ON TASY.PERMISSAO_ACOMPANHANTE
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PERACOP_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PERACOP_PK ON TASY.PERMISSAO_ACOMPANHANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PERMISSAO_ACOMPANHANTE ADD (
  CONSTRAINT PERACOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERMISSAO_ACOMPANHANTE ADD (
  CONSTRAINT PERACOP_CLASPES_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PF) 
 REFERENCES TASY.CLASSIF_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PERACOP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.PERMISSAO_ACOMPANHANTE TO NIVEL_1;

