ALTER TABLE TASY.ESCALA_FRAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_FRAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_FRAT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_QUEDA_6_MESES       VARCHAR2(1 BYTE),
  IE_MEDICACAO           VARCHAR2(1 BYTE),
  IE_DISPOSITIVO         VARCHAR2(1 BYTE),
  IE_AUXILIO_SUPERV      VARCHAR2(1 BYTE),
  IE_MARCHA              VARCHAR2(1 BYTE),
  IE_COMPROMETIMENTO     VARCHAR2(1 BYTE),
  IE_PERCEPCAO           VARCHAR2(1 BYTE),
  IE_ELIMINACAO          VARCHAR2(1 BYTE),
  IE_CATEGORIA           VARCHAR2(1 BYTE),
  IE_IMPULSIVIDADE       VARCHAR2(1 BYTE),
  IE_LIMITACAO           VARCHAR2(1 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PONTUACAO           NUMBER(3),
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_IDADE               NUMBER(1),
  NR_SEQ_PRESCR          NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCFRAT_ATEPACI_FK_I ON TASY.ESCALA_FRAT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAT_PEPRESC_FK_I ON TASY.ESCALA_FRAT
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAT_PESFISI_FK_I ON TASY.ESCALA_FRAT
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCFRAT_PK ON TASY.ESCALA_FRAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_frat_atual
before insert or update ON TASY.ESCALA_FRAT for each row
declare
/*qt_idade_w			number(3,0)	:= 0;*/
qt_pontos_w			number(3,0)	:= 0;
qt_reg_w			number(1);
cd_pessoa_fisica_w	varchar2(10);
begin

Consiste_Liberacao_Escala(173);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;
/*cd_pessoa_fisica_w := obter_pessoa_atendimento(:new.nr_atendimento, 'C');
qt_idade_w := nvl(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'), 0);

if	(qt_idade_w > 59) and (qt_idade_w < 70) then
qt_pontos_w := qt_pontos_w + 1;
elsif	(qt_idade_w > 69) and (qt_idade_w < 80) then
qt_pontos_w := qt_pontos_w + 2;
elsif	(qt_idade_w > 79) then
qt_pontos_w := qt_pontos_w + 3;
end if;*/

if (:new.ie_idade = 1) then
	qt_pontos_w := qt_pontos_w + 1;
elsif (:new.ie_idade = 2) then
	qt_pontos_w := qt_pontos_w + 2;
elsif (:new.ie_idade = 3) then
	qt_pontos_w := qt_pontos_w + 3;
end if;

if (:new.ie_queda_6_meses = 'S') then
qt_pontos_w := qt_pontos_w + 5;
end if;

if (:new.ie_eliminacao = 'I') then
qt_pontos_w := qt_pontos_w + 2;
elsif	(:new.ie_eliminacao = 'F') then
qt_pontos_w := qt_pontos_w + 2;
elsif	(:new.ie_eliminacao = 'U') then
qt_pontos_w := qt_pontos_w + 4;
end if;

if (:new.ie_medicacao = 'U') then
qt_pontos_w := qt_pontos_w + 3;
elsif	(:new.ie_medicacao = 'D') then
qt_pontos_w := qt_pontos_w + 5;
elsif	(:new.ie_medicacao = 'P') then
qt_pontos_w := qt_pontos_w + 7;
end if;

if (:new.ie_dispositivo = 'U') then
qt_pontos_w := qt_pontos_w + 1;
elsif	(:new.ie_dispositivo = 'D') then
qt_pontos_w := qt_pontos_w + 2;
elsif	(:new.ie_dispositivo = 'T') then
qt_pontos_w := qt_pontos_w + 3;
end if;

if (:new.ie_auxilio_superv = 'S') then
qt_pontos_w := qt_pontos_w + 2;
end if;

if (:new.ie_marcha = 'S') then
qt_pontos_w := qt_pontos_w + 2;
end if;

if (:new.ie_comprometimento = 'S') then
qt_pontos_w := qt_pontos_w + 2;
end if;

if (:new.ie_percepcao = 'S') then
qt_pontos_w := qt_pontos_w + 1;
end if;

if (:new.ie_impulsividade = 'S') then
qt_pontos_w := qt_pontos_w + 2;
end if;

if (:new.ie_limitacao = 'S') then
qt_pontos_w := qt_pontos_w + 4;
end if;

if (:new.ie_categoria = 'C') then
	qt_pontos_w := 5;
elsif (:new.ie_categoria is not null) then
	qt_pontos_w := 14;
end if;
:new.qt_pontuacao := qt_pontos_w;

<<Final>>
qt_reg_w := 0;
end escala_frat_atual;
/


ALTER TABLE TASY.ESCALA_FRAT ADD (
  CONSTRAINT ESCFRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_FRAT ADD (
  CONSTRAINT ESCFRAT_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCFRAT_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT ESCFRAT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_FRAT TO NIVEL_1;

