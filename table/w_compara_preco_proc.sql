DROP TABLE TASY.W_COMPARA_PRECO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_COMPARA_PRECO_PROC
(
  NR_SEQ_PROC_INTERNO  NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  VL_PROCEDIMENTO      NUMBER(15,2),
  CD_CONVENIO          NUMBER(5),
  CD_CATEGORIA         VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_COMPARA_PRECO_PROC TO NIVEL_1;

