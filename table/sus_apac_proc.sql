ALTER TABLE TASY.SUS_APAC_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_APAC_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_APAC_PROC
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NR_SEQ_APAC        NUMBER(10)                 NOT NULL,
  CD_PROCEDIMENTO    NUMBER(15)                 NOT NULL,
  IE_ORIGEM_PROCED   NUMBER(10)                 NOT NULL,
  CD_ATIVIDADE_PROF  NUMBER(3),
  QT_PROCEDIMENTO    NUMBER(11,4)               NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  CD_CGC_FORNECEDOR  VARCHAR2(14 BYTE),
  NR_NOTA_FISCAL     VARCHAR2(6 BYTE),
  NR_SEQ_PROPACI     NUMBER(10),
  CD_MEDICO_EXEC     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSAPPR_MEDICO_FK_I ON TASY.SUS_APAC_PROC
(CD_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAPPR_PESJURI_FK_I ON TASY.SUS_APAC_PROC
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAPPR_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUSAPPR_PK ON TASY.SUS_APAC_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAPPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSAPPR_PROCEDI_FK_I ON TASY.SUS_APAC_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAPPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAPPR_PROPACI_FK_I ON TASY.SUS_APAC_PROC
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAPPR_PROPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAPPR_SUSAPMO_FK_I ON TASY.SUS_APAC_PROC
(NR_SEQ_APAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAPPR_SUSATPR_FK_I ON TASY.SUS_APAC_PROC
(CD_ATIVIDADE_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAPPR_SUSATPR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUS_APAC_PROC_ATUAL
BEFORE INSERT OR UPDATE ON TASY.SUS_APAC_PROC FOR EACH ROW
BEGIN

if	(:new.ie_origem_proced <> 3) then
	--Os procedimentos APAC tem que ser origem 3 BPA');
	Wheb_mensagem_pck.exibir_mensagem_abort(261598);
end if;

END;
/


ALTER TABLE TASY.SUS_APAC_PROC ADD (
  CONSTRAINT SUSAPPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          768K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_APAC_PROC ADD (
  CONSTRAINT SUSAPPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSAPPR_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_EXEC) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SUSAPPR_SUSATPR_FK 
 FOREIGN KEY (CD_ATIVIDADE_PROF) 
 REFERENCES TASY.SUS_ATIVIDADE_PROFISSIONAL (CD_ATIVIDADE_PROFISSIONAL),
  CONSTRAINT SUSAPPR_SUSAPMO_FK 
 FOREIGN KEY (NR_SEQ_APAC) 
 REFERENCES TASY.SUS_APAC_MOVTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUSAPPR_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SUSAPPR_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUS_APAC_PROC TO NIVEL_1;

