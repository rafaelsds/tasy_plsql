ALTER TABLE TASY.PACIENTE_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_EXAME
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_EXAME_PAC               VARCHAR2(15 BYTE)  NOT NULL,
  DS_RESULTADO               VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_FORMA_REG           NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_EXAME                   DATE,
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACEXAM_ATCONSPEPA_FK_I ON TASY.PACIENTE_EXAME
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACEXAM_EHRREEL_FK_I ON TASY.PACIENTE_EXAME
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACEXAM_FOREPEX_FK_I ON TASY.PACIENTE_EXAME
(NR_SEQ_FORMA_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_FOREPEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACEXAM_HISTSAU_FK_I ON TASY.PACIENTE_EXAME
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACEXAM_I1 ON TASY.PACIENTE_EXAME
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_I1
  MONITORING USAGE;


CREATE INDEX TASY.PACEXAM_PESFISI_FK_I ON TASY.PACIENTE_EXAME
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACEXAM_PESFISI_FK2_I ON TASY.PACIENTE_EXAME
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACEXAM_PK ON TASY.PACIENTE_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACEXAM_TASASDI_FK_I ON TASY.PACIENTE_EXAME
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACEXAM_TASASDI_FK2_I ON TASY.PACIENTE_EXAME
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACEXAM_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pac_exame_pend_atual 
after insert or update ON TASY.PACIENTE_EXAME 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSP'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSP'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_exame_covid
before insert or update ON TASY.PACIENTE_EXAME for each row
declare

  nr_atendimento_w  atendimento_paciente.nr_atendimento%type;
  nr_seq_atend_adic_w  atend_paciente_adic.nr_sequencia%type;

begin

if ((:new.ie_exame_pac is not null) and (:new.ie_exame_pac = 'COVID-19')) then

  select max(nr_atendimento)
  into   nr_atendimento_w
  from   atendimento_paciente
  where  cd_pessoa_fisica = :new.cd_pessoa_fisica;

  select max(nr_sequencia)
  into   nr_seq_atend_adic_w
  from   atend_paciente_adic
  where  nr_atendimento = nr_atendimento_w;

  if ((trim(substr(upper(:new.ds_resultado),1,7))) like (substr(upper(OBTER_DESC_EXPRESSAO(296109)),1,7)) and (:new.ie_situacao = 'A')) then


    if (nr_seq_atend_adic_w is null) then

      insert into ATEND_PACIENTE_ADIC  (  nr_sequencia,
                          dt_atualizacao,
                          nm_usuario,
                          nr_atendimento,
                          ie_atend_covid_posit)
      values (atend_paciente_adic_seq.nextVal,sysdate,:new.nm_usuario,nr_atendimento_w,'S');
    else
      update ATEND_PACIENTE_ADIC  set ie_atend_covid_posit = 'S' where nr_sequencia = nr_seq_atend_adic_w;
    end if;
  else
  --  Raise_application_error(-20011,'#@#@'||upper(:new.ds_resultado)||' - '||upper(OBTER_DESC_EXPRESSAO(296109))||' - '||nr_seq_atend_adic_w);
    if (nr_seq_atend_adic_w is null) then

      insert into ATEND_PACIENTE_ADIC (nr_sequencia,
                      dt_atualizacao,
                      nm_usuario,
                      nr_atendimento,
                      ie_atend_covid_posit)
      values (ATEND_PACIENTE_ADIC_seq.nextVal,sysdate,:new.nm_usuario,nr_atendimento_w,'N');
    else
      update ATEND_PACIENTE_ADIC  set ie_atend_covid_posit = 'N' where nr_sequencia = nr_seq_atend_adic_w;
    end if;

  end if;

end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_EXAME_ATUAL
before insert or update ON TASY.PACIENTE_EXAME for each row
declare
NR_ATENDIMENTO_W                    NUMBER(10);

begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');

end if;

if	(((nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) and
	(:new.DS_RESULTADO is not null)) or
	((:new.DT_LIBERACAO is not null) and
	(:new.DS_RESULTADO is not null) and
	(nvl(:old.DS_RESULTADO,'XPTO') <> :new.DS_RESULTADO))) then


	--(:new.DS_RESULTADO is not null) and
	--(:old.DS_RESULTADO <> :new.DS_RESULTADO))
	if (:new.IE_EXAME_PAC = 'COVID-19') then
		select 	max(nr_atendimento)
		into	NR_ATENDIMENTO_W
		from	atendimento_paciente
		where 	cd_pessoa_fisica = :new.CD_PESSOA_FISICA;

		gerar_evolucao_exameRapido(NR_ATENDIMENTO_W, :NEW.NM_USUARIO,'EXA', obter_desc_expressao(964679)||': ' ||:new.DS_RESULTADO ||chr(10)||chr(13)||nvl(:new.ds_observacao,:old.ds_observacao), :new.CD_PESSOA_FISICA);

	end if;
end if;
end;
/


ALTER TABLE TASY.PACIENTE_EXAME ADD (
  CONSTRAINT PACEXAM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_EXAME ADD (
  CONSTRAINT PACEXAM_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACEXAM_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACEXAM_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACEXAM_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACEXAM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACEXAM_FOREPEX_FK 
 FOREIGN KEY (NR_SEQ_FORMA_REG) 
 REFERENCES TASY.FORMA_REG_PAC_EXAME (NR_SEQUENCIA),
  CONSTRAINT PACEXAM_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACEXAM_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_EXAME TO NIVEL_1;

