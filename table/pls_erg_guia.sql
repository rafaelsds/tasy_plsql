ALTER TABLE TASY.PLS_ERG_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ERG_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ERG_GUIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ERG_CABECALHO  NUMBER(10)              NOT NULL,
  NR_GUIA_ORIGEM        VARCHAR2(20 BYTE),
  NR_GUIA_OPERADORA     VARCHAR2(20 BYTE),
  CD_SENHA              VARCHAR2(20 BYTE),
  NR_SEQ_GRG_GUIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSERGG_PK ON TASY.PLS_ERG_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSERGG_PLSERGC_FK_I ON TASY.PLS_ERG_GUIA
(NR_SEQ_ERG_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSERGG_PLSGRGG_FK_I ON TASY.PLS_ERG_GUIA
(NR_SEQ_GRG_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ERG_GUIA ADD (
  CONSTRAINT PLSERGG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ERG_GUIA ADD (
  CONSTRAINT PLSERGG_PLSERGC_FK 
 FOREIGN KEY (NR_SEQ_ERG_CABECALHO) 
 REFERENCES TASY.PLS_ERG_CABECALHO (NR_SEQUENCIA),
  CONSTRAINT PLSERGG_PLSGRGG_FK 
 FOREIGN KEY (NR_SEQ_GRG_GUIA) 
 REFERENCES TASY.PLS_GRG_GUIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ERG_GUIA TO NIVEL_1;

