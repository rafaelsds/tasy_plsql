ALTER TABLE TASY.RP_EQUIP_MANUTENCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RP_EQUIP_MANUTENCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RP_EQUIP_MANUTENCAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EQUIP         NUMBER(10)               NOT NULL,
  DT_ENVIO             DATE,
  DT_PREV_RETORNO      DATE,
  NR_SEQ_MOTIVO        NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  DT_LIBERACAO         DATE,
  DS_JUSTIFICATIVA     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RPEQUIPMAN_PK ON TASY.RP_EQUIP_MANUTENCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEQUIPMAN_PK
  MONITORING USAGE;


CREATE INDEX TASY.RPEQUIPMAN_RPEQUIP_FK_I ON TASY.RP_EQUIP_MANUTENCAO
(NR_SEQ_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEQUIPMAN_RPEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPEQUIPMAN_RPMOTMAN_FK_I ON TASY.RP_EQUIP_MANUTENCAO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPEQUIPMAN_RPMOTMAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RP_EQUIP_MANUTENCAO ADD (
  CONSTRAINT RPEQUIPMAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RP_EQUIP_MANUTENCAO ADD (
  CONSTRAINT RPEQUIPMAN_RPEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIP) 
 REFERENCES TASY.RP_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT RPEQUIPMAN_RPMOTMAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.RP_MOTIVO_MANUTENCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RP_EQUIP_MANUTENCAO TO NIVEL_1;

