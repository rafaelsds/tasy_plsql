ALTER TABLE TASY.MEDICAL_DISCLOSURE_CLAIM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICAL_DISCLOSURE_CLAIM CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICAL_DISCLOSURE_CLAIM
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                 DATE,
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  IE_REIVINDICACAO_DIVULGACAO  VARCHAR2(1 BYTE),
  DT_REIVINDICACAO             DATE,
  DS_REIVINDICACAO             VARCHAR2(255 BYTE),
  CD_REIVINDICACAO             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MEDDICL_PK ON TASY.MEDICAL_DISCLOSURE_CLAIM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MEDICAL_DISCLOSURE_CLAIM ADD (
  CONSTRAINT MEDDICL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

