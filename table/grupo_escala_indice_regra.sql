ALTER TABLE TASY.GRUPO_ESCALA_INDICE_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GRUPO_ESCALA_INDICE_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GRUPO_ESCALA_INDICE_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO            NUMBER(10)            NOT NULL,
  CD_ESPECIALIDADE        NUMBER(5),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_SETOR_ATEND_EXCLUIR  NUMBER(5),
  IE_PERMITE              VARCHAR2(1 BYTE)      NOT NULL,
  NM_USUARIO_PARAM        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GRUESIR_ESPMEDI_FK_I ON TASY.GRUPO_ESCALA_INDICE_REGRA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GRUESIR_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GRUESIR_GRUESIN_FK_I ON TASY.GRUPO_ESCALA_INDICE_REGRA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GRUESIR_PK ON TASY.GRUPO_ESCALA_INDICE_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GRUESIR_SETATEN_FK_I ON TASY.GRUPO_ESCALA_INDICE_REGRA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GRUESIR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GRUESIR_SETATEN_FK2_I ON TASY.GRUPO_ESCALA_INDICE_REGRA
(CD_SETOR_ATEND_EXCLUIR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GRUESIR_SETATEN_FK2_I
  MONITORING USAGE;


ALTER TABLE TASY.GRUPO_ESCALA_INDICE_REGRA ADD (
  CONSTRAINT GRUESIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GRUPO_ESCALA_INDICE_REGRA ADD (
  CONSTRAINT GRUESIR_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT GRUESIR_GRUESIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_ESCALA_INDICE (NR_SEQUENCIA),
  CONSTRAINT GRUESIR_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_EXCLUIR) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT GRUESIR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.GRUPO_ESCALA_INDICE_REGRA TO NIVEL_1;

