ALTER TABLE TASY.PRIVACY_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRIVACY_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRIVACY_RULE
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DS_RULE                      VARCHAR2(150 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_TYPE                      VARCHAR2(15 BYTE),
  DT_VALITY_START              DATE,
  DT_VALITY_END                DATE,
  NR_ADT_TIME_DISCHARGE        NUMBER(4),
  NR_ADT_TIME_TRANSFER         NUMBER(4),
  IE_ENCOUNTER_CLOSED_ACCOUNT  VARCHAR2(1 BYTE),
  IE_ENCOUNTER_DISCHARGE       VARCHAR2(1 BYTE),
  IE_ENCOUNTER_PREVIOUS        VARCHAR2(1 BYTE),
  IE_REQUIRE_JUSTIFY           VARCHAR2(1 BYTE),
  IE_REQUIRE_REASON            VARCHAR2(1 BYTE),
  IE_REQUIRE_SECOND_USER       VARCHAR2(1 BYTE),
  IE_PERMISSION                VARCHAR2(15 BYTE),
  NR_SEQ_MESSAGE               NUMBER(10),
  IE_ADD_LOG_MESSAGE           VARCHAR2(1 BYTE),
  IE_RELEASE_ACCESS_ENCOUNTER  VARCHAR2(1 BYTE),
  IE_RELEASE_ACCESS_MEDICAL    VARCHAR2(1 BYTE),
  IE_ENCOUNTER_DECEASED        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRIV_RULE_PK ON TASY.PRIVACY_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRIV_RULE_PRUSMES_FK_I ON TASY.PRIVACY_RULE
(NR_SEQ_MESSAGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PRIVACY_RULE_ATUAL
before insert or update ON TASY.PRIVACY_RULE for each row
declare

qt_records_w	number(10);

begin

if (:new.dt_vality_end  < :new.dt_vality_start) then
	wheb_mensagem_pck.exibir_mensagem_abort(823223);
	--'Data de validade deve ser maior que a data de in�cio.'
end if;

if ((:new.ie_situacao <> :old.ie_situacao) and (:new.ie_situacao = 'I')) then
	select	sum(qt)
	into	qt_records_w
	from (
		select	count(*) qt
		from	privacy_estab_usergroup
		where	nr_seq_privacy_rule = :new.nr_sequencia
		and	ie_situacao = 'A'
		union all
		select	count(*) qt
		from	privacy_estab_team
		where	nr_seq_privacy_rule = :new.nr_sequencia
		and	ie_situacao = 'A'
		union all
		select	count(*) qt
		from	privacy_estab_profile
		where	nr_seq_privacy_rule = :new.nr_sequencia
		and	ie_situacao = 'A'
		union all
		select	count(*) qt
		from	privacy_estab_department
		where	nr_seq_privacy_rule = :new.nr_sequencia
		and	ie_situacao = 'A'
	);

	if (qt_records_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(950297);
		--N�o � poss�vel inativar esta regra pois ela est� associada a uma permiss�o de acesso.
		--Voc� precisa excluir ou inativar a permiss�o de acesso antes de inativar a regra.
	end if;
end if;

end;
/


ALTER TABLE TASY.PRIVACY_RULE ADD (
  CONSTRAINT PRIV_RULE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRIVACY_RULE ADD (
  CONSTRAINT PRIV_RULE_PRUSMES_FK 
 FOREIGN KEY (NR_SEQ_MESSAGE) 
 REFERENCES TASY.PRIVACY_USER_MESSAGE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRIVACY_RULE TO NIVEL_1;

