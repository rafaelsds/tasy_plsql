ALTER TABLE TASY.PLS_TIPO_GUIA_MED_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_GUIA_MED_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_GUIA_MED_PARTIC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_GUIA          NUMBER(10)          NOT NULL,
  NR_SEQ_TIPO_PRESTADOR     NUMBER(10),
  NR_SEQ_CLASSIF_PRESTADOR  NUMBER(10),
  NR_SEQ_GRUPO_PREST        NUMBER(10),
  NR_SEQ_PRESTADOR          NUMBER(10),
  IE_ENTRA_GUIA_MEDICO      VARCHAR2(1 BYTE)    NOT NULL,
  IE_ENTRA_PORTAL           VARCHAR2(1 BYTE),
  NR_SEQ_PLANO              NUMBER(10),
  NR_SEQ_REDE_ATEND         NUMBER(10),
  NR_SEQ_TIPO_ACOMODACAO    NUMBER(10),
  SG_ESTADO                 VARCHAR2(2 BYTE),
  CD_MUNICIPIO_IBGE         VARCHAR2(6 BYTE),
  NR_SEQ_REGIAO             NUMBER(10),
  CD_ESPECIALIDADE          NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTGPA_ESPMEDI_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTGPA_PK ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSCLPR_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_CLASSIF_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSPLAN_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSPRGP_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_GRUPO_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSREAT_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_REDE_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSREAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSREGI_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSREGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSTGME_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_TIPO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTGPA_PLSTIAC_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_PLSTIPR_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTGPA_SUSMUNI_FK_I ON TASY.PLS_TIPO_GUIA_MED_PARTIC
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTGPA_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_TIPO_GUIA_MED_PARTIC_tp  after update ON TASY.PLS_TIPO_GUIA_MED_PARTIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MUNICIPIO_IBGE,1,500);gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PLS_TIPO_GUIA_MED_PARTIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_REGIAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_REGIAO,1,4000),substr(:new.NR_SEQ_REGIAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGIAO',ie_log_w,ds_w,'PLS_TIPO_GUIA_MED_PARTIC',ds_s_w,ds_c_w);  ds_w:=substr(:new.SG_ESTADO,1,500);gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PLS_TIPO_GUIA_MED_PARTIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TIPO_GUIA_MED_PARTIC ADD (
  CONSTRAINT PLSTGPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_GUIA_MED_PARTIC ADD (
  CONSTRAINT PLSTGPA_PLSTGME_FK 
 FOREIGN KEY (NR_SEQ_TIPO_GUIA) 
 REFERENCES TASY.PLS_TIPO_GUIA_MEDICO (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PRESTADOR) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PREST) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSREAT_FK 
 FOREIGN KEY (NR_SEQ_REDE_ATEND) 
 REFERENCES TASY.PLS_REDE_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSREGI_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.PLS_REGIAO (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSTGPA_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PLSTGPA_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PLS_TIPO_GUIA_MED_PARTIC TO NIVEL_1;

