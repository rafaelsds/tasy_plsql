ALTER TABLE TASY.LEITO_HIG_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LEITO_HIG_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LEITO_HIG_CONTROLE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_FIM               DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NR_SEQ_INTERNO       NUMBER(10),
  IE_STATUS            VARCHAR2(3 BYTE),
  DT_INICIO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LTHGCTL_PESFISI_FK_I ON TASY.LEITO_HIG_CONTROLE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LTHGCTL_PK ON TASY.LEITO_HIG_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LTHGCTL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.leito_hig_controle_insert
after insert ON TASY.LEITO_HIG_CONTROLE for each row
declare

begin

if	(:new.dt_inicio is not null) and
	(nvl(:new.nr_Seq_interno,0)	> 0) then
	update	unidade_atendimento
	set	ie_status_unidade	= 'H',
		dt_higienizacao		= null,
		dt_inicio_higienizacao	= sysdate,
		nm_usuario		= :new.nm_usuario
	where	nr_seq_interno		= :new.nr_Seq_interno;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.leito_hig_controle_update
after update ON TASY.LEITO_HIG_CONTROLE for each row
declare

begin


if	(:new.dt_fim is not null) and
	(:old.dt_fim is null) and
	(nvl(:new.nr_seq_interno,0)	> 0) then
	update	unidade_atendimento
	set	ie_status_unidade 	= 'L',
		nm_usuario		= :new.nm_usuario,
		dt_atualizacao		= sysdate
	where	nr_seq_interno 		= :new.nr_seq_interno;
end if;

end;
/


ALTER TABLE TASY.LEITO_HIG_CONTROLE ADD (
  CONSTRAINT LTHGCTL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LEITO_HIG_CONTROLE ADD (
  CONSTRAINT LTHGCTL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.LEITO_HIG_CONTROLE TO NIVEL_1;

