ALTER TABLE TASY.W_CONSUMO_HALOGENADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_CONSUMO_HALOGENADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CONSUMO_HALOGENADO
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  NR_CIRURGIA        NUMBER(10),
  NR_SEQ_PEPO        NUMBER(10),
  NR_SEQ_AGENTE      NUMBER(10),
  DT_INICIO          DATE                       NOT NULL,
  DT_FIM             DATE                       NOT NULL,
  QT_HALOG_INS       NUMBER(15,4)               NOT NULL,
  QT_FLUXO_GAS_ML    NUMBER(15,4)               NOT NULL,
  QT_CONSUMO_ML      NUMBER(15,4)               NOT NULL,
  QT_VELOCIDADE_INF  NUMBER(15,4),
  IE_TIPO_DOSAGEM    VARCHAR2(10 BYTE),
  CD_MATERIAL        NUMBER(10)                 NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCONALO_AGEANES_FK_I ON TASY.W_CONSUMO_HALOGENADO
(NR_SEQ_AGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONALO_AGEANES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WCONALO_CIRURGI_FK_I ON TASY.W_CONSUMO_HALOGENADO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONALO_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WCONALO_MATERIA_FK_I ON TASY.W_CONSUMO_HALOGENADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONALO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WCONALO_PEPOCIR_FK_I ON TASY.W_CONSUMO_HALOGENADO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONALO_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WCONALO_PK ON TASY.W_CONSUMO_HALOGENADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCONALO_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_CONSUMO_HALOGENADO ADD (
  CONSTRAINT WCONALO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_CONSUMO_HALOGENADO ADD (
  CONSTRAINT WCONALO_AGEANES_FK 
 FOREIGN KEY (NR_SEQ_AGENTE) 
 REFERENCES TASY.AGENTE_ANESTESICO (NR_SEQUENCIA),
  CONSTRAINT WCONALO_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT WCONALO_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WCONALO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_CONSUMO_HALOGENADO TO NIVEL_1;

