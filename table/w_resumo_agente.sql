ALTER TABLE TASY.W_RESUMO_AGENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_RESUMO_AGENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RESUMO_AGENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CIRURGIA                NUMBER(10),
  CD_MATERIAL                NUMBER(6),
  NR_SEQ_AGENTE              NUMBER(10),
  QT_DOSE_TOTAL              NUMBER(15,3),
  CD_UNIDADE_CONSUMO         VARCHAR2(30 BYTE),
  CD_UNIDADE_MEDIDA          VARCHAR2(30 BYTE),
  QT_DISPENSACAO             NUMBER(15,3),
  CD_UNIDADE_DISPENSACAO     VARCHAR2(30 BYTE),
  QT_DOSE_CONSUMO            NUMBER(15,3),
  IE_ORIGEM_GASTO            VARCHAR2(2 BYTE),
  NR_SEQ_PEPO                NUMBER(10),
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WRESAGE_AGEANES_FK_I ON TASY.W_RESUMO_AGENTE
(NR_SEQ_AGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_AGEANES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_CIRURGI_FK_I ON TASY.W_RESUMO_AGENTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WRESAGE_PEPOCIR_FK_I ON TASY.W_RESUMO_AGENTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WRESAGE_PK ON TASY.W_RESUMO_AGENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_PK
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_TASASDI_FK_I ON TASY.W_RESUMO_AGENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_TASASDI_FK2_I ON TASY.W_RESUMO_AGENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_UNIMEDI_FK_I ON TASY.W_RESUMO_AGENTE
(CD_UNIDADE_CONSUMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_UNIMEDI_FK2_I ON TASY.W_RESUMO_AGENTE
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.WRESAGE_UNIMEDI_FK3_I ON TASY.W_RESUMO_AGENTE
(CD_UNIDADE_DISPENSACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESAGE_UNIMEDI_FK3_I
  MONITORING USAGE;


ALTER TABLE TASY.W_RESUMO_AGENTE ADD (
  CONSTRAINT WRESAGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_RESUMO_AGENTE ADD (
  CONSTRAINT WRESAGE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_CONSUMO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT WRESAGE_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT WRESAGE_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNIDADE_DISPENSACAO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT WRESAGE_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT WRESAGE_AGEANES_FK 
 FOREIGN KEY (NR_SEQ_AGENTE) 
 REFERENCES TASY.AGENTE_ANESTESICO (NR_SEQUENCIA),
  CONSTRAINT WRESAGE_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WRESAGE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT WRESAGE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_RESUMO_AGENTE TO NIVEL_1;

