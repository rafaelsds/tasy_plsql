ALTER TABLE TASY.PESSOA_FISICA_PRIVACIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_PRIVACIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_PRIVACIDADE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_CRIACAO            DATE,
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  DT_INATIVACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFISPRIV_PESFISI_FK_I ON TASY.PESSOA_FISICA_PRIVACIDADE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFISPRIV_PK ON TASY.PESSOA_FISICA_PRIVACIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pessoa_fisica_privac_insert
after insert ON TASY.PESSOA_FISICA_PRIVACIDADE for each row
declare
valor_dominio_9577_w 	valor_dominio.vl_dominio%type;

cursor C01 is
	select 	nvl(ie_mala_direta, 'N') ie_mala_direta,
		nvl(ie_nf_correio, 'N') ie_nf_correio,
		ie_tipo_complemento,
		nr_sequencia
	from   	compl_pessoa_fisica
	where  	cd_pessoa_fisica 	= :NEW.cd_pessoa_fisica;

C01row C01%rowtype;

cursor C02 is
	select	nvl(ie_perm_sms_email, 'N') ie_perm_sms_email
	from	pessoa_fisica
	where  	cd_pessoa_fisica	= :NEW.cd_pessoa_fisica;

C02row C02%rowtype;

cursor C03 is
	with tbl_pfcorresp as(
		select	ie_tipo_corresp,
			ie_tipo_doc,
			nr_sequencia
		from 	pessoa_fisica_corresp
		where 	cd_pessoa_fisica 	= :New.cd_pessoa_fisica
		and 	ie_tipo_corresp 	in ('Email', 'MCel', 'Mala' )
	)
	select 	a.vl_dominio as ie_tipo_corresp,
		b.vl_dominio as ie_tipo_doc,
		(	select	decode(count(1), 0, 'N', 'S')
			from 	tbl_pfcorresp c
			where 	c.ie_tipo_corresp 	= a.vl_dominio
			and 	c.ie_tipo_doc 		in (b.vl_dominio, 'Q')) as ie_aceite
	from   	valor_dominio a,
		valor_dominio b
	where  	a.cd_dominio 	= 1418
	and    	a.vl_dominio 	in ('Email', 'MCel', 'Mala' )
	and    	b.cd_dominio 	= 1491
	and    	b.vl_dominio 	in ('PM', 'AE');

C03row C03%rowtype;

cursor C04 is
	select 	DECODE(NVL(ie_tipo_compl_corresp, 0), 0,'N','S') ie_tipo_compl_corresp,
		NVL(ie_email_laudo, 'N') ie_email_laudo,
		ie_tipo_compl_corresp ie_tipo_complemento
	from 	medico
	where 	cd_pessoa_fisica = :NEW.cd_pessoa_fisica;

C04row C04%rowtype;

cursor C05 is
	select 	NVL(ie_correspondencia,'N') ie_correspondencia,
	  	ie_tipo_complemento,
		nr_sequencia
	from	compl_pessoa_fisica
	where 	cd_pessoa_fisica = :NEW.cd_pessoa_fisica
	and exists ( 	select 	cd_pessoa_fisica
			from	medico
			where 	cd_pessoa_fisica = :NEW.cd_pessoa_fisica );

C05row C05%rowtype;


procedure inserirRegistro( 	ie_tipo_consentimento_p		in	pessoa_fisica_privac_item.ie_tipo_consentimento%type,
				ie_tipo_documento_p		in	pessoa_fisica_privac_item.ie_tipo_documento%type,
				ie_tipo_complemento_pf_p	in	pessoa_fisica_privac_item.ie_tipo_complemento_pf%type,
				ie_aceite_p			in	pessoa_fisica_privac_item.ie_aceite%type,
				nr_sequencia_comp_pf_p		in	pessoa_fisica_privac_item.nr_seq_compl_pf%type ) is
begin
insert into pessoa_fisica_privac_item (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_pf_privacidade,
	ie_tipo_consentimento,
	ie_tipo_documento,
	ie_tipo_complemento_pf,
	ie_aceite,
	nr_seq_compl_pf)
values (pessoa_fisica_privac_item_seq.nextval,
	sysdate,
	:NEW.nm_usuario,
	sysdate,
	:NEW.nm_usuario,
	:NEW.nr_sequencia,
	ie_tipo_consentimento_p,
	ie_tipo_documento_p,
	ie_tipo_complemento_pf_p,
	ie_aceite_p,
	nr_sequencia_comp_pf_p);
end inserirRegistro;

begin

	<<ler_pessoa_fisica>>
	for C02row in C02
	loop
		inserirRegistro(	'SEA',
					null,
					null,
					C02row.ie_perm_sms_email,
					null );

		inserirRegistro(	'SAI',
					null,
					null,
					C02row.ie_perm_sms_email,
					null );
	end loop ler_pessoa_fisica;

	<<ler_complementos>>
	for C01row in C01
	loop
		inserirRegistro(	'CMD',
					null,
					C01row.ie_tipo_complemento,
					C01row.ie_mala_direta,
					C01row.nr_sequencia );

		inserirRegistro(	'CNC',
					null,
					C01row.ie_tipo_complemento,
					C01row.ie_nf_correio,
					C01row.nr_sequencia );
	end loop ler_complementos;


	<<ler_correspondecia>>
	for C03row in C03
	loop
		valor_dominio_9577_w	:= NULL;

		if	(C03row.ie_tipo_corresp = 'Email') and
			(C03row.ie_tipo_doc = 'AE') then
			valor_dominio_9577_w	:= 'EAE';
		end if;

		if	(C03row.ie_tipo_corresp = 'Email') and
			(C03row.ie_tipo_doc = 'PM') then
			valor_dominio_9577_w	:= 'EPM';
		end if;

		if	(C03row.ie_tipo_corresp = 'MCel') and
			(C03row.ie_tipo_doc = 'AE') then
			valor_dominio_9577_w	:= 'SAE';
		end if;

		if	(C03row.ie_tipo_corresp = 'MCel') and
			(C03row.ie_tipo_doc = 'PM') then
			valor_dominio_9577_w	:= 'SPM';
		end if;

		if	(C03row.ie_tipo_corresp = 'Mala') and
			(C03row.ie_tipo_doc = 'AE') then
			valor_dominio_9577_w	:= 'MAE';
		end if;

		if	(C03row.ie_tipo_corresp = 'Mala') and
			(C03row.ie_tipo_doc = 'PM') then
			valor_dominio_9577_w	:= 'MPM';
		end if;

		inserirRegistro(	valor_dominio_9577_w,
			C03row.ie_tipo_doc,
			null,
			C03row.ie_aceite,
			null );

	end loop ler_correspondecia;

	<<ler_medico>>
	for C04row in C04
	loop
		inserirRegistro(	'MCM',
					null,
					C04row.ie_tipo_complemento,
					C04row.ie_tipo_compl_corresp,
					null );

		inserirRegistro(	'ERM',
					null,
					null,
					C04row.ie_email_laudo,
					null );
	end loop ler_medico;

	<<ler_complemento_medico>>
	for C05row in C05
	loop
		-- Adicionar consentimento para receber correspondencia
		inserirRegistro(	'CCC',
					null,
					C05row.ie_tipo_complemento,
					C05row.ie_correspondencia,
					C05row.nr_sequencia );
	end loop ler_complemento_medico;
end;
/


ALTER TABLE TASY.PESSOA_FISICA_PRIVACIDADE ADD (
  CONSTRAINT PESFISPRIV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_PRIVACIDADE ADD (
  CONSTRAINT PESFISPRIV_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PESSOA_FISICA_PRIVACIDADE TO NIVEL_1;

