ALTER TABLE TASY.CODIFICACAO_ATEND_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CODIFICACAO_ATEND_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CODIFICACAO_ATEND_ITEM
(
  NR_SEQ_CODIFICACAO   NUMBER(10)               NOT NULL,
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO_ITEM         VARCHAR2(1 BYTE)         NOT NULL,
  CD_DOENCA_CID        VARCHAR2(10 BYTE),
  IE_STATUS            VARCHAR2(2 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  CD_CID_ONCOLOGIA     VARCHAR2(10 BYTE),
  IE_CLASSIFICACAO     VARCHAR2(1 BYTE),
  NR_SEQ_PROC_PAC      NUMBER(10),
  NR_SEQ_DIAG_PAC      NUMBER(10),
  DT_INATIVACAO        DATE,
  NR_SEQ_PROC_INTERNO  NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  NR_CIRURGIA          NUMBER(10)               DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CODATENDI_CIDDOEN_FK_I ON TASY.CODIFICACAO_ATEND_ITEM
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CODATENDI_CODATEND_FK_I ON TASY.CODIFICACAO_ATEND_ITEM
(NR_SEQ_CODIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CODATENDI_PK ON TASY.CODIFICACAO_ATEND_ITEM
(NR_SEQ_CODIFICACAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CODATENDI_PROPACI_FK_I ON TASY.CODIFICACAO_ATEND_ITEM
(NR_SEQ_PROC_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.codificacao_atend_item_update
before update ON TASY.CODIFICACAO_ATEND_ITEM for each row
declare

begin

if (:old.dt_inativacao is null) and
   (:new.dt_inativacao is not null) then

	if (:new.ie_tipo_item = 'D') then
		if (:new.ie_classificacao = 'P') then
			gerar_codificacao_atend_log(:new.nr_seq_codificacao, obter_desc_expressao(890454) || '. ' || obter_desc_expressao(287711) || ' : ' ||
				substr(obter_desc_cid(:old.cd_doenca_cid),1,100), wheb_usuario_pck.get_nm_usuario);
		elsif (:new.ie_classificacao = 'S') then
			gerar_codificacao_atend_log(:new.nr_seq_codificacao, obter_desc_expressao(890454) || '. ' || obter_desc_expressao(287716) || ' : ' ||
				substr(obter_desc_cid(:old.cd_doenca_cid),1,100), wheb_usuario_pck.get_nm_usuario);
		end if;
	elsif (:new.ie_tipo_item = 'P') then
		if (:new.ie_classificacao = 'P') then
			gerar_codificacao_atend_log(:new.nr_seq_codificacao, obter_desc_expressao(890454) || '. ' || obter_desc_expressao(911017) || ' : ' ||
				substr(Obter_Descricao_Procedimento(:old.nr_seq_proc_interno, null),1,255), wheb_usuario_pck.get_nm_usuario);
		elsif (:new.ie_classificacao = 'S') then
			gerar_codificacao_atend_log(:new.nr_seq_codificacao, obter_desc_expressao(890454) || '. ' || obter_desc_expressao(911019) || ' : ' ||
				substr(Obter_Descricao_Procedimento(:old.nr_seq_proc_interno, null),1,255), wheb_usuario_pck.get_nm_usuario);
		end if;
	end if;

end if;

end;
/


ALTER TABLE TASY.CODIFICACAO_ATEND_ITEM ADD (
  CONSTRAINT CODATENDI_PK
 PRIMARY KEY
 (NR_SEQ_CODIFICACAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CODIFICACAO_ATEND_ITEM ADD (
  CONSTRAINT CODATENDI_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT CODATENDI_CODATEND_FK 
 FOREIGN KEY (NR_SEQ_CODIFICACAO) 
 REFERENCES TASY.CODIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CODATENDI_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROC_PAC) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CODIFICACAO_ATEND_ITEM TO NIVEL_1;

