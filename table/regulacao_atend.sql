ALTER TABLE TASY.REGULACAO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGULACAO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGULACAO_ATEND
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  CD_ESPECIALIDADE         NUMBER(5),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_TIPO                  VARCHAR2(3 BYTE)     NOT NULL,
  IE_STATUS                VARCHAR2(3 BYTE),
  CD_PROFISSIONAL_DEST     VARCHAR2(10 BYTE),
  NM_USUARIO_STATUS        VARCHAR2(15 BYTE),
  DT_USUARIO_STATUS        DATE,
  NR_SEQ_ENCAMINHAMENTO    NUMBER(10),
  CD_EVOLUCAO_ORI          NUMBER(10),
  NR_SEQ_PARECER_ORI       NUMBER(10),
  NR_SEQ_RESP_PARECER_ORI  NUMBER(10),
  NR_SEQ_GRUPO_REGULACAO   NUMBER(10),
  IE_CIENTE                VARCHAR2(1 BYTE),
  DT_CIENTE                DATE,
  NM_USUARIO_CIENTE        VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO      NUMBER(10),
  CD_MATERIAL_EXAME        VARCHAR2(20 BYTE),
  NR_SEQ_EXAME             NUMBER(10),
  NR_SEQ_EXAME_CAD         NUMBER(10),
  NR_SEQ_PEDIDO            NUMBER(10),
  NR_SEQ_PEDIDO_ITEM       NUMBER(10),
  NR_SEQ_CPOE_PROC         NUMBER(10),
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  NR_SEQ_REQUISICAO_ITEM   NUMBER(10),
  NR_SEQ_SOLIC_TRANSF      NUMBER(10),
  NR_SEQ_CPOE              NUMBER(10),
  NR_SEQ_EQUIPAMENTO       NUMBER(10),
  NR_SEQ_PRIORIDADE        NUMBER(10),
  CD_PROCEDIMENTO_ENVIO    VARCHAR2(255 BYTE),
  NR_SEQ_GESTAO_VAGA       NUMBER(10),
  QT_SOLICITADO            NUMBER(10),
  IE_INTEGRACAO            VARCHAR2(1 BYTE),
  CD_MATERIAL_ENVIO        VARCHAR2(255 BYTE),
  CD_MATERIAL              NUMBER(6),
  NR_SEQ_RECEITA_AMB       NUMBER(10),
  NR_SEQ_RECEITA_ITEM      NUMBER(10),
  NR_SEQ_HOME_CARE         NUMBER(10),
  NR_SEQ_EQUIP_HOME        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGATEN_ATENEN_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_ENCAMINHAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_ATEPACI_FK_I ON TASY.REGULACAO_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_CPOEMAT_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_CPOEPRO_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_CPOE_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_ESPMEDI_FK_I ON TASY.REGULACAO_ATEND
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_EVOPACI_FK_I ON TASY.REGULACAO_ATEND
(CD_EVOLUCAO_ORI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_EXALABO_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_FAREFAI_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_RECEITA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_FAREFAR_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_RECEITA_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_GESVAGA_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_GESTAO_VAGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_GRUREGU_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_GRUPO_REGULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_HCPACEQ_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_EQUIP_HOME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_MANEQUI_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_MATEXLA_FK_I ON TASY.REGULACAO_ATEND
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_MEDEXPA_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_EXAME_CAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PACHOCA_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_HOME_CARE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PARMEDI_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_RESP_PARECER_ORI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PARMERE_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_PARECER_ORI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PEDEXEI_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_PEDIDO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PEDEXEX_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_PEDIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PESFISI_FK_I ON TASY.REGULACAO_ATEND
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PESFISI_FK2_I ON TASY.REGULACAO_ATEND
(CD_PROFISSIONAL_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGATEN_PK ON TASY.REGULACAO_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PROCEDI_FK_I ON TASY.REGULACAO_ATEND
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_PROINTE_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGATEN_REQITE_FK_I ON TASY.REGULACAO_ATEND
(NR_SEQ_REQUISICAO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGULACAO_ATEND ADD (
  CONSTRAINT REGATEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGULACAO_ATEND ADD (
  CONSTRAINT REGATEN_CPOEMAT_FK 
 FOREIGN KEY (NR_SEQ_CPOE) 
 REFERENCES TASY.CPOE_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT REGATEN_REQITE_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO_ITEM) 
 REFERENCES TASY.REQUISICAO_ITEM (NR_SEQUENCIA),
  CONSTRAINT REGATEN_GESVAGA_FK 
 FOREIGN KEY (NR_SEQ_GESTAO_VAGA) 
 REFERENCES TASY.GESTAO_VAGA (NR_SEQUENCIA),
  CONSTRAINT REGATEN_FAREFAI_FK 
 FOREIGN KEY (NR_SEQ_RECEITA_ITEM) 
 REFERENCES TASY.FA_RECEITA_FARMACIA_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGATEN_FAREFAR_FK 
 FOREIGN KEY (NR_SEQ_RECEITA_AMB) 
 REFERENCES TASY.FA_RECEITA_FARMACIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGATEN_PACHOCA_FK 
 FOREIGN KEY (NR_SEQ_HOME_CARE) 
 REFERENCES TASY.PACIENTE_HOME_CARE (NR_SEQUENCIA),
  CONSTRAINT REGATEN_HCPACEQ_FK 
 FOREIGN KEY (NR_SEQ_EQUIP_HOME) 
 REFERENCES TASY.HC_PAC_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_ATENEN_FK 
 FOREIGN KEY (NR_SEQ_ENCAMINHAMENTO) 
 REFERENCES TASY.ATEND_ENCAMINHAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT REGATEN_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT REGATEN_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO_ORI) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT REGATEN_GRUREGU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REGULACAO) 
 REFERENCES TASY.GRUPO_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_PARMEDI_FK 
 FOREIGN KEY (NR_SEQ_RESP_PARECER_ORI) 
 REFERENCES TASY.PARECER_MEDICO (NR_SEQ_INTERNO),
  CONSTRAINT REGATEN_PARMERE_FK 
 FOREIGN KEY (NR_SEQ_PARECER_ORI) 
 REFERENCES TASY.PARECER_MEDICO_REQ (NR_PARECER),
  CONSTRAINT REGATEN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGATEN_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL_DEST) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGATEN_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_CPOE_PROC) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT REGATEN_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT REGATEN_MEDEXPA_FK 
 FOREIGN KEY (NR_SEQ_EXAME_CAD) 
 REFERENCES TASY.MED_EXAME_PADRAO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_PEDEXEI_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO_ITEM) 
 REFERENCES TASY.PEDIDO_EXAME_EXTERNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT REGATEN_PEDEXEX_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO) 
 REFERENCES TASY.PEDIDO_EXAME_EXTERNO (NR_SEQUENCIA),
  CONSTRAINT REGATEN_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.REGULACAO_ATEND TO NIVEL_1;

