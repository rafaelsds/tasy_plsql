ALTER TABLE TASY.ATRIBUTO_COMPL_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATRIBUTO_COMPL_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATRIBUTO_COMPL_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_TIPO_LOTE_CONTAB  NUMBER(10)               NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EXP_DESCRICAO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ATRCOCO_PK ON TASY.ATRIBUTO_COMPL_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATRCOCO_TIPLOCO_FK_I ON TASY.ATRIBUTO_COMPL_HIST
(CD_TIPO_LOTE_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATRIBUTO_COMPL_HIST ADD (
  CONSTRAINT ATRCOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATRIBUTO_COMPL_HIST ADD (
  CONSTRAINT ATRCOCO_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTAB) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ATRIBUTO_COMPL_HIST TO NIVEL_1;

