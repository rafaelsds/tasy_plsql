ALTER TABLE TASY.REGRA_UTILIZACAO_CID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_UTILIZACAO_CID CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_UTILIZACAO_CID
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_DOENCA              VARCHAR2(10 BYTE)      NOT NULL,
  IE_SEXO                VARCHAR2(1 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_PERMITE_PRINCIPAL   VARCHAR2(1 BYTE),
  IE_PERMITE_BASICA      VARCHAR2(1 BYTE),
  QT_IDADE_MINIMA        NUMBER(10),
  QT_IDADE_MAXIMA        NUMBER(10),
  IE_PERMITE_FETAL       VARCHAR2(1 BYTE),
  IE_PERMITE_OBITO       VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  QT_IDADE_MIN_DIAS      NUMBER(6,2),
  QT_IDADE_MAX_DIAS      NUMBER(6,2),
  IE_RUBRICA_TYPE        VARCHAR2(1 BYTE),
  IE_PERMITE_ANOMALIA    VARCHAR2(1 BYTE)       DEFAULT null,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  NM_USUARIO_LIB         VARCHAR2(15 BYTE),
  IE_PERMITE_SELECIONAR  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.REGUTC_CIDDOEN_FK_I ON TASY.REGRA_UTILIZACAO_CID
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.REGUTC_PK ON TASY.REGRA_UTILIZACAO_CID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_UTILIZACAO_CID_tp  after update ON TASY.REGRA_UTILIZACAO_CID FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'REGRA_UTILIZACAO_CID',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'REGRA_UTILIZACAO_CID',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_UTILIZACAO_CID ADD (
  CONSTRAINT REGUTC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGRA_UTILIZACAO_CID ADD (
  CONSTRAINT REGUTC_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.REGRA_UTILIZACAO_CID TO NIVEL_1;

