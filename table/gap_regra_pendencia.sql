ALTER TABLE TASY.GAP_REGRA_PENDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GAP_REGRA_PENDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GAP_REGRA_PENDENCIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  IE_TIPO_PENDENCIA    VARCHAR2(5 BYTE),
  IE_AGRUPAR           VARCHAR2(5 BYTE),
  QT_DIAS_ANTECED      NUMBER(3),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GAP_REG_PE_ESTABEL_FK_I ON TASY.GAP_REGRA_PENDENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GAP_REG_PE_PK ON TASY.GAP_REGRA_PENDENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GAP_REGRA_PENDENCIA ADD (
  CONSTRAINT GAP_REG_PE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GAP_REGRA_PENDENCIA ADD (
  CONSTRAINT GAP_REG_PE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.GAP_REGRA_PENDENCIA TO NIVEL_1;

