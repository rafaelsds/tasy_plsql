ALTER TABLE TASY.ATEND_PERDA_GANHO_INF_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PERDA_GANHO_INF_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PERDA_GANHO_INF_ADIC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_PG       NUMBER(10)              NOT NULL,
  NR_SEQ_INF_ADIC       NUMBER(10)              NOT NULL,
  NR_SEQ_INF_ADIC_ITEM  NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPEGAI_ATEPEGA_FK_I ON TASY.ATEND_PERDA_GANHO_INF_ADIC
(NR_SEQ_ATEND_PG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPEGAI_PEGAINA_FK_I ON TASY.ATEND_PERDA_GANHO_INF_ADIC
(NR_SEQ_INF_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPEGAI_PEGAINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATPEGAI_PEGAINI_FK_I ON TASY.ATEND_PERDA_GANHO_INF_ADIC
(NR_SEQ_INF_ADIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPEGAI_PEGAINI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATPEGAI_PK ON TASY.ATEND_PERDA_GANHO_INF_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATEND_PERDA_GANHO_INF_ADIC ADD (
  CONSTRAINT ATPEGAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PERDA_GANHO_INF_ADIC ADD (
  CONSTRAINT ATPEGAI_ATEPEGA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PG) 
 REFERENCES TASY.ATENDIMENTO_PERDA_GANHO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATPEGAI_PEGAINA_FK 
 FOREIGN KEY (NR_SEQ_INF_ADIC) 
 REFERENCES TASY.PERDA_GANHO_INF_ADIC (NR_SEQUENCIA),
  CONSTRAINT ATPEGAI_PEGAINI_FK 
 FOREIGN KEY (NR_SEQ_INF_ADIC_ITEM) 
 REFERENCES TASY.PERDA_GANHO_INF_ADIC_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_PERDA_GANHO_INF_ADIC TO NIVEL_1;

