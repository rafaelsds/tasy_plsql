ALTER TABLE TASY.FIS_EFD_CAD_REGISTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_CAD_REGISTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_CAD_REGISTRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_REGISTRO          VARCHAR2(15 BYTE)        NOT NULL,
  DS_REGISTRO          VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_APRES         NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REG_SUPERIOR  NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO              VARCHAR2(15 BYTE)        NOT NULL,
  IE_ARQUIVO           VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EFDCADRE_EFDCADRE_FK_I ON TASY.FIS_EFD_CAD_REGISTRO
(NR_SEQ_REG_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EFDCADRE_PK ON TASY.FIS_EFD_CAD_REGISTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_EFD_CAD_REGISTRO ADD (
  CONSTRAINT EFDCADRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_EFD_CAD_REGISTRO ADD (
  CONSTRAINT EFDCADRE_EFDCADRE_FK 
 FOREIGN KEY (NR_SEQ_REG_SUPERIOR) 
 REFERENCES TASY.FIS_EFD_CAD_REGISTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_EFD_CAD_REGISTRO TO NIVEL_1;

