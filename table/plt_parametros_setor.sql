ALTER TABLE TASY.PLT_PARAMETROS_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLT_PARAMETROS_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLT_PARAMETROS_SETOR
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO     NUMBER(5)            NOT NULL,
  IE_SOLUCAO_ROTINA        VARCHAR2(5 BYTE),
  IE_PERMITE_ESTENDER      VARCHAR2(1 BYTE),
  QT_DIAS_ESTENDER         NUMBER(5),
  QT_HORAS_ESTENDER        NUMBER(5),
  QT_HORAS_ATRASO          NUMBER(5),
  IE_MEDICAMENTO_ROTINA    VARCHAR2(5 BYTE),
  IE_QUESTIONA_EXT         VARCHAR2(1 BYTE),
  IE_CONSIDERAR_SETOR_PAC  VARCHAR2(1 BYTE),
  DS_CODIGO_PROT_SOL       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLTPASE_PK ON TASY.PLT_PARAMETROS_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLTPASE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLTPASE_SETATEN_FK_I ON TASY.PLT_PARAMETROS_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLT_PARAMETROS_SETOR ADD (
  CONSTRAINT PLTPASE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLT_PARAMETROS_SETOR ADD (
  CONSTRAINT PLTPASE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PLT_PARAMETROS_SETOR TO NIVEL_1;

