ALTER TABLE TASY.CADASTRO_PROBLEMA_APRES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CADASTRO_PROBLEMA_APRES CASCADE CONSTRAINTS;

CREATE TABLE TASY.CADASTRO_PROBLEMA_APRES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_INFORMACAO        VARCHAR2(5 BYTE)         NOT NULL,
  NR_SEQ_APRESENTACAO  NUMBER(10)               NOT NULL,
  NR_SEQ_CAD_PROBLEMA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAPRAPR_CAPRARV_FK_I ON TASY.CADASTRO_PROBLEMA_APRES
(NR_SEQ_CAD_PROBLEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAPRAPR_PK ON TASY.CADASTRO_PROBLEMA_APRES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CADASTRO_PROBLEMA_APRES ADD (
  CONSTRAINT CAPRAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CADASTRO_PROBLEMA_APRES ADD (
  CONSTRAINT CAPRAPR_CAPRARV_FK 
 FOREIGN KEY (NR_SEQ_CAD_PROBLEMA) 
 REFERENCES TASY.CADASTRO_PROBLEMA_ARVORE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CADASTRO_PROBLEMA_APRES TO NIVEL_1;

