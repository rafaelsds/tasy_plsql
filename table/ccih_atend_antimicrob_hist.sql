ALTER TABLE TASY.CCIH_ATEND_ANTIMICROB_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CCIH_ATEND_ANTIMICROB_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.CCIH_ATEND_ANTIMICROB_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ANTIMICROB    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_HISTORICO         VARCHAR2(4000 BYTE)      NOT NULL,
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CCIHAAH_CCIHAAM_FK_I ON TASY.CCIH_ATEND_ANTIMICROB_HIST
(NR_SEQ_ANTIMICROB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CCIHAAH_CCIHAAM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CCIHAAH_PK ON TASY.CCIH_ATEND_ANTIMICROB_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CCIHAAH_PK
  MONITORING USAGE;


ALTER TABLE TASY.CCIH_ATEND_ANTIMICROB_HIST ADD (
  CONSTRAINT CCIHAAH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CCIH_ATEND_ANTIMICROB_HIST ADD (
  CONSTRAINT CCIHAAH_CCIHAAM_FK 
 FOREIGN KEY (NR_SEQ_ANTIMICROB) 
 REFERENCES TASY.CCIH_ATEND_ANTIMICROBIANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CCIH_ATEND_ANTIMICROB_HIST TO NIVEL_1;

