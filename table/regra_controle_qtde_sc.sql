ALTER TABLE TASY.REGRA_CONTROLE_QTDE_SC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CONTROLE_QTDE_SC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CONTROLE_QTDE_SC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_LOCAL_ESTOQUE       NUMBER(4),
  CD_CENTRO_CUSTO        NUMBER(8),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  IE_TIPO_SERVICO        VARCHAR2(15 BYTE),
  IE_TIPO_SOLICITACAO    VARCHAR2(15 BYTE),
  NR_SEQ_FORMA_COMPRA    NUMBER(10),
  IE_URGENTE             VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_MOTIVO_URGENTE  NUMBER(10),
  CD_GRUPO_MATERIAL      NUMBER(3),
  CD_SUBGRUPO_MATERIAL   NUMBER(3),
  CD_CLASSE_MATERIAL     NUMBER(5),
  CD_MATERIAL            NUMBER(10),
  IE_PERIODICIDADE       VARCHAR2(1 BYTE),
  IE_DIA_SEMANA          VARCHAR2(15 BYTE),
  IE_MES                 VARCHAR2(15 BYTE),
  QT_MAXIMO              NUMBER(13,4)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECQTS_CENCUST_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_CLAMATE_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_ESTABEL_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_GRUMATE_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_LOCESTO_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_MATERIA_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_MOURGCO_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(NR_SEQ_MOTIVO_URGENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RECQTS_PK ON TASY.REGRA_CONTROLE_QTDE_SC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_RELIFOC_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(NR_SEQ_FORMA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_SETATEN_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECQTS_SUBMATE_FK_I ON TASY.REGRA_CONTROLE_QTDE_SC
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_CONTROLE_QTDE_SC_ATUAL
before insert or update ON TASY.REGRA_CONTROLE_QTDE_SC for each row
declare

begin

if	(:new.IE_PERIODICIDADE = 'D') and
	(:new.IE_MES is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(687604); /*Quando a periodicidade � Di�ria, n�o � permitido informar o m�s.*/
end if;

if	(:new.IE_PERIODICIDADE = 'M') and
	(:new.IE_DIA_SEMANA is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(687613); /*Quando a periodicidade � Mensal, n�o � permitido informar o dia da semana.*/
end if;

end;
/


ALTER TABLE TASY.REGRA_CONTROLE_QTDE_SC ADD (
  CONSTRAINT RECQTS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CONTROLE_QTDE_SC ADD (
  CONSTRAINT RECQTS_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT RECQTS_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT RECQTS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RECQTS_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT RECQTS_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT RECQTS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT RECQTS_MOURGCO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_URGENTE) 
 REFERENCES TASY.MOTIVO_URGENCIA_COMPRAS (NR_SEQUENCIA),
  CONSTRAINT RECQTS_RELIFOC_FK 
 FOREIGN KEY (NR_SEQ_FORMA_COMPRA) 
 REFERENCES TASY.REG_LIC_FORMA_COMPRA (NR_SEQUENCIA),
  CONSTRAINT RECQTS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT RECQTS_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.REGRA_CONTROLE_QTDE_SC TO NIVEL_1;

