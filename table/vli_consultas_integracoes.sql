ALTER TABLE TASY.VLI_CONSULTAS_INTEGRACOES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VLI_CONSULTAS_INTEGRACOES CASCADE CONSTRAINTS;

CREATE TABLE TASY.VLI_CONSULTAS_INTEGRACOES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_CONSULTA          VARCHAR2(50 BYTE),
  DS_CONSULTA          VARCHAR2(4000 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  DT_ATUALIZACAO_NREC  DATE,
  CD_INTEGRACAO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.VLICONINT_SEQINT_PK ON TASY.VLI_CONSULTAS_INTEGRACOES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VLICONINT_VALIDINT_FK_I ON TASY.VLI_CONSULTAS_INTEGRACOES
(CD_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.VLI_CONSULTAS_INTEGRACOES ADD (
  CONSTRAINT VLICONINT_SEQINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VLI_CONSULTAS_INTEGRACOES ADD (
  CONSTRAINT VLICONINT_VALIDINT_FK 
 FOREIGN KEY (CD_INTEGRACAO) 
 REFERENCES TASY.VALIDADOR_INTEGRACOES (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.VLI_CONSULTAS_INTEGRACOES TO NIVEL_1;

