ALTER TABLE TASY.CONVENIO_RETORNO_MOVTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RETORNO_MOVTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RETORNO_MOVTO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_DOC_CONVENIO        VARCHAR2(255 BYTE),
  CD_AUTORIZACAO         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_PRONTUARIO          NUMBER(10),
  CD_PRESTADOR           NUMBER(14),
  CD_EXECUTOR_CONVENIO   VARCHAR2(15 BYTE),
  CD_RECEBEDOR_CONVENIO  VARCHAR2(15 BYTE),
  CD_USUARIO_CONVENIO    VARCHAR2(255 BYTE),
  TP_PACIENTE            VARCHAR2(1 BYTE),
  DS_ARQUIVO_ENVIO       VARCHAR2(255 BYTE),
  TP_ITEM                NUMBER(1),
  CD_ITEM                NUMBER(15),
  IE_VIA_ACESSO          NUMBER(1),
  IE_FUNCAO_MEDICO       VARCHAR2(10 BYTE),
  DT_EXECUCAO            DATE,
  HR_EXECUCAO            NUMBER(4),
  VL_COBRADO             NUMBER(17,4),
  VL_PAGO                NUMBER(17,4),
  QT_COBRADA             NUMBER(9,3),
  QT_PAGA                NUMBER(9,3),
  IE_SITUACAO_ITEM       VARCHAR2(1 BYTE),
  CD_MOTIVO              VARCHAR2(40 BYTE),
  DT_MESANO_PAGAMENTO    DATE,
  NR_SEQ_RETORNO         NUMBER(10),
  NR_SEQ_RET_ITEM        NUMBER(10),
  IE_DOCUMENTO           VARCHAR2(10 BYTE),
  DS_COMPLEMENTO         VARCHAR2(255 BYTE),
  NR_CONTA               NUMBER(10),
  VL_TOTAL_PAGO          NUMBER(17,4),
  QT_FILME               NUMBER(7,5),
  VL_FILME               NUMBER(17,4),
  QT_PONTO               NUMBER(5),
  VL_PONTO               NUMBER(5,3),
  IE_GERA_RESUMO         VARCHAR2(1 BYTE),
  VL_PAGO_ANT            NUMBER(17,4),
  DT_MESANO_ANT          DATE,
  PR_PAGO                NUMBER(5,2),
  NR_SEQ_RET_GLOSA       NUMBER(10),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  IE_TIPO_GUIA           VARCHAR2(2 BYTE),
  CD_TIPO_REGISTRO       NUMBER(10),
  VL_GLOSA               NUMBER(17,4),
  NM_USUARIO_GLOSA       VARCHAR2(15 BYTE),
  DS_ITEM_RETORNO        VARCHAR2(240 BYTE),
  ID_GLOSA               VARCHAR2(100 BYTE),
  NR_CONTA_CONVENIO      VARCHAR2(255 BYTE),
  NR_SEQ_ITEM_CONTA      NUMBER(10),
  NR_ATENDIMENTO         NUMBER(10),
  NR_PROTOCOLO           VARCHAR2(255 BYTE),
  NR_NOTA_FISCAL         VARCHAR2(255 BYTE),
  CD_MOTIVO_GLOSA_TISS   VARCHAR2(10 BYTE),
  NR_SEQUENCIA_ITEM      NUMBER(4)              DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREMO_CONREIT_FK_I ON TASY.CONVENIO_RETORNO_MOVTO
(NR_SEQ_RET_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREMO_CONRETO_FK_I ON TASY.CONVENIO_RETORNO_MOVTO
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREMO_CONRETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREMO_CORETGL_FK_I ON TASY.CONVENIO_RETORNO_MOVTO
(NR_SEQ_RET_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREMO_I1 ON TASY.CONVENIO_RETORNO_MOVTO
(NR_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREMO_I1
  MONITORING USAGE;


CREATE INDEX TASY.CONREMO_I2 ON TASY.CONVENIO_RETORNO_MOVTO
(NR_SEQ_RETORNO, NR_SEQ_RET_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONREMO_PK ON TASY.CONVENIO_RETORNO_MOVTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREMO_SETATEN_FK_I ON TASY.CONVENIO_RETORNO_MOVTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREMO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.convenio_retorno_movto_insert
BEFORE INSERT ON TASY.CONVENIO_RETORNO_MOVTO FOR EACH ROW
Declare
ie_existe_w		number(10);
cd_motivo_glosa_w	number(5);
cd_convenio_w		number(5);

BEGIN

if	(:new.cd_motivo is not null) then

	select	cd_convenio
	into	cd_convenio_w
	from	convenio_retorno
	where	nr_sequencia	= :new.nr_seq_retorno;

	select	nvl(max(cd_motivo_glosa),0)
	into	cd_motivo_glosa_w
	from	convenio_motivo_glosa
	where	cd_convenio		= cd_convenio_w
	and	cd_glosa_convenio		= :new.cd_motivo;

	if	(cd_motivo_glosa_w <> 0) then
		:new.cd_motivo	:= cd_motivo_glosa_w;
	elsif	(cd_motivo_glosa_w = 0) then

		select	count(*)
		into	ie_existe_w
		from	motivo_glosa
		where	to_char(cd_motivo_glosa) = :new.cd_motivo;

		if	(ie_existe_w = 0) then
			/*O motivo de glosa informado n�o existe na tabela Motivo Glosa!
			C�digo: #@CD_MOTIVO#@*/
			wheb_mensagem_pck.exibir_mensagem_abort(266934, 'CD_MOTIVO=' || :new.cd_motivo);
		end if;
	end if;
end if;

if ((:new.cd_item is not null) and ( :new.ds_item_retorno is null)) then

	select SUBSTR(obter_desc_item_conta(NVL (:new.nr_conta, TO_NUMBER(SUBSTR (obter_dados_ret_convenio					(:new.nr_seq_ret_item,1),1,10))), :new.cd_item), 1, 240)
	into	:new.ds_item_retorno
	from 	dual;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.convenio_retorno_movto_update
BEFORE UPDATE ON TASY.CONVENIO_RETORNO_MOVTO FOR EACH ROW
Declare
ie_existe_w		number(10);
cd_motivo_glosa_w	number(5);
cd_convenio_w		number(5);

BEGIN

if	(Nvl(:new.cd_motivo,'0') <> '0') and
	(Nvl(:new.cd_motivo,'0') <> Nvl(:old.cd_motivo,'0')) then

	select	cd_convenio
	into	cd_convenio_w
	from	convenio_retorno
	where	nr_sequencia	= :new.nr_seq_retorno;

	select	nvl(max(cd_motivo_glosa),0)
	into	cd_motivo_glosa_w
	from	convenio_motivo_glosa
	where	cd_convenio		= cd_convenio_w
	and	cd_glosa_convenio		= :new.cd_motivo;

	if	(cd_motivo_glosa_w <> 0) then
		:new.cd_motivo	:= cd_motivo_glosa_w;
	elsif	(cd_motivo_glosa_w = 0) then
		select	count(*)
		into	ie_existe_w
		from	motivo_glosa
		where	to_char(cd_motivo_glosa) = :new.cd_motivo;

		if	(ie_existe_w = 0) then
			/*O motivo de glosa informado n�o existe na tabela Motivo Glosa!
			C�digo: #@CD_MOTIVO#@*/
			wheb_mensagem_pck.exibir_mensagem_abort(266934, 'CD_MOTIVO=' || :new.cd_motivo);
		end if;
	end if;
end if;


END;
/


ALTER TABLE TASY.CONVENIO_RETORNO_MOVTO ADD (
  CONSTRAINT CONREMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RETORNO_MOVTO ADD (
  CONSTRAINT CONREMO_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA),
  CONSTRAINT CONREMO_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_RET_ITEM) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT CONREMO_CORETGL_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT CONREMO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CONVENIO_RETORNO_MOVTO TO NIVEL_1;

