ALTER TABLE TASY.MEDIDA_EXAME_REF_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIDA_EXAME_REF_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIDA_EXAME_REF_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MED_REFER     NUMBER(10)               NOT NULL,
  NR_SEQ_MED_ADIC      NUMBER(10)               NOT NULL,
  VL_MINIMO            NUMBER(15,4),
  VL_MAXIMO            NUMBER(15,4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDEXAD_MEDEXLA_FK_I ON TASY.MEDIDA_EXAME_REF_ADIC
(NR_SEQ_MED_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDEXAD_MEDEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDEXAD_MEDEXRE_FK_I ON TASY.MEDIDA_EXAME_REF_ADIC
(NR_SEQ_MED_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDEXAD_MEDEXRE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDEXAD_PK ON TASY.MEDIDA_EXAME_REF_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDEXAD_PK
  MONITORING USAGE;


ALTER TABLE TASY.MEDIDA_EXAME_REF_ADIC ADD (
  CONSTRAINT MEDEXAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDIDA_EXAME_REF_ADIC ADD (
  CONSTRAINT MEDEXAD_MEDEXRE_FK 
 FOREIGN KEY (NR_SEQ_MED_REFER) 
 REFERENCES TASY.MEDIDA_EXAME_REFER (NR_SEQUENCIA),
  CONSTRAINT MEDEXAD_MEDEXLA_FK 
 FOREIGN KEY (NR_SEQ_MED_ADIC) 
 REFERENCES TASY.MEDIDA_EXAME_LAUDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MEDIDA_EXAME_REF_ADIC TO NIVEL_1;

