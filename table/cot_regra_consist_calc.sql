ALTER TABLE TASY.COT_REGRA_CONSIST_CALC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_REGRA_CONSIST_CALC CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_REGRA_CONSIST_CALC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_BLOQUEIA          VARCHAR2(1 BYTE)         NOT NULL,
  VL_PARAMETRO         VARCHAR2(20 BYTE),
  IE_TIPO_REGRA        NUMBER(3)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_TAB_PRECO_MAT     NUMBER(4),
  IE_PFB_PMC           VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CORECOC_ESTABEL_FK_I ON TASY.COT_REGRA_CONSIST_CALC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CORECOC_PK ON TASY.COT_REGRA_CONSIST_CALC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORECOC_PK
  MONITORING USAGE;


CREATE INDEX TASY.CORECOC_TABPRMA_FK_I ON TASY.COT_REGRA_CONSIST_CALC
(CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORECOC_TABPRMA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.COT_REGRA_CONSIST_CALC_ATUAL
before insert or update ON TASY.COT_REGRA_CONSIST_CALC for each row
declare
begin
if	(:new.IE_TIPO_REGRA = '11') and
	(:new.CD_TAB_PRECO_MAT is null) then
	--R.aise_application_error(-20011,'Quando usar essa regra o sistema exige a informa��o da Tabela de pre�os.');
	wheb_mensagem_pck.Exibir_Mensagem_Abort(251652);
end if;

if	(:new.IE_TIPO_REGRA in ('1','8','9','15')) and
	(:new.vl_parametro is null) then
	--R.aise_application_error(-20011,'Quando usar essa regra o sistema exige a informa��o do Valor par�metro.');
	wheb_mensagem_pck.Exibir_Mensagem_Abort(251653);
end if;

if	(:new.IE_TIPO_REGRA not in ('1','8','9','15')) and
	(:new.vl_parametro is not null) then
	--R.aise_application_error(-20011,'Quando usar essa regra, n�o deve preencher o campo Valor par�metro.');
	wheb_mensagem_pck.Exibir_Mensagem_Abort(251654);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.COT_REGRA_CONSIST_CALC_tp  after update ON TASY.COT_REGRA_CONSIST_CALC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BLOQUEIA,1,4000),substr(:new.IE_BLOQUEIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_BLOQUEIA',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PFB_PMC,1,4000),substr(:new.IE_PFB_PMC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PFB_PMC',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_REGRA,1,4000),substr(:new.IE_TIPO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REGRA',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TAB_PRECO_MAT,1,4000),substr(:new.CD_TAB_PRECO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_TAB_PRECO_MAT',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'COT_REGRA_CONSIST_CALC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.COT_REGRA_CONSIST_CALC ADD (
  CONSTRAINT CORECOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_REGRA_CONSIST_CALC ADD (
  CONSTRAINT CORECOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CORECOC_TABPRMA_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT) 
 REFERENCES TASY.TABELA_PRECO_MATERIAL (CD_ESTABELECIMENTO,CD_TAB_PRECO_MAT));

GRANT SELECT ON TASY.COT_REGRA_CONSIST_CALC TO NIVEL_1;

