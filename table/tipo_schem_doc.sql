ALTER TABLE TASY.TIPO_SCHEM_DOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_SCHEM_DOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_SCHEM_DOC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_TIPO_SCHEMATIC  NUMBER(10),
  DS_TITULO              VARCHAR2(255 BYTE)     NOT NULL,
  DS_TITULO_ENG          VARCHAR2(255 BYTE),
  DS_PALAVRA_CHAVE       VARCHAR2(2000 BYTE),
  DS_DOCUMENTACAO        LONG,
  IE_GUIA_DEV            VARCHAR2(1 BYTE),
  IE_TIPO_DOCUMENTACAO   VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TPSCHDOC_PK ON TASY.TIPO_SCHEM_DOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TPSCHDOC_TPSCHE_FK_I ON TASY.TIPO_SCHEM_DOC
(NR_SEQ_TIPO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SCHEM_DOC_INSERT
after insert or update ON TASY.TIPO_SCHEM_DOC for each row
declare

ds_title_w				varchar2(2000);
ie_tipo_documentacao_w	varchar2(15);
ds_mail_origin_w		varchar2(255);
ds_mail_destiny_w		varchar2(255);
ds_tipo_documentacao_w	varchar2(255);
ds_usuario_w			varchar2(255);
ds_content_w			varchar2(4000);

Cursor c01 is
	select	a.nm_usuario_regra,
			c.ds_email
	from	usuario c,
			REGRA_ALERTA_SCH_DOC_TIPO b,
			REGRA_ALERTA_SCHEM_DOC a
	where	a.nr_sequencia = b.nr_seq_regra_alerta
	and		a.nm_usuario_regra = c.nm_usuario
	and		b.ie_tipo_documentacao = ie_tipo_documentacao_w
	and		a.ie_situacao = 'A'
	and		c.ds_email is not null;

begin

if	(:new.ds_titulo is not null) and
	(:new.dt_liberacao is not null and :old.dt_liberacao is null) then
	ds_tipo_documentacao_w	:= 	obter_valor_dominio(8334,:new.ie_tipo_documentacao);
	ds_usuario_w			:= obter_nome_usuario(:new.nm_usuario);

	ds_title_w	:= 'New schematic documentation - ' || to_char(sysdate,'dd/mm/yyyy');
	ds_content_w	:= 'A new documentation was added inside Schematic Documentation function.' || chr(13) || chr(10) ||
						'Title: ' || nvl(:new.ds_titulo_eng,:new.ds_titulo) || chr(13) || chr(10) ||
						'Included by: ' || ds_usuario_w || chr(13) || chr(10) ||
						'Type: ' || ds_tipo_documentacao_w || chr(13) || chr(10) || chr(13) || chr(10) ||
						'Please check more details on the function.';


	select	max(ds_email)
	into	ds_mail_origin_w
	from	usuario a
	where	a.nm_usuario = 'avisoemail';

	if	(ds_mail_origin_w is not null) then
		ie_tipo_documentacao_w	:= :new.ie_tipo_documentacao;

		for r_c01 in c01 loop
			enviar_email(ds_title_w,
						ds_content_w,
						ds_mail_origin_w,
						r_c01.ds_email,
						'avisoemail',
						'B',
						null);
		end loop;
	end if;
end if;

end;
/


ALTER TABLE TASY.TIPO_SCHEM_DOC ADD (
  CONSTRAINT TPSCHDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_SCHEM_DOC ADD (
  CONSTRAINT TPSCHDOC_TPSCHE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_SCHEMATIC) 
 REFERENCES TASY.TIPO_SCHEMATIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_SCHEM_DOC TO NIVEL_1;

