ALTER TABLE TASY.AUT_DC_IMP_REG_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUT_DC_IMP_REG_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUT_DC_IMP_REG_VALOR
(
  NM_TABELA        VARCHAR2(30 BYTE)            NOT NULL,
  NM_ATRIBUTO      VARCHAR2(50 BYTE)            NOT NULL,
  NR_SEQ_REGISTRO  NUMBER(15)                   NOT NULL,
  NR_SEQ_IMPORT    NUMBER(15)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADCIRVA_ADCIMP_FK_I ON TASY.AUT_DC_IMP_REG_VALOR
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADCIRVA_PK ON TASY.AUT_DC_IMP_REG_VALOR
(NM_TABELA, NM_ATRIBUTO, NR_SEQ_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AUT_DC_IMP_REG_VALOR ADD (
  CONSTRAINT ADCIRVA_PK
 PRIMARY KEY
 (NM_TABELA, NM_ATRIBUTO, NR_SEQ_REGISTRO));

ALTER TABLE TASY.AUT_DC_IMP_REG_VALOR ADD (
  CONSTRAINT ADCIRVA_ADCIMP_FK 
 FOREIGN KEY (NR_SEQ_IMPORT) 
 REFERENCES TASY.AUT_DC_IMPORT (NR_SEQUENCIA));

GRANT SELECT ON TASY.AUT_DC_IMP_REG_VALOR TO NIVEL_1;

