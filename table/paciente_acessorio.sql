ALTER TABLE TASY.PACIENTE_ACESSORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ACESSORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ACESSORIO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ACESSORIO           NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DT_INATIVACAO              DATE,
  NR_SEQ_CARACTERISTICA      NUMBER(10),
  NR_SEQ_LOCALIZACAO         NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  IE_NEGA_ACESSORIO          VARCHAR2(1 BYTE),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NM_USUARIO_TERMINO         VARCHAR2(15 BYTE),
  DS_JUST_TERMINO            VARCHAR2(255 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  DS_GERADOR                 VARCHAR2(80 BYTE),
  IE_RESSONANCIA_MAGNE       VARCHAR2(1 BYTE),
  DT_IMPLANTE                DATE,
  DT_INSPECAO                DATE,
  DS_ESTABELECIMENTO         VARCHAR2(255 BYTE),
  CD_MODO_RITMO              VARCHAR2(30 BYTE),
  IE_PROPOSITO               VARCHAR2(1 BYTE),
  CD_IMPLANTES_EXISTENTES    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACACES_ACECARA_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_CARACTERISTICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_ACECARA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_ACELOCA_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_ACELOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_ATCONSPEPA_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACACES_EHRREEL_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_HISTSAU_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACACES_I1 ON TASY.PACIENTE_ACESSORIO
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_I1
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_MORIMAR_FK_I ON TASY.PACIENTE_ACESSORIO
(CD_MODO_RITMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACACES_NISEALE_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_PESFISI_FK_I ON TASY.PACIENTE_ACESSORIO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACACES_PESFISI_FK2_I ON TASY.PACIENTE_ACESSORIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACACES_PK ON TASY.PACIENTE_ACESSORIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_TASASDI_FK_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACACES_TASASDI_FK2_I ON TASY.PACIENTE_ACESSORIO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACACES_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ACESSORIO_ATUAL
before update or insert ON TASY.PACIENTE_ACESSORIO for each row
declare
begin

if	(NVL(:new.ie_nega_acessorio,'N') = 'N') and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pac_acessorio_pend_atual 
after insert or update ON TASY.PACIENTE_ACESSORIO 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSAS'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSAS'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ACESSORIO_tp  after update ON TASY.PACIENTE_ACESSORIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_GERADOR,1,500);gravar_log_alteracao(substr(:old.DS_GERADOR,1,4000),substr(:new.DS_GERADOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_GERADOR',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_IMPLANTE,1,500);gravar_log_alteracao(to_char(:old.DT_IMPLANTE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_IMPLANTE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_IMPLANTE',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CARACTERISTICA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CARACTERISTICA,1,4000),substr(:new.NR_SEQ_CARACTERISTICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARACTERISTICA',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ACESSORIO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ACESSORIO,1,4000),substr(:new.NR_SEQ_ACESSORIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ACESSORIO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NEGA_ACESSORIO,1,500);gravar_log_alteracao(substr(:old.IE_NEGA_ACESSORIO,1,4000),substr(:new.IE_NEGA_ACESSORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGA_ACESSORIO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MODO_RITMO,1,500);gravar_log_alteracao(substr(:old.CD_MODO_RITMO,1,4000),substr(:new.CD_MODO_RITMO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MODO_RITMO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PROPOSITO,1,500);gravar_log_alteracao(substr(:old.IE_PROPOSITO,1,4000),substr(:new.IE_PROPOSITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROPOSITO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RESSONANCIA_MAGNE,1,500);gravar_log_alteracao(substr(:old.IE_RESSONANCIA_MAGNE,1,4000),substr(:new.IE_RESSONANCIA_MAGNE,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESSONANCIA_MAGNE',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_IMPLANTES_EXISTENTES,1,500);gravar_log_alteracao(substr(:old.CD_IMPLANTES_EXISTENTES,1,4000),substr(:new.CD_IMPLANTES_EXISTENTES,1,4000),:new.nm_usuario,nr_seq_w,'CD_IMPLANTES_EXISTENTES',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_LOCALIZACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_LOCALIZACAO,1,4000),substr(:new.NR_SEQ_LOCALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCALIZACAO',ie_log_w,ds_w,'PACIENTE_ACESSORIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_acessorio_afinsup
after insert or update ON TASY.PACIENTE_ACESSORIO for each row
declare

Cursor C01 is
	select	nr_sequencia
	from	accessory_pac_value_attrib
	where	nr_seq_accessory = :new.nr_seq_acessorio
	and	ie_situacao = 'A'
	order by nr_seq_display_order;

c01_w	c01%rowtype;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  and
	((inserting) or
	((updating) and (nvl(:old.nr_seq_acessorio,0)) <> nvl(:new.nr_seq_acessorio,0))) then

	if	(updating) then
		delete from result_value_pac_accessory
		where nr_seq_pac_acessory = :new.nr_sequencia;
	end if;

	if	(:new.nr_seq_acessorio > 0) then

		open c01;
		loop
		fetch C01 into
			C01_w;
		exit when C01%notfound;
			begin
			insert into result_value_pac_accessory(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pac_acessory,
				nr_seq_attribute)
			values(	result_value_pac_accessory_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.nr_sequencia,
				C01_w.nr_sequencia);
			end;
		end loop;
		close C01;
	end if;
end if;

end;
/


ALTER TABLE TASY.PACIENTE_ACESSORIO ADD (
  CONSTRAINT PACACES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ACESSORIO ADD (
  CONSTRAINT PACACES_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACACES_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACACES_MORIMAR_FK 
 FOREIGN KEY (CD_MODO_RITMO) 
 REFERENCES TASY.MODO_RITMO_MARCAPASSO (CD_MODO_RITMO),
  CONSTRAINT PACACES_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACACES_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACACES_ACELOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.ACESSORIO_LOCALIZACAO (NR_SEQUENCIA),
  CONSTRAINT PACACES_ACECARA_FK 
 FOREIGN KEY (NR_SEQ_CARACTERISTICA) 
 REFERENCES TASY.ACESSORIO_CARACTERISTICA (NR_SEQUENCIA),
  CONSTRAINT PACACES_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACACES_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACACES_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT PACACES_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_ACESSORIO TO NIVEL_1;

