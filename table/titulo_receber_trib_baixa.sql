ALTER TABLE TASY.TITULO_RECEBER_TRIB_BAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_TRIB_BAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_TRIB_BAIXA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  VL_BAIXA             NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_TIT_TRIB      NUMBER(10)               NOT NULL,
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_TIT_LIQ       NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10),
  DT_CONTABIL          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIRETRB_LOTCONT_FK_I ON TASY.TITULO_RECEBER_TRIB_BAIXA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIRETRB_PK ON TASY.TITULO_RECEBER_TRIB_BAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRETRB_TITRECE_FK_I ON TASY.TITULO_RECEBER_TRIB_BAIXA
(NR_TITULO, NR_SEQ_TIT_LIQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRETRB_TITRETR_FK_I ON TASY.TITULO_RECEBER_TRIB_BAIXA
(NR_SEQ_TIT_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRETRB_TITRETR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRETRB_TRAFINA_FK_I ON TASY.TITULO_RECEBER_TRIB_BAIXA
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_trib_baixa_afte
after insert or update ON TASY.TITULO_RECEBER_TRIB_BAIXA for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w			number(10);
cd_estabelecimento_w    	estabelecimento.cd_estabelecimento%type;
cd_estabelecimento_ww		estabelecimento.cd_estabelecimento%type;
dt_movimento_w          	ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w   	ctb_documento.nr_seq_trans_financ%type;
nr_documento_w          	ctb_documento.nr_documento%type;
nr_seq_doc_compl_w      	ctb_documento.nr_seq_doc_compl%type;
ie_lib_caixa_w			titulo_receber_liq.ie_lib_caixa%type;
ie_contab_tit_cancelado_w	parametro_contas_receber.ie_contab_tit_cancelado%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;

Cursor c01 is
	select 	a.cd_tipo_lote_contab,
		14 nr_seq_info_ctb,
		'TITULO_RECEBER_TRIB_BAIXA' nm_tabela,
		a.nm_atributo nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 39
	and	a.nm_atributo = 'VL_TRIB_TIT_REC'
	and	ie_concil_contab_w = 'S'
	and	exists ( select	1
			from 	titulo_receber_liq y
			where   y.nr_seq_lote_enc_contas is null
			and     y.nr_seq_pls_lote_camara is null
			and	nvl(y.ie_lib_caixa, 'S') = 'S'
			and	y.nr_titulo 	= :new.nr_titulo
			and	y.nr_sequencia	= :new.nr_seq_tit_liq
			and	exists	(select	1
					from	titulo_receber x
					where	x.nr_titulo	= y.nr_titulo
					and	x.ie_pls	= 'S'
					and	x.ie_origem_titulo = '3'
					and	((x.ie_situacao	<> '3' or ie_contab_tit_cancelado_w = 'S')
					or 	(x.ie_situacao = '3' and y.vl_rec_maior <> 0))));

	vet_c01 c01%rowtype;
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_seq_tit_liq is not null) and (:new.nr_titulo is not null) then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_seq_tit_liq)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p	:=	:new.nr_seq_tit_liq;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;

if	(inserting) then
	begin

	select  max(c.cd_estabelecimento)
	into 	cd_estabelecimento_w
	from    titulo_receber_trib_baixa a,
		titulo_receber_liq b,
		titulo_receber c
	where   a.nr_sequencia 	 = :new.nr_sequencia
	and     a.nr_titulo 	 = b.nr_titulo
	and     a.nr_seq_tit_liq = b.nr_sequencia
	and     b.nr_titulo 	 = c.nr_titulo;

	atualiza_tit_receb_trib_baixa(  :new.dt_contabil,
					cd_estabelecimento_w,
					:new.nr_seq_tit_trib,
					:new.vl_baixa,
					:new.nr_titulo,
					:new.nr_seq_trans_financ,
					:new.nr_sequencia,
					:new.nm_usuario);
	exception when others then
		cd_estabelecimento_w	:= null;
	end;

	begin
	select 	a.cd_estabelecimento,
		b.dt_recebimento,
		b.nr_seq_trans_fin,
		a.nr_titulo,
		b.nr_sequencia,
		b.ie_lib_caixa
	into	cd_estabelecimento_ww,
		dt_movimento_w,
		nr_seq_trans_financ_w,
		nr_documento_w,
		nr_seq_doc_compl_w,
		ie_lib_caixa_w
	from	titulo_receber a,
		titulo_receber_liq b,
		titulo_receber_trib_baixa c
	where	a.nr_titulo = b.nr_titulo
	and	b.nr_titulo = c.nr_titulo
	and	b.nr_sequencia = c.nr_seq_tit_liq
	and	c.nr_sequencia = :new.nr_sequencia;

	exception when others then
		nr_seq_trans_financ_w	:= null;
	end;

	begin
	select  nvl(max(ie_contab_tit_cancelado), 'S')
	into    ie_contab_tit_cancelado_w
	from    parametro_contas_receber
	where   cd_estabelecimento      = cd_estabelecimento_w;
	exception
	when others then
		ie_contab_tit_cancelado_w := 'S';
	end;

	begin
	select	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from	pls_visible_false
	where	cd_estabelecimento = cd_estabelecimento_ww;
	exception when others then
		ie_concil_contab_w := 'N';
	end;

	if      (nvl(:new.vl_baixa, 0) <> 0 and nvl(nr_seq_trans_financ_w,0) <> 0 and nvl(ie_lib_caixa_w, 'S') = 'S') then
		open c01;
		loop
		fetch c01 into
			vet_c01;
		exit when c01%notfound;
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_ww,
									dt_movimento_w,
									vet_c01.cd_tipo_lote_contab,
									nr_seq_trans_financ_w,
									vet_c01.nr_seq_info_ctb,
									nr_documento_w,
									nr_seq_doc_compl_w,
									:new.nr_sequencia,
									:new.vl_baixa,
									vet_c01.nm_tabela,
									vet_c01.nm_atributo,
									:new.nm_usuario);

		end;
		end loop;
		close c01;

	end if;
end if;

end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_TRIB_BAIXA ADD (
  CONSTRAINT TIRETRB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_TRIB_BAIXA ADD (
  CONSTRAINT TIRETRB_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TIRETRB_TITRECE_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_TIT_LIQ) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT TIRETRB_TITRETR_FK 
 FOREIGN KEY (NR_SEQ_TIT_TRIB) 
 REFERENCES TASY.TITULO_RECEBER_TRIB (NR_SEQUENCIA),
  CONSTRAINT TIRETRB_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_RECEBER_TRIB_BAIXA TO NIVEL_1;

