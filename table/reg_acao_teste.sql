ALTER TABLE TASY.REG_ACAO_TESTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_ACAO_TESTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_ACAO_TESTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_ORDENACAO           NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_PASSO               VARCHAR2(4000 BYTE)    NOT NULL,
  DS_RESULTADO           VARCHAR2(4000 BYTE)    NOT NULL,
  NR_SEQ_CASO_TESTE      NUMBER(10)             NOT NULL,
  CD_VERSAO              NUMBER(10),
  DS_FUNCIONALIDADE      VARCHAR2(255 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  NR_SEQ_SCRIPT          NUMBER(10),
  DS_RESULT_PASS         VARCHAR2(4000 BYTE),
  DS_RESULT_FAIL         VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGACO_PK ON TASY.REG_ACAO_TESTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGACO_RECATE_FK_I ON TASY.REG_ACAO_TESTE
(NR_SEQ_CASO_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_acao_teste_after
after insert or update ON TASY.REG_ACAO_TESTE for each row
declare

ie_tipo_alteracao_w		reg_acao_teste_hist.ie_tipo_alteracao%type;

begin

if (inserting) then
	ie_tipo_alteracao_w := 'I';
elsif (updating) then
	if (:old.ie_situacao = 'A' and :new.ie_situacao = 'I') then
		ie_tipo_alteracao_w := 'E';
	else
		ie_tipo_alteracao_w := 'A';
	end if;
end if;

insert into reg_acao_teste_hist (
	nr_sequencia,
	nr_ordenacao,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_passo,
	ds_resultado,
	nr_seq_caso_teste,
	cd_versao,
	nr_seq_reg_version_rev,
	ds_funcionalidade,
	nr_seq_acao_teste,
	ie_tipo_alteracao,
	dt_inclusao_hist)
values (
	reg_acao_teste_hist_seq.nextval,
	:new.nr_ordenacao,
	:new.dt_atualizacao,
	:new.nm_usuario,
	:new.dt_atualizacao_nrec,
	:new.nm_usuario_nrec,
	:new.ds_passo,
	:new.ds_resultado,
	:new.nr_seq_caso_teste,
	:new.cd_versao,
	null,
	:new.ds_funcionalidade,
	:new.nr_sequencia,
	ie_tipo_alteracao_w,
	sysdate);

end;
/


ALTER TABLE TASY.REG_ACAO_TESTE ADD (
  CONSTRAINT REGACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_ACAO_TESTE ADD (
  CONSTRAINT REGACO_RECATE_FK 
 FOREIGN KEY (NR_SEQ_CASO_TESTE) 
 REFERENCES TASY.REG_CASO_TESTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_ACAO_TESTE TO NIVEL_1;

