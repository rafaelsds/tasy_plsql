ALTER TABLE TASY.PF_EQUIPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_EQUIPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_EQUIPE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_EQUIPE             VARCHAR2(80 BYTE)       NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_EXIBIR_PEP         VARCHAR2(1 BYTE),
  IE_EXIBIR_REGULACAO   VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_EQUIPE    NUMBER(10),
  IE_ALMOCO             VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_PARADA  NUMBER(10),
  CD_ESPECIALIDADE      NUMBER(5),
  IE_CONSISTE_EST_PEP   VARCHAR2(1 BYTE),
  NR_ASV_TEAM           NUMBER(9)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFEQUIP_EMMOPAE_FK_I ON TASY.PF_EQUIPE
(NR_SEQ_MOTIVO_PARADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFEQUIP_EMMOPAE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PFEQUIP_ESPMEDI_FK_I ON TASY.PF_EQUIPE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFEQUIP_ESTABEL_FK_I ON TASY.PF_EQUIPE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFEQUIP_I1 ON TASY.PF_EQUIPE
(IE_EXIBIR_PEP, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFEQUIP_I2 ON TASY.PF_EQUIPE
(IE_EXIBIR_PEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFEQUIP_I2
  MONITORING USAGE;


CREATE INDEX TASY.PFEQUIP_PESFISI_FK_I ON TASY.PF_EQUIPE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFEQUIP_PFTIEQU_FK_I ON TASY.PF_EQUIPE
(NR_SEQ_TIPO_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFEQUIP_PFTIEQU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PFEQUIP_PK ON TASY.PF_EQUIPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pf_equipe_befupdate_log
before update ON TASY.PF_EQUIPE for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into pf_equipe_log(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_equipe,
		nr_seq_motivo_parada,
		cd_pessoa_fisica,
		ie_almoco,
		ie_situacao,
		ie_exibir_pep,
		ie_exibir_regulacao,
		nr_seq_tipo_equipe)
	values( pf_equipe_log_seq.NextVal,
		:old.cd_estabelecimento,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.ds_equipe,
		:old.nr_seq_motivo_parada,
		:new.cd_pessoa_fisica,
		:old.ie_almoco,
		:old.ie_situacao,
		:old.ie_exibir_pep,
		:old.ie_exibir_regulacao,
		:old.nr_seq_tipo_equipe);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PF_EQUIPE_tp  after update ON TASY.PF_EQUIPE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EQUIPE,1,4000),substr(:new.DS_EQUIPE,1,4000),:new.nm_usuario,nr_seq_w,'DS_EQUIPE',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_PARADA,1,4000),substr(:new.NR_SEQ_MOTIVO_PARADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_PARADA',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIBIR_PEP,1,4000),substr(:new.IE_EXIBIR_PEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIBIR_PEP',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIBIR_REGULACAO,1,4000),substr(:new.IE_EXIBIR_REGULACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIBIR_REGULACAO',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALMOCO,1,4000),substr(:new.IE_ALMOCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALMOCO',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_EQUIPE,1,4000),substr(:new.NR_SEQ_TIPO_EQUIPE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_EQUIPE',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_EST_PEP,1,4000),substr(:new.IE_CONSISTE_EST_PEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_EST_PEP',ie_log_w,ds_w,'PF_EQUIPE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PF_EQUIPE ADD (
  CONSTRAINT PFEQUIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_EQUIPE ADD (
  CONSTRAINT PFEQUIP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PFEQUIP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFEQUIP_PFTIEQU_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EQUIPE) 
 REFERENCES TASY.PF_TIPO_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT PFEQUIP_EMMOPAE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PARADA) 
 REFERENCES TASY.EME_MOTIVO_PARADA_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT PFEQUIP_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PF_EQUIPE TO NIVEL_1;

