ALTER TABLE TASY.NUT_GUIDANCE_COMPOSITION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_GUIDANCE_COMPOSITION CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_GUIDANCE_COMPOSITION
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_DISPLAY_SEQUENCE    NUMBER(6),
  CD_NUTRIENT            NUMBER(10),
  QT_AMOUNT              NUMBER(8,2),
  CD_UNIT_MEASUREMENT    VARCHAR2(30 BYTE),
  NR_SEQ_NUT_GUIDANCE    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUGUCO_NUGUPA_FK_I ON TASY.NUT_GUIDANCE_COMPOSITION
(NR_SEQ_NUT_GUIDANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NUGUCO_PK ON TASY.NUT_GUIDANCE_COMPOSITION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NUT_GUIDANCE_COMPOSITION ADD (
  CONSTRAINT NUGUCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NUT_GUIDANCE_COMPOSITION ADD (
  CONSTRAINT NUGUCO_NUGUPA_FK 
 FOREIGN KEY (NR_SEQ_NUT_GUIDANCE) 
 REFERENCES TASY.NUTRITIONAL_GUIDANCE_PAT (NR_SEQUENCIA)
    ON DELETE CASCADE);

