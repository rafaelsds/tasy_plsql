ALTER TABLE TASY.IMP_MATERIAL_SISTEMA_EXT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMP_MATERIAL_SISTEMA_EXT CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_MATERIAL_SISTEMA_EXT
(
  NR_SEQUENCIA         NUMBER(10),
  IE_SISTEMA           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(10)               DEFAULT null,
  CD_CODIGO            VARCHAR2(255 BYTE),
  IE_DIRTY_CHECK       NUMBER(1),
  NR_SEQ_MIMS_VERSION  NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPMST_IMPMATE_FK_I ON TASY.IMP_MATERIAL_SISTEMA_EXT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMPMST_PK ON TASY.IMP_MATERIAL_SISTEMA_EXT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_MATERIAL_SISTEMA_EXT ADD (
  CONSTRAINT IMPMST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMP_MATERIAL_SISTEMA_EXT ADD (
  CONSTRAINT IMPMST_IMPMATE_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.IMP_MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMP_MATERIAL_SISTEMA_EXT TO NIVEL_1;

