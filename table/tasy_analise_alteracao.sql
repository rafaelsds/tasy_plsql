ALTER TABLE TASY.TASY_ANALISE_ALTERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_ANALISE_ALTERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_ANALISE_ALTERACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_APLICACAO_TASY    VARCHAR2(10 BYTE)        NOT NULL,
  CD_VERSAO            VARCHAR2(15 BYTE)        NOT NULL,
  NR_BUILD             NUMBER(5),
  IE_TIPO              VARCHAR2(5 BYTE),
  QT_LINHA_ADICIONADA  NUMBER(10),
  QT_LINHA_MODIFICADA  NUMBER(10),
  QT_LINHA_REMOVIDA    NUMBER(10),
  QT_LINHA_TOTAL       NUMBER(10),
  DT_ANALISE           DATE,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAANALT_APLTAVE_FK_I ON TASY.TASY_ANALISE_ALTERACAO
(CD_APLICACAO_TASY, CD_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TAANALT_PK ON TASY.TASY_ANALISE_ALTERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_ANALISE_ALTERACAO_tp  after update ON TASY.TASY_ANALISE_ALTERACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_APLICACAO_TASY,1,4000),substr(:new.CD_APLICACAO_TASY,1,4000),:new.nm_usuario,nr_seq_w,'CD_APLICACAO_TASY',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO,1,4000),substr(:new.CD_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ANALISE,1,4000),substr(:new.DT_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'DT_ANALISE',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LINHA_ADICIONADA,1,4000),substr(:new.QT_LINHA_ADICIONADA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHA_ADICIONADA',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LINHA_MODIFICADA,1,4000),substr(:new.QT_LINHA_MODIFICADA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHA_MODIFICADA',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LINHA_REMOVIDA,1,4000),substr(:new.QT_LINHA_REMOVIDA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHA_REMOVIDA',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LINHA_TOTAL,1,4000),substr(:new.QT_LINHA_TOTAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHA_TOTAL',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_BUILD,1,4000),substr(:new.NR_BUILD,1,4000),:new.nm_usuario,nr_seq_w,'NR_BUILD',ie_log_w,ds_w,'TASY_ANALISE_ALTERACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TASY_ANALISE_ALTERACAO ADD (
  CONSTRAINT TAANALT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_ANALISE_ALTERACAO ADD (
  CONSTRAINT TAANALT_APLTAVE_FK 
 FOREIGN KEY (CD_APLICACAO_TASY, CD_VERSAO) 
 REFERENCES TASY.APLICACAO_TASY_VERSAO (CD_APLICACAO_TASY,CD_VERSAO));

GRANT SELECT ON TASY.TASY_ANALISE_ALTERACAO TO NIVEL_1;

