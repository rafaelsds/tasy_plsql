DROP TABLE TASY.W_ACOMP_FORNECEDOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ACOMP_FORNECEDOR
(
  CD_FORNECEDOR         VARCHAR2(14 BYTE),
  CD_MATERIAL           NUMBER(6),
  NR_ORDEM_COMPRA       NUMBER(10),
  DT_ORDEM_COMPRA       DATE,
  DT_PREVISTA_ENTREGA   DATE,
  QT_MATERIAL           NUMBER(13,4),
  VL_UNITARIO_MATERIAL  NUMBER(13,4),
  VL_ITEM_LIQUIDO       NUMBER(15,2),
  DT_REAL_ENTREGA       DATE,
  QT_REAL_ENTREGA       NUMBER(13,4),
  QT_ENTREGA_PONTUAL    NUMBER(13,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_ACOMP_FORNECEDOR TO NIVEL_1;

