ALTER TABLE TASY.PLS_PACOTE_COMPOSICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PACOTE_COMPOSICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PACOTE_COMPOSICAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DS_COMPOSICAO         VARCHAR2(120 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PACOTE         NUMBER(10)              NOT NULL,
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(30 BYTE),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  DT_NEGOCIACAO         DATE,
  DT_REAJUSTE           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPACC_PK ON TASY.PLS_PACOTE_COMPOSICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPACC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPACC_PLSPACO_FK_I ON TASY.PLS_PACOTE_COMPOSICAO
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPACC_PLSPACO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_PACOTE_COMPOSICAO_tp  after update ON TASY.PLS_PACOTE_COMPOSICAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_PACOTE,1,4000),substr(:new.NR_SEQ_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PACOTE',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PLS_PACOTE_COMPOSICAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PACOTE_COMPOSICAO ADD (
  CONSTRAINT PLSPACC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PACOTE_COMPOSICAO ADD (
  CONSTRAINT PLSPACC_PLSPACO_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PLS_PACOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PACOTE_COMPOSICAO TO NIVEL_1;

