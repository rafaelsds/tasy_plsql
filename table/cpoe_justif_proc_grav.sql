ALTER TABLE TASY.CPOE_JUSTIF_PROC_GRAV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_JUSTIF_PROC_GRAV CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_JUSTIF_PROC_GRAV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_PROC_CPOE     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CPOEJPG_PK ON TASY.CPOE_JUSTIF_PROC_GRAV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_JUSTIF_PROC_GRAV ADD (
  CONSTRAINT CPOEJPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.CPOE_JUSTIF_PROC_GRAV TO NIVEL_1;

