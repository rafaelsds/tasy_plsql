ALTER TABLE TASY.PLS_CTA_LOTE_PROC_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CTA_LOTE_PROC_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CTA_LOTE_PROC_CONTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_PROCESSO  NUMBER(10)              NOT NULL,
  NR_SEQ_CONTA          NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCLPC_I1 ON TASY.PLS_CTA_LOTE_PROC_CONTA
(NR_SEQ_LOTE_PROCESSO, NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCLPC_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCLPC_PK ON TASY.PLS_CTA_LOTE_PROC_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCLPC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCLPC_PLSCOME_FK_I ON TASY.PLS_CTA_LOTE_PROC_CONTA
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCLPC_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCLPC_PLSCTLP_FK_I ON TASY.PLS_CTA_LOTE_PROC_CONTA
(NR_SEQ_LOTE_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCLPC_PLSCTLP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CTA_LOTE_PROC_CONTA ADD (
  CONSTRAINT PLSCLPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CTA_LOTE_PROC_CONTA ADD (
  CONSTRAINT PLSCLPC_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCLPC_PLSCTLP_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PROCESSO) 
 REFERENCES TASY.PLS_CTA_LOTE_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CTA_LOTE_PROC_CONTA TO NIVEL_1;

