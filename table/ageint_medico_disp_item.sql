ALTER TABLE TASY.AGEINT_MEDICO_DISP_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_MEDICO_DISP_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_MEDICO_DISP_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_AGEINT_ITEM   NUMBER(10)               NOT NULL,
  NR_SEQ_AGEINT        NUMBER(10),
  DT_AGENDA            DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIMDDI_AGEINIT_FK_I ON TASY.AGEINT_MEDICO_DISP_ITEM
(NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMDDI_AGEINIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIMDDI_AGEINTE_FK_I ON TASY.AGEINT_MEDICO_DISP_ITEM
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMDDI_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIMDDI_PESFISI_FK_I ON TASY.AGEINT_MEDICO_DISP_ITEM
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGIMDDI_PK ON TASY.AGEINT_MEDICO_DISP_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIMDDI_PK
  MONITORING USAGE;


ALTER TABLE TASY.AGEINT_MEDICO_DISP_ITEM ADD (
  CONSTRAINT AGIMDDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_MEDICO_DISP_ITEM ADD (
  CONSTRAINT AGIMDDI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGIMDDI_AGEINIT_FK 
 FOREIGN KEY (NR_SEQ_AGEINT_ITEM) 
 REFERENCES TASY.AGENDA_INTEGRADA_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGIMDDI_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGEINT_MEDICO_DISP_ITEM TO NIVEL_1;

