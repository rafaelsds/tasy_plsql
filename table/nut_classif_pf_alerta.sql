ALTER TABLE TASY.NUT_CLASSIF_PF_ALERTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_CLASSIF_PF_ALERTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_CLASSIF_PF_ALERTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_INICIAL            DATE,
  DT_FINAL              DATE,
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  NR_ATENDIMENTO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTCLPF_ATEPACI_FK_I ON TASY.NUT_CLASSIF_PF_ALERTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTCLPF_NUTCLAL_FK_I ON TASY.NUT_CLASSIF_PF_ALERTA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTCLPF_NUTCLAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTCLPF_PESFISI_FK_I ON TASY.NUT_CLASSIF_PF_ALERTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NUTCLPF_PK ON TASY.NUT_CLASSIF_PF_ALERTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTCLPF_PK
  MONITORING USAGE;


ALTER TABLE TASY.NUT_CLASSIF_PF_ALERTA ADD (
  CONSTRAINT NUTCLPF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_CLASSIF_PF_ALERTA ADD (
  CONSTRAINT NUTCLPF_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NUTCLPF_NUTCLAL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.NUT_CLASSIFICACAO_ALERTA (NR_SEQUENCIA),
  CONSTRAINT NUTCLPF_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.NUT_CLASSIF_PF_ALERTA TO NIVEL_1;

