ALTER TABLE TASY.ATENDIMENTO_PACIENTE_INF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_PACIENTE_INF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_PACIENTE_INF
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_MEDICO                 VARCHAR2(10 BYTE),
  CD_EMPRESA                NUMBER(4),
  DT_REFERENCIA             DATE,
  DS_DEPARTMENTO_ESP        VARCHAR2(255 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  CD_CGC_INDICACAO          VARCHAR2(14 BYTE),
  DT_VALIDADE               DATE,
  IE_EXISTE                 VARCHAR2(1 BYTE),
  DS_ACIDENTE               VARCHAR2(255 BYTE)  DEFAULT null,
  CD_MUNICIPIO_OCORRENCIA   VARCHAR2(15 BYTE)   DEFAULT null,
  NR_SEQ_FORMA_CHEGADA      NUMBER(10)          DEFAULT null,
  NR_SEQ_TIPO_ACIDENTE      NUMBER(10)          DEFAULT null,
  NR_ACIDENTE               VARCHAR2(12 BYTE)   DEFAULT null,
  DT_ACIDENTE               DATE                DEFAULT null,
  DT_OCORRENCIA             DATE                DEFAULT null,
  NR_BSNR                   VARCHAR2(25 BYTE)   DEFAULT null,
  NR_RQE                    VARCHAR2(20 BYTE)   DEFAULT null,
  CD_CGC_EMPREGADOR         VARCHAR2(14 BYTE),
  DT_INICIO_TRABALHO        DATE,
  IE_TIPO_INFORMACAO        VARCHAR2(2 BYTE),
  IE_ATEND_CONVID_POSIT     VARCHAR2(1 BYTE),
  IE_TYPE_CODE              VARCHAR2(1 BYTE),
  NR_TELEFONE               VARCHAR2(15 BYTE),
  CD_PROFISSAO              NUMBER(10),
  DS_PROFISSAO              VARCHAR2(255 BYTE),
  DT_INICIO_VALIDADE        DATE,
  DS_EMPRESA                VARCHAR2(80 BYTE),
  NR_DDD_TELEFONE           VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE           VARCHAR2(3 BYTE),
  CD_CONTRACT_HOSP          VARCHAR2(32 BYTE),
  CD_CONTRACT_ROLE          VARCHAR2(2 BYTE),
  CD_CONTRACT_TYPE          NUMBER(2),
  DS_MUNICIPIO_EMPREG       VARCHAR2(40 BYTE),
  CD_CEP_EMPREG             VARCHAR2(15 BYTE),
  DS_ENDERECO_EMPREG        VARCHAR2(40 BYTE),
  NR_ENDERECO_EMPREG        VARCHAR2(10 BYTE),
  DS_PAIS_EMPREG            VARCHAR2(80 BYTE),
  IE_TRAT_TRANSPLANTE       VARCHAR2(1 BYTE),
  CD_PRE_HOSP_HOME          VARCHAR2(15 BYTE),
  CD_HOSPITALIZATION_ROUTE  VARCHAR2(15 BYTE),
  CD_EVOLUCAO               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPAINF_ATEPACI_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_EMPRESA_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_EVOPACI_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_FORCHEG_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(NR_SEQ_FORMA_CHEGADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_PESJURI_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(CD_CGC_EMPREGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_PESJURI_FK2_I ON TASY.ATENDIMENTO_PACIENTE_INF
(CD_CGC_INDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPAINF_PK ON TASY.ATENDIMENTO_PACIENTE_INF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_PROFISS_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPAINF_TIPACID_FK_I ON TASY.ATENDIMENTO_PACIENTE_INF
(NR_SEQ_TIPO_ACIDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PACIENTE_INF_ATUAL
after insert or update or delete ON TASY.ATENDIMENTO_PACIENTE_INF 
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w			number(10);
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
cd_pessoa_fisica_w		atendimento_paciente.cd_pessoa_fisica%type;
qt_eventos_w			number(10);
nr_seq_episodio_w		number(10);
ie_tipo_kv_w			tipo_admissao_fat.ie_tipo_kv%type;

function get_pf_atendimento(nr_atendimento_p number) return varchar2 is

cd_pessoa_fisica_w		atendimento_paciente.cd_pessoa_fisica%type;

pragma autonomous_transaction;
begin
select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

return cd_pessoa_fisica_w;
end get_pf_atendimento;

function get_tipo_atendimento(nr_atendimento_p number) return varchar2 is

ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;

pragma autonomous_transaction;
begin
select	max(ie_tipo_atendimento)
into	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

return ie_tipo_atendimento_w;

end get_tipo_atendimento;

function get_tipo_admissao_fat(nr_atendimento_p number) return number is

nr_seq_tipo_admissao_fat_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;

pragma autonomous_transaction;
begin
select	max(nr_seq_tipo_admissao_fat)
into	nr_seq_tipo_admissao_fat_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

return nr_seq_tipo_admissao_fat_w;

end get_tipo_admissao_fat;

function get_tipo_nr_episodio(nr_atendimento_p number) return number is

nr_seq_episodio_w	atendimento_paciente.nr_seq_episodio%type;

pragma autonomous_transaction;
begin
select	max(nr_seq_episodio)
into	nr_seq_episodio_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

return nr_seq_episodio_w;

end get_tipo_nr_episodio;

procedure enviar_atual_atendimentos_case (nr_seq_episodio_p number) is

reg_integracao_w		gerar_int_padrao.reg_integracao;

cursor c01 is
select	a.cd_pessoa_fisica,
	a.nr_atendimento,
	a.ie_tipo_atendimento
from	atendimento_paciente a
where	a.nr_seq_episodio = nr_seq_episodio_p
and	a.dt_cancelamento is null
and	a.dt_alta is null
and	exists(	select	1
		from	atend_paciente_unidade x
		where	x.nr_atendimento = a.nr_atendimento);

c01_w	c01%rowtype;

pragma autonomous_transaction;
begin
	begin
		open C01;
		loop
		fetch C01 into c01_w;
		exit when C01%notfound;

			reg_integracao_w.ie_operacao		:=	'A';	
			reg_integracao_w.cd_pessoa_fisica	:=	c01_w.cd_pessoa_fisica;
			reg_integracao_w.nr_atendimento		:=	c01_w.nr_atendimento;

			if	(get_case_encounter_type(null, null, reg_integracao_w.nr_atendimento, null) = '1') then	
				gerar_int_padrao.gravar_integracao('106', nr_seq_episodio_p, nvl(obter_usuario_ativo,:old.nm_usuario), reg_integracao_w);
			else
				begin
				select	count(1)
				into	qt_reg_w
				from	intpd_fila_transmissao
				where	nr_seq_documento	= c01_w.nr_atendimento
				and	ie_operacao		= 'I'
				and	ie_status		in ('P', 'E', 'AP')
				and	ie_evento		= '127'
				and	rownum			= 1;
				exception
				when others then
					qt_reg_w := 0;
				end;
			
				if	(qt_reg_w = 0) then
					gerar_int_padrao.gravar_integracao('120', reg_integracao_w.nr_atendimento, nvl(obter_usuario_ativo,:old.nm_usuario), reg_integracao_w);
				end if;	
			end if;

		end loop;
		close C01;

		commit;
	
	exception
	   when others then
		   rollback;
			wheb_mensagem_pck.exibir_mensagem_abort(sqlerrm);
   end;

end enviar_atual_atendimentos_case;

begin

nr_atendimento_w	:=	nvl(:new.nr_atendimento, :old.nr_atendimento);
cd_pessoa_fisica_w	:=	get_pf_atendimento(nr_atendimento_w);

if	((inserting or updating) and
	(nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S'))	then
	insere_pessoa_fisica_emp(	:new.cd_cgc_empregador,
							:new.dt_inicio_trabalho,
							:new.nm_usuario,
							cd_pessoa_fisica_w,
							null,
							:new.cd_profissao,
							:new.ds_profissao,
							nvl(:old.cd_cgc_empregador, :new.cd_cgc_empregador),
							'N',
							:new.ds_empresa,
							nvl(:old.ds_empresa, :new.ds_empresa),
                            :new.ds_endereco_empreg,
							:new.nr_endereco_empreg,
							:new.ds_pais_empreg,
							:new.cd_cep_empreg,
							:new.ds_municipio_empreg,
							:new.nr_telefone
                            );
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	/* Verificar se existe cadastrado pelo menos um evento*/
	begin
	select	count(1)
	into	qt_eventos_w
	from	intpd_eventos
	where	ie_evento	= '403'
	and	ie_situacao	= 'A'
	and	rownum		= 1;
	exception
	when others then
		qt_eventos_w	:= 0;
	end;
	
	nr_seq_episodio_w := nvl(get_tipo_nr_episodio(nr_atendimento_w),0);

	enviar_atual_atendimentos_case(nr_seq_episodio_w);
	
	if	((qt_eventos_w > 0) and (nvl(get_tipo_atendimento(nr_atendimento_w),'0') <> '1')) then
		
		begin
		select	1
		into	qt_reg_w
		from	atend_paciente_unidade
		where	nr_atendimento	= nr_atendimento_w
		and	rownum	= 1;
		exception
		when others then
			qt_reg_w := 0;
		end;
		
		/*Movido o tratamento para a ish_atend_pac_unidade_ aftins, para agendar a msg somente apos agendado o envio do case.
		Neste caso, ira agendar somente se o atendimento ja possuir unidade e o registro for criado apos*/
		if	(inserting) and 
			(qt_reg_w > 0) then	
			
			reg_integracao_p.nr_seq_tipo_admissao_fat	:= 	get_tipo_admissao_fat(nr_atendimento_w);			
			reg_integracao_p.ie_operacao			:=	'I';	
			reg_integracao_p.cd_pessoa_fisica		:=	cd_pessoa_fisica_w;
			reg_integracao_p.nr_atendimento			:=	nr_atendimento_w;
					
			gerar_int_padrao.gravar_integracao('403', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		
		elsif	(not inserting) and
			(qt_reg_w > 0) then
			
			reg_integracao_p.nr_seq_tipo_admissao_fat	:= 	get_tipo_admissao_fat(nr_atendimento_w);
			
			if (updating and :old.dt_inicio_validade is null and :new.dt_inicio_validade is not null and :old.dt_validade is null and :new.dt_validade is not null)then
                reg_integracao_p.ie_operacao		:=	'A';
			else
				reg_integracao_p.ie_operacao		:=	'E';
			end if;

			reg_integracao_p.cd_pessoa_fisica	:=	cd_pessoa_fisica_w;
			reg_integracao_p.nr_atendimento		:=	nr_atendimento_w;	
			--Em seguida, uma inclusao para o novo registro
			gerar_int_padrao.gravar_integracao('403', nvl(:new.nr_atendimento,:old.nr_atendimento), nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;
	end if;
end if;

end ATENDIMENTO_PACIENTE_INF_ATUAL;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PACIENTE_INF_BEFINSUP
before insert or update ON TASY.ATENDIMENTO_PACIENTE_INF for each row
declare

begin

	if (:new.dt_ocorrencia is not null and :new.dt_ocorrencia > sysdate) then
		wheb_mensagem_pck.exibir_mensagem_abort(1103201);
	end if;

	if (:new.dt_inicio_trabalho is not null and :new.dt_inicio_trabalho > sysdate) then
		wheb_mensagem_pck.exibir_mensagem_abort(1117614);
	end if;

end;
/


ALTER TABLE TASY.ATENDIMENTO_PACIENTE_INF ADD (
  CONSTRAINT ATPAINF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_PACIENTE_INF ADD (
  CONSTRAINT ATPAINF_PESJURI_FK 
 FOREIGN KEY (CD_CGC_EMPREGADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ATPAINF_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT ATPAINF_TIPACID_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACIDENTE) 
 REFERENCES TASY.TIPO_ACIDENTE (NR_SEQUENCIA),
  CONSTRAINT ATPAINF_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT ATPAINF_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_INDICACAO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ATPAINF_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPAINF_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATPAINF_FORCHEG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CHEGADA) 
 REFERENCES TASY.FORMA_CHEGADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_PACIENTE_INF TO NIVEL_1;

