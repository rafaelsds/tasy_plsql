ALTER TABLE TASY.SINAN_TRANSTORNOS_MENTAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SINAN_TRANSTORNOS_MENTAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SINAN_TRANSTORNOS_MENTAIS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_NOTIFICACAO         NUMBER(10),
  QT_TEMPO_EXPOSICAO_RISCO   VARCHAR2(2 BYTE),
  IE_TEMPO_EXPOSICAO_RISCO   VARCHAR2(1 BYTE),
  QT_TEMPO_EXPOSICAO_TABACO  VARCHAR2(2 BYTE),
  IE_TEMPO_EXPOSICAO_TABACO  VARCHAR2(1 BYTE),
  IE_REGIME_TRATAMENTO       VARCHAR2(1 BYTE),
  CD_CID                     VARCHAR2(10 BYTE),
  IE_HABITO_ALCOOL           VARCHAR2(1 BYTE),
  IE_HABITO_PSICOATIVAS      VARCHAR2(1 BYTE),
  IE_HABITO_PSICOFARMACOS    VARCHAR2(1 BYTE),
  IE_HABITO_FUMAR            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SITRMEN_CIDDOEN_FK_I ON TASY.SINAN_TRANSTORNOS_MENTAIS
(CD_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SITRMEN_NOTSINA_FK_I ON TASY.SINAN_TRANSTORNOS_MENTAIS
(NR_SEQ_NOTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SITRMEN_PK ON TASY.SINAN_TRANSTORNOS_MENTAIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SINAN_TRANSTORNOS_MENTAIS ADD (
  CONSTRAINT SITRMEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SINAN_TRANSTORNOS_MENTAIS ADD (
  CONSTRAINT SITRMEN_CIDDOEN_FK 
 FOREIGN KEY (CD_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SITRMEN_NOTSINA_FK 
 FOREIGN KEY (NR_SEQ_NOTIFICACAO) 
 REFERENCES TASY.NOTIFICACAO_SINAN (NR_SEQUENCIA));

GRANT SELECT ON TASY.SINAN_TRANSTORNOS_MENTAIS TO NIVEL_1;

