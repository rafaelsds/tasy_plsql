ALTER TABLE TASY.CLIENTE_INF_BASE_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLIENTE_INF_BASE_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLIENTE_INF_BASE_CLIENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INF           NUMBER(10)               NOT NULL,
  CD_CNPJ              VARCHAR2(14 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CLIINFBAC_BINFBASCLI_FK_I ON TASY.CLIENTE_INF_BASE_CLIENTE
(NR_SEQ_INF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CLIINFBAC_NR_SEQ_INF_CNPJ_UK ON TASY.CLIENTE_INF_BASE_CLIENTE
(NR_SEQ_INF, CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CLIINFBAC_NR_SEQ_INF_CNPJ_UK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CLIINFBAC_PK ON TASY.CLIENTE_INF_BASE_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CLIINFBAC_PK
  MONITORING USAGE;


ALTER TABLE TASY.CLIENTE_INF_BASE_CLIENTE ADD (
  CONSTRAINT CLIINFBAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CLIENTE_INF_BASE_CLIENTE ADD (
  CONSTRAINT CLIINFBAC_BINFBASCLI_FK 
 FOREIGN KEY (NR_SEQ_INF) 
 REFERENCES TASY.BUSCA_INF_BASE_CLIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CLIENTE_INF_BASE_CLIENTE TO NIVEL_1;

