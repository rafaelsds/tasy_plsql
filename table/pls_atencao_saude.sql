ALTER TABLE TASY.PLS_ATENCAO_SAUDE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATENCAO_SAUDE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATENCAO_SAUDE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESPECIALIDADE     NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TP_ATENCAO_SAUDE  VARCHAR2(2 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSATES_ESPMEDI_FK_I ON TASY.PLS_ATENCAO_SAUDE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSATES_PK ON TASY.PLS_ATENCAO_SAUDE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_atualizar_atencao_saude
after insert or update or delete ON TASY.PLS_ATENCAO_SAUDE for each row
declare
ie_permite_atualizar_w	varchar2(1);

cursor C01(especialidade_p Number) is
	select	nr_sequencia
	from	pls_prestador_med_espec
	where	cd_especialidade = especialidade_p;


begin

select	nvl(IE_ATUALIZA_PREST_ATE_SAU, 'N')
into	ie_permite_atualizar_w
from	PLS_PARAMETROS
where	(wheb_usuario_pck.get_cd_estabelecimento is null) or
	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_permite_atualizar_w = 'S') then
	for	r_c01_w	in	C01( :old.cd_especialidade ) loop
		delete	pls_prest_atencao_saude
		where	nr_sequencia = (	select	min(nr_sequencia)
						from	pls_prest_atencao_saude
						where	nr_seq_prest_med_espec = r_c01_w.nr_sequencia
						and	ie_tp_atencao_saude = :old.ie_tp_atencao_saude);
	end loop;

	if	(:new.nr_sequencia is not null) then
		for	r_c01_w	in	C01( :new.cd_especialidade ) loop
			insert into pls_prest_atencao_saude(	nr_sequencia,
								nr_seq_prest_med_espec,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ie_tp_atencao_saude,
								dt_inicio_vigencia,
								dt_fim_vigencia)
			values (pls_prest_atencao_saude_seq.nextval,
				r_c01_w.nr_sequencia,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.ie_tp_atencao_saude,
				nvl(:new.dt_inicio_vigencia, sysdate),
				:new.dt_fim_vigencia);
		end loop;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_ATENCAO_SAUDE_tp  after update ON TASY.PLS_ATENCAO_SAUDE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_ESPECIALIDADE,1,500);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PLS_ATENCAO_SAUDE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_ATENCAO_SAUDE ADD (
  CONSTRAINT PLSATES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATENCAO_SAUDE ADD (
  CONSTRAINT PLSATES_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PLS_ATENCAO_SAUDE TO NIVEL_1;

