ALTER TABLE TASY.CLASSIFICACAO_PROJETO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLASSIFICACAO_PROJETO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLASSIFICACAO_PROJETO_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NM_USUARIO_LIB        VARCHAR2(255 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  CD_PERFIL             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CLAPROJLIB_CLASPROJ_FK_I ON TASY.CLASSIFICACAO_PROJETO_LIB
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CLAPROJLIB_PERFIL_FK_I ON TASY.CLASSIFICACAO_PROJETO_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CLAPROJLIB_PK ON TASY.CLASSIFICACAO_PROJETO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CLASSIFICACAO_PROJETO_LIB ADD (
  CONSTRAINT CLAPROJLIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CLASSIFICACAO_PROJETO_LIB ADD (
  CONSTRAINT CLAPROJLIB_CLASPROJ_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_PROJETO (NR_SEQUENCIA),
  CONSTRAINT CLAPROJLIB_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.CLASSIFICACAO_PROJETO_LIB TO NIVEL_1;

