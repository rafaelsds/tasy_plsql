ALTER TABLE TASY.PROJ_ENVIO_CI_NIVEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_ENVIO_CI_NIVEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_ENVIO_CI_NIVEL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_USUARIO_REGRA     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJENVCI_PK ON TASY.PROJ_ENVIO_CI_NIVEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJENVCI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJENVCI_USUARIO_FK_I ON TASY.PROJ_ENVIO_CI_NIVEL
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJENVCI_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_ENVIO_CI_NIVEL ADD (
  CONSTRAINT PROJENVCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_ENVIO_CI_NIVEL ADD (
  CONSTRAINT PROJENVCI_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.PROJ_ENVIO_CI_NIVEL TO NIVEL_1;

