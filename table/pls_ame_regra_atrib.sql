ALTER TABLE TASY.PLS_AME_REGRA_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AME_REGRA_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AME_REGRA_ATRIB
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_BANDA              NUMBER(10),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ORDEM                  NUMBER(10),
  CD_ATRIBUTO               NUMBER(5),
  VL_PADRAO                 VARCHAR2(255 BYTE),
  CD_TIPO_ATRIBUTO          VARCHAR2(1 BYTE),
  VL_FIXO_INICIO            VARCHAR2(255 BYTE),
  QT_TAMANHO                NUMBER(10),
  CD_CARACTER_COMPLEMENTAR  VARCHAR2(2 BYTE),
  IE_POSICAO_COMPLEMENTAR   VARCHAR2(1 BYTE),
  DS_MASCARA                VARCHAR2(255 BYTE),
  IE_LETRAS_VALOR           VARCHAR2(1 BYTE),
  DS_FUNCTION               VARCHAR2(255 BYTE),
  NR_ORDEM_RESULTADO        NUMBER(5),
  IE_EXIBIR_ATRIBUTO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSARAT_PK ON TASY.PLS_AME_REGRA_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSARAT_PLSARBAN_FK_I ON TASY.PLS_AME_REGRA_ATRIB
(NR_SEQ_BANDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ame_regra_atrib_befinsup
before insert or update ON TASY.PLS_AME_REGRA_ATRIB for each row
declare

	ie_tipo_banda_w 	pls_ame_regra_banda.ie_tipo_banda%type;
	vl_atributo_w		VARCHAR(255);
	nm_atributo_w		VARCHAR(255);
begin

select 	a.ie_tipo_banda
into 	ie_tipo_banda_w
from 	pls_ame_regra_banda a
where 	a.nr_sequencia = :new.nr_seq_banda;

if	(ie_tipo_banda_w in (1, 4)) then

	nm_atributo_w := obter_valor_dominio(9036, :new.cd_atributo);

	if	(:new.cd_atributo in (3,4,5,6) and :new.ds_mascara is not null) then
		--datas
		begin
			vl_atributo_w := to_char(sysdate, :new.ds_mascara);
		exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(1057732, 'DS_MASCARA='|| :new.ds_mascara ||';'||'NM_ATRIBUTO_W='||nm_atributo_w);
		end;

	end if;
else

	nm_atributo_w := obter_valor_dominio(9037, :new.cd_atributo);

	if	(:new.cd_atributo in (5,6,10)) then
		--datas
		begin
			vl_atributo_w := to_char(sysdate, :new.ds_mascara);
		exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(1057732, 'DS_MASCARA='|| :new.ds_mascara ||';'||'NM_ATRIBUTO_W='||nm_atributo_w);
		end;
	end if;
end if;

end pls_ame_regra_atrib_befinsup;
/


ALTER TABLE TASY.PLS_AME_REGRA_ATRIB ADD (
  CONSTRAINT PLSARAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_AME_REGRA_ATRIB ADD (
  CONSTRAINT PLSARAT_PLSARBAN_FK 
 FOREIGN KEY (NR_SEQ_BANDA) 
 REFERENCES TASY.PLS_AME_REGRA_BANDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AME_REGRA_ATRIB TO NIVEL_1;

