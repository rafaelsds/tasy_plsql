ALTER TABLE TASY.INDICE_REAJUSTE_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INDICE_REAJUSTE_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.INDICE_REAJUSTE_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INDICE        NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE                     NOT NULL,
  TX_AJUSTE            NUMBER(18,9)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INRECON_PK ON TASY.INDICE_REAJUSTE_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INRECON_PK
  MONITORING USAGE;


CREATE INDEX TASY.INRECON_TIINREA_FK_I ON TASY.INDICE_REAJUSTE_VALOR
(NR_SEQ_INDICE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INRECON_TIINREA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.INDICE_REAJUSTE_VALOR ADD (
  CONSTRAINT INRECON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INDICE_REAJUSTE_VALOR ADD (
  CONSTRAINT INRECON_TIINREA_FK 
 FOREIGN KEY (NR_SEQ_INDICE) 
 REFERENCES TASY.TIPO_INDICE_REAJUSTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INDICE_REAJUSTE_VALOR TO NIVEL_1;

