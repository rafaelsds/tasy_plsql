DROP TABLE TASY.LOG_COT_COMPRA_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_COT_COMPRA_WEB
(
  NR_SEQUENCIA    NUMBER(13)                    NOT NULL,
  NM_USUARIO      VARCHAR2(20 BYTE)             NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  DS_UTC          VARCHAR2(50 BYTE),
  DS_LOG          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER TASY.LOG_COT_COMPRA_WEB_tp  after update ON TASY.LOG_COT_COMPRA_WEB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null; gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'LOG_COT_COMPRA_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LOG_COT_COMPRA_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOG,1,4000),substr(:new.DS_LOG,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOG',ie_log_w,ds_w,'LOG_COT_COMPRA_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_UTC,1,4000),substr(:new.DS_UTC,1,4000),:new.nm_usuario,nr_seq_w,'DS_UTC',ie_log_w,ds_w,'LOG_COT_COMPRA_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LOG_COT_COMPRA_WEB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


GRANT SELECT ON TASY.LOG_COT_COMPRA_WEB TO NIVEL_1;

