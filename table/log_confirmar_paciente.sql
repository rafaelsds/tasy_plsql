ALTER TABLE TASY.LOG_CONFIRMAR_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_CONFIRMAR_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_CONFIRMAR_PACIENTE
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  IE_MODO_LEITURA     VARCHAR2(1 BYTE),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_SEQ_PROCESSO     NUMBER(10),
  DS_MOTIVO           VARCHAR2(255 BYTE),
  IE_TIPO_ETIQUETA    VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO       NUMBER(10),
  NR_ATENDIMENTO      NUMBER(10),
  NR_SEQ_HORARIO      NUMBER(15),
  CD_MATERIAL         NUMBER(6)                 DEFAULT null,
  IE_MISMATCHED_READ  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGCOPAC_ADEPROC_FK_I ON TASY.LOG_CONFIRMAR_PACIENTE
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGCOPAC_ATEPACI_FK_I ON TASY.LOG_CONFIRMAR_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGCOPAC_MODIMA_FK_I ON TASY.LOG_CONFIRMAR_PACIENTE
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOGCOPAC_PK ON TASY.LOG_CONFIRMAR_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGCOPAC_PREMAHO_FK_I ON TASY.LOG_CONFIRMAR_PACIENTE
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOG_CONFIRMAR_PACIENTE ADD (
  CONSTRAINT LOGCOPAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_CONFIRMAR_PACIENTE ADD (
  CONSTRAINT LOGCOPAC_ADEPROC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.ADEP_PROCESSO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LOGCOPAC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT LOGCOPAC_MODIMA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_DIGITAL_MANUAL (NR_SEQUENCIA),
  CONSTRAINT LOGCOPAC_PREMAHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.PRESCR_MAT_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOG_CONFIRMAR_PACIENTE TO NIVEL_1;

