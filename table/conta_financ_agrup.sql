ALTER TABLE TASY.CONTA_FINANC_AGRUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_FINANC_AGRUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_FINANC_AGRUP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_CONTA_FINANC        NUMBER(10)             NOT NULL,
  CD_CONTA_FINANC_AGRUP  NUMBER(10)             NOT NULL,
  IE_INVERTE_VALOR       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COFIAGR_CONFINA_FK_I ON TASY.CONTA_FINANC_AGRUP
(CD_CONTA_FINANC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COFIAGR_CONFINA_FK2_I ON TASY.CONTA_FINANC_AGRUP
(CD_CONTA_FINANC_AGRUP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COFIAGR_CONFINA_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COFIAGR_PK ON TASY.CONTA_FINANC_AGRUP
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONTA_FINANC_AGRUP ADD (
  CONSTRAINT COFIAGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_FINANC_AGRUP ADD (
  CONSTRAINT COFIAGR_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT COFIAGR_CONFINA_FK2 
 FOREIGN KEY (CD_CONTA_FINANC_AGRUP) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC));

GRANT SELECT ON TASY.CONTA_FINANC_AGRUP TO NIVEL_1;

