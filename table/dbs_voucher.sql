ALTER TABLE TASY.DBS_VOUCHER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DBS_VOUCHER CASCADE CONSTRAINTS;

CREATE TABLE TASY.DBS_VOUCHER
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CLAIM           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_BENEFIT             VARCHAR2(10 BYTE),
  DT_SERVICE             DATE,
  DT_BIRTH               DATE,
  NM_FAMILY              VARCHAR2(40 BYTE),
  NM_FIRST               VARCHAR2(40 BYTE),
  CD_MEDICARE_CARD       NUMBER(10),
  NR_PATIENT_REF         NUMBER(10),
  IE_REFERRAL_OVERRIDE   VARCHAR2(10 BYTE),
  CD_REFERRING           VARCHAR2(10 BYTE),
  DT_REFERRAL            DATE,
  IE_REFERRAL_PERIOD     VARCHAR2(10 BYTE),
  CD_REQUESTING          VARCHAR2(10 BYTE),
  DT_REQUEST             DATE,
  IE_REQUEST_TYPE        VARCHAR2(10 BYTE),
  NR_TIME_SERVICE        NUMBER(10),
  NR_SEQ_ACCOUNT         NUMBER(10),
  CD_VOUCHER             VARCHAR2(4 BYTE),
  CD_MED_CARD_ISSUE_NUM  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DBSVOUC_DBSCLAI_FK_I ON TASY.DBS_VOUCHER
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DBSVOUC_PK ON TASY.DBS_VOUCHER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DBS_VOUCHER ADD (
  CONSTRAINT DBSVOUC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DBS_VOUCHER ADD (
  CONSTRAINT DBSVOUC_DBSCLAI_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.DBS_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.DBS_VOUCHER TO NIVEL_1;

