ALTER TABLE TASY.CTB_TIPO_ESTRUTURA_ORC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_TIPO_ESTRUTURA_ORC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_TIPO_ESTRUTURA_ORC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO              VARCHAR2(255 BYTE)       NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  IE_RESULTADO_CENTRO  VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBTPEOR_EMPRESA_FK_I ON TASY.CTB_TIPO_ESTRUTURA_ORC
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBTPEOR_EMPRESA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBTPEOR_PK ON TASY.CTB_TIPO_ESTRUTURA_ORC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBTPEOR_PK
  MONITORING USAGE;


ALTER TABLE TASY.CTB_TIPO_ESTRUTURA_ORC ADD (
  CONSTRAINT CTBTPEOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_TIPO_ESTRUTURA_ORC ADD (
  CONSTRAINT CTBTPEOR_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.CTB_TIPO_ESTRUTURA_ORC TO NIVEL_1;

