ALTER TABLE TASY.INTPD_EVENTOS_VALORES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_EVENTOS_VALORES CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_EVENTOS_VALORES
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_EVENTO_SISTEMA  NUMBER(10)             NOT NULL,
  NM_ELEMENTO            VARCHAR2(50 BYTE),
  NM_ATRIBUTO            VARCHAR2(60 BYTE),
  DS_VALOR               VARCHAR2(40 BYTE),
  IE_VAZIO               VARCHAR2(1 BYTE),
  IE_CONVERSAO           VARCHAR2(1 BYTE),
  IE_INVALIDO            VARCHAR2(1 BYTE),
  IE_MANTER_VALOR        VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPDEV_ESTABEL_FK_I ON TASY.INTPD_EVENTOS_VALORES
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTPDEV_INTPDESI_FK_I ON TASY.INTPD_EVENTOS_VALORES
(NR_SEQ_EVENTO_SISTEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPDEV_PK ON TASY.INTPD_EVENTOS_VALORES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTPD_EVENTOS_VALORES ADD (
  CONSTRAINT INTPDEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTPD_EVENTOS_VALORES ADD (
  CONSTRAINT INTPDEV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT INTPDEV_INTPDESI_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_SISTEMA) 
 REFERENCES TASY.INTPD_EVENTOS_SISTEMA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INTPD_EVENTOS_VALORES TO NIVEL_1;

