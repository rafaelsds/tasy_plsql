ALTER TABLE TASY.EIF_ESCALA_II_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIF_ESCALA_II_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIF_ESCALA_II_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESCALA        NUMBER(10)               NOT NULL,
  DS_RESULTADO         VARCHAR2(255 BYTE)       NOT NULL,
  QT_PONTOS_MIN        NUMBER(4)                NOT NULL,
  QT_PONTOS_MAX        NUMBER(4)                NOT NULL,
  NR_SEQ_TRIAGEM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EIFESRE_EIFESII_FK_I ON TASY.EIF_ESCALA_II_RESULT
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EIFESRE_EIFESII_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EIFESRE_PK ON TASY.EIF_ESCALA_II_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EIFESRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.EIFESRE_TRICLRI_FK_I ON TASY.EIF_ESCALA_II_RESULT
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EIFESRE_TRICLRI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EIF_ESCALA_II_RESULT ADD (
  CONSTRAINT EIFESRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EIF_ESCALA_II_RESULT ADD (
  CONSTRAINT EIFESRE_EIFESII_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.EIF_ESCALA_II (NR_SEQUENCIA),
  CONSTRAINT EIFESRE_TRICLRI_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_CLASSIF_RISCO (NR_SEQUENCIA));

GRANT SELECT ON TASY.EIF_ESCALA_II_RESULT TO NIVEL_1;

