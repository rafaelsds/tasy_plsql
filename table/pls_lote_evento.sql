ALTER TABLE TASY.PLS_LOTE_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_EVENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_COMPETENCIA       NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_COMPETENCIA           DATE,
  DT_GERACAO_TITULOS       DATE,
  DT_LIBERACAO             DATE,
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  IE_ORIGEM                VARCHAR2(1 BYTE),
  DT_INICIO_COMP           DATE,
  DT_FIM_COMP              DATE,
  NR_SEQ_LOTE_PAGAMENTO    NUMBER(10),
  NR_LOTE_CONTABIL         NUMBER(10),
  DT_GERACAO_BOLETO        DATE,
  NR_SEQ_EVENTO            NUMBER(10),
  NR_SEQ_LOTE_PLANT        NUMBER(10),
  DT_TITULOS_TERCEIROS     DATE,
  NR_SEQ_FRANQ_PAG         NUMBER(10),
  NR_SEQ_LOTE_PGTO_APROPR  NUMBER(10),
  NR_SEQ_PGTO_DESC_LIQ     NUMBER(10),
  NR_SEQ_LOTE_RET_TRIB     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOEV_ESTABEL_FK_I ON TASY.PLS_LOTE_EVENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_LOTCONT_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOEV_PK ON TASY.PLS_LOTE_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_PLSCOMP_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_PLSCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_PLSEVEN_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_PLSFRPA_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_FRANQ_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOEV_PLSLOPA_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_LOTE_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_PLSLOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_PLSLOPA_FK2_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_LOTE_PGTO_APROPR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOEV_PLSLOPA_FK3_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_PGTO_DESC_LIQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOEV_PLSLOPL_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_LOTE_PLANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOEV_PLSLOPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOEV_PLSLORT_FK_I ON TASY.PLS_LOTE_EVENTO
(NR_SEQ_LOTE_RET_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_lote_evento_del
before delete ON TASY.PLS_LOTE_EVENTO for each row
declare
qt_rotina_w		number(15);


begin
select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= 	(select userenv('sessionid')
			from 	dual)
and	username = 	(select username
			from 	v$session
			where 	audsid = (	select userenv('sessionid')
						from dual))
and	action like 'PLS_DESFAZER_LOTE_PAGAMENTO%';

if	(qt_rotina_w = 0) and
	(:old.nr_seq_lote_pagamento is not null) and
	(:old.ie_origem = 'A') then
	wheb_mensagem_pck.exibir_mensagem_abort(271816, 'NR_SEQ_LOTE_PGTO=' || :old.nr_seq_lote_pagamento);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_lote_evento_update
before update ON TASY.PLS_LOTE_EVENTO for each row
declare

qt_eventos_divergentes_w 	number(15);

begin
if	(:new.nr_seq_lote_pagamento <> :old.nr_seq_lote_pagamento) then
	select	count(1)
	into	qt_eventos_divergentes_w
	from	pls_evento_movimento a
	where	a.nr_seq_lote 		= :old.nr_sequencia
	and	a.nr_seq_lote_pgto 	<> :new.nr_seq_lote_pagamento
	and	rownum <= 1;

	if	(qt_eventos_divergentes_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(215009);
	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_LOTE_EVENTO ADD (
  CONSTRAINT PLSLOEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_EVENTO ADD (
  CONSTRAINT PLSLOEV_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSLOPL_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PLANT) 
 REFERENCES TASY.PLS_LOTE_PLANTONISTA (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSFRPA_FK 
 FOREIGN KEY (NR_SEQ_FRANQ_PAG) 
 REFERENCES TASY.PLS_FRANQ_PAG (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSLOPA_FK2 
 FOREIGN KEY (NR_SEQ_LOTE_PGTO_APROPR) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSLOPA_FK3 
 FOREIGN KEY (NR_SEQ_PGTO_DESC_LIQ) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSLORT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RET_TRIB) 
 REFERENCES TASY.PLS_LOTE_RETENCAO_TRIB (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_PLSCOMP_FK 
 FOREIGN KEY (NR_SEQ_COMPETENCIA) 
 REFERENCES TASY.PLS_COMPETENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSLOEV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOEV_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSLOEV_PLSLOPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PAGAMENTO) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_EVENTO TO NIVEL_1;

