ALTER TABLE TASY.COM_CLIENTE_INF_FUNCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLIENTE_INF_FUNCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLIENTE_INF_FUNCAO
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NR_SEQ_INFORMACAO  NUMBER(10)                 NOT NULL,
  CD_FUNCAO          NUMBER(10)                 NOT NULL,
  IE_TIPO_ACESSO     VARCHAR2(1 BYTE)           NOT NULL,
  QT_ACESSO          NUMBER(10)                 NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLIINF_COMCLIN_FK_I ON TASY.COM_CLIENTE_INF_FUNCAO
(NR_SEQ_INFORMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIINF_COMCLIN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMCLIINF_PK ON TASY.COM_CLIENTE_INF_FUNCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIINF_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_CLIENTE_INF_FUNCAO ADD (
  CONSTRAINT COMCLIINF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLIENTE_INF_FUNCAO ADD (
  CONSTRAINT COMCLIINF_COMCLIN_FK 
 FOREIGN KEY (NR_SEQ_INFORMACAO) 
 REFERENCES TASY.COM_CLIENTE_INFORMACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COM_CLIENTE_INF_FUNCAO TO NIVEL_1;

