ALTER TABLE TASY.CM_CICLO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CICLO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CICLO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_SEQ_EQUIPAMENTO         NUMBER(10)         NOT NULL,
  NR_CICLO                   NUMBER(15)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  QT_TEMPERATURA             NUMBER(6,2),
  QT_PRESSAO                 NUMBER(6,2),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_PRESSAO_ALTO_NIVEL      VARCHAR2(1 BYTE),
  IE_TEMPERATURA_ALTO_NIVEL  VARCHAR2(1 BYTE),
  CD_CICLO                   VARCHAR2(80 BYTE),
  NR_SEQ_CLASSIF             NUMBER(10),
  DT_CONFERENCIA             DATE,
  NM_USUARIO_CONFERENCIA     VARCHAR2(15 BYTE),
  DT_CANCELAMENTO            DATE,
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_CANCEL       NUMBER(10),
  IE_CICLO_LAVADORA          VARCHAR2(1 BYTE),
  NR_CICLO_CONTROLE          NUMBER(15),
  DT_IMPRESSAO               DATE,
  QT_PRESSAO_NEGATIVA        NUMBER(6,2),
  IE_USO_IMEDIATO            VARCHAR2(1 BYTE),
  NM_USUARIO_PREPARO         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCICLO_CMCLMET_FK_I ON TASY.CM_CICLO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_CMCLMET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCICLO_CMEQUIP_FK_I ON TASY.CM_CICLO
(NR_SEQ_EQUIPAMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_CMEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCICLO_CMMOCAC_FK_I ON TASY.CM_CICLO
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_CMMOCAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCICLO_ESTABEL_FK_I ON TASY.CM_CICLO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCICLO_PK ON TASY.CM_CICLO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCICLO_SETATEN_FK_I ON TASY.CM_CICLO
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_SETATEN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCICLO_UK ON TASY.CM_CICLO
(NR_CICLO, NR_SEQ_EQUIPAMENTO, NR_CICLO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCICLO_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_ciclo_after
after insert or update ON TASY.CM_CICLO 
for each row
declare

gerar_ceq_cod_ciclo_w	varchar2(2);

begin

if (inserting) then

	Obter_Param_Usuario(406, 48, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, gerar_ceq_cod_ciclo_w);
					
	gravar_log_tasy(675, 'Valor par�metro [48]: ' || gerar_ceq_cod_ciclo_w ||
				' - Seq ciclo: ' || :new.nr_sequencia ||
				' - Equipamento: ' || :new.nr_seq_equipamento ||	
				' - Ciclo: ' || :new.nr_ciclo ||		
				' - Lote: ' || :new.cd_ciclo ||
				' - Controle: ' || :new.nr_ciclo_controle ||
				' - Observa��o: ' || :new.ds_observacao ||
				' - Data Inicio: ' || :new.dt_inicio ||
				' - Data fim: ' || :new.dt_fim, :new.nm_usuario);

elsif (updating) then

	Obter_Param_Usuario(406, 48, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, gerar_ceq_cod_ciclo_w);

	gravar_log_tasy(675, 'Valor par�metro [48]: ' || gerar_ceq_cod_ciclo_w ||
				' - Seq ciclo: ' || :new.nr_sequencia ||			
				' - Equipamento antigo: ' || :old.nr_seq_equipamento ||
				' - Equipamento novo: ' || :new.nr_seq_equipamento ||			
				' - Ciclo antigo: ' || :old.nr_ciclo ||
				' - Ciclo novo: ' || :new.nr_ciclo ||				
				' - Lote antigo: ' || :old.cd_ciclo ||
				' - Lote novo: ' || :new.cd_ciclo || 			
				' - Controle antigo: ' || :old.nr_ciclo_controle ||
				' - Controle novo: ' || :new.nr_ciclo_controle ||		
				' - Observa��o antigo: ' || :old.ds_observacao ||
				' - Observa��o novo: ' || :new.ds_observacao ||	
				' - Data Inicio antigo: ' || :old.dt_inicio ||
				' - Data Inicio novo: ' || :new.dt_inicio ||
				' - Data fim antigo: ' || :old.dt_fim ||
				' - Data fim novo: ' || :new.dt_fim, :new.nm_usuario);

	if	(:old.nr_seq_equipamento <> :new.nr_seq_equipamento) then

		cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311186,'NR_SEQ_EQUIPAMENTO='||:old.nr_seq_equipamento||';NR_SEQ_EQUIPAMENTO_NEW='||:new.nr_seq_equipamento),
					'A',
					:new.nr_sequencia,
					null,
					:new.nm_usuario);

	end if;

	if	(:old.dt_fim is not null) and (:new.dt_fim is not null) then

		if	(:old.nr_seq_classif <> :new.nr_seq_classif) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311187,'NR_SEQ_CLASSIF='||:old.nr_seq_classif||';NR_SEQ_CLASSIF_NEW='||:new.nr_seq_classif),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.nr_ciclo <> :new.nr_ciclo) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311191,'NR_CICLO='||:old.nr_ciclo||';NR_CICLO_NEW='||:new.nr_ciclo),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.cd_ciclo <> :new.cd_ciclo) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311192,'CD_CICLO='||:old.cd_ciclo||';CD_CICLO_NEW='||:new.cd_ciclo),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.qt_temperatura <> :new.qt_temperatura) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311195,'QT_TEMPERATURA='||:old.qt_temperatura||';QT_TEMPERATURA_NEW='||:new.qt_temperatura),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.ds_observacao <> :new.ds_observacao) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311196,'DS_OBSERVACAO='||:old.ds_observacao||';DS_OBSERVACAO_NEW='||:new.ds_observacao),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.qt_pressao <> :new.qt_pressao) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311197,'QT_PRESSAO='||:old.qt_pressao||';QT_PRESSAO_NEW='||:new.qt_pressao),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.ie_pressao_alto_nivel <> :new.ie_pressao_alto_nivel) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311199,'IE_PRESSAO_ALTO_NIVEL='||:old.ie_pressao_alto_nivel||';IE_PRESSAO_ALTO_NIVEL_NEW='||:new.ie_pressao_alto_nivel),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

		if	(:old.ie_temperatura_alto_nivel <> :new.ie_temperatura_alto_nivel) then

			cm_gerar_hist_ciclo(	wheb_mensagem_pck.get_texto(311201,'IE_TEMPERATURA_ALTO_NIVEL='||:old.ie_temperatura_alto_nivel||';IE_TEMPERATURA_ALTO_NIVEL_NEW='||:new.ie_temperatura_alto_nivel),
						'A',
						:new.nr_sequencia,
						null,
						:new.nm_usuario);

		end if;

	end if;

	--desfazendo um ciclo, removemos o hist�rico/contagem
	if	(:old.dt_fim is not null) and (:new.dt_fim is null) then

		delete from cm_contagem_ciclo
		where nr_seq_ciclo = :new.nr_sequencia;
		
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cm_ciclo_update
before update ON TASY.CM_CICLO for each row
declare

nr_seq_conj_cont_w		cm_conjunto_cont.nr_sequencia%type;

cursor c01 is
	select	nr_sequencia
	from	cm_conjunto_cont
	where	nr_seq_ciclo = :new.nr_sequencia;

begin

	--finalizando um ciclo, geramos hist�rico/contagem para todos os conjuntos do ciclo finalizado.
	if	((:new.dt_fim is not null) and (:old.dt_fim is null)) then

		open c01;
		loop
		fetch c01 into
			nr_seq_conj_cont_w;
		exit when c01%notfound;
			begin
				insert into cm_contagem_ciclo (
						  nr_sequencia,
						  dt_atualizacao,
						  nm_usuario,
						  dt_atualizacao_nrec,
						  nm_usuario_nrec,
						  nr_seq_ciclo,
						  nr_seq_conj_cont)
				values (
						  cm_contagem_ciclo_seq.nextval,
						  sysdate,
						  :new.nm_usuario,
						  sysdate,
						  :new.nm_usuario,
						  :new.nr_sequencia,
						  nr_seq_conj_cont_w);
			end;
		end loop;
		close c01;

	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CM_CICLO_DELETE
BEFORE DELETE
  ON TASY.CM_CICLO   FOR EACH ROW
BEGIN

gravar_log_tasy(675, obter_desc_expressao(493387) ||' - '|| obter_desc_expressao(739907) || ': ' || :old.nr_sequencia ||
				' - ' || obter_desc_expressao(10651978) || ': '|| :old.nr_seq_equipamento ||
				' - ' ||obter_desc_expressao(612147) || ': '|| :old.nr_ciclo, :old.nm_usuario);

END;
/


ALTER TABLE TASY.CM_CICLO ADD (
  CONSTRAINT CMCICLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CMCICLO_UK
 UNIQUE (NR_CICLO, NR_SEQ_EQUIPAMENTO, NR_CICLO_CONTROLE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CICLO ADD (
  CONSTRAINT CMCICLO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMCICLO_CMEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.CM_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT CMCICLO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CMCICLO_CMCLMET_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CM_CLASSIF_METODO (NR_SEQUENCIA),
  CONSTRAINT CMCICLO_CMMOCAC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.CM_MOTIVO_CANCEL_CICLO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_CICLO TO NIVEL_1;

