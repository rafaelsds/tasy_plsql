ALTER TABLE TASY.PROJ_ATA_ATIVIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_ATA_ATIVIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_ATA_ATIVIDADE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATA           NUMBER(10)               NOT NULL,
  NR_SEQ_ATIVIDADE     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRATAAT_PK ON TASY.PROJ_ATA_ATIVIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRATAAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRATAAT_PROCRET_FK_I ON TASY.PROJ_ATA_ATIVIDADE
(NR_SEQ_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRATAAT_PROCRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRATAAT_PROJATA_FK_I ON TASY.PROJ_ATA_ATIVIDADE
(NR_SEQ_ATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRATAAT_PROJATA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_ATA_ATIVIDADE ADD (
  CONSTRAINT PRATAAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_ATA_ATIVIDADE ADD (
  CONSTRAINT PRATAAT_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_ATIVIDADE) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT PRATAAT_PROJATA_FK 
 FOREIGN KEY (NR_SEQ_ATA) 
 REFERENCES TASY.PROJ_ATA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_ATA_ATIVIDADE TO NIVEL_1;

