ALTER TABLE TASY.PEPO_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_PARTICIPANTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  IE_FUNCAO                  VARCHAR2(10 BYTE)  NOT NULL,
  NR_SEQ_PEPO                NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPOPAR_PEPOCIR_FK_I ON TASY.PEPO_PARTICIPANTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOPAR_PESFISI_FK_I ON TASY.PEPO_PARTICIPANTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPOPAR_PK ON TASY.PEPO_PARTICIPANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOPAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PEPOPAR_TASASDI_FK_I ON TASY.PEPO_PARTICIPANTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOPAR_TASASDI_FK2_I ON TASY.PEPO_PARTICIPANTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pepo_participante_pend_atual
after insert or update ON TASY.PEPO_PARTICIPANTE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_libera_partic_w	varchar2(10);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if 	(cd_pessoa_fisica_w is null) then
	select	max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	pepo_cirurgia c
	where	c.nr_sequencia = :new.nr_seq_pepo;
end if;

select	max(ie_libera_partic)
into	ie_libera_partic_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_libera_partic_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'PP';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XPP';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PEPO_PARTICIPANTE ADD (
  CONSTRAINT PEPOPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEPO_PARTICIPANTE ADD (
  CONSTRAINT PEPOPAR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPOPAR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPOPAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPOPAR_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PEPO_PARTICIPANTE TO NIVEL_1;

