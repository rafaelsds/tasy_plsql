ALTER TABLE TASY.PE_INTERV_REGRA_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_INTERV_REGRA_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_INTERV_REGRA_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_INTERVALO          VARCHAR2(7 BYTE),
  DS_OBSERVACAO_PADR    VARCHAR2(255 BYTE),
  NR_SEQ_PROC           NUMBER(10)              NOT NULL,
  DS_HORARIO_PADRAO     VARCHAR2(255 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEINRES_INTPRES_FK_I ON TASY.PE_INTERV_REGRA_SETOR
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINRES_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEINRES_PEPROCE_FK_I ON TASY.PE_INTERV_REGRA_SETOR
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINRES_PEPROCE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEINRES_PK ON TASY.PE_INTERV_REGRA_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINRES_PK
  MONITORING USAGE;


CREATE INDEX TASY.PEINRES_SETATEN_FK_I ON TASY.PE_INTERV_REGRA_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEINRES_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PE_INTERV_REGRA_SETOR ADD (
  CONSTRAINT PEINRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_INTERV_REGRA_SETOR ADD (
  CONSTRAINT PEINRES_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PEINRES_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PEINRES_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PE_INTERV_REGRA_SETOR TO NIVEL_1;

