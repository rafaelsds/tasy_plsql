ALTER TABLE TASY.IMP_MATERIAL_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMP_MATERIAL_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_MATERIAL_ESTAB
(
  NR_SEQUENCIA            NUMBER(10),
  CD_ESTABELECIMENTO      NUMBER(4),
  DT_ATUALIZACAO          DATE,
  NM_USUARIO              VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_DIRTY_CHECK          NUMBER(1),
  CD_MATERIAL             NUMBER(10)            DEFAULT null,
  IE_BAIXA_ESTOQ_PAC      VARCHAR2(1 BYTE),
  IE_MATERIAL_ESTOQUE     VARCHAR2(1 BYTE),
  QT_ESTOQUE_MINIMO       NUMBER(13,4),
  QT_PONTO_PEDIDO         NUMBER(13,4),
  QT_ESTOQUE_MAXIMO       NUMBER(15,4),
  NR_MINIMO_COTACAO       NUMBER(1),
  IE_RESSUPRIMENTO        VARCHAR2(1 BYTE),
  IE_CLASSIF_CUSTO        VARCHAR2(1 BYTE),
  IE_PRESCRICAO           VARCHAR2(1 BYTE),
  IE_PADRONIZADO          VARCHAR2(1 BYTE),
  IE_ESTOQUE_LOTE         VARCHAR2(2 BYTE),
  IE_REQUISICAO           VARCHAR2(1 BYTE),
  IE_CONTROLA_SERIE       VARCHAR2(1 BYTE),
  IE_UNID_CONSUMO_PRESCR  VARCHAR2(1 BYTE),
  NR_SEQ_MIMS_VERSION     NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MAT_ESTAB_ ON TASY.IMP_MATERIAL_ESTAB
(CD_ESTABELECIMENTO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MAT_ESTAB_ESTABEL_FK_I ON TASY.IMP_MATERIAL_ESTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MAT_ESTAB_IMPMATE_FK_I ON TASY.IMP_MATERIAL_ESTAB
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MAT_ESTAB_PK ON TASY.IMP_MATERIAL_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_MATERIAL_ESTAB ADD (
  CONSTRAINT MAT_ESTAB_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMP_MATERIAL_ESTAB ADD (
  CONSTRAINT MAT_ESTAB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MAT_ESTAB_IMPMATE_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.IMP_MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMP_MATERIAL_ESTAB TO NIVEL_1;

