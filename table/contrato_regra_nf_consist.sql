ALTER TABLE TASY.CONTRATO_REGRA_NF_CONSIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO_REGRA_NF_CONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO_REGRA_NF_CONSIST
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_NF       NUMBER(10)              NOT NULL,
  IE_TIPO_CONSISTENCIA  VARCHAR2(15 BYTE)       NOT NULL,
  VL_MINIMO             NUMBER(15,2)            NOT NULL,
  VL_MAXIMO             NUMBER(15,2)            NOT NULL,
  IE_JUSTIFICATIVA      VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CORNCON_CONRENF_FK_I ON TASY.CONTRATO_REGRA_NF_CONSIST
(NR_SEQ_REGRA_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CORNCON_PK ON TASY.CONTRATO_REGRA_NF_CONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CONTRATO_REGRA_NF_CONSIST_tp  after update ON TASY.CONTRATO_REGRA_NF_CONSIST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_CONSISTENCIA,1,4000),substr(:new.IE_TIPO_CONSISTENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONSISTENCIA',ie_log_w,ds_w,'CONTRATO_REGRA_NF_CONSIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICATIVA,1,4000),substr(:new.IE_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICATIVA',ie_log_w,ds_w,'CONTRATO_REGRA_NF_CONSIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAXIMO,1,4000),substr(:new.VL_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAXIMO',ie_log_w,ds_w,'CONTRATO_REGRA_NF_CONSIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO,1,4000),substr(:new.VL_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO',ie_log_w,ds_w,'CONTRATO_REGRA_NF_CONSIST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CONTRATO_REGRA_NF_CONSIST ADD (
  CONSTRAINT CORNCON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO_REGRA_NF_CONSIST ADD (
  CONSTRAINT CORNCON_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_NF) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CONTRATO_REGRA_NF_CONSIST TO NIVEL_1;

