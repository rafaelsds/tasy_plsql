ALTER TABLE TASY.COT_COMPRA_FORN_ITEM_EN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA_FORN_ITEM_EN CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA_FORN_ITEM_EN
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_COT_ITEM_FORN  NUMBER(10)              NOT NULL,
  DT_ENTREGA            DATE                    NOT NULL,
  QT_ENTREGA            NUMBER(13,4)            NOT NULL,
  NR_COT_COMPRA         NUMBER(10)              NOT NULL,
  NR_ITEM_COT_COMPRA    NUMBER(5)               NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COCOFIE_COTCOFI_FK_I ON TASY.COT_COMPRA_FORN_ITEM_EN
(NR_SEQ_COT_ITEM_FORN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOFIE_COTCOIT_FK_I ON TASY.COT_COMPRA_FORN_ITEM_EN
(NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOFIE_COTCOMP_FK_I ON TASY.COT_COMPRA_FORN_ITEM_EN
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOFIE_COTCOMP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COCOFIE_PK ON TASY.COT_COMPRA_FORN_ITEM_EN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOFIE_PK
  MONITORING USAGE;


ALTER TABLE TASY.COT_COMPRA_FORN_ITEM_EN ADD (
  CONSTRAINT COCOFIE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA_FORN_ITEM_EN ADD (
  CONSTRAINT COCOFIE_COTCOFI_FK 
 FOREIGN KEY (NR_SEQ_COT_ITEM_FORN) 
 REFERENCES TASY.COT_COMPRA_FORN_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COCOFIE_COTCOMP_FK 
 FOREIGN KEY (NR_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA (NR_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COCOFIE_COTCOIT_FK 
 FOREIGN KEY (NR_COT_COMPRA, NR_ITEM_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA_ITEM (NR_COT_COMPRA,NR_ITEM_COT_COMPRA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COT_COMPRA_FORN_ITEM_EN TO NIVEL_1;

