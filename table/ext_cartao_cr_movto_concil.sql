ALTER TABLE TASY.EXT_CARTAO_CR_MOVTO_CONCIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXT_CARTAO_CR_MOVTO_CONCIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXT_CARTAO_CR_MOVTO_CONCIL
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_EXT_MOVTO        NUMBER(10)            NOT NULL,
  VL_CONCILIADO           NUMBER(15,2)          NOT NULL,
  NR_SEQ_EXTRATO_PARCELA  NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ECMCONC_EXTCCM_FK_I ON TASY.EXT_CARTAO_CR_MOVTO_CONCIL
(NR_SEQ_EXT_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ECMCONC_EXTCCM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ECMCONC_EXTCCP_FK_I ON TASY.EXT_CARTAO_CR_MOVTO_CONCIL
(NR_SEQ_EXTRATO_PARCELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ECMCONC_EXTCCP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ECMCONC_PK ON TASY.EXT_CARTAO_CR_MOVTO_CONCIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ECMCONC_PK
  MONITORING USAGE;


ALTER TABLE TASY.EXT_CARTAO_CR_MOVTO_CONCIL ADD (
  CONSTRAINT ECMCONC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXT_CARTAO_CR_MOVTO_CONCIL ADD (
  CONSTRAINT ECMCONC_EXTCCP_FK 
 FOREIGN KEY (NR_SEQ_EXTRATO_PARCELA) 
 REFERENCES TASY.EXTRATO_CARTAO_CR_PARCELA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ECMCONC_EXTCCM_FK 
 FOREIGN KEY (NR_SEQ_EXT_MOVTO) 
 REFERENCES TASY.EXTRATO_CARTAO_CR_MOVTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.EXT_CARTAO_CR_MOVTO_CONCIL TO NIVEL_1;

