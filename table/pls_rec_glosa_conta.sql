ALTER TABLE TASY.PLS_REC_GLOSA_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REC_GLOSA_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REC_GLOSA_CONTA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PROTOCOLO        NUMBER(10)            NOT NULL,
  NR_SEQ_CONTA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_IMP        NUMBER(10),
  DS_JUSTIFICATIVA_OPER   VARCHAR2(255 BYTE),
  IE_STATUS               VARCHAR2(5 BYTE),
  VL_TOTAL_RECURSADO      NUMBER(10,2),
  VL_TOTAL_ACATADO        NUMBER(15,2),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  DS_JUSTIFICATIVA_PREST  VARCHAR2(4000 BYTE),
  NR_SEQ_ANALISE          NUMBER(10),
  IE_ACATADO_GLOSA        VARCHAR2(5 BYTE),
  NR_SEQ_TIPO_CONTA       NUMBER(10),
  CD_GUIA_REF             VARCHAR2(20 BYTE),
  IE_ACAO_TOTAL           VARCHAR2(5 BYTE),
  DT_CANCELAMENTO         DATE,
  CD_GUIA_REC             VARCHAR2(30 BYTE),
  CD_GUIA_PRESTADOR_REC   VARCHAR2(30 BYTE),
  CD_SENHA_REC            VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCO_PK ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRGCO_PLSANCO_FK_I ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQ_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCO_PLSCOME_FK_I ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCO_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRGCO_PLSTPCO_FK_I ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQ_TIPO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCO_PRGCIMP_FK_I ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQ_CONTA_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCO_PRGCIMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRGCO_PRGPROT_FK_I ON TASY.PLS_REC_GLOSA_CONTA
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCO_PRGPROT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_rec_glosa_cta_tiss_update
after update ON TASY.PLS_REC_GLOSA_CONTA for each row
declare

qt_rec_glosa_tiss_w	number(10);

Cursor C01 (nr_seq_conta_rec_pc	pls_rec_glosa_conta.nr_sequencia%type) is
	select	a.nr_seq_conta_proc,
		null nr_seq_conta_mat,
		a.vl_acatado,
		a.nr_sequencia nr_seq_proc_rec,
		null nr_seq_mat_rec
	from	pls_rec_glosa_proc	a
	where	a.nr_seq_conta_rec	= nr_seq_conta_rec_pc
	union all
	select	null nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		a.vl_acatado,
		null nr_seq_proc_rec,
		a.nr_sequencia nr_seq_mat_rec
	from	pls_rec_glosa_mat	a
	where	a.nr_seq_conta_rec	= nr_seq_conta_rec_pc;

begin
-- Gravar log TISS para quando ocorrer quando fecha o recurso de glosa

select	count(1)
into	qt_rec_glosa_tiss_w
from	pls_monitor_tiss_alt
where	nr_seq_conta 	= :new.nr_seq_conta
and	ie_tipo_evento 	= 'FR';

if	(:new.ie_status = '2') and (nvl(qt_rec_glosa_tiss_w,0) = 0) then
	for	r_c01_w in c01 (:new.nr_sequencia) loop
		pls_tiss_gravar_log_monitor(	'FR', :new.nr_seq_conta, r_c01_w.nr_seq_conta_proc,
						r_c01_w.nr_seq_conta_mat, :new.nr_sequencia, r_c01_w.nr_seq_proc_rec,
						r_c01_w.nr_seq_mat_rec, null, null,
						null, r_c01_w.vl_acatado, wheb_usuario_pck.get_nm_usuario,sysdate);
	end loop;
end if;

end;
/


ALTER TABLE TASY.PLS_REC_GLOSA_CONTA ADD (
  CONSTRAINT PLSRGCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REC_GLOSA_CONTA ADD (
  CONSTRAINT PLSRGCO_PLSANCO_FK 
 FOREIGN KEY (NR_SEQ_ANALISE) 
 REFERENCES TASY.PLS_ANALISE_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSRGCO_PLSTPCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONTA) 
 REFERENCES TASY.PLS_TIPO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSRGCO_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSRGCO_PRGCIMP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA_IMP (NR_SEQUENCIA),
  CONSTRAINT PLSRGCO_PRGPROT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_REC_GLOSA_PROTOCOLO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_REC_GLOSA_CONTA TO NIVEL_1;

