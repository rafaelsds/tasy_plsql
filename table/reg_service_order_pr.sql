ALTER TABLE TASY.REG_SERVICE_ORDER_PR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_SERVICE_ORDER_PR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_SERVICE_ORDER_PR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SERVICE_ORDER        NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_PRODUCT_REQUIREMENT  NUMBER(10)            NOT NULL,
  IE_IMPACT               VARCHAR2(255 BYTE),
  IE_TIPO_ALTERACAO       VARCHAR2(255 BYTE),
  NR_SEQ_OBJ              NUMBER(10),
  IE_TIPO_OBJ             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGOSPR_PK ON TASY.REG_SERVICE_ORDER_PR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_service_order_pr_after
after insert ON TASY.REG_SERVICE_ORDER_PR for each row
declare

ie_plataforma_w		man_ordem_servico.ie_plataforma%type;

begin

select	max(ie_plataforma)
into	ie_plataforma_w
from	man_ordem_servico
where	nr_sequencia = :new.nr_service_order;

if (ie_plataforma_w = 'H') then
	reg_test_plan_pck.add_pendency('S', :new.nr_service_order, null, :new.nm_usuario_nrec);
end if;

end;
/


ALTER TABLE TASY.REG_SERVICE_ORDER_PR ADD (
  CONSTRAINT REGOSPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REG_SERVICE_ORDER_PR TO NIVEL_1;

