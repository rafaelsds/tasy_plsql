ALTER TABLE TASY.REGRA_PERM_AGENDA_ESPEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PERM_AGENDA_ESPEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PERM_AGENDA_ESPEC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_MESMO_ESTAB        VARCHAR2(1 BYTE)        NOT NULL,
  IE_MESMA_AGENDA       VARCHAR2(1 BYTE)        NOT NULL,
  IE_MESMO_MEDICO       VARCHAR2(1 BYTE)        NOT NULL,
  IE_ACAO               VARCHAR2(1 BYTE)        NOT NULL,
  IE_CONSIDERA_RETORNO  VARCHAR2(1 BYTE),
  NR_DIAS_VALIDACAO     NUMBER(3)               NOT NULL,
  CD_ESPECIALIDADE      NUMBER(5)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPAGESP_ESPMEDI_FK_I ON TASY.REGRA_PERM_AGENDA_ESPEC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPAGESP_ESTABEL_FK_I ON TASY.REGRA_PERM_AGENDA_ESPEC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RPAGESP_PK ON TASY.REGRA_PERM_AGENDA_ESPEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_PERM_AGENDA_ESPEC ADD (
  CONSTRAINT RPAGESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGRA_PERM_AGENDA_ESPEC ADD (
  CONSTRAINT RPAGESP_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT RPAGESP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_PERM_AGENDA_ESPEC TO NIVEL_1;

