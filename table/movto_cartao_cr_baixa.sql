ALTER TABLE TASY.MOVTO_CARTAO_CR_BAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVTO_CARTAO_CR_BAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVTO_CARTAO_CR_BAIXA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOVTO         NUMBER(10)               NOT NULL,
  DT_BAIXA             DATE                     NOT NULL,
  VL_BAIXA             NUMBER(15,2)             NOT NULL,
  NR_SEQ_CONTA_BANCO   NUMBER(10),
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_PARCELA       NUMBER(10),
  VL_DESPESA           NUMBER(15,2),
  VL_DESP_EQUIP        NUMBER(15,2),
  NR_SEQ_LOTE          NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10),
  TX_ANTECIPACAO       NUMBER(7,4),
  NR_SEQ_BAIXA_ORIG    NUMBER(10),
  VL_IMPOSTO           NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOCRBAI_BANESTA_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOCRBAI_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOCRBAI_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(DT_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOCRBAI_LBCART_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOCRBAI_LBCART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOCRBAI_LOTCONT_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOCRBAI_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOCRBAI_MOCRBAI_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_BAIXA_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOCRBAI_MOCRPAR_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_PARCELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOCRBAI_MOVCACR_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOCRBAI_PK ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOCRBAI_PK
  MONITORING USAGE;


CREATE INDEX TASY.MOCRBAI_TRAFINA_FK_I ON TASY.MOVTO_CARTAO_CR_BAIXA
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOCRBAI_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.movto_cartao_cr_baixa_after
after insert or update ON TASY.MOVTO_CARTAO_CR_BAIXA for each row
declare

qt_reg_w                number(10);
reg_integracao_p        gerar_int_padrao.reg_integracao;

cd_estabelecimento_w	ctb_documento.cd_estabelecimento%type;
vl_movimento_w		ctb_documento.vl_movimento%type;
ie_contab_cartao_cr_w	parametro_contas_receber.ie_contab_cartao_cr%type;

cursor c01 is
	select	a.nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 5
	and 	a.nm_atributo in ('VL_BAIXA_CARTAO_CR', 'VL_DESP_EQUIP_CARTAO_CR', 'VL_DESPESA_CARTAO_CR');

c01_w		c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  count(*)
into    qt_reg_w
from    intpd_fila_transmissao
where   nr_seq_documento        = to_char(:new.nr_sequencia)
and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
and     ie_evento       in ('324');


if (qt_reg_w    = 0) then
        gerar_int_padrao.gravar_integracao('324', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
end if;

if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_seq_parcela,'CCB',:new.dt_baixa,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_movto,:new.nr_seq_parcela,'CCB',:new.dt_baixa,'A',:new.nm_usuario);
end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	movto_cartao_cr
where	nr_sequencia = :new.nr_seq_movto;

begin
select	nvl(ie_contab_cartao_cr, 'CB')
into	ie_contab_cartao_cr_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;
exception when others then
        ie_contab_cartao_cr_w := 'CB';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_cartao_cr, 'CB'), ie_contab_cartao_cr_w)
into    ie_contab_cartao_cr_w
from    ctb_param_lote_contas_rec a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	vl_movimento_w  :=      case	c01_w.nm_atributo
				when	'VL_BAIXA_CARTAO_CR'      then :new.vl_baixa
				when	'VL_DESP_EQUIP_CARTAO_CR' then :new.vl_desp_equip
				when	'VL_DESPESA_CARTAO_CR'    then :new.vl_despesa
				else
					null
				end;

	if	(nvl(vl_movimento_w, 0) <> 0) and (ie_contab_cartao_cr_w = 'CR') and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									:new.dt_baixa,
									5,
									:new.nr_seq_trans_financ,
									44,
									:new.nr_seq_movto,
									:new.nr_sequencia,
									0,
									vl_movimento_w,
									'MOVTO_CARTAO_CR_BAIXA',
									c01_w.nm_atributo,
									:new.nm_usuario);

		end;
	end if;

	end;
end loop;
close c01;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.movto_cartao_cr_baixa_del
before delete ON TASY.MOVTO_CARTAO_CR_BAIXA for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_movto,:old.nr_seq_parcela,'CCB',:old.dt_baixa,'E',:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MOVTO_CARTAO_CR_BAIXA_INSERT
BEFORE INSERT ON TASY.MOVTO_CARTAO_CR_BAIXA FOR EACH ROW
DECLARE

dt_transacao_w			date;
ds_comprovante_w		varchar2(100);
nr_autorizacao_w			varchar2(40);
ie_permite_sem_mes_bco_w		varchar2(1);
cd_estabelecimento_w		number(4);
qt_saldo_banco_w			number(10);
ie_permite_data_retroativa_w	varchar2(1);


begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
consiste_movto_banco(:new.nr_seq_conta_banco,:new.dt_baixa);

/* Francisco - 24/10/2007 - Inclui o tratamento abaixo para nao haver problemas com posicao contabil */

select	max(dt_transacao),
	max(nr_autorizacao),
	max(ds_comprovante),
	max(cd_estabelecimento)
into	dt_transacao_w,
	nr_autorizacao_w,
	ds_comprovante_w,
	cd_estabelecimento_w
from	movto_cartao_cr
where	nr_sequencia	= :new.nr_seq_movto;

begin
obter_param_usuario(3020,30,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_permite_data_retroativa_w);
exception when others then
	ie_permite_data_retroativa_w	:= 'N';
end;

if	(ie_permite_data_retroativa_w = 'N') then
	if	(trunc(:new.dt_baixa,'dd') < trunc(dt_transacao_w,'dd')) then
		/*'A data da baixa nao pode ser inferior a data do movimento!'
		'Autorizacao: ' || nr_autorizacao_w || ', CV: ' || ds_comprovante_w); */
		wheb_mensagem_pck.exibir_mensagem_abort(237911,'NR_AUTORIZACAO_W=' || nr_autorizacao_w || ';DS_COMPROVANTE_W=' || ds_comprovante_w);
	end if;
end if;


select	count(*)
into	qt_saldo_banco_w
from	banco_saldo a
where	a.nr_seq_conta			= :new.nr_seq_conta_banco
and	trunc(a.dt_referencia,'mm')	= trunc(:new.dt_baixa,'mm');

if	(nvl(qt_saldo_banco_w,0) = 0) then
	begin
	obter_param_usuario(3020,18,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_permite_sem_mes_bco_w);
	exception when others then
		ie_permite_sem_mes_bco_w	:= 'S';
	end;
	if	(nvl(ie_permite_sem_mes_bco_w,'S') = 'N') then
		/*'Nao existe mes aberto no Controle Bancario! Parametro [18]'*/
		wheb_mensagem_pck.exibir_mensagem_abort(237913);
	end if;
end if;
end if;

end;
/


ALTER TABLE TASY.MOVTO_CARTAO_CR_BAIXA ADD (
  CONSTRAINT MOCRBAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVTO_CARTAO_CR_BAIXA ADD (
  CONSTRAINT MOCRBAI_MOCRBAI_FK 
 FOREIGN KEY (NR_SEQ_BAIXA_ORIG) 
 REFERENCES TASY.MOVTO_CARTAO_CR_BAIXA (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_MOVCACR_FK 
 FOREIGN KEY (NR_SEQ_MOVTO) 
 REFERENCES TASY.MOVTO_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_MOCRPAR_FK 
 FOREIGN KEY (NR_SEQ_PARCELA) 
 REFERENCES TASY.MOVTO_CARTAO_CR_PARCELA (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_LBCART_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_BAIXA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOCRBAI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL));

GRANT SELECT ON TASY.MOVTO_CARTAO_CR_BAIXA TO NIVEL_1;

