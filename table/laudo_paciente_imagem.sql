ALTER TABLE TASY.LAUDO_PACIENTE_IMAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE_IMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE_IMAGEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_IMAGEM           NUMBER(6)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_ARQUIVO_IMAGEM       VARCHAR2(4000 BYTE),
  DS_IMAGEM               VARCHAR2(80 BYTE),
  PACS_ID                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NM_USUARIO_EXCLUSAO     VARCHAR2(15 BYTE),
  DT_EXCLUSAO             DATE,
  DS_MOTIVO_EXCLUSAO      VARCHAR2(255 BYTE),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10),
  DT_LIBERACAO            DATE,
  NR_CONTROLE_EXT         NUMBER(15),
  DS_ARQUIVO_BANCO        LONG RAW,
  DS_PATOLOGIA_EXT        VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUPAIM_ATCONSPEPA_FK_I ON TASY.LAUDO_PACIENTE_IMAGEM
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPAIM_LAUPACI_FK_I ON TASY.LAUDO_PACIENTE_IMAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAUPAIM_MEDEXLA_FK_I ON TASY.LAUDO_PACIENTE_IMAGEM
(NR_SEQ_IMAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUPAIM_MEDEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LAUPAIM_PK ON TASY.LAUDO_PACIENTE_IMAGEM
(NR_SEQUENCIA, NR_SEQ_IMAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_laudo_paciente_imagem_ins
after insert ON TASY.LAUDO_PACIENTE_IMAGEM for each row
declare

nr_atendimento_w number(10);
cd_pessoa_fisica_w varchar2(10);
nr_prescricao_w number(14);
cd_medico_resp_w varchar2(10);
dt_exame_w       date;

begin

    select  nr_atendimento,
            cd_pessoa_fisica,
            nr_prescricao,
            cd_medico_resp,
            dt_exame
    into    nr_atendimento_w,
            cd_pessoa_fisica_w,
            nr_prescricao_w,
            cd_medico_resp_w,
            dt_exame_w
    from LAUDO_PACIENTE
    where nr_sequencia = :new.nr_sequencia;

    if :new.ds_imagem is null then
    
        insert into hsj_workflow_agent_laudo
        (
            nr_sequencia, 
            ie_situacao,
            dt_atualizacao_nrec, 
            nm_usuario_nrec, 
            dt_atualizacao, 
            nm_usuario,
            nr_seq_laudo_origem, 
            nr_atendimento, 
            cd_pessoa_fisica, 
            ie_status,
            ds_arquivo,
            cd_funcao_origem,
            nr_prescricao,
            cd_medico_resp,
            dt_exame,
            nr_seq_imagem
        )
        values
        (
            hsj_workflow_agent_laudo_seq.nextval,
            'A',
            sysdate,
            obter_usuario_ativo,
            sysdate,
            obter_usuario_ativo,
            :new.nr_sequencia,
             nr_atendimento_w,
             cd_pessoa_fisica_w,
            'P',
            :new.ds_arquivo_imagem,
            28,
            nr_prescricao_w,
            cd_medico_resp_w,
            dt_exame_w,
            :new.nr_seq_imagem
        );
        
    
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_imagem_atual
before insert or update ON TASY.LAUDO_PACIENTE_IMAGEM for each row
declare

ie_atualizar_arquivo_laudo_w	varchar2(10);

begin

obter_param_usuario(28, 281, obter_perfil_ativo,
			     :new.nm_usuario,
			     wheb_usuario_pck.get_cd_estabelecimento, ie_atualizar_arquivo_laudo_w);

if (nvl(ie_atualizar_arquivo_laudo_w,'N') = 'S') then

	update  laudo_paciente
	set	ds_arquivo = :new.ds_arquivo_imagem,
		ie_formato = 3
	where   nr_sequencia = :new.nr_sequencia;


end if;



end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_laudo_pac_imagem_delete
after delete ON TASY.LAUDO_PACIENTE_IMAGEM for each row
declare

qt_reg_w number(10);

begin

    select count(*)
    into qt_reg_w
    from HSJ_WORKFLOW_AGENT_LAUDO
    where nr_seq_laudo_origem = :old.nr_sequencia
    and nr_seq_imagem = :old.nr_seq_imagem
    and ie_status = 'P'
    and ie_situacao = 'A';

    if(qt_reg_w > 0)then
        update HSJ_WORKFLOW_AGENT_LAUDO set ie_situacao = 'I'
        where nr_seq_laudo_origem = :old.nr_sequencia
        and nr_seq_imagem = :old.nr_seq_imagem
        and ie_status = 'P'
        and ie_situacao = 'A';
    end if;

end hsj_laudo_pac_imagem_delete;
/


CREATE OR REPLACE TRIGGER TASY.laudo_paciente_imagem_delete
after delete ON TASY.LAUDO_PACIENTE_IMAGEM for each row
declare

ie_atualizar_arquivo_laudo_w	varchar2(10);

begin

ie_atualizar_arquivo_laudo_w := '';

/*obter_param_usuario(28, 281, obter_perfil_ativo,
			     :old.nm_usuario,
			     wheb_usuario_pck.get_cd_estabelecimento, ie_atualizar_arquivo_laudo_w);

if (nvl(ie_atualizar_arquivo_laudo_w,'N') = 'S') then

	update  laudo_paciente
	set	ds_arquivo = null,
		ie_formato = null
	where   nr_sequencia = :old.nr_sequencia;

end if;	*/

end;
/


ALTER TABLE TASY.LAUDO_PACIENTE_IMAGEM ADD (
  CONSTRAINT LAUPAIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_IMAGEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE_IMAGEM ADD (
  CONSTRAINT LAUPAIM_LAUPACI_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LAUPAIM_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAUDO_PACIENTE_IMAGEM TO NIVEL_1;

