ALTER TABLE TASY.TITULO_PAGAR_BAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_BAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_BAIXA
(
  NR_TITULO                NUMBER(10)           NOT NULL,
  NR_SEQUENCIA             NUMBER(5),
  DT_BAIXA                 DATE                 NOT NULL,
  CD_MOEDA                 NUMBER(5)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_TIPO_BAIXA            NUMBER(5)            NOT NULL,
  IE_ACAO                  VARCHAR2(1 BYTE)     NOT NULL,
  VL_BAIXA                 NUMBER(15,2)         NOT NULL,
  VL_DESCONTOS             NUMBER(15,2)         NOT NULL,
  VL_OUTRAS_DEDUCOES       NUMBER(15,2),
  VL_JUROS                 NUMBER(15,2)         NOT NULL,
  VL_MULTA                 NUMBER(15,2)         NOT NULL,
  VL_OUTROS_ACRESCIMOS     NUMBER(15,2),
  VL_PAGO                  NUMBER(15,2),
  CD_BANCO                 NUMBER(5),
  NR_LOTE_CONTABIL         NUMBER(10),
  VL_DEVOLUCAO             NUMBER(13,2)         NOT NULL,
  NR_SEQ_DEVOLUCAO         NUMBER(10),
  NR_BORDERO               NUMBER(10),
  VL_IR                    NUMBER(15,2),
  NR_SEQ_TRANS_FIN         NUMBER(10),
  NR_SEQ_CONTA_BANCO       NUMBER(10),
  NR_SEQ_ESCRIT            NUMBER(10),
  VL_MOEDA_ORIG            VARCHAR2(15 BYTE),
  VL_IMPOSTO_MUNIC         NUMBER(15,2),
  VL_INSS                  NUMBER(15,2),
  CD_CONTA_CONTABIL        VARCHAR2(20 BYTE),
  CD_CENTRO_CUSTO          NUMBER(8),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  NR_SEQ_MOVTO_TRANS_FIN   NUMBER(10),
  DT_INTEGRACAO_EXTERNA    DATE,
  VL_IMPOSTO               NUMBER(15,2),
  NR_EXTERNO               VARCHAR2(255 BYTE),
  NR_SEQ_CLASSIF           NUMBER(10),
  NR_SEQ_BAIXA_ORIGEM      NUMBER(10),
  NR_TIT_RECEBER           NUMBER(10),
  VL_COTACAO_MOEDA         NUMBER(20,4),
  NR_SEQ_BAIXA_REC         NUMBER(10),
  NR_SEQ_LOTE_ENC_CONTAS   NUMBER(10),
  NR_SEQ_PLS_LOTE_CAMARA   NUMBER(10),
  IE_BAIXA_BLOQUETO        VARCHAR2(1 BYTE),
  VL_GLOSA                 NUMBER(15,2),
  NR_SEQ_PAG_ITEM          NUMBER(10),
  NR_SEQ_PLS_CONTA         NUMBER(10),
  VL_GLOSA_ATO_COOP_PRINC  NUMBER(15,2),
  VL_GLOSA_ATO_COOP_AUX    NUMBER(15,2),
  VL_GLOSA_ATO_NAO_COOP    NUMBER(15,2),
  NR_SEQ_PLS_LOTE_CONTEST  NUMBER(10),
  NR_SEQ_PLS_LOTE_DISC     NUMBER(10),
  VL_OUTRAS_DESPESAS       NUMBER(15,2),
  NR_SEQ_CHEQUE_CP         NUMBER(10),
  IE_ORIGEM_BAIXA          VARCHAR2(15 BYTE),
  VL_BAIXA_ESTRANG         NUMBER(15,2),
  VL_COTACAO               NUMBER(21,10),
  VL_COMPLEMENTO           NUMBER(15,2),
  VL_CAMBIAL_ATIVO         NUMBER(15,2),
  VL_CAMBIAL_PASSIVO       NUMBER(15,2),
  VL_JUROS_EMPREST         NUMBER(15,2),
  VL_AMORTIZA_EMPREST      NUMBER(15,2),
  DT_REAL_PAGAMENTO        DATE,
  CD_CGC_DESTINO           VARCHAR2(14 BYTE),
  CD_BANCO_DESTINO         NUMBER(5),
  CD_AGENCIA_DESTINO       VARCHAR2(8 BYTE),
  NR_CONTA_DESTINO         VARCHAR2(20 BYTE),
  CD_PESSOA_FISICA_DEST    VARCHAR2(10 BYTE),
  IE_DIGITO_AGENCIA_DEST   VARCHAR2(5 BYTE),
  IE_DIGITO_CONTA_DEST     VARCHAR2(5 BYTE),
  IE_AUTENTICACAO          VARCHAR2(64 BYTE)    DEFAULT null,
  NR_NFE_IMP               VARCHAR2(255 BYTE),
  DS_STACK                 VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPABA_BANCO ON TASY.TITULO_PAGAR_BAIXA
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_BANCO
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_BANESCR_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_BANESTA_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_BORPAGA_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_CENCUST_FK_I ON TASY.TITULO_PAGAR_BAIXA
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_CHEQUE_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_CHEQUE_CP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_CHEQUE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_CLTPBAI_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_CLTPBAI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_CONCONT_FK_I ON TASY.TITULO_PAGAR_BAIXA
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_I1 ON TASY.TITULO_PAGAR_BAIXA
(DT_BAIXA, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_I2 ON TASY.TITULO_PAGAR_BAIXA
(NR_TIT_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_LOTCONT_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_LOTENCO_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_LOTE_ENC_CONTAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_LOTENCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_MOEDA_FK_I ON TASY.TITULO_PAGAR_BAIXA
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_NOTFISC_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPABA_PK ON TASY.TITULO_PAGAR_BAIXA
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_PLSCOME_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_PLS_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_PLSLOCC_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_PLS_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_PLSLOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_PLSLOCO_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_PLS_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_PLSLODI_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_PLS_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_PLSPAIT_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_PAG_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_PLSPAIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPABA_TIPBACP_FK_I ON TASY.TITULO_PAGAR_BAIXA
(CD_TIPO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_TITPAGA_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPABA_TRAFINA_FK_I ON TASY.TITULO_PAGAR_BAIXA
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPABA_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_BAIXA_BEFINSERT
BEFORE INSERT ON TASY.TITULO_PAGAR_BAIXA FOR EACH ROW
DECLARE

ie_baixa_lote_contab_w		varchar2(2)	:= 'S';
cd_estabelecimento_w		number(5);
qt_registro_w			number(10);
cd_empresa_w			number(4);
cont_w				number(15);
ie_baixa_tit_repasse_nf_w	varchar2(255);
nr_repasse_terceiro_w		number(15);
ie_moeda_dif_w			varchar2(100);
cd_moeda_padrao_w		number(15);
nr_seq_nota_fiscal_w		number(10);
ie_exige_nf_baixa_titulo_w	varchar2(2);
ie_tipo_titulo_cpa_w		varchar2(2);
nr_seq_pls_pag_prest_w		varchar2(10);
cd_cgc_w			number(14);
cd_pessoa_fisica_w		number(10);
ie_movto_trans_fin_w		number(10);
LoteContabContabil_w		varchar2(100);
ie_status_w			varchar2(3);
dt_liberacao_w			date;
ie_origem_titulo_w		varchar2(10);
nr_lote_contabil_w		number(10);
ds_tipo_lote_contabil_w		varchar2(40);
nr_seq_banco_w			movto_trans_financ.nr_seq_banco%type;
nr_seq_caixa_w			movto_trans_financ.nr_seq_caixa%type;
qt_tit_rec_w			number(10);
ie_processo_camara_w		pls_parametros_camara.ie_processo_camara%type;
nr_titulo_pagar_w		pls_titulo_lote_camara.nr_titulo_pagar%type;
nr_seq_lote_w			pls_titulo_lote_camara.nr_sequencia%type;
ie_baixa_camara_compensacao_w	number(10);
cd_tipo_baixa_w tipo_baixa_cpa.cd_tipo_baixa%type;
ds_tipo_baixa_w tipo_baixa_cpa.ds_tipo_baixa%type;
nr_seq_imposto_w		titulo_pagar_imposto.nr_sequencia%type;
ie_gerar_titulo_pagar_w		tributo.ie_gerar_titulo_pagar%type;
vl_saldo_titulo_w		titulo_pagar.vl_saldo_titulo%type;

cursor c01 is
select	a.nr_sequencia,
	b.ie_gerar_titulo_pagar
from	tributo b,
	titulo_pagar_imposto a
where	a.cd_tributo	= b.cd_tributo
and Obter_Titulo_Imposto(a.nr_sequencia) = 0
and	a.nr_titulo	= :new.nr_titulo;


begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(cd_estabelecimento),
	max(nr_repasse_terceiro),
	max(nr_seq_nota_fiscal),
	max(ie_tipo_titulo),
	max(nr_seq_pls_pag_prest),
	nvl(max(ie_status),'D'),
	max(dt_liberacao),
	max(ie_origem_titulo)
into	cd_estabelecimento_w,
	nr_repasse_terceiro_w,
	nr_seq_nota_fiscal_w,
	ie_tipo_titulo_cpa_w,
	nr_seq_pls_pag_prest_w,
	ie_status_w,
	dt_liberacao_w,
	ie_origem_titulo_w
from	titulo_pagar
where	nr_titulo	= :new.nr_titulo;

select	max(nvl(ie_moeda_dif, 'S')),
	max(cd_moeda_padrao),
	nvl(max(ie_baixa_tit_repasse_nf), 'S')
into	ie_moeda_dif_w,
	cd_moeda_padrao_w,
	ie_baixa_tit_repasse_nf_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

select	max(cd_tipo_baixa)
into	cd_tipo_baixa_w
from	(
	select	a.cd_tipo_baixa
	from	tipo_baixa_cpa A,
		TIPO_BAIXA_CPA_PERFIL x
	where	a.CD_TIPO_BAIXA  = :new.CD_TIPO_BAIXA
	and 	x.cd_perfil = wheb_usuario_pck.get_cd_perfil
	and 	x.IE_SITUACAO = 'A'
	union
	select	a.cd_tipo_baixa
	from	tipo_baixa_cpa A
	where	a.CD_TIPO_BAIXA  = :new.CD_TIPO_BAIXA
	and 	 (not exists (select 1 from TIPO_BAIXA_CPA_PERFIL x where a.CD_TIPO_BAIXA  = x.CD_TIPO_BAIXA)
	or 	 not exists (select 1 from TIPO_BAIXA_CPA_PERFIL x where a.CD_TIPO_BAIXA  = x.CD_TIPO_BAIXA and x.IE_SITUACAO = 'A')));

if(cd_tipo_baixa_w is null) then

select	a.ds_tipo_baixa
into	ds_tipo_baixa_w
from	tipo_baixa_cpa A
where	a.CD_TIPO_BAIXA  = :new.CD_TIPO_BAIXA;
	--O tipo de baixa #@DS_TIPO_BAIXA#@, nao esta liberado para o perfil. Verifique a regra nos "Cadastros Gerais (Shift + F11)".
	Wheb_mensagem_pck.exibir_mensagem_abort(1076905,'DS_TIPO_BAIXA='||ds_tipo_baixa_w);
end if;


/* Francisco - 03/10/2011 - Tratar titulo provisorio, so pode baixar quando e glosa */
if	(ie_status_w = 'P') and
	(nvl(:new.vl_glosa,0) = 0) then
	/*O titulo #@NR_TITULO#@ esta com status provisorio, o mesmo nao pode ser baixado.*/
	Wheb_mensagem_pck.exibir_mensagem_abort(204532,'NR_TITULO='||:new.nr_titulo);

/*	Edgar 17/01/2013, OS 537641, este tratamento nao esta correto, comentado abaixo
else

	if	(dt_liberacao_w is null) and
		(ie_origem_titulo_w = '16')then
		-- O titulo #@NR_TITULO#@ nao esta liberado, o mesmo nao pode ser baixado.
		Wheb_mensagem_pck.exibir_mensagem_abort(204533,'NR_TITULO='||:new.nr_titulo);
	end if;
*/
end if;

if	(ie_baixa_tit_repasse_nf_w = 'N') and (nr_repasse_terceiro_w is not null) then
	select	count(*)
	into	cont_w
	from	repasse_nota_fiscal
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	if	(cont_w = 0) then
		/*Este titulo esta vinculado a um repasse sem nota fiscal gerada!*/
		Wheb_mensagem_pck.exibir_mensagem_abort(204534);
	end if;
end if;

if	(ie_moeda_dif_w = 'N') and (cd_moeda_padrao_w <> :new.cd_moeda) then
	/*Nao pode ser gerada baixa com moeda diferente da moeda padrao!*/
	Wheb_mensagem_pck.exibir_mensagem_abort(204535);
end if;

if	(fin_obter_se_mes_aberto(cd_estabelecimento_w, :new.dt_baixa,'CP',ie_tipo_titulo_cpa_w,null,null,null) = 'N') then
	/*Nao e possivel baixar o titulo com esta data pois este mes/dia de referencia financeiro ja esta fechado!*/
	Wheb_mensagem_pck.exibir_mensagem_abort(204536);
end if;

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_baixa);

obter_param_usuario(855, 47, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, LoteContabContabil_w);

/* Felipe e Francisco - 07/03/2011 - OS 297213 */
select	nvl(max(ie_exige_nf_baixa_titulo),'N')
into	ie_exige_nf_baixa_titulo_w
from	pls_parametro_pagamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_exige_nf_baixa_titulo_w <> 'N') and
	(nr_seq_nota_fiscal_w is null) and
	(nr_seq_pls_pag_prest_w is not null) and
	(ie_tipo_titulo_cpa_w <> '4') then

	select	cd_cgc,
		cd_pessoa_fisica
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	titulo_pagar
	where	nr_titulo = :new.nr_titulo;

	if	((ie_exige_nf_baixa_titulo_w = 'PJ') and (cd_cgc_w is not null)) or /* Se PJ */
		((ie_exige_nf_baixa_titulo_w = 'PF') and (cd_pessoa_fisica_w is not null)) or /* Se PF */
		(ie_exige_nf_baixa_titulo_w = 'S') then /* Todos */
		-- E necessario ter uma nota fiscal vinculada a este titulo (#@NR_TITULO#@) para poder baixar o mesmo. Parametro "Exige NF para baixa de titulo" em OPS - Gestao de Operadoras -> Parametros OPS -> Pagamento da producao.
		Wheb_mensagem_pck.exibir_mensagem_abort(204539,'NR_TITULO='||:new.nr_titulo);
	end if;

	-- Caso nao tenha pessoa no titulo
	if	(cd_cgc_w is null) and
		(cd_pessoa_fisica_w is null) then
		if	(ie_exige_nf_baixa_titulo_w = 'PJ') then
			-- E necessario ter uma pessoa juridica e uma nota fiscal vinculada a este titulo (#@NR_TITULO#@) para poder baixar o mesmo. Parametro "Exige NF para baixa de titulo" em OPS - Gestao de Operadoras -> Parametros OPS -> Pagamento da producao.
			Wheb_mensagem_pck.exibir_mensagem_abort(204537,'NR_TITULO='||:new.nr_titulo);

		elsif	(ie_exige_nf_baixa_titulo_w = 'PF') then
			-- E necessario ter uma pessoa fisica e uma nota fiscal vinculada a este titulo (#@NR_TITULO#@) para poder baixar o mesmo. Parametro "Exige NF para baixa de titulo" em OPS - Gestao de Operadoras -> Parametros OPS -> Pagamento da producao.
			Wheb_mensagem_pck.exibir_mensagem_abort(204538,'NR_TITULO='||:new.nr_titulo);
		end if;
	end if;
end if;

if	(cd_estabelecimento_w is not null) then

	select	max(ie_baixa_lote_contab)
	into	ie_baixa_lote_contab_w
	from	parametros_contas_pagar
	where 	cd_estabelecimento	= cd_estabelecimento_w;

	select	max(cd_empresa)
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;

	/* ahoffelder - OS 325513 - 17/06/2011 - a consistencia ja e feita na MOVTO_TRANS_FINANC_INSERT */
	select	count(*)
	into	ie_movto_trans_fin_w
	from	v$session
	where	audsid		= (select userenv('sessionid') from dual)
	and	upper(action) 	= 'ATUALIZAR_TRANSACAO_FINANCEIRA';

	if	(cd_empresa_w is not null) and
		(nvl(ie_movto_trans_fin_w,0) = 0) then

		if	(ie_baixa_lote_contab_w = 'N') then
			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa = cd_empresa_w
       	        	and	substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1) = 'F'
       		        and	trunc(dt_referencia,'month') = trunc(:new.dt_baixa,'month');

			if	(qt_registro_w > 0) then
				/*O mes de referencia na contabilidade para esta data de baixa (#@DT_BAIXA#@) ja foi fechado!*/
				Wheb_mensagem_pck.exibir_mensagem_abort(204540,'DT_BAIXA='||to_char(:new.dt_baixa,'dd/mm/yyyy'));
			end if;

		elsif	(ie_baixa_lote_contab_w = 'L') then

			if	(nvl(LoteContabContabil_w,'S') = 'S') or
				((nvl(LoteContabContabil_w,'S') = 'N') and (:new.nr_bordero is null)) then

				-- se o parametro 47 do bordero estiver para N, de nao permitir baixar o bordero com lote contab de CB fechado na data,
				-- e o numero de bordero nao for nulo, nao precisa fazer esta consistencia,
				-- pois estara sendo feito no Bordero a pagar.

				select	count(*),
					max(a.nr_lote_contabil),
					max(b.ds_tipo_lote_contabil)
				into	qt_registro_w,
					nr_lote_contabil_w,
					ds_tipo_lote_contabil_w
				from 	tipo_lote_contabil b,
					lote_contabil a
				where	a.cd_tipo_lote_contabil	= b.cd_tipo_lote_contabil
				and	a.cd_estabelecimento = cd_estabelecimento_w
				and	trunc(a.dt_referencia,'dd')	= trunc(:new.dt_baixa,'dd')
				and	a.cd_tipo_lote_contabil	= 7
				and	exists	(	select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil);

				if	(qt_registro_w > 0) then
					/*Ja foi gerado lote contabil para esta data de baixa.
					Lote: #@NR_LOTE_CONTABIL#@
					Tipo de lote contabil: #@DS_TIPO_LOTE_CONTABIL#@
					Data: #@DT_BAIXA#@*/
					Wheb_mensagem_pck.exibir_mensagem_abort(204541, 'NR_LOTE_CONTABIL='||nr_lote_contabil_w ||
											';DS_TIPO_LOTE_CONTABIL='||ds_tipo_lote_contabil_w ||
											';DT_BAIXA='||to_char(:new.dt_baixa,'dd/mm/yyyy'));
				end if;

			end if;

		elsif	(ie_baixa_lote_contab_w = 'M') then

			if	(nvl(LoteContabContabil_w,'S') = 'S') or
				((nvl(LoteContabContabil_w,'S') = 'N') and (:new.nr_bordero is null)) then

				-- se o parametro 47 do bordero estiver para N, de nao permitir baixar o bordero com lote contab de CB fechado na data,
				-- e o numero de bordero nao for nulo, nao precisa fazer esta consistencia,
				-- pois estara sendo feito no Bordero a pagar.

				select	count(*),
					max(a.nr_lote_contabil),
					max(b.ds_tipo_lote_contabil)
				into	qt_registro_w,
					nr_lote_contabil_w,
					ds_tipo_lote_contabil_w
				from 	tipo_lote_contabil b,
					lote_contabil a
				where	a.cd_tipo_lote_contabil	= b.cd_tipo_lote_contabil
				and	a.cd_estabelecimento = cd_estabelecimento_w
				and	trunc(a.dt_referencia,'dd')	>= trunc(:new.dt_baixa,'dd')
				and	trunc(a.dt_referencia,'month')	= trunc(:new.dt_baixa,'month')
				and	a.cd_tipo_lote_contabil	= 7
				and	exists	(	select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil);

				if	(qt_registro_w > 0) then
					/*Ja foi gerado lote contabil para esta data de baixa.
					Lote: #@NR_LOTE_CONTABIL#@
					Tipo de lote contabil: #@DS_TIPO_LOTE_CONTABIL#@
					Data: #@DT_BAIXA#@*/
					Wheb_mensagem_pck.exibir_mensagem_abort(204541, 'NR_LOTE_CONTABIL='||nr_lote_contabil_w ||
											';DS_TIPO_LOTE_CONTABIL='||ds_tipo_lote_contabil_w ||
											';DT_BAIXA='||to_char(:new.dt_baixa,'dd/mm/yyyy'));
				end if;

			end if;

		elsif	(ie_baixa_lote_contab_w = 'F') then

			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa = cd_empresa_w
                		and	trunc(dt_referencia,'month') = trunc(:new.dt_baixa,'month');

			if	(qt_registro_w > 0) then
				/*Ja existe mes de referencia na contabilidade para esta data de baixa (#@DT_BAIXA#@).*/
				Wheb_mensagem_pck.exibir_mensagem_abort(204542,'DT_BAIXA='||:new.dt_baixa);
			end if;

		end if;
	end if;

	/* Controle Bancario */
	select	max(a.nr_seq_banco)
	into 	nr_seq_banco_w
	from 	movto_trans_financ a
	where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

	/* tesoraria */
	select	max(a.nr_seq_caixa)
	into 	nr_seq_caixa_w
	from 	movto_trans_financ a
	where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

	/* abatimento */
	select	count(*)
	into	qt_tit_rec_w
	from	titulo_receber_liq a
	where	a.nr_seq_baixa_pagar	= :new.nr_sequencia
	and	a.nr_tit_pagar		= :new.nr_titulo;

	if	(nvl(qt_tit_rec_w,0)	> 0) then
		:new.ie_origem_baixa := 'AB';

	elsif	(:new.nr_bordero is not null) then
		:new.ie_origem_baixa := 'BP';

	elsif	(:new.nr_seq_movto_trans_fin is not null) and
		(nr_seq_banco_w is not null) then
		:new.ie_origem_baixa := 'CB';

	elsif	(:new.nr_seq_escrit is not null) then
		:new.ie_origem_baixa := 'PE';

	elsif	(:new.nr_seq_baixa_origem is not null) then
		:new.ie_origem_baixa := 'ET';

	elsif	(:new.nr_seq_lote_enc_contas is not null) then
		:new.ie_origem_baixa := 'EC';

	elsif	(:new.nr_seq_movto_trans_fin  is not null) and
		(nr_seq_caixa_w is not null) then
		:new.ie_origem_baixa := 'TS';
	else
		:new.ie_origem_baixa := 'TP';
	end if;

	/*verificar se o titulo que esta recebendo a baixa esta em um lote de camra de compensacao que nao esta baixado.*/
	select	count(a.nr_titulo_pagar),
		max(b.nr_sequencia)
	into	nr_titulo_pagar_w,
		nr_seq_lote_w
	from	pls_titulo_lote_camara a,
		pls_lote_camara_comp b
	where	a.nr_seq_lote_camara	= b.nr_sequencia
	and	a.nr_titulo_pagar	= :new.nr_titulo;

	select	nvl(max(a.ie_processo_camara),'CO')
	into	ie_processo_camara_w
	from	pls_parametros_camara a
	where 	cd_estabelecimento = cd_estabelecimento_w;

	if	(ie_processo_camara_w = 'CA') and	-- processo camara de compensacao for regime de caixa
		(nr_titulo_pagar_w > 0) and 		-- titulo estiver na camara
		(nvl(:new.vl_glosa,0) = 0) then 	-- nao ter valor de glosa

		/*verifica se a rotina que esta efetuando a baixa e a rotina que baixa pela camara. Se nao for, deve consistir.*/
		select	count(*)
		into	ie_baixa_camara_compensacao_w
		from	v$session
		where	audsid			= (select userenv('sessionid') from dual)
		and		(upper(action) 	= 'PLS_BAIXAR_LOTE_CAMARA_COMP' or
				 upper(action)  = 'PLS_ESTORNAR_LOTE_CAMARA_COMP');


		if	(ie_baixa_camara_compensacao_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(335779,	'NR_SEQ_LOTE_W=' || nr_seq_lote_w || ';NR_TITULO_W=' || :new.nr_titulo);
		end if;
	end if;

	select vl_saldo_titulo
	into vl_saldo_titulo_w
	from titulo_pagar
	where nr_titulo = :new.nr_titulo;

	if	(:new.vl_baixa > 0 and :new.vl_baixa = vl_saldo_titulo_w) then

		open c01;
		loop
		fetch c01 into
			nr_seq_imposto_w,
			ie_gerar_titulo_pagar_w;
		exit when c01%notfound;

			if	(ie_gerar_titulo_pagar_w = 'B') then
				Gerar_titulo_tributo(nr_seq_imposto_w, :new.nm_usuario);
			end if;

		end loop;
		close c01;

	end if;

end if;

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_baixa_delete
before delete ON TASY.TITULO_PAGAR_BAIXA for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,:old.nr_sequencia,'TPB',:old.dt_baixa,'E',:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_baixa_insert
AFTER INSERT OR UPDATE ON TASY.TITULO_PAGAR_BAIXA FOR EACH ROW
DECLARE

vl_saldo_juros_w              number(15, 2);
vl_saldo_multa_w              number(15, 2);
cd_estabelecimento_w          number(5);
ie_altera_valor_tit_escrit_w  varchar2(1);
qt_registro_w                 number(5);
dt_emissao_w                  date;
vl_movimento_w                ctb_documento.vl_movimento%type;
ie_tipo_titulo_w              titulo_pagar.ie_tipo_titulo%type;
vl_nota_credito_w             bordero_nc_nota_credito.vl_nota_credito%type;
nm_tabela_w                   ctb_documento.nm_tabela%type;
dt_baixa_origem_w             titulo_pagar_baixa.dt_baixa%type;
ie_contab_tit_interc_cancel_w pls_parametro_contabil.ie_contab_tit_interc_cancel%type;
ie_concil_contab_w            pls_visible_false.ie_concil_contab%type;
ie_contab_cp_no_cb_w          ctb_param_lote_cont_banco.ie_contab_cp_no_cb%type;
ie_situacao_ctb_w             ctb_documento.ie_situacao_ctb%type;

cursor c01 is
select a.nm_atributo,
       7 cd_tipo_lote_contab
from   atributo_contab a
where  a.cd_tipo_lote_contab = 7
and    a.nm_atributo in ('VL_BAIXA',
                         'VL_DESCONTOS',
                         'VL_OUTRAS_DEDUCOES',
                         'VL_JUROS',
                         'VL_MULTA',
                         'VL_OUTROS_ACRESCIMOS',
                         'VL_PAGO',
                         'VL_INSS',
                         'VL_IMPOSTO_MUNIC',
                         'VL_IMPOSTO_BAIXA',
                         'VL_OUTRAS_DESPESAS',
                         'VL_BAIXA_SEM_TRIB',
                         'VL_NOTA_CREDITO')
and    ie_contab_cp_no_cb_w = 'N'
union all
select a.nm_atributo,
       18 cd_tipo_lote_contab
from   atributo_contab a
where  a.cd_tipo_lote_contab = 7
and    a.nm_atributo in ('VL_BAIXA',
                         'VL_DESCONTOS',
                         'VL_OUTRAS_DEDUCOES',
                         'VL_JUROS',
                         'VL_MULTA',
                         'VL_OUTROS_ACRESCIMOS',
                         'VL_INSS',
                         'VL_IMPOSTO_MUNIC',
                         'VL_IMPOSTO_BAIXA',
                         'VL_OUTRAS_DESPESAS',
                         'VL_CAMBIAL_ATIVO',
                         'VL_CAMBIAL_PASSIVO')
and    ie_contab_cp_no_cb_w = 'S'
union all
select a.nm_atributo,
       36 cd_tipo_lote_contab
from   atributo_contab a
where  a.cd_tipo_lote_contab = 36
and    a.nm_atributo = 'VL_PAGO'
and    :new.nr_seq_lote_enc_contas is null
and    :new.nr_seq_pls_lote_camara is null
and    ie_concil_contab_w = 'S'
and    exists (select 1
              from   titulo_pagar x,
                     ptu_fatura y
              where  x.nr_titulo = y.nr_titulo
              and    x.nr_titulo = :new.nr_titulo
              and    nvl(x.ie_status, 'D') = 'D'
              and    x.ie_origem_titulo = '16'
              and    ((x.ie_situacao != 'C') or (ie_contab_tit_interc_cancel_w = 'S'))
              union all
              select 1
              from   titulo_pagar x,
                     ptu_fatura y
              where  x.nr_titulo = y.nr_titulo_ndc
              and    x.nr_titulo = :new.nr_titulo
              and    nvl(x.ie_status, 'D') = 'D'
              and    x.ie_origem_titulo = '16'
              and    ((x.ie_situacao != 'C') or (ie_contab_tit_interc_cancel_w = 'S')))
union all
select a.nm_atributo,
       37 cd_tipo_lote_contab
from   atributo_contab a
where  a.cd_tipo_lote_contab = 37
and    a.nm_atributo in ('VL_PAGO', 'VL_PAG_GLOSA')
and    nvl(:new.nr_seq_pls_lote_camara, 0) = 0
and    nvl(:new.nr_seq_lote_enc_contas, 0) = 0
and    ie_concil_contab_w = 'S'
and    exists (select 1
              from   titulo_pagar x
              where  x.nr_titulo = :new.nr_titulo
              and    x.ie_origem_titulo = '19'
              and    ((x.nr_seq_pls_lote_contest is not null) or (x.nr_seq_pls_lote_disc is not null)));

c01_w c01%rowtype;

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    if (inserting) then
        select vl_saldo_juros,
               vl_saldo_multa,
               dt_emissao
        into   vl_saldo_juros_w,
               vl_saldo_multa_w,
               dt_emissao_w
        from   titulo_pagar
        where  nr_titulo = :new.nr_titulo;

        if (trunc(:new.dt_baixa, 'dd') < trunc(dt_emissao_w, 'dd')) then
            wheb_mensagem_pck.exibir_mensagem_abort(471997);
        end if;

        vl_saldo_juros_w := vl_saldo_juros_w - :new.vl_juros;
        vl_saldo_multa_w := vl_saldo_multa_w - :new.vl_multa;

        if (vl_saldo_juros_w < 0) then
            vl_saldo_juros_w := 0;
        end if;

        if (vl_saldo_multa_w < 0) then
            vl_saldo_multa_w := 0;
        end if;

        update  titulo_pagar
        set     vl_saldo_juros = vl_saldo_juros_w,
                vl_saldo_multa = vl_saldo_multa_w
        where   nr_titulo = :new.nr_titulo;

        /* nao deixar salvar se tem registro na titulo_pagar_baixa vinculado ao titulo */
        select  max(cd_estabelecimento)
        into    cd_estabelecimento_w
        from    titulo_pagar
        where   nr_titulo       = :new.nr_titulo;

        begin
            select x.ie_contab_cp_no_cb
            into   ie_contab_cp_no_cb_w
            from   (select decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_cp_no_cb, 'N'), 'N') ie_contab_cp_no_cb
                    from   ctb_param_lote_cont_banco a
                    where  a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
                    and    nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
                    order  by (nvl(a.cd_estab_exclusivo, 0)) desc) x
            where  rownum = 1;
        exception
            when others then
                ie_contab_cp_no_cb_w := 'N';
        end;

        begin
        select  nvl(max(ie_concil_contab), 'N')
        into    ie_concil_contab_w
        from    pls_visible_false
        where   cd_estabelecimento = cd_estabelecimento_w;
        exception when others then
                ie_concil_contab_w := 'N';
        end;

        begin
            select nvl(ie_altera_valor_tit_escrit, 'N')
            into   ie_altera_valor_tit_escrit_w
            from   parametros_contas_pagar
            where  cd_estabelecimento = cd_estabelecimento_w;
        exception
            when others then
                ie_altera_valor_tit_escrit_w := 'N';
        end;

        if (ie_altera_valor_tit_escrit_w = 'N') then

            select count(*)
            into   qt_registro_w
            from   banco_escritural b,
                   titulo_pagar_escrit a
            where  b.nr_sequencia = a.nr_seq_escrit
            and    b.ie_remessa_retorno = 'R'
            and    a.nr_titulo = :new.nr_titulo;

            if (qt_registro_w > 0) then
                /* Se a baixa nao foi feita pelo pagamento escritural.. */
                if (:new.nr_seq_escrit is null) then
                    wheb_mensagem_pck.exibir_mensagem_abort(231370, 'NR_TITULO=' || :new.nr_titulo);
                    /* 'Titulo ja vinculado a uma remessa de pagamento escritural.' || chr(13) ||  'O mesmo so pode ser baixado pela funcao "Pagamento escritural",' || chr(13) || 'Titulo: ' || ); */
                end if;
            end if;
        end if;

        select nvl(max(ie_contab_tit_interc_cancel), 'S')
        into   ie_contab_tit_interc_cancel_w
        from   pls_parametro_contabil
        where  cd_estabelecimento = cd_estabelecimento_w;

        philips_contabil_pck.set_estabelecimento(cd_estabelecimento_w);
        philips_contabil_pck.set_nr_seq_trans_fin(:new.nr_seq_trans_fin);
        philips_contabil_pck.set_dt_baixa(:new.dt_baixa);
        atualizar_imposto_tit_pagar(:new.nr_titulo, :new.nr_sequencia, :new.vl_baixa, :new.nm_usuario, :new.nr_seq_baixa_origem);
        philips_contabil_pck.set_estabelecimento(null);
        philips_contabil_pck.set_nr_seq_trans_fin(null);
        philips_contabil_pck.set_dt_baixa(null);

        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo, :new.nr_sequencia, 'TPB', :new.dt_baixa, 'I', :new.nm_usuario);

        open c01;
        loop
            fetch c01
                into c01_w;
            exit when c01%notfound;
            begin

                nm_tabela_w         := 'TITULO_PAGAR_BAIXA_CONTAB_V';
                vl_movimento_w      := 0;
                ie_situacao_ctb_w   := 'P';

                if (c01_w.cd_tipo_lote_contab = 7) then
                    begin
                        if (c01_w.nm_atributo = 'VL_NOTA_CREDITO') then
                            begin
                                vl_nota_credito_w := 0;
                                begin

                                    select vl_nota_credito
                                    into   vl_nota_credito_w
                                    from   titulo_pagar c,
                                           bordero_nc b,
                                           bordero_nc_nota_credito a
                                    where  a.nr_seq_bordero = b.nr_sequencia
                                    and    b.nr_sequencia = c.nr_seq_bordero_nc
                                    and    c.nr_titulo = :new.nr_titulo
                                    and    c.ie_tipo_titulo = '21';

                                exception
                                    when others then
                                        vl_nota_credito_w := 0;
                                end;
                                nm_tabela_w := 'TITULO_PAGAR_BAIXA_CONTAB_V';

                            end;
                        end if;

                        if (c01_w.nm_atributo = 'VL_OUTRAS_DESPESAS') then
                            begin
                                nm_tabela_w := 'TITULO_PAGAR_BAIXA';
                            end;
                        elsif (c01_w.nm_atributo = 'VL_BAIXA_SEM_TRIB') then
                            begin
                                nm_tabela_w := 'TITULO_PAGAR';
                            end;
                        elsif (c01_w.nm_atributo = 'VL_IMPOSTO_BAIXA') then
                            begin
                                ie_situacao_ctb_w := 'N';
                            end;
                        end if;

                        vl_movimento_w:= case c01_w.nm_atributo
                                        when 'VL_BAIXA' then :new.vl_baixa
                                        when 'VL_DESCONTOS' then :new.vl_descontos
                                        when 'VL_OUTRAS_DEDUCOES'  then :new.vl_outras_deducoes
                                        when 'VL_JUROS'  then :new.vl_juros
                                        when 'VL_MULTA'  then :new.vl_multa
                                        when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
                                        when 'VL_PAGO'  then :new.vl_pago
                                        when 'VL_INSS'  then :new.vl_inss
                                        when 'VL_IMPOSTO_MUNIC'  then :new.vl_imposto_munic
                                        when 'VL_IMPOSTO_BAIXA'  then :new.vl_imposto
                                        when 'VL_BAIXA_SEM_TRIB'  then :new.vl_baixa - nvl(:new.vl_imposto,0)
                                        when 'VL_OUTRAS_DESPESAS'  then :new.vl_outras_despesas
                                        when 'VL_NOTA_CREDITO' then  (vl_nota_credito_w * sign(:new.vl_baixa))
                                        end;
                    end;
                elsif (c01_w.cd_tipo_lote_contab = 18) then
                    begin
                        nm_tabela_w := 'TITULO_PAGAR_BAIXA_CONTAB_V';

                        vl_movimento_w  := case c01_w.nm_atributo
                                        when 'VL_BAIXA' then :new.vl_baixa
                                        when 'VL_CAMBIAL_ATIVO' then :new.vl_cambial_ativo
                                        when 'VL_CAMBIAL_PASSIVO' then :new.vl_cambial_passivo
                                        when 'VL_DESCONTOS' then :new.vl_descontos
                                        when 'VL_IMPOSTO_BAIXA'  then :new.vl_imposto
                                        when 'VL_IMPOSTO_MUNIC'  then :new.vl_imposto_munic
                                        when 'VL_INSS'  then :new.vl_inss
                                        when 'VL_JUROS'  then :new.vl_juros
                                        when 'VL_MULTA'  then :new.vl_multa
                                        when 'VL_OUTRAS_DEDUCOES'  then :new.vl_outras_deducoes
                                        when 'VL_OUTRAS_DESPESAS'  then :new.vl_outras_despesas
                                        when 'VL_OUTROS_ACRESCIMOS'  then :new.vl_outros_acrescimos
                                        end;

                    end;
                elsif (c01_w.cd_tipo_lote_contab = 36) then
                    vl_movimento_w := :new.vl_baixa;
                    nm_tabela_w    := 'TITULO_PAGAR_BAIXA';
                elsif (c01_w.cd_tipo_lote_contab = 37) then
                    vl_movimento_w  :=      case c01_w.nm_atributo
                                            when 'VL_PAGO' then :new.vl_descontos --* nr_multiplicador_w
                                            when 'VL_PAG_GLOSA' then :new.vl_baixa --* nr_multiplicador_w
                                            end;
                    nm_tabela_w    := 'TITULO_PAGAR_BAIXA';
                end if;

                if (nvl(vl_movimento_w, 0) <> 0) then
                    begin

                        ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_w,
                                                                       :new.dt_baixa,
                                                                       c01_w.cd_tipo_lote_contab,
                                                                       :new.nr_seq_trans_fin,
                                                                       13,
                                                                       :new.nr_titulo,
                                                                       :new.nr_sequencia,
                                                                       null,
                                                                       vl_movimento_w,
                                                                       nm_tabela_w,
                                                                       c01_w.nm_atributo,
                                                                       :new.nm_usuario,
                                                                       ie_situacao_ctb_w);

                    end;
                end if;

            end;
        end loop;
        close c01;

    elsif (updating) then
        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo, :new.nr_sequencia, 'TPB', :new.dt_baixa, 'A', :new.nm_usuario);
    end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_BAIXA_UPDATE
BEFORE UPDATE ON TASY.TITULO_PAGAR_BAIXA FOR EACH ROW
DECLARE

nr_seq_banco_w			movto_trans_financ.nr_seq_banco%type;
nr_seq_caixa_w			movto_trans_financ.nr_seq_caixa%type;
qt_tit_rec_w			number(10);
cd_estabelecimento_w		number(5);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Estabelecimento */
select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	titulo_pagar
where	nr_titulo	= :new.nr_titulo;

/* Controle Bancario */
select	max(a.nr_seq_banco)
into 	nr_seq_banco_w
from 	movto_trans_financ a
where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

/* tesoraria */
select	max(a.nr_seq_caixa)
into 	nr_seq_caixa_w
from 	movto_trans_financ a
where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

/* abatimento */
select	count(*)
into	qt_tit_rec_w
from	titulo_receber_liq a
where	a.nr_seq_baixa_pagar	= :new.nr_sequencia
and	a.nr_tit_pagar		= :new.nr_titulo;

if	(nvl(qt_tit_rec_w,0)	> 0) then
	:new.ie_origem_baixa := 'AB';

elsif	(:new.nr_bordero is not null) then
	:new.ie_origem_baixa := 'BP';

elsif	(:new.nr_seq_movto_trans_fin is not null) and
	(nr_seq_banco_w is not null) then
	:new.ie_origem_baixa := 'CB';

elsif	(:new.nr_seq_escrit is not null) then
	:new.ie_origem_baixa := 'PE';

elsif	(:new.nr_seq_baixa_origem is not null) then
	:new.ie_origem_baixa := 'ET';


elsif	(:new.nr_seq_movto_trans_fin  is not null) and
	(nr_seq_caixa_w is not null) then
	:new.ie_origem_baixa := 'TS';
else
	:new.ie_origem_baixa := 'TP';
end if;

if	(:old.dt_baixa <> :new.dt_baixa) then
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_baixa);
end if;


 if 	(philips_param_pck.get_cd_pais = 2) then
		if	( :new.nr_nfe_imp is not null) and (:old.nr_nfe_imp is null) then
				ctb_uuid_pck.atualizar_uuid_movt_contab_doc(:new.nr_titulo,
									:new.nr_sequencia,
									13,
									:new.nr_nfe_imp,
									null,
									null
									);
		end if;
end if;
end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_BAIXA ADD (
  CONSTRAINT TITPABA_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_BAIXA ADD (
  CONSTRAINT TITPABA_CHEQUE_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE_CP) 
 REFERENCES TASY.CHEQUE (NR_SEQUENCIA),
  CONSTRAINT TITPABA_CLTPBAI_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_TIT_PAGAR_BAIXA (NR_SEQUENCIA),
  CONSTRAINT TITPABA_LOTENCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENC_CONTAS) 
 REFERENCES TASY.LOTE_ENCONTRO_CONTAS (NR_SEQUENCIA),
  CONSTRAINT TITPABA_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA),
  CONSTRAINT TITPABA_PLSPAIT_FK 
 FOREIGN KEY (NR_SEQ_PAG_ITEM) 
 REFERENCES TASY.PLS_PAGAMENTO_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITPABA_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_PLS_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT TITPABA_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT TITPABA_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT TITPABA_BANESCR_FK 
 FOREIGN KEY (NR_SEQ_ESCRIT) 
 REFERENCES TASY.BANCO_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT TITPABA_BORPAGA_FK 
 FOREIGN KEY (NR_BORDERO) 
 REFERENCES TASY.BORDERO_PAGAMENTO (NR_BORDERO),
  CONSTRAINT TITPABA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPABA_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITPABA_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_DEVOLUCAO) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT TITPABA_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITPABA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPABA_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TITPABA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TITPABA_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITPABA_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA));

GRANT SELECT ON TASY.TITULO_PAGAR_BAIXA TO NIVEL_1;

