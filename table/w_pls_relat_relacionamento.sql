ALTER TABLE TASY.W_PLS_RELAT_RELACIONAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_RELAT_RELACIONAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_RELAT_RELACIONAMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO             NUMBER(10),
  DS_GRUPO_CONTRATO        VARCHAR2(255 BYTE),
  TX_CONSULTAS             NUMBER(7,4),
  TX_EXAMES                NUMBER(7,4),
  TX_INTERNACAO            NUMBER(7,4),
  VL_CUSTO_TOTAL           NUMBER(15,2),
  VL_CUSTO_SERVICO         NUMBER(15,2),
  VL_CUSTO_DIVERSOS        NUMBER(15,2),
  VL_RECEITA_TOTAL         NUMBER(15,2),
  VL_FATOR_MODERADOR       NUMBER(15,2),
  QT_BENEFICIARIOS         NUMBER(10),
  TX_RESULTADO             NUMBER(15,4),
  IE_TIPO_RELATORIO        VARCHAR2(2 BYTE),
  NR_SEQ_PLANO             NUMBER(10),
  VL_DESPESA_BENEF         NUMBER(15,2),
  VL_RECEITA_BENEF         NUMBER(15,2),
  VL_LUCRO_PREJUIZO        NUMBER(15,2),
  TX_RENTABILIDADE         NUMBER(7,4),
  TX_SINISTRO              NUMBER(7,4),
  QT_BENEFICIARIO_GRUPO    NUMBER(10),
  QT_IDADE                 NUMBER(10),
  NR_SEQ_CONTRATO          NUMBER(10),
  DS_ESTIPULANTE_CONTRATO  VARCHAR2(255 BYTE),
  DT_REFERENCIA            DATE,
  QT_MESES_FILTRO          NUMBER(5),
  QT_IDADE_INICIAL         NUMBER(5),
  QT_IDADE_FINAL           NUMBER(5),
  QT_CONSULTA              NUMBER(10),
  QT_EXAME                 NUMBER(10),
  QT_INTERNACAO            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPLSRER_I ON TASY.W_PLS_RELAT_RELACIONAMENTO
(IE_TIPO_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRER_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WPLSRER_PK ON TASY.W_PLS_RELAT_RELACIONAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRER_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_RELAT_RELACIONAMENTO ADD (
  CONSTRAINT WPLSRER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_RELAT_RELACIONAMENTO TO NIVEL_1;

