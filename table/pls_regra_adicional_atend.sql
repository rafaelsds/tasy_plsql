ALTER TABLE TASY.PLS_REGRA_ADICIONAL_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ADICIONAL_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ADICIONAL_ATEND
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_REGRA             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ACAO_EVENTO       NUMBER(3),
  NR_SEQ_OPERADOR      NUMBER(10),
  DS_ASSINATURA_EMAIL  VARCHAR2(4000 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRADA_ESTABEL_FK_I ON TASY.PLS_REGRA_ADICIONAL_ATEND
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRADA_PK ON TASY.PLS_REGRA_ADICIONAL_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRADA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRADA_PLSOPER_FK_I ON TASY.PLS_REGRA_ADICIONAL_ATEND
(NR_SEQ_OPERADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRADA_PLSOPER_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_ADICIONAL_ATEND ADD (
  CONSTRAINT PLSRADA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_ADICIONAL_ATEND ADD (
  CONSTRAINT PLSRADA_PLSOPER_FK 
 FOREIGN KEY (NR_SEQ_OPERADOR) 
 REFERENCES TASY.PLS_OPERADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRADA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_ADICIONAL_ATEND TO NIVEL_1;

