ALTER TABLE TASY.W_ITEM_PRESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ITEM_PRESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ITEM_PRESCR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PRESCRICAO        NUMBER(14),
  CD_PROTOCOLO         NUMBER(10)               NOT NULL,
  NR_SEQ_SOLUCAO       NUMBER(15),
  NR_SEQ_ITEM          NUMBER(15)               NOT NULL,
  QT_DOSE              NUMBER(15,4)             NOT NULL,
  NR_SEQ_MATERIAL      NUMBER(6),
  IE_ORIGEM_INF        VARCHAR2(5 BYTE),
  CD_MATERIAL          NUMBER(6),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  IE_MOSTRAR_ITEM      VARCHAR2(1 BYTE),
  IE_EXCLUIDO          VARCHAR2(1 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  CD_KIT_MATERIAL      NUMBER(5),
  NR_SEQ_GAS           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WITPRES_I1 ON TASY.W_ITEM_PRESCR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_I1
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_I2 ON TASY.W_ITEM_PRESCR
(CD_PROTOCOLO, NR_PRESCRICAO, IE_ORIGEM_INF, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WITPRES_I3 ON TASY.W_ITEM_PRESCR
(IE_ORIGEM_INF, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WITPRES_I4 ON TASY.W_ITEM_PRESCR
(NR_PRESCRICAO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_I4
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_I5 ON TASY.W_ITEM_PRESCR
(NR_PRESCRICAO, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_I5
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_I6 ON TASY.W_ITEM_PRESCR
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_I6
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_I7 ON TASY.W_ITEM_PRESCR
(NR_PRESCRICAO, NVL("IE_EXCLUIDO",'N'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WITPRES_PK ON TASY.W_ITEM_PRESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_PK
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_PROCEDI_FK_I ON TASY.W_ITEM_PRESCR
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WITPRES_PROINTE_FK_I ON TASY.W_ITEM_PRESCR
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITPRES_PROINTE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_ITEM_PRESCR ADD (
  CONSTRAINT WITPRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_ITEM_PRESCR ADD (
  CONSTRAINT WITPRES_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT WITPRES_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_ITEM_PRESCR TO NIVEL_1;

