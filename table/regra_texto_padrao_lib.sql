ALTER TABLE TASY.REGRA_TEXTO_PADRAO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TEXTO_PADRAO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TEXTO_PADRAO_LIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ITEM_PRONT    NUMBER(10)               NOT NULL,
  CD_PERFIL            NUMBER(5),
  IE_PERMITE           VARCHAR2(1 BYTE),
  IE_TIPO_TEXTO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RETEPAL_PERFIL_FK_I ON TASY.REGRA_TEXTO_PADRAO_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RETEPAL_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RETEPAL_PK ON TASY.REGRA_TEXTO_PADRAO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RETEPAL_PK
  MONITORING USAGE;


CREATE INDEX TASY.RETEPAL_PROITEM_FK_I ON TASY.REGRA_TEXTO_PADRAO_LIB
(NR_SEQ_ITEM_PRONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_TEXTO_PADRAO_LIB ADD (
  CONSTRAINT RETEPAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TEXTO_PADRAO_LIB ADD (
  CONSTRAINT RETEPAL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT RETEPAL_PROITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PRONT) 
 REFERENCES TASY.PRONTUARIO_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_TEXTO_PADRAO_LIB TO NIVEL_1;

