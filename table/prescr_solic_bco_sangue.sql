ALTER TABLE TASY.PRESCR_SOLIC_BCO_SANGUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_SOLIC_BCO_SANGUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_SOLIC_BCO_SANGUE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_PRESCRICAO           NUMBER(15)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_DIAGNOSTICO          VARCHAR2(256 BYTE),
  QT_TRANSF_ANTERIOR      NUMBER(3),
  IE_TIPO_PACIENTE        VARCHAR2(2 BYTE)      NOT NULL,
  IE_PORTE_CIRURGICO      VARCHAR2(2 BYTE),
  IE_TIPO                 NUMBER(10)            NOT NULL,
  DT_PROGRAMADA           DATE,
  QT_PLAQUETA             NUMBER(15),
  QT_HEMOGLOBINA          NUMBER(15,2),
  QT_HEMATOCRITO          NUMBER(15,1),
  QT_TAP                  NUMBER(15,1),
  QT_TAP_INR              NUMBER(15,2),
  DS_COAGULOPATIA         VARCHAR2(80 BYTE),
  IE_TRANS_ANTERIOR       VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_ULTIMA_TRANSF        DATE,
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  IE_PRE_MEDICACAO        VARCHAR2(1 BYTE),
  DS_PRE_MEDICACAO        VARCHAR2(255 BYTE),
  QT_FIBRINOGENIO         NUMBER(15,1),
  QT_GRAVIDEZ             NUMBER(5),
  QT_ABORTO               NUMBER(5),
  CD_PERFIL_ATIVO         NUMBER(5),
  QT_TTPA                 NUMBER(15,2),
  IE_RESERVA              VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA       NUMBER(10),
  QT_IMC                  NUMBER(4,1),
  QT_PESO                 NUMBER(6,3),
  QT_ALTURA_CM            NUMBER(4,1),
  DT_CIRURGIA             DATE,
  DS_CIRURGIA             VARCHAR2(255 BYTE),
  DS_HEMOCOMP_REACAO      VARCHAR2(255 BYTE),
  QT_HEMOGLOBINA_S        NUMBER(15,2),
  QT_ALBUMINA             NUMBER(15,2),
  QT_BILIRRUBINA_DIR      NUMBER(15,1),
  QT_BILIRRUBINA_IND      NUMBER(15,1),
  QT_MAGNESIO             NUMBER(15,1),
  QT_CALCIO               NUMBER(15,1),
  IE_COOMBS_DIRETO        VARCHAR2(15 BYTE),
  NR_PRESCRICAO_ORIGINAL  NUMBER(15),
  NR_SEQ_REACAO           NUMBER(10),
  QT_FREQ_CARDIACA        NUMBER(3),
  QT_PA_SISTOLICA         NUMBER(3),
  QT_PA_DIASTOLICA        NUMBER(3),
  QT_TTPA_REL             NUMBER(15,2),
  QT_TEMP                 NUMBER(4,1),
  QT_LEUCOCITOS           NUMBER(15,4),
  CD_DOENCA_CID           VARCHAR2(10 BYTE),
  CD_PROTOCOLO            NUMBER(10),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  DS_STACK                VARCHAR2(2000 BYTE),
  NR_SEQ_ITEM_PROT        NUMBER(10),
  IE_GRAVIDEZ             VARCHAR2(1 BYTE),
  NR_SEQ_HEMO_CPOE        NUMBER(10),
  IE_HORARIO_SUSP         VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA_SUSP  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREHEMO_CIDDOEN_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREHEMO_CPOHEMO_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(NR_SEQ_HEMO_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREHEMO_PERFIL_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREHEMO_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREHEMO_PK ON TASY.PRESCR_SOLIC_BCO_SANGUE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREHEMO_PRESMED_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREHEMO_SANTIRE_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(NR_SEQ_REACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREHEMO_SANTIRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREHEMO_TASASDI_FK_I ON TASY.PRESCR_SOLIC_BCO_SANGUE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREHEMO_TASASDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.prescr_solic_bco_san_insert
BEFORE INSERT ON TASY.PRESCR_SOLIC_BCO_SANGUE FOR EACH ROW
BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_solic_bco_sangue_atual
BEFORE INSERT OR UPDATE ON TASY.PRESCR_SOLIC_BCO_SANGUE FOR EACH ROW
declare

ie_prescr_emergencia_w		varchar2(1);
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;

BEGIN

if (inserting) then

	begin

	if	(:new.dt_programada is not null) and
		(:new.dt_programada < sysdate) then

		select	nvl(max(ie_prescr_emergencia),'N'),
				max(cd_funcao_origem)
		into	ie_prescr_emergencia_w,
				cd_funcao_origem_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao;

		if	(ie_prescr_emergencia_w	<> 'S') and
			(cd_funcao_origem_w	<> 2314)  then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264);
		end if;
	end if;

	exception
	when others then

	if	(:new.dt_programada is not null) and
		(:new.dt_programada < sysdate) and
		(cd_funcao_origem_w	<> 2314) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264);
	end if;

	end;

else
	begin

	if	(((:old.dt_programada is null) and
		(:new.dt_programada is not null)) or
		((:new.dt_programada is not null) and
		(:new.dt_programada <> :old.dt_programada))) and
		(:new.dt_programada < sysdate) then

		select	nvl(max(ie_prescr_emergencia),'N'),
				max(cd_funcao_origem)
		into	ie_prescr_emergencia_w,
				cd_funcao_origem_w
		from	prescr_medica
		where	nr_prescricao = :new.nr_prescricao;

		if	(ie_prescr_emergencia_w	<> 'S') and
			(cd_funcao_origem_w	<> 2314)  then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264);
		end if;
	end if;

	exception
	when others then

	if	(((:old.dt_programada is null) and
		(:new.dt_programada is not null)) or
		((:new.dt_programada is not null) and
		(:new.dt_programada <> :old.dt_programada))) and
		(:new.dt_programada < sysdate) and
		(cd_funcao_origem_w	<> 2314) then

		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266264);
	end if;

	end;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_solic_bco_sangue_delete
before delete ON TASY.PRESCR_SOLIC_BCO_SANGUE FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
pragma autonomous_transaction;
BEGIN
begin
delete	med_avaliacao_paciente
where	nr_seq_prescr	= :old.nr_sequencia;

COMMIT;
exception
	when others then
      	dt_atualizacao := sysdate;
end;

end;
/


ALTER TABLE TASY.PRESCR_SOLIC_BCO_SANGUE ADD (
  CONSTRAINT PREHEMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_SOLIC_BCO_SANGUE ADD (
  CONSTRAINT PREHEMO_SANTIRE_FK 
 FOREIGN KEY (NR_SEQ_REACAO) 
 REFERENCES TASY.SAN_TIPO_REACAO (NR_SEQUENCIA),
  CONSTRAINT PREHEMO_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PREHEMO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PREHEMO_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PREHEMO_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PREHEMO_CPOHEMO_FK 
 FOREIGN KEY (NR_SEQ_HEMO_CPOE) 
 REFERENCES TASY.CPOE_HEMOTERAPIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_SOLIC_BCO_SANGUE TO NIVEL_1;

