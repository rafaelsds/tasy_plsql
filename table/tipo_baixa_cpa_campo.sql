ALTER TABLE TASY.TIPO_BAIXA_CPA_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_BAIXA_CPA_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_BAIXA_CPA_CAMPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TIPO_BAIXA        NUMBER(5)                NOT NULL,
  DS_ATRIBUTO          VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TBCPACA_ESTABEL_FK_I ON TASY.TIPO_BAIXA_CPA_CAMPO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TBCPACA_PK ON TASY.TIPO_BAIXA_CPA_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TBCPACA_PK
  MONITORING USAGE;


CREATE INDEX TASY.TBCPACA_TIPBACP_FK_I ON TASY.TIPO_BAIXA_CPA_CAMPO
(CD_TIPO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TBCPACA_TIPBACP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TIPO_BAIXA_CPA_CAMPO ADD (
  CONSTRAINT TBCPACA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_BAIXA_CPA_CAMPO ADD (
  CONSTRAINT TBCPACA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TBCPACA_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TIPO_BAIXA_CPA_CAMPO TO NIVEL_1;

