ALTER TABLE TASY.COMERCIAL_COMUNICADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMERCIAL_COMUNICADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMERCIAL_COMUNICADO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_TITULO               VARCHAR2(255 BYTE)    NOT NULL,
  DS_COMUNICADO           LONG                  NOT NULL,
  CD_PESSOA_ELABORACAO    VARCHAR2(10 BYTE)     NOT NULL,
  DT_LIBERACAO            DATE,
  DT_ENVIO                DATE,
  NR_SEQ_TIPO_COMUNICADO  NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCOMU_PESFISI_FK_I ON TASY.COMERCIAL_COMUNICADO
(CD_PESSOA_ELABORACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCOMU_PK ON TASY.COMERCIAL_COMUNICADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCOMU_PK
  MONITORING USAGE;


CREATE INDEX TASY.COMCOMU_TIPCOCO_FK_I ON TASY.COMERCIAL_COMUNICADO
(NR_SEQ_TIPO_COMUNICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCOMU_TIPCOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.COMERCIAL_COMUNICADO ADD (
  CONSTRAINT COMCOMU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMERCIAL_COMUNICADO ADD (
  CONSTRAINT COMCOMU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_ELABORACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COMCOMU_TIPCOCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMUNICADO) 
 REFERENCES TASY.TIPO_COMERCIAL_COMUNICADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMERCIAL_COMUNICADO TO NIVEL_1;

