DROP TABLE TASY.W_PAINEL_OS CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_PAINEL_OS
(
  NR_SEQ_TIPO_ORDEM    NUMBER(10),
  IE_STATUS            VARCHAR2(1 BYTE),
  DT_ORDEM             DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_ORDEM             NUMBER(10),
  QT_ORDEM2            NUMBER(10),
  QT_ORDEM3            NUMBER(10),
  QT_ORDEM4            NUMBER(10),
  QT_ORDEM5            NUMBER(10),
  QT_ORDEM6            NUMBER(10),
  QT_ORDEM7            NUMBER(10),
  QT_ORDEM8            NUMBER(10),
  QT_ORDEM9            NUMBER(10),
  QT_ORDEM10           NUMBER(10),
  QT_ORDEM11           NUMBER(10),
  QT_ORDEM12           NUMBER(10),
  QT_ORDEM13           NUMBER(10),
  QT_ORDEM14           NUMBER(10),
  QT_ORDEM15           NUMBER(10),
  QT_ORDEM16           NUMBER(10),
  QT_ORDEM17           NUMBER(10),
  QT_ORDEM18           NUMBER(10),
  QT_ORDEM19           NUMBER(10),
  QT_ORDEM20           NUMBER(10),
  QT_ORDEM21           NUMBER(10),
  QT_ORDEM22           NUMBER(10),
  QT_ORDEM23           NUMBER(10),
  QT_ORDEM24           NUMBER(10),
  QT_ORDEM25           NUMBER(10),
  QT_ORDEM26           NUMBER(10),
  QT_ORDEM27           NUMBER(10),
  QT_ORDEM28           NUMBER(10),
  QT_ORDEM29           NUMBER(10),
  QT_ORDEM30           NUMBER(10),
  QT_ORDEM31           NUMBER(10),
  IE_DIA_UTIL1         VARCHAR2(15 BYTE),
  IE_DIA_UTIL2         VARCHAR2(15 BYTE),
  IE_DIA_UTIL3         VARCHAR2(15 BYTE),
  IE_DIA_UTIL4         VARCHAR2(15 BYTE),
  IE_DIA_UTIL5         VARCHAR2(15 BYTE),
  IE_DIA_UTIL6         VARCHAR2(15 BYTE),
  IE_DIA_UTIL7         VARCHAR2(15 BYTE),
  IE_DIA_UTIL8         VARCHAR2(15 BYTE),
  IE_DIA_UTIL9         VARCHAR2(15 BYTE),
  IE_DIA_UTIL10        VARCHAR2(15 BYTE),
  IE_DIA_UTIL11        VARCHAR2(15 BYTE),
  IE_DIA_UTIL12        VARCHAR2(15 BYTE),
  IE_DIA_UTIL13        VARCHAR2(15 BYTE),
  IE_DIA_UTIL14        VARCHAR2(15 BYTE),
  IE_DIA_UTIL15        VARCHAR2(15 BYTE),
  IE_DIA_UTIL16        VARCHAR2(15 BYTE),
  IE_DIA_UTIL17        VARCHAR2(15 BYTE),
  IE_DIA_UTIL18        VARCHAR2(15 BYTE),
  IE_DIA_UTIL19        VARCHAR2(15 BYTE),
  IE_DIA_UTIL20        VARCHAR2(15 BYTE),
  IE_DIA_UTIL21        VARCHAR2(15 BYTE),
  IE_DIA_UTIL22        VARCHAR2(15 BYTE),
  IE_DIA_UTIL23        VARCHAR2(15 BYTE),
  IE_DIA_UTIL24        VARCHAR2(15 BYTE),
  IE_DIA_UTIL25        VARCHAR2(15 BYTE),
  IE_DIA_UTIL26        VARCHAR2(15 BYTE),
  IE_DIA_UTIL27        VARCHAR2(15 BYTE),
  IE_DIA_UTIL28        VARCHAR2(15 BYTE),
  IE_DIA_UTIL29        VARCHAR2(15 BYTE),
  IE_DIA_UTIL30        VARCHAR2(15 BYTE),
  IE_DIA_UTIL31        VARCHAR2(15 BYTE),
  IE_TIPO_CAMPO        VARCHAR2(15 BYTE),
  PR_ATRASO            NUMBER(15,4),
  QT_ATRASADA          NUMBER(10),
  QT_TOTAL_STATUS      NUMBER(10),
  QT_TOTAL_ATRAS       NUMBER(10),
  QT_TOTAL_OS          NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_PAINEL_OS TO NIVEL_1;

