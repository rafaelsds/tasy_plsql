ALTER TABLE TASY.PLS_FATURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FATURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FATURA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  DT_VENCIMENTO            DATE,
  NR_TITULO                NUMBER(10),
  VL_FATURA                NUMBER(15,2),
  NR_SEQ_PAGADOR           NUMBER(10),
  NR_SEQ_CONGENERE         NUMBER(10),
  DS_ARQUIVO               VARCHAR2(255 BYTE),
  DT_GERACAO_ARQ           DATE,
  NM_USUARIO_ARQ           VARCHAR2(15 BYTE),
  NR_AGRUPAMENTO           NUMBER(10),
  IE_IMPEDIMENTO_COBRANCA  VARCHAR2(3 BYTE),
  DT_MES_COMPETENCIA       DATE,
  IE_CANCELAMENTO          VARCHAR2(1 BYTE),
  NR_LOTE_CONTABIL         NUMBER(10),
  NR_SEQ_CANCEL_FAT        NUMBER(10),
  NR_TITULO_NDC            NUMBER(10),
  DT_VENCIMENTO_NDC        DATE,
  VL_TOTAL_NDC             NUMBER(15,2),
  IE_TIT_FAT_NDC           VARCHAR2(1 BYTE),
  IE_NF_FAT_NDC            VARCHAR2(1 BYTE),
  NR_SEQ_FATURA_ORIGEM     NUMBER(10),
  NR_FATURA                NUMBER(10),
  IE_TIPO_RELACAO          VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_PRESTADOR   NUMBER(10),
  IE_ORIGEM_FAT            VARCHAR2(1 BYTE),
  DT_CANCELAMENTO_FAT      DATE,
  DT_GERACAO_PTU           DATE,
  NR_SEQ_FAT_DIVISAO       NUMBER(10),
  NR_SEQ_MOTIVO_CANC       NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  DT_POSTAGEM_ARQUIVO      DATE,
  NM_USUARIO_ENVIO         VARCHAR2(15 BYTE),
  DT_EMISSAO               DATE,
  DT_GERACAO_XML           DATE,
  VL_ATO_PRINC             NUMBER(15,2),
  VL_ATO_SECUNDARIO        NUMBER(15,2),
  VL_ATO_AUX               NUMBER(15,2),
  DT_VENCIMENTO_MANUAL     DATE,
  IE_REMIDO                VARCHAR2(1 BYTE),
  IE_FATURA_TAXA           VARCHAR2(1 BYTE),
  NR_TITULO_LIQ            NUMBER(10),
  IE_TIPO_FATURA           VARCHAR2(1 BYTE)     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSFATU_LOTCONT_FK_I ON TASY.PLS_FATURA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSFATU_PK ON TASY.PLS_FATURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATU_PLCONPAG_FK_I ON TASY.PLS_FATURA
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLCONPAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_PLSCONG_FK_I ON TASY.PLS_FATURA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_PLSCREF_FK_I ON TASY.PLS_FATURA
(NR_SEQ_CANCEL_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLSCREF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_PLSFATU_FK_I ON TASY.PLS_FATURA
(NR_SEQ_FATURA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLSFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_PLSFATU_FK2_I ON TASY.PLS_FATURA
(NR_SEQ_FAT_DIVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATU_PLSLOFA_FK_I ON TASY.PLS_FATURA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLSLOFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_PLSMCFA_FK_I ON TASY.PLS_FATURA
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATU_PLSPRGP_FK_I ON TASY.PLS_FATURA
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFATU_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFATU_TITRECE_FK_I ON TASY.PLS_FATURA
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATU_TITRECE_FK2_I ON TASY.PLS_FATURA
(NR_TITULO_NDC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_fatura_atual
before insert or update ON TASY.PLS_FATURA for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: N�o ter loucuras na gera��o do t�tulo. Exemplo: Vencimento para 1970 ou pra 2235
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(:new.dt_vencimento <> :old.dt_vencimento) and
		(:new.dt_vencimento is not null) then
		if	(trunc(:new.dt_vencimento) < trunc(sysdate)) then -- Menor que a data atual
			wheb_mensagem_pck.exibir_mensagem_abort(260592);
		end if;

		if	(trunc(:new.dt_vencimento) > trunc(add_months(sysdate,120))) then -- Acima de 5 anos
			wheb_mensagem_pck.exibir_mensagem_abort(260593);
		end if;
	end if;

	if	(:new.dt_vencimento_ndc <> :old.dt_vencimento_ndc) and
		(:new.dt_vencimento_ndc is not null) then
		if	(trunc(:new.dt_vencimento_ndc) < trunc(sysdate)) then -- Menor que a data atual
			wheb_mensagem_pck.exibir_mensagem_abort(260592);
		end if;

		if	(trunc(:new.dt_vencimento_ndc) > trunc(add_months(sysdate,120))) then -- Acima de 5 anos
			wheb_mensagem_pck.exibir_mensagem_abort(260593);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_FATURA_tp  after update ON TASY.PLS_FATURA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_RELACAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_FATURA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_FATURA ADD (
  CONSTRAINT PLSFATU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FATURA ADD (
  CONSTRAINT PLSFATU_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_PLSLOFA_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT PLSFATU_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSFATU_PLSCREF_FK 
 FOREIGN KEY (NR_SEQ_CANCEL_FAT) 
 REFERENCES TASY.PLS_CANCEL_REC_FATURA (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_TITRECE_FK2 
 FOREIGN KEY (NR_TITULO_NDC) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT PLSFATU_PLSFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA_ORIGEM) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_PLSFATU_FK2 
 FOREIGN KEY (NR_SEQ_FAT_DIVISAO) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT PLSFATU_PLSMCFA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.PLS_MOTIVO_CANC_FAT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FATURA TO NIVEL_1;

