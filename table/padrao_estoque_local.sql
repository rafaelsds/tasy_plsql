ALTER TABLE TASY.PADRAO_ESTOQUE_LOCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PADRAO_ESTOQUE_LOCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PADRAO_ESTOQUE_LOCAL
(
  CD_LOCAL_ESTOQUE          NUMBER(4)           NOT NULL,
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_ESTOQUE_MAXIMO         NUMBER(13,4),
  QT_ESTOQUE_MINIMO         NUMBER(13,4),
  CD_LOCAL_ATENDE           NUMBER(4)           NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_CONTAGEM           NUMBER(5),
  DS_LOCALIZACAO            VARCHAR2(80 BYTE),
  IE_ATUALIZA_PADRAO_LOCAL  VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_ESTRUT_MAT         NUMBER(10),
  QT_CONSUMO                NUMBER(17,4),
  QT_DIA_AJUSTE_MINIMO      NUMBER(5),
  QT_DIA_AJUSTE_MAXIMO      NUMBER(5),
  QT_DIA_CONSUMO            NUMBER(5),
  QT_MINIMO_AJUSTE          NUMBER(13,4),
  QT_EMER_STK               NUMBER(13,4),
  VL_EMR_STK_PER            NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PADESLO_I1 ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_MATERIAL, CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PADESLO_LOCESAT_FK_I ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_LOCAL_ATENDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PADESLO_LOCESAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PADESLO_LOCESTO_FK_I ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PADESLO_MATERIA_FK_I ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PADESLO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PADESLO_MATESTR_FK_I ON TASY.PADRAO_ESTOQUE_LOCAL
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PADESLO_MATESTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PAESLOC_ESTABEL_FK_I ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PREMATE_PK ON TASY.PADRAO_ESTOQUE_LOCAL
(CD_LOCAL_ESTOQUE, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PADRAO_ESTOQUE_LOCAL_tp  after update ON TASY.PADRAO_ESTOQUE_LOCAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_LOCAL_ESTOQUE='||to_char(:old.CD_LOCAL_ESTOQUE)||'#@#@CD_MATERIAL='||to_char(:old.CD_MATERIAL);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_AJUSTE,1,4000),substr(:new.QT_MINIMO_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_AJUSTE',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ATENDE,1,4000),substr(:new.CD_LOCAL_ATENDE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ATENDE',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ESTOQUE_MAXIMO,1,4000),substr(:new.QT_ESTOQUE_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_ESTOQUE_MAXIMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOCALIZACAO,1,4000),substr(:new.DS_LOCALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOCALIZACAO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ESTOQUE_MINIMO,1,4000),substr(:new.QT_ESTOQUE_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_ESTOQUE_MINIMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTAGEM,1,4000),substr(:new.NR_SEQ_CONTAGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTAGEM',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZA_PADRAO_LOCAL,1,4000),substr(:new.IE_ATUALIZA_PADRAO_LOCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZA_PADRAO_LOCAL',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUT_MAT,1,4000),substr(:new.NR_SEQ_ESTRUT_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUT_MAT',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONSUMO,1,4000),substr(:new.QT_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONSUMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_AJUSTE_MAXIMO,1,4000),substr(:new.QT_DIA_AJUSTE_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_AJUSTE_MAXIMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_AJUSTE_MINIMO,1,4000),substr(:new.QT_DIA_AJUSTE_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_AJUSTE_MINIMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_CONSUMO,1,4000),substr(:new.QT_DIA_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_CONSUMO',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'PADRAO_ESTOQUE_LOCAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.padrao_estoque_local_delete
before delete ON TASY.PADRAO_ESTOQUE_LOCAL for each row
begin
gerar_int_dankia_pck.dankia_disp_material(:old.cd_material,:old.cd_local_estoque,'E',:old.nm_usuario);
gerar_int_dankia_pck.dankia_disp_padrao_local(:old.cd_material,:old.cd_local_estoque,:old.qt_estoque_minimo,:old.qt_estoque_maximo,'E',:old.nr_seq_estrut_mat,:old.nm_usuario);
end;
/


CREATE OR REPLACE TRIGGER TASY.padrao_estoque_local_atual
after insert or update ON TASY.PADRAO_ESTOQUE_LOCAL for each row
begin

if	(inserting)  or
	(:old.cd_estabelecimento <> :new.cd_estabelecimento) or
	(:old.cd_local_estoque <> :new.cd_local_estoque) or
	(:old.cd_material <> :new.cd_material) or
	(:old.qt_estoque_maximo <> :new.qt_estoque_maximo) or
	(:old.qt_estoque_minimo <> :new.qt_estoque_minimo) then

	int_disp_padrao_local(
			:new.cd_estabelecimento,
			:new.cd_local_estoque,
			:new.cd_material,
			:new.qt_estoque_maximo,
			:new.qt_estoque_minimo,
			:new.nm_usuario);
end if;
if	(inserting) then
	gerar_int_dankia_pck.dankia_disp_material(:new.cd_material,:new.cd_local_estoque,'I',:new.nm_usuario);
	gerar_int_dankia_pck.dankia_disp_padrao_local(:new.cd_material,:new.cd_local_estoque,:new.qt_estoque_minimo,:new.qt_estoque_maximo,'I',:new.nr_seq_estrut_mat,:new.nm_usuario);
elsif(updating) then
	if (nvl(:new.cd_material,0) <> nvl(:old.cd_material,0)) then
		gerar_int_dankia_pck.dankia_disp_material(:old.cd_material,:old.cd_local_estoque,'E',:old.nm_usuario);
		gerar_int_dankia_pck.dankia_disp_padrao_local(:old.cd_material,:old.cd_local_estoque,:old.qt_estoque_minimo,:old.qt_estoque_maximo,'E',:old.nr_seq_estrut_mat,:old.nm_usuario);

		gerar_int_dankia_pck.dankia_disp_material(:new.cd_material,:new.cd_local_estoque,'I',:new.nm_usuario);
		gerar_int_dankia_pck.dankia_disp_padrao_local(:new.cd_material,:new.cd_local_estoque,:new.qt_estoque_minimo,:new.qt_estoque_maximo,'I',:new.nr_seq_estrut_mat,:new.nm_usuario);

	elsif (nvl(:new.cd_local_estoque,0) <> nvl(:old.cd_local_estoque,0)) or
		(nvl(:new.qt_estoque_minimo,0) <> nvl(:old.qt_estoque_minimo,0)) or
		(nvl(:new.qt_estoque_maximo,0) <> nvl(:old.qt_estoque_maximo,0)) or
		(nvl(:new.nr_seq_estrut_mat,0) <> nvl(:old.nr_seq_estrut_mat,0)) then
			gerar_int_dankia_pck.dankia_disp_material(:new.cd_material,:new.cd_local_estoque,'A',:new.nm_usuario);
			gerar_int_dankia_pck.dankia_disp_padrao_local(:new.cd_material,:new.cd_local_estoque,:new.qt_estoque_minimo,:new.qt_estoque_maximo,'A',:new.nr_seq_estrut_mat,:new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.PADRAO_ESTOQUE_LOCAL ADD (
  CONSTRAINT PREMATE_PK
 PRIMARY KEY
 (CD_LOCAL_ESTOQUE, CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PADRAO_ESTOQUE_LOCAL ADD (
  CONSTRAINT PADESLO_MATESTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.MAT_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PADESLO_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT PADESLO_LOCESAT_FK 
 FOREIGN KEY (CD_LOCAL_ATENDE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT PADESLO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PAESLOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PADRAO_ESTOQUE_LOCAL TO NIVEL_1;

