ALTER TABLE TASY.DECLARACAO_OBITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DECLARACAO_OBITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DECLARACAO_OBITO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  IE_UTILIZACAO              VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  NM_PACIENTE                VARCHAR2(60 BYTE),
  DT_OBITO                   DATE,
  CD_MEDICO                  VARCHAR2(10 BYTE)  NOT NULL,
  IE_POS_CIRURGICO           VARCHAR2(1 BYTE),
  CD_CID_DIRETA              VARCHAR2(10 BYTE),
  CD_CID_ADIC_1              VARCHAR2(10 BYTE),
  CD_CID_ADIC_2              VARCHAR2(10 BYTE),
  CD_CID_BASICA              VARCHAR2(10 BYTE),
  NR_SEQ_CAUSA_MORTE         NUMBER(10),
  NM_PESSOA_RETIRADA         VARCHAR2(60 BYTE),
  CD_CID_ADIC_3              VARCHAR2(10 BYTE),
  CD_CID_ADIC_4              VARCHAR2(10 BYTE),
  IE_EMISSOR                 VARCHAR2(3 BYTE),
  IE_OBITO_MULHER            VARCHAR2(2 BYTE),
  NR_DECLARACAO              VARCHAR2(20 BYTE)  NOT NULL,
  CD_PERFIL_ATIVO            NUMBER(5),
  CD_MEDICO_ADICIONAL        VARCHAR2(10 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DS_ARQUIVO                 VARCHAR2(2000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_TIPO_MORT_FETAL         NUMBER(5),
  IE_TIPO_CERTIF_OBT         NUMBER(1),
  NR_SEQ_LOCAL_OBITO         NUMBER(10),
  IE_RECEBEU_ATENDIMENTO     VARCHAR2(1 BYTE),
  IE_OBITO_TRABALHO          VARCHAR2(1 BYTE),
  IE_AUTOPSIA                VARCHAR2(1 BYTE),
  IE_ASSINOU_CERT_OBITO      VARCHAR2(1 BYTE),
  DS_LIVRO_CERT_OBITO        VARCHAR2(10 BYTE)  DEFAULT null,
  NR_CERTIFICADO             VARCHAR2(20 BYTE),
  NR_SEQ_PARENTESCO          NUMBER(10),
  NR_SEQ_PERSON_NAME         NUMBER(10),
  IE_MORTE_COMPL_GRAVIDEZ    VARCHAR2(2 BYTE),
  NR_SEQ_OCOR_ENDERECO       NUMBER(10),
  NR_SEQ_END_ACIDENTE        NUMBER(10),
  CD_LUGAR_OCOR              VARCHAR2(2 BYTE),
  IE_PARENTESCO_AGRESSOR     VARCHAR2(2 BYTE),
  NR_NOTIFICACAO             NUMBER(10),
  DS_DIAGNOSTICO             VARCHAR2(255 BYTE),
  IE_GRAVIDEZ_CAUSOU_MORTE   VARCHAR2(2 BYTE),
  NR_SEQ_PERSON_CERT         NUMBER(10),
  DS_ESPC_CERT               VARCHAR2(25 BYTE),
  DS_CEDULA_PROF_CERT        VARCHAR2(20 BYTE),
  NR_SEQ_CERT_ENDERECO       NUMBER(10),
  NR_TELEFONE_CERT           VARCHAR2(15 BYTE),
  NR_DDI_TELEFONE_CERT       VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE_CERT       VARCHAR2(3 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_ACAO                    VARCHAR2(1 BYTE),
  IE_CORONER_CASE            VARCHAR2(1 BYTE),
  DS_CAUSA_MORTE             VARCHAR2(255 BYTE),
  IE_TEVE_ATENCAO_MEDICA     VARCHAR2(1 BYTE),
  IE_MEDICO_CERTIDAO         VARCHAR2(1 BYTE),
  IE_MORTE_VIOLENTA          VARCHAR2(1 BYTE),
  IE_MORTE_GRAVIDA           VARCHAR2(1 BYTE),
  IE_CERTIFICADO_POR         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DECOBIT_ATEPACI_FK_I ON TASY.DECLARACAO_OBITO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_CASITI_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_LOCAL_OBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_CAUSAMO_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_CAUSA_MORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CAUSAMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK_I ON TASY.DECLARACAO_OBITO
(CD_CID_DIRETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK2_I ON TASY.DECLARACAO_OBITO
(CD_CID_ADIC_1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK3_I ON TASY.DECLARACAO_OBITO
(CD_CID_ADIC_2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK4_I ON TASY.DECLARACAO_OBITO
(CD_CID_BASICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK5_I ON TASY.DECLARACAO_OBITO
(CD_CID_ADIC_3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_CIDDOEN_FK6_I ON TASY.DECLARACAO_OBITO
(CD_CID_ADIC_4)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_CIDDOEN_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_GRAUPA_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_I1 ON TASY.DECLARACAO_OBITO
(DT_OBITO, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_I1
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_PERFIL_FK_I ON TASY.DECLARACAO_OBITO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DECOBIT_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DECOBIT_PESFISI_FK_I ON TASY.DECLARACAO_OBITO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_PESFISI_FK1_I ON TASY.DECLARACAO_OBITO
(CD_MEDICO_ADICIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_PESSEND_ACI_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_END_ACIDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_PESSEND_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_OCOR_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_PESSEND_FK3_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_CERT_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DECOBIT_PK ON TASY.DECLARACAO_OBITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_TASASDI_FK_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DECOBIT_TASASDI_FK2_I ON TASY.DECLARACAO_OBITO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DECLARACAO_OBITO_ATUAL
before insert or update ON TASY.DECLARACAO_OBITO for each row
declare
qt_reg_w	number(10);
nr_atendimento_w	number(10);
dt_nascimento_w		date;

pragma autonomous_transaction;

begin

if	(:new.dt_obito is not null) and
	(:new.dt_obito >= sysdate) then
	wheb_mensagem_pck.exibir_mensagem_abort(235412);
end if;

if	(nvl(:old.DT_OBITO,sysdate+10) <> :new.DT_OBITO) and
	(:new.DT_OBITO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_OBITO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	begin
		select	nr_atendimento
		into	nr_atendimento_w
		from	declaracao_obito
		where	nr_declaracao = :new.nr_declaracao
		and		nr_atendimento <> :new.nr_atendimento
		and	nvl(ie_situacao,'A') = 'A'
		and	rownum = 1;
	exception
	when 	no_data_found then
			nr_atendimento_w := 0;
	end;

	if	(nr_atendimento_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(70998,'ITEM='||to_char(nr_atendimento_w));
	end if;
END IF;


select	max(a.dt_nascimento)
into	dt_nascimento_w
from	pessoa_fisica a,
		atendimento_paciente b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and		b.nr_atendimento	= :new.nr_atendimento;

if	(dt_nascimento_w	is not null) and
	(:new.DT_OBITO is not null) and
	(:new.DT_OBITO	< dt_nascimento_w)then
	wheb_mensagem_pck.exibir_mensagem_abort(356157);
end if;

atualizar_dados_obito_pac(:new.dt_liberacao, :old.dt_liberacao, :new.nm_usuario, :new.nr_atendimento, :new.dt_obito, :old.dt_obito, :new.cd_cid_direta);

if	(NVL(:new.NR_DECLARACAO,'0') <> NVL(:old.NR_DECLARACAO,'0')) and
	(length(somente_numero(:new.NR_DECLARACAO))	= length(:new.NR_DECLARACAO)) and
	(length(:new.NR_DECLARACAO) <= 10) then


	begin
	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'S'
	where	nr_declaracao	= :old.NR_DECLARACAO
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DO');
	exception
		when others then
		null;
	end;


	select	count(*)
	into	qt_reg_w
	from	regra_numeracao_declaracao a,
		REGRA_NUMERACAO_DEC_ITEM b
	where	a.nr_sequencia	= b.NR_SEQ_REGRA_NUM
	and	nr_declaracao	= :new.NR_DECLARACAO
	and	IE_DISPONIVEL	<> 'S'
	and	a.ie_situacao	= 'A'
	and	a.ie_tipo_numeracao	= 'DO';


	if	(qt_reg_w	> 0)  then
		-- Numera��o DO j� utilizada / Cancelada. #@#@
		Wheb_mensagem_pck.exibir_mensagem_abort(263595);
	end if;

	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'N'
	where	nr_declaracao	= :new.NR_DECLARACAO
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DO');
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.declaracao_obito_delete
before delete ON TASY.DECLARACAO_OBITO for each row
declare

begin

if	(:old.NR_DECLARACAO is not null) and
	(length(somente_numero(:old.NR_DECLARACAO))	= length(:old.NR_DECLARACAO)) then
	update	REGRA_NUMERACAO_DEC_ITEM a
	set	IE_DISPONIVEL = 'S'
	where	nr_declaracao	= :old.NR_DECLARACAO
	and	exists	(	select	1
				from	regra_numeracao_declaracao b
				where	a.NR_SEQ_REGRA_NUM	= b.nr_sequencia
				and	b.ie_situacao	= 'A'
				and	b.ie_tipo_numeracao	= 'DO');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.declaracao_obito_insert
after insert ON TASY.DECLARACAO_OBITO for each row
declare

begin
atualizar_dados_obito_pac(:new.dt_liberacao, :old.dt_liberacao, :new.nm_usuario, :new.nr_atendimento, :new.dt_obito, :old.dt_obito, :new.cd_cid_direta);
end;
/


CREATE OR REPLACE TRIGGER TASY.DECLARACAO_OBITO_SBIS_IN
before insert or update ON TASY.DECLARACAO_OBITO for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);
begin
	IF (INSERTING) THEN
		select 	log_alteracao_prontuario_seq.nextval
		into 	nr_log_seq_w
		from 	dual;

		insert into log_alteracao_prontuario (nr_sequencia,
											 dt_atualizacao,
											 ds_evento,
											 ds_maquina,
											 cd_pessoa_fisica,
											 ds_item,
											 nr_atendimento,
											 dt_liberacao,
											 dt_inativacao,
											 nm_usuario) values
											 (nr_log_seq_w,
											 sysdate,
											 obter_desc_expressao(656665) ,
											 wheb_usuario_pck.get_nm_maquina,
											 obter_pessoa_atendimento(:new.nr_atendimento,'C'),
											 obter_desc_expressao(314115),
											 :new.nr_atendimento,
											 :new.dt_liberacao,
											 :new.dt_inativacao,
											 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
	else
		ie_inativacao_w := 'N';

		if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
			ie_inativacao_w := 'S';
		end if;

		select 	log_alteracao_prontuario_seq.nextval
		into 	nr_log_seq_w
		from 	dual;

		insert into log_alteracao_prontuario (nr_sequencia,
											 dt_atualizacao,
											 ds_evento,
											 ds_maquina,
											 cd_pessoa_fisica,
											 ds_item,
											 nr_atendimento,
											 dt_liberacao,
											 dt_inativacao,
											 nm_usuario) values
											 (nr_log_seq_w,
											 sysdate,
											 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
											 wheb_usuario_pck.get_nm_maquina,
											 obter_pessoa_atendimento(:new.nr_atendimento,'C'),
											 obter_desc_expressao(314115),
											 :new.nr_atendimento,
											 :new.dt_liberacao,
											 :new.dt_inativacao,
											 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.declaracao_obito_bf_insert
before insert ON TASY.DECLARACAO_OBITO for each row
declare

qt_reg_w		number(10)				:= 0;
ie_lib_obito_w		parametro_medico.ie_lib_obito%type	:= 'N';
nr_atendimento_w	declaracao_obito.nr_atendimento%type	:= 0;
ie_inativa_obito_w	varchar2(2)				:= 'N';

pragma autonomous_transaction;
begin

ie_inativa_obito_w := nvl(obter_valor_param_usuario(916, 1125, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo),'N');

if ((ie_inativa_obito_w = 'N') or ((ie_inativa_obito_w = 'S') and (obter_funcao_ativa != 916))) then


	begin
		select	nvl(MAX(ie_lib_obito),'N')
		into	ie_lib_obito_w
		from	parametro_medico
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	exception
	when others then
		ie_lib_obito_w := 'N';
	end;

	begin
		select	nr_atendimento
		into	nr_atendimento_w
		from	declaracao_obito
		where	nr_declaracao = :new.nr_declaracao
		and	nvl(ie_situacao,'A') = 'A'
		and	(ie_lib_obito_w = 'N' or dt_liberacao is not null)
		and	rownum = 1;
	exception
	when 	no_data_found then
		nr_atendimento_w := 0;
	end;

	if	(nr_atendimento_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(70998,'ITEM='||to_char(nr_atendimento_w));
	else
		if	(ie_lib_obito_w = 'S') then

			begin
				select	1
				into	qt_reg_w
				from	declaracao_obito a,
						atendimento_paciente b
				where	a.nr_atendimento = b.nr_atendimento
				and	b.cd_pessoa_fisica = obter_pessoa_atendimento(:new.nr_atendimento, 'C')
				and	nvl(a.ie_situacao,'A') = 'A'
				and	nvl(a.ie_rn,'N') = 'N'
				and	rownum = 1;
			exception
			when 	no_data_found then
				qt_reg_w := 0;
			end;

			if	(qt_reg_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(319605);
			end if;

		end if;
	end if;
end if;
commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.declaracao_obito_pend_atual
after insert or update or delete ON TASY.DECLARACAO_OBITO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_declaracao_w	varchar2(10);
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select  max(IE_LIB_OBITO)
into	   ie_lib_declaracao_w
from	   parametro_medico
where   cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

begin

	if	(inserting) or
		(updating) then

		if	(nvl(ie_lib_declaracao_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'DOB';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XDOB';
			end if;


			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
			end if;


		end if;
	elsif	(deleting) then

		delete 	from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'DOB'
		and	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

		commit;

	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_DECLARACAO_OBITO
before insert or update ON TASY.DECLARACAO_OBITO for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.DECLARACAO_OBITO_SBIS_DEL
before delete ON TASY.DECLARACAO_OBITO for each row
declare
nr_log_seq_w		number(10);


begin
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 dt_liberacao,
										 dt_inativacao,
										 nm_usuario) values
										 (nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(493387) ,
										 wheb_usuario_pck.get_nm_maquina,
										 nvl(obter_pessoa_fisica_usuario(wheb_usuario_pck.get_nm_usuario,'C'),'TASY'),
										 obter_desc_expressao(314115),
										  :old.nr_atendimento,
										 :old.dt_liberacao,
										 :old.dt_inativacao,
										 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


ALTER TABLE TASY.DECLARACAO_OBITO ADD (
  CONSTRAINT DECOBIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DECLARACAO_OBITO ADD (
  CONSTRAINT DECOBIT_CASITI_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_OBITO) 
 REFERENCES TASY.CAT_SITIOS (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_PESSEND_ACI_FK 
 FOREIGN KEY (NR_SEQ_END_ACIDENTE) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_OCOR_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_PESSEND_FK3 
 FOREIGN KEY (NR_SEQ_CERT_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DECOBIT_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DECOBIT_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_DIRETA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_CIDDOEN_FK2 
 FOREIGN KEY (CD_CID_ADIC_1) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_CIDDOEN_FK3 
 FOREIGN KEY (CD_CID_ADIC_2) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_CIDDOEN_FK4 
 FOREIGN KEY (CD_CID_BASICA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_CAUSAMO_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_MORTE) 
 REFERENCES TASY.CAUSA_MORTE (NR_SEQUENCIA),
  CONSTRAINT DECOBIT_CIDDOEN_FK5 
 FOREIGN KEY (CD_CID_ADIC_3) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_CIDDOEN_FK6 
 FOREIGN KEY (CD_CID_ADIC_4) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DECOBIT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT DECOBIT_PESFISI_FK1 
 FOREIGN KEY (CD_MEDICO_ADICIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.DECLARACAO_OBITO TO NIVEL_1;

