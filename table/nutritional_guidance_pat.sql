ALTER TABLE TASY.NUTRITIONAL_GUIDANCE_PAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUTRITIONAL_GUIDANCE_PAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUTRITIONAL_GUIDANCE_PAT
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_CALORIES               NUMBER(6),
  QT_PROTEIN                NUMBER(6),
  QT_LIPID                  NUMBER(6),
  QT_CARBOHYDRATE           NUMBER(6),
  QT_SALT                   NUMBER(6),
  QT_POTASSIUM              NUMBER(6),
  QT_WATER                  NUMBER(6),
  QT_PHOSPHORUS             NUMBER(6),
  QT_IRON                   NUMBER(6),
  QT_CALCIUM                NUMBER(6),
  NM_GUIDANCE               VARCHAR2(255 BYTE)  NOT NULL,
  QT_PS_RATIO               NUMBER(2,1),
  QT_COMPOSITION_CARB       NUMBER(5,2),
  QT_VITAMIN                NUMBER(6),
  QT_COMPOSITION_POTASSIUM  NUMBER(5,2),
  QT_COMPOSITION_FAT        NUMBER(5,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.NUGUPA_PK ON TASY.NUTRITIONAL_GUIDANCE_PAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NUTRITIONAL_GUIDANCE_PAT ADD (
  CONSTRAINT NUGUPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

