ALTER TABLE TASY.OPDM_TRADE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_TRADE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_TRADE_ITEM
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_OPDM_TASY               NUMBER(10),
  CD_TRADE_ITEM_ID               NUMBER(10),
  CD_TRADE_ITEM_TYPE_ID          VARCHAR2(40 BYTE),
  NM_TRADE_ITEM                  VARCHAR2(100 BYTE),
  CD_INTERNAL_DEVICE_IDENT       NUMBER(10),
  CD_INTERNAL_DEVICE_IDENT_TYPE  VARCHAR2(40 BYTE),
  CD_GTIN                        VARCHAR2(50 BYTE),
  DS_GTIN_TYPE                   VARCHAR2(50 BYTE),
  DS_UNIT_DESCRIPTOR             VARCHAR2(50 BYTE),
  CD_REFERENCE_NUMBER            NUMBER(10),
  IE_REF_NUMBER_ACCORD_OPDM      VARCHAR2(1 BYTE),
  NM_MODEL                       VARCHAR2(50 BYTE),
  IE_MODEL_ACCORD_OPDM           VARCHAR2(1 BYTE),
  DS_MATURITY                    VARCHAR2(50 BYTE),
  NR_DEVICE_COUNT                NUMBER(3),
  DS_INSTRUCTION_USE             VARCHAR2(255 BYTE),
  CD_GLOBAL_PROD_CLASS_CODE      VARCHAR2(50 BYTE),
  IE_CONTROL_LOT                 VARCHAR2(1 BYTE),
  IE_CONTROL_DOM                 VARCHAR2(1 BYTE),
  IE_CONTROL_DOB                 VARCHAR2(1 BYTE),
  DS_MED_DEV_DATA_BLOCK_FILE     VARCHAR2(50 BYTE),
  DS_MDRD_EU_FILENAME            VARCHAR2(50 BYTE),
  NM_CATALOG_ITEM                VARCHAR2(50 BYTE),
  IE_GREEN_PRODUCT               VARCHAR2(1 BYTE),
  IE_SOFTWARE_PRODUCT            VARCHAR2(1 BYTE),
  DS_LEGAL_MANUFACTURER          VARCHAR2(255 BYTE),
  NM_ENGINEERING_RESP_ORG        VARCHAR2(255 BYTE),
  DS_TYPE_DEVICE                 VARCHAR2(50 BYTE),
  NM_BASE_MODEL                  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPDMTRAIT_OPDMTASY_FK_I ON TASY.OPDM_TRADE_ITEM
(NR_SEQ_OPDM_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPDMTRAIT_PK ON TASY.OPDM_TRADE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_TRADE_ITEM_tp  after update ON TASY.OPDM_TRADE_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_BASE_MODEL,1,4000),substr(:new.NM_BASE_MODEL,1,4000),:new.nm_usuario,nr_seq_w,'NM_BASE_MODEL',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TRADE_ITEM_ID,1,4000),substr(:new.CD_TRADE_ITEM_ID,1,4000),:new.nm_usuario,nr_seq_w,'CD_TRADE_ITEM_ID',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TRADE_ITEM_TYPE_ID,1,4000),substr(:new.CD_TRADE_ITEM_TYPE_ID,1,4000),:new.nm_usuario,nr_seq_w,'CD_TRADE_ITEM_TYPE_ID',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_TRADE_ITEM,1,4000),substr(:new.NM_TRADE_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NM_TRADE_ITEM',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERNAL_DEVICE_IDENT,1,4000),substr(:new.CD_INTERNAL_DEVICE_IDENT,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNAL_DEVICE_IDENT',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERNAL_DEVICE_IDENT_TYPE,1,4000),substr(:new.CD_INTERNAL_DEVICE_IDENT_TYPE,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNAL_DEVICE_IDENT_TYPE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GTIN,1,4000),substr(:new.CD_GTIN,1,4000),:new.nm_usuario,nr_seq_w,'CD_GTIN',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_GTIN_TYPE,1,4000),substr(:new.DS_GTIN_TYPE,1,4000),:new.nm_usuario,nr_seq_w,'DS_GTIN_TYPE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_UNIT_DESCRIPTOR,1,4000),substr(:new.DS_UNIT_DESCRIPTOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_UNIT_DESCRIPTOR',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_REFERENCE_NUMBER,1,4000),substr(:new.CD_REFERENCE_NUMBER,1,4000),:new.nm_usuario,nr_seq_w,'CD_REFERENCE_NUMBER',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REF_NUMBER_ACCORD_OPDM,1,4000),substr(:new.IE_REF_NUMBER_ACCORD_OPDM,1,4000),:new.nm_usuario,nr_seq_w,'IE_REF_NUMBER_ACCORD_OPDM',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_MODEL,1,4000),substr(:new.NM_MODEL,1,4000),:new.nm_usuario,nr_seq_w,'NM_MODEL',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MODEL_ACCORD_OPDM,1,4000),substr(:new.IE_MODEL_ACCORD_OPDM,1,4000),:new.nm_usuario,nr_seq_w,'IE_MODEL_ACCORD_OPDM',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATURITY,1,4000),substr(:new.DS_MATURITY,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATURITY',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DEVICE_COUNT,1,4000),substr(:new.NR_DEVICE_COUNT,1,4000),:new.nm_usuario,nr_seq_w,'NR_DEVICE_COUNT',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INSTRUCTION_USE,1,4000),substr(:new.DS_INSTRUCTION_USE,1,4000),:new.nm_usuario,nr_seq_w,'DS_INSTRUCTION_USE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GLOBAL_PROD_CLASS_CODE,1,4000),substr(:new.CD_GLOBAL_PROD_CLASS_CODE,1,4000),:new.nm_usuario,nr_seq_w,'CD_GLOBAL_PROD_CLASS_CODE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROL_LOT,1,4000),substr(:new.IE_CONTROL_LOT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROL_LOT',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROL_DOM,1,4000),substr(:new.IE_CONTROL_DOM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROL_DOM',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROL_DOB,1,4000),substr(:new.IE_CONTROL_DOB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROL_DOB',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MED_DEV_DATA_BLOCK_FILE,1,4000),substr(:new.DS_MED_DEV_DATA_BLOCK_FILE,1,4000),:new.nm_usuario,nr_seq_w,'DS_MED_DEV_DATA_BLOCK_FILE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MDRD_EU_FILENAME,1,4000),substr(:new.DS_MDRD_EU_FILENAME,1,4000),:new.nm_usuario,nr_seq_w,'DS_MDRD_EU_FILENAME',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_CATALOG_ITEM,1,4000),substr(:new.NM_CATALOG_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NM_CATALOG_ITEM',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GREEN_PRODUCT,1,4000),substr(:new.IE_GREEN_PRODUCT,1,4000),:new.nm_usuario,nr_seq_w,'IE_GREEN_PRODUCT',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOFTWARE_PRODUCT,1,4000),substr(:new.IE_SOFTWARE_PRODUCT,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOFTWARE_PRODUCT',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LEGAL_MANUFACTURER,1,4000),substr(:new.DS_LEGAL_MANUFACTURER,1,4000),:new.nm_usuario,nr_seq_w,'DS_LEGAL_MANUFACTURER',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ENGINEERING_RESP_ORG,1,4000),substr(:new.NM_ENGINEERING_RESP_ORG,1,4000),:new.nm_usuario,nr_seq_w,'NM_ENGINEERING_RESP_ORG',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TYPE_DEVICE,1,4000),substr(:new.DS_TYPE_DEVICE,1,4000),:new.nm_usuario,nr_seq_w,'DS_TYPE_DEVICE',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPDM_TASY,1,4000),substr(:new.NR_SEQ_OPDM_TASY,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPDM_TASY',ie_log_w,ds_w,'OPDM_TRADE_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_TRADE_ITEM ADD (
  CONSTRAINT OPDMTRAIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OPDM_TRADE_ITEM ADD (
  CONSTRAINT OPDMTRAIT_OPDMTASY_FK 
 FOREIGN KEY (NR_SEQ_OPDM_TASY) 
 REFERENCES TASY.OPDM_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_TRADE_ITEM TO NIVEL_1;

