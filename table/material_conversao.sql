ALTER TABLE TASY.MATERIAL_CONVERSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_CONVERSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_CONVERSAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  NR_GCR               NUMBER(10),
  NR_GFC               NUMBER(10),
  GENERIC_NAME         VARCHAR2(255 BYTE),
  GCR_NAME             VARCHAR2(255 BYTE),
  ROUTE_NAME           VARCHAR2(60 BYTE),
  DS_FORM              VARCHAR2(40 BYTE),
  STRENGTH             VARCHAR2(40 BYTE),
  GFC_CODE             VARCHAR2(40 BYTE),
  GCR_CODE             VARCHAR2(40 BYTE),
  DS_INFO              VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PSD_SIGLE         VARCHAR2(1 BYTE),
  IE_PSD_COMBO         VARCHAR2(1 BYTE),
  CD_UNID_MED_SINGLE   VARCHAR2(30 BYTE)        DEFAULT null,
  CD_UNID_MED_COMBO    VARCHAR2(30 BYTE)        DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATCONVS_MATERIAL_FK_I ON TASY.MATERIAL_CONVERSAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATCONVS_PK ON TASY.MATERIAL_CONVERSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_CONVERSAO_tp  after update ON TASY.MATERIAL_CONVERSAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.STRENGTH,1,4000),substr(:new.STRENGTH,1,4000),:new.nm_usuario,nr_seq_w,'STRENGTH',ie_log_w,ds_w,'MATERIAL_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.GFC_CODE,1,4000),substr(:new.GFC_CODE,1,4000),:new.nm_usuario,nr_seq_w,'GFC_CODE',ie_log_w,ds_w,'MATERIAL_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.GCR_CODE,1,4000),substr(:new.GCR_CODE,1,4000),:new.nm_usuario,nr_seq_w,'GCR_CODE',ie_log_w,ds_w,'MATERIAL_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FORM,1,4000),substr(:new.DS_FORM,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORM',ie_log_w,ds_w,'MATERIAL_CONVERSAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MATERIAL_CONVERSAO ADD (
  CONSTRAINT MATCONVS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_CONVERSAO ADD (
  CONSTRAINT MATCONVS_MATERIAL_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.MATERIAL_CONVERSAO TO NIVEL_1;

