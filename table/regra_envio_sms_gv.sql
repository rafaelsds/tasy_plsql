ALTER TABLE TASY.REGRA_ENVIO_SMS_GV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_ENVIO_SMS_GV CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_ENVIO_SMS_GV
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_STATUS             VARCHAR2(1 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_SOLICITACAO        VARCHAR2(15 BYTE),
  IE_TIPO_VAGA          VARCHAR2(15 BYTE),
  CD_TIPO_ACOMOD_DESEJ  NUMBER(5),
  CD_SETOR_DESEJADO     NUMBER(5),
  CD_SETOR_ATUAL        NUMBER(5),
  NR_SEQ_REGRA          NUMBER(10)              NOT NULL,
  NR_SEQ_STATUS_PAC     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RESMSGV_GVSTPAC_FK_I ON TASY.REGRA_ENVIO_SMS_GV
(NR_SEQ_STATUS_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_GVSTPAC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RESMSGV_PK ON TASY.REGRA_ENVIO_SMS_GV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_PK
  MONITORING USAGE;


CREATE INDEX TASY.RESMSGV_REGENSM_FK_I ON TASY.REGRA_ENVIO_SMS_GV
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_REGENSM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESMSGV_SETATEN_FK_I ON TASY.REGRA_ENVIO_SMS_GV
(CD_SETOR_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESMSGV_SETATEN_FK2_I ON TASY.REGRA_ENVIO_SMS_GV
(CD_SETOR_DESEJADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.RESMSGV_TIPACOM_FK_I ON TASY.REGRA_ENVIO_SMS_GV
(CD_TIPO_ACOMOD_DESEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESMSGV_TIPACOM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_ENVIO_SMS_GV ADD (
  CONSTRAINT RESMSGV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_ENVIO_SMS_GV ADD (
  CONSTRAINT RESMSGV_GVSTPAC_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PAC) 
 REFERENCES TASY.GESTAO_VAGA_STATUS_PAC (NR_SEQUENCIA),
  CONSTRAINT RESMSGV_REGENSM_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_ENVIO_SMS (NR_SEQUENCIA),
  CONSTRAINT RESMSGV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATUAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT RESMSGV_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_DESEJADO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT RESMSGV_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMOD_DESEJ) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO));

GRANT SELECT ON TASY.REGRA_ENVIO_SMS_GV TO NIVEL_1;

