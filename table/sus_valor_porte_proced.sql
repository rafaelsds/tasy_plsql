ALTER TABLE TASY.SUS_VALOR_PORTE_PROCED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_VALOR_PORTE_PROCED CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_VALOR_PORTE_PROCED
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA_PORTE    NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROCEDIMENTO   NUMBER(10),
  CD_PROCEDIMENTO       NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10)              NOT NULL,
  DT_PROCEDIMENTO       DATE,
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL,
  CD_ANESTESISTA        VARCHAR2(10 BYTE),
  VL_PORTE_ANESTESICO   NUMBER(15,2),
  PR_ALTER_PORTE        NUMBER(7,2),
  VL_TOTAL_ANESTESISTA  NUMBER(15,2)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSVPPR_ATEPACI_FK_I ON TASY.SUS_VALOR_PORTE_PROCED
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSVPPR_PESFISI_FK_I ON TASY.SUS_VALOR_PORTE_PROCED
(CD_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSVPPR_PK ON TASY.SUS_VALOR_PORTE_PROCED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSVPPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSVPPR_PROCEDI_FK_I ON TASY.SUS_VALOR_PORTE_PROCED
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSVPPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSVPPR_SUSRPPR_FK_I ON TASY.SUS_VALOR_PORTE_PROCED
(NR_SEQ_REGRA_PORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSVPPR_SUSRPPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_VALOR_PORTE_PROCED ADD (
  CONSTRAINT SUSVPPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_VALOR_PORTE_PROCED ADD (
  CONSTRAINT SUSVPPR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SUSVPPR_PESFISI_FK 
 FOREIGN KEY (CD_ANESTESISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSVPPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSVPPR_SUSRPPR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PORTE) 
 REFERENCES TASY.SUS_REGRA_PORTE_PROCED (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUS_VALOR_PORTE_PROCED TO NIVEL_1;

