ALTER TABLE TASY.ATEND_CONTROLE_SINAL_VITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_CONTROLE_SINAL_VITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_CONTROLE_SINAL_VITAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_REGRA             VARCHAR2(15 BYTE)        NOT NULL,
  QT_HORAS_ALERTA      NUMBER(12,2)             NOT NULL,
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  IE_CONSISTE_LIMITE   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATCONSV_ATEPACI_FK_I ON TASY.ATEND_CONTROLE_SINAL_VITAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATCONSV_PK ON TASY.ATEND_CONTROLE_SINAL_VITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATCONSV_PK
  MONITORING USAGE;


ALTER TABLE TASY.ATEND_CONTROLE_SINAL_VITAL ADD (
  CONSTRAINT ATCONSV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_CONTROLE_SINAL_VITAL ADD (
  CONSTRAINT ATCONSV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ATEND_CONTROLE_SINAL_VITAL TO NIVEL_1;

