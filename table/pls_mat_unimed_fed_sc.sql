ALTER TABLE TASY.PLS_MAT_UNIMED_FED_SC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MAT_UNIMED_FED_SC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MAT_UNIMED_FED_SC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_MATERIAL              NUMBER(10)           NOT NULL,
  IE_DIGITO                NUMBER(2)            NOT NULL,
  DS_MATERIAL              VARCHAR2(255 BYTE),
  DS_NOME_COMERCIAL        VARCHAR2(255 BYTE),
  DS_FABRICANTE            VARCHAR2(255 BYTE),
  IE_UNID_COMPRA           VARCHAR2(3 BYTE),
  QT_EMBALAGEM             NUMBER(10,4),
  IE_UNID_EMBALAGEM        VARCHAR2(3 BYTE),
  QT_UTILIZACAO            NUMBER(10,4),
  IE_UNIDADE_UTILIZACAO    VARCHAR2(3 BYTE),
  QT_FRACAO                NUMBER(10,4),
  IE_UNID_FRACAO           VARCHAR2(3 BYTE),
  IE_FRACIONAR             VARCHAR2(1 BYTE),
  DS_GRUPO_ESTOQUE         VARCHAR2(255 BYTE),
  IE_REUTILIZACAO_GEN      VARCHAR2(1 BYTE),
  QT_REUTILIZACAO          NUMBER(10,4),
  CD_BARRAS                VARCHAR2(30 BYTE),
  CD_MINISTERIO_SAUDE      VARCHAR2(30 BYTE),
  DT_VALIDADE              DATE,
  IE_MOEDA                 VARCHAR2(4 BYTE),
  IE_LISTA                 VARCHAR2(1 BYTE),
  VL_PRECO_UNIMED          NUMBER(15,4),
  CD_SIMPRO                NUMBER(15),
  VL_PRECO_SIMPRO          NUMBER(15,4),
  IE_UNID_PRECO            VARCHAR2(3 BYTE),
  DT_PRECO_SIMPRO          DATE,
  CD_BRASINDICE            VARCHAR2(30 BYTE),
  CD_TISS                  VARCHAR2(30 BYTE),
  VL_PRECO_BRASINDICE      NUMBER(15,4),
  DT_PRECO_BRASINDICE      DATE,
  TX_PROCEDIMENTO          NUMBER(10,4),
  IE_COTACAO               VARCHAR2(1 BYTE),
  IE_ORIGEM                VARCHAR2(2 BYTE),
  IE_TAB_UD                VARCHAR2(1 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DS_MOTIVO_INATIV         VARCHAR2(2000 BYTE),
  DT_SITUACAO              DATE,
  DT_INCLUSAO              DATE,
  DT_ALTERACAO             DATE,
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  CD_GRUPO_ESTOQUE         VARCHAR2(10 BYTE),
  DT_VINCULO_MAT           DATE,
  IE_TIPO                  VARCHAR2(3 BYTE),
  IE_OPME                  VARCHAR2(1 BYTE),
  IE_INCONSISTENTE         VARCHAR2(1 BYTE),
  CD_ANVISA                VARCHAR2(20 BYTE),
  DT_VALIDADE_ANVISA       DATE,
  DT_LIMITE_UTILIZACAO     DATE,
  DT_EXCLUSAO              DATE,
  IE_USO_RESTRITO          VARCHAR2(1 BYTE),
  DS_NOME_COMERCIAL_COMPL  VARCHAR2(200 BYTE),
  CD_MATERIAL_TUSS         NUMBER(10),
  DS_VERSAO_TISS           VARCHAR2(20 BYTE),
  DT_ATUALIZACAO_PRODUTO   DATE,
  IE_STATUS                VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMUFS_ESTABEL_FK_I ON TASY.PLS_MAT_UNIMED_FED_SC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFS_I1 ON TASY.PLS_MAT_UNIMED_FED_SC
(IE_INCONSISTENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFS_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFS_I2 ON TASY.PLS_MAT_UNIMED_FED_SC
(CD_MATERIAL_TUSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFS_I2
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFS_I3 ON TASY.PLS_MAT_UNIMED_FED_SC
(CD_MATERIAL, DS_VERSAO_TISS, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMUFS_PK ON TASY.PLS_MAT_UNIMED_FED_SC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_mat_unimed_fed_sc_update
before update ON TASY.PLS_MAT_UNIMED_FED_SC for each row
begin
if	(:new.cd_material <> :old.cd_material) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo do material, de ' || :old.cd_material || ', para ' || :new.cd_material || '. ',
		'CD_MATERIAL',
		:old.cd_material,
		:new.cd_material);
end if;

if	(:new.ds_material <> :old.ds_material) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada descri��o do material, de ' || :old.ds_material || ', para ' || :new.ds_material || '. ',
		'DS_MATERIAL',
		:old.ds_material,
		:new.ds_material);
end if;

if	(:new.ds_nome_comercial <> :old.ds_nome_comercial) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o nome comercial, de ' || :old.ds_nome_comercial || ', para ' || :new.ds_nome_comercial || '. ',
		'DS_NOME_COMERCIAL',
		:old.ds_nome_comercial,
		:new.ds_nome_comercial);
end if;

if	(:new.ds_fabricante <> :old.ds_fabricante) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o fabricante, de ' || :old.ds_fabricante || ', para ' || :new.ds_fabricante || '. ',
		'DS_FABRICANTE',
		:old.ds_fabricante,
		:new.ds_fabricante);
end if;

if	(:new.ie_unidade_utilizacao <> :old.ie_unidade_utilizacao) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a unidade de medida, de ' || :old.ie_unidade_utilizacao || ', para ' || :new.ie_unidade_utilizacao || '. ',
		'IE_UNIDADE_UTILIZACAO',
		:old.ie_unidade_utilizacao,
		:new.ie_unidade_utilizacao);
end if;

if	(:new.cd_grupo_estoque <> :old.cd_grupo_estoque) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o grupo de estoque, de ' || :old.cd_grupo_estoque || ', para ' || :new.cd_grupo_estoque || '. ',
		'CD_GRUPO_ESTOQUE',
		:old.cd_grupo_estoque,
		:new.cd_grupo_estoque);
end if;

if	(:new.cd_brasindice <> :old.cd_brasindice) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o bras�ndice, de ' || :old.cd_brasindice || ', para ' || :new.cd_brasindice || '. ',
		'CD_BRASINDICE',
		:old.cd_brasindice,
		:new.cd_brasindice);
end if;

if	(:new.cd_simpro <> :old.cd_simpro) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo simpro, de ' || :old.cd_simpro || ', para ' || :new.cd_simpro || '. ',
		'CD_SIMPRO',
		:old.cd_simpro,
		:new.cd_simpro);
end if;

if	(:new.ie_moeda <> :old.ie_moeda) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a moeda, de ' || :old.ie_moeda || ', para ' || :new.ie_moeda || '. ',
		'IE_MOEDA',
		:old.ie_moeda,
		:new.ie_moeda);
end if;

if	(:new.ie_origem <> :old.ie_origem) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a origem, de ' || :old.ie_origem || ', para ' || :new.ie_origem || '. ',
		'IE_ORIGEM',
		:old.ie_origem,
		:new.ie_origem);
end if;

if	(:new.ds_observacao <> :old.ds_observacao) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a observa��o, de ' || :old.ds_observacao || ', para ' || :new.ds_observacao || '. ',
		'DS_OBSERVACAO',
		:old.ds_observacao,
		:new.ds_observacao);
end if;

if	(:new.ie_lista <> :old.ie_lista) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada a lista, de ' || :old.ie_lista || ', para ' || :new.ie_lista || '. ',
		'IE_LISTA',
		:old.ie_lista,
		:new.ie_lista);
end if;

if	(:new.cd_tiss <> :old.cd_tiss) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo tiss, de ' || :old.cd_tiss || ', para ' || :new.cd_tiss || '. ',
		'CD_TISS',
		:old.cd_tiss,
		:new.cd_tiss);
end if;

if	(:new.ie_tipo <> :old.ie_tipo) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o tipo, de ' || :old.ie_tipo || ', para ' || :new.ie_tipo || '. ',
		'IE_TIPO',
		:old.ie_tipo,
		:new.ie_tipo);
end if;

if	(:new.ds_grupo_estoque <> :old.ds_grupo_estoque) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada descri��o do grupo de estoque do material, de ' || :old.ds_grupo_estoque || ', para ' || :new.ds_grupo_estoque || '. ',
		'DS_GRUPO_ESTOQUE',
		:old.ds_grupo_estoque,
		:new.ds_grupo_estoque);
end if;

if	(:new.ds_fabricante <> :old.ds_fabricante) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterada descri��o do fabricante do material, de ' || :old.ds_fabricante || ', para ' || :new.ds_fabricante || '. ',
		'DS_FABRICANTE',
		:old.ds_fabricante,
		:new.ds_fabricante);
end if;

if	(:new.cd_anvisa <> :old.cd_anvisa) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado o c�digo ANVISA do material, de ' || :old.cd_anvisa || ', para ' || :new.cd_anvisa || '. ',
		'CD_ANVISA',
		:old.cd_anvisa,
		:new.cd_anvisa);
end if;

if	(:new.dt_validade_anvisa <> :old.dt_validade_anvisa) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a data de v�lide ANVISA do material, de ' || :old.dt_validade_anvisa || ', para ' || :new.dt_validade_anvisa || '. ',
		'DT_VALIDADE_ANVISA',
		:old.dt_validade_anvisa,
		:new.dt_validade_anvisa);
end if;

if	(:new.ie_unid_compra <> :old.ie_unid_compra) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a unidade de compra do material, de ' || :old.ie_unid_compra || ', para ' || :new.ie_unid_compra || '. ',
		'IE_UNID_COMPRA',
		:old.ie_unid_compra,
		:new.ie_unid_compra);
end if;

if	(:new.qt_embalagem <> :old.qt_embalagem) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a quantidade do material na embalagem, de ' || :old.qt_embalagem || ', para ' || :new.qt_embalagem || '. ',
		'QT_EMBALAGEM',
		:old.qt_embalagem,
		:new.qt_embalagem);
end if;

if	(:new.ie_unid_embalagem <> :old.ie_unid_embalagem) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a unidade de compra do material, de ' || :old.ie_unid_embalagem || ', para ' || :new.ie_unid_embalagem || '. ',
		'IE_UNID_EMBALAGEM',
		:old.ie_unid_embalagem,
		:new.ie_unid_embalagem);
end if;

if	(:new.qt_utilizacao <> :old.qt_utilizacao) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a quantidade de utiliza��o, de ' || :old.qt_utilizacao || ', para ' || :new.qt_utilizacao || '. ',
		'QT_UTILIZACAO',
		:old.qt_utilizacao,
		:new.qt_utilizacao);
end if;

if	(:new.qt_fracao <> :old.qt_fracao) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a quantidade do material a ser fracionada, de ' || :old.qt_fracao || ', para ' || :new.qt_fracao || '. ',
		'QT_FRACAO',
		:old.qt_fracao,
		:new.qt_fracao);
end if;

if	(:new.ie_unid_fracao <> :old.ie_unid_fracao) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a unidade de fracionamento, de ' || :old.ie_unid_fracao || ', para ' || :new.ie_unid_fracao || '. ',
		'IE_UNID_FRACAO',
		:old.ie_unid_fracao,
		:new.ie_unid_fracao);
end if;

if	(:new.ie_fracionar <> :old.ie_fracionar) then
	insert into	pls_mat_uni_fed_sc_log
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		dt_alteracao,
		nr_seq_material_fed,
		ds_log,
		nm_campo,
		vl_anterior,
		vl_atual)
	values	(pls_mat_uni_fed_sc_log_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		sysdate,
		:old.nr_sequencia,
		'Alterado a informa��o se o material deve ou n�o ser fracionado, de ' || :old.ie_fracionar || ', para ' || :new.ie_fracionar || '. ',
		'IE_FRACIONAR',
		:old.ie_fracionar,
		:new.ie_fracionar);
end if;

end pls_mat_unimed_fed_sc_update;
/


ALTER TABLE TASY.PLS_MAT_UNIMED_FED_SC ADD (
  CONSTRAINT PLSMUFS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MAT_UNIMED_FED_SC ADD (
  CONSTRAINT PLSMUFS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MAT_UNIMED_FED_SC TO NIVEL_1;

