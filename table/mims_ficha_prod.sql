DROP TABLE TASY.MIMS_FICHA_PROD CASCADE CONSTRAINTS;

CREATE TABLE TASY.MIMS_FICHA_PROD
(
  PRODUCTCODE                 NUMBER(15),
  NR_SEQ_FICHA_TECNICA        NUMBER(15),
  NR_SEQ_MIMS_VERSION         NUMBER(15),
  NR_MAIN_TABLES_REF_VERSION  NUMBER(15)        DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.MIMS_FICHA_PROD TO NIVEL_1;

