ALTER TABLE TASY.SUP_FORMULA_PLANEJ_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_FORMULA_PLANEJ_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_FORMULA_PLANEJ_COMPRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_FORMULA           VARCHAR2(255 BYTE)       NOT NULL,
  DS_MACRO             VARCHAR2(30 BYTE)        NOT NULL,
  DS_FORMULA           VARCHAR2(4000 BYTE),
  IE_NIVEL_FORMULA     NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUFOPCO_PK ON TASY.SUP_FORMULA_PLANEJ_COMPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUFOPCO_PK
  MONITORING USAGE;


ALTER TABLE TASY.SUP_FORMULA_PLANEJ_COMPRA ADD (
  CONSTRAINT SUFOPCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_FORMULA_PLANEJ_COMPRA TO NIVEL_1;

