ALTER TABLE TASY.ROP_INV_AJUSTE_ROUPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_INV_AJUSTE_ROUPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_INV_AJUSTE_ROUPA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INVENTARIO    NUMBER(10)               NOT NULL,
  NR_SEQ_INV_SETOR     NUMBER(10)               NOT NULL,
  NR_SEQ_ROUPA         NUMBER(10),
  QT_CONTAGEM          NUMBER(5)                NOT NULL,
  NR_SEQ_KIT           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ROINVAR_PK ON TASY.ROP_INV_AJUSTE_ROUPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROINVAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.ROINVAR_ROINVAS_FK_I ON TASY.ROP_INV_AJUSTE_ROUPA
(NR_SEQ_INV_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROINVAR_ROINVAS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROINVAR_ROPINAJ_FK_I ON TASY.ROP_INV_AJUSTE_ROUPA
(NR_SEQ_INVENTARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROINVAR_ROPINAJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROINVAR_ROPKIRO_FK_I ON TASY.ROP_INV_AJUSTE_ROUPA
(NR_SEQ_KIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROINVAR_ROPKIRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROINVAR_ROROUPA_FK_I ON TASY.ROP_INV_AJUSTE_ROUPA
(NR_SEQ_ROUPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROINVAR_ROROUPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.rop_inv_ajuste_roupa_atual
BEFORE INSERT OR UPDATE ON TASY.ROP_INV_AJUSTE_ROUPA FOR EACH ROW
DECLARE

BEGIN

if	(:new.nr_seq_kit is null) and
	(:new.nr_seq_roupa is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265539);
	--'Deve ser informado uma Roupa ou um Kit.'
end if;

if	(:new.nr_seq_kit is not null) and
	(:new.nr_seq_roupa is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265540);
	--'Deve ser informado somente uma Roupa ou somente um Kit.'
end if;

END;
/


ALTER TABLE TASY.ROP_INV_AJUSTE_ROUPA ADD (
  CONSTRAINT ROINVAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_INV_AJUSTE_ROUPA ADD (
  CONSTRAINT ROINVAR_ROINVAS_FK 
 FOREIGN KEY (NR_SEQ_INV_SETOR) 
 REFERENCES TASY.ROP_INV_AJUSTE_SETOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ROINVAR_ROROUPA_FK 
 FOREIGN KEY (NR_SEQ_ROUPA) 
 REFERENCES TASY.ROP_ROUPA (NR_SEQUENCIA),
  CONSTRAINT ROINVAR_ROPKIRO_FK 
 FOREIGN KEY (NR_SEQ_KIT) 
 REFERENCES TASY.ROP_KIT_ROUPARIA (NR_SEQUENCIA),
  CONSTRAINT ROINVAR_ROPINAJ_FK 
 FOREIGN KEY (NR_SEQ_INVENTARIO) 
 REFERENCES TASY.ROP_INVENTARIO_AJUSTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ROP_INV_AJUSTE_ROUPA TO NIVEL_1;

