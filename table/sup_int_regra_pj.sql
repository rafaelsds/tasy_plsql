ALTER TABLE TASY.SUP_INT_REGRA_PJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_REGRA_PJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_REGRA_PJ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTEGRACAO    NUMBER(10)               NOT NULL,
  IE_NOVO              VARCHAR2(1 BYTE)         NOT NULL,
  IE_SALVAR            VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINRPJ_PK ON TASY.SUP_INT_REGRA_PJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINRPJ_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUINRPJ_SUPAINT_FK_I ON TASY.SUP_INT_REGRA_PJ
(NR_SEQ_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_INT_REGRA_PJ_tp  after update ON TASY.SUP_INT_REGRA_PJ FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_INT_REGRA_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALVAR,1,4000),substr(:new.IE_SALVAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALVAR',ie_log_w,ds_w,'SUP_INT_REGRA_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOVO,1,4000),substr(:new.IE_NOVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOVO',ie_log_w,ds_w,'SUP_INT_REGRA_PJ',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_INT_REGRA_PJ ADD (
  CONSTRAINT SUINRPJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_INT_REGRA_PJ ADD (
  CONSTRAINT SUINRPJ_SUPAINT_FK 
 FOREIGN KEY (NR_SEQ_INTEGRACAO) 
 REFERENCES TASY.SUP_PARAMETRO_INTEGRACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_INT_REGRA_PJ TO NIVEL_1;

