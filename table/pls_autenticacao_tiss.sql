ALTER TABLE TASY.PLS_AUTENTICACAO_TISS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AUTENTICACAO_TISS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AUTENTICACAO_TISS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_USUARIO_WS         VARCHAR2(20 BYTE),
  DS_SENHA_WS           VARCHAR2(32 BYTE),
  NR_SEQ_PROT_CONTA     NUMBER(10),
  NR_SEQ_PROTOCOLO_IMP  NUMBER(10),
  DS_TIPO_TRANSACAO     VARCHAR2(50 BYTE),
  NR_SEQ_TRANSACAO      VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAUTI_PK ON TASY.PLS_AUTENTICACAO_TISS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAUTI_PLSPRCO_FK_I ON TASY.PLS_AUTENTICACAO_TISS
(NR_SEQ_PROT_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAUTI_PPCTIMP_FK_I ON TASY.PLS_AUTENTICACAO_TISS
(NR_SEQ_PROTOCOLO_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_AUTENTICACAO_TISS ADD (
  CONSTRAINT PLSAUTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AUTENTICACAO_TISS ADD (
  CONSTRAINT PLSAUTI_PPCTIMP_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO_IMP) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSAUTI_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROT_CONTA) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_AUTENTICACAO_TISS TO NIVEL_1;

