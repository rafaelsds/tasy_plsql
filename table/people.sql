ALTER TABLE TASY.PEOPLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEOPLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEOPLE
(
  PERSON_ID                 NUMBER(10)          NOT NULL,
  PERSON_NAME               VARCHAR2(100 BYTE)  NOT NULL,
  BIRTH_DATE                DATE,
  PHONE                     VARCHAR2(40 BYTE),
  BLOOD_TYPE                VARCHAR2(3 BYTE),
  IS_EMPLOYEE               VARCHAR2(1 BYTE),
  DT_ATUALIZACAO            DATE,
  NM_USUARIO                VARCHAR2(15 BYTE),
  DS_INTERESTS              VARCHAR2(2000 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  QT_PESO                   NUMBER(5),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA         NUMBER(15),
  NR_SEQ_ASSINATURA_INATIV  NUMBER(15),
  COUNTRY_CODE              VARCHAR2(5 BYTE),
  DDD                       VARCHAR2(5 BYTE),
  IE_SEXO                   VARCHAR2(1 BYTE),
  NM_TIMEZONE               VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEOPLE_ESTABEL_FK_I ON TASY.PEOPLE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEOPLE_PK ON TASY.PEOPLE
(PERSON_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEOPLE_TASASDI_FK_I ON TASY.PEOPLE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEOPLE_TASASDI_FK2_I ON TASY.PEOPLE
(NR_SEQ_ASSINATURA_INATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PEOPLE ADD (
  CONSTRAINT PEOPLE_PK
 PRIMARY KEY
 (PERSON_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEOPLE ADD (
  CONSTRAINT PEOPLE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEOPLE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINATURA_INATIV) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEOPLE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PEOPLE TO NIVEL_1;

