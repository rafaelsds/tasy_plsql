ALTER TABLE TASY.SEVERITY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SEVERITY CASCADE CONSTRAINTS;

CREATE TABLE TASY.SEVERITY
(
  SEVERITYID       NUMBER(15),
  SEVERITYDESCEN   VARCHAR2(128 BYTE),
  SEVERITYRANKING  NUMBER(15),
  SEVERITYDEF      VARCHAR2(1024 BYTE),
  VERSION          NUMBER(10)                   DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SEVERITY_PK ON TASY.SEVERITY
(SEVERITYID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SEVERITY ADD (
  CONSTRAINT SEVERITY_PK
 PRIMARY KEY
 (SEVERITYID));

GRANT SELECT ON TASY.SEVERITY TO NIVEL_1;

