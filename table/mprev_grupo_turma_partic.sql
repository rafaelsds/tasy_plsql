ALTER TABLE TASY.MPREV_GRUPO_TURMA_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_GRUPO_TURMA_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_GRUPO_TURMA_PARTIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TURMA           NUMBER(10)             NOT NULL,
  NR_SEQ_PARTICIPANTE    NUMBER(10)             NOT NULL,
  DT_ENTRADA             DATE                   NOT NULL,
  DT_SAIDA               DATE,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DS_MOTIVO_SAIDA        VARCHAR2(2000 BYTE),
  NR_SEQ_MOTIVO_SAIDA    NUMBER(10),
  NR_SEQ_PART_CANC_ATIV  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRGTPA_MPRGCTU_FK_I ON TASY.MPREV_GRUPO_TURMA_PARTIC
(NR_SEQ_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTPA_MPRGCTU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRGTPA_MPRMSGR_FK_I ON TASY.MPREV_GRUPO_TURMA_PARTIC
(NR_SEQ_MOTIVO_SAIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRGTPA_MPRPART_FK_I ON TASY.MPREV_GRUPO_TURMA_PARTIC
(NR_SEQ_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTPA_MPRPART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRGTPA_MPRPCAT_FK_I ON TASY.MPREV_GRUPO_TURMA_PARTIC
(NR_SEQ_PART_CANC_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRGTPA_PK ON TASY.MPREV_GRUPO_TURMA_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGTPA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MPREV_GRUPO_TURMA_PARTIC_INS
AFTER INSERT ON TASY.MPREV_GRUPO_TURMA_PARTIC FOR EACH ROW
DECLARE

nr_sequencia_w		comunic_interna.nr_sequencia%type;
ds_comunicado_w		comunic_interna.ds_comunicado%type;
nm_turma_w		varchar(255);
ds_titulo_w		varchar2(400);
ie_ci_incluir_w		varchar2(1);
ie_email_incluir_w		varchar2(1);
ie_ci_excluir_w		varchar2(1);
ie_email_excluir_w		varchar2(1);
usuario_destino_w		varchar2(255);
ds_email_destino_w		varchar2(255);
ds_email_origem_w		varchar2(255) := null;
ie_email_valido_w		varchar2(255) := 'N';

begin

hdm_obter_dados_turma(:new.nr_sequencia, :new.nr_seq_turma, ie_ci_incluir_w, ie_email_incluir_w, ie_ci_excluir_w, ie_email_excluir_w, usuario_destino_w, ds_email_destino_w);

select 	max(a.nm_turma)
into 	nm_turma_w
from   	mprev_grupo_col_turma a
where  	a.nr_sequencia = :new.nr_seq_turma;

ds_comunicado_w	:= 	wheb_mensagem_pck.get_texto(416810, 'NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:new.nr_seq_participante),1,255)||';NM_TURMA='||nm_turma_w);
ds_titulo_w 	:= 	wheb_mensagem_pck.get_texto(416824, 'NM_TURMA='||nm_turma_w);

if (ie_ci_incluir_w = 'S') then

	if (usuario_destino_w is not null) then
	begin

		insert	into comunic_interna
				(dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao)
	    	values		(sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				wheb_usuario_pck.get_nm_usuario,
				sysdate,
				'N',
				usuario_destino_w,
				comunic_interna_seq.nextval,
				'N',
				sysdate);
	    exception
	    when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
					wheb_usuario_pck.get_nm_usuario,
					 33,
					 wheb_mensagem_pck.get_texto(419754, 'NM_TURMA='||nm_turma_w||';NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)));
	end;
	end if;
end if;

if (ie_email_incluir_w = 'S') then

	if (ds_email_destino_w is not null) then

		select 	substr(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'E'),1,255)
		into 	ds_email_origem_w
		from 	dual;

		if (ds_email_origem_w is null) then

			select 	substr(obter_compl_pf(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'C'), 1, 'M'),1,255)
			into 	ds_email_origem_w
			from 	dual;

		end if;

		select 	obter_se_email_valido(ds_email_destino_w)
		into 	ie_email_valido_w
		from	dual;

		if (ie_email_valido_w = 'S') then
		begin
			enviar_email(ds_titulo_w, ds_comunicado_w, ds_email_origem_w, ds_email_destino_w, wheb_usuario_pck.get_nm_usuario, 'M');
		exception
	   	when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log)  values (sysdate,
			         	 	wheb_usuario_pck.get_nm_usuario,
			         		33,
			         		wheb_mensagem_pck.get_texto(419745, 'NM_TURMA='||nm_turma_w||';DS_TITULO='||ds_titulo_w||';DS_COMUNICADO='||ds_comunicado_w||';DS_EMAIL_ORIGEM='||ds_email_origem_w||';DS_EMAIL_DESTINO='||ds_email_destino_w));
		end;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MPREV_GRUPO_TURMA_PARTIC_DEL
after delete ON TASY.MPREV_GRUPO_TURMA_PARTIC FOR EACH ROW
DECLARE

nr_sequencia_w		comunic_interna.nr_sequencia%type;
ds_comunicado_w		comunic_interna.ds_comunicado%type;
nm_turma_w		varchar(255);
ds_titulo_w		varchar2(400);
ie_ci_incluir_w		varchar2(1);
ie_email_incluir_w		varchar2(1);
ie_ci_excluir_w		varchar2(1);
ie_email_excluir_w		varchar2(1);
usuario_destino_w		varchar2(255);
ds_email_destino_w		varchar2(255);
ds_email_origem_w		varchar2(255) 	:= null;
ie_email_valido_w		varchar2(255) 	:= 'N';

begin

hdm_obter_dados_turma( :old.nr_sequencia, :old.nr_seq_turma, ie_ci_incluir_w, ie_email_incluir_w, ie_ci_excluir_w, ie_email_excluir_w, usuario_destino_w, ds_email_destino_w);

select 	max(a.nm_turma)
into 	nm_turma_w
from   	mprev_grupo_col_turma a
where  	a.nr_sequencia = :old.nr_seq_turma;

ds_comunicado_w		:=	wheb_mensagem_pck.get_texto(417642, 'NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)||';NM_TURMA='||nm_turma_w);
ds_titulo_w		:= 	wheb_mensagem_pck.get_texto(417640, 'NM_TURMA='||nm_turma_w);

if ( ie_ci_excluir_w = 'S') then

	if ( usuario_destino_w is not null) then
	begin

		insert	into comunic_interna
				(dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao)
	  	  values		(sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				wheb_usuario_pck.get_nm_usuario,
				sysdate,
				'N',
				usuario_destino_w,
				comunic_interna_seq.nextval,
				'N',
				sysdate);
	    exception
	    when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
					 wheb_usuario_pck.get_nm_usuario,
					 33,
					 wheb_mensagem_pck.get_texto(419754, 'NM_TURMA='||nm_turma_w||';NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)));
	    end;
	end if;
end if;

if (ie_email_excluir_w = 'S') then

	if (ds_email_destino_w is not null) then

		select 	substr(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'E'),1,255)
		into 	ds_email_origem_w
		from 	dual;

		if (ds_email_origem_w is null) then

			select 	substr(obter_compl_pf(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'C'), 1, 'M'),1,255)
			into 	ds_email_origem_w
			from 	dual;

		end if;

		select 	obter_se_email_valido(ds_email_destino_w)
		into 	ie_email_valido_w
		from	dual;

		if	(ie_email_valido_w = 'S') then
		begin
			enviar_email(ds_titulo_w, ds_comunicado_w, ds_email_origem_w, ds_email_destino_w, wheb_usuario_pck.get_nm_usuario, 'M');
		exception
	    	when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
			         wheb_usuario_pck.get_nm_usuario,
			         33,
			         wheb_mensagem_pck.get_texto(419745, 'NM_TURMA='||nm_turma_w||';DS_TITULO='||ds_titulo_w||';DS_COMUNICADO='||ds_comunicado_w||';DS_EMAIL_ORIGEM='||ds_email_origem_w||';DS_EMAIL_DESTINO='||ds_email_destino_w));
		end;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MPREV_GRUPO_TURMA_PARTIC_ATUAL
before insert or update ON TASY.MPREV_GRUPO_TURMA_PARTIC FOR EACH ROW
DECLARE

BEGIN

if	(trunc(:new.dt_saida) < trunc(:new.dt_entrada) and
	(trunc(:new.dt_entrada) <= trunc(sysdate))) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(834833);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.MPREV_GRUPO_TURMA_PARTIC_UPD
AFTER UPDATE ON TASY.MPREV_GRUPO_TURMA_PARTIC FOR EACH ROW
DECLARE

nr_sequencia_w		comunic_interna.nr_sequencia%type;
ds_comunicado_w		comunic_interna.ds_comunicado%type;
nm_turma_w		varchar(255);
ds_titulo_w		varchar2(400);
ie_ci_incluir_w		varchar2(1);
ie_email_incluir_w		varchar2(1);
ie_ci_excluir_w		varchar2(1);
ie_email_excluir_w		varchar2(1);
usuario_destino_w		varchar2(255);
ds_email_destino_w		varchar2(255) 	:= null;
ds_email_origem_w		varchar2(255) 	:= null;
ie_email_valido_w		varchar2(255) 	:= 'N';

/* Permite consulta o nome do participante na tabela pessoa_fisica mesmo quando a atualização da tabela mprev_grupo_turma_partic se faz pela trigger pessoa_fisica_atual ao inserir a dt_obito na pessoa_fisica.
Impede que a tabela pessoa_fisica se torne mutante ao buscar o nome da pessoa fisica do participante*/
pragma autonomous_transaction;

begin

hdm_obter_dados_turma(:new.nr_sequencia, :new.nr_seq_turma, ie_ci_incluir_w, ie_email_incluir_w, ie_ci_excluir_w, ie_email_excluir_w, usuario_destino_w, ds_email_destino_w);

select 	max(a.nm_turma)
into 	nm_turma_w
from 	mprev_grupo_col_turma a
where  	a.nr_sequencia = :new.nr_seq_turma;

ds_comunicado_w		:=	wheb_mensagem_pck.get_texto(417642, 'NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)||';NM_TURMA='||nm_turma_w);
ds_titulo_w		:= 	wheb_mensagem_pck.get_texto(417640, 'NM_TURMA='||nm_turma_w);

if ((ie_ci_excluir_w = 'S') and (:new.dt_saida is not null) and (:old.dt_saida is null)) then

	if (usuario_destino_w is not null) then
	begin

	insert	into comunic_interna
			(dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			dt_liberacao)
	values		(sysdate,
			ds_titulo_w,
			ds_comunicado_w,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			'N',
			usuario_destino_w,
			comunic_interna_seq.nextval,
			'N',
			sysdate);
	exception
	when others then
	insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
			 wheb_usuario_pck.get_nm_usuario,
			 33,
			 wheb_mensagem_pck.get_texto(419754, 'NM_TURMA='||nm_turma_w||';NM_PARTICIPANTE='||SUBSTR(mprev_obter_nm_participante(:old.nr_seq_participante),1,255)));


	end;
	end if;
end if;

if ((ie_email_excluir_w = 'S') and (:new.dt_saida is not null) and (:old.dt_saida is null)) then

	if (ds_email_destino_w is not null) then

		select 	substr(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'E'),1,255)
		into 	ds_email_origem_w
		from 	dual;

		if (ds_email_origem_w is null) then

			select 	substr(obter_compl_pf(obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario, 'C'), 1, 'M'),1,255)
			into 	ds_email_origem_w
			from 	dual;

		end if;

		select 	obter_se_email_valido(ds_email_destino_w)
		into 	ie_email_valido_w
		from	dual;

		if	(ie_email_valido_w = 'S') then
		begin
			enviar_email(ds_titulo_w, ds_comunicado_w, ds_email_origem_w, ds_email_destino_w, wheb_usuario_pck.get_nm_usuario, 'M');
		exception
	    when others then
			insert into log_tasy (dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,
			         		wheb_usuario_pck.get_nm_usuario,
			         		33,
			         		wheb_mensagem_pck.get_texto(419745, 'NM_TURMA='||nm_turma_w||';DS_TITULO='||ds_titulo_w||';DS_COMUNICADO='||ds_comunicado_w||';DS_EMAIL_ORIGEM='||
								 													ds_email_origem_w||';DS_EMAIL_DESTINO='||ds_email_destino_w));
		end;
		end if;
	end if;
end if;

commit;

end;
/


ALTER TABLE TASY.MPREV_GRUPO_TURMA_PARTIC ADD (
  CONSTRAINT MPRGTPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_GRUPO_TURMA_PARTIC ADD (
  CONSTRAINT MPRGTPA_MPRGCTU_FK 
 FOREIGN KEY (NR_SEQ_TURMA) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA (NR_SEQUENCIA),
  CONSTRAINT MPRGTPA_MPRPART_FK 
 FOREIGN KEY (NR_SEQ_PARTICIPANTE) 
 REFERENCES TASY.MPREV_PARTICIPANTE (NR_SEQUENCIA),
  CONSTRAINT MPRGTPA_MPRMSGR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SAIDA) 
 REFERENCES TASY.MPREV_MOTIVO_SAIDA_GRUPO (NR_SEQUENCIA),
  CONSTRAINT MPRGTPA_MPRPCAT_FK 
 FOREIGN KEY (NR_SEQ_PART_CANC_ATIV) 
 REFERENCES TASY.MPREV_PARTIC_CANC_ATIV (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_GRUPO_TURMA_PARTIC TO NIVEL_1;

