ALTER TABLE TASY.FIS_EFD_ICMSIPI_REGRA_B510
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_ICMSIPI_REGRA_B510 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_ICMSIPI_REGRA_B510
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  IE_IND_PROF          VARCHAR2(1 BYTE),
  IE_IND_ESC           VARCHAR2(1 BYTE),
  IE_IND_SOC           VARCHAR2(1 BYTE),
  NR_SEQ_LOTE          NUMBER(10),
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ICMSRB510_EFDICMSCON_FK_I ON TASY.FIS_EFD_ICMSIPI_REGRA_B510
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ICMSRB510_EFDICMSLOT_FK_I ON TASY.FIS_EFD_ICMSIPI_REGRA_B510
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ICMSRB510_PESFISI_FK_I ON TASY.FIS_EFD_ICMSIPI_REGRA_B510
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ICMSRB510_PK ON TASY.FIS_EFD_ICMSIPI_REGRA_B510
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_EFD_ICMSIPI_REGRA_B510 ADD (
  CONSTRAINT ICMSRB510_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_EFD_ICMSIPI_REGRA_B510 ADD (
  CONSTRAINT ICMSRB510_EFDICMSLOT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ICMSRB510_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ICMSRB510_EFDICMSCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_EFD_ICMSIPI_REGRA_B510 TO NIVEL_1;

