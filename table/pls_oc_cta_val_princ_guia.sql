ALTER TABLE TASY.PLS_OC_CTA_VAL_PRINC_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_PRINC_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_PRINC_GUIA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB        NUMBER(10)          NOT NULL,
  IE_VALIDA_CTA_PRINC_GUIA  VARCHAR2(2 BYTE)    NOT NULL,
  NR_SEQ_REGRA_TP_GUIA      NUMBER(10),
  IE_VALIDA_REGRAS          VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCPG_PK ON TASY.PLS_OC_CTA_VAL_PRINC_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCPG_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_PRINC_GUIA
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCPG_PLSRTGA_FK_I ON TASY.PLS_OC_CTA_VAL_PRINC_GUIA
(NR_SEQ_REGRA_TP_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_PRINC_GUIA_tp  after update ON TASY.PLS_OC_CTA_VAL_PRINC_GUIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_TP_GUIA,1,4000),substr(:new.NR_SEQ_REGRA_TP_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_TP_GUIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PRINC_GUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_CTA_PRINC_GUIA,1,4000),substr(:new.IE_VALIDA_CTA_PRINC_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_CTA_PRINC_GUIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PRINC_GUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PRINC_GUIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_PRINC_GUIA ADD (
  CONSTRAINT PLSOCPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_PRINC_GUIA ADD (
  CONSTRAINT PLSOCPG_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSOCPG_PLSRTGA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TP_GUIA) 
 REFERENCES TASY.PLS_REGRA_TIPO_GUIA_APRES (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_PRINC_GUIA TO NIVEL_1;

