ALTER TABLE TASY.PLS_REGRA_ICMS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ICMS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ICMS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  VL_PERC_ICMS         NUMBER(7,2),
  DT_FIM_VIGENCIA      DATE,
  DT_FIM_VIGENCIA_REF  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSICMS_I1 ON TASY.PLS_REGRA_ICMS
(DT_INICIO_VIGENCIA, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSICMS_PK ON TASY.PLS_REGRA_ICMS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_icms_atual
before insert or update ON TASY.PLS_REGRA_ICMS for each row
declare

begin

/* esta trigger foi criada para alimentar o campo de data  fim de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia,'F');


end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_ICMS_tp  after update ON TASY.PLS_REGRA_ICMS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.VL_PERC_ICMS,1,4000),substr(:new.VL_PERC_ICMS,1,4000),:new.nm_usuario,nr_seq_w,'VL_PERC_ICMS',ie_log_w,ds_w,'PLS_REGRA_ICMS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_ICMS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_ICMS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_ICMS ADD (
  CONSTRAINT PLSICMS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_ICMS TO NIVEL_1;

