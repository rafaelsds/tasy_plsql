ALTER TABLE TASY.INT_EQUIPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INT_EQUIPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.INT_EQUIPE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_EQUIPE_FUNCAO  NUMBER(10)              NOT NULL,
  DS_OBETIVO            VARCHAR2(4000 BYTE),
  NR_SEQ_INTEGRACAO     NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTEQUIPE_INTDOCT_FK_I ON TASY.INT_EQUIPE
(NR_SEQ_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTEQUIPE_INTDOCT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INTEQUIPE_INTEQFUNC_FK_I ON TASY.INT_EQUIPE
(NR_SEQ_EQUIPE_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTEQUIPE_INTEQFUNC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INTEQUIPE_PK ON TASY.INT_EQUIPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTEQUIPE_PK
  MONITORING USAGE;


ALTER TABLE TASY.INT_EQUIPE ADD (
  CONSTRAINT INTEQUIPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INT_EQUIPE ADD (
  CONSTRAINT INTEQUIPE_INTDOCT_FK 
 FOREIGN KEY (NR_SEQ_INTEGRACAO) 
 REFERENCES TASY.INT_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT INTEQUIPE_INTEQFUNC_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_FUNCAO) 
 REFERENCES TASY.INT_EQUIPE_FUNCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.INT_EQUIPE TO NIVEL_1;

