ALTER TABLE TASY.PF_CURRICULO_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_CURRICULO_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_CURRICULO_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NR_SEQ_CURRICULO     NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCUANE_PFCURRI_FK_I ON TASY.PF_CURRICULO_ANEXO
(NR_SEQ_CURRICULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFCUANE_PFCURRI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PFCUANE_PK ON TASY.PF_CURRICULO_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFCUANE_PK
  MONITORING USAGE;


ALTER TABLE TASY.PF_CURRICULO_ANEXO ADD (
  CONSTRAINT PFCUANE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_CURRICULO_ANEXO ADD (
  CONSTRAINT PFCUANE_PFCURRI_FK 
 FOREIGN KEY (NR_SEQ_CURRICULO) 
 REFERENCES TASY.PF_CURRICULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PF_CURRICULO_ANEXO TO NIVEL_1;

