ALTER TABLE TASY.INDICADOR_EVOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INDICADOR_EVOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INDICADOR_EVOLUCAO
(
  CD_INDICADOR_EVOLUCAO  NUMBER(5)              NOT NULL,
  DS_INDICADOR_EVOLUCAO  VARCHAR2(40 BYTE)      NOT NULL,
  IE_GRAFICO             VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE,
  NM_USUARIO             VARCHAR2(15 BYTE),
  IE_TIPO_EVOLUCAO       VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  IE_VALOR_PADRAO        VARCHAR2(1 BYTE),
  IE_TIPO_FONTE          VARCHAR2(3 BYTE),
  NR_SEQUENCIA           NUMBER(5),
  IE_TIPO_VALOR          VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.INDEVOL_PK ON TASY.INDICADOR_EVOLUCAO
(CD_INDICADOR_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INDICADOR_EVOLUCAO ADD (
  CONSTRAINT INDEVOL_PK
 PRIMARY KEY
 (CD_INDICADOR_EVOLUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.INDICADOR_EVOLUCAO TO NIVEL_1;

