ALTER TABLE TASY.ESCALA_NFCS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NFCS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NFCS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_FENDA_PALPEBRAL     VARCHAR2(1 BYTE)       NOT NULL,
  IE_SULCO_APROFUNDADO   VARCHAR2(1 BYTE)       NOT NULL,
  IE_BOCA_ABERTA         VARCHAR2(1 BYTE)       NOT NULL,
  IE_BOCA_ESTIRADA       VARCHAR2(1 BYTE)       NOT NULL,
  IE_LINGUA_TENSA        VARCHAR2(1 BYTE)       NOT NULL,
  IE_PROTUSAO_LINGUA     VARCHAR2(1 BYTE)       NOT NULL,
  IE_TREMOR_QUEIXO       VARCHAR2(1 BYTE)       NOT NULL,
  QT_PONTUACAO           NUMBER(10),
  IE_FRONTE_SALIENTE     VARCHAR2(1 BYTE)       NOT NULL,
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCNFCS_ATEPACI_FK_I ON TASY.ESCALA_NFCS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNFCS_PESFISI_FK_I ON TASY.ESCALA_NFCS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCNFCS_PK ON TASY.ESCALA_NFCS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_nfcs_atual
before insert or update ON TASY.ESCALA_NFCS for each row
declare
qt_ponto_w	number(10)	:= 0;

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(:new.IE_FRONTE_SALIENTE	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

if	(:new.IE_FENDA_PALPEBRAL	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

if	(:new.IE_SULCO_APROFUNDADO	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

if	(:new.IE_BOCA_ABERTA	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

if	(:new.IE_BOCA_ESTIRADA	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

if	(:new.IE_LINGUA_TENSA	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;
if	(:new.IE_PROTUSAO_LINGUA	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;
if	(:new.IE_TREMOR_QUEIXO	= 'S') then
	qt_ponto_w	:= qt_ponto_w+1;
end if;

:new.qt_pontuacao	:= qt_ponto_w;


end;
/


ALTER TABLE TASY.ESCALA_NFCS ADD (
  CONSTRAINT ESCNFCS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NFCS ADD (
  CONSTRAINT ESCNFCS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCNFCS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_NFCS TO NIVEL_1;

