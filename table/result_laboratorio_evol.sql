ALTER TABLE TASY.RESULT_LABORATORIO_EVOL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_LABORATORIO_EVOL CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_LABORATORIO_EVOL
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO     DATE,
  DS_RESULTADO       LONG,
  NR_SEQ_RESULT_LAB  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RESLAEV_PK ON TASY.RESULT_LABORATORIO_EVOL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESLAEV_RESLABO_FK_I ON TASY.RESULT_LABORATORIO_EVOL
(NR_SEQ_RESULT_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RESULT_LABORATORIO_EVOL ADD (
  CONSTRAINT RESLAEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULT_LABORATORIO_EVOL ADD (
  CONSTRAINT RESLAEV_RESLABO_FK 
 FOREIGN KEY (NR_SEQ_RESULT_LAB) 
 REFERENCES TASY.RESULT_LABORATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RESULT_LABORATORIO_EVOL TO NIVEL_1;

