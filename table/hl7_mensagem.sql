ALTER TABLE TASY.HL7_MENSAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HL7_MENSAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.HL7_MENSAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_MENSAGEM          VARCHAR2(100 BYTE)       NOT NULL,
  NR_SEQ_PROJETO       NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_LONG              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HL7MENS_HL7PROJ_FK_I ON TASY.HL7_MENSAGEM
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HL7MENS_HL7TPME_FK_I ON TASY.HL7_MENSAGEM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HL7MENS_PK ON TASY.HL7_MENSAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hl7_mensagem_insert
AFTER INSERT ON TASY.HL7_MENSAGEM FOR EACH ROW
DECLARE

cd_versao_w		number(10);
nr_seq_proj_origem_w	number(10);
nr_sequencia_w		number(10);

Cursor C01 is
	select	m.nr_sequencia,
		m.nm_mensagem,
		m.nr_seq_projeto,
		m.nr_seq_tipo
	from	wheb_hl7_mensagem m,
		wheb_hl7_projeto p
	where 	m.nr_seq_projeto 	= p.nr_sequencia
	and	p.cd_versao 		= cd_versao_w
	and	m.nr_seq_tipo  		= :new.nr_seq_tipo
	order by nr_sequencia;

hl7_mensagem_w c01%rowtype;

begin

select	max(cd_versao)
into	cd_versao_w
from	hl7_projeto
where	nr_sequencia = :new.nr_seq_projeto;

if	(cd_versao_w is not null) then

	open c01;
	loop
	fetch c01 into
		hl7_mensagem_w;
	exit when c01%notfound;
		begin
		wheb_hl7_pck.copiar_hl7_segmento_original(hl7_mensagem_w.nr_sequencia,:new.nr_sequencia,null);
		end;
	end loop;
	close C01;
end if;

end;
/


ALTER TABLE TASY.HL7_MENSAGEM ADD (
  CONSTRAINT HL7MENS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HL7_MENSAGEM ADD (
  CONSTRAINT HL7MENS_HL7TPME_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.HL7_TIPO_MENSAGEM (NR_SEQUENCIA),
  CONSTRAINT HL7MENS_HL7PROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.HL7_PROJETO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HL7_MENSAGEM TO NIVEL_1;

