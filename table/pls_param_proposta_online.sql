ALTER TABLE TASY.PLS_PARAM_PROPOSTA_ONLINE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAM_PROPOSTA_ONLINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAM_PROPOSTA_ONLINE
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_URL_MANUAL_INSTRUCAO     VARCHAR2(255 BYTE),
  DS_DIA_VENCIMENTO           VARCHAR2(255 BYTE),
  NR_SEQ_TERMO_DEC_SAUDE      NUMBER(10),
  NR_SEQ_MODELO               NUMBER(10),
  DS_URL_MANUAL_MPS           VARCHAR2(255 BYTE),
  DS_URL_MANUAL_ORIENT_BENEF  VARCHAR2(255 BYTE),
  DS_DIRETORIO_ANEXO          VARCHAR2(255 BYTE),
  QT_DIA_CANCELAMENTO         NUMBER(5),
  DS_OBSERVACAO_FINALIZAR     VARCHAR2(255 BYTE),
  QT_DIAS_VIGENCIA            NUMBER(2),
  NR_SEQ_VENDEDOR_CANAL       NUMBER(10),
  NR_SEQ_MOTIVO_INCLUSAO      NUMBER(10),
  IE_DECLARACAO_SAUDE         VARCHAR2(1 BYTE),
  IE_GERAR_PROPOSTA           VARCHAR2(1 BYTE),
  IE_CONFIRMA_PRODUTO         VARCHAR2(1 BYTE),
  IE_GERAR_CONTRATO           VARCHAR2(1 BYTE),
  IE_CARTAO_DEFINITIVO        VARCHAR2(1 BYTE),
  NR_SEQ_TERMO_CPT            NUMBER(10),
  NR_SEQ_TERMO_DEBITO_AUT     NUMBER(10),
  QT_DIAS_INADIMPLENCIA       NUMBER(3),
  NR_SEQ_TERMO_BOLETO_EMAIL   NUMBER(10),
  IE_COPIAR_CARENCIA          VARCHAR2(1 BYTE),
  QT_DIAS_LIMITE_CONTRATACAO  NUMBER(3),
  QT_DIAS_LIMITE_COMPLEMENTO  NUMBER(3),
  IE_SOLIC_PORTABILIDADE      VARCHAR2(1 BYTE),
  IE_VALIDAR_BENEF            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPAPON_ESTABEL_FK_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_MODELOT_FK_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPAPON_PK ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSMOISE_FK_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_MOTIVO_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSTMRESP_FK_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_TERMO_DEC_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSTMRESP_FK2_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_TERMO_CPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSTMRESP_FK3_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_TERMO_DEBITO_AUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSTMRESP_FK4_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_TERMO_BOLETO_EMAIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAPON_PLSVEND_FK_I ON TASY.PLS_PARAM_PROPOSTA_ONLINE
(NR_SEQ_VENDEDOR_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_param_prop_onl_afterupdate
after update ON TASY.PLS_PARAM_PROPOSTA_ONLINE for each row
declare

begin

if	(:new.ds_dia_vencimento <> :old.ds_dia_vencimento) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'DS_DIA_VENCIMENTO', :old.ds_dia_vencimento,
					:new.ds_dia_vencimento, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.qt_dia_cancelamento <> :old.qt_dia_cancelamento) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'QT_DIA_CANCELAMENTO', :old.qt_dia_cancelamento,
					:new.qt_dia_cancelamento, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ds_url_manual_instrucao <> :old.ds_url_manual_instrucao) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'DS_URL_MANUAL_INSTRUCAO', :old.ds_url_manual_instrucao,
					:new.ds_url_manual_instrucao, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.nr_seq_modelo <> :old.nr_seq_modelo) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'NR_SEQ_MODELO', :old.nr_seq_modelo,
					:new.nr_seq_modelo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.nr_seq_termo_dec_saude <> :old.nr_seq_termo_dec_saude) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'NR_SEQ_TERMO_DEC_SAUDE', :old.nr_seq_termo_dec_saude,
					:new.nr_seq_termo_dec_saude, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.nr_seq_motivo_inclusao <> :old.nr_seq_motivo_inclusao) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'NR_SEQ_MOTIVO_INCLUSAO', :old.nr_seq_motivo_inclusao,
					:new.nr_seq_motivo_inclusao, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ds_url_manual_mps <> :old.ds_url_manual_mps) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'DS_URL_MANUAL_MPS', :old.ds_url_manual_mps,
					:new.ds_url_manual_mps, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ds_diretorio_anexo <> :old.ds_diretorio_anexo) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'DS_DIRETORIO_ANEXO', :old.ds_diretorio_anexo,
					:new.ds_diretorio_anexo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.qt_dias_vigencia <> :old.qt_dias_vigencia) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'QT_DIAS_VIGENCIA', :old.qt_dias_vigencia,
					:new.qt_dias_vigencia, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.nr_seq_vendedor_canal <> :old.nr_seq_vendedor_canal) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'NR_SEQ_VENDEDOR_CANAL', :old.nr_seq_vendedor_canal,
					:new.nr_seq_vendedor_canal, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ie_declaracao_saude <> :old.ie_declaracao_saude) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'IE_DECLARACAO_SAUDE', :old.ie_declaracao_saude,
					:new.ie_declaracao_saude, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ie_gerar_proposta <> :old.ie_gerar_proposta) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'IE_GERAR_PROPOSTA', :old.ie_gerar_proposta,
					:new.ie_gerar_proposta, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

if	(:new.ds_observacao_finalizar <> :old.ds_observacao_finalizar) then
	pls_gerar_log_alt_parametros(	'PLS_PARAM_PROPOSTA_ONLINE', 'DS_OBSERVACAO_FINALIZAR', :old.ds_observacao_finalizar,
					:new.ds_observacao_finalizar, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
end if;

end;
/


ALTER TABLE TASY.PLS_PARAM_PROPOSTA_ONLINE ADD (
  CONSTRAINT PLSPAPON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PARAM_PROPOSTA_ONLINE ADD (
  CONSTRAINT PLSPAPON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPAPON_PLSMOISE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INCLUSAO) 
 REFERENCES TASY.PLS_MOTIVO_INCLUSAO_SEG (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_MODELOT_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MODELO (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_PLSTMRESP_FK 
 FOREIGN KEY (NR_SEQ_TERMO_DEC_SAUDE) 
 REFERENCES TASY.PLS_TERMO_RESPONSABILIDADE (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_CANAL) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_PLSTMRESP_FK2 
 FOREIGN KEY (NR_SEQ_TERMO_CPT) 
 REFERENCES TASY.PLS_TERMO_RESPONSABILIDADE (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_PLSTMRESP_FK3 
 FOREIGN KEY (NR_SEQ_TERMO_DEBITO_AUT) 
 REFERENCES TASY.PLS_TERMO_RESPONSABILIDADE (NR_SEQUENCIA),
  CONSTRAINT PLSPAPON_PLSTMRESP_FK4 
 FOREIGN KEY (NR_SEQ_TERMO_BOLETO_EMAIL) 
 REFERENCES TASY.PLS_TERMO_RESPONSABILIDADE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PARAM_PROPOSTA_ONLINE TO NIVEL_1;

