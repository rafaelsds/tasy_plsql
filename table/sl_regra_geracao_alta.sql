ALTER TABLE TASY.SL_REGRA_GERACAO_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SL_REGRA_GERACAO_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SL_REGRA_GERACAO_ALTA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_MOTIVO_ALTA          NUMBER(10)            NOT NULL,
  NR_REGRA_SERVICO        NUMBER(10),
  IE_GERAR_SERVICO_LEITO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SLREGEA_MOTALTA_FK_I ON TASY.SL_REGRA_GERACAO_ALTA
(CD_MOTIVO_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SLREGEA_PK ON TASY.SL_REGRA_GERACAO_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLREGEA_SLREGSE_FK_I ON TASY.SL_REGRA_GERACAO_ALTA
(NR_REGRA_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLREGEA_SLREGSE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SL_REGRA_GERACAO_ALTA ADD (
  CONSTRAINT SLREGEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SL_REGRA_GERACAO_ALTA ADD (
  CONSTRAINT SLREGEA_MOTALTA_FK 
 FOREIGN KEY (CD_MOTIVO_ALTA) 
 REFERENCES TASY.MOTIVO_ALTA (CD_MOTIVO_ALTA),
  CONSTRAINT SLREGEA_SLREGSE_FK 
 FOREIGN KEY (NR_REGRA_SERVICO) 
 REFERENCES TASY.SL_REGRA_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SL_REGRA_GERACAO_ALTA TO NIVEL_1;

