ALTER TABLE TASY.ALTERACAO_VENCIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALTERACAO_VENCIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALTERACAO_VENCIMENTO
(
  NR_TITULO               NUMBER(10)            NOT NULL,
  NR_SEQUENCIA            NUMBER(5)             NOT NULL,
  DT_ANTERIOR             DATE                  NOT NULL,
  DT_VENCIMENTO           DATE                  NOT NULL,
  CD_MOTIVO               NUMBER(5)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ALTERACAO            DATE                  NOT NULL,
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  NR_EXTERNO              VARCHAR2(255 BYTE),
  VL_JUROS                NUMBER(15,2),
  VL_MULTA                NUMBER(15,2),
  TX_JUROS                NUMBER(7,4),
  TX_MULTA                NUMBER(7,4),
  DT_BASE_JUROS_MULTA     DATE,
  IE_CALC_JUROS_MULTA     VARCHAR2(1 BYTE),
  IE_JUROS_MULTA_USUARIO  VARCHAR2(1 BYTE),
  NR_SEQ_ALT_VENCTO_ORIG  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ALTVENC_PK ON TASY.ALTERACAO_VENCIMENTO
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALTVENC_TITRECE_FK_I ON TASY.ALTERACAO_VENCIMENTO
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.alteracao_vencimento_befinsert
before insert ON TASY.ALTERACAO_VENCIMENTO for each row
declare

nt_seq_alt_venc_w	number(10);
ie_processo_camara_w		pls_parametros_camara.ie_processo_camara%type;
nr_titulo_receber_w			pls_titulo_lote_camara.nr_titulo_receber%type;
nr_seq_lote_w				pls_titulo_lote_camara.nr_sequencia%type;
ie_baixa_camara_compensacao_w	number(10);
cd_estabelecimento_w		titulo_receber.cd_estabelecimento%type;
ie_tipo_data_cr_w			pls_lote_camara_comp.ie_tipo_data_cr%type;
vl_valor_altera_venc_w		number(15,2);
vl_saldo_titulo_w			titulo_receber.vl_saldo_titulo%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(a.cd_estabelecimento),
		max(a.vl_saldo_titulo)
into	cd_estabelecimento_w,
		vl_saldo_titulo_w
from	titulo_receber a
where	a.nr_titulo = :new.nr_titulo;

/*OS 1555588 - Como e um valor, fiz da mesma forma que o parametro 148 na proc consistir_calcular_cot_compra*/
begin
	select	nvl(max(obter_valor_param_usuario(801,214, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo)),0)
	into	vl_valor_altera_venc_w
	from	dual;
exception when others then
	vl_valor_altera_venc_w := 0;
end;

if (nvl(vl_valor_altera_venc_w,0) > 0) and (vl_saldo_titulo_w > nvl(vl_valor_altera_venc_w,0)) then
	--Nao sera possivel alterar o vencimento do titulo. O saldo e maior que o valor mimino informado no parametro 214. Vl saldo: #@vl_saldo_titulo_w#@ Vl parametro: #@vl_valor_altera_venc_w#@
	wheb_mensagem_pck.exibir_mensagem_abort(995290,	'vl_saldo_titulo_w=' || campo_mascara_virgula(vl_saldo_titulo_w) ||
									';VL_VALOR_ALTERA_VENC_W=' || campo_mascara_virgula(vl_valor_altera_venc_w));
end if;
/*FIM OS 1555588 */

if (cd_estabelecimento_w is not null) then

	/*verificar se o titulo que esta recebendo a baixa esta em um lote de camra de compensacao que nao esta baixado.*/
	select	count(a.nr_titulo_receber),
			max(b.nr_sequencia),
			max(b.ie_tipo_data_cr)
	into	nr_titulo_receber_w,
			nr_seq_lote_w,
			ie_tipo_data_cr_w
	from	pls_titulo_lote_camara a,
			pls_lote_camara_comp b
	where	a.nr_seq_lote_camara	= b.nr_sequencia
	and		a.nr_titulo_receber		= :new.nr_titulo;

	select	max(a.ie_processo_camara)
	into	ie_processo_camara_w
	from	pls_parametros_camara a
	where 	cd_estabelecimento = cd_estabelecimento_w;

	if	(nvl(ie_processo_camara_w,'CO') = 'CA') and (nr_titulo_receber_w > 0) and (ie_tipo_data_cr_w = 'V') then /*se  o processo camara de compensacao for regime de caixa e o titulo estiver na camara e o tipo de data for vencimento*/

		/*verifica se a rotina que esta efetuando a baixa e a rotina que baixa pela camara. Se nao for, deve consistir.*/
		wheb_mensagem_pck.exibir_mensagem_abort(335816,	'NR_SEQ_LOTE_W=' || nr_seq_lote_w ||
									';NR_TITULO_W=' || :new.nr_titulo);
	end if;
end if;

select	nvl(max(a.nr_sequencia),0) + 1
into	nt_seq_alt_venc_w
from	w_alteracao_vencimento a
where	a.nr_titulo		= :new.nr_titulo;

insert	into w_alteracao_vencimento
	(dt_alteracao,
	dt_atualizacao,
	dt_atualizacao_nrec,
	ie_calc_juros_multa,
	nm_usuario,
	nm_usuario_nrec,
	nr_seq_alt_venc,
	nr_sequencia,
	nr_titulo,
	vl_juros,
	vl_multa)
values	(:new.dt_vencimento,
	sysdate,
	sysdate,
	:new.ie_calc_juros_multa,
	:new.nm_usuario,
	:new.nm_usuario,
	:new.nr_sequencia,
	nt_seq_alt_venc_w,
	:new.nr_titulo,
	:new.vl_juros,
	:new.vl_multa);

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_vencimento_update
before update ON TASY.ALTERACAO_VENCIMENTO for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
update	w_alteracao_vencimento a
set	a.vl_juros			= :new.vl_juros,
	a.vl_multa		= :new.vl_multa,
	a.ie_calc_juros_multa	= :new.ie_calc_juros_multa
where	a.nr_seq_alt_venc		= :new.nr_sequencia
and	a.nr_titulo			= :new.nr_titulo;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_vencimento_insert
after insert ON TASY.ALTERACAO_VENCIMENTO for each row
declare

ie_juros_multa_barras_bloq_w	varchar2(1);
ie_juros_multa_venc_orig_w	parametro_contas_receber.ie_juros_multa_venc_orig%type;
cd_estabelecimento_w		number(10);

ie_entrada_confirmada_w		titulo_receber.ie_entrada_confirmada%type;
cd_pessoa_fisica_w			titulo_receber.cd_pessoa_fisica%type;
cd_cgc_w					titulo_receber.cd_cgc%type;
nr_seq_conta_banco_w		titulo_receber.nr_seq_conta_banco%type;
cd_banco_w					banco_estabelecimento.cd_banco%type;
cd_agencia_bancaria_w		banco_estabelecimento.cd_agencia_bancaria%type;
nr_conta_w					banco_estabelecimento.cd_conta%type;
ie_digito_conta_w			banco_estabelecimento.ie_digito_conta%type;
cd_camara_compensacao_w		pessoa_fisica_conta.cd_camara_compensacao%type;
ie_retorno_w				number(10);
ie_tipo_ocorrencia_w		number(10);
vl_desc_previsto_w			titulo_receber.vl_desc_previsto%type;
vl_saldo_titulo_w			titulo_receber.vl_saldo_titulo%type;
cd_moeda_w					titulo_receber.cd_moeda%type;
ie_desc_previsto_w			varchar2(1);
nr_seq_motivo_desc_w		titulo_receber_liq_desc.nr_seq_motivo_desc%type;
cd_centro_custo_desc_w		titulo_receber_liq_desc.cd_centro_custo%type;
ie_juros_multa_w			varchar2(1);

cursor c01 is
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_fisica_conta
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_w
	union
	select	cd_banco,
			cd_agencia_bancaria,
			nr_conta,
			nr_digito_conta,
			cd_camara_compensacao
	from 	pessoa_juridica_conta
	where	cd_cgc 	= cd_cgc_w;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	titulo_receber
where	nr_titulo	= :new.nr_titulo;

select	nvl(max(ie_juros_multa_barras_bloq),'N'),
	nvl(max(ie_juros_multa_venc_orig),'N')
into	ie_juros_multa_barras_bloq_w,
	ie_juros_multa_venc_orig_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_juros_multa_barras_bloq_w <> 'N') and
	(ie_juros_multa_venc_orig_w = 'S') then
	gerar_bloqueto_tit_rec(:new.nr_titulo, 'MTR');
end if;


select	max(a.ie_entrada_confirmada),
		max(a.cd_pessoa_fisica),
		max(a.cd_cgc),
		max(a.nr_seq_conta_banco),
		max(a.vl_desc_previsto),
		max(a.vl_saldo_titulo),
		max(a.cd_moeda)
into	ie_entrada_confirmada_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		nr_seq_conta_banco_w,
		vl_desc_previsto_w,
		vl_saldo_titulo_w,
		cd_moeda_w
from	titulo_receber a
where	a.nr_titulo = :new.nr_titulo;

if (ie_entrada_confirmada_w = 'C') then
	/*Tipo de Ocorrencia igual a 6 pois 6 e para Alteracao de Vencimento*/
	ie_tipo_ocorrencia_w := 6;

	begin
	select	a.cd_banco,
			a.cd_agencia_bancaria,
			a.cd_conta,
			a.ie_digito_conta
	into	cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w
	from	banco_estabelecimento a
	where	a.nr_sequencia = nr_seq_conta_banco_w;
	exception when others then
			ie_retorno_w	:= 0;
	end;

	if	(ie_retorno_w = 0) then
		open c01;
		loop
		fetch c01 into
			cd_banco_w,
			cd_agencia_bancaria_w,
			nr_conta_w,
			ie_digito_conta_w,
			cd_camara_compensacao_w;
		exit when c01%notfound;
			cd_banco_w	:= cd_banco_w;
		end loop;
		close c01;
	end if;

	obter_param_usuario(815, 9, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_desc_previsto_w);
	obter_param_usuario(815, 16, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_juros_multa_w);

	select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc)
	into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w
	from	titulo_receber_liq_desc a
	where	a.nr_titulo		= :new.nr_titulo
	and		a.nr_bordero	is null
	and		a.nr_seq_liq	is null;

	insert into titulo_receber_instr (	nr_seq_cobranca,
										nr_titulo,
										vl_cobranca,
										dt_atualizacao,
										nm_usuario,
										cd_banco,
										cd_agencia_bancaria,
										nr_conta,
										cd_moeda,
										ie_digito_conta,
										cd_camara_compensacao,
										vl_desconto,
										vl_desc_previsto,
										vl_acrescimo,
										nr_sequencia,
										vl_despesa_bancaria,
										vl_saldo_inclusao,
										qt_dias_instrucao,
										cd_ocorrencia,
										ie_instrucao_enviada,
										nr_seq_motivo_desc,
										cd_centro_custo_desc,
										vl_juros,
										vl_multa,
										ie_selecionado)
							values	(	null,
										:new.nr_titulo,
										vl_saldo_titulo_w,
										sysdate,
										:new.nm_usuario,
										cd_banco_w,
										cd_agencia_bancaria_w,
										nr_conta_w,
										cd_moeda_w,
										ie_digito_conta_w,
										cd_camara_compensacao_w,
										decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0)) + nvl(to_number(obter_dados_titulo_receber(:new.nr_titulo,'VNC')),0),
										decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0),0),
										0,
										titulo_receber_instr_seq.nextval,
										0,
										vl_saldo_titulo_w,
										null,
										substr(obter_ocorrencia_envio_cre(ie_tipo_ocorrencia_w,cd_banco_w),1,3),
										'N',
										nr_seq_motivo_desc_w,
										cd_centro_custo_desc_w,
										decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','J')),null),
										decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','M')),null),
										'N');
end if;

end if;
end;
/


ALTER TABLE TASY.ALTERACAO_VENCIMENTO ADD (
  CONSTRAINT ALTVENC_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ALTERACAO_VENCIMENTO ADD (
  CONSTRAINT ALTVENC_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO));

GRANT SELECT ON TASY.ALTERACAO_VENCIMENTO TO NIVEL_1;

