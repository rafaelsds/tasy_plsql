ALTER TABLE TASY.FLUXO_CAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FLUXO_CAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FLUXO_CAIXA
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_REFERENCIA        DATE                     NOT NULL,
  CD_CONTA_FINANC      NUMBER(10)               NOT NULL,
  IE_CLASSIF_FLUXO     VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_FLUXO             NUMBER(17,4)             NOT NULL,
  IE_ORIGEM            VARCHAR2(1 BYTE),
  IE_PERIODO           VARCHAR2(1 BYTE),
  IE_INTEGRACAO        VARCHAR2(2 BYTE),
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_BANCO   NUMBER(10),
  NR_SEQ_PROJ_REC      NUMBER(10),
  IE_CONTRATO          VARCHAR2(1 BYTE),
  CD_MOEDA             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FLUCAIX_BANESTA_FK_I ON TASY.FLUXO_CAIXA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FLUCAIX_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FLUCAIX_CONFINA_FK_I ON TASY.FLUXO_CAIXA
(CD_CONTA_FINANC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FLUCAIX_EMPRESA_FK_I ON TASY.FLUXO_CAIXA
(CD_EMPRESA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FLUCAIX_ESTABEL_FK_I ON TASY.FLUXO_CAIXA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FLUCAIX_MOEDA_FK_I ON TASY.FLUXO_CAIXA
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FLUCAIX_PK ON TASY.FLUXO_CAIXA
(CD_EMPRESA, CD_ESTABELECIMENTO, DT_REFERENCIA, CD_CONTA_FINANC, IE_CLASSIF_FLUXO, 
IE_PERIODO, IE_INTEGRACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FLUCAIX_PRORECU_FK_I ON TASY.FLUXO_CAIXA
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FLUCAIX_PRORECU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.FLUXO_CAIXA_ATUAL
BEFORE INSERT OR UPDATE OR DELETE ON TASY.FLUXO_CAIXA FOR EACH ROW
DECLARE

cont_w	number(10) := 0;
ie_fechamento_diario_w	varchar2(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

Obter_Param_Usuario(830,22,obter_perfil_ativo, :new.nm_usuario ,:new.cd_estabelecimento,ie_fechamento_diario_w);

if (ie_fechamento_diario_w = 'S') then

	if	(nvl(:new.ie_classif_fluxo,:old.ie_classif_fluxo) = 'O') then
		select	count(*)
		into	cont_w
		from	fluxo_caixa_fechamento
		where	dt_referencia		 				= nvl(:new.dt_referencia,:old.dt_referencia)
		and	cd_empresa						= nvl(:new.cd_empresa,:old.cd_empresa)
		and	nvl(ie_tipo_fluxo, 'G')					= 'O'
		and	nvl(cd_estabelecimento, :new.cd_estabelecimento)		= :new.cd_estabelecimento;
	elsif	(nvl(:new.ie_classif_fluxo,:old.ie_classif_fluxo) = 'P') then
		select	count(*)
		into	cont_w
		from	fluxo_caixa_fechamento
		where	dt_referencia						= nvl(:new.dt_referencia,:old.dt_referencia)
		and	cd_empresa						= nvl(:new.cd_empresa,:old.cd_empresa)
		and	nvl(ie_tipo_fluxo, 'G')					= 'G'
		and	nvl(nvl(cd_estabelecimento, :new.cd_estabelecimento), -1)	= nvl(:new.cd_estabelecimento,nvl(:old.cd_estabelecimento,nvl(cd_estabelecimento,-1)));
	end if;

else

	if	(nvl(:new.ie_classif_fluxo,:old.ie_classif_fluxo) = 'O') then
		select	count(*)
		into	cont_w
		from	fluxo_caixa_fechamento
		where	trunc(dt_referencia,'month') 					= trunc(nvl(:new.dt_referencia,:old.dt_referencia),'month')
		and	cd_empresa						= nvl(:new.cd_empresa,:old.cd_empresa)
		and	nvl(ie_tipo_fluxo, 'G')					= 'O'
		and	nvl(cd_estabelecimento, :new.cd_estabelecimento) 		= :new.cd_estabelecimento;
	elsif	(nvl(:new.ie_classif_fluxo,:old.ie_classif_fluxo) = 'P') then
		select	count(*)
		into	cont_w
		from	fluxo_caixa_fechamento
		where	trunc(dt_referencia,'month') 					= trunc(nvl(:new.dt_referencia,:old.dt_referencia),'month')
		and	cd_empresa						= nvl(:new.cd_empresa,:old.cd_empresa)
		and	nvl(ie_tipo_fluxo, 'G')					= 'G'
		and	nvl(nvl(cd_estabelecimento, :new.cd_estabelecimento), -1)	= nvl(:new.cd_estabelecimento,nvl(:old.cd_estabelecimento,nvl(cd_estabelecimento,-1)));
	end if;

end if;

if	(cont_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(176744,	'DT_REFERENCIA_W=' || nvl(:new.dt_referencia,:old.dt_referencia) || ';' ||
							'CD_ESTABELECIMENTO_W=' || nvl(:new.cd_estabelecimento,:old.cd_estabelecimento));
end if;
end if;

end;
/


ALTER TABLE TASY.FLUXO_CAIXA ADD (
  CONSTRAINT FLUCAIX_PK
 PRIMARY KEY
 (CD_EMPRESA, CD_ESTABELECIMENTO, DT_REFERENCIA, CD_CONTA_FINANC, IE_CLASSIF_FLUXO, IE_PERIODO, IE_INTEGRACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FLUXO_CAIXA ADD (
  CONSTRAINT FLUCAIX_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC)
    ON DELETE CASCADE,
  CONSTRAINT FLUCAIX_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT FLUCAIX_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT FLUCAIX_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT FLUCAIX_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT FLUCAIX_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.FLUXO_CAIXA TO NIVEL_1;

