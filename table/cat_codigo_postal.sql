ALTER TABLE TASY.CAT_CODIGO_POSTAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAT_CODIGO_POSTAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAT_CODIGO_POSTAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CODIGO               VARCHAR2(5 BYTE)         NOT NULL,
  CVE_ASEN             VARCHAR2(15 BYTE)        NOT NULL,
  CVE_ENT              VARCHAR2(5 BYTE)         NOT NULL,
  CVE_MUN              VARCHAR2(5 BYTE)         NOT NULL,
  CVE_LOC              VARCHAR2(5 BYTE)         NOT NULL,
  CVE_PERIODO          NUMBER(10)               NOT NULL,
  AGREGADO             VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CACOPO_PK ON TASY.CAT_CODIGO_POSTAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CAT_CODIGO_POSTAL ADD (
  CONSTRAINT CACOPO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CAT_CODIGO_POSTAL TO NIVEL_1;

