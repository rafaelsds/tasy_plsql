ALTER TABLE TASY.OPDM_MED_DEV_REG_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_MED_DEV_REG_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_MED_DEV_REG_DATA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_OPDM_TASY               NUMBER(10),
  DS_MDRD_EU_FILENAME            VARCHAR2(50 BYTE),
  DS_REG_AUTHORITY               VARCHAR2(50 BYTE),
  DS_APP_REGULATION_CODE         VARCHAR2(50 BYTE),
  DS_REGISTERED_COUNTRY          VARCHAR2(400 BYTE),
  DS_NOTIFIED_BODY               VARCHAR2(50 BYTE),
  CD_CE_CERTIFICATE              VARCHAR2(50 BYTE),
  DT_CE_CERTIFICATE_ISSUE        DATE,
  DT_CE_CERTIFICATE_VALID_UNTIL  DATE,
  DS_DECLAR_CONF_DOC_NUMBER      VARCHAR2(255 BYTE),
  DT_FIRST_CONFORMITY            DATE,
  DS_MEDICAL_DEVICE_CLASS        VARCHAR2(50 BYTE),
  DS_STATUS                      VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPDMMEDR_OPDMTASY_FK_I ON TASY.OPDM_MED_DEV_REG_DATA
(NR_SEQ_OPDM_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPDMMEDR_PK ON TASY.OPDM_MED_DEV_REG_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_MED_DEV_REG_DATA_tp  after update ON TASY.OPDM_MED_DEV_REG_DATA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPDM_TASY,1,4000),substr(:new.NR_SEQ_OPDM_TASY,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPDM_TASY',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MDRD_EU_FILENAME,1,4000),substr(:new.DS_MDRD_EU_FILENAME,1,4000),:new.nm_usuario,nr_seq_w,'DS_MDRD_EU_FILENAME',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REG_AUTHORITY,1,4000),substr(:new.DS_REG_AUTHORITY,1,4000),:new.nm_usuario,nr_seq_w,'DS_REG_AUTHORITY',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_APP_REGULATION_CODE,1,4000),substr(:new.DS_APP_REGULATION_CODE,1,4000),:new.nm_usuario,nr_seq_w,'DS_APP_REGULATION_CODE',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REGISTERED_COUNTRY,1,4000),substr(:new.DS_REGISTERED_COUNTRY,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGISTERED_COUNTRY',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_STATUS,1,4000),substr(:new.DS_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'DS_STATUS',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CE_CERTIFICATE,1,4000),substr(:new.CD_CE_CERTIFICATE,1,4000),:new.nm_usuario,nr_seq_w,'CD_CE_CERTIFICATE',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CE_CERTIFICATE_ISSUE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CE_CERTIFICATE_ISSUE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CE_CERTIFICATE_ISSUE',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CE_CERTIFICATE_VALID_UNTIL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CE_CERTIFICATE_VALID_UNTIL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CE_CERTIFICATE_VALID_UNTIL',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DECLAR_CONF_DOC_NUMBER,1,4000),substr(:new.DS_DECLAR_CONF_DOC_NUMBER,1,4000),:new.nm_usuario,nr_seq_w,'DS_DECLAR_CONF_DOC_NUMBER',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIRST_CONFORMITY,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIRST_CONFORMITY,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIRST_CONFORMITY',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MEDICAL_DEVICE_CLASS,1,4000),substr(:new.DS_MEDICAL_DEVICE_CLASS,1,4000),:new.nm_usuario,nr_seq_w,'DS_MEDICAL_DEVICE_CLASS',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NOTIFIED_BODY,1,4000),substr(:new.DS_NOTIFIED_BODY,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOTIFIED_BODY',ie_log_w,ds_w,'OPDM_MED_DEV_REG_DATA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_MED_DEV_REG_DATA ADD (
  CONSTRAINT OPDMMEDR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OPDM_MED_DEV_REG_DATA ADD (
  CONSTRAINT OPDMMEDR_OPDMTASY_FK 
 FOREIGN KEY (NR_SEQ_OPDM_TASY) 
 REFERENCES TASY.OPDM_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_MED_DEV_REG_DATA TO NIVEL_1;

