ALTER TABLE TASY.W_GUIA_MEDICO_MALA_DIRETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_GUIA_MEDICO_MALA_DIRETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_GUIA_MEDICO_MALA_DIRETA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_MUNICIPIO                VARCHAR2(255 BYTE),
  DS_ESPECIALIDADE            VARCHAR2(4000 BYTE),
  IE_PREFIXO                  VARCHAR2(5 BYTE),
  NM_PRESTADOR                VARCHAR2(255 BYTE),
  DS_ENDERECO                 VARCHAR2(255 BYTE),
  NR_ENDERECO                 VARCHAR2(10 BYTE),
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  CD_CEP                      VARCHAR2(30 BYTE),
  DS_BAIRRO                   VARCHAR2(255 BYTE),
  SG_ESTADO                   VARCHAR2(2 BYTE),
  NR_DDD                      VARCHAR2(4 BYTE),
  NR_RAMAL                    VARCHAR2(10 BYTE),
  NR_TELEFONE                 VARCHAR2(30 BYTE),
  DS_FONE_ADIC                VARCHAR2(50 BYTE),
  NR_DDD_FAX                  VARCHAR2(4 BYTE),
  DS_FAX                      VARCHAR2(40 BYTE),
  DS_EMAIL                    VARCHAR2(255 BYTE),
  DS_WEBSITE                  VARCHAR2(255 BYTE),
  NM_MEDICO                   VARCHAR2(255 BYTE),
  NR_CRM                      VARCHAR2(40 BYTE),
  DS_TIPO_PRESTADOR           VARCHAR2(255 BYTE),
  DS_REGIAO                   VARCHAR2(255 BYTE),
  IE_APALC                    VARCHAR2(1 BYTE),
  IE_ADICQ                    VARCHAR2(1 BYTE),
  IE_AONA                     VARCHAR2(1 BYTE),
  IE_ACBA                     VARCHAR2(1 BYTE),
  IE_AIQG                     VARCHAR2(1 BYTE),
  IE_NOTIVISA                 VARCHAR2(1 BYTE),
  IE_POS_GRAD_360             VARCHAR2(1 BYTE),
  IE_RESIDENCIA               VARCHAR2(1 BYTE),
  IE_ESPECIALISTA             VARCHAR2(1 BYTE),
  IE_QUALISS                  VARCHAR2(1 BYTE),
  DS_QUALIFICACOES_AGRUPADAS  VARCHAR2(255 BYTE),
  NR_RQE                      VARCHAR2(20 BYTE),
  IE_ACREDITACAO              VARCHAR2(1 BYTE),
  IE_OUTRA_CERTIF             VARCHAR2(1 BYTE),
  IE_ISO                      VARCHAR2(1 BYTE),
  IE_DOUTORADO                VARCHAR2(1 BYTE),
  IE_MESTRADO                 VARCHAR2(5 BYTE),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  DS_RAZAO_SOCIAL             VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WGUIMMD_PK ON TASY.W_GUIA_MEDICO_MALA_DIRETA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WGUIMMD_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.W_GUIA_MEDICO_MALA_DIRETA_tp  after update ON TASY.W_GUIA_MEDICO_MALA_DIRETA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'W_GUIA_MEDICO_MALA_DIRETA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.W_GUIA_MEDICO_MALA_DIRETA ADD (
  CONSTRAINT WGUIMMD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_GUIA_MEDICO_MALA_DIRETA TO NIVEL_1;

