ALTER TABLE TASY.DUV_SRI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DUV_SRI CASCADE CONSTRAINTS;

CREATE TABLE TASY.DUV_SRI
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MENSAGEM      NUMBER(10)               NOT NULL,
  IE_TIPO_TRATAMENTO   VARCHAR2(1 BYTE),
  IE_TIPO_CONTA        VARCHAR2(1 BYTE),
  VL_TOTAL             NUMBER(15,2),
  VL_OUTROS            NUMBER(15,2),
  VL_TOTAL_CONTA       NUMBER(15,2),
  VL_MATERIAIS         NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DUVSRI_DUVMENS_FK_I ON TASY.DUV_SRI
(NR_SEQ_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DUVSRI_PK ON TASY.DUV_SRI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DUV_SRI ADD (
  CONSTRAINT DUVSRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DUV_SRI ADD (
  CONSTRAINT DUVSRI_DUVMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSAGEM) 
 REFERENCES TASY.DUV_MENSAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DUV_SRI TO NIVEL_1;

