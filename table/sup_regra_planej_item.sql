ALTER TABLE TASY.SUP_REGRA_PLANEJ_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_REGRA_PLANEJ_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_REGRA_PLANEJ_ITEM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PLANEJAMENTO    NUMBER(10)             NOT NULL,
  CD_MATERIAL            NUMBER(10)             NOT NULL,
  QT_CONSUMO_DIARIO      NUMBER(15,4),
  QT_TOT_COMPRA          NUMBER(15,4),
  QT_EST_ATUAL           NUMBER(15,4),
  QT_ENTREGA_PREV        NUMBER(15,4),
  QT_CONS_PREV           NUMBER(15,4),
  QT_EST_PREV_ENTREGA    NUMBER(15,4),
  QT_FALTA_PRIM_ENTREGA  NUMBER(15,4),
  QT_ENTREGA             NUMBER(15,4),
  VL_PRECO_UNITARIO      NUMBER(13,4),
  QT_ESTOQUE_MINIMO      NUMBER(15,4),
  QT_COMPRA_AJUSTADA     NUMBER(15,4),
  NR_SEQ_CALENDARIO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUREPLI_MATERIA_FK_I ON TASY.SUP_REGRA_PLANEJ_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUREPLI_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUREPLI_PK ON TASY.SUP_REGRA_PLANEJ_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUREPLI_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUREPLI_PLNCPCL_FK_I ON TASY.SUP_REGRA_PLANEJ_ITEM
(NR_SEQ_CALENDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUREPLI_PLNCPCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUREPLI_SUPLACO_FK_I ON TASY.SUP_REGRA_PLANEJ_ITEM
(NR_SEQ_PLANEJAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUREPLI_SUPLACO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUP_REGRA_PLANEJ_ITEM_tp  after update ON TASY.SUP_REGRA_PLANEJ_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_FALTA_PRIM_ENTREGA,1,4000),substr(:new.QT_FALTA_PRIM_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_FALTA_PRIM_ENTREGA',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PRECO_UNITARIO,1,4000),substr(:new.VL_PRECO_UNITARIO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PRECO_UNITARIO',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONSUMO_DIARIO,1,4000),substr(:new.QT_CONSUMO_DIARIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONSUMO_DIARIO',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TOT_COMPRA,1,4000),substr(:new.QT_TOT_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'QT_TOT_COMPRA',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COMPRA_AJUSTADA,1,4000),substr(:new.QT_COMPRA_AJUSTADA,1,4000),:new.nm_usuario,nr_seq_w,'QT_COMPRA_AJUSTADA',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ENTREGA_PREV,1,4000),substr(:new.QT_ENTREGA_PREV,1,4000),:new.nm_usuario,nr_seq_w,'QT_ENTREGA_PREV',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONS_PREV,1,4000),substr(:new.QT_CONS_PREV,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONS_PREV',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ENTREGA,1,4000),substr(:new.QT_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ENTREGA',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EST_PREV_ENTREGA,1,4000),substr(:new.QT_EST_PREV_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_EST_PREV_ENTREGA',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ESTOQUE_MINIMO,1,4000),substr(:new.QT_ESTOQUE_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_ESTOQUE_MINIMO',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EST_ATUAL,1,4000),substr(:new.QT_EST_ATUAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_EST_ATUAL',ie_log_w,ds_w,'SUP_REGRA_PLANEJ_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_REGRA_PLANEJ_ITEM ADD (
  CONSTRAINT SUREPLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_REGRA_PLANEJ_ITEM ADD (
  CONSTRAINT SUREPLI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SUREPLI_SUPLACO_FK 
 FOREIGN KEY (NR_SEQ_PLANEJAMENTO) 
 REFERENCES TASY.SUP_PLANEJAMENTO_COMPRAS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUREPLI_PLNCPCL_FK 
 FOREIGN KEY (NR_SEQ_CALENDARIO) 
 REFERENCES TASY.PLANEJ_COMPRAS_CALENDARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUP_REGRA_PLANEJ_ITEM TO NIVEL_1;

