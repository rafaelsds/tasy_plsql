ALTER TABLE TASY.PROTOCOLO_INT_CATEGORIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_INT_CATEGORIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_INT_CATEGORIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TP_EVENTO     NUMBER(10)               NOT NULL,
  NR_SEQ_ACAO          NUMBER(10)               NOT NULL,
  NR_ORDEM             NUMBER(5),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRINTCAT_PK ON TASY.PROTOCOLO_INT_CATEGORIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRINTCAT_PRINTPEV_FK_I ON TASY.PROTOCOLO_INT_CATEGORIA
(NR_SEQ_TP_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROTOCOLO_INT_CATEGORIA ADD (
  CONSTRAINT PRINTCAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PROTOCOLO_INT_CATEGORIA ADD (
  CONSTRAINT PRINTCAT_PRINTPEV_FK 
 FOREIGN KEY (NR_SEQ_TP_EVENTO) 
 REFERENCES TASY.PROTOCOLO_INT_TIPO_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

