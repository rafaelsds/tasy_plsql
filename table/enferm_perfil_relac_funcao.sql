DROP TABLE TASY.ENFERM_PERFIL_RELAC_FUNCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENFERM_PERFIL_RELAC_FUNCAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_REGISTRO                DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQ_CABECALHO           NUMBER(10)         NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_CUIDADO_ENF             VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMB_CUIDADO_ENF        VARCHAR2(1 BYTE)   NOT NULL,
  DS_CUIDADOR_NOME           VARCHAR2(1 BYTE)   NOT NULL,
  DS_CUIDADOR_IDADE          VARCHAR2(1 BYTE)   NOT NULL,
  IE_CUIDADOR_RELACAO        VARCHAR2(1 BYTE)   NOT NULL,
  IE_CUIDADOR_SAUDE          VARCHAR2(1 BYTE)   NOT NULL,
  IE_PESO_FAMILIA_CASA       VARCHAR2(1 BYTE)   NOT NULL,
  DS_PESO_FAMILIA_CASA       VARCHAR2(1 BYTE)   NOT NULL,
  DS_DESEJO_CUIDADO_PESSOA   VARCHAR2(1 BYTE)   NOT NULL,
  DS_FAMILIA_CARINHO         VARCHAR2(1 BYTE)   NOT NULL,
  DS_PESSOA_CHAVE_NOME       VARCHAR2(1 BYTE)   NOT NULL,
  DS_PESSOA_CHAVE_IDADE      VARCHAR2(1 BYTE)   NOT NULL,
  IE_PESSOA_CHAVE_RELACAO    VARCHAR2(1 BYTE)   NOT NULL,
  IE_PESSOA_CHAVE_SAUDE      VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_OUTROS_PESSOA       VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_OUTROS_NOME         VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_OUTROS_IDADE        VARCHAR2(1 BYTE)   NOT NULL,
  IE_REL_OUTROS_RELACAO      VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_OUTROS              VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_OUTROS_OBS          VARCHAR2(1 BYTE)   NOT NULL,
  IE_REL_FAMILIAR            VARCHAR2(1 BYTE)   NOT NULL,
  DS_REL_FAMILIAR            VARCHAR2(1 BYTE)   NOT NULL,
  DS_PAPEL_CASA              VARCHAR2(1 BYTE)   NOT NULL,
  IE_TRABALHANDO             VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMB1_TRABALHANDO       VARCHAR2(1 BYTE)   NOT NULL,
  IE_COMB2_TRABALHANDO       VARCHAR2(1 BYTE)   NOT NULL,
  IE_TRABALHANDO_ATUAL       VARCHAR2(1 BYTE)   NOT NULL,
  IE_TRABALHANDO_PASSADO     VARCHAR2(1 BYTE)   NOT NULL,
  IE_PLANO_TRABALHO          VARCHAR2(1 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENPEREFU_ATEPACI_FK_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_CABECAL_FK_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_ESTABEL_FK_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_PESFISI_FK_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_PESFISI_FK2_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_TASASDI_FK_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENPEREFU_TASASDI_FK2_I ON TASY.ENFERM_PERFIL_RELAC_FUNCAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ENFERM_PERFIL_RELAC_FUNCAO_tp  after update ON TASY.ENFERM_PERFIL_RELAC_FUNCAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null; gravar_log_alteracao(substr(:old.DS_REL_OUTROS,1,4000),substr(:new.DS_REL_OUTROS,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_OUTROS',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PLANO_TRABALHO,1,4000),substr(:new.IE_PLANO_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PLANO_TRABALHO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REL_FAMILIAR,1,4000),substr(:new.IE_REL_FAMILIAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REL_FAMILIAR',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REL_FAMILIAR,1,4000),substr(:new.DS_REL_FAMILIAR,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_FAMILIAR',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PAPEL_CASA,1,4000),substr(:new.DS_PAPEL_CASA,1,4000),:new.nm_usuario,nr_seq_w,'DS_PAPEL_CASA',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRABALHANDO,1,4000),substr(:new.IE_TRABALHANDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRABALHANDO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUIDADO_ENF,1,4000),substr(:new.IE_CUIDADO_ENF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUIDADO_ENF',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMB_CUIDADO_ENF,1,4000),substr(:new.IE_COMB_CUIDADO_ENF,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMB_CUIDADO_ENF',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CUIDADOR_NOME,1,4000),substr(:new.DS_CUIDADOR_NOME,1,4000),:new.nm_usuario,nr_seq_w,'DS_CUIDADOR_NOME',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CUIDADOR_IDADE,1,4000),substr(:new.DS_CUIDADOR_IDADE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CUIDADOR_IDADE',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PESO_FAMILIA_CASA,1,4000),substr(:new.DS_PESO_FAMILIA_CASA,1,4000),:new.nm_usuario,nr_seq_w,'DS_PESO_FAMILIA_CASA',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUIDADOR_RELACAO,1,4000),substr(:new.IE_CUIDADOR_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUIDADOR_RELACAO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUIDADOR_SAUDE,1,4000),substr(:new.IE_CUIDADOR_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUIDADOR_SAUDE',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PESO_FAMILIA_CASA,1,4000),substr(:new.IE_PESO_FAMILIA_CASA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PESO_FAMILIA_CASA',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DESEJO_CUIDADO_PESSOA,1,4000),substr(:new.DS_DESEJO_CUIDADO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DESEJO_CUIDADO_PESSOA',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FAMILIA_CARINHO,1,4000),substr(:new.DS_FAMILIA_CARINHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_FAMILIA_CARINHO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PESSOA_CHAVE_NOME,1,4000),substr(:new.DS_PESSOA_CHAVE_NOME,1,4000),:new.nm_usuario,nr_seq_w,'DS_PESSOA_CHAVE_NOME',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PESSOA_CHAVE_IDADE,1,4000),substr(:new.DS_PESSOA_CHAVE_IDADE,1,4000),:new.nm_usuario,nr_seq_w,'DS_PESSOA_CHAVE_IDADE',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PESSOA_CHAVE_RELACAO,1,4000),substr(:new.IE_PESSOA_CHAVE_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PESSOA_CHAVE_RELACAO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PESSOA_CHAVE_SAUDE,1,4000),substr(:new.IE_PESSOA_CHAVE_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PESSOA_CHAVE_SAUDE',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REL_OUTROS_PESSOA,1,4000),substr(:new.DS_REL_OUTROS_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_OUTROS_PESSOA',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REL_OUTROS_NOME,1,4000),substr(:new.DS_REL_OUTROS_NOME,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_OUTROS_NOME',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REL_OUTROS_IDADE,1,4000),substr(:new.DS_REL_OUTROS_IDADE,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_OUTROS_IDADE',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REL_OUTROS_RELACAO,1,4000),substr(:new.IE_REL_OUTROS_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REL_OUTROS_RELACAO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMB1_TRABALHANDO,1,4000),substr(:new.IE_COMB1_TRABALHANDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMB1_TRABALHANDO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMB2_TRABALHANDO,1,4000),substr(:new.IE_COMB2_TRABALHANDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMB2_TRABALHANDO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRABALHANDO_ATUAL,1,4000),substr(:new.IE_TRABALHANDO_ATUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRABALHANDO_ATUAL',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRABALHANDO_PASSADO,1,4000),substr(:new.IE_TRABALHANDO_PASSADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRABALHANDO_PASSADO',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REL_OUTROS_OBS,1,4000),substr(:new.DS_REL_OUTROS_OBS,1,4000),:new.nm_usuario,nr_seq_w,'DS_REL_OUTROS_OBS',ie_log_w,ds_w,'ENFERM_PERFIL_RELAC_FUNCAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ENFERM_PERFIL_RELAC_FUNCAO ADD (
  CONSTRAINT ENPEREFU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ENPEREFU_CABECAL_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.ENFERM_PERFIL_CABECALHO (NR_SEQUENCIA),
  CONSTRAINT ENPEREFU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ENPEREFU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENPEREFU_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENPEREFU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ENPEREFU_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

