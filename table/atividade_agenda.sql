ALTER TABLE TASY.ATIVIDADE_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATIVIDADE_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATIVIDADE_AGENDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO_ATIVIDADE    VARCHAR2(3 BYTE),
  NR_PRIORIDADE        NUMBER(3),
  CD_FUNCAO            NUMBER(5),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ATIVIDADE         VARCHAR2(150 BYTE)       NOT NULL,
  QT_TEMPO_ATIVIDADE   NUMBER(5),
  IE_FREQUENCIA        VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_DIA_SEMANA        NUMBER(2),
  IE_SEMANA            NUMBER(3),
  CD_GRUPO_ATIVIDADE   NUMBER(10),
  DS_COR_ATIVIDADE     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATIVAGE_FUNCAO_FK_I ON TASY.ATIVIDADE_AGENDA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATIVAGE_GRATAG_FK_I ON TASY.ATIVIDADE_AGENDA
(CD_GRUPO_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATIVAGE_GRATAG_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATIVAGE_PK ON TASY.ATIVIDADE_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATIVAGE_PK
  MONITORING USAGE;


ALTER TABLE TASY.ATIVIDADE_AGENDA ADD (
  CONSTRAINT ATIVAGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATIVIDADE_AGENDA ADD (
  CONSTRAINT ATIVAGE_GRATAG_FK 
 FOREIGN KEY (CD_GRUPO_ATIVIDADE) 
 REFERENCES TASY.GRUPO_ATIVIDADE_AGENDA (NR_SEQUENCIA),
  CONSTRAINT ATIVAGE_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO));

GRANT SELECT ON TASY.ATIVIDADE_AGENDA TO NIVEL_1;

