ALTER TABLE TASY.EMPRESTIMO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMPRESTIMO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMPRESTIMO_MATERIAL
(
  NR_EMPRESTIMO         NUMBER(10)              NOT NULL,
  NR_SEQUENCIA          NUMBER(5)               NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  QT_MATERIAL           NUMBER(13,4)            NOT NULL,
  QT_EMPRESTIMO         NUMBER(13,4),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_VALIDADE_MATERIAL  DATE,
  DS_LOTE_FORNEC        VARCHAR2(20 BYTE),
  NR_SEQ_LOTE           NUMBER(10),
  VL_REFERENCIA         NUMBER(13,4),
  IE_INDETERMINADO      VARCHAR2(1 BYTE),
  QT_NOTA_FISCAL        NUMBER(13,4),
  NR_ORDEM_COMPRA       NUMBER(10),
  NR_ITEM_OCI           NUMBER(5),
  NR_SEQ_MARCA          NUMBER(10),
  CD_CGC_LOTE           VARCHAR2(14 BYTE),
  NR_SEQ_REG_PCS        NUMBER(10),
  QT_SOLICITADA         NUMBER(13,4),
  IE_ATUALIZA_ESTOQUE   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMPMATE_EMPREST_FK_I ON TASY.EMPRESTIMO_MATERIAL
(NR_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_I1 ON TASY.EMPRESTIMO_MATERIAL
(QT_MATERIAL, CD_MATERIAL, NR_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_I2 ON TASY.EMPRESTIMO_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_MARCA_FK_I ON TASY.EMPRESTIMO_MATERIAL
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMATE_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPMATE_MATERIA_FK_I ON TASY.EMPRESTIMO_MATERIAL
(CD_MATERIAL, QT_MATERIAL, NR_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_MATLOFO_FK_I ON TASY.EMPRESTIMO_MATERIAL
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_ORCOITE_FK_I ON TASY.EMPRESTIMO_MATERIAL
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMPMATE_ORCOITE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMPMATE_PCSREAN_FK_I ON TASY.EMPRESTIMO_MATERIAL
(NR_SEQ_REG_PCS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPMATE_PESJURI_FK_I ON TASY.EMPRESTIMO_MATERIAL
(CD_CGC_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EMPMATE_PK ON TASY.EMPRESTIMO_MATERIAL
(NR_EMPRESTIMO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Emprestimo_Material_insert
BEFORE INSERT or UPDATE ON TASY.EMPRESTIMO_MATERIAL for each row
declare

dt_mesano_referencia_w	Date;
ie_consignado_w		Varchar2(1);
ie_material_estoque_w	Varchar2(1);
cd_estabelecimento_w		Number(05,0);
cd_local_estoque_w		Number(05,0);
qt_reg_saldo_w		Number(05,0);
cd_material_w			Number(06,0);

BEGIN
if	((inserting) or
	((updating) and (:new.qt_emprestimo <> :old.qt_emprestimo))) and
	(:new.qt_emprestimo <= 0)then
	/*A quantidade informada deve ser maior que zero.*/
	wheb_mensagem_pck.exibir_mensagem_abort(283707);
end if;

if	(updating) and
	(:new.qt_emprestimo <> :old.qt_emprestimo) then
	wheb_mensagem_pck.exibir_mensagem_abort(157714); /*N�o pode ser alterado a quantidade do material no empr�stimo.*/
end if;

if	(updating) and
	(:new.CD_MATERIAL <> :old.CD_MATERIAL) then
	wheb_mensagem_pck.exibir_mensagem_abort(157713); /*N�o pode ser alterado o material do empr�stimo.*/
end if;

if	(nvl(:new.ie_atualiza_estoque,'X') <> 'S') then
	:new.ie_atualiza_estoque	:=	'N';
end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	emprestimo
where	nr_emprestimo		= :new.nr_emprestimo;

select	ie_consignado,
	substr(obter_se_material_estoque(cd_estabelecimento_w, 0, cd_material),1,1),
	cd_material_estoque
into	ie_consignado_w,
	ie_material_estoque_w,
	cd_material_w
from	material
where	cd_material = :new.cd_material;

if	(ie_material_estoque_w	= 'S') and
	(ie_consignado_w 		<> '1') then
	begin
	select	cd_local_estoque,
		trunc(dt_emprestimo,'month')
	into	cd_local_estoque_w,
		dt_mesano_referencia_w
	from	emprestimo
	where	nr_emprestimo		= :new.nr_emprestimo;
	if	(dt_mesano_referencia_w > trunc(sysdate,'month')) then
		wheb_mensagem_pck.exibir_mensagem_abort(265976);
		--'O emprestimo n�o pode ter data superior que o mes vigente'
	end if;
	select	count(*)
	into	qt_reg_saldo_w
	from	saldo_estoque
	where	dt_mesano_referencia	= dt_mesano_referencia_w
	and	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_local_estoque	= cd_local_estoque_w
	and	cd_material		= cd_material_w;
	if	(qt_reg_saldo_w = 0) then
		insert into saldo_estoque(
			cd_estabelecimento, cd_local_estoque, cd_material,
			dt_mesano_referencia, qt_estoque, vl_estoque,
			qt_reservada_requisicao, qt_reservada, dt_atualizacao,
			nm_usuario, vl_custo_medio, vl_preco_ult_compra,
			dt_ult_compra, ie_status_valorizacao, ie_bloqueio_inventario)
		values(
			cd_estabelecimento_w, cd_local_estoque_w, cd_material_w,
			dt_mesano_referencia_w, 0, 0,
			0, 0, sysdate,
			:new.nm_usuario, 0, 0,
			null, 'N', 'N');
	end if;
	end;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.Emprestimo_Material_delete
before delete ON TASY.EMPRESTIMO_MATERIAL for each row
declare

ie_erro_w			varchar2(255);
ie_tipo_w			varchar2(1);
ie_acao_w			varchar2(1);
cd_estabelecimento_w		Number(05,0);
ie_estoque_lote_w		varchar2(1);
dt_mesano_referencia_w		Date;
cd_local_estoque_w		Number(05,0);
qt_movimentos_lote_w		number(10);
ie_disp_emprestimo_lib_w	parametro_estoque.ie_disp_emprestimo_lib%type;

pragma autonomous_transaction;

BEGIN
select	cd_estabelecimento,
	ie_tipo
into	cd_estabelecimento_w,
	ie_tipo_w
from	emprestimo
where	nr_emprestimo		= :old.nr_emprestimo;

select	substr(obter_se_material_estoque_lote(cd_estabelecimento_w, :old.cd_material),1,1)
into	ie_estoque_lote_w
from	dual;

select	max(ie_disp_emprestimo_lib)
into	ie_disp_emprestimo_lib_w
from	parametro_estoque
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(:old.ie_atualiza_estoque,'X') = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(450415);
end if;

if	(ie_estoque_lote_w = 'S') and
	(:old.nr_seq_lote is not null) and
	(:old.ds_lote_fornec is not null) then
	begin
	select	count(*)
	into	qt_movimentos_lote_w
	from	movimento_estoque
	where	nr_seq_lote_fornec = :old.nr_seq_lote;

	if	(ie_tipo_w = 'E') and
		(qt_movimentos_lote_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(252985);
	end if;

	if	(:old.dt_validade_material is not null) and
		(ie_disp_emprestimo_lib_w <> 'S') then
		begin
		select	cd_local_estoque,
			trunc(sysdate,'month')
		into	cd_local_estoque_w,
			dt_mesano_referencia_w
		from	emprestimo
		where	nr_emprestimo		= :old.nr_emprestimo;

		ie_acao_w	:= '2';
		if	(ie_tipo_w = 'S') then
			ie_acao_w	:= '1';
		end if;

		atualizar_saldo_lote(
				cd_estabelecimento_w,
				cd_local_estoque_w,
				:old.cd_material,
				dt_mesano_referencia_w,
				:old.nr_seq_lote,
				0,
				:old.qt_emprestimo,
				ie_acao_w,
				:old.nm_usuario,
				ie_erro_w);

		end;
	end if;
	end;
end if;
commit;
END Emprestimo_Material_delete;
/


ALTER TABLE TASY.EMPRESTIMO_MATERIAL ADD (
  CONSTRAINT EMPMATE_PK
 PRIMARY KEY
 (NR_EMPRESTIMO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMPRESTIMO_MATERIAL ADD (
  CONSTRAINT EMPMATE_PCSREAN_FK 
 FOREIGN KEY (NR_SEQ_REG_PCS) 
 REFERENCES TASY.PCS_REG_ANALISE (NR_SEQUENCIA),
  CONSTRAINT EMPMATE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_LOTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT EMPMATE_EMPREST_FK 
 FOREIGN KEY (NR_EMPRESTIMO) 
 REFERENCES TASY.EMPRESTIMO (NR_EMPRESTIMO)
    ON DELETE CASCADE,
  CONSTRAINT EMPMATE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT EMPMATE_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT EMPMATE_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI),
  CONSTRAINT EMPMATE_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMPRESTIMO_MATERIAL TO NIVEL_1;

