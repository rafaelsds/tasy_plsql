ALTER TABLE TASY.PBS_INDICATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PBS_INDICATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.PBS_INDICATION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_NOTE              CLOB,
  DS_ABREV_IND         CLOB,
  NR_MIMS_INDICATION   NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_INDICATION        CLOB,
  CD_AUTHORITY_CODE    VARCHAR2(50 BYTE)        DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PBSINDI_PK ON TASY.PBS_INDICATION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PBS_INDICATION ADD (
  CONSTRAINT PBSINDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PBS_INDICATION TO NIVEL_1;

