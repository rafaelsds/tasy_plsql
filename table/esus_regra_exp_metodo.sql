ALTER TABLE TASY.ESUS_REGRA_EXP_METODO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESUS_REGRA_EXP_METODO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESUS_REGRA_EXP_METODO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_METODO            VARCHAR2(255 BYTE)       NOT NULL,
  NM_CAMPO             VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO_VALOR        VARCHAR2(255 BYTE)       NOT NULL,
  DS_CHAMADA_METODO    VARCHAR2(255 BYTE),
  NR_SEQ_CLASSE        NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_ORDEM         NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESUSREM_ESUSREC_FK_I ON TASY.ESUS_REGRA_EXP_METODO
(NR_SEQ_CLASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESUSREM_PK ON TASY.ESUS_REGRA_EXP_METODO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESUS_REGRA_EXP_METODO ADD (
  CONSTRAINT ESUSREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESUS_REGRA_EXP_METODO ADD (
  CONSTRAINT ESUSREM_ESUSREC_FK 
 FOREIGN KEY (NR_SEQ_CLASSE) 
 REFERENCES TASY.ESUS_REGRA_EXP_CLASSE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESUS_REGRA_EXP_METODO TO NIVEL_1;

