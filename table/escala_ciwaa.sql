ALTER TABLE TASY.ESCALA_CIWAA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CIWAA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CIWAA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  DT_AVALIACAO             DATE                 NOT NULL,
  QT_SCORE                 NUMBER(3),
  IE_MAL_ESTAR             NUMBER(3),
  IE_PERTURBACAO_COCEIRA   NUMBER(3),
  IE_TREMOR_BRACO          NUMBER(3),
  IE_SUDORESE              NUMBER(3),
  IE_PERTURBACAO_AUDITIVA  NUMBER(3),
  IE_PERTURBACAO_VISUAL    NUMBER(3),
  IE_NERVOSISMO            NUMBER(3),
  IE_DOR_CABECA            NUMBER(3),
  IE_AGITACAO              NUMBER(3),
  IE_ALT_ORIENTACAO        NUMBER(3),
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCIWAA_ATEPACI_FK_I ON TASY.ESCALA_CIWAA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCIWAA_PESFISI_FK_I ON TASY.ESCALA_CIWAA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCIWAA_PK ON TASY.ESCALA_CIWAA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_ciwaa_atual
before insert or update ON TASY.ESCALA_CIWAA for each row
declare

qt_resultado_w	number(10) := 0;

begin

if	(:new.IE_MAL_ESTAR is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_MAL_ESTAR;
end if;

if	(:new.IE_TREMOR_BRACO is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_TREMOR_BRACO;
end if;

if	(:new.IE_SUDORESE is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_SUDORESE;
end if;

if	(:new.IE_PERTURBACAO_COCEIRA is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_PERTURBACAO_COCEIRA;
end if;

if	(:new.IE_PERTURBACAO_AUDITIVA is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_PERTURBACAO_AUDITIVA;
end if;

if	(:new.IE_PERTURBACAO_VISUAL is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_PERTURBACAO_VISUAL;
end if;

if	(:new.IE_NERVOSISMO is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_NERVOSISMO;
end if;

if	(:new.IE_DOR_CABECA is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_DOR_CABECA;
end if;

if	(:new.IE_AGITACAO is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_AGITACAO;
end if;

if	(:new.IE_ALT_ORIENTACAO is not null) then
	qt_resultado_w	:= qt_resultado_w + :new.IE_ALT_ORIENTACAO;
end if;


:new.QT_SCORE  := qt_resultado_w;

end;
/


ALTER TABLE TASY.ESCALA_CIWAA ADD (
  CONSTRAINT ESCIWAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CIWAA ADD (
  CONSTRAINT ESCIWAA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCIWAA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CIWAA TO NIVEL_1;

