ALTER TABLE TASY.LOTE_AUDIT_HIST_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_AUDIT_HIST_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_AUDIT_HIST_LIB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NM_USUARIO_LIB          VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_LOTE_AUDIT_HIST  NUMBER(10)            NOT NULL,
  NR_SEQ_REGRA_LIB        NUMBER(10),
  DT_APROVACAO            DATE,
  DT_REPROVACAO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOTAUHLI_LOTAUHI_FK_I ON TASY.LOTE_AUDIT_HIST_LIB
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTAUHLI_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LOTAUHLI_PK ON TASY.LOTE_AUDIT_HIST_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOTE_AUDIT_HIST_LIB ADD (
  CONSTRAINT LOTAUHLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_AUDIT_HIST_LIB ADD (
  CONSTRAINT LOTAUHLI_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOTE_AUDIT_HIST_LIB TO NIVEL_1;

