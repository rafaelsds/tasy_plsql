ALTER TABLE TASY.DIRF_LOG_EXCLUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIRF_LOG_EXCLUSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIRF_LOG_EXCLUSAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_DIRF     NUMBER(10),
  NR_TITULO            NUMBER(10),
  CD_TRIBUTO           NUMBER(10),
  CD_DARF              VARCHAR2(10 BYTE),
  IE_TIPO_ALTERACAO    VARCHAR2(2 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(255 BYTE),
  DS_ALTERACAO         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIRFLOGEX_DIRFLOTEM_FK_I ON TASY.DIRF_LOG_EXCLUSAO
(NR_SEQ_LOTE_DIRF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIRFLOGEX_DIRFLOTEM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DIRFLOGEX_PK ON TASY.DIRF_LOG_EXCLUSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIRFLOGEX_PK
  MONITORING USAGE;


ALTER TABLE TASY.DIRF_LOG_EXCLUSAO ADD (
  CONSTRAINT DIRFLOGEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIRF_LOG_EXCLUSAO ADD (
  CONSTRAINT DIRFLOGEX_DIRFLOTEM_FK 
 FOREIGN KEY (NR_SEQ_LOTE_DIRF) 
 REFERENCES TASY.DIRF_LOTE_MENSAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIRF_LOG_EXCLUSAO TO NIVEL_1;

