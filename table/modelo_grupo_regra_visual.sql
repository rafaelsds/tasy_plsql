ALTER TABLE TASY.MODELO_GRUPO_REGRA_VISUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MODELO_GRUPO_REGRA_VISUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MODELO_GRUPO_REGRA_VISUAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  NR_SEQ_PERGUNTA      NUMBER(10)               NOT NULL,
  IE_OPERACAO          VARCHAR2(2 BYTE)         NOT NULL,
  DS_RESULTADO         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MODGRVI_MODGRCO_FK_I ON TASY.MODELO_GRUPO_REGRA_VISUAL
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MODGRVI_PK ON TASY.MODELO_GRUPO_REGRA_VISUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MODELO_GRUPO_REGRA_VISUAL ADD (
  CONSTRAINT MODGRVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MODELO_GRUPO_REGRA_VISUAL ADD (
  CONSTRAINT MODGRVI_MODGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.MODELO_GRUPO_CONTEUDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MODELO_GRUPO_REGRA_VISUAL TO NIVEL_1;

