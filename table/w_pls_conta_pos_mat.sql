ALTER TABLE TASY.W_PLS_CONTA_POS_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_CONTA_POS_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_CONTA_POS_MAT
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CONTA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  NR_SEQ_SEGURADO             NUMBER(10),
  IE_TIPO_GUIA                VARCHAR2(2 BYTE),
  NR_SEQ_PLANO                NUMBER(10),
  NR_SEQ_CONGENERE_SEG        NUMBER(10),
  NR_SEQ_CONTA_MAT            NUMBER(10),
  NR_SEQ_REGRA_POS_ESTAB      NUMBER(10),
  IE_GERA_VALOR_POS_ESTAB     VARCHAR2(3 BYTE),
  DT_POSTAGEM_FATURA          DATE,
  IE_TIPO_SEGURADO            VARCHAR2(3 BYTE),
  IE_TIPO_DESPESA             VARCHAR2(1 BYTE),
  DT_RECEBIMENTO_FATURA       DATE,
  NR_SEQ_POS_ESTAB_INTERC     NUMBER(10),
  NR_SEQ_CONGENERE            NUMBER(10),
  IE_TIPO_REGRA_POS           VARCHAR2(3 BYTE),
  VL_MATERIAIS                NUMBER(15,2),
  IE_TIPO_INTERCAMBIO         VARCHAR2(10 BYTE),
  VL_ADMINISTRACAO            NUMBER(15,2),
  TX_ADMINISTRACAO            NUMBER(7,4),
  NR_SEQ_INTERCAMBIO          NUMBER(10),
  NR_SEQ_REGRA_TP_POS         NUMBER(10),
  CD_PROCEDIMENTO             NUMBER(15),
  QT_ITEM                     NUMBER(12,4),
  QT_ORIGINAL                 NUMBER(12,4),
  VL_MATERIAIS_CALC           NUMBER(15,2),
  IE_TIPO_TABELA_VALORIZACAO  VARCHAR2(3 BYTE),
  IE_PCMSO                    VARCHAR2(1 BYTE),
  VL_LIBERADO_MATERIAL_FAT    NUMBER(15,2),
  DT_ITEM                     DATE,
  NR_SEQ_MATERIAL             NUMBER(10),
  NR_SEQ_GRUPO_COOP           NUMBER(10),
  NR_SEQ_CABECALHO            NUMBER(10),
  DET_REG_ANVISA_OPME         VARCHAR2(50 BYTE),
  DT_ALTA                     DATE,
  CD_REF_MATERIAL_FAB_OPME    VARCHAR2(60 BYTE),
  IE_OBITO                    VARCHAR2(1 BYTE),
  NR_REGISTRO_ANVISA          VARCHAR2(15 BYTE),
  IE_ORIGEM_CONTA             VARCHAR2(1 BYTE),
  NR_SEQ_FATURA               NUMBER(10),
  VL_TAXA_MATERIAL            NUMBER(12,4),
  CD_REF_FABRICANTE           VARCHAR2(60 BYTE),
  IE_TIPO_CONTA               VARCHAR2(10 BYTE),
  NR_SEQ_REGRA_TX_INTER       NUMBER(10),
  DT_ATENDIMENTO              DATE,
  VL_LIB_TAXA_MATERIAL        NUMBER(15,2),
  VL_MATERIAL_TAB             NUMBER(15,2),
  IE_STATUS                   VARCHAR2(1 BYTE),
  DT_MES_COMPETENCIA          DATE,
  NR_SEQ_PROTOCOLO            NUMBER(10),
  CD_GUIA                     VARCHAR2(20 BYTE),
  IE_ORIGEM_PRECO             NUMBER(2),
  IE_CARATER_INTERNACAO       VARCHAR2(1 BYTE),
  VL_MATERIAL_TABELA          NUMBER(15,4),
  NR_SEQ_TIPO_ACOMODACAO      NUMBER(10),
  VL_PRESTADOR_PAG            NUMBER(15,4),
  NR_SEQ_CLINICA              NUMBER(10),
  VL_CH_MATERIAL              NUMBER(15,4),
  NR_SEQ_GUIA                 NUMBER(10),
  NR_SEQ_PRESTADOR_PROT       NUMBER(10),
  NR_SEQ_REGRA_PRECO          NUMBER(10),
  VL_PRESTADOR_APRESENTADO    NUMBER(15,4),
  NR_SEQ_TIPO_ATENDIMENTO     NUMBER(10),
  DS_ITEM_CONVERTIDO          VARCHAR2(255 BYTE),
  NR_SEQ_REGRA_CONV           NUMBER(10),
  CD_ITEM_CONVERTIDO          VARCHAR2(20 BYTE),
  NR_SEQ_PRESTADOR_EXEC       NUMBER(10),
  CD_SENHA_EXTERNA            VARCHAR2(30 BYTE),
  CD_MOEDA_CALCULO            NUMBER(3),
  NR_SEQ_SCA                  NUMBER(10),
  VL_PROVISAO                 NUMBER(15,2),
  NR_SEQ_AJUSTE_FAT           NUMBER(10),
  NR_SEQ_MAT_PRINC            NUMBER(10),
  NR_SEQ_CONTRATO             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPLSPM_ESTABEL_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_MOEDA_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(CD_MOEDA_CALCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WPLSPM_PK ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSCOMAT_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSCOME_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSCPCA_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSPRCO_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSPRES_FK2_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_PRESTADOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPM_PLSREIN_FK_I ON TASY.W_PLS_CONTA_POS_MAT
(NR_SEQ_REGRA_TX_INTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PLS_CONTA_POS_MAT ADD (
  CONSTRAINT WPLSPM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PLS_CONTA_POS_MAT ADD (
  CONSTRAINT WPLSPM_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WPLSPM_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_EXEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT WPLSPM_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_CALCULO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT WPLSPM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WPLSPM_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT WPLSPM_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT WPLSPM_PLSCPCA_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.PLS_CONTA_POS_CABECALHO (NR_SEQUENCIA),
  CONSTRAINT WPLSPM_PLSREIN_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TX_INTER) 
 REFERENCES TASY.PLS_REGRA_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PLS_CONTA_POS_MAT TO NIVEL_1;

