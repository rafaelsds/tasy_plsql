ALTER TABLE TASY.CM_CICLO_LAVACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CICLO_LAVACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CICLO_LAVACAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CICLO                   NUMBER(15)         NOT NULL,
  DT_CANCELAMENTO            DATE,
  QT_TEMPERATURA             NUMBER(6,2),
  QT_PRESSAO                 NUMBER(6,2),
  CD_CICLO                   VARCHAR2(80 BYTE),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  IE_PRESSAO_ALTO_NIVEL      VARCHAR2(1 BYTE),
  IE_TEMPERATURA_ALTO_NIVEL  VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF             NUMBER(10),
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_CANCEL       NUMBER(10),
  NR_SEQ_EQUIPAMENTO         NUMBER(10)         NOT NULL,
  DT_CONFERENCIA             DATE,
  NM_USUARIO_CONFERENCIA     VARCHAR2(15 BYTE),
  DS_LOTE_DET_ANVISA         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCILAV_CMCLMET_FK_I ON TASY.CM_CICLO_LAVACAO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_CMCLMET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCILAV_CMEQUIP_FK_I ON TASY.CM_CICLO_LAVACAO
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_CMEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCILAV_CMMOCAC_FK_I ON TASY.CM_CICLO_LAVACAO
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_CMMOCAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCILAV_ESTABEL_FK_I ON TASY.CM_CICLO_LAVACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCILAV_PK ON TASY.CM_CICLO_LAVACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_PK
  MONITORING USAGE;


CREATE INDEX TASY.CMCILAV_SETATEN_FK_I ON TASY.CM_CICLO_LAVACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCILAV_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_ciclo_lavacao_after
after insert or update ON TASY.CM_CICLO_LAVACAO 
for each row
declare

gerar_ceq_cod_ciclo_w	varchar2(2);

begin

if (inserting) then

	Obter_Param_Usuario(406, 48, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, gerar_ceq_cod_ciclo_w);
				
	gravar_log_tasy(675, 'Valor parâmetro [48]: ' || gerar_ceq_cod_ciclo_w ||
				' - Seq ciclo: ' || :new.nr_sequencia ||
				' - Equipamento: ' || :new.nr_seq_equipamento ||	
				' - Ciclo: ' || :new.nr_ciclo ||		
				' - Lote: ' || :new.cd_ciclo ||
				' - Observação: ' || :new.ds_observacao ||
				' - Data Inicio: ' || :new.dt_inicio ||
				' - Data fim: ' || :new.dt_fim, :new.nm_usuario);
	
elsif (updating) then
		
	Obter_Param_Usuario(406, 48, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, gerar_ceq_cod_ciclo_w);
			
	gravar_log_tasy(675, 'Valor parâmetro [48]: ' || gerar_ceq_cod_ciclo_w ||
			' - Seq ciclo: ' || :new.nr_sequencia ||			
			' - Equipamento antigo: ' || :old.nr_seq_equipamento ||
			' - Equipamento novo: ' || :new.nr_seq_equipamento ||			
			' - Ciclo antigo: ' || :old.nr_ciclo ||
			' - Ciclo novo: ' || :new.nr_ciclo ||				
			' - Lote antigo: ' || :old.cd_ciclo ||
			' - Lote novo: ' || :new.cd_ciclo ||		
			' - Observação antigo: ' || :old.ds_observacao ||
			' - Observação novo: ' || :new.ds_observacao ||	
			' - Data Inicio antigo: ' || :old.dt_inicio ||
			' - Data Inicio novo: ' || :new.dt_inicio ||
			' - Data fim antigo: ' || :old.dt_fim ||
			' - Data fim novo: ' || :new.dt_fim, :new.nm_usuario);	
	
end if;

end;
/


ALTER TABLE TASY.CM_CICLO_LAVACAO ADD (
  CONSTRAINT CMCILAV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CICLO_LAVACAO ADD (
  CONSTRAINT CMCILAV_CMCLMET_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CM_CLASSIF_METODO (NR_SEQUENCIA),
  CONSTRAINT CMCILAV_CMEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.CM_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT CMCILAV_CMMOCAC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.CM_MOTIVO_CANCEL_CICLO (NR_SEQUENCIA),
  CONSTRAINT CMCILAV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMCILAV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CM_CICLO_LAVACAO TO NIVEL_1;

