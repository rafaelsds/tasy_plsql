ALTER TABLE TASY.PE_DIAGNOSTICO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_DIAGNOSTICO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_DIAGNOSTICO_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DIAG          NUMBER(10)               NOT NULL,
  NR_SEQ_EVENTO        NUMBER(10)               NOT NULL,
  IE_GERA_EVENTO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEDIAEV_PEDIAGN_FK_I ON TASY.PE_DIAGNOSTICO_EVENTO
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIAEV_PEDIAGN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEDIAEV_PK ON TASY.PE_DIAGNOSTICO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIAEV_PK
  MONITORING USAGE;


CREATE INDEX TASY.PEDIAEV_QUAEVEN_FK_I ON TASY.PE_DIAGNOSTICO_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIAEV_QUAEVEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PE_DIAGNOSTICO_EVENTO ADD (
  CONSTRAINT PEDIAEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_DIAGNOSTICO_EVENTO ADD (
  CONSTRAINT PEDIAEV_PEDIAGN_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.PE_DIAGNOSTICO (NR_SEQUENCIA),
  CONSTRAINT PEDIAEV_QUAEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.QUA_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_DIAGNOSTICO_EVENTO TO NIVEL_1;

