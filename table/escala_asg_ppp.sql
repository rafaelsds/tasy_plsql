ALTER TABLE TASY.ESCALA_ASG_PPP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ASG_PPP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ASG_PPP
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PONTO_PESO              NUMBER(3),
  QT_PESO_ATUAL              NUMBER(10,3)       NOT NULL,
  QT_ALTURA_CM               NUMBER(5,2),
  QT_PESO_MES                NUMBER(10,3),
  QT_PESO_6_MES              NUMBER(10,3),
  IE_AUMENTO_PESO            VARCHAR2(10 BYTE)  NOT NULL,
  IE_INGESTAO_MES            VARCHAR2(3 BYTE),
  IE_INGESTAO_ATUAL          VARCHAR2(3 BYTE),
  QT_PONTO_INGESTAO          NUMBER(3),
  IE_SEM_PROBLEMA_ALIMENTAR  VARCHAR2(1 BYTE),
  IE_SEM_APETITE             VARCHAR2(1 BYTE),
  IE_NAUSEA                  VARCHAR2(1 BYTE),
  IE_VOMITO                  VARCHAR2(1 BYTE),
  IE_CONSTIPACAO             VARCHAR2(1 BYTE),
  IE_DIARREIA                VARCHAR2(1 BYTE),
  IE_FERIDA_BOCA             VARCHAR2(1 BYTE),
  IE_BOCA_SECA               VARCHAR2(1 BYTE),
  IE_ALIMENTO_SEM_GOSTO      VARCHAR2(1 BYTE),
  IE_CHEIRO_ENJOAM           VARCHAR2(1 BYTE),
  IE_PROBLEMA_ENGOLIR        VARCHAR2(1 BYTE),
  IE_RAPIDAMENTE_SAT         VARCHAR2(1 BYTE),
  IE_DOR                     VARCHAR2(1 BYTE),
  IE_OUTROS                  VARCHAR2(1 BYTE),
  QT_PONTO_SINTOMAS          NUMBER(3),
  IE_ATIVIDADE               VARCHAR2(3 BYTE)   NOT NULL,
  QT_PONTO_ATIVIDADE         NUMBER(3),
  QT_PONTO_DOENCA            NUMBER(3),
  IE_CANCER                  VARCHAR2(1 BYTE),
  IE_AIDS                    VARCHAR2(1 BYTE),
  IE_TRAUMA                  VARCHAR2(1 BYTE),
  IE_CAQUEIXA                VARCHAR2(1 BYTE),
  IE_ULCERA                  VARCHAR2(1 BYTE),
  IE_IDADE_65                VARCHAR2(1 BYTE),
  IE_FEBRE                   NUMBER(1),
  IE_DURACAO_FEBRE           NUMBER(1),
  IE_CORTICOSTEROIDES        NUMBER(1),
  QT_PONTO_ESTRESSE          NUMBER(3),
  IE_REG_PERI_ORBITAL        NUMBER(1),
  IE_PREGA_TRICEPS           NUMBER(1),
  IE_GORDURA_COSTELA         NUMBER(1),
  IE_GERAL_DEF_GORDURA       NUMBER(1),
  IE_TEMPORAS                NUMBER(1),
  IE_CLAVICULAS              NUMBER(1),
  IE_OMBROS                  NUMBER(1),
  IE_MUSC_INTRA_OSSEA        NUMBER(1),
  IE_ESCAPULA                NUMBER(1),
  IE_COXA                    NUMBER(1),
  IE_PANTURRILHA             NUMBER(1),
  IE_GERAL_DEF_MUSC          NUMBER(1),
  IE_EDEMA_TORNOZELO         NUMBER(1),
  IE_EDEMA_SACRAL            NUMBER(1),
  IE_ASCITE                  NUMBER(1),
  IE_GERAL_EST_HIDRATACAO    NUMBER(1),
  QT_PONTO_EXAME_FIS         NUMBER(3),
  QT_PONTO_TOTAL             NUMBER(3),
  IE_AVALIACAO_GLOBAL        VARCHAR2(1 BYTE),
  IE_PESO_MES_ANT            NUMBER(3),
  DS_DOR                     VARCHAR2(80 BYTE),
  DS_OUTROS_SINTOMAS         VARCHAR2(80 BYTE),
  IE_PESO_6_MESES            NUMBER(3),
  QT_PONTO_A                 NUMBER(3),
  PR_PERDA_MES               NUMBER(15,4),
  PR_PERDA_6_MES             NUMBER(15,4),
  QT_PONTOS_GORDURA          NUMBER(10),
  QT_PONTOS_MUSCULO          NUMBER(10),
  QT_PONTOS_HIDRATACAO       NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_COMIDA_NORMAL_MENOR     VARCHAR2(1 BYTE),
  IE_COMIDA_NORMAL_POUCA     VARCHAR2(1 BYTE),
  IE_APENAS_LIQUIDO          VARCHAR2(1 BYTE),
  IE_APENAS_SUPLEMENTO       VARCHAR2(1 BYTE),
  IE_POUCA_COMIDA            VARCHAR2(1 BYTE),
  IE_ALIMENTO_SONDA          VARCHAR2(1 BYTE),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  IE_VERSAO                  VARCHAR2(5 BYTE),
  IE_FADIGA                  VARCHAR2(1 BYTE),
  IE_INSUF_RENAL_CRONICA     VARCHAR2(1 BYTE),
  DS_OUTROS_DIAGNOSTICOS     VARCHAR2(80 BYTE),
  DS_ESTAGIO_DOENCA          VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCASGP_ATEPACI_FK_I ON TASY.ESCALA_ASG_PPP
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCASGP_EHRREEL_FK_I ON TASY.ESCALA_ASG_PPP
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCASGP_PESFISI_FK_I ON TASY.ESCALA_ASG_PPP
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCASGP_PESFISI_FK2_I ON TASY.ESCALA_ASG_PPP
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCASGP_PK ON TASY.ESCALA_ASG_PPP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCASGP_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCASGP_TASASDI_FK_I ON TASY.ESCALA_ASG_PPP
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCASGP_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCASGP_TASASDI_FK2_I ON TASY.ESCALA_ASG_PPP
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCASGP_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PONTUACAO_ESCALA_ASG_ATUAL
before insert or update ON TASY.ESCALA_ASG_PPP for each row
declare
qt_pontuacao_peso_w	number(3) := 0;
qt_pontuacao_ingest_w	number(3) := 0;
qt_pontuacao_ingest_ww	number(3) := 0;
qt_pontuacao_sint_w	number(3) := 0;
qt_pontuacao_ativ_w	number(3) := 0;
qt_pontuacao_score_a_w	number(5) := 0;
qt_pontuacao_doenca_w	number(3) := 0;
qt_pontuacao_demanda_w	number(5) := 0;
qt_pontuacao_fisico_w	number(5) := 0;
qt_pontuacao_fisico_b_w	number(5) := 0;
qt_pontuacao_fisico_c_w	number(5) := 0;
qt_ie_peso_mes_ant_convert_w number(5) := 0;
qt_ie_peso_6_mes_convert_w number(5) := 0;

begin

if (:new.QT_PESO_MES > 0) then
:new.PR_PERDA_MES := 100 - (:new.QT_PESO_ATUAL * 100) /:new.QT_PESO_MES;
end if;

if (:new.QT_PESO_6_MES > 0) then
:new.PR_PERDA_6_MES := 100 - (:new.QT_PESO_ATUAL * 100) /:new.QT_PESO_6_MES;
end if;

if	(:new.ie_aumento_peso = 'D') then
	qt_pontuacao_peso_w := qt_pontuacao_peso_w + 1;
end if;

if	(:new.IE_PESO_MES_ANT = 5) then
	qt_ie_peso_mes_ant_convert_w := 0;
else
	qt_ie_peso_mes_ant_convert_w := :new.IE_PESO_MES_ANT;
end if;

if	(:new.IE_PESO_6_MESES = 5) then
	qt_ie_peso_6_mes_convert_w := 0;
else
	qt_ie_peso_6_mes_convert_w := :new.IE_PESO_6_MESES;
end if;

if	(:new.QT_PESO_MES > 0) then
	qt_pontuacao_peso_w := qt_pontuacao_peso_w + 	qt_ie_peso_mes_ant_convert_w;
else
	qt_pontuacao_peso_w := qt_pontuacao_peso_w + 	qt_ie_peso_6_mes_convert_w;
end if;

:new.qt_ponto_peso := qt_pontuacao_peso_w;

qt_pontuacao_ingest_w	:=	0;

if	(:new.ie_ingestao_mes = 'ME') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 1;
end if;

if	(:new.ie_pouca_comida = 'S') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 4;
elsif	(:new.ie_apenas_liquido = 'S') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 3;
elsif	(:new.ie_apenas_suplemento = 'S') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 3;
elsif	(:new.ie_comida_normal_pouca = 'S') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 2;
elsif 	(:new.ie_comida_normal_menor = 'S') then
	qt_pontuacao_ingest_w := qt_pontuacao_ingest_w + 1;
end if;

:new.qt_ponto_ingestao := qt_pontuacao_ingest_w;

if	(:new.ie_sem_apetite = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 3;
end if;

if	(:new.ie_nausea = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_vomito = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 3;
end if;

if	(:new.ie_constipacao = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_diarreia = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 3;
end if;

if	(:new.ie_ferida_boca = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 2;
end if;

if	(:new.ie_boca_seca = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_alimento_sem_gosto = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_cheiro_enjoam = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_problema_engolir = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 2;
end if;

if	(:new.ie_rapidamente_sat = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_fadiga = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

if	(:new.ie_dor = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 3;
end if;

if	(:new.ie_outros = 'S') then
	qt_pontuacao_sint_w := qt_pontuacao_sint_w + 1;
end if;

:new.qt_ponto_sintomas := qt_pontuacao_sint_w;

if	(:new.ie_atividade = 'NT') then
	qt_pontuacao_ativ_w := 1;
elsif	(:new.ie_atividade = 'NS') then
	qt_pontuacao_ativ_w := 2;
elsif	(:new.ie_atividade = 'CF') then
	qt_pontuacao_ativ_w := 3;
elsif	(:new.ie_atividade = 'BT') then
	qt_pontuacao_ativ_w := 3;
end if;

:new.qt_ponto_atividade := qt_pontuacao_ativ_w;

qt_pontuacao_score_a_w := qt_pontuacao_peso_w + :new.qt_ponto_ingestao + qt_pontuacao_sint_w + qt_pontuacao_ativ_w;

:new.QT_PONTO_A	:= qt_pontuacao_score_a_w;

if	(:new.ie_cancer = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_aids = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_caqueixa = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_ulcera = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_idade_65 = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_insuf_renal_cronica = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

if	(:new.ie_trauma = 'S') then
	qt_pontuacao_doenca_w := qt_pontuacao_doenca_w + 1;
end if;

:new.qt_ponto_doenca := qt_pontuacao_doenca_w;

if	(:new.ie_febre = 1) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 1;
elsif	(:new.ie_febre = 2) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 2;
elsif	(:new.ie_febre = 3) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 3;
end if;

if 	(:new.ie_duracao_febre = 1) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 1;
elsif	(:new.ie_duracao_febre = 2) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 2;
elsif	(:new.ie_duracao_febre = 3) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 3;
end if;

if 	(:new.ie_corticosteroides = 1) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 1;
elsif	(:new.ie_corticosteroides = 2) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 2;
elsif	(:new.ie_corticosteroides = 3) then
	qt_pontuacao_demanda_w := qt_pontuacao_demanda_w + 3;
end if;

:new.qt_ponto_estresse := qt_pontuacao_demanda_w;

-- 7a.

/*if 	(:new.ie_reg_peri_orbital = 1) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 1;
elsif	(:new.ie_reg_peri_orbital = 2) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 2;
elsif	(:new.ie_reg_peri_orbital = 3) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 3;
end if;

if 	(:new.ie_prega_triceps = 1) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 1;
elsif	(:new.ie_prega_triceps = 2) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 2;
elsif	(:new.ie_prega_triceps = 3) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 3;
end if;

if 	(:new.ie_gordura_costela = 1) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 1;
elsif	(:new.ie_gordura_costela = 2) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 2;
elsif	(:new.ie_gordura_costela = 3) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 3;
end if;

if 	(:new.ie_geral_def_gordura = 1) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 1;
elsif	(:new.ie_geral_def_gordura = 2) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 2;
elsif	(:new.ie_geral_def_gordura = 3) then
	qt_pontuacao_fisico_w := qt_pontuacao_fisico_w + 3;
end if;*/

--:new.QT_PONTOS_GORDURA := qt_pontuacao_fisico_w;

:new.QT_PONTOS_GORDURA	:= :new.ie_geral_def_gordura;


--7b

/*if 	(:new.ie_temporas = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_temporas  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_temporas  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_claviculas = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_claviculas  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_claviculas  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_ombros = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_ombros  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_ombros  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_musc_intra_ossea = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_musc_intra_ossea  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_musc_intra_ossea  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_escapula = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_escapula  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_escapula  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_coxa = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_coxa  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_coxa  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_panturrilha = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_panturrilha  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_panturrilha  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;

if 	(:new.ie_geral_def_musc = 1) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 1;
elsif	(:new.ie_geral_def_musc  = 2) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 2;
elsif	(:new.ie_geral_def_musc  = 3) then
	qt_pontuacao_fisico_b_w := qt_pontuacao_fisico_b_w + 3;
end if;	*/

--:new.QT_PONTOS_MUSCULO := qt_pontuacao_fisico_b_w;
:new.QT_PONTOS_MUSCULO	:= :new.ie_geral_def_musc;

-- 7c
/*if 	(:new.ie_edema_tornozelo = 1) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 1;
elsif	(:new.ie_edema_tornozelo  = 2) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 2;
elsif	(:new.ie_edema_tornozelo  = 3) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 3;
end if;

if 	(:new.ie_edema_sacral = 1) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 1;
elsif	(:new.ie_edema_sacral  = 2) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 2;
elsif	(:new.ie_edema_sacral  = 3) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 3;
end if;

if 	(:new.ie_ascite = 1) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 1;
elsif	(:new.ie_ascite  = 2) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 2;
elsif	(:new.ie_ascite  = 3) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 3;
end if;

if 	(:new.ie_geral_est_hidratacao = 1) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 1;
elsif	(:new.ie_geral_est_hidratacao  = 2) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 2;
elsif	(:new.ie_geral_est_hidratacao  = 3) then
	qt_pontuacao_fisico_c_w := qt_pontuacao_fisico_c_w + 3;
end if;	*/

--:new.QT_PONTOS_HIDRATACAO := qt_pontuacao_fisico_c_w;

:new.QT_PONTOS_HIDRATACAO := :new.ie_geral_est_hidratacao;

if	(:new.ie_geral_def_gordura > :new.ie_geral_def_musc) then
	:new.qt_ponto_exame_fis := :new.ie_geral_def_gordura;
else
	:new.qt_ponto_exame_fis := :new.ie_geral_def_musc;
end if;

if	(:new.qt_ponto_exame_fis < :new.ie_geral_est_hidratacao) then
	:new.qt_ponto_exame_fis := :new.ie_geral_est_hidratacao;
end if;

:new.qt_ponto_total := 	qt_pontuacao_score_a_w + qt_pontuacao_doenca_w + qt_pontuacao_demanda_w + :new.qt_ponto_exame_fis;

end;
/


ALTER TABLE TASY.ESCALA_ASG_PPP ADD (
  CONSTRAINT ESCASGP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ASG_PPP ADD (
  CONSTRAINT ESCASGP_PESFISI_FK2 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCASGP_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCASGP_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCASGP_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCASGP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCASGP_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_ASG_PPP TO NIVEL_1;

