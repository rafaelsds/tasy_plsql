ALTER TABLE TASY.DIAGNOSTICO_DOENCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIAGNOSTICO_DOENCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIAGNOSTICO_DOENCA
(
  NR_ATENDIMENTO             NUMBER(10),
  DT_DIAGNOSTICO             DATE               NOT NULL,
  CD_DOENCA                  VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_DIAGNOSTICO             VARCHAR2(2000 BYTE),
  IE_CLASSIFICACAO_DOENCA    VARCHAR2(1 BYTE),
  IE_TIPO_DOENCA             VARCHAR2(2 BYTE),
  IE_UNIDADE_TEMPO           VARCHAR2(2 BYTE),
  QT_TEMPO                   NUMBER(15),
  IE_LADO                    VARCHAR2(1 BYTE),
  DT_MANIFESTACAO            DATE,
  NR_SEQ_DIAG_INTERNO        NUMBER(10),
  NR_SEQ_GRUPO_DIAG          NUMBER(10),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  DT_LIBERACAO               DATE,
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_TIPO_DIAGNOSTICO        NUMBER(3),
  CD_MEDICO                  VARCHAR2(10 BYTE),
  IE_TIPO_ATENDIMENTO        NUMBER(3),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_INTERNO             NUMBER(10),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  NR_SEQ_ETIOLOGIA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  NR_RECEM_NATO              NUMBER(3),
  DT_CID                     DATE,
  NR_SEQ_CLASSIF_ADIC        NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  IE_TIPO_DIAG_CLASSIF       VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_REPORTED                VARCHAR2(1 BYTE),
  DT_REPORTED                DATE,
  CD_DOENCA_SUPERIOR         VARCHAR2(10 BYTE),
  IE_SINTOMATICO             NUMBER(1),
  IE_TIPO_AFEICAO            VARCHAR2(1 BYTE),
  NR_SEQ_VERSAO_CID          NUMBER(10),
  NR_SEQ_LOCO_REG            NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10),
  NR_SEQ_PEND_PAC_ACAO       NUMBER(10),
  NR_SEQ_ROTINA              NUMBER(10)         DEFAULT null,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  IE_DIAG_ADMISSAO           VARCHAR2(1 BYTE),
  IE_DIAG_TRAT_CERT          VARCHAR2(1 BYTE),
  IE_DIAG_PRE_CIR            VARCHAR2(1 BYTE),
  IE_DIAG_CIRURGIA           VARCHAR2(1 BYTE),
  IE_DIAG_ALTA               VARCHAR2(1 BYTE),
  IE_DIAG_OBITO              VARCHAR2(1 BYTE),
  IE_DIAG_PRINC_DEPART       VARCHAR2(1 BYTE),
  IE_DIAG_PRINC_EPISODIO     VARCHAR2(1 BYTE),
  IE_DIAG_REFERENCIA         VARCHAR2(1 BYTE),
  IE_DIAG_TRATAMENTO         VARCHAR2(1 BYTE),
  NR_RQE                     VARCHAR2(20 BYTE),
  IE_DIAG_CRONICO            VARCHAR2(1 BYTE),
  IE_STATUS_DIAG             VARCHAR2(2 BYTE),
  IE_DIAG_TRAT_ESPECIAL      VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_ACAO                    VARCHAR2(1 BYTE),
  IE_RELEVANTE_DRG           VARCHAR2(1 BYTE),
  CD_CID_AUTOCOMPLETE        VARCHAR2(10 BYTE),
  NR_SEQ_ATEPACU             NUMBER(10),
  IE_SIST_EXT_ORIGEM         VARCHAR2(15 BYTE),
  CD_DISEASE_MEDIS           NUMBER(8),
  IE_MAIN_ENC_PROBL          VARCHAR2(1 BYTE),
  IE_STATUS_PROBLEMA         VARCHAR2(10 BYTE),
  IE_CONVENIO                VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO          NUMBER(10),
  CD_EVOLUCAO                NUMBER(10),
  CD_EFFECTIVE_MONTHS        VARCHAR2(10 BYTE),
  DT_EFFECTIVE_DATE          DATE,
  IE_MAIN_HOSPITALIZATION    VARCHAR2(1 BYTE),
  NR_SEQ_JAP_PREF_1          NUMBER(10),
  NR_SEQ_JAP_PREF_2          NUMBER(10),
  NR_SEQ_JAP_PREF_3          NUMBER(10),
  NR_SEQ_JAP_SUFI_1          NUMBER(10),
  NR_SEQ_JAP_SUFI_2          NUMBER(10),
  NR_SEQ_JAP_SUFI_3          NUMBER(10),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  NR_SEQ_DIAGNOSIS_PREF_SUF  NUMBER(15),
  IE_SIDE_MODIFIER           VARCHAR2(15 BYTE),
  CD_ESPECIALIDADE_MED       NUMBER(5),
  NR_SEQ_INTERNO_PAI         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIADOEN_ATCONSPEPA_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_ATEPACU_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_ATEPACU)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_CANLORE_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_LOCO_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_CIDDOEN_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_CIDDOEN_FK2_I ON TASY.DIAGNOSTICO_DOENCA
(CD_DOENCA_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_CIDDOVER_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_VERSAO_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_CIRURGI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_DIACLAA_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_CLASSIF_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_DIACLAA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_DIAGRUP_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_GRUPO_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_DIAGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_DIAINTE_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_DIAG_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_DIAINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_DIAMEDI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_ATENDIMENTO, DT_DIAGNOSTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_EHRREEL_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_EHRREGI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_ETIDIAG_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_ETIOLOGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_ETIDIAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_EVOPACI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_GQAPPAC_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_I1 ON TASY.DIAGNOSTICO_DOENCA
(DT_DIAGNOSTICO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_I2 ON TASY.DIAGNOSTICO_DOENCA
(DT_DIAGNOSTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_JPDOPSUX_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_DIAGNOSIS_PREF_SUF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_NAISINS_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_OFCOFOR_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_PEPOCIR_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_PERFIL_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_PESFISI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIADOEN_PK ON TASY.DIAGNOSTICO_DOENCA
(NR_ATENDIMENTO, DT_DIAGNOSTICO, CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_SETATEN_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIADOEN_TASASDI_FK_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DIADOEN_TASASDI_FK2_I ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DIADOEN_UK ON TASY.DIAGNOSTICO_DOENCA
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIADOEN_UK
  MONITORING USAGE;


CREATE INDEX TASY.MEDDECS_FK_I ON TASY.DIAGNOSTICO_DOENCA
(CD_DISEASE_MEDIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_AFTINSUPD
AFTER INSERT OR UPDATE ON TASY.DIAGNOSTICO_DOENCA FOR EACH ROW
Declare

CD_CATEGORIA_CID_w CID_DOENCA.CD_CATEGORIA_CID%type;
CD_PESSOA_FISICA_W  pessoa_fisica.CD_PESSOA_FISICA%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	SELECT	max(c.CD_PESSOA_FISICA)
	into	CD_PESSOA_FISICA_W
	FROM	ATENDIMENTO_PACIENTE c
	where	c.NR_ATENDIMENTO = :new.NR_ATENDIMENTO;

		if	(CD_PESSOA_FISICA_W is not null) then

			select	max(a.CD_CATEGORIA_CID)
			into	CD_CATEGORIA_CID_w
			from	CID_DOENCA a
			where	a.cd_DOENCA = :new.cd_DOENCA;


			if (CD_CATEGORIA_CID_w =  'E10') or (CD_CATEGORIA_CID_w =  'E11') or  (CD_CATEGORIA_CID_w =  'E12') or (CD_CATEGORIA_CID_w =  'E13') or (CD_CATEGORIA_CID_w =  'E14') then
				UPDATE HD_PAC_RENAL_CRONICO a
				SET IE_DIABETICO = 'S'
				WHERE a.cd_pessoa_fisica = CD_PESSOA_FISICA_W;

			elsif (CD_CATEGORIA_CID_w =  'I10') or (CD_CATEGORIA_CID_w =  'I11') or (CD_CATEGORIA_CID_w =  'I12') or (CD_CATEGORIA_CID_w =  'I13') or (CD_CATEGORIA_CID_w =  'I15') then
				UPDATE HD_PAC_RENAL_CRONICO a
				SET IE_HIPERTENSO = 'S'
				WHERE a.cd_pessoa_fisica = CD_PESSOA_FISICA_W;

			end if;
	  end if;
	end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_doenca_ish_atual
after insert or update or delete ON TASY.DIAGNOSTICO_DOENCA for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
nr_sequencia_w		varchar2(255);

procedure agendar_envio_diag_filho(
		ie_operacao_p		varchar2,
		nr_atendimento_p		diagnostico_doenca.nr_atendimento%type,
		dt_diagnostico_p		diagnostico_doenca.dt_diagnostico%type,
		cd_doenca_p		diagnostico_doenca.cd_doenca%type,
		nm_usuario_p		varchar2) is

cursor c01 is
select	nr_atendimento,
	nr_seq_interno,
	cd_doenca
from	diagnostico_doenca
where	nr_atendimento = nr_atendimento_p
and	dt_diagnostico = dt_diagnostico_p
and	cd_doenca_superior = cd_doenca_p
order by nr_seq_interno;

c01_w	c01%rowtype;

reg_integracao_w	gerar_int_padrao.reg_integracao;
nr_sequencia_ww		varchar2(255);

pragma autonomous_transaction;
begin
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin
	reg_integracao_w.ie_operacao	:=	ie_operacao_p;
	reg_integracao_w.ie_status		:=	'P';
	reg_integracao_w.nr_atendimento	:=	c01_w.nr_atendimento;

	nr_sequencia_ww := c01_w.nr_atendimento || '�' || c01_w.nr_seq_interno || '�' || c01_w.cd_doenca;

	gerar_int_padrao.gravar_integracao('82', nr_sequencia_ww, nm_usuario_p, reg_integracao_w);
	end;
end loop;
close c01;
commit;
end agendar_envio_diag_filho;

function obter_se_diag_lib(
		dt_liberacao_p		date,
		nr_atendimento_p		diagnostico_doenca.nr_atendimento%type,
		dt_diagnostico_p		diagnostico_doenca.dt_diagnostico%type,
		cd_doenca_superior_p	diagnostico_doenca.cd_doenca%type)
		return varchar2 is

ie_liberado_w		varchar2(1);
dt_liberacao_sup_w	date;

pragma autonomous_transaction;
begin
ie_liberado_w	:=	'N';

if	(nvl(cd_doenca_superior_p,'X') = 'X') then
	if	(dt_liberacao_p	is not null) then
		ie_liberado_w	:=	'S';
	end if;
else
	begin
	select	max(dt_liberacao)
	into	dt_liberacao_sup_w
	from	diagnostico_doenca
	where	nr_atendimento = nr_atendimento_p
	and	dt_diagnostico = dt_diagnostico_p
	and	cd_doenca = cd_doenca_superior_p;

	if	(dt_liberacao_sup_w is not null) and (dt_liberacao_p is not null) then
		ie_liberado_w	:=	'S';
	end if;
	end;
end if;

return ie_liberado_w;
end obter_se_diag_lib;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	reg_integracao_p.ie_status := 'AB';
	if	(obter_se_diag_lib(
			nvl(:new.dt_liberacao, :old.dt_liberacao),
			nvl(:new.nr_atendimento, :old.nr_atendimento),
			nvl(:new.dt_diagnostico, :old.dt_diagnostico),
			nvl(:new.cd_doenca_superior, :old.cd_doenca_superior)) = 'S') then
		begin
		if 	(inserting) or
			((updating) and (:old.dt_liberacao is null and :new.dt_liberacao is not null)) then
			begin
			if	(:new.cd_doenca_superior is null) then
				reg_integracao_p.ie_operacao	:=	'I';
				reg_integracao_p.ie_status		:=	'P';
				reg_integracao_p.nr_atendimento	:=	:new.nr_atendimento;
				nr_sequencia_w := :new.nr_atendimento||'�'||:new.nr_seq_interno||'�'||:new.cd_doenca;

				gerar_int_padrao.gravar_integracao('82', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);
				/* OS 1967752, removido tratamento para agendar msg dos diagn�sticos filhos, j� que ser�o enviados juntamente na msg do pai.
				agendar_envio_diag_filho(reg_integracao_p.ie_operacao, :new.nr_atendimento, :new.dt_diagnostico, :new.cd_doenca, :new.nm_usuario);
				*/
			end if;
			end;
		/* verifica se o diagnostico esta sendo inativado, caso sim ser� deletado no ish, pendente verificar se j� foi faturado!!!!*/
		elsif	(updating) then
			if	(:new.dt_inativacao is not null) and
				(:old.dt_inativacao is null) and
				(:new.ie_situacao = 'I') then
				begin

				reg_integracao_p.ie_operacao	:= 'E';
				reg_integracao_p.ie_status	:= 'P';

				--Quando for para excluir, deve agendar primeiro a exclus�o dos filhos e ap�s a do dig pai.
				/* OS 1967752, removido tratamento para agendar msg dos diagn�sticos filhos, j� que ser�o enviados juntamente na msg do pai.
				if	(:new.cd_doenca_superior is null) then
					agendar_envio_diag_filho(reg_integracao_p.ie_operacao, :new.nr_atendimento, :new.dt_diagnostico, :new.cd_doenca, :new.nm_usuario);
				end if;*/

				reg_integracao_p.nr_atendimento	:=	:new.nr_atendimento;

				nr_sequencia_w := :old.nr_atendimento||'�'||:new.nr_seq_interno||'�'||:old.cd_doenca;
				if	(:new.cd_doenca_superior is null) then
					gerar_int_padrao.gravar_integracao('82', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);
				end if;
				end;
			elsif	(:new.dt_inativacao is null) and
				(:new.cd_doenca_superior is null) then
				begin
				reg_integracao_p.ie_operacao	:=	'A';
				reg_integracao_p.ie_status		:=	'P';
				reg_integracao_p.nr_atendimento	:=	:new.nr_atendimento;

				nr_sequencia_w := :new.nr_atendimento||'�'||:new.nr_seq_interno||'�'||:new.cd_doenca;

				gerar_int_padrao.gravar_integracao('82', nr_sequencia_w, :new.nm_usuario, reg_integracao_p);
				end;
			end if;
		/* n�o dever� cair nesta condi��o pois o diagn�stico nao pode ser deletado do tasy */
		elsif	(deleting) then
			begin
			reg_integracao_p.ie_operacao	:=	'E';
			reg_integracao_p.ie_status		:=	'P';
			reg_integracao_p.nr_atendimento	:=	:old.nr_atendimento;

			nr_sequencia_w := :old.nr_atendimento||'�'||:old.nr_seq_interno||'�'||:old.cd_doenca;

			gerar_int_padrao.gravar_integracao('82', nr_sequencia_w, :old.nm_usuario, reg_integracao_p);
			end;
		end if;
		end;
	end if;
end if;

end diagnostico_doenca_ish_atual;
/


CREATE OR REPLACE TRIGGER TASY.Diagnostico_Doenca_Befins
BEFORE INSERT ON TASY.DIAGNOSTICO_DOENCA FOR EACH ROW
declare

ie_sexo_pf_w		varchar2(1);
ie_sexo_cid_w		varchar2(1);
ie_exige_lado_w		varchar2(10);
ie_GeraAtualDescCirurgia_w	varchar2(1):='N';
qt_dialise_w		number(10);
qt_pac_dialise_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_home_care_w	number(10);
ie_possui_home_care_w	varchar2(1);

cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
ie_gerar_proc_cih_w	varchar2(2);
cd_cid_primario_w 	varchar2(10);
nr_sequencia_w		number(5);
nr_seq_atepacu_w		number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	paciente_home_care a
	where	a.nr_atendimento_origem = :new.nr_atendimento
	and not exists (select	1
			from	paciente_hc_doenca b
			where	b.nr_seq_paciente = a.nr_sequencia
			and	b.cd_doenca_cid = :new.cd_doenca)
	order by 1;

begin

Obter_Param_Usuario(872,192,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_GeraAtualDescCirurgia_w);
Obter_Param_Usuario(916,938,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_gerar_proc_cih_w);
consistir_diagnostico_atend(:new.cd_doenca,:new.nr_atendimento);

/* obter sexo cid */
/*
select	nvl(ie_sexo,'A')
into	ie_sexo_cid_w
from	cid_doenca
where	cd_doenca_cid = :new.cd_doenca;
*/

/* obter sexo pf */
/*
select	nvl(obter_sexo_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),'C'),'I')
into	ie_sexo_pf_w
from	dual;
*/

/* consistir sexo cid x pf */
/*
if	(ie_sexo_cid_w <> 'A') and
	(ie_sexo_pf_w <> 'I') and
	(ie_sexo_cid_w <> ie_sexo_pf_w) then
	O sexo do cid e o sexo do paciente sao incompativeis, favor verificar!');
end if;
*/
select	max(ie_exige_lado)
into	ie_exige_lado_w
from	cid_doenca
where	cd_doenca_cid = :new.cd_doenca;
if	(ie_exige_lado_w	= 'S') and
	(:new.ie_lado is null) then
	--'Este CID exige a informacao do Lado. #@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(198329);
end if;

if	(:new.nr_seq_interno is null) then
	select	diagnostico_doenca_seq.nextval
	into	:new.nr_seq_interno
	from	dual;
end if;

select	count(*)
into	qt_dialise_w
from	HD_DIAG_DIALISE_PADRAO
where	cd_doenca_cid	= :new.cd_doenca;

if	(qt_dialise_w > 0) then

	select 	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_Atendimento;

	select 	count(*)
	into	qt_pac_dialise_w
	from	hd_pac_renal_cronico
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;

	if	(qt_pac_dialise_w > 0) then

		insert into HD_DIAGNOSTICO_DIALISE (	NR_SEQUENCIA,
							DT_ATUALIZACAO,
							NM_USUARIO,
							DT_ATUALIZACAO_NREC,
							NM_USUARIO_NREC,
							CD_PESSOA_FISICA,
							cd_doenca
							)
					values	(hd_diagnostico_dialise_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						cd_pessoa_fisica_w,
						:new.cd_doenca);

	end if;


end if;

if	(Obter_Funcao_Ativa = 872) and
	(ie_GeraAtualDescCirurgia_w = 'S') then
	gera_atualiz_cirur_descricao(:new.nr_cirurgia,:new.nr_seq_pepo,:new.cd_doenca,obter_perfil_ativo,Obter_Pessoa_Fisica_Usuario(:new.nm_usuario,'C'),:new.nm_usuario);
end if;

begin
select	'S'
into	ie_possui_home_care_w
from	paciente_home_care a
where	a.nr_atendimento_origem = :new.nr_atendimento
and not exists (select	1
		from	paciente_hc_doenca b
		where	b.nr_seq_paciente = a.nr_sequencia
		and	b.cd_doenca_cid = :new.cd_doenca)
and	rownum = 1;
exception
when others then
	ie_possui_home_care_w := 'N';
end;

if (ie_possui_home_care_w = 'S') then
	open C01;
	loop
	fetch C01 into
		nr_seq_home_care_w;
	exit when C01%notfound;
		begin

		insert into paciente_hc_doenca (
			nr_sequencia,
			nr_seq_paciente,
			dt_atualizacao,
			nm_usuario,
			cd_doenca_cid,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	paciente_hc_doenca_seq.nextval,
			nr_seq_home_care_w,
			sysdate,
			:new.nm_usuario,
			:new.cd_doenca,
			sysdate,
			:new.nm_usuario);

		end;
	end loop;
	close C01;
end if;

if	(ie_gerar_proc_cih_w = 'S') then

	begin

	select 	nvl(max(b.cd_procedimento),0),
		nvl(max(b.ie_origem_proced),0)
	into	cd_procedimento_w,
		ie_origem_proced_w
	from 	cid_doenca a,
		sus_procedimento_cid b
	where 	a.cd_doenca_cid = b.cd_doenca_cid
	and	a.cd_doenca_cid	= :new.cd_doenca;

	if	(cd_procedimento_w = 0) and
		(ie_origem_proced_w = 0) then

		select 	nvl(max(b.cd_procedimento),0),
			nvl(max(b.ie_origem_proced),0)
		into	cd_procedimento_w,
			ie_origem_proced_w
		from 	cid_doenca a,
			procedimento b
		where 	a.cd_doenca_cid = b.cd_doenca_cid
		and	a.cd_doenca_cid	= :new.cd_doenca;

	end if;

	if 	(cd_procedimento_w > 0)  and
		(ie_origem_proced_w > 0) then

		insert into PROCEDIMENTO_PACIENTE_CIH (
			nr_sequencia,
			NR_ATENDIMENTO,
			dt_atualizacao,
			nm_usuario,
			CD_CID_PRIMARIO,
			CD_PROCEDIMENTO,
			IE_ORIGEM_PROCED)
		values(	1,
			:new.NR_ATENDIMENTO,
			sysdate,
			:new.nm_usuario,
			:new.cd_doenca,
			cd_procedimento_w,
			ie_origem_proced_w);

	end if;

	exception
	when others then
		ie_gerar_proc_cih_w := 'N';
	end;
end if;

if (ie_gerar_proc_cih_w = 'C') then
	begin

	select	nvl(max(a.nr_sequencia),0),
			nvl(max(a.cd_cid_primario),''),
			nvl(max(a.cd_procedimento),0)
	into
		nr_sequencia_w,
		cd_cid_primario_w,
		cd_procedimento_w
	from 	procedimento_paciente_cih a
	where a.nr_atendimento = :new.NR_ATENDIMENTO;

	if 	(cd_cid_primario_w <> '') or
		(cd_procedimento_w <> 0)	then

		Update 	procedimento_paciente_cih
		set 	cd_cid_primario = :new.cd_doenca
		where 	nr_atendimento = :new.NR_ATENDIMENTO
		and 	nr_sequencia = nr_sequencia_w;

	else

		insert into PROCEDIMENTO_PACIENTE_CIH (
			nr_sequencia,
			NR_ATENDIMENTO,
			dt_atualizacao,
			nm_usuario,
			CD_CID_PRIMARIO)
		values(	1,
			:new.NR_ATENDIMENTO,
			sysdate,
			:new.nm_usuario,
			:new.cd_doenca);

	end if;

	exception
	when others then
		ie_gerar_proc_cih_w := 'N';
	end;

end if;

if	(:new.nr_seq_atepacu is null) then
	begin

	begin
	select	nvl(max(nr_seq_interno),0)
	into	nr_seq_atepacu_w
	from 	atend_paciente_unidade a
	where	a.nr_atendimento 		= :new.nr_atendimento
	  and 	nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999)	=
		(select max(nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999))
		from atend_paciente_unidade b
		where b.nr_atendimento 	= :new.nr_atendimento);

	if	(nr_seq_atepacu_w > 0) then
		:new.nr_seq_atepacu := nr_seq_atepacu_w;
	end if;

	exception when others then
		nr_seq_atepacu_w	:= null;
	end;

	end;
end if;

if	(nvl(pkg_i18n.get_user_locale, 'pt_BR')= 'ja_JP') and
	(nvl(:new.cd_setor_atendimento,0) = 0) then
	:new.cd_setor_atendimento := wheb_usuario_pck.get_cd_setor_atendimento;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_tp  after update ON TASY.DIAGNOSTICO_DOENCA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@DT_DIAGNOSTICO='||to_char(:old.DT_DIAGNOSTICO)||'#@#@CD_DOENCA='||to_char(:old.CD_DOENCA);gravar_log_alteracao(to_char(:old.DT_DIAGNOSTICO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_DIAGNOSTICO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_DIAGNOSTICO',ie_log_w,ds_w,'DIAGNOSTICO_DOENCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DIAGNOSTICO,1,4000),substr(:new.DS_DIAGNOSTICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIAGNOSTICO',ie_log_w,ds_w,'DIAGNOSTICO_DOENCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'DIAGNOSTICO_DOENCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'DIAGNOSTICO_DOENCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO_DOENCA,1,4000),substr(:new.IE_CLASSIFICACAO_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO_DOENCA',ie_log_w,ds_w,'DIAGNOSTICO_DOENCA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_doenca_pend_atual
after insert or update ON TASY.DIAGNOSTICO_DOENCA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_diag_medico_w	varchar2(10);
nr_seq_classif_diag_w		wl_worklist.nr_seq_classif_diag%type;
ie_usa_case_w				varchar2(1);
nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_lib_diag_medico)
into	ie_lib_diag_medico_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_lib_diag_medico_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'D';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XD';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_seq_interno, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;

if  (:new.dt_liberacao is not null) and (:old.dt_liberacao is null) then

	select	obter_uso_case(wheb_usuario_pck.get_nm_usuario)
	into	ie_usa_case_w
	from	dual;

	if (ie_usa_case_w = 'S') then
		select	nr_seq_episodio
		into	nr_seq_episodio_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		select	max(nvl(c.nr_sequencia,0))
		into	nr_seq_classif_diag_w
		from	classificacao_diagnostico c,
				medic_diagnostico_doenca d
		where	c.nr_sequencia	= d.nr_seq_classificacao
		and		d.nr_atendimento	= :new.nr_atendimento
		and		d.cd_doenca	= :new.cd_doenca
		and		d.dt_diagnostico	= :new.dt_diagnostico
		and		c.ie_situacao = 'A'
		and		d.ie_situacao = 'A';

	else

		select	max(nvl(c.nr_sequencia,0))
		into	nr_seq_classif_diag_w
		from	classificacao_diagnostico c,
				medic_diagnostico_doenca d
		where	c.nr_sequencia	= d.nr_seq_classificacao
		and		d.nr_atendimento in (select x.nr_atendimento from atendimento_paciente x, episodio_paciente y where x.nr_seq_episodio = y.nr_sequencia and y.nr_sequencia = nr_seq_episodio_w)
		and		d.cd_doenca	= :new.cd_doenca
		and		d.dt_diagnostico = :new.dt_diagnostico
		and		c.ie_situacao = 'A'
		and		d.ie_situacao = 'A';

	end if;

	wl_gerar_finalizar_tarefa('DG','F',:new.nr_atendimento,obter_pessoa_atendimento(:new.nr_atendimento,'C'),wheb_usuario_pck.get_nm_usuario,sysdate,'N',null,null,null,null,null,null,null,null,:new.nr_seq_interno,null,:new.ie_tipo_diagnostico,null,null,null,null,null,nr_seq_classif_diag_w);

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_doenca_aftdelete
after delete ON TASY.DIAGNOSTICO_DOENCA for each row
declare
	nr_seq_case_w		    episodio_paciente.nr_sequencia%type;
begin

/*if (billing_i18n_pck.obter_se_calcula_drg = 'S') then
	select nr_seq_episodio
	into   nr_seq_case_w
	from   atendimento_paciente
	where  nr_atendimento = :old.nr_atendimento;

	if(nr_seq_case_w is not null) then

		INATIVAR_EPISODIO_PACIENTE_DRG(nr_seq_case_w, null, null, :old.nm_usuario);
		/*update episodio_paciente_drg
		set    ie_situacao = 'I'
		where  nr_seq_episodio_paciente = nr_seq_case_w;*/
--	end if;
--end if;

delete
from 	medic_diagnostico_doenca
where 	nr_atendimento = :old.nr_atendimento
and	cd_doenca = :old.cd_doenca
and	dt_diagnostico = :old.dt_diagnostico;

end;
/


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_AFTINSERT
AFTER INSERT ON TASY.DIAGNOSTICO_DOENCA FOR EACH ROW
DECLARE

nr_sequencia_w			dados_envio_cross.nr_sequencia%type;
ie_integra_cross_w		varchar2(1);
cd_classif_setor_cross_w 	number(10);
qt_conv_int_cross_w		number(10);
cd_Setor_atendimento_w	setor_Atendimento.cd_setor_atendimento%type;
nr_Seq_atepacu_w		atend_paciente_unidade.nr_Seq_interno%type;

begin

nr_Seq_atepacu_w := obter_atepacu_paciente(:new.nr_atendimento, 'IA');

select	max(cd_setor_Atendimento)
into	cd_Setor_atendimento_w
from	atend_paciente_unidade
where	nr_Seq_interno	= nr_Seq_atepacu_w;

cd_classif_setor_cross_w := obter_classif_setor_cross(cd_Setor_atendimento_w);
qt_conv_int_cross_w := obter_conv_int_cross(:new.nr_atendimento);

select  nvl(max(ie_integra_cross),'N')
into	ie_integra_cross_w
from 	parametro_atendimento
where 	CD_ESTABELECIMENTO = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_integra_cross_w = 'S') and
	(obter_funcao_ativa = 916) and
	(cd_classif_setor_cross_w > 0) and
	(qt_conv_int_cross_w > 0) then
	--internacao
	gravar_integracao_cross(277, 'NR_ATENDIMENTO='|| :new.nr_atendimento || ';CD_ESTABELECIMENTO='|| wheb_usuario_pck.get_cd_estabelecimento || ';');
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_doenca_atual
before insert or update ON TASY.DIAGNOSTICO_DOENCA for each row
declare

nr_seq_regra_cid_pre_w 		number(10);
ie_situacao_w          		varchar2(10);
nr_seq_regra_cid_w     		number(10);
nr_seq_regra_proc_w    		number(10);
cd_procedimento_w      		number(15);
ie_origem_proced_w     		number(10);
ie_tipo_diagnostico_w  		char;
nr_sequencia_w         		cid_doenca_versao.nr_sequencia%type;
dt_entrada_w           		date;
nr_seq_case_w		   		episodio_paciente.nr_sequencia%type;
ie_diagnostico_admissao_w	varchar2(10);
nr_seq_classif_diag_w		wl_worklist.nr_seq_classif_diag%type;
ie_usa_case_w				varchar2(1);
nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;
cd_doenca_w					diagnostico_doenca.cd_doenca%type;
qt_tempo_da_w				wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_wl_da_w		wl_regra_item.nr_sequencia%type;
is_rule_tasklist_da_w		wl_item.nr_sequencia%type;

  cursor c01 is
    select ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(ap.dt_entrada) dt_entrada
    from atendimento_paciente ap
    where ap.nr_atendimento = :new.nr_atendimento;

  cursor c02 is
    select ie_data_cid
    from parametro_atendimento pa
    where pa.cd_estabelecimento = obter_estabelecimento_ativo();

  c02_w c02%rowtype;

  cursor c03(dt_entrada_w in DATE) is
    select nr_sequencia
    from   cid_doenca_versao
    where cd_doenca_cid = :new.cd_doenca
      and dt_entrada_w between dt_vigencia_inicial and dt_vigencia_final;

cursor c04 is
	select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DA'
							and		x.ie_situacao = 'A');
begin

:new.CD_CID_AUTOCOMPLETE := :new.CD_DOENCA;

if inserting then

   IF billing_i18n_pck.obter_se_versao_cid = 'S' THEN

      dt_entrada_w := null;
      c02_w        := null;

      --buscar a forma de atendimento
      open c02;
      fetch c02 into c02_w;
      close c02;

      if c02_w.ie_data_cid is not null then

        case c02_w.ie_data_cid
             when 1 then
             begin
               open c01;
               fetch c01 into dt_entrada_w;
               close c01;
             end;
             when 2 then
               dt_entrada_w := ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate);

        end case;

      end if;

      if dt_entrada_w is not null then
        open c03(dt_entrada_w);
        fetch c03 into :new.NR_SEQ_VERSAO_CID;
        close c03;
      end if;

   END IF;

end if;

if  (nvl(:old.DT_DIAGNOSTICO,sysdate+10) <> :new.DT_DIAGNOSTICO) and
  (:new.DT_DIAGNOSTICO is not null) then
  :new.ds_utc    := obter_data_utc(:new.DT_DIAGNOSTICO, 'HV');
end if;

if  (nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
  (:new.DT_LIBERACAO is not null) then
  :new.ds_utc_atualizacao  := obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if  (:new.dt_liberacao is not null) and
  (:old.dt_liberacao is null) then

  select  nvl(max(nr_sequencia),0)
  into  nr_seq_regra_cid_pre_w
  from  regra_cid_pre_agendamento
  where  cd_doenca   = :new.cd_doenca
  and  ie_situacao  = 'A';

  if  (nr_seq_regra_cid_pre_w > 0) then

    insert  into atend_cid_esp_pre_agend
      (
      nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      nr_atendimento,
      cd_especialidade,
      cd_doenca
      )
      (
      select  atend_cid_esp_pre_agend_seq.nextVal,
        sysdate,
        :new.nm_usuario,
        sysdate,
        :new.nm_usuario,
        :new.nr_atendimento,
        e.cd_especialidade,
        r.cd_doenca
      from  regra_cid_pre_agend_esp e,
        regra_cid_pre_agendamento r
      where  r.nr_sequencia    = nr_seq_regra_cid_pre_w
      and  e.nr_seq_regra_pre_agend= r.nr_sequencia
      and  e.ie_situacao     = 'A'
      );


  end if;

    send_diagnosis_integration(:new.nr_seq_interno,:new.nr_atendimento, :new.cd_doenca, :new.dt_diagnostico, :new.ie_classificacao_doenca,
						   :new.ie_situacao, :new.dt_liberacao, :new.dt_inativacao, :new.dt_atualizacao);

end if;
begin

if   (:new.ie_tipo_diag_classif is null) and
  (:new.nr_seq_classif_adic is not null) then

  select   max(ie_tipo_diagnostico)
  into  ie_tipo_diagnostico_w
  from  diagnostico_classif_adic
  where   nr_sequencia = :new.nr_seq_classif_adic;

  if (ie_tipo_diagnostico_w is not null) then
    :new.ie_tipo_diag_classif := ie_tipo_diagnostico_w;
  end if;

end if;

if  (:new.IE_TIPO_DIAGNOSTICO  <> nvl(:old.IE_TIPO_DIAGNOSTICO,'0')) then
  update  diagnostico_medico
  set  IE_TIPO_DIAGNOSTICO  = :new.IE_TIPO_DIAGNOSTICO
  where  nr_atendimento    = :new.nr_atendimento
  and  dt_diagnostico    = :new.dt_diagnostico;
end if;

exception
  when others then
  null;
end;

begin
  select  nr_sequencia
  into  nr_seq_regra_cid_w
  from  cid_permissao
  where  cd_cid_doenca = :new.cd_doenca
  and  rownum = 1;
exception
  when  no_data_found then
    nr_seq_regra_cid_w := 0;
end;

if  (nr_seq_regra_cid_w > 0) then

  cd_procedimento_w  := obter_proc_principal(:new.nr_atendimento, obter_convenio_atendimento(:new.nr_atendimento),
              obter_tipo_Atendimento(:new.nr_atendimento), null, 'CP');
  ie_origem_proced_w   := obter_proc_principal(:new.nr_atendimento, obter_convenio_atendimento(:new.nr_atendimento),
              obter_tipo_Atendimento(:new.nr_atendimento), null, 'IP');

  If  (nvl(cd_procedimento_w, 0) > 0) and
    (nvl(ie_origem_proced_w, 0) > 0) then

    begin
      select  nr_sequencia
      into  nr_seq_regra_proc_w
      from  cid_permissao_proc
      where  nr_seq_cid_permissao = nr_seq_regra_cid_w
      and  cd_procedimento = cd_procedimento_w
      and  ie_origem_proced = ie_origem_proced_w
      and  rownum = 1;
    exception
      when  no_data_found then
        nr_seq_regra_proc_w := 0;
      when  others then
        nr_seq_regra_proc_w := 1;
    end;

    If  (nr_seq_regra_proc_w = 0) then
      wheb_mensagem_pck.exibir_mensagem_abort(210243);
    end if;
  end if;
end if;

if (pkg_i18n.get_user_locale = 'ja_JP') then
if    (wheb_usuario_pck.get_ie_executar_trigger    = 'S')  then
begin
	select	count(a.nr_sequencia)
	into	is_rule_tasklist_da_w
	from	wl_item a,
		wl_regra_worklist b,
		wl_regra_item c
	where	a.nr_sequencia = b.nr_seq_item
	and	b.nr_sequencia = c.nr_seq_regra
	and	a.cd_categoria = 'DA'
	and	a.ie_situacao = 'A'
	and	c.ie_situacao = 'A';

if (:old.dt_liberacao is null and :new.dt_liberacao is not null and is_rule_tasklist_da_w > 0 ) then

	open c04;
	loop
	fetch c04 into
		qt_tempo_da_w,
		nr_seq_regra_wl_da_w;
	exit when c04%notfound;
		begin
			if	(nvl(qt_tempo_da_w,0) > 0) then

				wl_gerar_finalizar_tarefa('DA','I',:new.nr_atendimento,OBTER_PESSOA_ATENDIMENTO(:new.nr_atendimento,'C'),wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_da_w/24),'N',
										null,null,null,null,null,null,null,null,:new.nr_seq_interno ,nr_seq_regra_wl_da_w,:new.ie_tipo_diagnostico,null,null,null,null,null,
                    null,sysdate,null,null,null,null,null,null,:new.IE_CLASSIFICACAO_DOENCA);
			end if;
		end;
	end loop;
	close c04;
  end if;
end;
end if;

if (pkg_i18n.get_user_locale = 'de_DE') then
begin
	if (:new.ie_diag_princ_episodio = 'S') then
		:new.ie_classificacao_doenca := 'P';
	else
		:new.ie_classificacao_doenca := 'S';
	end if;

end;
end if;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

     update evolucao_paciente
     set dt_inativacao = sysdate,
     ie_situacao ='I',
     dt_atualizacao = sysdate ,
     nm_usuario = wheb_usuario_pck.get_nm_usuario,
     nm_usuario_inativacao =  wheb_usuario_pck.get_nm_usuario,
     DS_JUSTIFICATIVA = :new.DS_JUSTIFICATIVA
     where cd_evolucao = :new.cd_evolucao;

	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'DIAGNOSIS' and ie_stage = 1 and ie_soap_type = 'A';

end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_DIAGNOSTICO_DOENCA
before insert or update ON TASY.DIAGNOSTICO_DOENCA for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_SBIS_IN
before insert or update ON TASY.DIAGNOSTICO_DOENCA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       obter_pessoa_atendimento(:new.nr_atendimento,'C'),
                       obter_desc_expressao(316599),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,'TASY'));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       obter_pessoa_atendimento(:new.nr_atendimento,'C'),
                       obter_desc_expressao(316599),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,'TASY'));
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_DOENCA_SBIS_DEL
before delete ON TASY.DIAGNOSTICO_DOENCA for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 obter_pessoa_atendimento(:old.nr_atendimento,'C'),
									 obter_desc_expressao(316599),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario,'TASY'));


end;
/


ALTER TABLE TASY.DIAGNOSTICO_DOENCA ADD (
  CONSTRAINT DIADOEN_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, DT_DIAGNOSTICO, CD_DOENCA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          7M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT DIADOEN_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          488K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIAGNOSTICO_DOENCA ADD (
  CONSTRAINT DIADOEN_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT DIADOEN_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_ATEPACU) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO),
  CONSTRAINT MEDDECS_FK 
 FOREIGN KEY (CD_DISEASE_MEDIS) 
 REFERENCES TASY.MEDIS_DECEASE (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DIADOEN_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT DIADOEN_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_JPDOPSUX_FK 
 FOREIGN KEY (NR_SEQ_DIAGNOSIS_PREF_SUF) 
 REFERENCES TASY.JAPAN_DOENCA_PREF_SUF (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DIADOEN_CIDDOEN_FK2 
 FOREIGN KEY (CD_DOENCA_SUPERIOR) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DIADOEN_CIDDOVER_FK 
 FOREIGN KEY (NR_SEQ_VERSAO_CID) 
 REFERENCES TASY.CID_DOENCA_VERSAO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_CANLORE_FK 
 FOREIGN KEY (NR_SEQ_LOCO_REG) 
 REFERENCES TASY.CAN_LOCO_REGIONAL (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT DIADOEN_DIAMEDI_FK 
 FOREIGN KEY (NR_ATENDIMENTO, DT_DIAGNOSTICO) 
 REFERENCES TASY.DIAGNOSTICO_MEDICO (NR_ATENDIMENTO,DT_DIAGNOSTICO)
    ON DELETE CASCADE,
  CONSTRAINT DIADOEN_DIAGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DIAG) 
 REFERENCES TASY.DIAGNOSTICO_GRUPO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_DIAINTE_FK 
 FOREIGN KEY (NR_SEQ_DIAG_INTERNO) 
 REFERENCES TASY.DIAGNOSTICO_INTERNO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT DIADOEN_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DIADOEN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT DIADOEN_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_ETIDIAG_FK 
 FOREIGN KEY (NR_SEQ_ETIOLOGIA) 
 REFERENCES TASY.ETIOLOGIA_DIAGNOSTICO (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT DIADOEN_DIACLAA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ADIC) 
 REFERENCES TASY.DIAGNOSTICO_CLASSIF_ADIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIAGNOSTICO_DOENCA TO NIVEL_1;

