ALTER TABLE TASY.AGENDA_HORARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_HORARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_HORARIO
(
  CD_AGENDA                   NUMBER(10)        NOT NULL,
  DT_DIA_SEMANA               NUMBER(10)        NOT NULL,
  HR_INICIAL                  DATE              NOT NULL,
  HR_FINAL                    DATE              NOT NULL,
  NR_MINUTO_INTERVALO         NUMBER(10)        NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  HR_INICIAL_INTERVALO        DATE,
  HR_FINAL_INTERVALO          DATE,
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  DT_FINAL_VIGENCIA           DATE,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  NR_SEQ_MEDICO_EXEC          NUMBER(10),
  NR_SEQ_SALA                 NUMBER(10),
  DT_INICIO_VIGENCIA          DATE,
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_CLASSIF_AGENDA       NUMBER(10),
  CD_PROCEDIMENTO             NUMBER(15),
  IE_ORIGEM_PROCED            NUMBER(10),
  NR_SEQ_PROC_INTERNO         NUMBER(10),
  CD_ANESTESISTA              VARCHAR2(10 BYTE),
  IE_CARATER_CIRURGIA         VARCHAR2(1 BYTE),
  CD_PROCEDENCIA              NUMBER(5),
  IE_FREQUENCIA               VARCHAR2(15 BYTE),
  CD_CONVENIO                 NUMBER(5),
  CD_CATEGORIA                VARCHAR2(10 BYTE),
  NR_PRIORIDADE               NUMBER(10),
  NR_SEQ_TRANSPORTE           NUMBER(10),
  IE_FERIADO                  VARCHAR2(1 BYTE),
  IE_LADO                     VARCHAR2(1 BYTE),
  IE_FORMA_AGENDAMENTO        NUMBER(3),
  IE_SEMANA                   NUMBER(5),
  QT_IDADE_MIN                NUMBER(3),
  QT_IDADE_MAX                NUMBER(3),
  IE_TIPO_ATENDIMENTO         NUMBER(3),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF_PAC          NUMBER(10),
  QT_ENCAIXE                  NUMBER(3),
  CD_DEPARTAMENTO_MEDICO      NUMBER(6),
  DS_OBSERVACAO_ADIC          VARCHAR2(4000 BYTE),
  NR_SEQ_SCHEDULE_CYCLE       NUMBER(20),
  IE_DIA_SEMANA               NUMBER(1),
  IE_TURNO_AGENDAMENTO_GRUPO  VARCHAR2(1 BYTE),
  NR_SEQ_FILA_ESPERA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEHORA_AGENDA_FK_I ON TASY.AGENDA_HORARIO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_AGENMED_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_AGEPACL_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_CLASSIF_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_AGEPACL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_AGREGTR_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_AGREGTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_AGSACON_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_SALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_AGSACON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_CATCONV_FK_I ON TASY.AGENDA_HORARIO
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_CLASPES_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_CLASSIF_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_CLASPES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_CONBAMO_FK_I ON TASY.AGENDA_HORARIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_CONBAMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_DEPMED_FK_I ON TASY.AGENDA_HORARIO
(CD_DEPARTAMENTO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_FILESPS_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_FILA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_I1 ON TASY.AGENDA_HORARIO
(CD_AGENDA, DT_DIA_SEMANA, HR_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_I2 ON TASY.AGENDA_HORARIO
(CD_AGENDA, DT_DIA_SEMANA, QT_IDADE_MIN, 1)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_I3 ON TASY.AGENDA_HORARIO
(CD_AGENDA, DT_DIA_SEMANA, QT_IDADE_MAX, 1)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_PESFISI_FK_I ON TASY.AGENDA_HORARIO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_PESFISI_FK2_I ON TASY.AGENDA_HORARIO
(CD_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEHORA_PK ON TASY.AGENDA_HORARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHORA_PROCEDE_FK_I ON TASY.AGENDA_HORARIO
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_PROCEDI_FK_I ON TASY.AGENDA_HORARIO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_PROINTE_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHORA_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHORA_SCHCYWE_FK_I ON TASY.AGENDA_HORARIO
(NR_SEQ_SCHEDULE_CYCLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AGENDA_HORARIO_tp  after update ON TASY.AGENDA_HORARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_PAC,1,4000),substr(:new.NR_SEQ_CLASSIF_PAC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_PAC',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEMANA,1,4000),substr(:new.IE_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEMANA',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_INICIAL,'hh24:mi:ss'),to_char(:new.HR_INICIAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_INICIAL',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_FINAL,'hh24:mi:ss'),to_char(:new.HR_FINAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_FINAL',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MINUTO_INTERVALO,1,4000),substr(:new.NR_MINUTO_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MINUTO_INTERVALO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DIA_SEMANA,1,4000),substr(:new.DT_DIA_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'DT_DIA_SEMANA',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_INICIAL_INTERVALO,'hh24:mi:ss'),to_char(:new.HR_INICIAL_INTERVALO,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_INICIAL_INTERVALO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_FINAL_INTERVALO,'hh24:mi:ss'),to_char(:new.HR_FINAL_INTERVALO,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_FINAL_INTERVALO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DEPARTAMENTO_MEDICO,1,4000),substr(:new.CD_DEPARTAMENTO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DEPARTAMENTO_MEDICO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO_ADIC,1,4000),substr(:new.DS_OBSERVACAO_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO_ADIC',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FERIADO,1,4000),substr(:new.IE_FERIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FERIADO',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AGENDA,1,4000),substr(:new.CD_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_AGENDA',ie_log_w,ds_w,'AGENDA_HORARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_horario_update
before update on agenda_horario
for each row
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then


	if	(:new.cd_procedimento is null) and
		(:new.ie_origem_proced is not null) then
		:new.ie_origem_proced := null;
	end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Horario_Atual
before insert or update ON TASY.AGENDA_HORARIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
dt_final_w	date;
Cursor C01 is
	select	*
	from	agenda_horario
	where	hr_inicial > hr_final;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	cd_agenda	 = :new.cd_agenda;

for r_c01 in c01 loop
	begin
	dt_final_w	:= to_date(to_char( r_c01.hr_inicial,'dd/mm/yyyy') ||' '|| to_char( r_c01.hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

	update	agenda_horario
	set	hr_final = dt_final_w
	where	nr_sequencia = r_c01.nr_sequencia;
	end;
end loop;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Horario_Delete
before delete ON TASY.AGENDA_HORARIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
and	cd_agenda	 = :old.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Horario_After
after insert or update ON TASY.AGENDA_HORARIO for each row
declare
qt_regras_w		number(10);

begin

	select	count(1)
	into	qt_regras_w
	from	agenda_regra
	where	nr_seq_turno = :new.nr_sequencia;

	if (qt_regras_w > 0) then

		update 	agenda_regra
		set 	ie_dia_semana = :new.dt_dia_semana,
				hr_inicio = :new.hr_inicial,
				hr_fim = :new.hr_final,
				dt_inicio_vigencia = :new.dt_inicio_vigencia,
				dt_fim_vigencia = :new.dt_final_vigencia
		where 	nr_seq_turno = :new.nr_sequencia;

	end if;

end;
/


ALTER TABLE TASY.AGENDA_HORARIO ADD (
  CONSTRAINT AGEHORA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_HORARIO ADD (
  CONSTRAINT AGEHORA_SCHCYWE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULE_CYCLE) 
 REFERENCES TASY.SCHEDULE_CYCLE_WEEK (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_FILESPS_FK 
 FOREIGN KEY (NR_SEQ_FILA_ESPERA) 
 REFERENCES TASY.FILA_ESPERA_SENHA (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGEHORA_CLASPES_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PAC) 
 REFERENCES TASY.CLASSIF_PESSOA (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_AGSACON_FK 
 FOREIGN KEY (NR_SEQ_SALA) 
 REFERENCES TASY.AGENDA_SALA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_AGREGTR_FK 
 FOREIGN KEY (NR_SEQ_TRANSPORTE) 
 REFERENCES TASY.AGENDA_REGRA_TRANSPORTE (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_AGENMED_FK 
 FOREIGN KEY (NR_SEQ_MEDICO_EXEC) 
 REFERENCES TASY.AGENDA_MEDICO (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_AGEPACL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEHORA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGEHORA_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGEHORA_PESFISI_FK2 
 FOREIGN KEY (CD_ANESTESISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEHORA_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT AGEHORA_CONBAMO_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGEHORA_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AGEHORA_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO_MEDICO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO));

GRANT SELECT ON TASY.AGENDA_HORARIO TO NIVEL_1;

GRANT SELECT ON TASY.AGENDA_HORARIO TO ROBOCMTECNOLOGIA;

