ALTER TABLE TASY.PIX_COBRANCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PIX_COBRANCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PIX_COBRANCA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_TIPO_COBRANCA       VARCHAR2(10 BYTE),
  DS_TXID                VARCHAR2(255 BYTE),
  DS_CHAVE_PIX           VARCHAR2(255 BYTE),
  NR_SEQ_CONTA_BANCO     NUMBER(10),
  DS_STATUS              VARCHAR2(255 BYTE),
  DT_COBRANCA            DATE,
  DT_VENCIMENTO          DATE,
  NR_VALIDADE_APOS_VENC  NUMBER(10),
  NR_SEG_EXPIRACAO       NUMBER(10),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_CNPJ                VARCHAR2(14 BYTE),
  NR_ID_PAYLOAD          NUMBER(10),
  DS_URL_PAYLOAD         VARCHAR2(255 BYTE),
  DT_PAYLOAD             DATE,
  VL_ORIGINAL            NUMBER(15,2),
  IE_TIPO_MULTA          VARCHAR2(1 BYTE),
  VL_MULTA               NUMBER(15,2),
  IE_TIPO_JUROS          VARCHAR2(1 BYTE),
  VL_JUROS               NUMBER(15,2),
  IE_TIPO_ABATIMENTO     VARCHAR2(1 BYTE),
  VL_ABATIMENTO          NUMBER(15,2),
  IE_TIPO_DESCONTO       VARCHAR2(1 BYTE),
  VL_DESCONTO            NUMBER(15,2),
  VL_RECEBIDO            NUMBER(15,2),
  DS_MSG_PAGADOR         VARCHAR2(140 BYTE),
  DS_PIX_LINK            VARCHAR2(4000 BYTE),
  DS_QRCODE              VARCHAR2(4000 BYTE),
  DS_QRCODE_IMG          VARCHAR2(4000 BYTE),
  NR_REVISAO             NUMBER(10),
  NR_SEQ_TITULO_RECEBER  NUMBER(10),
  NR_ADIANTAMENTO        NUMBER(10),
  NR_SEQ_CAIXA_REC       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PIXCOBR_ADIANTA_FK_I ON TASY.PIX_COBRANCA
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PIXCOBR_BANESTA_FK_I ON TASY.PIX_COBRANCA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PIXCOBR_CAIREC_FK_I ON TASY.PIX_COBRANCA
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PIXCOBR_PESFISI_FK_I ON TASY.PIX_COBRANCA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PIXCOBR_PESJURI_FK_I ON TASY.PIX_COBRANCA
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PIXCOBR_PK ON TASY.PIX_COBRANCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PIXCOBR_TITRECE_FK_I ON TASY.PIX_COBRANCA
(NR_SEQ_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PIX_COBRANCA_tp  after update ON TASY.PIX_COBRANCA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_TIPO_COBRANCA,1,4000),substr(:new.DS_TIPO_COBRANCA,1,4000),:new.nm_usuario,nr_seq_w,'DS_TIPO_COBRANCA',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VENCIMENTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CHAVE_PIX,1,4000),substr(:new.DS_CHAVE_PIX,1,4000),:new.nm_usuario,nr_seq_w,'DS_CHAVE_PIX',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_BANCO,1,4000),substr(:new.NR_SEQ_CONTA_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_BANCO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_STATUS,1,4000),substr(:new.DS_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'DS_STATUS',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_VALIDADE_APOS_VENC,1,4000),substr(:new.NR_VALIDADE_APOS_VENC,1,4000),:new.nm_usuario,nr_seq_w,'NR_VALIDADE_APOS_VENC',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ,1,4000),substr(:new.CD_CNPJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ID_PAYLOAD,1,4000),substr(:new.NR_ID_PAYLOAD,1,4000),:new.nm_usuario,nr_seq_w,'NR_ID_PAYLOAD',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_URL_PAYLOAD,1,4000),substr(:new.DS_URL_PAYLOAD,1,4000),:new.nm_usuario,nr_seq_w,'DS_URL_PAYLOAD',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ORIGINAL,1,4000),substr(:new.VL_ORIGINAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_ORIGINAL',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MULTA,1,4000),substr(:new.IE_TIPO_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MULTA',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MULTA,1,4000),substr(:new.VL_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_MULTA',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_JUROS,1,4000),substr(:new.IE_TIPO_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_JUROS',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_JUROS,1,4000),substr(:new.VL_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'VL_JUROS',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ABATIMENTO,1,4000),substr(:new.IE_TIPO_ABATIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ABATIMENTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ABATIMENTO,1,4000),substr(:new.VL_ABATIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_ABATIMENTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESCONTO,1,4000),substr(:new.IE_TIPO_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESCONTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESCONTO,1,4000),substr(:new.VL_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_RECEBIDO,1,4000),substr(:new.VL_RECEBIDO,1,4000),:new.nm_usuario,nr_seq_w,'VL_RECEBIDO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MSG_PAGADOR,1,4000),substr(:new.DS_MSG_PAGADOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_MSG_PAGADOR',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PIX_LINK,1,4000),substr(:new.DS_PIX_LINK,1,4000),:new.nm_usuario,nr_seq_w,'DS_PIX_LINK',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_QRCODE_IMG,1,4000),substr(:new.DS_QRCODE_IMG,1,4000),:new.nm_usuario,nr_seq_w,'DS_QRCODE_IMG',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REVISAO,1,4000),substr(:new.NR_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_REVISAO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TITULO_RECEBER,1,4000),substr(:new.NR_SEQ_TITULO_RECEBER,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TITULO_RECEBER',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ADIANTAMENTO,1,4000),substr(:new.NR_ADIANTAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ADIANTAMENTO',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CAIXA_REC,1,4000),substr(:new.NR_SEQ_CAIXA_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CAIXA_REC',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TXID,1,4000),substr(:new.DS_TXID,1,4000),:new.nm_usuario,nr_seq_w,'DS_TXID',ie_log_w,ds_w,'PIX_COBRANCA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PIX_COBRANCA ADD (
  CONSTRAINT PIXCOBR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PIX_COBRANCA ADD (
  CONSTRAINT PIXCOBR_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT PIXCOBR_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT PIXCOBR_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT PIXCOBR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PIXCOBR_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PIXCOBR_TITRECE_FK 
 FOREIGN KEY (NR_SEQ_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO));

