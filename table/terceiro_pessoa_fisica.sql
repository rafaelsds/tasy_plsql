ALTER TABLE TASY.TERCEIRO_PESSOA_FISICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TERCEIRO_PESSOA_FISICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TERCEIRO_PESSOA_FISICA
(
  NR_SEQ_TERCEIRO      NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  IE_RESP_TERCEIRO     VARCHAR2(1 BYTE),
  CD_BARRAS            VARCHAR2(40 BYTE),
  IE_REPASSE           VARCHAR2(1 BYTE),
  IE_RECEB_PROT_ONCO   VARCHAR2(1 BYTE),
  VL_PISO_REP_MEDICO   NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TERPEFI_PESFISI_FK_I ON TASY.TERCEIRO_PESSOA_FISICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TERPEFI_PK ON TASY.TERCEIRO_PESSOA_FISICA
(NR_SEQ_TERCEIRO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERPEFI_TERCEIR_FK_I ON TASY.TERCEIRO_PESSOA_FISICA
(NR_SEQ_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERPEFI_TERCEIR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TERCEIRO_PESSOA_FISICA ADD (
  CONSTRAINT TERPEFI_PK
 PRIMARY KEY
 (NR_SEQ_TERCEIRO, CD_PESSOA_FISICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TERCEIRO_PESSOA_FISICA ADD (
  CONSTRAINT TERPEFI_PESFISI_FK_I 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT TERPEFI_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TERCEIRO_PESSOA_FISICA TO NIVEL_1;

