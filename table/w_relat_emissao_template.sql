ALTER TABLE TASY.W_RELAT_EMISSAO_TEMPLATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_RELAT_EMISSAO_TEMPLATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RELAT_EMISSAO_TEMPLATE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  DS_TEMPLATE       LONG,
  DT_REGISTRO       DATE,
  IE_TIPO_EVOLUCAO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WREEMTE_PK ON TASY.W_RELAT_EMISSAO_TEMPLATE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.W_RELAT_EMISSAO_TEMPLATE_tp  after update ON TASY.W_RELAT_EMISSAO_TEMPLATE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_EVOLUCAO,1,4000),substr(:new.IE_TIPO_EVOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_EVOLUCAO',ie_log_w,ds_w,'W_RELAT_EMISSAO_TEMPLATE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.W_RELAT_EMISSAO_TEMPLATE ADD (
  CONSTRAINT WREEMTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_RELAT_EMISSAO_TEMPLATE TO NIVEL_1;

