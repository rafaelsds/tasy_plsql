ALTER TABLE TASY.ESCALA_PELOD_II
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PELOD_II CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PELOD_II
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  NR_HORA                   NUMBER(2),
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  DT_LIBERACAO              DATE,
  QT_REL_PAO2_FIO2          NUMBER(3),
  QT_PTO_REL_PAO2_FIO2      NUMBER(3)           NOT NULL,
  QT_FREQ_CARDIACA          NUMBER(3),
  QT_PTO_FREQ_CARDIACA      NUMBER(3)           NOT NULL,
  QT_PACO2                  NUMBER(15,2),
  QT_PTO_PACO2              NUMBER(3)           NOT NULL,
  IE_VENTILACAO_MECANICA    VARCHAR2(1 BYTE),
  QT_PTO_VENT_MECANICA      NUMBER(3)           NOT NULL,
  QT_GLASGOW                NUMBER(2),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  QT_PTO_GLASGOW            NUMBER(3)           NOT NULL,
  CD_PERFIL_ATIVO           NUMBER(5),
  IE_PUPILA_FIXA            VARCHAR2(1 BYTE),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  QT_PTO_PUPILA_FIXA        NUMBER(3)           NOT NULL,
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  QT_GLOBULOS_BRANCOS       NUMBER(15,1),
  QT_PTO_GLOB_BRANCO        NUMBER(3)           NOT NULL,
  QT_CREATININA_SERICA      NUMBER(15,2),
  QT_PTO_CREATININA_SERICA  NUMBER(3)           NOT NULL,
  QT_PAM                    NUMBER(3),
  QT_PTO_PAM                NUMBER(3)           NOT NULL,
  QT_PLAQUETAS              NUMBER(15),
  QT_PTO_PLAQUETAS          NUMBER(3)           NOT NULL,
  QT_PTO_CARDIO             NUMBER(3)           NOT NULL,
  QT_PTO_SNC                NUMBER(3)           NOT NULL,
  QT_PTO_RENAL              NUMBER(3)           NOT NULL,
  QT_PTO_HEMATOLOGICA       NUMBER(3)           NOT NULL,
  QT_PTO_PULMONAR           NUMBER(3)           NOT NULL,
  QT_LACTATO_SERICO         NUMBER(4,2),
  QT_PONTUACAO              NUMBER(3)           NOT NULL,
  QT_PTO_LACTATO_SERICO     NUMBER(4,2)         NOT NULL,
  PR_MORTALIDADE            NUMBER(7,1)         NOT NULL,
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESPELII_ATEPACI_FK_I ON TASY.ESCALA_PELOD_II
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESPELII_PERFIL_FK_I ON TASY.ESCALA_PELOD_II
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESPELII_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESPELII_PESFISI_FK_I ON TASY.ESCALA_PELOD_II
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESPELII_PK ON TASY.ESCALA_PELOD_II
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESPELII_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_pelod2_atual
before update or insert ON TASY.ESCALA_PELOD_II for each row
declare

cd_pessoa_fisica_w	varchar2(10);
nr_idade_dias_w		number(10,0);
nr_idade_meses_w	number(10,0);
logit_w			number(15,4):= 0;
qt_reg_w	number(1);

begin

if	(:new.nr_hora is null) or
	(:new.dt_avaliacao <> :old.dt_avaliacao) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_avaliacao,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

/* Obter pessoa f�sica */
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

/* Obter a idade em meses */
select	nvl(obter_idade_pf(cd_pessoa_fisica_w, :new.dt_avaliacao, 'M'),0)
into	nr_idade_meses_w
from	dual;

/* Pontua��o da rela��o Pao2/Fio2*/
if	(:new.qt_rel_pao2_fio2 is null) then
	:new.qt_pto_rel_pao2_fio2 := 0;
elsif	(:new.qt_rel_pao2_fio2 > 60) then
	:new.qt_pto_rel_pao2_fio2 := 0;
else
	:new.qt_pto_rel_pao2_fio2 := 2;
end if;

/* Pontua��o do PaCO2*/
if	(:new.qt_paco2 is null) then
	:new.qt_pto_paco2 := 0;
elsif	(:new.qt_paco2 <= 58) then
	:new.qt_pto_paco2 := 0;
elsif	(:new.qt_paco2 > 58) and (:new.qt_paco2 < 95) then
	:new.qt_pto_paco2 := 1;
elsif	(:new.qt_paco2 > 94) then
	:new.qt_pto_paco2 := 3;
end if;

/* Pontua��o da ventila��o mec�nica */
if	(:new.ie_ventilacao_mecanica = 'S') then
	:new.qt_pto_vent_mecanica := 3;
elsif	(:new.ie_ventilacao_mecanica = 'N') then
	:new.qt_pto_vent_mecanica := 0;
end if;

:new.qt_pto_pulmonar := :new.qt_pto_paco2 + :new.qt_pto_vent_mecanica + :new.qt_pto_rel_pao2_fio2;

/* Pontua��o da Hematol�gica */
if	(:new.qt_globulos_brancos > 2) then
	:new.qt_pto_glob_branco := 0;
else
	:new.qt_pto_glob_branco := 2;
end if;

if	(:new.qt_plaquetas >= 142) then
	:new.qt_pto_plaquetas := 0;
elsif	(:new.qt_plaquetas <= 141) and
	(:new.qt_plaquetas >= 71) then
	:new.qt_pto_plaquetas := 1;
else
	:new.qt_pto_plaquetas := 2;
end if;

:new.qt_pto_hematologica := :new.qt_pto_plaquetas + :new.qt_pto_glob_branco;

/* Pontua��o da Neurologica */
:new.qt_pto_glasgow := 0;

if 	(:new.qt_glasgow >= 3) and (:new.qt_glasgow <= 4) then
	:new.qt_pto_glasgow := 4;
elsif 	(:new.qt_glasgow >= 5) and (:new.qt_glasgow <= 10) then
	:new.qt_pto_glasgow := 1;
end if;

if	(:new.ie_pupila_fixa = 'S') then
	:new.qt_pto_pupila_fixa := 5;
elsif	(:new.ie_pupila_fixa = 'N') then
	:new.qt_pto_pupila_fixa := 0;
end if;

:new.qt_pto_snc := :new.qt_pto_glasgow + :new.qt_pto_pupila_fixa;

/* Pontua��o da Cardiovascular */
:new.qt_pto_lactato_serico := 0;

if	(:new.qt_lactato_serico >= 5) and (:new.qt_lactato_serico <= 10.9) then
	:new.qt_pto_lactato_serico := 1;

end if;

if 	(nr_idade_meses_w < 0) then

	if	(:new.qt_pam <= 16) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 17) and (:new.qt_pam <= 30) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 31) and (:new.qt_pam <= 45) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 46) then
		:new.qt_pto_pam := 0;
	end if;

elsif 	(nr_idade_meses_w >= 1) and
		(nr_idade_meses_w <= 11) then

	if	(:new.qt_pam <= 24) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 25) and (:new.qt_pam <= 38) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 39) and (:new.qt_pam <= 54) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 55) then
		:new.qt_pto_pam := 0;
	end if;

elsif 	(nr_idade_meses_w >= 12) and
		(nr_idade_meses_w <= 23) then

	if	(:new.qt_pam <= 30) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 31) and (:new.qt_pam <= 43) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 44) and (:new.qt_pam <= 59) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 60) then
		:new.qt_pto_pam := 0;
	end if;

elsif 	(nr_idade_meses_w >= 24) and
		(nr_idade_meses_w <= 59) then

	if	(:new.qt_pam <= 31) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 32) and (:new.qt_pam <= 44) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 46) and (:new.qt_pam <= 61) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 62) then
		:new.qt_pto_pam := 0;
	end if;

elsif 	(nr_idade_meses_w >= 60) and
		(nr_idade_meses_w <= 143) then

	if	(:new.qt_pam <= 35) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 36) and (:new.qt_pam <= 48) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 49) and (:new.qt_pam <= 64) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 65) then
		:new.qt_pto_pam := 0;
	end if;

elsif 	(nr_idade_meses_w >= 144) then

	if	(:new.qt_pam <= 37) then
		:new.qt_pto_pam := 6;
	elsif	(:new.qt_pam >= 38) and (:new.qt_pam <= 51) then
		:new.qt_pto_pam := 3;
	elsif	(:new.qt_pam >= 52) and (:new.qt_pam <= 66) then
		:new.qt_pto_pam := 2;
	elsif 	(:new.qt_pam >= 67) then
		:new.qt_pto_pam := 0;
	end if;
end if;

:new.qt_pto_cardio := :new.qt_pto_pam + :new.qt_pto_lactato_serico;

/* Pontua��o da Renal */
if	(:new.qt_creatinina_serica is null) then
	:new.qt_pto_creatinina_serica := 0;
else
	if	(nr_idade_meses_w <= 0) then

		if	(:new.qt_creatinina_serica <= 69) then
			:new.qt_pto_creatinina_serica := 0;
		else
			:new.qt_pto_creatinina_serica := 2;
		end if;

	elsif	(nr_idade_meses_w >= 1) and
			(nr_idade_meses_w <= 11) then

			if	(:new.qt_creatinina_serica <= 22) then
				:new.qt_pto_creatinina_serica := 0;
			else
				:new.qt_pto_creatinina_serica := 2;
			end if;

	elsif	(nr_idade_meses_w >= 12) and
			(nr_idade_meses_w <= 23) then

			if	(:new.qt_creatinina_serica <= 34) then
				:new.qt_pto_creatinina_serica := 0;
			else
				:new.qt_pto_creatinina_serica := 2;
			end if;

	elsif	(nr_idade_meses_w >= 24) and
			(nr_idade_meses_w <= 59) then

			if	(:new.qt_creatinina_serica <= 50) then
				:new.qt_pto_creatinina_serica := 0;
			else
				:new.qt_pto_creatinina_serica := 2;
			end if;

	elsif	(nr_idade_meses_w >= 60) and
			(nr_idade_meses_w <= 143) then

			if	(:new.qt_creatinina_serica <= 58) then
				:new.qt_pto_creatinina_serica := 0;
			else
				:new.qt_pto_creatinina_serica := 2;
			end if;

	elsif	(nr_idade_meses_w >= 144) then

			if	(:new.qt_creatinina_serica <= 92) then
				:new.qt_pto_creatinina_serica := 0;
			else
				:new.qt_pto_creatinina_serica := 2;
			end if;
	end if;
end if;

:new.qt_pto_renal := :new.qt_pto_creatinina_serica;

/* Pontua��o total */
:new.qt_pontuacao :=	nvl(:new.qt_pto_renal,0)	+	nvl(:new.qt_pto_hematologica,0)	+
			nvl(:new.qt_pto_cardio,0)	+	nvl(:new.qt_pto_pulmonar,0)		+
			nvl(:new.qt_pto_snc,0);

/* Percentual de mortalidade -7.7631 */
logit_w := -6.61 + 0.47 * (:new.qt_pontuacao);

:new.pr_mortalidade	:= trunc(dividir(exp(logit_w), 1 + exp(logit_w)) * 100,1);

<<Final>>
qt_reg_w := 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_PELOD_II_tp  after update ON TASY.ESCALA_PELOD_II FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_FREQ_CARDIACA,1,4000),substr(:new.QT_FREQ_CARDIACA,1,4000),:new.nm_usuario,nr_seq_w,'QT_FREQ_CARDIACA',ie_log_w,ds_w,'ESCALA_PELOD_II',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ESCALA_PELOD_II ADD (
  CONSTRAINT ESPELII_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PELOD_II ADD (
  CONSTRAINT ESPELII_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESPELII_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ESPELII_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_PELOD_II TO NIVEL_1;

