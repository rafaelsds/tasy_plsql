ALTER TABLE TASY.TUSS_MATERIAL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TUSS_MATERIAL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TUSS_MATERIAL_ITEM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CARGA_TUSS        NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_MATERIAL              VARCHAR2(30 BYTE),
  DS_MATERIAL              VARCHAR2(255 BYTE),
  CD_REF_FABRICANTE        VARCHAR2(255 BYTE),
  DS_FABRICANTE            VARCHAR2(255 BYTE),
  DT_INICIO_VIGENCIA       DATE,
  DT_FINAL_VIGENCIA        DATE,
  DS_APRESENTACAO          VARCHAR2(255 BYTE),
  DS_LABORATORIO           VARCHAR2(255 BYTE),
  IE_TIPO_ITEM             NUMBER(1)            NOT NULL,
  CD_MATERIAL_TUSS         NUMBER(15),
  NR_REGISTRO_ANVISA       VARCHAR2(60 BYTE),
  DS_GRAU_RISCO            VARCHAR2(20 BYTE),
  DT_FIM_IMPLANTACAO       DATE,
  DS_GRUPO_FARMACOLOGICO   VARCHAR2(255 BYTE),
  DS_CLASSE_FARMACOLOGICA  VARCHAR2(255 BYTE),
  DT_INICIO_VIGENCIA_REF   DATE,
  DT_FIM_VIGENCIA_REF      DATE,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NM_TECNICO               VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TUSSITE_I1 ON TASY.TUSS_MATERIAL_ITEM
(NR_SEQ_CARGA_TUSS, CD_MATERIAL_TUSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TUSSITE_I2 ON TASY.TUSS_MATERIAL_ITEM
(CD_MATERIAL_TUSS, DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TUSSITE_PK ON TASY.TUSS_MATERIAL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TUSSITE_TUSSMAT_FK_I ON TASY.TUSS_MATERIAL_ITEM
(NR_SEQ_CARGA_TUSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_dt_ref_tuss_material_item
before insert or update ON TASY.TUSS_MATERIAL_ITEM for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_final_vigencia, 'F');

end pls_dt_ref_tuss_material_item;
/


ALTER TABLE TASY.TUSS_MATERIAL_ITEM ADD (
  CONSTRAINT TUSSITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TUSS_MATERIAL_ITEM ADD (
  CONSTRAINT TUSSITE_TUSSMAT_FK 
 FOREIGN KEY (NR_SEQ_CARGA_TUSS) 
 REFERENCES TASY.TUSS_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.TUSS_MATERIAL_ITEM TO NIVEL_1;

