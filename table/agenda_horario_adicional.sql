ALTER TABLE TASY.AGENDA_HORARIO_ADICIONAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_HORARIO_ADICIONAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_HORARIO_ADICIONAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_AGENDA            NUMBER(10)               NOT NULL,
  NR_MINUTO_DURACAO    NUMBER(10)               NOT NULL,
  DT_HORARIO           DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGHORAD_AGENDA_FK_I ON TASY.AGENDA_HORARIO_ADICIONAL
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGHORAD_PK ON TASY.AGENDA_HORARIO_ADICIONAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGHORAD_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Agenda_Horario_Adic_Delete
before delete ON TASY.AGENDA_HORARIO_ADICIONAL FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= trunc(sysdate)
and	dt_agenda between trunc(:old.dt_horario) and trunc(:old.dt_horario) + 86399 / 86400
and	cd_agenda	 = :old.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Horario_Adicional_Atual
before insert or update ON TASY.AGENDA_HORARIO_ADICIONAL FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= trunc(sysdate)
and	dt_agenda between trunc(:new.dt_horario) and trunc(:new.dt_horario) + 86399 / 86400
and	cd_agenda	 = :new.cd_agenda;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.AGENDA_HORARIO_ADICIONAL ADD (
  CONSTRAINT AGHORAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_HORARIO_ADICIONAL ADD (
  CONSTRAINT AGHORAD_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA));

GRANT SELECT ON TASY.AGENDA_HORARIO_ADICIONAL TO NIVEL_1;

