ALTER TABLE TASY.CADSUS_LOG_RETORNO_WS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CADSUS_LOG_RETORNO_WS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CADSUS_LOG_RETORNO_WS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_CARTAO_NAC_SUS        VARCHAR2(20 BYTE),
  NM_PESSOA_FISICA         VARCHAR2(255 BYTE),
  NM_PESSOA_MAE            VARCHAR2(255 BYTE),
  IE_SEXO_PACIENTE         VARCHAR2(1 BYTE),
  DT_NASCIMENTO            DATE,
  CD_MUNICIPIO_IBGE_NASC   VARCHAR2(6 BYTE),
  NR_CPF                   VARCHAR2(11 BYTE),
  DS_MUNICIPIO_IBGE        VARCHAR2(255 BYTE),
  IE_SITUACAO_CNS          VARCHAR2(1 BYTE),
  NM_SOCIAL                VARCHAR2(200 BYTE),
  NM_PESSOA_PAI            VARCHAR2(255 BYTE),
  NR_SEQ_COR_PELE          NUMBER(10),
  NR_SEQ_ETNIA             NUMBER(10),
  CD_PAIS                  NUMBER(5),
  DT_NATURALIZACAO_PF      DATE,
  DT_ENTRADA_PAIS          DATE,
  NR_PORTARIA_NAT          VARCHAR2(16 BYTE),
  CD_NACIONALIDADE         VARCHAR2(8 BYTE),
  NR_IDENTIDADE            VARCHAR2(15 BYTE),
  DT_EMISSAO_RG            DATE,
  COMPL_CD_MUNICIPIO_IBGE  VARCHAR2(6 BYTE),
  UF_EMISSAO_RG            VARCHAR2(2 BYTE),
  CD_ORGAO_EMISSOR_RG      VARCHAR2(3 BYTE),
  NR_CTPS                  NUMBER(10),
  NR_SERIE_CTPS            VARCHAR2(10 BYTE),
  DT_EMISSAO_CTPS          DATE,
  NR_PIS_PASEP             VARCHAR2(11 BYTE),
  NR_RIC                   VARCHAR2(11 BYTE),
  CD_DECLARACAO_NASC_VIVO  VARCHAR2(30 BYTE),
  NR_DDI_CELULAR           VARCHAR2(3 BYTE),
  NR_DDD_CELULAR           VARCHAR2(3 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  NR_CELULAR_NUMERO        VARCHAR2(20 BYTE),
  DS_ENDERECO              VARCHAR2(100 BYTE),
  NR_ENDERECO              NUMBER(5),
  DS_COMPLEMENTO           VARCHAR2(40 BYTE),
  DS_BAIRRO                VARCHAR2(80 BYTE),
  SG_ESTADO                VARCHAR2(15 BYTE),
  CD_CEP                   VARCHAR2(15 BYTE),
  DS_EMAIL                 VARCHAR2(255 BYTE),
  DT_OBITO                 DATE,
  DS_OBSERVACAO_OBITO      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CADSUSLOGR_PK ON TASY.CADSUS_LOG_RETORNO_WS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CADSUS_LOG_RETORNO_WS ADD (
  CONSTRAINT CADSUSLOGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CADSUS_LOG_RETORNO_WS TO NIVEL_1;

