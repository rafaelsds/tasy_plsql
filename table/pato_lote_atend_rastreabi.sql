ALTER TABLE TASY.PATO_LOTE_ATEND_RASTREABI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATO_LOTE_ATEND_RASTREABI CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATO_LOTE_ATEND_RASTREABI
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ATENDIMENTO       NUMBER(10),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAATRAS_PALORAS_FK_I ON TASY.PATO_LOTE_ATEND_RASTREABI
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAATRAS_PALORAS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PAATRAS_PK ON TASY.PATO_LOTE_ATEND_RASTREABI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAATRAS_PK
  MONITORING USAGE;


ALTER TABLE TASY.PATO_LOTE_ATEND_RASTREABI ADD (
  CONSTRAINT PAATRAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PATO_LOTE_ATEND_RASTREABI ADD (
  CONSTRAINT PAATRAS_PALORAS_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PATO_LOTE_RASTREABILIDADE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PATO_LOTE_ATEND_RASTREABI TO NIVEL_1;

