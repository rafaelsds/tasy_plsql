ALTER TABLE TASY.SUS_REGRA_EXP_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_REGRA_EXP_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_REGRA_EXP_PREST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10),
  NR_SEQ_SUBGRUPO      NUMBER(10),
  CD_CGC_EXEC          VARCHAR2(14 BYTE),
  CD_CGC_EXEC_EXP      VARCHAR2(14 BYTE),
  NR_SEQ_FORMA_ORG     NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  IE_AIH               VARCHAR2(1 BYTE),
  IE_BPA               VARCHAR2(1 BYTE),
  IE_APAC              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSREPT_PESJURI_FK_I ON TASY.SUS_REGRA_EXP_PREST
(CD_CGC_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSREPT_PESJURI_FK2_I ON TASY.SUS_REGRA_EXP_PREST
(CD_CGC_EXEC_EXP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUSREPT_PK ON TASY.SUS_REGRA_EXP_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSREPT_PROCEDI_FK_I ON TASY.SUS_REGRA_EXP_PREST
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSREPT_SUSFORG_FK_I ON TASY.SUS_REGRA_EXP_PREST
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSREPT_SUSGRUP_FK_I ON TASY.SUS_REGRA_EXP_PREST
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSREPT_SUSSUBG_FK_I ON TASY.SUS_REGRA_EXP_PREST
(NR_SEQ_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSREPT_SUSSUBG_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_REGRA_EXP_PREST ADD (
  CONSTRAINT SUSREPT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_REGRA_EXP_PREST ADD (
  CONSTRAINT SUSREPT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSREPT_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT SUSREPT_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA),
  CONSTRAINT SUSREPT_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT SUSREPT_PESJURI_FK 
 FOREIGN KEY (CD_CGC_EXEC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SUSREPT_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_EXEC_EXP) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.SUS_REGRA_EXP_PREST TO NIVEL_1;

