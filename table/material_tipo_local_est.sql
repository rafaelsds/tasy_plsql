ALTER TABLE TASY.MATERIAL_TIPO_LOCAL_EST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_TIPO_LOCAL_EST CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_TIPO_LOCAL_EST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  IE_PRESTADOR         VARCHAR2(1 BYTE)         NOT NULL,
  IE_OPERADORA         VARCHAR2(1 BYTE)         NOT NULL,
  IE_FARMACIA          VARCHAR2(1 BYTE)         NOT NULL,
  IE_DISTRIBUIDORA     VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MATTLES_PK ON TASY.MATERIAL_TIPO_LOCAL_EST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTLES_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATTLES_UK ON TASY.MATERIAL_TIPO_LOCAL_EST
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_tipo_local_est_update
before insert or update ON TASY.MATERIAL_TIPO_LOCAL_EST for each row
declare
qt_registro_w		number(5);

begin

/* N�o considerar quando for atualizado pela exclus�o da PLS_MATERIAL - MATERIAL_ESTAB */
if	(:new.ie_operadora <> 'X') and
	(:new.ie_prestador <> 'X') and
	(:new.ie_farmacia <> 'X') then
	select	count(*)
	into	qt_registro_w
	from	material_estab a
	where	cd_material = :old.cd_material
	and	exists (select	1
			from	parametro_gestao_material y,
				estabelecimento x
			where	x.nr_seq_unid_neg	= y.nr_seq_unidade_hosp
			and	x.cd_estabelecimento	= a.cd_estabelecimento);

	if	(:new.ie_prestador = 'N') and
		(qt_registro_w > 0) then
		--O campo "Prestador (hospitalar)" n�o pode ser desmarcado pois h� cadastro na atividade correspondente.
		wheb_mensagem_pck.exibir_mensagem_abort(267088);
	end if;

	select	count(*)
	into	qt_registro_w
	from	pls_material
	where	cd_material = :old.cd_material;

	if	(:new.ie_operadora = 'N') and
		(qt_registro_w > 0) then
		--O campo "Operadora" n�o pode ser desmarcado pois h� cadastro  na atividade correspondente.
		wheb_mensagem_pck.exibir_mensagem_abort(267089);
	end if;

	select	count(*)
	into	qt_registro_w
	from	material_estab a
	where	cd_material = :old.cd_material
	and	exists (select	1
			from	parametro_gestao_material y,
				estabelecimento x
			where	x.nr_seq_unid_neg	= y.nr_seq_unidade_farm
			and	x.cd_estabelecimento	= a.cd_estabelecimento);

	if	(:new.ie_farmacia = 'N') and
		(qt_registro_w > 0) then
		--O campo "Farm�cia comercial" n�o pode ser desmarcado pois h� cadastro na atividade correspondente.
		wheb_mensagem_pck.exibir_mensagem_abort(267090);
	end if;
end if;

if	(:new.ie_operadora = 'X') then
	:new.ie_operadora	:= 'N';
elsif	(:new.ie_prestador = 'X') then
	:new.ie_prestador	:= 'N';
elsif	(:new.ie_farmacia = 'X') then
	:new.ie_farmacia	:= 'N';
end if;
null;

end;
/


ALTER TABLE TASY.MATERIAL_TIPO_LOCAL_EST ADD (
  CONSTRAINT MATTLES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT MATTLES_UK
 UNIQUE (CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_TIPO_LOCAL_EST ADD (
  CONSTRAINT MATTLES_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MATERIAL_TIPO_LOCAL_EST TO NIVEL_1;

