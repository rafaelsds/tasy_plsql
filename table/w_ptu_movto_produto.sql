ALTER TABLE TASY.W_PTU_MOVTO_PRODUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PTU_MOVTO_PRODUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PTU_MOVTO_PRODUTO
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  IE_TIPO_REG                     NUMBER(1),
  DT_GERACAO_ARQUIVO              DATE,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  CD_ANS                          VARCHAR2(20 BYTE),
  DS_CONSTANTE                    VARCHAR2(3 BYTE),
  DS_MODALIDADE                   VARCHAR2(1 BYTE),
  SG_UF                           VARCHAR2(2 BYTE),
  NM_BENEFICIARIO                 VARCHAR2(255 BYTE),
  QT_REG_INCONSISTENTES           NUMBER(10),
  DS_LOGRADOURO                   VARCHAR2(255 BYTE),
  CD_CGC                          VARCHAR2(14 BYTE),
  CD_CEP                          VARCHAR2(15 BYTE),
  NR_SEQ_ARQUIVO                  NUMBER(7),
  DT_MESANO_REFERENCIA            DATE,
  CD_USUARIO_PLANO                VARCHAR2(30 BYTE),
  NM_BEFICIARIO                   VARCHAR2(60 BYTE),
  DT_NASCIMENTO                   DATE,
  IE_SEXO                         NUMBER(1),
  NR_CPF                          VARCHAR2(11 BYTE),
  NR_PIS_PASEP                    VARCHAR2(11 BYTE),
  NM_MAE_BENEF                    VARCHAR2(60 BYTE),
  CD_CNES                         VARCHAR2(15 BYTE),
  NR_IDENTIDADE                   VARCHAR2(30 BYTE),
  DS_ORGAO_EMISSOR_CI             VARCHAR2(40 BYTE),
  CD_NACIONALIDADE                NUMBER(3),
  CD_USUARIO_PLANO_SUP            VARCHAR2(30 BYTE),
  CD_PLANO_ANS                    VARCHAR2(20 BYTE),
  CD_PLANO_ANS_PRE                VARCHAR2(20 BYTE),
  CD_SEGMENTACAO_PRE              NUMBER(5),
  CD_ABRANGENCIA_PRE              NUMBER(5),
  TP_CONTRACAO_PRE                NUMBER(5),
  DT_ADESAO_PLANO                 DATE,
  CD_INDICACAO_COPARTIC_FRANQUIA  NUMBER(1),
  DT_CANCELAMENTO                 DATE,
  DT_REINCLUSAO                   DATE,
  CD_MOTIVO                       NUMBER(2),
  IE_CARENCIA_TEMP                NUMBER(1),
  IE_ITENS_EXCLUID_COBERTURA      NUMBER(1),
  DT_ADAPTACAO                    DATE,
  CD_CGC_ESTIPULANTE              VARCHAR2(14 BYTE),
  CD_CEI                          VARCHAR2(14 BYTE),
  DS_NUMERO                       VARCHAR2(5 BYTE),
  DS_COMPLEMENTO                  VARCHAR2(15 BYTE),
  DS_BAIRRO                       VARCHAR2(30 BYTE),
  DS_MUNICIPIO                    VARCHAR2(30 BYTE),
  CD_VINCULO_BENEF                NUMBER(2),
  DS_OBSERVACAO                   VARCHAR2(255 BYTE),
  QT_TIPO1                        NUMBER(5),
  QT_TIPO2                        NUMBER(5),
  QT_TIPO3                        NUMBER(5),
  QT_TIPO4                        NUMBER(5),
  QT_TIPO5                        NUMBER(5),
  QT_TIPO6                        NUMBER(5),
  QT_TIPO7                        NUMBER(5),
  QT_TIPO8                        NUMBER(5),
  CD_CNS                          NUMBER(15),
  CD_PESSOA_FISICA                VARCHAR2(10 BYTE),
  NR_SEQ_SEGURADO                 NUMBER(10),
  CD_USUARIO_ANT                  VARCHAR2(30 BYTE),
  NM_PAIS                         VARCHAR2(200 BYTE),
  CD_PAIS                         VARCHAR2(30 BYTE),
  NR_SEQ_TASY_ANS                 NUMBER(10),
  NR_SEQ_TITULAR                  NUMBER(10),
  NR_PROT_ANS_ORIGEM              VARCHAR2(20 BYTE),
  IE_RESID_BRASIL                 NUMBER(1),
  CD_MOTIVO_CANCELAMENTO          VARCHAR2(10 BYTE),
  NR_SEQ_PORTABILIDADE            NUMBER(10),
  NR_SEQ_PLANO_PORTAB             NUMBER(10),
  NR_CCO                          NUMBER(10),
  IE_DIGITO_CCO                   NUMBER(2),
  IE_TIPO_REENVIO                 NUMBER(2),
  IE_DIG_MUNICIPIO_IBGE           VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_MOVTO               NUMBER(10),
  NR_SEQ_MOVIMENTACAO             NUMBER(10),
  NR_SEQ_MOVTO_BENEF              NUMBER(10),
  NR_SEQ_REG_ARQUIVO              NUMBER(10),
  IE_TIPO_MOVIMENTO               VARCHAR2(2 BYTE),
  IE_ESTADO_CIVIL                 VARCHAR2(2 BYTE),
  DS_EMAIL_1                      VARCHAR2(255 BYTE),
  DS_EMAIL_2                      VARCHAR2(255 BYTE),
  NR_SEQ_CONTRATO                 NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPTUMOP_PK ON TASY.W_PTU_MOVTO_PRODUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPTUMOP_PK
  MONITORING USAGE;


CREATE INDEX TASY.WPTUMOP_PLSCONT_FK_I ON TASY.W_PTU_MOVTO_PRODUTO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPTUMOP_PLSTAAN_FK_I ON TASY.W_PTU_MOVTO_PRODUTO
(NR_SEQ_TASY_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPTUMOP_PLSTAAN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.W_PTU_MOVTO_PRODUTO_tp  after update ON TASY.W_PTU_MOVTO_PRODUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'W_PTU_MOVTO_PRODUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.W_PTU_MOVTO_PRODUTO ADD (
  CONSTRAINT WPTUMOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PTU_MOVTO_PRODUTO ADD (
  CONSTRAINT WPTUMOP_PLSTAAN_FK 
 FOREIGN KEY (NR_SEQ_TASY_ANS) 
 REFERENCES TASY.PLS_SIB_TASY_ANS (NR_SEQUENCIA),
  CONSTRAINT WPTUMOP_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PTU_MOVTO_PRODUTO TO NIVEL_1;

