ALTER TABLE TASY.TABELA_SISTEMA_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_SISTEMA_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_SISTEMA_PERFIL
(
  CD_PERFIL       NUMBER(5)                     NOT NULL,
  NM_TABELA       VARCHAR2(50 BYTE)             NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  IE_CONTROLE     VARCHAR2(1 BYTE)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TABSIPE_PERFIL_FK_I ON TASY.TABELA_SISTEMA_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TABSIPE_PK ON TASY.TABELA_SISTEMA_PERFIL
(CD_PERFIL, NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABSIPE_TABSIST_FK_I ON TASY.TABELA_SISTEMA_PERFIL
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TABELA_SISTEMA_PERFIL_tp  after update ON TASY.TABELA_SISTEMA_PERFIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_PERFIL='||to_char(:old.CD_PERFIL)||'#@#@NM_TABELA='||to_char(:old.NM_TABELA);gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'TABELA_SISTEMA_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_TABELA,1,4000),substr(:new.NM_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NM_TABELA',ie_log_w,ds_w,'TABELA_SISTEMA_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TABELA_SISTEMA_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLE,1,4000),substr(:new.IE_CONTROLE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE',ie_log_w,ds_w,'TABELA_SISTEMA_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TABELA_SISTEMA_PERFIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TABELA_SISTEMA_PERFIL ADD (
  CONSTRAINT TABSIPE_PK
 PRIMARY KEY
 (CD_PERFIL, NM_TABELA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_SISTEMA_PERFIL ADD (
  CONSTRAINT TABSIPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT TABSIPE_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TABELA_SISTEMA_PERFIL TO NIVEL_1;

