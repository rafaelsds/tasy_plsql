ALTER TABLE TASY.TITULO_PAGAR_ADIANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_ADIANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_ADIANT
(
  NR_TITULO           NUMBER(10)                NOT NULL,
  NR_SEQUENCIA        NUMBER(5)                 NOT NULL,
  NR_ADIANTAMENTO     NUMBER(10)                NOT NULL,
  VL_ADIANTAMENTO     NUMBER(15,2)              NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_LOTE_CONTABIL    NUMBER(10),
  NR_SEQ_TRANS_FIN    NUMBER(10),
  DS_OBSERVACAO       VARCHAR2(4000 BYTE),
  DT_CONTABIL         DATE,
  VL_IMPOSTO          NUMBER(15,2),
  VL_ADTO_ESTRANG     NUMBER(15,2),
  CD_MOEDA            NUMBER(5),
  VL_COTACAO          NUMBER(21,10),
  VL_COMPLEMENTO      NUMBER(15,2),
  VL_MULTA            NUMBER(15,2),
  VL_PAGO_ADIANT      NUMBER(15,2),
  VL_ACRESCIMOS       NUMBER(15,2),
  VL_OUTRAS_DESPESAS  NUMBER(15,2),
  VL_JUROS            NUMBER(15,2),
  VL_CAMBIAL_ATIVO    NUMBER(15,2),
  VL_CAMBIAL_PASSIVO  NUMBER(15,2),
  NR_CODIGO_CONTROLE  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPAAD_ADIPAGO_FK_I ON TASY.TITULO_PAGAR_ADIANT
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAD_LOTCONT_FK_I ON TASY.TITULO_PAGAR_ADIANT
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAD_MOEDA_FK_I ON TASY.TITULO_PAGAR_ADIANT
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPAAD_PK ON TASY.TITULO_PAGAR_ADIANT
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAD_TITPAGA_FK_I ON TASY.TITULO_PAGAR_ADIANT
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAD_TRAFINA_FK_I ON TASY.TITULO_PAGAR_ADIANT
(NR_SEQ_TRANS_FIN)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAAD_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_adiant_after
after insert or delete ON TASY.TITULO_PAGAR_ADIANT for each row
declare

cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
vl_movimento_w          ctb_documento.vl_movimento%type;
contador                integer;
cd_estabelecimento_logado_w	estabelecimento.cd_estabelecimento%type;
ie_adiant_pago_w  parametro_fluxo_caixa.ie_adiant_pago%type;
nr_sequencia_docto_w fluxo_caixa_docto.nr_sequencia%type default null;
cont_w             integer;

cursor  c01 is
select  a.nm_atributo,
        a.cd_tipo_lote_contab
from    atributo_contab a
where   a.cd_tipo_lote_contab = 7
and     a.nm_atributo in ( 'VL_ADIANT_TIT_PAGAR', 'VL_IMPOSTO_ADIANT');

c01_w   c01%rowtype;


begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if      (inserting) then

		cd_estabelecimento_logado_w   := wheb_usuario_pck.get_cd_estabelecimento;

		select nvl(max(IE_ADIANT_PAGO),'XX')
		into    IE_ADIANT_PAGO_W
		from PARAMETRO_FLUXO_CAIXA
		WHERE CD_ESTABELECIMENTO = cd_estabelecimento_logado_w;

		select	count(*)
		into    cont_w
		from 	FLUXO_CAIXA_DOCTO a,
			movto_trans_financ x
		where	x.nr_adiant_pago	= :new.NR_ADIANTAMENTO
		AND x.nr_sequencia =  a.nr_seq_movto_trans;

		if (cont_w <> 0) then
			if (IE_ADIANT_PAGO_W = 'A') then
				delete from FLUXO_CAIXA_DOCTO
				where nr_sequencia in (	select	nvl(a.nr_sequencia,0)
										from	movto_trans_financ x,
												FLUXO_CAIXA_DOCTO a
										where	x.nr_adiant_pago	= :new.NR_ADIANTAMENTO
										AND x.nr_sequencia =  a.nr_seq_movto_trans);
			end if;
		end if;

			/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
			gravar_agend_fluxo_caixa(:new.nr_titulo,:new.nr_sequencia,'TPBA',:new.dt_contabil,'I', :new.nm_usuario);
        open c01;
        loop
        fetch c01 into
                c01_w;
        exit when c01%notfound;
        begin
                select  max(cd_estabelecimento)
                into    cd_estabelecimento_w
                from    titulo_pagar
                where   nr_titulo       = :new.nr_titulo;

                if      (c01_w.nm_atributo = 'VL_ADIANT_TIT_PAGAR') then
                        vl_movimento_w := :new.vl_adiantamento;

                elsif   (c01_w.nm_atributo = 'VL_IMPOSTO_ADIANT' ) then
                        vl_movimento_w := :new.vl_imposto;
                end if;


                if      (nvl(vl_movimento_w, 0) <> 0) then
                        begin
                        ctb_concil_financeira_pck.ctb_gravar_documento( cd_estabelecimento_w,
                                                                        :new.dt_contabil,
                                                                        7,
                                                                        :new.nr_seq_trans_fin,
                                                                        16,
                                                                        :new.nr_titulo,
                                                                        :new.nr_sequencia,
                                                                        null,
                                                                        vl_movimento_w,
                                                                        'TITULO_PAGAR_ADIANT',
                                                                        c01_w.nm_atributo,
                                                                        :new.nm_usuario);
                        end;
                end if;
        end;
        end loop;
        close c01;

elsif   (deleting) then
        begin

	select  max(cd_estabelecimento)
	into    cd_estabelecimento_w
	from    titulo_pagar
	where   nr_titulo       = :old.nr_titulo;

	delete  from ctb_documento
	where   cd_estabelecimento	= cd_estabelecimento_w
	and     dt_competencia	= :old.dt_contabil
	and     cd_tipo_lote_contabil	= 7
	and     nr_Seq_trans_financ	= :old.nr_seq_trans_fin
	and     nr_seq_info		= 16
	and     nr_documento	= :old.nr_titulo
	and     nr_seq_doc_compl	= :old.nr_sequencia
	and     vl_movimento            	= :old.vl_adiantamento
	and     nm_tabela               	= 'TITULO_PAGAR_ADIANT'
	and     nm_atributo             	= 'VL_ADIANT_TIT_PAGAR';
	exception when others then
		cd_estabelecimento_w	:= null;
        end;

	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:old.nr_titulo,:old.nr_sequencia,'TPBA',:old.dt_contabil,'E',:old.nm_usuario);

elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,:new.nr_sequencia,'TPBA',:new.dt_contabil,'A',:new.nm_usuario);
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_ADIANT_INSERT
BEFORE INSERT ON TASY.TITULO_PAGAR_ADIANT FOR EACH ROW
DECLARE

cd_estabelecimento_w		number(5);
ie_altera_valor_tit_escrit_w	varchar2(1);
qt_registro_w			number(5);
vl_saldo_juros_w	number(15,2) ;
vl_saldo_multa_w	number(15,2);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(cd_estabelecimento),
	nvl(max(vl_saldo_juros),0),
	nvl(max(vl_saldo_multa),0)
into	cd_estabelecimento_w,
	vl_saldo_juros_w,
	vl_saldo_multa_w
from	titulo_pagar
where	nr_titulo	= :new.nr_titulo;

vl_saldo_juros_w := vl_saldo_juros_w - :new.vl_juros;
vl_saldo_multa_w := vl_saldo_multa_w - :new.vl_multa;

if (vl_saldo_juros_w < 0) then
	vl_saldo_juros_w := 0;
end if;

if (vl_saldo_multa_w < 0) then
	vl_saldo_multa_w := 0;
end if;

update	titulo_pagar
set	vl_saldo_juros = nvl(vl_saldo_juros_w,0),
	vl_saldo_multa = nvl(vl_saldo_multa_w,0)
where	nr_titulo = :new.nr_titulo;

/* nao deixar salvar se tem registro na titulo_pagar_adiant vinculado ao titulo */

select	nvl(max(ie_altera_valor_tit_escrit),'S')
into	ie_altera_valor_tit_escrit_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_altera_valor_tit_escrit_w = 'N') then

	select	count(*)
	into	qt_registro_w
	from	titulo_pagar_escrit
	where	nr_titulo	= :new.nr_titulo;

	if	(qt_registro_w > 0) then
		/* Titulo ja vinculado a uma remessa de pagamento escritural.
		O mesmo so pode ser baixado pela funcao "Pagamento escritural".
		Titulo: :new.nr_titulo */
		wheb_mensagem_pck.exibir_mensagem_abort(262125,'NR_TITULO_W='||:new.nr_titulo);
	end if;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_adiant_delete
before delete ON TASY.TITULO_PAGAR_ADIANT for each row
declare

vl_saldo_juros_w	titulo_pagar.vl_saldo_juros%type;
vl_saldo_multa_w	titulo_pagar.vl_saldo_multa%type;

begin

select	nvl(max(vl_saldo_juros),0),
	nvl(max(vl_saldo_multa),0)
into	vl_saldo_juros_w,
	vl_saldo_multa_w
from	titulo_pagar
where	nr_titulo = :old.nr_titulo;

vl_saldo_juros_w := vl_saldo_juros_w + :old.vl_juros;
vl_saldo_multa_w := vl_saldo_multa_w + :old.vl_multa;

if (vl_saldo_juros_w < 0) then
	vl_saldo_juros_w := 0;
end if;

if (vl_saldo_multa_w < 0) then
	vl_saldo_multa_w := 0;
end if;

update	titulo_pagar
set	vl_saldo_juros = nvl(vl_saldo_juros_w,0),
	vl_saldo_multa = nvl(vl_saldo_multa_w,0)
where	nr_titulo = :old.nr_titulo;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_ADIANT ADD (
  CONSTRAINT TITPAAD_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_ADIANT ADD (
  CONSTRAINT TITPAAD_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITPAAD_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAAD_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPAAD_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT TITPAAD_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.TITULO_PAGAR_ADIANT TO NIVEL_1;

