ALTER TABLE TASY.PLS_REGRA_GLOSA_OPER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_GLOSA_OPER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_GLOSA_OPER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_MOTIVO_GLOSA  NUMBER(10)               NOT NULL,
  IE_APLICACAO_REGRA   VARCHAR2(3 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREGLO_PK ON TASY.PLS_REGRA_GLOSA_OPER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGLO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSREGLO_PLSPRES_FK_I ON TASY.PLS_REGRA_GLOSA_OPER
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGLO_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREGLO_TISSMGL_FK_I ON TASY.PLS_REGRA_GLOSA_OPER
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGLO_TISSMGL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_GLOSA_OPER ADD (
  CONSTRAINT PLSREGLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_GLOSA_OPER ADD (
  CONSTRAINT PLSREGLO_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREGLO_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_GLOSA_OPER TO NIVEL_1;

