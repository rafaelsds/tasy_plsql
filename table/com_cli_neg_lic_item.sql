ALTER TABLE TASY.COM_CLI_NEG_LIC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLI_NEG_LIC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLI_NEG_LIC_ITEM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_NEG_LIC         NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_LICENCA             DATE                   NOT NULL,
  NR_SEQ_MOD_LICENC      NUMBER(10)             NOT NULL,
  QT_LICENCA             NUMBER(15)             NOT NULL,
  VL_UNITARIO            NUMBER(17,4)           NOT NULL,
  VL_TOTAL               NUMBER(17,4)           NOT NULL,
  NR_SEQ_CANAL           NUMBER(10)             NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_SEQ_POLITICA        NUMBER(10),
  DT_INICIO_VENC         DATE,
  VL_UNIT_MANUT          NUMBER(17,4),
  VL_MANUT_MENSAL        NUMBER(17,4),
  DT_INICIO_VENC_MANUT   DATE,
  VL_UNIT_DISTR          NUMBER(17,4)           NOT NULL,
  VL_DISTRIBUIDOR        NUMBER(17,4)           NOT NULL,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  VL_ORIGINAL            NUMBER(17,4)           NOT NULL,
  VL_ATUAL               NUMBER(17,4)           NOT NULL,
  IE_ANTERIOR            VARCHAR2(1 BYTE)       NOT NULL,
  VL_UNIT_DISTR_ATUAL    NUMBER(17,4),
  VL_DISTRIBUIDOR_ATUAL  NUMBER(17,4),
  QT_MES_REAJ            NUMBER(2),
  DT_VIGENCIA            DATE,
  VL_CONSULTORIA         NUMBER(15,2),
  VL_COORDENACAO         NUMBER(15,2),
  IE_INDICE_REAJUSTE     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLNI_COMCANA_FK_I ON TASY.COM_CLI_NEG_LIC_ITEM
(NR_SEQ_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLNI_COMCANA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLNI_COMCLNL_FK_I ON TASY.COM_CLI_NEG_LIC_ITEM
(NR_SEQ_NEG_LIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLNI_COMCLNL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLNI_COMMOLI_FK_I ON TASY.COM_CLI_NEG_LIC_ITEM
(NR_SEQ_MOD_LICENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLNI_COMMOLI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLNI_COMPOCO_FK_I ON TASY.COM_CLI_NEG_LIC_ITEM
(NR_SEQ_POLITICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLNI_COMPOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLNI_PESFISI_FK_I ON TASY.COM_CLI_NEG_LIC_ITEM
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCLNI_PK ON TASY.COM_CLI_NEG_LIC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLNI_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_CLI_NEG_LIC_ITEM ADD (
  CONSTRAINT COMCLNI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLI_NEG_LIC_ITEM ADD (
  CONSTRAINT COMCLNI_COMCLNL_FK 
 FOREIGN KEY (NR_SEQ_NEG_LIC) 
 REFERENCES TASY.COM_CLI_NEG_LIC (NR_SEQUENCIA),
  CONSTRAINT COMCLNI_COMCANA_FK 
 FOREIGN KEY (NR_SEQ_CANAL) 
 REFERENCES TASY.COM_CANAL (NR_SEQUENCIA),
  CONSTRAINT COMCLNI_COMMOLI_FK 
 FOREIGN KEY (NR_SEQ_MOD_LICENC) 
 REFERENCES TASY.COM_MODALIDADE_LIC (NR_SEQUENCIA),
  CONSTRAINT COMCLNI_COMPOCO_FK 
 FOREIGN KEY (NR_SEQ_POLITICA) 
 REFERENCES TASY.COM_POLITICA_COML (NR_SEQUENCIA),
  CONSTRAINT COMCLNI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.COM_CLI_NEG_LIC_ITEM TO NIVEL_1;

