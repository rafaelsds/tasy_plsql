ALTER TABLE TASY.PLS_PRESTADOR_NEGATIVA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_NEGATIVA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_NEGATIVA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PRESTADOR     NUMBER(10)               NOT NULL,
  NR_SEQ_SEGURADO      NUMBER(10)               NOT NULL,
  NR_SEQ_GUIA          NUMBER(10)               NOT NULL,
  DT_RECLAMACAO        DATE                     NOT NULL,
  NR_SEQ_RECLAMACAO    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRNE_PK ON TASY.PLS_PRESTADOR_NEGATIVA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRNE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRNE_PLSGUIA_FK_I ON TASY.PLS_PRESTADOR_NEGATIVA
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRNE_PLSGUIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRNE_PLSMORB_FK_I ON TASY.PLS_PRESTADOR_NEGATIVA
(NR_SEQ_RECLAMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRNE_PLSMORB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRNE_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_NEGATIVA
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRNE_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRNE_PLSSEGU_FK_I ON TASY.PLS_PRESTADOR_NEGATIVA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRNE_PLSSEGU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PRESTADOR_NEGATIVA ADD (
  CONSTRAINT PLSPRNE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_NEGATIVA ADD (
  CONSTRAINT PLSPRNE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRNE_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSPRNE_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSPRNE_PLSMORB_FK 
 FOREIGN KEY (NR_SEQ_RECLAMACAO) 
 REFERENCES TASY.PLS_MOT_RECLAMACAO_BENEF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_NEGATIVA TO NIVEL_1;

