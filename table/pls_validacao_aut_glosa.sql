ALTER TABLE TASY.PLS_VALIDACAO_AUT_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_VALIDACAO_AUT_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_VALIDACAO_AUT_GLOSA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_OCOR_AUT_COMBINADA  NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_GLOSA        NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSVAG_PK ON TASY.PLS_VALIDACAO_AUT_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVAG_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSVAG_PLSOAUC_FK_I ON TASY.PLS_VALIDACAO_AUT_GLOSA
(NR_SEQ_OCOR_AUT_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVAG_PLSOAUC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVAG_TISSMGL_FK_I ON TASY.PLS_VALIDACAO_AUT_GLOSA
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVAG_TISSMGL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_VALIDACAO_AUT_GLOSA_tp  after update ON TASY.PLS_VALIDACAO_AUT_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_GLOSA,1,4000),substr(:new.NR_SEQ_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_GLOSA',ie_log_w,ds_w,'PLS_VALIDACAO_AUT_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_VALIDACAO_AUT_GLOSA ADD (
  CONSTRAINT PLSVAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_VALIDACAO_AUT_GLOSA ADD (
  CONSTRAINT PLSVAG_PLSOAUC_FK 
 FOREIGN KEY (NR_SEQ_OCOR_AUT_COMBINADA) 
 REFERENCES TASY.PLS_OCOR_AUT_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSVAG_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_VALIDACAO_AUT_GLOSA TO NIVEL_1;

