ALTER TABLE TASY.ESCALA_IPPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_IPPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_IPPS
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  DT_AVALIACAO              DATE                NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_ESVAZIAR_BEXIGA_TOTAL  NUMBER(1)           NOT NULL,
  IE_URINAR_2_HORAS         NUMBER(1)           NOT NULL,
  IE_URINAR_RECOMECOU       NUMBER(1)           NOT NULL,
  IE_DIFIC_CONTER_URINA     NUMBER(1)           NOT NULL,
  IE_JATO_FRACO             NUMBER(1)           NOT NULL,
  IE_FORCA_URINAR           NUMBER(1)           NOT NULL,
  IE_VEZES_URINAR_NOITE     NUMBER(1)           NOT NULL,
  QT_PONTUACAO              NUMBER(3),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCIPPS_ATEPACI_FK_I ON TASY.ESCALA_IPPS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCIPPS_PESFISI_FK_I ON TASY.ESCALA_IPPS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCIPPS_PK ON TASY.ESCALA_IPPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCIPPS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_IPSS_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_IPPS FOR EACH ROW
declare
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_pontuacao	:= 	:new.IE_ESVAZIAR_BEXIGA_TOTAL +
				:new.IE_URINAR_2_HORAS +
				:new.IE_URINAR_RECOMECOU +
				:new.IE_DIFIC_CONTER_URINA +
				:new.IE_JATO_FRACO +
				:new.IE_FORCA_URINAR +
				:new.IE_VEZES_URINAR_NOITE;
<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ESCALA_IPPS ADD (
  CONSTRAINT ESCIPPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_IPPS ADD (
  CONSTRAINT ESCIPPS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCIPPS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_IPPS TO NIVEL_1;

