ALTER TABLE TASY.QT_LOCAL_TURNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_LOCAL_TURNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_LOCAL_TURNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  DT_DIA_SEMANA        NUMBER(10),
  HR_INICIAL           DATE,
  HR_FINAL             DATE,
  IE_FERIADO           VARCHAR2(1 BYTE),
  DT_VIGENCIA_FINAL    DATE,
  DT_VIGENCIA_INICIAL  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QTLOCTR_PK ON TASY.QT_LOCAL_TURNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTLOCTR_PK
  MONITORING USAGE;


CREATE INDEX TASY.QTLOCTR_QTLOCAL_FK_I ON TASY.QT_LOCAL_TURNO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.qt_local_turno_Atual
before insert or update ON TASY.QT_LOCAL_TURNO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(sysdate)
and	nr_seq_local	= :new.nr_seq_local;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.qt_local_turno_Delete
before delete ON TASY.QT_LOCAL_TURNO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(sysdate)
and	nr_seq_local	 = :old.nr_seq_local;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.QT_LOCAL_TURNO ADD (
  CONSTRAINT QTLOCTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QT_LOCAL_TURNO ADD (
  CONSTRAINT QTLOCTR_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.QT_LOCAL_TURNO TO NIVEL_1;

