ALTER TABLE TASY.OPCOES_CRUD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPCOES_CRUD CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPCOES_CRUD
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_OPCAO_CRUD            VARCHAR2(2 BYTE),
  NR_SEQ_OBJETO_SCHEMATIC  NUMBER(10),
  DS_OPCAO_CRUD            VARCHAR2(255 BYTE),
  IE_INSERT                VARCHAR2(2 BYTE),
  IE_UPDATE                VARCHAR2(2 BYTE),
  IE_DELETE                VARCHAR2(2 BYTE),
  IE_DUPLICAR              VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPCRU_OBJSCHE_FK_I ON TASY.OPCOES_CRUD
(NR_SEQ_OBJETO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPCRU_PK ON TASY.OPCOES_CRUD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPCOES_CRUD_tp  after update ON TASY.OPCOES_CRUD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_INSERT,1,4000),substr(:new.IE_INSERT,1,4000),:new.nm_usuario,nr_seq_w,'IE_INSERT',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UPDATE,1,4000),substr(:new.IE_UPDATE,1,4000),:new.nm_usuario,nr_seq_w,'IE_UPDATE',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DUPLICAR,1,4000),substr(:new.IE_DUPLICAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_DUPLICAR',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DELETE,1,4000),substr(:new.IE_DELETE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DELETE',ie_log_w,ds_w,'OPCOES_CRUD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPCOES_CRUD ADD (
  CONSTRAINT OPCRU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OPCOES_CRUD ADD (
  CONSTRAINT OPCRU_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_OBJETO_SCHEMATIC) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPCOES_CRUD TO NIVEL_1;

