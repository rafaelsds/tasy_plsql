ALTER TABLE TASY.CONTROLE_PESSOA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTROLE_PESSOA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTROLE_PESSOA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_TIPO_CONTROLE     NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_CGC               VARCHAR2(14 BYTE),
  CD_CLASSIF_PRINC     VARCHAR2(20 BYTE),
  CD_CLASSIF_SEC       VARCHAR2(20 BYTE),
  NR_SEQ_CLIENTE       NUMBER(10),
  NR_SEQ_COM_CLIENTE   NUMBER(10),
  NM_PESSOA_CONTATO    VARCHAR2(255 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_PF_RESP           VARCHAR2(10 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONPESS_CLITASY_FK_I ON TASY.CONTROLE_PESSOA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPESS_CLITASY_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPESS_COMCLIE_FK_I ON TASY.CONTROLE_PESSOA
(NR_SEQ_COM_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPESS_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPESS_PESFISI_FK_I ON TASY.CONTROLE_PESSOA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPESS_PESFISI_FK2_I ON TASY.CONTROLE_PESSOA
(CD_PF_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPESS_PESJURI_FK_I ON TASY.CONTROLE_PESSOA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPESS_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONPESS_PK ON TASY.CONTROLE_PESSOA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPESS_PK
  MONITORING USAGE;


CREATE INDEX TASY.CONPESS_TIPCOPE_FK_I ON TASY.CONTROLE_PESSOA
(CD_TIPO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPESS_TIPCOPE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CONTROLE_PESSOA ADD (
  CONSTRAINT CONPESS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTROLE_PESSOA ADD (
  CONSTRAINT CONPESS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT CONPESS_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT CONPESS_TIPCOPE_FK 
 FOREIGN KEY (CD_TIPO_CONTROLE) 
 REFERENCES TASY.TIPO_CONTROLE_PESSOA (CD_TIPO_CONTROLE)
    ON DELETE CASCADE,
  CONSTRAINT CONPESS_CLITASY_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.CLIENTE_TASY (NR_SEQUENCIA),
  CONSTRAINT CONPESS_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_COM_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT CONPESS_PESFISI_FK2 
 FOREIGN KEY (CD_PF_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CONTROLE_PESSOA TO NIVEL_1;

