ALTER TABLE TASY.NUT_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_PAC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_PRESCRICAO              NUMBER(14)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  QT_PESO                    NUMBER(15,3)       NOT NULL,
  QT_IDADE_DIA               NUMBER(15)         NOT NULL,
  QT_IDADE_MES               NUMBER(15)         NOT NULL,
  QT_IDADE_ANO               NUMBER(15)         NOT NULL,
  QT_ALTURA_CM               NUMBER(15)         NOT NULL,
  QT_DIA_NPT                 NUMBER(15)         NOT NULL,
  QT_KCAL_TOTAL              NUMBER(16,2)       NOT NULL,
  QT_KCAL_KG                 NUMBER(15,1)       NOT NULL,
  QT_FASE_NPT                NUMBER(15)         NOT NULL,
  QT_NEC_HIDRICA_DIARIA      NUMBER(16,1),
  QT_APORTE_HIDRICO_DIARIO   NUMBER(15,2),
  QT_VEL_INF_GLICOSE         NUMBER(15,2),
  QT_NEC_KCAL_KG_DIA         NUMBER(15,1),
  NR_SEQ_FATOR_ATIV          NUMBER(15),
  NR_SEQ_FATOR_STRESS        NUMBER(15),
  QT_EQUIPO                  NUMBER(15,1),
  PR_CONC_GLIC_SOLUCAO       NUMBER(20,6),
  QT_REL_CAL_NIT             NUMBER(17,2),
  QT_NEC_KCAL_DIA            NUMBER(15,1),
  IE_CALCULO_AUTO            VARCHAR2(1 BYTE)   NOT NULL,
  IE_CORRECAO                VARCHAR2(1 BYTE)   NOT NULL,
  HR_PRIM_HORARIO            VARCHAR2(5 BYTE),
  IE_AJUSTAR_POTASSIO        VARCHAR2(1 BYTE)   NOT NULL,
  QT_GOTEJO_NPT              NUMBER(15,2),
  IE_EMISSAO                 NUMBER(1)          NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_STATUS                  DATE,
  IE_STATUS                  VARCHAR2(3 BYTE),
  IE_SUSPENSO                VARCHAR2(1 BYTE),
  DT_SUSPENSAO               DATE,
  NM_USUARIO_SUSP            VARCHAR2(15 BYTE),
  QT_VOLUME_ADEP             NUMBER(15,3),
  IE_FORMA_INFUSAO           VARCHAR2(1 BYTE),
  PR_CARBOIDRATO             NUMBER(4,1),
  PR_LIPIDIO                 NUMBER(4,1),
  PR_NPT                     NUMBER(5,2),
  PR_PROTEINA                NUMBER(4,1),
  IE_NPT_ADULTA              VARCHAR2(1 BYTE)   NOT NULL,
  IE_BOMBA_INFUSAO           VARCHAR2(1 BYTE),
  QT_GRAMA_NITROGENIO        NUMBER(15,4),
  IE_GERA_PRODUTO            VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOLO           NUMBER(10),
  IE_FORMA                   VARCHAR2(1 BYTE),
  QT_KCAL_PROTEINA           NUMBER(15,1),
  QT_KCAL_PROTEICO           NUMBER(15,1),
  QT_KCAL_LIPIDIO            NUMBER(15,1),
  QT_KCAL_NAO_PROTEICO       NUMBER(15,1),
  QT_KCAL_CARBOIDRATO        NUMBER(15,1),
  PR_CONC_LIPIDIO_SOLUCAO    NUMBER(20,6),
  PR_CONC_PROTEINA_SOLUCAO   NUMBER(20,6),
  QT_OSMOLARIDADE_TOTAL      NUMBER(15,2),
  IE_VIA_ADMINISTRACAO       VARCHAR2(1 BYTE),
  QT_VOLUME_DIARIO           NUMBER(15,2),
  CD_SETOR_EXEC_INIC         NUMBER(5),
  CD_SETOR_EXEC_FIM          NUMBER(5),
  QT_PESO_IDEAL              NUMBER(15,3),
  QT_PESO_AJUSTADO           NUMBER(15,3),
  QT_TMB                     NUMBER(15,2),
  QT_GRAMA_PROTEINA_KG_DIA   NUMBER(15,2),
  QT_MULTIPLICADOR           NUMBER(5,2),
  QT_HORA_INF                NUMBER(3),
  QT_MIN_INF                 NUMBER(2),
  CD_PERFIL_ATIVO            NUMBER(5),
  QT_KCAL_KG_PED             NUMBER(15,1),
  QT_NEC_HIDRICA_DIARIA_PED  NUMBER(15),
  QT_PESO_CALORICO           NUMBER(15,2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_SUPLEMENTACAO           VARCHAR2(1 BYTE),
  IE_PERMITE_ALTERACAO       VARCHAR2(1 BYTE),
  IE_DISPOSITIVO_INFUSAO     VARCHAR2(1 BYTE),
  NR_SEQ_INTERF_FARM         NUMBER(10),
  CD_INTERVALO               VARCHAR2(7 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(4000 BYTE),
  DS_DIAGNOSTICO             VARCHAR2(256 BYTE),
  NR_SEQ_ANTERIOR            NUMBER(15),
  IE_ESTENDIDO               VARCHAR2(1 BYTE),
  IE_AJUSTAR_SODIO           VARCHAR2(1 BYTE),
  DT_EXTENSAO                DATE,
  QT_TEMPO_ETAPA             NUMBER(15,4),
  IE_CALCULA_VEL             VARCHAR2(1 BYTE),
  IE_EDITADO                 VARCHAR2(1 BYTE),
  NR_PRESCRICAO_ORIGINAL     NUMBER(15),
  DS_ORIENTACAO              VARCHAR2(255 BYTE),
  QT_CONC_CALCIO             NUMBER(15,1),
  IE_ZINCO                   VARCHAR2(1 BYTE),
  IE_PESO_CALORICO           VARCHAR2(1 BYTE),
  DS_STACK                   VARCHAR2(2000 BYTE),
  IE_ALTEROU_VCT             VARCHAR2(1 BYTE),
  IE_MODIFICADO              VARCHAR2(1 BYTE),
  QT_REL_CAL_FOS             NUMBER(17,2),
  IE_GERACAO_FOS_CAL         VARCHAR2(1 BYTE),
  QT_KCAL_TOTAL_ORIGEM       NUMBER(15,1),
  NR_SEQUENCIA_ANTERIOR      NUMBER(10),
  NR_SEQ_ASSINATURA_SUSP     NUMBER(10),
  QT_VOLUME_PRINC            NUMBER(15,2),
  QT_DESCONTAR_HIDRICO       NUMBER(15,4),
  IE_INCLUI_AGUA             VARCHAR2(1 BYTE),
  IE_ACM                     VARCHAR2(1 BYTE),
  IE_ALTERADA                VARCHAR2(1 BYTE),
  IE_ERRO                    NUMBER(3),
  DS_COR_ERRO                VARCHAR2(15 BYTE),
  NR_SEQ_NPT_CPOE            NUMBER(10),
  DT_PRIM_HORARIO            DATE,
  CD_PROTOCOLO               NUMBER(10),
  NR_SEQ_PROT_NPT            NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTPACT_CPOEDIE_FK_I ON TASY.NUT_PAC
(NR_SEQ_NPT_CPOE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACT_I1 ON TASY.NUT_PAC
(IE_EMISSAO, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACT_I2 ON TASY.NUT_PAC
(NR_SEQ_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_I2
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_NUTFAAT_FK_I ON TASY.NUT_PAC
(NR_SEQ_FATOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_NUTFAAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_NUTFAST_FK_I ON TASY.NUT_PAC
(NR_SEQ_FATOR_STRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_NUTFAST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_PERFIL_FK_I ON TASY.NUT_PAC
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NUTPACT_PK ON TASY.NUT_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACT_PRESMED_FK_I ON TASY.NUT_PAC
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACT_PROTNPT_FK_I ON TASY.NUT_PAC
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_PROTNPT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_SETATEN_FK_I ON TASY.NUT_PAC
(CD_SETOR_EXEC_INIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_SETATEN_FK2_I ON TASY.NUT_PAC
(CD_SETOR_EXEC_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACT_TASASDI_FK_I ON TASY.NUT_PAC
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACT_TASASDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nut_pac_update
before update ON TASY.NUT_PAC for each row
Declare
nr_seq_elemento_w	Number(10,0);
ie_seg_fase_w		Varchar2(1);
ie_terc_fase_w		Varchar2(1);
ie_quar_fase_w		Varchar2(1);
nr_sequencia_w		Number(10,0);

cursor C01 is
	select	nr_sequencia,
		ie_seg_fase,
		ie_terc_fase,
		ie_quar_fase
	from 	Nut_Elemento
	where	ie_situacao	= 'A'
	and	ie_gerar_ped	= 'S'
	and	cd_unidade_medida is not null
	Order by nr_seq_apresent, ds_elemento;

begin

if	(:new.qt_multiplicador is not null) and
	(:new.qt_multiplicador <> :old.qt_multiplicador) then
	begin

	update	nut_pac_elem_mat
	set		qt_volume	= qt_volume * :new.qt_multiplicador,
			qt_dose		= qt_dose * :new.qt_multiplicador
	where	nr_seq_nut_pac	= :new.nr_sequencia
	and		qt_volume is not null;

	end;
end if;

if	(:new.qt_fase_npt <> :old.qt_fase_npt) and
	(:new.ie_npt_adulta <> 'S') then
	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_elemento_w,
		ie_seg_fase_w,
		ie_terc_fase_w,
		ie_quar_fase_w;
	exit when C01%notfound;
		begin
		if	(:new.qt_fase_npt = 3) then
			ie_quar_fase_w	:= 'N';
		elsif	(:new.qt_fase_npt = 2) then
			ie_terc_fase_w	:= 'N';
			ie_quar_fase_w	:= 'N';
		elsif	(:new.qt_fase_npt = 1) then
			ie_terc_fase_w	:= 'N';
			ie_seg_fase_w	:= 'N';
			ie_quar_fase_w	:= 'N';
		end if;

		update	nut_pac_elemento
		set	ie_seg_fase	= ie_seg_fase_w,
			ie_terc_fase	= ie_terc_fase_w,
			ie_quar_fase	= ie_quar_fase_w
		where	nr_seq_nut_pac	= :new.nr_sequencia
		  and	nr_seq_elemento	= nr_seq_elemento_w;

		end;
	end Loop;
	close C01;
	end;
end if;

if	(nvl(:new.ie_suspenso,'N') <> 'N') then
	Suspender_Medic_Npt(:new.nr_prescricao, :new.nr_sequencia, 'NAN', wheb_usuario_pck.get_nm_usuario);

	update	nut_paciente_hor
	set		dt_suspensao		 = sysdate,
			nm_usuario_susp		 = :new.nm_usuario,
			ie_status 			 = 'S'
	where	nr_seq_nut_protocolo = :new.nr_sequencia
	and		dt_suspensao   is null
	and		dt_fim_horario is null;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nut_pac_atual
before insert or update ON TASY.NUT_PAC for each row
declare

begin
begin
	if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
		:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.nut_pac_insert
BEFORE INSERT ON TASY.NUT_PAC FOR EACH ROW
DECLARE

BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


ALTER TABLE TASY.NUT_PAC ADD (
  CONSTRAINT NUTPACT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_PAC ADD (
  CONSTRAINT NUTPACT_CPOEDIE_FK 
 FOREIGN KEY (NR_SEQ_NPT_CPOE) 
 REFERENCES TASY.CPOE_DIETA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NUTPACT_NUTFAAT_FK 
 FOREIGN KEY (NR_SEQ_FATOR_ATIV) 
 REFERENCES TASY.NUT_FATOR_ATIV (NR_SEQUENCIA),
  CONSTRAINT NUTPACT_NUTFAST_FK 
 FOREIGN KEY (NR_SEQ_FATOR_STRESS) 
 REFERENCES TASY.NUT_FATOR_STRESS (NR_SEQUENCIA),
  CONSTRAINT NUTPACT_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT NUTPACT_PROTNPT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_NPT (NR_SEQUENCIA),
  CONSTRAINT NUTPACT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC_INIC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NUTPACT_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_EXEC_FIM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NUTPACT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT NUTPACT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.NUT_PAC TO NIVEL_1;

