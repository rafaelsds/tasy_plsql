ALTER TABLE TASY.USUARIO_HIST_SENHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_HIST_SENHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_HIST_SENHA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_SENHA             VARCHAR2(64 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TEC               VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.USUHISE_I1 ON TASY.USUARIO_HIST_SENHA
(UPPER("NM_USUARIO"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUHISE_PK ON TASY.USUARIO_HIST_SENHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUHISE_PK
  MONITORING USAGE;


CREATE INDEX TASY.USUHISE_USUARIO_FK_I ON TASY.USUARIO_HIST_SENHA
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUHISE_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.USUARIO_HIST_SENHA ADD (
  CONSTRAINT USUHISE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_HIST_SENHA ADD (
  CONSTRAINT USUHISE_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.USUARIO_HIST_SENHA TO NIVEL_1;

