ALTER TABLE TASY.REG_LIC_EDITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_LIC_EDITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_LIC_EDITAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LICITACAO       NUMBER(10)             NOT NULL,
  DT_GERACAO             DATE,
  DS_ARQUIVO             VARCHAR2(255 BYTE),
  NR_CONTROLE_INTERNO    VARCHAR2(100 BYTE),
  NM_USUARIO_APROV       VARCHAR2(15 BYTE)      NOT NULL,
  NM_USUARIO_PAREC_JUR   VARCHAR2(15 BYTE),
  NM_USUARIO_HOMOLOG     VARCHAR2(15 BYTE)      NOT NULL,
  DT_ABERTURA_EDITAL     DATE,
  DT_APROVACAO_EDITAL    DATE,
  DT_PARECER_JURIDICO    DATE,
  DT_HOMOLOGACAO_EDITAL  DATE,
  NR_SEQ_TIPO_ANEXO      NUMBER(10),
  DT_RECEB_PROPOSTA      DATE,
  DT_ABERTURA_PROPOSTA   DATE,
  NR_SEQ_DOC_PADRAO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELIEDI_LICDOPA_FK_I ON TASY.REG_LIC_EDITAL
(NR_SEQ_DOC_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELIEDI_PK ON TASY.REG_LIC_EDITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELIEDI_PK
  MONITORING USAGE;


CREATE INDEX TASY.RELIEDI_RELICIT_FK_I ON TASY.REG_LIC_EDITAL
(NR_SEQ_LICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELIEDI_RELITAN_FK_I ON TASY.REG_LIC_EDITAL
(NR_SEQ_TIPO_ANEXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELIEDI_RELITAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REG_LIC_EDITAL ADD (
  CONSTRAINT RELIEDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_LIC_EDITAL ADD (
  CONSTRAINT RELIEDI_LICDOPA_FK 
 FOREIGN KEY (NR_SEQ_DOC_PADRAO) 
 REFERENCES TASY.LIC_DOCUMENTACAO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT RELIEDI_RELICIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO) 
 REFERENCES TASY.REG_LICITACAO (NR_SEQUENCIA),
  CONSTRAINT RELIEDI_RELITAN_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ANEXO) 
 REFERENCES TASY.REG_LIC_TIPO_ANEXO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_LIC_EDITAL TO NIVEL_1;

