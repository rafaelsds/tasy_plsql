ALTER TABLE TASY.PROC_PACIENTE_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_PACIENTE_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_PACIENTE_MATERIAL
(
  NR_SEQ_PROCEDIMENTO  NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  QT_UTILIZADO         NUMBER(9,3)              NOT NULL,
  QT_PERDIDO           NUMBER(9,3)              NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPAMA_MATERIA_FK_I ON TASY.PROC_PACIENTE_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPAMA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROPAMA_PK ON TASY.PROC_PACIENTE_MATERIAL
(NR_SEQ_PROCEDIMENTO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPAMA_PROPACI_FK_I ON TASY.PROC_PACIENTE_MATERIAL
(NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPAMA_PROPACI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROC_PACIENTE_MATERIAL ADD (
  CONSTRAINT PROPAMA_PK
 PRIMARY KEY
 (NR_SEQ_PROCEDIMENTO, CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_PACIENTE_MATERIAL ADD (
  CONSTRAINT PROPAMA_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROPAMA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PROC_PACIENTE_MATERIAL TO NIVEL_1;

