ALTER TABLE TASY.ARM_SCOPE_STAGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARM_SCOPE_STAGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARM_SCOPE_STAGE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DS_STAGE             VARCHAR2(255 BYTE)       NOT NULL,
  DS_DESCRIPTION       VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ARMSCOSTA_PK ON TASY.ARM_SCOPE_STAGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ARM_SCOPE_STAGE_tp  after update ON TASY.ARM_SCOPE_STAGE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ARM_SCOPE_STAGE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DESCRIPTION,1,500);gravar_log_alteracao(substr(:old.DS_DESCRIPTION,1,4000),substr(:new.DS_DESCRIPTION,1,4000),:new.nm_usuario,nr_seq_w,'DS_DESCRIPTION',ie_log_w,ds_w,'ARM_SCOPE_STAGE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_STAGE,1,500);gravar_log_alteracao(substr(:old.DS_STAGE,1,4000),substr(:new.DS_STAGE,1,4000),:new.nm_usuario,nr_seq_w,'DS_STAGE',ie_log_w,ds_w,'ARM_SCOPE_STAGE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'ARM_SCOPE_STAGE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ARM_SCOPE_STAGE ADD (
  CONSTRAINT ARMSCOSTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ARM_SCOPE_STAGE TO NIVEL_1;

