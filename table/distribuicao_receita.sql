ALTER TABLE TASY.DISTRIBUICAO_RECEITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DISTRIBUICAO_RECEITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.DISTRIBUICAO_RECEITA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LOTE            NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA_BANCO     NUMBER(10)             NOT NULL,
  NR_SEQ_COBR_ESCRIT     NUMBER(10),
  NR_SEQ_FORMA_COBRANCA  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DISTRREC_BANESTA_FK_I ON TASY.DISTRIBUICAO_RECEITA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DISTRREC_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DISTRREC_COBESCR_FK_I ON TASY.DISTRIBUICAO_RECEITA
(NR_SEQ_COBR_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DISTRREC_COBESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DISTRREC_LTDISTREC_FK_I ON TASY.DISTRIBUICAO_RECEITA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DISTRREC_LTDISTREC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DISTRREC_PK ON TASY.DISTRIBUICAO_RECEITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DISTRREC_PK
  MONITORING USAGE;


ALTER TABLE TASY.DISTRIBUICAO_RECEITA ADD (
  CONSTRAINT DISTRREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DISTRIBUICAO_RECEITA ADD (
  CONSTRAINT DISTRREC_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT DISTRREC_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBR_ESCRIT) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT DISTRREC_LTDISTREC_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_DISTRIBUICAO_RECEITA (NR_SEQUENCIA));

GRANT SELECT ON TASY.DISTRIBUICAO_RECEITA TO NIVEL_1;

