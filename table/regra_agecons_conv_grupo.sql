ALTER TABLE TASY.REGRA_AGECONS_CONV_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_AGECONS_CONV_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_AGECONS_CONV_GRUPO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA_AGECONS  NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RGAGCGR_AGEINGR_FK_I ON TASY.REGRA_AGECONS_CONV_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RGAGCGR_PK ON TASY.REGRA_AGECONS_CONV_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RGAGCGR_REAGCON_FK_I ON TASY.REGRA_AGECONS_CONV_GRUPO
(NR_SEQ_REGRA_AGECONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_AGECONS_CONV_GRUPO ADD (
  CONSTRAINT RGAGCGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_AGECONS_CONV_GRUPO ADD (
  CONSTRAINT RGAGCGR_AGEINGR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.AGENDA_INT_GRUPO (NR_SEQUENCIA),
  CONSTRAINT RGAGCGR_REAGCON_FK 
 FOREIGN KEY (NR_SEQ_REGRA_AGECONS) 
 REFERENCES TASY.REGRA_AGECONS_CONVENIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_AGECONS_CONV_GRUPO TO NIVEL_1;

