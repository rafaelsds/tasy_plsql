ALTER TABLE TASY.DATASOURCE_CLIENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DATASOURCE_CLIENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.DATASOURCE_CLIENT
(
  ID_DATASOURCE  VARCHAR2(36 BYTE)              NOT NULL,
  ID_CLIENT      VARCHAR2(36 BYTE)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DATACLI_CLIENT_FK_I ON TASY.DATASOURCE_CLIENT
(ID_CLIENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DATACLI_DATASOU_FK_I ON TASY.DATASOURCE_CLIENT
(ID_DATASOURCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DATACLI_PK ON TASY.DATASOURCE_CLIENT
(ID_DATASOURCE, ID_CLIENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DATASOURCE_CLIENT ADD (
  CONSTRAINT DATACLI_PK
 PRIMARY KEY
 (ID_DATASOURCE, ID_CLIENT)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DATASOURCE_CLIENT ADD (
  CONSTRAINT DATACLI_CLIENT_FK 
 FOREIGN KEY (ID_CLIENT) 
 REFERENCES TASY.CLIENT (ID),
  CONSTRAINT DATACLI_DATASOU_FK 
 FOREIGN KEY (ID_DATASOURCE) 
 REFERENCES TASY.DATASOURCE (ID));

GRANT SELECT ON TASY.DATASOURCE_CLIENT TO NIVEL_1;

