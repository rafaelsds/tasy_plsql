ALTER TABLE TASY.ATEND_PREVISAO_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PREVISAO_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PREVISAO_ALTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_REGISTRO            DATE                   NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_PREVISTO_ALTA       DATE,
  NR_SEQ_CLASSIF_MEDICO  NUMBER(10),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_PROBABILIDADE_ALTA  VARCHAR2(3 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_ALTA_HOSPITALAR     VARCHAR2(2 BYTE)       NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_ESPECIALIDADE       NUMBER(5),
  NR_SEQ_REG_ELEMENTO    NUMBER(10),
  NR_DIAS_PREV_ALTA      NUMBER(3),
  DT_ULTIMA_REFEICAO     DATE,
  IE_REFEICAO            VARCHAR2(10 BYTE),
  IE_EUP                 VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPREAL_ATEPACI_FK_I ON TASY.ATEND_PREVISAO_ALTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPREAL_EHRREEL_FK_I ON TASY.ATEND_PREVISAO_ALTA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPREAL_ESPMEDI_FK_I ON TASY.ATEND_PREVISAO_ALTA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPREAL_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATPREAL_PESFISI_FK_I ON TASY.ATEND_PREVISAO_ALTA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPREAL_PK ON TASY.ATEND_PREVISAO_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_previsao_alta_atual
before insert or update ON TASY.ATEND_PREVISAO_ALTA for each row
declare

dt_previsto_alta_w		date;
ie_probabilidade_alta_w		varchar2(3);
nr_seq_classif_medico_w		number(10);
ds_obs_prev_alta_w		varchar2(255);
new_ds_observacao_w		varchar2(255);
nr_dias_prev_alta_w 		number(3);
ie_consistir_w			boolean := true;

pragma autonomous_transaction;
begin


if	inserting then
	if	(:new.cd_especialidade is null) then
		:new.cd_especialidade	:= nvl(wheb_usuario_pck.get_cd_especialidade, Obter_Especialidade_Medico(:new.cd_profissional,'C'));
	end if;

elsif	(:new.nr_atendimento is not null) then

	if(:new.dt_previsto_alta is not null) then
	   nr_dias_prev_alta_w := :new.nr_dias_prev_alta;
	end if;

	if	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) and
		(nvl(:new.ie_alta_hospitalar,'N') = 'S') then
		update	atendimento_paciente set
			dt_previsto_alta	= :new.dt_previsto_alta,
			ie_probabilidade_alta	= :new.ie_probabilidade_alta,
			nr_seq_classif_medico	= :new.nr_seq_classif_medico,
			ds_obs_prev_alta	= :new.ds_observacao
		where	nr_atendimento = :new.nr_atendimento;

		ie_consistir_w := false;

	        commit;


	       if(:new.dt_previsto_alta is not null) then
			update	atendimento_paciente set
				NR_DIAS_PREV_ALTA = nr_dias_prev_alta_w
			where	nr_atendimento = :new.nr_atendimento;
		end if;

		wl_gerar_finalizar_tarefa('ED', 'F', :new.nr_atendimento, 0, :new.nm_usuario, sysdate, 'N',null,null,null,null,null,null,null,null,null,null,null,null,null,null,:new.nr_sequencia);

		commit;
	elsif	(nvl(:old.ie_situacao,'A') = 'A') and
		(nvl(:new.ie_situacao,'A') = 'I') and
		(nvl(:new.ie_alta_hospitalar,'N') = 'S') then

		select	max(dt_previsto_alta),
			max(ie_probabilidade_alta),
			max(nr_seq_classif_medico),
			max(ds_obs_prev_alta)
		into	dt_previsto_alta_w,
			ie_probabilidade_alta_w,
			nr_seq_classif_medico_w,
			ds_obs_prev_alta_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		if	(dt_previsto_alta_w = :new.dt_previsto_alta) and
			(nvl(ie_probabilidade_alta_w,'XPTO') = nvl(:new.ie_probabilidade_alta,'XPTO')) and
			(nvl(nr_seq_classif_medico_w,0) = nvl(:new.nr_seq_classif_medico,0)) and
			(nvl(ds_obs_prev_alta_w,'xpto') = nvl(:new.ds_observacao,'xpto')) then

			select	max(dt_previsto_alta),
				max(ie_probabilidade_alta),
				max(nr_seq_classif_medico),
				max(ds_observacao)
			into	dt_previsto_alta_w,
				ie_probabilidade_alta_w,
				nr_seq_classif_medico_w,
				ds_obs_prev_alta_w
			from	atend_previsao_alta
			where	nr_atendimento = :new.nr_atendimento
			and	nr_sequencia = (select	max(nr_sequencia)
						from	atend_previsao_alta
						where	nr_atendimento = :new.nr_atendimento
						and	nvl(ie_situacao,'A') = 'A'
						and	dt_liberacao is not null
						and	nvl(ie_alta_hospitalar,'N') = 'S'
						and	nr_sequencia <> :new.nr_sequencia);

			if	(dt_previsto_alta_w is not null) or
				(ie_probabilidade_alta_w is not null) or
				(nr_seq_classif_medico_w is not null) or
				(ds_obs_prev_alta_w is not null) then
				update	atendimento_paciente set
					dt_previsto_alta	= dt_previsto_alta_w,
					ie_probabilidade_alta	= ie_probabilidade_alta_w,
					nr_seq_classif_medico	= nr_seq_classif_medico_w,
					ds_obs_prev_alta	= ds_obs_prev_alta_w,
					NR_DIAS_PREV_ALTA = nr_dias_prev_alta_w
				where	nr_atendimento = :new.nr_atendimento;

				ie_consistir_w := false;
			else
				update	atendimento_paciente set
					dt_previsto_alta	= null,
					ie_probabilidade_alta	= null,
					nr_seq_classif_medico	= null,
					ds_obs_prev_alta	= null,
					NR_DIAS_PREV_ALTA = null
				where	nr_atendimento = :new.nr_atendimento;

				ie_consistir_w := false;

			end if;

			commit;
		end if;
	end if;

	if	(ie_consistir_w) then
		if	((:new.dt_previsto_alta <> :old.dt_previsto_alta) or
			 ((:old.dt_previsto_alta is null) and (:new.dt_previsto_alta is not null)) or
			 ((:old.dt_liberacao is null) and (:new.dt_liberacao is not null)) or
			 (nvl(:new.ie_situacao,'A') <> nvl(:old.ie_situacao,'A')))  then
			 consiste_tipo_visita_atend(nr_atendimento_p => :new.nr_atendimento,
						    dt_previsto_alta_p => :new.dt_previsto_alta,
						    ie_alteracao_p => 'D',
						    ie_alt_data_prev_p => 'S');
		end if;
	end if;
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PREVISAO_ALTA_AFTIN
after update ON TASY.ATEND_PREVISAO_ALTA for each row
declare

    ds_param_integration_w  varchar2(500);

begin

    if (((:new.dt_liberacao is not null) and (:old.dt_liberacao is null)) or
        ((:new.dt_inativacao is not null) and (:old.dt_inativacao is null))) then

        ds_param_integration_w :=  '{"recordId" : "' || :new.nr_sequencia|| '"' || '}';

        execute_bifrost_integration(253,ds_param_integration_w);

    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PREVISAO_ALTA_tp  after update ON TASY.ATEND_PREVISAO_ALTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PROBABILIDADE_ALTA,1,4000),substr(:new.IE_PROBABILIDADE_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROBABILIDADE_ALTA',ie_log_w,ds_w,'ATEND_PREVISAO_ALTA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_PREVISTO_ALTA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PREVISTO_ALTA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PREVISTO_ALTA',ie_log_w,ds_w,'ATEND_PREVISAO_ALTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_MEDICO,1,4000),substr(:new.NR_SEQ_CLASSIF_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_MEDICO',ie_log_w,ds_w,'ATEND_PREVISAO_ALTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ATEND_PREVISAO_ALTA ADD (
  CONSTRAINT ATPREAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PREVISAO_ALTA ADD (
  CONSTRAINT ATPREAL_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATPREAL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATPREAL_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPREAL_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.ATEND_PREVISAO_ALTA TO NIVEL_1;

