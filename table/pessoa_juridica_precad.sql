ALTER TABLE TASY.PESSOA_JURIDICA_PRECAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA_PRECAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA_PRECAD
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_CGC                      VARCHAR2(14 BYTE),
  DS_RAZAO_SOCIAL             VARCHAR2(80 BYTE) NOT NULL,
  NM_FANTASIA                 VARCHAR2(80 BYTE) NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  CD_CEP                      VARCHAR2(15 BYTE),
  DS_ENDERECO                 VARCHAR2(40 BYTE) NOT NULL,
  DS_BAIRRO                   VARCHAR2(40 BYTE),
  DS_MUNICIPIO                VARCHAR2(40 BYTE) NOT NULL,
  SG_ESTADO                   VARCHAR2(15 BYTE) NOT NULL,
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_TELEFONE                 VARCHAR2(15 BYTE),
  NR_ENDERECO                 VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  NR_FAX                      VARCHAR2(15 BYTE),
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DS_EMAIL                    VARCHAR2(60 BYTE),
  NM_PESSOA_CONTATO           VARCHAR2(255 BYTE),
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  NR_RAMAL_CONTATO            NUMBER(5),
  NR_INSCRICAO_ESTADUAL       VARCHAR2(20 BYTE),
  DT_LIBERACAO                DATE,
  NR_SEQ_PROCESSO             NUMBER(10)        NOT NULL,
  IE_PROD_FABRIC              VARCHAR2(3 BYTE),
  NR_INSCRICAO_MUNICIPAL      VARCHAR2(20 BYTE),
  DS_SITE_INTERNET            VARCHAR2(255 BYTE),
  DS_NOME_ABREV               VARCHAR2(18 BYTE),
  DS_RESP_TECNICO             VARCHAR2(255 BYTE),
  NR_ALVARA_SANITARIO         VARCHAR2(20 BYTE),
  DS_ORGAO_REG_RESP_TECNICO   VARCHAR2(10 BYTE),
  NR_CERTIFICADO_BOAS_PRAT    VARCHAR2(20 BYTE),
  NR_ALVARA_SANITARIO_MUNIC   VARCHAR2(20 BYTE),
  NR_AUTOR_FUNC               VARCHAR2(20 BYTE),
  DT_VALIDADE_ALVARA_SANIT    DATE,
  DT_VALIDADE_AUTOR_FUNC      DATE,
  DT_VALIDADE_CERT_BOAS_PRAT  DATE,
  DT_VALIDADE_RESP_TECNICO    DATE,
  NR_REGISTRO_RESP_TECNICO    VARCHAR2(20 BYTE),
  NR_SEQ_PAIS                 NUMBER(10),
  NR_CEI                      VARCHAR2(50 BYTE),
  NR_SEQ_IDIOMA               NUMBER(10),
  NR_REGISTRO_PLS             VARCHAR2(20 BYTE),
  NR_SEQ_CNAE                 NUMBER(10),
  NR_SEQ_NAT_JURIDICA         NUMBER(10),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDI_FAX                  VARCHAR2(3 BYTE),
  NR_DDD_FAX                  VARCHAR2(3 BYTE),
  DS_SENHA                    VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_LOGRADOURO      NUMBER(10),
  NM_USUARIO_REVISAO          VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_ASEN            NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DS_OBSERVACAO_COMPL         VARCHAR2(4000 BYTE),
  NR_CCM                      NUMBER(10),
  NR_SEQ_REGIAO               NUMBER(10),
  CD_UF_IBGE                  NUMBER(2),
  NR_SEQ_IDENT_CNES           NUMBER(10),
  NR_MATRICULA_CEI            VARCHAR2(15 BYTE),
  DS_ORIENTACAO_COBRANCA      VARCHAR2(4000 BYTE),
  NR_AUTOR_TRANSP_RESID       VARCHAR2(20 BYTE),
  NR_AUTOR_RECEB_RESID        VARCHAR2(20 BYTE),
  NR_SEQ_PESSOA_ENDERECO      NUMBER(10),
  DS_EMAIL_NFE                VARCHAR2(255 BYTE),
  CD_TIPO_PESSOA              NUMBER(3),
  NM_USUARIO_APROV            VARCHAR2(15 BYTE),
  DT_APROVACAO                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PJPRCAD_CATIASS_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_TIPO_ASEN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_CNESIDE_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_CNSTILO_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_ESTABEL_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_PAIS_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_PESSEND_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PJPRCAD_PK ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_PLSCNAE_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_CNAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_PROPREC_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_SUPNATJ_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_NAT_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_SUSMURE_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_TASYIDI_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJPRCAD_TIPPEJU_FK_I ON TASY.PESSOA_JURIDICA_PRECAD
(CD_TIPO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_JURIDICA_PRECAD_tp  after update ON TASY.PESSOA_JURIDICA_PRECAD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO,1,4000),substr(:new.NR_ALVARA_SANITARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PESSOA,1,4000),substr(:new.CD_TIPO_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PESSOA',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_RESP_TECNICO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_RESP_TECNICO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERT_BOAS_PRAT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERT_BOAS_PRAT',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_AUTOR_FUNC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_AUTOR_FUNC',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_ALVARA_SANIT,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_ALVARA_SANIT',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SITE_INTERNET,1,4000),substr(:new.DS_SITE_INTERNET,1,4000),:new.nm_usuario,nr_seq_w,'DS_SITE_INTERNET',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_RAZAO_SOCIAL,1,4000),substr(:new.DS_RAZAO_SOCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RAZAO_SOCIAL',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_REG_RESP_TECNICO,1,4000),substr(:new.DS_ORGAO_REG_RESP_TECNICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_REG_RESP_TECNICO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NOME_ABREV,1,4000),substr(:new.DS_NOME_ABREV,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOME_ABREV',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROD_FABRIC,1,4000),substr(:new.IE_PROD_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROD_FABRIC',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_LOGRADOURO,1,4000),substr(:new.NR_SEQ_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_LOGRADOURO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_MUNICIPAL,1,4000),substr(:new.NR_INSCRICAO_MUNICIPAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_MUNICIPAL',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_ESTADUAL,1,4000),substr(:new.NR_INSCRICAO_ESTADUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_ESTADUAL',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FAX,1,4000),substr(:new.NR_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_TELEFONE,1,4000),substr(:new.NR_DDI_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_FAX,1,4000),substr(:new.NR_DDI_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_TELEFONE,1,4000),substr(:new.NR_DDD_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_FAX,1,4000),substr(:new.NR_DDD_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_BOAS_PRAT,1,4000),substr(:new.NR_CERTIFICADO_BOAS_PRAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_BOAS_PRAT',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CCM,1,4000),substr(:new.NR_CCM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CCM',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUTOR_FUNC,1,4000),substr(:new.NR_AUTOR_FUNC,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUTOR_FUNC',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ALVARA_SANITARIO_MUNIC,1,4000),substr(:new.NR_ALVARA_SANITARIO_MUNIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ALVARA_SANITARIO_MUNIC',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FANTASIA,1,4000),substr(:new.NM_FANTASIA,1,4000),:new.nm_usuario,nr_seq_w,'NM_FANTASIA',ie_log_w,ds_w,'PESSOA_JURIDICA_PRECAD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JURIDICA_PRECAD ADD (
  CONSTRAINT PJPRCAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PESSOA_JURIDICA_PRECAD ADD (
  CONSTRAINT PJPRCAD_CATIASS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ASEN) 
 REFERENCES TASY.CAT_TIPO_ASSENTAMENTO (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_CNSTILO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LOGRADOURO) 
 REFERENCES TASY.CNS_TIPO_LOGRADOURO (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PJPRCAD_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_PLSCNAE_FK 
 FOREIGN KEY (NR_SEQ_CNAE) 
 REFERENCES TASY.PLS_CNAE (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_PROPREC_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_PRE_CADASTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PJPRCAD_SUPNATJ_FK 
 FOREIGN KEY (NR_SEQ_NAT_JURIDICA) 
 REFERENCES TASY.SUP_NATUREZA_JURIDICA (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT PJPRCAD_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PESSOA) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA));

GRANT SELECT ON TASY.PESSOA_JURIDICA_PRECAD TO NIVEL_1;

