ALTER TABLE TASY.PLS_CONTRATO_DOC_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTRATO_DOC_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTRATO_DOC_ARQ
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_DOCUMENTO       NUMBER(10),
  DS_ARQUIVO             VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO        NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  NM_ARQUIVO             VARCHAR2(255 BYTE),
  DT_DOCUMENTO           DATE,
  IE_APRESENTA_PORTAL    VARCHAR2(1 BYTE),
  DT_INI_APRESENTA       DATE,
  DT_FIN_APRESENTA       DATE,
  DT_INI_APRESENTA_REF   DATE,
  DT_FIN_APRESENTA_REF   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCODA_PK ON TASY.PLS_CONTRATO_DOC_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCODA_PLSCONT_FK_I ON TASY.PLS_CONTRATO_DOC_ARQ
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCODA_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCODA_TIPODOC_FK_I ON TASY.PLS_CONTRATO_DOC_ARQ
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCODA_TIPODOC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_contrato_doc_arq_atual
before insert or update ON TASY.PLS_CONTRATO_DOC_ARQ for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo
:new.dt_ini_apresenta_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_ini_apresenta, 'I');
:new.dt_fin_apresenta_ref    := pls_util_pck.obter_dt_vigencia_null( :new.dt_fin_apresenta, 'F');

end pls_contrato_doc_arq_atual;
/


ALTER TABLE TASY.PLS_CONTRATO_DOC_ARQ ADD (
  CONSTRAINT PLSCODA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTRATO_DOC_ARQ ADD (
  CONSTRAINT PLSCODA_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCODA_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTRATO_DOC_ARQ TO NIVEL_1;

