DROP TABLE TASY.W_SUITES CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_SUITES
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_UNIDADE_BASICA     VARCHAR2(20 BYTE),
  CD_UNIDADE_COMPL      VARCHAR2(20 BYTE),
  DIA1                  VARCHAR2(20 BYTE),
  DIA2                  VARCHAR2(20 BYTE),
  DIA3                  VARCHAR2(20 BYTE),
  DIA4                  VARCHAR2(20 BYTE),
  DIA5                  VARCHAR2(20 BYTE),
  DIA6                  VARCHAR2(20 BYTE),
  DIA7                  VARCHAR2(20 BYTE),
  DIA8                  VARCHAR2(20 BYTE),
  DIA9                  VARCHAR2(20 BYTE),
  DIA10                 VARCHAR2(20 BYTE),
  DIA11                 VARCHAR2(20 BYTE),
  DIA12                 VARCHAR2(20 BYTE),
  DIA13                 VARCHAR2(20 BYTE),
  DIA14                 VARCHAR2(20 BYTE),
  DIA15                 VARCHAR2(20 BYTE),
  DIA16                 VARCHAR2(20 BYTE),
  DIA17                 VARCHAR2(20 BYTE),
  DIA18                 VARCHAR2(20 BYTE),
  DIA19                 VARCHAR2(20 BYTE),
  DIA20                 VARCHAR2(20 BYTE),
  DIA21                 VARCHAR2(20 BYTE),
  DIA22                 VARCHAR2(20 BYTE),
  DIA23                 VARCHAR2(20 BYTE),
  DIA24                 VARCHAR2(20 BYTE),
  DIA25                 VARCHAR2(20 BYTE),
  DIA26                 VARCHAR2(20 BYTE),
  DIA27                 VARCHAR2(20 BYTE),
  DIA28                 VARCHAR2(20 BYTE),
  DIA29                 VARCHAR2(20 BYTE),
  DIA30                 VARCHAR2(20 BYTE),
  DIA31                 VARCHAR2(20 BYTE),
  DT_REFERENCIA         DATE
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_SUITES TO NIVEL_1;

