ALTER TABLE TASY.REGRA_GERAR_AUTOR_AGRUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_GERAR_AUTOR_AGRUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_GERAR_AUTOR_AGRUP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  IE_VALOR             VARCHAR2(2 BYTE)         NOT NULL,
  QT_PONTO_MIN         NUMBER(15,4)             NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  IE_EVENTO            VARCHAR2(2 BYTE)         NOT NULL,
  IE_FORMA_PERIODO     VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGERAA_CONVENI_FK_I ON TASY.REGRA_GERAR_AUTOR_AGRUP
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGERAA_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGERAA_ESTABEL_FK_I ON TASY.REGRA_GERAR_AUTOR_AGRUP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGERAA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGERAA_PK ON TASY.REGRA_GERAR_AUTOR_AGRUP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGERAA_PK
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_GERAR_AUTOR_AGRUP ADD (
  CONSTRAINT REGERAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_GERAR_AUTOR_AGRUP ADD (
  CONSTRAINT REGERAA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGERAA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.REGRA_GERAR_AUTOR_AGRUP TO NIVEL_1;

