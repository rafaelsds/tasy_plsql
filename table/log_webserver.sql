ALTER TABLE TASY.LOG_WEBSERVER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_WEBSERVER CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_WEBSERVER
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  DS_XML          LONG                          NOT NULL,
  NR_SEQ_PROJETO  NUMBER(10)                    NOT NULL,
  ID_USUARIO      VARCHAR2(20 BYTE)             NOT NULL,
  DT_TRANSACAO    DATE                          NOT NULL,
  IE_STATUS       VARCHAR2(1 BYTE)              NOT NULL,
  NR_SEQ_ORIGEM   NUMBER(10),
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGWEBS_LOGWEBS_FK_I ON TASY.LOG_WEBSERVER
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGWEBS_LOGWEBS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LOGWEBS_PK ON TASY.LOG_WEBSERVER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGWEBS_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOG_WEBSERVER ADD (
  CONSTRAINT LOGWEBS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_WEBSERVER ADD (
  CONSTRAINT LOGWEBS_LOGWEBS_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.LOG_WEBSERVER (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOG_WEBSERVER TO NIVEL_1;

