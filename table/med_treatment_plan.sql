ALTER TABLE TASY.MED_TREATMENT_PLAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_TREATMENT_PLAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_TREATMENT_PLAN
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  SI_TYPE_FORM                  VARCHAR2(15 BYTE) NOT NULL,
  CD_DEPARTMENT                 NUMBER(10)      NOT NULL,
  CD_WARD                       NUMBER(5)       NOT NULL,
  CD_ROOM                       VARCHAR2(10 BYTE) NOT NULL,
  DT_HOSPITALIZATION            DATE            NOT NULL,
  DS_ESTIMATED_ADM_PERIOD       VARCHAR2(255 BYTE) NOT NULL,
  DS_INDICATION                 VARCHAR2(4000 BYTE),
  DS_PURPOSE_ADMISSION          VARCHAR2(4000 BYTE),
  DS_INF_TEST_SCHEDULE          VARCHAR2(4000 BYTE),
  DS_INF_SURGICAL_SCHEDULE      VARCHAR2(4000 BYTE),
  DS_INF_NURSE                  VARCHAR2(4000 BYTE),
  DS_INF_PARAMEDICAL            VARCHAR2(4000 BYTE),
  DS_NOTES                      VARCHAR2(4000 BYTE),
  SI_SPECIAL_NUTRITION_CONTROL  VARCHAR2(1 BYTE) NOT NULL,
  SI_ADDITIONAL_CHARGES_MENU    VARCHAR2(1 BYTE) NOT NULL,
  DS_HOSP_MEDICAL_PROTECT       VARCHAR2(255 BYTE),
  DS_ACTIONS_DISCHARGE          VARCHAR2(4000 BYTE),
  DS_INF_HOME_RECOVERY_SUP      VARCHAR2(4000 BYTE),
  DT_LIBERACAO                  DATE,
  NM_USUARIO_LIBERACAO          VARCHAR2(15 BYTE),
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_PREVISAO_ALTA              DATE,
  DS_PLAN_POS_ALTA              VARCHAR2(4000 BYTE),
  DS_NOTAS_POS_ALTA             VARCHAR2(4000 BYTE),
  DS_SERV_SOCIAL_POS_ALT        VARCHAR2(4000 BYTE),
  DS_ENFERMAGEM_POS_ALTA        VARCHAR2(4000 BYTE),
  DS_OBS_POS_ALTA               VARCHAR2(4000 BYTE),
  IE_PATIENT_STATUS             VARCHAR2(1 BYTE),
  CD_EVOLUCAO                   NUMBER(10),
  NR_SEQ_GES_VAG                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDTRPL_ATEPACI_FK_I ON TASY.MED_TREATMENT_PLAN
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDTRPL_DEPMED_FK_I ON TASY.MED_TREATMENT_PLAN
(CD_DEPARTMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDTRPL_EVOPACI_FK_I ON TASY.MED_TREATMENT_PLAN
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDTRPL_GESVAGA_FK_I ON TASY.MED_TREATMENT_PLAN
(NR_SEQ_GES_VAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDTRPL_PK ON TASY.MED_TREATMENT_PLAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDTRPL_SETATEN_FK_I ON TASY.MED_TREATMENT_PLAN
(CD_WARD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MED_TREATMENT_PLAN_AFINSERT
after insert ON TASY.MED_TREATMENT_PLAN for each row
declare

nr_sequencia_w	med_treat_plan_rules.nr_sequencia%type;

Cursor C01 is
select	ds_who_confirm_plan,
	sy_requires_confirmation
from	med_treat_plan_rules_conf
where	nr_seq_rule = nr_sequencia_w;
c01_w	c01%rowtype;


begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	med_treat_plan_rules
where	sy_type_form = :new.si_type_form
and	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nr_sequencia_w > 0) then

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin

		insert into med_treatment_plan_conf(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_med_treat_plan,
			ds_who_confirm_plan,
			sy_requires_confirmation)
		values(	med_treatment_plan_conf_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			c01_w.ds_who_confirm_plan,
			c01_w.sy_requires_confirmation);

		end;
	end loop;
	close C01;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MED_TREATMENT_PLAN_BEF_UPDATE
before update ON TASY.MED_TREATMENT_PLAN for each row
declare

begin

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	:new.nm_usuario_liberacao := :new.nm_usuario;
	:new.ie_patient_status := 'F';

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MED_TREATMENT_PLAN_AFTUPDT
after update ON TASY.MED_TREATMENT_PLAN for each row
declare ie_form_type_w varchar2(15);
begin
if    (wheb_usuario_pck.get_ie_executar_trigger    = 'S')  then

select decode(:new.SI_TYPE_FORM,'5','PD_MED_TP','IP_MED_TP')  into ie_form_type_w from dual;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

     update evolucao_paciente
     set dt_inativacao = sysdate,
     ie_situacao ='I',
     dt_atualizacao = sysdate ,
     nm_usuario = wheb_usuario_pck.get_nm_usuario,
     nm_usuario_inativacao =  wheb_usuario_pck.get_nm_usuario,
     DS_JUSTIFICATIVA = :new.DS_JUSTIFICATIVA
     where cd_evolucao = :new.cd_evolucao;

     delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and IE_MED_REC_TYPE = ie_form_type_w  and IE_STAGE = 1  and IE_SOAP_TYPE = 'P' and NR_SEQ_MED_ITEM = :new.nr_sequencia;

end if;

end if;

end;
/


ALTER TABLE TASY.MED_TREATMENT_PLAN ADD (
  CONSTRAINT MEDTRPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MED_TREATMENT_PLAN ADD (
  CONSTRAINT MEDTRPL_DEPSET_FK 
 FOREIGN KEY (CD_DEPARTMENT) 
 REFERENCES TASY.DEPARTAMENTO_SETOR (NR_SEQUENCIA),
  CONSTRAINT MEDTRPL_SETATEN_FK 
 FOREIGN KEY (CD_WARD) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MEDTRPL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT MEDTRPL_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTMENT) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT MEDTRPL_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT MEDTRPL_GESVAGA_FK 
 FOREIGN KEY (NR_SEQ_GES_VAG) 
 REFERENCES TASY.GESTAO_VAGA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_TREATMENT_PLAN TO NIVEL_1;

