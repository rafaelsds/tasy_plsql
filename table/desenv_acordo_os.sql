ALTER TABLE TASY.DESENV_ACORDO_OS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_ACORDO_OS CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_ACORDO_OS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ACORDO         NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_INCLUSAO           DATE                    NOT NULL,
  DT_ENTREGA_PREV       DATE,
  CD_PESSOA_INCLUSAO    VARCHAR2(10 BYTE),
  IE_STATUS_ACORDO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA      VARCHAR2(2000 BYTE),
  IE_ACORDO_ADIC        VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_ORDEM_SERVICO  NUMBER(10),
  DS_COMENTARIO         VARCHAR2(4000 BYTE),
  DS_PROJETO            VARCHAR2(2000 BYTE),
  IE_TIPO_SOLUCAO       VARCHAR2(3 BYTE),
  IE_TIPO_ACORDO        NUMBER(5),
  DT_ENTREGA_DESEJADA   DATE,
  CD_PESSOA_SOLIC       VARCHAR2(10 BYTE),
  NR_SEQ_PROJETO        NUMBER(10),
  DT_POSICIONAMENTO     DATE,
  DT_RETORNO_CLI        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESACOS_DESACOR_FK_I ON TASY.DESENV_ACORDO_OS
(NR_SEQ_ACORDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESACOS_DESACOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DESACOS_MANORSE_FK_I ON TASY.DESENV_ACORDO_OS
(NR_SEQ_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESACOS_PESFISI_FK_I ON TASY.DESENV_ACORDO_OS
(CD_PESSOA_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESACOS_PESFISI_FK2_I ON TASY.DESENV_ACORDO_OS
(CD_PESSOA_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESACOS_PK ON TASY.DESENV_ACORDO_OS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESACOS_PK
  MONITORING USAGE;


CREATE INDEX TASY.DESACOS_PROPROJ_FK_I ON TASY.DESENV_ACORDO_OS
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DESENV_ACORDO_OS_tp  after update ON TASY.DESENV_ACORDO_OS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_STATUS_ACORDO,1,4000),substr(:new.IE_STATUS_ACORDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_ACORDO',ie_log_w,ds_w,'DESENV_ACORDO_OS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.DESENV_ACORDO_OS_BEFORE
before update or insert ON TASY.DESENV_ACORDO_OS for each row
declare

begin
if	(updating and (:old.ie_status_acordo = 'I') and
	((:new.ie_status_acordo = 'A') or
	(:new.ie_status_acordo = 'R'))) then
	:new.dt_posicionamento	:= sysdate;
end if;

if	(not (substr(dbms_utility.format_call_stack,1,4000) like '%MAN_ORDEM_SERVICO_ATUAL%'))then
	update	man_ordem_servico
	set 	dt_externa_acordo	= :new.dt_entrega_prev,
		dt_atualizacao		= sysdate,
		nm_usuario		= wheb_usuario_pck.get_nm_usuario
	where 	nr_sequencia		= :new.nr_seq_ordem_servico;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.DESENV_ACORDO_OS_DELETE
before delete ON TASY.DESENV_ACORDO_OS for each row
declare

begin

if	(not (substr(dbms_utility.format_call_stack,1,4000) like '%MAN_ORDEM_SERVICO_ATUAL%'))then
	update	man_ordem_servico
	set 	dt_externa_acordo	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= wheb_usuario_pck.get_nm_usuario
	where 	nr_sequencia		= :old.nr_seq_ordem_servico;
end if;

end;
/


ALTER TABLE TASY.DESENV_ACORDO_OS ADD (
  CONSTRAINT DESACOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_ACORDO_OS ADD (
  CONSTRAINT DESACOS_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT DESACOS_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DESACOS_DESACOR_FK 
 FOREIGN KEY (NR_SEQ_ACORDO) 
 REFERENCES TASY.DESENV_ACORDO (NR_SEQUENCIA),
  CONSTRAINT DESACOS_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT DESACOS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_INCLUSAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.DESENV_ACORDO_OS TO NIVEL_1;

