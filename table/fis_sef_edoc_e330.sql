ALTER TABLE TASY.FIS_SEF_EDOC_E330
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_SEF_EDOC_E330 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_SEF_EDOC_E330
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  CD_IND_TOT           VARCHAR2(1 BYTE),
  VL_CONT              NUMBER(15,2),
  VL_OP_ISS            NUMBER(15,2),
  VL_BC_ICMS           NUMBER(15,2),
  VL_ICMS              NUMBER(15,2),
  VL_ICMS_ST           NUMBER(15,2),
  VL_ST_ENT            NUMBER(15,2),
  VL_ST_FNT            NUMBER(15,2),
  VL_ST_UF             NUMBER(15,2),
  VL_ST_OE             NUMBER(15,2),
  VL_AT                NUMBER(15,2),
  VL_ISNT_ICMS         NUMBER(15,2),
  VL_OUT_ICMS          NUMBER(15,2),
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SEFE330_CONEDOC_FK_I ON TASY.FIS_SEF_EDOC_E330
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SEFE330_PK ON TASY.FIS_SEF_EDOC_E330
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_SEF_EDOC_E330 ADD (
  CONSTRAINT SEFE330_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_SEF_EDOC_E330 ADD (
  CONSTRAINT SEFE330_CONEDOC_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_SEF_EDOC_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_SEF_EDOC_E330 TO NIVEL_1;

