DROP TABLE TASY.W_FUNCIONARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_FUNCIONARIO
(
  NM_PESSOA_FISICA       VARCHAR2(60 BYTE),
  NR_CPF                 VARCHAR2(11 BYTE),
  DT_NASCIMENTO          DATE,
  CD_CARGO_EXT           NUMBER(10),
  CD_SETOR_EXT           NUMBER(10),
  CD_ESTABELECIMENTO     NUMBER(4),
  NM_USUARIO             VARCHAR2(15 BYTE),
  DT_ADMISSAO            DATE,
  DT_DEMISSAO            DATE,
  DS_CONSELHO_PROF       VARCHAR2(100 BYTE),
  NR_SEQ_CONSELHO        NUMBER(10),
  SG_CONSELHO            VARCHAR2(3 BYTE),
  IE_RETORNO_INTEGRACAO  NUMBER(10),
  IE_TIPO_INTEGRACAO     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_FUNCIONARIO TO NIVEL_1;

