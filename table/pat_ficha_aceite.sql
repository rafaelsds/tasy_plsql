ALTER TABLE TASY.PAT_FICHA_ACEITE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_FICHA_ACEITE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_FICHA_ACEITE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_REGISTRO          DATE                     NOT NULL,
  IE_ACEITE            VARCHAR2(1 BYTE),
  NR_SEQ_BEM           NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  DT_LIBERACAO         DATE,
  IE_ORIGEM            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATFIACE_PATBEM_FK_I ON TASY.PAT_FICHA_ACEITE
(NR_SEQ_BEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATFIACE_PATLOCA_FK_I ON TASY.PAT_FICHA_ACEITE
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATFIACE_PATLOCA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PATFIACE_PK ON TASY.PAT_FICHA_ACEITE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATFIACE_PK
  MONITORING USAGE;


ALTER TABLE TASY.PAT_FICHA_ACEITE ADD (
  CONSTRAINT PATFIACE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_FICHA_ACEITE ADD (
  CONSTRAINT PATFIACE_PATLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.PAT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PATFIACE_PATBEM_FK 
 FOREIGN KEY (NR_SEQ_BEM) 
 REFERENCES TASY.PAT_BEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PAT_FICHA_ACEITE TO NIVEL_1;

