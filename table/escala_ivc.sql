ALTER TABLE TASY.ESCALA_IVC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_IVC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_IVC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_PONTOS                 NUMBER(3)           NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  DT_AVALIACAO              DATE                NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_PERDA_URINA            VARCHAR2(1 BYTE)    NOT NULL,
  IE_PROB_VISAO             VARCHAR2(1 BYTE)    NOT NULL,
  IE_PROB_AUDIT             VARCHAR2(1 BYTE)    NOT NULL,
  IE_COND_COMORBIDADE       VARCHAR2(1 BYTE)    NOT NULL,
  IE_SAUDE_TRABALHO         VARCHAR2(1 BYTE)    NOT NULL,
  IE_QUEDA                  VARCHAR2(1 BYTE)    NOT NULL,
  IE_DIFICULDADE            VARCHAR2(1 BYTE)    NOT NULL,
  IE_COND_MOBILIDADE        VARCHAR2(1 BYTE)    NOT NULL,
  IE_INCAPAZ_SEGURAR        VARCHAR2(1 BYTE)    NOT NULL,
  IE_INCAPAZ_ELEVAR         VARCHAR2(1 BYTE)    NOT NULL,
  IE_PERDA_INTERESSE        VARCHAR2(1 BYTE)    NOT NULL,
  IE_DESANIMO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_ATIVIDADE_ESQ          VARCHAR2(1 BYTE)    NOT NULL,
  IE_PIORA_ESQ              VARCHAR2(1 BYTE)    NOT NULL,
  IE_ESQUECIDO              VARCHAR2(1 BYTE)    NOT NULL,
  IE_SAUDE_BANHO            VARCHAR2(1 BYTE)    NOT NULL,
  IE_SAUDE_CONTAS           VARCHAR2(1 BYTE)    NOT NULL,
  IE_SAUDE_COMPRAS          VARCHAR2(1 BYTE)    NOT NULL,
  IE_IDADE                  VARCHAR2(1 BYTE)    NOT NULL,
  IE_COMP_IDADE             VARCHAR2(1 BYTE)    NOT NULL,
  IE_PERDA_PESO             VARCHAR2(1 BYTE),
  IE_PERDA_PESO_CORPORAL    VARCHAR2(1 BYTE),
  IE_INDICE_MASSA_CORPORAL  VARCHAR2(1 BYTE),
  IE_CIRCUFERENCIA_PANT     VARCHAR2(1 BYTE),
  IE_GASTO_VELOCIDADE       VARCHAR2(1 BYTE),
  IE_DOENCA_CRONICA         VARCHAR2(1 BYTE),
  IE_MEDICAMENTOS_DIARIOS   VARCHAR2(1 BYTE),
  IE_INTERNACAO_ANTERIOR    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAIVC_ATEPACI_FK_I ON TASY.ESCALA_IVC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAIVC_PESFISI_FK_I ON TASY.ESCALA_IVC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAIVC_PK ON TASY.ESCALA_IVC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_ivc_atual
before insert or update ON TASY.ESCALA_IVC for each row
declare

qt_pontuacao_w number(10);

begin

Consiste_Liberacao_Escala(180);

qt_pontuacao_w := 0;

--------------------------------------------- IDADE ---------------------------------------------

if (:new.IE_IDADE = 'A') then
	qt_pontuacao_w := 0;
elsif (:new.IE_IDADE = 'B') then
	qt_pontuacao_w := 1;
elsif (:new.IE_IDADE = 'C') then
	qt_pontuacao_w := 3;
end if;

--------------------------------------------- AUTO-PERCEP��O DA SA�DE ---------------------------------------------

if (:new.IE_COMP_IDADE = 'R') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

--------------------------------------------- ATIVIDADES DE VIDA DI�RIA ---------------------------------------------
--------------------------------------------- AVD Instrumental ---------------------------------------------

if (:new.IE_SAUDE_COMPRAS = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 4;
elsif (:new.IE_SAUDE_CONTAS = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 4;
elsif (:new.IE_SAUDE_TRABALHO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 4;
end if;

--------------------------------------------- AVD B�sica ---------------------------------------------

if (:new.IE_SAUDE_BANHO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 6;
end if;

--------------------------------------------- COGNI��O ---------------------------------------------

if (:new.IE_ESQUECIDO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

if (:new.IE_PIORA_ESQ = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

if (:new.IE_ATIVIDADE_ESQ = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- HUMOR ---------------------------------------------

if (:new.IE_DESANIMO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

if (:new.IE_PERDA_INTERESSE = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- MOBILIDADE ---------------------------------------------
--------------------------------------------- Alcance, preens�o e pin�a ---------------------------------------------

if (:new.IE_INCAPAZ_ELEVAR = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

if (:new.IE_INCAPAZ_SEGURAR = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 1;
end if;

---------------------------------------------  Capacidade aer�bica e / ou muscular ---------------------------------------------

if (:new.IE_COND_MOBILIDADE = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- Marcha ---------------------------------------------

if (:new.IE_DIFICULDADE = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

if (:new.IE_QUEDA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

---------------------------------------------  Continencia esfincteriana ---------------------------------------------

if (:new.IE_PERDA_URINA = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- COMUNICA��O ---------------------------------------------
--------------------------------------------- Vis�o ---------------------------------------------

if (:new.IE_PROB_VISAO = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- Audi��o ---------------------------------------------

if (:new.IE_PROB_AUDIT = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 2;
end if;

--------------------------------------------- COMORBIDADES MULTIPLAS ---------------------------------------------

if (:new.IE_COND_COMORBIDADE = 'S') then
	qt_pontuacao_w := qt_pontuacao_w + 4;
end if;

:new.QT_PONTOS := qt_pontuacao_w;


end;
/


ALTER TABLE TASY.ESCALA_IVC ADD (
  CONSTRAINT ESCAIVC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_IVC ADD (
  CONSTRAINT ESCAIVC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAIVC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_IVC TO NIVEL_1;

