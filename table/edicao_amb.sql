ALTER TABLE TASY.EDICAO_AMB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EDICAO_AMB CASCADE CONSTRAINTS;

CREATE TABLE TASY.EDICAO_AMB
(
  CD_EDICAO_AMB        NUMBER(6)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_EDICAO_AMB        VARCHAR2(255 BYTE),
  IE_ORIGEM_PROCED     NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TISS_TABELA   NUMBER(10),
  IE_TIPO_TABELA       VARCHAR2(1 BYTE),
  IE_CALCULO_TUSS      VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_GERAR_A1200       VARCHAR2(1 BYTE),
  DT_NEGOCIACAO        DATE,
  DT_PUBLICACAO        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.EDICAO_AMB.CD_EDICAO_AMB IS 'Codigo edicao AMB';

COMMENT ON COLUMN TASY.EDICAO_AMB.IE_SITUACAO IS 'Indicador de situacao';

COMMENT ON COLUMN TASY.EDICAO_AMB.DT_ATUALIZACAO IS 'Data de atualizacao';

COMMENT ON COLUMN TASY.EDICAO_AMB.NM_USUARIO IS 'Nome do usuario';


CREATE INDEX TASY.EDIAMB_ESTABEL_FK_I ON TASY.EDICAO_AMB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EDIAMB_PK ON TASY.EDICAO_AMB
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EDIAMB_TISSTTA_FK_I ON TASY.EDICAO_AMB
(NR_SEQ_TISS_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EDICAO_AMB_tp  after update ON TASY.EDICAO_AMB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_EDICAO_AMB);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_TISS_TABELA,1,4000),substr(:new.NR_SEQ_TISS_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TISS_TABELA',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EDICAO_AMB,1,4000),substr(:new.DS_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'DS_EDICAO_AMB',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CALCULO_TUSS,1,4000),substr(:new.IE_CALCULO_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CALCULO_TUSS',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NEGOCIACAO,1,4000),substr(:new.DT_NEGOCIACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NEGOCIACAO',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PUBLICACAO,1,4000),substr(:new.DT_PUBLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_PUBLICACAO',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TABELA,1,4000),substr(:new.IE_TIPO_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TABELA',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_A1200,1,4000),substr(:new.IE_GERAR_A1200,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_A1200',ie_log_w,ds_w,'EDICAO_AMB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.EDICAO_AMB ADD (
  CONSTRAINT EDIAMB_PK
 PRIMARY KEY
 (CD_EDICAO_AMB)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EDICAO_AMB ADD (
  CONSTRAINT EDIAMB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EDIAMB_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TABELA) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA));

GRANT SELECT ON TASY.EDICAO_AMB TO NIVEL_1;

