ALTER TABLE TASY.CPOE_FAV_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_FAV_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_FAV_MATERIAL
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_MATERIAL                 NUMBER(6)         NOT NULL,
  CD_UNIDADE_MEDIDA           VARCHAR2(30 BYTE) NOT NULL,
  QT_DOSE                     NUMBER(18,6),
  IE_TIPO_DOSAGEM             VARCHAR2(3 BYTE),
  QT_DOSAGEM                  NUMBER(15,4),
  QT_SOLUCAO_TOTAL            NUMBER(15,4),
  DT_ATUALIZACAO              DATE              NOT NULL,
  QT_TEMPO_APLICACAO          NUMBER(15,4),
  CD_INTERVALO                VARCHAR2(7 BYTE),
  DS_SOLUCAO                  VARCHAR2(100 BYTE),
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_VIA_APLICACAO            VARCHAR2(5 BYTE),
  QT_VOLUME                   NUMBER(15,4),
  IE_URGENCIA                 VARCHAR2(3 BYTE),
  NR_OCORRENCIA               NUMBER(15,4),
  NR_ETAPAS                   NUMBER(3),
  QT_SOLUCAO                  NUMBER(15,4),
  IE_BOMBA_INFUSAO            VARCHAR2(1 BYTE),
  DS_DOSE_DIFERENCIADA        VARCHAR2(50 BYTE),
  IE_MEDICACAO_PACIENTE       VARCHAR2(1 BYTE),
  NR_DIA_UTIL                 NUMBER(10),
  HR_MIN_APLICACAO            VARCHAR2(5 BYTE),
  QT_HORA_FASE                VARCHAR2(5 BYTE),
  QT_DIAS_SOLICITADO          NUMBER(3),
  QT_DIAS_LIBERADO            NUMBER(3),
  IE_SE_NECESSARIO            VARCHAR2(1 BYTE),
  IE_TIPO_SOLUCAO             VARCHAR2(15 BYTE),
  QT_MIN_APLICACAO            NUMBER(4),
  IE_APLIC_BOLUS              VARCHAR2(1 BYTE),
  IE_APLIC_LENTA              VARCHAR2(1 BYTE),
  IE_ACM                      VARCHAR2(1 BYTE),
  QT_HORA_APLICACAO           NUMBER(3),
  IE_OBJETIVO                 VARCHAR2(1 BYTE),
  IE_ORIGEM_INFECCAO          VARCHAR2(1 BYTE),
  CD_TOPOGRAFIA_CIH           NUMBER(10),
  CD_AMOSTRA_CIH              NUMBER(10),
  CD_MICROORGANISMO_CIH       NUMBER(10),
  IE_USO_ANTIMICROBIANO       VARCHAR2(1 BYTE),
  QT_VOL_ADIC_RECONST         NUMBER(15,4),
  IE_INDICACAO                VARCHAR2(1 BYTE),
  IE_LADO                     VARCHAR2(1 BYTE),
  QT_DOSE_ATAQUE              NUMBER(18,6),
  QT_MIN_APLIC_DOSE_ATAQUE    NUMBER(4),
  IE_ANTIBIOTICO              VARCHAR2(1 BYTE),
  DS_ORIENTACAO_PREPARO       VARCHAR2(2000 BYTE),
  IE_TIPO_REGISTRO            VARCHAR2(1 BYTE),
  IE_SOLUCAO                  VARCHAR2(1 BYTE),
  IE_TIPO_ANALGESIA           VARCHAR2(15 BYTE),
  IE_PCA_MODO_PROG            VARCHAR2(15 BYTE),
  QT_DOSE_INICIAL_PCA         NUMBER(10,1),
  IE_UM_DOSE_INICIO_PCA       VARCHAR2(15 BYTE),
  QT_VOL_INFUSAO_PCA          NUMBER(10,1),
  IE_UM_FLUXO_PCA             VARCHAR2(15 BYTE),
  IE_UM_BOLUS_PCA             VARCHAR2(15 BYTE),
  QT_BOLUS_PCA                NUMBER(10,1),
  QT_INTERVALO_BLOQUEIO       NUMBER(10),
  QT_LIMITE_QUATRO_HORA       NUMBER(10,1),
  IE_UM_LIMITE_PCA            VARCHAR2(15 BYTE),
  QT_LIMITE_UMA_HORA          NUMBER(10,1),
  IE_DURACAO                  VARCHAR2(3 BYTE),
  IE_UM_LIMITE_HORA_PCA       VARCHAR2(15 BYTE),
  IE_ADMINISTRACAO            VARCHAR2(3 BYTE),
  IE_EVENTO_UNICO             VARCHAR2(1 BYTE),
  QT_TOTAL_DIAS_LIB           NUMBER(5),
  NR_SEQ_MAT_DILUICAO         NUMBER(10),
  NR_SEQ_MAT_REDILUICAO       NUMBER(10),
  NR_SEQ_MAT_RECONST          NUMBER(10),
  CD_MAT_SOLUC1               NUMBER(6),
  CD_MAT_SOLUC2               NUMBER(6),
  CD_MAT_SOLUC3               NUMBER(6),
  CD_MAT_SOLUC4               NUMBER(6),
  CD_MAT_SOLUC5               NUMBER(6),
  CD_UNID_MED_DOSE_SOL1       VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_SOL2       VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_SOL3       VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_SOL4       VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_SOL5       VARCHAR2(30 BYTE),
  QT_DOSE_SOLUC1              NUMBER(18,6),
  QT_DOSE_SOLUC2              NUMBER(18,6),
  QT_DOSE_SOLUC3              NUMBER(18,6),
  QT_DOSE_SOLUC4              NUMBER(18,6),
  QT_DOSE_SOLUC5              NUMBER(18,6),
  QT_SOLUCAO_DIL              NUMBER(15,4),
  CD_MAT_DIL                  NUMBER(6),
  QT_DOSE_DIL                 NUMBER(18,6),
  IE_REF_CALCULO              VARCHAR2(3 BYTE),
  CD_UNID_MED_DOSE_DIL        VARCHAR2(30 BYTE),
  IE_CONTROLE_TEMPO           VARCHAR2(3 BYTE),
  QT_SOLUCAO_RED              NUMBER(15,4),
  QT_DOSE_RED                 NUMBER(18,6),
  CD_MAT_RED                  NUMBER(6),
  CD_UNID_MED_DOSE_RED        VARCHAR2(30 BYTE),
  CD_MAT_RECONS               NUMBER(6),
  CD_UNID_MED_DOSE_RECONS     VARCHAR2(30 BYTE),
  QT_DOSE_RECONS              NUMBER(18,6),
  CD_MAT_COMP1                NUMBER(6),
  CD_MAT_COMP2                NUMBER(6),
  CD_MAT_COMP3                NUMBER(6),
  CD_UNID_MED_DOSE_COMP1      VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_COMP2      VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_COMP3      VARCHAR2(30 BYTE),
  QT_DOSE_COMP1               NUMBER(18,6),
  QT_DOSE_COMP2               NUMBER(18,6),
  QT_DOSE_COMP3               NUMBER(18,6),
  QT_DOSE_CORRECAO1           NUMBER(18,6),
  QT_DOSE_CORRECAO2           NUMBER(18,6),
  QT_DOSE_CORRECAO3           NUMBER(18,6),
  QT_DOSE_CORRECAO4           NUMBER(18,6),
  QT_DOSE_CORRECAO5           NUMBER(18,6),
  IE_FATOR_CORRECAO           VARCHAR2(1 BYTE),
  DS_REF_CALCULO              VARCHAR2(4000 BYTE),
  IE_DOSE_ATAQUE              VARCHAR2(2 BYTE),
  QT_DOSE_ATAQUE_COMP1        NUMBER(18),
  QT_DOSE_ATAQUE_COMP2        NUMBER(18),
  QT_DOSE_ATAQUE_COMP3        NUMBER(18),
  QT_DOSE_ATAQUE_DIL          NUMBER(18),
  HR_MIN_APLIC_ATAQUE         VARCHAR2(5 BYTE),
  DS_DOSE_DIFERENCIADA_COMP1  VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_COMP2  VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_COMP3  VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_DIL    VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_REC    VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_RED    VARCHAR2(50 BYTE),
  QT_DOSAGEM_DIFERENCIADA     VARCHAR2(1000 BYTE),
  QT_HORA_FASE_DIFERENCIADA   VARCHAR2(1000 BYTE),
  DT_MIN_APLICACAO            DATE,
  DT_MIN_APLIC_ATAQUE         DATE,
  CD_MAT_COMP4                NUMBER(6),
  CD_MAT_COMP5                NUMBER(6),
  CD_MAT_COMP6                NUMBER(6),
  CD_UNID_MED_DOSE_COMP4      VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_COMP5      VARCHAR2(30 BYTE),
  CD_UNID_MED_DOSE_COMP6      VARCHAR2(30 BYTE),
  QT_DOSE_COMP4               NUMBER(18,6),
  QT_DOSE_COMP5               NUMBER(18,6),
  QT_DOSE_COMP6               NUMBER(18,6),
  QT_DOSE_CORRECAO6           NUMBER(18,6),
  QT_DOSE_CORRECAO7           NUMBER(18,6),
  QT_DOSE_CORRECAO8           NUMBER(18,6),
  IE_PACIENTE_COMP1           VARCHAR2(1 BYTE),
  IE_PACIENTE_COMP2           VARCHAR2(1 BYTE),
  IE_PACIENTE_COMP3           VARCHAR2(1 BYTE),
  IE_PACIENTE_COMP4           VARCHAR2(1 BYTE),
  IE_PACIENTE_COMP5           VARCHAR2(1 BYTE),
  IE_PACIENTE_COMP6           VARCHAR2(1 BYTE),
  DS_DOSE_DIFERENCIADA_COMP4  VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_COMP5  VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_COMP6  VARCHAR2(50 BYTE),
  CD_MAT_COMP7                NUMBER(6),
  IE_PENDENTE                 VARCHAR2(1 BYTE),
  IE_MATERIAL                 VARCHAR2(3 BYTE),
  IE_SEGUNDA                  VARCHAR2(1 BYTE),
  IE_TERCA                    VARCHAR2(1 BYTE),
  IE_QUARTA                   VARCHAR2(1 BYTE),
  IE_QUINTA                   VARCHAR2(1 BYTE),
  IE_SEXTA                    VARCHAR2(1 BYTE),
  IE_SABADO                   VARCHAR2(1 BYTE),
  IE_DOMINGO                  VARCHAR2(1 BYTE),
  QT_DOSE_SEG                 NUMBER(18,6),
  QT_DOSE_TER                 NUMBER(18,6),
  QT_DOSE_QUA                 NUMBER(18,6),
  QT_DOSE_QUI                 NUMBER(18,6),
  QT_DOSE_SEXTA               NUMBER(18,6),
  QT_DOSE_SAB                 NUMBER(18,6),
  QT_DOSE_DOM                 NUMBER(18,6),
  DS_HORARIOS_SEG             VARCHAR2(2000 BYTE),
  DS_HORARIOS_TER             VARCHAR2(2000 BYTE),
  DS_HORARIOS_QUA             VARCHAR2(2000 BYTE),
  DS_HORARIOS_QUI             VARCHAR2(2000 BYTE),
  DS_HORARIOS_SEXTA           VARCHAR2(2000 BYTE),
  DS_HORARIOS_SAB             VARCHAR2(2000 BYTE),
  DS_HORARIOS_DOM             VARCHAR2(2000 BYTE),
  DS_DOSE_DIF_SEG             VARCHAR2(50 BYTE),
  DS_DOSE_DIF_TER             VARCHAR2(50 BYTE),
  DS_DOSE_DIF_QUA             VARCHAR2(50 BYTE),
  DS_DOSE_DIF_QUI             VARCHAR2(50 BYTE),
  DS_DOSE_DIF_SEXTA           VARCHAR2(50 BYTE),
  DS_DOSE_DIF_SAB             VARCHAR2(50 BYTE),
  DS_DOSE_DIF_DOM             VARCHAR2(50 BYTE),
  DS_DOSE_DIFERENCIADA_COMP7  VARCHAR2(50 BYTE),
  CD_UNID_MED_DOSE_COMP7      VARCHAR2(30 BYTE),
  QT_DOSE_COMP7               NUMBER(18,6),
  QT_VOL_DIL_CORRIGIDO        NUMBER(18,6)      DEFAULT null,
  QT_VOL_REC_CORRIGIDO        NUMBER(18,6)      DEFAULT null,
  QT_VOL_RED_CORRIGIDO        NUMBER(18,6)      DEFAULT null,
  IE_UM_FINAL_CONC_PCA        VARCHAR2(15 BYTE),
  QT_FINAL_CONCENTRATION      NUMBER(15,4),
  DS_VOLUME_DIFERENCIADO      VARCHAR2(50 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOFAMA_CIHAMCU_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_AMOSTRA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_CIHMICR_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_MICROORGANISMO_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_CIHTOPO_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_TOPOGRAFIA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_INTPRES_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK10_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_COMP1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK11_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_COMP2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK13_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_COMP3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK2_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_SOLUC1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK3_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_SOLUC2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK4_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_SOLUC3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK5_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_SOLUC4)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK6_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_SOLUC5)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK7_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_DIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK8_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_RED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIA_FK9_I ON TASY.CPOE_FAV_MATERIAL
(CD_MAT_RECONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_MATERIAL_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOFAMA_PK ON TASY.CPOE_FAV_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK10_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_COMP1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK11_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_COMP2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK12_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_COMP3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK2_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_SOL1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK3_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_SOL2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK4_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_SOL3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK5_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_SOL4)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK6_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_SOL5)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK7_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_DIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK8_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_RED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_UNIMEDI_FK9_I ON TASY.CPOE_FAV_MATERIAL
(CD_UNID_MED_DOSE_RECONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOFAMA_VIAAPLI_FK_I ON TASY.CPOE_FAV_MATERIAL
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cpoe_fav_material_atual
before insert or update ON TASY.CPOE_FAV_MATERIAL for each row
declare

begin
begin
    if (:new.hr_min_aplicacao is not null) and ((:new.hr_min_aplicacao <> :old.hr_min_aplicacao) or (:old.dt_min_aplicacao is null)) then
		:new.dt_min_aplicacao := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_min_aplicacao,'dd/mm/yyyy hh24:mi');
	end if;
    if (:new.hr_min_aplic_ataque is not null) and ((:new.hr_min_aplic_ataque <> :old.hr_min_aplic_ataque) or (:old.dt_min_aplic_ataque is null)) then
		:new.dt_min_aplic_ataque := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_min_aplic_ataque,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.CPOE_FAV_MATERIAL ADD (
  CONSTRAINT CPOFAMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_FAV_MATERIAL ADD (
  CONSTRAINT CPOFAMA_MATERIA_FK6 
 FOREIGN KEY (CD_MAT_SOLUC5) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK7 
 FOREIGN KEY (CD_MAT_DIL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK8 
 FOREIGN KEY (CD_MAT_RED) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK9 
 FOREIGN KEY (CD_MAT_RECONS) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK10 
 FOREIGN KEY (CD_UNID_MED_DOSE_COMP1) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK11 
 FOREIGN KEY (CD_UNID_MED_DOSE_COMP2) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK12 
 FOREIGN KEY (CD_UNID_MED_DOSE_COMP3) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MED_DOSE_SOL1) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNID_MED_DOSE_SOL2) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK4 
 FOREIGN KEY (CD_UNID_MED_DOSE_SOL3) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK5 
 FOREIGN KEY (CD_UNID_MED_DOSE_SOL4) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK6 
 FOREIGN KEY (CD_UNID_MED_DOSE_SOL5) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK7 
 FOREIGN KEY (CD_UNID_MED_DOSE_DIL) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK8 
 FOREIGN KEY (CD_UNID_MED_DOSE_RED) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_UNIMEDI_FK9 
 FOREIGN KEY (CD_UNID_MED_DOSE_RECONS) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CPOFAMA_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT CPOFAMA_CIHAMCU_FK 
 FOREIGN KEY (CD_AMOSTRA_CIH) 
 REFERENCES TASY.CIH_AMOSTRA_CULTURA (CD_AMOSTRA_CULTURA),
  CONSTRAINT CPOFAMA_CIHMICR_FK 
 FOREIGN KEY (CD_MICROORGANISMO_CIH) 
 REFERENCES TASY.CIH_MICROORGANISMO (CD_MICROORGANISMO),
  CONSTRAINT CPOFAMA_CIHTOPO_FK 
 FOREIGN KEY (CD_TOPOGRAFIA_CIH) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT CPOFAMA_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT CPOFAMA_MATERIAL_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK10 
 FOREIGN KEY (CD_MAT_COMP1) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK11 
 FOREIGN KEY (CD_MAT_COMP2) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK13 
 FOREIGN KEY (CD_MAT_COMP3) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK2 
 FOREIGN KEY (CD_MAT_SOLUC1) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK3 
 FOREIGN KEY (CD_MAT_SOLUC2) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK4 
 FOREIGN KEY (CD_MAT_SOLUC3) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CPOFAMA_MATERIA_FK5 
 FOREIGN KEY (CD_MAT_SOLUC4) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.CPOE_FAV_MATERIAL TO NIVEL_1;

