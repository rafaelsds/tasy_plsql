ALTER TABLE TASY.EXAME_LAB_REGRA_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LAB_REGRA_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LAB_REGRA_SETOR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_GRUPO           NUMBER(10),
  NR_SEQ_EXAME           NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_TIPO_ATENDIMENTO    NUMBER(3),
  CD_SETOR_SOLICITACAO   NUMBER(5),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  CD_SETOR_COLETA        NUMBER(5),
  CD_SETOR_ENTREGA       NUMBER(5),
  QT_DIA_ENTREGA         NUMBER(3),
  IE_EMITE_MAPA          VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_HORA_FIXA           VARCHAR2(2 BYTE),
  NR_SEQ_MATERIAL        NUMBER(10),
  IE_DATA_RESULTADO      VARCHAR2(1 BYTE),
  QT_MIN_ENTREGA         NUMBER(10),
  NR_SEQ_GRUPO_IMP       NUMBER(10),
  CD_ESTABELECIMENTO     NUMBER(4),
  DS_HORA_INICIO         VARCHAR2(5 BYTE),
  DS_HORA_FIM            VARCHAR2(5 BYTE),
  IE_DIA_SEMANA          NUMBER(1),
  IE_ATUALIZAR_RECOLETA  VARCHAR2(1 BYTE)       NOT NULL,
  IE_URGENCIA            VARCHAR2(1 BYTE),
  IE_DIA_SEMANA_FINAL    NUMBER(10),
  IE_ATUL_DATA_RESULT    VARCHAR2(1 BYTE),
  QT_MIN_ATRASO          NUMBER(4),
  NR_SEQ_PRIORIDADE      NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXRESET_ESTABEL_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_EXALABO_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_GRUEXLA_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_GRUEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_LABGRIM_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(NR_SEQ_GRUPO_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_LABGRIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_MATEXLA_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_MATEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EXRESET_PK ON TASY.EXAME_LAB_REGRA_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXRESET_SETATEN_FK_I ON TASY.EXAME_LAB_REGRA_SETOR
(CD_SETOR_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_SETATEN_FK2_I ON TASY.EXAME_LAB_REGRA_SETOR
(CD_SETOR_COLETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_SETATEN_FK3_I ON TASY.EXAME_LAB_REGRA_SETOR
(CD_SETOR_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_SETATEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.EXRESET_SETATEN_FK4_I ON TASY.EXAME_LAB_REGRA_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXRESET_SETATEN_FK4_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.EXAME_LAB_REGRA_SETOR_INS_UPD 
before insert or update or delete ON TASY.EXAME_LAB_REGRA_SETOR 
for each row
declare 
 
ie_operacao_p	number(1); 
 
begin 
 
if	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then 
 
	IF 	INSERTING THEN 
		ie_operacao_p	:= 1; 
	elsif	UPDATING then 
		ie_operacao_p	:= 2; 
	elsif	DELETING then 
		ie_operacao_p	:= 3; 
	end if; 
 
	integrar_tasylab(	 
				null, 
				null, 
				296, 
				null, 
				null, 
				null, 
				null, 
				null, 
				null, 
				:new.nr_sequencia, 
				'EXAME_LAB_REGRA_SETOR', 
				ie_operacao_p, 
				'N'); 
	 
end if; 
 
end;
/


CREATE OR REPLACE TRIGGER TASY.EXAME_LAB_REGRA_SETOR_tp  after update ON TASY.EXAME_LAB_REGRA_SETOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ATUL_DATA_RESULT,1,4000),substr(:new.IE_ATUL_DATA_RESULT,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUL_DATA_RESULT',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIA_SEMANA_FINAL,1,4000),substr(:new.IE_DIA_SEMANA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIA_SEMANA_FINAL',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO,1,4000),substr(:new.NR_SEQ_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EXAME,1,4000),substr(:new.NR_SEQ_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_SOLICITACAO,1,4000),substr(:new.CD_SETOR_SOLICITACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_SOLICITACAO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_COLETA,1,4000),substr(:new.CD_SETOR_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_COLETA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ENTREGA,1,4000),substr(:new.CD_SETOR_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ENTREGA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_ENTREGA,1,4000),substr(:new.QT_DIA_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_ENTREGA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EMITE_MAPA,1,4000),substr(:new.IE_EMITE_MAPA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMITE_MAPA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DATA_RESULTADO,1,4000),substr(:new.IE_DATA_RESULTADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_RESULTADO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_ENTREGA,1,4000),substr(:new.QT_MIN_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_ENTREGA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_IMP,1,4000),substr(:new.NR_SEQ_GRUPO_IMP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_IMP',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HORA_INICIO,1,4000),substr(:new.DS_HORA_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_HORA_INICIO',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HORA_FIM,1,4000),substr(:new.DS_HORA_FIM,1,4000),:new.nm_usuario,nr_seq_w,'DS_HORA_FIM',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIA_SEMANA,1,4000),substr(:new.IE_DIA_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIA_SEMANA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_RECOLETA,1,4000),substr(:new.IE_ATUALIZAR_RECOLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_RECOLETA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_URGENCIA,1,4000),substr(:new.IE_URGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_URGENCIA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HORA_FIXA,1,4000),substr(:new.DS_HORA_FIXA,1,4000),:new.nm_usuario,nr_seq_w,'DS_HORA_FIXA',ie_log_w,ds_w,'EXAME_LAB_REGRA_SETOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.EXAME_LAB_REGRA_SETOR ADD (
  CONSTRAINT EXRESET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXAME_LAB_REGRA_SETOR ADD (
  CONSTRAINT EXRESET_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_SOLICITACAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EXRESET_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_COLETA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EXRESET_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_ENTREGA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EXRESET_SETATEN_FK4 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EXRESET_GRUEXLA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXRESET_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT EXRESET_MATEXLA_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXRESET_LABGRIM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_IMP) 
 REFERENCES TASY.LAB_GRUPO_IMPRESSAO (NR_SEQUENCIA),
  CONSTRAINT EXRESET_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.EXAME_LAB_REGRA_SETOR TO NIVEL_1;

