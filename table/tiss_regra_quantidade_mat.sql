ALTER TABLE TASY.TISS_REGRA_QUANTIDADE_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_QUANTIDADE_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_QUANTIDADE_MAT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_MATERIAL            NUMBER(10),
  CD_GRUPO_MATERIAL      NUMBER(3),
  CD_SUBGRUPO_MATERIAL   NUMBER(3),
  CD_CLASSE_MATERIAL     NUMBER(5),
  CD_CONVENIO            NUMBER(5),
  IE_CONVERSAO_QTDE_MAT  VARCHAR2(5 BYTE)       NOT NULL,
  CD_PROCEDIMENTO        NUMBER(15),
  IE_ORIGEM_PROCED       NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  CD_AREA_PROCEDIMENTO   NUMBER(15),
  CD_ESPECIALIDADE       NUMBER(15),
  CD_GRUPO_PROC          NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRQTM_AREPROC_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRQTM_CLAMATE_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRQTM_CONVENI_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRQTM_ESPPROC_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRQTM_ESTABEL_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRQTM_GRUMATE_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRQTM_GRUPROC_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRQTM_MATERIA_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRQTM_PK ON TASY.TISS_REGRA_QUANTIDADE_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRQTM_PROCEDI_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRQTM_SUBMATE_FK_I ON TASY.TISS_REGRA_QUANTIDADE_MAT
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRQTM_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_QUANTIDADE_MAT
BEFORE UPDATE ON TASY.TISS_REGRA_QUANTIDADE_MAT FOR EACH ROW
declare


BEGIN
if	(wheb_usuario_pck.GET_IE_EXECUTAR_TRIGGER = 'S') then

	if	(:new.ie_origem_proced is not null) and
		(:new.cd_procedimento is null) then
		:new.ie_origem_proced := null;
	end if;

	if	((:new.cd_material is not null) or
		(:new.cd_grupo_material is not null) or
		(:new.cd_classe_material is not null) or
		(:new.cd_subgrupo_material is not null)) and
		((:new.CD_PROCEDIMENTO is not null) or
		(:new.CD_AREA_PROCEDIMENTO is not null) or
		(:new.CD_ESPECIALIDADE is not null) or
		(:new.CD_GRUPO_PROC is not null)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(853996);
	end if;

end if;

END;
/


ALTER TABLE TASY.TISS_REGRA_QUANTIDADE_MAT ADD (
  CONSTRAINT TISSRQTM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_QUANTIDADE_MAT ADD (
  CONSTRAINT TISSRQTM_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT TISSRQTM_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSRQTM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRQTM_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT TISSRQTM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT TISSRQTM_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT TISSRQTM_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT TISSRQTM_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT TISSRQTM_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT TISSRQTM_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.TISS_REGRA_QUANTIDADE_MAT TO NIVEL_1;

