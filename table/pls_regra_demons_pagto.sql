ALTER TABLE TASY.PLS_REGRA_DEMONS_PAGTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_DEMONS_PAGTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_DEMONS_PAGTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_USUARIO          NUMBER(10),
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_VALOR_PROCESSADO     VARCHAR2(1 BYTE),
  NR_SEQ_PRESTADOR_PGTO   NUMBER(10),
  NR_SEQ_PRESTADOR_ATEND  NUMBER(10),
  IE_GERAR_DEB_CRED       VARCHAR2(5 BYTE),
  IE_VALOR_BRUTO          VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRDPG_ESTABEL_FK_I ON TASY.PLS_REGRA_DEMONS_PAGTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRDPG_PK ON TASY.PLS_REGRA_DEMONS_PAGTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRDPG_PLSPRES_FK_I ON TASY.PLS_REGRA_DEMONS_PAGTO
(NR_SEQ_PRESTADOR_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRDPG_PLSPRES_FK2_I ON TASY.PLS_REGRA_DEMONS_PAGTO
(NR_SEQ_PRESTADOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRDPG_PLSUSWE_FK_I ON TASY.PLS_REGRA_DEMONS_PAGTO
(NR_SEQ_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_DEMONS_PAGTO_tp  after update ON TASY.PLS_REGRA_DEMONS_PAGTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR_PGTO,1,4000),substr(:new.NR_SEQ_PRESTADOR_PGTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR_PGTO',ie_log_w,ds_w,'PLS_REGRA_DEMONS_PAGTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR_ATEND,1,4000),substr(:new.NR_SEQ_PRESTADOR_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR_ATEND',ie_log_w,ds_w,'PLS_REGRA_DEMONS_PAGTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_DEMONS_PAGTO ADD (
  CONSTRAINT PLSRDPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_DEMONS_PAGTO ADD (
  CONSTRAINT PLSRDPG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRDPG_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PGTO) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRDPG_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_ATEND) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRDPG_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_USUARIO) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_DEMONS_PAGTO TO NIVEL_1;

