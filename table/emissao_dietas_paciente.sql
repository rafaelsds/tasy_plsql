ALTER TABLE TASY.EMISSAO_DIETAS_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMISSAO_DIETAS_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMISSAO_DIETAS_PACIENTE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_SCHEDULER         NUMBER(10)           NOT NULL,
  CD_TIPO_REL_IMPRESSAO    VARCHAR2(5 BYTE),
  IE_CENSO                 VARCHAR2(1 BYTE),
  IE_ACOMPANHANTES         VARCHAR2(1 BYTE),
  IE_ACOMPANHANTE_SERVICO  VARCHAR2(1 BYTE),
  IE_LIB_JEJUM             VARCHAR2(1 BYTE),
  IE_LIB_LANCHE            VARCHAR2(1 BYTE),
  IE_DIETAS_ESPECIAIS      VARCHAR2(1 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  QT_INTERVALO             NUMBER(5),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  DS_CRON                  VARCHAR2(50 BYTE),
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_PRESCRICAO_AGORA      VARCHAR2(1 BYTE),
  DS_IMPRESSORA            VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.EDIETASC_PK ON TASY.EMISSAO_DIETAS_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EDIETASC_USUSCHE_FK_I ON TASY.EMISSAO_DIETAS_PACIENTE
(NR_SEQ_SCHEDULER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EMISSAO_DIETAS_PACIENTE ADD (
  CONSTRAINT EDIETASC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMISSAO_DIETAS_PACIENTE ADD (
  CONSTRAINT EDIETASC_USUSCHE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULER) 
 REFERENCES TASY.USUARIO_SCHEDULER (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMISSAO_DIETAS_PACIENTE TO NIVEL_1;

