ALTER TABLE TASY.PLS_MONITOR_TISS_PROC_VAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_PROC_VAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_PROC_VAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CTA_VAL         NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA           NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA_PROC      NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_TABELA_REF          VARCHAR2(2 BYTE),
  QT_PROCEDIMENTO        NUMBER(12,4),
  VL_PROCEDIMENTO        NUMBER(15,2),
  CD_PROCEDIMENTO        NUMBER(15),
  IE_ORIGEM_PROCED       NUMBER(10),
  CD_GRUPO_PROC          VARCHAR2(3 BYTE),
  VL_LIBERADO            NUMBER(15,2),
  QT_LIBERADO            NUMBER(12,4),
  IE_TIPO_DIARIA         VARCHAR2(3 BYTE),
  VL_GLOSA               NUMBER(15,2),
  CD_DENTE               VARCHAR2(20 BYTE),
  CD_REGIAO_BOCA         VARCHAR2(20 BYTE),
  CD_FACE_DENTE          VARCHAR2(20 BYTE),
  IE_TIPO_DESPESA        VARCHAR2(1 BYTE),
  NR_SEQ_PACOTE          NUMBER(10),
  DT_EVENTO              DATE,
  IE_TIPO_EVENTO         VARCHAR2(3 BYTE),
  IE_GLOSA               VARCHAR2(1 BYTE),
  IE_ITEM_ATUALIZADO     VARCHAR2(1 BYTE),
  NR_SEQ_CTA_ALT         NUMBER(10)             NOT NULL,
  NR_SEQ_PROC_REC        NUMBER(10),
  NR_SEQ_CONTA_REC       NUMBER(10),
  NR_SEQ_CTA_TISS_PROC   NUMBER(10),
  VL_COPARTICIPACAO      NUMBER(18,2),
  IE_ORIGEM_TAB_REF      VARCHAR2(15 BYTE),
  IE_ORIGEM_GRUPO_PROC   VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TAB_REF   NUMBER(10),
  NR_SEQ_REGRA_GPO_PROC  NUMBER(10),
  NR_SEQ_CONTA_DISC      NUMBER(10),
  NR_SEQ_DISC_PROC       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTPV_I1 ON TASY.PLS_MONITOR_TISS_PROC_VAL
(IE_ORIGEM_PROCED, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTPV_PK ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSCOPRO_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_CONTA, NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSDISP_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_DISC_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSMOTP_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_CTA_TISS_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSMTCV_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_CTA_VAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSMTRG_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_REGRA_GPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSRETT_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_REGRA_TAB_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PLSRGCO_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_CONTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PRGPROC_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(NR_SEQ_PROC_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_PROCEDI_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_TISSDEN_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(CD_DENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMTPV_TISSRGBO_FK_I ON TASY.PLS_MONITOR_TISS_PROC_VAL
(CD_REGIAO_BOCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_proc_val_del
before delete ON TASY.PLS_MONITOR_TISS_PROC_VAL for each row
declare
vl_total_pago_w			pls_monitor_tiss_cta_val.vl_total_pago%type;
vl_total_apres_w		pls_monitor_tiss_cta_val.vl_cobranca_guia%type;
vl_total_item_w			pls_monitor_tiss_cta_val.vl_total_pago%type;
vl_total_tab_propria_w		pls_monitor_tiss_cta_val.vl_total_pago%type;
vl_total_glosa_w		pls_monitor_tiss_cta_val.vl_total_glosa%type;
vl_item_pago_w			pls_monitor_tiss_proc_val.vl_liberado%type;
vl_item_apres_w			pls_monitor_tiss_proc_val.vl_procedimento%type;
vl_item_glosa_w			pls_monitor_tiss_proc_val.vl_glosa%type;
ie_tipo_despesa_w		pls_conta_mat.ie_tipo_despesa%type;

begin
vl_item_pago_w := nvl(:old.vl_liberado,0);
vl_item_apres_w := nvl(:old.vl_procedimento,0);
vl_item_glosa_w := nvl(:old.vl_glosa,0);

select	vl_cobranca_guia,
	vl_total_pago,
	vl_total_glosa
into	vl_total_apres_w,
	vl_total_pago_w,
	vl_total_glosa_w
from	pls_monitor_tiss_cta_val
where	nr_sequencia = :old.nr_seq_cta_val;

select	ie_tipo_despesa
into	ie_tipo_despesa_w
from	pls_conta_proc
where	nr_sequencia = :old.nr_seq_conta_proc;

if	(:old.cd_tabela_ref in ('00','98')) then
	select	vl_total_tabela_propria
	into	vl_total_tab_propria_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	select	vl_total_procedimento
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_tabela_propria = vl_total_tab_propria_w - vl_item_pago_w,
		vl_total_procedimento	= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	(:old.cd_tabela_ref = '18') and (ie_tipo_despesa_w = '2') then
	select	vl_total_taxa
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_taxa		= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	(:old.cd_tabela_ref = '22') and (ie_tipo_despesa_w = '1') then
	select	vl_total_procedimento
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_procedimento	= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;

elsif	(:old.cd_tabela_ref = '18') and (ie_tipo_despesa_w = '3') then
	select	vl_total_diaria
	into	vl_total_item_w
	from	pls_monitor_tiss_cta_val
	where	nr_sequencia = :old.nr_seq_cta_val;

	update	pls_monitor_tiss_cta_val set
		vl_total_diaria		= vl_total_item_w - vl_item_pago_w,
		vl_cobranca_guia 	= vl_total_apres_w - vl_item_apres_w,
		vl_total_pago 		= vl_total_pago_w - vl_item_pago_w,
		vl_total_glosa 		= vl_total_glosa_w - vl_item_glosa_w
	where	nr_sequencia 		= :old.nr_seq_cta_val;
end if;

end;
/


ALTER TABLE TASY.PLS_MONITOR_TISS_PROC_VAL ADD (
  CONSTRAINT PLSMTPV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_PROC_VAL ADD (
  CONSTRAINT PLSMTPV_PLSDISP_FK 
 FOREIGN KEY (NR_SEQ_DISC_PROC) 
 REFERENCES TASY.PLS_DISCUSSAO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA, NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQ_CONTA,NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSMTCV_FK 
 FOREIGN KEY (NR_SEQ_CTA_VAL) 
 REFERENCES TASY.PLS_MONITOR_TISS_CTA_VAL (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_TISSDEN_FK 
 FOREIGN KEY (CD_DENTE) 
 REFERENCES TASY.TISS_DENTE (CD_DENTE),
  CONSTRAINT PLSMTPV_TISSRGBO_FK 
 FOREIGN KEY (CD_REGIAO_BOCA) 
 REFERENCES TASY.TISS_REGIAO_BOCA (CD_REGIAO),
  CONSTRAINT PLSMTPV_PRGPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSRGCO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSMOTP_FK 
 FOREIGN KEY (NR_SEQ_CTA_TISS_PROC) 
 REFERENCES TASY.PLS_MONITOR_TISS_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSMTRG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GPO_PROC) 
 REFERENCES TASY.PLS_MONITOR_TISS_REG_GPO (NR_SEQUENCIA),
  CONSTRAINT PLSMTPV_PLSRETT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TAB_REF) 
 REFERENCES TASY.PLS_REGRA_TABELA_TISS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_PROC_VAL TO NIVEL_1;

