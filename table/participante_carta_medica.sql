ALTER TABLE TASY.PARTICIPANTE_CARTA_MEDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARTICIPANTE_CARTA_MEDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARTICIPANTE_CARTA_MEDICA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_DESTINATARIO      VARCHAR2(255 BYTE)       DEFAULT null,
  NR_SEQ_CARTA_MAE     NUMBER(10),
  NR_SEQ_CARTA         NUMBER(10),
  DT_ASSINATURA        DATE,
  NM_USUARIO_RESP      VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_ASSINAT   VARCHAR2(15 BYTE),
  IE_DEVE_ASSINAR      VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_APRESENT      NUMBER(10),
  NR_SEQ_MODELO        NUMBER(10)               DEFAULT null,
  IE_INCLUSO           VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA    NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACAMED_CAMEMOD_FK_I ON TASY.PARTICIPANTE_CARTA_MEDICA
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACAMED_CARMED_FK_I ON TASY.PARTICIPANTE_CARTA_MEDICA
(NR_SEQ_CARTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACAMED_PESFISC_FK_I ON TASY.PARTICIPANTE_CARTA_MEDICA
(NM_USUARIO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACAMED_PESFISI_FK_I ON TASY.PARTICIPANTE_CARTA_MEDICA
(NM_USUARIO_ASSINAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACAMED_PK ON TASY.PARTICIPANTE_CARTA_MEDICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.part_carta_medica_update
before update ON TASY.PARTICIPANTE_CARTA_MEDICA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;

begin
	if ((:new.dt_assinatura is not null
		and :old.dt_assinatura is not null
		and :new.dt_assinatura != :old.dt_assinatura)
		or (:new.dt_assinatura is not null
			and :old.dt_assinatura is null)) then
		if (obter_permissao_ass_med(:new.nm_usuario_resp, :new.nm_usuario, 'S') = 'S') then
			:new.nm_usuario_assinat := :new.nm_usuario;
		end if;
	elsif (:new.dt_assinatura is null
			and :old.dt_assinatura is not null) then
		:new.nm_usuario_assinat := null;
	end if;

	if (:new.dt_assinatura is not null and :old.dt_assinatura is null) then
		select	max(a.nr_seq_regra)
		into	nr_seq_regra_w
		from	wl_worklist a,
				wl_item b,
				wl_regra_item c
		where	a.nr_seq_carta_mae = :new.nr_seq_carta_mae
		and		b.nr_sequencia = a.nr_seq_item
		and		c.nr_sequencia = a.nr_seq_regra
		and		a.dt_final_real is null
		and		b.cd_categoria = 'ML'
		and		c.ie_tipo_pend_carta = 'A'
		and		b.ie_situacao = 'A'
		and		c.ie_situacao = 'A';

		wl_gerar_finalizar_tarefa('ML','F',null,null,wheb_usuario_pck.get_nm_usuario,null,'N',null,null,null,null,null,:new.nr_seq_carta_mae,null,null,null,nr_seq_regra_w,
									null,null,null,null,null,null,null,null,null,null,obter_pf_usuario(:new.nm_usuario_resp,'C'));
	end if;
end;
/


ALTER TABLE TASY.PARTICIPANTE_CARTA_MEDICA ADD (
  CONSTRAINT PACAMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARTICIPANTE_CARTA_MEDICA ADD (
  CONSTRAINT PACAMED_CARMED_FK 
 FOREIGN KEY (NR_SEQ_CARTA) 
 REFERENCES TASY.CARTA_MEDICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACAMED_CAMEMOD_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.CARTA_MEDICA_MODELO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PARTICIPANTE_CARTA_MEDICA TO NIVEL_1;

