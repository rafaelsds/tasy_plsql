ALTER TABLE TASY.ATENDIMENTO_SINAL_VITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_SINAL_VITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_SINAL_VITAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  DT_SINAL_VITAL             DATE               NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  QT_PA_DIASTOLICA           NUMBER(3),
  QT_PA_SISTOLICA            NUMBER(3),
  IE_PRESSAO                 VARCHAR2(1 BYTE),
  QT_FREQ_CARDIACA           NUMBER(3),
  QT_FREQ_RESP               NUMBER(3),
  QT_TEMP                    NUMBER(4,1),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  QT_PESO                    NUMBER(10,3),
  QT_IMC                     NUMBER(4,1),
  QT_SUPERF_CORPORIA         NUMBER(15,2),
  QT_BCF                     NUMBER(3),
  QT_ALTURA_CM               NUMBER(5,2),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  IE_MEMBRO                  VARCHAR2(3 BYTE),
  IE_MANGUITO                VARCHAR2(1 BYTE),
  QT_SATURACAO_O2            NUMBER(3),
  QT_GLICEMIA_CAPILAR        NUMBER(15,1),
  CD_ESCALA_DOR              VARCHAR2(5 BYTE),
  QT_ESCALA_DOR              NUMBER(3,1),
  QT_PAM                     NUMBER(3),
  QT_PVC                     NUMBER(5,2),
  QT_PAE                     NUMBER(3),
  IE_APARELHO_PA             VARCHAR2(3 BYTE),
  QT_PVC_H2O                 NUMBER(5,2),
  QT_TEMP_INCUBADORA         NUMBER(4,1),
  NR_SEQ_TOPOGRAFIA_DOR      NUMBER(10),
  QT_PRESSAO_INTRA_ABD       NUMBER(5,2),
  QT_PRESSAO_INTRA_CRANIO    NUMBER(3),
  QT_PERIMETRO_CEFALICO      NUMBER(5,2),
  QT_INSULINA                NUMBER(15,4),
  IE_RITMO_ECG               VARCHAR2(15 BYTE),
  QT_SEGMENTO_ST             NUMBER(15,2),
  NR_CIRURGIA                NUMBER(10),
  QT_MAEC                    NUMBER(3),
  IE_SITIO                   NUMBER(3),
  IE_RN                      VARCHAR2(1 BYTE),
  QT_PERIMETRO_TORACICO      NUMBER(5,2),
  QT_PERIMITRO_ABDOMINAL     NUMBER(5,2),
  IE_LADO                    VARCHAR2(1 BYTE),
  NR_HORA                    NUMBER(2),
  IE_RELEVANTE_APAP          VARCHAR2(1 BYTE),
  CD_PACIENTE                VARCHAR2(10 BYTE),
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_DECUBITO                VARCHAR2(15 BYTE),
  DT_REFERENCIA              DATE,
  NR_SEQ_SAEP                NUMBER(10),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_CONDICAO_DOR        NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  QT_BILIRRUBINA_TRONCO      NUMBER(5,2),
  QT_BILIRRUBINA_FRONTE      NUMBER(5,2),
  QT_PUPILA_TAMANHO          NUMBER(1),
  IE_PUPILA_REACAO_LUZ       VARCHAR2(15 BYTE),
  IE_PUPILA_ALTERACAO        VARCHAR2(15 BYTE),
  IE_PUPILA_LADO             VARCHAR2(15 BYTE),
  IE_SINAL_FOCAL             VARCHAR2(15 BYTE),
  IE_SINAL_FOCAL_LOCAL       VARCHAR2(15 BYTE),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_ORIGEM_PESO             VARCHAR2(2 BYTE),
  IE_ORIGEM_ALTURA           VARCHAR2(2 BYTE),
  QT_PESO_UM                 NUMBER(10),
  QT_TOF_BLOQ_NEURO_MUSC     NUMBER(3),
  IE_NIVEL_CONSCIENCIA       VARCHAR2(15 BYTE),
  NR_SEQ_INTERVENCAO         NUMBER(15),
  IE_RPA                     VARCHAR2(1 BYTE),
  QT_PUPILA_TAM_ESQ          NUMBER(1),
  IE_PUPILA_REACAO_LUZ_E     VARCHAR2(15 BYTE),
  IE_BABINSKI                VARCHAR2(15 BYTE),
  QT_VARIACAO_GLIC           NUMBER(15,1),
  QT_GLIC_MEDIA_DIA          NUMBER(15,1),
  QT_GLIC_MEDIA_ATEND        NUMBER(15,1),
  QT_PPA                     NUMBER(5,1),
  QT_PPC                     NUMBER(5,1),
  QT_O2_SUPLEMENTAR          NUMBER(8,3),
  IE_UNID_MED_O2_SUPLEM      VARCHAR2(3 BYTE),
  QT_PERIMETRO_QUADRIL       NUMBER(5,2),
  QT_RELACAO_ABD_QUAD        NUMBER(5,2),
  QT_ANGULO_CABECEIRA        NUMBER(3),
  QT_CIRCUNF_PANTURRILHA     NUMBER(15,2),
  QT_CIRCUNF_BRACO           NUMBER(15,2),
  QT_PREGA_CUTANEA_TRICEPS   NUMBER(15,2),
  NR_SEQ_HORARIO             NUMBER(15),
  NR_SEQ_SINAL_VITAL         NUMBER(10),
  NR_SEQ_TRIAGEM             NUMBER(10),
  QT_DELTA_PESO              NUMBER(5,2),
  DT_DELTA_PESO              DATE,
  IE_AGITACAO                VARCHAR2(1 BYTE),
  IE_SUDORESE                VARCHAR2(1 BYTE),
  IE_AUM_TRAB_RESP           VARCHAR2(1 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  QT_FREQ_CARD_MONIT         NUMBER(3),
  NR_SEQ_INAPTO_DOR          NUMBER(10),
  NR_SEQ_RESULT_DOR          NUMBER(10),
  IE_MAE_CANGURU             VARCHAR2(1 BYTE),
  IE_DERIVACAO_SEG_ST        VARCHAR2(15 BYTE),
  IE_GLIC_EXTRAPOL           VARCHAR2(1 BYTE),
  QT_GLICOSE_ADM             NUMBER(5,1),
  QT_IRRADIANCIA             NUMBER(5,2),
  QT_BCF_2                   NUMBER(3),
  QT_BCF_3                   NUMBER(3),
  NR_RECEM_NATO              NUMBER(3),
  IE_IMPORTADO               VARCHAR2(1 BYTE),
  QT_DERIV_VENTRIC_EXTER     NUMBER(3),
  QT_PIC_TEMP                NUMBER(4,1),
  IE_COND_SAT_O2             VARCHAR2(2 BYTE),
  IE_MEMBRO_SAT_O2           VARCHAR2(3 BYTE),
  QT_PRESSAO_INTRA_ABD_H2O   NUMBER(5,2),
  IE_INTEGRACAO              VARCHAR2(1 BYTE),
  CD_DESC_VALOR_DOR          NUMBER(10),
  DS_REGISTRO_TEMP_MOBILE    VARCHAR2(2000 BYTE),
  QT_PERIODO                 NUMBER(10),
  IE_PERIODO                 VARCHAR2(15 BYTE),
  IE_TIPO_DOR                VARCHAR2(10 BYTE),
  IE_FREQUENCIA_DOR          VARCHAR2(10 BYTE),
  IE_INTEGRACAO_EXTRA        VARCHAR2(1 BYTE),
  NR_SEQ_REG_REGISTRO        NUMBER(10),
  QT_TEMP_BERCO              NUMBER(4,1),
  QT_UMID_INCUBADORA         NUMBER(3),
  QT_UI_INSULINA_INT         NUMBER(10,2),
  IE_COLETA_GLIC             VARCHAR2(1 BYTE),
  IE_UNID_MED_PESO           VARCHAR2(10 BYTE),
  QT_PESO_ATUAL              NUMBER(10),
  QT_ALTURA_M                NUMBER(15,2),
  QT_ALTURA_UM               NUMBER(10),
  QT_SPO2R                   NUMBER(10),
  IE_UNID_MED_ALTURA         VARCHAR2(10 BYTE),
  NR_SEQ_LOG_MENSAGEM        NUMBER(10),
  IE_INTEGRACAO_REGRA        VARCHAR2(1 BYTE),
  IE_INTEGRACAO_MENTOR       VARCHAR2(1 BYTE),
  NR_SEQ_MIN_PHILIPS         NUMBER(10),
  NR_SEQ_LOG_INTEGRACAO      NUMBER(10),
  IE_DIALISE                 VARCHAR2(3 BYTE),
  QT_CETONA                  NUMBER(15,4),
  DS_SERIAL_ABBOTT           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_UM_PA_SIST              VARCHAR2(10 BYTE),
  IE_UM_PA_DIAST             VARCHAR2(10 BYTE),
  IE_UM_PAM                  VARCHAR2(10 BYTE),
  IE_UM_FC                   VARCHAR2(10 BYTE),
  IE_UM_FR                   VARCHAR2(10 BYTE),
  IE_UM_TEMP                 VARCHAR2(10 BYTE),
  IE_UM_IMC                  VARCHAR2(10 BYTE),
  IE_UM_SC                   VARCHAR2(10 BYTE),
  QT_AU_CM                   NUMBER(3,1),
  IE_DINAMICA_UTERINA        VARCHAR2(1 BYTE),
  NR_SEQ_ESCUTA              NUMBER(10),
  QT_CONTRACOES              NUMBER(3),
  NR_SEQ_SOAP                NUMBER(10),
  IE_TIPO_AVALIACAO          VARCHAR2(1 BYTE),
  IE_PERF_CAP                VARCHAR2(1 BYTE),
  IE_TIPO_MEDICAO            NUMBER(1),
  IE_NIVEL_CONSCIENCIA_PED   VARCHAR2(15 BYTE),
  IE_PA_INAUDIVEL            VARCHAR2(1 BYTE)   DEFAULT null,
  QT_GLICEMIA_MMOL           NUMBER(11,6),
  IE_PATIENT_CONDITION       VARCHAR2(1 BYTE),
  QT_ENCHIMENTO_CAPILAR      NUMBER(3),
  IE_VALOR_FORA_FAIXA        VARCHAR2(3 BYTE),
  QT_CIRCUNF_CINTURA         NUMBER(5,2),
  IE_LADO_PE                 VARCHAR2(1 BYTE),
  IE_REVISAO_PE              VARCHAR2(1 BYTE),
  IE_FUNDO_OLHO              VARCHAR2(1 BYTE),
  IE_NIVEL_CONF_DOR          NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  QT_PA_DIASTOLICA_CMGH      NUMBER(3,1)        DEFAULT null,
  QT_PAM_CMHG                NUMBER(3,1)        DEFAULT null,
  QT_PA_SISTOLICA_CMGH       NUMBER(3,1)        DEFAULT null,
  IE_LADO_DOR                VARCHAR2(1 BYTE),
  NR_SEQ_TOP_DOR_IRRADIA     NUMBER(10),
  IE_ACAO                    VARCHAR2(1 BYTE),
  NR_DISTRESS                NUMBER(3),
  QT_PERIMITRO_TRICIPITAL    NUMBER(5,2),
  NR_SEQ_EQUIPAMENTO         NUMBER(10),
  NR_SEQ_EQUIPAMENTO_RESP    NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10),
  IE_MEDIANA                 VARCHAR2(10 BYTE),
  IE_MEDIANA_GERADA          VARCHAR2(10 BYTE),
  NR_SEQ_CONDUTA             NUMBER(10),
  QT_PERFUSION_INDEX         NUMBER(4,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATESIVI_ATCONSPEPA_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_ATEPACI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_ATESINI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_ESCUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_ATESIVI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_SINAL_VITAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_ATESIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_ATESOAP_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_CIRURGI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_CONDDOR_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_CONDICAO_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_CONDDOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_EHRREEL_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_EHRREGI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_EHRUNME_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(QT_PESO_UM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_EQUPVS_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_ESCDORE_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_RESULT_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_INAPDOR_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_INAPTO_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_INAPDOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_I1 ON TASY.ATENDIMENTO_SINAL_VITAL
(DT_SINAL_VITAL, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_I2 ON TASY.ATENDIMENTO_SINAL_VITAL
(DT_SINAL_VITAL, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_I3 ON TASY.ATENDIMENTO_SINAL_VITAL
(NVL("IE_SITUACAO",'A'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_I4 ON TASY.ATENDIMENTO_SINAL_VITAL
(CD_ESCALA_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_I4
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_I5 ON TASY.ATENDIMENTO_SINAL_VITAL
(DT_SINAL_VITAL, CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_I6 ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_ATENDIMENTO, IE_SITUACAO, NVL("IE_RN",'N'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_I7 ON TASY.ATENDIMENTO_SINAL_VITAL
(NM_USUARIO, DT_LIBERACAO, NR_SEQ_PEPO, NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ATESIVI_I9 ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_ATENDIMENTO, DT_SINAL_VITAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ATESIVI_LIISPH_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_MIN_PHILIPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_PEPOCIR_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_PEPROCE_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_INTERVENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_PEPROCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_PERFIL_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_PESFISI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_PESFISI_FK2_I ON TASY.ATENDIMENTO_SINAL_VITAL
(CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATESIVI_PK ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESIVI_SAEPERO_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_SETATEN_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_TASASDI_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_TASASDI_FK2_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_TOPDOR_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_TOPOGRAFIA_DOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_TOPDOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATESIVI_TRPRAT_FK_I ON TASY.ATENDIMENTO_SINAL_VITAL
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATESIVI_TRPRAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atend_sinal_vital_pend_atual
after insert or update ON TASY.ATENDIMENTO_SINAL_VITAL for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_sinal_vital_w	varchar2(10);
nr_seq_reg_elemento_w	number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_lib_sinal_vital)
into	ie_lib_sinal_vital_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

nr_seq_reg_elemento_w	:= :new.nr_seq_reg_elemento;

if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'SV';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XSV';
	end if;

	begin
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_paciente, :new.nr_atendimento, :new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_reg_elemento_w);
	end if;
	exception
		when others then
		null;
	end;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_liberacao_sv_sepse_insupd
BEFORE INSERT OR UPDATE ON TASY.ATENDIMENTO_SINAL_VITAL FOR EACH ROW
DECLARE


BEGIN

    if (:new.dt_liberacao is not null and :old.dt_liberacao is null and :new.nr_atendimento is not null and :new.nr_cirurgia is null) then

        begin
            hsj_gerar_escala_sepse_js(:new.nr_atendimento,:new.cd_pessoa_fisica, :new.nm_usuario, null, null, :new.nr_sequencia);
        end;
        
    end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_SINAL_VITAL_SBIS_IN
before insert or update ON TASY.ATENDIMENTO_SINAL_VITAL for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);
cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica%type;

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  cd_pessoa_fisica_w := obter_pessoa_atendimento(:new.nr_atendimento,'C');

  if (cd_pessoa_fisica_w is null) then
    cd_pessoa_fisica_w := :new.cd_pessoa_fisica;
  end if;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
					   nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                      obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                        cd_pessoa_fisica_w,
                         obter_desc_expressao(298520),
                        :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
	       nvl(wheb_usuario_pck.get_nm_usuario,'integracao'));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       cd_pessoa_fisica_w,
                       obter_desc_expressao(298520),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
	       nvl(wheb_usuario_pck.get_nm_usuario,'integracao'));
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Atendimento_sinal_vital_Atual
BEFORE INSERT OR UPDATE ON TASY.ATENDIMENTO_SINAL_VITAL FOR EACH ROW
DECLARE

ds_hora_w	varchar2(20);
dt_registro_w	date;
dt_apap_w	date;
qt_hora_w	number(15,2);
qt_reg_w	number(1);
cd_estabelecimento_w	number(4);
nr_seq_evento_w			number(10);
total_escore_alerta_w	number(10);
qt_idade_w		number(10);
cd_setor_paciente_w	number(10);
ds_alerta_modificado_w	varchar2(512);
ds_retorno_w		varchar2(4000);
ie_retorno_w		varchar2(10);
ie_motivo_dor_inapto_w	varchar2(1);
ds_Alerta_sinais_w	varchar2(4000);
ie_setor_w		varchar2(1);
cd_funcao_ativa_w	number(5);
ie_sepse_lib_sv_w	varchar2(1);
qt_horas_passado_sv_w	number(15,5);

Cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	=	'APM'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	total_escore_alerta_w between nvl(qt_pto_min,0)	and nvl(qt_pto_max,9999)
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w)	= cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';

Cursor C02 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	=  'CSVR'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w)	= cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';

BEGIN
cd_funcao_ativa_w := obter_funcao_ativa;
Obter_Param_Usuario(872,485,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_setor_w);
ds_alerta_modificado_w	:= '';
qt_idade_w	:= nvl(obter_idade_pf(:new.CD_PACIENTE,sysdate,'A'),0);
:new.ie_rn	:= nvl(:new.ie_rn,'N');

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.dt_sinal_vital,sysdate+10) <> :new.dt_sinal_vital) and
	(:new.dt_sinal_vital is not null) then
	:new.ds_utc		:= obter_data_utc(:new.dt_sinal_vital,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

select 	max(ie_motivo_dor_inapto),
	nvl(max(qt_horas_passado_sv),0)
into	ie_motivo_dor_inapto_w,
	qt_horas_passado_sv_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if 	((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) and
	( ie_motivo_dor_inapto_w = 'S') and
	((:new.cd_escala_dor = 'IA') and (:new.nr_seq_inapto_dor is null))then
	-- E necessario informar o motivo do paciente estar inapto para a avaliacao da dor.
	Wheb_mensagem_pck.exibir_mensagem_abort(264596);
end if;

if	((:new.dt_liberacao is not null) and
	 (:old.dt_liberacao is null)) then

	 If	(:new.cd_escala_dor is null) then

		:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;
		:new.qt_escala_dor		:= null;
		:new.nr_seq_inapto_dor		:= null;


	else
                    if :new.cd_escala_dor = 'CFT' then
                       update ESCALA_COMFORT_B
                       set  dt_liberacao = :new.dt_liberacao
                       where nr_seq_sv = :new.nr_sequencia;
                    end if;
               end  if;

	if	(:new.cd_escala_dor <> 'IA') and
		(:new.cd_escala_dor is not null) and
		(:new.qt_escala_dor is null) then
		-- E necessario informar a pontuacao da escala de dor selecionada.
		Wheb_mensagem_pck.exibir_mensagem_abort(264598);

		/*:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;
		:new.cd_escala_dor		:= null;
		:new.nr_seq_inapto_dor		:= null;*/

	end if;

	if	(:new.cd_escala_dor = 'IA')  then

		--:new.cd_escala_dor		:= null;
		:new.nr_seq_result_dor  	:= null;
		:new.nr_seq_topografia_dor 	:= null;
		:new.ie_lado 			:= null;
		:new.nr_seq_condicao_dor 	:= null;

	end if;

end if;

if	(:new.cd_escala_dor is not null) and
	(:new.qt_escala_dor is not null) and
	(:new.nr_seq_result_dor is null) then

	:new.nr_seq_result_dor := Obter_Seq_Result_Dor(:new.cd_escala_dor,:new.qt_escala_dor);

end if;


if	(:new.nr_Atendimento is not null) and
	(((nvl(cd_funcao_ativa_w,0) = 872) and (ie_setor_w = 'N')) or (nvl(cd_funcao_ativa_w,0) <> 872)) and
	(:new.cd_setor_atendimento is null)then
	begin
	:new.cd_setor_atendimento 	:= obter_setor_atendimento(:new.nr_atendimento);
	exception
	when others then
		null;
	end;
end if;

if	(:new.cd_paciente is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_paciente	:= obter_Pessoa_Atendimento(:new.nr_atendimento,'C');
end if;

if	(:new.nr_hora is null) or
	(:new.dt_sinal_vital <> :old.dt_sinal_vital) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_sinal_vital,'hh24'),'hh24'));
	end;
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_sinal_vital is null) or
	 (:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_sinal_vital <> :old.dt_sinal_vital)) then
	begin
	ds_hora_w	:= lpad(:new.nr_hora,2,'0');
	dt_registro_w	:= trunc(:new.dt_sinal_vital,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_sinal_vital,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_sinal_vital,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_referencia	:= round(:new.dt_sinal_vital,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_sinal_vital,'hh24') - to_date(to_char(:new.dt_sinal_vital,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;
total_escore_alerta_w:=0;
if 	((:new.qt_freq_cardiaca >= 51) and
	(:new.qt_freq_cardiaca 	<= 100)) then
		total_escore_alerta_w:=0;
elsif	((:new.qt_freq_cardiaca >= 41)	and
	(:new.qt_freq_cardiaca	<= 50))	or
	((:new.qt_freq_cardiaca >= 101)	and
	(:new.qt_freq_cardiaca	<= 110))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' ||  :new.qt_freq_cardiaca;
elsif	(:new.qt_freq_cardiaca 	<= 40)	or
	((:new.qt_freq_cardiaca >= 111)	and
	(:new.qt_freq_cardiaca	<= 120))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' || :new.qt_freq_cardiaca;
elsif	(:new.qt_freq_cardiaca 	>  120)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := Obter_desc_expressao(290490,'Freq. cardiaca') || ' = ' || :new.qt_freq_cardiaca;
end if;

if 	((:new.qt_freq_resp 	>= 10) and
	(:new.qt_freq_resp 	<= 14)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_freq_resp 	>= 15)	and
	(:new.qt_freq_resp	<= 20))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
elsif	(:new.qt_freq_resp 	<= 9)	or
	((:new.qt_freq_resp 	>= 21)	and
	(:new.qt_freq_resp	<= 29))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
elsif	(:new.qt_freq_resp 	>  30)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(557498,'Freq. respiratoria') || ' = ' || :new.qt_freq_resp;
end if;

if 	((:new.qt_pa_sistolica >= 101) and
	(:new.qt_pa_sistolica <= 199)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_pa_sistolica 	>= 81)	and
	(:new.qt_pa_sistolica	<= 100))	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
elsif	((:new.qt_pa_sistolica 	>= 71)	and
	(:new.qt_pa_sistolica 	<= 80))	or
	(:new.qt_pa_sistolica 	>= 200)	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
elsif	(:new.qt_pa_sistolica 	<= 70)	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(310302,'Pa. sistolica') || ' = ' || :new.qt_pa_sistolica;
end if;

if 	(:new.ie_nivel_consciencia 	= '0') then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	(:new.ie_nivel_consciencia 	= '1')	then
		total_escore_alerta_w:= total_escore_alerta_w + 1;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
elsif	(:new.ie_nivel_consciencia 	= '2')	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
elsif	(:new.ie_nivel_consciencia 	= '3')	then
		total_escore_alerta_w:= total_escore_alerta_w + 3;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) ||  Obter_desc_expressao(294136,null) || ' = ' || :new.ie_nivel_consciencia;
end if;

if 	((:new.qt_temp >= 35.1) and
	(:new.qt_temp <= 37.8)) then
		total_escore_alerta_w:= total_escore_alerta_w + 0;
elsif	((:new.qt_temp 	<= 35)	or
	(:new.qt_temp	>= 37.9))	then
		total_escore_alerta_w:= total_escore_alerta_w + 2;
		ds_alerta_modificado_w := ds_alerta_modificado_w || chr(13) || chr(10) || Obter_desc_expressao(299207,'Temperatura') || ' = ' || :new.qt_temp;
end if;

if	(:new.qt_pa_diastolica is not null) and
	(:new.qt_pa_sistolica is not null) and
	(:new.qt_pam is null) then
	:new.qt_pam 	:=	((:new.qt_pa_sistolica +(:new.qt_pa_diastolica * 2)) / 3);
end if;

if 	((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) 	then
	begin
		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		begin
		cd_setor_paciente_w	:= obter_setor_atendimento(:new.nr_atendimento);
		exception
			when others then
			null;
		end;
		open C01;
			loop
			fetch C01 into
				nr_seq_evento_w;
			exit when C01%notfound;
				begin
				gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_paciente,null,:new.nm_usuario,ds_alerta_modificado_w,:new.dt_sinal_vital);
				end;
			end loop;
		close C01;
	end;
end if;


if (((:new.dt_liberacao is not null) and  (:old.dt_liberacao is null)) and
		(:new.QT_TEMP > 38 OR :new.QT_TEMP < 36) OR
		(:new.QT_FREQ_RESP > 20) OR
		(:new.QT_FREQ_CARDIACA > 100) OR
		(:new.QT_PA_SISTOLICA < 90) OR
		(:new.QT_PAM < 65)) then
	begin
		cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
		SELECT	SUBSTR(CASE WHEN (:new.QT_TEMP > 38 OR :new.QT_TEMP < 36 ) THEN Obter_desc_expressao(299207,'Temperatura') || ': ' || :new.QT_TEMP ||
		SUBSTR(Obter_Atrib_Unid_Med_Unica('ATENDIMENTO_SINAL_VITAL','QT_TEMP',:new.DT_ATUALIZACAO, 1),1,30) || CHR(13) || CHR(10) ELSE '' END ||
		CASE WHEN (:new.QT_FREQ_RESP > 20) THEN Obter_desc_expressao(557498,null) || ': ' || :new.QT_FREQ_RESP || ' ('||Obter_desc_expressao(558642,'FR') || ')' || CHR(13) || CHR(10)  ELSE '' END ||
		CASE WHEN (:new.QT_FREQ_CARDIACA > 100) THEN Obter_desc_expressao(290490,null) || ': ' || :new.QT_FREQ_CARDIACA || ' ('||Obter_desc_expressao(311848,'FR') || ')' ||  CHR(13) || CHR(10) ELSE '' END||
		CASE WHEN (:new.QT_PA_SISTOLICA < 90) THEN Obter_desc_expressao(295132,'PA max') || ': ' || :new.QT_PA_SISTOLICA || ' (mmHg)' || CHR(13) || CHR(10)  ELSE ''  END ||
		CASE WHEN (:new.QT_PAM < 65) THEN Obter_desc_expressao(295239,'PAM') || ': ' || :new.QT_PAM || ' '|| Obter_desc_expressao(727862,'(mmHg)')|| CHR(13) || CHR(10) ELSE '' END,1,4000) DESCR
		INTO ds_Alerta_sinais_w
		FROM dual;

		begin
		cd_setor_paciente_w	:= obter_setor_atendimento(:new.nr_atendimento);
		exception
			when others then
			null;
		end;
		open C02;
			loop
			fetch C02 into
				nr_seq_evento_w;
			exit when C02%notfound;
				begin
				gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_paciente,null,:new.nm_usuario,ds_Alerta_sinais_w,:new.dt_sinal_vital);
				end;
			end loop;
		close C02;
	end;
end if;


if	(:new.qt_peso	is not null) then
	consiste_sinal_vital(	:new.cd_paciente,
				:new.qt_peso,
				12,
				ds_retorno_w,
				ie_retorno_w,
				:new.qt_peso_um,
				:new.nr_atendimento,
				null,
				:new.ie_rn);
	if	(ie_retorno_w	= 'E') then
		Wheb_mensagem_pck.exibir_mensagem_abort(264607,	'DS_RETORNO_W='||DS_RETORNO_W);
	end if;
end if;

if (nvl(:new.qt_peso,0) <> nvl(:old.qt_peso,0)) then
	cpoe_update_peso_atual_dialise( :new.nr_atendimento, :new.dt_sinal_vital, :new.qt_peso);
end if;


if	(:new.qt_peso is not null) and
	(:new.qt_altura_cm is not null) then
	begin
	if	(:new.qt_imc is null) then
		:new.qt_imc	:= Obter_IMC(:new.qt_peso,:new.qt_altura_cm);
	end if;

	exception
		when others then
		null;
	end;
	begin
	if	(:new.QT_SUPERF_CORPORIA is null) then
		:new.QT_SUPERF_CORPORIA	:= Obter_Superficie_Corporea(:new.qt_altura_cm,:new.qt_peso);
	end if;
	exception
		when others then
		null;
	end;

end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_atendimento is not null) then

	begin
		gravar_agend_rothman(:new.nr_atendimento,:new.nr_sequencia,'SV',:new.nm_usuario);
	exception
	when others then
		null;
	end;
/*
select	max(nvl(ie_sepse_lib_sv,'N'))
into	ie_sepse_lib_sv_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_sepse_lib_sv_w = 'S') and
	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin
	gerar_escala_sepse_js(:new.nr_atendimento,:new.cd_pessoa_fisica,:new.nr_sequencia,:new.nm_usuario);
	end;
end if;
*/
end if;

if	(:new.dt_liberacao is null) and
	(:new.dt_inativacao is null) and
	(nvl(:new.ie_integracao,'N')	= 'N') and
	((:new.dt_sinal_vital		> sysdate) or
	(qt_horas_passado_sv_w > 0 and :new.dt_sinal_vital	< sysdate - (qt_horas_passado_sv_w/24) )) then
	Wheb_mensagem_pck.exibir_mensagem_abort(15222,	'QT_HR_PASSADO_SV='||qt_horas_passado_sv_w);
end if;

if (:old.dt_liberacao is null and :new.dt_liberacao is not null) then --Apenas calcular quando liberar


	if (nvl(:new.qt_glicemia_capilar,0) > 0 and nvl(:new.qt_glicemia_mmol,0) = 0) then
		:new.qt_glicemia_mmol := converter_mg_mmol(:new.qt_glicemia_capilar, 'MMOL');
	elsif (nvl(:new.qt_glicemia_mmol,0) > 0 and nvl(:new.qt_glicemia_capilar,0) = 0) then
		:new.qt_glicemia_capilar := converter_mg_mmol(:new.qt_glicemia_mmol, 'MG');
	end if;

end if;

if ((:old.dt_liberacao is null and :new.dt_liberacao is not null) or
	(:old.dt_inativacao is null and :new.dt_inativacao is not null)) then

	send_physio_data_integration(null, null, :new.nr_atendimento, :new.cd_pessoa_fisica, :new.nr_sequencia, :new.dt_sinal_vital,
	:new.qt_pa_sistolica, :new.qt_pa_diastolica, :new.qt_freq_cardiaca, :new.qt_temp, :new.dt_atualizacao);

end if;

if (inserting) and (nvl(:new.ie_integracao,'N')	= 'S') then
   record_integration_notify(:new.cd_paciente,:new.nr_atendimento,'SV',:new.nr_sequencia,null,'S');
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.atend_sv_cft_analgesia
after insert or update ON TASY.ATENDIMENTO_SINAL_VITAL for each row
declare

qt_reg_w		number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if((:new.cd_escala_dor = 'CFT') and (:new.dt_liberacao is not null) and
	 (:old.dt_liberacao is null))then

	insert into atend_aval_analgesia(
		nr_sequencia,
		nr_atendimento,
		dt_avaliacao,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		cd_profissional,
		qt_comfort_b,
		ie_situacao)
	values(
		atend_aval_analgesia_seq.nextval,
		:new.nr_atendimento,
		nvl(:new.dt_sinal_vital,sysdate),
		sysdate,
		sysdate,
		:new.nm_usuario,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:new.qt_escala_dor,
				'A');

end if;


<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_sinal_vital_bb
AFTER INSERT OR UPDATE ON  atendimento_sinal_vital
FOR EACH ROW
BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

  IF (:old.dt_liberacao is null AND :new.dt_liberacao is not null AND NVL(:new.ie_integracao, 'N') <> 'S') THEN
    integrar_sinais_vitais_bb(
        nr_sequencia_p => :new.nr_sequencia,
        nr_atendimento_p => :new.nr_atendimento,
        dt_sinal_vital_p => :new.dt_sinal_vital,
        qt_freq_cardiaca_p => :new.qt_freq_cardiaca,
        qt_freq_resp_p => :new.qt_freq_resp,
        qt_saturacao_o2_p => :new.qt_saturacao_o2,
        qt_temp_p => :new.qt_temp,
        qt_pam_p => :new.qt_pam,
        qt_pa_sistolica_p => :new.qt_pa_sistolica,
        qt_pa_diastolica_p => :new.qt_pa_diastolica,
        ie_aparelho_pa_p => :new.ie_aparelho_pa);
  END IF;

  IF ((:old.dt_inativacao is null AND :new.dt_inativacao is not null) AND
      (:new.qt_freq_cardiaca is not null OR
      :new.qt_freq_resp is not null OR
      :new.qt_saturacao_o2 is not null OR
      :new.qt_temp is not null OR
      :new.qt_pam is not null OR
      :new.qt_pa_sistolica is not null OR
      :new.qt_pa_diastolica is not null)) THEN
    p_cancelar_flowsheet(:new.nr_sequencia, :new.nr_atendimento, 'A');
  END IF;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Atendimento_sinal_vital_Inup
AFTER INSERT OR UPDATE ON TASY.ATENDIMENTO_SINAL_VITAL FOR EACH ROW
DECLARE

ie_atualiza_sinal_vital_w		varchar2(1);
qt_reg_w				number(1);
cd_estabelecimento_w			number(5);
ie_altura_peso_pf_w			varchar2(2);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(3130, 57, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_atualiza_sinal_vital_w);

select	nvl(max(ie_altura_peso_pf),'N')
into	ie_altura_peso_pf_w
from	parametro_atendimento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_atualiza_sinal_vital_w = 'S') then
	update	prescr_medica a
	set	a.qt_peso = :new.qt_peso,
		a.qt_altura_cm = :new.qt_altura_cm
	where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.dt_prescricao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_sinal_vital)
	and	a.cd_pessoa_fisica = (Select cd_pessoa_fisica from atendimento_paciente where nr_atendimento = :new.nr_atendimento)
	and	exists(select 1 from paciente_atendimento b where b.nr_prescricao = a.nr_prescricao);
end if;

if	(ie_altura_peso_pf_w = 'S') then
	update	pessoa_fisica
	set	qt_altura_cm	= :new.qt_altura_cm,
		qt_peso		= :new.qt_peso
	where	cd_pessoa_fisica =	(Select	cd_pessoa_fisica
					 from	atendimento_paciente
					 where nr_atendimento = :new.nr_atendimento);
end if;



<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_ATENDIMENTO_SINAL_VITAL
before insert or update ON TASY.ATENDIMENTO_SINAL_VITAL for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_SINAL_VITAL_SBIS_DEL
before delete ON TASY.ATENDIMENTO_SINAL_VITAL for each row
declare
nr_log_seq_w		number(10);

begin
select log_alteracao_prontuario_seq.nextVal
into nr_log_seq_w
from dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 nvl(obter_pessoa_atendimento(:old.nr_atendimento,'C'), :old.cd_pessoa_fisica),
									 obter_desc_expressao(298520),
									 :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario,:old.nm_usuario));

end;
/


ALTER TABLE TASY.ATENDIMENTO_SINAL_VITAL ADD (
  CONSTRAINT ATESIVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_SINAL_VITAL ADD (
  CONSTRAINT ATESIVI_LIISPH_FK 
 FOREIGN KEY (NR_SEQ_MIN_PHILIPS) 
 REFERENCES TASY.LIB_ITENS_SV_PHILIPS (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_EHRUNME_FK 
 FOREIGN KEY (QT_PESO_UM) 
 REFERENCES TASY.EHR_UNIDADE_MEDIDA (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_ATESINI_FK 
 FOREIGN KEY (NR_SEQ_ESCUTA) 
 REFERENCES TASY.ATEND_ESCUTA_INICIAL (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_EQUPVS_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPMENT_VITAL_SIGN (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATESIVI_ESCDORE_FK 
 FOREIGN KEY (NR_SEQ_RESULT_DOR) 
 REFERENCES TASY.ESCALA_DOR_REGRA (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATESIVI_ATESIVI_FK 
 FOREIGN KEY (NR_SEQ_SINAL_VITAL) 
 REFERENCES TASY.ATENDIMENTO_SINAL_VITAL (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_TRPRAT_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_PRONTO_ATEND (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATESIVI_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_INTERVENCAO) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATESIVI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATESIVI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATESIVI_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA_DOR) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ATESIVI_PESFISI_FK2 
 FOREIGN KEY (CD_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATESIVI_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATESIVI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_CONDDOR_FK 
 FOREIGN KEY (NR_SEQ_CONDICAO_DOR) 
 REFERENCES TASY.CONDICAO_DOR (NR_SEQUENCIA),
  CONSTRAINT ATESIVI_INAPDOR_FK 
 FOREIGN KEY (NR_SEQ_INAPTO_DOR) 
 REFERENCES TASY.INAPTO_DOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_SINAL_VITAL TO NIVEL_1;

