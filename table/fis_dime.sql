ALTER TABLE TASY.FIS_DIME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_DIME CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_DIME
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_DECLARACAO     VARCHAR2(1 BYTE),
  IE_REGIME_APURACAO     VARCHAR2(1 BYTE),
  IE_PORTE_EMPRESA       VARCHAR2(1 BYTE),
  IE_CONSOLIDADA         VARCHAR2(1 BYTE),
  IE_CENTRALIZADA        VARCHAR2(1 BYTE),
  IE_TRANSF_CREDITOS     VARCHAR2(1 BYTE),
  IE_CREDITO_PRESU       VARCHAR2(1 BYTE),
  IE_INCENTIVOS_FISCAIS  VARCHAR2(1 BYTE),
  IE_MOVIMENTO           VARCHAR2(1 BYTE),
  IE_SUBST_TRIBUTARIO    VARCHAR2(1 BYTE),
  IE_ESCRITA_CONTABIL    VARCHAR2(1 BYTE),
  NR_TRABALHADORES       NUMBER(10),
  DT_INICIAL             DATE,
  DT_FINAL               DATE,
  DS_DIME                VARCHAR2(500 BYTE),
  DS_REGRA               VARCHAR2(255 BYTE)     NOT NULL,
  CD_CONTA_DESC_ICMS     VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISDIME_CONCONT_FK_I ON TASY.FIS_DIME
(CD_CONTA_DESC_ICMS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FISDIME_ESTABEL_FK_I ON TASY.FIS_DIME
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISDIME_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FISDIME_PK ON TASY.FIS_DIME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_DIME ADD (
  CONSTRAINT FISDIME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_DIME ADD (
  CONSTRAINT FISDIME_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FISDIME_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_DESC_ICMS) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.FIS_DIME TO NIVEL_1;

