ALTER TABLE TASY.RXT_AGENDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_AGENDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_AGENDA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_AGENDA                  DATE               NOT NULL,
  DT_ATUALIZACAO_NREC        DATE               NOT NULL,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_TRATAMENTO          NUMBER(10),
  NR_SEQ_EQUIPAMENTO         NUMBER(10)         NOT NULL,
  NR_MINUTO_DURACAO          NUMBER(10)         NOT NULL,
  IE_STATUS_AGENDA           VARCHAR2(15 BYTE)  NOT NULL,
  DT_AGENDAMENTO             DATE               NOT NULL,
  NM_USUARIO_AGENDA          VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF             NUMBER(10),
  DT_CONFIRMACAO             DATE,
  NM_USUARIO_CONFIRMACAO     VARCHAR2(15 BYTE),
  DT_CHEGADA                 DATE,
  NM_USUARIO_CHEGADA         VARCHAR2(15 BYTE),
  DT_TRATAMENTO              DATE,
  NM_USUARIO_TRATAMENTO      VARCHAR2(15 BYTE),
  DT_EXECUCAO                DATE,
  NM_USUARIO_EXECUCAO        VARCHAR2(15 BYTE),
  NM_USUARIO_ACESSO          VARCHAR2(15 BYTE),
  NR_SEQ_FASE                NUMBER(10),
  NR_SEQ_DIA                 NUMBER(10),
  NR_SEQ_DIA_FASE            NUMBER(10),
  IE_TIPO_AGENDA             VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  DT_SIMULACAO               DATE,
  NM_USUARIO_SIMULACAO       VARCHAR2(15 BYTE),
  DT_EXEC_SIMULACAO          DATE,
  NM_USUARIO_EXEC_SIMULACAO  VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_CAMPO_ROENTGEN      NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_FALTA        NUMBER(10),
  DT_CANCELAMENTO            DATE,
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  IE_CLASSIF_AGENDA          VARCHAR2(3 BYTE),
  NR_SEQ_MOTIVO_CANC         NUMBER(10),
  IE_TRANSFERENCIA           VARCHAR2(1 BYTE),
  CD_AGENDAMENTO_EXTERNO     VARCHAR2(250 BYTE),
  DT_CONF_PACIENTE           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RXTAGEN_ATEPACI_FK_I ON TASY.RXT_AGENDA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTAGEN_FAST_I ON TASY.RXT_AGENDA
(NR_SEQ_EQUIPAMENTO, IE_STATUS_AGENDA, DT_AGENDA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTAGEN_PESFISI_FK_I ON TASY.RXT_AGENDA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RXTAGEN_PK ON TASY.RXT_AGENDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAGEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTAGEN_RXMTCAN_FK_I ON TASY.RXT_AGENDA
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTAGEN_RXTCLAG_FK_I ON TASY.RXT_AGENDA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAGEN_RXTCLAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAGEN_RXTEQUI_FK_I ON TASY.RXT_AGENDA
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAGEN_RXTEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAGEN_RXTMOFA_FK_I ON TASY.RXT_AGENDA
(NR_SEQ_MOTIVO_FALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAGEN_RXTMOFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTAGEN_RXTTRAT_FK_I ON TASY.RXT_AGENDA
(NR_SEQ_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTAGEN_RXTTRAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RXT_Agenda_Update
BEFORE UPDATE ON TASY.RXT_AGENDA FOR EACH ROW
DECLARE

nr_sessao_w		number(10,0);
cd_perfil_w		number(5,0);
cd_pessoa_fisica_w	varchar2(10);
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	nvl(max(nr_sessao),0),
	nvl(max(cd_perfil),0)
into	nr_sessao_w,
	cd_perfil_w
from	rxt_parametro;

if	(:new.ie_status_agenda = 'E') and
	(:new.nr_seq_dia = nr_sessao_w) and
	(nr_sessao_w > 0) then
	begin
	if 	(cd_perfil_w > 0) then
		begin

		/* Obter dados paciente */
		select	nvl(rxt_obter_pf_tratamento(:new.nr_seq_tratamento,null),:new.cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	dual;

		if 	(cd_pessoa_fisica_w is not null) then
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(799247),
				wheb_mensagem_pck.get_texto(799241) || ' ' || cd_pessoa_fisica_w ||' - '|| obter_nome_pf(cd_pessoa_fisica_w) || chr(13) || chr (10) || wheb_mensagem_pck.get_texto(799252),
				:new.nm_usuario,
				sysdate,
				'N',
				'',
				cd_perfil_w ||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate);
		end if;
		end;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.rxt_agenda_befins_uk
before insert or update ON TASY.RXT_AGENDA for each row

/* dom�nio 2217 - RXT Status agenda
A	Aguardando
AP	Aplica��o parcial
B	Bloqueada
C	Cancelado
E	Executado
F	Falta
H	N�o Houve tratamento
L	Livre
M	Agendado
P	Planejamento
S	Em simula��o
SE	Simula��o exec
T	Em tratamento
V	Confirmado
*/
declare
ie_registros_existentes_w       number(10);

pragma autonomous_transaction;
begin
        if      (:new.IE_STATUS_AGENDA not in ('C','F','H','L','B')) then

                select  count(*)
                into    ie_registros_existentes_w
                from    rxt_agenda
                where   nr_seq_equipamento      =  :new.NR_SEQ_EQUIPAMENTO
                and     nr_sequencia            <> :new.NR_SEQUENCIA
                and     ie_status_agenda        =  :new.IE_STATUS_AGENDA
                and     to_date(to_char(dt_agenda, 'dd/mm/yyyy hh24:mi'), 'dd/mm/yyyy hh24:mi') = to_date(to_char(:new.DT_AGENDA, 'dd/mm/yyyy hh24:mi'), 'dd/mm/yyyy hh24:mi')
                and     ie_status_agenda        not  in ('C', 'F', 'H','L','B')
		and	((nvl(ie_transferencia, 'N') = 'N')
			or 	((nvl(ie_transferencia, 'N') = 'S')
			and 	(nr_seq_tratamento <> :new.NR_SEQ_TRATAMENTO)));

                if      (ie_registros_existentes_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(419973);
                end if;
        end if;

end;
/


ALTER TABLE TASY.RXT_AGENDA ADD (
  CONSTRAINT RXTAGEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_AGENDA ADD (
  CONSTRAINT RXTAGEN_RXTTRAT_FK 
 FOREIGN KEY (NR_SEQ_TRATAMENTO) 
 REFERENCES TASY.RXT_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT RXTAGEN_RXTCLAG_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.RXT_CLASSIF_AGENDA (NR_SEQUENCIA),
  CONSTRAINT RXTAGEN_RXTEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.RXT_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT RXTAGEN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RXTAGEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT RXTAGEN_RXTMOFA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FALTA) 
 REFERENCES TASY.RXT_MOTIVO_FALTA (NR_SEQUENCIA),
  CONSTRAINT RXTAGEN_RXMTCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.RXT_MOTIVO_CANCELAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RXT_AGENDA TO NIVEL_1;

