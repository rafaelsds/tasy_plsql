ALTER TABLE TASY.QUA_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_DOCUMENTO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  NR_SEQ_TIPO                  NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_DOCUMENTO                 VARCHAR2(20 BYTE) NOT NULL,
  NM_DOCUMENTO                 VARCHAR2(255 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  DT_APROVACAO                 DATE,
  DT_EMISSAO                   DATE,
  NM_USUARIO_APROV             VARCHAR2(15 BYTE),
  CD_PESSOA_APROV              VARCHAR2(10 BYTE),
  DS_ARQUIVO                   VARCHAR2(255 BYTE),
  DS_PALAVRA_CHAVE             VARCHAR2(255 BYTE),
  NR_SEQ_LOC                   NUMBER(10),
  CD_PESSOA_ELABORACAO         VARCHAR2(10 BYTE),
  DT_ELABORACAO                DATE,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_STATUS                    VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_FORMA_ARMAZENAMENTO       VARCHAR2(1 BYTE),
  DS_FORMA_RECUPERACAO         VARCHAR2(255 BYTE),
  IE_FORMA_DESCARTE            VARCHAR2(2 BYTE),
  QT_TEMPO_RETENCAO_MES        NUMBER(2),
  QT_TEMPO_RETENCAO_ANO        NUMBER(2),
  IE_TEMPO_RETENCAO            VARCHAR2(1 BYTE),
  QT_TEMPO_RETENCAO            NUMBER(4,2),
  CD_PESSOA_VALIDACAO          VARCHAR2(10 BYTE),
  DT_VALIDACAO                 DATE,
  DS_FORMA_PROTECAO            VARCHAR2(255 BYTE),
  QT_DIAS_REVISAO              NUMBER(5),
  IE_PERMITE_EMAIL             VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_REF_DOC          NUMBER(10),
  DT_REVALIDACAO               DATE,
  NM_USUARIO_REVALIDACAO       VARCHAR2(15 BYTE),
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DT_INATIVACAO                DATE,
  CD_DOCUMENTO_EXT             VARCHAR2(20 BYTE),
  IE_EXTERNO                   VARCHAR2(15 BYTE),
  DS_IMPACTO                   LONG,
  NR_SEQ_MOTIVO_INATIVACAO     NUMBER(10),
  QT_DIAS_VALIDACAO            NUMBER(5),
  QT_DIAS_APROVACAO            NUMBER(5),
  IE_PERMITE_SALVAR            VARCHAR2(15 BYTE),
  IE_PERMITE_IMPRIMIR          VARCHAR2(15 BYTE),
  IE_PERMANENTE                VARCHAR2(15 BYTE),
  NM_USUARIO_SOLIC_TREIN       VARCHAR2(15 BYTE),
  DT_SOLIC_TREINAMENTO         DATE,
  DT_REPROVACAO                DATE,
  IE_DOC_RESP                  VARCHAR2(1 BYTE),
  IE_PERGUNTA                  VARCHAR2(1 BYTE),
  IE_ARMAZENA_ARQ_MORTO        VARCHAR2(15 BYTE),
  IE_FORMA_DESCARTE_ARQ_MORTO  VARCHAR2(2 BYTE),
  QT_TEMPO_RETENCAO_ARQ_MORTO  NUMBER(4,2),
  IE_TEMPO_RETENCAO_ARQ_MORTO  VARCHAR2(1 BYTE),
  NR_SEQ_SUPERIOR              NUMBER(10),
  NR_SEQ_IDIOMA                NUMBER(10),
  CD_EXP_DOCUMENTO             NUMBER(10),
  NR_SEQ_ORDEM_TREIN           NUMBER(10),
  NR_SEQ_CLASSIF               NUMBER(10),
  NR_SEQ_DOC_REF               NUMBER(5,2),
  IE_PERMITE_IMPRIMIR_DOC      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUADOCU_DICEXPR_FK_I ON TASY.QUA_DOCUMENTO
(CD_EXP_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_ESTABEL_FK_I ON TASY.QUA_DOCUMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADOCU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUADOCU_MANORSE_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_ORDEM_TREIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_PESFISI_FK_I ON TASY.QUA_DOCUMENTO
(CD_PESSOA_APROV)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_PESFISI_FK2_I ON TASY.QUA_DOCUMENTO
(CD_PESSOA_ELABORACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_PESFISI_FK3_I ON TASY.QUA_DOCUMENTO
(CD_PESSOA_VALIDACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUADOCU_PK ON TASY.QUA_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_QUACLDOC_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_QUADOCU_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_QUALODO_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_LOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADOCU_QUALODO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUADOCU_QUAMIDO_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_MOTIVO_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADOCU_QUAMIDO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUADOCU_QUATIDO_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_QUATREF_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_TIPO_REF_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUADOCU_QUATREF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUADOCU_SETATEN_FK_I ON TASY.QUA_DOCUMENTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUADOCU_TASYIDI_FK_I ON TASY.QUA_DOCUMENTO
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.qua_documento_Update
BEFORE UPDATE ON TASY.QUA_DOCUMENTO FOR EACH ROW
declare
cd_pessoa_fisica_w	varchar2(10);
cd_cargo_w		number(10);
ie_permite_aprov_w		varchar2(1);
qt_registro_w		Number(5);
qt_reg_w			number(1);
cd_cargo_pf_aprov_w	number(10);

Cursor C01 is
	select	cd_pessoa_fisica,
		cd_cargo
	from	qua_tipo_doc_aprov
	where	nr_seq_tipo_doc = :new.nr_seq_tipo;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;
ie_permite_aprov_w	:= 'N';

if	(:new.dt_aprovacao is not null) and
	(:old.dt_aprovacao is null) and (:new.nr_seq_superior is null) then
	begin

	select	count(*)
	into	qt_registro_w
	from	qua_tipo_doc_aprov a
	where	(a.cd_pessoa_fisica is not null
		or a.cd_cargo is not null)
	and not exists(	select	1
			from	qua_tipo_doc_aprov x
			where	a.nr_seq_tipo_doc = x.nr_seq_tipo_doc
			and	x.cd_pessoa_fisica is null
			and	x.cd_cargo is null)
	and	a.nr_seq_tipo_doc = :new.nr_seq_tipo;

	if	(qt_registro_w > 0) then
		begin

		begin
		select	cd_cargo
		into	cd_cargo_pf_aprov_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_aprov;
		exception
		when others then
			cd_cargo_pf_aprov_w := null;
		end;

		open  C01;
		loop
		fetch C01 into
			cd_pessoa_fisica_w,
			cd_cargo_w;
		exit when C01%notfound;
			begin
			if	(:new.cd_pessoa_aprov = nvl(cd_pessoa_fisica_w,'0')) or
				(cd_cargo_pf_aprov_w = nvl(cd_cargo_w,'0')) then
				ie_permite_aprov_w	:= 'S';
			end if;
			end;
		end loop;
		close C01;

		select	count(*)
		into	qt_registro_w
		from	qua_doc_aprov
		where	nr_seq_doc = :new.nr_sequencia;

		if	(qt_registro_w > 0) then
			begin
			if	(ie_permite_aprov_w = 'N') then
				begin
				select	'S'
				into	ie_permite_aprov_w
				from	qua_doc_aprov a
				where	a.cd_cargo is not null
				and	a.dt_aprovacao is not null
				and	a.nr_seq_doc = :new.nr_sequencia
				and	a.cd_cargo = substr(Obter_Dados_Usuario_Opcao(:new.nm_usuario,'R'),1,10)
				and exists(	select	1
						from	qua_tipo_doc_aprov b
						where	a.cd_cargo = b.cd_cargo
						and	b.cd_cargo is not null
						and	b.nr_seq_tipo_doc = :new.nr_seq_tipo)
				and	rownum < 2;
				exception
				when others then
					ie_permite_aprov_w := 'N';
				end;
			end if;

			if	(ie_permite_aprov_w = 'N') then
				begin
				select	'S'
				into	ie_permite_aprov_w
				from	qua_doc_aprov a
				where	a.cd_setor_atendimento is not null
				and	a.dt_aprovacao is not null
				and	a.nr_seq_doc 		= :new.nr_sequencia
				and	a.cd_setor_atendimento 	= substr(Obter_Dados_Usuario_Opcao(:new.nm_usuario,'S'),1,10)
				and exists(	select	1
						from	qua_tipo_doc_aprov b
						where	a.cd_cargo = b.cd_cargo
						and	b.cd_cargo is not null
						and	b.nr_seq_tipo_doc = :new.nr_seq_tipo)
				and	rownum < 2;
				exception
				when others then
					ie_permite_aprov_w := 'N';
				end;
			end if;
			end;
		end if;

		if	(ie_permite_aprov_w = 'N') then
			wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(188032);
		end if;
		end;
	end if;
	end;
end if;

if	(:old.ie_situacao <> :new.ie_situacao) then
	begin
	if	(:new.ie_situacao = 'I') then
		:new.dt_inativacao 		:= sysdate;
		:new.nm_usuario_inativacao	:= :new.nm_usuario;

		delete from qms_treinamento_cargo
		where nr_seq_documento = :new.nr_sequencia;
	else
		:new.dt_inativacao 		:= null;
		:new.nm_usuario_inativacao	:= '';
	end if;
	end;
end if;

if	(:old.ie_status <> :new.ie_status) then
	begin
	if	(:new.ie_status = 'T') then
		begin
		:new.dt_solic_treinamento	:= sysdate;
		:new.nm_usuario_solic_trein	:= :new.nm_usuario;
		end;
	else
		begin
		:new.dt_solic_treinamento	:= null;
		:new.nm_usuario_solic_trein	:= '';
		end;
	end if;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.qua_documento_update_afterpost
AFTER UPDATE ON TASY.QUA_DOCUMENTO FOR EACH ROW
declare
nr_seq_ordem_w			man_ordem_servico.nr_sequencia%type;
nr_seq_estagio_w		man_ordem_servico.nr_seq_estagio%type;

Cursor	c01 is
select	nr_sequencia
from	man_ordem_servico
where	nr_seq_documento = :new.nr_sequencia
and	ie_tipo_ordem = 11 /*Solicita��o de Normatiza��o*/
and	ie_status_ordem <> '3';

begin

if((:new.ie_status = 'D') and (:new.dt_aprovacao is not null)) then
	begin
		open C01;
		loop
		fetch C01 into
			nr_seq_ordem_w;
		exit when C01%notfound;
			begin

			if	(	(:new.nr_seq_classif = 1)
				and	:new.nr_seq_tipo in (2, 3, 5, 15)
				and	(obter_se_base_corp = 'S'
					or	obter_se_base_wheb = 'S')) then
				begin
					select	max(mes.nr_sequencia)
					into	nr_seq_estagio_w
					from	man_estagio_processo mes
					where	mes.nr_sequencia = 1871;

					if	(nr_seq_estagio_w is not null) then
						begin
							update	man_ordem_servico
							set	nr_seq_estagio = nr_seq_estagio_w
							where	nr_sequencia = nr_seq_ordem_w;
						end;
					end if;
				end;
			else
				begin
					/* 'OS encerrada automaticamente ap�s aprova��o do documento '||:new.cd_documento||' - '||substr(obter_desc_expressao(:new.cd_exp_documento, :new.nm_documento), 1, 255) */
					man_encerrar_OS(nr_seq_ordem_w,9,:new.nm_usuario_aprov,substr(WHEB_MENSAGEM_PCK.get_texto(823511, 'NEW_CD_DOCUMENTO='||:new.cd_documento||';DESC_EXPRESSAO_DOC='||substr(obter_desc_expressao(:new.cd_exp_documento, :new.nm_documento), 1, 255)),1,255),'N');
				end;
			end if;
			end;
		end loop;
		close C01;
	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.QUA_DOCUMENTO_tp  after update ON TASY.QUA_DOCUMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_REPROVACAO,1,500);gravar_log_alteracao(to_char(:old.DT_REPROVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REPROVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REPROVACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INATIVACAO,1,500);gravar_log_alteracao(to_char(:old.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INATIVACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_EMISSAO,1,500);gravar_log_alteracao(to_char(:old.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_APROVACAO,1,500);gravar_log_alteracao(to_char(:old.DT_APROVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_APROVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_APROVACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_SUPERIOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_SUPERIOR,1,4000),substr(:new.NR_SEQ_SUPERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SUPERIOR',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_REVALIDACAO,1,500);gravar_log_alteracao(to_char(:old.DT_REVALIDACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REVALIDACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REVALIDACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ARQUIVO,1,500);gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_DOCUMENTO,1,500);gravar_log_alteracao(substr(:old.CD_DOCUMENTO,1,4000),substr(:new.CD_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOCUMENTO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_DOCUMENTO,1,500);gravar_log_alteracao(substr(:old.NM_DOCUMENTO,1,4000),substr(:new.NM_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_DOCUMENTO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ELABORACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ELABORACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ELABORACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ELABORACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_VALIDACAO,1,500);gravar_log_alteracao(to_char(:old.DT_VALIDACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_VALIDACAO,1,4000),substr(:new.CD_PESSOA_VALIDACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_VALIDACAO',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_APROV,1,4000),substr(:new.CD_PESSOA_APROV,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_APROV',ie_log_w,ds_w,'QUA_DOCUMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.QUA_DOCUMENTO ADD (
  CONSTRAINT QUADOCU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_DOCUMENTO ADD (
  CONSTRAINT QUADOCU_QUACLDOC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.QUA_CLASSIF_DOC (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUADOCU_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_DOCUMENTO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT QUADOCU_QUADOCU_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.QUA_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_QUAMIDO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INATIVACAO) 
 REFERENCES TASY.QUA_MOTIVO_INATIV_DOC (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_TREIN) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_QUATIDO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.QUA_TIPO_DOC (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT QUADOCU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_APROV) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADOCU_QUALODO_FK 
 FOREIGN KEY (NR_SEQ_LOC) 
 REFERENCES TASY.QUA_LOC_DOC (NR_SEQUENCIA),
  CONSTRAINT QUADOCU_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_ELABORACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADOCU_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_VALIDACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUADOCU_QUATREF_FK 
 FOREIGN KEY (NR_SEQ_TIPO_REF_DOC) 
 REFERENCES TASY.QUA_TIPO_REFERENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_DOCUMENTO TO NIVEL_1;

