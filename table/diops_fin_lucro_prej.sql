ALTER TABLE TASY.DIOPS_FIN_LUCRO_PREJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_FIN_LUCRO_PREJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_FIN_LUCRO_PREJ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_PERIODO       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_CONTA             VARCHAR2(255 BYTE),
  VL_SALDO_FINAL       NUMBER(15,2),
  DS_DESCRICAO_CONTA   VARCHAR2(255 BYTE),
  NR_SEQ_OPERADORA     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIFILPR_DIOPERI_FK_I ON TASY.DIOPS_FIN_LUCRO_PREJ
(NR_SEQ_PERIODO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIFILPR_ESTABEL_FK_I ON TASY.DIOPS_FIN_LUCRO_PREJ
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIFILPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DIFILPR_PK ON TASY.DIOPS_FIN_LUCRO_PREJ
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIFILPR_PLSOUTO_FK_I ON TASY.DIOPS_FIN_LUCRO_PREJ
(NR_SEQ_OPERADORA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIFILPR_PLSOUTO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.DIOPS_FIN_LUCRO_PREJ ADD (
  CONSTRAINT DIFILPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIOPS_FIN_LUCRO_PREJ ADD (
  CONSTRAINT DIFILPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DIFILPR_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA),
  CONSTRAINT DIFILPR_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OPERADORA) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIOPS_FIN_LUCRO_PREJ TO NIVEL_1;

