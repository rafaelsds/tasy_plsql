ALTER TABLE TASY.PARTOGRAMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARTOGRAMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARTOGRAMA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_HORA                    NUMBER(2),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_REGISTRO                DATE               NOT NULL,
  QT_DILATACAO_CERVICAL      NUMBER(2),
  QT_BCF                     NUMBER(3),
  IE_PLANO_LEE               VARCHAR2(15 BYTE),
  IE_INTERV_CONTRACOES       VARCHAR2(15 BYTE),
  IE_ASPECTO_LIQUIDO         NUMBER(1),
  IE_BOLSA                   VARCHAR2(15 BYTE),
  IE_OCITOCINA               VARCHAR2(1 BYTE)   NOT NULL,
  IE_APRESENTACAO            VARCHAR2(15 BYTE),
  QT_OCITOCINA               NUMBER(15,3),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_CONTRACAO               NUMBER(2),
  IE_PLANO_HODGE             VARCHAR2(15 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_APRESENTACAO_CEFA       VARCHAR2(15 BYTE),
  IE_EXAUSTAO_MAE            VARCHAR2(1 BYTE),
  IE_ESTAG_PARTO             VARCHAR2(1 BYTE),
  QT_ESTAG_PARTO_MIN         NUMBER(10),
  IE_ESTAG_DILATACAO         VARCHAR2(1 BYTE),
  QT_ESTAG_DILATACAO_MIN     NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARTOGR_ATCONSPEPA_FK_I ON TASY.PARTOGRAMA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARTOGR_ATEPACI_FK_I ON TASY.PARTOGRAMA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARTOGR_I1 ON TASY.PARTOGRAMA
(NR_ATENDIMENTO, DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARTOGR_PESFISI_FK_I ON TASY.PARTOGRAMA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARTOGR_PK ON TASY.PARTOGRAMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARTOGR_TASASDI_FK_I ON TASY.PARTOGRAMA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARTOGR_TASASDI_FK2_I ON TASY.PARTOGRAMA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.partograma_atual
before insert or update ON TASY.PARTOGRAMA for each row
declare

ie_ultima_hora_registrada_w	number(2);
dt_registro_hora_w		date;
novo_ie_hora_w			number(10);

pragma autonomous_transaction;

begin

if	(:new.ie_hora is null) then
	select	max(ie_hora)
	into	ie_ultima_hora_registrada_w
	from	partograma
	where	nr_atendimento = :new.nr_atendimento;

	if	(ie_ultima_hora_registrada_w is null) then
		:new.ie_hora	:= 0;
	else
		select	min(dt_registro)
		into	dt_registro_hora_w
		from	partograma
		where	nr_atendimento = :new.nr_atendimento
		and	ie_hora = ie_ultima_hora_registrada_w;

		novo_ie_hora_w	:= ie_ultima_hora_registrada_w + trunc((:new.dt_registro - dt_registro_hora_w)*24);

		if	(novo_ie_hora_w < 0) then
			:new.ie_hora	:= 0;
		elsif	(novo_ie_hora_w > 23) then
			:new.ie_hora	:= null;
		else
			:new.ie_hora	:= novo_ie_hora_w;
		end if;
	end if;
end if;
end;
/


ALTER TABLE TASY.PARTOGRAMA ADD (
  CONSTRAINT PARTOGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARTOGRAMA ADD (
  CONSTRAINT PARTOGR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PARTOGR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PARTOGR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARTOGR_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PARTOGR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PARTOGRAMA TO NIVEL_1;

