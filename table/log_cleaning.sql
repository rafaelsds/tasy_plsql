ALTER TABLE TASY.LOG_CLEANING
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_CLEANING CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_CLEANING
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_LOG_TYPE          VARCHAR2(20 BYTE)        NOT NULL,
  QT_MONTH             NUMBER(3)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_LOG_ESTAB     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOG_CLNG_ESTABEL_FK_I ON TASY.LOG_CLEANING
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOG_CLNG_LOG_ESTAB_FK_I ON TASY.LOG_CLEANING
(NR_SEQ_LOG_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOG_CLNG_PK ON TASY.LOG_CLEANING
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOG_CLNG_UK ON TASY.LOG_CLEANING
(CD_ESTABELECIMENTO, CD_LOG_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOG_CLEANING_ATUAL
before insert or update ON TASY.LOG_CLEANING for each row
declare

qt_month_w	number(3,0);

begin

	select 	nvl(max(qt_storage_month),0)
	into 	qt_month_w
	from 	log_establishment
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if (:new.qt_month >= qt_month_w) then
		if (qt_month_w > 1) then
            wheb_mensagem_pck.exibir_mensagem_abort(805159, 'QT_MONTH=' || qt_month_w);
        else
            wheb_mensagem_pck.exibir_mensagem_abort(805163, 'QT_MONTH=' || qt_month_w);
        end if;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOG_CLEANING_tp  after update ON TASY.LOG_CLEANING FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_LOG_ESTAB,1,4000),substr(:new.NR_SEQ_LOG_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOG_ESTAB',ie_log_w,ds_w,'LOG_CLEANING',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'LOG_CLEANING',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'LOG_CLEANING',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MONTH,1,4000),substr(:new.QT_MONTH,1,4000),:new.nm_usuario,nr_seq_w,'QT_MONTH',ie_log_w,ds_w,'LOG_CLEANING',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOG_TYPE,1,4000),substr(:new.CD_LOG_TYPE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOG_TYPE',ie_log_w,ds_w,'LOG_CLEANING',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOG_CLEANING ADD (
  CONSTRAINT LOG_CLNG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT LOG_CLNG_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_LOG_TYPE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_CLEANING ADD (
  CONSTRAINT LOG_CLNG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT LOG_CLNG_LOG_ESTAB_FK 
 FOREIGN KEY (NR_SEQ_LOG_ESTAB) 
 REFERENCES TASY.LOG_ESTABLISHMENT (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_CLEANING TO NIVEL_1;

