ALTER TABLE TASY.MOTIVO_ISOLAMENTO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_ISOLAMENTO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_ISOLAMENTO_ATEND
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_ISOL            NUMBER(10)      NOT NULL,
  DT_INICIO_ISOLAMENTO          DATE,
  DT_FIM_ISOLAMENTO             DATE,
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  DS_MOTIVO                     VARCHAR2(255 BYTE),
  IE_TIPO_ISOLAMENTO            VARCHAR2(2 BYTE),
  NR_SEQ_CIH_TIPO_ISOLAMENTO    NUMBER(10),
  NR_SEQ_ATENDIMENTO_PRECAUCAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOTISAT_ATEPACI_FK_I ON TASY.MOTIVO_ISOLAMENTO_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOTISAT_ATEPREC_FK_I ON TASY.MOTIVO_ISOLAMENTO_ATEND
(NR_SEQ_ATENDIMENTO_PRECAUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOTISAT_ATEPREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOTISAT_CIHTISO_FK_I ON TASY.MOTIVO_ISOLAMENTO_ATEND
(NR_SEQ_CIH_TIPO_ISOLAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOTISAT_CIHTISO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOTISAT_MOTPACON_FK_I ON TASY.MOTIVO_ISOLAMENTO_ATEND
(NR_SEQ_MOTIVO_ISOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOTISAT_MOTPACON_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MOTISAT_PK ON TASY.MOTIVO_ISOLAMENTO_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOTISAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.MOTIVO_ISOLAMENTO_ATEND ADD (
  CONSTRAINT MOTISAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_ISOLAMENTO_ATEND ADD (
  CONSTRAINT MOTISAT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MOTISAT_MOTPACON_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ISOL) 
 REFERENCES TASY.MOTIVO_ISOLAMENTO (NR_SEQUENCIA),
  CONSTRAINT MOTISAT_CIHTISO_FK 
 FOREIGN KEY (NR_SEQ_CIH_TIPO_ISOLAMENTO) 
 REFERENCES TASY.CIH_TIPO_ISOLAMENTO (NR_SEQUENCIA),
  CONSTRAINT MOTISAT_ATEPREC_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO_PRECAUCAO) 
 REFERENCES TASY.ATENDIMENTO_PRECAUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MOTIVO_ISOLAMENTO_ATEND TO NIVEL_1;

