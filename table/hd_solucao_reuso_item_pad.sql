ALTER TABLE TASY.HD_SOLUCAO_REUSO_ITEM_PAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_SOLUCAO_REUSO_ITEM_PAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_SOLUCAO_REUSO_ITEM_PAD
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA      VARCHAR2(30 BYTE),
  PR_SOLUCAO             NUMBER(10,3),
  NR_SEQ_SOLUCAO_PADRAO  NUMBER(10),
  CD_MATERIAL            NUMBER(10)             NOT NULL,
  IE_REGISTRAR_LOTE      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDSREIP_HDSOREP_FK_I ON TASY.HD_SOLUCAO_REUSO_ITEM_PAD
(NR_SEQ_SOLUCAO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDSREIP_HDSOREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDSREIP_MATERIA_FK_I ON TASY.HD_SOLUCAO_REUSO_ITEM_PAD
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDSREIP_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDSREIP_PK ON TASY.HD_SOLUCAO_REUSO_ITEM_PAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDSREIP_PK
  MONITORING USAGE;


CREATE INDEX TASY.HDSREIP_UNIMEDI_FK_I ON TASY.HD_SOLUCAO_REUSO_ITEM_PAD
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDSREIP_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.HD_SOLUCAO_REUSO_ITEM_PAD ADD (
  CONSTRAINT HDSREIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_SOLUCAO_REUSO_ITEM_PAD ADD (
  CONSTRAINT HDSREIP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT HDSREIP_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT HDSREIP_HDSOREP_FK 
 FOREIGN KEY (NR_SEQ_SOLUCAO_PADRAO) 
 REFERENCES TASY.HD_SOLUCAO_REUSO_PADRAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_SOLUCAO_REUSO_ITEM_PAD TO NIVEL_1;

