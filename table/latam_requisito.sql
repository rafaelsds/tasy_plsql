ALTER TABLE TASY.LATAM_REQUISITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LATAM_REQUISITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LATAM_REQUISITO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_MODULO            NUMBER(10),
  DS_REQUISITO             VARCHAR2(4000 BYTE)  NOT NULL,
  QT_TEMPO_HORAS           NUMBER(5),
  QT_PROGRAMADOR           NUMBER(2),
  VL_HORA                  NUMBER(15,4),
  VL_CUSTO                 NUMBER(15,4),
  CD_RESPONSAVEL           VARCHAR2(10 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  DS_CRITERIO              VARCHAR2(2000 BYTE),
  QT_PESO_CLIENTE          NUMBER(2),
  QT_PONTUACAO_OBTIDA      NUMBER(2),
  QT_MAXIMO_PONTOS         NUMBER(5),
  QT_NOTA                  NUMBER(2),
  CD_REQUISITO             VARCHAR2(30 BYTE)    NOT NULL,
  NR_SEQ_GAP               NUMBER(10),
  IE_COMPLEXIDADE          NUMBER(2),
  IE_TIPO_REQUISITO        VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO         VARCHAR2(2 BYTE),
  IE_IMPORTANCIA_CLIENTE   VARCHAR2(2 BYTE),
  IE_IMPORTANCIA_PAIS      VARCHAR2(2 BYTE),
  IE_REGULATORIO           VARCHAR2(1 BYTE),
  IE_SITUACAO_REQ          VARCHAR2(2 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM_SERV        NUMBER(10),
  IE_LIBERAR_DESENV        VARCHAR2(2 BYTE),
  CD_CONSULTOR_RESP        VARCHAR2(10 BYTE),
  CD_RESPONSAVEL_BA        VARCHAR2(10 BYTE),
  IE_ESFORCO_DESENV        NUMBER(2),
  CD_REQUISITO_AGRUP       VARCHAR2(30 BYTE),
  IE_IMPORTANCIA_PHILIPS   VARCHAR2(2 BYTE),
  IE_IMPORTANCIA_DESENV    VARCHAR2(1 BYTE),
  DS_PROCESSO              VARCHAR2(255 BYTE),
  DS_SUB_PROCESSO          VARCHAR2(255 BYTE),
  NR_SEQ_PROJ_CRON_ETAPA   NUMBER(10),
  NR_SEQ_MILESTONE         NUMBER(10),
  NR_SEQ_CUSTOMER          NUMBER(10),
  DS_REQUISITO_BREVE       VARCHAR2(80 BYTE),
  QT_HORA_INICIALIZACAO    NUMBER(5),
  NM_USUARIO_LIB           VARCHAR2(15 BYTE),
  DT_LIBERACAO             DATE,
  IE_NECESSITA_TECNOLOGIA  VARCHAR2(1 BYTE),
  IE_NECESSITA_DESIGN      VARCHAR2(1 BYTE),
  PR_CONFIANCA             NUMBER(15,2),
  QT_HORAS_TEC             NUMBER(15,2),
  QT_HORAS_DESIGN          NUMBER(15,2),
  IE_SOB_MEDIDA            VARCHAR2(1 BYTE),
  NR_SEQ_SUBPROCESSO       NUMBER(10),
  NR_SEQ_REQUISITO         NUMBER(10),
  IE_REQUISITO_CORE        VARCHAR2(1 BYTE),
  NR_SEQ_PRS               NUMBER(10),
  NR_SEQ_SUPERIOR          NUMBER(10),
  IE_TYPE_REQUIREMENT      VARCHAR2(2 BYTE),
  NR_SEQ_REQ_PRS           NUMBER(10),
  NR_SEQ_GERENCIA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LATREQU_GERWHEB_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_LAREPRS_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_PRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_LASUBPR_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_SUBPROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_LATAMGAP_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_GAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATREQU_LATAMGAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LATREQU_LATMODU_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATREQU_LATMODU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LATREQU_MANORSE_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_PESFISI_FK_I ON TASY.LATAM_REQUISITO
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_PESFISI_FK2_I ON TASY.LATAM_REQUISITO
(CD_CONSULTOR_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_PESFISI_FK3_I ON TASY.LATAM_REQUISITO
(CD_RESPONSAVEL_BA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LATREQU_PK ON TASY.LATAM_REQUISITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_REGCR_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_CUSTOMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_REGPR_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_REQ_PRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LATREQU_TASYREQ_FK_I ON TASY.LATAM_REQUISITO
(NR_SEQ_REQUISITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.latam_requisito_before
BEFORE INSERT OR UPDATE ON TASY.LATAM_REQUISITO FOR EACH ROW
DECLARE

qt_reg_w		number(10);

BEGIN
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;

	if (nvl(:new.ie_sob_medida,'N') = 'N') then
		begin
			:new.qt_tempo_horas :=  nvl(calcular_esforco_requisito(:new.nr_sequencia,
									:new.nr_seq_gap,
									:new.nr_seq_prs,
									:new.ie_esforco_desenv,
									:new.ie_complexidade,
									:new.pr_confianca),0);
		end;
	end if;

	if (:new.ie_necessita_tecnologia = 'S') then
		begin
			:new.qt_horas_tec := nvl(calcular_esforco_area(:new.nr_sequencia,
								:new.nr_seq_gap,
								:new.nr_seq_prs,
								'TE',
								:new.ie_necessita_design,
								:new.ie_necessita_tecnologia,
								:new.qt_tempo_horas),0);
		end;
	end if;

	if (:new.ie_necessita_design = 'S') then
		begin
			:new.qt_horas_design := nvl(calcular_esforco_area(:new.nr_sequencia,
									:new.nr_seq_gap,
									:new.nr_seq_prs,
									'DE',
									:new.ie_necessita_design,
									:new.ie_necessita_tecnologia,
									:new.qt_tempo_horas),0);
		end;
	end if;

	<<Final>>
	qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.latam_requisito_after
after INSERT OR UPDATE ON TASY.LATAM_REQUISITO FOR EACH ROW
DECLARE

ie_tipo_gap_w			varchar2(10);
qt_funcao_w			number(10);

cd_funcao_w			funcao.cd_funcao%type;
nr_seq_reg_prs_w		reg_product_requirement.nr_sequencia%type;
nr_seq_old_reg_prs_w		reg_product_requirement.nr_sequencia%type;
nr_customer_requirement_w	reg_product_requirement.nr_customer_requirement%type;
nr_seq_latam_crs_w		latam_requisito_crs.nr_sequencia%type;

Cursor C01 is
	select	a.cd_funcao
	from	reg_funcao_pr	a,
		reg_product_requirement b
	where	a.nr_seq_product_req = b.nr_sequencia
	and	b.nr_sequencia = nr_seq_reg_prs_w;

BEGIN

begin
select	a.ie_tipo_gap,
	e.nr_seq_prs
into	ie_tipo_gap_w,
	nr_seq_reg_prs_w
from	latam_gap a,
	latam_modulo b,
	latam_subprocesso c,
	latam_requisito_crs d,
	latam_requisito_prs e
where	a.nr_sequencia	= b.nr_seq_gap
and	b.nr_sequencia	= c.nr_seq_modulo
and	c.nr_sequencia	= d.nr_seq_latam_subprocesso
and	d.nr_sequencia	= e.nr_seq_requisito_crs
and	e.nr_sequencia	= :new.nr_seq_prs;
exception
when others then
	begin
	ie_tipo_gap_w		:= 'X';
	nr_seq_reg_prs_w	:= null;
	end;
end;

if	(:new.nr_seq_prs is not null) and
	(nvl(:old.nr_seq_prs,0) <> nvl(:new.nr_seq_prs,0)) then

	select	max(b.nr_seq_crs)
	into	nr_customer_requirement_w
	from	latam_requisito_prs a,
		latam_requisito_crs b
	where	a.nr_seq_requisito_crs = b.nr_sequencia
	and	a.nr_sequencia = :new.nr_seq_prs;

	select	latam_requisito_crs_seq.nextval
	into	nr_seq_latam_crs_w
	from	dual;

	insert into latam_requisito_crs (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_requisito,
					nr_seq_crs)
	values			(	nr_seq_latam_crs_w,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_sequencia,
					nr_customer_requirement_w);

	select	max(a.nr_seq_prs)
	into	nr_seq_reg_prs_w
	from	latam_requisito_prs a
	where	a.nr_sequencia = :new.nr_seq_prs;



	insert	into latam_requisito_prs (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_requisito_crs,
					nr_seq_prs)
	values			(	latam_requisito_prs_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					nr_seq_latam_crs_w,
					nr_seq_reg_prs_w);
end if;

if	(:new.nr_seq_req_prs is not null) and
	(nvl(:old.nr_seq_req_prs,0) <> nvl(:new.nr_seq_req_prs,0)) then

	delete	latam_requisito_crs
	where 	nr_seq_requisito = :new.nr_sequencia;

	select	max(nr_customer_requirement)
	into	nr_customer_requirement_w
	from	reg_product_requirement
	where	nr_sequencia = :new.nr_seq_req_prs;

	select	latam_requisito_crs_seq.nextval
	into	nr_seq_latam_crs_w
	from	dual;

	insert into latam_requisito_crs (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_requisito,
					nr_seq_crs)
	values			(	nr_seq_latam_crs_w,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_sequencia,
					nr_customer_requirement_w);

	insert	into latam_requisito_prs (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_requisito_crs,
					nr_seq_prs)
	values			(	latam_requisito_prs_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					nr_seq_latam_crs_w,
					:new.nr_seq_req_prs);
end if;

nr_seq_old_reg_prs_w	:= :old.nr_seq_req_prs;
nr_seq_reg_prs_w 	:= :new.nr_seq_req_prs;
if	((:new.nr_seq_req_prs is not null) and
	 (nvl(:old.nr_seq_req_prs,0) <> nvl(:new.nr_seq_req_prs,0))) or
	((:new.nr_seq_prs is not null) and
	 (nvl(:old.nr_seq_prs,0) <> nvl(:new.nr_seq_prs,0))) then

	if	(:new.nr_seq_prs is not null) then
		select	max(a.nr_seq_prs)
		into	nr_seq_reg_prs_w
		from	latam_requisito_prs a
		where	a.nr_sequencia = :new.nr_seq_prs;

		select	max(a.nr_seq_prs)
		into	nr_seq_old_reg_prs_w
		from	latam_requisito_prs a
		where	a.nr_sequencia = :old.nr_seq_prs;
	end if;

	delete	latam_requisito_funcao a
	where	a.nr_seq_requisito = :new.nr_sequencia
	and	exists (select	1
			from	reg_funcao_pr x
			where	x.cd_funcao = a.cd_funcao
			and	x.nr_seq_product_req = nr_seq_old_reg_prs_w);

	open C01;
	loop
	fetch C01 into
		cd_funcao_w;
	exit when C01%notfound;
	begin

	select	count(*)
	into	qt_funcao_w
	from	latam_requisito_funcao
	where	nr_seq_requisito = :new.nr_sequencia
	and	cd_funcao = cd_funcao_w;

	if		(qt_funcao_w	= 0) then
			insert into latam_requisito_funcao
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_requisito,
					cd_funcao)
			values(latam_requisito_funcao_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_sequencia,
					cd_funcao_w);
	end if;

	end;
	end loop;
	close C01;

end if;

end;
/


ALTER TABLE TASY.LATAM_REQUISITO ADD (
  CONSTRAINT LATREQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LATAM_REQUISITO ADD (
  CONSTRAINT LATREQU_LASUBPR_FK 
 FOREIGN KEY (NR_SEQ_SUBPROCESSO) 
 REFERENCES TASY.LATAM_SUBPROCESSO (NR_SEQUENCIA),
  CONSTRAINT LATREQU_TASYREQ_FK 
 FOREIGN KEY (NR_SEQ_REQUISITO) 
 REFERENCES TASY.TASY_REQUISITO (NR_SEQUENCIA),
  CONSTRAINT LATREQU_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA),
  CONSTRAINT LATREQU_REGPR_FK 
 FOREIGN KEY (NR_SEQ_REQ_PRS) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT LATREQU_LAREPRS_FK 
 FOREIGN KEY (NR_SEQ_PRS) 
 REFERENCES TASY.LATAM_REQUISITO_PRS (NR_SEQUENCIA),
  CONSTRAINT LATREQU_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LATREQU_PESFISI_FK2 
 FOREIGN KEY (CD_CONSULTOR_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LATREQU_PESFISI_FK3 
 FOREIGN KEY (CD_RESPONSAVEL_BA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LATREQU_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT LATREQU_REGCR_FK 
 FOREIGN KEY (NR_SEQ_CUSTOMER) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT LATREQU_LATAMGAP_FK 
 FOREIGN KEY (NR_SEQ_GAP) 
 REFERENCES TASY.LATAM_GAP (NR_SEQUENCIA),
  CONSTRAINT LATREQU_LATMODU_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.LATAM_MODULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LATAM_REQUISITO TO NIVEL_1;

