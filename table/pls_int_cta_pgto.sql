ALTER TABLE TASY.PLS_INT_CTA_PGTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_INT_CTA_PGTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_INT_CTA_PGTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_INT_CTA           NUMBER(10)           NOT NULL,
  NR_SEQ_INT_CTA_PROC      NUMBER(10),
  NR_SEQ_INT_CTA_MAT       NUMBER(10),
  NR_SEQ_INT_CTA_PART      NUMBER(10),
  NR_SEQ_LOTE              NUMBER(10),
  CD_PRESTADOR_PGTO        VARCHAR2(20 BYTE),
  NM_PRESTADOR_PGTO        VARCHAR2(255 BYTE),
  VL_PAGAMENTO             NUMBER(15,2),
  CD_SERVICO               VARCHAR2(20 BYTE),
  IE_TIPO_REGISTRO         VARCHAR2(5 BYTE),
  CD_CHAVE                 VARCHAR2(20 BYTE),
  CD_CHAVE_SUP             VARCHAR2(20 BYTE),
  NR_SEQ_PREST_INTER_PGTO  NUMBER(10),
  IE_TIPO_RELACAO          VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO     NUMBER(10),
  IE_TIPO_TRIBUTACAO       VARCHAR2(15 BYTE),
  NR_CPF_PREST_PAGTO       VARCHAR2(11 BYTE),
  CD_CGC_PREST_PAGTO       VARCHAR2(14 BYTE),
  NR_SEQ_PRESTADOR_PGTO    NUMBER(10),
  IE_TIPO_SERVICO          VARCHAR2(5 BYTE),
  NR_SEQ_CONTA             NUMBER(10),
  NR_SEQ_CONTA_RESUMO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSICPG_PK ON TASY.PLS_INT_CTA_PGTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSCGLT_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSCLPR_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSCOME_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSICPT_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_INT_CTA_PART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSINCM_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_INT_CTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSINCP_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_INT_CTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSICPG_PLSINTC_FK_I ON TASY.PLS_INT_CTA_PGTO
(NR_SEQ_INT_CTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_INT_CTA_PGTO_tp  after update ON TASY.PLS_INT_CTA_PGTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_RELACAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_INT_CTA_PGTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_INT_CTA_PGTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TRIBUTACAO,1,4000),substr(:new.IE_TIPO_TRIBUTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TRIBUTACAO',ie_log_w,ds_w,'PLS_INT_CTA_PGTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_INT_CTA_PGTO ADD (
  CONSTRAINT PLSICPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_INT_CTA_PGTO ADD (
  CONSTRAINT PLSICPG_PLSICPT_FK 
 FOREIGN KEY (NR_SEQ_INT_CTA_PART) 
 REFERENCES TASY.PLS_INT_CTA_PART (NR_SEQUENCIA),
  CONSTRAINT PLSICPG_PLSINCM_FK 
 FOREIGN KEY (NR_SEQ_INT_CTA_MAT) 
 REFERENCES TASY.PLS_INT_CTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSICPG_PLSINCP_FK 
 FOREIGN KEY (NR_SEQ_INT_CTA_PROC) 
 REFERENCES TASY.PLS_INT_CTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSICPG_PLSINTC_FK 
 FOREIGN KEY (NR_SEQ_INT_CTA) 
 REFERENCES TASY.PLS_INT_CTA (NR_SEQUENCIA),
  CONSTRAINT PLSICPG_PLSCGLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_CG_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSICPG_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_INT_CTA_PGTO TO NIVEL_1;

