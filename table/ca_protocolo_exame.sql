ALTER TABLE TASY.CA_PROTOCOLO_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CA_PROTOCOLO_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.CA_PROTOCOLO_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAPROEX_CAEXAME_FK_I ON TASY.CA_PROTOCOLO_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAPROEX_CAEXAME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CAPROEX_CAPROTO_FK_I ON TASY.CA_PROTOCOLO_EXAME
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAPROEX_CAPROTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CAPROEX_PK ON TASY.CA_PROTOCOLO_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAPROEX_PK
  MONITORING USAGE;


ALTER TABLE TASY.CA_PROTOCOLO_EXAME ADD (
  CONSTRAINT CAPROEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CA_PROTOCOLO_EXAME ADD (
  CONSTRAINT CAPROEX_CAEXAME_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.CA_EXAME (NR_SEQUENCIA),
  CONSTRAINT CAPROEX_CAPROTO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.CA_PROTOCOLO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CA_PROTOCOLO_EXAME TO NIVEL_1;

