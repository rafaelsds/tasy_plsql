ALTER TABLE TASY.DIOPS_CONTRAP_EV_CORRESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_CONTRAP_EV_CORRESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_CONTRAP_EV_CORRESP
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_TIPO_MOVIMENTO           NUMBER(10),
  NR_SEQ_PERIODO              NUMBER(10),
  VL_PRECO_PRE_CORRESP_PRE    NUMBER(15,2),
  VL_PRECO_PRE_CORRESP_POS    NUMBER(15,2),
  VL_PRECO_POS_CORRESP_PRE    NUMBER(15,2),
  VL_PRECO_POS_CORRESP_POS    NUMBER(15,2),
  VL_PRECO_PRE_CART_PROPR     NUMBER(15,2),
  IE_TIPO_EVENTO              VARCHAR2(15 BYTE),
  VL_PRECO_PRE_CORRESP_ASSUM  NUMBER(15,2),
  VL_PRECO_POS_CART_PROPR     NUMBER(15,2),
  VL_PRECO_POS_CORRESP_ASSUM  NUMBER(15,2),
  CD_TIPO_MOVIMENTO_SUP       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIOPSCEC_DIOPERI_FK_I ON TASY.DIOPS_CONTRAP_EV_CORRESP
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIOPSCEC_ESTABEL_FK_I ON TASY.DIOPS_CONTRAP_EV_CORRESP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIOPSCEC_PK ON TASY.DIOPS_CONTRAP_EV_CORRESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.diops_ev_corresp_2018_atual
before insert or update ON TASY.DIOPS_CONTRAP_EV_CORRESP for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Manter as informacoes do quadro Eventos em corresponsabilidade 2018 atualizados na tabela que e utilizada para a geracao do XML
-------------------------------------------------------------------------------------------------------------------

ie_tipo_cobertura: 	1 = Pre
			2 = Pos

ie_tipo_origem:		1 = CCPRE (Corresp Cedida preco pre)
			2 = CCPOS (Corresp Cedida preco pos)
			3 = CRPRO (Carteira propria)
			4 = CRASS (Corresponsabilidade assumida)

ie_tipo_movimentacao:	0 = Contraprestacao cedida 			Equivalente no dominio: 7
			1 = (-) Recuperacao por coparticipacao		Equivalente no dominio: 8 (C) ou 14 (E)
			2 = (-) Glosas					Equivalente no dominio: 9 (C) ou 13 (E)
			3 = Despesa com eventos/sinistros		Equivalente no dominio: 12
			4 = Despesas com eventos/sinistros - Judicial	Equivalente no dominio: 15
			5 = (-) Outras recuperacoes			Equivalente no dominio: 16

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

ie_tipo_movimentacao_w 		diops_contr_ev_corresp_en.ie_tipo_movimentacao%type;
ds_plano_w			diops_contr_ev_corresp_en.ds_plano%type;

begin

select	decode(:new.cd_tipo_movimento, 		7, 	'0',
						8, 	'1',
						14, 	'1',
						9, 	'2',
						13, 	'2',
						12, 	'3',
						15, 	'4',
						16, 	'5'),
	decode(:new.cd_tipo_movimento_sup,	1, 	'IFAL',
						2, 	'IFPL',
						3, 	'PLAL',
						4, 	'PLAP',
						5, 	'PCEA',
						6, 	'PCEL')
into	ie_tipo_movimentacao_w,
	ds_plano_w
from 	dual;

if 	(:new.ie_tipo_evento = 'C') then

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pre_corresp_pre
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '1'
	and	ie_tipo_origem		= '1'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pre_corresp_pos
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '1'
	and	ie_tipo_origem		= '2'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pos_corresp_pre
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '2'
	and	ie_tipo_origem		= '1'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pos_corresp_pos
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '2'
	and	ie_tipo_origem		= '2'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;


elsif	(:new.ie_tipo_evento = 'E') then
	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pre_cart_propr
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '1'
	and	ie_tipo_origem		= '3'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pre_corresp_assum
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '1'
	and	ie_tipo_origem		= '4'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pos_cart_propr
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '2'
	and	ie_tipo_origem		= '3'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;

	update	diops_contr_ev_corresp_en
	set	vl_movimento 		= :new.vl_preco_pos_corresp_assum
	where	ds_plano 		= ds_plano_w
	and	ie_tipo_cobertura	= '2'
	and	ie_tipo_origem		= '4'
	and	ie_tipo_movimentacao 	= ie_tipo_movimentacao_w
	and	cd_estabelecimento	= :new.cd_estabelecimento
	and	nr_seq_periodo		= :new.nr_seq_periodo;
end if;


if	(:new.cd_tipo_movimento in (9, 11)) then
	:new.vl_preco_pre_corresp_pre := null;
	:new.vl_preco_pos_corresp_pre := null;
end if;

end;
/


ALTER TABLE TASY.DIOPS_CONTRAP_EV_CORRESP ADD (
  CONSTRAINT DIOPSCEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DIOPS_CONTRAP_EV_CORRESP ADD (
  CONSTRAINT DIOPSCEC_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA),
  CONSTRAINT DIOPSCEC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.DIOPS_CONTRAP_EV_CORRESP TO NIVEL_1;

