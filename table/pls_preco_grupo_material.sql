ALTER TABLE TASY.PLS_PRECO_GRUPO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRECO_GRUPO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRECO_GRUPO_MATERIAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_GRUPO               VARCHAR2(255 BYTE)     NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_GESTAO_CAD_MAT      VARCHAR2(1 BYTE)       NOT NULL,
  IE_CAD_PRESTADOR       VARCHAR2(1 BYTE),
  IE_COBERTURA_CONTRATO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRGM_ESTABEL_FK_I ON TASY.PLS_PRECO_GRUPO_MATERIAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRGM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPRGM_PK ON TASY.PLS_PRECO_GRUPO_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_PRECO_GRUPO_MATERIAL_tp  after update ON TASY.PLS_PRECO_GRUPO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_COBERTURA_CONTRATO,1,4000),substr(:new.IE_COBERTURA_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBERTURA_CONTRATO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAD_PRESTADOR,1,4000),substr(:new.IE_CAD_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAD_PRESTADOR',ie_log_w,ds_w,'PLS_PRECO_GRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_GRUPO,1,4000),substr(:new.DS_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRUPO',ie_log_w,ds_w,'PLS_PRECO_GRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GESTAO_CAD_MAT,1,4000),substr(:new.IE_GESTAO_CAD_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_GESTAO_CAD_MAT',ie_log_w,ds_w,'PLS_PRECO_GRUPO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PRECO_GRUPO_MATERIAL ADD (
  CONSTRAINT PLSPRGM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRECO_GRUPO_MATERIAL ADD (
  CONSTRAINT PLSPRGM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_PRECO_GRUPO_MATERIAL TO NIVEL_1;

