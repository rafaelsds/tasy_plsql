ALTER TABLE TASY.AGENDA_MONTA_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_MONTA_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_MONTA_EQUIPAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AGENDA         NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_CLASSIF_EQUIP  NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_APROVACAO          DATE,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGMOEQU_AGEPACI_FK_I ON TASY.AGENDA_MONTA_EQUIPAMENTO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOEQU_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGMOEQU_CLEQUIP_FK_I ON TASY.AGENDA_MONTA_EQUIPAMENTO
(NR_SEQ_CLASSIF_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOEQU_CLEQUIP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGMOEQU_PK ON TASY.AGENDA_MONTA_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOEQU_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_monta_equipamento_atual
before insert or update ON TASY.AGENDA_MONTA_EQUIPAMENTO for each row
declare
ds_alteracao_w		varchar2(4000);
ds_equipamento_w	varchar2(255);

begin

if	(updating) then
	if	(nvl(:old.nr_seq_classif_equip,'-1') <> nvl(:new.nr_seq_classif_equip,'-1')) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791491,
							'DS_CLASSIF_OLD='||obter_desc_classif_equip(:old.nr_seq_classif_equip) ||
							';DS_CLASSIF_NEW='||obter_desc_classif_equip(:new.nr_seq_classif_equip)),1,4000);
	end if;
	if	(nvl(:old.ds_observacao,'X') <> nvl(:new.ds_observacao,'X')) then
		ds_alteracao_w	:=	substr(ds_alteracao_w || CHR(10) || CHR(13) ||
					wheb_mensagem_pck.get_texto(791471,
							'DS_OBSERVACAO_OLD='||nvl(:old.ds_observacao, wheb_mensagem_pck.get_texto(791500)) ||
							';DS_OBSERVACAO_NEW='||nvl(:new.ds_observacao, wheb_mensagem_pck.get_texto(791500))),1,4000);

	end if;
	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'AE',ds_alteracao_w,:new.nm_usuario);
	end if;
else
	ds_equipamento_w := substr(obter_desc_classif_equip(:new.nr_seq_classif_equip),1,255);
	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791517,
							'DS_EQUIP='||ds_equipamento_w),1,4000);

	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'IE',ds_alteracao_w,:new.nm_usuario);
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_monta_equip_delete
before delete ON TASY.AGENDA_MONTA_EQUIPAMENTO for each row
declare
ds_alteracao_w		varchar2(4000);
begin

if	(:old.nr_seq_classif_equip is not null) then
	ds_alteracao_w	:=	substr(obter_desc_expressao(500805)||' '||obter_desc_classif_equip(:old.nr_seq_classif_equip),1,4000);
end if;

if	(ds_alteracao_w is not null) then
	gravar_historico_montagem(:old.nr_seq_agenda,'EE',ds_alteracao_w,:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_MONTA_EQUIPAMENTO_tp  after update ON TASY.AGENDA_MONTA_EQUIPAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_EQUIP,1,4000),substr(:new.NR_SEQ_CLASSIF_EQUIP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_EQUIP',ie_log_w,ds_w,'AGENDA_MONTA_EQUIPAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AGENDA_MONTA_EQUIPAMENTO ADD (
  CONSTRAINT AGMOEQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_MONTA_EQUIPAMENTO ADD (
  CONSTRAINT AGMOEQU_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGMOEQU_CLEQUIP_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_EQUIP) 
 REFERENCES TASY.CLASSIF_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_MONTA_EQUIPAMENTO TO NIVEL_1;

