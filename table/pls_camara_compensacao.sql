ALTER TABLE TASY.PLS_CAMARA_COMPENSACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CAMARA_COMPENSACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CAMARA_COMPENSACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_CAMARA                VARCHAR2(20 BYTE)    NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_CAMARA                VARCHAR2(255 BYTE)   NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  IE_TIPO_CAMARA           VARCHAR2(3 BYTE)     NOT NULL,
  NR_SEQ_COOP_RESP         NUMBER(10)           NOT NULL,
  TX_ADMINISTRATIVA        NUMBER(7,4),
  VL_DESC_BENEFIC_CUSTO    NUMBER(15,2),
  IE_INCIDENCIA_TAXA       VARCHAR2(1 BYTE),
  IE_CONSIDERA_TX_BAIXA    VARCHAR2(5 BYTE),
  IE_DATA_TIT_PAG_A560     VARCHAR2(5 BYTE),
  IE_TIPO_BASE_TAXA        VARCHAR2(5 BYTE),
  IE_DATA_BAIXA_REG_CAIXA  VARCHAR2(5 BYTE),
  IE_GERAR_DATA_PAG_A510   VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCACO_PK ON TASY.PLS_CAMARA_COMPENSACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCACO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCACO_PLSCONG_FK_I ON TASY.PLS_CAMARA_COMPENSACAO
(NR_SEQ_COOP_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCACO_PLSCONG_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CAMARA_COMPENSACAO ADD (
  CONSTRAINT PLSCACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CAMARA_COMPENSACAO ADD (
  CONSTRAINT PLSCACO_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_COOP_RESP) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CAMARA_COMPENSACAO TO NIVEL_1;

