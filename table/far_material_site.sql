ALTER TABLE TASY.FAR_MATERIAL_SITE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FAR_MATERIAL_SITE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FAR_MATERIAL_SITE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM         NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESTRUTURA     NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  VL_PRECO             NUMBER(17,4)             NOT NULL,
  DS_DESCRICAO         VARCHAR2(255 BYTE)       NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  DS_LOCAL_IMAGEM      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FARMASI_FARESMS_FK_I ON TASY.FAR_MATERIAL_SITE
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FARMASI_FARESMS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FARMASI_MATERIA_FK_I ON TASY.FAR_MATERIAL_SITE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FARMASI_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FARMASI_PK ON TASY.FAR_MATERIAL_SITE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FARMASI_PK
  MONITORING USAGE;


ALTER TABLE TASY.FAR_MATERIAL_SITE ADD (
  CONSTRAINT FARMASI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FAR_MATERIAL_SITE ADD (
  CONSTRAINT FARMASI_FARESMS_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.FAR_ESTRUT_MAT_SITE (NR_SEQUENCIA),
  CONSTRAINT FARMASI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.FAR_MATERIAL_SITE TO NIVEL_1;

