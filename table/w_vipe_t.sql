DROP TABLE TASY.W_VIPE_T CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_VIPE_T
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  HORA1                  VARCHAR2(100 BYTE),
  HORA2                  VARCHAR2(100 BYTE),
  HORA3                  VARCHAR2(100 BYTE),
  HORA4                  VARCHAR2(100 BYTE),
  HORA5                  VARCHAR2(100 BYTE),
  HORA6                  VARCHAR2(100 BYTE),
  HORA7                  VARCHAR2(100 BYTE),
  HORA8                  VARCHAR2(100 BYTE),
  HORA9                  VARCHAR2(100 BYTE),
  HORA10                 VARCHAR2(100 BYTE),
  HORA11                 VARCHAR2(100 BYTE),
  HORA12                 VARCHAR2(100 BYTE),
  HORA13                 VARCHAR2(100 BYTE),
  HORA14                 VARCHAR2(100 BYTE),
  HORA15                 VARCHAR2(100 BYTE),
  HORA16                 VARCHAR2(100 BYTE),
  HORA17                 VARCHAR2(100 BYTE),
  HORA18                 VARCHAR2(100 BYTE),
  HORA19                 VARCHAR2(100 BYTE),
  HORA20                 VARCHAR2(100 BYTE),
  HORA21                 VARCHAR2(100 BYTE),
  HORA22                 VARCHAR2(100 BYTE),
  HORA23                 VARCHAR2(100 BYTE),
  HORA24                 VARCHAR2(100 BYTE),
  HORA25                 VARCHAR2(100 BYTE),
  HORA26                 VARCHAR2(100 BYTE),
  HORA27                 VARCHAR2(100 BYTE),
  HORA28                 VARCHAR2(100 BYTE),
  HORA29                 VARCHAR2(100 BYTE),
  HORA30                 VARCHAR2(100 BYTE),
  HORA31                 VARCHAR2(100 BYTE),
  HORA32                 VARCHAR2(100 BYTE),
  HORA33                 VARCHAR2(100 BYTE),
  HORA34                 VARCHAR2(100 BYTE),
  HORA35                 VARCHAR2(100 BYTE),
  HORA36                 VARCHAR2(100 BYTE),
  HORA37                 VARCHAR2(100 BYTE),
  HORA38                 VARCHAR2(100 BYTE),
  HORA39                 VARCHAR2(100 BYTE),
  HORA40                 VARCHAR2(100 BYTE),
  CD_ITEM                VARCHAR2(255 BYTE),
  DS_ITEM                VARCHAR2(240 BYTE),
  IE_TIPO_ITEM           VARCHAR2(3 BYTE),
  IE_CLASSIF_ADEP        VARCHAR2(15 BYTE),
  NR_PRESCRICOES         VARCHAR2(255 BYTE),
  NR_PRESCRICAO          NUMBER(14),
  NR_SEQ_ITEM            NUMBER(10),
  NR_SEQ_PROC_INTERNO    NUMBER(15,4),
  DS_PRESCRICAO          VARCHAR2(240 BYTE),
  CD_INTERVALO           VARCHAR2(7 BYTE),
  QT_ITEM                NUMBER(15,4),
  IE_ACM_SN              VARCHAR2(1 BYTE),
  IE_MEDIC_CONTROLADO    VARCHAR2(2 BYTE),
  IE_DIFERENCIADO        VARCHAR2(1 BYTE),
  DS_DILUICAO            VARCHAR2(2000 BYTE),
  NR_DIA_UTIL            NUMBER(10),
  NR_AGRUPAMENTO         NUMBER(7,1),
  IE_SUSPENSO            VARCHAR2(1 BYTE),
  IE_STATUS_ITEM         VARCHAR2(15 BYTE),
  IE_PENDENTE_LIBERACAO  VARCHAR2(2 BYTE),
  DT_PREV_TERM           DATE,
  NR_SEQ_LAB             VARCHAR2(20 BYTE),
  IE_PRESCRICAO_ALTA     VARCHAR2(1 BYTE),
  NR_SEQ_PROT_GLIC       NUMBER(10),
  CD_UNID_MED_QTDE       VARCHAR2(30 BYTE),
  IE_VIA_APLICACAO       VARCHAR2(5 BYTE),
  DS_INTERV_PRESCR       VARCHAR2(240 BYTE),
  NR_SEQ_APRES_GRUPO     NUMBER(10),
  NR_SEQ_APRES_INF       NUMBER(10),
  QT_TOTAL_DIAS_LIB      NUMBER(5),
  IE_COMPOSTO            VARCHAR2(1 BYTE),
  DS_DOSE_DIFERENCIADA   VARCHAR2(50 BYTE),
  DS_RECOMENDACAO        VARCHAR2(2000 BYTE),
  IE_COPIAR              VARCHAR2(1 BYTE),
  IE_PINTAR              VARCHAR2(1 BYTE),
  IE_POSSUI_HORARIOS     VARCHAR2(1 BYTE),
  IE_OBSERVACAO          VARCHAR2(1 BYTE),
  IE_CIENCIA             VARCHAR2(1 BYTE),
  IE_HEMODIALISE         VARCHAR2(3 BYTE),
  IE_ADMINISTRAR         VARCHAR2(1 BYTE),
  IE_FORMA               VARCHAR2(1 BYTE),
  IE_PERMITE_ACAO        VARCHAR2(1 BYTE),
  IE_MODIFICADA          VARCHAR2(1 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_VIPE_T TO NIVEL_1;

