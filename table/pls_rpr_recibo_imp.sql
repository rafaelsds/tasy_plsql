ALTER TABLE TASY.PLS_RPR_RECIBO_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_RPR_RECIBO_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_RPR_RECIBO_IMP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_RPR_RESPOSTA      NUMBER(10)           NOT NULL,
  NR_REGISTRO_ANS          VARCHAR2(6 BYTE),
  NR_GUIA_REC_GLOSA_PREST  VARCHAR2(20 BYTE),
  NM_OPERADORA             VARCHAR2(70 BYTE),
  NR_GUIA_REC_GLOSA_OPER   VARCHAR2(20 BYTE),
  NR_LOTE                  VARCHAR2(12 BYTE),
  NR_PROTOCOLO             VARCHAR2(12 BYTE),
  IE_OBJETO_RECURSO        VARCHAR2(5 BYTE),
  DT_RECURSO               DATE,
  VL_TOTAL_RECURSADO       NUMBER(15,2),
  VL_TOTAL_ACATADO         NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPRB_PK ON TASY.PLS_RPR_RECIBO_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPRB_PLSRPRR_FK_I ON TASY.PLS_RPR_RECIBO_IMP
(NR_SEQ_RPR_RESPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_RPR_RECIBO_IMP ADD (
  CONSTRAINT PLSRPRB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_RPR_RECIBO_IMP ADD (
  CONSTRAINT PLSRPRB_PLSRPRR_FK 
 FOREIGN KEY (NR_SEQ_RPR_RESPOSTA) 
 REFERENCES TASY.PLS_RPR_RESPOSTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_RPR_RECIBO_IMP TO NIVEL_1;

