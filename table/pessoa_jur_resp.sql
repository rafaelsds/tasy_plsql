ALTER TABLE TASY.PESSOA_JUR_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JUR_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JUR_RESP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_CGC                   VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  CD_CARGO                 NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_RESP_CADASTRO         VARCHAR2(1 BYTE)     NOT NULL,
  IE_REPRESENTANTE_LEGAL   VARCHAR2(1 BYTE),
  IE_AVALIACAO_FORNECEDOR  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEJURES_CARGO_FK_I ON TASY.PESSOA_JUR_RESP
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEJURES_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEJURES_PESFISI_FK_I ON TASY.PESSOA_JUR_RESP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEJURES_PESJURI_FK_I ON TASY.PESSOA_JUR_RESP
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEJURES_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEJURES_PK ON TASY.PESSOA_JUR_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PESSOA_JUR_RESP_tp  after update ON TASY.PESSOA_JUR_RESP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PESSOA_JUR_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESP_CADASTRO,1,4000),substr(:new.IE_RESP_CADASTRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESP_CADASTRO',ie_log_w,ds_w,'PESSOA_JUR_RESP',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'PESSOA_JUR_RESP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_jur_resp_delete
after delete ON TASY.PESSOA_JUR_RESP for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

select  obter_tipo_pessoa_juridica(:old.cd_cgc)
into	cd_tipo_pessoa_w
from    dual;

reg_integracao_p.ie_operacao		:=	'A';
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :old.cd_cgc,:old.nm_usuario, reg_integracao_p);

end;
/


ALTER TABLE TASY.PESSOA_JUR_RESP ADD (
  CONSTRAINT PEJURES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JUR_RESP ADD (
  CONSTRAINT PEJURES_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEJURES_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PEJURES_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO));

GRANT SELECT ON TASY.PESSOA_JUR_RESP TO NIVEL_1;

