ALTER TABLE TASY.INTPD_EVENTO_SUPERIOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_EVENTO_SUPERIOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_EVENTO_SUPERIOR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO          NUMBER(10)             NOT NULL,
  NR_SEQ_EVENTO_SUP      NUMBER(10)             NOT NULL,
  NR_ORDENACAO           NUMBER(5),
  NR_SEQ_EVENTO_SISTEMA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTEVSU_INTPDESI_FK_I ON TASY.INTPD_EVENTO_SUPERIOR
(NR_SEQ_EVENTO_SISTEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTEVSU_INTPDEVE_FK_I ON TASY.INTPD_EVENTO_SUPERIOR
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INTEVSU_INTPDEVE_FK2_I ON TASY.INTPD_EVENTO_SUPERIOR
(NR_SEQ_EVENTO_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTEVSU_PK ON TASY.INTPD_EVENTO_SUPERIOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTPD_EVENTO_SUPERIOR ADD (
  CONSTRAINT INTEVSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.INTPD_EVENTO_SUPERIOR ADD (
  CONSTRAINT INTEVSU_INTPDEVE_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.INTPD_EVENTOS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INTEVSU_INTPDEVE_FK2 
 FOREIGN KEY (NR_SEQ_EVENTO_SUP) 
 REFERENCES TASY.INTPD_EVENTOS (NR_SEQUENCIA),
  CONSTRAINT INTEVSU_INTPDESI_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_SISTEMA) 
 REFERENCES TASY.INTPD_EVENTOS_SISTEMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTPD_EVENTO_SUPERIOR TO NIVEL_1;

