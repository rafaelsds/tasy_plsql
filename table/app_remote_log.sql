ALTER TABLE TASY.APP_REMOTE_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APP_REMOTE_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.APP_REMOTE_LOG
(
  NR_SEQUENCE      NUMBER(10)                   NOT NULL,
  CGC              VARCHAR2(14 BYTE),
  DB_JDBC_URL      VARCHAR2(500 BYTE),
  EXECUTE_ORDER    NUMBER(3),
  TIME             TIMESTAMP(6),
  EXECUTE_KEY      VARCHAR2(40 BYTE),
  LOG_TYPE         VARCHAR2(200 BYTE),
  MESSAGE          CLOB,
  CURRENT_VERSION  VARCHAR2(50 BYTE),
  USER_NAME        VARCHAR2(50 BYTE),
  EC2_NAME         VARCHAR2(50 BYTE),
  HOST_NAME        VARCHAR2(50 BYTE),
  STAGE            VARCHAR2(100 BYTE),
  ERROR            NUMBER(1),
  NEXT_VERSION     VARCHAR2(50 BYTE),
  SCHEDULER        NUMBER(1),
  PACKAGE          NUMBER(10),
  SQL_COMMAND      CLOB,
  RELEASE          NUMBER(10),
  SEQ_SCRIPT       NUMBER(20)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (SQL_COMMAND) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (MESSAGE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.APPREMLOG_PK ON TASY.APP_REMOTE_LOG
(NR_SEQUENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APP_REMOTE_LOG_UK ON TASY.APP_REMOTE_LOG
(CGC, TIME, EXECUTE_KEY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.APP_REMOTE_LOG ADD (
  CONSTRAINT APPREMLOG_PK
 PRIMARY KEY
 (NR_SEQUENCE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT APP_REMOTE_LOG_UK
 UNIQUE (CGC, TIME, EXECUTE_KEY)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.APP_REMOTE_LOG TO NIVEL_1;

