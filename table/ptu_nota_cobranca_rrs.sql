ALTER TABLE TASY.PTU_NOTA_COBRANCA_RRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_NOTA_COBRANCA_RRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_NOTA_COBRANCA_RRS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FATURA        NUMBER(10)               NOT NULL,
  NR_LOTE              NUMBER(8),
  NR_NOTA              VARCHAR2(20 BYTE),
  NR_NOTA_NUMERICO     VARCHAR2(20 BYTE),
  CD_UNIMED            VARCHAR2(10 BYTE),
  ID_BENEF             VARCHAR2(13 BYTE),
  NM_BENEFICIARIO      VARCHAR2(25 BYTE),
  DT_NASC              DATE,
  ID_RN                VARCHAR2(1 BYTE),
  TP_SEXO              VARCHAR2(1 BYTE),
  DT_REEMBOLSO         DATE,
  TP_CARATER_ATEND     VARCHAR2(1 BYTE),
  TP_PESSOA            VARCHAR2(1 BYTE),
  NR_CNPJ_CPF          VARCHAR2(14 BYTE),
  NM_PRESTADOR         VARCHAR2(70 BYTE),
  ID_REEMBOLSO_SUS     VARCHAR2(5 BYTE),
  NR_SEQ_CONTA         NUMBER(10),
  ID_REEM_PAR_INT      VARCHAR2(5 BYTE),
  NR_SEQ_PREST_INTER   NUMBER(10),
  CD_CNES              VARCHAR2(7 BYTE),
  DS_STACK             VARCHAR2(2000 BYTE),
  NR_SEQ_CONTA_SUS     NUMBER(10),
  ID_REEMBOLSO         VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUCRRS_PK ON TASY.PTU_NOTA_COBRANCA_RRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUCRRS_PLSCOME_FK_I ON TASY.PTU_NOTA_COBRANCA_RRS
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUCRRS_PLSPRIN_FK_I ON TASY.PTU_NOTA_COBRANCA_RRS
(NR_SEQ_PREST_INTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUCRRS_PTUFATU_FK_I ON TASY.PTU_NOTA_COBRANCA_RRS
(NR_SEQ_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_nota_cobranca_rrs_atual
before insert or update ON TASY.PTU_NOTA_COBRANCA_RRS for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

begin

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
end if;

:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/


ALTER TABLE TASY.PTU_NOTA_COBRANCA_RRS ADD (
  CONSTRAINT PTUCRRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_NOTA_COBRANCA_RRS ADD (
  CONSTRAINT PTUCRRS_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PTUCRRS_PTUFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA) 
 REFERENCES TASY.PTU_FATURA (NR_SEQUENCIA),
  CONSTRAINT PTUCRRS_PLSPRIN_FK 
 FOREIGN KEY (NR_SEQ_PREST_INTER) 
 REFERENCES TASY.PLS_PRESTADOR_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_NOTA_COBRANCA_RRS TO NIVEL_1;

