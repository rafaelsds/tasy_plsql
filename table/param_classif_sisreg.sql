ALTER TABLE TASY.PARAM_CLASSIF_SISREG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAM_CLASSIF_SISREG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAM_CLASSIF_SISREG
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_CLASSIF_AGE_CON      VARCHAR2(5 BYTE),
  NR_SEQ_CLASSIF_AGE_EX   NUMBER(10),
  IE_TIPO_CLASSIF_SISREG  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PACLASIS_PK ON TASY.PARAM_CLASSIF_SISREG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PARAM_CLASSIF_SISREG_tp  after update ON TASY.PARAM_CLASSIF_SISREG FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_AGE_EX,1,4000),substr(:new.NR_SEQ_CLASSIF_AGE_EX,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_AGE_EX',ie_log_w,ds_w,'PARAM_CLASSIF_SISREG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_AGE_CON,1,4000),substr(:new.IE_CLASSIF_AGE_CON,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_AGE_CON',ie_log_w,ds_w,'PARAM_CLASSIF_SISREG',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PARAM_CLASSIF_SISREG ADD (
  CONSTRAINT PACLASIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PARAM_CLASSIF_SISREG TO NIVEL_1;

