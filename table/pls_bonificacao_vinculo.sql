ALTER TABLE TASY.PLS_BONIFICACAO_VINCULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_BONIFICACAO_VINCULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_BONIFICACAO_VINCULO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_BONIFICACAO         NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_PAGADOR             NUMBER(10),
  NR_SEQ_CONTRATO            NUMBER(10),
  NR_SEQ_SEGURADO            NUMBER(10),
  NR_SEQ_PROPOSTA            NUMBER(10),
  TX_BONIFICACAO             NUMBER(7,4),
  NR_SEQ_SCA                 NUMBER(10),
  VL_BONIFICACAO             NUMBER(15,2),
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  NR_SEQ_CLIENTE             NUMBER(10),
  NR_SEQ_PLANO               NUMBER(10),
  NR_SEQ_SEGURADO_PROP       NUMBER(10),
  NR_SEQ_SIMULACAO           NUMBER(10),
  IE_BASE_MAIOR_VALOR        VARCHAR2(1 BYTE),
  CD_SISTEMA_ANTERIOR        VARCHAR2(30 BYTE),
  NR_SEQ_SEGURADO_SIMUL      NUMBER(10),
  NR_SEQ_GRUPO_CONTRATO      NUMBER(10),
  NR_SEQ_INTERCAMBIO         NUMBER(10),
  IE_LANCAMENTO_BONIFICACAO  VARCHAR2(2 BYTE),
  NR_SEQ_SIMULACAO_PERFIL    NUMBER(10),
  NR_SEQ_PROPOSTA_ONLINE     NUMBER(10),
  IE_LANCAMENTO_AUTOMATICO   VARCHAR2(1 BYTE),
  NR_SEQ_SCA_SEGURO_OBITO    NUMBER(10),
  NR_SEQ_CONTRATO_GRUPO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSBOVI_PK ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLCONPAG_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSBONI_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_BONIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSCOCL_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBOVI_PLSCOCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBOVI_PLSCONT_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSCONT_FK2_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_CONTRATO_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSGRCO_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSIMIF_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SEGURADO_SIMUL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBOVI_PLSIMIF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBOVI_PLSINCA_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSPLAN_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSPLAN_FK2_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SCA_SEGURO_OBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSPRAD_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBOVI_PLSPRAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBOVI_PLSPROB_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SEGURADO_PROP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBOVI_PLSPROB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBOVI_PLSPRON_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_PROPOSTA_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSSCOA_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBOVI_PLSSCOA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBOVI_PLSSEGU_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBOVI_PLSSIPE_FK_I ON TASY.PLS_BONIFICACAO_VINCULO
(NR_SEQ_SIMULACAO_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_bonificacao_vinculo_delete
before delete ON TASY.PLS_BONIFICACAO_VINCULO for each row
declare

nr_sequencia_w		NUMBER(10);

begin

nr_sequencia_w := nr_sequencia_w;

/*aaschlote 27/02/2012 - Desncess�rio a cria��o de uma trigger, tratar tudo na procedure
delete
from	pls_proposta_resumo
where	nr_seq_bonificacao = :old.nr_sequencia
and	nr_seq_ordem	   = 4;
*/
end;
/


ALTER TABLE TASY.PLS_BONIFICACAO_VINCULO ADD (
  CONSTRAINT PLSBOVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_BONIFICACAO_VINCULO ADD (
  CONSTRAINT PLSBOVI_PLSSIPE_FK 
 FOREIGN KEY (NR_SEQ_SIMULACAO_PERFIL) 
 REFERENCES TASY.PLS_SIMULACAO_PERFIL (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSPRON_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_SCA_SEGURO_OBITO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSCONT_FK2 
 FOREIGN KEY (NR_SEQ_CONTRATO_GRUPO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSPROB_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO_PROP) 
 REFERENCES TASY.PLS_PROPOSTA_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSIMIF_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO_SIMUL) 
 REFERENCES TASY.PLS_SIMULPRECO_INDIVIDUAL (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSBONI_FK 
 FOREIGN KEY (NR_SEQ_BONIFICACAO) 
 REFERENCES TASY.PLS_BONIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSSCOA_FK 
 FOREIGN KEY (NR_SEQ_SCA) 
 REFERENCES TASY.PLS_SERVICO_COBERTURA_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSCOCL_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.PLS_COMERCIAL_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PLSBOVI_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_BONIFICACAO_VINCULO TO NIVEL_1;

