ALTER TABLE TASY.PLS_TRIBUTO_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TRIBUTO_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TRIBUTO_LOG
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_TRIBUTO                  NUMBER(3)         NOT NULL,
  DS_TRIBUTO                  VARCHAR2(40 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_TIPO_VALOR               NUMBER(5),
  IE_SOMA_DIMINUI             VARCHAR2(1 BYTE),
  IE_CORPO_ITEM               VARCHAR2(1 BYTE),
  IE_CENTRO_CUSTO             VARCHAR2(1 BYTE),
  IE_RATEIO                   VARCHAR2(1 BYTE),
  IE_CONTA_PAGAR              VARCHAR2(1 BYTE)  NOT NULL,
  IE_GERAR_TITULO_PAGAR       VARCHAR2(1 BYTE)  NOT NULL,
  IE_TIPO_TRIBUTO             VARCHAR2(15 BYTE) NOT NULL,
  IE_PF_PJ                    VARCHAR2(2 BYTE)  NOT NULL,
  IE_VENCIMENTO               VARCHAR2(1 BYTE)  NOT NULL,
  IE_VENC_NF                  VARCHAR2(1 BYTE)  NOT NULL,
  IE_GERAR_TRIB_NF            VARCHAR2(1 BYTE)  NOT NULL,
  IE_APURACAO_PISO            VARCHAR2(1 BYTE)  NOT NULL,
  NR_SEQ_TRANS_REC            NUMBER(10),
  NR_SEQ_TRANS_IMP            NUMBER(10),
  IE_REPASSE_TITULO           VARCHAR2(1 BYTE)  NOT NULL,
  IE_GRUPO_TRIBUTO            VARCHAR2(2 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_ATUALIZAR_VENC_TRIB      VARCHAR2(1 BYTE)  NOT NULL,
  IE_ATUALIZAR_TRIB_LIQ       VARCHAR2(1 BYTE)  NOT NULL,
  IE_BAIXA_TITULO             VARCHAR2(1 BYTE)  NOT NULL,
  IE_CNPJ                     VARCHAR2(15 BYTE) NOT NULL,
  IE_NF_TIT_REC               VARCHAR2(1 BYTE)  NOT NULL,
  NR_SEQ_TRANS_TIT_REC        NUMBER(10),
  IE_DT_CONTAB_TIT_TRIB       VARCHAR2(2 BYTE),
  IE_ACUMULAR_TRIB_LIQ        VARCHAR2(1 BYTE),
  IE_SOMENTE_NF               VARCHAR2(1 BYTE),
  IE_CCM                      VARCHAR2(1 BYTE),
  IE_SUPER_SIMPLES            VARCHAR2(1 BYTE),
  IE_SALDO_TIT_PAGAR          VARCHAR2(1 BYTE),
  IE_FORMA_ARRED_NF           VARCHAR2(15 BYTE) NOT NULL,
  CD_CONTA_FINANC             NUMBER(10),
  CD_ESTABELECIMENTO          NUMBER(4),
  IE_INCIDE_CONTA             VARCHAR2(1 BYTE),
  IE_RESTRINGE_ESTAB          VARCHAR2(1 BYTE),
  NR_TRIBUTACAO_MUNICIPIO     VARCHAR2(10 BYTE),
  IE_DESC_NF                  VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_PROV_TRIB_REC  NUMBER(10),
  IE_VENC_REPASSE             VARCHAR2(1 BYTE),
  NR_SEQ_TRANS_CONV_REC       NUMBER(10),
  IE_FORMA_ARRED_OPS          VARCHAR2(15 BYTE) NOT NULL,
  IE_REGRA_PF_SOBRE_CP        VARCHAR2(1 BYTE)  NOT NULL,
  IE_VALOR_BASE_TITULO_NF     VARCHAR2(1 BYTE),
  IE_GERAR_TRIB_NFSE          VARCHAR2(1 BYTE),
  IE_RESTRINGE_EMPRESA        VARCHAR2(1 BYTE)  NOT NULL,
  IE_CONSIDERA_VALOR_PF       VARCHAR2(1 BYTE),
  IE_GERA_NF_DEVOLUCAO        VARCHAR2(1 BYTE),
  IE_CONTAB_PROV_CP           VARCHAR2(1 BYTE),
  CD_RETENCAO                 VARCHAR2(10 BYTE),
  IE_RATEIO_CLASSIF           VARCHAR2(1 BYTE),
  IE_DATA_IRPF                VARCHAR2(15 BYTE),
  IE_ISENTO                   VARCHAR2(15 BYTE),
  IE_VENC_PLS_PAG_PROD        VARCHAR2(5 BYTE),
  NR_SEQ_LOTE_PGTO            NUMBER(10),
  NR_SEQ_PP_LOTE              NUMBER(10),
  NR_SEQ_TERMO_CRED_DEB       NUMBER(10),
  IE_DESC_DESP_CARTAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTRLG_CONFINA_FK_I ON TASY.PLS_TRIBUTO_LOG
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_ESTABEL_FK_I ON TASY.PLS_TRIBUTO_LOG
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_I1 ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_PP_LOTE, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSTRLG_PK ON TASY.PLS_TRIBUTO_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_PLSLOPA_FK_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_LOTE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_PPPLOTE_FK_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_PP_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TIPVALO_FK_I ON TASY.PLS_TRIBUTO_LOG
(CD_TIPO_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TRAFINA_FK_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_TRANS_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TRAFINA_FK2_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_TRANS_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TRAFINA_FK3_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_TRANS_TIT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TRAFINA_FK4_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_TRANS_PROV_TRIB_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRLG_TRAFINA_FK5_I ON TASY.PLS_TRIBUTO_LOG
(NR_SEQ_TRANS_CONV_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_TRIBUTO_LOG_tp  after update ON TASY.PLS_TRIBUTO_LOG FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_VALOR_BASE_TITULO_NF,1,500);gravar_log_alteracao(substr(:old.IE_VALOR_BASE_TITULO_NF,1,4000),substr(:new.IE_VALOR_BASE_TITULO_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_BASE_TITULO_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_TIT_REC,1,4000),substr(:new.IE_NF_TIT_REC,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_TIT_REC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_TIT_REC,1,4000),substr(:new.NR_SEQ_TRANS_TIT_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_TIT_REC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_REC,1,4000),substr(:new.NR_SEQ_TRANS_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_REC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_PROV_TRIB_REC,1,4000),substr(:new.NR_SEQ_TRANS_PROV_TRIB_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_PROV_TRIB_REC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_IMP,1,4000),substr(:new.NR_SEQ_TRANS_IMP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_IMP',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INCIDE_CONTA,1,4000),substr(:new.IE_INCIDE_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INCIDE_CONTA',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRUPO_TRIBUTO,1,4000),substr(:new.IE_GRUPO_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRUPO_TRIBUTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_TRIB_NF,1,4000),substr(:new.IE_GERAR_TRIB_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_TRIB_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_TITULO_PAGAR,1,4000),substr(:new.IE_GERAR_TITULO_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_TITULO_PAGAR',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_ARRED_OPS,1,4000),substr(:new.IE_FORMA_ARRED_OPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ARRED_OPS',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_ARRED_NF,1,4000),substr(:new.IE_FORMA_ARRED_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ARRED_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DT_CONTAB_TIT_TRIB,1,4000),substr(:new.IE_DT_CONTAB_TIT_TRIB,1,4000),:new.nm_usuario,nr_seq_w,'IE_DT_CONTAB_TIT_TRIB',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESC_NF,1,4000),substr(:new.IE_DESC_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESC_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CORPO_ITEM,1,4000),substr(:new.IE_CORPO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CORPO_ITEM',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_PAGAR,1,4000),substr(:new.IE_CONTA_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_PAGAR',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VENC_REPASSE,1,4000),substr(:new.IE_VENC_REPASSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_VENC_REPASSE',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VENC_NF,1,4000),substr(:new.IE_VENC_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_VENC_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CNPJ,1,4000),substr(:new.IE_CNPJ,1,4000),:new.nm_usuario,nr_seq_w,'IE_CNPJ',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_CUSTO,1,4000),substr(:new.IE_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_CUSTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CCM,1,4000),substr(:new.IE_CCM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CCM',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_TITULO,1,4000),substr(:new.IE_BAIXA_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_TITULO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_VENC_TRIB,1,4000),substr(:new.IE_ATUALIZAR_VENC_TRIB,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_VENC_TRIB',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR_TRIB_LIQ,1,4000),substr(:new.IE_ATUALIZAR_TRIB_LIQ,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR_TRIB_LIQ',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APURACAO_PISO,1,4000),substr(:new.IE_APURACAO_PISO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APURACAO_PISO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACUMULAR_TRIB_LIQ,1,4000),substr(:new.IE_ACUMULAR_TRIB_LIQ,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACUMULAR_TRIB_LIQ',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VENCIMENTO,1,4000),substr(:new.IE_VENCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VENCIMENTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TRIBUTO,1,4000),substr(:new.IE_TIPO_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TRIBUTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SUPER_SIMPLES,1,4000),substr(:new.IE_SUPER_SIMPLES,1,4000),:new.nm_usuario,nr_seq_w,'IE_SUPER_SIMPLES',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_NF,1,4000),substr(:new.IE_SOMENTE_NF,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_NF',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMA_DIMINUI,1,4000),substr(:new.IE_SOMA_DIMINUI,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMA_DIMINUI',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALDO_TIT_PAGAR,1,4000),substr(:new.IE_SALDO_TIT_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALDO_TIT_PAGAR',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRINGE_ESTAB,1,4000),substr(:new.IE_RESTRINGE_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRINGE_ESTAB',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REPASSE_TITULO,1,4000),substr(:new.IE_REPASSE_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REPASSE_TITULO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RATEIO,1,4000),substr(:new.IE_RATEIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RATEIO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TRIBUTO,1,4000),substr(:new.DS_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TRIBUTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_VALOR,1,4000),substr(:new.CD_TIPO_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_VALOR',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PF_PJ,1,4000),substr(:new.IE_PF_PJ,1,4000),:new.nm_usuario,nr_seq_w,'IE_PF_PJ',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_FINANC,1,4000),substr(:new.CD_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TRIBUTACAO_MUNICIPIO,1,4000),substr(:new.NR_TRIBUTACAO_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_TRIBUTACAO_MUNICIPIO',ie_log_w,ds_w,'PLS_TRIBUTO_LOG',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TRIBUTO_LOG ADD (
  CONSTRAINT PLSTRLG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TRIBUTO_LOG ADD (
  CONSTRAINT PLSTRLG_PPPLOTE_FK 
 FOREIGN KEY (NR_SEQ_PP_LOTE) 
 REFERENCES TASY.PLS_PP_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT PLSTRLG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTRLG_PLSLOPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PGTO) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_TIPVALO_FK 
 FOREIGN KEY (CD_TIPO_VALOR) 
 REFERENCES TASY.TIPO_VALOR (CD_TIPO_VALOR),
  CONSTRAINT PLSTRLG_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_REC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_IMP) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_TRAFINA_FK3 
 FOREIGN KEY (NR_SEQ_TRANS_TIT_REC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_TRAFINA_FK4 
 FOREIGN KEY (NR_SEQ_TRANS_PROV_TRIB_REC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSTRLG_TRAFINA_FK5 
 FOREIGN KEY (NR_SEQ_TRANS_CONV_REC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TRIBUTO_LOG TO NIVEL_1;

