ALTER TABLE TASY.PROJ_PROJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_PROJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_PROJETO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CLIENTE           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_COORDENADOR           VARCHAR2(10 BYTE),
  CD_GERENTE_CLIENTE       VARCHAR2(10 BYTE),
  DS_TITULO                VARCHAR2(80 BYTE)    NOT NULL,
  DT_INICIO_PREV           DATE,
  DT_FIM_PREV              DATE,
  IE_STATUS                VARCHAR2(15 BYTE)    NOT NULL,
  DT_INICIO_REAL           DATE,
  DT_FIM_REAL              DATE,
  NR_SEQ_CLASSIF           NUMBER(10),
  NM_CURTO_AGENDA          VARCHAR2(7 BYTE),
  DT_PROJETO               DATE                 NOT NULL,
  DT_REPASSE_COMERCIAL     DATE,
  NR_SEQ_FORMA_CONT        NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_VIRADA                DATE,
  IE_ESCORE                VARCHAR2(1 BYTE),
  IE_ORIGEM                VARCHAR2(15 BYTE),
  NR_SEQ_GERENCIA          NUMBER(10),
  NR_SEQ_ESTAGIO           NUMBER(10),
  IE_RESULTADO             VARCHAR2(1 BYTE),
  PR_REALIZACAO            NUMBER(5,2),
  NR_SEQ_ORDEM_SERV        NUMBER(10),
  NR_SEQ_GRUPO_DES         NUMBER(10),
  NR_SEQ_PRIORIDADE        NUMBER(10),
  NR_SEQ_GRUPO             NUMBER(10),
  CD_FUNCAO                NUMBER(5),
  NR_SEQ_ORIGEM_PROJ       NUMBER(10),
  IE_ATRASO                NUMBER(3),
  DS_INFORMACAO_EXTERNA    VARCHAR2(255 BYTE),
  DS_POSICAO_EXTERNA       VARCHAR2(255 BYTE),
  DT_FIM_PREV_ORIG         DATE,
  IE_TIPO_PRODUTO          VARCHAR2(10 BYTE),
  IE_POSICAO               VARCHAR2(1 BYTE),
  IE_POSICAO_EXTERNA       VARCHAR2(1 BYTE),
  IE_STATUS_GANT           VARCHAR2(1 BYTE),
  IE_REF_MIGR              VARCHAR2(1 BYTE),
  CD_GERENTE_PROJETO       VARCHAR2(10 BYTE),
  IE_POSICAO_COMERCIAL     VARCHAR2(1 BYTE),
  IE_VISAO_RESP_MIGR       VARCHAR2(15 BYTE),
  IE_PLANEJ_ESTRATEG       VARCHAR2(1 BYTE),
  DT_REINICIO              DATE,
  DT_PLANEJADA             DATE,
  DT_PLANEJADA_TERM        DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  NR_SEQ_MACRO_PROJ        NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_RECONHECER_RECEITA    DATE,
  DS_OBJETIVO              VARCHAR2(2000 BYTE),
  IE_NIVEL_PROJETO         VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_PROJETO      NUMBER(10),
  DT_APROVACAO             DATE,
  NM_USUARIO_APROV         VARCHAR2(15 BYTE),
  DT_TESTE_CLIENTE         DATE,
  IE_POS_VENDAS            VARCHAR2(1 BYTE),
  IE_RELAT_COMPLETO        VARCHAR2(1 BYTE),
  IE_CAPITALIZAR           VARCHAR2(1 BYTE),
  CD_CONTA_CONTABIL        VARCHAR2(20 BYTE),
  DS_ESCOPO                VARCHAR2(4000 BYTE),
  DS_FORA_ESCOPO           VARCHAR2(4000 BYTE),
  DS_PREMISSAS             VARCHAR2(4000 BYTE),
  NR_SEQ_PROGRAMA          NUMBER(10),
  QT_HORA_CONTRATADA       NUMBER(10),
  NR_SEQ_PROJ_SUP          NUMBER(10),
  DS_VERSAO                VARCHAR2(10 BYTE),
  IE_DELPHI                VARCHAR2(1 BYTE),
  IE_JAVA                  VARCHAR2(1 BYTE),
  IE_HTML5                 VARCHAR2(1 BYTE),
  CD_METODOLOGIA           NUMBER(10),
  NR_SEQ_ARM_PROJ          NUMBER(10),
  NR_SEQ_PAIS              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPROJ_ARMPRJ_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_ARM_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_COMCLIE_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_CONCONT_FK_I ON TASY.PROJ_PROJETO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_ESTABEL_FK_I ON TASY.PROJ_PROJETO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_FUNCAO_FK_I ON TASY.PROJ_PROJETO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_GERWHEB_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_GRUDESE_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_GRUDESE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_MANORSE_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PAIS_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PESFISI_FK_I ON TASY.PROJ_PROJETO
(CD_COORDENADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PESFISI_FK2_I ON TASY.PROJ_PROJETO
(CD_GERENTE_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PESFISI_FK3_I ON TASY.PROJ_PROJETO
(CD_GERENTE_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROPROJ_PK ON TASY.PROJ_PROJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PROCLAF_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PROFOCT_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_FORMA_CONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_PROFOCT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_PROGRUP_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_PROGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_PROJEST_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PROJPRO_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_PROGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PROMAPJ_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_MACRO_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_PROMAPJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_PROORPR_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_ORIGEM_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_PROORPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPROJ_PROPROJ_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_PROJ_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPROJ_PRPTPPROJ_FK_I ON TASY.PROJ_PROJETO
(NR_SEQ_TIPO_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPROJ_PRPTPPROJ_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_projeto_atual
before insert or update ON TASY.PROJ_PROJETO for each row
declare

nr_seq_ordem_serv_pai_w		man_ordem_servico.nr_seq_ordem_serv_pai%type;

begin

select	max(mos.nr_seq_ordem_serv_pai)
into	nr_seq_ordem_serv_pai_w
from	man_ordem_servico mos
where	mos.nr_sequencia = :new.nr_seq_ordem_serv;

if	(nr_seq_ordem_serv_pai_w is not null) then
	begin
		wheb_mensagem_pck.exibir_mensagem_abort(1085000);
	end;
end if;

if	(:new.nr_seq_estagio is not null) and
	(:new.nr_seq_estagio <> nvl(:old.nr_seq_estagio,0)) then
	insert into proj_estagio_hist
		(nr_sequencia, nr_seq_proj, nr_seq_estagio,
		dt_historico, dt_atualizacao, nm_usuario,
		ie_status)
	values(	proj_estagio_hist_seq.nextval, :new.nr_sequencia, :new.nr_seq_estagio,
		sysdate, sysdate, :new.nm_usuario,
		:new.ie_status);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.proj_projeto_atual_after
after insert or update ON TASY.PROJ_PROJETO for each row
declare
qt_reg_w	number(1);
ds_sep_bv_w	varchar2(50) := obter_separador_bv;
ds_mensagem_w	varchar2(4000);
ds_titulo_w	varchar2(4000);
ds_email_origem_w	varchar2(255);
ds_email_destino_w	varchar2(255);
ds_macro_w	varchar2(255);
ie_base_corp_w 	varchar2(1);
ie_base_wheb_w	varchar2(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Fim;
end if;

ie_base_corp_w := obter_se_base_corp;
ie_base_wheb_w := obter_se_base_wheb;

/* Envio de email ao alterar datas dt terminio previsto ou dt versao
   previsto com o estagio 'desenvolvimento e Implementacao' selecionado */

if	(ie_base_corp_w = 'S' or ie_base_wheb_w = 'S') then
	if	(:new.ie_origem = 'D' and :new.ie_status = 'E' and (:new.ds_versao <> :old.ds_versao or :new.dt_fim_prev <> :old.dt_fim_prev)) then
		begin
			ds_email_origem_w	:= 'grp_pmo_emr@Philips.onmicrosoft.com';
			ds_email_destino_w	:= 'grp_pmo_emr@Philips.onmicrosoft.com';
			ds_macro_w	:= 'NR_SEQ_PROJ=' || :new.nr_sequencia;
			ds_titulo_w	:= substr(obter_desc_exp_idioma(933549, wheb_usuario_pck.get_nr_seq_idioma, ds_macro_w), 1, 255);

			ds_macro_w	:= ds_macro_w ||
						';NM_PROJ=' || nvl(:new.ds_titulo, 'N/A') ||
						';DT_OLD=' || nvl(to_char(:old.dt_fim_prev), 'N/A') ||
						';DT_NEW=' || nvl(to_char(:new.dt_fim_prev), 'N/A') ||
						';CD_VERSAO_OLD=' || nvl(:old.ds_versao, 'N/A') ||
						';CD_VERSAO_NEW=' || nvl(:new.ds_versao, 'N/A');
			ds_mensagem_w	:= obter_desc_exp_idioma(933548, wheb_usuario_pck.get_nr_seq_idioma, ds_macro_w);

			enviar_email(ds_titulo_w, ds_mensagem_w, ds_email_origem_w, ds_email_destino_w, :new.nm_usuario, 'M');
		end;
	end if;
end if;

/* Origem Desenvolvimento e Estagio Encerramento */
if	(ie_base_corp_w = 'S' or ie_base_wheb_w = 'S') and
	(:new.ie_origem = 'D') and
	(:new.nr_seq_estagio = 16) and
	(nvl(:old.nr_seq_estagio,0) <> :new.nr_seq_estagio) and
	(:new.nr_seq_ordem_serv is not null) then
	begin
	select	count(*)
	into	qt_reg_w
	from	gpw_pendencia
	where	nr_seq_ordem_serv = :new.nr_seq_ordem_serv;

	if	(qt_reg_w = 0) then
		exec_sql_dinamico_bv('','begin gpw_gerar_pendencia_ordem_serv(:nr_seq_ordem_serv,:nm_usuario); end;',
					'nr_seq_ordem_serv=' || :new.nr_seq_ordem_serv || ds_sep_bv_w ||
					'nm_usuario=' || :new.nm_usuario || ds_sep_bv_w);
	end if;
	end;
end if;

if	(ie_base_corp_w = 'S' or ie_base_wheb_w = 'S') and
	(:new.ie_origem = 'D') and
	((:new.ds_versao <> :old.ds_versao) or
	(:new.dt_inicio_prev <> :old.dt_inicio_prev) or
	(:new.dt_fim_prev <> :old.dt_fim_prev) or
	(:new.ie_status <> :old.ie_status) or
	(:new.nr_seq_ordem_serv <> :old.nr_seq_ordem_serv) or
	(:new.cd_metodologia <> :old.cd_metodologia) or
	(:new.ds_escopo <> :old.ds_escopo)) then
	update_arm_project(:new.ds_versao, :new.dt_inicio_prev, :new.dt_fim_prev, :new.ie_status, :new.nr_seq_ordem_serv, :new.cd_metodologia, :new.ds_escopo, :new.nm_usuario, :new.nr_sequencia);
end if;
<<Fim>>
qt_reg_w := 0;

/* Atualizar o campo NR_SEQ_CLIENTE do PROJ_CRONOGRAMA assim que atualizar o mesmo campo do PROJ_PROJETO */
if	(updating) and
	(:old.nr_seq_cliente <> :new.nr_seq_cliente) then
	begin

	update	proj_cronograma
	set	nr_seq_cliente = :new.nr_seq_cliente
	where	nr_seq_proj = :old.nr_sequencia;

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.proj_projeto_atual_wheb
before insert or update ON TASY.PROJ_PROJETO for each row
declare
dt_ordem_servico_w		date;
qt_historico_w			number(10);
qt_equipe_analise_w		number(10);
qt_equipe_solicitante_w		number(10);
qt_equipe_desenv_w		number(10);
ie_permissao_w			number(10);
qt_requisito_def_w		number(10);
qt_requisito_inc_w		number(10);
qt_requisito_nao_aprov_w	number(10);
qt_historico_envio_w		number(10);
qt_historico_req_w		number(10);
qt_historico_plano_w		number(10);
qt_cronograma_w			number(10);
qt_requisito_nao_atend_w	number(10);
qt_riscos_nao_encerrados_w	number(10);
qt_conteudo_ata_w		number(10);
nr_seq_proj_w			number(10);
nm_cadastro_w			varchar2(255);
ie_email_nao_inform_w		varchar2(15);
ie_ficha_projeto_w		varchar2(1);
qt_historico_prog_w		number(10);
qt_anexo_w			number(10);

begin
if	(:new.ie_origem = 'D') and
	(:new.dt_cancelamento is null) then /*Projetos de desenvolvimento*/
	begin
	/* Consist�ncias de datas */
	if	(trunc(:new.dt_projeto) > trunc(:new.dt_inicio_prev)) then
		/*'A data de in�cio previsto (' || :new.dt_inicio_prev || ') tem que ser maior ou igual a data do
projeto (' ||
					:new.dt_projeto || '). Projeto: ' || :new.nr_sequencia || ' #@#@');*/
		wheb_mensagem_pck.exibir_mensagem_abort(209953,'DT_INICIO_PREV=' || :new.dt_inicio_prev || ';' ||
								'DT_PROJETO=' || :new.dt_projeto || ';' ||
								'NR_SEQUENCIA=' || :new.nr_sequencia);
	end if;
	if	(:new.dt_inicio_prev > nvl(:new.dt_fim_prev,:new.dt_inicio_prev)) then
		/*A data de in�cio previsto tem que ser menor ou igual a data de t�rmino previsto*/
		wheb_mensagem_pck.exibir_mensagem_abort(209954);
	end if;
	if	(:new.dt_inicio_real > :new.dt_fim_real) then
		/*A data de in�cio real tem que ser menor ou igual a data de t�rmino real*/
		wheb_mensagem_pck.exibir_mensagem_abort(209955);
	end if;
	/*Atualizar fim previsto do projeto na ordem de sevi�o de origem*/
	if	(:new.DT_FIM_PREV is not null) then
		update man_ordem_servico
		set	dt_fim_previsto  = :new.dt_fim_prev
		where	nr_sequencia = :new.nr_seq_ordem_serv;
	end if;

	/*Garantir que seja o gerente quem cadastra o projeto*/
	if	(inserting) then
		select	count(*)
		into	ie_permissao_w
		from	usuario a
		where	a.nm_usuario		= :new.nm_usuario
		and exists(	select 1
					from  proj_permissao_cadastro b
					where b.ie_situacao = 'A'
					and	nvl(b.ie_tipo,'A') in ('C','A')
					and (b.nm_usuario_perm = a.nm_usuario
					or b.cd_cargo_perm = (	select cd_cargo
								from pessoa_fisica c
								where c.cd_pessoa_fisica = a.cd_pessoa_fisica)));
		if	(ie_permissao_w = 0) then

			/*Voc� n�o possui permiss�o para cadastrar projetos no sistesma, conforme cadastro geral "#@nome_cadastro#@".*/

			select obter_desc_expressao((select CD_EXP_CADASTRO
			from tabela_sistema where nm_tabela = 'PROJ_PERMISSAO_CADASTRO'), 'Permiss�o cadastro de projetos')
			into nm_cadastro_w
			from dual;

			wheb_mensagem_pck.exibir_mensagem_abort(745374, 'NOME_CADASTRO=' || nm_cadastro_w || '''');
		end if;
	end if;

	if	(:new.NR_SEQ_CLASSIF 	<> 45) then

		/* Garantir estudo de viabilidade e v�nculo com OS com data correta */
		if	(:new.nr_seq_ordem_serv is not null) and
			(:old.nr_seq_ordem_serv is null) then
			select	dt_ordem_servico
			into	dt_ordem_servico_w
			from	man_ordem_servico
			where	nr_sequencia	= :new.nr_seq_ordem_serv;
			if	(trunc(dt_ordem_servico_w,'dd') > :new.dt_projeto) then
				/*A data da ordem de servi�o que deu origem ao projeto deve ser inferior a data do projeto*/
				wheb_mensagem_pck.exibir_mensagem_abort(209958);
			end if;
			if	(:new.nr_seq_classif <> 14) then
				begin
				select	count(*)
				into	qt_historico_w
				from	man_ordem_serv_tecnico
				where	nr_seq_tipo	= 21 /*Hist�rico do tipo Estudo de viabilidade*/
				and	nr_seq_ordem_serv = :new.nr_seq_ordem_serv
				and	dt_liberacao is not null;
				if	(qt_historico_w = 0) then
					/*Para que uma OS gere um projeto a mesma deve possuir um hist�rico do tipo Estudo de
	viabilidade liberado*/
					wheb_mensagem_pck.exibir_mensagem_abort(209959);
				end if;
				end;
			end if;
		end if;

	end if;

	if	(:new.NR_SEQ_CLASSIF 	<> 45) then

		if	(:new.nr_seq_estagio = 20) and /*Levantamento dos requisitos*/
			(:old.nr_seq_estagio = 1) then
			select	count(*)
			into	qt_equipe_analise_w
			from	proj_equipe_papel b,
				proj_equipe a
			where	a.nr_sequencia	= b.nr_seq_equipe
			and	a.nr_seq_proj	= :new.nr_sequencia
			and	a.nr_seq_equipe_funcao	= 10; /*Equipe de an�lise*/
			if	(qt_equipe_analise_w = 0) then
				/*O projeto precisa ter equipe de an�lise definida. Verificar com o Gerente*/
				wheb_mensagem_pck.exibir_mensagem_abort(209960);
			end if;
			select	count(*)
			into	qt_equipe_solicitante_w
			from	proj_equipe_papel b,
				proj_equipe a
			where	a.nr_sequencia	= b.nr_seq_equipe
			and	a.nr_seq_proj	= :new.nr_sequencia
			and	a.nr_seq_equipe_funcao	= 13; /*Equipe do solicitante*/
			if	(qt_equipe_solicitante_w = 0) then
				/*O projeto precisa ter equipe do solicitante definida. Verificar com o Gerente.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209961);
			end if;
			select	decode(count(*), 0, 'N', 'S')
			into	ie_email_nao_inform_w
			from	proj_equipe_papel b,
				proj_equipe a
			where	a.nr_sequencia	= b.nr_seq_equipe
			and	a.nr_seq_proj	= :new.nr_sequencia
			and	b.ds_email is null
			and	b.nr_seq_funcao is not null;

			if	(ie_email_nao_inform_w = 'S') then
				/*Devem ser informados os e-mails de todos os integrantes das equipes*/
				wheb_mensagem_pck.exibir_mensagem_abort(983988);
			end if;
		end if;

	end if;
	if	(:new.nr_seq_estagio = 2) and /*Requisito definido*/
		(:old.nr_seq_estagio = 20) then
		select	count(*)
		into	qt_requisito_def_w
		from	des_requisito_item b,
			des_requisito a
		where	a.nr_seq_projeto	= :new.nr_sequencia
		and	a.nr_sequencia		= b.nr_seq_requisito;
		if	(qt_requisito_def_w = 0) then
			/*'O projeto precisa ter os requisitos definidos.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(209962);
		end if;
		select	count(*)
		into	qt_requisito_inc_w
		from	des_requisito_item b,
			des_requisito a
		where	a.nr_seq_projeto	= :new.nr_sequencia
		and	a.nr_sequencia		= b.nr_seq_requisito
		and	((b.ie_status is null) or (b.ie_tipo_requisito is null));
		if	(qt_requisito_inc_w > 0) then
			/*Existem requisitos sem status ou sem tipo definido.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(209963);
		end if;
	end if;
	if	(:new.NR_SEQ_CLASSIF 	<> 45) then
		if	(:new.nr_seq_estagio = 4) and /*Aguardando aprov. requisito*/
			(:old.nr_seq_estagio = 3) then
			select	count(*)
			into	qt_historico_envio_w
			from	com_cliente_hist
			where	nr_seq_projeto	= :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo	= 12;
			if	(qt_historico_envio_w = 0) then
				/*O projeto precisa ter um hist�rico de envio dos requisitos para aprova��o do cliente.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209964);
			end if;
		end if;

	end if;


	if	(:new.nr_seq_estagio = 5) and /*Requisito aprovado*/
		(:old.nr_seq_estagio = 4) then
		select	count(*)
		into	qt_requisito_nao_aprov_w
		from	des_requisito
		where	nr_seq_projeto	= :new.nr_sequencia
		and	dt_aprovacao is null;
		if	(qt_requisito_nao_aprov_w > 0) then
			/*'O projeto possui requisitos n�o aprovados.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(209965);
		end if;
		if	(:new.NR_SEQ_CLASSIF 	<> 45) then
			select	count(*)
			into	qt_historico_req_w
			from	com_cliente_hist
			where	nr_seq_projeto	= :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo	= 13;
			if	(qt_historico_req_w = 0) then
				/*O projeto precisa ter um hist�rico de requisitos aprovados pelo cliente.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209966);
			end if;

		end if;

	end if;

	if	(:new.NR_SEQ_CLASSIF 	<> 45) then
		if	(:new.nr_seq_estagio = 8) and /*Aguardando aprova��o do plano*/
			(:old.nr_seq_estagio = 7) then
			select	count(*)
			into	qt_equipe_desenv_w
			from	proj_equipe_papel b,
				proj_equipe a
			where	a.nr_sequencia	= b.nr_seq_equipe
			and	a.nr_seq_proj	= :new.nr_sequencia
			and	a.nr_seq_equipe_funcao in (11, 20); /*Equipe de desenvolvimento*/
			if	(qt_equipe_desenv_w = 0) then
				/*O projeto precisa ter equipe de desenvolvimento definida. Verificar com o Gerente.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209967);
			end if;

			select	decode(count(*), 0, 'N', 'S')
			into	ie_email_nao_inform_w
			from	proj_equipe_papel b,
				proj_equipe a
			where	a.nr_sequencia	= b.nr_seq_equipe
			and	a.nr_seq_proj	= :new.nr_sequencia
			and	b.nr_seq_funcao is not null
			and	b.ds_email is null;

			if	(ie_email_nao_inform_w = 'S') then
			/*Devem ser informados os e-mails de todos os integrantes das equipes*/
				wheb_mensagem_pck.exibir_mensagem_abort(983988);
			end if;

			select	count(*)
			into	qt_historico_plano_w
			from	com_cliente_hist
			where	nr_seq_projeto	= :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo	= 14;
			if	(qt_historico_plano_w = 0) then
				/*'O projeto precisa ter um hist�rico de envio do plano para aprova��o do solicitante.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209971);
			end if;
			select	count(*)
			into	qt_historico_plano_w
			from	pessoa_fisica p,
				com_cliente_hist a,
				usuario u
			where	a.nr_seq_projeto	= :new.nr_sequencia
			and	a.cd_profissional	= p.cd_pessoa_fisica
			and	u.CD_PESSOA_FISICA	= p.cd_pessoa_fisica
			and	a.dt_liberacao is not null
			and	a.nr_seq_tipo	= 16
			and 	exists(	select 	1
					from 	proj_permissao_cadastro b
					where 	b.ie_situacao = 'A'
					and	nvl(b.ie_tipo,'A') in ('P','A')
					and 	(b.nm_usuario_perm = u.nm_usuario or b.cd_cargo_perm = p.cd_cargo));


			if	(qt_historico_plano_w = 0) then
				/*O projeto precisa ter um hist�rico de aprova��o interna do plano pelo gerente
	operacional.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209972);
			end if;
			select	count(*)
			into	qt_cronograma_w
			from	proj_cronograma
			where	nr_seq_proj		= :new.nr_sequencia
			and	ie_tipo_cronograma	= 'E'
			and	ie_situacao 		= 'A'
			and	nvl(IE_CLASSIFICACAO, 'D')	= 'D';
			if	(qt_cronograma_w = 0) then
				/*O projeto precisa ter um cronograma de execu��o.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209973);
			end if;
		end if;


		if	(:new.nr_seq_estagio = 9) and /*Plano aprovado*/
			(:old.nr_seq_estagio = 8) then
			select	count(*)
			into	qt_cronograma_w
			from	proj_cronograma
			where	nr_seq_proj		= :new.nr_sequencia
			and	ie_tipo_cronograma	= 'E'
			and	ie_situacao 		= 'A'
			and	IE_CLASSIFICACAO	= 'D'
			and	dt_aprovacao is null;
			if	(qt_cronograma_w > 0) then
				/*O projeto precisa ter um cronograma de execu��o com data de aprova��o informada.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209974);
			end if;
			select	count(*)
			into	qt_historico_plano_w
			from	com_cliente_hist
			where	nr_seq_projeto	= :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo	= 15;
			if	(qt_historico_plano_w = 0) then
				/*O projeto precisa ter um hist�rico de aprova��o do plano pelo solicitante.#@#@');*/
				wheb_mensagem_pck.exibir_mensagem_abort(209975);
			end if;
		end if;


		if	(:old.nr_seq_estagio = 10) /*Reuni�o de Implementa��o */ and
			(:new.nr_seq_estagio = 42) /*Aguardando Inicio Execu��o */ then
			/*Reuni�o de implementa��o*/
			select	count(*)
			into	qt_historico_plano_w
			from	com_cliente_hist
			where	nr_seq_projeto	= :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo	= 27;
			if	(qt_historico_plano_w = 0 ) then
				/*'O projeto precisa ter uma ata de Reuni�o de Implementa��o com a equipe*/
				wheb_mensagem_pck.exibir_mensagem_abort(209952);
			end if;
		end if;

	end if;
	if	(:new.nr_seq_estagio = 12) and /*Programa��o*/
		(:old.nr_seq_estagio <> 12) then
		if (:new.dt_fim_prev is null) then
			/*Projetos em programa��o devem possuir a data de fim previsto informada.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(209976);
		end if;

		if (:new.nr_seq_proj_sup is null) then

			select	count(1)
			into	qt_historico_prog_w
			from	com_cliente_hist
			where	nr_seq_projeto = :new.nr_sequencia
			and	dt_liberacao is not null
			and	nr_seq_tipo = 55; -- Aus�ncia de modelagem de dados e objetos

			if (qt_historico_prog_w = 0) then

				select	count(1)
				into	qt_historico_prog_w
				from	com_cliente_hist
				where	nr_seq_projeto = :new.nr_sequencia
				and	dt_liberacao is not null
				and	nr_seq_tipo = 54; -- Aprova��o de Modelagem de dados

				select	count(1)
				into	qt_anexo_w
				from	proj_documento
				where	nr_seq_proj = :new.nr_sequencia
				and	nr_seq_tipo_documento = 23; -- Modelo de dados

				if (qt_historico_prog_w = 0 or qt_anexo_w = 0) then
					/* Para avan�ar para o est�gio "Programa��o", � necess�rio que exista um documento com o tipo "Modelagem de dados" e um hist�rico do tipo "Aprova��o de modelagem de dados" ou um hist�rico
					do tipo "Aus�ncia de modelagem de dados e objetos" */
					wheb_mensagem_pck.exibir_mensagem_abort(1029792);
				end if;
			end if;
		end if;
	end if;
	if	(:new.nr_seq_estagio = 17) and /*Teste no Cliente*/
		(:old.nr_seq_estagio <> 17) then
		SELECT  COUNT(*)
		into	qt_requisito_nao_atend_w
		FROM   	DES_REQUISITO a,
		 	DES_REQUISITO_item b
		WHERE   a.nr_sequencia = b.nr_seq_requisito
		AND	b.ie_status = 'N'
		AND	a.nr_seq_projeto = :new.nr_sequencia
		/* Francisco - 07/08/2013 - Considerar apenas requisitos nossos, s�o estes que o cliente aprova */
		and	a.ie_cliente_wheb = 'W';
		if	(qt_requisito_nao_atend_w > 0 ) then
			/*Para colocar o projeto em teste no cliente n�o podem existir requisitos n�o
atendidos.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(209977);
		end if;

		:new.dt_teste_cliente := sysdate;
	end if;
	if	(:new.nr_seq_estagio = 16) and /*Encerramento*/
		(:old.nr_seq_estagio <> 16) then
		select  count(*)
		into	qt_riscos_nao_encerrados_w
		from   	proj_risco_implantacao
		where   nr_seq_proj = :new.nr_sequencia
		and	dt_fim_real is null;
		if	(qt_riscos_nao_encerrados_w > 0 ) then
			/* N�o � permitido encerrar o projeto se todos os riscos n�o estiverem encerrados.#@#@');*/
			wheb_mensagem_pck.exibir_mensagem_abort(241326);
		end if;
	end if;
	if	(:new.IE_STATUS  = 'E') and /*Desenvolvimento e Implementa��o*/
		(:new.dt_fim_prev is null) then
			/* N�o permitir que um projeto tenha seu status alterado para Desenvolvimento e Implementa��o
caso n�o possua um T�rmino previsto*/
			wheb_mensagem_pck.exibir_mensagem_abort(243490);
	end if;
	--if	(:new.IE_STATUS  = 'E') and /*Desenvolvimento e Implementa��o*/
--		(:new.dt_fim_prev <> :old.dt_fim_prev) then
			/*  Se um projeto j� possuir o campo Status como Desenvolvimento e Implementa��o, n�o permitir
altera��o no campo T�rmino previsto */
--			wheb_mensagem_pck.exibir_mensagem_abort(243495);
--	end if;

	if	(:old.IE_STATUS  <> 'F') and
		(:new.IE_STATUS  = 'F') then
		select	max(decode(:new.nr_seq_classif, 45, 'N', decode((select 1 from proj_ficha_projeto b where :new.nr_sequencia = b.nr_seq_projeto and dt_liberacao is not null), 1, 'N', 'S')))
		into	ie_ficha_projeto_w
		from	dual;

		if (ie_ficha_projeto_w = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort(1012262);
		end if;
	end if;

	if	(:old.IE_STATUS  = 'E') and
		(:new.IE_STATUS  = 'P') then
		:new.dt_inicio_real := null;
	end if;

	/*OS*/
	if	(:new.IE_STATUS = 'E') then
		:new.IE_PLANEJ_ESTRATEG := 'E'; /*Execu��o*/

		select	max(nr_seq_proj)
		into	nr_seq_proj_w
		from	proj_cronograma
		where	nr_seq_proj = :new.nr_sequencia
		and	dt_aprovacao is not null
		and	ie_situacao = 'A';

		if	(nr_seq_proj_w is not null) and (:new.nr_seq_estagio = 12) then
			update	proj_cronograma
			set	dt_inicio_real = sysdate
			where	nr_seq_proj = nr_seq_proj_w
			and	dt_inicio_real is null
			and	ie_classificacao = 'D'
			and	ie_situacao = 'A';
		end if;

		if	(:new.nr_seq_estagio = 12) and (:new.dt_inicio_real is null) then
			:new.dt_inicio_real := sysdate;
		end if;

		if	(:new.IE_STATUS_GANT is null ) then
			:new.ie_status_gant := 1; /*Verde*/
		end if;
	end if;

	if	(:new.ie_status = 'F') and
		(:new.dt_fim_real is null) then
		:new.dt_fim_real	:= sysdate;
	end if;
	end;
end if;
end;
/


ALTER TABLE TASY.PROJ_PROJETO ADD (
  CONSTRAINT PROPROJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_PROJETO ADD (
  CONSTRAINT PROPROJ_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PROPROJ_PROJPRO_FK 
 FOREIGN KEY (NR_SEQ_PROGRAMA) 
 REFERENCES TASY.PROJ_PROGRAMA (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_SUP) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_ARMPRJ_FK 
 FOREIGN KEY (NR_SEQ_ARM_PROJ) 
 REFERENCES TASY.ARM_PROJECT (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PRPTPPROJ_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PROJETO) 
 REFERENCES TASY.PRP_TIPO_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PESFISI_FK 
 FOREIGN KEY (CD_COORDENADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPROJ_PESFISI_FK2 
 FOREIGN KEY (CD_GERENTE_CLIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPROJ_PROCLAF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROJ_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PROFOCT_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CONT) 
 REFERENCES TASY.PROJ_FORMA_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PROJEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.PROJ_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PROGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PROJ_GRUPO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT PROPROJ_PROORPR_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM_PROJ) 
 REFERENCES TASY.PROJ_ORIGEM_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_PESFISI_FK3 
 FOREIGN KEY (CD_GERENTE_PROJETO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPROJ_PROMAPJ_FK 
 FOREIGN KEY (NR_SEQ_MACRO_PROJ) 
 REFERENCES TASY.PROJ_MACRO_PROJETO (NR_SEQUENCIA),
  CONSTRAINT PROPROJ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PROJ_PROJETO TO NIVEL_1;

