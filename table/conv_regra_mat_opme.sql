ALTER TABLE TASY.CONV_REGRA_MAT_OPME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_REGRA_MAT_OPME CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_REGRA_MAT_OPME
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_CONVENIO           NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(10),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  IE_OPME               VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREMAO_CLAMATE_FK_I ON TASY.CONV_REGRA_MAT_OPME
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREMAO_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COREMAO_CONVENI_FK_I ON TASY.CONV_REGRA_MAT_OPME
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COREMAO_GRUMATE_FK_I ON TASY.CONV_REGRA_MAT_OPME
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREMAO_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COREMAO_MATERIA_FK_I ON TASY.CONV_REGRA_MAT_OPME
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREMAO_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COREMAO_PK ON TASY.CONV_REGRA_MAT_OPME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COREMAO_SUBMATE_FK_I ON TASY.CONV_REGRA_MAT_OPME
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREMAO_SUBMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CONV_REGRA_MAT_OPME ADD (
  CONSTRAINT COREMAO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_REGRA_MAT_OPME ADD (
  CONSTRAINT COREMAO_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT COREMAO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT COREMAO_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT COREMAO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COREMAO_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.CONV_REGRA_MAT_OPME TO NIVEL_1;

