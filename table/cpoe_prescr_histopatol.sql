ALTER TABLE TASY.CPOE_PRESCR_HISTOPATOL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_PRESCR_HISTOPATOL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_PRESCR_HISTOPATOL
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_RESULT_HPV            VARCHAR2(1 BYTE),
  IE_RESULT_ASCUS          VARCHAR2(1 BYTE),
  IE_RESULT_NICI           VARCHAR2(1 BYTE),
  IE_RESULT_NICII          VARCHAR2(1 BYTE),
  IE_RESULT_NICIII         VARCHAR2(1 BYTE),
  IE_RESULT_CAR_ESC_INV    VARCHAR2(1 BYTE),
  IE_RESULT_ADEN_IN_SITO   VARCHAR2(1 BYTE),
  IE_RESULT_ADEN_INVASIVO  VARCHAR2(1 BYTE),
  IE_RESULT_OUTROS         VARCHAR2(1 BYTE),
  IE_RESULT_NAO_FORNEC     VARCHAR2(1 BYTE),
  DS_RESULT_EXAME_OUTRO    VARCHAR2(255 BYTE),
  IE_COLPOSCOPIA           VARCHAR2(5 BYTE)     NOT NULL,
  IE_COLPOSCOPIA_ANORMAL   VARCHAR2(5 BYTE),
  IE_PROCEDIMENTO          VARCHAR2(5 BYTE)     NOT NULL,
  IE_CAF                   VARCHAR2(5 BYTE),
  DT_PRIM_HORARIO          DATE,
  DS_INF_ADIC              VARCHAR2(255 BYTE),
  NR_ATENDIMENTO           NUMBER(10),
  CD_PERFIL_ATIVO          NUMBER(5),
  HR_PRIM_HORARIO          VARCHAR2(5 BYTE),
  DT_LIBERACAO             DATE,
  DT_SUSPENSAO             DATE,
  NM_USUARIO_SUSP          VARCHAR2(15 BYTE),
  IE_DURACAO               VARCHAR2(3 BYTE)     NOT NULL,
  DT_PROX_GERACAO          DATE,
  DT_INICIO                DATE                 NOT NULL,
  DT_FIM                   DATE,
  IE_ADMINISTRACAO         VARCHAR2(3 BYTE),
  IE_EVENTO_UNICO          VARCHAR2(1 BYTE),
  NR_SEQ_CPOE_ANTERIOR     NUMBER(10),
  DT_LIB_SUSPENSAO         DATE,
  NR_SEQ_TRANSCRICAO       NUMBER(10),
  DS_STACK                 VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  NM_USUARIO_LIB_ENF       VARCHAR2(15 BYTE),
  CD_FARMAC_LIB            VARCHAR2(10 BYTE),
  CD_FUNCAO_ORIGEM         NUMBER(10),
  IE_BAIXADO_POR_ALTA      VARCHAR2(1 BYTE),
  IE_CPOE_FARM             VARCHAR2(1 BYTE),
  NR_CPOE_INTERF_FARM      NUMBER(10),
  IE_MOTIVO_PRESCRICAO     VARCHAR2(3 BYTE),
  IE_ITEM_VALIDO           VARCHAR2(1 BYTE),
  IE_INTERVENCAO_FARM      VARCHAR2(1 BYTE),
  IE_ITEM_ALTA             VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  NR_SEQ_PROC_ANATOMIA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOPREH_ATEPACI_FK_I ON TASY.CPOE_PRESCR_HISTOPATOL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPREH_CPOANAP_FK_I ON TASY.CPOE_PRESCR_HISTOPATOL
(NR_SEQ_PROC_ANATOMIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPREH_PERFIL_FK_I ON TASY.CPOE_PRESCR_HISTOPATOL
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPREH_PESFISI_FK_I ON TASY.CPOE_PRESCR_HISTOPATOL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOPREH_PK ON TASY.CPOE_PRESCR_HISTOPATOL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPREH_SETATEN_FK_I ON TASY.CPOE_PRESCR_HISTOPATOL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CPOE_PRESCR_HISTOPATOL_tp  after update ON TASY.CPOE_PRESCR_HISTOPATOL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'CPOE_PRESCR_HISTOPATOL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'CPOE_PRESCR_HISTOPATOL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO',ie_log_w,ds_w,'CPOE_PRESCR_HISTOPATOL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM',ie_log_w,ds_w,'CPOE_PRESCR_HISTOPATOL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CPOE_PRESCR_HISTOPATOL ADD (
  CONSTRAINT CPOPREH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_PRESCR_HISTOPATOL ADD (
  CONSTRAINT CPOPREH_CPOANAP_FK 
 FOREIGN KEY (NR_SEQ_PROC_ANATOMIA) 
 REFERENCES TASY.CPOE_ANATOMIA_PATOLOGICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CPOPREH_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT CPOPREH_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CPOPREH_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPOPREH_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CPOE_PRESCR_HISTOPATOL TO NIVEL_1;

