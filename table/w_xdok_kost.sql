ALTER TABLE TASY.W_XDOK_KOST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_XDOK_KOST CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_XDOK_KOST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QUALIFIER            VARCHAR2(1 BYTE),
  JAHR                 NUMBER(4),
  VERSION              NUMBER(1),
  KOSTENTRAGER         VARCHAR2(2 BYTE),
  ABRECHNUNGSRELEVANZ  VARCHAR2(1 BYTE),
  BEZEICHNUNG          VARCHAR2(50 BYTE),
  LANGBEZEICHNUNG      VARCHAR2(150 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WXKOST_I1 ON TASY.W_XDOK_KOST
(QUALIFIER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WXKOST_PK ON TASY.W_XDOK_KOST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WXKOST_WXIMPT_FK_I ON TASY.W_XDOK_KOST
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_XDOK_KOST ADD (
  CONSTRAINT WXKOST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

