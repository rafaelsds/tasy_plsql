ALTER TABLE TASY.W_AVAL_WHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_AVAL_WHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_AVAL_WHO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO              VARCHAR2(3 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SEXO              VARCHAR2(1 BYTE),
  IE_VERSAO            VARCHAR2(10 BYTE),
  QT_ANOS              NUMBER(5),
  QT_MESES             NUMBER(5),
  QT_ALTURA_CM         NUMBER(5,2),
  QT_SEMANAS           NUMBER(5),
  QT_SD_MENOS_3        NUMBER(5,2),
  QT_SD_MENOS_2        NUMBER(5,2),
  QT_SD_MENOS_1        NUMBER(5,2),
  QT_MEDIA             NUMBER(5,2),
  QT_SD_MAIS_1         NUMBER(5,2),
  QT_SD_MAIS_2         NUMBER(5,2),
  QT_SD_MAIS_3         NUMBER(5,2),
  QT_1                 NUMBER(5,2),
  QT_3                 NUMBER(5,2),
  QT_5                 NUMBER(5,2),
  QT_15                NUMBER(5,2),
  QT_25                NUMBER(5,2),
  QT_50                NUMBER(6,2),
  QT_75                NUMBER(5,2),
  QT_85                NUMBER(5,2),
  QT_95                NUMBER(5,2),
  QT_97                NUMBER(5,2),
  QT_99                NUMBER(5,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WAVAWHO_PK ON TASY.W_AVAL_WHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_AVAL_WHO ADD (
  CONSTRAINT WAVAWHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_AVAL_WHO TO NIVEL_1;

