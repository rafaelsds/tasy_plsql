ALTER TABLE TASY.ODONT_HISTORICO_PROCED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ODONT_HISTORICO_PROCED CASCADE CONSTRAINTS;

CREATE TABLE TASY.ODONT_HISTORICO_PROCED
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_FACE_HIST         NUMBER(10),
  NR_SEQ_PROC_INTERNO      NUMBER(10)           NOT NULL,
  QT_PROCEDIMENTO          NUMBER(10)           NOT NULL,
  CD_PROFISSIONAL          VARCHAR2(10 BYTE),
  IE_PROFISSIONAL_EXTERNO  VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE         VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO          NUMBER(15)           NOT NULL,
  IE_ORIGEM_PROCED         NUMBER(10),
  IE_FACE_DISTAL           VARCHAR2(1 BYTE),
  IE_FACE_INCISAL          VARCHAR2(1 BYTE),
  IE_FACE_LINGUAL          VARCHAR2(1 BYTE),
  IE_FACE_MESIAL           VARCHAR2(1 BYTE),
  IE_FACE_OCLUSAL          VARCHAR2(1 BYTE),
  IE_FACE_PALATINA         VARCHAR2(1 BYTE),
  NR_SEQ_ODONT_HIST        NUMBER(10),
  IE_FACE_VESTIBULAR       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ODHISPRO_ODHISFAC_FK_I ON TASY.ODONT_HISTORICO_PROCED
(NR_SEQ_FACE_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ODHISPRO_ODOHIST_FK_I ON TASY.ODONT_HISTORICO_PROCED
(NR_SEQ_ODONT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ODHISPRO_PK ON TASY.ODONT_HISTORICO_PROCED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ODHISPRO_PROCEDI_FK_I ON TASY.ODONT_HISTORICO_PROCED
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ODHISPRO_PROINTE_FK_I ON TASY.ODONT_HISTORICO_PROCED
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ODONT_HISTORICO_PROC_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ODONT_HISTORICO_PROCED FOR EACH ROW
DECLARE

qt_reg_w		number(1);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);

BEGIN


if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if	(:new.NR_SEQ_PROC_INTERNO is not null) and
	(:new.CD_PROCEDIMENTO is null) Then

	select	max(cd_procedimento),
			max(ie_origem_proced)
	into	cd_procedimento_w,
			ie_origem_proced_w
	from	proc_interno
	where	nr_sequencia = :new.NR_SEQ_PROC_INTERNO;

	:new.CD_PROCEDIMENTO := cd_procedimento_w;
	:new.IE_ORIGEM_PROCED := ie_origem_proced_w;

end if;


<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.ODONT_HISTORICO_PROCED ADD (
  CONSTRAINT ODHISPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ODONT_HISTORICO_PROCED ADD (
  CONSTRAINT ODHISPRO_ODHISFAC_FK 
 FOREIGN KEY (NR_SEQ_FACE_HIST) 
 REFERENCES TASY.ODONT_HISTORICO_FACE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ODHISPRO_ODOHIST_FK 
 FOREIGN KEY (NR_SEQ_ODONT_HIST) 
 REFERENCES TASY.ODONT_HISTORICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ODHISPRO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT ODHISPRO_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ODONT_HISTORICO_PROCED TO NIVEL_1;

