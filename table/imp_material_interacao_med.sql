ALTER TABLE TASY.IMP_MATERIAL_INTERACAO_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMP_MATERIAL_INTERACAO_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_MATERIAL_INTERACAO_MED
(
  NR_SEQUENCIA            NUMBER(10),
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_MATERIAL             NUMBER(10),
  DT_ATUALIZACAO          DATE,
  NM_USUARIO              VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_MATERIAL_INTERACAO   NUMBER(10),
  DS_TIPO                 VARCHAR2(225 BYTE),
  IE_SEVERIDADE           VARCHAR2(3 BYTE),
  DS_ORIENTACAO           VARCHAR2(2000 BYTE),
  NR_SEQ_FICHA            NUMBER(10),
  NR_SEQ_FICHA_INTERACAO  NUMBER(10),
  CD_GRUPO_MATERIAL       NUMBER(3),
  IE_EXIBIR_GRAVIDADE     VARCHAR2(1 BYTE),
  CD_SUBGRUPO_MATERIAL    NUMBER(10)            DEFAULT null,
  CD_CLASSE_MATERIAL      NUMBER(10)            DEFAULT null,
  IE_MENSAGEM_CADASTRADA  VARCHAR2(1 BYTE),
  IE_CONSISTIR            VARCHAR2(15 BYTE),
  IE_DIRTY_CHECK          NUMBER(1),
  DS_REF_BIBLIOGRAFICA    VARCHAR2(4000 BYTE),
  IE_GRAVAR               VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO        VARCHAR2(15 BYTE),
  IE_FUNCAO_PRESCRITOR    VARCHAR2(3 BYTE),
  NR_SEQ_MIMS_VERSION     NUMBER(15),
  DS_OBSERVATION_NOTES    VARCHAR2(256 BYTE),
  DS_PRECAUTION_NOTES     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATINTMED_IMMEFITE_FK_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINTMED_IMMEFITE_FK2_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(NR_SEQ_FICHA_INTERACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINTMED_IMPCM_FK_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINTMED_IMPMATE_FK_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINTMED_IMPMATE_FK2_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(CD_MATERIAL_INTERACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATINTMED_MIMS_SUB_M_FK_I ON TASY.IMP_MATERIAL_INTERACAO_MED
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATINTMED_PK ON TASY.IMP_MATERIAL_INTERACAO_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_MATERIAL_INTERACAO_MED ADD (
  CONSTRAINT MATINTMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMP_MATERIAL_INTERACAO_MED ADD (
  CONSTRAINT MATINTMED_IMMEFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.IMP_MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT MATINTMED_IMMEFITE_FK2 
 FOREIGN KEY (NR_SEQ_FICHA_INTERACAO) 
 REFERENCES TASY.IMP_MEDIC_FICHA_TECNICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATINTMED_IMPCM_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.IMP_CLASSE_MATERIAL (CD_CLASSE_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATINTMED_IMPMATE_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.IMP_MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATINTMED_IMPMATE_FK2 
 FOREIGN KEY (CD_MATERIAL_INTERACAO) 
 REFERENCES TASY.IMP_MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATINTMED_MIMS_SUB_M_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.IMP_SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMP_MATERIAL_INTERACAO_MED TO NIVEL_1;

