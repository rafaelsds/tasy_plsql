ALTER TABLE TASY.PRESCR_REP_HE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_REP_HE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_REP_HE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_PRESCRICAO             NUMBER(14)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_PESO                   NUMBER(15,3)        NOT NULL,
  QT_IDADE_DIA              NUMBER(15)          NOT NULL,
  QT_IDADE_MES              NUMBER(15)          NOT NULL,
  QT_IDADE_ANO              NUMBER(15)          NOT NULL,
  QT_ALTURA_CM              NUMBER(15),
  QT_KCAL_TOTAL             NUMBER(15,1)        NOT NULL,
  QT_KCAL_KG                NUMBER(15,1)        NOT NULL,
  QT_ETAPA                  NUMBER(15)          NOT NULL,
  QT_NEC_HIDRICA_DIARIA     NUMBER(15,2),
  QT_APORTE_HIDRICO_DIARIO  NUMBER(15,2),
  QT_VEL_INF_GLICOSE        NUMBER(15,2),
  QT_NEC_KCAL_KG_DIA        NUMBER(15,2),
  QT_NEC_KCAL_DIA           NUMBER(15,2),
  QT_EQUIPO                 NUMBER(15,1)        NOT NULL,
  PR_CONC_GLIC_SOLUCAO      NUMBER(15,1),
  IE_CALCULO_AUTO           VARCHAR2(1 BYTE)    NOT NULL,
  IE_CORRECAO               VARCHAR2(1 BYTE)    NOT NULL,
  HR_PRIM_HORARIO           VARCHAR2(5 BYTE),
  QT_GOTEJO                 NUMBER(15,2),
  IE_EMISSAO                NUMBER(1)           NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_VOL_DESCONTO           NUMBER(15,1)        NOT NULL,
  QT_BE                     NUMBER(15,1),
  QT_HORA_VALIDADE          NUMBER(15),
  QT_HORA_FASE              NUMBER(15),
  IE_BOMBA_INFUSAO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_MAGNESIO               VARCHAR2(1 BYTE),
  IE_PESO_CALORICO          VARCHAR2(1 BYTE),
  IE_PROCESSO_HIDRICO       VARCHAR2(15 BYTE),
  DT_PRIM_HORARIO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREREHE_I1 ON TASY.PRESCR_REP_HE
(IE_EMISSAO, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREREHE_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREREHE_PK ON TASY.PRESCR_REP_HE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREREHE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PREREHE_PRESMED_FK_I ON TASY.PRESCR_REP_HE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_rep_he_atual
before insert or update ON TASY.PRESCR_REP_HE for each row
declare

begin
begin
	if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
		:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.PRESCR_REP_HE ADD (
  CONSTRAINT PREREHE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_REP_HE ADD (
  CONSTRAINT PREREHE_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_REP_HE TO NIVEL_1;

