ALTER TABLE TASY.RELAC_PF_INSTITUICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELAC_PF_INSTITUICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELAC_PF_INSTITUICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_REGISTRO          DATE                     NOT NULL,
  CD_RESPONSAVEL       VARCHAR2(10 BYTE)        NOT NULL,
  DS_EVENTO            VARCHAR2(4000 BYTE)      NOT NULL,
  NR_SEQ_ASSUNTO       NUMBER(10),
  DT_CONCLUSAO         DATE,
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  NR_SEQ_TIPO_USUARIO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELPFIN_PESFISI_FK_I ON TASY.RELAC_PF_INSTITUICAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELPFIN_PESFISI_FK2_I ON TASY.RELAC_PF_INSTITUICAO
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELPFIN_PK ON TASY.RELAC_PF_INSTITUICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELPFIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.RELPFIN_TIASREP_FK_I ON TASY.RELAC_PF_INSTITUICAO
(NR_SEQ_ASSUNTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELPFIN_TIASREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELPFIN_TPUSURE_FK_I ON TASY.RELAC_PF_INSTITUICAO
(NR_SEQ_TIPO_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELPFIN_TPUSURE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.relac_pf_inst_befupdate_log
before update ON TASY.RELAC_PF_INSTITUICAO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into relac_pf_instituicao_log(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		dt_registro,
		cd_responsavel,
		ds_evento,
		nr_seq_assunto,
		dt_conclusao,
		ds_arquivo,
		nr_seq_tipo_usuario)
	values( relac_pf_instituicao_log_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.cd_pessoa_fisica,
		:old.dt_registro,
		:old.cd_responsavel,
		:old.ds_evento,
		:old.nr_seq_assunto,
		:old.dt_conclusao,
		:old.ds_arquivo,
		:old.nr_seq_tipo_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.RELAC_PF_INSTITUICAO_tp  after update ON TASY.RELAC_PF_INSTITUICAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_RESPONSAVEL,1,4000),substr(:new.CD_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'CD_RESPONSAVEL',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EVENTO,1,4000),substr(:new.DS_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_EVENTO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_USUARIO,1,4000),substr(:new.NR_SEQ_TIPO_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_USUARIO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ASSUNTO,1,4000),substr(:new.NR_SEQ_ASSUNTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ASSUNTO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CONCLUSAO,1,4000),substr(:new.DT_CONCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CONCLUSAO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REGISTRO,1,4000),substr(:new.DT_REGISTRO,1,4000),:new.nm_usuario,nr_seq_w,'DT_REGISTRO',ie_log_w,ds_w,'RELAC_PF_INSTITUICAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RELAC_PF_INSTITUICAO ADD (
  CONSTRAINT RELPFIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELAC_PF_INSTITUICAO ADD (
  CONSTRAINT RELPFIN_PESFISI_FK2 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RELPFIN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RELPFIN_TIASREP_FK 
 FOREIGN KEY (NR_SEQ_ASSUNTO) 
 REFERENCES TASY.TIPO_ASSUNTO_RELAC_PF (NR_SEQUENCIA),
  CONSTRAINT RELPFIN_TPUSURE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_USUARIO) 
 REFERENCES TASY.TIPO_USUARIO_RELAC_PF (NR_SEQUENCIA));

GRANT SELECT ON TASY.RELAC_PF_INSTITUICAO TO NIVEL_1;

