ALTER TABLE TASY.PLS_GRUPO_PLANO_ONLINE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRUPO_PLANO_ONLINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRUPO_PLANO_ONLINE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_PLANO             VARCHAR2(255 BYTE)       NOT NULL,
  DS_PLANO             VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_GRUPO_PLANO       LONG,
  IE_TIPO_OPERACAO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSGPON_PK ON TASY.PLS_GRUPO_PLANO_ONLINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_grupo_plano_online_atual
before update ON TASY.PLS_GRUPO_PLANO_ONLINE for each row
declare

qt_nr_seq_grupo_plano_w	number(10);

begin

if	(:old.ie_tipo_operacao <> :new.ie_tipo_operacao) then
	select	count(1)
	into	qt_nr_seq_grupo_plano_w
	from	pls_plano_online	a
	where	a.nr_seq_grupo_plano	= :new.nr_sequencia;

	if	(qt_nr_seq_grupo_plano_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1084160); --N�o � poss�vel realizar essa opera��o, pois h� produtos vinculados a esse grupo
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_GRUPO_PLANO_ONLINE ADD (
  CONSTRAINT PLSGPON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_GRUPO_PLANO_ONLINE TO NIVEL_1;

