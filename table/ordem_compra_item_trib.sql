ALTER TABLE TASY.ORDEM_COMPRA_ITEM_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ITEM_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ITEM_TRIB
(
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  NR_ITEM_OCI          NUMBER(5)                NOT NULL,
  CD_TRIBUTO           NUMBER(5)                NOT NULL,
  PR_TRIBUTO           NUMBER(7,4)              NOT NULL,
  VL_TRIBUTO           NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_CORPO_ITEM        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCOITR_ORCOITE_FK_I ON TASY.ORDEM_COMPRA_ITEM_TRIB
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCOITR_PK ON TASY.ORDEM_COMPRA_ITEM_TRIB
(NR_ORDEM_COMPRA, NR_ITEM_OCI, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOITR_TRIBUTO_FK_I ON TASY.ORDEM_COMPRA_ITEM_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTPD_ORDEM_COMPRA_ITEM_TR
before insert or update or delete ON TASY.ORDEM_COMPRA_ITEM_TRIB 
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_registros_w			number(10);
dt_aprovacao_w			ordem_compra.dt_aprovacao%type;

pragma autonomous_transaction;
begin

reg_integracao_p.cd_estab_documento	:= null;
reg_integracao_p.ie_tipo_ordem		:= null;
reg_integracao_p.nr_seq_tipo_compra	:= null;
reg_integracao_p.nr_seq_mod_compra	:= null;
reg_integracao_p.ds_id_origin		:= null;

select 	max(dt_aprovacao) 
into	dt_aprovacao_w
from 	ordem_compra 
where 	nr_ordem_compra = :new.nr_ordem_compra;

if	(inserting or updating) and
	(dt_aprovacao_w is null) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :new.nr_ordem_compra
	and	ie_evento = '2'
	and	nvl(ie_status,'P') not in ('P','R')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then	
		reg_integracao_p.ie_operacao		:= 'A';			
		gerar_int_padrao.gravar_integracao('2',:new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);
	end if;

elsif	(deleting) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :old.nr_ordem_compra
	and	ie_evento = '2'
	and	ie_status in ('S')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;	

	if	(qt_registros_w > 0) then	
		reg_integracao_p.ie_operacao		:= 'E';
		gerar_int_padrao.gravar_integracao('2',:old.nr_ordem_compra,:old.nm_usuario, reg_integracao_p);
	end if;
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ITEM_TRIB_tp  after update ON TASY.ORDEM_COMPRA_ITEM_TRIB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ORDEM_COMPRA='||to_char(:old.NR_ORDEM_COMPRA)||'#@#@NR_ITEM_OCI='||to_char(:old.NR_ITEM_OCI)||'#@#@CD_TRIBUTO='||to_char(:old.CD_TRIBUTO);gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_OCI,1,4000),substr(:new.NR_ITEM_OCI,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_OCI',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TRIBUTO,1,4000),substr(:new.CD_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TRIBUTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_TRIBUTO,1,4000),substr(:new.VL_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_TRIBUTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CORPO_ITEM,1,4000),substr(:new.IE_CORPO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CORPO_ITEM',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_TRIBUTO,1,4000),substr(:new.PR_TRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'PR_TRIBUTO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ITEM_TRIB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORDEM_COMPRA_ITEM_TRIB ADD (
  CONSTRAINT ORCOITR_PK
 PRIMARY KEY
 (NR_ORDEM_COMPRA, NR_ITEM_OCI, CD_TRIBUTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ITEM_TRIB ADD (
  CONSTRAINT ORCOITR_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT ORCOITR_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_ITEM_TRIB TO NIVEL_1;

