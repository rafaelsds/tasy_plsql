ALTER TABLE TASY.QT_AGENDA_ALTERA_STATUS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_AGENDA_ALTERA_STATUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_AGENDA_ALTERA_STATUS
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  QT_HORAS_RETRO          NUMBER(5)             NOT NULL,
  IE_STATUS_AGENDA_ATUAL  VARCHAR2(3 BYTE)      NOT NULL,
  IE_NOVO_STATUS          VARCHAR2(3 BYTE)      NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_LOCAL            NUMBER(10),
  IE_FORMA_CONS_PERIODO   VARCHAR2(3 BYTE),
  NR_SEQ_AGRUPAMENTO      NUMBER(10),
  DT_INICIAL              DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AQTALTS_AGRUAGE_FK_I ON TASY.QT_AGENDA_ALTERA_STATUS
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AQTALTS_ESTABEL_FK_I ON TASY.QT_AGENDA_ALTERA_STATUS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AQTALTS_PK ON TASY.QT_AGENDA_ALTERA_STATUS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AQTALTS_QTLOCAL_FK_I ON TASY.QT_AGENDA_ALTERA_STATUS
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AQTALTS_SETATEN_FK_I ON TASY.QT_AGENDA_ALTERA_STATUS
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QT_AGENDA_ALTERA_STATUS ADD (
  CONSTRAINT AQTALTS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QT_AGENDA_ALTERA_STATUS ADD (
  CONSTRAINT AQTALTS_AGRUAGE_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.AGRUPAMENTO_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AQTALTS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AQTALTS_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT AQTALTS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.QT_AGENDA_ALTERA_STATUS TO NIVEL_1;

