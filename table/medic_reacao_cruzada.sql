ALTER TABLE TASY.MEDIC_REACAO_CRUZADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIC_REACAO_CRUZADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIC_REACAO_CRUZADA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_FICHA_TECNICA  NUMBER(10)              NOT NULL,
  CD_MATERIAL           NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDREAC_MATERIA_FK_I ON TASY.MEDIC_REACAO_CRUZADA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDREAC_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDREAC_MEDFITE_FK_I ON TASY.MEDIC_REACAO_CRUZADA
(NR_SEQ_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDREAC_MEDFITE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDREAC_PK ON TASY.MEDIC_REACAO_CRUZADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MEDIC_REACAO_CRUZADA ADD (
  CONSTRAINT MEDREAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDIC_REACAO_CRUZADA ADD (
  CONSTRAINT MEDREAC_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MEDREAC_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MEDIC_REACAO_CRUZADA TO NIVEL_1;

