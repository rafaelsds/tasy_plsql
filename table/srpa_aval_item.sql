ALTER TABLE TASY.SRPA_AVAL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SRPA_AVAL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SRPA_AVAL_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_AVALIACAO     NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_RESULT        NUMBER(10),
  QT_RESULT            NUMBER(15)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SRPAAVI_ATEAVSR_FK_I ON TASY.SRPA_AVAL_ITEM
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SRPAAVI_ATEAVSR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SRPAAVI_PK ON TASY.SRPA_AVAL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SRPAAVI_SRPAITE_FK_I ON TASY.SRPA_AVAL_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SRPAAVI_SRPAITE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SRPA_AVAL_ITEM_ATUAL
BEFORE INSERT OR UPDATE ON TASY.SRPA_AVAL_ITEM FOR EACH ROW
DECLARE

qt_peso_w	number(15,0);

BEGIN

if	(:new.nr_seq_result is not null) then
	select	qt_peso
	into	:new.qt_result
	from	srpa_item_result
	where	nr_sequencia	= :new.nr_seq_result;
else
	:new.qt_result	:= 0;
end if;

END;
/


ALTER TABLE TASY.SRPA_AVAL_ITEM ADD (
  CONSTRAINT SRPAAVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SRPA_AVAL_ITEM ADD (
  CONSTRAINT SRPAAVI_ATEAVSR_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.ATEND_AVAL_SRPA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SRPAAVI_SRPAITE_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.SRPA_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.SRPA_AVAL_ITEM TO NIVEL_1;

