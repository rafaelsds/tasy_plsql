ALTER TABLE TASY.TISS_CAMPO_ESP_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_CAMPO_ESP_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_CAMPO_ESP_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  DS_CAMPO             VARCHAR2(255 BYTE)       NOT NULL,
  VL_ORIGEM            VARCHAR2(100 BYTE)       NOT NULL,
  VL_CONVERSAO         VARCHAR2(100 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TISSCES_PK ON TASY.TISS_CAMPO_ESP_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCES_TISSRCE_FK_I ON TASY.TISS_CAMPO_ESP_CONV
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCES_TISSRCE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TISS_CAMPO_ESP_CONV_tp  after update ON TASY.TISS_CAMPO_ESP_CONV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_CAMPO,1,500);gravar_log_alteracao(substr(:old.DS_CAMPO,1,4000),substr(:new.DS_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMPO',ie_log_w,ds_w,'TISS_CAMPO_ESP_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_CONVERSAO,1,500);gravar_log_alteracao(substr(:old.VL_CONVERSAO,1,4000),substr(:new.VL_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'VL_CONVERSAO',ie_log_w,ds_w,'TISS_CAMPO_ESP_CONV',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_ORIGEM,1,500);gravar_log_alteracao(substr(:old.VL_ORIGEM,1,4000),substr(:new.VL_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_ORIGEM',ie_log_w,ds_w,'TISS_CAMPO_ESP_CONV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_CAMPO_ESP_CONV ADD (
  CONSTRAINT TISSCES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_CAMPO_ESP_CONV ADD (
  CONSTRAINT TISSCES_TISSRCE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.TISS_REGRA_CAMPO_ESP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TISS_CAMPO_ESP_CONV TO NIVEL_1;

