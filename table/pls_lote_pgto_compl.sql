ALTER TABLE TASY.PLS_LOTE_PGTO_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_PGTO_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_PGTO_COMPL
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_LOTE               NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR          NUMBER(10),
  NR_SEQ_CONTA              NUMBER(10),
  NR_SEQ_PROTOCOLO          NUMBER(10),
  IE_TIPO_GUIA              VARCHAR2(2 BYTE),
  IE_TIPO_DESPESA_PROC      VARCHAR2(1 BYTE),
  IE_TIPO_DESPESA_MAT       VARCHAR2(1 BYTE),
  NR_PROTOCOLO_PRESTADOR    VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_PRESTADOR     NUMBER(10),
  NR_SEQ_CLASSIF_PRESTADOR  NUMBER(10),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_PLANO              NUMBER(10),
  NR_SEQ_GRUPO_CONTRATO     NUMBER(10),
  NR_SEQ_EVENTO             NUMBER(10),
  IE_CONTA_INTERCAMBIO      VARCHAR2(1 BYTE),
  IE_ORIGEM_CONTA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLPGC_PK ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSCLPR_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_CLASSIF_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSCOME_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSCONT_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSEVEN_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSLOPA_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSLOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSPLAN_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSPRCO_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSPRCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSPRES_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSPRGC_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPGC_PLSTIPR_FK_I ON TASY.PLS_LOTE_PGTO_COMPL
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPGC_PLSTIPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_PGTO_COMPL ADD (
  CONSTRAINT PLSLPGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_PGTO_COMPL ADD (
  CONSTRAINT PLSLPGC_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSLOPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSLPGC_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PRESTADOR) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSLPGC_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_PGTO_COMPL TO NIVEL_1;

