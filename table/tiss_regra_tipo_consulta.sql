ALTER TABLE TASY.TISS_REGRA_TIPO_CONSULTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_TIPO_CONSULTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_TIPO_CONSULTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_CONSULTA_TISS  NUMBER(3)              NOT NULL,
  NR_SEQ_CLASSIF_ATEND   NUMBER(10),
  CD_CONVENIO            NUMBER(5),
  IE_ATEND_RETORNO       VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRTC_CLAATEN_FK_I ON TASY.TISS_REGRA_TIPO_CONSULTA
(NR_SEQ_CLASSIF_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRTC_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRTC_CONVENI_FK_I ON TASY.TISS_REGRA_TIPO_CONSULTA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRTC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRTC_ESTABEL_FK_I ON TASY.TISS_REGRA_TIPO_CONSULTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSRTC_PK ON TASY.TISS_REGRA_TIPO_CONSULTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRTC_PK
  MONITORING USAGE;


ALTER TABLE TASY.TISS_REGRA_TIPO_CONSULTA ADD (
  CONSTRAINT TISSRTC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_TIPO_CONSULTA ADD (
  CONSTRAINT TISSRTC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSRTC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRTC_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ATEND) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TISS_REGRA_TIPO_CONSULTA TO NIVEL_1;

