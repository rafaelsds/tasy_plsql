ALTER TABLE TASY.MDR_SP_RULES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MDR_SP_RULES CASCADE CONSTRAINTS;

CREATE TABLE TASY.MDR_SP_RULES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SEVERITY      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MDRSPRL_MANSEVE_FK_I ON TASY.MDR_SP_RULES
(NR_SEQ_SEVERITY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MDRSPRL_PK ON TASY.MDR_SP_RULES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MDR_SP_RULES ADD (
  CONSTRAINT MDRSPRL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MDR_SP_RULES ADD (
  CONSTRAINT MDRSPRL_MANSEVE_FK 
 FOREIGN KEY (NR_SEQ_SEVERITY) 
 REFERENCES TASY.MAN_SEVERIDADE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MDR_SP_RULES TO NIVEL_1;

