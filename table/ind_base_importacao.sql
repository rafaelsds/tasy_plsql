ALTER TABLE TASY.IND_BASE_IMPORTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IND_BASE_IMPORTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.IND_BASE_IMPORTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_INDICADOR     NUMBER(10)               NOT NULL,
  DT_REFERENCIA        DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_DIMENSAO          VARCHAR2(255 BYTE),
  DS_DIMENSAO_VALOR    VARCHAR2(255 BYTE),
  DS_INFORMACAO        VARCHAR2(255 BYTE),
  DS_INFORMACAO_VALOR  VARCHAR2(255 BYTE),
  QT_VALOR             NUMBER(22,4),
  NR_SEQ_DIMENSAO      NUMBER(10),
  NR_SEQ_INFORMACAO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDBASIMP_INDBASE_FK_I ON TASY.IND_BASE_IMPORTACAO
(NR_SEQ_INDICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDBASIMP_INDDIM_FK_I ON TASY.IND_BASE_IMPORTACAO
(NR_SEQ_DIMENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDBASIMP_INDINF_FK_I ON TASY.IND_BASE_IMPORTACAO
(NR_SEQ_INFORMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDBASIMP_PK ON TASY.IND_BASE_IMPORTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IND_BASE_IMPORTACAO ADD (
  CONSTRAINT INDBASIMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IND_BASE_IMPORTACAO ADD (
  CONSTRAINT INDBASIMP_INDBASE_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.IND_BASE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INDBASIMP_INDDIM_FK 
 FOREIGN KEY (NR_SEQ_DIMENSAO) 
 REFERENCES TASY.IND_DIMENSAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INDBASIMP_INDINF_FK 
 FOREIGN KEY (NR_SEQ_INFORMACAO) 
 REFERENCES TASY.IND_INFORMACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IND_BASE_IMPORTACAO TO NIVEL_1;

