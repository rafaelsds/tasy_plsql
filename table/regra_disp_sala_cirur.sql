ALTER TABLE TASY.REGRA_DISP_SALA_CIRUR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DISP_SALA_CIRUR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DISP_SALA_CIRUR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_INTERNO       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_DIA_SEMANA        NUMBER(1)                NOT NULL,
  QT_MINUTOS           NUMBER(4)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REDISAC_PK ON TASY.REGRA_DISP_SALA_CIRUR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REDISAC_UNIATEN_FK_I ON TASY.REGRA_DISP_SALA_CIRUR
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REDISAC_UNIATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_DISP_SALA_CIRUR ADD (
  CONSTRAINT REDISAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DISP_SALA_CIRUR ADD (
  CONSTRAINT REDISAC_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO));

GRANT SELECT ON TASY.REGRA_DISP_SALA_CIRUR TO NIVEL_1;

