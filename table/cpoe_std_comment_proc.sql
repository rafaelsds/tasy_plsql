ALTER TABLE TASY.CPOE_STD_COMMENT_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_STD_COMMENT_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_STD_COMMENT_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_STD_COMMENT   NUMBER(10),
  NR_SEQ_ESTRUTURA     NUMBER(10),
  NR_SEQ_PROC_INTERNO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOESCP_CPOESCO_FK_I ON TASY.CPOE_STD_COMMENT_PROC
(NR_SEQ_STD_COMMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOESCP_PIESTTR_FK_I ON TASY.CPOE_STD_COMMENT_PROC
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOESCP_PK ON TASY.CPOE_STD_COMMENT_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOESCP_PROINTE_FK_I ON TASY.CPOE_STD_COMMENT_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CPOE_STD_COMMENT_PROC_tp  after update ON TASY.CPOE_STD_COMMENT_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'CPOE_STD_COMMENT_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CPOE_STD_COMMENT_PROC ADD (
  CONSTRAINT CPOESCP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_STD_COMMENT_PROC ADD (
  CONSTRAINT CPOESCP_CPOESCO_FK 
 FOREIGN KEY (NR_SEQ_STD_COMMENT) 
 REFERENCES TASY.CPOE_STD_COMMENT (NR_SEQUENCIA),
  CONSTRAINT CPOESCP_PIESTTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PI_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT CPOESCP_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CPOE_STD_COMMENT_PROC TO NIVEL_1;

