ALTER TABLE TASY.LAB_REGRA_DESC_ANTIBIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_REGRA_DESC_ANTIBIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_REGRA_DESC_ANTIBIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_VALOR             VARCHAR2(30 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_VALOR             VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LRDA_IE_VALOR_UK ON TASY.LAB_REGRA_DESC_ANTIBIO
(IE_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LRDA_PK ON TASY.LAB_REGRA_DESC_ANTIBIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAB_REGRA_DESC_ANTIBIO ADD (
  CONSTRAINT LRDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT LRDA_IE_VALOR_UK
 UNIQUE (IE_VALOR));

GRANT SELECT ON TASY.LAB_REGRA_DESC_ANTIBIO TO NIVEL_1;

