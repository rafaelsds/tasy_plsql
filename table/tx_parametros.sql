ALTER TABLE TASY.TX_PARAMETROS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_PARAMETROS CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_PARAMETROS
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  NR_SEQ_EXAME_HLA               NUMBER(10)     NOT NULL,
  NR_SEQ_EXAME_PAINEL            NUMBER(10)     NOT NULL,
  NR_SEQ_EXAME_CROSSMATCH        NUMBER(10)     NOT NULL,
  IE_TIPO_RECEITA                VARCHAR2(15 BYTE),
  NR_SEQ_MODELO                  NUMBER(10),
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_PERFIL_SNT                  NUMBER(5),
  CD_PERFIL                      NUMBER(5),
  CD_MEDICO_RESP                 VARCHAR2(10 BYTE),
  CD_PERFIL_EXAME_VENCIDO        NUMBER(5),
  CD_PERFIL_ATEND                NUMBER(5),
  IE_GERAR_PRESCR_SEM_ATEND      VARCHAR2(1 BYTE),
  CD_MOTIVO_ALTA_AUTOMATICA      NUMBER(3),
  CD_PERFIL_RECREDENC            NUMBER(5),
  NR_SEQ_EXAME_MELD              NUMBER(10),
  CD_PERFIL_PRIOR                NUMBER(5),
  NR_SEQ_MOTIVO_FIM_TRATAMENTO   NUMBER(10),
  NR_SEQ_MOTIVO_FIM_TRAT_TRANSP  NUMBER(10),
  CD_PERFIL_PACIENTE             NUMBER(5),
  CD_PERFIL_PACIENTE_FIM         NUMBER(5),
  CD_PERFIL_SOLIC_EXAME          NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TXPARAM_ESTABEL_FK_I ON TASY.TX_PARAMETROS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXPARAM_EXALABO_FK_I ON TASY.TX_PARAMETROS
(NR_SEQ_EXAME_HLA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_EXALABO_FK2_I ON TASY.TX_PARAMETROS
(NR_SEQ_EXAME_PAINEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_EXALABO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_EXALABO_FK3_I ON TASY.TX_PARAMETROS
(NR_SEQ_EXAME_CROSSMATCH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_EXALABO_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_EXALABO_FK4_I ON TASY.TX_PARAMETROS
(NR_SEQ_EXAME_MELD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_EXALABO_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_MEDICO_FK_I ON TASY.TX_PARAMETROS
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXPARAM_MEMAREC_FK_I ON TASY.TX_PARAMETROS
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_MEMAREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_MOTALTA_FK_I ON TASY.TX_PARAMETROS
(CD_MOTIVO_ALTA_AUTOMATICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_MOTALTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_MOTIFIM_FK_I ON TASY.TX_PARAMETROS
(NR_SEQ_MOTIVO_FIM_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_MOTIFIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_MOTIFIM_FK2_I ON TASY.TX_PARAMETROS
(NR_SEQ_MOTIVO_FIM_TRAT_TRANSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_MOTIFIM_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK_I ON TASY.TX_PARAMETROS
(CD_PERFIL_SNT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK2_I ON TASY.TX_PARAMETROS
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK3_I ON TASY.TX_PARAMETROS
(CD_PERFIL_EXAME_VENCIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK4_I ON TASY.TX_PARAMETROS
(CD_PERFIL_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK5_I ON TASY.TX_PARAMETROS
(CD_PERFIL_RECREDENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK6_I ON TASY.TX_PARAMETROS
(CD_PERFIL_PRIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK7_I ON TASY.TX_PARAMETROS
(CD_PERFIL_PACIENTE_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK8_I ON TASY.TX_PARAMETROS
(CD_PERFIL_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK8_I
  MONITORING USAGE;


CREATE INDEX TASY.TXPARAM_PERFIL_FK9_I ON TASY.TX_PARAMETROS
(CD_PERFIL_SOLIC_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PERFIL_FK9_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXPARAM_PK ON TASY.TX_PARAMETROS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXPARAM_PK
  MONITORING USAGE;


ALTER TABLE TASY.TX_PARAMETROS ADD (
  CONSTRAINT TXPARAM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_PARAMETROS ADD (
  CONSTRAINT TXPARAM_MEMAREC_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MEDIC_MASCARA_RECEITA (NR_SEQUENCIA),
  CONSTRAINT TXPARAM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TXPARAM_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME_HLA) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT TXPARAM_EXALABO_FK2 
 FOREIGN KEY (NR_SEQ_EXAME_PAINEL) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT TXPARAM_EXALABO_FK3 
 FOREIGN KEY (NR_SEQ_EXAME_CROSSMATCH) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT TXPARAM_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_SNT) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT TXPARAM_PERFIL_FK2 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_PERFIL_FK3 
 FOREIGN KEY (CD_PERFIL_EXAME_VENCIDO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_MOTALTA_FK 
 FOREIGN KEY (CD_MOTIVO_ALTA_AUTOMATICA) 
 REFERENCES TASY.MOTIVO_ALTA (CD_MOTIVO_ALTA),
  CONSTRAINT TXPARAM_PERFIL_FK4 
 FOREIGN KEY (CD_PERFIL_ATEND) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_PERFIL_FK5 
 FOREIGN KEY (CD_PERFIL_RECREDENC) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_EXALABO_FK4 
 FOREIGN KEY (NR_SEQ_EXAME_MELD) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT TXPARAM_PERFIL_FK6 
 FOREIGN KEY (CD_PERFIL_PRIOR) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_MOTIFIM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM_TRATAMENTO) 
 REFERENCES TASY.MOTIVO_FIM (NR_SEQUENCIA),
  CONSTRAINT TXPARAM_MOTIFIM_FK2 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM_TRAT_TRANSP) 
 REFERENCES TASY.MOTIVO_FIM (NR_SEQUENCIA),
  CONSTRAINT TXPARAM_PERFIL_FK7 
 FOREIGN KEY (CD_PERFIL_PACIENTE_FIM) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_PERFIL_FK8 
 FOREIGN KEY (CD_PERFIL_PACIENTE) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TXPARAM_PERFIL_FK9 
 FOREIGN KEY (CD_PERFIL_SOLIC_EXAME) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.TX_PARAMETROS TO NIVEL_1;

