ALTER TABLE TASY.ATENDIMENTO_ANOT_ENF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_ANOT_ENF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_ANOT_ENF
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_ANOTACAO            DATE                   NOT NULL,
  IE_PENDENTE            VARCHAR2(1 BYTE)       NOT NULL,
  DS_ANOTACAO            VARCHAR2(4000 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  DS_OBSERVACAO          VARCHAR2(1000 BYTE),
  NR_SEQ_TURNO           NUMBER(10),
  DS_UTC                 VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO       VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO     VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEANEN_ATEPACI_FK_I ON TASY.ATENDIMENTO_ANOT_ENF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANEN_I1 ON TASY.ATENDIMENTO_ANOT_ENF
(DT_ANOTACAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANEN_I1
  MONITORING USAGE;


CREATE INDEX TASY.ATEANEN_I2 ON TASY.ATENDIMENTO_ANOT_ENF
(NR_ATENDIMENTO, IE_PENDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEANEN_PERFIL_FK_I ON TASY.ATENDIMENTO_ANOT_ENF
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANEN_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEANEN_PK ON TASY.ATENDIMENTO_ANOT_ENF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.ATEANEN_REGTUGP_FK_I ON TASY.ATENDIMENTO_ANOT_ENF
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEANEN_REGTUGP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_ANOT_ENF_ATUAL
before insert or update ON TASY.ATENDIMENTO_ANOT_ENF for each row
declare


begin
if	(nvl(:old.DT_ANOTACAO,sysdate+10) <> :new.DT_ANOTACAO) and
	(:new.DT_ANOTACAO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ANOTACAO,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


ALTER TABLE TASY.ATENDIMENTO_ANOT_ENF ADD (
  CONSTRAINT ATEANEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_ANOT_ENF ADD (
  CONSTRAINT ATEANEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEANEN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEANEN_REGTUGP_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.REGRA_TURNO_GP (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_ANOT_ENF TO NIVEL_1;

