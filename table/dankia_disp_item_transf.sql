ALTER TABLE TASY.DANKIA_DISP_ITEM_TRANSF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DANKIA_DISP_ITEM_TRANSF CASCADE CONSTRAINTS;

CREATE TABLE TASY.DANKIA_DISP_ITEM_TRANSF
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_TRANSFERENCIA          VARCHAR2(20 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_MATERIAL               NUMBER(10)          NOT NULL,
  CD_BARRAS                 VARCHAR2(255 BYTE),
  CD_ITEM_TRANSFERENCIA     VARCHAR2(20 BYTE)   NOT NULL,
  IE_OPERACAO               VARCHAR2(1 BYTE),
  CD_LOCAL_ESTOQUE_ORIGEM   NUMBER(4),
  QT_MATERIAL               NUMBER(15,3)        NOT NULL,
  CD_LOCAL_ESTOQUE_DESTINO  NUMBER(4)           NOT NULL,
  DT_LIDO_DANKIA            DATE,
  IE_PROCESSADO             VARCHAR2(1 BYTE)    NOT NULL,
  DS_PROCESSADO_OBSERVACAO  VARCHAR2(400 BYTE),
  DS_STACK                  VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DANDISTRAN_ESTABEL_FK_I ON TASY.DANKIA_DISP_ITEM_TRANSF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANDISTRAN_LOCESTO_FK_I ON TASY.DANKIA_DISP_ITEM_TRANSF
(CD_LOCAL_ESTOQUE_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANDISTRAN_MATERIA_FK_I ON TASY.DANKIA_DISP_ITEM_TRANSF
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DANDISTRAN_PK ON TASY.DANKIA_DISP_ITEM_TRANSF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DANKIA_DISP_ITEM_TRANSF ADD (
  CONSTRAINT DANDISTRAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DANKIA_DISP_ITEM_TRANSF ADD (
  CONSTRAINT DANDISTRAN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DANDISTRAN_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE_DESTINO) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT DANDISTRAN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.DANKIA_DISP_ITEM_TRANSF TO NIVEL_1;

