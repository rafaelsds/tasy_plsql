ALTER TABLE TASY.MED_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_CLIENTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_MEDICO              VARCHAR2(10 BYTE)      NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_PRIMEIRA_CONSULTA   DATE,
  DT_ULTIMA_CONSULTA     DATE,
  DT_ULTIMA_ATUALIZ      DATE,
  CD_PESSOA_SIST_ORIG    VARCHAR2(10 BYTE),
  DT_ULTIMA_VISUALIZ     DATE,
  CD_CONVENIO            NUMBER(5),
  CD_USUARIO_CONVENIO    VARCHAR2(30 BYTE),
  DS_ENCAMINHAMENTO      VARCHAR2(40 BYTE),
  DT_VALIDADE_CARTEIRA   DATE,
  IE_FICHA_PAPEL         VARCHAR2(1 BYTE),
  IE_EXAME_CONSULTORIO   VARCHAR2(1 BYTE),
  IE_EXAME_VIRTUAL       VARCHAR2(1 BYTE),
  CD_CATEGORIA           VARCHAR2(10 BYTE),
  NR_SEQ_PLANO           NUMBER(10),
  NR_SEQ_CLASSIF         NUMBER(10),
  IE_GEMELAR             VARCHAR2(1 BYTE),
  IE_PAIS_SEPARADOS      VARCHAR2(1 BYTE),
  NM_USUARIO_EXPORTACAO  VARCHAR2(15 BYTE),
  IE_EXPORTADO           VARCHAR2(1 BYTE),
  DT_EXPORTACAO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDCLIE_CONVENI_FK_I ON TASY.MED_CLIENTE
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDCLIE_I1 ON TASY.MED_CLIENTE
(CD_PESSOA_SIST_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLIE_I1
  MONITORING USAGE;


CREATE INDEX TASY.MEDCLIE_I2 ON TASY.MED_CLIENTE
(CD_MEDICO, CD_PESSOA_FISICA, CD_PESSOA_SIST_ORIG)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDCLIE_MEDCLPA_FK_I ON TASY.MED_CLIENTE
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLIE_MEDCLPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCLIE_MEDPLAN_FK_I ON TASY.MED_CLIENTE
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLIE_MEDPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCLIE_PESFISI_FK_I ON TASY.MED_CLIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDCLIE_PESFISI_FK2_I ON TASY.MED_CLIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCLIE_PESFISI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDCLIE_PK ON TASY.MED_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MED_CLIENTE ADD (
  CONSTRAINT MEDCLIE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_CLIENTE ADD (
  CONSTRAINT MEDCLIE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT MEDCLIE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT MEDCLIE_MEDPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.MED_PLANO (NR_SEQUENCIA),
  CONSTRAINT MEDCLIE_MEDCLPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.MED_CLASSIF_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDCLIE_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MED_CLIENTE TO NIVEL_1;

