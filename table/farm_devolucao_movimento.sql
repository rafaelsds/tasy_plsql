ALTER TABLE TASY.FARM_DEVOLUCAO_MOVIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FARM_DEVOLUCAO_MOVIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FARM_DEVOLUCAO_MOVIMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA      VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  DT_BAIXA              DATE,
  DT_DEVOLUCAO          DATE                    NOT NULL,
  DT_ENTRADA_UNIDADE    DATE,
  DT_LIBERACAO          DATE,
  NR_DOC_INTERNO        NUMBER(15),
  DT_ENTREGA_FISICA     DATE,
  DT_IMPRESSAO          DATE,
  NM_USUARIO_BAIXA      VARCHAR2(15 BYTE),
  NM_USUARIO_DEVOL      VARCHAR2(15 BYTE),
  NM_USUARIO_ENTREGA    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FARDEVMOV_ATEPACI_FK_I ON TASY.FARM_DEVOLUCAO_MOVIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FARDEVMOV_ATEPACU_FK_I ON TASY.FARM_DEVOLUCAO_MOVIMENTO
(NR_ATENDIMENTO, DT_ENTRADA_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FARDEVMOV_PESFISI_FK_I ON TASY.FARM_DEVOLUCAO_MOVIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FARDEVMOV_PK ON TASY.FARM_DEVOLUCAO_MOVIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FARDEVMOV_SETATEN_FK_I ON TASY.FARM_DEVOLUCAO_MOVIMENTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FARM_DEVOLUCAO_MOVIMENTO ADD (
  CONSTRAINT FARDEVMOV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FARM_DEVOLUCAO_MOVIMENTO ADD (
  CONSTRAINT FARDEVMOV_ATEPACU_FK 
 FOREIGN KEY (NR_ATENDIMENTO, DT_ENTRADA_UNIDADE) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_ATENDIMENTO,DT_ENTRADA_UNIDADE),
  CONSTRAINT FARDEVMOV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FARDEVMOV_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FARDEVMOV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.FARM_DEVOLUCAO_MOVIMENTO TO NIVEL_1;

