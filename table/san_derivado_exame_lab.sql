ALTER TABLE TASY.SAN_DERIVADO_EXAME_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_DERIVADO_EXAME_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_DERIVADO_EXAME_LAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DERIVADO      NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANDEEL_EXALABO_FK_I ON TASY.SAN_DERIVADO_EXAME_LAB
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDEEL_EXALABO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANDEEL_PK ON TASY.SAN_DERIVADO_EXAME_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANDEEL_SANDERI_FK_I ON TASY.SAN_DERIVADO_EXAME_LAB
(NR_SEQ_DERIVADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANDEEL_SANDERI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_DERIVADO_EXAME_LAB ADD (
  CONSTRAINT SANDEEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_DERIVADO_EXAME_LAB ADD (
  CONSTRAINT SANDEEL_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT SANDEEL_SANDERI_FK 
 FOREIGN KEY (NR_SEQ_DERIVADO) 
 REFERENCES TASY.SAN_DERIVADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_DERIVADO_EXAME_LAB TO NIVEL_1;

