ALTER TABLE TASY.TASY_CONFIG_INCONSISTENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_CONFIG_INCONSISTENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_CONFIG_INCONSISTENCIA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NM_INDICE                   VARCHAR2(50 BYTE),
  NM_TABELA                   VARCHAR2(50 BYTE) NOT NULL,
  NM_INTEGRIDADE_REFERENCIAL  VARCHAR2(50 BYTE),
  IE_ACAO_PRIORIDADE1         VARCHAR2(1 BYTE),
  IE_ACAO_PRIORIDADE2         VARCHAR2(1 BYTE),
  IE_ACAO_PRIORIDADE3         VARCHAR2(1 BYTE),
  IE_ACAO_PRIORIDADE4         VARCHAR2(1 BYTE),
  IE_ACOMPANHAMENTO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASCONI_INDICE_FK_I ON TASY.TASY_CONFIG_INCONSISTENCIA
(NM_TABELA, NM_INDICE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASCONI_INTREFE_FK_I ON TASY.TASY_CONFIG_INCONSISTENCIA
(NM_TABELA, NM_INTEGRIDADE_REFERENCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASCONI_PK ON TASY.TASY_CONFIG_INCONSISTENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASCONI_UK ON TASY.TASY_CONFIG_INCONSISTENCIA
(NM_INDICE, NM_TABELA, NM_INTEGRIDADE_REFERENCIAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_CONFIG_INCONSISTENCIA_tp  after update ON TASY.TASY_CONFIG_INCONSISTENCIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_ACOMPANHAMENTO,1,500);gravar_log_alteracao(substr(:old.IE_ACOMPANHAMENTO,1,4000),substr(:new.IE_ACOMPANHAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMPANHAMENTO',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACAO_PRIORIDADE4,1,500);gravar_log_alteracao(substr(:old.IE_ACAO_PRIORIDADE4,1,4000),substr(:new.IE_ACAO_PRIORIDADE4,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_PRIORIDADE4',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_TABELA,1,500);gravar_log_alteracao(substr(:old.NM_TABELA,1,4000),substr(:new.NM_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NM_TABELA',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_INTEGRIDADE_REFERENCIAL,1,500);gravar_log_alteracao(substr(:old.NM_INTEGRIDADE_REFERENCIAL,1,4000),substr(:new.NM_INTEGRIDADE_REFERENCIAL,1,4000),:new.nm_usuario,nr_seq_w,'NM_INTEGRIDADE_REFERENCIAL',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACAO_PRIORIDADE1,1,500);gravar_log_alteracao(substr(:old.IE_ACAO_PRIORIDADE1,1,4000),substr(:new.IE_ACAO_PRIORIDADE1,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_PRIORIDADE1',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACAO_PRIORIDADE2,1,500);gravar_log_alteracao(substr(:old.IE_ACAO_PRIORIDADE2,1,4000),substr(:new.IE_ACAO_PRIORIDADE2,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_PRIORIDADE2',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACAO_PRIORIDADE3,1,500);gravar_log_alteracao(substr(:old.IE_ACAO_PRIORIDADE3,1,4000),substr(:new.IE_ACAO_PRIORIDADE3,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_PRIORIDADE3',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_INDICE,1,500);gravar_log_alteracao(substr(:old.NM_INDICE,1,4000),substr(:new.NM_INDICE,1,4000),:new.nm_usuario,nr_seq_w,'NM_INDICE',ie_log_w,ds_w,'TASY_CONFIG_INCONSISTENCIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TASY_CONFIG_INCONSISTENCIA ADD (
  CONSTRAINT TASCONI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TASCONI_UK
 UNIQUE (NM_INDICE, NM_TABELA, NM_INTEGRIDADE_REFERENCIAL)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_CONFIG_INCONSISTENCIA ADD (
  CONSTRAINT TASCONI_INDICE_FK 
 FOREIGN KEY (NM_TABELA, NM_INDICE) 
 REFERENCES TASY.INDICE (NM_TABELA,NM_INDICE)
    ON DELETE CASCADE,
  CONSTRAINT TASCONI_INTREFE_FK 
 FOREIGN KEY (NM_TABELA, NM_INTEGRIDADE_REFERENCIAL) 
 REFERENCES TASY.INTEGRIDADE_REFERENCIAL (NM_TABELA,NM_INTEGRIDADE_REFERENCIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TASY_CONFIG_INCONSISTENCIA TO NIVEL_1;

