ALTER TABLE TASY.PLS_REGRA_RESC_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_RESC_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_RESC_USUARIO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_RESC_CONTRATO        NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PERFIL                   NUMBER(5),
  NM_USUARIO_PARAM            VARCHAR2(15 BYTE),
  IE_RESTRINGE_RESC_CONTRATO  VARCHAR2(1 BYTE),
  IE_RESTRINGE_RESC_BENEF     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREREUSU_PERFIL_FK_I ON TASY.PLS_REGRA_RESC_USUARIO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREREUSU_PK ON TASY.PLS_REGRA_RESC_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREREUSU_PLSREGRCO_FK_I ON TASY.PLS_REGRA_RESC_USUARIO
(NR_SEQ_RESC_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_RESC_USUARIO ADD (
  CONSTRAINT PLSREREUSU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_RESC_USUARIO ADD (
  CONSTRAINT PLSREREUSU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PLSREREUSU_PLSREGRCO_FK 
 FOREIGN KEY (NR_SEQ_RESC_CONTRATO) 
 REFERENCES TASY.PLS_REGRA_RESC_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_RESC_USUARIO TO NIVEL_1;

