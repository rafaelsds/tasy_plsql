ALTER TABLE TASY.PLS_SOLICITACAO_RESCISAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLICITACAO_RESCISAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLICITACAO_RESCISAO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_SOLICITACAO                DATE            NOT NULL,
  IE_ORIGEM_SOLICITACAO         VARCHAR2(2 BYTE) NOT NULL,
  IE_STATUS                     NUMBER(2)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  DT_LIBERACAO                  DATE,
  NR_SEQ_CONTRATO               NUMBER(10),
  NR_CONTRATO                   NUMBER(10),
  NR_SEQ_ATENDIMENTO            NUMBER(10),
  NR_SEQ_PROTOCOLO_ATEND        NUMBER(10),
  NR_SEQ_MOTIVO_REJEICAO        NUMBER(10),
  DT_RESCISAO                   DATE,
  IE_TIPO_CONTRATACAO           VARCHAR2(2 BYTE),
  IE_RESCINDIR_CONTRATO         VARCHAR2(1 BYTE),
  IE_FORMA_PEDIDO               NUMBER(10),
  DS_OBSERVACAO                 VARCHAR2(4000 BYTE),
  NM_USUARIO_LIBERACAO          VARCHAR2(15 BYTE),
  IE_FORMA_COBRANCA             NUMBER(2),
  NM_PESSOA_DEVOLUCAO           VARCHAR2(255 BYTE),
  NR_CPF_DEVOLUCAO              VARCHAR2(11 BYTE),
  CD_BANCO                      NUMBER(5),
  CD_AGENCIA_BANCARIA           VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA             VARCHAR2(1 BYTE),
  CD_CONTA                      VARCHAR2(20 BYTE),
  IE_DIGITO_CONTA               VARCHAR2(1 BYTE),
  CD_PF_DEVOLUCAO               VARCHAR2(10 BYTE),
  DS_OBSERVACAO_REJEICAO        VARCHAR2(255 BYTE) DEFAULT null,
  CD_ESTABELECIMENTO            NUMBER(4),
  NR_SEQ_MOV_BENEF              NUMBER(10),
  DS_JUSTIFICATIVA_DEV_PRESENC  VARCHAR2(2000 BYTE),
  CD_CGC_DEVOLUCAO              VARCHAR2(14 BYTE),
  ID_APP_EXTERNO                NUMBER(10),
  IE_APP_EXTERNO                VARCHAR2(2 BYTE),
  NR_SEQ_NOTIFIC_PAGADOR        NUMBER(10),
  DT_EFETIVACAO                 DATE,
  NM_USUARIO_EFETIVACAO         VARCHAR2(15 BYTE),
  DT_CIENCIA                    DATE,
  NR_SEQ_INTERCAMBIO            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSRES_BANCO_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_ESTABEL_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PESFISI_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(CD_PF_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PESJURI_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(CD_CGC_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSRES_PK ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSATEN_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSCONT_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSINBE_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_MOV_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSINCA_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSMRJR_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_MOTIVO_REJEICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSNOPA_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_NOTIFIC_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRES_PLSPROT_FK_I ON TASY.PLS_SOLICITACAO_RESCISAO
(NR_SEQ_PROTOCOLO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_solicitacao_rescisao_ins
before insert or update ON TASY.PLS_SOLICITACAO_RESCISAO for each row
declare
qt_registro_w		pls_integer;
nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(updating) and
		(:new.nr_contrato <> :old.nr_contrato) then
		select	count(1)
		into	qt_registro_w
		from	pls_solic_rescisao_benef
		where	nr_seq_solicitacao	= :new.nr_sequencia;

		if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(783818); --Ja foram selecionados os beneficiarios. Nao sera possivel alterar o contrato!
		end if;
	end if;

	--Atualizar o contrato
	if	(:new.nr_contrato is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato = :new.nr_contrato;


		if	(nr_seq_contrato_w is not null) then
			:new.nr_seq_contrato := nr_seq_contrato_w;
		else
			wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado nao existe. Verifique!
		end if;
	else
		:new.nr_seq_contrato	:= null;
	end if;

	if	(:new.nr_seq_contrato is not null) then
		:new.ie_tipo_contratacao := pls_obter_dados_contrato(:new.nr_seq_contrato,'TC');
	else
		:new.ie_tipo_contratacao := pls_obter_dados_intercambio(:new.nr_seq_intercambio,'TC');
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_solicitacao_rescisao_befin
before insert or update ON TASY.PLS_SOLICITACAO_RESCISAO for each row
declare

ie_tipo_contratacao_permitid_w		varchar2(2);
ie_exibir_mensagem_w			varchar2(1);

begin

ie_tipo_contratacao_permitid_w	:= nvl(obter_valor_param_usuario(268, 17, Obter_Perfil_Ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'T');
ie_exibir_mensagem_w	:= 'N';

if	(ie_tipo_contratacao_permitid_w <> 'T') then
	if	(ie_tipo_contratacao_permitid_w = 'C') then
		if	(:new.ie_tipo_contratacao not in ('CE', 'CA')) then
			ie_exibir_mensagem_w	:= 'S';
		end if;
	elsif	(ie_tipo_contratacao_permitid_w <> :new.ie_tipo_contratacao) then
		ie_exibir_mensagem_w	:= 'S';
	end if;

	if	(ie_exibir_mensagem_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(1122527, 'DS_TIPO_CONTRATACAO='|| obter_valor_dominio(1666,:new.ie_tipo_contratacao));
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_SOLICITACAO_RESCISAO ADD (
  CONSTRAINT PLSSRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SOLICITACAO_RESCISAO ADD (
  CONSTRAINT PLSSRES_PESJURI_FK 
 FOREIGN KEY (CD_CGC_DEVOLUCAO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSSRES_PLSNOPA_FK 
 FOREIGN KEY (NR_SEQ_NOTIFIC_PAGADOR) 
 REFERENCES TASY.PLS_NOTIFICACAO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSMRJR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REJEICAO) 
 REFERENCES TASY.PLS_MOTIVO_REJEICAO_RESC (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSPROT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO_ATEND) 
 REFERENCES TASY.PLS_PROTOCOLO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_PLSINBE_FK 
 FOREIGN KEY (NR_SEQ_MOV_BENEF) 
 REFERENCES TASY.PLS_INCLUSAO_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSSRES_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT PLSSRES_PESFISI_FK 
 FOREIGN KEY (CD_PF_DEVOLUCAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSSRES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT UPDATE ON TASY.PLS_SOLICITACAO_RESCISAO TO DWONATAM;

GRANT SELECT ON TASY.PLS_SOLICITACAO_RESCISAO TO NIVEL_1;

GRANT UPDATE ON TASY.PLS_SOLICITACAO_RESCISAO TO WESLLEN;

