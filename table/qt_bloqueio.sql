ALTER TABLE TASY.QT_BLOQUEIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_BLOQUEIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_BLOQUEIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIAL           DATE                     NOT NULL,
  DT_FINAL             DATE                     NOT NULL,
  HR_INICIO_BLOQUEIO   DATE,
  HR_FINAL_BLOQUEIO    DATE,
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  DT_DIA_SEMANA        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QTBLOQ_I1 ON TASY.QT_BLOQUEIO
(NR_SEQ_LOCAL, DT_INICIAL, DT_FINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QTBLOQ_PK ON TASY.QT_BLOQUEIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLOQ_PK
  MONITORING USAGE;


CREATE INDEX TASY.QTBLOQ_QTLOCAL_FK_I ON TASY.QT_BLOQUEIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLOQ_QTLOCAL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.qt_bloqueio_Delete
before delete ON TASY.QT_BLOQUEIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(sysdate)
and	(nr_seq_local	 = :old.nr_seq_local or :old.nr_seq_local is null);

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.qt_bloqueio_Atual
before insert or update ON TASY.QT_BLOQUEIO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
if	(:new.dt_final < :new.dt_inicial) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262398);
end if;

begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(sysdate)
and	(nr_seq_local	= :new.nr_seq_local or :new.nr_seq_local is null);

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.QT_BLOQUEIO ADD (
  CONSTRAINT QTBLOQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QT_BLOQUEIO ADD (
  CONSTRAINT QTBLOQ_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.QT_BLOQUEIO TO NIVEL_1;

