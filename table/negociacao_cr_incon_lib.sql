ALTER TABLE TASY.NEGOCIACAO_CR_INCON_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NEGOCIACAO_CR_INCON_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.NEGOCIACAO_CR_INCON_LIB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_INCONSISTENCIA  NUMBER(10)             NOT NULL,
  NM_USUARIO_LIB         VARCHAR2(15 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NEGCRIL_FK_I ON TASY.NEGOCIACAO_CR_INCON_LIB
(NR_SEQ_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NEGCRIL_PK ON TASY.NEGOCIACAO_CR_INCON_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRIL_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NEGCRIL_UK ON TASY.NEGOCIACAO_CR_INCON_LIB
(NR_SEQ_INCONSISTENCIA, NM_USUARIO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NEGCRIL_UK
  MONITORING USAGE;


ALTER TABLE TASY.NEGOCIACAO_CR_INCON_LIB ADD (
  CONSTRAINT NEGCRIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT NEGCRIL_UK
 UNIQUE (NR_SEQ_INCONSISTENCIA, NM_USUARIO_LIB)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NEGOCIACAO_CR_INCON_LIB ADD (
  CONSTRAINT NEGCRIL_NEGCRIN_FK 
 FOREIGN KEY (NR_SEQ_INCONSISTENCIA) 
 REFERENCES TASY.NEGOCIACAO_CR_INCONSIST (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NEGOCIACAO_CR_INCON_LIB TO NIVEL_1;

