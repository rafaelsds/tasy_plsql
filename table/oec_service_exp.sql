ALTER TABLE TASY.OEC_SERVICE_EXP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OEC_SERVICE_EXP CASCADE CONSTRAINTS;

CREATE TABLE TASY.OEC_SERVICE_EXP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EXP_ROW           NUMBER(4),
  CD_SERVICE_EXP       NUMBER(4),
  DS_SERVICE_EXP       VARCHAR2(80 BYTE),
  NR_SEQ_SERVICE_RESP  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.OECSERVEXP_PK ON TASY.OEC_SERVICE_EXP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OEC_SERVICE_EXP ADD (
  CONSTRAINT OECSERVEXP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.OEC_SERVICE_EXP TO NIVEL_1;

