ALTER TABLE TASY.PPM_OBJETIVO_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PPM_OBJETIVO_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PPM_OBJETIVO_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_METRICA       NUMBER(10)               NOT NULL,
  DT_REFERENCIA        DATE,
  VL_RESULTADO_CALC    NUMBER(15,2),
  VL_RESULTADO_MANUAL  NUMBER(15,2),
  VL_MONTANTE          NUMBER(15,2),
  VL_INDIVIDUAL        NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PPMOBMRE_PK ON TASY.PPM_OBJETIVO_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PPMOBMRE_PPMOBMTR_FK_I ON TASY.PPM_OBJETIVO_RESULT
(NR_SEQ_METRICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PPM_OBJETIVO_RESULT_BEFORE
before insert or update ON TASY.PPM_OBJETIVO_RESULT for each row
declare

dt_fim_ano_w		date;
dt_referencia_w		date;
nr_seq_acumulado_w	number(10);
nr_seq_result_w		number(10);
vl_calculado_w		ppm_objetivo_result.vl_resultado_calc%type;
vl_montante_w		ppm_objetivo_result.vl_montante%type;
vl_individual_w		ppm_objetivo_result.vl_individual%type;
ie_tipo_metrica_w	ppm_metrica.ie_tipo%type;
ie_informacao_w		ppm_metrica.ie_informacao%type;
qt_registro_w		number(10);

pragma autonomous_transaction;
begin

select	a.ie_tipo,
	a.ie_informacao
into	ie_tipo_metrica_w,
	ie_informacao_w
from	ppm_metrica a,
	ppm_objetivo_metrica b
where	a.nr_sequencia = b.nr_seq_metrica
and	b.nr_sequencia = :new.nr_seq_metrica;

if	(:new.vl_resultado_calc <> 0) and
	(ie_tipo_metrica_w = 'I') and
	(:new.nm_usuario not in ('Tasy','TasyPPM')) then

	dt_fim_ano_w	:= PKG_DATE_UTILS.end_of(to_date('31/12/' || to_char(:new.dt_referencia,'yyyy'),'dd/mm/yyyy'),'DAY');

	select	max(nr_sequencia)
	into	nr_seq_acumulado_w
	from	ppm_objetivo_result a
	where	a.nr_seq_metrica	= :new.nr_seq_metrica
	and	dt_referencia		= dt_fim_ano_w;

	if	(:new.dt_referencia < dt_fim_ano_w) then
		dt_referencia_w	:= sysdate;
	else
		dt_referencia_w	:= dt_fim_ano_w;
	end if;

	if	(inserting) then
		select	count(1) + 1,
			nvl(sum(vl_resultado_calc),0) + :new.vl_resultado_calc
		into	qt_registro_w,
			vl_montante_w
		from	ppm_objetivo_result a
		where	a.nr_seq_metrica = :new.nr_seq_metrica
		and	a.nr_sequencia	not in (nr_seq_acumulado_w)
		and	a.dt_referencia between trunc(dt_referencia_w,'year') and dt_referencia_w;
	else
		select	count(1) + 1,
			nvl(sum(vl_resultado_calc),0) + :new.vl_resultado_calc
		into	qt_registro_w,
			vl_montante_w
		from	ppm_objetivo_result a
		where	a.nr_seq_metrica 	= :new.nr_seq_metrica
		and	a.nr_sequencia		not in (nr_seq_acumulado_w)
		and	a.nr_sequencia		<> :old.nr_sequencia
		and	a.dt_referencia 	between trunc(dt_referencia_w,'year') and dt_referencia_w;
	end if;

	if	(ie_informacao_w = 'V') then
		vl_calculado_w	:= vl_montante_w;
	else
		vl_calculado_w	:= dividir(vl_montante_w,qt_registro_w);
	end if;

	/* Inserir linha de acumulado caso n�o exista para essa m�trica */
	if	(nr_seq_acumulado_w is null) then
		select	ppm_objetivo_result_seq.nextval
		into	nr_seq_result_w
		from	dual;

		if	(vl_calculado_w is not null) then

			insert into ppm_objetivo_result
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_metrica,
				dt_referencia,
				vl_resultado_calc,
				vl_resultado_manual,
				vl_montante,
				vl_individual)
			values	(nr_seq_result_w,
				sysdate,
				'Tasy',
				sysdate,
				'Tasy',
				:new.nr_seq_metrica,
				dt_fim_ano_w,
				nvl(vl_calculado_w,0),
				null,
				vl_montante_w,
				vl_individual_w);

			insert into ppm_objetivo_result_ind
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_result,
				nr_seq_metrica)
			values	(ppm_objetivo_result_ind_seq.nextval,
				sysdate,
				'Tasy',
				sysdate,
				'Tasy',
				nr_seq_result_w,
				:new.nr_seq_metrica);

			insert into ppm_objetivo_result_gestor
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_result,
				nr_seq_metrica)
			values	(ppm_objetivo_result_gestor_seq.nextval,
				sysdate,
				'Tasy',
				sysdate,
				'Tasy',
				nr_seq_result_w,
				:new.nr_seq_metrica);
		end if;
	else
		update	ppm_objetivo_result
		set	vl_resultado_calc 	= vl_calculado_w,
			vl_montante		= vl_montante_w,
			vl_individual		= vl_individual_w
		where	nr_sequencia 		= nr_seq_acumulado_w;
	end if;

end if;

commit;

end PPM_OBJETIVO_RESULT_BEFORE;
/


ALTER TABLE TASY.PPM_OBJETIVO_RESULT ADD (
  CONSTRAINT PPMOBMRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PPM_OBJETIVO_RESULT ADD (
  CONSTRAINT PPMOBMRE_PPMOBMTR_FK 
 FOREIGN KEY (NR_SEQ_METRICA) 
 REFERENCES TASY.PPM_OBJETIVO_METRICA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PPM_OBJETIVO_RESULT TO NIVEL_1;

