ALTER TABLE TASY.SYS_WS_LIT_BILLING
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SYS_WS_LIT_BILLING CASCADE CONSTRAINTS;

CREATE TABLE TASY.SYS_WS_LIT_BILLING
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_FATURA            NUMBER(10),
  NR_LOTE                  NUMBER(8),
  NR_NOTA                  VARCHAR2(20 BYTE),
  CD_UNIMED                VARCHAR2(10 BYTE),
  CD_USUARIO_PLANO         VARCHAR2(13 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_TIPO_SAIDA_SPDAT      VARCHAR2(1 BYTE),
  IE_TIPO_ATENDIMENTO      VARCHAR2(2 BYTE),
  NM_BENEFICIARIO          VARCHAR2(25 BYTE),
  DT_ATENDIMENTO           DATE,
  CD_CID                   VARCHAR2(6 BYTE),
  NR_GUIA_PRINCIPAL        NUMBER(20),
  NR_SEQ_SERV_PRE_PAGTO    NUMBER(10),
  CD_EXCECAO               VARCHAR2(1 BYTE),
  IE_CARATER_ATENDIMENTO   NUMBER(1),
  IE_PACIENTE              VARCHAR2(1 BYTE),
  IE_PCMSO                 VARCHAR2(1 BYTE),
  PR_TAXA                  NUMBER(15,2),
  IE_ENVIA_CONTA           VARCHAR2(1 BYTE),
  NR_LINHA                 NUMBER(10),
  IE_NECESSITA_DOC_FISICO  VARCHAR2(1 BYTE),
  IE_DOC_FISICO_CONF       VARCHAR2(1 BYTE),
  NM_USUARIO_DOC_FISICO    VARCHAR2(15 BYTE),
  DT_CONFERENCIA_DOC       DATE,
  DT_INICIO_GERACAO_CONTA  DATE,
  DT_FIM_GERACAO_CONTA     DATE,
  DT_INICIO_CONSIST_CONTA  DATE,
  DT_FIM_CONSIST_CONTA     DATE,
  DT_INTERNACAO            DATE,
  DT_ALTA                  DATE,
  DT_ULTIMA_AUTORIZ        DATE,
  TP_NOTA                  NUMBER(1),
  ID_NOTA_PRINCIPAL        VARCHAR2(1 BYTE),
  NR_VER_TISS              VARCHAR2(7 BYTE),
  NR_GUIA_TISS_PRESTADOR   VARCHAR2(20 BYTE),
  NR_GUIA_TISS_PRINCIPAL   VARCHAR2(20 BYTE),
  NR_GUIA_TISS_OPERADORA   VARCHAR2(20 BYTE),
  TP_IND_ACIDENTE          VARCHAR2(1 BYTE),
  MOTIVO_ENCERRAM          VARCHAR2(2 BYTE),
  NR_CNPJ_CPF_REQ          VARCHAR2(14 BYTE),
  NM_PREST_REQ             VARCHAR2(40 BYTE),
  SG_CONS_PROF_REQ         VARCHAR2(12 BYTE),
  NR_CONS_PROF_REQ         VARCHAR2(15 BYTE),
  SG_UF_CONS_REQ           VARCHAR2(2 BYTE),
  NR_CBO_REQ               NUMBER(6),
  NR_FATURA_GLOSADA        NUMBER(11),
  NR_NDR_GLOSADA           NUMBER(11),
  NR_LOTE_GLOSADO          NUMBER(8),
  NR_NOTA_GLOSADA          VARCHAR2(20 BYTE),
  DT_PROTOCOLO             DATE,
  ID_RN                    VARCHAR2(1 BYTE),
  NR_SEQ_CONTA             NUMBER(10),
  IE_CONSISTENTE           VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SYSWLBL_PK ON TASY.SYS_WS_LIT_BILLING
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SYS_WS_LIT_BILLING ADD (
  CONSTRAINT SYSWLBL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SYS_WS_LIT_BILLING TO NIVEL_1;

