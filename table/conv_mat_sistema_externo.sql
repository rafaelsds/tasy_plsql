ALTER TABLE TASY.CONV_MAT_SISTEMA_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_MAT_SISTEMA_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_MAT_SISTEMA_EXTERNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SISTEMA           VARCHAR2(15 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  CD_SISTEMA_EXTERNO   VARCHAR2(50 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMASEX_ESTABEL_FK_I ON TASY.CONV_MAT_SISTEMA_EXTERNO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMASEX_MATERIA_FK_I ON TASY.CONV_MAT_SISTEMA_EXTERNO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMASEX_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMASEX_PK ON TASY.CONV_MAT_SISTEMA_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMASEX_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.conv_mat_sistema_ext_update
before update ON TASY.CONV_MAT_SISTEMA_EXTERNO for each row
declare

begin

if	(:new.cd_material <> :old.cd_material or
	:new.cd_sistema_externo <> :old.cd_sistema_externo or
	:new.ie_sistema <> :old.ie_sistema) then

	if	(obter_se_compra_pend_entrega(:new.cd_material) = 'S') then
		--'Sem permiss�o para altera a regra de convers�o para este material'
		wheb_mensagem_pck.exibir_mensagem_abort(216341);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CONV_MAT_SISTEMA_EXTERNO_INS
before insert or update ON TASY.CONV_MAT_SISTEMA_EXTERNO for each row
declare
qt_registros_w	number(10);
Pragma Autonomous_Transaction;

begin

select	count(*)
into	qt_registros_w
from	conv_mat_sistema_externo
where	nr_sequencia <> :new.nr_sequencia
and	cd_material = :new.cd_material
and	ie_sistema = :new.ie_sistema
and	cd_estabelecimento = :new.cd_estabelecimento
and	obter_se_periodo_vigente(pkg_date_utils.start_of(dt_inicio_vigencia, 'DD', null), pkg_date_utils.start_of(dt_fim_vigencia, 'DD', null), pkg_date_utils.start_of(sysdate, 'DD', null)) = 'S';

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(819869);
	/*J� existe um registro vig�nte para esse material. N�o � poss�vel incluir registros duplicados.*/
end if;

select	count(*)
into	qt_registros_w
from	conv_mat_sistema_externo
where	nr_sequencia <> :new.nr_sequencia
and	cd_sistema_externo = :new.cd_sistema_externo
and	cd_estabelecimento = :new.cd_estabelecimento
and	obter_se_periodo_vigente(pkg_date_utils.start_of(:new.dt_inicio_vigencia, 'DD', null), pkg_date_utils.start_of(:new.dt_fim_vigencia, 'DD', null), pkg_date_utils.start_of(sysdate, 'DD', null)) = 'S';

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(850434);
	/*O c�digo do material externo digitado j� est� sendo usado para outro material. N�o � poss�vel vincular o mesmo c�digo para dois ou mais materiais.*/
end if;

end;
/


ALTER TABLE TASY.CONV_MAT_SISTEMA_EXTERNO ADD (
  CONSTRAINT COMASEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_MAT_SISTEMA_EXTERNO ADD (
  CONSTRAINT COMASEX_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMASEX_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.CONV_MAT_SISTEMA_EXTERNO TO NIVEL_1;

