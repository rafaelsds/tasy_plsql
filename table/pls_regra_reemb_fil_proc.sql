ALTER TABLE TASY.PLS_REGRA_REEMB_FIL_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_REEMB_FIL_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_REEMB_FIL_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_FILTRO   NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO_SERVICO  NUMBER(10),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRRFP_AREPROC_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFP_ESPPROC_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFP_GRUPROC_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRRFP_PK ON TASY.PLS_REGRA_REEMB_FIL_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFP_PLSPRGS_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFP_PLSRRFI_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(NR_SEQ_REGRA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFP_PROCEDI_FK_I ON TASY.PLS_REGRA_REEMB_FIL_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_REEMB_FIL_PROC ADD (
  CONSTRAINT PLSRRFP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_REEMB_FIL_PROC ADD (
  CONSTRAINT PLSRRFP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSRRFP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSRRFP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSRRFP_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSRRFP_PLSRRFI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_FILTRO) 
 REFERENCES TASY.PLS_REGRA_REEMB_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSRRFP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_REGRA_REEMB_FIL_PROC TO NIVEL_1;

