ALTER TABLE TASY.RELATORIO_VALIDACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_VALIDACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_VALIDACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TESTE_AUTOMATIZADO   VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM_EXEC       NUMBER(10),
  NR_SEQ_RELATORIO        NUMBER(10),
  QT_SIMILARIDADE         VARCHAR2(255 BYTE),
  QT_DISTANCIA            VARCHAR2(255 BYTE),
  QT_SIMILARIDADE_IMAGE   VARCHAR2(255 BYTE),
  QT_DISTANCIA_IMAGE      VARCHAR2(255 BYTE),
  QT_SIMILARIDADE_JPRXML  VARCHAR2(255 BYTE),
  QT_DISTANCIA_JPRXML     VARCHAR2(255 BYTE),
  IE_STATUS_COMPARACAO    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RELAT_VAL_PK ON TASY.RELATORIO_VALIDACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELAT_VAL_RELATOR_FK_I ON TASY.RELATORIO_VALIDACAO
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RELATORIO_VALIDACAO_tp  after update ON TASY.RELATORIO_VALIDACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_ORDEM_EXEC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_EXEC,1,4000),substr(:new.NR_SEQ_ORDEM_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_EXEC',ie_log_w,ds_w,'RELATORIO_VALIDACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RELATORIO_VALIDACAO ADD (
  CONSTRAINT RELAT_VAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO_VALIDACAO ADD (
  CONSTRAINT RELAT_VAL_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RELATORIO_VALIDACAO TO NIVEL_1;

