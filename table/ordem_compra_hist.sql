ALTER TABLE TASY.ORDEM_COMPRA_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  DS_HISTORICO         VARCHAR2(4000 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO              VARCHAR2(1 BYTE),
  DT_LIBERACAO         DATE,
  IE_MOTIVO_HIST       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORDCOHI_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_HIST
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORDCOHI_PK ON TASY.ORDEM_COMPRA_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_HIST_tp  after update ON TASY.ORDEM_COMPRA_HIST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_HISTORICO,1,4000),substr(:new.DT_HISTORICO,1,4000),:new.nm_usuario,nr_seq_w,'DT_HISTORICO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOTIVO_HIST,1,4000),substr(:new.IE_MOTIVO_HIST,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOTIVO_HIST',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HISTORICO,1,4000),substr(:new.DS_HISTORICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_HISTORICO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'ORDEM_COMPRA_HIST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORDEM_COMPRA_HIST ADD (
  CONSTRAINT ORDCOHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_HIST ADD (
  CONSTRAINT ORDCOHI_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_HIST TO NIVEL_1;

