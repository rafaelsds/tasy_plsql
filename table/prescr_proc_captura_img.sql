ALTER TABLE TASY.PRESCR_PROC_CAPTURA_IMG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_CAPTURA_IMG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_CAPTURA_IMG
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_PRESCRICAO            NUMBER(14)           NOT NULL,
  NR_SEQUENCIA_PRESCRICAO  NUMBER(6)            NOT NULL,
  NR_SEQ_CAPTURA           NUMBER(10)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRPROCAPIM_CAPIMG_FK_I ON TASY.PRESCR_PROC_CAPTURA_IMG
(NR_SEQ_CAPTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRPROCAPIM_PK ON TASY.PRESCR_PROC_CAPTURA_IMG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPROCAPIM_PRESPRO_FK_I ON TASY.PRESCR_PROC_CAPTURA_IMG
(NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_proc_captura_img_delete
BEFORE DELETE ON TASY.PRESCR_PROC_CAPTURA_IMG FOR EACH ROW
DECLARE

BEGIN
  gravar_log_exclusao('PRESCR_PROC_CAPTURA_IMG', obter_usuario_ativo,
                      'NR_SEQUENCIA=' || :OLD.nr_sequencia ||', NR_PRESCRICAO=' || :OLD.nr_prescricao ||', NR_SEQUENCIA_PRESCRICAO=' || :OLD.nr_sequencia_prescricao || ', NR_SEQ_CAPTURA=' || :OLD.nr_seq_captura,
                      'N');
END;
/


CREATE OR REPLACE TRIGGER TASY.PRESCR_PROC_CAPTURA_IMG_tp  after update ON TASY.PRESCR_PROC_CAPTURA_IMG FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PRESCR_PROC_CAPTURA_IMG',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.captura_imagem_logs
BEFORE INSERT OR UPDATE OR DELETE ON TASY.PRESCR_PROC_CAPTURA_IMG FOR EACH ROW
DECLARE

nr_sequencia_w NUMBER(10);

BEGIN

  SELECT prescr_proc_captura_log_seq.NEXTVAL
  INTO nr_sequencia_w
  FROM dual;

  IF (INSERTING) THEN
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :NEW.nm_usuario,
           SYSDATE,
           :NEW.nm_usuario,
           :NEW.nr_prescricao,
           :NEW.nr_sequencia_prescricao,
           :NEW.nr_seq_captura,
           'INSERT');
  END;
  ELSIF (UPDATING) THEN
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :NEW.nm_usuario,
           SYSDATE,
           :NEW.nm_usuario,
           :NEW.nr_prescricao,
           :NEW.nr_sequencia_prescricao,
           :NEW.nr_seq_captura,
           'UPDATE');
  END;
  ELSE
  BEGIN
    INSERT INTO prescr_proc_captura_log(NR_SEQUENCIA,
                                        DS_CALL_STACK,
                                        DT_ATUALIZACAO,
                                        NM_USUARIO,
                                        DT_ATUALIZACAO_NREC,
                                        NM_USUARIO_NREC,
                                        NR_PRESCRICAO,
                                        NR_SEQ_PRESCRICAO,
                                        NR_SEQ_CAPTURA_IMAGEM,
                                        DS_OPERACAO)
    VALUES(nr_sequencia_w,
           substr(dbms_utility.format_call_stack,1,3000),
           SYSDATE,
           :OLD.nm_usuario,
           SYSDATE,
           :OLD.nm_usuario,
           :OLD.nr_prescricao,
           :OLD.nr_sequencia_prescricao,
           :OLD.nr_seq_captura,
           'DELETE');
  END;
  END IF;
END;
/


ALTER TABLE TASY.PRESCR_PROC_CAPTURA_IMG ADD (
  CONSTRAINT PRPROCAPIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_CAPTURA_IMG ADD (
  CONSTRAINT PRPROCAPIM_CAPIMG_FK 
 FOREIGN KEY (NR_SEQ_CAPTURA) 
 REFERENCES TASY.CAPTURA_IMAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRPROCAPIM_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_PROC_CAPTURA_IMG TO NIVEL_1;

