ALTER TABLE TASY.PLS_LOTE_TRANSACOES_INT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_TRANSACOES_INT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_TRANSACOES_INT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_LOTE              VARCHAR2(255 BYTE)       NOT NULL,
  DT_INICIO_PERIODO    DATE                     NOT NULL,
  NM_USUARIO_CRIACAO   VARCHAR2(255 BYTE)       NOT NULL,
  DT_CRIACAO_LOTE      DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_FINAL_PERIODO     DATE,
  NM_USUARIO_ENVIO     VARCHAR2(255 BYTE),
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLTRI_PK ON TASY.PLS_LOTE_TRANSACOES_INT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLTRI_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_TRANSACOES_INT ADD (
  CONSTRAINT PLSLTRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_LOTE_TRANSACOES_INT TO NIVEL_1;

