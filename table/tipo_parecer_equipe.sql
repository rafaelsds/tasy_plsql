ALTER TABLE TASY.TIPO_PARECER_EQUIPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_PARECER_EQUIPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_PARECER_EQUIPE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_PARECER  NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPE        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPAEQU_PFEQUIP_FK_I ON TASY.TIPO_PARECER_EQUIPE
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPAEQU_PFEQUIP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIPAEQU_PK ON TASY.TIPO_PARECER_EQUIPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPAEQU_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIPAEQU_TIPPARE_FK_I ON TASY.TIPO_PARECER_EQUIPE
(NR_SEQ_TIPO_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_PARECER_EQUIPE ADD (
  CONSTRAINT TIPAEQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_PARECER_EQUIPE ADD (
  CONSTRAINT TIPAEQU_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT TIPAEQU_TIPPARE_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PARECER) 
 REFERENCES TASY.TIPO_PARECER (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_PARECER_EQUIPE TO NIVEL_1;

