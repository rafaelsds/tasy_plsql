DROP TABLE TASY.ELEMAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ELEMAR
(
  NR_ATENDIMENTO    NUMBER(10)                  NOT NULL,
  NR_INTERNO_CONTA  NUMBER(10)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.ELEMAR TO NIVEL_1;

