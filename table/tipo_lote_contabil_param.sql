ALTER TABLE TASY.TIPO_LOTE_CONTABIL_PARAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_LOTE_CONTABIL_PARAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_LOTE_CONTABIL_PARAM
(
  CD_TIPO_LOTE_CONTABIL  NUMBER(10)             NOT NULL,
  NR_SEQ_PARAMETRO       NUMBER(3)              NOT NULL,
  DS_PARAMETRO           VARCHAR2(40 BYTE)      NOT NULL,
  IE_TIPO_PARAMETRO      VARCHAR2(2 BYTE)       NOT NULL,
  IE_PARAMETRO_FIXO      VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_PARAMETRO           DATE,
  VL_PARAMETRO           NUMBER(15,4),
  DS_VALOR_PARAMETRO     VARCHAR2(40 BYTE),
  CD_DOMINIO             NUMBER(5),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_EXP_PARAMETRO       NUMBER(10),
  NR_SEQ_DIC_OBJ         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPLOCP_DICEXPR_FK_I ON TASY.TIPO_LOTE_CONTABIL_PARAM
(CD_EXP_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPLOCP_DOMINIO_FK_I ON TASY.TIPO_LOTE_CONTABIL_PARAM
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPLOCP_PK ON TASY.TIPO_LOTE_CONTABIL_PARAM
(CD_TIPO_LOTE_CONTABIL, NR_SEQ_PARAMETRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPLOCP_TIPLOCO_FK_I ON TASY.TIPO_LOTE_CONTABIL_PARAM
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TIPO_LOTE_CONTABIL_PARAM_tp  after update ON TASY.TIPO_LOTE_CONTABIL_PARAM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_TIPO_LOTE_CONTABIL='||to_char(:old.CD_TIPO_LOTE_CONTABIL)||'#@#@NR_SEQ_PARAMETRO='||to_char(:old.NR_SEQ_PARAMETRO);gravar_log_alteracao(substr(:old.CD_EXP_PARAMETRO,1,4000),substr(:new.CD_EXP_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DIC_OBJ,1,4000),substr(:new.NR_SEQ_DIC_OBJ,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DIC_OBJ',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PARAMETRO,1,4000),substr(:new.IE_TIPO_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PARAMETRO,1,4000),substr(:new.DT_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'DT_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VALOR_PARAMETRO,1,4000),substr(:new.DS_VALOR_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALOR_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOMINIO,1,4000),substr(:new.CD_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOMINIO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PARAMETRO_FIXO,1,4000),substr(:new.IE_PARAMETRO_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PARAMETRO_FIXO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PARAMETRO,1,4000),substr(:new.DS_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOTE_CONTABIL,1,4000),substr(:new.CD_TIPO_LOTE_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOTE_CONTABIL',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PARAMETRO,1,4000),substr(:new.NR_SEQ_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PARAMETRO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL_PARAM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TIPO_LOTE_CONTABIL_PARAM ADD (
  CONSTRAINT TIPLOCP_PK
 PRIMARY KEY
 (CD_TIPO_LOTE_CONTABIL, NR_SEQ_PARAMETRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_LOTE_CONTABIL_PARAM ADD (
  CONSTRAINT TIPLOCP_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL),
  CONSTRAINT TIPLOCP_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO),
  CONSTRAINT TIPLOCP_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_PARAMETRO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.TIPO_LOTE_CONTABIL_PARAM TO NIVEL_1;

