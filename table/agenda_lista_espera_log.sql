ALTER TABLE TASY.AGENDA_LISTA_ESPERA_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_LISTA_ESPERA_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_LISTA_ESPERA_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESTAGIO       NUMBER(10),
  NR_SEQ_ESTAGIO_ANT   NUMBER(10),
  NR_SEQ_LISTA_ESPERA  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGLESRL_AGLIESE_FK_I ON TASY.AGENDA_LISTA_ESPERA_LOG
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLESRL_AGLIESE_FK2_I ON TASY.AGENDA_LISTA_ESPERA_LOG
(NR_SEQ_ESTAGIO_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLESRL_AGLIESP_FK_I ON TASY.AGENDA_LISTA_ESPERA_LOG
(NR_SEQ_LISTA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGLESRL_PK ON TASY.AGENDA_LISTA_ESPERA_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGENDA_LISTA_ESPERA_LOG ADD (
  CONSTRAINT AGLESRL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_LISTA_ESPERA_LOG ADD (
  CONSTRAINT AGLESRL_AGLIESE_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.AG_LISTA_ESPERA_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT AGLESRL_AGLIESE_FK2 
 FOREIGN KEY (NR_SEQ_ESTAGIO_ANT) 
 REFERENCES TASY.AG_LISTA_ESPERA_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT AGLESRL_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA_ESPERA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_LISTA_ESPERA_LOG TO NIVEL_1;

