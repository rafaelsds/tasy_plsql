ALTER TABLE TASY.REGRA_TURNO_PRESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TURNO_PRESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TURNO_PRESCR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  HR_PRIM_TURNO         VARCHAR2(5 BYTE)        NOT NULL,
  HR_SEG_TURNO          VARCHAR2(5 BYTE),
  HR_TER_TURNO          VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_PRIM_TURNO         DATE,
  DT_SEG_TURNO          DATE,
  DT_TER_TURNO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGTUPR_ESTABEL_FK_I ON TASY.REGRA_TURNO_PRESCR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGTUPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGTUPR_PK ON TASY.REGRA_TURNO_PRESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTUPR_SETATEN_FK_I ON TASY.REGRA_TURNO_PRESCR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGTUPR_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atual_regra_turno_prescr
before insert or update ON TASY.REGRA_TURNO_PRESCR for each row
declare

begin

if (:new.hr_prim_turno is not null) and
   ((:new.hr_prim_turno <> :old.hr_prim_turno) or (:old.dt_prim_turno is null)) then
	:new.dt_prim_turno := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_turno || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_seg_turno is not null) and
   ((:new.hr_seg_turno <> :old.hr_seg_turno) or (:old.dt_seg_turno is null)) then
	:new.dt_seg_turno := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_seg_turno || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_ter_turno is not null) and
   ((:new.hr_ter_turno <> :old.hr_ter_turno) or (:old.dt_ter_turno is null)) then
	:new.dt_ter_turno := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_ter_turno || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/


ALTER TABLE TASY.REGRA_TURNO_PRESCR ADD (
  CONSTRAINT REGTUPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TURNO_PRESCR ADD (
  CONSTRAINT REGTUPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGTUPR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.REGRA_TURNO_PRESCR TO NIVEL_1;

