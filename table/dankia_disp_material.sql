ALTER TABLE TASY.DANKIA_DISP_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DANKIA_DISP_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.DANKIA_DISP_MATERIAL
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_LIDO_DANKIA            DATE,
  CD_MATERIAL               NUMBER(10)          NOT NULL,
  CD_MATERIAL_GENERICO      NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DS_MATERIAL               VARCHAR2(255 BYTE)  NOT NULL,
  DS_UNIDADE_MEDIDA         VARCHAR2(40 BYTE)   NOT NULL,
  CD_UNIDADE_MEDIDA         VARCHAR2(30 BYTE)   NOT NULL,
  IE_TIPO_MATERIAL          VARCHAR2(3 BYTE)    NOT NULL,
  IE_OPERACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DS_PROCESSADO_OBSERVACAO  VARCHAR2(400 BYTE),
  DS_STACK                  VARCHAR2(2000 BYTE),
  IE_CONTROLADO             VARCHAR2(1 BYTE),
  IE_PROCESSADO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DANDISMAT_ESTABEL_FK_I ON TASY.DANKIA_DISP_MATERIAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANDISMAT_MATERIA_FK_I ON TASY.DANKIA_DISP_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DANDISMAT_PK ON TASY.DANKIA_DISP_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANDISMAT_UNIMEDI_FK_I ON TASY.DANKIA_DISP_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DANKIA_DISP_MATERIAL ADD (
  CONSTRAINT DANDISMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DANKIA_DISP_MATERIAL ADD (
  CONSTRAINT DANDISMAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DANDISMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT DANDISMAT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.DANKIA_DISP_MATERIAL TO NIVEL_1;

