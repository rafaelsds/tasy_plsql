ALTER TABLE TASY.ESCALA_GRACE_II
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_GRACE_II CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_GRACE_II
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_HORA                   NUMBER(2),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  QT_IDADE                  NUMBER(3)           NOT NULL,
  QT_FREQ_CARDIACA          NUMBER(3)           NOT NULL,
  QT_PA_SISTOLICA           NUMBER(3)           NOT NULL,
  QT_CREATININA             NUMBER(6,2),
  QT_PONTUACAO              NUMBER(10,5),
  IE_KILLIP                 VARCHAR2(3 BYTE)    NOT NULL,
  QT_PONTUACAO_6            NUMBER(10,5),
  QT_PONTUACAO_SCORE_6      NUMBER(10,5),
  QT_PONTUACAO_1            NUMBER(10,5),
  QT_PONTUACAO_3            NUMBER(10,5),
  QT_PONTUACAO_MI           NUMBER(10,5),
  IE_MARCADOR_CARDIACO_ELE  VARCHAR2(1 BYTE)    NOT NULL,
  IE_PARADA_CARDIACA_ADM    VARCHAR2(1 BYTE)    NOT NULL,
  IE_DESVIO_SEG_ST          VARCHAR2(1 BYTE)    NOT NULL,
  IE_PONT_HOSPITAL_MAIOR    VARCHAR2(1 BYTE),
  IE_INSUFICIENCIA_RENAL    VARCHAR2(1 BYTE),
  IE_PONT_1_ANO_MAIOR       VARCHAR2(1 BYTE),
  IE_PONT_3_ANO_MAIOR       VARCHAR2(1 BYTE),
  IE_PONT_6_MESES_MAIOR     VARCHAR2(1 BYTE),
  IE_PONT_1_ANO_MI_MAIOR    VARCHAR2(1 BYTE),
  IE_USO_DIURETICO          VARCHAR2(1 BYTE),
  IE_UTILIZA_CREATININA     VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCGRA2_ATEPACI_FK_I ON TASY.ESCALA_GRACE_II
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCGRA2_PESFISI_FK_I ON TASY.ESCALA_GRACE_II
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCGRA2_PK ON TASY.ESCALA_GRACE_II
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_grace_ii_atual
before insert or update ON TASY.ESCALA_GRACE_II for each row
declare

--Hospital
qt_pont_idade_h_w			Number(15,8) := 0;
qt_pont_pulso_h_w			Number(15,8) := 0;
qt_pont_pa_sis_h_w			Number(15,8) := 0;
qt_pont_creatinina_h_w		Number(15,8) := 0;
qt_pont_killip_h_w			Number(15,8) := 0;
qt_pont_desvio_st_h_w		Number(15,8) := 0;
qt_pont_marcador_h_w		Number(15,8) := 0;
qt_pont_parada_h_w			Number(15,8) := 0;
qt_total_ponto_h_w			Number(15,8) := 0;
qt_chance_h_w				Number(15,5) := 0;
ds_chance_h_w				varchar2(200);

--6 meses apos
qt_pont_idade_6_w			Number(15,8) := 0;
qt_pont_pulso_6_w			Number(15,8) := 0;
qt_pont_pa_sis_6_w			Number(15,8) := 0;
qt_pont_creatinina_6_w		Number(15,8) := 0;
qt_pont_killip_6_w			Number(15,8) := 0;
qt_pont_desvio_st_6_w		Number(15,8) := 0;
qt_pont_marcador_6_w		Number(15,8) := 0;
qt_pont_parada_6_w			Number(15,8) := 0;
qt_total_ponto_6_w			Number(15,8) := 0;
qt_chance_6_w				Number(15,5) := 0;
ds_chance_6_w				varchar2(200);

--1 ano apos
qt_pont_idade_1_w			Number(15,8) := 0;
qt_pont_pulso_1_w			Number(15,8) := 0;
qt_pont_pa_sis_1_w			Number(15,8) := 0;
qt_pont_creatinina_1_w		Number(15,8) := 0;
qt_pont_killip_1_w			Number(15,8) := 0;
qt_pont_desvio_st_1_w		Number(15,8) := 0;
qt_pont_marcador_1_w		Number(15,8) := 0;
qt_pont_parada_1_w			Number(15,8) := 0;
qt_total_ponto_1_w			Number(15,8) := 0;
qt_chance_1_w				Number(15,5) := 0;
ds_chance_1_w				Varchar2(200);

--1 ano apos MI
qt_pont_idade_1mi_w			Number(15,8) := 0;
qt_pont_pulso_1mi_w			Number(15,8) := 0;
qt_pont_pa_sis_1mi_w		Number(15,8) := 0;
qt_pont_creatinina_1mi_w	Number(15,8) := 0;
qt_pont_killip_1mi_w		Number(15,8) := 0;
qt_pont_desvio_st_1mi_w		Number(15,8) := 0;
qt_pont_marcador_1mi_w		Number(15,8) := 0;
qt_pont_parada_1mi_w		Number(15,8) := 0;
qt_total_ponto_1mi_w		Number(15,8) := 0;
qt_chance_1mi_w				Number(15,5) := 0;
ds_chance_1mi_w				Varchar2(200);

--3 ano apos
qt_pont_idade_3_w			Number(15,8) := 0;
qt_pont_pulso_3_w			Number(15,8) := 0;
qt_pont_pa_sis_3_w			Number(15,8) := 0;
qt_pont_creatinina_3_w		Number(15,8) := 0;
qt_pont_killip_3_w			Number(15,8) := 0;
qt_pont_desvio_st_3_w		Number(15,8) := 0;
qt_pont_marcador_3_w		Number(15,8) := 0;
qt_pont_parada_3_w			Number(15,8) := 0;
qt_total_ponto_3_w			Number(15,8) := 0;
qt_chance_3_w				Number(15,5) := 0;
ds_chance_3_w				Varchar2(200);

-- Utilizado para 1 ano e 3 anos
bool_substituido_w			BOOLEAN := true;

pragma autonomous_transaction;

begin

if	(:new.nr_hora is null) or (:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then

	begin
		:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));

	end;
end if;

-------------------------------------------------------
--						Hospital					 --
-------------------------------------------------------


--Zera a pontua��o final
qt_total_ponto_h_w	:= 0;

--1. AGE in years
if (:new.qt_idade < 30)  then
	qt_pont_idade_h_w := 1.5399;

elsif (:new.qt_idade < 40) then
	qt_pont_idade_h_w := 1.8054;

elsif (:new.qt_idade < 45) then
	qt_pont_idade_h_w := 2.2302;

elsif (:new.qt_idade < 50) then
	qt_pont_idade_h_w := 2.4957;

elsif (:new.qt_idade >= 50) and (:new.qt_idade <= 89) then
	qt_pont_idade_h_w := trunc(:new.qt_idade * 0.0531,8);

elsif (:new.qt_idade < 95) then
	qt_pont_idade_h_w := 4.8852;

elsif (:new.qt_idade >= 95) then
	qt_pont_idade_h_w := 5.0445;

end if;


--2. PULSE in beats/minute
if (:new.qt_freq_cardiaca < 50)  then
	qt_pont_pulso_h_w := 0.4263;

elsif (:new.qt_freq_cardiaca < 70) then
	qt_pont_pulso_h_w := 0.51765;

elsif (:new.qt_freq_cardiaca < 80) then
	qt_pont_pulso_h_w := 0.64815;

elsif (:new.qt_freq_cardiaca < 90) then
	qt_pont_pulso_h_w := 0.73515;

elsif (:new.qt_freq_cardiaca < 100) then
	qt_pont_pulso_h_w := 0.82215;

elsif (:new.qt_freq_cardiaca < 110) then
	qt_pont_pulso_h_w := 0.90915;

elsif (:new.qt_freq_cardiaca < 130) then
	qt_pont_pulso_h_w := 1.03965;

elsif (:new.qt_freq_cardiaca < 150) then
	qt_pont_pulso_h_w := 1.21365;

elsif (:new.qt_freq_cardiaca < 200) then
	qt_pont_pulso_h_w := 1.51815;

elsif (:new.qt_freq_cardiaca >= 200) then
	qt_pont_pulso_h_w := 1.74;

end if;


-- BPSYS is systolic blood pressure (mm Hg);
if (:new.qt_pa_sistolica < 80)  then
	qt_pont_pa_sis_h_w := -1.3272;

elsif (:new.qt_pa_sistolica < 100) then
	qt_pont_pa_sis_h_w := -1.5036;

elsif (:new.qt_pa_sistolica < 110) then
	qt_pont_pa_sis_h_w := -1.7556;

elsif (:new.qt_pa_sistolica < 120) then
	qt_pont_pa_sis_h_w := -1.9236;

elsif (:new.qt_pa_sistolica < 130) then
	qt_pont_pa_sis_h_w := -2.0916;

elsif (:new.qt_pa_sistolica < 140) then
	qt_pont_pa_sis_h_w := -2.2596;

elsif (:new.qt_pa_sistolica < 160) then
	qt_pont_pa_sis_h_w := -2.5116;

elsif (:new.qt_pa_sistolica < 180) then
	qt_pont_pa_sis_h_w := -2.8476;

elsif (:new.qt_pa_sistolica >= 180) then
	qt_pont_pa_sis_h_w := -3.024;

end if;


--Creatinine in mg/dl (same points as for death score);
if (:new.ie_utiliza_creatinina = 'S') then

	if (:new.ie_insuficiencia_renal = 'S') then
		qt_pont_creatinina_h_w := 0.3646;

	else
		qt_pont_creatinina_h_w := 0.09115;

	end if;

elsif (:new.qt_creatinina < 0.4)  then
	qt_pont_creatinina_h_w := 0.0355485;

elsif (:new.qt_creatinina < 0.8) then
	qt_pont_creatinina_h_w := 0.1084685;

elsif (:new.qt_creatinina < 1.20) then
	qt_pont_creatinina_h_w := 0.1813885;

elsif (:new.qt_creatinina < 1.60) then
	qt_pont_creatinina_h_w := 0.2543085;

elsif (:new.qt_creatinina < 2) then
	qt_pont_creatinina_h_w := 0.3272285;

elsif (:new.qt_creatinina < 4) then
	qt_pont_creatinina_h_w := 0.5459885;

elsif (:new.qt_creatinina >= 4) then
	qt_pont_creatinina_h_w := 0.7292;

end if;


-- Killip class I,II,III,IV;
if (:new.ie_killip = 'I')  then
	qt_pont_killip_h_w := 0.6931;

elsif (:new.ie_killip = 'II') then
	qt_pont_killip_h_w := 1.3862;

elsif (:new.ie_killip = 'III') then
	qt_pont_killip_h_w := 2.0793;

elsif (:new.ie_killip = 'IV') then
	qt_pont_killip_h_w := 2.7724;

elsif (:new.ie_killip = 'X') then

	if (:new.ie_uso_diuretico = 'S') then
		qt_pont_killip_h_w := 1.73275;

	else
		qt_pont_killip_h_w := 0.6931;

	end if;

end if;

--Desvio do segmento ST
if	(:new.ie_desvio_seg_st = 'S') then
	qt_pont_desvio_st_h_w := 0.8755;

end if;


--Marcadores cardiacos elevados
if	(:new.ie_marcador_cardiaco_ele = 'S') then
	qt_pont_marcador_h_w := 0.4700;

end if;


--Parada cardiaca na adminiss�o
if	(:new.ie_parada_cardiaca_adm = 'S') then
	qt_pont_parada_h_w := 1.4586;

end if;

--Calculo final
qt_total_ponto_h_w := TRUNC(-7.7035 + qt_pont_idade_h_w + qt_pont_creatinina_h_w + qt_pont_pa_sis_h_w + qt_pont_pulso_h_w + qt_pont_killip_h_w + qt_pont_desvio_st_h_w + qt_pont_parada_h_w + qt_pont_marcador_h_w,5);


----------------------------------------------------------------------
--						 	6 meses									--
----------------------------------------------------------------------


--Zera a pontua��o final
qt_total_ponto_6_w	:= 0;

--1. AGE in years
if (:new.qt_idade < 40)  then
	qt_pont_idade_6_w := 0;

elsif (:new.qt_idade < 45) then
	qt_pont_idade_6_w := 12.6;

elsif (:new.qt_idade < 50) then
	qt_pont_idade_6_w := 21.6;

elsif (:new.qt_idade >= 50) and (:new.qt_idade <= 65) then
	qt_pont_idade_6_w := trunc((:new.qt_idade * 1.8) - 63, 8);

elsif (:new.qt_idade < 75) then
	qt_pont_idade_6_w := trunc(((:new.qt_idade - 65) * 1.9) + 54, 8);

elsif (:new.qt_idade < 90) then
	qt_pont_idade_6_w := trunc((:new.qt_idade * 1.8) - 62, 8);

elsif (:new.qt_idade >= 90) then
	qt_pont_idade_6_w := 100;

end if;


--2. PULSE in beats/minute
if (:new.qt_freq_cardiaca < 70)  then
	qt_pont_pulso_6_w := 0;

elsif (:new.qt_freq_cardiaca < 80) then
	qt_pont_pulso_6_w := 1.35;

elsif (:new.qt_freq_cardiaca < 90) then
	qt_pont_pulso_6_w := 3.9;

elsif (:new.qt_freq_cardiaca < 100) then
	qt_pont_pulso_6_w := 6.35;

elsif (:new.qt_freq_cardiaca < 110) then
	qt_pont_pulso_6_w := 8.9;

elsif (:new.qt_freq_cardiaca < 130) then
	qt_pont_pulso_6_w := 12.85;

elsif (:new.qt_freq_cardiaca < 150) then
	qt_pont_pulso_6_w := 18.85;

elsif (:new.qt_freq_cardiaca < 200) then
	qt_pont_pulso_6_w := 29.35;

elsif (:new.qt_freq_cardiaca >= 200) then
	qt_pont_pulso_6_w := 34.0;

end if;


-- BPSYS is systolic blood pressure (mm Hg);
if (:new.qt_pa_sistolica < 80)  then
	qt_pont_pa_sis_6_w := 40;

elsif (:new.qt_pa_sistolica < 100) then
	qt_pont_pa_sis_6_w := 37.15;

elsif (:new.qt_pa_sistolica < 110) then
	qt_pont_pa_sis_6_w := 32.65;

elsif (:new.qt_pa_sistolica < 120) then
	qt_pont_pa_sis_6_w := 29.2;

elsif (:new.qt_pa_sistolica < 130) then
	qt_pont_pa_sis_6_w := 25.65;

elsif (:new.qt_pa_sistolica < 140) then
	qt_pont_pa_sis_6_w := 22.65;

elsif (:new.qt_pa_sistolica < 160) then
	qt_pont_pa_sis_6_w := 16.2;

elsif (:new.qt_pa_sistolica < 180) then
	qt_pont_pa_sis_6_w := 11.15;

elsif (:new.qt_pa_sistolica >= 180) then
	qt_pont_pa_sis_6_w := 8.0;

end if;


--Creatinine in mg/dl (same points as for death score);
if (:new.ie_utiliza_creatinina = 'S') then

	if (:new.ie_insuficiencia_renal = 'S') then
		qt_pont_creatinina_6_w := 14;

	else
		qt_pont_creatinina_6_w := 3.5;

	end if;

elsif (:new.qt_creatinina < 0.4)  then
	qt_pont_creatinina_6_w := 0.975;

elsif (:new.qt_creatinina < 0.8) then
	qt_pont_creatinina_6_w := 3.975;

elsif (:new.qt_creatinina < 1.20) then
	qt_pont_creatinina_6_w := 6.975;

elsif (:new.qt_creatinina < 1.60) then
	qt_pont_creatinina_6_w := 9.95;

elsif (:new.qt_creatinina < 2) then
	qt_pont_creatinina_6_w := 12.95;

elsif (:new.qt_creatinina < 4) then
	qt_pont_creatinina_6_w := 20.965;

elsif (:new.qt_creatinina >= 4) then
	qt_pont_creatinina_6_w := 28;

end if;


-- Killip class I,II,III,IV;
if (:new.ie_killip = 'I')  then
	qt_pont_killip_6_w := 0;

elsif (:new.ie_killip = 'II') then
	qt_pont_killip_6_w := 15;

elsif (:new.ie_killip = 'III') then
	qt_pont_killip_6_w := 29;

elsif (:new.ie_killip = 'IV') then
	qt_pont_killip_6_w := 44;

elsif (:new.ie_killip = 'X') then

	if (:new.ie_uso_diuretico = 'S') then
		qt_pont_killip_6_w := 20;

	else
		qt_pont_killip_6_w := 0;

	end if;

end if;

--Desvio do segmento ST
if	(:new.ie_desvio_seg_st = 'S') then
	qt_pont_desvio_st_6_w := 17;

end if;


--Marcadores cardiacos elevados
if	(:new.ie_marcador_cardiaco_ele = 'S') then
	qt_pont_marcador_6_w := 13;

end if;


--Parada cardiaca na adminiss�o
if	(:new.ie_parada_cardiaca_adm = 'S') then
	qt_pont_parada_6_w := 30;

end if;

qt_total_ponto_6_w := TRUNC(qt_pont_idade_6_w + qt_pont_creatinina_6_w + qt_pont_pa_sis_6_w + qt_pont_pulso_6_w + qt_pont_killip_6_w + qt_pont_desvio_st_6_w + qt_pont_parada_6_w + qt_pont_marcador_6_w,5);

--------------------------------------------------------------------------------------
--									1 ano											--
--------------------------------------------------------------------------------------
--Zera a pontua��o final
qt_total_ponto_1_w	:= 0;

-- Verifica se � valores normais de 1 ano ou substituidos. Para ser normal tem que ter valor de killipe cratinina
if (:new.ie_killip <> 'X') and (:new.ie_utiliza_creatinina <> 'S') then
	bool_substituido_w := false;

end if;

--1. AGE in years
if (:new.qt_idade < 30)  then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 1.193553;

	else
		qt_pont_idade_1_w := 1.28267;

	end if;

elsif (:new.qt_idade < 40) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 1.399338;

	else
		qt_pont_idade_1_w := 1.50382;

	end if;

elsif (:new.qt_idade < 45) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 1.728594;

	else
		qt_pont_idade_1_w := 1.85766;

	end if;

elsif (:new.qt_idade < 50) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 1.934379;

	else
		qt_pont_idade_1_w := 2.07881;

	end if;

elsif (:new.qt_idade >= 50) and (:new.qt_idade <= 89) then
	case :new.qt_idade
		when 50 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.0579297;

			else
				qt_pont_idade_1_w := 2.2115761;

			end if;

		when 51 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.0993009;

			else
				qt_pont_idade_1_w := 2.2560105;

			end if;

		when 52 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.1408916;

			else
				qt_pont_idade_1_w := 2.3006543;

			end if;

		when 53 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.1827791;

			else
				qt_pont_idade_1_w := 2.3455814;

			end if;

		when 54 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.2250408;

			else
				qt_pont_idade_1_w := 2.3908656;

			end if;

		when 55 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.2677541;

			else
				qt_pont_idade_1_w := 2.4365807;

			end if;

		when 56 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.3109965;

			else
				qt_pont_idade_1_w := 2.4828007;

			end if;

		when 57 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.3548453;

			else
				qt_pont_idade_1_w := 2.5295994;

			end if;

		when 58 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.3993779;

			else
				qt_pont_idade_1_w := 2.5770507;

			end if;

		when 59 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.4446718;

			else
				qt_pont_idade_1_w := 2.6252283;

			end if;

		when 60 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.4908043;

			else
				qt_pont_idade_1_w := 2.6742062;

			end if;

		when 61 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.5378528;

			else
				qt_pont_idade_1_w := 2.7240583;

			end if;

		when 62 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.5858947;

			else
				qt_pont_idade_1_w := 2.7748583;

			end if;

		when 63 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.6350075;

			else
				qt_pont_idade_1_w := 2.8266802;

			end if;

		when 64 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.6852685;

			else
				qt_pont_idade_1_w := 2.8795979;

			end if;

		when 65 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.7367551;

			else
				qt_pont_idade_1_w := 2.9336851;

			end if;

		when 66 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.7895448;

			else
				qt_pont_idade_1_w := 2.9890157;

			end if;

		when 67 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.8437127;

			else
				qt_pont_idade_1_w := 3.0456615;

			end if;

		when 68 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.8992632;

			else
				qt_pont_idade_1_w := 3.1036267;

			end if;

		when 69 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 2.9561152;

			else
				qt_pont_idade_1_w := 3.1628338;

			end if;

		when 70 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.0141823;

			else
				qt_pont_idade_1_w := 3.2232005;

			end if;

		when 71 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.0733784;

			else
				qt_pont_idade_1_w := 3.2846446;

			end if;

		when 72 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.1336173;

			else
				qt_pont_idade_1_w := 3.3470838;

			end if;

		when 73 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.1948128;

			else
				qt_pont_idade_1_w := 3.4104357;

			end if;

		when 74 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.2568786;

			else
				qt_pont_idade_1_w := 3.4746183;

			end if;

		when 75 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.3197285;

			else
				qt_pont_idade_1_w := 3.539549;

			end if;

		when 76 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.3832763;

			else
				qt_pont_idade_1_w := 3.6051458;

			end if;

		when 77 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.4474359;

			else
				qt_pont_idade_1_w := 3.6713263;

			end if;

		when 78 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.5121209;

			else
				qt_pont_idade_1_w := 3.7380082;

			end if;

		when 79 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.5772452;

			else
				qt_pont_idade_1_w := 3.8051093;

			end if;

		when 80 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.6427226;

			else
				qt_pont_idade_1_w := 3.8725474;

			end if;

		when 81 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.7084667;

			else
				qt_pont_idade_1_w := 3.94024;

			end if;

		when 82 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.7743915;

			else
				qt_pont_idade_1_w := 4.008105;

			end if;

		when 83 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.8404108;

			else
				qt_pont_idade_1_w := 4.0760601;

			end if;

		when 84 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.9064488;

			else
				qt_pont_idade_1_w := 4.1440332;

			end if;

		when 85 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 3.9724869;

			else
				qt_pont_idade_1_w := 4.2120063;

			end if;

		when 86 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 4.0385249;

			else
				qt_pont_idade_1_w := 4.2799794;

			end if;

		when 87 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 4.104563;

			else
				qt_pont_idade_1_w := 4.3479525;

			end if;

		when 88 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 4.1706011;

			else
				qt_pont_idade_1_w := 4.4159256;

			end if;

		when 89 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1_w := 4.2366391;

			else
				qt_pont_idade_1_w := 4.4838987;

			end if;

	end case;

elsif (:new.qt_idade < 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 4.4347533;

	else
		qt_pont_idade_1_w := 4.687818;

	end if;

elsif (:new.qt_idade >= 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1_w := 4.6328675;

	else
		qt_pont_idade_1_w := 4.8917373;

	end if;

end if;


--2. PULSE in beats/minute
if (:new.qt_freq_cardiaca < 50)  then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.2145873;

	else
		qt_pont_pulso_1_w := 0.2728483;

	end if;

elsif (:new.qt_freq_cardiaca < 70) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.2687747;

	else
		qt_pont_pulso_1_w := 0.340286;

	end if;

elsif (:new.qt_freq_cardiaca < 80) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.5049219;

	else
		qt_pont_pulso_1_w := 0.6102857;

	end if;

elsif (:new.qt_freq_cardiaca < 90) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.7361786;

	else
		qt_pont_pulso_1_w := 0.8734965;

	end if;

elsif (:new.qt_freq_cardiaca < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.8628367;

	else
		qt_pont_pulso_1_w := 1.0277325;

	end if;

elsif (:new.qt_freq_cardiaca < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.8966225;

	else
		qt_pont_pulso_1_w := 1.0847059;

	end if;

elsif (:new.qt_freq_cardiaca < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.8578086;

	else
		qt_pont_pulso_1_w := 1.0764426;

	end if;

elsif (:new.qt_freq_cardiaca < 150) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.7824602;

	else
		qt_pont_pulso_1_w := 1.040713;

	end if;

elsif (:new.qt_freq_cardiaca < 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.6506006;

	else
		qt_pont_pulso_1_w := 0.9781862;

	end if;

elsif (:new.qt_freq_cardiaca >= 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1_w := 0.5545314;

	else
		qt_pont_pulso_1_w := 0.9326309;

	end if;

end if;


-- BPSYS is systolic blood pressure (mm Hg);
if (:new.qt_pa_sistolica < 80)  then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := 0.4071228;

	else
		qt_pont_pa_sis_1_w := 1.2243367;

	end if;

elsif (:new.qt_pa_sistolica < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := 0.2421043;

	else
		qt_pont_pa_sis_1_w := 0.9928463;

	end if;

elsif (:new.qt_pa_sistolica < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := 0.0063635;

	else
		qt_pont_pa_sis_1_w := 0.6621456;

	end if;

elsif (:new.qt_pa_sistolica < 120) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.1499551;

	else
		qt_pont_pa_sis_1_w := 0.4431929;

	end if;

elsif (:new.qt_pa_sistolica < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.2985642;

	else
		qt_pont_pa_sis_1_w := 0.23811;

	end if;

elsif (:new.qt_pa_sistolica < 140) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.4299701;

	else
		qt_pont_pa_sis_1_w := 0.0639759;

	end if;

elsif (:new.qt_pa_sistolica < 160) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.5769774;

	else
		qt_pont_pa_sis_1_w := -0.1070916;

	end if;

elsif (:new.qt_pa_sistolica < 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.6951633;

	else
		qt_pont_pa_sis_1_w := -0.195175;

	end if;

elsif (:new.qt_pa_sistolica >= 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1_w := -0.7399799;

	else
		qt_pont_pa_sis_1_w := -0.2104199;

	end if;

end if;


--Creatinine in mg/dl (same points as for death score);
if (bool_substituido_w = false) then
	if (:new.qt_creatinina < 0.4)  then
		qt_pont_creatinina_1_w := -0.0999551;

	elsif (:new.qt_creatinina < 0.8) then
		qt_pont_creatinina_1_w := -0.3049911;

	elsif (:new.qt_creatinina < 1.20) then
		qt_pont_creatinina_1_w := -0.2920628;

	elsif (:new.qt_creatinina < 1.60) then
		qt_pont_creatinina_1_w := 0.2625441;

	elsif (:new.qt_creatinina < 2) then
		qt_pont_creatinina_1_w := 0.4717919;

	elsif (:new.qt_creatinina < 4) then
		qt_pont_creatinina_1_w := 0.6085038;

	elsif (:new.qt_creatinina >= 4) then
		qt_pont_creatinina_1_w := 0.7131714;

	end if;

else
	if (:new.ie_killip = 'X') and (:new.ie_utiliza_creatinina <> 'S') then
		if (:new.qt_creatinina > 1.59) then
			qt_pont_creatinina_1_w := 0.44469;

		end if;

	elsif (:new.ie_insuficiencia_renal = 'S') then
		qt_pont_creatinina_1_w := 0.44469;

	end if;

end if;


-- Killip class I,II,III,IV;
if (bool_substituido_w = false) then
	if (:new.ie_killip = 'I')  then
		qt_pont_killip_1_w := 0;

	elsif (:new.ie_killip = 'II') then
		qt_pont_killip_1_w := 0.63827;

	elsif (:new.ie_killip = 'III') then
		qt_pont_killip_1_w := 0.85325;

	elsif (:new.ie_killip = 'IV') then
		qt_pont_killip_1_w := 1.29372;

	end if;

else
	if (:new.ie_killip <> 'X') and (:new.ie_utiliza_creatinina = 'S') then
		if (:new.ie_killip = 'III') or (:new.ie_killip = 'IV') then
			qt_pont_killip_1_w := 0.68008;

		end if;

	elsif (:new.ie_uso_diuretico = 'S') then
		qt_pont_killip_1_w := 0.68008;

	end if;
end if;


--Desvio do segmento ST
if	(:new.ie_desvio_seg_st = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_desvio_st_1_w := 0.44303;

	else
		qt_pont_desvio_st_1_w := 0.46524;

	end if;

end if;


--Marcadores cardiacos elevados
if	(:new.ie_marcador_cardiaco_ele = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_marcador_1_w := 0.37660;

	else
		qt_pont_marcador_1_w := 0.39944;

	end if;

end if;


--Parada cardiaca na adminiss�o
if	(:new.ie_parada_cardiaca_adm = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_parada_1_w := 0.87185;

	else
		qt_pont_parada_1_w := 1.18193;

	end if;

end if;

qt_total_ponto_1_w := TRUNC(qt_pont_idade_1_w + qt_pont_creatinina_1_w + qt_pont_pa_sis_1_w + qt_pont_pulso_1_w + qt_pont_killip_1_w + qt_pont_desvio_st_1_w + qt_pont_parada_1_w + qt_pont_marcador_1_w, 5);


--------------------------------------------------------------------------------------
--									1 ano MI										--
--------------------------------------------------------------------------------------
--Zera a pontua��o final
qt_total_ponto_1mi_w	:= 0;

--1. AGE in years
if (:new.qt_idade < 30)  then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 0.425633;

	else
		qt_pont_idade_1mi_w := 0.513068;

	end if;

elsif (:new.qt_idade < 40) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 0.499018;

	else
		qt_pont_idade_1mi_w := 0.601528;

	end if;

elsif (:new.qt_idade < 45) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 0.616434;

	else
		qt_pont_idade_1mi_w := 0.743064;

	end if;

elsif (:new.qt_idade < 50) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 0.689819;

	else
		qt_pont_idade_1mi_w := 0.831524;

	end if;

elsif (:new.qt_idade >= 50) and (:new.qt_idade <= 89) then
	case :new.qt_idade
		when 50 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.7339791;

			else
				qt_pont_idade_1mi_w := 0.8847244;

			end if;

		when 51 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.7490032;

			else
				qt_pont_idade_1mi_w := 0.9027507;

			end if;

		when 52 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.7643828;

			else
				qt_pont_idade_1mi_w := 0.9211194;

			end if;

		when 53 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.7802433;

			else
				qt_pont_idade_1mi_w := 0.9399513;

			end if;

		when 54 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.7967101;

			else
				qt_pont_idade_1mi_w := 0.9593671;

			end if;

		when 55 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.8139086;

			else
				qt_pont_idade_1mi_w := 0.9794878;

			end if;

		when 56 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.8319643;

			else
				qt_pont_idade_1mi_w := 1.0004339;

			end if;

		when 57 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.8510024;

			else
				qt_pont_idade_1mi_w := 1.0223264;

			end if;

		when 58 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.8711484;

			else
				qt_pont_idade_1mi_w := 1.0452859;

			end if;

		when 59 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.8925278;

			else
				qt_pont_idade_1mi_w := 1.0694333;

			end if;

		when 60 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.9152658;

			else
				qt_pont_idade_1mi_w := 1.0948894;

			end if;

		when 61 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.939488;

			else
				qt_pont_idade_1mi_w := 1.1217749;

			end if;

		when 62 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.9653197;

			else
				qt_pont_idade_1mi_w := 1.1502106;

			end if;

		when 63 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 0.9928863;

			else
				qt_pont_idade_1mi_w := 1.1803173;

			end if;

		when 64 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.0223132;

			else
				qt_pont_idade_1mi_w := 1.2122158;

			end if;

		when 65 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.0537258;

			else
				qt_pont_idade_1mi_w := 1.2460269;

			end if;

		when 66 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.0872495;

			else
				qt_pont_idade_1mi_w := 1.2818712;

			end if;

		when 67 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.1230062;

			else
				qt_pont_idade_1mi_w := 1.3198663;

			end if;

		when 68 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.1610029;

			else
				qt_pont_idade_1mi_w := 1.3600189;

			end if;

		when 69 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.2011081;

			else
				qt_pont_idade_1mi_w := 1.4022023;

			end if;

		when 70 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.2431822;

			else
				qt_pont_idade_1mi_w := 1.4462819;

			end if;

		when 71 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.2870853;

			else
				qt_pont_idade_1mi_w := 1.4921233;

			end if;

		when 72 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.332678;

			else
				qt_pont_idade_1mi_w := 1.539592;

			end if;

		when 73 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.3798204;

			else
				qt_pont_idade_1mi_w := 1.5885533;

			end if;

		when 74 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.4283729;

			else
				qt_pont_idade_1mi_w := 1.6388727;

			end if;

		when 75 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.4781958;

			else
				qt_pont_idade_1mi_w := 1.6904158;

			end if;

		when 76 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.5291494;

			else
				qt_pont_idade_1mi_w := 1.7430479;

			end if;

		when 77 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.5810941;

			else
				qt_pont_idade_1mi_w := 1.7966346;

			end if;

		when 78 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.6338902;

			else
				qt_pont_idade_1mi_w := 1.8510413;

			end if;

		when 79 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.6873979;

			else
				qt_pont_idade_1mi_w := 1.9061334;

			end if;

		when 80 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.7414776;

			else
				qt_pont_idade_1mi_w := 1.9617764;

			end if;

		when 81 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.7959897;

			else
				qt_pont_idade_1mi_w := 2.0178359;

			end if;

		when 82 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.8507944;

			else
				qt_pont_idade_1mi_w := 2.0741772;

			end if;

		when 83 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.905752;

			else
				qt_pont_idade_1mi_w := 2.1306658;

			end if;

		when 84 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 1.9607402;

			else
				qt_pont_idade_1mi_w := 2.1871838;

			end if;

		when 85 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 2.0157283;

			else
				qt_pont_idade_1mi_w := 2.2437019;

			end if;

		when 86 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 2.0707165;

			else
				qt_pont_idade_1mi_w := 2.3002199;

			end if;

		when 87 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 2.1257047;

			else
				qt_pont_idade_1mi_w := 2.3567379;

			end if;

		when 88 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 2.1806929;

			else
				qt_pont_idade_1mi_w := 2.413256;

			end if;

		when 89 then
			if (bool_substituido_w = false) then
				qt_pont_idade_1mi_w := 2.2356811;

			else
				qt_pont_idade_1mi_w := 2.469774;

			end if;

	end case;

elsif (:new.qt_idade < 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 2.4006456;

	else
		qt_pont_idade_1mi_w := 2.6393281;

	end if;

elsif (:new.qt_idade >= 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_1mi_w := 2.5656102;

	else
		qt_pont_idade_1mi_w := 2.8088822;

	end if;

end if;


--2. PULSE in beats/minute
if (:new.qt_freq_cardiaca < 50)  then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.06909;

	else
		qt_pont_pulso_1mi_w := 0.0504863;

	end if;

elsif (:new.qt_freq_cardiaca < 70) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.0916176;

	else
		qt_pont_pulso_1mi_w := 0.0699143;

	end if;

elsif (:new.qt_freq_cardiaca < 80) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.2734092;

	else
		qt_pont_pulso_1mi_w := 0.2646505;

	end if;

elsif (:new.qt_freq_cardiaca < 90) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.4685902;

	else
		qt_pont_pulso_1mi_w := 0.4814061;

	end if;

elsif (:new.qt_freq_cardiaca < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.5750222;

	else
		qt_pont_pulso_1mi_w := 0.608825;

	end if;

elsif (:new.qt_freq_cardiaca < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.6017448;

	else
		qt_pont_pulso_1mi_w := 0.655008;

	end if;

elsif (:new.qt_freq_cardiaca < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.5650198;

	else
		qt_pont_pulso_1mi_w := 0.6460028;

	end if;

elsif (:new.qt_freq_cardiaca < 150) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.4958011;

	else
		qt_pont_pulso_1mi_w := 0.6133558;

	end if;

elsif (:new.qt_freq_cardiaca < 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.3746683;

	else
		qt_pont_pulso_1mi_w := 0.5562236;

	end if;

elsif (:new.qt_freq_cardiaca >= 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_1mi_w := 0.2864144;

	else
		qt_pont_pulso_1mi_w := 0.5145987;

	end if;

end if;


-- BPSYS is systolic blood pressure (mm Hg);
if (:new.qt_pa_sistolica < 80)  then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.6609103;

	else
		qt_pont_pa_sis_1mi_w := 1.2166452;

	end if;

elsif (:new.qt_pa_sistolica < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.529623;

	else
		qt_pont_pa_sis_1mi_w := 1.0395593;

	end if;

elsif (:new.qt_pa_sistolica < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.3420698;

	else
		qt_pont_pa_sis_1mi_w := 0.7865796;

	end if;

elsif (:new.qt_pa_sistolica < 120) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.2178761;

	else
		qt_pont_pa_sis_1mi_w := 0.619228;

	end if;

elsif (:new.qt_pa_sistolica < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.101392;

	else
		qt_pont_pa_sis_1mi_w := 0.4637959;

	end if;

elsif (:new.qt_pa_sistolica < 140) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := 0.0021112;

	else
		qt_pont_pa_sis_1mi_w := 0.3349614;

	end if;

elsif (:new.qt_pa_sistolica < 160) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := -0.0967086;

	else
		qt_pont_pa_sis_1mi_w := 0.2191705;

	end if;

elsif (:new.qt_pa_sistolica < 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := -0.1506446;

	else
		qt_pont_pa_sis_1mi_w := 0.1851043;

	end if;

elsif (:new.qt_pa_sistolica >= 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_1mi_w := -0.1617299;

	else
		qt_pont_pa_sis_1mi_w := 0.1938601;

	end if;

end if;


--Creatinine in mg/dl (same points as for death score);
if (bool_substituido_w = false) then
	if (:new.qt_creatinina < 0.4)  then
		qt_pont_creatinina_1mi_w := -0.0311318;

	elsif (:new.qt_creatinina < 0.8) then
		qt_pont_creatinina_1mi_w := -0.0949918;

	elsif (:new.qt_creatinina < 1.20) then
		qt_pont_creatinina_1mi_w := -0.0134909;

	elsif (:new.qt_creatinina < 1.60) then
		qt_pont_creatinina_1mi_w := 0.4146596;

	elsif (:new.qt_creatinina < 2) then
		qt_pont_creatinina_1mi_w := 0.5860543;

	elsif (:new.qt_creatinina < 4) then
		qt_pont_creatinina_1mi_w := 0.7379644;

	elsif (:new.qt_creatinina >= 4) then
		qt_pont_creatinina_1mi_w := 0.8579377;

	end if;

else
	if (:new.ie_killip = 'X') and (:new.ie_utiliza_creatinina <> 'S') then
		if (:new.qt_creatinina > 1.59) then
			qt_pont_creatinina_1mi_w := 0.47766;

		end if;

	elsif (:new.ie_insuficiencia_renal = 'S') then
		qt_pont_creatinina_1mi_w := 0.47766;

	end if;

end if;


-- Killip class I,II,III,IV;
if (bool_substituido_w = false) then
	if (:new.ie_killip = 'I')  then
		qt_pont_killip_1mi_w := 0;

	elsif (:new.ie_killip = 'II') then
		qt_pont_killip_1mi_w := 0.53625;

	elsif (:new.ie_killip = 'III') then
		qt_pont_killip_1mi_w := 0.68594;

	elsif (:new.ie_killip = 'IV') then
		qt_pont_killip_1mi_w := 1.15850;

	end if;

else
	if (:new.ie_killip <> 'X') and (:new.ie_utiliza_creatinina = 'S') then
		if (:new.ie_killip = 'III') or (:new.ie_killip = 'IV') then
			qt_pont_killip_1mi_w := 0.53363;

		end if;

	elsif (:new.ie_uso_diuretico = 'S') then
		qt_pont_killip_1mi_w := 0.53363;

	end if;
end if;


--Desvio do segmento ST
if	(:new.ie_desvio_seg_st = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_desvio_st_1mi_w := 0.32831;

	else
		qt_pont_desvio_st_1mi_w := 0.34746;

	end if;

end if;


--Marcadores cardiacos elevados
if	(:new.ie_marcador_cardiaco_ele = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_marcador_1mi_w := 0.22710;

	else
		qt_pont_marcador_1mi_w := 0.25469;

	end if;

end if;


--Parada cardiaca na adminiss�o
if	(:new.ie_parada_cardiaca_adm = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_parada_1mi_w := 0.67071;

	else
		qt_pont_parada_1mi_w := 0.92949;

	end if;

end if;

qt_total_ponto_1mi_w := TRUNC(qt_pont_idade_1mi_w + qt_pont_creatinina_1mi_w + qt_pont_pa_sis_1mi_w + qt_pont_pulso_1mi_w + qt_pont_killip_1mi_w + qt_pont_desvio_st_1mi_w + qt_pont_parada_1mi_w + qt_pont_marcador_1mi_w, 5);

--------------------------------------------------------------------------------------
--									3 anos											--
--------------------------------------------------------------------------------------
--Zera a pontua��o final
qt_total_ponto_3_w	:= 0;


--1. AGE in years
if (:new.qt_idade < 30)  then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 1.767115;

	else
		qt_pont_idade_3_w := 1.710942;

	end if;

elsif (:new.qt_idade < 40) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 2.07179;

	else
		qt_pont_idade_3_w := 2.005932;

	end if;

elsif (:new.qt_idade < 45) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 2.55927;

	else
		qt_pont_idade_3_w := 2.477916;

	end if;

elsif (:new.qt_idade < 50) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 2.863945;

	else
		qt_pont_idade_3_w := 2.772906;

	end if;

elsif (:new.qt_idade >= 50) and (:new.qt_idade <= 89) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := :new.qt_idade * 0.060935;

	else
		qt_pont_idade_3_w := :new.qt_idade * 0.058998;

	end if;

elsif (:new.qt_idade < 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 5.60602;

	else
		qt_pont_idade_3_w := 5.427816;

	end if;

elsif (:new.qt_idade >= 95) then
	if (bool_substituido_w = false) then
		qt_pont_idade_3_w := 5.788825;

	else
		qt_pont_idade_3_w := 5.60481;

	end if;

end if;


--2. PULSE in beats/minute
if (:new.qt_freq_cardiaca < 50)  then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := -0.0889187;

	else
		qt_pont_pulso_3_w := -0.1549543;

	end if;

elsif (:new.qt_freq_cardiaca < 70) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := -0.0910469;

	else
		qt_pont_pulso_3_w := -0.1704667;

	end if;

elsif (:new.qt_freq_cardiaca < 80) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.2343003;

	else
		qt_pont_pulso_3_w := 0.1507527;

	end if;

elsif (:new.qt_freq_cardiaca < 90) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.6246916;

	else
		qt_pont_pulso_3_w := 0.5491089;

	end if;

elsif (:new.qt_freq_cardiaca < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.8450473;

	else
		qt_pont_pulso_3_w := 0.7759076;

	end if;

elsif (:new.qt_freq_cardiaca < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.9101415;

	else
		qt_pont_pulso_3_w := 0.8453205;

	end if;

elsif (:new.qt_freq_cardiaca < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.8581709;

	else
		qt_pont_pulso_3_w := 0.7977809;

	end if;

elsif (:new.qt_freq_cardiaca < 150) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.7494289;

	else
		qt_pont_pulso_3_w := 0.6944071;

	end if;

elsif (:new.qt_freq_cardiaca < 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.5591304;

	else
		qt_pont_pulso_3_w := 0.5135029;

	end if;

elsif (:new.qt_freq_cardiaca >= 200) then
	if (bool_substituido_w = false) then
		qt_pont_pulso_3_w := 0.4204844;

	else
		qt_pont_pulso_3_w := 0.3817013;

	end if;

end if;


-- BPSYS is systolic blood pressure (mm Hg);
if (:new.qt_pa_sistolica < 80)  then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 3.5872931;

	else
		qt_pont_pa_sis_3_w := 4.5695482;

	end if;

elsif (:new.qt_pa_sistolica < 100) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 3.0903948;

	else
		qt_pont_pa_sis_3_w := 3.9853504;

	end if;

elsif (:new.qt_pa_sistolica < 110) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 2.6394952;

	else
		qt_pont_pa_sis_3_w := 3.4167333;

	end if;

elsif (:new.qt_pa_sistolica < 120) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 2.442021;

	else
		qt_pont_pa_sis_3_w := 3.1524173;

	end if;

elsif (:new.qt_pa_sistolica < 130) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 2.2607121;

	else
		qt_pont_pa_sis_3_w := 2.9277477;

	end if;

elsif (:new.qt_pa_sistolica < 140) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 2.0407927;

	else
		qt_pont_pa_sis_3_w := 2.7001331;

	end if;

elsif (:new.qt_pa_sistolica < 160) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 1.6991854;

	else
		qt_pont_pa_sis_3_w := 2.4021178;

	end if;

elsif (:new.qt_pa_sistolica < 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 1.5533218;

	else
		qt_pont_pa_sis_3_w := 2.3409886;

	end if;

elsif (:new.qt_pa_sistolica >= 180) then
	if (bool_substituido_w = false) then
		qt_pont_pa_sis_3_w := 1.5973204;

	else
		qt_pont_pa_sis_3_w := 2.4324792;

	end if;

end if;


--Creatinine in mg/dl (same points as for death score);
if (bool_substituido_w = false) then
	if (:new.qt_creatinina < 0.4)  then
		qt_pont_creatinina_3_w := 0.0768359;

	elsif (:new.qt_creatinina < 0.8) then
		qt_pont_creatinina_3_w := 0.2344479;

	elsif (:new.qt_creatinina < 1.20) then
		qt_pont_creatinina_3_w := 0.3920599;

	elsif (:new.qt_creatinina < 1.60) then
		qt_pont_creatinina_3_w := 0.5496719;

	elsif (:new.qt_creatinina < 2) then
		qt_pont_creatinina_3_w := 0.7072839;

	elsif (:new.qt_creatinina < 4) then
		qt_pont_creatinina_3_w := 1.1801199;

	elsif (:new.qt_creatinina >= 4) then
		qt_pont_creatinina_3_w := 1.57612;

	end if;

else
	if (:new.ie_killip = 'X') and (:new.ie_utiliza_creatinina <> 'S') then
		if (:new.qt_creatinina > 1.59) then
			qt_pont_creatinina_3_w := 0.69806;

		end if;

	elsif (:new.ie_insuficiencia_renal = 'S') then
		qt_pont_creatinina_3_w := 0.69806;

	end if;

end if;


-- Killip class I,II,III,IV;
if (bool_substituido_w = false) then
	if (:new.ie_killip = 'I')  then
		qt_pont_killip_3_w := 0;

	elsif (:new.ie_killip = 'II') then
		qt_pont_killip_3_w := 0.08466;

	elsif (:new.ie_killip = 'III') then
		qt_pont_killip_3_w := 0.82842;

	elsif (:new.ie_killip = 'IV') then
		qt_pont_killip_3_w := 0.82842;

	end if;

else
	if (:new.ie_killip <> 'X') and (:new.ie_utiliza_creatinina = 'S') then
		if (:new.ie_killip = 'III') or (:new.ie_killip = 'IV') then
			qt_pont_killip_3_w := 0.68142;

		end if;

	elsif (:new.ie_uso_diuretico = 'S') then
		qt_pont_killip_3_w := 0.68142;

	end if;
end if;


--Desvio do segmento ST
if	(:new.ie_desvio_seg_st = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_desvio_st_3_w := 0.41228;

	else
		qt_pont_desvio_st_3_w := 0.32912;

	end if;

end if;


--Marcadores cardiacos elevados
if	(:new.ie_marcador_cardiaco_ele = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_marcador_3_w := 0;

	else
		qt_pont_marcador_3_w := 0;

	end if;

end if;


--Parada cardiaca na adminiss�o
if	(:new.ie_parada_cardiaca_adm = 'S') then
	if (bool_substituido_w = false) then
		qt_pont_parada_3_w := 1.07623;

	else
		qt_pont_parada_3_w := 1.17867;

	end if;

end if;

qt_total_ponto_3_w := TRUNC(qt_pont_idade_3_w + qt_pont_creatinina_3_w + qt_pont_pa_sis_3_w + qt_pont_pulso_3_w + qt_pont_killip_3_w + qt_pont_desvio_st_3_w + qt_pont_parada_3_w + qt_pont_marcador_3_w, 5);


--------------------------------------------------------------------------------------------
--								Calculo de chance										  --
--------------------------------------------------------------------------------------------

-----------------------------------
--		 Chance Hospital		 --
-----------------------------------
qt_chance_h_w := exp(qt_total_ponto_h_w);
qt_chance_h_w := (qt_chance_h_w / (1 + qt_chance_h_w)) * 100;
qt_chance_h_w := Arredondondar_Grace_II(qt_chance_h_w, 1);

if (qt_chance_h_w > 91) then

	qt_chance_h_w := 91;
	:new.IE_PONT_HOSPITAL_MAIOR := 'S';

else

	:new.IE_PONT_HOSPITAL_MAIOR := 'N';

end if;

:new.QT_PONTUACAO  := qt_chance_h_w;


-----------------------------------
--		 Chance 6 meses			 --
-----------------------------------

if (qt_total_ponto_6_w <= 6) then
	qt_chance_6_w := 0.002;

elsif (qt_total_ponto_6_w <= 27) then
	qt_chance_6_w := 0.004;

elsif (qt_total_ponto_6_w <= 39) then
	qt_chance_6_w := 0.006;

elsif (qt_total_ponto_6_w <= 48) then
	qt_chance_6_w := 0.008;

elsif (qt_total_ponto_6_w <= 55) then
	qt_chance_6_w := 0.01;

elsif (qt_total_ponto_6_w <= 60) then
	qt_chance_6_w := 0.012;

elsif (qt_total_ponto_6_w <= 65) then
	qt_chance_6_w := 0.014;

elsif (qt_total_ponto_6_w <= 69) then
	qt_chance_6_w := 0.016;

elsif (qt_total_ponto_6_w <= 73) then
	qt_chance_6_w := 0.018;

elsif (qt_total_ponto_6_w <= 76) then
	qt_chance_6_w := 0.02;

elsif (qt_total_ponto_6_w <= 88) then
	qt_chance_6_w := 0.03;

elsif (qt_total_ponto_6_w <= 97) then
	qt_chance_6_w := 0.04;

elsif (qt_total_ponto_6_w <= 104) then
	qt_chance_6_w := 0.05;

elsif (qt_total_ponto_6_w <= 110) then
	qt_chance_6_w := 0.06;

elsif (qt_total_ponto_6_w <= 115) then
	qt_chance_6_w := 0.07;

elsif (qt_total_ponto_6_w <= 119) then
	qt_chance_6_w := 0.08;

elsif (qt_total_ponto_6_w <= 123) then
	qt_chance_6_w := 0.09;

elsif (qt_total_ponto_6_w <= 126) then
	qt_chance_6_w := 0.10;

elsif (qt_total_ponto_6_w <= 129) then
	qt_chance_6_w := 0.11;

elsif (qt_total_ponto_6_w <= 132) then
	qt_chance_6_w := 0.12;

elsif (qt_total_ponto_6_w <= 134) then
	qt_chance_6_w := 0.13;

elsif (qt_total_ponto_6_w <= 137) then
	qt_chance_6_w := 0.14;

elsif (qt_total_ponto_6_w <= 139) then
	qt_chance_6_w := 0.15;

elsif (qt_total_ponto_6_w <= 141) then
	qt_chance_6_w := 0.16;

elsif (qt_total_ponto_6_w <= 143) then
	qt_chance_6_w := 0.17;

elsif (qt_total_ponto_6_w <= 146) then
	qt_chance_6_w := 0.18;

elsif (qt_total_ponto_6_w <= 147) then
	qt_chance_6_w := 0.19;

elsif (qt_total_ponto_6_w <= 149) then
	qt_chance_6_w := 0.20;

elsif (qt_total_ponto_6_w <= 150) then
	qt_chance_6_w := 0.21;

elsif (qt_total_ponto_6_w <= 152) then
	qt_chance_6_w := 0.22;

elsif (qt_total_ponto_6_w <= 153) then
	qt_chance_6_w := 0.23;

elsif (qt_total_ponto_6_w <= 155) then
	qt_chance_6_w := 0.24;

elsif (qt_total_ponto_6_w <= 156) then
	qt_chance_6_w := 0.25;

elsif (qt_total_ponto_6_w <= 158) then
	qt_chance_6_w := 0.26;

elsif (qt_total_ponto_6_w <= 159) then
	qt_chance_6_w := 0.27;

elsif (qt_total_ponto_6_w <= 160) then
	qt_chance_6_w := 0.28;

elsif (qt_total_ponto_6_w <= 162) then
	qt_chance_6_w := 0.29;

elsif (qt_total_ponto_6_w <= 163) then
	qt_chance_6_w := 0.3;

elsif (qt_total_ponto_6_w <= 174) then
	qt_chance_6_w := 0.4;

elsif (qt_total_ponto_6_w <= 183) then
	qt_chance_6_w := 0.5;

elsif (qt_total_ponto_6_w <= 191) then
	qt_chance_6_w := 0.6;

elsif (qt_total_ponto_6_w <= 200) then
	qt_chance_6_w := 0.7;

elsif (qt_total_ponto_6_w <= 208) then
	qt_chance_6_w := 0.8;

elsif (qt_total_ponto_6_w <= 219) then
	qt_chance_6_w := 0.9;

elsif (qt_total_ponto_6_w <= 240) then
	qt_chance_6_w := 0.99;

elsif (qt_total_ponto_6_w > 240) then
	qt_chance_6_w := 1.13;

end if;

:new.QT_PONTUACAO_SCORE_6 := qt_total_ponto_6_w;

qt_chance_6_w := ((qt_chance_6_w * 100) * (80 / 91));

if (qt_chance_6_w > 90) then
	qt_chance_6_w := 92;

end if;
qt_chance_6_w := Arredondondar_Grace_II(qt_chance_6_w, 1);


:new.QT_PONTUACAO_6  := qt_chance_6_w;

--Valor a ser exibido ser� calculado posteriormente, junto com o calculo de 1 ano


-----------------------------------
--				1 ano			 --
-----------------------------------
qt_chance_1_w := exp(qt_total_ponto_1_w);

if (bool_substituido_w = false) then
	qt_chance_1_w := power(0.9983577131, qt_chance_1_w);

else
	qt_chance_1_w := power(0.9994190637, qt_chance_1_w);

end if;

qt_chance_1_w := 1 - qt_chance_1_w;
qt_chance_1_w := qt_chance_1_w * 100;

qt_chance_1_w := Arredondondar_Grace_II(qt_chance_1_w, 1);


:new.QT_PONTUACAO_1  := qt_chance_1_w;

-------------------------------------------------------------rft--------------
--				Verifica se a % de 6 meses � maior que 1 ano			 --
---------------------------------------------------------------------------
if (qt_chance_6_w > 91) then

	qt_chance_6_w := 91;

	:new.IE_PONT_6_meses_MAIOR := 'S';

	:new.QT_PONTUACAO_6  := qt_chance_6_w;

else

	:new.IE_PONT_6_meses_MAIOR := 'N';

end if;



if (qt_chance_1_w > 91) then

	qt_chance_1_w := 91;

	:new.IE_PONT_1_ANO_MAIOR := 'S';

	:new.QT_PONTUACAO_1  := qt_chance_1_w;

else

	:new.IE_PONT_1_ANO_MAIOR := 'N';

end if;


-----------------------------------
--			  1 ano MI			 --
-----------------------------------
qt_chance_1mi_w := exp(qt_total_ponto_1mi_w);

if (bool_substituido_w = false) then
	qt_chance_1mi_w := power(0.9865696068, qt_chance_1mi_w);

else
	qt_chance_1mi_w := power(0.9919559302, qt_chance_1mi_w);

end if;

qt_chance_1mi_w := 1 - qt_chance_1mi_w;
qt_chance_1mi_w := qt_chance_1mi_w * 100;

qt_chance_1mi_w := Arredondondar_Grace_II(qt_chance_1mi_w, 1);

---------------------------------------------------------------------------
--				Verifica se a % de 1 ano MI � maior que 1 ano			 --
---------------------------------------------------------------------------
if (qt_chance_1mi_w > 91) then

	:new.IE_PONT_1_ANO_MI_MAIOR := 'S';

	qt_chance_1mi_w := 91;

else

	:new.IE_PONT_1_ANO_MI_MAIOR := 'N';

end if;

:new.QT_PONTUACAO_MI  := qt_chance_1mi_w;

-----------------------------------
--				3 anos			 --
-----------------------------------
qt_chance_3_w := exp(qt_total_ponto_3_w);

if (bool_substituido_w = false) then
	qt_chance_3_w := trunc(power(0.9998715509, qt_chance_3_w),11);

else
	qt_chance_3_w := trunc(power(0.9998961080, qt_chance_3_w),11);

end if;


qt_chance_3_w := 1 - qt_chance_3_w;
qt_chance_3_w := qt_chance_3_w * 100;

qt_chance_3_w := Arredondondar_Grace_II(qt_chance_3_w, 1);

---------------------------------------------------------------------------
--				Verifica se a % � maior que 91							 --
---------------------------------------------------------------------------
if (qt_chance_3_w > 91) then

	:new.IE_PONT_3_ANO_MAIOR := 'S';

	qt_chance_3_w := 91;

else

	:new.IE_PONT_3_ANO_MAIOR := 'N';

end if;

:new.QT_PONTUACAO_3  := qt_chance_3_w;


end;
/


ALTER TABLE TASY.ESCALA_GRACE_II ADD (
  CONSTRAINT ESCGRA2_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_GRACE_II ADD (
  CONSTRAINT ESCGRA2_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCGRA2_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_GRACE_II TO NIVEL_1;

