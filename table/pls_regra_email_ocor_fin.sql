ALTER TABLE TASY.PLS_REGRA_EMAIL_OCOR_FIN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_EMAIL_OCOR_FIN CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_EMAIL_OCOR_FIN
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO        NUMBER(10),
  DS_EMAIL_ORIGEM      VARCHAR2(255 BYTE)       NOT NULL,
  IE_EMAIL_DESTINO     VARCHAR2(5 BYTE)         NOT NULL,
  DS_MENSAGEM          VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREOF_ESTABEL_FK_I ON TASY.PLS_REGRA_EMAIL_OCOR_FIN
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREOF_PK ON TASY.PLS_REGRA_EMAIL_OCOR_FIN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREOF_PLSEVEN_FK_I ON TASY.PLS_REGRA_EMAIL_OCOR_FIN
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_EMAIL_OCOR_FIN ADD (
  CONSTRAINT PLSREOF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_EMAIL_OCOR_FIN ADD (
  CONSTRAINT PLSREOF_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREOF_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_EMAIL_OCOR_FIN TO NIVEL_1;

