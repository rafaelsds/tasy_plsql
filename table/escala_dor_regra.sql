ALTER TABLE TASY.ESCALA_DOR_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DOR_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DOR_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ESCALA            VARCHAR2(30 BYTE)        NOT NULL,
  IE_TOPOGRAFIA        VARCHAR2(2 BYTE),
  IE_LADO              VARCHAR2(2 BYTE),
  IE_CONDICAO          VARCHAR2(2 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  QT_INTENSIDADE       NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESCDORE_PK ON TASY.ESCALA_DOR_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESCALA_DOR_REGRA ADD (
  CONSTRAINT ESCDORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ESCALA_DOR_REGRA TO NIVEL_1;

