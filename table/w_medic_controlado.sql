ALTER TABLE TASY.W_MEDIC_CONTROLADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_MEDIC_CONTROLADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MEDIC_CONTROLADO
(
  CD_MATERIAL        NUMBER(6),
  QT_SALDO_ANTERIOR  NUMBER(15,4),
  QT_PRODUCAO        NUMBER(15,4),
  QT_AQUISICAO       NUMBER(15,4),
  QT_IMPORTACAO      NUMBER(15,4),
  QT_SOMA            NUMBER(15,4),
  QT_SAIDA           NUMBER(15,4),
  QT_SALDO_ATUAL     NUMBER(15,4)
)
TABLESPACE TASY_WORK
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WMEDCON_PK ON TASY.W_MEDIC_CONTROLADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_WORK
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WMEDCON_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_MEDIC_CONTROLADO ADD (
  CONSTRAINT WMEDCON_PK
 PRIMARY KEY
 (CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_WORK
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_MEDIC_CONTROLADO TO NIVEL_1;

