ALTER TABLE TASY.SAN_GERAR_SOLIC_HEMOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_GERAR_SOLIC_HEMOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_GERAR_SOLIC_HEMOC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DERIVADO      NUMBER(10)               NOT NULL,
  IE_TIPO_SANGUE       VARCHAR2(2 BYTE)         NOT NULL,
  IE_FATOR_RH          VARCHAR2(1 BYTE)         NOT NULL,
  QT_ESTOQUE_MIN       NUMBER(5)                NOT NULL,
  QT_ESTOQUE_MAX       NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANGSOLHEM_PK ON TASY.SAN_GERAR_SOLIC_HEMOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_GERAR_SOLIC_HEMOC ADD (
  CONSTRAINT SANGSOLHEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SAN_GERAR_SOLIC_HEMOC TO NIVEL_1;

