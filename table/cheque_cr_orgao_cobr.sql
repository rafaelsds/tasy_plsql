ALTER TABLE TASY.CHEQUE_CR_ORGAO_COBR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE_CR_ORGAO_COBR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE_CR_ORGAO_COBR
(
  IE_SELECIONADO         VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CHEQUE          NUMBER(10)             NOT NULL,
  CD_CGC                 VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  NR_CHEQUE              VARCHAR2(20 BYTE)      NOT NULL,
  VL_CHEQUE              NUMBER(15,2)           NOT NULL,
  DT_LIBERACAO_ENVIO     DATE,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_LIBERACAO_EXCLUSAO  DATE,
  NR_SEQ_REGRA           NUMBER(5),
  NR_SEQ_LOTE            NUMBER(5),
  NR_SEQ_ORGAO_COBR      NUMBER(10)             NOT NULL,
  NR_SEQ_COBRANCA        NUMBER(10),
  DT_ENVIO               DATE,
  DT_EXCLUSAO            DATE,
  NR_SEQ_LOTE_EXC        NUMBER(5),
  NR_SEQ_PERDA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEORGCO_CHEQUES_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_COBRORG_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_LOTORGCO_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_ORGCOBR_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_ORGAO_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_PESFISI_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_PESJURI_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHEORGCO_PK ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_PRDACRE_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEORGCO_REGAPROR_FK_I ON TASY.CHEQUE_CR_ORGAO_COBR
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CHEQUE_CR_ORGAO_COBR ADD (
  CONSTRAINT CHEORGCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE_CR_ORGAO_COBR ADD (
  CONSTRAINT CHEORGCO_COBRORG_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA (NR_SEQUENCIA),
  CONSTRAINT CHEORGCO_PRDACRE_FK 
 FOREIGN KEY (NR_SEQ_PERDA) 
 REFERENCES TASY.PERDA_CONTAS_RECEBER (NR_SEQUENCIA),
  CONSTRAINT CHEORGCO_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE),
  CONSTRAINT CHEORGCO_LOTORGCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_ORGAO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT CHEORGCO_ORGCOBR_FK 
 FOREIGN KEY (NR_SEQ_ORGAO_COBR) 
 REFERENCES TASY.ORGAO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT CHEORGCO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHEORGCO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CHEORGCO_REGAPROR_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_APROVACAO_ORGAO_COBR (NR_SEQUENCIA));

GRANT SELECT ON TASY.CHEQUE_CR_ORGAO_COBR TO NIVEL_1;

