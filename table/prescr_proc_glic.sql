ALTER TABLE TASY.PRESCR_PROC_GLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_GLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_GLIC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_PRESCRICAO           NUMBER(14)            NOT NULL,
  NR_SEQ_PROCEDIMENTO     NUMBER(6)             NOT NULL,
  NR_SEQ_PROTOCOLO        NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE                  NOT NULL,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE)     NOT NULL,
  QT_GLIC_INIC            NUMBER(10,1)          NOT NULL,
  QT_GLIC_FIM             NUMBER(10,1)          NOT NULL,
  QT_UI_INSULINA          NUMBER(10,2),
  DS_SUGESTAO             VARCHAR2(2000 BYTE),
  QT_GLICOSE              NUMBER(5,1),
  QT_MINUTOS_MEDICAO      NUMBER(10),
  NR_PRESCRICAO_ORIGINAL  NUMBER(15),
  QT_UI_INSULINA_INT      NUMBER(10,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREPRGL_PEPPRGL_FK_I ON TASY.PRESCR_PROC_GLIC
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPRGL_PEPPRGL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREPRGL_PK ON TASY.PRESCR_PROC_GLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPRGL_PRESPRO_FK_I ON TASY.PRESCR_PROC_GLIC
(NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_proc_glic_update
before update ON TASY.PRESCR_PROC_GLIC for each row
declare
ds_log_w	varchar2(1800) := '';
begin
/*
if 	(nvl(:new.QT_GLIC_INIC,0)	<> nvl(:old.QT_GLIC_INIC,0)) then
	ds_log_w := ':new.QT_GLIC_INIC='||:new.QT_GLIC_INIC||' :old.QT_GLIC_INIC='||:old.QT_GLIC_INIC;
end if;
if	(nvl(:new.QT_GLIC_FIM,0)	<> nvl(:old.QT_GLIC_FIM,0)) then
	ds_log_w := ds_log_w||chr(13)||':new.QT_GLIC_FIM='||:new.QT_GLIC_FIM||' :old.QT_GLIC_FIM='||:old.QT_GLIC_FIM;
end if;
if	(nvl(:new.QT_UI_INSULINA,0)	<> nvl(:old.QT_UI_INSULINA,0)) then
	ds_log_w := ds_log_w||chr(13)||':new.QT_UI_INSULINA='||:new.QT_UI_INSULINA||' :old.QT_UI_INSULINA='||:old.QT_UI_INSULINA;
end if;
if	(nvl(:new.QT_GLICOSE,0)		<> nvl(:old.QT_GLICOSE,0)) then
	ds_log_w := ds_log_w||chr(13)||':new.QT_GLICOSE='||:new.QT_GLICOSE||' :old.QT_GLICOSE='||:old.QT_GLICOSE;
end if;
*/
null;
end;
/


ALTER TABLE TASY.PRESCR_PROC_GLIC ADD (
  CONSTRAINT PREPRGL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_GLIC ADD (
  CONSTRAINT PREPRGL_PEPPRGL_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PEP_PROTOCOLO_GLICEMIA (NR_SEQUENCIA),
  CONSTRAINT PREPRGL_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_PROC_GLIC TO NIVEL_1;

