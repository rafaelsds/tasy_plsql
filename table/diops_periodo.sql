ALTER TABLE TASY.DIOPS_PERIODO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_PERIODO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_PERIODO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_PERIODO              VARCHAR2(60 BYTE)     NOT NULL,
  DT_PERIODO_INICIAL      DATE                  NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_PERIODO_FINAL        DATE,
  NM_USUARIO_ENVIO        VARCHAR2(15 BYTE),
  DS_MAQUINA_ENVIO        VARCHAR2(80 BYTE),
  DT_ENVIO                DATE,
  DT_INICIO_GERACAO       DATE,
  DT_FIM_GERACAO          DATE,
  IE_FLUXO_CAIXA          VARCHAR2(2 BYTE),
  IE_IDADE_SALDO          VARCHAR2(2 BYTE),
  IE_LUCRO_PREJUIZO       VARCHAR2(2 BYTE),
  IE_SOLVENCIA            VARCHAR2(2 BYTE),
  IE_BALANCETE_ATIVO      VARCHAR2(2 BYTE),
  IE_BALANCETE_PASSIVO    VARCHAR2(2 BYTE),
  IE_BALANCETE_RECEITA    VARCHAR2(2 BYTE),
  IE_BALANCETE_DESPESA    VARCHAR2(2 BYTE),
  IE_TIPO_PERIODO_DIOPS   VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  IE_INTERCAMBIO          VARCHAR2(2 BYTE),
  IE_NORMAL_ENCERRAMENTO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIOPERI_ESTABEL_FK_I ON TASY.DIOPS_PERIODO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIOPERI_PK ON TASY.DIOPS_PERIODO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIOPS_PERIODO_UPDATE
before update ON TASY.DIOPS_PERIODO for each row
declare

ds_maquina_w		Varchar2(80);
nm_usuario_maquina_w	Varchar2(30);

begin

ds_maquina_w		:= substr(obter_inf_sessao(0),1,80);
nm_usuario_maquina_w	:= substr(obter_inf_sessao(1),1,30);

if	(:new.ie_fluxo_caixa		<> :old.ie_fluxo_caixa) or
	(:new.ie_idade_saldo		<> :old.ie_idade_saldo) or
	(:new.ie_lucro_prejuizo		<> :old.ie_lucro_prejuizo) or
	(:new.ie_solvencia		<> :old.ie_solvencia) or
	(:new.ie_balancete_ativo	<> :old.ie_balancete_ativo) or
	(:new.ie_balancete_passivo	<> :old.ie_balancete_passivo) or
	(:new.ie_balancete_receita	<> :old.ie_balancete_receita) or
	(:new.ie_balancete_despesa	<> :old.ie_balancete_despesa) then

	/*
	insert into logXXXX_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values(	sysdate, :new.nm_usuario, 1227,
	' NR_SEQUENCIA= '		|| :old.nr_sequencia		||
	' MAQUINA= '			|| ds_maquina_w			||
	' OUSER= '			|| nm_usuario_maquina_w 	||
	' ie_fluxo_caixa= ' 		|| :old.ie_fluxo_caixa 		|| ' - ' || :new.ie_fluxo_caixa 	||
	' ie_idade_saldo= '		|| :old.ie_idade_saldo 		|| ' - ' || :new.ie_idade_saldo 	||
	' ie_lucro_prejuizo= '		|| :old.ie_lucro_prejuizo 	|| ' - ' || :new.ie_lucro_prejuizo 	||
	' ie_solvencia= '		|| :old.ie_solvencia 		|| ' - ' || :new.ie_solvencia 		||
	' ie_balancete_ativo= '		|| :old.ie_balancete_ativo 	|| ' - ' || :new.ie_balancete_ativo 	||
	' ie_balancete_passivo= '	|| :old.ie_balancete_passivo	|| ' - ' || :new.ie_balancete_passivo 	||
	' ie_balancete_receita= '	|| :old.ie_balancete_receita 	|| ' - ' || :new.ie_balancete_receita	||
	' ie_balancete_despesa= '	|| :old.ie_balancete_despesa 	|| ' - ' || :new.ie_balancete_despesa);
	*/
	:new.ie_fluxo_caixa		:= :old.ie_fluxo_caixa;
	:new.ie_idade_saldo		:= :old.ie_idade_saldo;
	:new.ie_lucro_prejuizo		:= :old.ie_lucro_prejuizo;
	:new.ie_solvencia		:= :old.ie_solvencia;
	:new.ie_balancete_ativo		:= :old.ie_balancete_ativo;
	:new.ie_balancete_passivo	:= :old.ie_balancete_passivo;
	:new.ie_balancete_receita	:= :old.ie_balancete_receita;
	:new.ie_balancete_despesa	:= :old.ie_balancete_despesa;
end if;

end;
/


ALTER TABLE TASY.DIOPS_PERIODO ADD (
  CONSTRAINT DIOPERI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIOPS_PERIODO ADD (
  CONSTRAINT DIOPERI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.DIOPS_PERIODO TO NIVEL_1;

