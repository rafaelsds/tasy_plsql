ALTER TABLE TASY.PF_TIPO_DEFICIENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_TIPO_DEFICIENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_TIPO_DEFICIENCIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_DEF            NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  IE_ALERTA                  VARCHAR2(1 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_NEGA_DEFICIENCIA        VARCHAR2(1 BYTE),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_CAUSA_LESAO         NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_GRAU_DEFICIENCIA        NUMBER(1),
  CD_CIF                     VARCHAR2(10 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  CD_CUD                     NUMBER(11),
  CD_CERTIFICADO_DEF         NUMBER(10),
  DS_CERTIFICADO_DEF         VARCHAR2(250 BYTE),
  DS_CERTIFICACAO_DEF        VARCHAR2(250 BYTE),
  IE_LADO                    VARCHAR2(2 BYTE),
  DS_ASSISTANCE_METHOD       VARCHAR2(4000 BYTE),
  IE_GUIDE_DOG               VARCHAR2(1 BYTE),
  IE_SIGN_LANGUAGE           VARCHAR2(1 BYTE),
  IE_ANARTHRIA               VARCHAR2(1 BYTE),
  IE_APHASIA                 VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFTIPDE_ATCONSPEPA_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_CLACIF_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(CD_CIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_EHRREEL_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFTIPDE_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PFTIPDE_HISTSAU_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_PESFISI_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_PESFISI_FK2_I ON TASY.PF_TIPO_DEFICIENCIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_PFCALES_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_CAUSA_LESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFTIPDE_PK ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFTIPDE_TASASDI_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFTIPDE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PFTIPDE_TASASDI_FK2_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFTIPDE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PFTIPDE_TIPDEFI_FK_I ON TASY.PF_TIPO_DEFICIENCIA
(NR_SEQ_TIPO_DEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFTIPDE_TIPDEFI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pf_tipo_defic_pend_atual 
after insert or update ON TASY.PF_TIPO_DEFICIENCIA 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSD'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSD'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PF_TIPO_DEFICIENCIA_tp  after update ON TASY.PF_TIPO_DEFICIENCIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PF_TIPO_DEFICIENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_DEF,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_DEF,1,4000),substr(:new.NR_SEQ_TIPO_DEF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_DEF',ie_log_w,ds_w,'PF_TIPO_DEFICIENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PF_TIPO_DEFICIENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PF_TIPO_DEFICIENCIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PF_TIPO_DEFICIENCIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PF_TIPO_DEFICIENCIA_ATUAL
before update or insert ON TASY.PF_TIPO_DEFICIENCIA for each row
declare


begin
if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	:new.NM_USUARIO_LIBERACAO := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO_NREC, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


ALTER TABLE TASY.PF_TIPO_DEFICIENCIA ADD (
  CONSTRAINT PFTIPDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_TIPO_DEFICIENCIA ADD (
  CONSTRAINT PFTIPDE_CLACIF_FK 
 FOREIGN KEY (CD_CIF) 
 REFERENCES TASY.CLASSIFICACAO_CIF (CD_CIF),
  CONSTRAINT PFTIPDE_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PFTIPDE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PFTIPDE_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFTIPDE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFTIPDE_TIPDEFI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DEF) 
 REFERENCES TASY.TIPO_DEFICIENCIA (NR_SEQUENCIA),
  CONSTRAINT PFTIPDE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PFTIPDE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PFTIPDE_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFTIPDE_PFCALES_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_LESAO) 
 REFERENCES TASY.PF_CAUSA_LESAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PF_TIPO_DEFICIENCIA TO NIVEL_1;

