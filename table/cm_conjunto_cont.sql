ALTER TABLE TASY.CM_CONJUNTO_CONT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CONJUNTO_CONT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CONJUNTO_CONT
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_SEQ_CONJUNTO            NUMBER(10)         DEFAULT null,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_STATUS_CONJUNTO         NUMBER(3)          NOT NULL,
  DT_ORIGEM                  DATE               NOT NULL,
  NM_USUARIO_ORIGEM          VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_EMBALAGEM           NUMBER(10)         NOT NULL,
  DT_RECEB_ESTER             DATE,
  NM_USUARIO_ESTER           VARCHAR2(15 BYTE),
  NR_SEQ_CICLO               NUMBER(10),
  DT_VALIDADE                DATE,
  NR_SEQ_CONJ_FISICO         NUMBER(10),
  DT_SAIDA                   DATE,
  NM_USUARIO_SAIDA           VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA              NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  CD_CGC                     VARCHAR2(14 BYTE),
  CD_LOCAL_ESTOQUE           NUMBER(4),
  CD_SETOR_ATEND_ORIG        NUMBER(5),
  CD_SETOR_ATEND_DEST        NUMBER(5),
  DT_ALOC_AGENDA             DATE,
  NM_USUARIO_AGENDA          VARCHAR2(15 BYTE),
  DT_BAIXA_CIRURGIA          DATE,
  NR_CIRURGIA                NUMBER(10),
  QT_PONTO                   NUMBER(15,4)       NOT NULL,
  VL_ESTERILIZACAO           NUMBER(15,2)       NOT NULL,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_CLIENTE             NUMBER(10),
  NR_SEQ_CONTROLE            NUMBER(10),
  NR_SEQ_LOTE                NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  CD_PESSOA_RESP             VARCHAR2(10 BYTE),
  IE_TIPO_ESTERILIZACAO      VARCHAR2(2 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DT_LANCAMENTO_AUTOMATICO   DATE,
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_PEPO                NUMBER(10),
  NM_USUARIO_VINC            VARCHAR2(15 BYTE),
  DT_VINCULO_CONJ            DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  DT_LIBERACAO               DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NM_USUARIO_INICIO          VARCHAR2(15 BYTE),
  NM_USUARIO_FIM             VARCHAR2(15 BYTE),
  NM_USUARIO_REPASSE         VARCHAR2(15 BYTE),
  NR_SEQ_CONJ_ORIGEM         NUMBER(10),
  CD_MEDICO                  VARCHAR2(10 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NM_PESSOA_RECEB            VARCHAR2(255 BYTE),
  CD_PESSOA_RECEB            VARCHAR2(10 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_CONTROLE_CONJ           VARCHAR2(255 BYTE),
  NR_SEQ_POSICAO_CONJ        NUMBER(10),
  NR_SEQ_CICLO_LAV           NUMBER(10),
  IE_EXPURGO                 VARCHAR2(1 BYTE),
  NM_USUARIO_BAIXA_CIRURGIA  VARCHAR2(15 BYTE),
  NR_SEQ_ENVIO_EXT_ITEM      NUMBER(10),
  DT_IMPRESSAO               DATE,
  IE_INDETERMINADO           VARCHAR2(1 BYTE),
  NR_SEQ_RECEB_EXT           NUMBER(10),
  NR_SEQ_REGRA_PEPO          NUMBER(10),
  DT_CONFERENCIA_ITENS       DATE,
  NM_CONJUNTO_AUX            VARCHAR2(200 BYTE),
  DS_EQUIPAMENTO             VARCHAR2(80 BYTE),
  NR_CICLO_AUX               NUMBER(10),
  DS_CLASSIFICACAO_AUX       VARCHAR2(200 BYTE),
  DS_STATUS_CONJUNTO_AUX     VARCHAR2(200 BYTE),
  DS_EMBALAGEM_AUX           VARCHAR2(50 BYTE),
  DS_LOCAL_ESTOQUE_AUX       VARCHAR2(40 BYTE),
  NM_PESSOA_FISICA_AUX       VARCHAR2(60 BYTE),
  DS_SETOR_ATEND_ORIG_AUX    VARCHAR2(255 BYTE),
  DS_SETOR_REQUISICAO_AUX    VARCHAR2(255 BYTE),
  DS_RAZAO_SOCIAL_AUX        VARCHAR2(80 BYTE),
  DS_COR_CONJUNTO_AUX        VARCHAR2(15 BYTE),
  NM_MEDICO_AUX              VARCHAR2(255 BYTE),
  NM_PESSOA_RESP_AUX         VARCHAR2(60 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_ITEM                NUMBER(10),
  IE_PROCESSO_LIMPEZA        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCOCON_AGEPACI_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_ATEPACI_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_CIRURGI_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCICLO_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CICLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMCICLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCILAV_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CICLO_LAV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMCILAV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCLEMB_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_EMBALAGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMCLEMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCLIEN_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMCLIEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCONFI_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CONJ_FISICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMCONFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMCONJU_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CONJUNTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_CMENEIT_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_ENVIO_EXT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMENEIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMITEM_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_CMLOTCON_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMLOTCON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_CMPOCON_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_POSICAO_CONJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_CMPOCON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_ESTABEL_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_I1 ON TASY.CM_CONJUNTO_CONT
(IE_STATUS_CONJUNTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_I1
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_I2 ON TASY.CM_CONJUNTO_CONT
(DT_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_I2
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_I3 ON TASY.CM_CONJUNTO_CONT
(IE_STATUS_CONJUNTO, DT_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_I3
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_I4 ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_CONTROLE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_I4
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_I5 ON TASY.CM_CONJUNTO_CONT
(CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO, DT_ORIGEM, NR_SEQ_LOTE, IE_STATUS_CONJUNTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_I6 ON TASY.CM_CONJUNTO_CONT
(CD_SETOR_ATENDIMENTO, DT_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_LOCEST_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_LOCAL_ESTOQUE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_LOCEST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_MEDICO_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_PEPOCIR_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_PERFIL_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_PESFISI_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_PESFISI_FK2_I ON TASY.CM_CONJUNTO_CONT
(CD_PESSOA_RESP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_PESJURI_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCOCON_PK ON TASY.CM_CONJUNTO_CONT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMCOCON_REGLANP_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_REGRA_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_REGLANP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_SETATEN_FK_I ON TASY.CM_CONJUNTO_CONT
(CD_SETOR_ATEND_ORIG)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_SETATEN_FK2_I ON TASY.CM_CONJUNTO_CONT
(CD_SETOR_ATEND_DEST)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_SETATEN_FK9_I ON TASY.CM_CONJUNTO_CONT
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_SETATEN_FK9_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_TASASDI_FK_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMCOCON_TASASDI_FK2_I ON TASY.CM_CONJUNTO_CONT
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCOCON_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_conjunto_cont_atual
before update ON TASY.CM_CONJUNTO_CONT for each row
declare
ds_status_ant_w		varchar2(255);
ds_status_novo_w		varchar2(255);
ds_local_estoque_w	varchar2(255);
ds_equipamento_w		varchar2(255);
nr_seq_equipamento_w	cm_equipamento.nr_sequencia%type;
ie_se_conjunto_equip_w	varchar2(1);

begin

if	(nvl(:old.DT_ORIGEM,sysdate+10) <> :new.DT_ORIGEM) and
	(:new.DT_ORIGEM is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ORIGEM, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.ie_status_conjunto <> :old.ie_status_conjunto) then
	begin

	select	substr(obter_valor_dominio(403,:old.ie_status_conjunto),1,255),
		substr(obter_valor_dominio(403,:new.ie_status_conjunto),1,255)
	into	ds_status_ant_w,
		ds_status_novo_w
	from	dual;

	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311255,'DS_STATUS_ANT='||ds_status_ant_w||';DS_STATUS_NOVO='||ds_status_novo_w),:new.nm_usuario);

	update	cm_expurgo_retirada
	set	ie_status = :new.ie_status_conjunto
	where	nr_conjunto_cont = :new.nr_sequencia;

	update	cm_expurgo_receb
	set	ie_status = :new.ie_status_conjunto
	where	nr_conjunto_cont = :new.nr_sequencia;

	end;
end if;

if	(:new.cd_local_estoque <> :old.cd_local_estoque) then
	begin

	select	ds_local_estoque
	into	ds_local_estoque_w
	from	local_estoque
	where	cd_local_estoque = :new.cd_local_estoque;

	if	(ds_local_estoque_w is not null) then
		begin

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311257,'DS_LOCAL_ESTOQUE='||ds_local_estoque_w),:new.nm_usuario);

		:new.nm_usuario_repasse	:= wheb_usuario_pck.get_nm_usuario;

		if	(:new.nr_seq_ciclo is not null) then
			cm_gerar_hist_ciclo(wheb_mensagem_pck.get_texto(311258,'NR_SEQUENCIA='||:new.nr_sequencia||';DS_LOCAL_ESTOQUE='||ds_local_estoque_w),'E',:new.nr_seq_ciclo,null,:new.nm_usuario);
		elsif	(:new.nr_seq_ciclo_lav is not null) then
			cm_gerar_hist_ciclo(wheb_mensagem_pck.get_texto(311258,'NR_SEQUENCIA='||:new.nr_sequencia||';DS_LOCAL_ESTOQUE='||ds_local_estoque_w),'E',null,:new.nr_seq_ciclo,:new.nm_usuario);
		end if;

		end;
	end if;

	end;
end if;

if	(nvl(:new.nr_seq_ciclo,0) <> nvl(:old.nr_seq_ciclo,0)) then
	begin

	if	(:old.nr_seq_ciclo is null) and
		(:new.nr_seq_ciclo is not null) then
		begin

		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150),
			nr_seq_equipamento
		into	ds_equipamento_w,
			nr_seq_equipamento_w
		from	cm_ciclo
		where	nr_sequencia = :new.nr_seq_ciclo;

		ie_se_conjunto_equip_w := obter_se_conjunto_equip(:new.nr_seq_conjunto, nr_seq_equipamento_w);

		if	(ie_se_conjunto_equip_w = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(767182);
		end if;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311325) || ' ' || :new.nr_seq_ciclo ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);

		end;
	elsif	(:old.nr_seq_ciclo is not null) and
		(:new.nr_seq_ciclo is null) then
		begin

		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo
		where	nr_sequencia = :old.nr_seq_ciclo;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311330) || ' ' || :old.nr_seq_ciclo ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);

		end;
	elsif	(:old.nr_seq_ciclo_lav is null) and
		(:new.nr_seq_ciclo_lav is not null) and (1=2) then
		begin

		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo_lavacao
		where	nr_sequencia = :new.nr_seq_ciclo_lav;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311325) || ' ' || :new.nr_seq_ciclo_lav ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);

		end;
	elsif	(:old.nr_seq_ciclo_lav is not null) and
		(:new.nr_seq_ciclo_lav is null) then
		begin

		select	substr(cme_obter_desc_equip(nr_seq_equipamento),1,150)
		into	ds_equipamento_w
		from	cm_ciclo_lavacao
		where	nr_sequencia = :old.nr_seq_ciclo_lav;

		cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311330) || ' ' || :old.nr_seq_ciclo_lav ||
							' - ' || wheb_mensagem_pck.get_texto(311259) || ': ' || ds_equipamento_w,:new.nm_usuario);

		end;
	end if;

	end;
end if;

if	(:new.cd_pessoa_receb is not null) and
	(:new.cd_pessoa_receb <> :old.cd_pessoa_receb) then
	begin

	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311332,'NM_PESSOA='||substr(obter_nome_pf(:new.cd_pessoa_receb),1,180)),:new.nm_usuario);

	end;
end if;

if	(:new.nm_pessoa_receb is not null) and
	(:new.nm_pessoa_receb <> :old.nm_pessoa_receb) then
	begin

	cm_gerar_historico_conj(:new.nr_sequencia,wheb_mensagem_pck.get_texto(311332,'NM_PESSOA='||substr(:new.nm_pessoa_receb,1,180)),:new.nm_usuario);

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cm_conjunto_cont_insert
after insert ON TASY.CM_CONJUNTO_CONT for each row
declare
nr_seq_conj_movto_w		number(10);

begin

select	cm_conjunto_movto_seq.nextval
into	nr_seq_conj_movto_w
from	dual;

if	(nvl(nr_seq_conj_movto_w,0) > 0) then
	begin

	insert into cm_conjunto_movto(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conjunto,
		cd_estab_origem,
		cd_estab_atual) values (
			nr_seq_conj_movto_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			:new.cd_estabelecimento,
			:new.cd_estabelecimento);

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cm_conjunto_cont_pend_atual 
after insert or update ON TASY.CM_CONJUNTO_CONT 
for each row
declare 
 
qt_reg_w			number(1); 
ie_tipo_w			varchar2(10); 
ie_libera_cme_w			varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_libera_cme) 
into	ie_libera_cme_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_libera_cme_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'CJ'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
 
		 
		ie_tipo_w := 'XCJ'; 
	end if; 
			 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario); 
	end if; 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.cm_conjunto_cont_tie
after insert or update or delete ON TASY.CM_CONJUNTO_CONT for each row
declare

json_w		philips_json;
json_data_w	clob;

begin

if (wheb_usuario_pck.get_nm_usuario is not null) then

	if (inserting or updating) then

		json_w := philips_json();
		json_w.put('id', :new.nr_sequencia);
		json_w.put('kit', :new.nr_seq_conjunto);
		json_w.put('item', :new.nr_seq_item);
		json_w.put('kitStatus', :new.ie_status_conjunto);
		json_w.put('sterilizationDepartment', :new.cd_setor_atendimento);
		json_w.put('pack', :new.nr_seq_embalagem);
		json_w.put('sterilisationValue', :new.vl_esterilizacao);
		json_w.put('points', :new.qt_ponto);
		json_w.put('establishment', :new.cd_estabelecimento);
		json_w.put('receiptUser', :new.nm_usuario_ester);
		json_w.put('originalUser', :new.nm_usuario_origem);
		json_w.put('exitUser', :new.nm_usuario_saida);
		json_w.put('scheduleUser', :new.nm_usuario_agenda);
		json_w.put('linkUser', :new.nm_usuario_vinc);
		json_w.put('deactivationUser', :new.nm_usuario_inativacao);
		json_w.put('startCycleUser', :new.nm_usuario_inicio);
		json_w.put('endCycleUser', :new.nm_usuario_fim);
		json_w.put('stockLocationTransferUser', :new.nm_usuario_repasse);
		json_w.put('surgerySettlementUser', :new.nm_usuario_baixa_cirurgia);
		json_w.put('receiptDate', :new.dt_receb_ester);
		json_w.put('originDate', :new.dt_origem);
		json_w.put('expirationDate', :new.dt_validade);
		json_w.put('exitDate', :new.dt_saida);
		json_w.put('scheduleDate', :new.dt_aloc_agenda);
		json_w.put('surgerySettlementDate', :new.dt_baixa_cirurgia);
		json_w.put('automaticEntryDate', :new.dt_lancamento_automatico);
		json_w.put('linkDate', :new.dt_vinculo_conj);
		json_w.put('deactivationDate', :new.dt_inativacao);
		json_w.put('releaseDate', :new.dt_liberacao);
		json_w.put('printingDate', :new.dt_impressao);
		json_w.put('itemCheckDate', :new.dt_conferencia_itens);
		json_w.put('eventDate', :new.ds_utc);
		json_w.put('registrationDate', :new.ds_utc_atualizacao);
		json_w.put('undeterminedExpiration', :new.ie_indeterminado);
		json_w.put('cycle', :new.nr_seq_ciclo);
		json_w.put('physicalKit', :new.nr_seq_conj_fisico);
		json_w.put('schedule', :new.nr_seq_agenda);
		json_w.put('person', :new.cd_pessoa_fisica);
		json_w.put('legalEntity', :new.cd_cgc);
		json_w.put('stockLocation', :new.cd_local_estoque);
		json_w.put('surgery', :new.nr_cirurgia);
		json_w.put('originalDepartment', :new.cd_setor_atend_orig);
		json_w.put('destinationDepartment', :new.cd_setor_atend_dest);
		json_w.put('clientDepartment', :new.nr_seq_cliente);
		json_w.put('control', :new.nr_seq_controle);
		json_w.put('batch', :new.nr_seq_lote);
		json_w.put('notes', :new.ds_observacao);
		json_w.put('personResponsible', :new.cd_pessoa_resp);
		json_w.put('sterilisation', :new.ie_tipo_esterilizacao);
		json_w.put('encounter', :new.nr_atendimento);
		json_w.put('perioperativeElectronicPatientChartId', :new.nr_seq_pepo);
		json_w.put('activeProfile', :new.cd_perfil_ativo);
		json_w.put('status', :new.ie_situacao);
		json_w.put('justification', :new.ds_justificativa);
		json_w.put('originKit', :new.nr_seq_conj_origem);
		json_w.put('physician', :new.cd_medico);
		json_w.put('signature', :new.nr_seq_assinatura);
		json_w.put('deactivationSignature', :new.nr_seq_assinat_inativacao);
		json_w.put('receiptPersonId', :new.cd_pessoa_receb);
		json_w.put('receiptPerson', :new.nm_pessoa_receb);
		json_w.put('institutionControl', :new.cd_controle_conj);
		json_w.put('kitPosition', :new.nr_seq_posicao_conj);
		json_w.put('purge', :new.ie_expurgo);
		json_w.put('sterilizationCycle', :new.nr_seq_ciclo_lav);
		json_w.put('externalDeliveryItem', :new.nr_seq_envio_ext_item);
		json_w.put('externalReceipt', :new.nr_seq_receb_ext);
		json_w.put('perioperativeElectronicPatientChartRule', :new.nr_seq_regra_pepo);
		json_w.put('equipment', :new.ds_equipamento);
		json_w.put('daylightSavingTime', :new.ie_horario_verao);
		json_w.put('cleaningProcess', :new.ie_processo_limpeza);
		json_w.put('auxiliaryKit ', :new.nm_conjunto_aux);
		json_w.put('auxiliaryCycle', :new.nr_ciclo_aux);
		json_w.put('auxiliaryClassification', :new.ds_classificacao_aux);
		json_w.put('auxiliaryKitStatus', :new.ds_status_conjunto_aux);
		json_w.put('auxiliaryPackage', :new.ds_embalagem_aux);
		json_w.put('auxiliaryStockLocation', :new.ds_local_estoque_aux);
		json_w.put('auxiliaryCorporateName', :new.ds_razao_social_aux);
		json_w.put('auxiliaryPerson', :new.nm_pessoa_fisica_aux);
		json_w.put('auxiliaryOriginalDepartment', :new.ds_setor_atend_orig_aux);
		json_w.put('auxiliaryRequisitionDepartment', :new.ds_setor_requisicao_aux);
		json_w.put('auxiliaryColor', :new.ds_cor_conjunto_aux);
		json_w.put('auxiliaryPhysician', :new.nm_medico_aux);
		json_w.put('auxiliaryPersonResponsible', :new.nm_pessoa_resp_aux);
		json_w.put('lastUpdate', :new.dt_atualizacao);
		json_w.put('lastUpdatedBy', :new.nm_usuario);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		if (inserting) then
			json_data_w := bifrost.send_integration_content('cssd.management.add.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		elsif (updating) then
			json_data_w := bifrost.send_integration_content('cssd.management.update.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		end if;

	elsif (deleting) then

		json_w := philips_json();
		json_w.put('id', :old.nr_sequencia);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.delete.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cm_conj_cont_insert_update
before insert or update ON TASY.CM_CONJUNTO_CONT for each row
declare

qt_registro_w			number(10);
qt_conjunto_w			number(13);
qt_registro_unico_w		number(10);
ie_controla_lav_conj_w   	varchar2(1);
ie_controla_lav_item_w		varchar2(1);
ie_permite_esterilizacao_w  funcao_param_usuario.vl_parametro%type;
nm_conjunto_w	cm_conjunto.nm_conjunto%type;

pragma autonomous_transaction;

begin

obter_param_usuario(406, 180, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_esterilizacao_w);

if	(inserting) then
	if	(:new.nr_seq_conjunto is null and :new.nr_seq_item is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1051031);
		/*Favor informar o conjunto ou o item que deseja receber.*/
	end if;

	if	(:new.nr_seq_conjunto is not null and :new.nr_seq_item is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1051032);
		/*Favor informar somente o conjunto ou o item que deseja receber.*/
	end if;

	if	(:new.nr_seq_conjunto is not null) then

		select 	count(*)
		into	qt_registro_unico_w
		from 	cm_item
		where 	nr_sequencia in (select nr_seq_item from cm_conjunto_item where nr_seq_conjunto = :new.nr_seq_conjunto)
		and 	ie_unico = 'S';

		select	count(*)
		into	qt_registro_w
		from	cm_conjunto_cont
		where	ie_status_conjunto not in (10,6)
		and	nr_seq_conjunto = :new.nr_seq_conjunto;

		if	(qt_registro_w > 0 and qt_registro_unico_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1051033);
			/*Este conjunto ja esta em processo de esterilizacao. Nao e permitido registra-lo em duplicidade.*/
		end if;
	end if;

	if	(:new.nr_seq_item is not null) then
		select	count(*)
		into	qt_registro_w
		from	cm_conjunto_cont
		where	ie_status_conjunto not in (10,6)
		and	nr_seq_item = :new.nr_seq_item
		and	nr_sequencia <> :new.nr_sequencia;

		if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1051033);
			/*Este conjunto ja esta em processo de esterilizacao. Nao e permitido registra-lo em duplicidade.*/
		end if;
	end if;


	if(:new.nr_seq_conjunto is not null)then
		select	nvl(ie_controla_processo_lav,'N')
			into	ie_controla_lav_conj_w
			from	cm_conjunto
			where	nr_sequencia = :new.nr_seq_conjunto;
	end if;
    if(:new.nr_seq_item is not null)then
			select	nvl(ie_controla_processo_lav,'N')
				into	ie_controla_lav_item_w
				from	cm_item
				where	nr_sequencia = :new.nr_seq_item;
	end if;

	if(:new.nr_seq_ciclo_lav is not null )then
		:new.ie_processo_limpeza := 'S';
		else if(:new.ie_processo_limpeza is null) then
			:new.ie_processo_limpeza := 'N';
		end if;
	end if;


	if	(:new.nr_seq_ciclo is not null) then
		if((ie_controla_lav_conj_w = 'S' or ie_controla_lav_item_w = 'S') and ie_permite_esterilizacao_w = 'N' and :new.ie_processo_limpeza = 'N')then
			wheb_mensagem_pck.exibir_mensagem_abort(1118085);
		end if;
	end if;

	if (:new.nr_seq_conjunto is not null) then
	begin
		select  nm_conjunto
		into    nm_conjunto_w
		from    cm_conjunto
		where   nr_sequencia = :new.nr_seq_conjunto
		and     ie_situacao  = 'I';
		exception when others then
			nm_conjunto_w := null;
		end;
		if (nm_conjunto_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1142754, 'NR_SEQUENCIA=' || :new.nr_seq_conjunto || ';' || 'DS_CONJUNTO=' || nm_conjunto_w);
	   end if;
	end if;

end if;

if (:new.nr_seq_conjunto is not null and :new.nr_seq_ciclo is not null and :old.nr_seq_ciclo is null) then
	begin
		select  nm_conjunto
		into    nm_conjunto_w
		from    cm_conjunto
		where   nr_sequencia = :new.nr_seq_conjunto
		and     ie_situacao  = 'I';
		exception when others then
			nm_conjunto_w := null;
		end;
	   if (nm_conjunto_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(1142754, 'NR_SEQUENCIA=' || :new.nr_seq_conjunto || ';' || 'DS_CONJUNTO=' || nm_conjunto_w);
	   end if;

end if;

if(:new.nr_seq_conjunto is not null)then
		select	nvl(ie_controla_processo_lav,'N')
			into	ie_controla_lav_conj_w
			from	cm_conjunto
			where	nr_sequencia = :new.nr_seq_conjunto;
end if;

if(:new.nr_seq_item is not null)then
		select	nvl(ie_controla_processo_lav,'N')
			into	ie_controla_lav_item_w
			from	cm_item
			where	nr_sequencia = :new.nr_seq_item;
end if;

if	(nvl(:new.nr_seq_ciclo_lav,0) <> nvl(:old.nr_seq_ciclo_lav,0)) then
	:new.ie_processo_limpeza := 'S';
end if;

if	(nvl(:new.nr_seq_ciclo,0) <> nvl(:old.nr_seq_ciclo,0)) then
	if((ie_controla_lav_conj_w = 'S' or ie_controla_lav_item_w = 'S') and ie_permite_esterilizacao_w = 'N' and :new.ie_processo_limpeza = 'N')then
		wheb_mensagem_pck.exibir_mensagem_abort(1118085);
	end if;
end if;


if	(nvl(:new.nr_seq_conjunto,0) <> nvl(:old.nr_seq_conjunto,0) and nvl(:new.nr_seq_conjunto,0) <> 0) then
	:new.nm_conjunto_aux := substr(cme_obter_nome_conjunto(:new.nr_seq_conjunto),1,200);
	:new.ds_classificacao_aux := substr(cme_obter_desc_classif_conj2(:new.nr_seq_conjunto),1,200);
	:new.ds_cor_conjunto_aux := substr(cme_obter_cor_conjunto(:new.nr_seq_conjunto),1,15);
end if;

if	(nvl(:new.nr_seq_ciclo,0) <> nvl(:old.nr_seq_ciclo,0)) then
	if	(nvl(:new.nr_seq_ciclo,0) > 0) then
		:new.nr_ciclo_aux := substr(cm_obter_nr_ciclo(:new.nr_seq_ciclo),1,10);
		:new.ds_equipamento := substr(obter_dados_ciclo(:new.nr_seq_ciclo,'E'),1,80);
	else
		:new.nr_ciclo_aux := :new.nr_seq_ciclo;
		:new.ds_equipamento := null;
	end if;
end if;

if	(nvl(:new.ie_status_conjunto,0) <> nvl(:old.ie_status_conjunto,0)) then
	:new.ds_status_conjunto_aux := substr(obter_valor_dominio(403,:new.ie_status_conjunto),1,200);
end if;

if	(nvl(:new.nr_seq_embalagem,0) <> nvl(:old.nr_seq_embalagem,0)) then
	:new.ds_embalagem_aux := substr(cme_obter_classif_embalagem(:new.nr_seq_embalagem),1,50);
end if;

if	(nvl(:new.cd_local_estoque,0) <> nvl(:old.cd_local_estoque,0)) then
	:new.ds_local_estoque_aux := substr(obter_desc_local_estoque(:new.cd_local_estoque),1,40);
end if;

if	(nvl(:new.cd_pessoa_fisica,'X') <> nvl(:old.cd_pessoa_fisica,'X')) then
	:new.nm_pessoa_fisica_aux := substr(obter_nome_pf(:new.cd_pessoa_fisica),1,60);
end if;

if	(nvl(:new.cd_setor_atend_orig,0) <> nvl(:old.cd_setor_atend_orig,0)) then
	:new.ds_setor_atend_orig_aux := substr(obter_nome_setor(:new.cd_setor_atend_orig),1,255);
end if;

if	(nvl(:new.cd_setor_atend_dest,0) <> nvl(:old.cd_setor_atend_dest,0)) then
	:new.ds_setor_requisicao_aux := substr(obter_nome_setor(:new.cd_setor_atend_dest),1,255);
end if;

if	(nvl(:new.cd_cgc,'X') <> nvl(:old.cd_cgc,'X')) then
	:new.ds_razao_social_aux := substr(obter_razao_social(:new.cd_cgc),1,80);
end if;

if	(nvl(:new.cd_pessoa_resp,'X') <> nvl(:old.cd_pessoa_resp,'X'))then
	:new.nm_pessoa_resp_aux := substr(obter_nome_pessoa_fisica(:new.cd_pessoa_resp, null),1,60);
end if;

if	(nvl(:new.cd_medico,'X') <> nvl(:old.cd_medico,'X')) then
	:new.nm_medico_aux := substr(obter_nome_medico(:new.cd_medico,'G'),1,255);
end if;

end;
/


ALTER TABLE TASY.CM_CONJUNTO_CONT ADD (
  CONSTRAINT CMCOCON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CONJUNTO_CONT ADD (
  CONSTRAINT CMCOCON_REGLANP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PEPO) 
 REFERENCES TASY.REGRA_LANCAMENTO_PEPO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCOCON_CMITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.CM_ITEM (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMENEIT_FK 
 FOREIGN KEY (NR_SEQ_ENVIO_EXT_ITEM) 
 REFERENCES TASY.CM_ENVIO_EXTERNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMCONJU_FK 
 FOREIGN KEY (NR_SEQ_CONJUNTO) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_LOCEST_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT CMCOCON_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMCOCON_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CMCOCON_CMCICLO_FK 
 FOREIGN KEY (NR_SEQ_CICLO) 
 REFERENCES TASY.CM_CICLO (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMCONFI_FK 
 FOREIGN KEY (NR_SEQ_CONJ_FISICO) 
 REFERENCES TASY.CM_CONJUNTO_FISICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCOCON_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CMCOCON_CMCLEMB_FK 
 FOREIGN KEY (NR_SEQ_EMBALAGEM) 
 REFERENCES TASY.CM_CLASSIF_EMBALAGEM (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMCOCON_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATEND_ORIG) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CMCOCON_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_DEST) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CMCOCON_SETATEN_FK9 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CMCOCON_CMCLIEN_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.CM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMLOTCON_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.CM_LOTE_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMCOCON_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CMCOCON_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CMCOCON_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMCOCON_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT CMCOCON_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMPOCON_FK 
 FOREIGN KEY (NR_SEQ_POSICAO_CONJ) 
 REFERENCES TASY.CM_POSICAO_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMCOCON_CMCILAV_FK 
 FOREIGN KEY (NR_SEQ_CICLO_LAV) 
 REFERENCES TASY.CM_CICLO_LAVACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_CONJUNTO_CONT TO NIVEL_1;

