ALTER TABLE TASY.CNES_SERVICO_CLASSIF_FICHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CNES_SERVICO_CLASSIF_FICHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CNES_SERVICO_CLASSIF_FICHA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SERVICO        NUMBER(10)              NOT NULL,
  NR_SEQ_CLASSIFICACAO  NUMBER(10)              NOT NULL,
  NR_SEQ_IDENTIFICACAO  NUMBER(10)              NOT NULL,
  IE_PROPRIO_TERCEIRO   VARCHAR2(1 BYTE)        NOT NULL,
  IE_AMBULATORIAL       VARCHAR2(1 BYTE)        NOT NULL,
  IE_HOSPITALAR         VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CNES_TERCEIRO      VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CNESECF_CNESECL_FK_I ON TASY.CNES_SERVICO_CLASSIF_FICHA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CNESECF_CNESECL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CNESECF_CNESIDE_FK_I ON TASY.CNES_SERVICO_CLASSIF_FICHA
(NR_SEQ_IDENTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CNESECF_CNESIDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CNESECF_CNESSER_FK_I ON TASY.CNES_SERVICO_CLASSIF_FICHA
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CNESECF_CNESSER_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CNESECF_PK ON TASY.CNES_SERVICO_CLASSIF_FICHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CNESECF_PK
  MONITORING USAGE;


ALTER TABLE TASY.CNES_SERVICO_CLASSIF_FICHA ADD (
  CONSTRAINT CNESECF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CNES_SERVICO_CLASSIF_FICHA ADD (
  CONSTRAINT CNESECF_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENTIFICACAO) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT CNESECF_CNESSER_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.CNES_SERVICO (NR_SEQUENCIA),
  CONSTRAINT CNESECF_CNESECL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CNES_SERVICO_CLASSIFICACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CNES_SERVICO_CLASSIF_FICHA TO NIVEL_1;

