ALTER TABLE TASY.HEM_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_LAUDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_REGISTRO          DATE,
  DS_LAUDO             LONG,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  DS_OBS_LAUDO         VARCHAR2(255 BYTE),
  NR_SEQ_PROC          NUMBER(10)               NOT NULL,
  NR_SEQ_CONDUTA       NUMBER(10),
  NR_SEQ_COMPLICACAO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMLAUD_HEMCOLA_FK_I ON TASY.HEM_LAUDO
(NR_SEQ_CONDUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMLAUD_HEMCOMP_FK_I ON TASY.HEM_LAUDO
(NR_SEQ_COMPLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMLAUD_HEMPROC_FK_I ON TASY.HEM_LAUDO
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMLAUD_PK ON TASY.HEM_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEM_LAUDO ADD (
  CONSTRAINT HEMLAUD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_LAUDO ADD (
  CONSTRAINT HEMLAUD_HEMCOLA_FK 
 FOREIGN KEY (NR_SEQ_CONDUTA) 
 REFERENCES TASY.HEM_CONDUTA_LAUDO (NR_SEQUENCIA),
  CONSTRAINT HEMLAUD_HEMPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.HEM_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMLAUD_HEMCOMP_FK 
 FOREIGN KEY (NR_SEQ_COMPLICACAO) 
 REFERENCES TASY.HEM_COMPLICACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_LAUDO TO NIVEL_1;

