ALTER TABLE TASY.BANCO_CARTEIRA_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_CARTEIRA_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_CARTEIRA_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_BANCO   NUMBER(10)               NOT NULL,
  NR_SEQ_CARTEIRA      NUMBER(10)               NOT NULL,
  VL_MINIMO            NUMBER(15,2),
  VL_MAXIMO            NUMBER(15,2),
  CD_CONVENIO          NUMBER(5),
  IE_TIPO_CONVENIO     NUMBER(2),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  TX_JUROS             NUMBER(7,4),
  TX_MULTA             NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BACAREG_BANCART_FK_I ON TASY.BANCO_CARTEIRA_REGRA
(NR_SEQ_CARTEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BACAREG_BANCART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BACAREG_BANESTA_FK_I ON TASY.BANCO_CARTEIRA_REGRA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BACAREG_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BACAREG_CONVENI_FK_I ON TASY.BANCO_CARTEIRA_REGRA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BACAREG_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BACAREG_ESTABEL_FK_I ON TASY.BANCO_CARTEIRA_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BACAREG_PK ON TASY.BANCO_CARTEIRA_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BACAREG_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.BANCO_CARTEIRA_REGRA_tp  after update ON TASY.BANCO_CARTEIRA_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_CARTEIRA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CARTEIRA,1,4000),substr(:new.NR_SEQ_CARTEIRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTEIRA',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_MINIMO,1,500);gravar_log_alteracao(substr(:old.VL_MINIMO,1,4000),substr(:new.VL_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_MAXIMO,1,500);gravar_log_alteracao(substr(:old.VL_MAXIMO,1,4000),substr(:new.VL_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAXIMO',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONVENIO,1,500);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.TX_MULTA,1,500);gravar_log_alteracao(substr(:old.TX_MULTA,1,4000),substr(:new.TX_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_MULTA',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_FIM_VIGENCIA,1,500);gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_CONVENIO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_CONVENIO,1,4000),substr(:new.IE_TIPO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONVENIO',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CONTA_BANCO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_BANCO,1,4000),substr(:new.NR_SEQ_CONTA_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_BANCO',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.TX_JUROS,1,500);gravar_log_alteracao(substr(:old.TX_JUROS,1,4000),substr(:new.TX_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'TX_JUROS',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INICIO_VIGENCIA,1,500);gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'BANCO_CARTEIRA_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.BANCO_CARTEIRA_REGRA ADD (
  CONSTRAINT BACAREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_CARTEIRA_REGRA ADD (
  CONSTRAINT BACAREG_BANCART_FK 
 FOREIGN KEY (NR_SEQ_CARTEIRA) 
 REFERENCES TASY.BANCO_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT BACAREG_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT BACAREG_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT BACAREG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.BANCO_CARTEIRA_REGRA TO NIVEL_1;

