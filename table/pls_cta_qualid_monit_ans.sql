ALTER TABLE TASY.PLS_CTA_QUALID_MONIT_ANS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CTA_QUALID_MONIT_ANS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CTA_QUALID_MONIT_ANS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(20 BYTE),
  IE_TP_PRESTADOR       VARCHAR2(1 BYTE),
  CD_GUIA_PRESTADOR     VARCHAR2(20 BYTE),
  CD_GUIA_OPERADORA     VARCHAR2(20 BYTE),
  CD_REEMBOLSO          VARCHAR2(20 BYTE),
  DT_PROCESSAMENTO      DATE,
  DT_PROCESSAMENTO_ANS  DATE,
  IE_CAMPO_ANS          VARCHAR2(3 BYTE),
  DS_CONTEUDO_ANS       VARCHAR2(20 BYTE),
  CD_ERRO_ANS           VARCHAR2(4 BYTE),
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  CD_CGC                VARCHAR2(14 BYTE),
  CD_CNES               VARCHAR2(7 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCTAQAN_PESFISI_FK_I ON TASY.PLS_CTA_QUALID_MONIT_ANS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAQAN_PESJURI_FK_I ON TASY.PLS_CTA_QUALID_MONIT_ANS
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCTAQAN_PK ON TASY.PLS_CTA_QUALID_MONIT_ANS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAQAN_PLSLQM_FK_I ON TASY.PLS_CTA_QUALID_MONIT_ANS
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CTA_QUALID_MONIT_ANS_tp  after update ON TASY.PLS_CTA_QUALID_MONIT_ANS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PLS_CTA_QUALID_MONIT_ANS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CTA_QUALID_MONIT_ANS ADD (
  CONSTRAINT PLSCTAQAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CTA_QUALID_MONIT_ANS ADD (
  CONSTRAINT PLSCTAQAN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSCTAQAN_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSCTAQAN_PLSLQM_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_QUALIDA_MONIT_ANS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CTA_QUALID_MONIT_ANS TO NIVEL_1;

