ALTER TABLE TASY.MAN_SLA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_SLA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_SLA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITULO            VARCHAR2(60 BYTE)        NOT NULL,
  DT_VIGENCIA          DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  NR_SEQ_GRUPO_TRAB    NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  PR_MULTA             NUMBER(5,2),
  PR_DESVIO            NUMBER(5,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANSLA_I1 ON TASY.MAN_SLA
(TRUNC("DT_VIGENCIA"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANSLA_MANGRTR_FK_I ON TASY.MAN_SLA
(NR_SEQ_GRUPO_TRAB)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANSLA_MANGRTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANSLA_PK ON TASY.MAN_SLA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_man_sla_insupd
before insert or update or delete ON TASY.MAN_SLA for each row
begin

    if(wheb_usuario_pck.get_cd_perfil != 272)then
    
        if(inserting)then
            hsj_gerar_log('Perfil sem permiss�o para inserir o registro!');
        elsif(updating)then
            hsj_gerar_log('Perfil sem permiss�o para alterar o registro!');
        elsif(deleting)then
            hsj_gerar_log('Perfil sem permiss�o para excluir o registro!');
        else
            hsj_gerar_log('Perfil sem permiss�o!');
        end if;
    
    end if;
    

    
end hsj_man_sla_insupd;
/


ALTER TABLE TASY.MAN_SLA ADD (
  CONSTRAINT MANSLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_SLA ADD (
  CONSTRAINT MANSLA_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRAB) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_SLA TO NIVEL_1;

