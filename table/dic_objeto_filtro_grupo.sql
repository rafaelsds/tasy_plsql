ALTER TABLE TASY.DIC_OBJETO_FILTRO_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIC_OBJETO_FILTRO_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIC_OBJETO_FILTRO_GRUPO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_OBJ_FILTRO        NUMBER(10),
  IE_APRESENTACAO_INICIAL  VARCHAR2(1 BYTE)     NOT NULL,
  CD_EXP_TITULO            NUMBER(10),
  NM_TITULO                VARCHAR2(255 BYTE)   NOT NULL,
  IE_APRESENTA_TITULO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DOFGRUP_DICEXPR_FK_I ON TASY.DIC_OBJETO_FILTRO_GRUPO
(CD_EXP_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DOFGRUP_DICOBJE_FK_I ON TASY.DIC_OBJETO_FILTRO_GRUPO
(NR_SEQ_OBJ_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DOFGRUP_PK ON TASY.DIC_OBJETO_FILTRO_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIC_OBJETO_FILTR_GRUPO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.DIC_OBJETO_FILTRO_GRUPO FOR EACH ROW
declare
	ds_expressao_br_w		DIC_EXPRESSAO.ds_expressao_br%type;
	cd_expressao_w			DIC_EXPRESSAO.cd_expressao%type;
	nm_coluna_desc_w		varchar2(50);
	nm_coluna_exp_w			varchar2(50);
BEGIN
    begin
	if	(:new.cd_exp_titulo is not null or
		 :old.cd_exp_titulo is not null) then

		nm_coluna_desc_w	:= 'NM_TITULO';
		nm_coluna_exp_w		:= 'CD_EXP_TITULO';
		cd_expressao_w		:= :new.cd_exp_titulo;
		ds_expressao_br_w	:= obter_desc_expressao_br(cd_expressao_w);
		:new.nm_titulo		:= ds_expressao_br_w;
	end if;
	exception when others then
		-- Caso a expressao nao couber no campo, avisa usu�rio
		if	(sqlcode = -6502) then
			wheb_mensagem_pck.exibir_mensagem_abort(290787, 'CD_EXPRESSAO_P='||cd_expressao_w||
									';DS_ATRIBUTO_P='||nm_coluna_desc_w||
									';NM_ATRIBUTO_EXP_P='||nm_coluna_exp_w);
		end if;
	end;

END  DIC_OBJETO_FILTR_GRUPO_ATUAL;
/


ALTER TABLE TASY.DIC_OBJETO_FILTRO_GRUPO ADD (
  CONSTRAINT DOFGRUP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIC_OBJETO_FILTRO_GRUPO ADD (
  CONSTRAINT DOFGRUP_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_TITULO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DOFGRUP_DICOBJE_FK 
 FOREIGN KEY (NR_SEQ_OBJ_FILTRO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIC_OBJETO_FILTRO_GRUPO TO NIVEL_1;

