ALTER TABLE TASY.MED_CONFIDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_CONFIDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_CONFIDENCIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_CONFIDENCIA          DATE                  NOT NULL,
  DS_CONFIDENCIA          LONG                  NOT NULL,
  NR_SEQ_CLIENTE          NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CONSULTA         NUMBER(10),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  DS_TITULO_CONF          VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDCONFI_ATCONSPEPA_FK_I ON TASY.MED_CONFIDENCIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDCONFI_MEDATEN_FK_I ON TASY.MED_CONFIDENCIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONFI_MEDATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCONFI_MEDCLIE_FK_I ON TASY.MED_CONFIDENCIA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONFI_MEDCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCONFI_MEDCONS_FK_I ON TASY.MED_CONFIDENCIA
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONFI_MEDCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCONFI_PESFISI_FK_I ON TASY.MED_CONFIDENCIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDCONFI_PK ON TASY.MED_CONFIDENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONFI_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_CONFIDENCIA ADD (
  CONSTRAINT MEDCONFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_CONFIDENCIA ADD (
  CONSTRAINT MEDCONFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDCONFI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT MEDCONFI_MEDATEN_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT MEDCONFI_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDCONFI_MEDCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.MED_CONSULTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_CONFIDENCIA TO NIVEL_1;

