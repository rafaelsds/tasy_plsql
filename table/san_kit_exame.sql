ALTER TABLE TASY.SAN_KIT_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_KIT_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_KIT_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_KIT               VARCHAR2(80 BYTE)        NOT NULL,
  DS_LOTE_KIT          VARCHAR2(20 BYTE)        NOT NULL,
  DT_VIGENCIA_INI      DATE                     NOT NULL,
  DT_VIGENCIA_FINAL    DATE                     NOT NULL,
  NR_SEQ_EXAME         NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANKIEX_PK ON TASY.SAN_KIT_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANKIEX_SANEXAM_FK_I ON TASY.SAN_KIT_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANKIEX_SANEXAM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SAN_KIT_EXAME_tp  after update ON TASY.SAN_KIT_EXAME FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SAN_KIT_EXAME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_KIT,1,4000),substr(:new.DS_KIT,1,4000),:new.nm_usuario,nr_seq_w,'DS_KIT',ie_log_w,ds_w,'SAN_KIT_EXAME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_FINAL,1,4000),substr(:new.DT_VIGENCIA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_FINAL',ie_log_w,ds_w,'SAN_KIT_EXAME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_INI,1,4000),substr(:new.DT_VIGENCIA_INI,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_INI',ie_log_w,ds_w,'SAN_KIT_EXAME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOTE_KIT,1,4000),substr(:new.DS_LOTE_KIT,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOTE_KIT',ie_log_w,ds_w,'SAN_KIT_EXAME',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SAN_KIT_EXAME ADD (
  CONSTRAINT SANKIEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_KIT_EXAME ADD (
  CONSTRAINT SANKIEX_SANEXAM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_KIT_EXAME TO NIVEL_1;

