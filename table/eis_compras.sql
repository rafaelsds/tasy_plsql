ALTER TABLE TASY.EIS_COMPRAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIS_COMPRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_COMPRAS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_REFERENCIA          DATE                   NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_PERIODO             VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_CGC                 VARCHAR2(14 BYTE),
  IE_PROD_FABRIC         VARCHAR2(3 BYTE)       NOT NULL,
  CD_GRUPO_MATERIAL      NUMBER(3)              NOT NULL,
  IE_TIPO_MATERIAL       VARCHAR2(3 BYTE)       NOT NULL,
  CD_MATERIAL            NUMBER(6)              NOT NULL,
  QT_DIAS_PRAZO_ENTREGA  NUMBER(5)              NOT NULL,
  QT_DIAS_PRAZO_PAGTO    NUMBER(5)              NOT NULL,
  QT_DIAS_ATRASO         NUMBER(5)              NOT NULL,
  QT_COMPRA              NUMBER(15,3)           NOT NULL,
  VL_COMPRA              NUMBER(15,2)           NOT NULL,
  VL_COMPRA_AVISTA       NUMBER(15,2)           NOT NULL,
  VL_CUSTO_PADRAO        NUMBER(22,4)           NOT NULL,
  CD_COMPRADOR           VARCHAR2(10 BYTE),
  VL_MEDIO_FATUR         NUMBER(15,2)           NOT NULL,
  IE_URGENTE             VARCHAR2(1 BYTE)       NOT NULL,
  VL_LIQ_MOEDA_REF       NUMBER(15,2),
  VL_COMPRA_PRAZO_MEDIO  NUMBER(15,2)           NOT NULL,
  IE_CLASSIFICACAO       VARCHAR2(2 BYTE),
  IE_CURVA_ABC           VARCHAR2(1 BYTE),
  CD_OPERACAO_NF         NUMBER(4),
  IE_VINC_ORDEM          VARCHAR2(1 BYTE),
  IE_VINC_CONTRATO       VARCHAR2(1 BYTE),
  IE_ORDEM_CONTRATO      VARCHAR2(1 BYTE),
  NR_SEQ_PROJ_REC        NUMBER(10),
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE),
  QT_COMPRA_URGENTE      NUMBER(15,3),
  QT_COMPRA_INFORMAL     NUMBER(15,3),
  CD_LOCAL_ESTOQUE       NUMBER(4),
  VL_BRUTO_COMPRA        NUMBER(15,2),
  CD_CENTRO_CUSTO        NUMBER(8),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_COMPRA     NUMBER(10),
  QT_COUNT_ITENS         NUMBER(10),
  PR_DESCONTO_MEDIO      NUMBER(13,4),
  VL_META                NUMBER(15,4),
  IE_CONTRATO_MERCADO    VARCHAR2(15 BYTE),
  IE_TIPO_FORNECEDOR     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA_EIS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          58M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISCOMP_I ON TASY.EIS_COMPRAS
(IE_PERIODO, DT_REFERENCIA, CD_ESTABELECIMENTO, CD_CGC, IE_PROD_FABRIC, 
CD_GRUPO_MATERIAL, IE_TIPO_MATERIAL, CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          33M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EISCOMP_PK ON TASY.EIS_COMPRAS
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX_EIS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.eis_compras_bf_ins_upd
before insert or update ON TASY.EIS_COMPRAS for each row
begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


ALTER TABLE TASY.EIS_COMPRAS ADD (
  CONSTRAINT EISCOMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX_EIS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          9M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.EIS_COMPRAS TO NIVEL_1;

