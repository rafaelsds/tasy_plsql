ALTER TABLE TASY.PEPO_MODELO_SECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_MODELO_SECAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_MODELO_SECAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO   NUMBER(3),
  DS_TITULO             VARCHAR2(100 BYTE)      NOT NULL,
  NR_SEQ_MODELO         NUMBER(10)              NOT NULL,
  CD_SECAO              VARCHAR2(2 BYTE),
  IE_PADRAO_VISIVEL     VARCHAR2(1 BYTE)        NOT NULL,
  NR_ESCALA_MAXIMA_ESQ  NUMBER(3),
  NR_ESCALA_MINIMA_ESQ  NUMBER(3),
  IE_MODO_VISUALIZACAO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEMODSE_PEPOMOD_FK_I ON TASY.PEPO_MODELO_SECAO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEMODSE_PK ON TASY.PEPO_MODELO_SECAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEMODSE_UK ON TASY.PEPO_MODELO_SECAO
(NR_SEQ_MODELO, CD_SECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PEPO_MODELO_SECAO ADD (
  CONSTRAINT PEMODSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT PEMODSE_UK
 UNIQUE (NR_SEQ_MODELO, CD_SECAO));

ALTER TABLE TASY.PEPO_MODELO_SECAO ADD (
  CONSTRAINT PEMODSE_PEPOMOD_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PEPO_MODELO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PEPO_MODELO_SECAO TO NIVEL_1;

