ALTER TABLE TASY.PLS_SIB_DEVOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SIB_DEVOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SIB_DEVOLUCAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOTE_SIB      NUMBER(10)               NOT NULL,
  DT_IMPORTACAO        DATE                     NOT NULL,
  NM_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_PROTOCOLO         VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSSBDV_PK ON TASY.PLS_SIB_DEVOLUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSBDV_PLSSBLT_FK_I ON TASY.PLS_SIB_DEVOLUCAO
(NR_SEQ_LOTE_SIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SIB_DEVOLUCAO ADD (
  CONSTRAINT PLSSBDV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SIB_DEVOLUCAO ADD (
  CONSTRAINT PLSSBDV_PLSSBLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_SIB) 
 REFERENCES TASY.PLS_SIB_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SIB_DEVOLUCAO TO NIVEL_1;

