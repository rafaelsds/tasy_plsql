ALTER TABLE TASY.QUA_CLASSIF_ANALISE_PROB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_CLASSIF_ANALISE_PROB CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_CLASSIF_ANALISE_PROB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DS_CLASSIFICACAO      VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_EXP_CLASSIFICACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUACAPR_DICEXPR_FK_I ON TASY.QUA_CLASSIF_ANALISE_PROB
(CD_EXP_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUACAPR_ESTABEL_FK_I ON TASY.QUA_CLASSIF_ANALISE_PROB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUACAPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUACAPR_PK ON TASY.QUA_CLASSIF_ANALISE_PROB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUACAPR_PK
  MONITORING USAGE;


ALTER TABLE TASY.QUA_CLASSIF_ANALISE_PROB ADD (
  CONSTRAINT QUACAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_CLASSIF_ANALISE_PROB ADD (
  CONSTRAINT QUACAPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUACAPR_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CLASSIFICACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.QUA_CLASSIF_ANALISE_PROB TO NIVEL_1;

