ALTER TABLE TASY.DIOPS_MOVTO_PEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_MOVTO_PEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_MOVTO_PEL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_MOVTO    NUMBER(10),
  NR_SEQ_PERIODO       NUMBER(10),
  VL_MES_N             NUMBER(15,2),
  VL_MES_N1            NUMBER(15,2),
  VL_MES_N2            NUMBER(15,2),
  VL_MES_N3            NUMBER(15,2),
  VL_MES_N4            NUMBER(15,2),
  VL_MES_N5            NUMBER(15,2),
  VL_MES_N6            NUMBER(15,2),
  VL_MES_N7            NUMBER(15,2),
  VL_MES_N8            NUMBER(15,2),
  VL_MES_N9            NUMBER(15,2),
  VL_MES_N10           NUMBER(15,2),
  VL_MES_N11           NUMBER(15,2),
  VL_MES_N12           NUMBER(15,2),
  VL_MES_N13           NUMBER(15,2),
  VL_MES_N14           NUMBER(15,2),
  VL_MES_N15           NUMBER(15,2),
  VL_MES_N16           NUMBER(15,2),
  VL_MES_N17           NUMBER(15,2),
  VL_MES_N18           NUMBER(15,2),
  VL_MES_N19           NUMBER(15,2),
  VL_MES_N20           NUMBER(15,2),
  VL_MES_N21           NUMBER(15,2),
  VL_MES_N22           NUMBER(15,2),
  VL_MES_N23           NUMBER(15,2),
  VL_MES_N24           NUMBER(15,2),
  VL_MES_N25           NUMBER(15,2),
  VL_MES_N26           NUMBER(15,2),
  VL_MES_N27           NUMBER(15,2),
  VL_MES_N28           NUMBER(15,2),
  VL_MES_N29           NUMBER(15,2),
  VL_MES_N30           NUMBER(15,2),
  VL_MES_N31           NUMBER(15,2),
  VL_MES_N32           NUMBER(15,2),
  VL_MES_N33           NUMBER(15,2),
  VL_MES_N34           NUMBER(15,2),
  VL_MES_N35           NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIOPSMVPEL_DIOPERI_FK_I ON TASY.DIOPS_MOVTO_PEL
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIOPSMVPEL_DIOPSPEL_FK_I ON TASY.DIOPS_MOVTO_PEL
(NR_SEQ_TIPO_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIOPSMVPEL_PK ON TASY.DIOPS_MOVTO_PEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.diops_movto_pel_atual
before update ON TASY.DIOPS_MOVTO_PEL 
for each row
declare

dt_mes_competencia_w	date;
nr_campo_alteracao_w	number(1)	:= null;
vl_evento_w		number(15,2) 	:= 0;
nm_usuario_w		varchar(255);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if	(:old.vl_mes_n	<> :new.vl_mes_n) then
		vl_evento_w		:= :new.vl_mes_n;
		nr_campo_alteracao_w	:= 0;
	elsif	(:old.vl_mes_n1	<> :new.vl_mes_n1) then
		vl_evento_w		:= :new.vl_mes_n1;
		nr_campo_alteracao_w	:= -1;
	elsif	(:old.vl_mes_n2	<> :new.vl_mes_n2) then
		vl_evento_w		:= :new.vl_mes_n2;
		nr_campo_alteracao_w	:= -2;
	end if;

	if	(nr_campo_alteracao_w is not null) then
		select	add_months(trunc(a.dt_periodo_final,'month'),nr_campo_alteracao_w)
		into	dt_mes_competencia_w
		from	diops_periodo	a
		where	a.nr_sequencia	= :new.nr_seq_periodo;

		nm_usuario_w	:= wheb_usuario_pck.get_nm_usuario;

		update 	w_diops_movto_pel
		set	vl_evento 				= vl_evento_w,
			dt_atualizacao 				= sysdate,
			nm_usuario 				= nm_usuario_w
		where	nr_seq_periodo 				= :new.nr_seq_periodo
		and	nr_seq_tipo_movto			= :new.nr_seq_tipo_movto
		and	trunc(dt_mes_competencia, 'month')	= dt_mes_competencia_w
		and	ie_tipo_segurado			<> 'R';
						
		:new.nm_usuario		:= nm_usuario_w;
		:new.dt_atualizacao	:= sysdate;
	end if;
end if;

end;
/


ALTER TABLE TASY.DIOPS_MOVTO_PEL ADD (
  CONSTRAINT DIOPSMVPEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIOPS_MOVTO_PEL ADD (
  CONSTRAINT DIOPSMVPEL_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA),
  CONSTRAINT DIOPSMVPEL_DIOPSPEL_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MOVTO) 
 REFERENCES TASY.DIOPS_TIPO_MOVTO_PEL (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIOPS_MOVTO_PEL TO NIVEL_1;

