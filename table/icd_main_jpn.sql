DROP TABLE TASY.ICD_MAIN_JPN CASCADE CONSTRAINTS;

CREATE TABLE TASY.ICD_MAIN_JPN
(
  NR_CHNG_SEGM              NUMBER(2),
  NR_DISEASE_NUMBER         NUMBER(10),
  DS_DISEASE_NAME           VARCHAR2(200 BYTE),
  DS_NAME_OF_CANA_DISEASE   VARCHAR2(320 BYTE),
  NR_ADOPTED_CATEGORY       NUMBER(2),
  NR_CODE_EXCHANGE          VARCHAR2(6 BYTE),
  CD_ICD_CODE               VARCHAR2(10 BYTE),
  CD_MULTICLASSIF_CODE      VARCHAR2(12 BYTE),
  DS_PRELIMINARY1           VARCHAR2(20 BYTE),
  DS_PRELIMINARY2           VARCHAR2(28 BYTE),
  NR_RECEIPT_COMPUTER_CODE  NUMBER(36),
  DS_OMITED_DISEASE_NAME    VARCHAR2(120 BYTE),
  NR_AREA_OF_USE            NUMBER(3),
  NR_CHANGE_HISTORY         NUMBER(5),
  DT_UPDATE                 VARCHAR2(50 BYTE),
  NR_DSTN_DISEASE_NUMBER    NUMBER(10),
  IE_PROHIBITED_CATEGORY    VARCHAR2(2 BYTE),
  IE_OUT_OF_INSURANCE       NUMBER(1),
  DS_PRELIMINARY3           VARCHAR2(640 BYTE),
  DS_PRELIMINARY4           VARCHAR2(28 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.ICD_MAIN_JPN TO NIVEL_1;

