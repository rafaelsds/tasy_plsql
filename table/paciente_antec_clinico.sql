ALTER TABLE TASY.PACIENTE_ANTEC_CLINICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ANTEC_CLINICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ANTEC_CLINICO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  NR_ATENDIMENTO               NUMBER(10),
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_REGISTRO                  DATE             NOT NULL,
  DS_DOENCA                    VARCHAR2(60 BYTE),
  IE_STATUS                    VARCHAR2(3 BYTE),
  IE_INTENSIDADE               VARCHAR2(3 BYTE),
  DT_INICIO                    DATE,
  DT_FIM                       DATE,
  DS_ABORDAGEM                 VARCHAR2(255 BYTE),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  NR_SEQ_DOENCA                NUMBER(10),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DT_LIBERACAO                 DATE,
  NM_USUARIO_LIBERACAO         VARCHAR2(15 BYTE),
  NR_SEQ_NIVEL_SEG             NUMBER(10),
  NR_SEQ_SAEP                  NUMBER(10),
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  IE_NEGA_DOENCAS              VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO          NUMBER(10),
  CD_PERFIL_ATIVO              NUMBER(5),
  CD_DOENCA                    VARCHAR2(10 BYTE),
  IE_PACIENTE                  VARCHAR2(1 BYTE),
  NR_SEQ_PARENTESCO            NUMBER(10),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NM_USUARIO_REVISAO           VARCHAR2(15 BYTE),
  DT_REVISAO                   DATE,
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  IE_ALERTA                    VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  IE_SEXO                      VARCHAR2(1 BYTE),
  DT_NASCIMENTO_PARENTE        DATE,
  NM_PARENTE                   VARCHAR2(80 BYTE),
  DT_OBITO_PARENTE             DATE,
  CD_SETOR_REVISAO             NUMBER(5),
  NM_USUARIO_TERMINO           VARCHAR2(15 BYTE),
  DS_JUST_TERMINO              VARCHAR2(255 BYTE),
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  CD_PROFISSIONAL              VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE),
  IE_LISTA_PROBLEMA            VARCHAR2(1 BYTE),
  IE_CAUSA_MORTE               VARCHAR2(1 BYTE),
  NR_SEQ_HIST_ROTINA           NUMBER(10),
  IE_DOENCA_CRONICA            VARCHAR2(2 BYTE),
  IE_TRATAMENTO_FARMACOLOGICO  VARCHAR2(1 BYTE),
  IE_TRATAMENTO_NAO_FARMACO    VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA       NUMBER(10),
  IE_ACAO                      VARCHAR2(1 BYTE),
  QT_AGE_DIAGNOSIS             NUMBER(3),
  IE_INSERIR_DIAGNOSTICO       VARCHAR2(1 BYTE),
  CD_CID                       VARCHAR2(120 BYTE),
  DS_TREATING_ESTAB            VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACANCL_ATCONSPEPA_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_ATEPACI_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_CIDDOEN_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_EHRREEL_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_GRAUPA_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_GRAUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_HISTSAU_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_I1 ON TASY.PACIENTE_ANTEC_CLINICO
(DT_REGISTRO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_I2 ON TASY.PACIENTE_ANTEC_CLINICO
(IE_PACIENTE, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_I2
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_NISEALE_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_PEDOENC_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_PERFIL_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_PESFISI_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_PESFISI_FK2_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACANCL_PK ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACANCL_SAEPERO_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_SETATEN_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_SETATEN_FK2_I ON TASY.PACIENTE_ANTEC_CLINICO
(CD_SETOR_REVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_TASASDI_FK_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACANCL_TASASDI_FK2_I ON TASY.PACIENTE_ANTEC_CLINICO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACANCL_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pac_antec_clinico_pend_atual
after insert or update ON TASY.PACIENTE_ANTEC_CLINICO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_liberar_hist_saude_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_liberar_hist_saude)
into	ie_liberar_hist_saude_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then
	if	(:new.ie_paciente = 'N') then
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'HSAM';
		elsif	(:old.dt_liberacao is null) and
				(:new.dt_liberacao is not null) then
			ie_tipo_w := 'XHSAM';
		end if;
	else
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'HSAC';
		elsif	(:old.dt_liberacao is null) and
				(:new.dt_liberacao is not null) then
			ie_tipo_w := 'XHSAC';
		end if;
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ANTEC_CLINICO_ATUAL
before update or insert ON TASY.PACIENTE_ANTEC_CLINICO for each row
declare
begin
if	(:new.nr_atendimento is not null) then
	:new.cd_setor_atendimento := obter_setor_atendimento(:new.nr_atendimento);
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(NVL(:new.ie_nega_doencas,'N') = 'N') and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_comorbidity_integration(:new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nr_seq_hist_rotina, :new.dt_liberacao, :new.dt_inativacao, :new.dt_atualizacao);
end if;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
	send_comorbidity_integration(:new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nr_seq_hist_rotina, :new.dt_liberacao, :new.dt_inativacao, :new.dt_atualizacao);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ANTEC_CLINICO_tp  after update ON TASY.PACIENTE_ANTEC_CLINICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PACIENTE_ANTEC_CLINICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'PACIENTE_ANTEC_CLINICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_PACIENTE_ANTEC_CLINICO
before insert or update ON TASY.PACIENTE_ANTEC_CLINICO for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIE_ANTEC_CLINICO_SBIS_INS
before insert or update ON TASY.PACIENTE_ANTEC_CLINICO for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
										 :new.cd_pessoa_fisica,
										 obter_desc_expressao(314080),
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario),
										 :new.nr_atendimento);
else
	ie_inativacao_w := 'N';


	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 :new.cd_pessoa_fisica,
										 obter_desc_expressao(314080),
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario),
										 :new.nr_atendimento);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.PACIE_ANTEC_CLINICO_SBIS_DEL
before delete ON TASY.PACIENTE_ANTEC_CLINICO for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nm_usuario,
									 nr_atendimento) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(314080),
									 nvl(wheb_usuario_pck.get_nm_usuario,:old.nm_usuario),
									 :old.nr_atendimento);

end;
/


ALTER TABLE TASY.PACIENTE_ANTEC_CLINICO ADD (
  CONSTRAINT PACANCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ANTEC_CLINICO ADD (
  CONSTRAINT PACANCL_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_REVISAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACANCL_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACANCL_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACANCL_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACANCL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACANCL_PEDOENC_FK 
 FOREIGN KEY (NR_SEQ_DOENCA) 
 REFERENCES TASY.PE_DOENCA (NR_SEQUENCIA),
  CONSTRAINT PACANCL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACANCL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACANCL_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT PACANCL_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT PACANCL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACANCL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PACANCL_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PACANCL_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PACANCL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACANCL_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PACIENTE_ANTEC_CLINICO TO NIVEL_1;

