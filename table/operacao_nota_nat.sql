ALTER TABLE TASY.OPERACAO_NOTA_NAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPERACAO_NOTA_NAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPERACAO_NOTA_NAT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_OPERACAO_NF        NUMBER(4)               NOT NULL,
  CD_NATUREZA_OPERACAO  NUMBER(4)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPENONA_ESTABEL_FK_I ON TASY.OPERACAO_NOTA_NAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OPENONA_NATOPER_FK_I ON TASY.OPERACAO_NOTA_NAT
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OPENONA_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OPENONA_OPENOTA_FK_I ON TASY.OPERACAO_NOTA_NAT
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPENONA_PK ON TASY.OPERACAO_NOTA_NAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OPENONA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.OPERACAO_NOTA_NAT_tp  after update ON TASY.OPERACAO_NOTA_NAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_OPERACAO_NF,1,4000),substr(:new.CD_OPERACAO_NF,1,4000),:new.nm_usuario,nr_seq_w,'CD_OPERACAO_NF',ie_log_w,ds_w,'OPERACAO_NOTA_NAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'OPERACAO_NOTA_NAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NATUREZA_OPERACAO,1,4000),substr(:new.CD_NATUREZA_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_NATUREZA_OPERACAO',ie_log_w,ds_w,'OPERACAO_NOTA_NAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPERACAO_NOTA_NAT ADD (
  CONSTRAINT OPENONA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OPERACAO_NOTA_NAT ADD (
  CONSTRAINT OPENONA_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF)
    ON DELETE CASCADE,
  CONSTRAINT OPENONA_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT OPENONA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.OPERACAO_NOTA_NAT TO NIVEL_1;

