ALTER TABLE TASY.REGRA_LIB_TIT_PAGAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LIB_TIT_PAGAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LIB_TIT_PAGAR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_PESSOA         VARCHAR2(1 BYTE)       NOT NULL,
  VL_MINIMO              NUMBER(15,2),
  VL_MAXIMO              NUMBER(15,2),
  CD_TIPO_PJ             NUMBER(3),
  CD_OPERACAO_NF         NUMBER(4),
  IE_LIBERAR             VARCHAR2(1 BYTE),
  CD_CGC                 VARCHAR2(14 BYTE),
  IE_NIVEL_LIB           VARCHAR2(5 BYTE),
  DT_INICIO_VIGENCIA     DATE,
  DT_FIM_VIGENCIA        DATE,
  IE_VALOR_NF            VARCHAR2(1 BYTE),
  IE_PROJETO_RECURSO     VARCHAR2(1 BYTE),
  IE_TIPO_TITULO         VARCHAR2(2 BYTE),
  NR_SEQ_CLASSE          NUMBER(10),
  IE_ORIGEM_TITULO       VARCHAR2(10 BYTE),
  IE_ORDEM_COMPRA        VARCHAR2(1 BYTE),
  IE_ORIGEM_TITULO_EXCE  VARCHAR2(1 BYTE)       NOT NULL,
  IE_EXIGE_NOTA_FISCAL   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELIITP_CLTPUSU_FK_I ON TASY.REGRA_LIB_TIT_PAGAR
(NR_SEQ_CLASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELIITP_CLTPUSU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELIITP_ESTABEL_FK_I ON TASY.REGRA_LIB_TIT_PAGAR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELIITP_OPENOTA_FK_I ON TASY.REGRA_LIB_TIT_PAGAR
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELIITP_PESJURI_FK_I ON TASY.REGRA_LIB_TIT_PAGAR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELIITP_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RELIITP_PK ON TASY.REGRA_LIB_TIT_PAGAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELIITP_TIPPEJU_FK_I ON TASY.REGRA_LIB_TIT_PAGAR
(CD_TIPO_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELIITP_TIPPEJU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_LIB_TIT_PAGAR ADD (
  CONSTRAINT RELIITP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_LIB_TIT_PAGAR ADD (
  CONSTRAINT RELIITP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RELIITP_TIPPEJU_FK 
 FOREIGN KEY (CD_TIPO_PJ) 
 REFERENCES TASY.TIPO_PESSOA_JURIDICA (CD_TIPO_PESSOA),
  CONSTRAINT RELIITP_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT RELIITP_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT RELIITP_CLTPUSU_FK 
 FOREIGN KEY (NR_SEQ_CLASSE) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_LIB_TIT_PAGAR TO NIVEL_1;

