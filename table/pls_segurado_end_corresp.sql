ALTER TABLE TASY.PLS_SEGURADO_END_CORRESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SEGURADO_END_CORRESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SEGURADO_END_CORRESP
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_SEGURADO           NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_CGC                    VARCHAR2(14 BYTE),
  IE_PESSOA_ENDERECO        VARCHAR2(2 BYTE),
  IE_ENDERECO_BOLETO        VARCHAR2(5 BYTE),
  NR_SEQ_COMPL_PF_TEL_ADIC  NUMBER(10),
  NR_SEQ_COMPL_PJ           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSEND_COPFTEA_FK_I ON TASY.PLS_SEGURADO_END_CORRESP
(NR_SEQ_COMPL_PF_TEL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEND_COPFTEA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSEND_ESTABEL_FK_I ON TASY.PLS_SEGURADO_END_CORRESP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSEND_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSSEND_PK ON TASY.PLS_SEGURADO_END_CORRESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEND_PLSCONT_FK_I ON TASY.PLS_SEGURADO_END_CORRESP
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSEND_PLSSEGU_FK_I ON TASY.PLS_SEGURADO_END_CORRESP
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SEGURADO_END_CORRESP ADD (
  CONSTRAINT PLSSEND_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SEGURADO_END_CORRESP ADD (
  CONSTRAINT PLSSEND_COPFTEA_FK 
 FOREIGN KEY (NR_SEQ_COMPL_PF_TEL_ADIC) 
 REFERENCES TASY.COMPL_PF_TEL_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSSEND_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSEND_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSSEND_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SEGURADO_END_CORRESP TO NIVEL_1;

