ALTER TABLE TASY.PLS_SIMULACAO_RESUMO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SIMULACAO_RESUMO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SIMULACAO_RESUMO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_SIMULACAO         NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DS_ITEM_RESUMO           VARCHAR2(255 BYTE)   NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_TIPO_PESSOA           VARCHAR2(2 BYTE),
  NR_SEQ_ORDEM             NUMBER(10),
  VL_RESUMO                NUMBER(15,2),
  NR_SEQ_SEGURADO_SIMUL    NUMBER(10),
  NR_SEQ_VINC_BONIFICACAO  NUMBER(10),
  NR_SEQ_VINC_SCA          NUMBER(10),
  NR_SEQ_PLANO             NUMBER(10),
  NR_SEQ_SIMUL_COLETIVO    NUMBER(10),
  VL_PRO_RATA_DIA          NUMBER(15,2),
  VL_ANTECIPACAO           NUMBER(15,2),
  IE_TIPO_ITEM             VARCHAR2(2 BYTE),
  VL_SCA_EMBUTIDO          NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSIRE_ESTABEL_FK_I ON TASY.PLS_SIMULACAO_RESUMO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSIRE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSSIRE_PK ON TASY.PLS_SIMULACAO_RESUMO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSIRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSSIRE_PLSIMIF_FK_I ON TASY.PLS_SIMULACAO_RESUMO
(NR_SEQ_SEGURADO_SIMUL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSIRE_PLSIMIF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSIRE_PLSSIPR_FK_I ON TASY.PLS_SIMULACAO_RESUMO
(NR_SEQ_SIMULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SIMULACAO_RESUMO ADD (
  CONSTRAINT PLSSIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SIMULACAO_RESUMO ADD (
  CONSTRAINT PLSSIRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSIRE_PLSSIPR_FK 
 FOREIGN KEY (NR_SEQ_SIMULACAO) 
 REFERENCES TASY.PLS_SIMULACAO_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSSIRE_PLSIMIF_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO_SIMUL) 
 REFERENCES TASY.PLS_SIMULPRECO_INDIVIDUAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SIMULACAO_RESUMO TO NIVEL_1;

