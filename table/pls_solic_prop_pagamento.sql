ALTER TABLE TASY.PLS_SOLIC_PROP_PAGAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLIC_PROP_PAGAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLIC_PROP_PAGAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_ORIGEM             VARCHAR2(2 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_BOLETO             VARCHAR2(1 BYTE),
  IE_CARTAO_CREDITO     VARCHAR2(1 BYTE),
  IE_CARTAO_DEBITO      VARCHAR2(1 BYTE),
  IE_DEBITO_AUTOMATICO  VARCHAR2(1 BYTE),
  NR_SEQ_PLANO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSPOP_ESTABEL_FK_I ON TASY.PLS_SOLIC_PROP_PAGAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSPOP_PK ON TASY.PLS_SOLIC_PROP_PAGAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSPOP_PLSPLAN_FK_I ON TASY.PLS_SOLIC_PROP_PAGAMENTO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_solic_prop_pag_befinsup
before insert or update ON TASY.PLS_SOLIC_PROP_PAGAMENTO for each row
declare

qt_origem_w	number(10);

pragma autonomous_transaction;

begin

select	count(1)
into	qt_origem_w
from	pls_solic_prop_pagamento
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_origem = :new.ie_origem
and	nr_sequencia <> :new.nr_sequencia;

if	(qt_origem_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1092518); -- J� h� regra para essa origem.
end if;

if	((:new.ie_origem = 2) and (:new.ie_cartao_debito = 'S')) then
	wheb_mensagem_pck.exibir_mensagem_abort(1092520); -- A forma de pagamento "Cart�o de d�bito" n�o pode ser selecionada para a origem "Demais mensalidades".
end if;

end;
/


ALTER TABLE TASY.PLS_SOLIC_PROP_PAGAMENTO ADD (
  CONSTRAINT PLSSPOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_SOLIC_PROP_PAGAMENTO ADD (
  CONSTRAINT PLSSPOP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSPOP_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SOLIC_PROP_PAGAMENTO TO NIVEL_1;

