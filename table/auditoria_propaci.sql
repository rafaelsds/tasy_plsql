ALTER TABLE TASY.AUDITORIA_PROPACI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUDITORIA_PROPACI CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUDITORIA_PROPACI
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_AUDITORIA             NUMBER(10)       NOT NULL,
  NR_SEQ_PROPACI               NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO                NUMBER(10),
  QT_ORIGINAL                  NUMBER(15,3)     NOT NULL,
  QT_AJUSTE                    NUMBER(15,3),
  TX_PROC_ORIGINAL             NUMBER(15,4)     NOT NULL,
  TX_PROC_AJUSTE               NUMBER(15,4),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  CD_CONVENIO_AJUSTE           NUMBER(5),
  CD_CATEGORIA_AJUSTE          VARCHAR2(10 BYTE),
  IE_TIPO_AUDITORIA            VARCHAR2(1 BYTE) NOT NULL,
  IE_PERDA                     VARCHAR2(1 BYTE),
  CD_SETOR_AJUSTE              NUMBER(10),
  NR_SEQ_ATEPACU_AJUSTE        NUMBER(10),
  CD_SETOR_RESP_GLOSA          NUMBER(5),
  CD_PESSOA_RESP_JUST          VARCHAR2(10 BYTE),
  VL_PROCEDIMENTO              NUMBER(15,2),
  VL_MEDICO                    NUMBER(15,2),
  VL_MATERIAIS                 NUMBER(15,2),
  VL_PROCEDIMENTO_AJUSTE       NUMBER(15,2),
  VL_MEDICO_AJUSTE             NUMBER(15,2),
  VL_MATERIAIS_AJUSTE          NUMBER(15,2),
  VL_CUSTO_OPERACIONAL         NUMBER(15,2),
  VL_CUSTO_OPERACIONAL_AJUSTE  NUMBER(15,2),
  NR_CONTA_ORIGEM              NUMBER(10),
  NR_SEQ_ESTAGIO_PEND          NUMBER(10),
  VL_TOTAL_AJUSTE              NUMBER(15,2),
  IE_EXIGE_AUDITORIA           VARCHAR2(1 BYTE),
  CD_PESSOA_RESP_GLOSA         VARCHAR2(10 BYTE),
  CD_SETOR_ORIGINAL            NUMBER(5),
  IE_ORIGINAL                  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUDPROP_ATEPACU_FK_I ON TASY.AUDITORIA_PROPACI
(NR_SEQ_ATEPACU_AJUSTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_ATEPACU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_AUDCOPA_FK_I ON TASY.AUDITORIA_PROPACI
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDPROP_AUDMOTI_FK_I ON TASY.AUDITORIA_PROPACI
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_AUDMOTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_CATCONV_FK_I ON TASY.AUDITORIA_PROPACI
(CD_CONVENIO_AJUSTE, CD_CATEGORIA_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_CONVENI_FK_I ON TASY.AUDITORIA_PROPACI
(CD_CONVENIO_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_PESFISI_FK_I ON TASY.AUDITORIA_PROPACI
(CD_PESSOA_RESP_JUST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDPROP_PESFISI_FK2_I ON TASY.AUDITORIA_PROPACI
(CD_PESSOA_RESP_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUDPROP_PK ON TASY.AUDITORIA_PROPACI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDPROP_PROPACI_FK_I ON TASY.AUDITORIA_PROPACI
(NR_SEQ_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUDPROP_SETATEN_FK_I ON TASY.AUDITORIA_PROPACI
(CD_SETOR_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_SETATEN_FK2_I ON TASY.AUDITORIA_PROPACI
(CD_SETOR_RESP_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.AUDPROP_SETATEN_FK3_I ON TASY.AUDITORIA_PROPACI
(CD_SETOR_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUDPROP_SETATEN_FK3_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_PROPACI_UPDATE
before update ON TASY.AUDITORIA_PROPACI for each row
declare

nr_seq_pend_w			Number(10,0);
nr_seq_estagio_pend_w	Number(10,0);
cd_setor_resp_glosa_w	number(5):=0;

vl_item_w		number(15,2);
qt_ajuste_w		number(10,0);
qt_original_w		number(10,0);
vl_unitario_w		number(15,4);
qt_dif_w		number(10,0);
tx_dif_w		number(15,4);

nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
nr_conta_audit_w	number(10,0);
ds_call_stack_w		varchar2(2000);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_conta_w		date;
dt_procedimento_w	date;
qt_item_periodo_w	number(10,0);

begin

select 	max(nr_sequencia)
into	nr_seq_pend_w
from 	cta_pendencia
where 	nr_seq_auditoria = :new.nr_seq_auditoria;

if	(nvl(nr_seq_pend_w,0) > 0) then

	select 	max(nr_seq_estagio)
	into	nr_seq_estagio_pend_w
	from  	cta_pendencia
	where 	nr_sequencia = nr_seq_pend_w;

	if	(nvl(nr_seq_estagio_pend_w,0) > 0) then

		:new.nr_seq_estagio_pend:= nr_seq_estagio_pend_w;

	end if;
end if;

if	(nvl(:old.nr_seq_motivo,0) <> nvl(:new.nr_seq_motivo,0)) then

	insert into audit_propaci_motivo(
			nr_sequencia,
			nr_seq_audit_proc,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_motivo,
			nr_seq_motivo_ant)
		values	(audit_propaci_motivo_seq.NextVal,
			:new.nr_sequencia,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_seq_motivo, -- Novo
			:old.nr_seq_motivo); -- Antigo

end if;

if	(:new.nr_seq_motivo is not null) then

	begin
	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_resp_glosa_w
	from 	regra_setor_resp_glosa
	where	nr_seq_motivo = :new.nr_seq_motivo
	and		ie_situacao = 'A';
	exception
	when others then
		cd_setor_resp_glosa_w := 0;
	end;

	if	(cd_setor_resp_glosa_w > 0) then
		:new.cd_setor_resp_glosa	 := cd_setor_resp_glosa_w;
	end if;

end if;

-- OS 377968
begin
select	nvl(a.vl_procedimento,0),
	dividir(nvl(a.vl_procedimento,0), :new.qt_original)
into	vl_item_w,
	vl_unitario_w
from	procedimento_paciente a
where	a.nr_sequencia		= :new.nr_seq_propaci
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
exception
	when others then
	:new.vl_total_ajuste:= 0;
end;

if	(:new.ie_tipo_auditoria = 'X') then
	:new.vl_total_ajuste:= 0;
else
	if	(:new.ie_tipo_auditoria = 'V') or (:new.vl_procedimento_ajuste is not null) then

		:new.vl_total_ajuste:= :new.vl_procedimento_ajuste;

		if 	(:new.qt_ajuste is not null) then

			qt_dif_w:= :new.qt_ajuste - :new.qt_original;
			:new.vl_total_ajuste:=   :new.vl_total_ajuste + (dividir(nvl(:new.vl_total_ajuste,0), :new.qt_original) * qt_dif_w);

		elsif	(:new.tx_proc_ajuste is not null) and
			(:new.tx_proc_ajuste <> :old.tx_proc_ajuste) then

			tx_dif_w:= :new.tx_proc_ajuste - :new.tx_proc_original;
			:new.vl_total_ajuste:= :new.vl_total_ajuste + (:new.vl_total_ajuste * tx_dif_w / 100);

		end if;

	else
		if 	(:new.qt_ajuste is not null) and
			(nvl(:new.qt_ajuste,-1) <> nvl(:old.qt_ajuste,-1)) then

			:new.vl_total_ajuste:= vl_item_w;
			qt_dif_w:= :new.qt_ajuste - :new.qt_original;
			:new.vl_total_ajuste:=   :new.vl_total_ajuste + (vl_unitario_w * qt_dif_w);

		elsif	(:new.tx_proc_ajuste is not null) and
			(nvl(:new.tx_proc_ajuste,-1) <> nvl(:old.tx_proc_ajuste,-1)) then

			:new.vl_total_ajuste:= vl_item_w;
			tx_dif_w:= :new.tx_proc_ajuste - :new.tx_proc_original;
			:new.vl_total_ajuste:= :new.vl_total_ajuste + (vl_item_w * tx_dif_w / 100);

		end if;

	end if;
end if;

select	max(nr_interno_conta),
	max(nr_atendimento),
	max(dt_conta),
	max(dt_procedimento)
into	nr_interno_conta_w,
	nr_atendimento_w,
	dt_conta_w,
	dt_procedimento_w
from	procedimento_paciente
where	nr_sequencia = :new.nr_seq_propaci;

select	max(nr_interno_conta),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_conta_audit_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from	auditoria_conta_paciente
where	nr_sequencia = :new.nr_seq_auditoria;

qt_item_periodo_w	:= 1;
begin
select	nvl(max(1),0)
into	qt_item_periodo_w
from	dual
where	((dt_conta_w between dt_periodo_inicial_w and dt_periodo_final_w) or
	 (dt_procedimento_w between dt_periodo_inicial_w and dt_periodo_final_w));
exception
	when others then
	qt_item_periodo_w	:= 1;
end;

if	((nvl(nr_interno_conta_w,0) <> 0) and
	 (nvl(nr_conta_audit_w,0) <> 0) and
	 (nvl(nr_interno_conta_w,0) <> nvl(nr_conta_audit_w,0))) or
	(qt_item_periodo_w = 0) then

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into w_log_atend_audit (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_audit_item,
		nr_seq_auditoria,
		ds_call_stack,
		ie_tipo_item,
		nr_seq_item,
		dt_periodo_inicial,
		dt_periodo_final,
		ds_observacao
	) values (
		w_log_atend_audit_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_auditoria,
		ds_call_stack_w,
		'P',
		:new.nr_seq_propaci,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		decode(qt_item_periodo_w, 0, substr(wheb_mensagem_pck.get_texto(309510),1,255), substr(wheb_mensagem_pck.get_texto(309511),1,255))
	);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_PROPACI_INSERT
before insert ON TASY.AUDITORIA_PROPACI for each row
declare

nr_seq_pend_w		Number(10,0);
nr_seq_estagio_pend_w	Number(10,0);
vl_item_w		Number(15,2);
nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
nr_conta_audit_w	number(10,0);
ds_call_stack_w		varchar2(2000);
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
dt_conta_w		date;
dt_procedimento_w	date;
qt_item_periodo_w	number(10,0);

begin

select 	max(nr_sequencia)
into	nr_seq_pend_w
from 	cta_pendencia
where 	nr_seq_auditoria = :new.nr_seq_auditoria;

if	(nvl(nr_seq_pend_w,0) > 0) then

	select 	max(nr_seq_estagio)
	into	nr_seq_estagio_pend_w
	from  	cta_pendencia
	where 	nr_sequencia = nr_seq_pend_w;

	if	(nvl(nr_seq_estagio_pend_w,0) > 0) then

		:new.nr_seq_estagio_pend:= nr_seq_estagio_pend_w;

	end if;
end if;

begin
select	nvl(a.vl_procedimento,0)
into	vl_item_w
from	procedimento_paciente a
where	a.nr_sequencia		= :new.nr_seq_propaci
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote,0)	<> a.nr_sequencia;
exception
	when others then
	:new.vl_total_ajuste:= 0;
	vl_item_w:= 0;
end;

:new.vl_total_ajuste:= vl_item_w;

:new.ie_exige_auditoria := obter_se_exige_auditoria(:new.nr_seq_propaci, 2);

select	max(nr_interno_conta),
	max(nr_atendimento),
	max(dt_conta),
	max(dt_procedimento)
into	nr_interno_conta_w,
	nr_atendimento_w,
	dt_conta_w,
	dt_procedimento_w
from	procedimento_paciente
where	nr_sequencia = :new.nr_seq_propaci;

select	max(nr_interno_conta),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	nr_conta_audit_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from	auditoria_conta_paciente
where	nr_sequencia = :new.nr_seq_auditoria;

qt_item_periodo_w	:= 1;
begin
select	nvl(max(1),0)
into	qt_item_periodo_w
from	dual
where	((dt_conta_w between dt_periodo_inicial_w and dt_periodo_final_w) or
	 (dt_procedimento_w between dt_periodo_inicial_w and dt_periodo_final_w));
exception
	when others then
	qt_item_periodo_w	:= 1;
end;

if	((nvl(nr_interno_conta_w,0) <> 0) and
	 (nvl(nr_conta_audit_w,0) <> 0) and
	 (nvl(nr_interno_conta_w,0) <> nvl(nr_conta_audit_w,0))) or
	(qt_item_periodo_w = 0) then

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into w_log_atend_audit (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_audit_item,
		nr_seq_auditoria,
		ds_call_stack,
		ie_tipo_item,
		nr_seq_item,
		dt_periodo_inicial,
		dt_periodo_final,
		ds_observacao
	) values (
		w_log_atend_audit_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_auditoria,
		ds_call_stack_w,
		'P',
		:new.nr_seq_propaci,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		decode(qt_item_periodo_w, 0, substr(wheb_mensagem_pck.get_texto(309510),1,255), substr(wheb_mensagem_pck.get_texto(309511),1,255))
	);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AUDITORIA_PROPACI_tp  after update ON TASY.AUDITORIA_PROPACI FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.QT_AJUSTE,1,500);gravar_log_alteracao(substr(:old.QT_AJUSTE,1,4000),substr(:new.QT_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_AJUSTE',ie_log_w,ds_w,'AUDITORIA_PROPACI',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AUDITORIA_PROPACI ADD (
  CONSTRAINT AUDPROP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUDITORIA_PROPACI ADD (
  CONSTRAINT AUDPROP_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_ORIGINAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDPROP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_AJUSTE) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AUDPROP_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_AJUSTE, CD_CATEGORIA_AJUSTE) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AUDPROP_AUDCOPA_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.AUDITORIA_CONTA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUDPROP_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUDPROP_AUDMOTI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.AUDITORIA_MOTIVO (NR_SEQUENCIA),
  CONSTRAINT AUDPROP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_AJUSTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDPROP_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_RESP_GLOSA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUDPROP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP_JUST) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUDPROP_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP_GLOSA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AUDITORIA_PROPACI TO NIVEL_1;

