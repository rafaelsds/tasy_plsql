ALTER TABLE TASY.W_EST_DIAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_EST_DIAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_EST_DIAS
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  PER_FINAL     NUMBER(10),
  NM_USUARIO    VARCHAR2(15 BYTE),
  PER_INICIAL   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WESTDIAS_PK ON TASY.W_EST_DIAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WESTDIAS_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_EST_DIAS ADD (
  CONSTRAINT WESTDIAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_EST_DIAS TO NIVEL_1;

