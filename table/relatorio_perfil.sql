ALTER TABLE TASY.RELATORIO_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_PERFIL
(
  CD_PERFIL               NUMBER(5)             NOT NULL,
  NR_SEQ_RELATORIO        NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  QT_COPIA                NUMBER(6)             NOT NULL,
  IE_LOGO                 VARCHAR2(1 BYTE)      NOT NULL,
  IE_CONFIGURAR           VARCHAR2(1 BYTE)      NOT NULL,
  IE_IMPRIMIR             VARCHAR2(1 BYTE),
  IE_OUTPUTBIN            NUMBER(3),
  DS_IMPRESSORA           VARCHAR2(60 BYTE),
  NR_SEQ_APRESENT         NUMBER(10),
  IE_GRAVAR_LOG           VARCHAR2(1 BYTE)      NOT NULL,
  IE_VISUALIZAR           VARCHAR2(1 BYTE)      NOT NULL,
  IE_APRESENTA            VARCHAR2(1 BYTE),
  IE_FORMA_IMPRESSAO      VARCHAR2(5 BYTE),
  IE_APRES_MENSAGEM       VARCHAR2(1 BYTE),
  IE_FRENTE_VERSO         VARCHAR2(1 BYTE),
  IE_FOCAR_OK             VARCHAR2(1 BYTE),
  CD_ESTAB                NUMBER(4),
  IE_VISUALIZA_QRPRINTER  VARCHAR2(1 BYTE),
  IE_IMPRESSORA_SETOR     VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  QT_VIAS                 NUMBER(7)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELPERF_ESTABEL_FK_I ON TASY.RELATORIO_PERFIL
(CD_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELPERF_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELPERF_I1 ON TASY.RELATORIO_PERFIL
(CD_PERFIL, NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELPERF_PERFIL_FK_I_I ON TASY.RELATORIO_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELPERF_PK ON TASY.RELATORIO_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELPERF_RELATOR_FK_I_I ON TASY.RELATORIO_PERFIL
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RELATORIO_PERFIL_tp  after update ON TASY.RELATORIO_PERFIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_COPIA,1,4000),substr(:new.QT_COPIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_COPIA',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_VIAS,1,4000),substr(:new.QT_VIAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_VIAS',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRIMIR,1,4000),substr(:new.IE_IMPRIMIR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRIMIR',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OUTPUTBIN,1,4000),substr(:new.IE_OUTPUTBIN,1,4000),:new.nm_usuario,nr_seq_w,'IE_OUTPUTBIN',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RELATORIO,1,4000),substr(:new.NR_SEQ_RELATORIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RELATORIO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOGO,1,4000),substr(:new.IE_LOGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOGO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENT,1,4000),substr(:new.NR_SEQ_APRESENT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENT',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_IMPRESSORA,1,4000),substr(:new.DS_IMPRESSORA,1,4000),:new.nm_usuario,nr_seq_w,'DS_IMPRESSORA',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAVAR_LOG,1,4000),substr(:new.IE_GRAVAR_LOG,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAVAR_LOG',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VISUALIZAR,1,4000),substr(:new.IE_VISUALIZAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_VISUALIZAR',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APRESENTA,1,4000),substr(:new.IE_APRESENTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRESENTA',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_IMPRESSAO,1,4000),substr(:new.IE_FORMA_IMPRESSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_IMPRESSAO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APRES_MENSAGEM,1,4000),substr(:new.IE_APRES_MENSAGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRES_MENSAGEM',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRENTE_VERSO,1,4000),substr(:new.IE_FRENTE_VERSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRENTE_VERSO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FOCAR_OK,1,4000),substr(:new.IE_FOCAR_OK,1,4000),:new.nm_usuario,nr_seq_w,'IE_FOCAR_OK',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTAB,1,4000),substr(:new.CD_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTAB',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VISUALIZA_QRPRINTER,1,4000),substr(:new.IE_VISUALIZA_QRPRINTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_VISUALIZA_QRPRINTER',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRESSORA_SETOR,1,4000),substr(:new.IE_IMPRESSORA_SETOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRESSORA_SETOR',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONFIGURAR,1,4000),substr(:new.IE_CONFIGURAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONFIGURAR',ie_log_w,ds_w,'RELATORIO_PERFIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RELATORIO_PERFIL ADD (
  CONSTRAINT RELPERF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO_PERFIL ADD (
  CONSTRAINT RELPERF_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RELPERF_PERFIL_FK_I 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT RELPERF_RELATOR_FK_I 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RELATORIO_PERFIL TO NIVEL_1;

