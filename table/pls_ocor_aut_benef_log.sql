ALTER TABLE TASY.PLS_OCOR_AUT_BENEF_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCOR_AUT_BENEF_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCOR_AUT_BENEF_LOG
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  IE_FUNCAO                  VARCHAR2(2 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_OCORRENCIA_BENEF    NUMBER(10),
  NR_SEQ_GUIA_PLANO          NUMBER(10),
  NR_SEQ_EXECUCAO            NUMBER(10),
  NR_SEQ_REQUISICAO          NUMBER(10),
  NR_SEQ_GUIA_PLANO_MAT      NUMBER(10),
  NR_SEQ_GUIA_PLANO_PROC     NUMBER(10),
  NR_SEQ_REQUISICAO_PROC     NUMBER(10),
  NR_SEQ_REQUISICAO_MAT      NUMBER(10),
  NR_SEQ_EXEC_MAT            NUMBER(10),
  NR_SEQ_OCOR_REGRA          NUMBER(10),
  NR_SEQ_EXEC_PROC           NUMBER(10),
  NR_SEQ_OCOR_AUT_COMBINADA  NUMBER(10),
  NR_SEQ_OCOR_AUT_FILTRO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOABL_PK ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLGUIMAT_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_GUIA_PLANO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLGUIMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLGUPRO_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_GUIA_PLANO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLGUPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSEXRE_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_EXECUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSEXRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSEXRI_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_EXEC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSEXRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSEXRP_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_EXEC_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSEXRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSGUIA_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_GUIA_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSGUIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSOAUC_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_OCOR_AUT_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSOAUC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSOAUF_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_OCOR_AUT_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSOAUF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSOCBR_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_OCORRENCIA_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSOCBR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSOCRE_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_OCOR_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSOCRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSREQU_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSREQU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSRMAT_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_REQUISICAO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSRMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOABL_PLSRPRO_FK_I ON TASY.PLS_OCOR_AUT_BENEF_LOG
(NR_SEQ_REQUISICAO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOABL_PLSRPRO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_OCOR_AUT_BENEF_LOG ADD (
  CONSTRAINT PLSOABL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCOR_AUT_BENEF_LOG ADD (
  CONSTRAINT PLSOABL_PLGUIMAT_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PLANO_MAT) 
 REFERENCES TASY.PLS_GUIA_PLANO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLGUPRO_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PLANO_PROC) 
 REFERENCES TASY.PLS_GUIA_PLANO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSEXRE_FK 
 FOREIGN KEY (NR_SEQ_EXECUCAO) 
 REFERENCES TASY.PLS_EXECUCAO_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSOABL_PLSEXRI_FK 
 FOREIGN KEY (NR_SEQ_EXEC_MAT) 
 REFERENCES TASY.PLS_EXECUCAO_REQ_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSEXRP_FK 
 FOREIGN KEY (NR_SEQ_EXEC_PROC) 
 REFERENCES TASY.PLS_EXECUCAO_REQ_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA_PLANO) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSOAUC_FK 
 FOREIGN KEY (NR_SEQ_OCOR_AUT_COMBINADA) 
 REFERENCES TASY.PLS_OCOR_AUT_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSOAUF_FK 
 FOREIGN KEY (NR_SEQ_OCOR_AUT_FILTRO) 
 REFERENCES TASY.PLS_OCOR_AUT_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSOCBR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA_BENEF) 
 REFERENCES TASY.PLS_OCORRENCIA_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSOCRE_FK 
 FOREIGN KEY (NR_SEQ_OCOR_REGRA) 
 REFERENCES TASY.PLS_OCORRENCIA_REGRA (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSOABL_PLSRMAT_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO_MAT) 
 REFERENCES TASY.PLS_REQUISICAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSOABL_PLSRPRO_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO_PROC) 
 REFERENCES TASY.PLS_REQUISICAO_PROC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OCOR_AUT_BENEF_LOG TO NIVEL_1;

