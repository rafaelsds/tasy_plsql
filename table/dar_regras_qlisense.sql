ALTER TABLE TASY.DAR_REGRAS_QLISENSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DAR_REGRAS_QLISENSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DAR_REGRAS_QLISENSE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_IP                VARCHAR2(255 BYTE),
  DS_DOMINIO           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DREGQLIK_PK ON TASY.DAR_REGRAS_QLISENSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DAR_REGRAS_QLISENSE ADD (
  CONSTRAINT DREGQLIK_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.DAR_REGRAS_QLISENSE TO NIVEL_1;

