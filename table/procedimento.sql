ALTER TABLE TASY.PROCEDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO
(
  CD_PROCEDIMENTO           NUMBER(15)          NOT NULL,
  DS_PROCEDIMENTO           VARCHAR2(255 BYTE)  NOT NULL,
  DS_COMPLEMENTO            VARCHAR2(240 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_GRUPO_PROC             NUMBER(15)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_TIPO_PROCEDIMENTO      NUMBER(3),
  IE_CLASSIFICACAO          VARCHAR2(1 BYTE)    NOT NULL,
  CD_LAUDO_PADRAO           NUMBER(5),
  CD_SETOR_EXCLUSIVO        NUMBER(5),
  IE_ORIGEM_PROCED          NUMBER(10)          NOT NULL,
  QT_DIA_INTERNACAO_SUS     NUMBER(5),
  QT_IDADE_MINIMA_SUS       NUMBER(5),
  QT_IDADE_MAXIMA_SUS       NUMBER(4),
  IE_SEXO_SUS               VARCHAR2(1 BYTE),
  IE_INREAL_SUS             VARCHAR2(1 BYTE),
  IE_INATOM_SUS             VARCHAR2(1 BYTE),
  CD_GRUPO_SUS              VARCHAR2(8 BYTE),
  CD_DOENCA_CID             VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO         VARCHAR2(10 BYTE),
  DS_PROC_INTERNO           VARCHAR2(80 BYTE),
  NR_PROC_INTERNO           VARCHAR2(20 BYTE),
  IE_UTIL_PRESCRICAO        VARCHAR2(1 BYTE),
  CD_KIT_MATERIAL           NUMBER(5),
  DS_ORIENTACAO             VARCHAR2(4000 BYTE),
  IE_VALOR_ESPECIAL         VARCHAR2(1 BYTE),
  DT_CARGA                  DATE,
  QT_MAX_PROCEDIMENTO       NUMBER(5),
  IE_EXIGE_LAUDO            VARCHAR2(1 BYTE),
  IE_FORMA_APRESENTACAO     NUMBER(2),
  IE_APURACAO_CUSTO         VARCHAR2(1 BYTE),
  QT_HORA_BAIXAR_PRESCR     NUMBER(5),
  NR_SEQ_GRUPO_REC          NUMBER(10),
  DS_PRESCRICAO             VARCHAR2(25 BYTE),
  IE_EXIGE_AUTOR_SUS        VARCHAR2(1 BYTE)    NOT NULL,
  QT_EXEC_BARRA             NUMBER(3)           NOT NULL,
  DS_PROCEDIMENTO_PESQUISA  VARCHAR2(255 BYTE),
  IE_ESPECIALIDADE_AIH      NUMBER(2),
  IE_ATIV_PROF_BPA          VARCHAR2(1 BYTE)    NOT NULL,
  CD_ATIVIDADE_PROF_BPA     NUMBER(3),
  IE_ALTA_COMPLEXIDADE      VARCHAR2(1 BYTE)    NOT NULL,
  CD_GRUPO_PROC_AIH         NUMBER(8),
  IE_IGNORA_ORIGEM          VARCHAR2(1 BYTE)    NOT NULL,
  IE_ESTADIO                VARCHAR2(1 BYTE),
  IE_EXIGE_LADO             VARCHAR2(1 BYTE),
  IE_CLASSIF_CUSTO          VARCHAR2(1 BYTE)    NOT NULL,
  CD_INTERVALO              VARCHAR2(7 BYTE),
  CD_SUBGRUPO_BPA           NUMBER(15),
  IE_CREDENCIAMENTO_SUS     VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_ORG          NUMBER(10),
  IE_ASSOC_ADEP             VARCHAR2(1 BYTE),
  IE_TIPO_DESPESA_TISS      VARCHAR2(3 BYTE),
  IE_LOCALIZADOR            VARCHAR2(1 BYTE),
  IE_PORTE_CIRURGIA         VARCHAR2(1 BYTE),
  CD_PROC_CIH               NUMBER(15),
  DS_ORIENTACAO_SMS         VARCHAR2(150 BYTE),
  IE_COBRA_ADEP             VARCHAR2(1 BYTE),
  IE_COBRAR_HORARIO         VARCHAR2(1 BYTE),
  IE_EXIGE_PESO_AGENDA      VARCHAR2(1 BYTE),
  NR_SEQ_ITEM_SERV          NUMBER(10),
  DS_COMPLEMENTO_SUS        VARCHAR2(4000 BYTE),
  IE_GERA_ASSOCIADO         VARCHAR2(1 BYTE),
  IE_DUPLA_CHECAGEM         VARCHAR2(1 BYTE),
  IE_PROTOCOLO_TEV          VARCHAR2(1 BYTE),
  IE_ALTO_CUSTO_IPASGO      VARCHAR2(15 BYTE),
  IE_ODONTOLOGICO           VARCHAR2(1 BYTE),
  CD_PROCIMENTO_MX          VARCHAR2(5 BYTE),
  CD_PROCEDIMENTO_LOC       VARCHAR2(15 BYTE),
  DS_TIPO_VASECTOMIA        VARCHAR2(2 BYTE),
  CD_SISTEMA_ANT            VARCHAR2(80 BYTE),
  NR_SEQ_TUSS_SERV          NUMBER(10),
  IE_PROCED_TYPE            VARCHAR2(1 BYTE),
  IE_PATIENT_CLASS          VARCHAR2(2 BYTE),
  NR_SEQ_CATALOGO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.PROCEDIMENTO.CD_PROCEDIMENTO IS 'Codigo do Procedimento Hospitalar';

COMMENT ON COLUMN TASY.PROCEDIMENTO.DS_PROCEDIMENTO IS 'Descri��o do Procedimento';

COMMENT ON COLUMN TASY.PROCEDIMENTO.DS_COMPLEMENTO IS 'Descri�ao Complementar do Procedimento';

COMMENT ON COLUMN TASY.PROCEDIMENTO.IE_SITUACAO IS 'Indicador da Situacao';

COMMENT ON COLUMN TASY.PROCEDIMENTO.CD_GRUPO_PROC IS 'Codigo do Grupo de Procedimentos';


CREATE INDEX TASY.PROCEDI_CAPROC_FK_I ON TASY.PROCEDIMENTO
(CD_PROCIMENTO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_CIDDOEN_FK_I ON TASY.PROCEDIMENTO
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_CIDSECU_FK_I ON TASY.PROCEDIMENTO
(CD_CID_SECUNDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_CIDSECU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_CIHPROC_FK_I ON TASY.PROCEDIMENTO
(CD_PROC_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          296K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_GRUPRO_FK_I ON TASY.PROCEDIMENTO
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_GRURECE_FK_I ON TASY.PROCEDIMENTO
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_GRUSRVIT_FK_I ON TASY.PROCEDIMENTO
(NR_SEQ_ITEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_GRUSRVIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_ICD ON TASY.PROCEDIMENTO
(CD_TIPO_PROCEDIMENTO, CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_I1 ON TASY.PROCEDIMENTO
(DS_PROCEDIMENTO, IE_CLASSIFICACAO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_I11 ON TASY.PROCEDIMENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED, IE_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_I2 ON TASY.PROCEDIMENTO
(NR_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_I3 ON TASY.PROCEDIMENTO
(DS_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_I3
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_I9 ON TASY.PROCEDIMENTO
(DS_PROCEDIMENTO_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_KITMATE_FK_I ON TASY.PROCEDIMENTO
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROCEDI_PK ON TASY.PROCEDIMENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_SEARCH ON TASY.PROCEDIMENTO
("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_PROCEDIMENTO"))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_SETATEN_FK_I ON TASY.PROCEDIMENTO
(CD_SETOR_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_SUSATPR_FK_I ON TASY.PROCEDIMENTO
(CD_ATIVIDADE_PROF_BPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_SUSATPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_SUSFORG_FK_I ON TASY.PROCEDIMENTO
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_SUSGRPA_FK_I ON TASY.PROCEDIMENTO
(CD_GRUPO_PROC_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCEDI_SUSGRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCEDI_SUSSBPA_FK_I ON TASY.PROCEDIMENTO
(CD_SUBGRUPO_BPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_TUSSERV_FK_I ON TASY.PROCEDIMENTO
(NR_SEQ_TUSS_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCEDI_12 ON TASY.PROCEDIMENTO
(CD_PROCEDIMENTO_LOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.procedimento_atual

before insert or update or delete ON TASY.PROCEDIMENTO 
for each row
declare

  vl_tam_max_proc_w CONSTANT number := 240;

  qt_reg_w                   number(10,0);

  ie_consiste_inativacao_w   varchar2(1);

  nr_seq_combinacao_w        pls_combinacao_item_cta.nr_sequencia%type;

  ie_origem_proced_w         procedimento.ie_origem_proced%type;

  cd_procedimento_w          procedimento.cd_procedimento%type;

begin


--Foi necess�rio utilizar a trigger na dele��o de registros, ent�o o que a trigger tratava antes, deixamos intocado, n�o fazendo no evento de deletar.

if  (not deleting) then


  if billing_i18n_pck.obter_se_ds_proc_255 = 'N' then

    :new.ds_procedimento :=  substr(STR1 => :new.ds_procedimento
                                   ,POS  => 1
                                   ,LEN  => vl_tam_max_proc_w);

  end if;



  ie_consiste_inativacao_w:= nvl(Obter_valor_param_usuario(25,14,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, WHEB_USUARIO_PCK.get_cd_estabelecimento),'N');



  if  (length(:new.cd_procedimento) > 15) then

    --r.aise_application_error(-20001,'O tamanho m�ximo do codigo do procedimento � de 15 digitos!');

    wheb_mensagem_pck.exibir_mensagem_abort(263422);

  end if;



  if  (:new.ds_procedimento_pesquisa is null) or

    (:new.ds_procedimento <> :old.ds_procedimento ) then

    :new.ds_procedimento_pesquisa  := upper(elimina_acentuacao(:new.ds_procedimento));

  end if;



  if      (ie_consiste_inativacao_w = 'S') and

    (updating) and

    (:new.ie_situacao = 'I') and   --Registro de exclus�o

    (:old.ie_situacao = 'A') then



    select   count(*)

    into  qt_reg_w

    from   proc_interno

    where   cd_procedimento  = :new.cd_procedimento

    and   ie_origem_proced = :new.ie_origem_proced

    and  ie_situacao   = 'A';



    if  (qt_reg_w > 0) then

      --R.aise_application_error(-20011,'N�o � poss�vel inativar o procedimento, pois o mesmo est� vinculado � um procedimento interno que est� ativo');

      wheb_mensagem_pck.exibir_mensagem_abort(263423);

    else

      select   count(*)

      into  qt_reg_w

      from   proc_interno_conv a,

        proc_interno    b

      where   a.nr_seq_proc_interno = b.nr_sequencia

      and   a.cd_procedimento  = :new.cd_procedimento

      and   a.ie_origem_proced = :new.ie_origem_proced

      and  a.ie_situacao   = 'A'

      and  b.ie_situacao   = 'A';



      if  (qt_reg_w > 0) then

        --R.aise_application_error(-20011,'N�o � poss�vel inativar o procedimento, pois o mesmo est� vinculado � um procedimento interno que est� ativo');

        wheb_mensagem_pck.exibir_mensagem_abort(263423);

      end  if;

    end if;

  end if;

end if;



if (deleting or updating) then



  -- se for apagar o registro

  if  (deleting) then

    ie_origem_proced_w  := :old.ie_origem_proced;

    cd_procedimento_w  := :old.cd_procedimento;



  else

    ie_origem_proced_w  := :new.ie_origem_proced;

    cd_procedimento_w  := :new.cd_procedimento;

  end if;



  pls_gerencia_upd_obj_pck.marcar_para_atualizacao(  'PLS_ESTRUTURA_OCOR_TM', wheb_usuario_pck.get_nm_usuario,

                'cd_procedimento_p=' || cd_procedimento_w||',ie_origem_proced_p='||ie_origem_proced_w);



end if;



end;
/


CREATE OR REPLACE TRIGGER TASY.PROCEDIMENTO_tp  after update ON TASY.PROCEDIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_PROCEDIMENTO='||to_char(:old.CD_PROCEDIMENTO)||'#@#@IE_ORIGEM_PROCED='||to_char(:old.IE_ORIGEM_PROCED);gravar_log_alteracao(substr(:old.IE_ODONTOLOGICO,1,4000),substr(:new.IE_ODONTOLOGICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ODONTOLOGICO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_SMS,1,4000),substr(:new.DS_ORIENTACAO_SMS,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_SMS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FORMA_ORG,1,4000),substr(:new.NR_SEQ_FORMA_ORG,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FORMA_ORG',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR_ESPECIAL,1,4000),substr(:new.IE_VALOR_ESPECIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_ESPECIAL',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UTIL_PRESCRICAO,1,4000),substr(:new.IE_UTIL_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTIL_PRESCRICAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO,1,4000),substr(:new.DS_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROC_INTERNO,1,4000),substr(:new.DS_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROC_INTERNO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PROC_INTERNO,1,4000),substr(:new.NR_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PROC_INTERNO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LAUDO_PADRAO,1,4000),substr(:new.CD_LAUDO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_LAUDO_PADRAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_EXCLUSIVO,1,4000),substr(:new.CD_SETOR_EXCLUSIVO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_EXCLUSIVO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MAXIMA_SUS,1,4000),substr(:new.QT_IDADE_MAXIMA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MAXIMA_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INREAL_SUS,1,4000),substr(:new.IE_INREAL_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_INREAL_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOENCA_CID,1,4000),substr(:new.CD_DOENCA_CID,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA_CID',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROCEDIMENTO,1,4000),substr(:new.DS_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROCEDIMENTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_KIT_MATERIAL,1,4000),substr(:new.CD_KIT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_KIT_MATERIAL',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INATOM_SUS,1,4000),substr(:new.IE_INATOM_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_INATOM_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_SUS,1,4000),substr(:new.CD_GRUPO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MINIMA_SUS,1,4000),substr(:new.QT_IDADE_MINIMA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MINIMA_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_LAUDO,1,4000),substr(:new.IE_EXIGE_LAUDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_LAUDO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CID_SECUNDARIO,1,4000),substr(:new.CD_CID_SECUNDARIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CID_SECUNDARIO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_PROCEDIMENTO,1,4000),substr(:new.CD_TIPO_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_PROCEDIMENTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_APRESENTACAO,1,4000),substr(:new.IE_FORMA_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_APRESENTACAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APURACAO_CUSTO,1,4000),substr(:new.IE_APURACAO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APURACAO_CUSTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_HORA_BAIXAR_PRESCR,1,4000),substr(:new.QT_HORA_BAIXAR_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORA_BAIXAR_PRESCR',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAX_PROCEDIMENTO,1,4000),substr(:new.QT_MAX_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_PROCEDIMENTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CARGA,1,4000),substr(:new.DT_CARGA,1,4000),:new.nm_usuario,nr_seq_w,'DT_CARGA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO,1,4000),substr(:new.IE_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_INTERNACAO_SUS,1,4000),substr(:new.QT_DIA_INTERNACAO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_INTERNACAO_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO_SUS,1,4000),substr(:new.IE_SEXO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PRESCRICAO,1,4000),substr(:new.DS_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PRESCRICAO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROCEDIMENTO_PESQUISA,1,4000),substr(:new.DS_PROCEDIMENTO_PESQUISA,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROCEDIMENTO_PESQUISA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTA_COMPLEXIDADE,1,4000),substr(:new.IE_ALTA_COMPLEXIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTA_COMPLEXIDADE',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EXEC_BARRA,1,4000),substr(:new.QT_EXEC_BARRA,1,4000),:new.nm_usuario,nr_seq_w,'QT_EXEC_BARRA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_AUTOR_SUS,1,4000),substr(:new.IE_EXIGE_AUTOR_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_AUTOR_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC_AIH,1,4000),substr(:new.CD_GRUPO_PROC_AIH,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC_AIH',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IGNORA_ORIGEM,1,4000),substr(:new.IE_IGNORA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_IGNORA_ORIGEM',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESPECIALIDADE_AIH,1,4000),substr(:new.IE_ESPECIALIDADE_AIH,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESPECIALIDADE_AIH',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATIV_PROF_BPA,1,4000),substr(:new.IE_ATIV_PROF_BPA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATIV_PROF_BPA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ATIVIDADE_PROF_BPA,1,4000),substr(:new.CD_ATIVIDADE_PROF_BPA,1,4000),:new.nm_usuario,nr_seq_w,'CD_ATIVIDADE_PROF_BPA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADIO,1,4000),substr(:new.IE_ESTADIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADIO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_LADO,1,4000),substr(:new.IE_EXIGE_LADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_LADO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_CUSTO,1,4000),substr(:new.IE_CLASSIF_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_CUSTO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_BPA,1,4000),substr(:new.CD_SUBGRUPO_BPA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_BPA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERVALO,1,4000),substr(:new.CD_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA_TISS,1,4000),substr(:new.IE_TIPO_DESPESA_TISS,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA_TISS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ASSOC_ADEP,1,4000),substr(:new.IE_ASSOC_ADEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_ASSOC_ADEP',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOCALIZADOR,1,4000),substr(:new.IE_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOCALIZADOR',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTE_CIRURGIA,1,4000),substr(:new.IE_PORTE_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTE_CIRURGIA',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_CIH,1,4000),substr(:new.CD_PROC_CIH,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_CIH',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CREDENCIAMENTO_SUS,1,4000),substr(:new.IE_CREDENCIAMENTO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CREDENCIAMENTO_SUS',ie_log_w,ds_w,'PROCEDIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


DROP SYNONYM TASY_CONSULTA.PROCEDIMENTO;

CREATE SYNONYM TASY_CONSULTA.PROCEDIMENTO FOR TASY.PROCEDIMENTO;


ALTER TABLE TASY.PROCEDIMENTO ADD (
  CONSTRAINT PROCEDI_PK
 PRIMARY KEY
 (CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          704K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO ADD (
  CONSTRAINT PROCEDI_TUSSERV_FK 
 FOREIGN KEY (NR_SEQ_TUSS_SERV) 
 REFERENCES TASY.TUSS_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PROCEDI_SUSATPR_FK 
 FOREIGN KEY (CD_ATIVIDADE_PROF_BPA) 
 REFERENCES TASY.SUS_ATIVIDADE_PROFISSIONAL (CD_ATIVIDADE_PROFISSIONAL),
  CONSTRAINT PROCEDI_SUSGRPA_FK 
 FOREIGN KEY (CD_GRUPO_PROC_AIH) 
 REFERENCES TASY.SUS_GRUPO_PROC_AIH (CD_GRUPO_PROC_AIH),
  CONSTRAINT PROCEDI_SUSSBPA_FK 
 FOREIGN KEY (CD_SUBGRUPO_BPA) 
 REFERENCES TASY.SUS_SUBGRUPO_BPA (CD_SUBGRUPO_BPA),
  CONSTRAINT PROCEDI_GRUSRVIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM_SERV) 
 REFERENCES TASY.GRUPO_SERVICO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PROCEDI_CIHPROC_FK 
 FOREIGN KEY (CD_PROC_CIH) 
 REFERENCES TASY.CIH_PROCEDIMENTO (CD_PROCEDIMENTO),
  CONSTRAINT PROCEDI_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT PROCEDI_CIDSECU_FK 
 FOREIGN KEY (CD_CID_SECUNDARIO) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PROCEDI_GRUPRO_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PROCEDI_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT PROCEDI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXCLUSIVO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PROCEDI_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PROCEDI_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROCEDIMENTO TO NIVEL_1;

GRANT SELECT ON TASY.PROCEDIMENTO TO ROBOCMTECNOLOGIA;

GRANT SELECT ON TASY.PROCEDIMENTO TO TASY_CONSULTA;

