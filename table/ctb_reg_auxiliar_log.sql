ALTER TABLE TASY.CTB_REG_AUXILIAR_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REG_AUXILIAR_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REG_AUXILIAR_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REG_AUXILIAR  NUMBER(10),
  IE_TIPO_LOG          VARCHAR2(15 BYTE),
  DS_LOG               VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CTBREGLOG_PK ON TASY.CTB_REG_AUXILIAR_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_reg_auxiliar_log_delete
before delete or insert or update ON TASY.CTB_REG_AUXILIAR_LOG for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	wheb_mensagem_pck.exibir_mensagem_abort(404646);

end if;

end;
/


ALTER TABLE TASY.CTB_REG_AUXILIAR_LOG ADD (
  CONSTRAINT CTBREGLOG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CTB_REG_AUXILIAR_LOG TO NIVEL_1;

