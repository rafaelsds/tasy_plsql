ALTER TABLE TASY.TX_MEDIC_PADROES_CADAVER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_MEDIC_PADROES_CADAVER CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_MEDIC_PADROES_CADAVER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)        NOT NULL,
  QT_DOSE              NUMBER(15,3),
  CD_INTERVALO         VARCHAR2(7 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TXMEDPACADD_INTPRES_FK_I ON TASY.TX_MEDIC_PADROES_CADAVER
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXMEDPACADD_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXMEDPACADD_MATERIA_FK_I ON TASY.TX_MEDIC_PADROES_CADAVER
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXMEDPACADD_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXMEDPACADD_PK ON TASY.TX_MEDIC_PADROES_CADAVER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXMEDPACADD_UNIMEDI_FK_I ON TASY.TX_MEDIC_PADROES_CADAVER
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXMEDPACADD_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXMEDPACADD_VIAAPLI_FK_I ON TASY.TX_MEDIC_PADROES_CADAVER
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXMEDPACADD_VIAAPLI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TX_MEDIC_PADROES_CADAVER ADD (
  CONSTRAINT TXMEDPACADD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_MEDIC_PADROES_CADAVER ADD (
  CONSTRAINT TXMEDPACADD_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT TXMEDPACADD_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT TXMEDPACADD_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT TXMEDPACADD_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO));

GRANT SELECT ON TASY.TX_MEDIC_PADROES_CADAVER TO NIVEL_1;

