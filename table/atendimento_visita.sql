ALTER TABLE TASY.ATENDIMENTO_VISITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_VISITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_VISITA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_ATENDIMENTO        NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ENTRADA            DATE                    NOT NULL,
  NR_SEQ_TIPO           NUMBER(10),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  DT_SAIDA              DATE,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_CONTROLE       NUMBER(15),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_VISITANTE          VARCHAR2(60 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_IDENTIDADE         VARCHAR2(15 BYTE),
  NR_TELEFONE           VARCHAR2(15 BYTE),
  DT_NASCIMENTO         DATE,
  DS_CONTROLE           VARCHAR2(50 BYTE),
  DS_MODELO_CARRO       VARCHAR2(30 BYTE),
  CD_PLACA_CARRO        VARCHAR2(10 BYTE),
  CD_FUNCIONARIO        VARCHAR2(10 BYTE),
  NR_SENHA_ESPERA       NUMBER(6),
  IE_STATUS             VARCHAR2(15 BYTE),
  IE_PACIENTE           VARCHAR2(1 BYTE),
  CD_EMPRESA            NUMBER(10),
  NM_USUARIO_SAIDA      VARCHAR2(15 BYTE),
  DT_ENTRADA_REAL       DATE,
  DT_SAIDA_REAL         DATE,
  DT_LIBERACAO_ACESSO   DATE,
  CD_SETOR_ATEND        NUMBER(5),
  NR_CONTROLE_ACESSO    NUMBER(10),
  NM_COMPUTADOR         VARCHAR2(255 BYTE),
  NR_DDI_TELEFONE       VARCHAR2(3 BYTE),
  IE_SEXO               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEVISI_ATEPACI_FK_I ON TASY.ATENDIMENTO_VISITA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_EMPREFE_FK_I ON TASY.ATENDIMENTO_VISITA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEVISI_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEVISI_I1 ON TASY.ATENDIMENTO_VISITA
(DT_ENTRADA, DT_SAIDA, CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_I2 ON TASY.ATENDIMENTO_VISITA
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_I3 ON TASY.ATENDIMENTO_VISITA
(NR_ATENDIMENTO, DT_ENTRADA, NVL("IE_PACIENTE",'N'), DT_SAIDA, 1)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_PESFISI_FK_I ON TASY.ATENDIMENTO_VISITA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_PESFISI_FK2_I ON TASY.ATENDIMENTO_VISITA
(CD_FUNCIONARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEVISI_PK ON TASY.ATENDIMENTO_VISITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_SETATEN_FK_I ON TASY.ATENDIMENTO_VISITA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEVISI_SETATEN_FK2_I ON TASY.ATENDIMENTO_VISITA
(CD_SETOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEVISI_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEVISI_VISITAN_FK_I ON TASY.ATENDIMENTO_VISITA
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEVISI_VISITAN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.HSJ_ATENDIMENTO_VISITA_UPDATE
after update ON TASY.ATENDIMENTO_VISITA for each row
DECLARE
nm_usuario_w varchar(20);
begin


    begin
    select nm_usuario
    into nm_usuario_w
    from HSJ_AUTENTICACAO_WIFI where nm_usuario = TO_CHAR(OBTER_DADOS_PF(:old.cd_pessoa_fisica,'CPF'));
   
    EXCEPTION WHEN NO_DATA_FOUND THEN
      nm_usuario_w := 'N'; 
    end;                                       
    
    if nm_usuario_w <> 'N' then
      delete from tasy.hsj_autenticacao_wifi where nm_usuario = nm_usuario_w;
    end if;    
    
    
  

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_visita_insert
before INSERT ON TASY.ATENDIMENTO_VISITA FOR EACH ROW
declare
nr_seq_controle_w	number(15,0);
ie_controla_w		varchar2(5);
ie_considera_estab_w	varchar2(5);
ie_controle_diferente_w	varchar2(5);
ie_possui_w		varchar2(5);
ie_possui_alta_w	varchar2(2);
ie_permite_alta_w     varchar(2);
nr_controle_unico_w		varchar(2);
ds_origem_w			varchar2(1800);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	Obter_Param_Usuario(44, 10, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_controla_w);
	Obter_Param_Usuario(8014, 59, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_considera_estab_w);
	Obter_Param_Usuario(8014, 97, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_controle_diferente_w);
	Obter_Param_Usuario(8014, 112, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_permite_alta_w);
	Obter_Param_Usuario(8014, 119, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_controle_unico_w);

	Select 	nvl(max('S'),'N')
	into 	ie_possui_alta_w
	from 	atendimento_paciente a
	where 	:new.nr_atendimento = a.nr_atendimento
	and 	a.dt_alta is not null;

	if 	(ie_possui_alta_w = 'S') and
		(ie_permite_alta_w = 'N')	then
		wheb_mensagem_pck.exibir_mensagem_abort(284133);
	end if;


	if	(ie_controla_w = 'S') and
		(:new.nr_seq_controle is null) then

			if (ie_considera_estab_w = 'S') then
				nr_seq_controle_w	:=	obter_nr_controle_estab(wheb_usuario_pck.get_cd_estabelecimento);

			elsif(nr_controle_unico_w = 'S') then
					select	CONTROLE_VISITA_UNICO_SEQ.nextval
					into	nr_seq_controle_w
					from	dual;
			else
				select	atendimento_visita_seq2.nextval
				into	nr_seq_controle_w
				from	dual;
			end if;

		:new.nr_seq_controle	:= nr_seq_controle_w;
	end if;

	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

	insert into log_mov(
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values(	sysdate,
		'Tasy',
		131713,
		'obter_perfil_ativo: '||obter_perfil_ativo||
		' - :new.nm_usuario: '||:new.nm_usuario||
		' - :new.nr_seq_controle: '||:new.nr_seq_controle||
		' - Stack: ' || ds_origem_w);

	select	controle_visita_seq.nextval
	into	:new.nr_controle_acesso
	from	dual;

	if (ie_controle_diferente_w = 'S') then

		nr_seq_controle_w	:= obter_novo_numero_controle(:new.nr_seq_controle);
		:new.nr_seq_controle	:= nr_seq_controle_w;

	end if;
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_visita_update
before update ON TASY.ATENDIMENTO_VISITA for each row
DECLARE
ie_possui_alta_w      varchar2(2);
ie_permite_alta_w     varchar2(2);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	Obter_Param_Usuario(8014, 112, obter_perfil_ativo, :new.nm_usuario, 0, ie_permite_alta_w);

	if 	(:old.dt_saida is not null) and
		(:new.dt_saida is null)     and
		(ie_permite_alta_w = 'N')   then

		Select 	nvl(max('S'),'N')
		into 	ie_possui_alta_w
		from 	atendimento_paciente a
		where 	:new.nr_atendimento = a.nr_atendimento
		and 	a.dt_alta is not null;

		if (ie_possui_alta_w = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort(284133);
		end if;

	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_VISITA_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ATENDIMENTO_VISITA FOR EACH ROW
DECLARE
ie_atend_visita_lib_w varchar2(1);
ie_visita_lib_w varchar2(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

    IF 	(:NEW.NR_SEQ_CONTROLE IS NOT NULL) AND
        ((:OLD.NR_SEQ_CONTROLE IS NULL) OR (:NEW.NR_SEQ_CONTROLE != :OLD.NR_SEQ_CONTROLE)) THEN
        VALIDA_CRACHA_INTEGRACAO(:NEW.NM_USUARIO, :NEW.NR_SEQ_CONTROLE);
    END IF;

    if 	((inserting) and (:new.nr_atendimento is not null) and (nvl(:new.ie_paciente,'N') = 'N')) or
        ((updating) and (:new.nr_atendimento is not null) and (nvl(:new.ie_paciente,'N') = 'N') and (:new.dt_saida is null)) then
            select	decode(count(1), 0, 'N', 'S')
        into	ie_atend_visita_lib_w
        from	atendimento_visita_lib
        where	nr_atendimento = :new.nr_atendimento;

        if (nvl(ie_atend_visita_lib_w,'N') = 'S') then
            SELECT 	decode(COUNT(*),0,'N','S')
            into	ie_visita_lib_w
            FROM 	atendimento_visita_lib
            WHERE 	nr_atendimento  = :new.nr_atendimento
            and 	((cd_pessoa_lib is not null and cd_pessoa_lib = :new.cd_pessoa_fisica) or (nm_pessoa_liberacao is not null and upper(nm_pessoa_liberacao) = upper(:new.nm_visitante)));

            if (nvl(ie_visita_lib_w,'S') = 'N') then
                Wheb_mensagem_pck.exibir_mensagem_abort(41594);
            end if;
        end if;
    end if;
end if;
END;
/


ALTER TABLE TASY.ATENDIMENTO_VISITA ADD (
  CONSTRAINT ATEVISI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_VISITA ADD (
  CONSTRAINT ATEVISI_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEVISI_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT ATEVISI_PESFISI_FK2 
 FOREIGN KEY (CD_FUNCIONARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEVISI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEVISI_VISITAN_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.VISITANTE (NR_SEQUENCIA),
  CONSTRAINT ATEVISI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEVISI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.ATENDIMENTO_VISITA TO NIVEL_1;

