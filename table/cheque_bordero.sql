ALTER TABLE TASY.CHEQUE_BORDERO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE_BORDERO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE_BORDERO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_BORDERO       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_CHEQUE        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEBORD_BORCHEQ_FK_I ON TASY.CHEQUE_BORDERO
(NR_SEQ_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEBORD_CHEQUES_FK_I ON TASY.CHEQUE_BORDERO
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEBORD_CHEQUES_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CHEBORD_PK ON TASY.CHEQUE_BORDERO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEBORD_PK
  MONITORING USAGE;


ALTER TABLE TASY.CHEQUE_BORDERO ADD (
  CONSTRAINT CHEBORD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE_BORDERO ADD (
  CONSTRAINT CHEBORD_BORCHEQ_FK 
 FOREIGN KEY (NR_SEQ_BORDERO) 
 REFERENCES TASY.BORDERO_CHEQUE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CHEBORD_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE));

GRANT SELECT ON TASY.CHEQUE_BORDERO TO NIVEL_1;

