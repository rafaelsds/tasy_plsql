ALTER TABLE TASY.TIPO_HIST_COB_REGRA_UTIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_HIST_COB_REGRA_UTIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_HIST_COB_REGRA_UTIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_HIST     NUMBER(10)               NOT NULL,
  CD_PERFIL            NUMBER(5),
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPHCRU_ESTABEL_FK_I ON TASY.TIPO_HIST_COB_REGRA_UTIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPHCRU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIPHCRU_PERFIL_FK_I ON TASY.TIPO_HIST_COB_REGRA_UTIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPHCRU_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIPHCRU_PK ON TASY.TIPO_HIST_COB_REGRA_UTIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPHCRU_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIPHCRU_TIPHICO_FK_I ON TASY.TIPO_HIST_COB_REGRA_UTIL
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_HIST_COB_REGRA_UTIL ADD (
  CONSTRAINT TIPHCRU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_HIST_COB_REGRA_UTIL ADD (
  CONSTRAINT TIPHCRU_TIPHICO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.TIPO_HIST_COB (NR_SEQUENCIA),
  CONSTRAINT TIPHCRU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TIPHCRU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TIPO_HIST_COB_REGRA_UTIL TO NIVEL_1;

