ALTER TABLE TASY.REGRA_LEGENDA_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LEGENDA_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LEGENDA_CLASSIF
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_COR               VARCHAR2(15 BYTE)        NOT NULL,
  DS_LEGENDA           VARCHAR2(25 BYTE),
  QT_IDADE_MIN         NUMBER(3),
  QT_IDADE_MAX         NUMBER(3),
  NR_SEQ_APRESENT      NUMBER(5),
  NR_SEQUENCIA         NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_COR_HTML          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGLEGCL_PK ON TASY.REGRA_LEGENDA_CLASSIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.regra_legenda_classif_insert
before insert ON TASY.REGRA_LEGENDA_CLASSIF for each row
begin
	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		if (:new.ds_cor is null and :new.ds_cor_html is not null) then
			:new.ds_cor := :new.ds_cor_html;
		end if;
	end if;
end;
/


ALTER TABLE TASY.REGRA_LEGENDA_CLASSIF ADD (
  CONSTRAINT REGLEGCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_LEGENDA_CLASSIF TO NIVEL_1;

