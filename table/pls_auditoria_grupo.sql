ALTER TABLE TASY.PLS_AUDITORIA_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AUDITORIA_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AUDITORIA_GRUPO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_GRUPO             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM             NUMBER(10),
  DT_LIBERACAO             DATE,
  NR_SEQ_AUDITORIA         NUMBER(10)           NOT NULL,
  IE_STATUS                VARCHAR2(1 BYTE)     NOT NULL,
  NM_USUARIO_EXEC          VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_SOLIC       NUMBER(10),
  IE_MANUAL                VARCHAR2(1 BYTE),
  DT_INICIO_AUDITORIA      DATE,
  NR_SEQ_ACAO_ANALISE_AUT  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAUGR_PK ON TASY.PLS_AUDITORIA_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAUGR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSAUGR_PLSACAA_FK_I ON TASY.PLS_AUDITORIA_GRUPO
(NR_SEQ_ACAO_ANALISE_AUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAUGR_PLSACAA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAUGR_PLSAUDI_FK_I ON TASY.PLS_AUDITORIA_GRUPO
(NR_SEQ_AUDITORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAUGR_PLSGRAU_FK_I ON TASY.PLS_AUDITORIA_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAUGR_PLSGRAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAUGR_PLSGRAU_FK2_I ON TASY.PLS_AUDITORIA_GRUPO
(NR_SEQ_GRUPO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSAUGR_PLSGRAU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSAUGR_USUARIO_FK_I ON TASY.PLS_AUDITORIA_GRUPO
(NM_USUARIO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_AUDITORIA_GRUPO ADD (
  CONSTRAINT PLSAUGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AUDITORIA_GRUPO ADD (
  CONSTRAINT PLSAUGR_PLSGRAU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA),
  CONSTRAINT PLSAUGR_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_EXEC) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT PLSAUGR_PLSAUDI_FK 
 FOREIGN KEY (NR_SEQ_AUDITORIA) 
 REFERENCES TASY.PLS_AUDITORIA (NR_SEQUENCIA),
  CONSTRAINT PLSAUGR_PLSGRAU_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_SOLIC) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA),
  CONSTRAINT PLSAUGR_PLSACAA_FK 
 FOREIGN KEY (NR_SEQ_ACAO_ANALISE_AUT) 
 REFERENCES TASY.PLS_ACAO_ANALISE_AUT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AUDITORIA_GRUPO TO NIVEL_1;

