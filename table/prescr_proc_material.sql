ALTER TABLE TASY.PRESCR_PROC_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_MATERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(10)               NOT NULL,
  NR_SEQ_MATERIAL      NUMBER(10)               NOT NULL,
  QT_VOLUME            NUMBER(5)                NOT NULL,
  QT_TEMPO             NUMBER(4,2)              NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  QT_MINUTO            NUMBER(2)                NOT NULL,
  DT_COLETA            DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_BARRAS            VARCHAR2(50 BYTE),
  NR_SEQ_GRUPO         NUMBER(10),
  NR_AMOSTRA           NUMBER(5),
  IE_STATUS            NUMBER(2),
  IE_AMOSTRA           VARCHAR2(1 BYTE),
  NR_SEQ_FRASCO        NUMBER(10),
  NR_SEQ_GRUPO_IMP     NUMBER(10),
  NR_SEQ_INT_PRESCR    NUMBER(10),
  IE_COLETA_EXT        VARCHAR2(1 BYTE),
  QT_TEMPO_GEST        NUMBER(5),
  DS_CONDICAO          VARCHAR2(255 BYTE),
  CD_AMOSTRA_VFR       VARCHAR2(20 BYTE),
  QT_PESO              NUMBER(5),
  DT_INTEGRACAO        DATE,
  QT_VOLUME_ENF        NUMBER(10,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          47M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRPRMAT_FRAEXLA_FK_I ON TASY.PRESCR_PROC_MATERIAL
(NR_SEQ_FRASCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRMAT_FRAEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRPRMAT_LABGRIM_FK_I ON TASY.PRESCR_PROC_MATERIAL
(NR_SEQ_GRUPO_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRMAT_LABGRIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRPRMAT_MATEXLA_FK_I ON TASY.PRESCR_PROC_MATERIAL
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRMAT_MATEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRPRMAT_PK ON TASY.PRESCR_PROC_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPRMAT_PRESMED_FK_I ON TASY.PRESCR_PROC_MATERIAL
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRPRMAT_UK ON TASY.PRESCR_PROC_MATERIAL
(CD_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRMAT_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PRESCR_PROC_MATERIAL_tp  after update ON TASY.PRESCR_PROC_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_VOLUME,1,4000),substr(:new.QT_VOLUME,1,4000),:new.nm_usuario,nr_seq_w,'QT_VOLUME',ie_log_w,ds_w,'PRESCR_PROC_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_proc_material_update
BEFORE UPDATE ON TASY.PRESCR_PROC_MATERIAL FOR EACH ROW
declare

nr_seq_evento_w		number(10,0);
ds_atributo_alt_w	varchar2(4000);
qt_volume_alt_w		varchar2(255);
qt_tempo_alt_w		varchar2(255);
qt_minuto_alt_w		varchar2(255);
dt_coleta_alt_w		varchar2(255);
ds_condicao_alt_w	varchar2(255);
qt_tempo_gest_alt_w	varchar2(255);
ds_minuto_alt_w		varchar2(255);
ds_mensagem_w		varchar2(4000);
ds_titulo_w			varchar2(100);

cursor c01 is
select	a.nr_seq_evento
from	regra_envio_sms a
where	a.ie_evento_disp = 'ALTAMO'
and	nvl(a.ie_situacao,'A') = 'A';

BEGIN

ds_atributo_alt_w := '';

gravar_log_lab(97, :new.qt_volume || ' - ' || :old.qt_volume, :new.nm_usuario, :new.nr_prescricao);

if(:new.qt_volume <> :old.qt_volume) and (nvl(:old.qt_volume,0) <> 0) then
	qt_volume_alt_w := wheb_mensagem_pck.get_texto(737000) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.qt_volume||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.qt_volume;
end if;

if(:new.qt_tempo <> :old.qt_tempo) and (nvl(:old.qt_tempo,0) <> 0) then
	qt_tempo_alt_w := wheb_mensagem_pck.get_texto(737001) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.qt_tempo||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.qt_tempo;
end if;

if(:new.qt_minuto <> :old.qt_minuto) and (nvl(:old.qt_minuto,0) <> 0) then
	qt_minuto_alt_w := wheb_mensagem_pck.get_texto(737002) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.qt_minuto||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.qt_minuto;
end if;

if(:new.dt_coleta <> :old.dt_coleta) then
	dt_coleta_alt_w := wheb_mensagem_pck.get_texto(737003) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.dt_coleta||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.dt_coleta;
end if;

if(:new.ds_condicao <> :old.ds_condicao) then
	ds_condicao_alt_w := wheb_mensagem_pck.get_texto(737004) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.ds_condicao||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.ds_condicao;
end if;

if(:new.qt_tempo_gest <> :old.qt_tempo_gest) and (nvl(:old.qt_tempo_gest,0) <> 0) then
	qt_tempo_gest_alt_w := wheb_mensagem_pck.get_texto(737005) || ' ' || wheb_mensagem_pck.get_texto(737007)||' '||:old.qt_tempo_gest||' '||wheb_mensagem_pck.get_texto(737008)||' '||:new.qt_tempo_gest;
end if;

if (qt_volume_alt_w is not null
		or qt_tempo_alt_w is not null
		or qt_minuto_alt_w is not null
		or dt_coleta_alt_w is not null
		or ds_condicao_alt_w is not null
		or qt_tempo_gest_alt_w is not null) then

	open c01;
		loop
		fetch c01 into
			nr_seq_evento_w;
		exit when c01%notfound;
		begin
			ds_atributo_alt_w := '';

			select	ds_titulo,
					ds_mensagem
			into	ds_titulo_w,
					ds_mensagem_w
			from	ev_evento
			where	nr_sequencia = nr_seq_evento_w;

			if ((ds_mensagem_w like '%@volume%' or ds_titulo_w like '%@volume%')
					and qt_volume_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @volume[' || qt_volume_alt_w || ']@';
			end if;

			if ((ds_mensagem_w like '%@tempo%' or ds_titulo_w like '%@tempo%')
					and qt_tempo_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @tempo[' || qt_tempo_alt_w || ']@';
			end if;

			if ((ds_mensagem_w like '%@minuto%' or ds_titulo_w like '%@minuto%')
					and qt_minuto_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @minuto[' || qt_minuto_alt_w || ']@';
			end if;

			if ((ds_mensagem_w like '%@data_coleta_alt%' or ds_titulo_w like '%@data_coleta_alt%')
					and dt_coleta_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @data_coleta_alt[' || dt_coleta_alt_w || ']@';
			end if;

			if ((ds_mensagem_w like '%@ds_condicao%' or ds_titulo_w like '%@ds_condicao%')
					and ds_condicao_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @ds_condicao[' || ds_condicao_alt_w || ']@';
			end if;

			if ((ds_mensagem_w like '%@qt_tempo_gest%' or ds_titulo_w like '%@qt_tempo_gest%')
					and qt_tempo_gest_alt_w is not null) then
				ds_atributo_alt_w := ds_atributo_alt_w || ' @qt_tempo_gest[' || qt_tempo_gest_alt_w || ']@';
			end if;

			if (ds_atributo_alt_w is not null) then
				gravar_log_lab(99, ds_atributo_alt_w, :new.nm_usuario, :new.nr_prescricao);

				gerar_evento_alt_dado_amostra(nr_seq_evento_w, :new.nm_usuario, :new.nr_prescricao, :new.nr_seq_material, :new.dt_coleta, ds_atributo_alt_w, 'S');
			end if;





		end;
		end loop;
	close c01;
end if;

END;
/


ALTER TABLE TASY.PRESCR_PROC_MATERIAL ADD (
  CONSTRAINT PRPRMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          17M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRPRMAT_UK
 UNIQUE (CD_BARRAS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_MATERIAL ADD (
  CONSTRAINT PRPRMAT_MATEXLA_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT PRPRMAT_FRAEXLA_FK 
 FOREIGN KEY (NR_SEQ_FRASCO) 
 REFERENCES TASY.FRASCO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT PRPRMAT_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PRPRMAT_LABGRIM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_IMP) 
 REFERENCES TASY.LAB_GRUPO_IMPRESSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_PROC_MATERIAL TO NIVEL_1;

