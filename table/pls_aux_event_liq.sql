ALTER TABLE TASY.PLS_AUX_EVENT_LIQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AUX_EVENT_LIQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AUX_EVENT_LIQ
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_REG_AUXILIAR         NUMBER(10)        NOT NULL,
  NR_SEQ_PRESTADOR            NUMBER(10),
  NR_TITULO                   NUMBER(10),
  NR_SEQ_LOTE_PGTO            NUMBER(10),
  DT_VENCIMENTO               DATE,
  CD_CONTA_CONTABIL           VARCHAR2(20 BYTE),
  NR_DOCUMENTO                VARCHAR2(255 BYTE),
  IE_TIPO_DOCUMENTO           VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO             NUMBER(10),
  IE_TIPO_CONTRATACAO         VARCHAR2(15 BYTE),
  IE_TIPO_SEGMENTACAO         VARCHAR2(15 BYTE),
  IE_TIPO_SEGURADO            VARCHAR2(15 BYTE),
  IE_TIPO_RELACAO             VARCHAR2(15 BYTE),
  DT_OCORRENCIA               DATE,
  DT_AVISO                    DATE,
  DT_EMISSAO                  DATE,
  NR_SEQ_SEGURADO             NUMBER(10),
  NR_SEQ_PAGADOR              NUMBER(10),
  VL_EVENTO                   NUMBER(15,2),
  VL_SALDO                    NUMBER(15,2),
  DT_CONTABIL                 DATE,
  IE_CLASSIF_PEL              VARCHAR2(15 BYTE),
  IE_CLASSIF_DIOPS            VARCHAR2(15 BYTE),
  NR_SEQ_CONTA                NUMBER(10),
  NR_CONTRATO                 NUMBER(10),
  IE_TIPO_REPASSE             VARCHAR2(15 BYTE),
  NR_SEQ_CONGENERE            NUMBER(10),
  IE_TIPO_EVENTO              VARCHAR2(10 BYTE),
  NR_REGISTRO_PRODUTO         VARCHAR2(20 BYTE),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  IE_PRECO                    VARCHAR2(2 BYTE),
  DT_INICIO_COMPARTILHAMENTO  DATE,
  IE_TIPO_COMPARTILHAMENTO    NUMBER(2),
  DT_FIM_COMPARTILHAMENTO     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSAUXEL_CONCONT_FK_I ON TASY.PLS_AUX_EVENT_LIQ
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAUXEL_CTBLIVAUX_FK_I ON TASY.PLS_AUX_EVENT_LIQ
(NR_SEQ_REG_AUXILIAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSAUXEL_PK ON TASY.PLS_AUX_EVENT_LIQ
(NR_SEQ_REG_AUXILIAR, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_AUX_EVENT_LIQ ADD (
  CONSTRAINT PLSAUXEL_PK
 PRIMARY KEY
 (NR_SEQ_REG_AUXILIAR, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AUX_EVENT_LIQ ADD (
  CONSTRAINT PLSAUXEL_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSAUXEL_CTBLIVAUX_FK 
 FOREIGN KEY (NR_SEQ_REG_AUXILIAR) 
 REFERENCES TASY.CTB_LIVRO_AUXILIAR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AUX_EVENT_LIQ TO NIVEL_1;

