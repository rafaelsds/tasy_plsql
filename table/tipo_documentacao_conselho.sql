ALTER TABLE TASY.TIPO_DOCUMENTACAO_CONSELHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_DOCUMENTACAO_CONSELHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_DOCUMENTACAO_CONSELHO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_DOC      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONSELHO      NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPODCO_CONPROF_FK_I ON TASY.TIPO_DOCUMENTACAO_CONSELHO
(NR_SEQ_CONSELHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPODCO_CONPROF_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIPODCO_PK ON TASY.TIPO_DOCUMENTACAO_CONSELHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPODCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIPODCO_TIPODOC_FK_I ON TASY.TIPO_DOCUMENTACAO_CONSELHO
(NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_DOCUMENTACAO_CONSELHO ADD (
  CONSTRAINT TIPODCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_DOCUMENTACAO_CONSELHO ADD (
  CONSTRAINT TIPODCO_CONPROF_FK 
 FOREIGN KEY (NR_SEQ_CONSELHO) 
 REFERENCES TASY.CONSELHO_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT TIPODCO_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_DOCUMENTACAO_CONSELHO TO NIVEL_1;

