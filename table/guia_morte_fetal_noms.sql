ALTER TABLE TASY.GUIA_MORTE_FETAL_NOMS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GUIA_MORTE_FETAL_NOMS CASCADE CONSTRAINTS;

CREATE TABLE TASY.GUIA_MORTE_FETAL_NOMS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_OBITO                    DATE,
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  NR_ATENDIMENTO_MAE          NUMBER(10),
  IE_SEXO_PROD                VARCHAR2(15 BYTE),
  NR_FOLIO                    VARCHAR2(40 BYTE),
  QT_IDADE_PROD               NUMBER(5,2),
  QT_PESO_PROD                NUMBER(4),
  QT_FETOS                    NUMBER(1),
  IE_PRE_NATAL                VARCHAR2(1 BYTE),
  QT_CONSULTAS                NUMBER(3),
  IE_RISCO_GRAVIDEZ           VARCHAR2(1 BYTE),
  IE_TIPO_NASCIMENTO          VARCHAR2(3 BYTE),
  QT_TEXTURA_PELE             NUMBER(10),
  IE_EMBARAZO_ESP_ASSIS       VARCHAR2(1 BYTE),
  NR_DIA_NASCIMENTO           VARCHAR2(2 BYTE),
  NR_MES_NASCIMENTO           VARCHAR2(2 BYTE),
  NR_ANO_NASCIMENTO           VARCHAR2(4 BYTE),
  NR_HORA_NASCIMENTO          VARCHAR2(2 BYTE),
  NR_MIN_NASCIMENTO           VARCHAR2(2 BYTE),
  CD_TIPO_VIALIDAD_ESTAB      VARCHAR2(50 BYTE),
  DS_ENDERECO_PROD            VARCHAR2(255 BYTE),
  DS_BAIRRO_PROD              VARCHAR2(255 BYTE),
  CD_LOCALIDADE_PROD          VARCHAR2(50 BYTE),
  CD_MUNICIPIO_PROD           VARCHAR2(50 BYTE),
  CD_ESTADO_PROD              VARCHAR2(50 BYTE),
  IE_TIPO_INST_SAUDE_PROD     VARCHAR2(5 BYTE),
  NR_INTERIOR_ESTAB           VARCHAR2(255 BYTE),
  NR_EXTERIOR_ESTAB           VARCHAR2(255 BYTE),
  DS_ESTAB_SAUDE              VARCHAR2(255 BYTE),
  CD_PRO_CLUES                VARCHAR2(50 BYTE),
  CD_ATEND_PARTO_MF           NUMBER(5),
  QT_ABORTOS_ESP              NUMBER(2),
  QT_ABORTOS_PROV             NUMBER(2),
  QT_ABORTOS_TERAP            NUMBER(2),
  IE_TIPO_PARTO_PROD          NUMBER(5),
  IE_PARTO_NORMAL             VARCHAR2(1 BYTE),
  IE_PARTO_CESARIA            VARCHAR2(1 BYTE),
  IE_PARTO_FORCEPS            VARCHAR2(1 BYTE),
  DS_ESPEC_PARTO              VARCHAR2(255 BYTE),
  IE_AGRESSAO                 VARCHAR2(2 BYTE),
  IE_PARENTESCO_AGRESSOR      VARCHAR2(2 BYTE),
  CD_CIE10_CAUSA_BASICA       VARCHAR2(10 BYTE),
  CD_CIE10_CAUSA_A            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_A            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_A        VARCHAR2(1 BYTE),
  CD_CIE10_CAUSA_B            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_B            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_B        VARCHAR2(1 BYTE),
  CD_CIE10_CAUSA_C            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_C            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_C        VARCHAR2(1 BYTE),
  CD_CIE10_CAUSA_D            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_D            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_D        VARCHAR2(1 BYTE),
  CD_CIE10_CAUSA_E            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_E            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_E        VARCHAR2(1 BYTE),
  CD_CIE10_CAUSA_F            VARCHAR2(10 BYTE),
  DS_CIE10_CAUSA_F            VARCHAR2(4000 BYTE),
  IE_TIPO_RESP_CAUSA_F        VARCHAR2(1 BYTE),
  NM_USUARIO_REGISTRO         VARCHAR2(10 BYTE),
  DT_REGISTRO                 DATE,
  NM_USUARIO_MODIFICACAO      VARCHAR2(10 BYTE),
  DT_MODIFICACAO              DATE,
  IE_TIPO_CERTIF_OBT          NUMBER(1),
  NR_SEQ_PERSON_NAME_MAE      NUMBER(10),
  CD_ESTADO_CAPT_CERT         VARCHAR2(2 BYTE),
  CD_CURP_MAE                 VARCHAR2(18 BYTE),
  CD_NACIONALIDADE_MAE        VARCHAR2(1 BYTE),
  NR_SEQ_LINGUA_INDIG_MAE     VARCHAR2(1 BYTE),
  QT_IDADE_MAE                VARCHAR2(5 BYTE),
  IE_ESTADO_CIVIL_MAE         VARCHAR2(2 BYTE),
  DS_ENDERECO_MAE             VARCHAR2(4000 BYTE),
  DS_BAIRRO_MAE               VARCHAR2(255 BYTE),
  CD_LOCALIDADE_MAE           VARCHAR2(255 BYTE),
  CD_MUNICIPIO_MAE            VARCHAR2(255 BYTE),
  CD_ESTADO_MAE               VARCHAR2(255 BYTE),
  NR_INTERIOR_MAE             VARCHAR2(255 BYTE),
  NR_EXTERIOR_MAE             VARCHAR2(255 BYTE),
  CD_CARGO_MAE                VARCHAR2(255 BYTE),
  IE_TRABALHA_MAE             VARCHAR2(2 BYTE),
  QT_FILHOS_VIVOS             VARCHAR2(2 BYTE),
  QT_NATIMORTOS               VARCHAR2(2 BYTE),
  QT_SOBREVIVENTES            VARCHAR2(2 BYTE),
  IE_GRAU_INSTRUCAO           VARCHAR2(2 BYTE),
  CD_DER_FETAL                VARCHAR2(2 BYTE),
  NR_SPSS_MADRE               VARCHAR2(15 BYTE),
  IE_SOBREVIVE_MAE            VARCHAR2(1 BYTE),
  NR_FOLIO_DEF_MAE            VARCHAR2(20 BYTE),
  NR_SEQ_PERSON_NAME_INF      NUMBER(10),
  CD_GRAU_PARENTESCO_INF      VARCHAR2(2 BYTE),
  CD_CERT_PARTO_MF_INF        VARCHAR2(5 BYTE),
  DS_CODIGO_PROF_CERTIF       VARCHAR2(15 BYTE),
  NR_SEQ_PERSON_NAME_CERT     NUMBER(10),
  DS_ENDERECO_CERTIF          VARCHAR2(4000 BYTE),
  DS_BAIRRO_CERTIF            VARCHAR2(255 BYTE),
  CD_LOCALIDADE_CERTIF        VARCHAR2(255 BYTE),
  CD_MUNICIPIO_CERTIF         VARCHAR2(255 BYTE),
  CD_ESTADO_CERTIF            VARCHAR2(255 BYTE),
  NR_INTERIOR_CERT            VARCHAR2(255 BYTE),
  NR_EXTERIOR_CERT            VARCHAR2(255 BYTE),
  NR_TELEFONE_CERTIF          VARCHAR2(15 BYTE),
  IE_ASSINOU_CERT_OBITO       VARCHAR2(1 BYTE),
  NR_DIA_CERTIFICACAO         VARCHAR2(2 BYTE),
  NR_MES_CERTIFICACAO         VARCHAR2(2 BYTE),
  NR_ANO_CERTIFICACAO         VARCHAR2(4 BYTE),
  IE_NECROPCIA                VARCHAR2(1 BYTE),
  CD_TIPO_ASENTAMIENTO_ESTAB  VARCHAR2(50 BYTE),
  CD_ASENTAMIENTO_ESTAB       VARCHAR2(50 BYTE),
  CD_CODIGO_POSTAL_ESTAB      VARCHAR2(50 BYTE),
  CD_TIPO_ASENTAMIENTO_MAE    VARCHAR2(50 BYTE),
  CD_TIPO_VIALIDAD_MAE        VARCHAR2(50 BYTE),
  CD_ASENTAMIENTO_MAE         VARCHAR2(50 BYTE),
  CD_CODIGO_POSTAL_MAE        VARCHAR2(50 BYTE),
  CD_TIPO_VIALIDAD_CERT       VARCHAR2(50 BYTE),
  CD_TIPO_ASENTAMIENTO_CERT   VARCHAR2(50 BYTE),
  CD_ASENTAMIENTO_CERT        VARCHAR2(50 BYTE),
  CD_CODIGO_POSTAL_CERT       VARCHAR2(50 BYTE),
  DS_NACIONALIDADE_MAE        VARCHAR2(255 BYTE),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  NR_SEQ_INDIC_CESAREA        NUMBER(10),
  IE_TIPO_PARTO               VARCHAR2(2 BYTE),
  DS_OUTRO_TIPO_PARTO         VARCHAR2(255 BYTE),
  CD_ANOMALIA1                VARCHAR2(10 BYTE),
  DS_ANOMALIA1                VARCHAR2(255 BYTE),
  CD_ANOMALIA2                VARCHAR2(10 BYTE),
  DS_ANOMALIA2                VARCHAR2(255 BYTE),
  NR_SEQ_CATALOGO_ESTAB       NUMBER(10),
  NR_SEQ_CATALOGO_PROD        NUMBER(10),
  NR_SEQ_CATALOGO_CAPT        NUMBER(10),
  NR_SEQ_CATALOGO_MAE         NUMBER(10),
  NR_SEQ_CATALOGO_CERT        NUMBER(10),
  DS_ESPC_CERT                VARCHAR2(50 BYTE),
  IE_RESIDE_EXTERIOR          VARCHAR2(1 BYTE),
  IE_IGNORA_CP_MAE            VARCHAR2(1 BYTE),
  CD_PAIS                     VARCHAR2(10 BYTE),
  QT_GESTACOES                NUMBER(10),
  NR_TELEFONE_MAE             VARCHAR2(40 BYTE),
  NR_DDD_FONE_MAE             VARCHAR2(3 BYTE),
  NR_DDI_FONE_MAE             VARCHAR2(3 BYTE),
  CD_JURISDICAO_REG           VARCHAR2(255 BYTE),
  CD_LOCALIDADE_REG           VARCHAR2(255 BYTE),
  NR_FOLIO_CONTROL            VARCHAR2(50 BYTE),
  IE_IGNORA_CP_CERT           VARCHAR2(1 BYTE),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIB              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GMOFENOM_ATEPACI_FK_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ATEPACI_FK1_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_ATENDIMENTO_MAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_CIDDOEN_FK_I ON TASY.GUIA_MORTE_FETAL_NOMS
(CD_CIE10_CAUSA_BASICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ENDCATA_FK_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQ_CATALOGO_CAPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ENDCATA_FK2_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQ_CATALOGO_CERT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ENDCATA_FK3_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQ_CATALOGO_ESTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ENDCATA_FK4_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQ_CATALOGO_MAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GMOFENOM_ENDCATA_FK5_I ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQ_CATALOGO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GMOFENOM_PK ON TASY.GUIA_MORTE_FETAL_NOMS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GUIA_MORTE_FETAL_NOMS_UPD
before update ON TASY.GUIA_MORTE_FETAL_NOMS for each row
declare

begin
:new.DT_MODIFICACAO := :new.dt_atualizacao;

end;
/


ALTER TABLE TASY.GUIA_MORTE_FETAL_NOMS ADD (
  CONSTRAINT GMOFENOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GUIA_MORTE_FETAL_NOMS ADD (
  CONSTRAINT GMOFENOM_CIDDOEN_FK 
 FOREIGN KEY (CD_CIE10_CAUSA_BASICA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT GMOFENOM_ENDCATA_FK 
 FOREIGN KEY (NR_SEQ_CATALOGO_CAPT) 
 REFERENCES TASY.END_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT GMOFENOM_ENDCATA_FK2 
 FOREIGN KEY (NR_SEQ_CATALOGO_CERT) 
 REFERENCES TASY.END_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT GMOFENOM_ENDCATA_FK3 
 FOREIGN KEY (NR_SEQ_CATALOGO_ESTAB) 
 REFERENCES TASY.END_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT GMOFENOM_ENDCATA_FK4 
 FOREIGN KEY (NR_SEQ_CATALOGO_MAE) 
 REFERENCES TASY.END_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT GMOFENOM_ENDCATA_FK5 
 FOREIGN KEY (NR_SEQ_CATALOGO_PROD) 
 REFERENCES TASY.END_CATALOGO (NR_SEQUENCIA),
  CONSTRAINT GMOFENOM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT GMOFENOM_ATEPACI_FK1 
 FOREIGN KEY (NR_ATENDIMENTO_MAE) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.GUIA_MORTE_FETAL_NOMS TO NIVEL_1;

