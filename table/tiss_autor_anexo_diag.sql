ALTER TABLE TASY.TISS_AUTOR_ANEXO_DIAG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_AUTOR_ANEXO_DIAG CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_AUTOR_ANEXO_DIAG
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_USUARIO_LIB         VARCHAR2(15 BYTE),
  DT_LIBERACAO           DATE,
  NR_SEQ_ANEXO_GUIA      NUMBER(10),
  DT_DIAGNOSTICO         DATE,
  CD_CID                 VARCHAR2(10 BYTE),
  CD_CID_2               VARCHAR2(10 BYTE),
  DS_PLANO_TERAPEUTICO   VARCHAR2(1000 BYTE),
  CD_ESTADIAMENTO_TUMOR  VARCHAR2(1 BYTE),
  CD_TIPO_QUIMIO         VARCHAR2(1 BYTE),
  CD_FINALIDADE_TRAT     VARCHAR2(3 BYTE),
  CD_ECOG                VARCHAR2(3 BYTE),
  DS_HISTOPATOLOGICO     VARCHAR2(1000 BYTE),
  DS_INFO_RELEVANTE      VARCHAR2(1000 BYTE),
  DS_OBS_JUTIFICATIVA    VARCHAR2(1000 BYTE),
  DS_JUSTIFICATIVA_TEC   VARCHAR2(1000 BYTE),
  DS_ESPECIFIC_MAT       VARCHAR2(500 BYTE),
  NR_SEQUENCIA_AUTOR     NUMBER(10)             NOT NULL,
  CD_DIAGNOSTICO_IMAGEM  VARCHAR2(2 BYTE),
  CD_METASTASE           VARCHAR2(2 BYTE),
  CD_NODULO              VARCHAR2(2 BYTE),
  CD_TUMOR               VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSANDI_I1 ON TASY.TISS_AUTOR_ANEXO_DIAG
(NR_SEQ_ANEXO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSANDI_PK ON TASY.TISS_AUTOR_ANEXO_DIAG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TISS_AUTOR_ANEXO_DIAG ADD (
  CONSTRAINT TISSANDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TISS_AUTOR_ANEXO_DIAG TO NIVEL_1;

