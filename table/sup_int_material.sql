ALTER TABLE TASY.SUP_INT_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_MATERIAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  IE_FORMA_INTEGRACAO        VARCHAR2(15 BYTE)  NOT NULL,
  DT_LIBERACAO               DATE,
  DT_LEITURA                 DATE,
  DT_CONFIRMA_INTEGRACAO     DATE,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  DS_MATERIAL                VARCHAR2(255 BYTE) NOT NULL,
  DS_REDUZIDA                VARCHAR2(255 BYTE) NOT NULL,
  CD_MATERIAL_GENERICO       NUMBER(6),
  CD_MATERIAL_ESTOQUE        NUMBER(6)          NOT NULL,
  CD_CLASSE_MATERIAL         NUMBER(5),
  CD_MATERIAL_CONTA          NUMBER(6),
  CD_UNIDADE_MEDIDA_COMPRA   VARCHAR2(30 BYTE)  NOT NULL,
  CD_UNIDADE_MEDIDA_ESTOQUE  VARCHAR2(30 BYTE)  NOT NULL,
  CD_UNIDADE_MEDIDA_CONSUMO  VARCHAR2(30 BYTE)  NOT NULL,
  CD_UNIDADE_MEDIDA_SOLIC    VARCHAR2(30 BYTE),
  QT_CONV_COMPRA_ESTOQUE     NUMBER(13,4)       NOT NULL,
  QT_CONV_ESTOQUE_CONSUMO    NUMBER(13,4)       NOT NULL,
  QT_MINIMO_MULTIPLO_SOLIC   NUMBER(13,4)       NOT NULL,
  IE_PRESCRICAO              VARCHAR2(1 BYTE),
  IE_PRECO_COMPRA            VARCHAR2(1 BYTE),
  IE_INF_ULTIMA_COMPRA       VARCHAR2(1 BYTE)   NOT NULL,
  IE_CONSIGNADO              VARCHAR2(1 BYTE),
  IE_BAIXA_ESTOQ_PAC         VARCHAR2(1 BYTE)   NOT NULL,
  IE_TIPO_MATERIAL           VARCHAR2(3 BYTE)   NOT NULL,
  IE_PADRONIZADO             VARCHAR2(1 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  IE_OBRIG_VIA_APLICACAO     VARCHAR2(1 BYTE),
  IE_RECEITA                 VARCHAR2(1 BYTE)   NOT NULL,
  IE_COBRA_PACIENTE          VARCHAR2(1 BYTE)   NOT NULL,
  IE_DISPONIVEL_MERCADO      VARCHAR2(1 BYTE),
  IE_MATERIAL_ESTOQUE        VARCHAR2(1 BYTE)   NOT NULL,
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  QT_DIA_ESTOQUE_MINIMO      NUMBER(2),
  QT_DIAS_VALIDADE           NUMBER(5),
  QT_DIA_INTERV_RESSUP       NUMBER(3),
  QT_DIA_RESSUP_FORN         NUMBER(3),
  NR_SEQ_LOCALIZACAO         NUMBER(10),
  NR_SEQ_FAMILIA             NUMBER(10),
  CD_FABRICANTE              VARCHAR2(80 BYTE),
  CD_SISTEMA_ANT             VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO         NUMBER(4),
  IE_PACTUADO                VARCHAR2(1 BYTE),
  IE_CATALOGO                VARCHAR2(1 BYTE),
  NR_DOC_EXTERNO             VARCHAR2(80 BYTE),
  IE_CURVA_XYZ               VARCHAR2(1 BYTE),
  CD_NCM                     VARCHAR2(20 BYTE),
  DS_ESPEC_TECNICA           VARCHAR2(4000 BYTE),
  NR_SEQ_TAB_ORIGEM          NUMBER(10),
  NR_SEQ_ITEM_TAB_ORIGEM     NUMBER(10),
  CD_SISTEMA_EXTERNO         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUINMAT_I1 ON TASY.SUP_INT_MATERIAL
(NR_SEQ_TAB_ORIGEM, NR_SEQ_ITEM_TAB_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUINMAT_PK ON TASY.SUP_INT_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINMAT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_material_insert
before insert ON TASY.SUP_INT_MATERIAL for each row
declare
nr_sequencia_w	number(10);

begin

if	(:new.nr_sequencia = 0) then

	select	sup_int_material_seq.nextval
	into	nr_sequencia_w
	from	dual;

	:new.nr_sequencia	:= nr_sequencia_w;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sup_int_material_atual
before update or insert ON TASY.SUP_INT_MATERIAL for each row
declare
qt_existe_erros_w		number(10);
cd_material_w		number(6);
qt_existe_w		number(10) := 0;
cd_grupo_material_w	number(3);

begin
if	(:new.cd_classe_material is not null) then
	begin
	select	nvl(max(a.cd_grupo_material),0)
	into	cd_grupo_material_w
	from	grupo_material a,
		subgrupo_material b,
		classe_material c
	where	a.cd_grupo_material = b.cd_grupo_material
	and	b.cd_subgrupo_material = c.cd_subgrupo_material
	and	c.cd_classe_material = :new.cd_classe_material;

	if	(cd_grupo_material_w > 0) then
		select	count(*)
		into	qt_existe_w
		from	sup_parametro_integracao a,
			sup_int_regra_material b
		where	a.nr_sequencia = b.nr_seq_integracao
		and	a.ie_evento = 'CM'
		and	a.ie_forma = 'R'
		and	a.ie_situacao = 'A'
		and	b.ie_situacao = 'A'
		and	b.cd_grupo_material = cd_grupo_material_w;
	end if;
	end;
elsif	(updating) then
	begin
	select	nvl(max(cd_material),0)
	into	cd_material_w
	from	material
	where	substr(obter_dados_material_estab(cd_material, :new.cd_estabelecimento,'CSA'),1,20) = :new.cd_material;

	if	(cd_material_w > 0) then
		qt_existe_w	:=	1;
	end if;

	cd_material_w	:=	null;
	end;
end if;

if	(updating) and
	(nvl(:old.cd_sistema_ant,'X') = 'X') and
	(nvl(:new.cd_sistema_ant,'X') <> 'X') and
	(:new.ie_forma_integracao = 'E') then
	begin
	update	material
	set	cd_sistema_ant = :new.cd_sistema_ant,
		nm_usuario = 'INTEGR_TASY',
		dt_atualizacao = sysdate
	where	cd_material = :new.cd_material
	and	nvl(cd_sistema_ant,'X') = 'X';
	end;
elsif	(qt_existe_w > 0) and
	(:new.dt_liberacao is not null) and
	(:new.dt_confirma_integracao is null) and
	(:new.ie_forma_integracao = 'R') then

	:new.dt_leitura	:= sysdate;

	consiste_sup_int_material(
			:new.nr_sequencia,
			:new.cd_material_generico,
			:new.cd_material_estoque,
			:new.cd_material_conta,
			:new.cd_classe_material,
			:new.cd_unidade_medida_compra,
			:new.cd_unidade_medida_estoque,
			:new.cd_unidade_medida_consumo,
			:new.cd_unidade_medida_solic,
			:new.nr_seq_localizacao,
			:new.nr_seq_familia,
			:new.ie_via_aplicacao,
			:new.ie_tipo_material,
			:new.cd_estabelecimento);

	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_material_consist
	where	nr_sequencia = :new.nr_sequencia;

	if	(qt_existe_erros_w = 0) then
		begin
		gerar_material_integracao(
			:new.cd_material,
			'INTEGR_TASY',
			:new.cd_estabelecimento,
			:new.cd_classe_material,
			:new.cd_material_conta,
			:new.cd_material_estoque,
			:new.cd_material_generico,
			:new.cd_sistema_ant,
			:new.cd_unidade_medida_compra,
			:new.cd_unidade_medida_consumo,
			:new.cd_unidade_medida_estoque,
			:new.cd_unidade_medida_solic,
			:new.ds_material,
			:new.ds_reduzida,
			:new.ie_baixa_estoq_pac,
			:new.ie_cobra_paciente,
			:new.ie_consignado,
			:new.ie_disponivel_mercado,
			:new.ie_inf_ultima_compra,
			:new.ie_prescricao,
			:new.ie_preco_compra,
			:new.ie_tipo_material,
			:new.ie_padronizado,
			:new.ie_situacao,
			:new.ie_obrig_via_aplicacao,
			:new.ie_receita,
			:new.ie_material_estoque,
			:new.ie_via_aplicacao,
			:new.qt_conv_compra_estoque,
			:new.qt_conv_estoque_consumo,
			:new.qt_minimo_multiplo_solic,
			:new.qt_dia_estoque_minimo,
			:new.qt_dias_validade,
			:new.qt_dia_interv_ressup,
			:new.qt_dia_ressup_forn,
			:new.nr_seq_localizacao,
			:new.nr_seq_familia,
			:new.cd_fabricante,
			:new.nr_doc_externo,
			:new.ie_catalogo,
			:new.ie_curva_xyz,
			:new.cd_ncm,
			cd_material_w);
		exception
		when others then
			begin
			cd_material_w	:=	null;
			grava_sup_int_material_consist(:new.nr_sequencia,substr(sqlerrm,1,255));
			end;
		end;

		if	(cd_material_w is not null) then
			begin
			:new.dt_confirma_integracao	:= sysdate;
			:new.cd_sistema_ant	:= cd_material_w;
			end;
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.SUP_INT_MATERIAL ADD (
  CONSTRAINT SUINMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_MATERIAL TO NIVEL_1;

