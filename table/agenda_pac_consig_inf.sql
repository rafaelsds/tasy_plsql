ALTER TABLE TASY.AGENDA_PAC_CONSIG_INF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_CONSIG_INF CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_CONSIG_INF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AGE_CONSIG    NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  IE_DISPENSADO        VARCHAR2(1 BYTE),
  QT_MATERIAL          NUMBER(18,6),
  IE_TIPO_INFORMACAO   VARCHAR2(15 BYTE),
  DS_CME_PREPARADO     VARCHAR2(255 BYTE),
  NR_SEQ_ANTERIOR      NUMBER(10),
  QT_DEVOLVIDA         NUMBER(15,3),
  QT_DISPENSADA        NUMBER(15,3),
  QT_COBRADA           NUMBER(15,3),
  IE_RETIRADA          VARCHAR2(1 BYTE),
  IE_TIPO_PESSOA       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGPACOI_AGPACON_FK_I ON TASY.AGENDA_PAC_CONSIG_INF
(NR_SEQ_AGE_CONSIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACOI_AGPACON_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGPACOI_PK ON TASY.AGENDA_PAC_CONSIG_INF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPACOI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_pac_consig_inf_atual
before insert or update ON TASY.AGENDA_PAC_CONSIG_INF for each row
declare
ds_alteracao_w		varchar2(4000);
ds_inclusao_w		varchar2(255);
cme_preparado_w		varchar2(255);
nr_seq_agenda_w		Number(10);
solicitacao_medica_w	varchar2(255);

begin

select max(nr_seq_agenda),
	max(ds_material)
into   nr_seq_agenda_w,
	solicitacao_medica_w
from   agenda_pac_consignado
where  nr_sequencia = :new.nr_seq_age_consig;


if	(updating) then
	if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'C') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
							'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
							';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'AC',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:old.ie_tipo_informacao,'X') = 'M') then

		if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'M') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
								'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
								';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'ACM',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

		if	 nvl(:old.IE_DISPENSADO,'XPTO') <> nvl(:new.IE_DISPENSADO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'M') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791570,
								'IE_DISPENSADO_OLD='||:old.IE_DISPENSADO ||
								';IE_DISPENSADO_NEW='||:new.IE_DISPENSADO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'ACM',ds_alteracao_w,:new.nm_usuario);
			end if;

		end if;

		if	 nvl(:old.ds_cme_preparado,'XPTO') <> nvl(:new.ds_cme_preparado,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'M') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791571,
								'DS_CME_PREPARADO_OLD='||:old.ds_cme_preparado ||
								';DS_CME_PREPARADO_NEW='||:new.ds_cme_preparado ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'ACM',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

	end if;


	if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'A') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
								'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
								';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AAG',ds_alteracao_w,:new.nm_usuario);
			end if;
	end if;


	if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'E') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
								'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
								';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AE',ds_alteracao_w,:new.nm_usuario);
			end if;
	end if;


	if (nvl(:old.ie_tipo_informacao,'X') = 'F') then

		if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'F') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
								'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
								';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AF',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

		if	 nvl(:old.QT_MATERIAL,'-1') <> nvl(:new.QT_MATERIAL,'-1') and
			(nvl(:old.ie_tipo_informacao,'X') = 'F') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791572,
								'QT_MATERIAL_OLD='||:old.QT_MATERIAL ||
								';QT_MATERIAL_NEW='||:new.QT_MATERIAL ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AF',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

		if	 nvl(:old.IE_DISPENSADO,'XPTO') <> nvl(:new.IE_DISPENSADO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'F') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791575,
								'IE_DISPENSADO_OLD='||:old.IE_DISPENSADO ||
								';IE_DISPENSADO_OLD_NEW='||:new.IE_DISPENSADO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AF',ds_alteracao_w,:new.nm_usuario);
			end if;

		end if;
	end if;

	if (nvl(:old.ie_tipo_informacao,'X') = 'R') then

		if	 nvl(:old.DS_OBSERVACAO,'XPTO') <> nvl(:new.DS_OBSERVACAO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'R') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791569,
								'DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO ||
								';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AR',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

		if	 nvl(:old.QT_MATERIAL,'-1') <> nvl(:new.QT_MATERIAL,'-1') and
			(nvl(:old.ie_tipo_informacao,'X') = 'R') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791572,
								'QT_MATERIAL_OLD='||:old.QT_MATERIAL ||
								';QT_MATERIAL_NEW='||:new.QT_MATERIAL ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AR',ds_alteracao_w,:new.nm_usuario);
			end if;
		end if;

		if	 nvl(:old.IE_DISPENSADO,'XPTO') <> nvl(:new.IE_DISPENSADO,'XPTO') and
			(nvl(:old.ie_tipo_informacao,'X') = 'R') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791576,
								'IE_DISPENSADO_OLD='||:old.IE_DISPENSADO ||
								';IE_DISPENSADO_NEW='||:new.IE_DISPENSADO ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

			if	(ds_alteracao_w is not null) then
				gravar_hist_planeja_consig(nr_seq_agenda_w,'AR',ds_alteracao_w,:new.nm_usuario);
			end if;

		end if;
	end if;

	if	nvl(:old.QT_MATERIAL,'-1') <> nvl(:new.QT_MATERIAL,'-1') and
		(nvl(:old.ie_tipo_informacao,'X') = 'DE') then
			ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791572,
								'QT_MATERIAL_OLD='||:old.QT_MATERIAL ||
								';QT_MATERIAL_NEW='||:new.QT_MATERIAL ||
								';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ADE',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'RE') then

		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791577,
							'IE_RETIRADA_OLD='||:old.ie_retirada ||
							';IE_RETIRADA_NEW='||:new.ie_retirada ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ARE',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if	nvl(:old.QT_MATERIAL,'-1') <> nvl(:new.QT_MATERIAL,'-1') and
		(nvl(:old.ie_tipo_informacao,'X') = 'DI') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791572,
							'QT_MATERIAL_OLD='||:old.QT_MATERIAL ||
							';QT_MATERIAL_NEW='||:new.QT_MATERIAL ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ADI',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if	nvl(:old.QT_MATERIAL,'-1') <> nvl(:new.QT_MATERIAL,'-1') and
		(nvl(:old.ie_tipo_informacao,'X') = 'CO') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791572,
							'QT_MATERIAL_OLD='||:old.QT_MATERIAL ||
							';QT_MATERIAL_NEW='||:new.QT_MATERIAL ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ACO',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;



elsif   (:new.NR_SEQ_ANTERIOR is null) then

	if (nvl(:new.ie_tipo_informacao,'X') = 'C') then
		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791578,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IC',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'M') then

		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		cme_preparado_w := substr(:new.ds_cme_preparado,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791579,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_CME_PREPARADO='||cme_preparado_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ICM',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'A') then

		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791578,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IAG',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'E') then

		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791578,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IE',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'F') then

		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791580,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IF',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'R') then

		ds_inclusao_w := substr(:new.DS_OBSERVACAO,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791580,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IR',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'DE') then

		ds_inclusao_w := substr(:new.qt_material,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791581,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IDE',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'DI') then

		ds_inclusao_w := substr(:new.qt_material,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791581,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IDI',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'CO') then

		ds_inclusao_w := substr(:new.qt_material,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791581,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ICO',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

	if (nvl(:new.ie_tipo_informacao,'X') = 'RE') then

		ds_inclusao_w := substr(:new.IE_RETIRADA,1,255);
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791582,
							'DS_INCLUSAO='||ds_inclusao_w ||
							';DS_SOLIC_MEDICA='||solicitacao_medica_w),1,4000);

		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'IRE',ds_alteracao_w,:new.nm_usuario);
		end if;
	end if;

end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_pac_consig_inf_delete
before delete ON TASY.AGENDA_PAC_CONSIG_INF for each row
declare
ds_alteracao_w		varchar2(4000) := null;
nr_seq_agenda_w		number(10);
solicitacao_medica_w	varchar2(255);

begin

begin
select max(nr_seq_agenda),
       max(ds_material)
into   nr_seq_agenda_w,
       solicitacao_medica_w
from   agenda_pac_consignado
where  nr_sequencia = :old.nr_seq_age_consig;
exception
when others then
	nr_seq_agenda_w := 0;
end;

if	(nr_seq_agenda_w > 0) then

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'C') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EC',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'DE') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EDE',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'DI') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'EDI',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(:old.qt_material is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'CO') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796525,
								'QT_MATERIAL='||:old.qt_material||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ECO',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(:old.ie_retirada is not null) and
		(nvl(:old.ie_tipo_informacao,'X') = 'RE') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796527,
								'IE_RETIRADA='||:old.ie_retirada||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
			gravar_hist_planeja_consig(nr_seq_agenda_w,'ERE',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;


	if	((nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') or (nvl(:old.DS_CME_PREPARADO,'XPTO') <> 'XPTO')) and
		(nvl(:old.ie_tipo_informacao,'X') = 'M') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796528,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_CME_PREPARADO='||:old.DS_CME_PREPARADO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'ECM',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'A') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EAG',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'E') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EE',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'F') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'EF',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;

	if	(nvl(:old.DS_OBSERVACAO,'XPTO') <> 'XPTO') and
		(nvl(:old.ie_tipo_informacao,'X') = 'R') then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(796514,
								'DS_OBSERVACAO='||:old.DS_OBSERVACAO||
								';DS_MATERIAL='||solicitacao_medica_w),1,4000);
		if	(ds_alteracao_w is not null) then
		gravar_hist_planeja_consig(nr_seq_agenda_w,'ER',ds_alteracao_w,:old.nm_usuario);
		end if;
	end if;
end if;


end;
/


ALTER TABLE TASY.AGENDA_PAC_CONSIG_INF ADD (
  CONSTRAINT AGPACOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_CONSIG_INF ADD (
  CONSTRAINT AGPACOI_AGPACON_FK 
 FOREIGN KEY (NR_SEQ_AGE_CONSIG) 
 REFERENCES TASY.AGENDA_PAC_CONSIGNADO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_PAC_CONSIG_INF TO NIVEL_1;

