ALTER TABLE TASY.ATEND_AVAL_ANALGESIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_AVAL_ANALGESIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_AVAL_ANALGESIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_RAMSAY                  NUMBER(3),
  IE_PRURIDO                 NUMBER(3),
  IE_NAUSEA                  NUMBER(3),
  IE_BLOQUEIO_MOTOR          NUMBER(3),
  IE_CATETER_PERIDURAL       NUMBER(3),
  IE_RASS                    NUMBER(3),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  IE_RICKER                  NUMBER(1),
  IE_MAAS                    NUMBER(1),
  IE_COOK_AO                 NUMBER(1),
  IE_COOK_RESP_PROCD_ENF     NUMBER(1),
  IE_COOK_TOSSE              NUMBER(1),
  IE_COOK_RESPIRACAO         NUMBER(1),
  IE_COMUNIC_ESPONTANEA      VARCHAR2(1 BYTE),
  QT_COOK_PONTUACAO          NUMBER(3),
  QT_MAEC                    NUMBER(3),
  NR_HORA                    NUMBER(2),
  IE_RELEVANTE_APAP          VARCHAR2(1 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_REFERENCIA              DATE,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_ANALGESIA           NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_POSICAO_CATETER         VARCHAR2(5 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_POSS                NUMBER(2),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  QT_NPASS_PONTUACAO         NUMBER(2),
  QT_POTENCIA_EMG            NUMBER(3),
  QT_TX_SUPRESSAO            NUMBER(3),
  IE_INTEGRACAO              VARCHAR2(1 BYTE),
  QT_COMFORT_B               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEAVAN_ATCONSPEPA_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEAVAN_ATEPACI_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEAVAN_I1 ON TASY.ATEND_AVAL_ANALGESIA
(DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEAVAN_MODANAL_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQ_ANALGESIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_MODANAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_PEPOCIR_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_PERFIL_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_PESFISI_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEAVAN_PK ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_PK
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_SETATEN_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_TASASDI_FK_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEAVAN_TASASDI_FK2_I ON TASY.ATEND_AVAL_ANALGESIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEAVAN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATEND_AVAL_ANALG_pend_atual 
after insert or update ON TASY.ATEND_AVAL_ANALGESIA 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_lib_sinal_vital_w	varchar2(10); 
cd_pessoa_fisica_w		varchar2(30); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_lib_sinal_vital) 
into	ie_lib_sinal_vital_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'SVMA'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XSVMA'; 
	end if; 
	 
	select 	max(cd_pessoa_fisica) 
	into	cd_pessoa_fisica_w 
	from	atendimento_paciente 
	where 	nr_atendimento	=	:new.nr_atendimento; 
 
	begin 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario); 
	end if; 
	exception 
		when others then 
		null; 
	end; 
	 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.atend_aval_analgesia_atual
before insert or update ON TASY.ATEND_AVAL_ANALGESIA for each row
declare

qt_comunic_w	number(10,0):= 0;
ds_hora_w	varchar2(20);
dt_registro_w	date;
dt_apap_w	date;
qt_hora_w	number(15,2);
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.nr_hora is null) or
	(:new.dt_avaliacao <> :old.dt_avaliacao) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_avaliacao,'hh24'),'hh24'));
	end;
end if;

if	(nvl(:old.DT_AVALIACAO,sysdate+10) <> :new.DT_AVALIACAO) and
	(:new.DT_AVALIACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_AVALIACAO,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_avaliacao is null) or
	 (:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_avaliacao <> :old.dt_avaliacao)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_avaliacao,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_avaliacao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_avaliacao,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_referencia	:= round(:new.dt_avaliacao,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_avaliacao,'hh24') - to_date(to_char(:new.dt_avaliacao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_avaliacao + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_avaliacao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_avaliacao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_avaliacao - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;

if	(:new.ie_comunic_espontanea = 'S') then
	qt_comunic_w := 2;
elsif	(:new.ie_comunic_espontanea = 'N') then
	qt_comunic_w := 0;
end if;

:new.qt_cook_pontuacao :=	nvl(:new.ie_cook_ao,0)		+	nvl(:new.ie_cook_resp_procd_enf,0)	+
				nvl(:new.ie_cook_tosse,0)	+	nvl(:new.ie_cook_respiracao,0)		+
				nvl(qt_comunic_w,0);

if (inserting) and (nvl(:new.ie_integracao,'N')	= 'S') then
   record_integration_notify(null,:new.nr_atendimento,'MA',:new.nr_sequencia,null,'S');
end if;

:new.qt_npass_pontuacao := obtem_desc_escala_npass(:new.nr_atendimento);


if	(:old.DT_LIBERACAO is null) and	(:new.DT_LIBERACAO is not null) then
	select count(1)
	into   qt_reg_w
	from   escala_comfort_b
	where  nr_seq_analgesia = :new.nr_sequencia;

	if (qt_reg_w > 0) then
		update escala_comfort_b
		set    dt_liberacao = sysdate
		where  nr_seq_analgesia = :new.nr_sequencia;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ATEND_AVAL_ANALGESIA ADD (
  CONSTRAINT ATEAVAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_AVAL_ANALGESIA ADD (
  CONSTRAINT ATEAVAN_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEAVAN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEAVAN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEAVAN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEAVAN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEAVAN_MODANAL_FK 
 FOREIGN KEY (NR_SEQ_ANALGESIA) 
 REFERENCES TASY.MODALIDADE_ANALGESIA (NR_SEQUENCIA),
  CONSTRAINT ATEAVAN_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEAVAN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEAVAN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_AVAL_ANALGESIA TO NIVEL_1;

