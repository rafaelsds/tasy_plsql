ALTER TABLE TASY.PLS_ECARTA_MATRIZ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ECARTA_MATRIZ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ECARTA_MATRIZ
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_IDENTIFICADOR         VARCHAR2(255 BYTE)   NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DS_MATRIZ                VARCHAR2(255 BYTE)   NOT NULL,
  QT_LIMITE_DIARIO         NUMBER(10)           NOT NULL,
  IE_AR_DIGITAL            VARCHAR2(1 BYTE)     NOT NULL,
  IE_ARQUIVO_COMPLEMENTAR  VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_CARTAO_POSTAGEM       NUMBER(10),
  NR_CONTRATO_CORREIOS     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTRZ_ESTABEL_FK_I ON TASY.PLS_ECARTA_MATRIZ
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTRZ_PK ON TASY.PLS_ECARTA_MATRIZ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTRZ_UK ON TASY.PLS_ECARTA_MATRIZ
(CD_ESTABELECIMENTO, CD_IDENTIFICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ECARTA_MATRIZ ADD (
  CONSTRAINT PLSMTRZ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSMTRZ_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_IDENTIFICADOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          32K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ECARTA_MATRIZ ADD (
  CONSTRAINT PLSMTRZ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_ECARTA_MATRIZ TO NIVEL_1;

