ALTER TABLE TASY.FIS_EFD_ICMSIPI_C460
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_ICMSIPI_C460 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_ICMSIPI_C460
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  CD_MOD               VARCHAR2(2 BYTE),
  CD_SIT               VARCHAR2(2 BYTE),
  NR_DOC               VARCHAR2(9 BYTE),
  DT_DOC               DATE,
  VL_DOC               NUMBER(15,2),
  VL_PIS               NUMBER(15,2),
  VL_COFINS            NUMBER(15,2),
  CD_CPF_CNPJ          VARCHAR2(14 BYTE),
  DS_NOM_ADQ           VARCHAR2(60 BYTE),
  NR_ECF_CX            VARCHAR2(3 BYTE),
  NR_SEQ_CONTROLE      NUMBER(10),
  NR_SEQ_NOTA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ICMSC460_EFDICMSCON_FK_I ON TASY.FIS_EFD_ICMSIPI_C460
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ICMSC460_NOTFISC_FK_I ON TASY.FIS_EFD_ICMSIPI_C460
(NR_SEQ_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ICMSC460_PK ON TASY.FIS_EFD_ICMSIPI_C460
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_EFD_ICMSIPI_C460 ADD (
  CONSTRAINT ICMSC460_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_EFD_ICMSIPI_C460 ADD (
  CONSTRAINT ICMSC460_EFDICMSCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ICMSC460_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_EFD_ICMSIPI_C460 TO NIVEL_1;

