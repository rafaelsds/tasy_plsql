ALTER TABLE TASY.W_KIT_ESTOQUE_COMP_PDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_KIT_ESTOQUE_COMP_PDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_KIT_ESTOQUE_COMP_PDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(6),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE),
  NR_SEQ_LOTE_FORNEC   NUMBER(10),
  CD_KIT_MATERIAL      NUMBER(10),
  NR_SEQ_KIT_ESTOQUE   NUMBER(10),
  NR_SEQ_MAP           NUMBER(10),
  QT_MATERIAL          NUMBER(11,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WKTCPDA_PK ON TASY.W_KIT_ESTOQUE_COMP_PDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WKTCPDA_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_KIT_ESTOQUE_COMP_PDA ADD (
  CONSTRAINT WKTCPDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_KIT_ESTOQUE_COMP_PDA TO NIVEL_1;

