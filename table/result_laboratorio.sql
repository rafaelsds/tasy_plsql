ALTER TABLE TASY.RESULT_LABORATORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULT_LABORATORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULT_LABORATORIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_PRESCRICAO           NUMBER(14),
  NR_SEQ_PRESCRICAO       NUMBER(6),
  NM_USUARIO              VARCHAR2(15 BYTE),
  DT_ATUALIZACAO          DATE,
  DS_RESULTADO            LONG,
  IE_COBRANCA             VARCHAR2(1 BYTE),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_PROCEDIMENTO         NUMBER(15),
  DT_COLETA               DATE,
  IE_STATUS_CONVERSAO     NUMBER(1)             NOT NULL,
  DS_RESULT_CODIGO        VARCHAR2(2000 BYTE),
  IE_FORMATO_TEXTO        NUMBER(2),
  NR_SEQ_EXAME            NUMBER(10),
  NR_SEQ_PAGINA           NUMBER(10),
  DT_RETIFICACAO          DATE,
  IE_STATUS               VARCHAR2(2 BYTE),
  IE_FINAL                VARCHAR2(1 BYTE),
  NR_SEQ_RESULTADO        NUMBER(10),
  NR_SEQ_MATERIAL_INTEGR  NUMBER(10),
  CD_EXAME_INTEGRACAO     VARCHAR2(20 BYTE),
  IE_RESULTADO_CRITICO    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1093M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RESLABO_I1 ON TASY.RESULT_LABORATORIO
(IE_STATUS_CONVERSAO, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          29M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLABO_I1
  MONITORING USAGE;


CREATE INDEX TASY.RESLABO_I2 ON TASY.RESULT_LABORATORIO
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLABO_I2
  MONITORING USAGE;


CREATE INDEX TASY.RESLABO_I3 ON TASY.RESULT_LABORATORIO
(NR_SEQ_RESULTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESLABO_MATEXLAINT_FK_I ON TASY.RESULT_LABORATORIO
(NR_SEQ_MATERIAL_INTEGR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESLABO_MATEXLAINT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RESLABO_PK ON TASY.RESULT_LABORATORIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESLABO_PRESPRO_FK_I ON TASY.RESULT_LABORATORIO
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          25M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.result_laboratorio_Insert
BEFORE INSERT ON TASY.RESULT_LABORATORIO FOR EACH ROW
DECLARE

ie_conta_lab_ext_w		Varchar2(1);
ie_laudo_texto_w		Varchar2(1);
ie_status_baixa_w		number(2);
cd_estabelecimento_w		number(5);
nr_seq_exame_w			number(10);
ie_atual_status_integracao_w	varchar2(1);
ie_atual_status_exec_laudo_w	varchar2(1);
ie_existe_w			varchar2(1);
cd_material_exame_w		material_exame_lab.cd_material_exame%type;
qt_exame_w 			number(10);
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
ds_codigo_prof_w		pessoa_fisica.ds_codigo_prof%type;
cd_medico_resp_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_exec_w		prescr_procedimento.cd_medico_exec%type;
cd_medico_w				prescr_medica.cd_medico%type;
ie_atualiza_data_coleta_w	lab_parametro.ie_atualiza_data_coleta%type;

cursor c01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.ie_evento_disp = 'APRPRPI'
	and	nvl(a.ie_situacao,'A') = 'A';

c01_w	c01%rowtype;

BEGIN

if 	(:new.nr_seq_resultado is not null) then

	:new.IE_COBRANCA := 'N';
else
	if 	(:new.NR_PRESCRICAO is null) then
		--'O campo prescricao deve ser informado !
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1061479);
	end if;

	if (:new.NR_SEQ_PRESCRICAO is null) then
		--'O campo sequencia deve ser informado !
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1061480);
	end if;

	if (:new.IE_COBRANCA is null) then
		--'The billing field must be filled in !
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1061482);
	end if;

end if;

:new.ie_status_conversao := 0;

if nvl(:new.nr_sequencia, 0) <= 0 then
	select RESULT_LABORATORIO_SEQ.nextval
	into :new.nr_sequencia
	from dual;
end if;

:new.dt_atualizacao	:= sysdate;
if  	(:new.nm_usuario is null) then
	:new.nm_usuario	:= wheb_mensagem_pck.get_texto(309720); -- Laboratory
end if;

select	nvl(max(nr_seq_exame),null),
	nvl(max(cd_procedimento),:new.cd_procedimento),
	nvl(max(ie_origem_proced),:new.ie_origem_proced),
	MAX(cd_medico_exec)
into	nr_seq_exame_w,
	:new.cd_procedimento,
	:new.ie_origem_proced,
	cd_medico_exec_w
from	prescr_procedimento
where 	nr_prescricao	= :new.nr_prescricao
  and	nr_sequencia	= :new.nr_seq_prescricao;

select	nvl(max(ie_conta_lab_ext),'N'),
	nvl(max(ie_laudo_texto),'N'),
	coalesce(max(ie_status_baixa_integ),max(ie_status_baixa),99),
	nvl(max(c.cd_estabelecimento),1),
	nvl(max(ie_atual_status_integracao),'N'),
	nvl(max(ie_atualiza_status_exec_laudo),'N'),
	MAX(a.cd_medico),
	MAX(nvl(c.ie_atualiza_data_coleta,'N'))
into	ie_conta_lab_ext_w,
	ie_laudo_texto_w,
	ie_status_baixa_w,
	cd_estabelecimento_w,
	ie_atual_status_integracao_w,
	ie_atual_status_exec_laudo_w,
	cd_medico_w,
	ie_atualiza_data_coleta_w
from 	Lab_parametro c,
	Atendimento_paciente b,
	prescr_medica a
where	a.nr_prescricao		= :new.nr_prescricao
  and a.nr_atendimento		= b.nr_atendimento
  and	b.cd_estabelecimento	= c.cd_estabelecimento;

if 	(:new.IE_COBRANCA = 'S') and
	((ie_conta_lab_ext_w = 'R') or
	 (ie_conta_lab_ext_w = 'E' and lab_obter_regra_fatur(cd_estabelecimento_w, nr_seq_exame_w) = 'R')) then
	Gerar_Proc_Pac_Prescricao(:new.nr_prescricao, :new.nr_seq_prescricao, 0, 0, :new.nm_usuario, null, null,null);
end if;


if (nvl(:new.ie_final,'S') = 'S') then

	update prescr_procedimento
	set	cd_motivo_baixa		= 1,
			dt_baixa 		= sysdate,
		ie_status_atend		= 40,
		ie_status_execucao	= decode(ie_atual_status_exec_laudo_w,'S','40',ie_status_execucao),
		nm_usuario		= :new.nm_usuario,
		dt_atualizacao		= sysdate,
		dt_coleta		= decode(ie_atualiza_data_coleta_w, 'S', nvl(:new.dt_coleta, dt_coleta), dt_coleta)
	where nr_prescricao		= :new.nr_prescricao
	  and nr_sequencia		= :new.nr_seq_prescricao
	  and cd_motivo_baixa	= 0
	  and ie_status_atend	= decode(ie_status_baixa_w,99,ie_status_atend,ie_status_baixa_w);

	if (nvl(ie_atual_status_integracao_w,'N') = 'S') then
		update prescr_procedimento
		set	ie_status_atend	= 40,
			ie_status_execucao	= decode(ie_atual_status_exec_laudo_w,'S','40',ie_status_execucao),
			nm_usuario		= :new.nm_usuario
		where nr_prescricao		= :new.nr_prescricao
		  and nr_sequencia		= :new.nr_seq_prescricao
		  and ie_status_atend	= decode(ie_status_baixa_w,99,ie_status_atend,ie_status_baixa_w)
		  and dt_integracao is not null;
	end if;
	if (:new.nr_prescricao is not null) then
		open C01;
		loop
		fetch C01 into
			c01_w;
		exit when C01%notfound;
			begin

			select	count(*)
			into	qt_exame_w
			from	prescr_procedimento a,
					prescr_medica b
			where	a.ie_status_atend >= 35
			and	a.nr_prescricao = b.nr_prescricao
			and	b.nr_prescricao = :new.nr_prescricao
			and	a.nr_sequencia 	<> :new.nr_seq_prescricao;

			if (qt_exame_w = 0) then

				cd_medico_resp_w := nvl(cd_medico_exec_w,cd_medico_w);

				if	(:new.nm_usuario is not null) then
					select	nvl(max(cd_pessoa_fisica),null)
					into	cd_pessoa_fisica_w
					from	usuario
					where	upper(nm_usuario) = upper(:new.nm_usuario);

					if	(cd_pessoa_fisica_w is not null) then
						select 	nvl(max(substr(obter_crm_medico(cd_pessoa_fisica),1, 255)),'')
						into	ds_codigo_prof_w
						from 	medico
						where 	cd_pessoa_fisica = cd_pessoa_fisica_w;

						select 	nvl(max(ds_codigo_prof), ds_codigo_prof_w)
						into	ds_codigo_prof_w
						from 	pessoa_fisica
						where 	cd_pessoa_fisica = cd_pessoa_fisica_w
						and 	(length(ds_codigo_prof_w) = 0 or ds_codigo_prof_w is null);

						if	(ds_codigo_prof_w is not null) then
							cd_medico_resp_w := cd_pessoa_fisica_w;
						end if;
					end if;
				end if;
				gerar_evento_aprov_res_atend(c01_w.nr_seq_evento,:new.nm_usuario, :new.nr_prescricao, :new.nr_seq_prescricao, nr_seq_exame_w, 'S', sysdate, cd_medico_resp_w);
			end if;

			end;
		end loop;
		close C01;
	end if;
	if  (ie_atualiza_data_coleta_w = 'S') and
		(:new.dt_coleta is not null ) then

		update  prescr_procedimento
		set		dt_coleta = :new.dt_coleta,
				nm_usuario = :new.nm_usuario,
				dt_atualizacao = sysdate
		where   nr_prescricao  = :new.nr_prescricao
		and		nr_sequencia   = :new.nr_seq_prescricao;

	end if;

elsif (nvl(:new.ie_final,'S') = 'N') then

	update 	prescr_procedimento
	set	ie_status_atend		= 30
	where	nr_prescricao		= :new.nr_prescricao
	and 	nr_sequencia		= :new.nr_seq_prescricao;

end if;

select	decode(count(*),0,'N','S')
into	ie_existe_w
from	prescr_procedimento a
where	nr_prescricao		= :new.nr_prescricao
and 	nr_sequencia		= :new.nr_seq_prescricao
and	nvl(ie_result_lab_gerado,'N') = 'N';

if	(ie_existe_w = 'S') then

	/*if	(wheb_usuario_pck.is_evento_ativo(221) = 'S') then

		select	max(b.cd_material_exame)
		into	cd_material_exame_w
		from	prescr_medica a,
			prescr_procedimento b
		where	a.nr_prescricao = b.nr_prescricao
		and	b.nr_prescricao = :new.nr_prescricao
		and 	b.nr_sequencia	= :new.nr_seq_prescricao;

		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	lab_tasylab_cli_prescr a
		where	a.nr_prescricao = :new.nr_prescricao;

		if	(ie_existe_w = 'S') then
			integrar_tasylab(	:new.nr_prescricao,
						:new.nr_seq_prescricao,
						221,
						cd_material_exame_w,
						null,
						wheb_usuario_pck.get_cd_funcao,
						obter_perfil_ativo,
						wheb_usuario_pck.get_nm_usuario,
						wheb_usuario_pck.get_cd_estabelecimento,
						null,
						null,
						'N'); --Evitar commit na trigger.
		end if;
	end if;*/

	update 	prescr_procedimento
	set	ie_result_lab_gerado = 'S'
	where	nr_prescricao		= :new.nr_prescricao
	and 	nr_sequencia		= :new.nr_seq_prescricao
	and	nvl(ie_result_lab_gerado,'N') = 'N';
end if;

END;
/


ALTER TABLE TASY.RESULT_LABORATORIO ADD (
  CONSTRAINT RESLABO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          14M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULT_LABORATORIO ADD (
  CONSTRAINT RESLABO_MATEXLAINT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_INTEGR) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT RESLABO_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RESULT_LABORATORIO TO NIVEL_1;

