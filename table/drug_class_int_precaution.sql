ALTER TABLE TASY.DRUG_CLASS_INT_PRECAUTION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DRUG_CLASS_INT_PRECAUTION CASCADE CONSTRAINTS;

CREATE TABLE TASY.DRUG_CLASS_INT_PRECAUTION
(
  PRECAUTIONID          NUMBER(15)              NOT NULL,
  DRUGINTERACTCLASSID1  NUMBER(15)              NOT NULL,
  DRUGINTERACTCLASSID2  NUMBER(15)              NOT NULL,
  VERSION               NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DRGPREC_PK ON TASY.DRUG_CLASS_INT_PRECAUTION
(DRUGINTERACTCLASSID1, DRUGINTERACTCLASSID2, PRECAUTIONID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DRUG_CLASS_INT_PRECAUTION ADD (
  CONSTRAINT DRGPREC_PK
 PRIMARY KEY
 (DRUGINTERACTCLASSID1, DRUGINTERACTCLASSID2, PRECAUTIONID));

