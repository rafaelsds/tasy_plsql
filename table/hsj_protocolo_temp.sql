DROP TABLE TASY.HSJ_PROTOCOLO_TEMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_PROTOCOLO_TEMP
(
  NR_ATENDIMENTO    NUMBER(10),
  NR_INTERNO_CONTA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


