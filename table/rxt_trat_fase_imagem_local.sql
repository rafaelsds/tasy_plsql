ALTER TABLE TASY.RXT_TRAT_FASE_IMAGEM_LOCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_TRAT_FASE_IMAGEM_LOCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_TRAT_FASE_IMAGEM_LOCAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_FASE          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DS_TITULO            VARCHAR2(40 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RXTRILO_PK ON TASY.RXT_TRAT_FASE_IMAGEM_LOCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTRILO_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTRILO_RXTFATR_FK_I ON TASY.RXT_TRAT_FASE_IMAGEM_LOCAL
(NR_SEQ_FASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTRILO_RXTFATR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RXT_TRAT_FASE_IMAGEM_LOCAL ADD (
  CONSTRAINT RXTRILO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_TRAT_FASE_IMAGEM_LOCAL ADD (
  CONSTRAINT RXTRILO_RXTFATR_FK 
 FOREIGN KEY (NR_SEQ_FASE) 
 REFERENCES TASY.RXT_FASE_TRATAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RXT_TRAT_FASE_IMAGEM_LOCAL TO NIVEL_1;

